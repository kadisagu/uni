package ru.tandemservice.aspirantrmc.base.ext.ExternalOrgUnit.ui.WizardStep3Add;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.shared.ctr.base.bo.ExternalOrgUnit.ExternalOrgUnitManager;
import org.tandemframework.shared.ctr.base.bo.ExternalOrgUnit.ui.WizardStep3Add.ExternalOrgUnitWizardStep3AddUI;
import ru.tandemservice.aspirantrmc.base.ext.ExternalOrgUnit.utils.UICommonAddon;

public class ExternalOrgUnitWizardStep3AddUIExt extends UICommonAddon {

    ExternalOrgUnitWizardStep3AddUI parent = getPresenter();

    public ExternalOrgUnitWizardStep3AddUIExt(IUIPresenter presenter, String name, String componentId) {
        super(presenter, name, componentId);
    }

    public void onClickApply()
    {
        ExternalOrgUnitManager.instance().dao().updateExternalOrgUnit(parent.getOu());
        super.onClickApply(parent, parent.getOu());
    }
}
