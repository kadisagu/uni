package ru.tandemservice.aspirantrmc.base.bo.PersonnelResource.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.component.impl.BusinessComponent;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.employeebase.base.bo.Employee.logic.EmployeePostDSHandler;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.DataEdit.EmployeeDataEdit;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostAddEdit.EmployeePostAddEdit;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import org.tandemframework.shared.person.base.bo.Person.util.SecureRoleContext;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.aspirantrmc.base.bo.PersonnelResource.PersonnelResourceManager;
import ru.tandemservice.aspirantrmc.base.bo.PersonnelResource.ui.Add.PersonnelResourceAdd;
import ru.tandemservice.aspirantrmc.entity.AspirantToEmployee;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.ArrayList;
import java.util.List;

@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "student.id"),
        @Bind(key = "selectedTab", binding = "selectedTab"),
        @Bind(key = "selectedDataTab", binding = "selectedDataTab")
})

public class PersonnelResourcePubUI extends UIPresenter {

    public static final String EMPLOYEE_TAB_PANEL_REGION_NAME = "employeeTabPanel";

    private Employee _employee = new Employee();
    private Student student = new Student();
    private String _selectedTab;
    private String _selectedDataTab;
    private boolean _archivingDisabled;
    private List<PersonEduInstitution> _personEduInstitutionList = new ArrayList<PersonEduInstitution>();
    private String _qualification;

    @Override
    public void onComponentRefresh()
    {
        student = DataAccessServices.dao().getNotNull(Student.class, student.getId());
        _employee.setEmployeeCode(" ");
        if (getEmployee(student) != null) {

            _employee = getEmployee(student);
            _archivingDisabled = !PersonnelResourceManager.instance().dao().isEmployeeArchivable(_employee);
            _personEduInstitutionList = PersonnelResourceManager.instance().dao().getPersonEduInstitution(_employee.getPerson().getId());
            StringBuilder stringBuilder = new StringBuilder();
            if (_employee.getPerson().getPersonEduInstitution() != null) {
                if (_employee.getPerson().getPersonEduInstitution().getDiplomaQualification() != null)
                    stringBuilder.append(_employee.getPerson().getPersonEduInstitution().getDiplomaQualification().getTitle());
                for (PersonEduInstitution personEduInstitution : _personEduInstitutionList)
                    stringBuilder.append("<p/>").append(personEduInstitution.getDiplomaQualification().getTitle());
            }
            _qualification = stringBuilder.toString();
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (PersonnelResourcePub.EMPLOYEE_POST_DS.equals(dataSource.getName()))
            dataSource.put(EmployeePostDSHandler.EMPLOYEE_ID, _employee.getId());
    }

    // Util

    public boolean isAccessible()
    {
        return !_employee.isArchival();
    }

    public boolean isActive() {
        return getEmployee(student) == null && isAccessible();
    }

    public ISecureRoleContext getSecureRoleContext()
    {
        return SecureRoleContext.instance(_employee).personRoleName(Employee.class.getSimpleName()).accessible(isAccessible());
    }

    public Employee getEmployee(Student aspirant) {
        MQBuilder builder = new MQBuilder(AspirantToEmployee.ENTITY_CLASS, "ae")
                .add(MQExpression.eq("ae", AspirantToEmployee.L_ASPIRANT, aspirant));
        if (builder.uniqueResult(_uiSupport.getSession()) == null)
            return null;
        else
            return ((AspirantToEmployee) builder.uniqueResult(_uiSupport.getSession())).getEmployee();
    }

    // Getters & Setters


    public Employee getEmployee()
    {
        return _employee;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public void setEmployee(Employee employee)
    {
        _employee = employee;
    }

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }

    public String getSelectedDataTab()
    {
        return _selectedDataTab;
    }

    public void setSelectedDataTab(String selectedDataTab)
    {
        _selectedDataTab = selectedDataTab;
    }

    public boolean isArchivingDisabled()
    {
        return _archivingDisabled;
    }

    public void setArchivingDisabled(boolean archivingDisabled)
    {
        _archivingDisabled = archivingDisabled;
    }

    public List<PersonEduInstitution> getPersonEduInstitutionList() {
        return _personEduInstitutionList;
    }

    public void setPersonEduInstitutionList(List<PersonEduInstitution> personEduInstitutionList) {
        this._personEduInstitutionList = personEduInstitutionList;
    }

    public String getQualification() {
        return _qualification;
    }

    public void setQualification(String qualification) {
        this._qualification = qualification;
    }

// Listeners

    public void onClickArchive()
    {
        PersonnelResourceManager.instance().dao().doArchiveEmployee(_employee.getId());

        //TODO: hack #538
        ((BusinessComponent) (getConfig().getBusinessComponent().getParentRegion()).getOwner()).refresh();
    }

    public void onClickUnArchive()
    {
        PersonnelResourceManager.instance().dao().doUnArchiveEmployee(_employee.getId());

        //TODO: hack #538
        ((BusinessComponent) (getConfig().getBusinessComponent().getParentRegion()).getOwner()).refresh();
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegion(EmployeePostAddEdit.class.getSimpleName(), EMPLOYEE_TAB_PANEL_REGION_NAME)
                .parameter("employeePostId", getListenerParameterAsLong())
                .activate();
    }

    public void onDeleteEntityFromList()
    {
        PersonnelResourceManager.instance().dao().deleteEmployeePost(getListenerParameterAsLong());
    }

    public void onClickAddEmployee() {
        _uiActivation.asRegion(PersonnelResourceAdd.class.getSimpleName(), EMPLOYEE_TAB_PANEL_REGION_NAME)
                .parameter("studentId", student.getId())
                .activate();
    }

    public void onClickEditEmployeeData()
    {
        _uiActivation.asRegion(EmployeeDataEdit.class.getSimpleName(), EMPLOYEE_TAB_PANEL_REGION_NAME)
                .parameter("employeeId", _employee.getId())
                .activate();
    }

    public void onClickAddEmployeePost()
    {
        _uiActivation.asRegion(EmployeePostAddEdit.class.getSimpleName(), EMPLOYEE_TAB_PANEL_REGION_NAME)
                .parameter("employeeId", _employee.getId())
                .activate();
    }
}
