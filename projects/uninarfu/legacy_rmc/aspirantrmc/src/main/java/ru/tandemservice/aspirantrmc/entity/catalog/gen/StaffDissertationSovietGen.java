package ru.tandemservice.aspirantrmc.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.aspirantrmc.entity.catalog.StaffDissertationSoviet;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Состав диссертационных советов
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StaffDissertationSovietGen extends EntityBase
 implements INaturalIdentifiable<StaffDissertationSovietGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.aspirantrmc.entity.catalog.StaffDissertationSoviet";
    public static final String ENTITY_NAME = "staffDissertationSoviet";
    public static final int VERSION_HASH = -896442672;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_LAST_NAME = "lastName";
    public static final String P_FIRST_NAME = "firstName";
    public static final String P_MIDDLE_NAME = "middleName";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _lastName;     // Фамилия
    private String _firstName;     // Имя
    private String _middleName;     // Отчество
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Фамилия. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getLastName()
    {
        return _lastName;
    }

    /**
     * @param lastName Фамилия. Свойство не может быть null.
     */
    public void setLastName(String lastName)
    {
        dirty(_lastName, lastName);
        _lastName = lastName;
    }

    /**
     * @return Имя. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFirstName()
    {
        return _firstName;
    }

    /**
     * @param firstName Имя. Свойство не может быть null.
     */
    public void setFirstName(String firstName)
    {
        dirty(_firstName, firstName);
        _firstName = firstName;
    }

    /**
     * @return Отчество. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getMiddleName()
    {
        return _middleName;
    }

    /**
     * @param middleName Отчество. Свойство не может быть null.
     */
    public void setMiddleName(String middleName)
    {
        dirty(_middleName, middleName);
        _middleName = middleName;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StaffDissertationSovietGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((StaffDissertationSoviet)another).getCode());
            }
            setLastName(((StaffDissertationSoviet)another).getLastName());
            setFirstName(((StaffDissertationSoviet)another).getFirstName());
            setMiddleName(((StaffDissertationSoviet)another).getMiddleName());
            setTitle(((StaffDissertationSoviet)another).getTitle());
        }
    }

    public INaturalId<StaffDissertationSovietGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<StaffDissertationSovietGen>
    {
        private static final String PROXY_NAME = "StaffDissertationSovietNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof StaffDissertationSovietGen.NaturalId) ) return false;

            StaffDissertationSovietGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StaffDissertationSovietGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StaffDissertationSoviet.class;
        }

        public T newInstance()
        {
            return (T) new StaffDissertationSoviet();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "lastName":
                    return obj.getLastName();
                case "firstName":
                    return obj.getFirstName();
                case "middleName":
                    return obj.getMiddleName();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "lastName":
                    obj.setLastName((String) value);
                    return;
                case "firstName":
                    obj.setFirstName((String) value);
                    return;
                case "middleName":
                    obj.setMiddleName((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "lastName":
                        return true;
                case "firstName":
                        return true;
                case "middleName":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "lastName":
                    return true;
                case "firstName":
                    return true;
                case "middleName":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "lastName":
                    return String.class;
                case "firstName":
                    return String.class;
                case "middleName":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StaffDissertationSoviet> _dslPath = new Path<StaffDissertationSoviet>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StaffDissertationSoviet");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.aspirantrmc.entity.catalog.StaffDissertationSoviet#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Фамилия. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.catalog.StaffDissertationSoviet#getLastName()
     */
    public static PropertyPath<String> lastName()
    {
        return _dslPath.lastName();
    }

    /**
     * @return Имя. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.catalog.StaffDissertationSoviet#getFirstName()
     */
    public static PropertyPath<String> firstName()
    {
        return _dslPath.firstName();
    }

    /**
     * @return Отчество. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.catalog.StaffDissertationSoviet#getMiddleName()
     */
    public static PropertyPath<String> middleName()
    {
        return _dslPath.middleName();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.aspirantrmc.entity.catalog.StaffDissertationSoviet#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends StaffDissertationSoviet> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _lastName;
        private PropertyPath<String> _firstName;
        private PropertyPath<String> _middleName;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.aspirantrmc.entity.catalog.StaffDissertationSoviet#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(StaffDissertationSovietGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Фамилия. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.catalog.StaffDissertationSoviet#getLastName()
     */
        public PropertyPath<String> lastName()
        {
            if(_lastName == null )
                _lastName = new PropertyPath<String>(StaffDissertationSovietGen.P_LAST_NAME, this);
            return _lastName;
        }

    /**
     * @return Имя. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.catalog.StaffDissertationSoviet#getFirstName()
     */
        public PropertyPath<String> firstName()
        {
            if(_firstName == null )
                _firstName = new PropertyPath<String>(StaffDissertationSovietGen.P_FIRST_NAME, this);
            return _firstName;
        }

    /**
     * @return Отчество. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.catalog.StaffDissertationSoviet#getMiddleName()
     */
        public PropertyPath<String> middleName()
        {
            if(_middleName == null )
                _middleName = new PropertyPath<String>(StaffDissertationSovietGen.P_MIDDLE_NAME, this);
            return _middleName;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.aspirantrmc.entity.catalog.StaffDissertationSoviet#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(StaffDissertationSovietGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return StaffDissertationSoviet.class;
        }

        public String getEntityName()
        {
            return "staffDissertationSoviet";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
