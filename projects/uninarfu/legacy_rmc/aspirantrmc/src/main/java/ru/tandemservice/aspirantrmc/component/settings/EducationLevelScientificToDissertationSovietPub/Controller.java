package ru.tandemservice.aspirantrmc.component.settings.EducationLevelScientificToDissertationSovietPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;

public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = (Model) getModel(component);

        ((IDAO) getDao()).prepare(model);

        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        Model model = (Model) getModel(component);
        if (model.getDataSource() != null) {
            return;
        }

        DynamicListDataSource<Wrapper> dataSource = new DynamicListDataSource<>(component, this);

        dataSource.addColumn(new SimpleColumn("Диссертационный совет", "soviet.fullTitle").setClickable(false));
        dataSource.addColumn(new SimpleColumn("Председатель", "chairman.fullFIO").setClickable(false));
        dataSource.addColumn(new SimpleColumn("Ученый секретарь", "secretary.fullFIO").setClickable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", "edit", "onClickEditItem"));
        model.setDataSource(dataSource);
    }

    public void updateListDataSource(IBusinessComponent component) {
        ((IDAO) getDao()).prepareListDataSource(getModel(component));
    }

    protected void refresh(IBusinessComponent component)
    {
        ((Model) getModel(component)).getDataSource().refresh();
    }

    public void onClickAdd(IBusinessComponent component) {
        component.createDefaultChildRegion(new ComponentActivator("ru.tandemservice.aspirantrmc.component.settings.EducationLevelScientificToDissertationSovietAdd", ParametersMap.createWith("id", getModel(component).getEducationLevels().getId())));
    }

    public void onClickCancel(IBusinessComponent component) {
        deactivate(component);
    }

    public void onClickEditItem(IBusinessComponent component) {
        component.createDefaultChildRegion(new ComponentActivator("ru.tandemservice.aspirantrmc.component.settings.EducationLevelScientificToDissertationSovietAdd", ParametersMap.createWith("idSoviet", component.getListenerParameter())));
    }

}
