package ru.tandemservice.aspirantrmc.component.settings.EducationLevelScientificToDissertationSovietPub;

import org.tandemframework.core.entity.EntityBase;
import ru.tandemservice.aspirantrmc.entity.catalog.DissertationSoviet;
import ru.tandemservice.aspirantrmc.entity.catalog.StaffDissertationSoviet;

public class Wrapper extends EntityBase {
    private DissertationSoviet soviet;
    private StaffDissertationSoviet chairman;
    private StaffDissertationSoviet secretary;

    public Long getId() {
        return soviet.getId();
    }

    public DissertationSoviet getSoviet() {
        return soviet;
    }

    public void setSoviet(DissertationSoviet soviet) {
        this.soviet = soviet;
    }

    public StaffDissertationSoviet getChairman() {
        return chairman;
    }

    public void setChairman(StaffDissertationSoviet chairman) {
        this.chairman = chairman;
    }

    public StaffDissertationSoviet getSecretary() {
        return secretary;
    }

    public void setSecretary(StaffDissertationSoviet secretary) {
        this.secretary = secretary;
    }


}
