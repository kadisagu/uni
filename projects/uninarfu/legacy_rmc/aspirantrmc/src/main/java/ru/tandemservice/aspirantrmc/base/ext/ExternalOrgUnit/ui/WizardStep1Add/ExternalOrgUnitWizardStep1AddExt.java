package ru.tandemservice.aspirantrmc.base.ext.ExternalOrgUnit.ui.WizardStep1Add;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtension;
import org.tandemframework.caf.ui.config.datasource.IColumnListExtensionBuilder;
import org.tandemframework.caf.ui.config.presenter.IPresenterExtensionBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.ctr.base.bo.ExternalOrgUnit.ui.WizardStep1Add.ExternalOrgUnitWizardStep1Add;

@Configuration
public class ExternalOrgUnitWizardStep1AddExt extends BusinessComponentExtensionManager {
    @Autowired
    ExternalOrgUnitWizardStep1Add parent;

    @Bean
    public ColumnListExtension similarExternalOrgUnitDS()
    {
        IColumnListExtensionBuilder builder = columnListExtensionBuilder(parent.similarExternalOrgUnitDS());
        builder.addColumn(radioColumn("radio").before("title"));
        return builder.create();
    }

    @Bean
    public PresenterExtension presenterExtension() {
        IPresenterExtensionBuilder pi = presenterExtensionBuilder(parent.presenterExtPoint());
        pi.addAddon(uiAddon("ui_addon", ExternalOrgUnitWizardStep1AddUIExt.class));
        return pi.create();
    }
}
