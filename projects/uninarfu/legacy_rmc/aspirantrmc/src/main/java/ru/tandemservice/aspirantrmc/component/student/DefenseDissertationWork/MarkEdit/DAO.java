package ru.tandemservice.aspirantrmc.component.student.DefenseDissertationWork.MarkEdit;

import org.tandemframework.hibsupport.builder.MQBuilder;
import ru.tandemservice.aspirantrmc.entity.DefenseDissertationWork;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;

public class DAO extends UniDao<Model> {

    @Override
    public void prepare(Model model) {
        if (model.getStudentId() != null)
            model.setStudent(getNotNull(Student.class, model.getStudentId()));
        if (model.getDissertationWorkId() != null) {
            model.setMark(((DefenseDissertationWork) getNotNull(DefenseDissertationWork.class, model.getDissertationWorkId())).getMark());
        }
        model.setMarkModel(new UniQueryFullCheckSelectModel(new String[]{SessionMarkGradeValueCatalogItem.title().s()}) {
            @Override
            protected MQBuilder query(String alias, String filter) {
                MQBuilder builder = new MQBuilder(SessionMarkGradeValueCatalogItem.ENTITY_CLASS, alias)
                        .addOrder(alias, SessionMarkGradeValueCatalogItem.title());
                return builder;
            }
        });
    }

    @Override
    public void update(Model model) {
        if (model.getDissertationWorkId() != null) {
            DefenseDissertationWork entity = getNotNull(DefenseDissertationWork.class, model.getDissertationWorkId());
            entity.setMark(model.getMark());
            saveOrUpdate(entity);
        }
    }

}
