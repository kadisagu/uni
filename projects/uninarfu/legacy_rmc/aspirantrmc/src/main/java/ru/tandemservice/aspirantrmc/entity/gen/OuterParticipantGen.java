package ru.tandemservice.aspirantrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.aspirantrmc.entity.OuterParticipant;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Внешний сотрудник
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OuterParticipantGen extends PersonRole
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.aspirantrmc.entity.OuterParticipant";
    public static final String ENTITY_NAME = "outerParticipant";
    public static final int VERSION_HASH = 2010664372;
    private static IEntityMeta ENTITY_META;

    public static final String L_ADDRESS = "address";
    public static final String P_EMPLOYEE_POST = "employeePost";
    public static final String P_WORK_PLACE = "workPlace";

    private AddressDetailed _address;     // Юридический адрес места работы
    private String _employeePost;     // Должность
    private String _workPlace;     // Место работы

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Юридический адрес места работы.
     */
    public AddressDetailed getAddress()
    {
        return _address;
    }

    /**
     * @param address Юридический адрес места работы.
     */
    public void setAddress(AddressDetailed address)
    {
        dirty(_address, address);
        _address = address;
    }

    /**
     * @return Должность.
     */
    @Length(max=255)
    public String getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Должность.
     */
    public void setEmployeePost(String employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    /**
     * @return Место работы.
     */
    @Length(max=255)
    public String getWorkPlace()
    {
        return _workPlace;
    }

    /**
     * @param workPlace Место работы.
     */
    public void setWorkPlace(String workPlace)
    {
        dirty(_workPlace, workPlace);
        _workPlace = workPlace;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof OuterParticipantGen)
        {
            setAddress(((OuterParticipant)another).getAddress());
            setEmployeePost(((OuterParticipant)another).getEmployeePost());
            setWorkPlace(((OuterParticipant)another).getWorkPlace());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OuterParticipantGen> extends PersonRole.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OuterParticipant.class;
        }

        public T newInstance()
        {
            return (T) new OuterParticipant();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "address":
                    return obj.getAddress();
                case "employeePost":
                    return obj.getEmployeePost();
                case "workPlace":
                    return obj.getWorkPlace();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "address":
                    obj.setAddress((AddressDetailed) value);
                    return;
                case "employeePost":
                    obj.setEmployeePost((String) value);
                    return;
                case "workPlace":
                    obj.setWorkPlace((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "address":
                        return true;
                case "employeePost":
                        return true;
                case "workPlace":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "address":
                    return true;
                case "employeePost":
                    return true;
                case "workPlace":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "address":
                    return AddressDetailed.class;
                case "employeePost":
                    return String.class;
                case "workPlace":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OuterParticipant> _dslPath = new Path<OuterParticipant>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OuterParticipant");
    }
            

    /**
     * @return Юридический адрес места работы.
     * @see ru.tandemservice.aspirantrmc.entity.OuterParticipant#getAddress()
     */
    public static AddressDetailed.Path<AddressDetailed> address()
    {
        return _dslPath.address();
    }

    /**
     * @return Должность.
     * @see ru.tandemservice.aspirantrmc.entity.OuterParticipant#getEmployeePost()
     */
    public static PropertyPath<String> employeePost()
    {
        return _dslPath.employeePost();
    }

    /**
     * @return Место работы.
     * @see ru.tandemservice.aspirantrmc.entity.OuterParticipant#getWorkPlace()
     */
    public static PropertyPath<String> workPlace()
    {
        return _dslPath.workPlace();
    }

    public static class Path<E extends OuterParticipant> extends PersonRole.Path<E>
    {
        private AddressDetailed.Path<AddressDetailed> _address;
        private PropertyPath<String> _employeePost;
        private PropertyPath<String> _workPlace;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Юридический адрес места работы.
     * @see ru.tandemservice.aspirantrmc.entity.OuterParticipant#getAddress()
     */
        public AddressDetailed.Path<AddressDetailed> address()
        {
            if(_address == null )
                _address = new AddressDetailed.Path<AddressDetailed>(L_ADDRESS, this);
            return _address;
        }

    /**
     * @return Должность.
     * @see ru.tandemservice.aspirantrmc.entity.OuterParticipant#getEmployeePost()
     */
        public PropertyPath<String> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new PropertyPath<String>(OuterParticipantGen.P_EMPLOYEE_POST, this);
            return _employeePost;
        }

    /**
     * @return Место работы.
     * @see ru.tandemservice.aspirantrmc.entity.OuterParticipant#getWorkPlace()
     */
        public PropertyPath<String> workPlace()
        {
            if(_workPlace == null )
                _workPlace = new PropertyPath<String>(OuterParticipantGen.P_WORK_PLACE, this);
            return _workPlace;
        }

        public Class getEntityClass()
        {
            return OuterParticipant.class;
        }

        public String getEntityName()
        {
            return "outerParticipant";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
