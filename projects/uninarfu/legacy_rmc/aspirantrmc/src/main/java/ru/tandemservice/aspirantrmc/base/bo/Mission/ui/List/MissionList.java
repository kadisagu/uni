package ru.tandemservice.aspirantrmc.base.bo.Mission.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import ru.tandemservice.aspirantrmc.base.bo.Mission.MissionManager;
import ru.tandemservice.aspirantrmc.base.bo.Mission.ui.Pub.MissionPub;
import ru.tandemservice.aspirantrmc.base.bo.Mission.ui.Pub.MissionPubUI;
import ru.tandemservice.aspirantrmc.entity.ScienceMission;
import ru.tandemservice.aspirantrmc.entity.catalog.Financing;
import ru.tandemservice.aspirantrmc.entity.catalog.StatusMission;
import ru.tandemservice.uni.util.FilterUtils;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class MissionList extends BusinessComponentManager {
    public static final String LIST_DS = "listDS";
    protected DQLOrderDescriptionRegistry _orderRegistry = new DQLOrderDescriptionRegistry(ScienceMission.class, "e");

    protected static final Map<String, Object> briefInfoParams = new HashMap<String, Object>() {{
        put(PublisherActivator.PUBLISHER_ID_KEY, ScienceMission.id());
        put(MissionPubUI.SELECTED_TAB, MissionPub.BRIEF_INFO_TAB);
    }};
    protected static final Map<String, Object> contactInfoParams = new HashMap<String, Object>() {{
        put(PublisherActivator.PUBLISHER_ID_KEY, ScienceMission.id());
        put(MissionPubUI.SELECTED_TAB, MissionPub.CONTACT_INFO_TAB);
    }};

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(LIST_DS, columnListExtPoint(), entityDSHandler()).numberOfRecords(5))
                .addDataSource(selectDS(MissionManager.STATUS_MISSION_DS, MissionManager.instance().statusMissionDSHadnler()))
                .addDataSource(selectDS(MissionManager.FINANCING_DS, MissionManager.instance().financeDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint columnListExtPoint() {
        return columnListExtPointBuilder(LIST_DS)
                .addColumn(publisherColumn("name", ScienceMission.name().s())
                                   .publisherLinkResolver(new SimplePublisherLinkResolver(ScienceMission.id()).params(briefInfoParams))
                                   .order().create()
                )
                .addColumn(dateColumn("startDate", ScienceMission.startDate().s()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order().create())
                .addColumn(dateColumn("endDate", ScienceMission.endDate().s()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order().create())
                .addColumn(publisherColumn("destination", ScienceMission.organization().s())
                                   .publisherLinkResolver(new SimplePublisherLinkResolver(ScienceMission.id()).params(contactInfoParams))
                                   .order().create()
                )
                .addColumn(textColumn("financing", ScienceMission.financing().title().s()).order().create())
                .addColumn(textColumn("statusMission", ScienceMission.statusMission().title().s()).order().create())
                .addColumn(actionColumn("edit", new Icon("edit"), "onEditEntry")
                                   .permissionKey("aspirantrmc_editMission")
                                   .create()
                )
                .addColumn(actionColumn("delete", new Icon("delete"), "onDeleteEntry")
                                   .permissionKey("aspirantrmc_deleteMission")
                                   .alert(FormattedMessage.with().template(LIST_DS + ".delete.alert").create())
                                   .create()
                )

                .create();
    }

    @Bean
    IBusinessHandler<DSInput, DSOutput> entityDSHandler() {
        return new DefaultSearchDataSourceHandler(getName()) {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context) {
                Long studentId = context.get(MissionListUI.STUDENT_ID);
                StatusMission statusMission = context.get(MissionManager.STATUS_MISSION_DS);
                Financing financing = context.get(MissionManager.FINANCING_DS);

                DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(ScienceMission.class, "e");
                dql.where(DQLExpressions.eq(
                        DQLExpressions.property(ScienceMission.student().id().fromAlias("e")),
                        DQLExpressions.value(studentId)
                ));
                FilterUtils.applySelectFilter(dql, "e", ScienceMission.statusMission().getPath(), statusMission);
                FilterUtils.applySelectFilter(dql, "e", ScienceMission.financing().getPath(), financing);

                _orderRegistry.applyOrder(dql, input.getEntityOrder());

                return DQLSelectOutputBuilder.get(input, dql, context.getSession()).pageable(true).build();
            }
        };
    }
}
