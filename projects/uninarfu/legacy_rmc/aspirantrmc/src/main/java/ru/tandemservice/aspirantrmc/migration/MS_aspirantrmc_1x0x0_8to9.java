package ru.tandemservice.aspirantrmc.migration;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.DBType;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class MS_aspirantrmc_1x0x0_8to9 extends IndependentMigrationScript {
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.5.2")
                };

    }


    @Override
    public ScriptDependency[] getAfterDependencies()
    {
        return new ScriptDependency[0];
    }

    @Override
    public ScriptDependency[] getBeforeDependencies()
    {
        // опираемся на UNI
        return new ScriptDependency[]{new ScriptDependency("uni", "2.5.2", 0)};
    }

    public void run(DBTool tool) throws Exception
    {

        if (!tool.tableExists("addressbase_t"))
            throw new ApplicationException("addressbase_t не существует, ошибка в порядке исполнения миграций");


        PreparedStatement insertAddressBase = tool.prepareStatement("insert into addressbase_t (id, discriminator) values (?, ?)", new Object[0]);
        PreparedStatement insertAddressDetailed = tool.prepareStatement("insert into addressdetailed_t (id, country_id, settlement_id) values (?, ?, ?)", new Object[0]);
        PreparedStatement insertAddressRu = tool.prepareStatement("insert into addressru_t (id, district_id, street_id, housenumber_p, houseunitnumber_p, flatnumber_p, inheritedpostcode_p) values (?, ?, ?, ?, ?, ?, ?)", new Object[0]);
        PreparedStatement insertAddressInter = tool.prepareStatement("insert into addressinter_t (id, region_p, district_p, addresslocation_p, postcode_p) values (?, ?, ?, ?, ?)", new Object[0]);
        PreparedStatement insertAddressString = tool.prepareStatement("insert into addressstring_t (id, title_p) values (?, ?)", new Object[0]);

        Short discrAddressRu = tool.entityCodes().get("addressRu");
        Short discrAddressInter = tool.entityCodes().get("addressInter");
        Short discrAddressString = tool.entityCodes().get("addressString");

        ISQLTranslator translator = tool.getDialect().getSQLTranslator();

        if (tool.tableExists("outerparticipant_t")) {
            tool.dropConstraint("outerparticipant_t", "fk_address_outerparticipant");
            tool.createColumn("outerparticipant_t", new DBColumn("address_tmp_id", DBType.LONG));

            PreparedStatement updateAddressTmp = tool.prepareStatement("update outerparticipant_t set address_tmp_id = ? where id = ?", new Object[0]);

            SQLSelectQuery selectAddreesQuery = new SQLSelectQuery();
            selectAddreesQuery.from(SQLFrom.table("outerparticipant_t", "pk").innerJoin(SQLFrom.table("address_t", "addr"), "pk.address_id=addr.id").leftJoin(SQLFrom.table("addresscountry_t", "cntr"), "addr.country_id=cntr.id").leftJoin(SQLFrom.table("addressdistrict_t", "dstr"), "addr.district_id=dstr.id").leftJoin(SQLFrom.table("addressstreet_t", "street"), "addr.street_id=street.id").leftJoin(SQLFrom.table("addresstype_t", "streetType"), "street.addresstype_id=streetType.id"));

            selectAddreesQuery.column("pk.id", "pkid");
            selectAddreesQuery.column("pk.address_id", "pka");
            selectAddreesQuery.column("addr.country_id", "addrc");
            selectAddreesQuery.column("addr.settlement_id", "addrsettl");
            selectAddreesQuery.column("addr.district_id", "addrdis");
            selectAddreesQuery.column("addr.street_id", "addrstr");
            selectAddreesQuery.column("addr.housenumber_p", "addrhn");
            selectAddreesQuery.column("addr.houseunitnumber_p", "addrhun");
            selectAddreesQuery.column("addr.flatnumber_p", "addrfn");
            selectAddreesQuery.column("addr.inheritedpostcode_p", "addrpc");
            selectAddreesQuery.column("cntr.code_p", "cntrcd");
            selectAddreesQuery.column("dstr.title_p", "dstrtitle");
            selectAddreesQuery.column("street.title_p", "streettitle");
            selectAddreesQuery.column("streetType.shorttitle_p", "streettype");

            String addressSql = translator.toSql(selectAddreesQuery);

            Statement addressStatement = tool.getConnection().createStatement();
            addressStatement.execute(addressSql);

            ResultSet addressResult = addressStatement.getResultSet();

            while (addressResult.next()) {
                Long pkId = addressResult.getLong("pkid") != 0L ? Long.valueOf(addressResult.getLong("pkid")) : null;
                Long postalAddrId = addressResult.getLong("pka") != 0L ? Long.valueOf(addressResult.getLong("pka")) : null;

                Long countryId = addressResult.getLong("addrc") != 0L ? Long.valueOf(addressResult.getLong("addrc")) : null;
                Long settlementId = addressResult.getLong("addrsettl") != 0L ? Long.valueOf(addressResult.getLong("addrsettl")) : null;
                Long districtId = addressResult.getLong("addrdis") != 0L ? Long.valueOf(addressResult.getLong("addrdis")) : null;
                Long streetId = addressResult.getLong("addrstr") != 0L ? Long.valueOf(addressResult.getLong("addrstr")) : null;
                String houseNumber = addressResult.getString("addrhn");
                String houseUnitNumber = addressResult.getString("addrhun");
                String flatNumber = addressResult.getString("addrfn");
                String postcode = addressResult.getString("addrpc");
                Integer countryCode = Integer.valueOf(addressResult.getInt("cntrcd"));
                String districtTitle = addressResult.getString("dstrtitle");
                String streetTitle = addressResult.getString("streettitle");
                String streetType = addressResult.getString("streettype");

                if (countryId != null) {
                    boolean isAddressRus = countryCode.intValue() == 0;
                    Long id = EntityIDGenerator.generateNewId((isAddressRus ? discrAddressRu : discrAddressInter).shortValue());

                    insertAddressBase.clearParameters();
                    insertAddressBase.setLong(1, id.longValue());
                    insertAddressBase.setShort(2, (isAddressRus ? discrAddressRu : discrAddressInter).shortValue());
                    insertAddressBase.executeUpdate();

                    insertAddressDetailed.clearParameters();
                    insertAddressDetailed.setLong(1, id.longValue());
                    insertAddressDetailed.setLong(2, countryId.longValue());
                    if (null != settlementId)
                        insertAddressDetailed.setLong(3, settlementId.longValue());
                    else
                        insertAddressDetailed.setNull(3, -5);
                    insertAddressDetailed.executeUpdate();

                    if (isAddressRus) {
                        insertAddressRu.clearParameters();
                        insertAddressRu.setLong(1, id.longValue());
                        if (null != districtId)
                            insertAddressRu.setLong(2, districtId.longValue());
                        else
                            insertAddressRu.setNull(2, -5);
                        if (null != streetId)
                            insertAddressRu.setLong(3, streetId.longValue());
                        else
                            insertAddressRu.setNull(3, -5);
                        if (!StringUtils.isEmpty(houseNumber))
                            insertAddressRu.setString(4, houseNumber);
                        else
                            insertAddressRu.setString(4, null);
                        if (!StringUtils.isEmpty(houseUnitNumber))
                            insertAddressRu.setString(5, houseUnitNumber);
                        else
                            insertAddressRu.setString(5, null);
                        if (!StringUtils.isEmpty(flatNumber))
                            insertAddressRu.setString(6, flatNumber);
                        else
                            insertAddressRu.setString(6, null);
                        if (!StringUtils.isEmpty(postcode))
                            insertAddressRu.setString(7, postcode);
                        else {
                            insertAddressRu.setString(7, null);
                        }
                        insertAddressRu.executeUpdate();
                    }
                    else {
                        insertAddressInter.clearParameters();
                        insertAddressInter.setLong(1, id.longValue());
                        String region = "";
                        if (null != settlementId) {
                            region = getRegion(settlementId, tool);
                        }
                        if (!StringUtils.isEmpty(region))
                            insertAddressInter.setString(2, region);
                        else
                            insertAddressInter.setNull(2, 12);
                        if (!StringUtils.isEmpty(districtTitle))
                            insertAddressInter.setString(3, new StringBuilder().append(districtTitle).append(" р-н").toString());
                        else
                            insertAddressInter.setString(3, null);
                        if ((!StringUtils.isEmpty(streetType)) && (!StringUtils.isEmpty(streetTitle))) {
                            StringBuilder location = new StringBuilder(new StringBuilder().append(streetType).append(" ").append(streetTitle).toString());
                            if (!StringUtils.isEmpty(houseNumber))
                                location.append(new StringBuilder().append(", д. ").append(houseNumber).toString());
                            if (!StringUtils.isEmpty(houseUnitNumber))
                                location.append(new StringBuilder().append(", кор. ").append(houseUnitNumber).toString());
                            if (!StringUtils.isEmpty(flatNumber))
                                location.append(new StringBuilder().append(", кв. ").append(flatNumber).toString());
                            insertAddressInter.setString(4, location.toString());
                        }
                        else {
                            insertAddressInter.setString(4, null);
                        }
                        if (!StringUtils.isEmpty(postcode))
                            insertAddressInter.setString(5, postcode);
                        else
                            insertAddressInter.setString(5, null);
                        insertAddressInter.executeUpdate();
                    }

                    updateAddressTmp.clearParameters();
                    updateAddressTmp.setLong(1, id.longValue());
                    updateAddressTmp.setLong(2, pkId.longValue());
                    updateAddressTmp.executeUpdate();
                }
                else {
                    tool.prepareStatement("update outerparticipant_t set address_id=? where id=?", new Object[]{null, pkId}).executeUpdate();
                }
            }

            tool.prepareStatement("update outerparticipant_t set address_id=address_tmp_id", new Object[0]).executeUpdate();
            tool.dropColumn("outerparticipant_t", "address_tmp_id");
        }


        if (tool.tableExists("aspactionsupervisor_t")) {
            tool.dropConstraint("aspactionsupervisor_t", "fk_address_aspactionsupervisor");
            tool.createColumn("aspactionsupervisor_t", new DBColumn("address_tmp_id", DBType.LONG));

            PreparedStatement updateAddressTmp = tool.prepareStatement("update aspactionsupervisor_t set address_tmp_id = ? where id = ?", new Object[0]);

            SQLSelectQuery selectAddreesQuery = new SQLSelectQuery();
            selectAddreesQuery.from(SQLFrom.table("aspactionsupervisor_t", "pk").innerJoin(SQLFrom.table("address_t", "addr"), "pk.address_id=addr.id").leftJoin(SQLFrom.table("addresscountry_t", "cntr"), "addr.country_id=cntr.id").leftJoin(SQLFrom.table("addressdistrict_t", "dstr"), "addr.district_id=dstr.id").leftJoin(SQLFrom.table("addressstreet_t", "street"), "addr.street_id=street.id").leftJoin(SQLFrom.table("addresstype_t", "streetType"), "street.addresstype_id=streetType.id"));

            selectAddreesQuery.column("pk.id", "pkid");
            selectAddreesQuery.column("pk.address_id", "pka");
            selectAddreesQuery.column("addr.country_id", "addrc");
            selectAddreesQuery.column("addr.settlement_id", "addrsettl");
            selectAddreesQuery.column("addr.district_id", "addrdis");
            selectAddreesQuery.column("addr.street_id", "addrstr");
            selectAddreesQuery.column("addr.housenumber_p", "addrhn");
            selectAddreesQuery.column("addr.houseunitnumber_p", "addrhun");
            selectAddreesQuery.column("addr.flatnumber_p", "addrfn");
            selectAddreesQuery.column("addr.inheritedpostcode_p", "addrpc");
            selectAddreesQuery.column("cntr.code_p", "cntrcd");
            selectAddreesQuery.column("dstr.title_p", "dstrtitle");
            selectAddreesQuery.column("street.title_p", "streettitle");
            selectAddreesQuery.column("streetType.shorttitle_p", "streettype");

            String addressSql = translator.toSql(selectAddreesQuery);

            Statement addressStatement = tool.getConnection().createStatement();
            addressStatement.execute(addressSql);

            ResultSet addressResult = addressStatement.getResultSet();

            while (addressResult.next()) {
                Long pkId = addressResult.getLong("pkid") != 0L ? Long.valueOf(addressResult.getLong("pkid")) : null;
                Long postalAddrId = addressResult.getLong("pka") != 0L ? Long.valueOf(addressResult.getLong("pka")) : null;

                Long countryId = addressResult.getLong("addrc") != 0L ? Long.valueOf(addressResult.getLong("addrc")) : null;
                Long settlementId = addressResult.getLong("addrsettl") != 0L ? Long.valueOf(addressResult.getLong("addrsettl")) : null;
                Long districtId = addressResult.getLong("addrdis") != 0L ? Long.valueOf(addressResult.getLong("addrdis")) : null;
                Long streetId = addressResult.getLong("addrstr") != 0L ? Long.valueOf(addressResult.getLong("addrstr")) : null;
                String houseNumber = addressResult.getString("addrhn");
                String houseUnitNumber = addressResult.getString("addrhun");
                String flatNumber = addressResult.getString("addrfn");
                String postcode = addressResult.getString("addrpc");
                Integer countryCode = Integer.valueOf(addressResult.getInt("cntrcd"));
                String districtTitle = addressResult.getString("dstrtitle");
                String streetTitle = addressResult.getString("streettitle");
                String streetType = addressResult.getString("streettype");

                if (countryId != null) {
                    boolean isAddressRus = countryCode.intValue() == 0;
                    Long id = EntityIDGenerator.generateNewId((isAddressRus ? discrAddressRu : discrAddressInter).shortValue());

                    insertAddressBase.clearParameters();
                    insertAddressBase.setLong(1, id.longValue());
                    insertAddressBase.setShort(2, (isAddressRus ? discrAddressRu : discrAddressInter).shortValue());
                    insertAddressBase.executeUpdate();

                    insertAddressDetailed.clearParameters();
                    insertAddressDetailed.setLong(1, id.longValue());
                    insertAddressDetailed.setLong(2, countryId.longValue());
                    if (null != settlementId)
                        insertAddressDetailed.setLong(3, settlementId.longValue());
                    else
                        insertAddressDetailed.setNull(3, -5);
                    insertAddressDetailed.executeUpdate();

                    if (isAddressRus) {
                        insertAddressRu.clearParameters();
                        insertAddressRu.setLong(1, id.longValue());
                        if (null != districtId)
                            insertAddressRu.setLong(2, districtId.longValue());
                        else
                            insertAddressRu.setNull(2, -5);
                        if (null != streetId)
                            insertAddressRu.setLong(3, streetId.longValue());
                        else
                            insertAddressRu.setNull(3, -5);
                        if (!StringUtils.isEmpty(houseNumber))
                            insertAddressRu.setString(4, houseNumber);
                        else
                            insertAddressRu.setString(4, null);
                        if (!StringUtils.isEmpty(houseUnitNumber))
                            insertAddressRu.setString(5, houseUnitNumber);
                        else
                            insertAddressRu.setString(5, null);
                        if (!StringUtils.isEmpty(flatNumber))
                            insertAddressRu.setString(6, flatNumber);
                        else
                            insertAddressRu.setString(6, null);
                        if (!StringUtils.isEmpty(postcode))
                            insertAddressRu.setString(7, postcode);
                        else {
                            insertAddressRu.setString(7, null);
                        }
                        insertAddressRu.executeUpdate();
                    }
                    else {
                        insertAddressInter.clearParameters();
                        insertAddressInter.setLong(1, id.longValue());
                        String region = "";
                        if (null != settlementId) {
                            region = getRegion(settlementId, tool);
                        }
                        if (!StringUtils.isEmpty(region))
                            insertAddressInter.setString(2, region);
                        else
                            insertAddressInter.setNull(2, 12);
                        if (!StringUtils.isEmpty(districtTitle))
                            insertAddressInter.setString(3, new StringBuilder().append(districtTitle).append(" р-н").toString());
                        else
                            insertAddressInter.setString(3, null);
                        if ((!StringUtils.isEmpty(streetType)) && (!StringUtils.isEmpty(streetTitle))) {
                            StringBuilder location = new StringBuilder(new StringBuilder().append(streetType).append(" ").append(streetTitle).toString());
                            if (!StringUtils.isEmpty(houseNumber))
                                location.append(new StringBuilder().append(", д. ").append(houseNumber).toString());
                            if (!StringUtils.isEmpty(houseUnitNumber))
                                location.append(new StringBuilder().append(", кор. ").append(houseUnitNumber).toString());
                            if (!StringUtils.isEmpty(flatNumber))
                                location.append(new StringBuilder().append(", кв. ").append(flatNumber).toString());
                            insertAddressInter.setString(4, location.toString());
                        }
                        else {
                            insertAddressInter.setString(4, null);
                        }
                        if (!StringUtils.isEmpty(postcode))
                            insertAddressInter.setString(5, postcode);
                        else
                            insertAddressInter.setString(5, null);
                        insertAddressInter.executeUpdate();
                    }

                    updateAddressTmp.clearParameters();
                    updateAddressTmp.setLong(1, id.longValue());
                    updateAddressTmp.setLong(2, pkId.longValue());
                    updateAddressTmp.executeUpdate();
                }
                else {
                    tool.prepareStatement("update aspactionsupervisor_t set address_id=? where id=?", new Object[]{null, pkId}).executeUpdate();
                }
            }

            tool.prepareStatement("update aspactionsupervisor_t set address_id=address_tmp_id", new Object[0]).executeUpdate();
            tool.dropColumn("aspactionsupervisor_t", "address_tmp_id");
        }


        if (tool.tableExists("aspaction_t")) {
            tool.dropConstraint("aspaction_t", "fk_address_aspaction");
            tool.createColumn("aspaction_t", new DBColumn("address_tmp_id", DBType.LONG));

            PreparedStatement updateAddressTmp = tool.prepareStatement("update aspaction_t set address_tmp_id = ? where id = ?", new Object[0]);

            SQLSelectQuery selectAddreesQuery = new SQLSelectQuery();
            selectAddreesQuery.from(SQLFrom.table("aspaction_t", "pk").innerJoin(SQLFrom.table("address_t", "addr"), "pk.address_id=addr.id").leftJoin(SQLFrom.table("addresscountry_t", "cntr"), "addr.country_id=cntr.id").leftJoin(SQLFrom.table("addressdistrict_t", "dstr"), "addr.district_id=dstr.id").leftJoin(SQLFrom.table("addressstreet_t", "street"), "addr.street_id=street.id").leftJoin(SQLFrom.table("addresstype_t", "streetType"), "street.addresstype_id=streetType.id"));

            selectAddreesQuery.column("pk.id", "pkid");
            selectAddreesQuery.column("pk.address_id", "pka");
            selectAddreesQuery.column("addr.country_id", "addrc");
            selectAddreesQuery.column("addr.settlement_id", "addrsettl");
            selectAddreesQuery.column("addr.district_id", "addrdis");
            selectAddreesQuery.column("addr.street_id", "addrstr");
            selectAddreesQuery.column("addr.housenumber_p", "addrhn");
            selectAddreesQuery.column("addr.houseunitnumber_p", "addrhun");
            selectAddreesQuery.column("addr.flatnumber_p", "addrfn");
            selectAddreesQuery.column("addr.inheritedpostcode_p", "addrpc");
            selectAddreesQuery.column("cntr.code_p", "cntrcd");
            selectAddreesQuery.column("dstr.title_p", "dstrtitle");
            selectAddreesQuery.column("street.title_p", "streettitle");
            selectAddreesQuery.column("streetType.shorttitle_p", "streettype");

            String addressSql = translator.toSql(selectAddreesQuery);

            Statement addressStatement = tool.getConnection().createStatement();
            addressStatement.execute(addressSql);

            ResultSet addressResult = addressStatement.getResultSet();

            while (addressResult.next()) {
                Long pkId = addressResult.getLong("pkid") != 0L ? Long.valueOf(addressResult.getLong("pkid")) : null;
                Long postalAddrId = addressResult.getLong("pka") != 0L ? Long.valueOf(addressResult.getLong("pka")) : null;

                Long countryId = addressResult.getLong("addrc") != 0L ? Long.valueOf(addressResult.getLong("addrc")) : null;
                Long settlementId = addressResult.getLong("addrsettl") != 0L ? Long.valueOf(addressResult.getLong("addrsettl")) : null;
                Long districtId = addressResult.getLong("addrdis") != 0L ? Long.valueOf(addressResult.getLong("addrdis")) : null;
                Long streetId = addressResult.getLong("addrstr") != 0L ? Long.valueOf(addressResult.getLong("addrstr")) : null;
                String houseNumber = addressResult.getString("addrhn");
                String houseUnitNumber = addressResult.getString("addrhun");
                String flatNumber = addressResult.getString("addrfn");
                String postcode = addressResult.getString("addrpc");
                Integer countryCode = Integer.valueOf(addressResult.getInt("cntrcd"));
                String districtTitle = addressResult.getString("dstrtitle");
                String streetTitle = addressResult.getString("streettitle");
                String streetType = addressResult.getString("streettype");

                if (countryId != null) {
                    boolean isAddressRus = countryCode.intValue() == 0;
                    Long id = EntityIDGenerator.generateNewId((isAddressRus ? discrAddressRu : discrAddressInter).shortValue());

                    insertAddressBase.clearParameters();
                    insertAddressBase.setLong(1, id.longValue());
                    insertAddressBase.setShort(2, (isAddressRus ? discrAddressRu : discrAddressInter).shortValue());
                    insertAddressBase.executeUpdate();

                    insertAddressDetailed.clearParameters();
                    insertAddressDetailed.setLong(1, id.longValue());
                    insertAddressDetailed.setLong(2, countryId.longValue());
                    if (null != settlementId)
                        insertAddressDetailed.setLong(3, settlementId.longValue());
                    else
                        insertAddressDetailed.setNull(3, -5);
                    insertAddressDetailed.executeUpdate();

                    if (isAddressRus) {
                        insertAddressRu.clearParameters();
                        insertAddressRu.setLong(1, id.longValue());
                        if (null != districtId)
                            insertAddressRu.setLong(2, districtId.longValue());
                        else
                            insertAddressRu.setNull(2, -5);
                        if (null != streetId)
                            insertAddressRu.setLong(3, streetId.longValue());
                        else
                            insertAddressRu.setNull(3, -5);
                        if (!StringUtils.isEmpty(houseNumber))
                            insertAddressRu.setString(4, houseNumber);
                        else
                            insertAddressRu.setString(4, null);
                        if (!StringUtils.isEmpty(houseUnitNumber))
                            insertAddressRu.setString(5, houseUnitNumber);
                        else
                            insertAddressRu.setString(5, null);
                        if (!StringUtils.isEmpty(flatNumber))
                            insertAddressRu.setString(6, flatNumber);
                        else
                            insertAddressRu.setString(6, null);
                        if (!StringUtils.isEmpty(postcode))
                            insertAddressRu.setString(7, postcode);
                        else {
                            insertAddressRu.setString(7, null);
                        }
                        insertAddressRu.executeUpdate();
                    }
                    else {
                        insertAddressInter.clearParameters();
                        insertAddressInter.setLong(1, id.longValue());
                        String region = "";
                        if (null != settlementId) {
                            region = getRegion(settlementId, tool);
                        }
                        if (!StringUtils.isEmpty(region))
                            insertAddressInter.setString(2, region);
                        else
                            insertAddressInter.setNull(2, 12);
                        if (!StringUtils.isEmpty(districtTitle))
                            insertAddressInter.setString(3, new StringBuilder().append(districtTitle).append(" р-н").toString());
                        else
                            insertAddressInter.setString(3, null);
                        if ((!StringUtils.isEmpty(streetType)) && (!StringUtils.isEmpty(streetTitle))) {
                            StringBuilder location = new StringBuilder(new StringBuilder().append(streetType).append(" ").append(streetTitle).toString());
                            if (!StringUtils.isEmpty(houseNumber))
                                location.append(new StringBuilder().append(", д. ").append(houseNumber).toString());
                            if (!StringUtils.isEmpty(houseUnitNumber))
                                location.append(new StringBuilder().append(", кор. ").append(houseUnitNumber).toString());
                            if (!StringUtils.isEmpty(flatNumber))
                                location.append(new StringBuilder().append(", кв. ").append(flatNumber).toString());
                            insertAddressInter.setString(4, location.toString());
                        }
                        else {
                            insertAddressInter.setString(4, null);
                        }
                        if (!StringUtils.isEmpty(postcode))
                            insertAddressInter.setString(5, postcode);
                        else
                            insertAddressInter.setString(5, null);
                        insertAddressInter.executeUpdate();
                    }

                    updateAddressTmp.clearParameters();
                    updateAddressTmp.setLong(1, id.longValue());
                    updateAddressTmp.setLong(2, pkId.longValue());
                    updateAddressTmp.executeUpdate();
                }
                else {
                    tool.prepareStatement("update aspaction_t set address_id=? where id=?", new Object[]{null, pkId}).executeUpdate();
                }
            }

            tool.prepareStatement("update aspaction_t set address_id=address_tmp_id", new Object[0]).executeUpdate();
            tool.dropColumn("aspaction_t", "address_tmp_id");
        }


        if (tool.tableExists("sciencemission_t")) {
            tool.dropConstraint("sciencemission_t", "fk_address_sciencemission");
            tool.createColumn("sciencemission_t", new DBColumn("address_tmp_id", DBType.LONG));

            PreparedStatement updateAddressTmp = tool.prepareStatement("update sciencemission_t set address_tmp_id = ? where id = ?", new Object[0]);

            SQLSelectQuery selectAddreesQuery = new SQLSelectQuery();
            selectAddreesQuery.from(SQLFrom.table("sciencemission_t", "pk").innerJoin(SQLFrom.table("address_t", "addr"), "pk.address_id=addr.id").leftJoin(SQLFrom.table("addresscountry_t", "cntr"), "addr.country_id=cntr.id").leftJoin(SQLFrom.table("addressdistrict_t", "dstr"), "addr.district_id=dstr.id").leftJoin(SQLFrom.table("addressstreet_t", "street"), "addr.street_id=street.id").leftJoin(SQLFrom.table("addresstype_t", "streetType"), "street.addresstype_id=streetType.id"));

            selectAddreesQuery.column("pk.id", "pkid");
            selectAddreesQuery.column("pk.address_id", "pka");
            selectAddreesQuery.column("addr.country_id", "addrc");
            selectAddreesQuery.column("addr.settlement_id", "addrsettl");
            selectAddreesQuery.column("addr.district_id", "addrdis");
            selectAddreesQuery.column("addr.street_id", "addrstr");
            selectAddreesQuery.column("addr.housenumber_p", "addrhn");
            selectAddreesQuery.column("addr.houseunitnumber_p", "addrhun");
            selectAddreesQuery.column("addr.flatnumber_p", "addrfn");
            selectAddreesQuery.column("addr.inheritedpostcode_p", "addrpc");
            selectAddreesQuery.column("cntr.code_p", "cntrcd");
            selectAddreesQuery.column("dstr.title_p", "dstrtitle");
            selectAddreesQuery.column("street.title_p", "streettitle");
            selectAddreesQuery.column("streetType.shorttitle_p", "streettype");

            String addressSql = translator.toSql(selectAddreesQuery);

            Statement addressStatement = tool.getConnection().createStatement();
            addressStatement.execute(addressSql);

            ResultSet addressResult = addressStatement.getResultSet();

            while (addressResult.next()) {
                Long pkId = addressResult.getLong("pkid") != 0L ? Long.valueOf(addressResult.getLong("pkid")) : null;
                Long postalAddrId = addressResult.getLong("pka") != 0L ? Long.valueOf(addressResult.getLong("pka")) : null;

                Long countryId = addressResult.getLong("addrc") != 0L ? Long.valueOf(addressResult.getLong("addrc")) : null;
                Long settlementId = addressResult.getLong("addrsettl") != 0L ? Long.valueOf(addressResult.getLong("addrsettl")) : null;
                Long districtId = addressResult.getLong("addrdis") != 0L ? Long.valueOf(addressResult.getLong("addrdis")) : null;
                Long streetId = addressResult.getLong("addrstr") != 0L ? Long.valueOf(addressResult.getLong("addrstr")) : null;
                String houseNumber = addressResult.getString("addrhn");
                String houseUnitNumber = addressResult.getString("addrhun");
                String flatNumber = addressResult.getString("addrfn");
                String postcode = addressResult.getString("addrpc");
                Integer countryCode = Integer.valueOf(addressResult.getInt("cntrcd"));
                String districtTitle = addressResult.getString("dstrtitle");
                String streetTitle = addressResult.getString("streettitle");
                String streetType = addressResult.getString("streettype");

                if (countryId != null) {
                    boolean isAddressRus = countryCode.intValue() == 0;
                    Long id = EntityIDGenerator.generateNewId((isAddressRus ? discrAddressRu : discrAddressInter).shortValue());

                    insertAddressBase.clearParameters();
                    insertAddressBase.setLong(1, id.longValue());
                    insertAddressBase.setShort(2, (isAddressRus ? discrAddressRu : discrAddressInter).shortValue());
                    insertAddressBase.executeUpdate();

                    insertAddressDetailed.clearParameters();
                    insertAddressDetailed.setLong(1, id.longValue());
                    insertAddressDetailed.setLong(2, countryId.longValue());
                    if (null != settlementId)
                        insertAddressDetailed.setLong(3, settlementId.longValue());
                    else
                        insertAddressDetailed.setNull(3, -5);
                    insertAddressDetailed.executeUpdate();

                    if (isAddressRus) {
                        insertAddressRu.clearParameters();
                        insertAddressRu.setLong(1, id.longValue());
                        if (null != districtId)
                            insertAddressRu.setLong(2, districtId.longValue());
                        else
                            insertAddressRu.setNull(2, -5);
                        if (null != streetId)
                            insertAddressRu.setLong(3, streetId.longValue());
                        else
                            insertAddressRu.setNull(3, -5);
                        if (!StringUtils.isEmpty(houseNumber))
                            insertAddressRu.setString(4, houseNumber);
                        else
                            insertAddressRu.setString(4, null);
                        if (!StringUtils.isEmpty(houseUnitNumber))
                            insertAddressRu.setString(5, houseUnitNumber);
                        else
                            insertAddressRu.setString(5, null);
                        if (!StringUtils.isEmpty(flatNumber))
                            insertAddressRu.setString(6, flatNumber);
                        else
                            insertAddressRu.setString(6, null);
                        if (!StringUtils.isEmpty(postcode))
                            insertAddressRu.setString(7, postcode);
                        else {
                            insertAddressRu.setString(7, null);
                        }
                        insertAddressRu.executeUpdate();
                    }
                    else {
                        insertAddressInter.clearParameters();
                        insertAddressInter.setLong(1, id.longValue());
                        String region = "";
                        if (null != settlementId) {
                            region = getRegion(settlementId, tool);
                        }
                        if (!StringUtils.isEmpty(region))
                            insertAddressInter.setString(2, region);
                        else
                            insertAddressInter.setNull(2, 12);
                        if (!StringUtils.isEmpty(districtTitle))
                            insertAddressInter.setString(3, new StringBuilder().append(districtTitle).append(" р-н").toString());
                        else
                            insertAddressInter.setString(3, null);
                        if ((!StringUtils.isEmpty(streetType)) && (!StringUtils.isEmpty(streetTitle))) {
                            StringBuilder location = new StringBuilder(new StringBuilder().append(streetType).append(" ").append(streetTitle).toString());
                            if (!StringUtils.isEmpty(houseNumber))
                                location.append(new StringBuilder().append(", д. ").append(houseNumber).toString());
                            if (!StringUtils.isEmpty(houseUnitNumber))
                                location.append(new StringBuilder().append(", кор. ").append(houseUnitNumber).toString());
                            if (!StringUtils.isEmpty(flatNumber))
                                location.append(new StringBuilder().append(", кв. ").append(flatNumber).toString());
                            insertAddressInter.setString(4, location.toString());
                        }
                        else {
                            insertAddressInter.setString(4, null);
                        }
                        if (!StringUtils.isEmpty(postcode))
                            insertAddressInter.setString(5, postcode);
                        else
                            insertAddressInter.setString(5, null);
                        insertAddressInter.executeUpdate();
                    }

                    updateAddressTmp.clearParameters();
                    updateAddressTmp.setLong(1, id.longValue());
                    updateAddressTmp.setLong(2, pkId.longValue());
                    updateAddressTmp.executeUpdate();
                }
                else {
                    tool.prepareStatement("update sciencemission_t set address_id=? where id=?", new Object[]{null, pkId}).executeUpdate();
                }
            }

            tool.prepareStatement("update sciencemission_t set address_id=address_tmp_id", new Object[0]).executeUpdate();
            tool.dropColumn("sciencemission_t", "address_tmp_id");
        }

    }

    private String getRegion(Long settlementId, DBTool tool) throws Exception {
        Long parentId = settlementId;
        String region = null;

        SQLSelectQuery selQuery = new SQLSelectQuery();
        selQuery.from(SQLFrom.table("addressitem_t", "ai").leftJoin(SQLFrom.table("addresstype_t", "at"), "ai.parent_id=at.id"));
        selQuery.column("ai.parent_id", "pr");
        selQuery.column("ai.title_p", "title");
        selQuery.column("at.shorttitle_p", "type");
        selQuery.where("ai.id=?");

        ISQLTranslator translator = tool.getDialect().getSQLTranslator();
        String sql = translator.toSql(selQuery);

        PreparedStatement settleStatement = tool.prepareStatement(sql, new Object[0]);

        Long lastId = settlementId;

        while (null != parentId) {
            settleStatement.clearParameters();
            settleStatement.setLong(1, parentId.longValue());
            ResultSet settleStatementResult = settleStatement.executeQuery();

            while (settleStatementResult.next()) {
                Long parId = Long.valueOf(settleStatementResult.getLong("pr"));
                if ((parId != null) && (parId.longValue() != 0L)) {
                    String title = settleStatementResult.getString("title");
                    String type = settleStatementResult.getString("type");
                    if (!StringUtils.isEmpty(title)) {
                        region = new StringBuilder().append(!StringUtils.isEmpty(type) ? new StringBuilder().append(type).append(" ").toString() : "").append(title).toString();
                    }
                    lastId = parId;
                    parentId = parId;
                }
                else {
                    parentId = null;
                }
            }
        }
        return !lastId.equals(settlementId) ? region : null;
    }
}
