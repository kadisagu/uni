package ru.tandemservice.aspirantrmc.component.settings.EducationLevelScientificToDissertationSovietPub;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.aspirantrmc.entity.RelDissertationSovietStaffPost;
import ru.tandemservice.aspirantrmc.entity.RelEducationLevelScientificDissertationSoviet;
import ru.tandemservice.aspirantrmc.entity.catalog.DissertationSoviet;
import ru.tandemservice.aspirantrmc.entity.catalog.StaffDissertationSoviet;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.ArrayList;
import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        super.prepare(model);
        if (model.getId() != null) {
            model.setEducationLevels(getNotNull(EducationLevelsHighSchool.class, model.getId()));
        }
        model.setEducationLevelScientific(model.getEducationLevels().getPrintTitle());
        model.setAssignedQualification(model.getEducationLevels().getQualificationTitleNullSafe());
    }

    @Override
    public void prepareListDataSource(Model model) {
        DynamicListDataSource<Wrapper> dataSource = model.getDataSource();

        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(RelEducationLevelScientificDissertationSoviet.class, "rel")
                .where(DQLExpressions.eq(DQLExpressions.property(RelEducationLevelScientificDissertationSoviet.educationLevelScientific().fromAlias("rel")),
                                         DQLExpressions.value(model.getEducationLevels().getEducationLevel())));
        List<RelEducationLevelScientificDissertationSoviet> relList = getList(builder);
        List<Wrapper> resultList = new ArrayList<>();
        for (RelEducationLevelScientificDissertationSoviet rel : relList) {
            Wrapper wrapper = new Wrapper();
            DissertationSoviet soviet = rel.getDissertationSoviet();
            wrapper.setSoviet(soviet);
            wrapper.setChairman(getPost(soviet, "1"));//председатель
            wrapper.setSecretary(getPost(soviet, "3"));//секретарь
            resultList.add(wrapper);
        }
        UniBaseUtils.createPage(dataSource, resultList);

    }

    public StaffDissertationSoviet getPost(DissertationSoviet soviet, String postCode) {
        //поиск ФИО по должности и диссертационному совету
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(RelDissertationSovietStaffPost.class, "rel")
                .where(DQLExpressions.eq(DQLExpressions.property(RelDissertationSovietStaffPost.dissertationSoviet().fromAlias("rel")), DQLExpressions.value(soviet)))
                .where(DQLExpressions.eq(DQLExpressions.property(RelDissertationSovietStaffPost.postDissertationSoviet().code().fromAlias("rel")), DQLExpressions.value(postCode)));
        if (!getList(builder).isEmpty())
            return ((RelDissertationSovietStaffPost) getList(builder).get(0)).getStaffDissertationSoviet();
        else
            return null;
    }

}
