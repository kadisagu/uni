package ru.tandemservice.aspirantrmc.entity;

import org.tandemframework.core.common.ITitled;
import ru.tandemservice.aspirantrmc.entity.gen.OuterParticipantGen;

/**
 * Внешний сотрудник
 */
public class OuterParticipant extends OuterParticipantGen implements ITitled {
    @Override
    public String getFullTitle() {
        return "Внешний сотрудник: " + getTitle();
    }

    @Override
    public String getFullTitlePR() {
        return "Внешний сотрудник";
    }

    @Override
    public String getTitle() {
        return getPerson().getTitle();
    }

    @Override
    public String getFio() {
        return getPerson().getFio();
    }

    @Override
    public String getContextTitle()
    {
        return getFullTitle();
    }
}