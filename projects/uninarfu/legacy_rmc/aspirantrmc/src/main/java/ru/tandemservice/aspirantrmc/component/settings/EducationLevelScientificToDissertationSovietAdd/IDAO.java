package ru.tandemservice.aspirantrmc.component.settings.EducationLevelScientificToDissertationSovietAdd;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {
    public void onChange(Model model);
}
