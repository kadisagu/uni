package ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.aspirantrmc.entity.OuterParticipant;
import ru.tandemservice.uni.dao.IUniBaseDao;

import java.util.ArrayList;
import java.util.List;


public class PersonAutocompleteModel extends FullCheckSelectModel {

    private final List<Long> outerParticipants = new ArrayList<Long>();

    @Override
    public ListResult<Person> findValues(String filter) {

        DQLSelectBuilder dqlEmployee = new DQLSelectBuilder();
        dqlEmployee.fromEntity(Employee.class, "emp");
        dqlEmployee.column(Employee.person().id().fromAlias("emp").s());

        DQLSelectBuilder dqlOuterParticipant = new DQLSelectBuilder();
        dqlOuterParticipant.fromEntity(OuterParticipant.class, "o");
        dqlOuterParticipant.column(OuterParticipant.person().id().fromAlias("o").s());

        List<Long> ids = IUniBaseDao.instance.get().getList(dqlOuterParticipant);

        outerParticipants.addAll(ids);

        dqlEmployee.union(dqlOuterParticipant.getSelectRule());

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Person.class, "e")
                .where(DQLExpressions.in(DQLExpressions.property(Person.id().fromAlias("e")), dqlEmployee.getQuery()));

        if (StringUtils.isNotEmpty(filter))
            builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property("e", Person.identityCard().fullFio())), DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));

        builder.order("e." + Person.identityCard().fullFio());

        return new ListResult(IUniBaseDao.instance.get().getList(builder));
    }

    @Override
    public String getLabelFor(Object value, int columnIndex) {
        Person person = (Person) value;
        if (outerParticipants.contains(person.getId()))
            return person.getTitle() + " (внешний)";
        else
            return person.getTitle();
    }

}
