package ru.tandemservice.aspirantrmc.component.student.DefenseDissertationWork.DefenseDissertationWorkPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.aspirantrmc.entity.DefenseDissertationWork;
import ru.tandemservice.uni.entity.employee.Student;

@Input({
        @Bind(key = "studentId", binding = "studentId"),
})
public class Model {

    private Long studentId;
    private Student student;
    //	private DynamicListDataSource dataSource;
    private DefenseDissertationWork dissertationWork;

    public DefenseDissertationWork getDissertationWork() {
        return dissertationWork;
    }

    public void setDissertationWork(DefenseDissertationWork dissertationWork) {
        this.dissertationWork = dissertationWork;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }


}
