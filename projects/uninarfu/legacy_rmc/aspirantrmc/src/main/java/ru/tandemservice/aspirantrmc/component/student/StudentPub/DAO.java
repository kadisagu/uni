package ru.tandemservice.aspirantrmc.component.student.StudentPub;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.aspirantrmc.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.aspirantrmc.entity.CandidateExams;
import ru.tandemservice.aspirantrmc.entity.LibCardNumber;
import ru.tandemservice.unibase.UniBaseUtils;


public class DAO extends ru.tandemservice.uni.component.student.StudentPub.DAO implements IDAO
{

    @Override
    public void prepare(ru.tandemservice.uni.component.student.StudentPub.Model model)
    {
        super.prepare(model);
        Model myModel = (Model) model;
        if (model.getStudent().getStudentCategory().getCode().equals(StudentCategoryCodes.STUDENT_CATEGORY_ASPIRANT) || model.getStudent().getStudentCategory().getCode().equals(StudentCategoryCodes.STUDENT_CATEGORY_APPLICANT))
        {
//			если категория обучаемого аспирант или соискатель
            myModel.setAspirant(true);
            myModel.setStudentTabCaption("Обучающийся");
            myModel.setStudentDataTabCaption("Данные обучающегося");
            myModel.setEditStudentDataCaption("Редактировать данные обучающегося");
            myModel.setEditStudentAdditionalDataCaption("Редактировать дополнительные данные обучающегося");

        } else
        {
            myModel.setAspirant(false);
            myModel.setStudentTabCaption("Студент");
            myModel.setStudentDataTabCaption("Данные студента");
            myModel.setEditStudentDataCaption("Редактировать данные студента");
            myModel.setEditStudentAdditionalDataCaption("Редактировать дополнительные данные студента");
            LibCardNumber libCardNumber = get(LibCardNumber.class, LibCardNumber.student(), model.getStudent());
            if (libCardNumber != null)
                myModel.setLibCardNumber(libCardNumber);
        }
        if (!getCandidateExamsBuilder(myModel).getResultList(getSession()).isEmpty())
            myModel.setListActive(true);
    }

    @Override
    public void prepareListDataSource(ru.tandemservice.uni.component.student.StudentPub.Model myModel)
    {
        Model model = (Model) myModel;
        MQBuilder builder = getCandidateExamsBuilder(model);
        new OrderDescriptionRegistry("lst").applyOrder(builder, model.getDataSource().getEntityOrder());
        int count = Math.max(builder.getResultList(getSession()).size() + 1, 2);
        model.getDataSource().setTotalSize(count);
        model.getDataSource().setCountRow(count);
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }

    @Override
    public LibCardNumber getLibCardNumber()
    {
        return null;
    }

    public MQBuilder getCandidateExamsBuilder(Model model)
    {
        return new MQBuilder(CandidateExams.ENTITY_CLASS, "lst").
                add(MQExpression.eq("lst", CandidateExams.L_ASPIRANT, model.getStudent()));
    }
}
