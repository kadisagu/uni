package ru.tandemservice.aspirantrmc.base.bo.Mission.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.fias.base.bo.AddressBase.AddressBaseManager;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInline;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInlineConfig;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInlineUI;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import ru.tandemservice.aspirantrmc.base.bo.Mission.MissionManager;
import ru.tandemservice.aspirantrmc.entity.ScienceMission;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;

@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "studentHolder.id"),
        @Bind(key = MissionAddEditUI.MISSION_ID_KEY, binding = "missionId")
})
public class MissionAddEditUI extends UIPresenter {
    public static final String MISSION_ID_KEY = "missionId";

    private Long missionId;
    private EntityHolder<Student> studentHolder = new EntityHolder<Student>();
    private ScienceMission mission;

    //private Model addressModel = new Model();


    @Override
    public void onComponentRefresh() {
        studentHolder.refresh(Student.class);
        if (missionId != null) {
            mission = UniDaoFacade.getCoreDao().get(ScienceMission.class, missionId);
        }
        else {
            mission = new ScienceMission();
            mission.setStudent(studentHolder.getValue());
            mission.setStatusMission(MissionManager.instance().modifyDao().getDefaultStatusMission());
        }

        AddressBaseEditInlineConfig addressConfig = new AddressBaseEditInlineConfig();
        addressConfig.setEntityId(mission.getId());
        addressConfig.setDetailedOnly(true);
        addressConfig.setDetailLevel(4);
        addressConfig.setOffice(true);
        addressConfig.setInline(true);
        addressConfig.setFieldSetTitle("Место назначения:");
        addressConfig.setAddressProperty(ScienceMission.address().s());

        this._uiActivation.asRegion(AddressBaseEditInline.class, "addressRegion").parameter("addressEditUIConfig", addressConfig).activate();
    }


    public void onClickApply() {
        AddressBaseEditInlineUI legalAddressEditUI = (AddressBaseEditInlineUI) this._uiSupport.getChildUI("addressRegion");
        AddressDetailed address = (AddressDetailed) legalAddressEditUI.getResult();
        AddressBaseManager.instance().dao().saveOrUpdateAddressWithoutEntityUpdate(mission, address, ScienceMission.address().s());

        UniDaoFacade.getCoreDao().saveOrUpdate(mission);
        deactivate();
    }

    public Long getMissionId() {
        return missionId;
    }

    public void setMissionId(Long missionId) {
        this.missionId = missionId;
    }

    public Student getStudent() {
        return studentHolder.getValue();
    }

    public EntityHolder<Student> getStudentHolder() {
        return studentHolder;
    }

    public void setStudentHolder(EntityHolder<Student> studentHolder) {
        this.studentHolder = studentHolder;
    }

    public ScienceMission getMission() {
        return mission;
    }

    public void setMission(ScienceMission mission) {
        this.mission = mission;
    }
}
