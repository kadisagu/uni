package ru.tandemservice.aspirantrmc.component.student.DefenseDissertationWork.DefenseDissertationWorkPub;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.aspirantrmc.entity.DefenseDissertationWork;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;

public class DAO extends UniDao<Model> implements IDAO {
    @Override
    public void prepare(Model model) {
        if (model.getStudentId() != null) {
            model.setStudent(getNotNull(Student.class, model.getStudentId()));
        }

        if (getDissertationWork(model.getStudent()) != null) {
            model.setDissertationWork(getDissertationWork(model.getStudent()));
        }// else
        //if (getScientificResearch(model.getStudent())!= null){
        //	update(model);
        //		}


    }

    /*
    @Override
     public void update(Model model) {
        DefenseDissertationWork entity = new DefenseDissertationWork();
        entity.setAspirant(model.getStudent());
        //entity.setTitle(getScientificResearch(model.getStudent()).getTitle());

        model.setDissertationWork(entity);
    }
    */
    public DefenseDissertationWork getDissertationWork(Student student) {
        MQBuilder builder = new MQBuilder(DefenseDissertationWork.ENTITY_NAME, "ddw")
                .add(MQExpression.eq("ddw", DefenseDissertationWork.L_ASPIRANT, student));
        return (DefenseDissertationWork) builder.uniqueResult(getSession());
    }
    /*
	public ScientificResearch getScientificResearch(Student student){
		MQBuilder builder = new MQBuilder(ScientificResearch.ENTITY_NAME, "sr")
			.add(MQExpression.eq("sr", ScientificResearch.L_STUDENT, student));
		return (ScientificResearch) builder.uniqueResult(getSession());
	}
	*/
}
