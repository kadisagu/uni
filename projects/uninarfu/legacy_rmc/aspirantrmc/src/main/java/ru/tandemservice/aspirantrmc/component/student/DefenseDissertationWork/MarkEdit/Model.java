package ru.tandemservice.aspirantrmc.component.student.DefenseDissertationWork.MarkEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;

@Input({
        @Bind(key = "studentId", binding = "studentId"),
        @Bind(key = "dissertationWorkId", binding = "dissertationWorkId"),
})
public class Model {

    private Long studentId;
    private Student student;
    private Long dissertationWorkId;

    private ISelectModel markModel;
    private SessionMarkGradeValueCatalogItem mark;


    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getDissertationWorkId() {
        return dissertationWorkId;
    }

    public void setDissertationWorkId(Long dissertationWorkId) {
        this.dissertationWorkId = dissertationWorkId;
    }

    public ISelectModel getMarkModel() {
        return markModel;
    }

    public void setMarkModel(ISelectModel markModel) {
        this.markModel = markModel;
    }

    public SessionMarkGradeValueCatalogItem getMark() {
        return mark;
    }

    public void setMark(SessionMarkGradeValueCatalogItem mark) {
        this.mark = mark;
    }


}
