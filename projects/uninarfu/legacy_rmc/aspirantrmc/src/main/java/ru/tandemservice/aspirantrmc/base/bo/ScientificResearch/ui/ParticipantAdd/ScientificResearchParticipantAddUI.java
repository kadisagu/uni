package ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.ParticipantAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ScientificResearchManager;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.logic.PersonAutocompleteModel;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.OuterParticipantAddEdit.ScientificResearchOuterParticipantAddEdit;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.OuterParticipantAddEdit.ScientificResearchOuterParticipantAddEditUI;
import ru.tandemservice.aspirantrmc.entity.OuterParticipant;
import ru.tandemservice.aspirantrmc.entity.ScienceParticipant;
import ru.tandemservice.aspirantrmc.entity.ScientificResearch;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;

import java.util.Map;


@State({
        @Bind(key = "researchId", binding = "researchId"),
        @Bind(key = "participantId", binding = "participantId"),
        @Bind(key = "participantType", binding = "participantType")
})
public class ScientificResearchParticipantAddUI extends UIPresenter {

    public static final String REGION = "outherParticipantRegion";

    private Long researchId;
    private Long participantId;
    private String participantType;

    private ScientificResearch research;
    private ScienceParticipant participant;

    private String academicStatuses;
    private String academicDegree;

    private ISelectModel personModel;

    @Override
    public void onComponentRefresh() {
        if (participant == null) {
            if (participantId != null) {
                participant = UniDaoFacade.getCoreDao().get(ScienceParticipant.class, participantId);
                research = participant.getResearch();
            }
            else if (researchId != null) {
                research = UniDaoFacade.getCoreDao().get(ScientificResearch.class, researchId);
                participant = new ScienceParticipant();
                participant.setParticipantType(ScientificResearchManager.instance().modifyDao().getParticipantTypeByCode(participantType));
                participant.setResearch(research);
                participant.setIsInner(true);
            }
            setPersonModel(new PersonAutocompleteModel());
        }
        if (getParticipant().getPerson() != null)
            updateStatusAndDegree();
    }

    public void updateStatusAndDegree() {
        participant.setIsInner(IUniBaseDao.instance.get().get(OuterParticipant.class, OuterParticipant.person().s(), getParticipant().getPerson()) == null);
        if (participant.getPerson() != null) {
            academicStatuses = ScientificResearchManager.instance().modifyDao().getStatusesFromPerson(participant.getPerson());
            academicDegree = ScientificResearchManager.instance().modifyDao().getDegreeFromPersonStr(participant.getPerson());
        }
    }

    @Override
    public void onComponentBindReturnParameters(String s,
                                                Map<String, Object> map)
    {
        if (REGION.equals(s)) {
            Object id = map.get(ScientificResearchOuterParticipantAddEditUI.OUTHER_PARTICIPANT_ID);
            if (id instanceof Long) {
                OuterParticipant outerParticipant = (OuterParticipant) IUniBaseDao.instance.get().get((Long) id);
                this.getParticipant().setPerson(outerParticipant.getPerson());
                this.getParticipant().setIsInner(false);
            }
        }
    }

    public void onClickAddOutherParticipant() {
        _uiConfig.getBusinessComponent().createRegion(REGION, new ComponentActivator(ScientificResearchOuterParticipantAddEdit.class.getSimpleName()));
    }

    public void onClickApply() {
        if (participant.getPerson() != null)
            UniDaoFacade.getCoreDao().saveOrUpdate(participant);
        deactivate();
    }

    public ScientificResearch getResearch() {
        return research;
    }

    public void setResearch(ScientificResearch research) {
        this.research = research;
    }

    public ScienceParticipant getParticipant() {
        return participant;
    }

    public void setParticipant(ScienceParticipant participant) {
        this.participant = participant;
    }

    public Long getResearchId() {
        return researchId;
    }

    public void setResearchId(Long researchId) {
        this.researchId = researchId;
    }

    public Long getParticipantId() {
        return participantId;
    }

    public void setParticipantId(Long participantId) {
        this.participantId = participantId;
    }

    public String getAcademicStatuses() {
        return academicStatuses;
    }

    public void setAcademicStatuses(String academicStatuses) {
        this.academicStatuses = academicStatuses;
    }

    public String getAcademicDegree() {
        return academicDegree;
    }

    public void setAcademicDegree(String academicDegree) {
        this.academicDegree = academicDegree;
    }

    public String getParticipantType() {
        return participantType;
    }

    public void setParticipantType(String participantType) {
        this.participantType = participantType;
    }

    public ISelectModel getPersonModel() {
        return personModel;
    }

    public void setPersonModel(ISelectModel personModel) {
        this.personModel = personModel;
    }

}
