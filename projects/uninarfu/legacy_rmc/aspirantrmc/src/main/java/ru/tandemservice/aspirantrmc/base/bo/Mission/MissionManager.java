package ru.tandemservice.aspirantrmc.base.bo.Mission;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import ru.tandemservice.aspirantrmc.base.bo.Mission.logic.DAO;
import ru.tandemservice.aspirantrmc.base.bo.Mission.logic.IDAO;
import ru.tandemservice.aspirantrmc.entity.catalog.Financing;
import ru.tandemservice.aspirantrmc.entity.catalog.StatusMission;

@Configuration
public class MissionManager extends BusinessObjectManager {
    public static final String FINANCING_DS = "financingDS";
    public static final String STATUS_MISSION_DS = "statusMissionDS";

    public static MissionManager instance() {
        return instance(MissionManager.class);
    }

    @Bean
    public IDAO modifyDao() {
        return new DAO();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> financeDSHandler() {
        return new DefaultComboDataSourceHandler(getName(), Financing.class);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> statusMissionDSHadnler() {
        return new DefaultComboDataSourceHandler(getName(), StatusMission.class);
    }
}
