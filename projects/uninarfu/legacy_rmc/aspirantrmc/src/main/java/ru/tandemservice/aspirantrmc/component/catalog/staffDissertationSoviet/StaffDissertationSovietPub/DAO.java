package ru.tandemservice.aspirantrmc.component.catalog.staffDissertationSoviet.StaffDissertationSovietPub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.aspirantrmc.entity.catalog.StaffDissertationSoviet;

public class DAO extends DefaultCatalogPubDAO<StaffDissertationSoviet, Model> implements IDAO {
    @Override
    protected void applyFilters(Model model, MQBuilder builder) {
        String title = (String) model.getSettings().get("title");
        if (StringUtils.isNotEmpty(title))
            builder.add(MQExpression.like("ci", StaffDissertationSoviet.lastName(), CoreStringUtils.escapeLike(title)));
    }
}
