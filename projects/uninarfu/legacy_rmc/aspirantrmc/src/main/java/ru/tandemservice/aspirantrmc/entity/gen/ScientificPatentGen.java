package ru.tandemservice.aspirantrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.aspirantrmc.entity.ScientificPatent;
import ru.tandemservice.aspirantrmc.entity.catalog.TypeInvention;
import ru.tandemservice.uni.entity.employee.Student;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Изобретение аспиранта/соискателя
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ScientificPatentGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.aspirantrmc.entity.ScientificPatent";
    public static final String ENTITY_NAME = "scientificPatent";
    public static final int VERSION_HASH = -1166227744;
    private static IEntityMeta ENTITY_META;

    public static final String L_AUTHOR = "author";
    public static final String L_INVENTION_TYPE = "inventionType";
    public static final String P_TITLE = "title";
    public static final String P_CERTIFICATE_NUMBER = "certificateNumber";
    public static final String P_PATENT_NUMBER = "patentNumber";
    public static final String P_COLLABORATORS = "collaborators";
    public static final String P_IMPRINT_DATE = "imprintDate";
    public static final String P_NOTE = "note";

    private Student _author;     // Автор
    private TypeInvention _inventionType;     // Вид изобретения
    private String _title;     // Наименование изобретения
    private String _certificateNumber;     // Номер авторского свидетельства
    private String _patentNumber;     // Номер патента
    private String _collaborators;     // Фамилия И.О.соавторов и их ученые степени и звания
    private Integer _imprintDate;     // Год издания
    private String _note;     // Примечание

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Автор.
     */
    public Student getAuthor()
    {
        return _author;
    }

    /**
     * @param author Автор.
     */
    public void setAuthor(Student author)
    {
        dirty(_author, author);
        _author = author;
    }

    /**
     * @return Вид изобретения.
     */
    public TypeInvention getInventionType()
    {
        return _inventionType;
    }

    /**
     * @param inventionType Вид изобретения.
     */
    public void setInventionType(TypeInvention inventionType)
    {
        dirty(_inventionType, inventionType);
        _inventionType = inventionType;
    }

    /**
     * @return Наименование изобретения. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Наименование изобретения. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Номер авторского свидетельства.
     */
    @Length(max=255)
    public String getCertificateNumber()
    {
        return _certificateNumber;
    }

    /**
     * @param certificateNumber Номер авторского свидетельства.
     */
    public void setCertificateNumber(String certificateNumber)
    {
        dirty(_certificateNumber, certificateNumber);
        _certificateNumber = certificateNumber;
    }

    /**
     * @return Номер патента.
     */
    @Length(max=255)
    public String getPatentNumber()
    {
        return _patentNumber;
    }

    /**
     * @param patentNumber Номер патента.
     */
    public void setPatentNumber(String patentNumber)
    {
        dirty(_patentNumber, patentNumber);
        _patentNumber = patentNumber;
    }

    /**
     * @return Фамилия И.О.соавторов и их ученые степени и звания.
     */
    @Length(max=2048)
    public String getCollaborators()
    {
        return _collaborators;
    }

    /**
     * @param collaborators Фамилия И.О.соавторов и их ученые степени и звания.
     */
    public void setCollaborators(String collaborators)
    {
        dirty(_collaborators, collaborators);
        _collaborators = collaborators;
    }

    /**
     * @return Год издания.
     */
    public Integer getImprintDate()
    {
        return _imprintDate;
    }

    /**
     * @param imprintDate Год издания.
     */
    public void setImprintDate(Integer imprintDate)
    {
        dirty(_imprintDate, imprintDate);
        _imprintDate = imprintDate;
    }

    /**
     * @return Примечание.
     */
    @Length(max=255)
    public String getNote()
    {
        return _note;
    }

    /**
     * @param note Примечание.
     */
    public void setNote(String note)
    {
        dirty(_note, note);
        _note = note;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ScientificPatentGen)
        {
            setAuthor(((ScientificPatent)another).getAuthor());
            setInventionType(((ScientificPatent)another).getInventionType());
            setTitle(((ScientificPatent)another).getTitle());
            setCertificateNumber(((ScientificPatent)another).getCertificateNumber());
            setPatentNumber(((ScientificPatent)another).getPatentNumber());
            setCollaborators(((ScientificPatent)another).getCollaborators());
            setImprintDate(((ScientificPatent)another).getImprintDate());
            setNote(((ScientificPatent)another).getNote());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ScientificPatentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ScientificPatent.class;
        }

        public T newInstance()
        {
            return (T) new ScientificPatent();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "author":
                    return obj.getAuthor();
                case "inventionType":
                    return obj.getInventionType();
                case "title":
                    return obj.getTitle();
                case "certificateNumber":
                    return obj.getCertificateNumber();
                case "patentNumber":
                    return obj.getPatentNumber();
                case "collaborators":
                    return obj.getCollaborators();
                case "imprintDate":
                    return obj.getImprintDate();
                case "note":
                    return obj.getNote();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "author":
                    obj.setAuthor((Student) value);
                    return;
                case "inventionType":
                    obj.setInventionType((TypeInvention) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "certificateNumber":
                    obj.setCertificateNumber((String) value);
                    return;
                case "patentNumber":
                    obj.setPatentNumber((String) value);
                    return;
                case "collaborators":
                    obj.setCollaborators((String) value);
                    return;
                case "imprintDate":
                    obj.setImprintDate((Integer) value);
                    return;
                case "note":
                    obj.setNote((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "author":
                        return true;
                case "inventionType":
                        return true;
                case "title":
                        return true;
                case "certificateNumber":
                        return true;
                case "patentNumber":
                        return true;
                case "collaborators":
                        return true;
                case "imprintDate":
                        return true;
                case "note":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "author":
                    return true;
                case "inventionType":
                    return true;
                case "title":
                    return true;
                case "certificateNumber":
                    return true;
                case "patentNumber":
                    return true;
                case "collaborators":
                    return true;
                case "imprintDate":
                    return true;
                case "note":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "author":
                    return Student.class;
                case "inventionType":
                    return TypeInvention.class;
                case "title":
                    return String.class;
                case "certificateNumber":
                    return String.class;
                case "patentNumber":
                    return String.class;
                case "collaborators":
                    return String.class;
                case "imprintDate":
                    return Integer.class;
                case "note":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ScientificPatent> _dslPath = new Path<ScientificPatent>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ScientificPatent");
    }
            

    /**
     * @return Автор.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificPatent#getAuthor()
     */
    public static Student.Path<Student> author()
    {
        return _dslPath.author();
    }

    /**
     * @return Вид изобретения.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificPatent#getInventionType()
     */
    public static TypeInvention.Path<TypeInvention> inventionType()
    {
        return _dslPath.inventionType();
    }

    /**
     * @return Наименование изобретения. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificPatent#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Номер авторского свидетельства.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificPatent#getCertificateNumber()
     */
    public static PropertyPath<String> certificateNumber()
    {
        return _dslPath.certificateNumber();
    }

    /**
     * @return Номер патента.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificPatent#getPatentNumber()
     */
    public static PropertyPath<String> patentNumber()
    {
        return _dslPath.patentNumber();
    }

    /**
     * @return Фамилия И.О.соавторов и их ученые степени и звания.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificPatent#getCollaborators()
     */
    public static PropertyPath<String> collaborators()
    {
        return _dslPath.collaborators();
    }

    /**
     * @return Год издания.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificPatent#getImprintDate()
     */
    public static PropertyPath<Integer> imprintDate()
    {
        return _dslPath.imprintDate();
    }

    /**
     * @return Примечание.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificPatent#getNote()
     */
    public static PropertyPath<String> note()
    {
        return _dslPath.note();
    }

    public static class Path<E extends ScientificPatent> extends EntityPath<E>
    {
        private Student.Path<Student> _author;
        private TypeInvention.Path<TypeInvention> _inventionType;
        private PropertyPath<String> _title;
        private PropertyPath<String> _certificateNumber;
        private PropertyPath<String> _patentNumber;
        private PropertyPath<String> _collaborators;
        private PropertyPath<Integer> _imprintDate;
        private PropertyPath<String> _note;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Автор.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificPatent#getAuthor()
     */
        public Student.Path<Student> author()
        {
            if(_author == null )
                _author = new Student.Path<Student>(L_AUTHOR, this);
            return _author;
        }

    /**
     * @return Вид изобретения.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificPatent#getInventionType()
     */
        public TypeInvention.Path<TypeInvention> inventionType()
        {
            if(_inventionType == null )
                _inventionType = new TypeInvention.Path<TypeInvention>(L_INVENTION_TYPE, this);
            return _inventionType;
        }

    /**
     * @return Наименование изобретения. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificPatent#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(ScientificPatentGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Номер авторского свидетельства.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificPatent#getCertificateNumber()
     */
        public PropertyPath<String> certificateNumber()
        {
            if(_certificateNumber == null )
                _certificateNumber = new PropertyPath<String>(ScientificPatentGen.P_CERTIFICATE_NUMBER, this);
            return _certificateNumber;
        }

    /**
     * @return Номер патента.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificPatent#getPatentNumber()
     */
        public PropertyPath<String> patentNumber()
        {
            if(_patentNumber == null )
                _patentNumber = new PropertyPath<String>(ScientificPatentGen.P_PATENT_NUMBER, this);
            return _patentNumber;
        }

    /**
     * @return Фамилия И.О.соавторов и их ученые степени и звания.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificPatent#getCollaborators()
     */
        public PropertyPath<String> collaborators()
        {
            if(_collaborators == null )
                _collaborators = new PropertyPath<String>(ScientificPatentGen.P_COLLABORATORS, this);
            return _collaborators;
        }

    /**
     * @return Год издания.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificPatent#getImprintDate()
     */
        public PropertyPath<Integer> imprintDate()
        {
            if(_imprintDate == null )
                _imprintDate = new PropertyPath<Integer>(ScientificPatentGen.P_IMPRINT_DATE, this);
            return _imprintDate;
        }

    /**
     * @return Примечание.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificPatent#getNote()
     */
        public PropertyPath<String> note()
        {
            if(_note == null )
                _note = new PropertyPath<String>(ScientificPatentGen.P_NOTE, this);
            return _note;
        }

        public Class getEntityClass()
        {
            return ScientificPatent.class;
        }

        public String getEntityName()
        {
            return "scientificPatent";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
