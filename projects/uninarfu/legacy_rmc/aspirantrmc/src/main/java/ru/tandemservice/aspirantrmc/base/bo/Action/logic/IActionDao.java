package ru.tandemservice.aspirantrmc.base.bo.Action.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.aspirantrmc.entity.AspActionOriginator;

public interface IActionDao extends INeedPersistenceSupport {
    public ListResult getAspOriginatorsListResult(String filter);

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public AspActionOriginator getOriginatorByTitle(String name);
}
