package ru.tandemservice.aspirantrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.aspirantrmc.entity.DefenseDissertationWork;
import ru.tandemservice.aspirantrmc.entity.catalog.DissertationSoviet;
import ru.tandemservice.aspirantrmc.entity.catalog.StaffDissertationSoviet;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Защита диссертационной работы
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DefenseDissertationWorkGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.aspirantrmc.entity.DefenseDissertationWork";
    public static final String ENTITY_NAME = "defenseDissertationWork";
    public static final int VERSION_HASH = 941697051;
    private static IEntityMeta ENTITY_META;

    public static final String L_ASPIRANT = "aspirant";
    public static final String P_TITLE = "title";
    public static final String L_MARK = "mark";
    public static final String L_DISSERTATION_SOVIET = "dissertationSoviet";
    public static final String L_EDUCATION_LEVEL_SCIENTIFIC = "educationLevelScientific";
    public static final String L_CHAIRMAN = "chairman";
    public static final String L_VICE_CHAIRMAN = "viceChairman";
    public static final String L_SECRETARY = "secretary";

    private Student _aspirant;     // Аспирант
    private String _title;     // Тема диссертационной работы
    private SessionMarkGradeValueCatalogItem _mark;     // Оценка
    private DissertationSoviet _dissertationSoviet;     // Диссертационный совет
    private EducationLevelsHighSchool _educationLevelScientific;     // Направление подготовки
    private StaffDissertationSoviet _chairman;     // Председатель
    private StaffDissertationSoviet _viceChairman;     // Заместитель председателя
    private StaffDissertationSoviet _secretary;     // Секретарь

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Аспирант. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public Student getAspirant()
    {
        return _aspirant;
    }

    /**
     * @param aspirant Аспирант. Свойство не может быть null и должно быть уникальным.
     */
    public void setAspirant(Student aspirant)
    {
        dirty(_aspirant, aspirant);
        _aspirant = aspirant;
    }

    /**
     * @return Тема диссертационной работы. Свойство не может быть null.
     */
    @NotNull
    @Length(max=1024)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Тема диссертационной работы. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Оценка.
     */
    public SessionMarkGradeValueCatalogItem getMark()
    {
        return _mark;
    }

    /**
     * @param mark Оценка.
     */
    public void setMark(SessionMarkGradeValueCatalogItem mark)
    {
        dirty(_mark, mark);
        _mark = mark;
    }

    /**
     * @return Диссертационный совет.
     */
    public DissertationSoviet getDissertationSoviet()
    {
        return _dissertationSoviet;
    }

    /**
     * @param dissertationSoviet Диссертационный совет.
     */
    public void setDissertationSoviet(DissertationSoviet dissertationSoviet)
    {
        dirty(_dissertationSoviet, dissertationSoviet);
        _dissertationSoviet = dissertationSoviet;
    }

    /**
     * @return Направление подготовки.
     */
    public EducationLevelsHighSchool getEducationLevelScientific()
    {
        return _educationLevelScientific;
    }

    /**
     * @param educationLevelScientific Направление подготовки.
     */
    public void setEducationLevelScientific(EducationLevelsHighSchool educationLevelScientific)
    {
        dirty(_educationLevelScientific, educationLevelScientific);
        _educationLevelScientific = educationLevelScientific;
    }

    /**
     * @return Председатель.
     */
    public StaffDissertationSoviet getChairman()
    {
        return _chairman;
    }

    /**
     * @param chairman Председатель.
     */
    public void setChairman(StaffDissertationSoviet chairman)
    {
        dirty(_chairman, chairman);
        _chairman = chairman;
    }

    /**
     * @return Заместитель председателя.
     */
    public StaffDissertationSoviet getViceChairman()
    {
        return _viceChairman;
    }

    /**
     * @param viceChairman Заместитель председателя.
     */
    public void setViceChairman(StaffDissertationSoviet viceChairman)
    {
        dirty(_viceChairman, viceChairman);
        _viceChairman = viceChairman;
    }

    /**
     * @return Секретарь.
     */
    public StaffDissertationSoviet getSecretary()
    {
        return _secretary;
    }

    /**
     * @param secretary Секретарь.
     */
    public void setSecretary(StaffDissertationSoviet secretary)
    {
        dirty(_secretary, secretary);
        _secretary = secretary;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DefenseDissertationWorkGen)
        {
            setAspirant(((DefenseDissertationWork)another).getAspirant());
            setTitle(((DefenseDissertationWork)another).getTitle());
            setMark(((DefenseDissertationWork)another).getMark());
            setDissertationSoviet(((DefenseDissertationWork)another).getDissertationSoviet());
            setEducationLevelScientific(((DefenseDissertationWork)another).getEducationLevelScientific());
            setChairman(((DefenseDissertationWork)another).getChairman());
            setViceChairman(((DefenseDissertationWork)another).getViceChairman());
            setSecretary(((DefenseDissertationWork)another).getSecretary());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DefenseDissertationWorkGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DefenseDissertationWork.class;
        }

        public T newInstance()
        {
            return (T) new DefenseDissertationWork();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "aspirant":
                    return obj.getAspirant();
                case "title":
                    return obj.getTitle();
                case "mark":
                    return obj.getMark();
                case "dissertationSoviet":
                    return obj.getDissertationSoviet();
                case "educationLevelScientific":
                    return obj.getEducationLevelScientific();
                case "chairman":
                    return obj.getChairman();
                case "viceChairman":
                    return obj.getViceChairman();
                case "secretary":
                    return obj.getSecretary();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "aspirant":
                    obj.setAspirant((Student) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "mark":
                    obj.setMark((SessionMarkGradeValueCatalogItem) value);
                    return;
                case "dissertationSoviet":
                    obj.setDissertationSoviet((DissertationSoviet) value);
                    return;
                case "educationLevelScientific":
                    obj.setEducationLevelScientific((EducationLevelsHighSchool) value);
                    return;
                case "chairman":
                    obj.setChairman((StaffDissertationSoviet) value);
                    return;
                case "viceChairman":
                    obj.setViceChairman((StaffDissertationSoviet) value);
                    return;
                case "secretary":
                    obj.setSecretary((StaffDissertationSoviet) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "aspirant":
                        return true;
                case "title":
                        return true;
                case "mark":
                        return true;
                case "dissertationSoviet":
                        return true;
                case "educationLevelScientific":
                        return true;
                case "chairman":
                        return true;
                case "viceChairman":
                        return true;
                case "secretary":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "aspirant":
                    return true;
                case "title":
                    return true;
                case "mark":
                    return true;
                case "dissertationSoviet":
                    return true;
                case "educationLevelScientific":
                    return true;
                case "chairman":
                    return true;
                case "viceChairman":
                    return true;
                case "secretary":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "aspirant":
                    return Student.class;
                case "title":
                    return String.class;
                case "mark":
                    return SessionMarkGradeValueCatalogItem.class;
                case "dissertationSoviet":
                    return DissertationSoviet.class;
                case "educationLevelScientific":
                    return EducationLevelsHighSchool.class;
                case "chairman":
                    return StaffDissertationSoviet.class;
                case "viceChairman":
                    return StaffDissertationSoviet.class;
                case "secretary":
                    return StaffDissertationSoviet.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DefenseDissertationWork> _dslPath = new Path<DefenseDissertationWork>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DefenseDissertationWork");
    }
            

    /**
     * @return Аспирант. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.aspirantrmc.entity.DefenseDissertationWork#getAspirant()
     */
    public static Student.Path<Student> aspirant()
    {
        return _dslPath.aspirant();
    }

    /**
     * @return Тема диссертационной работы. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.DefenseDissertationWork#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Оценка.
     * @see ru.tandemservice.aspirantrmc.entity.DefenseDissertationWork#getMark()
     */
    public static SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem> mark()
    {
        return _dslPath.mark();
    }

    /**
     * @return Диссертационный совет.
     * @see ru.tandemservice.aspirantrmc.entity.DefenseDissertationWork#getDissertationSoviet()
     */
    public static DissertationSoviet.Path<DissertationSoviet> dissertationSoviet()
    {
        return _dslPath.dissertationSoviet();
    }

    /**
     * @return Направление подготовки.
     * @see ru.tandemservice.aspirantrmc.entity.DefenseDissertationWork#getEducationLevelScientific()
     */
    public static EducationLevelsHighSchool.Path<EducationLevelsHighSchool> educationLevelScientific()
    {
        return _dslPath.educationLevelScientific();
    }

    /**
     * @return Председатель.
     * @see ru.tandemservice.aspirantrmc.entity.DefenseDissertationWork#getChairman()
     */
    public static StaffDissertationSoviet.Path<StaffDissertationSoviet> chairman()
    {
        return _dslPath.chairman();
    }

    /**
     * @return Заместитель председателя.
     * @see ru.tandemservice.aspirantrmc.entity.DefenseDissertationWork#getViceChairman()
     */
    public static StaffDissertationSoviet.Path<StaffDissertationSoviet> viceChairman()
    {
        return _dslPath.viceChairman();
    }

    /**
     * @return Секретарь.
     * @see ru.tandemservice.aspirantrmc.entity.DefenseDissertationWork#getSecretary()
     */
    public static StaffDissertationSoviet.Path<StaffDissertationSoviet> secretary()
    {
        return _dslPath.secretary();
    }

    public static class Path<E extends DefenseDissertationWork> extends EntityPath<E>
    {
        private Student.Path<Student> _aspirant;
        private PropertyPath<String> _title;
        private SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem> _mark;
        private DissertationSoviet.Path<DissertationSoviet> _dissertationSoviet;
        private EducationLevelsHighSchool.Path<EducationLevelsHighSchool> _educationLevelScientific;
        private StaffDissertationSoviet.Path<StaffDissertationSoviet> _chairman;
        private StaffDissertationSoviet.Path<StaffDissertationSoviet> _viceChairman;
        private StaffDissertationSoviet.Path<StaffDissertationSoviet> _secretary;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Аспирант. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.aspirantrmc.entity.DefenseDissertationWork#getAspirant()
     */
        public Student.Path<Student> aspirant()
        {
            if(_aspirant == null )
                _aspirant = new Student.Path<Student>(L_ASPIRANT, this);
            return _aspirant;
        }

    /**
     * @return Тема диссертационной работы. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.DefenseDissertationWork#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(DefenseDissertationWorkGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Оценка.
     * @see ru.tandemservice.aspirantrmc.entity.DefenseDissertationWork#getMark()
     */
        public SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem> mark()
        {
            if(_mark == null )
                _mark = new SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem>(L_MARK, this);
            return _mark;
        }

    /**
     * @return Диссертационный совет.
     * @see ru.tandemservice.aspirantrmc.entity.DefenseDissertationWork#getDissertationSoviet()
     */
        public DissertationSoviet.Path<DissertationSoviet> dissertationSoviet()
        {
            if(_dissertationSoviet == null )
                _dissertationSoviet = new DissertationSoviet.Path<DissertationSoviet>(L_DISSERTATION_SOVIET, this);
            return _dissertationSoviet;
        }

    /**
     * @return Направление подготовки.
     * @see ru.tandemservice.aspirantrmc.entity.DefenseDissertationWork#getEducationLevelScientific()
     */
        public EducationLevelsHighSchool.Path<EducationLevelsHighSchool> educationLevelScientific()
        {
            if(_educationLevelScientific == null )
                _educationLevelScientific = new EducationLevelsHighSchool.Path<EducationLevelsHighSchool>(L_EDUCATION_LEVEL_SCIENTIFIC, this);
            return _educationLevelScientific;
        }

    /**
     * @return Председатель.
     * @see ru.tandemservice.aspirantrmc.entity.DefenseDissertationWork#getChairman()
     */
        public StaffDissertationSoviet.Path<StaffDissertationSoviet> chairman()
        {
            if(_chairman == null )
                _chairman = new StaffDissertationSoviet.Path<StaffDissertationSoviet>(L_CHAIRMAN, this);
            return _chairman;
        }

    /**
     * @return Заместитель председателя.
     * @see ru.tandemservice.aspirantrmc.entity.DefenseDissertationWork#getViceChairman()
     */
        public StaffDissertationSoviet.Path<StaffDissertationSoviet> viceChairman()
        {
            if(_viceChairman == null )
                _viceChairman = new StaffDissertationSoviet.Path<StaffDissertationSoviet>(L_VICE_CHAIRMAN, this);
            return _viceChairman;
        }

    /**
     * @return Секретарь.
     * @see ru.tandemservice.aspirantrmc.entity.DefenseDissertationWork#getSecretary()
     */
        public StaffDissertationSoviet.Path<StaffDissertationSoviet> secretary()
        {
            if(_secretary == null )
                _secretary = new StaffDissertationSoviet.Path<StaffDissertationSoviet>(L_SECRETARY, this);
            return _secretary;
        }

        public Class getEntityClass()
        {
            return DefenseDissertationWork.class;
        }

        public String getEntityName()
        {
            return "defenseDissertationWork";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
