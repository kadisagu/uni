/* $Id$ */
package ru.tandemservice.aspirantrmc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import ru.tandemservice.uni.migration.MS_uni_2x9x3_2to3;

/**
 * @author Nikolay Fedorovskih
 * @since 05.02.2016
 */
public class MS_aspirantrmc_2x9x3_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBeforeDependencies()
    {
        return new ScriptDependency[] {
                new ScriptDependency("org.tandemframework", "1.6.18"),
                new ScriptDependency("org.tandemframework.shared", "1.9.2"),
                new ScriptDependency("ru.tandemservice.uni.product", "2.9.2")
        };
    }

    @Override
    public ScriptDependency[] getAfterDependencies() {
        return new ScriptDependency[] {
                MigrationUtils.createScriptDependency(MS_uni_2x9x3_2to3.class)
        };
    }

    @Override
    public void run(DBTool tool) throws Exception {

        // Выставляем higher для аспирантских специальносте
        tool.executeUpdate("update structureeducationlevels_t set higher_p=? where code_p in (?,?)", true, "40", "41");

        // Удаляем неиспользуемые уровни
        tool.executeUpdate("delete from structureeducationlevels_t where code_p in (?,?,?,?)", "150", "151", "152", "153");
    }
}