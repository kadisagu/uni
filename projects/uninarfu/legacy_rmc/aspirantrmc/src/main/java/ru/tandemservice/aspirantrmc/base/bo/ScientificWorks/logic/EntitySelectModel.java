package ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.logic;

import org.hibernate.Session;
import org.tandemframework.core.entity.dsl.PropertyPath;
import org.tandemframework.tapsupport.component.selection.ListResult;
import org.tandemframework.tapsupport.component.selection.SingleSelectTextModel;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ScientificWorksManager;

/**
 * Created with IntelliJ IDEA.
 * User: ramaslov
 * Date: 31.01.13
 * Time: 15:30
 * To change this template use File | Settings | File Templates.
 */
public class EntitySelectModel extends SingleSelectTextModel {
    private Class entityClass;
    private PropertyPath<String> dslPath;
    private Session session;

    public EntitySelectModel(Class entityClass, PropertyPath<String> dslPath, Session session) {
        this.entityClass = entityClass;
        this.dslPath = dslPath;
        this.session = session;
    }

    @Override
    public ListResult findValues(String filter) {
        return ScientificWorksManager.instance().modifyDao().getEntitiesListResult(entityClass, dslPath, filter);
    }
}
