package ru.tandemservice.aspirantrmc.component.catalog.staffDissertationSoviet.StaffDissertationSovietAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.aspirantrmc.entity.catalog.StaffDissertationSoviet;

public class Model extends DefaultCatalogAddEditModel<StaffDissertationSoviet> {

    private ISelectModel lastNameModel;
    private ISelectModel firstNameModel;
    private ISelectModel middleNameModel;

    public ISelectModel getLastNameModel() {
        return lastNameModel;
    }

    public void setLastNameModel(ISelectModel lastNameModel) {
        this.lastNameModel = lastNameModel;
    }

    public ISelectModel getFirstNameModel() {
        return firstNameModel;
    }

    public void setFirstNameModel(ISelectModel firstNameModel) {
        this.firstNameModel = firstNameModel;
    }

    public ISelectModel getMiddleNameModel() {
        return middleNameModel;
    }

    public void setMiddleNameModel(ISelectModel middleNameModel) {
        this.middleNameModel = middleNameModel;
    }


}
