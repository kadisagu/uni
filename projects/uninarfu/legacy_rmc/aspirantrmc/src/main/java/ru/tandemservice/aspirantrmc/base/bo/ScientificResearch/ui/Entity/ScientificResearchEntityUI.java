package ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.Entity;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.*;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.ctr.base.bo.ExternalOrgUnit.ui.WizardStep1Add.ExternalOrgUnitWizardStep1Add;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ScientificResearchManager;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.logic.IScientificResearchDao;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.PracticeAddEdit.ScientificResearchPracticeAddEdit;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.ThemeEdit.ScientificResearchThemeEdit;
import ru.tandemservice.aspirantrmc.base.ext.ExternalOrgUnit.ui.WizardStep3Add.ExternalOrgUnitWizardStep3AddUIExt;
import ru.tandemservice.aspirantrmc.entity.LeadOrganization2Research;
import ru.tandemservice.aspirantrmc.entity.PracticeWork2Research;
import ru.tandemservice.aspirantrmc.entity.ScientificResearch;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;

@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "studentHolder.id", required = true)
})
@Input({
        @Bind(key = ExternalOrgUnitWizardStep3AddUIExt.EXTERNAL_ORG_UNIT, binding = "addedExternalOrgUnit"),
})
@Output({
        @Bind(key = ExternalOrgUnitWizardStep3AddUIExt.DEACTIVATE_MODE, binding = "deactivationMode")
})
public class ScientificResearchEntityUI extends UIPresenter {
    private EntityHolder<Student> studentHolder = new EntityHolder<Student>();
    private ScientificResearch research;
    private ExternalOrgUnit addedExternalOrgUnit;
    private ExtOrgUnitAddType extOrgUnitAddType;
    private ExternalOrgUnitWizardStep3AddUIExt.DeactivateMode deactivationMode = ExternalOrgUnitWizardStep3AddUIExt.DeactivateMode.DEACTIVATE;

    public static final String RESEARCH_VALUE = "researchValue";

    public static enum ExtOrgUnitAddType {
        PRACTICE,
        LEAD_ORG
    }

    ;

    @Override
    public void onComponentRefresh() {
        getStudentHolder().refresh(Student.class);
        IScientificResearchDao dao = ScientificResearchManager.instance().modifyDao();

        research = dao.createResearch(getStudent());

        if (addedExternalOrgUnit != null && extOrgUnitAddType != null) {
            if (extOrgUnitAddType.equals(ExtOrgUnitAddType.PRACTICE)) {
                PracticeWork2Research p2r = new PracticeWork2Research();
                p2r.setResearch(research);
                p2r.setExternalOrgUnit(addedExternalOrgUnit);
                p2r.setCurrent(true);
                UniDaoFacade.getCoreDao().save(p2r);
            }
            else {
                LeadOrganization2Research l2r = new LeadOrganization2Research();
                l2r.setResearch(research);
                l2r.setExternalOrgUnit(addedExternalOrgUnit);
                l2r.setCurrent(true);
                UniDaoFacade.getCoreDao().save(l2r);
            }
            addedExternalOrgUnit = null;
            extOrgUnitAddType = null;
            onComponentRefresh();
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource ds) {
        if (ds.getName().equals(ScientificResearchEntity.PRACTICE_DS) || ds.getName().equals(ScientificResearchEntity.LEAD_ORG_DS)) {
            ds.put(RESEARCH_VALUE, research.getId());
        }
    }

    public void onClickAddPractice() {
        setExtOrgUnitAddType(ExtOrgUnitAddType.PRACTICE);
        getActivationBuilder().asRegion(ScientificResearchPracticeAddEdit.class).activate();
    }

    public void onClickEditPractice() {
        setExtOrgUnitAddType(ExtOrgUnitAddType.PRACTICE);
        getActivationBuilder().asRegion(ScientificResearchPracticeAddEdit.class).parameter("practiceId", getListenerParameterAsLong()).activate();
    }

    public void onClickAddLeadOrg() {
        setExtOrgUnitAddType(ExtOrgUnitAddType.LEAD_ORG);
        getActivationBuilder().asRegion(ExternalOrgUnitWizardStep1Add.class).activate();
    }

    public void onClickDeletePractice() {
        UniDaoFacade.getCoreDao().delete(getListenerParameterAsLong());
    }

    public void onClickDeleteLeadOrg() {
        UniDaoFacade.getCoreDao().delete(getListenerParameterAsLong());
    }

    public void onClickSetPracticeNoCurrent() {
        PracticeWork2Research p2r = UniDaoFacade.getCoreDao().get(getListenerParameterAsLong());
        p2r.setCurrent(false);
        UniDaoFacade.getCoreDao().update(p2r);
    }

    public void onClickSetPracticeCurrent() {
        PracticeWork2Research p2r = UniDaoFacade.getCoreDao().get(getListenerParameterAsLong());
        p2r.setCurrent(true);
        UniDaoFacade.getCoreDao().update(p2r);
    }

    public void onClickSetLeadOrgNoCurrent() {
        LeadOrganization2Research l2r = UniDaoFacade.getCoreDao().get(getListenerParameterAsLong());
        l2r.setCurrent(false);
        UniDaoFacade.getCoreDao().update(l2r);
    }

    public void onClickSetLeadOrgCurrent() {
        LeadOrganization2Research l2r = UniDaoFacade.getCoreDao().get(getListenerParameterAsLong());
        l2r.setCurrent(true);
        UniDaoFacade.getCoreDao().update(l2r);
    }


    public void onClickChangeTheme() {
        getActivationBuilder().asRegion(ScientificResearchThemeEdit.class).parameter(PublisherActivator.PUBLISHER_ID_KEY, getResearch().getId()).activate();
    }

    public Student getStudent() {
        return studentHolder.getValue();
    }

    public EntityHolder<Student> getStudentHolder() {
        return studentHolder;
    }

    public ScientificResearch getResearch() {
        return research;
    }

    public void setResearch(ScientificResearch research) {
        this.research = research;
    }

    public ExternalOrgUnit getAddedExternalOrgUnit() {
        return addedExternalOrgUnit;
    }

    public void setAddedExternalOrgUnit(ExternalOrgUnit addedExternalOrgUnit) {
        this.addedExternalOrgUnit = addedExternalOrgUnit;
    }

    public ExtOrgUnitAddType getExtOrgUnitAddType() {
        return extOrgUnitAddType;
    }

    public void setExtOrgUnitAddType(ExtOrgUnitAddType extOrgUnitAddType) {
        this.extOrgUnitAddType = extOrgUnitAddType;
    }

    public ExternalOrgUnitWizardStep3AddUIExt.DeactivateMode getDeactivationMode() {
        return deactivationMode;
    }

    public void setDeactivationMode(ExternalOrgUnitWizardStep3AddUIExt.DeactivateMode deactivationMode) {
        this.deactivationMode = deactivationMode;
    }
}
