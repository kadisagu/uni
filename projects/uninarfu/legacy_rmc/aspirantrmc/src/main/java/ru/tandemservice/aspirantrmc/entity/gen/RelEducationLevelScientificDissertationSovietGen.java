package ru.tandemservice.aspirantrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.aspirantrmc.entity.RelEducationLevelScientificDissertationSoviet;
import ru.tandemservice.aspirantrmc.entity.catalog.DissertationSoviet;
import ru.tandemservice.uni.entity.catalog.EducationLevelHigher;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность-связь Научные направления подготовки-Диссертационный совет
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RelEducationLevelScientificDissertationSovietGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.aspirantrmc.entity.RelEducationLevelScientificDissertationSoviet";
    public static final String ENTITY_NAME = "relEducationLevelScientificDissertationSoviet";
    public static final int VERSION_HASH = -769205905;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDUCATION_LEVEL_SCIENTIFIC = "educationLevelScientific";
    public static final String L_DISSERTATION_SOVIET = "dissertationSoviet";

    private EducationLevelHigher _educationLevelScientific;     // Направление подготовки кадров высшей квалификации
    private DissertationSoviet _dissertationSoviet;     // Диссертационные советы

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Направление подготовки кадров высшей квалификации. Свойство не может быть null.
     */
    @NotNull
    public EducationLevelHigher getEducationLevelScientific()
    {
        return _educationLevelScientific;
    }

    /**
     * @param educationLevelScientific Направление подготовки кадров высшей квалификации. Свойство не может быть null.
     */
    public void setEducationLevelScientific(EducationLevelHigher educationLevelScientific)
    {
        dirty(_educationLevelScientific, educationLevelScientific);
        _educationLevelScientific = educationLevelScientific;
    }

    /**
     * @return Диссертационные советы. Свойство не может быть null.
     */
    @NotNull
    public DissertationSoviet getDissertationSoviet()
    {
        return _dissertationSoviet;
    }

    /**
     * @param dissertationSoviet Диссертационные советы. Свойство не может быть null.
     */
    public void setDissertationSoviet(DissertationSoviet dissertationSoviet)
    {
        dirty(_dissertationSoviet, dissertationSoviet);
        _dissertationSoviet = dissertationSoviet;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RelEducationLevelScientificDissertationSovietGen)
        {
            setEducationLevelScientific(((RelEducationLevelScientificDissertationSoviet)another).getEducationLevelScientific());
            setDissertationSoviet(((RelEducationLevelScientificDissertationSoviet)another).getDissertationSoviet());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RelEducationLevelScientificDissertationSovietGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RelEducationLevelScientificDissertationSoviet.class;
        }

        public T newInstance()
        {
            return (T) new RelEducationLevelScientificDissertationSoviet();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "educationLevelScientific":
                    return obj.getEducationLevelScientific();
                case "dissertationSoviet":
                    return obj.getDissertationSoviet();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "educationLevelScientific":
                    obj.setEducationLevelScientific((EducationLevelHigher) value);
                    return;
                case "dissertationSoviet":
                    obj.setDissertationSoviet((DissertationSoviet) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "educationLevelScientific":
                        return true;
                case "dissertationSoviet":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "educationLevelScientific":
                    return true;
                case "dissertationSoviet":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "educationLevelScientific":
                    return EducationLevelHigher.class;
                case "dissertationSoviet":
                    return DissertationSoviet.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RelEducationLevelScientificDissertationSoviet> _dslPath = new Path<RelEducationLevelScientificDissertationSoviet>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RelEducationLevelScientificDissertationSoviet");
    }
            

    /**
     * @return Направление подготовки кадров высшей квалификации. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.RelEducationLevelScientificDissertationSoviet#getEducationLevelScientific()
     */
    public static EducationLevelHigher.Path<EducationLevelHigher> educationLevelScientific()
    {
        return _dslPath.educationLevelScientific();
    }

    /**
     * @return Диссертационные советы. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.RelEducationLevelScientificDissertationSoviet#getDissertationSoviet()
     */
    public static DissertationSoviet.Path<DissertationSoviet> dissertationSoviet()
    {
        return _dslPath.dissertationSoviet();
    }

    public static class Path<E extends RelEducationLevelScientificDissertationSoviet> extends EntityPath<E>
    {
        private EducationLevelHigher.Path<EducationLevelHigher> _educationLevelScientific;
        private DissertationSoviet.Path<DissertationSoviet> _dissertationSoviet;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Направление подготовки кадров высшей квалификации. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.RelEducationLevelScientificDissertationSoviet#getEducationLevelScientific()
     */
        public EducationLevelHigher.Path<EducationLevelHigher> educationLevelScientific()
        {
            if(_educationLevelScientific == null )
                _educationLevelScientific = new EducationLevelHigher.Path<EducationLevelHigher>(L_EDUCATION_LEVEL_SCIENTIFIC, this);
            return _educationLevelScientific;
        }

    /**
     * @return Диссертационные советы. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.RelEducationLevelScientificDissertationSoviet#getDissertationSoviet()
     */
        public DissertationSoviet.Path<DissertationSoviet> dissertationSoviet()
        {
            if(_dissertationSoviet == null )
                _dissertationSoviet = new DissertationSoviet.Path<DissertationSoviet>(L_DISSERTATION_SOVIET, this);
            return _dissertationSoviet;
        }

        public Class getEntityClass()
        {
            return RelEducationLevelScientificDissertationSoviet.class;
        }

        public String getEntityName()
        {
            return "relEducationLevelScientificDissertationSoviet";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
