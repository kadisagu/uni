package ru.tandemservice.aspirantrmc.component.student.DefenseDissertationWork.ThemeDissertationWorkAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.uni.entity.employee.Student;

@Input({
        @Bind(key = "studentId", binding = "studentId"),
        @Bind(key = "dissertationWorkId", binding = "dissertationWorkId"),
})
public class Model {

    private Long dissertationWorkId;
    private Long studentId;
    private String theme;
    private Student student;


    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public Long getDissertationWorkId() {
        return dissertationWorkId;
    }

    public void setDissertationWorkId(Long dissertationWorkId) {
        this.dissertationWorkId = dissertationWorkId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }


}
