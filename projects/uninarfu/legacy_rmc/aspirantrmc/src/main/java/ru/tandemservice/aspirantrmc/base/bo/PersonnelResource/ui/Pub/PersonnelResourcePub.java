package ru.tandemservice.aspirantrmc.base.bo.PersonnelResource.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.block.BlockListExtPoint;
import org.tandemframework.caf.ui.config.button.ButtonListExtPoint;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.shared.employeebase.base.bo.Employee.logic.EmployeePostDSHandler;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;

@Configuration
public class PersonnelResourcePub extends BusinessComponentManager {

    public static final String EMPLOYEE_TAB_PANEL = "employeeTabPanel";
    public static final String EMPLOYEE_DATA_TAB = "employeeDataTab";
    public static final String EMPLOYE_PUB_BLOCK_LIST = "employeePubBlockList";
    public static final String EMPLOYEE_ATTRS_BLOCK = "employeeAttrsBlock";
    public static final String EMPLOYEE_POST_BLOCK = "employeePostBlock";
    public static final String EMPLOYEE_PUB_BUTTON_LIST = "employeePubButtonList";
    public static final String EMPLOYEE_ADD = "addEmployeeBtn";
    public static final String EMPLOYEE_DATA_EDIT = "editEmployeeAttrsBtn";
    public static final String EMPLOYEE_POST_ADD = "addEmployeePostBtn";
    public static final String EMPLOYEE_POST_DS = "employeePostDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(EMPLOYEE_POST_DS, employeePostDS(), employeePostDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint employeePostDS()
    {
        return columnListExtPointBuilder(EMPLOYEE_POST_DS)
                .addColumn(publisherColumn("title", EmployeePost.postRelation().postBoundedWithQGandQL().title()).order())
                .addColumn(textColumn("employeeType", EmployeePost.postRelation().postBoundedWithQGandQL().post().employeeType().shortTitle()).order())
                .addColumn(textColumn("profQualificationGroup", EmployeePost.postRelation().postBoundedWithQGandQL().profQualificationGroup().shortTitle()).order())
                .addColumn(textColumn("qualificationLevel", EmployeePost.postRelation().postBoundedWithQGandQL().qualificationLevel().shortTitle()).order())
                .addColumn(textColumn("etksLevel", EmployeePost.postRelation().postBoundedWithQGandQL().qualificationLevel().shortTitle()).order())
                .addColumn(textColumn("postType", EmployeePost.postType().title()).order())
                .addColumn(textColumn("postStatus", EmployeePost.postStatus().title()).order())
                .addColumn(textColumn("orgUnit", EmployeePost.orgUnit().fullTitle()).order())
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("editPersonnelResource").disabled(EmployeePost.orgUnit().archival().s()).visible("ui:accessible"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER,
                                        // TODO: #9806
                                        FormattedMessage.with().template("employeePostDS.delete.alert")
                                                .parameter(EmployeePost.postRelation().postBoundedWithQGandQL().post().title())
                                                .parameter(EmployeePost.orgUnit().orgUnitType().title())
                                                .parameter(EmployeePost.orgUnit().title())
                                                .create()
                ).permissionKey("editPersonnelResource").disabled(EmployeePost.orgUnit().archival().s()).visible("ui:accessible").create())
                .create();
    }

    @Bean
    public TabPanelExtPoint employeeTabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(EMPLOYEE_TAB_PANEL)
                .addTab(htmlTab(EMPLOYEE_DATA_TAB, "EmployeeDataTab").permissionKey("viewPersonnelResource"))
                .create();
    }

    @Bean
    public BlockListExtPoint employeePubBlockListExtPoint()
    {
        return blockListExtPointBuilder(EMPLOYE_PUB_BLOCK_LIST)
                .addBlock(htmlBlock(EMPLOYEE_ATTRS_BLOCK, "EmployeeAttrsBlock").permissionKey("viewPersonnelResource"))
                .addBlock(htmlBlock(EMPLOYEE_POST_BLOCK, "EmployeePostBlock").permissionKey("viewPersonnelResource"))
                .create();
    }

    @Bean
    public ButtonListExtPoint employeePubButtonListExtPoint()
    {
        return buttonListExtPointBuilder(EMPLOYEE_PUB_BUTTON_LIST)
                .addButton(submitButton(EMPLOYEE_ADD, "onClickAddEmployee").permissionKey("editPersonnelResource").visible("ui:active"))
                .addButton(submitButton(EMPLOYEE_DATA_EDIT, "onClickEditEmployeeData").permissionKey("editPersonnelResource").visible("ui:accessible").disabled("ui:active"))
                .addButton(submitButton(EMPLOYEE_POST_ADD, "onClickAddEmployeePost").permissionKey("editPersonnelResource").visible("ui:accessible").disabled("ui:active"))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> employeePostDSHandler()
    {
        return new EmployeePostDSHandler(getName());
    }
}
