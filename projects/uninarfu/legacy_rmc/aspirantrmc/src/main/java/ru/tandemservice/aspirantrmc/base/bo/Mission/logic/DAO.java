package ru.tandemservice.aspirantrmc.base.bo.Mission.logic;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.aspirantrmc.entity.catalog.StatusMission;
import ru.tandemservice.uni.dao.UniBaseDao;

import java.util.List;

public class DAO extends UniBaseDao implements IDAO {
    @Override
    public StatusMission getDefaultStatusMission() {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(StatusMission.class, "e");
        dql.where(DQLExpressions.eq(
                DQLExpressions.property(StatusMission.code().fromAlias("e")),
                DQLExpressions.value("1")
        ));
        List<StatusMission> result = dql.createStatement(getSession()).setMaxResults(1).list();
        if (result.size() > 0) {
            return result.get(0);
        }
        return null;
    }
}
