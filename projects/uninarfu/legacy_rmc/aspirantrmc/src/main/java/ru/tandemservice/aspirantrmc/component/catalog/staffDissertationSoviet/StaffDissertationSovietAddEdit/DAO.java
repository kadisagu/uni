package ru.tandemservice.aspirantrmc.component.catalog.staffDissertationSoviet.StaffDissertationSovietAddEdit;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.tapsupport.component.selection.ListResult;
import org.tandemframework.tapsupport.component.selection.SingleSelectTextModel;
import ru.tandemservice.aspirantrmc.entity.catalog.StaffDissertationSoviet;

import java.util.List;

public class DAO extends DefaultCatalogAddEditDAO<StaffDissertationSoviet, Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        super.prepare(model);

        model.setLastNameModel(new SingleSelectTextModel() {
            public ListResult<String> findValues(String filter) {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StaffDissertationSoviet.class, "s")
                        .addColumn(DQLExpressions.property(StaffDissertationSoviet.lastName().fromAlias("s")))
                        .setPredicate(DQLPredicateType.distinct)
                        .where(DQLExpressions.likeUpper(DQLExpressions.property(StaffDissertationSoviet.lastName().fromAlias("s")), DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));
                List list = builder.createStatement(getSession()).setMaxResults(50).list();
                Number number = (Number) builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();
                return new ListResult(list, number != null ? number.intValue() : 0L);
            }
        });

        model.setFirstNameModel(new SingleSelectTextModel() {
            public ListResult<String> findValues(String filter) {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StaffDissertationSoviet.class, "s")
                        .addColumn(DQLExpressions.property(StaffDissertationSoviet.firstName().fromAlias("s")))
                        .setPredicate(DQLPredicateType.distinct)
                        .where(DQLExpressions.likeUpper(DQLExpressions.property(StaffDissertationSoviet.firstName().fromAlias("s")), DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));
                List list = builder.createStatement(getSession()).setMaxResults(50).list();
                Number number = (Number) builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();
                return new ListResult(list, number != null ? number.intValue() : 0L);
            }
        });
        model.setMiddleNameModel(new SingleSelectTextModel() {
            public ListResult<String> findValues(String filter) {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StaffDissertationSoviet.class, "s")
                        .addColumn(DQLExpressions.property(StaffDissertationSoviet.middleName().fromAlias("s")))
                        .setPredicate(DQLPredicateType.distinct)
                        .where(DQLExpressions.likeUpper(DQLExpressions.property(StaffDissertationSoviet.middleName().fromAlias("s")), DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));
                List list = builder.createStatement(getSession()).setMaxResults(50).list();
                Number number = (Number) builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();
                return new ListResult(list, number != null ? number.intValue() : 0L);
            }
        });

    }

}
