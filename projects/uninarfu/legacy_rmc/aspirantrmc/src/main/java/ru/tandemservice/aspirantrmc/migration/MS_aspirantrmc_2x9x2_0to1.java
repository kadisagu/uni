/* $Id$ */
package ru.tandemservice.aspirantrmc.migration;

import org.tandemframework.core.debug.Debug;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import ru.tandemservice.uni.migration.MS_uni_2x9x2_0to1;

import java.util.List;

import static org.tandemframework.shared.commonbase.utils.MigrationUtils.BatchInsertBuilder;
import static org.tandemframework.shared.commonbase.utils.MigrationUtils.processor;

/**
 * @author Nikolay Fedorovskih
 * @since 11.12.2015
 */
public class MS_aspirantrmc_2x9x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBeforeDependencies()
    {
        return new ScriptDependency[] {
                new ScriptDependency("org.tandemframework", "1.6.18"),
                new ScriptDependency("org.tandemframework.shared", "1.9.2"),
                new ScriptDependency("ru.tandemservice.uni.product", "2.9.1")
        };
    }

    @Override
    public ScriptDependency[] getAfterDependencies() {
        // Надо выполнить до MS_uni_2x9x2_0to1
        return new ScriptDependency[] {
                MigrationUtils.createScriptDependency(MS_uni_2x9x2_0to1.class)
        };
    }

    @Override
    public void run(DBTool tool) throws Exception {

        if (tool.columnExists("educationlevelshighschool_t", "programorientation_id")) {
            // Уже не актуально
            return;
        }

        // Переименовываем перечень
        final long indexId = tool.getNumericResult("select id from edu_c_pr_subject_index_t where code_p=?", "2013.96");
        if (indexId == 0) {
            throw new IllegalStateException("Subject index with code '2013.96' not found");
        }
        final Long gen2009 = MigrationUtils.getIdByCode(tool, "edu_c_pr_subject_index_gen_t", "2009");
        tool.executeUpdate("update edu_c_pr_subject_index_t set title_p=?, shorttitle_p=?, edulevelandindexnotation_p=?, eduprogramsubjecttype_p=?, generation_id=? where id=?",
                           "Перечень направлений аспирантуры 2009", "ВО аспирантура (2009)", "ВО ФГОС 2009", "направление подготовки в аспирантуре", gen2009, indexId);

        // Меняем код квалификации, если она была. Или создаем.
        final String old_q_code = "2013.96.aspirantsRmc";
        final String new_q_code = "2013.96.PhD";
        long phd_q_id = tool.getNumericResult("select id from edu_c_pr_qual_t where code_p=?", new_q_code);

        if (phd_q_id == 0) {
            phd_q_id = tool.getNumericResult("select id from edu_c_pr_qual_t where code_p=?", old_q_code);
            if (phd_q_id == 0) {
                final short entityCode = tool.entityCodes().get("eduProgramQualification");
                phd_q_id = EntityIDGenerator.generateNewId(entityCode);
                tool.executeUpdate("insert into edu_c_pr_qual_t (id, discriminator, code_p, qualificationlevelcode_p, subjectindex_id, title_p) values(?,?,?,?,?,?)",
                                   phd_q_id, entityCode, new_q_code, null, indexId, "Кандидат наук");
            } else {
                tool.executeUpdate("update edu_c_pr_qual_t set code_p=?, title_p=? where id=?", new_q_code, "Кандидат наук", phd_q_id);
            }
        }


        // Создаем связи квалификации с направлением, если их нет
        final List<Object[]> rows = tool.executeQuery(
                processor(1),
                "select p.id from edu_c_pr_subject_t p where p.subjectindex_id=? and p.id not in(select programsubject_id from edu_c_pr_subject_qual_t)",
                indexId
        );
        final short relCode = tool.entityCodes().get("eduProgramSubjectQualification");
        final BatchInsertBuilder ins = new BatchInsertBuilder(relCode, "programsubject_id", "programqualification_id");
        for (final Object[] row : rows) {
            ins.addRow(row[0], phd_q_id);
        }
        final int n = ins.executeInsert(tool, "edu_c_pr_subject_qual_t");
        if (n > 0 && Debug.isEnabled()) {
            System.out.println(this.getClass().getSimpleName() + ": " + n + " new relations with qualification created");
        }
    }
}