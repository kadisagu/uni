package ru.tandemservice.aspirantrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.aspirantrmc.entity.LeadOrganization2Research;
import ru.tandemservice.aspirantrmc.entity.ScientificResearch;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь ведущей организации и научного исследования
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class LeadOrganization2ResearchGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.aspirantrmc.entity.LeadOrganization2Research";
    public static final String ENTITY_NAME = "leadOrganization2Research";
    public static final int VERSION_HASH = -398247448;
    private static IEntityMeta ENTITY_META;

    public static final String L_RESEARCH = "research";
    public static final String L_EXTERNAL_ORG_UNIT = "externalOrgUnit";
    public static final String P_CURRENT = "current";

    private ScientificResearch _research;     // Научное исследование
    private ExternalOrgUnit _externalOrgUnit;     // Внешняя организация
    private boolean _current = false;     // Текущая

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Научное исследование.
     */
    public ScientificResearch getResearch()
    {
        return _research;
    }

    /**
     * @param research Научное исследование.
     */
    public void setResearch(ScientificResearch research)
    {
        dirty(_research, research);
        _research = research;
    }

    /**
     * @return Внешняя организация.
     */
    public ExternalOrgUnit getExternalOrgUnit()
    {
        return _externalOrgUnit;
    }

    /**
     * @param externalOrgUnit Внешняя организация.
     */
    public void setExternalOrgUnit(ExternalOrgUnit externalOrgUnit)
    {
        dirty(_externalOrgUnit, externalOrgUnit);
        _externalOrgUnit = externalOrgUnit;
    }

    /**
     * @return Текущая. Свойство не может быть null.
     */
    @NotNull
    public boolean isCurrent()
    {
        return _current;
    }

    /**
     * @param current Текущая. Свойство не может быть null.
     */
    public void setCurrent(boolean current)
    {
        dirty(_current, current);
        _current = current;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof LeadOrganization2ResearchGen)
        {
            setResearch(((LeadOrganization2Research)another).getResearch());
            setExternalOrgUnit(((LeadOrganization2Research)another).getExternalOrgUnit());
            setCurrent(((LeadOrganization2Research)another).isCurrent());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends LeadOrganization2ResearchGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) LeadOrganization2Research.class;
        }

        public T newInstance()
        {
            return (T) new LeadOrganization2Research();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "research":
                    return obj.getResearch();
                case "externalOrgUnit":
                    return obj.getExternalOrgUnit();
                case "current":
                    return obj.isCurrent();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "research":
                    obj.setResearch((ScientificResearch) value);
                    return;
                case "externalOrgUnit":
                    obj.setExternalOrgUnit((ExternalOrgUnit) value);
                    return;
                case "current":
                    obj.setCurrent((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "research":
                        return true;
                case "externalOrgUnit":
                        return true;
                case "current":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "research":
                    return true;
                case "externalOrgUnit":
                    return true;
                case "current":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "research":
                    return ScientificResearch.class;
                case "externalOrgUnit":
                    return ExternalOrgUnit.class;
                case "current":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<LeadOrganization2Research> _dslPath = new Path<LeadOrganization2Research>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "LeadOrganization2Research");
    }
            

    /**
     * @return Научное исследование.
     * @see ru.tandemservice.aspirantrmc.entity.LeadOrganization2Research#getResearch()
     */
    public static ScientificResearch.Path<ScientificResearch> research()
    {
        return _dslPath.research();
    }

    /**
     * @return Внешняя организация.
     * @see ru.tandemservice.aspirantrmc.entity.LeadOrganization2Research#getExternalOrgUnit()
     */
    public static ExternalOrgUnit.Path<ExternalOrgUnit> externalOrgUnit()
    {
        return _dslPath.externalOrgUnit();
    }

    /**
     * @return Текущая. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.LeadOrganization2Research#isCurrent()
     */
    public static PropertyPath<Boolean> current()
    {
        return _dslPath.current();
    }

    public static class Path<E extends LeadOrganization2Research> extends EntityPath<E>
    {
        private ScientificResearch.Path<ScientificResearch> _research;
        private ExternalOrgUnit.Path<ExternalOrgUnit> _externalOrgUnit;
        private PropertyPath<Boolean> _current;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Научное исследование.
     * @see ru.tandemservice.aspirantrmc.entity.LeadOrganization2Research#getResearch()
     */
        public ScientificResearch.Path<ScientificResearch> research()
        {
            if(_research == null )
                _research = new ScientificResearch.Path<ScientificResearch>(L_RESEARCH, this);
            return _research;
        }

    /**
     * @return Внешняя организация.
     * @see ru.tandemservice.aspirantrmc.entity.LeadOrganization2Research#getExternalOrgUnit()
     */
        public ExternalOrgUnit.Path<ExternalOrgUnit> externalOrgUnit()
        {
            if(_externalOrgUnit == null )
                _externalOrgUnit = new ExternalOrgUnit.Path<ExternalOrgUnit>(L_EXTERNAL_ORG_UNIT, this);
            return _externalOrgUnit;
        }

    /**
     * @return Текущая. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.LeadOrganization2Research#isCurrent()
     */
        public PropertyPath<Boolean> current()
        {
            if(_current == null )
                _current = new PropertyPath<Boolean>(LeadOrganization2ResearchGen.P_CURRENT, this);
            return _current;
        }

        public Class getEntityClass()
        {
            return LeadOrganization2Research.class;
        }

        public String getEntityName()
        {
            return "leadOrganization2Research";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
