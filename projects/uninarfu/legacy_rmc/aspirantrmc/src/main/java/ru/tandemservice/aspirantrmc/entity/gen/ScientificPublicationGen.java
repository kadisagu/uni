package ru.tandemservice.aspirantrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.catalog.entity.ForeignLanguage;
import ru.tandemservice.aspirantrmc.entity.PublishingHouse;
import ru.tandemservice.aspirantrmc.entity.ScientificPublication;
import ru.tandemservice.aspirantrmc.entity.catalog.TypePublication;
import ru.tandemservice.uni.entity.employee.Student;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Научная публикация аспиранта/соискателя
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ScientificPublicationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.aspirantrmc.entity.ScientificPublication";
    public static final String ENTITY_NAME = "scientificPublication";
    public static final int VERSION_HASH = 1713273397;
    private static IEntityMeta ENTITY_META;

    public static final String L_AUTHOR = "author";
    public static final String L_PUBLICATION_TYPE = "publicationType";
    public static final String L_PUBLISHING_HOUSE = "publishingHouse";
    public static final String L_LANGUAGE = "language";
    public static final String P_TITLE = "title";
    public static final String P_COLLABORATORS = "collaborators";
    public static final String P_IMPRINT_DATE = "imprintDate";
    public static final String P_NUMBERS_OF_PAGES = "numbersOfPages";

    private Student _author;     // Автор
    private TypePublication _publicationType;     // Вид публикации
    private PublishingHouse _publishingHouse;     // Издательство, журнал
    private ForeignLanguage _language;     // Язык публикации
    private String _title;     // Наименование публикации
    private String _collaborators;     // Фамилия И.О.соавторов и их ученые степени и звания
    private Integer _imprintDate;     // Год издания
    private String _numbersOfPages;     // Количество печатных листов или страниц

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Автор.
     */
    public Student getAuthor()
    {
        return _author;
    }

    /**
     * @param author Автор.
     */
    public void setAuthor(Student author)
    {
        dirty(_author, author);
        _author = author;
    }

    /**
     * @return Вид публикации.
     */
    public TypePublication getPublicationType()
    {
        return _publicationType;
    }

    /**
     * @param publicationType Вид публикации.
     */
    public void setPublicationType(TypePublication publicationType)
    {
        dirty(_publicationType, publicationType);
        _publicationType = publicationType;
    }

    /**
     * @return Издательство, журнал.
     */
    public PublishingHouse getPublishingHouse()
    {
        return _publishingHouse;
    }

    /**
     * @param publishingHouse Издательство, журнал.
     */
    public void setPublishingHouse(PublishingHouse publishingHouse)
    {
        dirty(_publishingHouse, publishingHouse);
        _publishingHouse = publishingHouse;
    }

    /**
     * @return Язык публикации.
     */
    public ForeignLanguage getLanguage()
    {
        return _language;
    }

    /**
     * @param language Язык публикации.
     */
    public void setLanguage(ForeignLanguage language)
    {
        dirty(_language, language);
        _language = language;
    }

    /**
     * @return Наименование публикации. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Наименование публикации. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Фамилия И.О.соавторов и их ученые степени и звания.
     */
    @Length(max=1255)
    public String getCollaborators()
    {
        return _collaborators;
    }

    /**
     * @param collaborators Фамилия И.О.соавторов и их ученые степени и звания.
     */
    public void setCollaborators(String collaborators)
    {
        dirty(_collaborators, collaborators);
        _collaborators = collaborators;
    }

    /**
     * @return Год издания.
     */
    public Integer getImprintDate()
    {
        return _imprintDate;
    }

    /**
     * @param imprintDate Год издания.
     */
    public void setImprintDate(Integer imprintDate)
    {
        dirty(_imprintDate, imprintDate);
        _imprintDate = imprintDate;
    }

    /**
     * @return Количество печатных листов или страниц.
     */
    @Length(max=255)
    public String getNumbersOfPages()
    {
        return _numbersOfPages;
    }

    /**
     * @param numbersOfPages Количество печатных листов или страниц.
     */
    public void setNumbersOfPages(String numbersOfPages)
    {
        dirty(_numbersOfPages, numbersOfPages);
        _numbersOfPages = numbersOfPages;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ScientificPublicationGen)
        {
            setAuthor(((ScientificPublication)another).getAuthor());
            setPublicationType(((ScientificPublication)another).getPublicationType());
            setPublishingHouse(((ScientificPublication)another).getPublishingHouse());
            setLanguage(((ScientificPublication)another).getLanguage());
            setTitle(((ScientificPublication)another).getTitle());
            setCollaborators(((ScientificPublication)another).getCollaborators());
            setImprintDate(((ScientificPublication)another).getImprintDate());
            setNumbersOfPages(((ScientificPublication)another).getNumbersOfPages());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ScientificPublicationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ScientificPublication.class;
        }

        public T newInstance()
        {
            return (T) new ScientificPublication();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "author":
                    return obj.getAuthor();
                case "publicationType":
                    return obj.getPublicationType();
                case "publishingHouse":
                    return obj.getPublishingHouse();
                case "language":
                    return obj.getLanguage();
                case "title":
                    return obj.getTitle();
                case "collaborators":
                    return obj.getCollaborators();
                case "imprintDate":
                    return obj.getImprintDate();
                case "numbersOfPages":
                    return obj.getNumbersOfPages();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "author":
                    obj.setAuthor((Student) value);
                    return;
                case "publicationType":
                    obj.setPublicationType((TypePublication) value);
                    return;
                case "publishingHouse":
                    obj.setPublishingHouse((PublishingHouse) value);
                    return;
                case "language":
                    obj.setLanguage((ForeignLanguage) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "collaborators":
                    obj.setCollaborators((String) value);
                    return;
                case "imprintDate":
                    obj.setImprintDate((Integer) value);
                    return;
                case "numbersOfPages":
                    obj.setNumbersOfPages((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "author":
                        return true;
                case "publicationType":
                        return true;
                case "publishingHouse":
                        return true;
                case "language":
                        return true;
                case "title":
                        return true;
                case "collaborators":
                        return true;
                case "imprintDate":
                        return true;
                case "numbersOfPages":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "author":
                    return true;
                case "publicationType":
                    return true;
                case "publishingHouse":
                    return true;
                case "language":
                    return true;
                case "title":
                    return true;
                case "collaborators":
                    return true;
                case "imprintDate":
                    return true;
                case "numbersOfPages":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "author":
                    return Student.class;
                case "publicationType":
                    return TypePublication.class;
                case "publishingHouse":
                    return PublishingHouse.class;
                case "language":
                    return ForeignLanguage.class;
                case "title":
                    return String.class;
                case "collaborators":
                    return String.class;
                case "imprintDate":
                    return Integer.class;
                case "numbersOfPages":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ScientificPublication> _dslPath = new Path<ScientificPublication>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ScientificPublication");
    }
            

    /**
     * @return Автор.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificPublication#getAuthor()
     */
    public static Student.Path<Student> author()
    {
        return _dslPath.author();
    }

    /**
     * @return Вид публикации.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificPublication#getPublicationType()
     */
    public static TypePublication.Path<TypePublication> publicationType()
    {
        return _dslPath.publicationType();
    }

    /**
     * @return Издательство, журнал.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificPublication#getPublishingHouse()
     */
    public static PublishingHouse.Path<PublishingHouse> publishingHouse()
    {
        return _dslPath.publishingHouse();
    }

    /**
     * @return Язык публикации.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificPublication#getLanguage()
     */
    public static ForeignLanguage.Path<ForeignLanguage> language()
    {
        return _dslPath.language();
    }

    /**
     * @return Наименование публикации. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificPublication#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Фамилия И.О.соавторов и их ученые степени и звания.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificPublication#getCollaborators()
     */
    public static PropertyPath<String> collaborators()
    {
        return _dslPath.collaborators();
    }

    /**
     * @return Год издания.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificPublication#getImprintDate()
     */
    public static PropertyPath<Integer> imprintDate()
    {
        return _dslPath.imprintDate();
    }

    /**
     * @return Количество печатных листов или страниц.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificPublication#getNumbersOfPages()
     */
    public static PropertyPath<String> numbersOfPages()
    {
        return _dslPath.numbersOfPages();
    }

    public static class Path<E extends ScientificPublication> extends EntityPath<E>
    {
        private Student.Path<Student> _author;
        private TypePublication.Path<TypePublication> _publicationType;
        private PublishingHouse.Path<PublishingHouse> _publishingHouse;
        private ForeignLanguage.Path<ForeignLanguage> _language;
        private PropertyPath<String> _title;
        private PropertyPath<String> _collaborators;
        private PropertyPath<Integer> _imprintDate;
        private PropertyPath<String> _numbersOfPages;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Автор.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificPublication#getAuthor()
     */
        public Student.Path<Student> author()
        {
            if(_author == null )
                _author = new Student.Path<Student>(L_AUTHOR, this);
            return _author;
        }

    /**
     * @return Вид публикации.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificPublication#getPublicationType()
     */
        public TypePublication.Path<TypePublication> publicationType()
        {
            if(_publicationType == null )
                _publicationType = new TypePublication.Path<TypePublication>(L_PUBLICATION_TYPE, this);
            return _publicationType;
        }

    /**
     * @return Издательство, журнал.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificPublication#getPublishingHouse()
     */
        public PublishingHouse.Path<PublishingHouse> publishingHouse()
        {
            if(_publishingHouse == null )
                _publishingHouse = new PublishingHouse.Path<PublishingHouse>(L_PUBLISHING_HOUSE, this);
            return _publishingHouse;
        }

    /**
     * @return Язык публикации.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificPublication#getLanguage()
     */
        public ForeignLanguage.Path<ForeignLanguage> language()
        {
            if(_language == null )
                _language = new ForeignLanguage.Path<ForeignLanguage>(L_LANGUAGE, this);
            return _language;
        }

    /**
     * @return Наименование публикации. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificPublication#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(ScientificPublicationGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Фамилия И.О.соавторов и их ученые степени и звания.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificPublication#getCollaborators()
     */
        public PropertyPath<String> collaborators()
        {
            if(_collaborators == null )
                _collaborators = new PropertyPath<String>(ScientificPublicationGen.P_COLLABORATORS, this);
            return _collaborators;
        }

    /**
     * @return Год издания.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificPublication#getImprintDate()
     */
        public PropertyPath<Integer> imprintDate()
        {
            if(_imprintDate == null )
                _imprintDate = new PropertyPath<Integer>(ScientificPublicationGen.P_IMPRINT_DATE, this);
            return _imprintDate;
        }

    /**
     * @return Количество печатных листов или страниц.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificPublication#getNumbersOfPages()
     */
        public PropertyPath<String> numbersOfPages()
        {
            if(_numbersOfPages == null )
                _numbersOfPages = new PropertyPath<String>(ScientificPublicationGen.P_NUMBERS_OF_PAGES, this);
            return _numbersOfPages;
        }

        public Class getEntityClass()
        {
            return ScientificPublication.class;
        }

        public String getEntityName()
        {
            return "scientificPublication";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
