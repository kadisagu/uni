package ru.tandemservice.aspirantrmc.base.bo.Action.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import ru.tandemservice.aspirantrmc.base.bo.Action.ActionManager;
import ru.tandemservice.aspirantrmc.entity.AspAction;
import ru.tandemservice.uni.util.FilterUtils;

@Configuration
public class ActionList extends BusinessComponentManager {
    public static final String LIST_DS = "listDS";

    protected DQLOrderDescriptionRegistry _orderRegistry = new DQLOrderDescriptionRegistry(AspAction.class, "e");

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(LIST_DS, columnListHandler(), entityDSHandler()).numberOfRecords(5))
                .addDataSource(selectDS(ActionManager.STATUS_ACTION_DS, ActionManager.instance().statusActionDSHandler()))
                .addDataSource(selectDS(ActionManager.TYPE_ACTION_DS, ActionManager.instance().typeActionDSHandler()))
                .addDataSource(selectDS(ActionManager.TYPE_PARTICIPATION_DS, ActionManager.instance().typeParticipationDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint columnListHandler() {
        return columnListExtPointBuilder(LIST_DS)
                .addColumn(publisherColumn("name", AspAction.name().s())
                                   .order()
                                   .publisherLinkResolver(new SimplePublisherLinkResolver(AspAction.id()))
                                   .create()
                )
                .addColumn(textColumn("status", AspAction.statusAction().title().s()).order().create())
                .addColumn(textColumn("type", AspAction.typeAction().title().s()).order().create())
                .addColumn(dateColumn("startDate", AspAction.startDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order().create())
                .addColumn(dateColumn("endDate", AspAction.endDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order().create())
                .addColumn(textColumn("address", AspAction.address().settlement().title().s()).order().create())
                .addColumn(textColumn("typeParticipation", AspAction.typeParticipation().title().s()).order().create())

                .addColumn(actionColumn(("edit"), new Icon("edit"), "onEditEntry")
                                   .permissionKey("aspirantrmc_ActionEdit").create())
                .addColumn(actionColumn("delete", new Icon("delete"), "onDeleteEntry")
                                   .alert(FormattedMessage.with().template(LIST_DS + ".delete.alert").create())
                                   .permissionKey("aspirantrmc_ActionDelete").create())

                .create();
    }


    @Bean
    public IBusinessHandler<DSInput, DSOutput> entityDSHandler() {
        return new DefaultSearchDataSourceHandler(getName()) {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context) {

                //ид студента передали через context:
                Long studentId = context.get(ActionListUI.STUDENT_ID);


                DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(AspAction.class, "e");

                FilterUtils.applySelectFilter(dql, "e", AspAction.statusAction().getPath(), context.get(ActionListUI.STATUS_ACTION_FILTER));
                FilterUtils.applySelectFilter(dql, "e", AspAction.typeParticipation().getPath(), context.get(ActionListUI.TYPE_PARTICIPATION_FILTER));
                FilterUtils.applySelectFilter(dql, "e", AspAction.typeAction().getPath(), context.get(ActionListUI.TYPE_ACTION_FILTER));

                dql.where(DQLExpressions.eq(
                        DQLExpressions.property(AspAction.student().id().fromAlias("e")),
                        DQLExpressions.value(studentId)
                ));

                _orderRegistry.applyOrder(dql, input.getEntityOrder());

                return DQLSelectOutputBuilder.get(input, dql, context.getSession()).pageable(true).build();
            }
        };
    }

}
