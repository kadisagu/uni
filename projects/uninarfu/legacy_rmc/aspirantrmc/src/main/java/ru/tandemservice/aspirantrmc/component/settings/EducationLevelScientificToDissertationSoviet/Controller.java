package ru.tandemservice.aspirantrmc.component.settings.EducationLevelScientificToDissertationSoviet;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.unibase.UniBaseUtils;

public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate {

    public void updateListDataSource(IBusinessComponent context) {
        ((IDAO) getDao()).prepareListDataSource(getModel(context));
    }

    @Override
    public void onRefreshComponent(IBusinessComponent context) {
        Model model = (Model) getModel(context);

        ((IDAO) getDao()).prepare(model);

        prepareDataSource(context);

        model.setSettings(UniBaseUtils.getDataSettings(context, "EducationLevelScientificToDissertationSoviet.filter"));
    }

    protected void refresh(IBusinessComponent context)
    {
        ((Model) getModel(context)).getDataSource().refresh();
    }

    public void onClickSearch(IBusinessComponent context)
    {
        ((Model) getModel(context)).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent context)
    {
        ((Model) getModel(context)).getSettings().clear();
        onClickSearch(context);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        Model model = (Model) getModel(component);
        if (model.getDataSource() != null) {
            return;
        }

        DynamicListDataSource<EducationLevelsHighSchool> dataSource = new DynamicListDataSource<>(component, this);

        dataSource.addColumn(new SimpleColumn("Название", EducationLevelsHighSchool.P_PRINT_TITLE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Присваиваемая квалификация", EducationLevelsHighSchool.assignedQualification().title()).setClickable(false));
        dataSource.addColumn(new IndicatorColumn("Добавить дис.совет", null, "onClickAdd").defaultIndicator(new IndicatorColumn.Item("edit", "Добавить")).setImageHeader(false).setDisableSecondSubmit(false));

        model.setDataSource(dataSource);
    }

    public void onClickAdd(IBusinessComponent component) {
        component.createDefaultChildRegion(new ComponentActivator("ru.tandemservice.aspirantrmc.component.settings.EducationLevelScientificToDissertationSovietPub", ParametersMap.createWith("id", component.getListenerParameter())));
    }

}
