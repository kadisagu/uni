package ru.tandemservice.aspirantrmc.entity.catalog;

import ru.tandemservice.aspirantrmc.entity.catalog.gen.DissertationSovietGen;

/**
 * Диссертационные советы
 */
public class DissertationSoviet extends DissertationSovietGen {
    public String getFullTitle() {
        return new StringBuilder(getSovietCode()).append(" ").append(getTitle()).toString();
    }
}