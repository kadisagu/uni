package ru.tandemservice.aspirantrmc.component.catalog.dissertationSoviet.DissertationSovietPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.aspirantrmc.entity.catalog.DissertationSoviet;

public class Controller extends DefaultCatalogPubController<DissertationSoviet, Model, IDAO> {

    @Override
    protected DynamicListDataSource<DissertationSoviet> createListDataSource(IBusinessComponent context) {
        Model model = getModel(context);
        DynamicListDataSource<DissertationSoviet> dataSource = new DynamicListDataSource<DissertationSoviet>(context, this);

        dataSource.addColumn(new SimpleColumn("Шифр диссертационных советов", DissertationSoviet.P_SOVIET_CODE).setClickable(false), 1);
        dataSource.addColumn(new SimpleColumn("Организация, на базе которой создан диссертационный совет", DissertationSoviet.P_TITLE).setClickable(false), 2);
        dataSource.addColumn(new ToggleColumn("Использовать", DissertationSoviet.P_USE).setListener("onClickToggleUse"), 3);
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
        dataSource.addColumn(new ActionColumn("Удалить", "delete", "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", new Object[]{"title"}).setPermissionKey(model.getCatalogItemDelete()).setDisableHandler(model.getDisabledEntityHandler()));
        dataSource.setOrder(DissertationSoviet.P_SOVIET_CODE, OrderDirection.asc);
        return dataSource;
    }

    public void onClickToggleUse(IBusinessComponent component)
    {
        getDao().updateInUse(getModel(component), (Long) component.getListenerParameter());
    }
}
