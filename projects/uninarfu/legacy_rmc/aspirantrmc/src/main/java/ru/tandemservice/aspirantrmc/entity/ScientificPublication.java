package ru.tandemservice.aspirantrmc.entity;

import ru.tandemservice.aspirantrmc.entity.gen.ScientificPublicationGen;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * Научная публикация аспиранта/соискателя
 */
public class ScientificPublication extends ScientificPublicationGen
{
    public ScientificPublication() {
    }

    public ScientificPublication(Student student) {
        this.setAuthor(student);
    }
}