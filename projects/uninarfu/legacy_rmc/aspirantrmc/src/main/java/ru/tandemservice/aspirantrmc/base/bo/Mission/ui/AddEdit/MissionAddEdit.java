package ru.tandemservice.aspirantrmc.base.bo.Mission.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.aspirantrmc.base.bo.Mission.MissionManager;

@Configuration
public class MissionAddEdit extends BusinessComponentManager {
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(MissionManager.STATUS_MISSION_DS, MissionManager.instance().statusMissionDSHadnler()))
                .addDataSource(selectDS(MissionManager.FINANCING_DS, MissionManager.instance().financeDSHandler()))
                .create();
    }
}
