package ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.logic;

import org.hibernate.Session;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.person.catalog.entity.ForeignLanguage;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDaoFacade;

import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: ramaslov
 * Date: 31.01.13
 * Time: 13:11
 * To change this template use File | Settings | File Templates.
 */
public class EntityLanguageSelectModel extends FullCheckSelectModel {
    private Session session;

    public EntityLanguageSelectModel(Session session) {
        this.session = session;
    }

    @Override
    public ListResult findValues(String filter) {
        MQBuilder builder = new MQBuilder(ForeignLanguage.ENTITY_CLASS, "o");
        builder.add(MQExpression.like("o", ForeignLanguage.P_TITLE, CoreStringUtils.escapeLike(filter)));
        builder.addOrder("o", ForeignLanguage.P_TITLE);
        return new ListResult(builder.getResultList(session));
    }

    protected boolean checkLevel(Pattern pattern, ForeignLanguage language) {
        return pattern.matcher(language.getTitle().toUpperCase()).find();
    }

    @Override
    public Object getValue(Object primaryKey) {
        ForeignLanguage language = UniDaoFacade.getCoreDao().get(ForeignLanguage.class, (Long) primaryKey);
        if (getFilteredList().contains(language)) {
            language.getTitle(); // unproxy
            return language;
        }
        return null;
    }

    protected List<ForeignLanguage> getFilteredList() {
        MQBuilder builder = new MQBuilder(ForeignLanguage.ENTITY_CLASS, "el")
                .add(MQExpression.isNotNull("el", ForeignLanguage.title()));

        List<ForeignLanguage> list = UniDaoFacade.getCoreDao().getList(builder);
        Collections.sort(list, new EntityComparator<ForeignLanguage>());

        return list;
    }

    @Override
    public String getLabelFor(Object value, int columnIndex) {
        return ((ForeignLanguage) value).getTitle();
    }
}
