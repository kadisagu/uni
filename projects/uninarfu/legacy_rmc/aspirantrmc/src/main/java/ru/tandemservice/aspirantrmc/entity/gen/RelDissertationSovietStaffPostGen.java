package ru.tandemservice.aspirantrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.aspirantrmc.entity.RelDissertationSovietStaffPost;
import ru.tandemservice.aspirantrmc.entity.catalog.DissertationSoviet;
import ru.tandemservice.aspirantrmc.entity.catalog.PostDissertationSoviet;
import ru.tandemservice.aspirantrmc.entity.catalog.StaffDissertationSoviet;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность-связь Диссертационный совет - Состав диссертационного совета - Должности диссертационного совета
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RelDissertationSovietStaffPostGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.aspirantrmc.entity.RelDissertationSovietStaffPost";
    public static final String ENTITY_NAME = "relDissertationSovietStaffPost";
    public static final int VERSION_HASH = 1863507057;
    private static IEntityMeta ENTITY_META;

    public static final String L_DISSERTATION_SOVIET = "dissertationSoviet";
    public static final String L_STAFF_DISSERTATION_SOVIET = "staffDissertationSoviet";
    public static final String L_POST_DISSERTATION_SOVIET = "postDissertationSoviet";

    private DissertationSoviet _dissertationSoviet;     // Диссертационные советы
    private StaffDissertationSoviet _staffDissertationSoviet;     // Состав диссертационных советов
    private PostDissertationSoviet _postDissertationSoviet;     // Должности диссертационного совета

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Диссертационные советы. Свойство не может быть null.
     */
    @NotNull
    public DissertationSoviet getDissertationSoviet()
    {
        return _dissertationSoviet;
    }

    /**
     * @param dissertationSoviet Диссертационные советы. Свойство не может быть null.
     */
    public void setDissertationSoviet(DissertationSoviet dissertationSoviet)
    {
        dirty(_dissertationSoviet, dissertationSoviet);
        _dissertationSoviet = dissertationSoviet;
    }

    /**
     * @return Состав диссертационных советов.
     */
    public StaffDissertationSoviet getStaffDissertationSoviet()
    {
        return _staffDissertationSoviet;
    }

    /**
     * @param staffDissertationSoviet Состав диссертационных советов.
     */
    public void setStaffDissertationSoviet(StaffDissertationSoviet staffDissertationSoviet)
    {
        dirty(_staffDissertationSoviet, staffDissertationSoviet);
        _staffDissertationSoviet = staffDissertationSoviet;
    }

    /**
     * @return Должности диссертационного совета.
     */
    public PostDissertationSoviet getPostDissertationSoviet()
    {
        return _postDissertationSoviet;
    }

    /**
     * @param postDissertationSoviet Должности диссертационного совета.
     */
    public void setPostDissertationSoviet(PostDissertationSoviet postDissertationSoviet)
    {
        dirty(_postDissertationSoviet, postDissertationSoviet);
        _postDissertationSoviet = postDissertationSoviet;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RelDissertationSovietStaffPostGen)
        {
            setDissertationSoviet(((RelDissertationSovietStaffPost)another).getDissertationSoviet());
            setStaffDissertationSoviet(((RelDissertationSovietStaffPost)another).getStaffDissertationSoviet());
            setPostDissertationSoviet(((RelDissertationSovietStaffPost)another).getPostDissertationSoviet());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RelDissertationSovietStaffPostGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RelDissertationSovietStaffPost.class;
        }

        public T newInstance()
        {
            return (T) new RelDissertationSovietStaffPost();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "dissertationSoviet":
                    return obj.getDissertationSoviet();
                case "staffDissertationSoviet":
                    return obj.getStaffDissertationSoviet();
                case "postDissertationSoviet":
                    return obj.getPostDissertationSoviet();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "dissertationSoviet":
                    obj.setDissertationSoviet((DissertationSoviet) value);
                    return;
                case "staffDissertationSoviet":
                    obj.setStaffDissertationSoviet((StaffDissertationSoviet) value);
                    return;
                case "postDissertationSoviet":
                    obj.setPostDissertationSoviet((PostDissertationSoviet) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "dissertationSoviet":
                        return true;
                case "staffDissertationSoviet":
                        return true;
                case "postDissertationSoviet":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "dissertationSoviet":
                    return true;
                case "staffDissertationSoviet":
                    return true;
                case "postDissertationSoviet":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "dissertationSoviet":
                    return DissertationSoviet.class;
                case "staffDissertationSoviet":
                    return StaffDissertationSoviet.class;
                case "postDissertationSoviet":
                    return PostDissertationSoviet.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RelDissertationSovietStaffPost> _dslPath = new Path<RelDissertationSovietStaffPost>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RelDissertationSovietStaffPost");
    }
            

    /**
     * @return Диссертационные советы. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.RelDissertationSovietStaffPost#getDissertationSoviet()
     */
    public static DissertationSoviet.Path<DissertationSoviet> dissertationSoviet()
    {
        return _dslPath.dissertationSoviet();
    }

    /**
     * @return Состав диссертационных советов.
     * @see ru.tandemservice.aspirantrmc.entity.RelDissertationSovietStaffPost#getStaffDissertationSoviet()
     */
    public static StaffDissertationSoviet.Path<StaffDissertationSoviet> staffDissertationSoviet()
    {
        return _dslPath.staffDissertationSoviet();
    }

    /**
     * @return Должности диссертационного совета.
     * @see ru.tandemservice.aspirantrmc.entity.RelDissertationSovietStaffPost#getPostDissertationSoviet()
     */
    public static PostDissertationSoviet.Path<PostDissertationSoviet> postDissertationSoviet()
    {
        return _dslPath.postDissertationSoviet();
    }

    public static class Path<E extends RelDissertationSovietStaffPost> extends EntityPath<E>
    {
        private DissertationSoviet.Path<DissertationSoviet> _dissertationSoviet;
        private StaffDissertationSoviet.Path<StaffDissertationSoviet> _staffDissertationSoviet;
        private PostDissertationSoviet.Path<PostDissertationSoviet> _postDissertationSoviet;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Диссертационные советы. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.RelDissertationSovietStaffPost#getDissertationSoviet()
     */
        public DissertationSoviet.Path<DissertationSoviet> dissertationSoviet()
        {
            if(_dissertationSoviet == null )
                _dissertationSoviet = new DissertationSoviet.Path<DissertationSoviet>(L_DISSERTATION_SOVIET, this);
            return _dissertationSoviet;
        }

    /**
     * @return Состав диссертационных советов.
     * @see ru.tandemservice.aspirantrmc.entity.RelDissertationSovietStaffPost#getStaffDissertationSoviet()
     */
        public StaffDissertationSoviet.Path<StaffDissertationSoviet> staffDissertationSoviet()
        {
            if(_staffDissertationSoviet == null )
                _staffDissertationSoviet = new StaffDissertationSoviet.Path<StaffDissertationSoviet>(L_STAFF_DISSERTATION_SOVIET, this);
            return _staffDissertationSoviet;
        }

    /**
     * @return Должности диссертационного совета.
     * @see ru.tandemservice.aspirantrmc.entity.RelDissertationSovietStaffPost#getPostDissertationSoviet()
     */
        public PostDissertationSoviet.Path<PostDissertationSoviet> postDissertationSoviet()
        {
            if(_postDissertationSoviet == null )
                _postDissertationSoviet = new PostDissertationSoviet.Path<PostDissertationSoviet>(L_POST_DISSERTATION_SOVIET, this);
            return _postDissertationSoviet;
        }

        public Class getEntityClass()
        {
            return RelDissertationSovietStaffPost.class;
        }

        public String getEntityName()
        {
            return "relDissertationSovietStaffPost";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
