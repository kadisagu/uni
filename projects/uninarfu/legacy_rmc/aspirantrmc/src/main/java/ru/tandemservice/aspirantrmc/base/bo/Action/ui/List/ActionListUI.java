package ru.tandemservice.aspirantrmc.base.bo.Action.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.aspirantrmc.base.bo.Action.ui.AddEdit.ActionAddEdit;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;


@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "studentHolder.id")
})
public class ActionListUI extends UIPresenter {
    public static final String STUDENT_ID = "studentId";
    public static final String TYPE_ACTION_FILTER = "typeAction";
    public static final String TYPE_PARTICIPATION_FILTER = "typeParticipation";
    public static final String STATUS_ACTION_FILTER = "statusAction";

    private EntityHolder<Student> studentHolder = new EntityHolder<Student>();

    @Override
    public void onComponentRefresh() {
        studentHolder.refresh(Student.class);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource ds) {
        if (ds.getName().equals(ActionList.LIST_DS)) {
            ds.put(STUDENT_ID, getStudent().getId());
            ds.put(TYPE_ACTION_FILTER, getSettings().get(TYPE_ACTION_FILTER));
            ds.put(TYPE_PARTICIPATION_FILTER, getSettings().get(TYPE_PARTICIPATION_FILTER));
            ds.put(STATUS_ACTION_FILTER, getSettings().get(STATUS_ACTION_FILTER));
        }
    }

    public void onEditEntry() {
        getActivationBuilder().asRegion(ActionAddEdit.class)
                .parameter("publisherId", getStudent().getId())
                .parameter("actionId", getListenerParameterAsLong())
                .activate();
    }

    public void onDeleteEntry() {
        UniDaoFacade.getCoreDao().delete(getListenerParameterAsLong());
        onComponentRefresh();
    }


    public void onClickAddAction() {
        getActivationBuilder().asRegion(ActionAddEdit.class).activate();
    }

    public void onClickSearch() {
        getSettings().save();
    }

    public void onClickClear() {
        getSettings().clear();
    }


    public Student getStudent() {
        return studentHolder.getValue();
    }

    public EntityHolder<Student> getStudentHolder() {
        return studentHolder;
    }

    public void setStudentHolder(EntityHolder<Student> studentHolder) {
        this.studentHolder = studentHolder;
    }
}
