package ru.tandemservice.aspirantrmc.base.bo.Mission.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.aspirantrmc.entity.catalog.StatusMission;

public interface IDAO extends INeedPersistenceSupport {
    public StatusMission getDefaultStatusMission();
}
