package ru.tandemservice.aspirantrmc.component.catalog.aspirantPracticeType.AspirantPracticeTypeAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.aspirantrmc.entity.catalog.AspirantPracticeType;

public interface IDAO extends IDefaultCatalogAddEditDAO<AspirantPracticeType, Model> {
}
