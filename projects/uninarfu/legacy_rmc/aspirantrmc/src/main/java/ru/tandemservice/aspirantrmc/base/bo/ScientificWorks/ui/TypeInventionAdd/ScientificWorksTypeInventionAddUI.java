package ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ui.TypeInventionAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ScientificWorksManager;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.logic.IScienetificWorksModifyDao;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ui.PatentAddEdit.ScientificWorksPatentAddEditUI;
import ru.tandemservice.aspirantrmc.entity.catalog.TypeInvention;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: ramaslov
 * Date: 10.02.13
 * Time: 17:48
 * To change this template use File | Settings | File Templates.
 */
@State({
        @Bind(
                key = ScientificWorksTypeInventionAddUI.BIND_KEY_INVENTION_AUTHOR,
                binding = ScientificWorksTypeInventionAddUI.BIND_KEY_INVENTION_AUTHOR, required = false),
        @Bind(
                key = ScientificWorksTypeInventionAddUI.BIND_KEY_INVENTION_CERTIFICATE_NUMBER,
                binding = ScientificWorksTypeInventionAddUI.BIND_KEY_INVENTION_CERTIFICATE_NUMBER, required = false),
        @Bind(
                key = ScientificWorksTypeInventionAddUI.BIND_KEY_INVENTION_COLLABORATORS,
                binding = ScientificWorksTypeInventionAddUI.BIND_KEY_INVENTION_COLLABORATORS, required = false),
        @Bind(
                key = ScientificWorksTypeInventionAddUI.BIND_KEY_INVENTION_IMPRINT_DATE,
                binding = ScientificWorksTypeInventionAddUI.BIND_KEY_INVENTION_IMPRINT_DATE, required = false),
        @Bind(
                key = ScientificWorksTypeInventionAddUI.BIND_KEY_INVENTION_INVENTION_TYPE,
                binding = ScientificWorksTypeInventionAddUI.BIND_KEY_INVENTION_INVENTION_TYPE, required = false),
        @Bind(
                key = ScientificWorksTypeInventionAddUI.BIND_KEY_INVENTION_NOTE,
                binding = ScientificWorksTypeInventionAddUI.BIND_KEY_INVENTION_NOTE, required = false),
        @Bind(
                key = ScientificWorksTypeInventionAddUI.BIND_KEY_INVENTION_TITLE,
                binding = ScientificWorksTypeInventionAddUI.BIND_KEY_INVENTION_TITLE, required = false),
        @Bind(
                key = ScientificWorksTypeInventionAddUI.BIND_KEY_INVENTION_PATENT_NUMBER,
                binding = ScientificWorksTypeInventionAddUI.BIND_KEY_INVENTION_PATENT_NUMBER, required = false)
})
public class ScientificWorksTypeInventionAddUI extends UIPresenter {

    public static final String BIND_KEY_INVENTION_AUTHOR = "author";
    public static final String BIND_KEY_INVENTION_INVENTION_TYPE = "inventionType";
    public static final String BIND_KEY_INVENTION_TITLE = "title";
    public static final String BIND_KEY_INVENTION_CERTIFICATE_NUMBER = "certificateNumber";
    public static final String BIND_KEY_INVENTION_PATENT_NUMBER = "patentNumber";
    public static final String BIND_KEY_INVENTION_COLLABORATORS = "collaborators";
    public static final String BIND_KEY_INVENTION_IMPRINT_DATE = "imprintDate";
    public static final String BIND_KEY_INVENTION_NOTE = "note";

    private TypeInvention typeInvention = new TypeInvention();

    private Long author;
    private Long inventionType;
    private String title;
    private String certificateNumber;
    private String patentNumber;
    private String collaborators;
    private Integer imprintDate;
    private String note;

    @Override
    public void deactivate() {
        Map params = getParamsMap();
        deactivate(params);
    }

    public void onClickApply() {
        IScienetificWorksModifyDao dao = ScientificWorksManager.instance().modifyDao();
        typeInvention.setCode(dao.getNewTypeInventionCode());
        dao.saveTypeInvention(typeInvention);
        Map params = getParamsMap();
        params.put(ScientificWorksPatentAddEditUI.BIND_KEY_INVENTION_INVENTION_TYPE, typeInvention.getId());

        deactivate(params);
    }

    private Map<String, Object> getParamsMap() {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(ScientificWorksPatentAddEditUI.BIND_KEY_INVENTION_AUTHOR, author);
        params.put(ScientificWorksPatentAddEditUI.BIND_KEY_INVENTION_INVENTION_TYPE, inventionType);
        params.put(ScientificWorksPatentAddEditUI.BIND_KEY_INVENTION_TITLE, title);
        params.put(ScientificWorksPatentAddEditUI.BIND_KEY_INVENTION_CERTIFICATE_NUMBER, certificateNumber);
        params.put(ScientificWorksPatentAddEditUI.BIND_KEY_INVENTION_PATENT_NUMBER, patentNumber);
        params.put(ScientificWorksPatentAddEditUI.BIND_KEY_INVENTION_COLLABORATORS, collaborators);
        params.put(ScientificWorksPatentAddEditUI.BIND_KEY_INVENTION_IMPRINT_DATE, imprintDate);
        params.put(ScientificWorksPatentAddEditUI.BIND_KEY_INVENTION_NOTE, note);
        return params;
    }

    /*
    *-------------GETTERS & SETTERS-------------------
     */

    public TypeInvention getTypeInvention() {
        return typeInvention;
    }

    public void setTypeInvention(TypeInvention typeInvention) {
        this.typeInvention = typeInvention;
    }

    public Long getAuthor() {
        return author;
    }

    public void setAuthor(Long author) {
        this.author = author;
    }

    public Long getInventionType() {
        return inventionType;
    }

    public void setInventionType(Long inventionType) {
        this.inventionType = inventionType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCertificateNumber() {
        return certificateNumber;
    }

    public void setCertificateNumber(String certificateNumber) {
        this.certificateNumber = certificateNumber;
    }

    public String getPatentNumber() {
        return patentNumber;
    }

    public void setPatentNumber(String patentNumber) {
        this.patentNumber = patentNumber;
    }

    public String getCollaborators() {
        return collaborators;
    }

    public void setCollaborators(String collaborators) {
        this.collaborators = collaborators;
    }

    public Integer getImprintDate() {
        return imprintDate;
    }

    public void setImprintDate(Integer imprintDate) {
        this.imprintDate = imprintDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
