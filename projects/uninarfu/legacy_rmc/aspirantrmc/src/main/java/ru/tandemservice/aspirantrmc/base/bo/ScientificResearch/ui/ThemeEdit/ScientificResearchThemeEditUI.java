package ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.ThemeEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.logic.EntitySelectModel;
import ru.tandemservice.aspirantrmc.entity.ScientificResearch;
import ru.tandemservice.uni.dao.UniDaoFacade;

@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "researchHolder.id", required = true)
})
public class ScientificResearchThemeEditUI extends UIPresenter {
    private EntityHolder<ScientificResearch> researchHolder = new EntityHolder<ScientificResearch>();
    private ISelectModel themeSelectModel;

    @Override
    public void onComponentRefresh() {
        getResearchHolder().refresh(ScientificResearch.class);
        themeSelectModel = new EntitySelectModel(ScientificResearch.class, ScientificResearch.title());
    }

    public void onClickApply() {
        if (researchHolder.getValue().getTitle().equals("undefined"))
            researchHolder.getValue().setTitle(null);
        UniDaoFacade.getCoreDao().saveOrUpdate(researchHolder.getValue());
        deactivate();
    }

    public ScientificResearch getResearch() {
        return researchHolder.getValue();
    }

    public EntityHolder<ScientificResearch> getResearchHolder() {
        return researchHolder;
    }

    public void setResearchHolder(EntityHolder<ScientificResearch> researchHolder) {
        this.researchHolder = researchHolder;
    }

    public ISelectModel getThemeSelectModel() {
        return themeSelectModel;
    }

    public void setThemeSelectModel(ISelectModel themeSelectModel) {
        this.themeSelectModel = themeSelectModel;
    }
}
