package ru.tandemservice.aspirantrmc.component.student.DefenseDissertationWork.ThemeDissertationWorkAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onActivateComponent(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        ((IDAO) getDao()).prepare(model);
    }

    public void onClickApply(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        ((IDAO) getDao()).update(model);
        deactivate(component);
    }
}
