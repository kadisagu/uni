package ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ui.TypePublicationAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ScientificWorksManager;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.logic.IScienetificWorksModifyDao;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ui.AddEdit.ScientificWorksAddEditUI;
import ru.tandemservice.aspirantrmc.entity.ScientificPublication;
import ru.tandemservice.aspirantrmc.entity.catalog.TypePublication;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: ramaslov
 * Date: 10.02.13
 * Time: 17:48
 * To change this template use File | Settings | File Templates.
 */
@State({
        @Bind(
                key = ScientificWorksTypePublicationAddUI.BIND_KEY_PUBLICATION_TITLE,
                binding = ScientificWorksTypePublicationAddUI.BIND_KEY_PUBLICATION_TITLE, required = false),
        @Bind(
                key = ScientificWorksTypePublicationAddUI.BIND_KEY_PUBLICATION_COLLABORATORS,
                binding = ScientificWorksTypePublicationAddUI.BIND_KEY_PUBLICATION_COLLABORATORS, required = false),
        @Bind(
                key = ScientificWorksTypePublicationAddUI.BIND_KEY_PUBLICATION_AUTHOR,
                binding = ScientificWorksTypePublicationAddUI.BIND_KEY_PUBLICATION_AUTHOR, required = false),
        @Bind(
                key = ScientificWorksTypePublicationAddUI.BIND_KEY_PUBLICATION_PUBLISHING_HOUSE,
                binding = ScientificWorksTypePublicationAddUI.BIND_KEY_PUBLICATION_PUBLISHING_HOUSE, required = false),
        @Bind(
                key = ScientificWorksTypePublicationAddUI.BIND_KEY_PUBLICATION_LANGUAGE,
                binding = ScientificWorksTypePublicationAddUI.BIND_KEY_PUBLICATION_LANGUAGE, required = false),
        @Bind(
                key = ScientificWorksTypePublicationAddUI.BIND_KEY_PUBLICATION_IMPRINT_DATE,
                binding = ScientificWorksTypePublicationAddUI.BIND_KEY_PUBLICATION_IMPRINT_DATE, required = false),
        @Bind(
                key = ScientificWorksTypePublicationAddUI.BIND_KEY_PUBLICATION_NUMBERS_OF_PAGES,
                binding = ScientificWorksTypePublicationAddUI.BIND_KEY_PUBLICATION_NUMBERS_OF_PAGES, required = false),
        @Bind(
                key = ScientificWorksTypePublicationAddUI.BIND_KEY_PUBLICATION_ID,
                binding = ScientificWorksTypePublicationAddUI.BIND_KEY_PUBLICATION_ID, required = false)
})
public class ScientificWorksTypePublicationAddUI extends UIPresenter {

    public static final String BIND_KEY_PUBLICATION_ID = "idPublication";
    public static final String BIND_KEY_PUBLICATION_TITLE = "title";
    public static final String BIND_KEY_PUBLICATION_COLLABORATORS = "collaborators";
    public static final String BIND_KEY_PUBLICATION_AUTHOR = "author";
    public static final String BIND_KEY_PUBLICATION_PUBLISHING_HOUSE = "publishingHouse";
    public static final String BIND_KEY_PUBLICATION_LANGUAGE = "language";
    public static final String BIND_KEY_PUBLICATION_IMPRINT_DATE = "imprintDate";
    public static final String BIND_KEY_PUBLICATION_NUMBERS_OF_PAGES = "numbersOfPages";

    private ScientificPublication publication;
    private TypePublication typePublication = new TypePublication();

    private String title;
    private String collaborators;
    private Long author;
    private Long idPublication;
    private String publishingHouse;
    private Long language;
    private Integer imprintDate;
    private String numbersOfPages;

    @Override
    public void deactivate() {
        Map params = getParamsMap();
        deactivate(params);
    }

    public void onClickApply() {
        IScienetificWorksModifyDao dao = ScientificWorksManager.instance().modifyDao();
        typePublication.setCode(dao.getNewTypePublicationCode());
        dao.saveTypePublication(typePublication);
        Map params = getParamsMap();
        params.put(ScientificWorksAddEditUI.BIND_KEY_PUBLICATION_TYPE_PUBLICATION, typePublication.getId());

        deactivate(params);
    }

    private Map<String, Object> getParamsMap() {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(ScientificWorksAddEditUI.BIND_KEY_PUBLICATION_TITLE, title);
        params.put(ScientificWorksAddEditUI.BIND_KEY_PUBLICATION_COLLABORATORS, collaborators);
        params.put(ScientificWorksAddEditUI.BIND_KEY_PUBLICATION_AUTHOR, author);
        params.put(ScientificWorksAddEditUI.BIND_KEY_PUBLICATION_PUBLISHING_HOUSE, publishingHouse);
        params.put(ScientificWorksAddEditUI.BIND_KEY_PUBLICATION_LANGUAGE, language);
        params.put(ScientificWorksAddEditUI.BIND_KEY_PUBLICATION_IMPRINT_DATE, imprintDate);
        params.put(ScientificWorksAddEditUI.BIND_KEY_PUBLICATION_NUMBERS_OF_PAGES, numbersOfPages);
        return params;
    }

    /*
    *-------------GETTERS & SETTERS-------------------
     */
    public TypePublication getTypePublication() {
        return typePublication;
    }

    public void setTypePublication(TypePublication typePublication) {
        this.typePublication = typePublication;
    }

    public ScientificPublication getPublication() {
        return publication;
    }

    public void setPublication(ScientificPublication publication) {
        this.publication = publication;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCollaborators() {
        return collaborators;
    }

    public void setCollaborators(String collaborators) {
        this.collaborators = collaborators;
    }

    public Long getAuthor() {
        return author;
    }

    public void setAuthor(Long author) {
        this.author = author;
    }

    public String getPublishingHouse() {
        return publishingHouse;
    }

    public void setPublishingHouse(String publishingHouse) {
        this.publishingHouse = publishingHouse;
    }

    public Long getLanguage() {
        return language;
    }

    public void setLanguage(Long language) {
        this.language = language;
    }

    public Integer getImprintDate() {
        return imprintDate;
    }

    public void setImprintDate(Integer imprintDate) {
        this.imprintDate = imprintDate;
    }

    public String getNumbersOfPages() {
        return numbersOfPages;
    }

    public void setNumbersOfPages(String numbersOfPages) {
        this.numbersOfPages = numbersOfPages;
    }

    public Long getIdPublication() {
        return idPublication;
    }

    public void setIdPublication(Long idPublication) {
        this.idPublication = idPublication;
    }
}
