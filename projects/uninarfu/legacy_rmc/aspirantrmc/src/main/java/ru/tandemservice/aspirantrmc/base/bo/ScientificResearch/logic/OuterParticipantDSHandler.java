package ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.UIAbstracts.AbstractParticipantUI;
import ru.tandemservice.aspirantrmc.entity.ScienceParticipant;

public class OuterParticipantDSHandler extends DefaultSearchDataSourceHandler {
    private static final DQLOrderDescriptionRegistry order = new DQLOrderDescriptionRegistry(ScienceParticipant.class, "sp");
    String participantType;

    public OuterParticipantDSHandler(String ownerId, String participantType) {
        super(ownerId);
        this.participantType = participantType;
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context) {
        Long researchId = context.get(AbstractParticipantUI.RESEARCH_VALUE);

        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(ScienceParticipant.class, "sp");
        dql.addColumn("sp");

        dql.where(DQLExpressions.eq(
                DQLExpressions.property(ScienceParticipant.research().id().fromAlias("sp")),
                DQLExpressions.value(researchId)
        ));
        dql.where(DQLExpressions.eq(
                DQLExpressions.property(ScienceParticipant.participantType().code().fromAlias("sp")),
                DQLExpressions.value(participantType)
        ));

        order.applyOrder(dql, input.getEntityOrder());

        return DQLSelectOutputBuilder.get(input, dql, context.getSession()).pageable(true).build();
    }
}
