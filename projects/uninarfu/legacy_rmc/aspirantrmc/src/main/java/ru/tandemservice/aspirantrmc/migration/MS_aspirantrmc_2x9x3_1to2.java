package ru.tandemservice.aspirantrmc.migration;

import com.google.common.collect.ImmutableSet;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.shared.commonbase.utils.MergeBuilder;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.sql.SQLException;
import java.util.Map;
import java.util.Set;

import static org.tandemframework.shared.commonbase.utils.MergeBuilder.*;
import static org.tandemframework.shared.commonbase.utils.MigrationUtils.getIdByCode;

@SuppressWarnings({"unused", "deprecation"})
public class MS_aspirantrmc_2x9x3_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies() {
        return new ScriptDependency[] {
                new ScriptDependency("org.tandemframework", "1.6.18"),
                new ScriptDependency("org.tandemframework.shared", "1.9.3"),
                new ScriptDependency("ru.tandemservice.uni.product", "2.9.3")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // Избавление от аспирантских НПм рамека
        //
        // 1.  Пересадка уровней образования. [done]
        // 1.1 Уровень образования в кодом "41" переносим в продукт. [done]
        // 1.2 НПм, с уровнем "40" и ОКСО отличном от xx.06.xx пересаживаем на уровень "41" (через мерж). [done]
        // 1.3 Уровень "40" удаляем вообще. [done]
        //
        // 2.  Избавление от класса EducationLevelScientific. [done]
        // 2.1 Меняем класс сущности с EducationLevelScientific на EducationLevelHigher. [done]
        // 2.2 Удаляем сущность EducationLevelScientific. [done]
        //
        // 3.  Перенос перечня аспирантов 2009 года в продукт.
        // 3.1 Переносим перечень в json. Меняем системный код (в базе тоже), рефакторим код сафу. [done]
        // 3.2 Делаем так, чтобы форма добавления НПв ругалась на русском, если попытаться добавить на аспирантов 2009.
        // 3.3 Переносим квалификацию "Кандидат наук" в продукт. [done]
        // 3.4 Удаляем код рамека, связанный с импортом НПм, созданием направлений проф. образования, квалификаций и прочего.
        // 3.5 Меняем код квалификации проф. образования "2013.96.PhD" на "2009.00.PhD" (оно только в рамеке есть). [done]
        // 3.6 В ИжГТУ всех с квалификацией (Qualifications) "Кандидат наук" и "Доктор наук" пересаживаем на квалификацию "Аспирантура" ("Асп"). Сами квалификации удаляем из справочника. [done]
        // 3.7 В САФУ на квалификацию "Аспирантура" надо пересадить те НПм, которые переехали на уровень "38". Квалификацию "Доктор наук" удаляем из справочника и из базы. [done]
        //
        // 4.  Смена класса направлениям проф. образования для аспирантов 2009 года на правильный класс сущности. [done]

        // Меняем класс сущности с EducationLevelScientific на EducationLevelHigher для аспирантов рамека
        migrateEduLevels(tool);

        // Пересаживаем аспирантские НПм на продуктовый уровень образования (38 и 39)
        migrateEduLevelTypes(tool);

        // Мигрируем направления подготовки проф. образования 2013 года, которые реально должны быть 2009 года - меняем класс сущности
        migrateProgramSubject(tool);

        // Избавляемся от квалификации "Доктор наук"
        mergeQualifications(tool);

        // Выставляем квалификацию "Аспирантура" всем НПм на уровнях продуктовой аспирантуры (38/39)
        fixQualifications(tool);

        // Удаляем модуль aspirantbasermc - он больше не нужен
        dropBaseAspModule(tool);
    }

    private void migrateEduLevels(DBTool tool) throws SQLException {

        tool.table("educationlevels_t").uniqueKeys().clear();
        final String idsExpression = "select id from educationlevels_t where discriminator=" + tool.entityCodes().get("educationLevelScientific");
        final int a = MigrationUtils.changeEntityClass(tool, "educationLevelHigher", "educationlevels_t", idsExpression);
        final int b = tool.executeUpdate("update educationlevels_t set catalogcode_p='educationLevelHigher' where catalogcode_p='educationLevelScientific'");
        if (a != b) {
            throw new IllegalStateException(String.format("a, b : %d, %d", a, b));
        }

        MigrationUtils.mergeBuilder(tool, new MergeBuilder.MergeRuleBuilder("educationlevels_t")
                .key("catalogcode_p", "code_p")
                .order("id", OrderDirection.desc)
                .where("eduprogramsubject_id is not null")
                .build())
                .addRule(byLinkRule("educationlevelshighschool_t", 0, "educationlevel_id", "assignedqualification_id", "orgunit_id", "programorientation_id", "title_p").printMergedIdsToLog(true).build())
                .addRule(byLinkRule("educationlevelshighschool_t", 0, "educationlevel_id", "assignedqualification_id", "orgunit_id", "programorientation_id", "shorttitle_p").printMergedIdsToLog(true).build())
                .addRule(byLinkRule("educationorgunit_t", 0, "educationlevelhighschool_id", "formativeorgunit_id", "territorialorgunit_id", "developform_id", "developtech_id", "developcondition_id", "developperiod_id").printMergedIdsToLog(true))
                .addRule(stopRule("epl_student_t", "educationorgunit_id", "compensationsource_id", "group_id"))
                .addRule(stopRule("epl_student_t", "educationorgunit_id", "compensationsource_id", "group_id", "developgrid_id"))
                .addRule(stopRule("ctrpr_prel_period", "element_id", "durationbegin_p"))
                .addRule(stopRule("eductr_ou_config", "educationorgunit_id", "cipher_p", "educationyear_id"))
                .addRule(singleRule("epp_eduou_epv_rel_t", "educationorgunit_id"))
                .addRule(byLinkRule("student2orientation_t", 0, "orientation_id", "student_id"))
                .addRule(byLinkRule("orientation2eduplanversion_t", 0, "orientation_id", "eduplanversion_id", "educationyear_id"))
                .addRule(byLinkRule("sc_eduou_config", 0, "educationorgunit_id", "educationyear_id", "cipher_p"))
                .addRule(singleRule("sc_eduou_config_relation", "config_id"))
                .addRule(byLinkRule("profileeducationorgunit_t", 0, "educationorgunit_id", "enrollmentdirection_id"))
                .addRule(byLinkRule("priorityprofileeduou_t", 0, "profileeducationorgunit_id", "rqstdenrllmntdrctn_id"))
                .addRule(byLinkRule("onlinepriorityprofileeduou_t", 0, "profileeducationorgunit_id", "nlnrqstdenrllmntdrctn_id"))
                .addRule(stopRule("enrollmentdirection_t", "educationorgunit_id", "enrollmentcampaign_id")) // Будем надеяться, что до этого не дойдет...
                .addRule(singleRule("ecf_conv_eduou_t", "educationorgunit_id"))
                .addRule(byLinkRule("accreditatedgroup_t", 0, "base_id", "formativeorgunit_id", "branch_id"))
                .merge("educationLevels");

        // удаляем сущность educationLevelScientific
        tool.entityCodes().delete("educationLevelScientific");

        // Нужно будет перегенерить названия НПв
        tool.executeUpdate("delete from settings_s where owner_p=?", "EduLevelHSDisplayableTitleSync");
    }

    private void migrateProgramSubject(DBTool tool) throws SQLException {

        // Перечень аспирантов 2009 перетащен в продукт.
        // Код перечня 2013.96 меняем на 2009.00
        final Long asp2013subjectIndexId = MigrationUtils.getIdByCode(tool, "edu_c_pr_subject_index_t", "2013.06");
        final Long asp2009subjectIndexId = MigrationUtils.getIdByCode(tool, "edu_c_pr_subject_index_t", "2013.96");

        final Long gen2009 = MigrationUtils.getIdByCode(tool, "edu_c_pr_subject_index_gen_t", "2009");
        tool.executeUpdate("update edu_c_pr_subject_index_t set code_p=?, title_p=?, shorttitle_p=?, edulevelandindexnotation_p=?, eduprogramsubjecttype_p=?, generation_id=? where id=?",
                           "2009.00", "Перечень направлений аспирантуры 2009", "ВО аспирантура (2009)", "ВО ФГОС 2009", "направление подготовки в аспирантуре", gen2009, asp2009subjectIndexId);

        final Map<String, Long> levelMap = MigrationUtils.getCatalogCode2IdMap(tool, "structureeducationlevels_t");
        if (levelMap.containsKey("40")) {
            // Этого уровня уже быть не должно
            throw new IllegalStateException();
        }

        // Удаляем неиспользуемые связи квалификации с направлением для старых аспирантов
        final String cleanedIds = "select id from edu_c_pr_subject_qual_t where programsubject_id in (select id from edu_c_pr_subject_t where subjectindex_id in ("+asp2013subjectIndexId+","+asp2009subjectIndexId+"))";
        MigrationUtils.deleteNotRelatedRows(tool, "eduProgramSubjectQualification", cleanedIds);

        // Всем новым аспирантским направления надо проставить перечень аспирантов 13 года
        tool.executeUpdate("update edu_c_pr_subject_t set subjectindex_id=? where subjectindex_id=? and subjectcode_p like '__.06.__'", asp2013subjectIndexId, asp2009subjectIndexId);

        // Всем старым аспирантским направления надо проставить перечень аспирантов 09 года
        tool.executeUpdate("update edu_c_pr_subject_t set subjectindex_id=? where subjectindex_id=? and not (subjectcode_p like '__.06.__')", asp2009subjectIndexId, asp2013subjectIndexId);

        tool.table("edu_c_pr_subject_t").uniqueKeys().clear();

        // Меняем коды направлений 2013.96.xx.xx.xx -> 2009.xx.xx.xx
        tool.executeUpdate(
                "update edu_c_pr_subject_t set code_p='2009.'+subjectcode_p where subjectindex_id=? and code_p<>'2009.'+subjectcode_p",
                asp2009subjectIndexId);

        // Направлениям 13 года выставляем правильный код, если стоят кривые
        tool.executeUpdate(
                "update edu_c_pr_subject_t set code_p='2013.'+subjectcode_p where subjectindex_id=? and code_p<>'2013.'+subjectcode_p",
                asp2013subjectIndexId);

        // Мержим по subjectCode+title
        MigrationUtils.mergeBuilder(tool, new MergeBuilder.MergeRuleBuilder("edu_c_pr_subject_t")
                .key("code_p")
                .where("subjectindex_id in (" + asp2013subjectIndexId + "," + asp2009subjectIndexId + ")")
                //.order(tool.getDialect().getLengthFunction("code_p"), OrderDirection.asc)
                .order("id", OrderDirection.desc)
                .build())
                .addRule(skipRule("enr14fis_s_st_exam_min_mark_t", "enrollmentcampaign_id", "eduprogramsubject_id", "discipline_id"))
                .addRule(skipRule("enr14fis_s_st_exam_min_mark_t", "enrollmentcampaign_id", "eduprogramsubject_id", "discipline_id", "stateexamsubject_id"))
                .addRule(stopRule("edu_c_pr_subject_qual_t", "programsubject_id", "programqualification_id"))
                .addRule(stopRule("dip_edu_pr_subject_compare_t", "oldprogramsubject_id"))
                .addRule(stopRuleWithCondition("educationlevels_t", "eduprogramsubject_id is not null", "eduprogramsubject_id", "eduprogramspecialization_id", "leveltype_id"))
                .merge("eduProgramSubject");

        // Удаляем неиспользуемые направления
        final String orphanSubjectSql = MigrationUtils.getOrphanIdsSQL(tool, "eduProgramSubject", cleanedIds, ImmutableSet.of("edu_c_pr_subject__2013_t"));
        final Set<String> subj2013Tables = ImmutableSet.of("edu_c_pr_subject__2013_t", "edu_c_pr_subject_t");
        MigrationUtils.deleteRows(tool, subj2013Tables, orphanSubjectSql);

        // Сначала надо создать фэйковую укрупненную группу, на которую переедут затронутые направления
        final Short oksoGroupCode = tool.entityCodes().get("eduProgramSubjectOksoGroup");
        final Long fakeGroupId = EntityIDGenerator.generateNewId(oksoGroupCode);
        final String fakeGroupCode = "000000";
        final String fakeGroupTitle = "Научные направления";
        tool.executeUpdate("insert into edu_c_pr_subject__okso_group_t (id, discriminator, code_p, title_p) values (?,?,?,?)", fakeGroupId, oksoGroupCode, fakeGroupCode, fakeGroupTitle);

        final String migratedIdsExpression = "select id from edu_c_pr_subject_t where subjectindex_id=" + asp2009subjectIndexId;

        // Удаляем записи из edu_c_pr_subject__2013_t
        final int a = tool.executeUpdate("delete from edu_c_pr_subject__2013_t where id in (" + migratedIdsExpression + ")");

        // Вставляем соответствующие записи в edu_c_pr_subject__2009_t (для всех, для кого там нет ещё). при этом assignSpecialDegree ставим false
        final int b = tool.executeUpdate("insert into edu_c_pr_subject__2009_t (id, group_id, assignspecialdegree_p) " +
                                                 " select x.id, ?, ? from (" + migratedIdsExpression + ") x", fakeGroupId, false);

        // Мигрируем
        final int c = MigrationUtils.changeEntityClass(tool, "eduProgramSubject2009", "edu_c_pr_subject_t", migratedIdsExpression);

        if (ImmutableSet.of(a, b, c).size() != 1) {
            throw new IllegalStateException(String.format("a, b, c = %d, %d, %d", a, b, c));
        }
    }

    private void migrateEduLevelTypes(DBTool tool) throws SQLException {

        // Мержим уровни 40 и 41 (40 удаляется, 41 перенесен в продукт)
        final MergeBuilder.MergeRule levelTypeMainRule = new MergeBuilder.MergeRuleBuilder("structureeducationlevels_t")
                .key("title_p") // оба уровня имеют название "специальность"
                .where("code_p in ('40', '41')")
                .order("code_p", OrderDirection.desc) // Оставляем уровень с кодом 41
                .build();

        // Мержиться НПм не должны вроде бы
        final MergeBuilder.MergeRule eduLvlStopRule = new MergeBuilder.MergeRule("educationlevels_t", ImmutableSet.of("eduprogramsubject_id", "eduprogramspecialization_id", "leveltype_id")) {
            @Override public boolean isStopped() { return true; }
            @Override protected String getWhere() { return "eduprogramsubject_id is not null"; }
            @Override protected Long findMainRow(Map<Long, Object[]> groupRows, Long newLinkId, DBTool tool) throws SQLException {
                throw new IllegalStateException("Not expected merge by rule " + this + ". NewLink.id: " + newLinkId + ". " + MergeBuilder.groupRowsToString(groupRows));
            }
            @Override public String toString() { return "STOP ON " + super.toString(); }
        };

        MigrationUtils.mergeBuilder(tool, levelTypeMainRule)
                .addRule(eduLvlStopRule)
                .addRule(stopRule("structureeducationlevels_t", "parent_id", "priority_p"))
                .addRule(stopRule("nrllmntdrctnrstrctnrmc_t", "enrollmentcampaign_id", "developform_id", "compensationtype_id", "structureeducationlevels_id"))
                .merge("structureEducationLevels");

        // Если в ОКСО код xx.06.xx и рутовая направленность, пересаживаем на 38 (направление аспирантов)
        // Если в ОКСО код xx.06.xx и не рутовая направленность, пересаживаем на 39 (направленность аспирантов)
        final Map<String, Long> levelMap = MigrationUtils.getCatalogCode2IdMap(tool, "structureeducationlevels_t");
        final Short rootSpecializationCode = tool.entityCodes().get("eduProgramSpecializationRoot");
        final Short higherEduLevelCode = tool.entityCodes().get("educationLevelHigher");
        final String prefix = tool.getDataSource().getSqlFunctionPrefix();

        int n = tool.executeUpdate(
                "update educationlevels_t set leveltype_id=? " +
                        " where " + prefix + "getEntityCode(eduprogramspecialization_id)=? " +
                        " and leveltype_id in (?,?) " +
                        " and okso_p like '__.06.__'",
                levelMap.get("38"), rootSpecializationCode, levelMap.get("39"), levelMap.get("41"));

        if (n > 0) {
            tool.info(n + " rows in educationlevels_t moved to level type '38'");
        }

        n = tool.executeUpdate(
                "update educationlevels_t set leveltype_id=? " +
                        " where " + prefix + "getEntityCode(eduprogramspecialization_id)<>? " +
                        " and leveltype_id in (?,?) " +
                        " and okso_p like '__.06.__'",
                levelMap.get("39"), rootSpecializationCode, levelMap.get("38"), levelMap.get("41"));

        if (n > 0) {
            tool.info(n + " rows in educationlevels_t moved to level type '39'");
        }

        // Возвращаем продуктовые названия уровням 38-39
        tool.executeUpdate("update structureeducationlevels_t set title_p=?, shorttitle_p=null where code_p=?", "направление подготовки в аспирантуре", "38");
        tool.executeUpdate("update structureeducationlevels_t set title_p=?, shorttitle_p=null where code_p=?", "направленность подготовки в аспирантуре", "39");
    }

    private void mergeQualifications(DBTool tool) throws SQLException {

        // Квалификации (Qualifications) "Кандидат наук" и "Доктор наук" объединяем ("Доктор наук" удаляется, а "Кандидат наук" уезжает в продукт).
        // Просто делаем им одиннаковый код и мержим мержинатором.

        tool.table("qualifications_t").indexes().clear();
        tool.table("qualifications_t").uniqueKeys().clear();
        tool.executeUpdate("update qualifications_t set code_p=? where code_p=?", "КН", "ДН");

        final MergeBuilder.MergeRule mainRule = new MergeBuilder.MergeRuleBuilder("qualifications_t").key("code_p")
                .order("title_p", OrderDirection.desc) // по названию Кандидат наук после Доктора
                .build();

        MigrationUtils.mergeBuilder(tool, mainRule)
                .addRule(stopRule("qlfctn2cmpttnkndrltn_t", "qualification_id", "competitionkind_id", "enrollmentcampaign_id", "studentcategory_id"))
                .addRule(stopRule("qualificationtofis_t", "eceducationlevelfis_id", "qualifications_id"))
                .addRule(stopRule("inspectionlevel_t", "educationlevelcode_p", "qualifications_id"))
                .merge("qualifications");
    }

    private void fixQualifications(DBTool tool) throws SQLException {

        // Для всех НПм, оказавшихся на уровне образования с кодом "38" или "39" выставляем квалификацию "Аспирантура".
        final Long aspId = getIdByCode(tool, "qualifications_t", "Асп");
        final Long level38 = getIdByCode(tool, "structureeducationlevels_t", "38");
        final Long level39 = getIdByCode(tool, "structureeducationlevels_t", "39");
        tool.executeUpdate("update educationlevels_t set qualification_id=?, inheritedqualification_id=? where leveltype_id in (?,?)", aspId, aspId, level38, level39);

        // Для НПм на уровне "41" выставляем "Кандидад наук"
        final Long phdQualificationId = getIdByCode(tool, "qualifications_t", "КН");
        final Long level41 = getIdByCode(tool, "structureeducationlevels_t", "41");
        tool.executeUpdate("update educationlevels_t set qualification_id=?, inheritedqualification_id=? where leveltype_id=?", phdQualificationId, phdQualificationId, level41);

        // Квалификация проф. образования "Кандидат наук". Меняем код "2013.96.PhD" -> "2009.00.PhD"
        tool.executeUpdate("update edu_c_pr_qual_t set code_p=? where code_p=?", "2009.00.PhD", "2013.96.PhD");
    }


    private void dropBaseAspModule(DBTool tool) throws SQLException {

        if (ApplicationRuntime.hasModule("aspirantbasermc"))
            throw new RuntimeException("Module 'aspirantbasermc' is not deleted");

        MigrationUtils.removeModuleFromVersion_s(tool, "aspirantbasermc");
    }
}