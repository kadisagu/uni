package ru.tandemservice.aspirantrmc.component.student.StudentPub;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.aspirantrmc.entity.CandidateExams;
import ru.tandemservice.aspirantrmc.entity.LibCardNumber;

public class Model extends ru.tandemservice.uni.component.student.StudentPub.Model
{

    private boolean aspirant = false;
    private String studentTabCaption;
    private String studentDataTabCaption;
    private String editStudentDataCaption;
    private String editStudentAdditionalDataCaption;
    private DynamicListDataSource<CandidateExams> dataSource;
    private boolean listActive;
    private LibCardNumber libCardNumber = new LibCardNumber();

    public DynamicListDataSource<CandidateExams> getDataSource()
    {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<CandidateExams> dataSource)
    {
        this.dataSource = dataSource;
    }

    public boolean isListActive()
    {
        return listActive;
    }

    public void setListActive(boolean listActive)
    {
        this.listActive = listActive;
    }

    public boolean isAccessibl()
    {
        return super.isAccessible() && !aspirant;
    }

    public boolean isAspirant()
    {
        return aspirant;
    }

    public void setAspirant(boolean aspirant)
    {
        this.aspirant = aspirant;
    }

    public String getStudentTabCaption()
    {
        return studentTabCaption;
    }

    public void setStudentTabCaption(String studentTabCaption)
    {
        this.studentTabCaption = studentTabCaption;
    }

    public String getStudentDataTabCaption()
    {
        return studentDataTabCaption;
    }

    public void setStudentDataTabCaption(String studentDataTabCaption)
    {
        this.studentDataTabCaption = studentDataTabCaption;
    }

    public String getEditStudentDataCaption()
    {
        return editStudentDataCaption;
    }

    public void setEditStudentDataCaption(String editStudentDataCaption)
    {
        this.editStudentDataCaption = editStudentDataCaption;
    }

    public String getEditStudentAdditionalDataCaption()
    {
        return editStudentAdditionalDataCaption;
    }

    public void setEditStudentAdditionalDataCaption(String editStudentAdditionalDataCaption)
    {
        this.editStudentAdditionalDataCaption = editStudentAdditionalDataCaption;
    }

    public String getLibCardNumber()
    {
        return libCardNumber.getNumber();
    }

    public void setLibCardNumber(LibCardNumber libCardNumber)
    {
        this.libCardNumber = libCardNumber;
    }

    public String getTableTitle()
    {
        return aspirant? "Дополнительные данные обучающегося" : "Дополнительные данные студента";
    }

    public String getMainTableCaption()
    {
        return aspirant? "Данные обучающегося" : "Данные студента";
    }
}
