package ru.tandemservice.aspirantrmc.component.catalog.aspirantPracticeType.AspirantPracticeTypeAddEdit;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.aspirantrmc.entity.catalog.AspirantPracticeType;

import java.util.List;

public class DAO extends DefaultCatalogAddEditDAO<AspirantPracticeType, Model> {
    public void prepare(Model model) {
        super.prepare(model);
        MQBuilder levelBuilder = new MQBuilder(AspirantPracticeType.ENTITY_CLASS, "a");
        List<AspirantPracticeType> list = levelBuilder.getResultList(getSession());
        model.setStructureList(HierarchyUtil.listHierarchyNodesWithParents(list, true));
    }
}
