package ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.Entity.ScientificResearchEntityUI;
import ru.tandemservice.aspirantrmc.entity.LeadOrganization2Research;

public class LeadOrgDSHandler extends DefaultSearchDataSourceHandler {
    private static final DQLOrderDescriptionRegistry order = new DQLOrderDescriptionRegistry(LeadOrganization2Research.class, "l2r");

    public LeadOrgDSHandler(String ownerId) {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context) {
        Long researchId = context.get(ScientificResearchEntityUI.RESEARCH_VALUE);

        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(LeadOrganization2Research.class, "l2r");
        dql.addColumn("l2r");

        dql.where(DQLExpressions.eq(
                DQLExpressions.property(LeadOrganization2Research.research().id().fromAlias("l2r")),
                DQLExpressions.value(researchId)
        ));
        order.applyOrder(dql, input.getEntityOrder());

        return DQLSelectOutputBuilder.get(input, dql, context.getSession()).pageable(true).build();
    }
}
