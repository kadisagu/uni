package ru.tandemservice.aspirantrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.aspirantrmc.entity.PracticeWork2Research;
import ru.tandemservice.aspirantrmc.entity.ScientificResearch;
import ru.tandemservice.aspirantrmc.entity.catalog.AspirantPracticeType;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь практики на производстве и научного исследования
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PracticeWork2ResearchGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.aspirantrmc.entity.PracticeWork2Research";
    public static final String ENTITY_NAME = "practiceWork2Research";
    public static final int VERSION_HASH = 2136289476;
    private static IEntityMeta ENTITY_META;

    public static final String L_RESEARCH = "research";
    public static final String L_EXTERNAL_ORG_UNIT = "externalOrgUnit";
    public static final String P_START_DATE = "startDate";
    public static final String P_END_DATE = "endDate";
    public static final String L_TYPE = "type";
    public static final String L_MARK = "mark";
    public static final String P_CURRENT = "current";

    private ScientificResearch _research;     // Научное исследование
    private ExternalOrgUnit _externalOrgUnit;     // Внешняя организация
    private Date _startDate;     // Дата начала практики
    private Date _endDate;     // Дата окончания практики
    private AspirantPracticeType _type;     // Название
    private SessionMarkGradeValueCatalogItem _mark;     // оценка
    private boolean _current = false;     // Текущая

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Научное исследование.
     */
    public ScientificResearch getResearch()
    {
        return _research;
    }

    /**
     * @param research Научное исследование.
     */
    public void setResearch(ScientificResearch research)
    {
        dirty(_research, research);
        _research = research;
    }

    /**
     * @return Внешняя организация.
     */
    public ExternalOrgUnit getExternalOrgUnit()
    {
        return _externalOrgUnit;
    }

    /**
     * @param externalOrgUnit Внешняя организация.
     */
    public void setExternalOrgUnit(ExternalOrgUnit externalOrgUnit)
    {
        dirty(_externalOrgUnit, externalOrgUnit);
        _externalOrgUnit = externalOrgUnit;
    }

    /**
     * @return Дата начала практики.
     */
    public Date getStartDate()
    {
        return _startDate;
    }

    /**
     * @param startDate Дата начала практики.
     */
    public void setStartDate(Date startDate)
    {
        dirty(_startDate, startDate);
        _startDate = startDate;
    }

    /**
     * @return Дата окончания практики.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания практики.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Название.
     */
    public AspirantPracticeType getType()
    {
        return _type;
    }

    /**
     * @param type Название.
     */
    public void setType(AspirantPracticeType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return оценка.
     */
    public SessionMarkGradeValueCatalogItem getMark()
    {
        return _mark;
    }

    /**
     * @param mark оценка.
     */
    public void setMark(SessionMarkGradeValueCatalogItem mark)
    {
        dirty(_mark, mark);
        _mark = mark;
    }

    /**
     * @return Текущая. Свойство не может быть null.
     */
    @NotNull
    public boolean isCurrent()
    {
        return _current;
    }

    /**
     * @param current Текущая. Свойство не может быть null.
     */
    public void setCurrent(boolean current)
    {
        dirty(_current, current);
        _current = current;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PracticeWork2ResearchGen)
        {
            setResearch(((PracticeWork2Research)another).getResearch());
            setExternalOrgUnit(((PracticeWork2Research)another).getExternalOrgUnit());
            setStartDate(((PracticeWork2Research)another).getStartDate());
            setEndDate(((PracticeWork2Research)another).getEndDate());
            setType(((PracticeWork2Research)another).getType());
            setMark(((PracticeWork2Research)another).getMark());
            setCurrent(((PracticeWork2Research)another).isCurrent());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PracticeWork2ResearchGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PracticeWork2Research.class;
        }

        public T newInstance()
        {
            return (T) new PracticeWork2Research();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "research":
                    return obj.getResearch();
                case "externalOrgUnit":
                    return obj.getExternalOrgUnit();
                case "startDate":
                    return obj.getStartDate();
                case "endDate":
                    return obj.getEndDate();
                case "type":
                    return obj.getType();
                case "mark":
                    return obj.getMark();
                case "current":
                    return obj.isCurrent();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "research":
                    obj.setResearch((ScientificResearch) value);
                    return;
                case "externalOrgUnit":
                    obj.setExternalOrgUnit((ExternalOrgUnit) value);
                    return;
                case "startDate":
                    obj.setStartDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "type":
                    obj.setType((AspirantPracticeType) value);
                    return;
                case "mark":
                    obj.setMark((SessionMarkGradeValueCatalogItem) value);
                    return;
                case "current":
                    obj.setCurrent((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "research":
                        return true;
                case "externalOrgUnit":
                        return true;
                case "startDate":
                        return true;
                case "endDate":
                        return true;
                case "type":
                        return true;
                case "mark":
                        return true;
                case "current":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "research":
                    return true;
                case "externalOrgUnit":
                    return true;
                case "startDate":
                    return true;
                case "endDate":
                    return true;
                case "type":
                    return true;
                case "mark":
                    return true;
                case "current":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "research":
                    return ScientificResearch.class;
                case "externalOrgUnit":
                    return ExternalOrgUnit.class;
                case "startDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "type":
                    return AspirantPracticeType.class;
                case "mark":
                    return SessionMarkGradeValueCatalogItem.class;
                case "current":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PracticeWork2Research> _dslPath = new Path<PracticeWork2Research>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PracticeWork2Research");
    }
            

    /**
     * @return Научное исследование.
     * @see ru.tandemservice.aspirantrmc.entity.PracticeWork2Research#getResearch()
     */
    public static ScientificResearch.Path<ScientificResearch> research()
    {
        return _dslPath.research();
    }

    /**
     * @return Внешняя организация.
     * @see ru.tandemservice.aspirantrmc.entity.PracticeWork2Research#getExternalOrgUnit()
     */
    public static ExternalOrgUnit.Path<ExternalOrgUnit> externalOrgUnit()
    {
        return _dslPath.externalOrgUnit();
    }

    /**
     * @return Дата начала практики.
     * @see ru.tandemservice.aspirantrmc.entity.PracticeWork2Research#getStartDate()
     */
    public static PropertyPath<Date> startDate()
    {
        return _dslPath.startDate();
    }

    /**
     * @return Дата окончания практики.
     * @see ru.tandemservice.aspirantrmc.entity.PracticeWork2Research#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.aspirantrmc.entity.PracticeWork2Research#getType()
     */
    public static AspirantPracticeType.Path<AspirantPracticeType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return оценка.
     * @see ru.tandemservice.aspirantrmc.entity.PracticeWork2Research#getMark()
     */
    public static SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem> mark()
    {
        return _dslPath.mark();
    }

    /**
     * @return Текущая. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.PracticeWork2Research#isCurrent()
     */
    public static PropertyPath<Boolean> current()
    {
        return _dslPath.current();
    }

    public static class Path<E extends PracticeWork2Research> extends EntityPath<E>
    {
        private ScientificResearch.Path<ScientificResearch> _research;
        private ExternalOrgUnit.Path<ExternalOrgUnit> _externalOrgUnit;
        private PropertyPath<Date> _startDate;
        private PropertyPath<Date> _endDate;
        private AspirantPracticeType.Path<AspirantPracticeType> _type;
        private SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem> _mark;
        private PropertyPath<Boolean> _current;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Научное исследование.
     * @see ru.tandemservice.aspirantrmc.entity.PracticeWork2Research#getResearch()
     */
        public ScientificResearch.Path<ScientificResearch> research()
        {
            if(_research == null )
                _research = new ScientificResearch.Path<ScientificResearch>(L_RESEARCH, this);
            return _research;
        }

    /**
     * @return Внешняя организация.
     * @see ru.tandemservice.aspirantrmc.entity.PracticeWork2Research#getExternalOrgUnit()
     */
        public ExternalOrgUnit.Path<ExternalOrgUnit> externalOrgUnit()
        {
            if(_externalOrgUnit == null )
                _externalOrgUnit = new ExternalOrgUnit.Path<ExternalOrgUnit>(L_EXTERNAL_ORG_UNIT, this);
            return _externalOrgUnit;
        }

    /**
     * @return Дата начала практики.
     * @see ru.tandemservice.aspirantrmc.entity.PracticeWork2Research#getStartDate()
     */
        public PropertyPath<Date> startDate()
        {
            if(_startDate == null )
                _startDate = new PropertyPath<Date>(PracticeWork2ResearchGen.P_START_DATE, this);
            return _startDate;
        }

    /**
     * @return Дата окончания практики.
     * @see ru.tandemservice.aspirantrmc.entity.PracticeWork2Research#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(PracticeWork2ResearchGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.aspirantrmc.entity.PracticeWork2Research#getType()
     */
        public AspirantPracticeType.Path<AspirantPracticeType> type()
        {
            if(_type == null )
                _type = new AspirantPracticeType.Path<AspirantPracticeType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return оценка.
     * @see ru.tandemservice.aspirantrmc.entity.PracticeWork2Research#getMark()
     */
        public SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem> mark()
        {
            if(_mark == null )
                _mark = new SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem>(L_MARK, this);
            return _mark;
        }

    /**
     * @return Текущая. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.PracticeWork2Research#isCurrent()
     */
        public PropertyPath<Boolean> current()
        {
            if(_current == null )
                _current = new PropertyPath<Boolean>(PracticeWork2ResearchGen.P_CURRENT, this);
            return _current;
        }

        public Class getEntityClass()
        {
            return PracticeWork2Research.class;
        }

        public String getEntityName()
        {
            return "practiceWork2Research";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
