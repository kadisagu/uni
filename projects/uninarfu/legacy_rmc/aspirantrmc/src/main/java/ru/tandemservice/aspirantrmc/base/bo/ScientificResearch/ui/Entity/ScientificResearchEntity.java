package ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.Entity;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.logic.LeadOrgDSHandler;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.logic.PracticeWorkDSHandler;
import ru.tandemservice.aspirantrmc.entity.LeadOrganization2Research;
import ru.tandemservice.aspirantrmc.entity.PracticeWork2Research;

@Configuration
public class ScientificResearchEntity extends BusinessComponentManager {
    public static final String PRACTICE_DS = "practiceDS";
    public static final String LEAD_ORG_DS = "leadOrgDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(PRACTICE_DS, practiceColumnListHandler(), practiceDSHandler()).numberOfRecords(5))
                .addDataSource(searchListDS(LEAD_ORG_DS, leadOrgColumnListHandler(), leadOrgDSHandler()).numberOfRecords(5))
                .create();
    }

    @Bean
    public ColumnListExtPoint practiceColumnListHandler() {
        return columnListExtPointBuilder(PRACTICE_DS)
                .addColumn(textColumn("title", PracticeWork2Research.type().title()).order().create())
                .addColumn(textColumn("mark", PracticeWork2Research.mark().printTitle()).order().create())
                .addColumn(publisherColumn("name", PracticeWork2Research.externalOrgUnit().title())
                                   .publisherLinkResolver(new SimplePublisherLinkResolver(PracticeWork2Research.externalOrgUnit().id()))
                                   .order()
                                   .create()
                )
                .addColumn(textColumn("address", PracticeWork2Research.externalOrgUnit().factAddress().title()).order().create())
                .addColumn(toggleColumn("current", PracticeWork2Research.current())
                                   .toggleOffListener("onClickSetPracticeNoCurrent").toggleOffListenerAlert(alert(PRACTICE_DS + ".setNoCurrent.alert", PracticeWork2Research.externalOrgUnit().title().s())).toggleOffLabel(PRACTICE_DS + ".noCurrentLbl")
                                   .toggleOnListener("onClickSetPracticeCurrent").toggleOnListenerAlert(alert(PRACTICE_DS + ".setCurrent.alert", PracticeWork2Research.externalOrgUnit().title().s())).toggleOnLabel(PRACTICE_DS + ".currentLbl")
                )
                .addColumn(actionColumn("edit", CommonDefines.ICON_EDIT, "onClickEditPractice")
                                   .permissionKey("aspirantrmc_scientificresearch_edit_practice").create()
                )
                .addColumn(actionColumn("delete", CommonDefines.ICON_DELETE, "onClickDeletePractice")
                                   .alert(FormattedMessage.with().template(PRACTICE_DS + ".delete.alert").create())
                                   .permissionKey("aspirantrmc_scientificresearch_delete_practice").create()
                )
                .create();
    }

    @Bean
    public ColumnListExtPoint leadOrgColumnListHandler() {
        return columnListExtPointBuilder(LEAD_ORG_DS)
                .addColumn(publisherColumn("name", LeadOrganization2Research.externalOrgUnit().title())
                                   .publisherLinkResolver(new SimplePublisherLinkResolver(LeadOrganization2Research.externalOrgUnit().id()))
                                   .order()
                                   .create()
                )
                .addColumn(textColumn("address", LeadOrganization2Research.externalOrgUnit().factAddress().title()).order().create())
                .addColumn(toggleColumn("current", LeadOrganization2Research.current())
                                   .toggleOffListener("onClickSetLeadOrgNoCurrent").toggleOffListenerAlert(alert(LEAD_ORG_DS + ".setNoCurrent.alert", PracticeWork2Research.externalOrgUnit().title().s())).toggleOffLabel(LEAD_ORG_DS + ".noCurrentLbl")
                                   .toggleOnListener("onClickSetLeadOrgCurrent").toggleOnListenerAlert(alert(LEAD_ORG_DS + ".setCurrent.alert", PracticeWork2Research.externalOrgUnit().title().s())).toggleOnLabel(LEAD_ORG_DS + ".currentLbl")
                )
                .addColumn(actionColumn("delete", CommonDefines.ICON_DELETE, "onClickDeleteLeadOrg")
                                   .alert(FormattedMessage.with().template(LEAD_ORG_DS + ".delete.alert").create())
                                   .permissionKey("aspirantrmc_scientificresearch_delete_leadorg").create()
                )
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> practiceDSHandler() {
        return new PracticeWorkDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> leadOrgDSHandler() {
        return new LeadOrgDSHandler(getName());
    }
}
