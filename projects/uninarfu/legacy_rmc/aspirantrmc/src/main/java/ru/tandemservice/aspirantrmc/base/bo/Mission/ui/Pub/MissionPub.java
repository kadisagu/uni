package ru.tandemservice.aspirantrmc.base.bo.Mission.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;

@Configuration
public class MissionPub extends BusinessComponentManager {
    public static final String TAB_PANEL = "missionTabPanel";

    public static final String BRIEF_INFO_TAB = "briefInfoTab";
    public static final String CONTACT_INFO_TAB = "contactInfoTab";


    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder().create();
    }

    @Bean
    public TabPanelExtPoint missionTabPanelExtPoint() {
        return tabPanelExtPointBuilder(TAB_PANEL)
                .addTab(htmlTab(BRIEF_INFO_TAB, "BriefInformation").permissionKey("aspirantrmc_viewMissionBriefInfoTab"))
                .addTab(htmlTab(CONTACT_INFO_TAB, "ContactInformation").permissionKey("aspirantrmc_viewMissionContactInfoTab"))
                .create();
    }
}
