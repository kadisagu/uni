package ru.tandemservice.aspirantrmc.base.bo.Mission.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.aspirantrmc.base.bo.Mission.ui.AddEdit.MissionAddEdit;
import ru.tandemservice.aspirantrmc.base.bo.Mission.ui.AddEdit.MissionAddEditUI;
import ru.tandemservice.aspirantrmc.entity.ScienceMission;

@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "missionHolder.id"),
        @Bind(key = MissionPubUI.SELECTED_TAB, binding = "selectedTab")
})
public class MissionPubUI extends UIPresenter {
    public static final String SELECTED_TAB = "selectedTab";

    private EntityHolder<ScienceMission> missionHolder = new EntityHolder<ScienceMission>();
    private String selectedTab;

    @Override
    public void onComponentRefresh() {
        missionHolder.refresh(ScienceMission.class);
    }

    public void onClickEditMission() {
        getActivationBuilder().asDesktopRoot(MissionAddEdit.class)
                .parameter(PublisherActivator.PUBLISHER_ID_KEY, getMission().getStudent().getId())
                .parameter(MissionAddEditUI.MISSION_ID_KEY, getMission().getId())
                .activate();
    }

    public ScienceMission getMission() {
        return missionHolder.getValue();
    }

    public EntityHolder<ScienceMission> getMissionHolder() {
        return missionHolder;
    }

    public void setMissionHolder(EntityHolder<ScienceMission> missionHolder) {
        this.missionHolder = missionHolder;
    }

    public String getSelectedTab() {
        return selectedTab;
    }

    public void setSelectedTab(String selectedTab) {
        this.selectedTab = selectedTab;
    }
}
