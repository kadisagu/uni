package ru.tandemservice.aspirantrmc.component.catalog.dissertationSoviet.DissertationSovietPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.aspirantrmc.entity.catalog.DissertationSoviet;

public class DAO extends DefaultCatalogPubDAO<DissertationSoviet, Model> implements IDAO {

    public void updateInUse(Model model, Long id) {
        DissertationSoviet dissertationSoviet = getNotNull(DissertationSoviet.class, id);
        if (dissertationSoviet.isUse())
            dissertationSoviet.setUse(false);
        else
            dissertationSoviet.setUse(true);
        save(dissertationSoviet);
    }

}
