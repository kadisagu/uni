package ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.Supervisors;

import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.UIAbstracts.AbstractParticipantUI;
import ru.tandemservice.aspirantrmc.entity.catalog.codes.ScientificResearchParticipantTypeCodes;

public class ScientificResearchSupervisorsUI extends AbstractParticipantUI {
    @Override
    protected String getParticipantType()
    {
        return ScientificResearchParticipantTypeCodes.SUPERVISOR;
    }
}
