package ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.UIAbstracts;

import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.IColumnListExtPointBuilder;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import org.tandemframework.shared.person.catalog.entity.ScienceDegree;
import ru.tandemservice.aspirantrmc.entity.ScienceParticipant;

import static org.tandemframework.caf.ui.config.BusinessComponentManagerBase.*;

public class AbstractParticipantHandlers {
    public static ColumnListExtPoint addColumnsToColumnList(IColumnListExtPointBuilder builder, String resourcePrefix, String participantTypeCode) {
        return builder
                .addColumn(publisherColumn("name", "entity." + ScienceParticipant.person().identityCard().fullFio())
                                   .publisherLinkResolver(new SimplePublisherLinkResolver("entity." + ScienceParticipant.person().id()))
                                   .create()
                )
                .addColumn(textColumn("degree", "degree." + ScienceDegree.title()))
                .addColumn(textColumn("academicStatus", "academicStatus"))
                .addColumn(toggleColumn("isMajor", "entity." + ScienceParticipant.isMajor())
                                   .toggleOffListener("onClickSetElementNoMajor").toggleOffListenerAlert(alert(resourcePrefix + ".setNoMajor.alert", ScienceParticipant.person().identityCard().fio().s())).toggleOffLabel(resourcePrefix + ".noMajorLbl")
                                   .toggleOnListener("onClickSetElementMajor").toggleOnListenerAlert(alert(resourcePrefix + ".setMajor.alert", ScienceParticipant.person().identityCard().fio().s())).toggleOnLabel(resourcePrefix + ".majorLbl")
                )
                .addColumn(actionColumn("edit", CommonDefines.ICON_EDIT, "onClickEditElement")
                                   .permissionKey("aspirantrmc_edit_" + participantTypeCode).create()
                )
                .addColumn(actionColumn("delete", CommonDefines.ICON_DELETE, "onClickDeleteElement")
                                   .alert(FormattedMessage.with().template(resourcePrefix + ".delete.alert").parameter(ScienceParticipant.person().identityCard().fio().s()).create())
                                   .permissionKey("aspirantrmc_delete_" + participantTypeCode).create()
                )
                .create();
    }
}
