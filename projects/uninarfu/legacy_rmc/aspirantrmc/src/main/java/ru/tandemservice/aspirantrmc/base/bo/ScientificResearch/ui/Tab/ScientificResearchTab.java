package ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.Tab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.Consultants.ScientificResearchConsultants;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.Entity.ScientificResearchEntity;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.Opponents.ScientificResearchOpponents;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.Reviewers.ScientificResearchReviewers;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.Supervisors.ScientificResearchSupervisors;

@Configuration
public class ScientificResearchTab extends BusinessComponentManager {
    //название таб листа:
    public static final String TAB_LIST = "researchesTabList";

    //название табов:
    public static final String ENTITY_TAB = "researchesTab";
    public static final String SUPERVISORS_TAB = "supervisorTab";
    public static final String CONSULTANTS_TAB = "consultantsTab";
    public static final String OPPONENTS_TAB = "opponentsTab";
    public static final String REVIEWERS_TAB = "reviewersTab";

    @Bean
    public TabPanelExtPoint tabPanelExtPoint() {
        return tabPanelExtPointBuilder(TAB_LIST)
                .addTab(componentTab(ENTITY_TAB, ScientificResearchEntity.class).permissionKey("view_aspirantrmc_entity_tab"))
                .addTab(componentTab(SUPERVISORS_TAB, ScientificResearchSupervisors.class).permissionKey("view_aspirantrmc_research_supervisors_tab"))
                .addTab(componentTab(CONSULTANTS_TAB, ScientificResearchConsultants.class).permissionKey("view_aspirantrmc_research_consultants_tab"))
                .addTab(componentTab(OPPONENTS_TAB, ScientificResearchOpponents.class).permissionKey("view_aspirantrmc_research_opponents_tab"))
                .addTab(componentTab(REVIEWERS_TAB, ScientificResearchReviewers.class).permissionKey("view_aspirantrmc_research_reviewers_tab"))
                .create();
    }

}
