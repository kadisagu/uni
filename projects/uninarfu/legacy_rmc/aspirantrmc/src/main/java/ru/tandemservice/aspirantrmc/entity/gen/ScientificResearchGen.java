package ru.tandemservice.aspirantrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.aspirantrmc.entity.ScientificResearch;
import ru.tandemservice.uni.entity.employee.Student;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Научное исследование
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ScientificResearchGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.aspirantrmc.entity.ScientificResearch";
    public static final String ENTITY_NAME = "scientificResearch";
    public static final int VERSION_HASH = 2035235296;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT = "student";
    public static final String P_TITLE = "title";
    public static final String P_APPROVE_DATE = "approveDate";

    private Student _student;     // Студент
    private String _title;     // Тема научного исследования
    private Date _approveDate;     // Дата утверждения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Тема научного исследования.
     */
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Тема научного исследования.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Дата утверждения.
     */
    public Date getApproveDate()
    {
        return _approveDate;
    }

    /**
     * @param approveDate Дата утверждения.
     */
    public void setApproveDate(Date approveDate)
    {
        dirty(_approveDate, approveDate);
        _approveDate = approveDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ScientificResearchGen)
        {
            setStudent(((ScientificResearch)another).getStudent());
            setTitle(((ScientificResearch)another).getTitle());
            setApproveDate(((ScientificResearch)another).getApproveDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ScientificResearchGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ScientificResearch.class;
        }

        public T newInstance()
        {
            return (T) new ScientificResearch();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "student":
                    return obj.getStudent();
                case "title":
                    return obj.getTitle();
                case "approveDate":
                    return obj.getApproveDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "approveDate":
                    obj.setApproveDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "student":
                        return true;
                case "title":
                        return true;
                case "approveDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "student":
                    return true;
                case "title":
                    return true;
                case "approveDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "student":
                    return Student.class;
                case "title":
                    return String.class;
                case "approveDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ScientificResearch> _dslPath = new Path<ScientificResearch>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ScientificResearch");
    }
            

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificResearch#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Тема научного исследования.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificResearch#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Дата утверждения.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificResearch#getApproveDate()
     */
    public static PropertyPath<Date> approveDate()
    {
        return _dslPath.approveDate();
    }

    public static class Path<E extends ScientificResearch> extends EntityPath<E>
    {
        private Student.Path<Student> _student;
        private PropertyPath<String> _title;
        private PropertyPath<Date> _approveDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificResearch#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Тема научного исследования.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificResearch#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(ScientificResearchGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Дата утверждения.
     * @see ru.tandemservice.aspirantrmc.entity.ScientificResearch#getApproveDate()
     */
        public PropertyPath<Date> approveDate()
        {
            if(_approveDate == null )
                _approveDate = new PropertyPath<Date>(ScientificResearchGen.P_APPROVE_DATE, this);
            return _approveDate;
        }

        public Class getEntityClass()
        {
            return ScientificResearch.class;
        }

        public String getEntityName()
        {
            return "scientificResearch";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
