package ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.OuterParticipant;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import org.tandemframework.shared.person.base.bo.Person.ui.Tab.PersonTab;

@Configuration
public class ScientificResearchOuterParticipant extends BusinessComponentManager {
    public static final String TAB_PANEL = "outerParticipantTabPanel";

    public static final String PERSON_TAB = "passportTab";
    public static final String WORK_INFO_TAB = "workInfoTab";

    @Bean
    public TabPanelExtPoint outerParticipantTabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(TAB_PANEL)
                .addTab(componentTab(PERSON_TAB, PersonTab.class)
                                .parameters("mvel:['personRoleModel':presenter.secureRoleContext]")
                                .permissionKey("aspirantrmc_viewOuterParticipantPersonTab"))
                .addTab(htmlTab(WORK_INFO_TAB, "WorkInfoTab")
                                .permissionKey("aspirantrmc_viewOuterParticipantWorkInfoTab"))
                .create();
    }

}
