package ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ui.AddEdit.ScientificWorksAddEdit;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ui.AddEdit.ScientificWorksAddEditUI;
import ru.tandemservice.aspirantrmc.entity.ScientificPublication;

/**
 * Created with IntelliJ IDEA.
 * User: ramaslov
 * Date: 31.01.13
 * Time: 20:23
 * To change this template use File | Settings | File Templates.
 */

@State({
        @Bind(
                key = ScientificWorksPubUI.PUBLISHER_ID_KEY_PUBLICATION,
                binding = ScientificWorksPubUI.PUBLISHER_ID_KEY_PUBLICATION, required = false)
})
public class ScientificWorksPubUI extends UIPresenter {

    public static final String PUBLISHER_ID_KEY_PUBLICATION = "publicationHolder.id";

    private EntityHolder<ScientificPublication> publicationHolder = new EntityHolder<ScientificPublication>();

    public void onClickEditPublication() {
        getActivationBuilder()
                .asRegion(ScientificWorksAddEdit.class)
                .parameter(ScientificWorksAddEditUI.PUBLISHER_ID_KEY_PUBLICATION, publicationHolder.getValue().getId())
                .activate();
    }

    public void onClickReturn() {
        deactivate();
    }

    public EntityHolder<ScientificPublication> getPublicationHolder() {
        return publicationHolder;
    }

    public void setPublicationHolder(EntityHolder<ScientificPublication> publicationHolder) {
        this.publicationHolder = publicationHolder;
    }

    public ScientificPublication getPublication() {
        return publicationHolder.getValue();
    }

    public void setPublicationHolder(ScientificPublication publication) {
        this.publicationHolder.setValue(publication);
    }
}
