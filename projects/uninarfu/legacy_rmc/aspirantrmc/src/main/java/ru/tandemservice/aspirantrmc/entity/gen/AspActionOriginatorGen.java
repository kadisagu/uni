package ru.tandemservice.aspirantrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.aspirantrmc.entity.AspActionOriginator;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Организатор мероприятия
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class AspActionOriginatorGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.aspirantrmc.entity.AspActionOriginator";
    public static final String ENTITY_NAME = "aspActionOriginator";
    public static final int VERSION_HASH = 1359153447;
    private static IEntityMeta ENTITY_META;

    public static final String P_NAME = "name";

    private String _name;     // Название организации

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Название организации.
     */
    @Length(max=255)
    public String getName()
    {
        return _name;
    }

    /**
     * @param name Название организации.
     */
    public void setName(String name)
    {
        dirty(_name, name);
        _name = name;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof AspActionOriginatorGen)
        {
            setName(((AspActionOriginator)another).getName());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends AspActionOriginatorGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) AspActionOriginator.class;
        }

        public T newInstance()
        {
            return (T) new AspActionOriginator();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "name":
                    return obj.getName();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "name":
                    obj.setName((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "name":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "name":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "name":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<AspActionOriginator> _dslPath = new Path<AspActionOriginator>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "AspActionOriginator");
    }
            

    /**
     * @return Название организации.
     * @see ru.tandemservice.aspirantrmc.entity.AspActionOriginator#getName()
     */
    public static PropertyPath<String> name()
    {
        return _dslPath.name();
    }

    public static class Path<E extends AspActionOriginator> extends EntityPath<E>
    {
        private PropertyPath<String> _name;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Название организации.
     * @see ru.tandemservice.aspirantrmc.entity.AspActionOriginator#getName()
     */
        public PropertyPath<String> name()
        {
            if(_name == null )
                _name = new PropertyPath<String>(AspActionOriginatorGen.P_NAME, this);
            return _name;
        }

        public Class getEntityClass()
        {
            return AspActionOriginator.class;
        }

        public String getEntityName()
        {
            return "aspActionOriginator";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
