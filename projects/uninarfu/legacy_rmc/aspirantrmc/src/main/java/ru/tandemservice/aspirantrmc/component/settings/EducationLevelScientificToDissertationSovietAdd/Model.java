package ru.tandemservice.aspirantrmc.component.settings.EducationLevelScientificToDissertationSovietAdd;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.aspirantrmc.entity.catalog.DissertationSoviet;
import ru.tandemservice.aspirantrmc.entity.catalog.StaffDissertationSoviet;

@Input({
        @Bind(key = "id", binding = "id"),
        @Bind(key = "idSoviet", binding = "idSoviet")
})
public class Model {

    private Long id;
    private Long idSoviet;
    private ISelectModel dissertationSovietModel;
    private ISelectModel staffDissertationSovietModel;
    private DissertationSoviet dissertationSoviet;
    private StaffDissertationSoviet chairman;
    private StaffDissertationSoviet viceChairman;
    private StaffDissertationSoviet secretary;


    public Long getIdSoviet() {
        return idSoviet;
    }

    public void setIdSoviet(Long idSoviet) {
        this.idSoviet = idSoviet;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ISelectModel getDissertationSovietModel() {
        return dissertationSovietModel;
    }

    public void setDissertationSovietModel(ISelectModel dissertationSovietModel) {
        this.dissertationSovietModel = dissertationSovietModel;
    }

    public ISelectModel getStaffDissertationSovietModel() {
        return staffDissertationSovietModel;
    }

    public void setStaffDissertationSovietModel(
            ISelectModel staffDissertationSovietModel)
    {
        this.staffDissertationSovietModel = staffDissertationSovietModel;
    }

    public DissertationSoviet getDissertationSoviet() {
        return dissertationSoviet;
    }

    public void setDissertationSoviet(DissertationSoviet dissertationSoviet) {
        this.dissertationSoviet = dissertationSoviet;
    }

    public StaffDissertationSoviet getChairman() {
        return chairman;
    }

    public void setChairman(StaffDissertationSoviet chairman) {
        this.chairman = chairman;
    }

    public StaffDissertationSoviet getViceChairman() {
        return viceChairman;
    }

    public void setViceChairman(StaffDissertationSoviet viceChairman) {
        this.viceChairman = viceChairman;
    }

    public StaffDissertationSoviet getSecretary() {
        return secretary;
    }

    public void setSecretary(StaffDissertationSoviet secretary) {
        this.secretary = secretary;
    }


}
