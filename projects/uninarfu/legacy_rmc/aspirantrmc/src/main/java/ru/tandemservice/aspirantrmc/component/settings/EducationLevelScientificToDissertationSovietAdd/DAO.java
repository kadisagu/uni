package ru.tandemservice.aspirantrmc.component.settings.EducationLevelScientificToDissertationSovietAdd;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.aspirantrmc.entity.RelDissertationSovietStaffPost;
import ru.tandemservice.aspirantrmc.entity.RelEducationLevelScientificDissertationSoviet;
import ru.tandemservice.aspirantrmc.entity.catalog.DissertationSoviet;
import ru.tandemservice.aspirantrmc.entity.catalog.PostDissertationSoviet;
import ru.tandemservice.aspirantrmc.entity.catalog.StaffDissertationSoviet;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelHigher;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        if (model.getIdSoviet() != null) {
            DissertationSoviet soviet = getNotNull(DissertationSoviet.class, model.getIdSoviet());
            model.setDissertationSoviet(soviet);
            onChange(model);
        }

        model.setDissertationSovietModel(new UniQueryFullCheckSelectModel(new String[]{"fullTitle"}) {
            @Override
            protected MQBuilder query(String alias, String filter) {
                MQBuilder builder = new MQBuilder(DissertationSoviet.ENTITY_CLASS, alias)
                        .addOrder(alias, DissertationSoviet.title());

                if (!StringUtils.isEmpty(filter))
                    builder.add(MQExpression.or(
                            MQExpression.like(alias, DissertationSoviet.title(), "%" + filter + "%"),
                            MQExpression.like(alias, DissertationSoviet.sovietCode(), "%" + filter + "%")
                    ));

                return builder;
            }
        });

        model.setStaffDissertationSovietModel(new UniQueryFullCheckSelectModel(new String[]{"fullFIO"}) {
            @Override
            protected MQBuilder query(String alias, String filter) {
                MQBuilder builder = new MQBuilder(StaffDissertationSoviet.ENTITY_CLASS, alias)
                        .addOrder(alias, StaffDissertationSoviet.lastName());

                if (!StringUtils.isEmpty(filter))
                    builder.add(MQExpression.or(
                            MQExpression.like(alias, StaffDissertationSoviet.lastName(), "%" + filter + "%"),
                            MQExpression.like(alias, StaffDissertationSoviet.firstName(), "%" + filter + "%"),
                            MQExpression.like(alias, StaffDissertationSoviet.middleName(), "%" + filter + "%")
                    ));

                return builder;
            }
        });

    }

    @Override
    public void update(Model model) {

        DissertationSoviet soviet = model.getDissertationSoviet();
        RelDissertationSovietStaffPost entity;
        if (model.getChairman() != null) {
            PostDissertationSoviet post = get(PostDissertationSoviet.class, PostDissertationSoviet.code(), "1");//председатель
            if (getRelDissertationSovietStaffPost(soviet, post) != null)//проверяем наличие такойже записи в БД
                entity = getRelDissertationSovietStaffPost(soviet, post);//если она есть, поднимаем ее
            else
                entity = new RelDissertationSovietStaffPost(); //иначе содаем новую
            entity.setDissertationSoviet(soviet);
            entity.setPostDissertationSoviet(post);
            entity.setStaffDissertationSoviet(model.getChairman());
            saveOrUpdate(entity);
        }
        if (model.getViceChairman() != null) {
            PostDissertationSoviet post = get(PostDissertationSoviet.class, PostDissertationSoviet.code(), "2");//зам. председателя
            if (getRelDissertationSovietStaffPost(soviet, post) != null)//проверяем наличие такойже записи в БД
                entity = getRelDissertationSovietStaffPost(soviet, post);//если она есть, поднимаем ее
            else
                entity = new RelDissertationSovietStaffPost();//иначе содаем новую
            entity.setDissertationSoviet(soviet);
            entity.setPostDissertationSoviet(post);
            entity.setStaffDissertationSoviet(model.getViceChairman());
            saveOrUpdate(entity);
        }
        if (model.getSecretary() != null) {
            PostDissertationSoviet post = get(PostDissertationSoviet.class, PostDissertationSoviet.code(), "3");//секретарь
            if (getRelDissertationSovietStaffPost(soviet, post) != null)//проверяем наличие такойже записи в БД
                entity = getRelDissertationSovietStaffPost(soviet, post);//если она есть, поднимаем ее
            else
                entity = new RelDissertationSovietStaffPost();//иначе содаем новую
            entity.setDissertationSoviet(soviet);
            entity.setPostDissertationSoviet(post);
            entity.setStaffDissertationSoviet(model.getSecretary());
            saveOrUpdate(entity);
        }
        if (model.getChairman() == null && model.getViceChairman() == null && model.getSecretary() == null) {
            entity = new RelDissertationSovietStaffPost();
            entity.setDissertationSoviet(soviet);
            saveOrUpdate(entity);
        }
        //сохраняем связь направление подготовки и диссертационного совета
        if (model.getIdSoviet() == null) {
            EducationLevelsHighSchool educationLevel = null;
            if (model.getId() != null) {
                educationLevel = getNotNull(EducationLevelsHighSchool.class, model.getId());

                RelEducationLevelScientificDissertationSoviet rel = new RelEducationLevelScientificDissertationSoviet();
                rel.setDissertationSoviet(soviet);
                rel.setEducationLevelScientific((EducationLevelHigher) educationLevel.getEducationLevel());

                saveOrUpdate(rel);
            }
        }
    }

    public RelDissertationSovietStaffPost getRelDissertationSovietStaffPost(DissertationSoviet soviet, PostDissertationSoviet post) {
        //поиск ФИО по должности и диссертационному совету
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(RelDissertationSovietStaffPost.class, "rel")
                .where(DQLExpressions.eq(DQLExpressions.property(RelDissertationSovietStaffPost.dissertationSoviet().fromAlias("rel")), DQLExpressions.value(soviet)))
                .where(DQLExpressions.eq(DQLExpressions.property(RelDissertationSovietStaffPost.postDissertationSoviet().fromAlias("rel")), DQLExpressions.value(post)));
        if (!getList(builder).isEmpty())
            return (RelDissertationSovietStaffPost) getList(builder).get(0);
        else
            return null;
    }

    public void onChange(Model model) {
        //заполнение формы уже существующими данными
        DissertationSoviet soviet = model.getDissertationSoviet();
        if (getRelDissertationSovietStaffPost(soviet, get(PostDissertationSoviet.class, PostDissertationSoviet.code(), "1")) != null)
            model.setChairman(getRelDissertationSovietStaffPost(soviet, get(PostDissertationSoviet.class, PostDissertationSoviet.code(), "1")).getStaffDissertationSoviet());
        else
            model.setChairman(null);
        if (getRelDissertationSovietStaffPost(soviet, get(PostDissertationSoviet.class, PostDissertationSoviet.code(), "2")) != null)
            model.setViceChairman(getRelDissertationSovietStaffPost(soviet, get(PostDissertationSoviet.class, PostDissertationSoviet.code(), "2")).getStaffDissertationSoviet());
        else
            model.setViceChairman(null);
        if (getRelDissertationSovietStaffPost(soviet, get(PostDissertationSoviet.class, PostDissertationSoviet.code(), "3")) != null)
            model.setSecretary(getRelDissertationSovietStaffPost(soviet, get(PostDissertationSoviet.class, PostDissertationSoviet.code(), "3")).getStaffDissertationSoviet());
        else
            model.setSecretary(null);
    }


}



