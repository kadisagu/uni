package ru.tandemservice.aspirantrmc.component.catalog.staffDissertationSoviet.StaffDissertationSovietAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.aspirantrmc.entity.catalog.StaffDissertationSoviet;

public interface IDAO extends IDefaultCatalogAddEditDAO<StaffDissertationSoviet, Model> {

}
