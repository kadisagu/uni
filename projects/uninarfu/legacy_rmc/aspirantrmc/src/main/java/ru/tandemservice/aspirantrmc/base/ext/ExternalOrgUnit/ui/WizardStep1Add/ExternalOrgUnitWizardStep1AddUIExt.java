package ru.tandemservice.aspirantrmc.base.ext.ExternalOrgUnit.ui.WizardStep1Add;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.column.RadioButtonColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.ctr.base.bo.ExternalOrgUnit.ExternalOrgUnitManager;
import org.tandemframework.shared.ctr.base.bo.ExternalOrgUnit.ui.WizardStep1Add.ExternalOrgUnitWizardStep1AddUI;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.aspirantrmc.base.ext.ExternalOrgUnit.utils.UICommonAddon;
import ru.tandemservice.uni.dao.IUniBaseDao;


public class ExternalOrgUnitWizardStep1AddUIExt extends UICommonAddon {
    ExternalOrgUnitWizardStep1AddUI parent = getPresenter();

    public ExternalOrgUnitWizardStep1AddUIExt(IUIPresenter presenter, String name, String componentId) {
        super(presenter, name, componentId);
    }

    public void onClickApply()
    {
        ExternalOrgUnit ou = ExternalOrgUnitManager.instance().dao().saveExternalOrgUnit(parent.getOu(), null, null);
        super.onClickApply(parent, ou);
    }

    public void onClickChoseExternalOrgUnit() {
        RadioButtonColumn rc = (RadioButtonColumn) getDataSource("similarExternalOrgUnitDS").getColumn("radio");
        if (rc.getSelectedEntity() != null) {
            ExternalOrgUnit externalOrgUnit = (ExternalOrgUnit) IUniBaseDao.instance.get().get(rc.getSelectedEntity().getId());
            super.onClickApply(parent, externalOrgUnit);
        }
        else {
            throw new ApplicationException("Выберите организацию");
        }
    }

    public DynamicListDataSource getDataSource(String name) {
        return (DynamicListDataSource) getPresenter().getConfig().getDataSource(name).getResult();
    }
}
