package ru.tandemservice.aspirantrmc.base.bo.PersonnelResource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.aspirantrmc.base.bo.PersonnelResource.logic.IPersonnelResourceDAO;
import ru.tandemservice.aspirantrmc.base.bo.PersonnelResource.logic.PersonnelResourceDAO;

@Configuration
public class PersonnelResourceManager extends BusinessObjectManager {

    public static PersonnelResourceManager instance()
    {
        return instance(PersonnelResourceManager.class);
    }

    @Bean
    public IPersonnelResourceDAO dao()
    {
        return new PersonnelResourceDAO();
    }


}
