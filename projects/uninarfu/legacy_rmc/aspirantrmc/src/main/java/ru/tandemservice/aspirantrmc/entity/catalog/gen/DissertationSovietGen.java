package ru.tandemservice.aspirantrmc.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.aspirantrmc.entity.catalog.DissertationSoviet;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Диссертационные советы
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DissertationSovietGen extends EntityBase
 implements INaturalIdentifiable<DissertationSovietGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.aspirantrmc.entity.catalog.DissertationSoviet";
    public static final String ENTITY_NAME = "dissertationSoviet";
    public static final int VERSION_HASH = 576523914;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_SOVIET_CODE = "sovietCode";
    public static final String P_TITLE = "title";
    public static final String P_USE = "use";

    private String _code;     // Системный код
    private String _sovietCode;     // Шифр диссертационных советов
    private String _title;     // Организация, на базе которой создан диссертационный совет
    private boolean _use;     // Использовать

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Шифр диссертационных советов. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getSovietCode()
    {
        return _sovietCode;
    }

    /**
     * @param sovietCode Шифр диссертационных советов. Свойство не может быть null.
     */
    public void setSovietCode(String sovietCode)
    {
        dirty(_sovietCode, sovietCode);
        _sovietCode = sovietCode;
    }

    /**
     * @return Организация, на базе которой создан диссертационный совет. Свойство не может быть null.
     */
    @NotNull
    @Length(max=1024)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Организация, на базе которой создан диссертационный совет. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Использовать. Свойство не может быть null.
     */
    @NotNull
    public boolean isUse()
    {
        return _use;
    }

    /**
     * @param use Использовать. Свойство не может быть null.
     */
    public void setUse(boolean use)
    {
        dirty(_use, use);
        _use = use;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DissertationSovietGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((DissertationSoviet)another).getCode());
            }
            setSovietCode(((DissertationSoviet)another).getSovietCode());
            setTitle(((DissertationSoviet)another).getTitle());
            setUse(((DissertationSoviet)another).isUse());
        }
    }

    public INaturalId<DissertationSovietGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<DissertationSovietGen>
    {
        private static final String PROXY_NAME = "DissertationSovietNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof DissertationSovietGen.NaturalId) ) return false;

            DissertationSovietGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DissertationSovietGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DissertationSoviet.class;
        }

        public T newInstance()
        {
            return (T) new DissertationSoviet();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "sovietCode":
                    return obj.getSovietCode();
                case "title":
                    return obj.getTitle();
                case "use":
                    return obj.isUse();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "sovietCode":
                    obj.setSovietCode((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "use":
                    obj.setUse((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "sovietCode":
                        return true;
                case "title":
                        return true;
                case "use":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "sovietCode":
                    return true;
                case "title":
                    return true;
                case "use":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "sovietCode":
                    return String.class;
                case "title":
                    return String.class;
                case "use":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DissertationSoviet> _dslPath = new Path<DissertationSoviet>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DissertationSoviet");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.aspirantrmc.entity.catalog.DissertationSoviet#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Шифр диссертационных советов. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.catalog.DissertationSoviet#getSovietCode()
     */
    public static PropertyPath<String> sovietCode()
    {
        return _dslPath.sovietCode();
    }

    /**
     * @return Организация, на базе которой создан диссертационный совет. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.catalog.DissertationSoviet#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Использовать. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.catalog.DissertationSoviet#isUse()
     */
    public static PropertyPath<Boolean> use()
    {
        return _dslPath.use();
    }

    public static class Path<E extends DissertationSoviet> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _sovietCode;
        private PropertyPath<String> _title;
        private PropertyPath<Boolean> _use;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.aspirantrmc.entity.catalog.DissertationSoviet#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(DissertationSovietGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Шифр диссертационных советов. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.catalog.DissertationSoviet#getSovietCode()
     */
        public PropertyPath<String> sovietCode()
        {
            if(_sovietCode == null )
                _sovietCode = new PropertyPath<String>(DissertationSovietGen.P_SOVIET_CODE, this);
            return _sovietCode;
        }

    /**
     * @return Организация, на базе которой создан диссертационный совет. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.catalog.DissertationSoviet#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(DissertationSovietGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Использовать. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.catalog.DissertationSoviet#isUse()
     */
        public PropertyPath<Boolean> use()
        {
            if(_use == null )
                _use = new PropertyPath<Boolean>(DissertationSovietGen.P_USE, this);
            return _use;
        }

        public Class getEntityClass()
        {
            return DissertationSoviet.class;
        }

        public String getEntityName()
        {
            return "dissertationSoviet";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
