package ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.Tab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.aspirantrmc.entity.ScientificResearch;
import ru.tandemservice.uni.entity.employee.Student;

@Input({
        @Bind(key = "selectedTab", binding = "selectedTab"),
        @Bind(key = "studentId", binding = "studentHolder.id")
})
public class ScientificResearchTabUI extends UIPresenter {
    private String selectedTab;
    private ScientificResearch research;

    private final EntityHolder<Student> studentHolder = new EntityHolder<>();

    public EntityHolder<Student> getStudentHolder()
    {
        return this.studentHolder;
    }

    public Student getStudent()
    {
        return this.studentHolder.getValue();
    }


    public ScientificResearch getResearch() {
        return research;
    }

    public void setResearch(ScientificResearch research) {
        this.research = research;
    }

    public String getSelectedTab() {
        return selectedTab;
    }

    public void setSelectedTab(String selectedTab) {
        this.selectedTab = selectedTab;
    }
}
