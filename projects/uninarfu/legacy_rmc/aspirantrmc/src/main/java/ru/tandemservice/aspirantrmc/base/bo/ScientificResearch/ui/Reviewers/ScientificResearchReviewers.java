package ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.Reviewers;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.logic.ParticipantDSHandler;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.UIAbstracts.AbstractParticipantHandlers;
import ru.tandemservice.aspirantrmc.entity.catalog.codes.ScientificResearchParticipantTypeCodes;

@Configuration
public class ScientificResearchReviewers extends BusinessComponentManager {

    public static final String LIST_DS = "listDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(LIST_DS, participantColumnListHandler(), participantDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint participantColumnListHandler() {
        return AbstractParticipantHandlers.addColumnsToColumnList(columnListExtPointBuilder(LIST_DS), LIST_DS, getParticipantTypeCode());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> participantDSHandler() {
        return new ParticipantDSHandler(getName(), getParticipantTypeCode());
    }

    public String getParticipantTypeCode() {
        return ScientificResearchParticipantTypeCodes.REVIEWER;
    }

}
