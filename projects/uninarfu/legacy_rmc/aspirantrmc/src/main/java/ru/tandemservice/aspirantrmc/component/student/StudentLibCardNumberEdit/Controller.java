/*$Id$*/
package ru.tandemservice.aspirantrmc.component.student.StudentLibCardNumberEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * @author DMITRY KNYAZEV
 * @since 16.09.2015
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = getModel(component);
        getDao().prepare(model);
    }

    public void onClickApply(IBusinessComponent component) {
        Model model = getModel(component);
        getDao().update(model);
        deactivate(component);
    }
}
