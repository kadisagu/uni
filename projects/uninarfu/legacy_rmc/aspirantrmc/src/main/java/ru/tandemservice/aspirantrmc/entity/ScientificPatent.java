package ru.tandemservice.aspirantrmc.entity;

import ru.tandemservice.aspirantrmc.entity.gen.ScientificPatentGen;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * Изобретение аспиранта/соискателя
 */
public class ScientificPatent extends ScientificPatentGen
{
    public ScientificPatent() {
    }

    public ScientificPatent(Student student) {
        this.setAuthor(student);
    }
}