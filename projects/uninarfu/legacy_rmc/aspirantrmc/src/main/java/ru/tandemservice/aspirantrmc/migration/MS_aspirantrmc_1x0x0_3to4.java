package ru.tandemservice.aspirantrmc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;

public class MS_aspirantrmc_1x0x0_3to4 extends IndependentMigrationScript {
    @Override
    public void run(DBTool tool) throws Exception {
        //Удалить поле Соискатель из справочника "Формы освоения"
        if (tool.tableExists("developform_t")) {
            tool.executeUpdate("delete from developform_t where code_p = 6");
        }
    }
}
