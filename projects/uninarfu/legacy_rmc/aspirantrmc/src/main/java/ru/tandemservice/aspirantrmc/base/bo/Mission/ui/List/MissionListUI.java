package ru.tandemservice.aspirantrmc.base.bo.Mission.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.aspirantrmc.base.bo.Mission.MissionManager;
import ru.tandemservice.aspirantrmc.base.bo.Mission.ui.AddEdit.MissionAddEdit;
import ru.tandemservice.aspirantrmc.base.bo.Mission.ui.AddEdit.MissionAddEditUI;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;

@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "studentHolder.id")
})
public class MissionListUI extends UIPresenter {
    public static String STUDENT_ID = "studentId";

    private EntityHolder<Student> studentHolder = new EntityHolder<Student>();

    @Override
    public void onComponentRefresh() {
        studentHolder.refresh(Student.class);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource ds) {
        if (ds.getName().equals(MissionList.LIST_DS)) {
            ds.put(STUDENT_ID, getStudent().getId());
            ds.put(MissionManager.FINANCING_DS, getSettings().get(MissionManager.FINANCING_DS));
            ds.put(MissionManager.STATUS_MISSION_DS, getSettings().get(MissionManager.STATUS_MISSION_DS));
        }
    }

    public void onEditEntry() {
        getActivationBuilder().asDesktopRoot(MissionAddEdit.class)
                .parameter(PublisherActivator.PUBLISHER_ID_KEY, getStudent().getId())
                .parameter(MissionAddEditUI.MISSION_ID_KEY, getListenerParameterAsLong())
                .activate();
    }

    public void onDeleteEntry() {
        UniDaoFacade.getCoreDao().delete(getListenerParameterAsLong());
        onComponentRefresh();
    }

    public void onClickAddMission() {
        getActivationBuilder().asDesktopRoot(MissionAddEdit.class)
                .parameter(PublisherActivator.PUBLISHER_ID_KEY, getStudent().getId())
                .activate();
    }


    public void onClickSearch() {
        getSettings().save();
    }

    public void onClickClear() {
        getSettings().clear();
    }


    public Student getStudent() {
        return studentHolder.getValue();
    }

    public EntityHolder<Student> getStudentHolder() {
        return studentHolder;
    }

    public void setStudentHolder(EntityHolder<Student> studentHolder) {
        this.studentHolder = studentHolder;
    }
}
