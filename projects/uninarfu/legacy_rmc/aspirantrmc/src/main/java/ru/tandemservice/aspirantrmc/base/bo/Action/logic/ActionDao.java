package ru.tandemservice.aspirantrmc.base.bo.Action.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.aspirantrmc.entity.AspActionOriginator;
import ru.tandemservice.uni.dao.UniBaseDao;

import java.util.List;

public class ActionDao extends UniBaseDao implements IActionDao {
    @Override
    public ListResult getAspOriginatorsListResult(String filter) {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(AspActionOriginator.class, "e");
        dql.column(AspActionOriginator.name().fromAlias("e").s());
        dql.setPredicate(DQLPredicateType.distinct);

        if (StringUtils.isNotBlank(filter)) {
            dql.where(DQLExpressions.like(
                    DQLFunctions.upper(DQLExpressions.property(AspActionOriginator.name().fromAlias("e"))),
                    DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))
            ));
        }
        List<AspActionOriginator> list = dql.createStatement(getSession()).setMaxResults(50).list();
        Number count = (Number) dql.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();

        return new ListResult(list, count == null ? 0L : count.intValue());
    }

    @Override
    public AspActionOriginator getOriginatorByTitle(String name) {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(AspActionOriginator.class, "o");
        dql.column("o");
        dql.where(DQLExpressions.eq(
                DQLExpressions.property(AspActionOriginator.name().fromAlias("o")),
                DQLExpressions.value(name)
        ));

        IDQLStatement dqlStatement = dql.createStatement(getSession());
        dqlStatement.setMaxResults(1);
        List<AspActionOriginator> originators = dqlStatement.list();
        if (originators.size() > 0) {
            return originators.get(0);
        }

        AspActionOriginator newOriginator = new AspActionOriginator();
        newOriginator.setName(name);
        getSession().saveOrUpdate(newOriginator);

        return newOriginator;
    }
}
