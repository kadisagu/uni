package ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ScientificWorksManager;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.logic.IScienetificWorksModifyDao;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.logic.PublicationListDSHandler;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ui.AddEdit.ScientificWorksAddEdit;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ui.AddEdit.ScientificWorksAddEditUI;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ui.PatentAddEdit.ScientificWorksPatentAddEdit;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ui.PatentAddEdit.ScientificWorksPatentAddEditUI;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ui.PatentPub.ScientificWorksPatentPub;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ui.PatentPub.ScientificWorksPatentPubUI;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ui.Pub.ScientificWorksPub;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ui.Pub.ScientificWorksPubUI;
import ru.tandemservice.aspirantrmc.entity.ScientificPatent;
import ru.tandemservice.aspirantrmc.entity.ScientificPublication;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * Created with IntelliJ IDEA.
 * User: ramaslov
 * Date: 27.01.13
 * Time: 12:34
 * To change this template use File | Settings | File Templates.
 */

@State({@org.tandemframework.core.component.Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "studentHolder.id", required = true)})
public class ScientificWorksListUI extends UIPresenter {

    private EntityHolder<Student> studentHolder = new EntityHolder<Student>();


    private BaseSearchListDataSource _contentDS;
    private BaseSearchListDataSource _contentPatentDS;

    @Override
    public void onComponentRefresh() {
        getStudentHolder().refresh(Student.class);
        IScienetificWorksModifyDao dao = ScientificWorksManager.instance().modifyDao();
    }

    public void onClickAddPublication() {
        getActivationBuilder()
                .asRegion(ScientificWorksAddEdit.class)
                .parameter(ScientificWorksAddEditUI.PUBLISHER_ID_KEY_STUDENT, studentHolder.getValue().getId())
                .activate();
    }

    public void onClickAddPatent() {
        getActivationBuilder()
                .asRegion(ScientificWorksPatentAddEdit.class)
                .parameter(ScientificWorksPatentAddEditUI.PUBLISHER_ID_KEY_STUDENT, studentHolder.getValue().getId())
                .activate();
    }

    public void onClickViewPublication() {
        ScientificPublication piblication = getContentDS().getRecordById(getListenerParameterAsLong());
        Long publicationId = piblication.getId();
        getActivationBuilder()
                .asRegion(ScientificWorksPub.class)
                .parameter(ScientificWorksPubUI.PUBLISHER_ID_KEY_PUBLICATION, publicationId)
                .activate();
    }

    public void onClickViewPatent() {
        ScientificPatent patent = getContentPatentDS().getRecordById(getListenerParameterAsLong());
        Long id = patent.getId();
        getActivationBuilder()
                .asRegion(ScientificWorksPatentPub.class)
                .parameter(ScientificWorksPatentPubUI.PUBLISHER_ID_KEY_PATENT, id)
                .activate();
    }

    public void onClickDeletePublication() {
        ScientificPublication piblication = ((ScientificPublication) getContentDS().getRecordById(getListenerParameterAsLong()));
        Long publicationId = piblication.getId();
        ScientificWorksManager.instance().modifyDao().deletePublication(publicationId);
    }

    public void onClickDeletePatent() {
        ScientificPatent patent = ((ScientificPatent) getContentPatentDS().getRecordById(getListenerParameterAsLong()));
        Long id = patent.getId();
        ScientificWorksManager.instance().modifyDao().deletePatent(id);
    }

    public void onClickEditPatent() {
        ScientificPatent patent = ((ScientificPatent) getContentPatentDS().getRecordById(getListenerParameterAsLong()));
        Long id = patent.getId();
        getActivationBuilder()
                .asRegion(ScientificWorksPatentAddEdit.class)
                .parameter(ScientificWorksPatentAddEditUI.PUBLISHER_ID_KEY_PATENT, id)
                .activate();
    }

    public void onClickEditPublication() {
        ScientificPublication piblication = ((ScientificPublication) getContentDS().getRecordById(getListenerParameterAsLong()));
        Long publicationId = piblication.getId();
        getActivationBuilder()
                .asRegion(ScientificWorksAddEdit.class)
                .parameter(ScientificWorksAddEditUI.PUBLISHER_ID_KEY_PUBLICATION, publicationId)
                .activate();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource iuiDataSource) {

        if (ScientificWorksList.PUBLICATION_LIST_DS.equals(iuiDataSource.getName())) {
            Long studentId = this.studentHolder.getValue().getId();
            iuiDataSource.put(PublicationListDSHandler.STUDENT_FILTER, studentId);
        }
        if (ScientificWorksList.PATENT_LIST_DS.equals(iuiDataSource.getName())) {
            Long studentId = this.studentHolder.getValue().getId();
            iuiDataSource.put(PublicationListDSHandler.STUDENT_FILTER, studentId);
        }
    }

    /*
    *-----------GETTERS & SETTERS----------------
     */

    public EntityHolder<Student> getStudentHolder() {
        return studentHolder;
    }

    public void setStudentHolder(EntityHolder<Student> studentHolder) {
        this.studentHolder = studentHolder;
    }

    public Student getStudent() {
        return studentHolder.getValue();
    }

    public BaseSearchListDataSource getContentDS() {
        if (_contentDS == null)
            _contentDS = (BaseSearchListDataSource) getConfig().getDataSource(ScientificWorksList.PUBLICATION_LIST_DS);
        return _contentDS;
    }

    public void setContentDS(BaseSearchListDataSource _contentDS) {
        this._contentDS = _contentDS;
    }

    public BaseSearchListDataSource getContentPatentDS() {
        if (_contentPatentDS == null)
            _contentPatentDS = (BaseSearchListDataSource) getConfig().getDataSource(ScientificWorksList.PATENT_LIST_DS);
        return _contentPatentDS;
    }

    public void setContentPatentDS(BaseSearchListDataSource _contentDS) {
        this._contentPatentDS = _contentDS;
    }
}
