package ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.PracticeAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.*;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.ctr.base.bo.ExternalOrgUnit.ui.WizardStep1Add.ExternalOrgUnitWizardStep1Add;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ScientificResearchManager;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.logic.IScientificResearchDao;
import ru.tandemservice.aspirantrmc.base.ext.ExternalOrgUnit.ui.WizardStep3Add.ExternalOrgUnitWizardStep3AddUIExt;
import ru.tandemservice.aspirantrmc.entity.PracticeWork2Research;
import ru.tandemservice.aspirantrmc.entity.ScientificResearch;
import ru.tandemservice.aspirantrmc.entity.catalog.AspirantPracticeType;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;

import java.util.Date;
import java.util.List;

@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "studentHolder.id", required = true)
})
@Input({
        @Bind(key = ExternalOrgUnitWizardStep3AddUIExt.EXTERNAL_ORG_UNIT, binding = "addedExternalOrgUnit"),
        @Bind(key = "practiceId", binding = "practiceId")
})
@Output({
        @Bind(key = ExternalOrgUnitWizardStep3AddUIExt.DEACTIVATE_MODE, binding = "deactivationMode")
})

//@Return({@org.tandemframework.core.component.Bind(key= ScientificResearchOuterParticipantAddEditUI.OUTHER_PARTICIPANT_ID, binding= ScientificResearchOuterParticipantAddEditUI.OUTHER_PARTICIPANT_ID)})
public class ScientificResearchPracticeAddEditUI extends UIPresenter {

    Long practiceId;
    boolean edit;
    PracticeWork2Research entity;


    private AspirantPracticeType practiceType;
    private Date beginDate;
    private Date endDate;
    private SessionMarkGradeValueCatalogItem mark;
    private boolean needOrgUnit;

    private EntityHolder<Student> studentHolder = new EntityHolder<Student>();
    private ScientificResearch research;
    private ExternalOrgUnit addedExternalOrgUnit;
    private ExternalOrgUnitWizardStep3AddUIExt.DeactivateMode deactivationMode = ExternalOrgUnitWizardStep3AddUIExt.DeactivateMode.DEACTIVATE;

    String test;

    private List<HSelectOption> _structureList;


    private ISelectModel marksModel;


    @Override
    public void onComponentRefresh() {
        edit = practiceId != null;
        if (edit) {
            entity = IUniBaseDao.instance.get().getUnique(PracticeWork2Research.class, "id", practiceId);
            research = entity.getResearch();
            beginDate = entity.getStartDate();
            endDate = entity.getEndDate();
            practiceType = entity.getType();
            mark = entity.getMark();
            if (entity.getExternalOrgUnit() != null)
                needOrgUnit = true;
        }
        List<AspirantPracticeType> list = IUniBaseDao.instance.get().getCatalogItemList(AspirantPracticeType.class);
        setStructureList(HierarchyUtil.listHierarchyNodesWithParents(list, true));
        setMarksModel(new LazySimpleSelectModel<>(SessionMarkGradeValueCatalogItem.class));

        getStudentHolder().refresh(Student.class);
        IScientificResearchDao dao = ScientificResearchManager.instance().modifyDao();

        research = dao.createResearch(getStudent());

        if (addedExternalOrgUnit != null) {
            if (edit) {
                //  PracticeWork2Research p2r = new PracticeWork2Research();
                entity.setResearch(research);
                entity.setExternalOrgUnit(addedExternalOrgUnit);
                entity.setCurrent(true);
                entity.setStartDate(getBeginDate());
                entity.setEndDate(getEndDate());
                entity.setMark(mark);
                entity.setType(practiceType);
                UniDaoFacade.getCoreDao().save(entity);

            }
            else {
                PracticeWork2Research p2r = new PracticeWork2Research();
                p2r.setResearch(research);
                p2r.setExternalOrgUnit(addedExternalOrgUnit);
                p2r.setCurrent(true);
                p2r.setStartDate(getBeginDate());
                p2r.setEndDate(getEndDate());
                p2r.setMark(mark);
                p2r.setType(practiceType);
                UniDaoFacade.getCoreDao().save(p2r);
            }


            addedExternalOrgUnit = null;

            deactivate();
        }
    }

    public void onClickApply() {
        if (needOrgUnit && (entity == null || entity.getExternalOrgUnit() == null))
            getActivationBuilder().asRegion(ExternalOrgUnitWizardStep1Add.class).activate();
        else {
            if (edit) {
                //  PracticeWork2Research p2r = new PracticeWork2Research();
                entity.setResearch(research);
                if (!needOrgUnit)
                    entity.setExternalOrgUnit(null);
                entity.setCurrent(true);
                entity.setStartDate(getBeginDate());
                entity.setEndDate(getEndDate());
                entity.setMark(mark);
                entity.setType(practiceType);
                UniDaoFacade.getCoreDao().save(entity);

            }
            else {
                PracticeWork2Research p2r = new PracticeWork2Research();
                p2r.setResearch(research);
                //   p2r.setExternalOrgUnit(addedExternalOrgUnit);
                p2r.setCurrent(true);
                p2r.setStartDate(getBeginDate());
                p2r.setEndDate(getEndDate());
                p2r.setMark(mark);
                p2r.setType(practiceType);
                UniDaoFacade.getCoreDao().save(p2r);
            }
            deactivate();
        }
//		outerParticipantToAdd.setAddress(addressModel.getAddress());
//        ScientificResearchManager.instance().modifyDao().saveOuterParticipant(outerParticipantToAdd);
//
//        setOutherParticipantId(outerParticipantToAdd.getId());
//
//        deactivate();
    }

    public AspirantPracticeType getPracticeType() {
        return practiceType;
    }

    public void setPracticeType(AspirantPracticeType practiceType) {
        this.practiceType = practiceType;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public SessionMarkGradeValueCatalogItem getMark() {
        return mark;
    }

    public void setMark(SessionMarkGradeValueCatalogItem mark) {
        this.mark = mark;
    }

    public ISelectModel getMarksModel() {
        return marksModel;
    }

    public void setMarksModel(ISelectModel marksModel) {
        this.marksModel = marksModel;
    }

    public boolean isEdit() {
        return edit;
    }

    public boolean isNeedOrgUnit() {
        return needOrgUnit;
    }

    public void setNeedOrgUnit(boolean needOrgUnit) {
        this.needOrgUnit = needOrgUnit;
    }

    public void setEdit(boolean edit) {
        this.edit = edit;
    }

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }

    public Student getStudent() {
        return studentHolder.getValue();
    }

    public EntityHolder<Student> getStudentHolder() {
        return studentHolder;
    }

    public void setStudentHolder(EntityHolder<Student> studentHolder) {
        this.studentHolder = studentHolder;
    }

    public ScientificResearch getResearch() {
        return research;
    }

    public void setResearch(ScientificResearch research) {
        this.research = research;
    }

    public ExternalOrgUnit getAddedExternalOrgUnit() {
        return addedExternalOrgUnit;
    }

    public void setAddedExternalOrgUnit(ExternalOrgUnit addedExternalOrgUnit) {
        this.addedExternalOrgUnit = addedExternalOrgUnit;
    }

    public ExternalOrgUnitWizardStep3AddUIExt.DeactivateMode getDeactivationMode() {
        return deactivationMode;
    }

    public void setDeactivationMode(ExternalOrgUnitWizardStep3AddUIExt.DeactivateMode deactivationMode) {
        this.deactivationMode = deactivationMode;
    }

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }

    public List<HSelectOption> getStructureList() {
        return _structureList;
    }

    public void setStructureList(List<HSelectOption> _structureList) {
        this._structureList = _structureList;
    }
}
