package ru.tandemservice.aspirantrmc.component.catalog.staffDissertationSoviet.StaffDissertationSovietPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;
import ru.tandemservice.aspirantrmc.entity.catalog.StaffDissertationSoviet;

public interface IDAO extends IDefaultCatalogPubDAO<StaffDissertationSoviet, Model> {

}
