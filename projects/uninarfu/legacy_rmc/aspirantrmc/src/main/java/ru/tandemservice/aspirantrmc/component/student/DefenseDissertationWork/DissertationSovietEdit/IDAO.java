package ru.tandemservice.aspirantrmc.component.student.DefenseDissertationWork.DissertationSovietEdit;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {

    public void onChange(Model model);
}
