package ru.tandemservice.aspirantrmc.base.ext.ExternalOrgUnit.utils;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.shared.ctr.base.bo.ExternalOrgUnit.ui.Pub.ExternalOrgUnitPub;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;

import java.util.HashMap;
import java.util.Map;

public class UICommonAddon extends UIAddon {
    public static final String DEACTIVATE_MODE = "deactivateMode";
    public static final String EXTERNAL_ORG_UNIT = "extOrgUnit";

    public static enum DeactivateMode {
        ACTIVATE_PUBLISHER,
        DEACTIVATE,
    }

    ;


    public UICommonAddon(IUIPresenter presenter, String name, String componentId) {
        super(presenter, name, componentId);
    }

    protected DeactivateMode getDeactivateMode() {
        return (DeactivateMode) this.getPresenter().getUserContext().getCurrentRegion().getBusinessData().get(DEACTIVATE_MODE);
    }

    /**
     * Здесь собрана общая логика по обработке нажатия клавиши "завершить".
     * Сохранение сущности нужно делать в вызывающем методе
     */
    public void onClickApply(UIPresenter parent, ExternalOrgUnit ou) {
        DeactivateMode deactivateMode = getDeactivateMode();
        if (deactivateMode != null && deactivateMode.equals(DeactivateMode.DEACTIVATE)) {
            // закрываем эту страницу
            Map<String, Object> params = new HashMap<String, Object>();
            params.put(EXTERNAL_ORG_UNIT, ou);
            parent.deactivate(params);

        }
        else {
            // переходим в карточку организации
            parent.getActivationBuilder().asDesktopRoot(ExternalOrgUnitPub.class).parameter(UIPresenter.PUBLISHER_ID, ou.getId()).activate();
        }
    }


}
