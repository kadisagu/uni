package ru.tandemservice.aspirantrmc.component.catalog.dissertationSoviet.DissertationSovietAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.aspirantrmc.entity.catalog.DissertationSoviet;

public interface IDAO extends IDefaultCatalogAddEditDAO<DissertationSoviet, Model> {

}
