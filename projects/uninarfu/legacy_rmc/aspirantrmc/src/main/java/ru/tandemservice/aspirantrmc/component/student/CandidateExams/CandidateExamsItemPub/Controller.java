package ru.tandemservice.aspirantrmc.component.student.CandidateExams.CandidateExamsItemPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.shared.commonbase.base.util.UniMap;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        ((IDAO) getDao()).prepare(model);
    }

    public void onClickEditItem(IBusinessComponent component) {
        activate(component, new ComponentActivator("ru.tandemservice.aspirantrmc.component.student.CandidateExams.CandidateExamsAddEdit", new UniMap()
                .add("candidateExamsId", ((Model) getModel(component)).getCandidateExamsId())
        ));
    }
}
