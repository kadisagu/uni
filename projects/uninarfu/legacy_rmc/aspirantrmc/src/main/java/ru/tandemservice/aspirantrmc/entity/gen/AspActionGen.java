package ru.tandemservice.aspirantrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import ru.tandemservice.aspirantrmc.entity.AspAction;
import ru.tandemservice.aspirantrmc.entity.AspActionOriginator;
import ru.tandemservice.aspirantrmc.entity.AspActionSupervisor;
import ru.tandemservice.aspirantrmc.entity.catalog.StatusAction;
import ru.tandemservice.aspirantrmc.entity.catalog.TypeAction;
import ru.tandemservice.aspirantrmc.entity.catalog.TypeParticipation;
import ru.tandemservice.uni.entity.employee.Student;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Мероприятия
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class AspActionGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.aspirantrmc.entity.AspAction";
    public static final String ENTITY_NAME = "aspAction";
    public static final int VERSION_HASH = -2075375000;
    private static IEntityMeta ENTITY_META;

    public static final String L_STATUS_ACTION = "statusAction";
    public static final String L_TYPE_ACTION = "typeAction";
    public static final String L_TYPE_PARTICIPATION = "typeParticipation";
    public static final String L_STUDENT = "student";
    public static final String P_NAME = "name";
    public static final String P_START_DATE = "startDate";
    public static final String P_END_DATE = "endDate";
    public static final String L_ORIGINATOR = "originator";
    public static final String L_SUPERVISOR = "supervisor";
    public static final String L_ADDRESS = "address";

    private StatusAction _statusAction;     // Статус мероприятия
    private TypeAction _typeAction;     // Тип мероприятия
    private TypeParticipation _typeParticipation;     // Вид участия
    private Student _student;     // Студент
    private String _name;     // Тема мероприятия
    private Date _startDate;     // Дата начала мероприятия
    private Date _endDate;     // Дата окончания мероприятия
    private AspActionOriginator _originator;     // Организатор мероприятия
    private AspActionSupervisor _supervisor;     // Руководитель аспиранта на мероприятии
    private AddressDetailed _address;     // Адрес проведения мероприятия

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Статус мероприятия. Свойство не может быть null.
     */
    @NotNull
    public StatusAction getStatusAction()
    {
        return _statusAction;
    }

    /**
     * @param statusAction Статус мероприятия. Свойство не может быть null.
     */
    public void setStatusAction(StatusAction statusAction)
    {
        dirty(_statusAction, statusAction);
        _statusAction = statusAction;
    }

    /**
     * @return Тип мероприятия. Свойство не может быть null.
     */
    @NotNull
    public TypeAction getTypeAction()
    {
        return _typeAction;
    }

    /**
     * @param typeAction Тип мероприятия. Свойство не может быть null.
     */
    public void setTypeAction(TypeAction typeAction)
    {
        dirty(_typeAction, typeAction);
        _typeAction = typeAction;
    }

    /**
     * @return Вид участия. Свойство не может быть null.
     */
    @NotNull
    public TypeParticipation getTypeParticipation()
    {
        return _typeParticipation;
    }

    /**
     * @param typeParticipation Вид участия. Свойство не может быть null.
     */
    public void setTypeParticipation(TypeParticipation typeParticipation)
    {
        dirty(_typeParticipation, typeParticipation);
        _typeParticipation = typeParticipation;
    }

    /**
     * @return Студент.
     */
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Тема мероприятия.
     */
    @Length(max=255)
    public String getName()
    {
        return _name;
    }

    /**
     * @param name Тема мероприятия.
     */
    public void setName(String name)
    {
        dirty(_name, name);
        _name = name;
    }

    /**
     * @return Дата начала мероприятия.
     */
    public Date getStartDate()
    {
        return _startDate;
    }

    /**
     * @param startDate Дата начала мероприятия.
     */
    public void setStartDate(Date startDate)
    {
        dirty(_startDate, startDate);
        _startDate = startDate;
    }

    /**
     * @return Дата окончания мероприятия.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания мероприятия.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Организатор мероприятия.
     */
    public AspActionOriginator getOriginator()
    {
        return _originator;
    }

    /**
     * @param originator Организатор мероприятия.
     */
    public void setOriginator(AspActionOriginator originator)
    {
        dirty(_originator, originator);
        _originator = originator;
    }

    /**
     * @return Руководитель аспиранта на мероприятии.
     */
    public AspActionSupervisor getSupervisor()
    {
        return _supervisor;
    }

    /**
     * @param supervisor Руководитель аспиранта на мероприятии.
     */
    public void setSupervisor(AspActionSupervisor supervisor)
    {
        dirty(_supervisor, supervisor);
        _supervisor = supervisor;
    }

    /**
     * @return Адрес проведения мероприятия.
     */
    public AddressDetailed getAddress()
    {
        return _address;
    }

    /**
     * @param address Адрес проведения мероприятия.
     */
    public void setAddress(AddressDetailed address)
    {
        dirty(_address, address);
        _address = address;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof AspActionGen)
        {
            setStatusAction(((AspAction)another).getStatusAction());
            setTypeAction(((AspAction)another).getTypeAction());
            setTypeParticipation(((AspAction)another).getTypeParticipation());
            setStudent(((AspAction)another).getStudent());
            setName(((AspAction)another).getName());
            setStartDate(((AspAction)another).getStartDate());
            setEndDate(((AspAction)another).getEndDate());
            setOriginator(((AspAction)another).getOriginator());
            setSupervisor(((AspAction)another).getSupervisor());
            setAddress(((AspAction)another).getAddress());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends AspActionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) AspAction.class;
        }

        public T newInstance()
        {
            return (T) new AspAction();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "statusAction":
                    return obj.getStatusAction();
                case "typeAction":
                    return obj.getTypeAction();
                case "typeParticipation":
                    return obj.getTypeParticipation();
                case "student":
                    return obj.getStudent();
                case "name":
                    return obj.getName();
                case "startDate":
                    return obj.getStartDate();
                case "endDate":
                    return obj.getEndDate();
                case "originator":
                    return obj.getOriginator();
                case "supervisor":
                    return obj.getSupervisor();
                case "address":
                    return obj.getAddress();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "statusAction":
                    obj.setStatusAction((StatusAction) value);
                    return;
                case "typeAction":
                    obj.setTypeAction((TypeAction) value);
                    return;
                case "typeParticipation":
                    obj.setTypeParticipation((TypeParticipation) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "name":
                    obj.setName((String) value);
                    return;
                case "startDate":
                    obj.setStartDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "originator":
                    obj.setOriginator((AspActionOriginator) value);
                    return;
                case "supervisor":
                    obj.setSupervisor((AspActionSupervisor) value);
                    return;
                case "address":
                    obj.setAddress((AddressDetailed) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "statusAction":
                        return true;
                case "typeAction":
                        return true;
                case "typeParticipation":
                        return true;
                case "student":
                        return true;
                case "name":
                        return true;
                case "startDate":
                        return true;
                case "endDate":
                        return true;
                case "originator":
                        return true;
                case "supervisor":
                        return true;
                case "address":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "statusAction":
                    return true;
                case "typeAction":
                    return true;
                case "typeParticipation":
                    return true;
                case "student":
                    return true;
                case "name":
                    return true;
                case "startDate":
                    return true;
                case "endDate":
                    return true;
                case "originator":
                    return true;
                case "supervisor":
                    return true;
                case "address":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "statusAction":
                    return StatusAction.class;
                case "typeAction":
                    return TypeAction.class;
                case "typeParticipation":
                    return TypeParticipation.class;
                case "student":
                    return Student.class;
                case "name":
                    return String.class;
                case "startDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "originator":
                    return AspActionOriginator.class;
                case "supervisor":
                    return AspActionSupervisor.class;
                case "address":
                    return AddressDetailed.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<AspAction> _dslPath = new Path<AspAction>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "AspAction");
    }
            

    /**
     * @return Статус мероприятия. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.AspAction#getStatusAction()
     */
    public static StatusAction.Path<StatusAction> statusAction()
    {
        return _dslPath.statusAction();
    }

    /**
     * @return Тип мероприятия. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.AspAction#getTypeAction()
     */
    public static TypeAction.Path<TypeAction> typeAction()
    {
        return _dslPath.typeAction();
    }

    /**
     * @return Вид участия. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.AspAction#getTypeParticipation()
     */
    public static TypeParticipation.Path<TypeParticipation> typeParticipation()
    {
        return _dslPath.typeParticipation();
    }

    /**
     * @return Студент.
     * @see ru.tandemservice.aspirantrmc.entity.AspAction#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Тема мероприятия.
     * @see ru.tandemservice.aspirantrmc.entity.AspAction#getName()
     */
    public static PropertyPath<String> name()
    {
        return _dslPath.name();
    }

    /**
     * @return Дата начала мероприятия.
     * @see ru.tandemservice.aspirantrmc.entity.AspAction#getStartDate()
     */
    public static PropertyPath<Date> startDate()
    {
        return _dslPath.startDate();
    }

    /**
     * @return Дата окончания мероприятия.
     * @see ru.tandemservice.aspirantrmc.entity.AspAction#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Организатор мероприятия.
     * @see ru.tandemservice.aspirantrmc.entity.AspAction#getOriginator()
     */
    public static AspActionOriginator.Path<AspActionOriginator> originator()
    {
        return _dslPath.originator();
    }

    /**
     * @return Руководитель аспиранта на мероприятии.
     * @see ru.tandemservice.aspirantrmc.entity.AspAction#getSupervisor()
     */
    public static AspActionSupervisor.Path<AspActionSupervisor> supervisor()
    {
        return _dslPath.supervisor();
    }

    /**
     * @return Адрес проведения мероприятия.
     * @see ru.tandemservice.aspirantrmc.entity.AspAction#getAddress()
     */
    public static AddressDetailed.Path<AddressDetailed> address()
    {
        return _dslPath.address();
    }

    public static class Path<E extends AspAction> extends EntityPath<E>
    {
        private StatusAction.Path<StatusAction> _statusAction;
        private TypeAction.Path<TypeAction> _typeAction;
        private TypeParticipation.Path<TypeParticipation> _typeParticipation;
        private Student.Path<Student> _student;
        private PropertyPath<String> _name;
        private PropertyPath<Date> _startDate;
        private PropertyPath<Date> _endDate;
        private AspActionOriginator.Path<AspActionOriginator> _originator;
        private AspActionSupervisor.Path<AspActionSupervisor> _supervisor;
        private AddressDetailed.Path<AddressDetailed> _address;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Статус мероприятия. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.AspAction#getStatusAction()
     */
        public StatusAction.Path<StatusAction> statusAction()
        {
            if(_statusAction == null )
                _statusAction = new StatusAction.Path<StatusAction>(L_STATUS_ACTION, this);
            return _statusAction;
        }

    /**
     * @return Тип мероприятия. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.AspAction#getTypeAction()
     */
        public TypeAction.Path<TypeAction> typeAction()
        {
            if(_typeAction == null )
                _typeAction = new TypeAction.Path<TypeAction>(L_TYPE_ACTION, this);
            return _typeAction;
        }

    /**
     * @return Вид участия. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.AspAction#getTypeParticipation()
     */
        public TypeParticipation.Path<TypeParticipation> typeParticipation()
        {
            if(_typeParticipation == null )
                _typeParticipation = new TypeParticipation.Path<TypeParticipation>(L_TYPE_PARTICIPATION, this);
            return _typeParticipation;
        }

    /**
     * @return Студент.
     * @see ru.tandemservice.aspirantrmc.entity.AspAction#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Тема мероприятия.
     * @see ru.tandemservice.aspirantrmc.entity.AspAction#getName()
     */
        public PropertyPath<String> name()
        {
            if(_name == null )
                _name = new PropertyPath<String>(AspActionGen.P_NAME, this);
            return _name;
        }

    /**
     * @return Дата начала мероприятия.
     * @see ru.tandemservice.aspirantrmc.entity.AspAction#getStartDate()
     */
        public PropertyPath<Date> startDate()
        {
            if(_startDate == null )
                _startDate = new PropertyPath<Date>(AspActionGen.P_START_DATE, this);
            return _startDate;
        }

    /**
     * @return Дата окончания мероприятия.
     * @see ru.tandemservice.aspirantrmc.entity.AspAction#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(AspActionGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Организатор мероприятия.
     * @see ru.tandemservice.aspirantrmc.entity.AspAction#getOriginator()
     */
        public AspActionOriginator.Path<AspActionOriginator> originator()
        {
            if(_originator == null )
                _originator = new AspActionOriginator.Path<AspActionOriginator>(L_ORIGINATOR, this);
            return _originator;
        }

    /**
     * @return Руководитель аспиранта на мероприятии.
     * @see ru.tandemservice.aspirantrmc.entity.AspAction#getSupervisor()
     */
        public AspActionSupervisor.Path<AspActionSupervisor> supervisor()
        {
            if(_supervisor == null )
                _supervisor = new AspActionSupervisor.Path<AspActionSupervisor>(L_SUPERVISOR, this);
            return _supervisor;
        }

    /**
     * @return Адрес проведения мероприятия.
     * @see ru.tandemservice.aspirantrmc.entity.AspAction#getAddress()
     */
        public AddressDetailed.Path<AddressDetailed> address()
        {
            if(_address == null )
                _address = new AddressDetailed.Path<AddressDetailed>(L_ADDRESS, this);
            return _address;
        }

        public Class getEntityClass()
        {
            return AspAction.class;
        }

        public String getEntityName()
        {
            return "aspAction";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
