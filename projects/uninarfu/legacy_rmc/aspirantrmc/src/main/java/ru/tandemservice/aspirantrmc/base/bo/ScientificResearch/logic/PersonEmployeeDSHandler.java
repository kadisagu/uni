package ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.person.base.entity.Person;

public class PersonEmployeeDSHandler extends DefaultComboDataSourceHandler {
    public PersonEmployeeDSHandler(String name) {
        super(name, Person.class, Person.identityCard().fullFio());
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep) {
        super.prepareConditions(ep);
        DQLSelectBuilder dqlIn = new DQLSelectBuilder();
        dqlIn.fromEntity(Employee.class, "emp");
        dqlIn.addColumn(Employee.person().id().fromAlias("emp").s());

        ep.dqlBuilder.where(DQLExpressions.in(
                DQLExpressions.property(Person.id().fromAlias("e")),
                dqlIn.getQuery()
        ));
    }
}
