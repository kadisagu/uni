package ru.tandemservice.aspirantrmc.base.bo.Action.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.aspirantrmc.entity.AspAction;

@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "actionHolder.id", required = true)
})
public class ActionPubUI extends UIPresenter {
    private EntityHolder<AspAction> actionHolder = new EntityHolder<AspAction>();

    @Override
    public void onComponentRefresh() {
        actionHolder.refresh(AspAction.class);
    }

    public AspAction getAction() {
        return actionHolder.getValue();
    }

    public EntityHolder<AspAction> getActionHolder() {
        return actionHolder;
    }

    public void setActionHolder(EntityHolder<AspAction> actionHolder) {
        this.actionHolder = actionHolder;
    }
}
