package ru.tandemservice.aspirantrmc.base.bo.Action.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.fias.base.bo.AddressBase.AddressBaseManager;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInline;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInlineConfig;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInlineUI;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import org.tandemframework.tapsupport.component.selection.SingleSelectTextModel;
import ru.tandemservice.aspirantrmc.base.bo.Action.ActionManager;
import ru.tandemservice.aspirantrmc.entity.AspAction;
import ru.tandemservice.aspirantrmc.entity.AspActionOriginator;
import ru.tandemservice.aspirantrmc.entity.AspActionSupervisor;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;

@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "studentHolder.id"),
        @Bind(key = "actionId", binding = "actionId")
})
public class ActionAddEditUI extends UIPresenter {
    private Long actionId;

    private EntityHolder<Student> studentHolder = new EntityHolder<Student>();
    private AspAction model;
    private AspActionSupervisor supervisor;
    private ISelectModel originatorSelectModel;
    private String originatorStr;
    Boolean datesEqual = false;

    //private org.tandemframework.shared.person.base.bo.Person.ui.AddressEdit.Model addressModel = new org.tandemframework.shared.person.base.bo.Person.ui.AddressEdit.Model();
    //private org.tandemframework.shared.person.base.bo.Person.ui.AddressEdit.Model addressSupModel = new org.tandemframework.shared.person.base.bo.Person.ui.AddressEdit.Model();

    private AddressBaseEditInlineConfig getAddressConfig(String fieldSetTitle, Long entityId, String propertyPath) {
        AddressBaseEditInlineConfig addressConfig = new AddressBaseEditInlineConfig();
        addressConfig.setEntityId(entityId);
        addressConfig.setDetailedOnly(true);
        addressConfig.setDetailLevel(4);
        addressConfig.setOffice(true);
        addressConfig.setInline(true);
        addressConfig.setFieldSetTitle(fieldSetTitle);
        addressConfig.setAddressProperty(propertyPath);

        return addressConfig;
    }

    @Override
    public void onComponentRefresh() {
        studentHolder.refresh(Student.class);
        if (actionId != null) {
            model = UniDaoFacade.getCoreDao().get(AspAction.class, actionId);
            supervisor = model.getSupervisor();
            if (model.getOriginator() != null)
                originatorStr = model.getOriginator().getName();
        }
        else {
            model = new AspAction();
            supervisor = new AspActionSupervisor();

            model.setStudent(studentHolder.getValue());
            model.setSupervisor(supervisor);
        }

        this._uiActivation.asRegion(AddressBaseEditInline.class, "addressRegion").parameter("addressEditUIConfig", getAddressConfig("Адрес проведения мероприятия", model.getId(), AspAction.address().s())).activate();
        this._uiActivation.asRegion(AddressBaseEditInline.class, "addressSupRegion").parameter("addressEditUIConfig", getAddressConfig("Организатор мероприятия", supervisor.getId(), AspActionSupervisor.address().s())).activate();

        //инициализаци select-моделей:
        originatorSelectModel = new SingleSelectTextModel() {
            @Override
            public ListResult findValues(String filter) {
                return ActionManager.instance().modifyDao().getAspOriginatorsListResult(filter);
            }
        };
    }


    public void onClickApply() {
        if (originatorStr != null) {
            AspActionOriginator originator = ActionManager.instance().modifyDao().getOriginatorByTitle(originatorStr);
            model.setOriginator(originator);
        }

        AddressBaseEditInlineUI addressEditUI = (AddressBaseEditInlineUI) this._uiSupport.getChildUI("addressRegion");
        AddressDetailed address = (AddressDetailed) addressEditUI.getResult();
        AddressBaseManager.instance().dao().saveOrUpdateAddressWithoutEntityUpdate(model, address, AspAction.address().s());

        addressEditUI = (AddressBaseEditInlineUI) this._uiSupport.getChildUI("addressSupRegion");
        address = (AddressDetailed) addressEditUI.getResult();
        AddressBaseManager.instance().dao().saveOrUpdateAddressWithoutEntityUpdate(supervisor, address, AspActionSupervisor.address().s());

        UniDaoFacade.getCoreDao().saveOrUpdate(supervisor);
        UniDaoFacade.getCoreDao().saveOrUpdate(model);
        deactivate();
    }

    public void updateDatesEqualFlag() {
        if (datesEqual && model.getStartDate() != null) {
            model.setEndDate(model.getStartDate());
        }
    }

    public Long getActionId() {
        return actionId;
    }

    public void setActionId(Long actionId) {
        this.actionId = actionId;
    }

    public AspAction getModel() {
        return model;
    }

    public void setModel(AspAction model) {
        this.model = model;
    }

    public AspActionSupervisor getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(AspActionSupervisor supervisor) {
        this.supervisor = supervisor;
    }

    public Student getStudent() {
        return studentHolder.getValue();
    }

    public EntityHolder<Student> getStudentHolder() {
        return studentHolder;
    }

    public void setStudentHolder(EntityHolder<Student> studentHolder) {
        this.studentHolder = studentHolder;
    }

    public ISelectModel getOriginatorSelectModel() {
        return originatorSelectModel;
    }

    public void setOriginatorSelectModel(ISelectModel originatorSelectModel) {
        this.originatorSelectModel = originatorSelectModel;
    }

    public String getOriginatorStr() {
        return originatorStr;
    }

    public void setOriginatorStr(String originatorStr) {
        this.originatorStr = originatorStr;
    }

    public Boolean getDatesEqual() {
        return datesEqual;
    }

    public void setDatesEqual(Boolean datesEqual) {
        this.datesEqual = datesEqual;
    }
}
