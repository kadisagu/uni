package ru.tandemservice.aspirantrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_aspirantrmc_1x0x0_6to7 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.13"),
                        new ScriptDependency("org.tandemframework.shared", "1.3.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.3.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность practiceWork2Research

        // создано свойство startDate
        {
            // создать колонку
            tool.createColumn("practicework2research_t", new DBColumn("startdate_p", DBType.TIMESTAMP));

        }

        // создано свойство endDate
        {
            // создать колонку
            tool.createColumn("practicework2research_t", new DBColumn("enddate_p", DBType.TIMESTAMP));

        }

        // создано свойство type
        {
            // создать колонку
            tool.createColumn("practicework2research_t", new DBColumn("type_id", DBType.LONG));

        }

        // создано свойство mark
        {
            // создать колонку
            tool.createColumn("practicework2research_t", new DBColumn("mark_id", DBType.LONG));

        }


    }
}