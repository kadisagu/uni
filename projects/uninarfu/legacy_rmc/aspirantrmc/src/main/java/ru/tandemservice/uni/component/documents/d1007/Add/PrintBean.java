package ru.tandemservice.uni.component.documents.d1007.Add;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.uni.component.documents.DocumentPrintBean;
import ru.tandemservice.uni.util.NumberConvertingUtil;


public class PrintBean extends DocumentPrintBean<Model> {
    @Override
    protected RtfInjectModifier createInjectModifier(Model model)
    {
        RtfInjectModifier injectModifier = super.createInjectModifier(model);

        injectModifier
                .put("date", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getFormingDate()))
                .put("num", Integer.toString(model.getNumber()))
                .put("on", model.getStudent().getPerson().isMale() ? "он" : "она")
                .put("FIO", model.getStudentTitleStr())
                .put("course", NumberConvertingUtil.getSpelledNumeric(model.getStudent().getCourse().getIntValue(), false, NumberConvertingUtil.GENETIVE_CASE))
                .put("vozm", model.getStudent().getCompensationType().getShortTitle())
                .put("spec", model.getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getOkso() + " " + model.getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getTitle())
                .put("kafedra", model.getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getOrgUnit().getGenitiveCaseTitle())
                .put("date_begin", model.getDateBegin() == null ? "" : RussianDateFormatUtils.getDateFormattedWithMonthName(model.getDateBegin()))
                .put("date_num_order", model.getOrderInfo() == null ? "" : model.getOrderInfo())
                .put("comment", model.getComment() == null ? "" : model.getComment())
                .put("place", model.getPurpose() == null ? "" : model.getPurpose())
                .put("form", model.getStudent().getEducationOrgUnit().getDevelopForm().getTitle().toLowerCase())
                .put("date_end", model.getDateEnd() == null ? "" : RussianDateFormatUtils.getDateFormattedWithMonthName(model.getDateEnd()));

        return injectModifier;
    }
}
