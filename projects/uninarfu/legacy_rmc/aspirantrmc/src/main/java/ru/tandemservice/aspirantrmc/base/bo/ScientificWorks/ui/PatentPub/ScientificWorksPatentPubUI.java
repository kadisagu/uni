package ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ui.PatentPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ui.PatentAddEdit.ScientificWorksPatentAddEdit;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ui.PatentAddEdit.ScientificWorksPatentAddEditUI;
import ru.tandemservice.aspirantrmc.entity.ScientificPatent;

/**
 * Created with IntelliJ IDEA.
 * User: ramaslov
 * Date: 31.01.13
 * Time: 20:23
 * To change this template use File | Settings | File Templates.
 */

@State({
        @Bind(
                key = ScientificWorksPatentPubUI.PUBLISHER_ID_KEY_PATENT,
                binding = ScientificWorksPatentPubUI.PUBLISHER_ID_KEY_PATENT, required = false)
})
public class ScientificWorksPatentPubUI extends UIPresenter {

    public static final String PUBLISHER_ID_KEY_PATENT = "patentHolder.id";

    private EntityHolder<ScientificPatent> patentHolder = new EntityHolder<ScientificPatent>();

    public void onClickEditPatent() {
        getActivationBuilder()
                .asRegion(ScientificWorksPatentAddEdit.class)
                .parameter(ScientificWorksPatentAddEditUI.PUBLISHER_ID_KEY_PATENT, patentHolder.getValue().getId())
                .activate();
    }

    public void onClickReturn() {
        deactivate();
    }

    public EntityHolder<ScientificPatent> getPatentHolder() {
        return patentHolder;
    }

    public void setPatentHolder(EntityHolder<ScientificPatent> patentHolder) {
        this.patentHolder = patentHolder;
    }

    public ScientificPatent getPatent() {
        return patentHolder.getValue();
    }

    public void setPatent(ScientificPatent patent) {
        this.patentHolder.setValue(patent);
    }

}
