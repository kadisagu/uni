package ru.tandemservice.movestudentrmc.component.catalog.osspPgo.OsspPgoPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;
import ru.tandemservice.movestudentrmc.entity.catalog.OsspPgo;

public interface IDAO extends IDefaultCatalogPubDAO<OsspPgo, Model> {
    void updatePrintable(Model model, Long id);
}
