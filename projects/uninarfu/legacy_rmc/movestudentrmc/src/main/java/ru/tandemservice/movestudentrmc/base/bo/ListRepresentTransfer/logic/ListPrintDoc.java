package ru.tandemservice.movestudentrmc.base.bo.ListRepresentTransfer.logic;

import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudentrmc.docord.IListPrintDoc;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListRepresentTransfer;
import ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents;
import ru.tandemservice.movestudentrmc.entity.RelListRepresentStudentsOldData;
import ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.*;

public class ListPrintDoc implements IListPrintDoc
{

    private Map<Student, OrderParagraphInfo> hashMap = new HashMap<>();

    public static final String TEMPLATE_HEADER = "list.rep.transfer.header";
    public static final String TEMPLATE_PARAG = "list.rep.transfer.parag";
//	public  final String TEMPLATE_ADDON = "list.rep.transfer.addon";

    public RtfDocument creationPrintDocOrder(List<? extends ListRepresent> listOfRepresents) {
        PrintTemplate templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(PrintTemplate.class, TEMPLATE_HEADER);
        RtfDocument templateHeader = new RtfReader().read(templateDocument.getContent());

        templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(PrintTemplate.class, TEMPLATE_PARAG);
        RtfDocument templateParag = new RtfReader().read(templateDocument.getContent());

        Map<OrgUnit, Map<String, List<Object[]>>> dataMap = getStudentData(listOfRepresents);
        //сортировка формирующих по алфавиту
        List<OrgUnit> orgUnitList = new ArrayList<>(dataMap.keySet());
        Collections.sort(orgUnitList, new EntityComparator<>(new EntityOrder(OrgUnit.title())));

        RtfDocument result = templateHeader.getClone();
        result.setSettings(templateParag.getSettings());
        IRtfElement paragTemp = UniRtfUtil.findElement(result.getElementList(), "parag");
        new RtfInjectModifier().put("representTitle", listOfRepresents.get(0).getRepresentationType().getTitle()).modify(result);

        RtfDocument res = RtfBean.getElementFactory().createRtfDocument();
        res.setHeader(templateParag.getHeader());
        res.setSettings(templateParag.getSettings());
        //------------------------------------------------------------------------------------
        //res.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.TAB));
        //------------------------------------------------------------------------------------

        int formCounter = 0;

        for (OrgUnit formOU : orgUnitList) {
            Map<String, List<Object[]>> formMap = dataMap.get(formOU);

            formCounter++;

            res.getElementList().add(RtfBean.getElementFactory().createRtfText(String.valueOf(formCounter).trim() + "."));
            res.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.TAB));
            insertRow(res, "По " + formOU.getDativeCaseTitle());
            int counter = 0;
            for (List<Object[]> studentList : formMap.values()) {
                counter++;
                RtfDocument par = getParagraph(templateParag, formCounter, counter, studentList);
                res.getElementList().addAll(par.getElementList());
            }
        }

        //основания
//		res.getElementList().add( RtfBean.getElementFactory().createRtfControl(IRtfData.PAR) );
        insertRow(res, "Основание: " + UtilPrintSupport.getBasementTitles(listOfRepresents));
        result.getElementList().addAll(result.getElementList().indexOf(paragTemp), res.getElementList());

        // Ivan Anishchenko <ivan.anishchenko@citeck.ru>
        // лишний лист (использовался при печати приложения)
        //result.getElementList().add( RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE) );

        SharedRtfUtil.removeParagraphsWithTagsRecursive(result, Collections.singletonList("parag"), false, false);
        return result;
    }

    private RtfDocument getParagraph(RtfDocument template, int formCounter, int counter, List<Object[]> studentList) {
        RtfDocument result = template.getClone();
        RelListRepresentStudentsOldData oldData = (RelListRepresentStudentsOldData) studentList.get(0)[2];
        ListRepresentTransfer listRepresent = (ListRepresentTransfer) studentList.get(0)[1];

        Map<Student, RelListRepresentStudentsOldData> map = new HashMap<>();
        List<Student> students = new ArrayList<>();
        for (Object[] o : studentList) {
            Student s = (Student) o[0];
            hashMap.put(s, new OrderParagraphInfo(formCounter, counter));
            students.add(s);
            map.put(s, (RelListRepresentStudentsOldData) o[2]);
        }
        RtfInjectModifier im = new RtfInjectModifier();

        im.put("i", String.valueOf(formCounter).trim());
        im.put("j", String.valueOf(counter).trim());

        injectModifier(im, listRepresent, oldData);
        Collections.sort(students,
                new EntityComparator<>(new EntityOrder(Student.person().identityCard().fullFio())));
        int n = 1;
        for (Student student : students) {

        	//------------------------------------------------------------------------------------
        	hashMap.get(student).setStudentNumber(n);	//Порядковый номер студента для выписки
        	//------------------------------------------------------------------------------------
            result.getElementList().addAll(new RtfString().append(n++ + ")").append(IRtfData.TAB)
                                                   .append(student.getPerson().getFullFio())
                                                   .append(map.get(student).getOldCompensationType().isBudget() ? "" : " (по договору)")
                                                   .par().toList());
        }
        result.addElement(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));

        im.modify(result);
        return result;
    }

    public static void injectModifier(RtfInjectModifier im, ListRepresentTransfer listRepresent, RelListRepresentStudentsOldData oldData) {

        EducationOrgUnit educationOrgUnit = listRepresent.getEducationOrgUnit() == null ? listRepresent.getGroupNew().getEducationOrgUnit() : listRepresent.getEducationOrgUnit();

        boolean isFirst = true;
        
        //---------------------------------------------------------------------------------------------------------
        //Утвердить ИУП
        im.put("transfer", listRepresent.isIndividualPlan() ? "Утвердить индивидуальный учебный план и перевести" : "Перевести");        	
        //---------------------------------------------------------------------------------------------------------

        im.put("dateTransfer", UtilPrintSupport.getDateFormatterWithMonthString(listRepresent.getDateTransfer()));
        if (listRepresent.getCourseNew() != null && !oldData.getOldCourse().equals(listRepresent.getCourseNew())) {
            im.put("courseNew", " на " + listRepresent.getCourseNew().getTitle() + " курс");
            isFirst = false;
        }
        else
            im.put("courseNew", "");

        if (listRepresent.getGroupNew() != null) {
            im.put("groupNew", " в " + listRepresent.getGroupNew().getTitle() + " группу");
            isFirst = false;
        }
        else if (oldData.getOldGroup() != null) {
            im.put("groupNew", " в " + oldData.getOldGroup().getTitle() + " группу");
            isFirst = false;
        }
        else
            im.put("groupNew", "");

        if (!oldData.getOldEducationOrgUnit().getDevelopForm().equals(educationOrgUnit.getDevelopForm())) {
            im.put("developFormNew", " " + UtilPrintSupport.getDevelopFormGen(educationOrgUnit.getDevelopForm()) + " формы");
            isFirst = false;
        }
        else
            im.put("developFormNew", "");
        if (!oldData.getOldEducationOrgUnit().getFormativeOrgUnit().equals(educationOrgUnit.getFormativeOrgUnit())) {
            im.put("formOrgUnitNew", " в " + educationOrgUnit.getFormativeOrgUnit().getAccusativeCaseTitle());
            isFirst = false;
        }
        else
            im.put("formOrgUnitNew", "");
        if (oldData.getOldEducationOrgUnit().getEducationLevelHighSchool().equals(educationOrgUnit.getEducationLevelHighSchool())
                && oldData.getOldEducationOrgUnit().getDevelopCondition().equals(educationOrgUnit.getDevelopCondition())
                && oldData.getOldEducationOrgUnit().getDevelopPeriod().equals(educationOrgUnit.getDevelopPeriod()))
            im.put("highSchoolNew", "");
        else {
            RtfString str = UtilPrintSupport.getHighLevelSchoolTypeString(educationOrgUnit.getEducationLevelHighSchool());
            str.append(" (")
                    .append(educationOrgUnit.getDevelopCondition().getTitle().toLowerCase())
                    .append(" ")
                    .append(educationOrgUnit.getDevelopPeriod().getTitle())
                    .append(")");

            if (!isFirst)
                str.toList().add(0, RtfBean.getElementFactory().createRtfText(", "));
            else
                str.toList().add(0, RtfBean.getElementFactory().createRtfText(" "));

            im.put("highSchoolNew", str);
            isFirst = false;
        }

        if (listRepresent.getCompensationTypeNew() != null)
            im.put("compTypeNew", (isFirst ? "" : ",") + " для обучения " + UtilPrintSupport.getCompensationTypeStr(listRepresent.getCompensationTypeNew()));
        else
            im.put("compTypeNew", "");

        im.put("courseOld", oldData.getOldCourse().getTitle());
        im.put("developFormOld", UtilPrintSupport.getDevelopFormGen(oldData.getOldEducationOrgUnit().getDevelopForm()));
        RtfString str = UtilPrintSupport.getHighLevelSchoolTypeString(oldData.getOldEducationOrgUnit().getEducationLevelHighSchool());
        str.append(" (")
                .append(oldData.getOldEducationOrgUnit().getDevelopCondition().getTitle().toLowerCase())
                .append(" ")
                .append(oldData.getOldEducationOrgUnit().getDevelopPeriod().getTitle())
                .append(")");
        im.put("highSchoolOld", str);
        im.put("compTypeOld", UtilPrintSupport.getCompensationTypeStr(oldData.getOldCompensationType()));

        if (listRepresent.isDifferenceEduPlan()) {
            RtfString sb = new RtfString();
            sb.append(", с условием ликвидации разницы в учебных планах до ");
            UtilPrintSupport.getDateFormatterWithMonthStringAndYear(listRepresent.getDateLiquidation(), sb);
            im.put("isDifferenceEduPlan", sb);
        }
        else
            im.put("isDifferenceEduPlan", "");
        im.put("reason", listRepresent.getRepresentationReason().getTitle());
    }

    private void insertRow(RtfDocument document, String text) {
        document.getElementList().add(RtfBean.getElementFactory().createRtfText(text));
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
    }

    private String getKey(Group group, Course course, EducationOrgUnit educationOrgUnit, ListRepresentTransfer represent) {
        StringBuilder key = new StringBuilder()
                .append(course.getId())
                .append(educationOrgUnit.getDevelopForm().getId())
                .append(educationOrgUnit.getEducationLevelHighSchool().getId())
                .append(educationOrgUnit.getDevelopCondition().getId())
                .append(educationOrgUnit.getDevelopPeriod().getId())
                .append(represent.getDateTransfer().getTime())
                //-------------------------------------------------
                .append(represent.isIndividualPlan())
                .append(represent.isDifferenceEduPlan())
                .append(represent.getDateLiquidation() != null ? represent.getDateLiquidation().getTime() : "")
                //-------------------------------------------------
                ;

        if (represent.getCourseNew() != null)
            key.append(represent.getCourseNew().getId());

        if (represent.getGroupNew() != null)
            key.append(represent.getGroupNew().getId());
        else if (group != null)
            key.append(group.getId());

        if (represent.getCompensationTypeNew() != null)
            key.append(represent.getCompensationTypeNew().getId());

        if (represent.getEducationOrgUnit() != null)
            key.append(represent.getEducationOrgUnit().getId());

        key.append(represent.isDifferenceEduPlan())
                .append(represent.isDifferenceEduPlan() ? represent.getDateTransfer().getTime() : "");

        return key.toString();
    }

    private Map<OrgUnit, Map<String, List<Object[]>>> getStudentData(List<? extends ListRepresent> listOfRepresents) {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(RelListRepresentStudents.class, "rel")
                .where(DQLExpressions.in(
                        DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("rel")),
                        listOfRepresents
                ))
                .joinEntity(
                        "rel",
                        DQLJoinType.inner,
                        ListRepresentTransfer.class,
                        "rep",
                        DQLExpressions.eq(
                                DQLExpressions.property(ListRepresentTransfer.id().fromAlias("rep")),
                                DQLExpressions.property(RelListRepresentStudents.representation().id().fromAlias("rel"))
                        ))
                .column(DQLExpressions.property(RelListRepresentStudents.student().fromAlias("rel")))
                .column("rep")
                .column("rel")
                .order(DQLExpressions.property(RelListRepresentStudents.student().person().identityCard().fullFio().fromAlias("rel")));

        List<Object[]> studentDataList = UniDaoFacade.getCoreDao().getList(dql);

        Map<OrgUnit, Map<String, List<Object[]>>> dataMap = new HashMap<>();
        for (Object[] arr : studentDataList) {
            Student student = (Student) arr[0];
            ListRepresentTransfer represent = (ListRepresentTransfer) arr[1];

            OrgUnit formOU = student.getEducationOrgUnit().getFormativeOrgUnit();
            if (dataMap.get(formOU) == null)
                dataMap.put(formOU, new HashMap<>());
            RelListRepresentStudentsOldData oldData = (RelListRepresentStudentsOldData) arr[2];
            String key = getKey(oldData.getOldGroup(), oldData.getOldCourse(), oldData.getOldEducationOrgUnit(), represent);
            if (dataMap.get(formOU).get(key) == null)
                dataMap.get(formOU).put(key, new ArrayList<>());

            dataMap.get(formOU).get(key).add(arr);
        }

        return dataMap;
    }

    @Override
    public Map<Student, OrderParagraphInfo> getParagInfomap() {
        return hashMap;
    }

}
