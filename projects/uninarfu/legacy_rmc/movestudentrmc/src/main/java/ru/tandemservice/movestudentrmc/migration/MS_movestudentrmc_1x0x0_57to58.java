package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_movestudentrmc_1x0x0_57to58 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность listRepresentAdmissionToPractice

        // создано свойство registryElementRow
        {
            // создать колонку
            tool.createColumn("listrepradmissionpractice_t", new DBColumn("registryelementrow_id", DBType.LONG));

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность listRepresentPractice

        // создано свойство registryElementRow
        {
            // создать колонку
            tool.createColumn("listrepresentpractice_t", new DBColumn("registryelementrow_id", DBType.LONG));

        }


    }
}