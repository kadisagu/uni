package ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab;

import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.List;

@State({@org.tandemframework.core.component.Bind(key = "orgUnitId", binding = "orgUnit.id")})
public class Model {

    private OrgUnit orgUnit = new OrgUnit();

    private List<EducationYear> eduYearList;
    private EducationYear eduYear;

    private List<MonthWrapper> monthModel;
    private MonthWrapper month;

    private DynamicListDataSource<GrantWrapper> dataSource;

    public OrgUnit getOrgUnit() {
        return orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit) {
        this.orgUnit = orgUnit;
    }

    public List<EducationYear> getEduYearList() {
        return eduYearList;
    }

    public void setEduYearList(List<EducationYear> eduYearList) {
        this.eduYearList = eduYearList;
    }

    public EducationYear getEduYear() {
        return eduYear;
    }

    public void setEduYear(EducationYear eduYear) {
        this.eduYear = eduYear;
    }

    public List<MonthWrapper> getMonthModel() {
        return monthModel;
    }

    public void setMonthModel(List<MonthWrapper> monthModel) {
        this.monthModel = monthModel;
    }

    public MonthWrapper getMonth() {
        return month;
    }

    public void setMonth(MonthWrapper month) {
        this.month = month;
    }

    public DynamicListDataSource<GrantWrapper> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<GrantWrapper> dataSource) {
        this.dataSource = dataSource;
    }


}
