package ru.tandemservice.movestudentrmc.component.catalog.document.DocumentAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.movestudentrmc.entity.catalog.Document;

public class DAO extends DefaultCatalogAddEditDAO<Document, Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        super.prepare(model);
        if (model.isAddForm()) {
            model.getCatalogItem().setShowName(true);
            model.getCatalogItem().setNumberExist(true);
            model.getCatalogItem().setNumberRequired(true);
            model.getCatalogItem().setIssuanceDateExist(true);
            model.getCatalogItem().setIssuanceDateRequired(true);
            model.getCatalogItem().setPeriodDateExist(true);
        }
    }

}
