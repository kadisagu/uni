package ru.tandemservice.movestudentrmc.base.bo.DORepresentSupport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentSupport.logic.DORepresentSupportDAO;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentSupport.ui.Edit.DORepresentSupportEdit;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentSupport.ui.View.DORepresentSupportView;
import ru.tandemservice.movestudentrmc.docord.IDOObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IDOObjectModifyDAO;

@Configuration
public class DORepresentSupportManager extends BusinessObjectManager implements IDOObjectByRepresentTypeManager {

    public static DORepresentSupportManager instance() {
        return instance(DORepresentSupportManager.class);
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return DORepresentSupportEdit.class;
    }

    @Bean
    public IDOObjectModifyDAO getDOObjectModifyDAO() {
        return new DORepresentSupportDAO();
    }

    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return DORepresentSupportView.class;
    }

}
