package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.DORepresentBaseManager;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class TypeRepresentationDSHandler extends DefaultComboDataSourceHandler {

    public TypeRepresentationDSHandler(String ownerId) {

        super(ownerId, RepresentationType.class, RepresentationType.P_TITLE);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        String filter = ep.input.getComboFilterByValue();

        MQBuilder builder = new MQBuilder(RepresentationType.ENTITY_CLASS, "r");
        builder.add(MQExpression.eq("r", RepresentationType.use(), Boolean.TRUE));
        builder.add(MQExpression.eq("r", RepresentationType.listRepresentation(), Boolean.FALSE));

        builder.getSelectAliasList().clear();
        builder.addSelect(RepresentationType.code().s());

        List<String> list = builder.getResultList(ep.context.getSession());
        List<String> trueCode = new ArrayList<String>();

        for (String string : list) {
            if (DORepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(string) != null)
                trueCode.add(string);
        }

        ep.dqlBuilder.where(DQLExpressions.in(property(RepresentationType.code().fromAlias("e")), trueCode));
        if (StringUtils.isNotBlank(filter)) {
            ep.dqlBuilder.where(like(DQLFunctions.upper(property(RepresentationType.title().fromAlias("e"))), value(CoreStringUtils.escapeLike(filter, true))));
        }
    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
    {
        ep.dqlBuilder.order("e." + RepresentationType.title());
    }
}
