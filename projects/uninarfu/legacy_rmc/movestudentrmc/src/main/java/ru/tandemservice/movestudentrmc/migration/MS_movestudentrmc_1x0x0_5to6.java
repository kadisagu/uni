package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.BooleanDBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;

import java.sql.PreparedStatement;

public class MS_movestudentrmc_1x0x0_5to6 extends IndependentMigrationScript {
    @Override
    public void run(DBTool tool) throws Exception {

        if (tool.tableExists("representationbasement_t")) {

            if (!tool.columnExists("representationbasement_t", "connectedwithdocument_p")) {
                tool.createColumn("representationbasement_t", new BooleanDBColumn("connectedwithdocument_p"));
                PreparedStatement pst = tool.getConnection().prepareStatement("update representationbasement_t SET connectedwithdocument_p=?");
                pst.setBoolean(1, false);
                pst.execute();
            }


            if (tool.columnExists("representationbasement_t", "noterequired_p")) {
                PreparedStatement pst = tool.getConnection().prepareStatement("update representationbasement_t SET noterequired_p=?");
                pst.setBoolean(1, false);
                pst.execute();
            }

        }


        if (tool.tableExists("representationtype_t")) {

            if (!tool.columnExists("representationtype_t", "listrepresentation_p")) {
                tool.createColumn("representationtype_t", new BooleanDBColumn("listrepresentation_p"));
                PreparedStatement pst = tool.getConnection().prepareStatement("update representationtype_t SET listrepresentation_p=?");
                pst.setBoolean(1, false);
                pst.execute();
            }
        }

    }
}
