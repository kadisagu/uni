package ru.tandemservice.movestudentrmc.base.bo.DOOrder.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.logic.*;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.ReasonRepresentDSHandler;
import ru.tandemservice.movestudentrmc.entity.DocumentOrder;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.entity.employee.Student;


@Configuration
public class DOOrderList extends BusinessComponentManager {

    public static final String ORDER_DS = "orderDS";
    public static final String TYPE_REPRESENT_DS = "typeRepresentDS";
    public static final String REASON_REPRESENT_DS = "reasonRepresentDS";
    public static final String FORMATIVE_ORG_UNIT_DS = "formativeOrgUnitDS";
    public static final String STUDENT_COMBO_DS = "studentComboDS";

    @Bean
    public ColumnListExtPoint orderDS()
    {
        return columnListExtPointBuilder(ORDER_DS)
                .addColumn(publisherColumn(OrderCommitDSHandler.ORDER_TITLE_COLUMN, DocumentOrder.fullTitle()).permissionKey("rmc_view_order").order().create())
                .addColumn(textColumn(OrderDSHandler.STATE_COLUMN, DocumentOrder.state().title()).order().create())
                .addColumn(dateColumn(OrderDSHandler.CREATE_DATE_COLUMN, DocumentOrder.createDate()).order().create())
                .addColumn(textColumn(OrderDSHandler.REPRESENTATION_TYPES, DocumentOrder.P_REPRESENTATION_TYPES).create())
                .addColumn(textColumn(OrderDSHandler.OPERATOR, DocumentOrder.operator().fullFio()).order().create())
                .addColumn(textColumn(OrderDSHandler.REPRESENTATION_COUNT, DocumentOrder.P_REPRESENTATION_COUNT).create())
                .addColumn(actionColumn(OrderCommitDSHandler.PRINT_COLUMN_NAME, CommonDefines.ICON_PRINT, "onClickPrintFromList").permissionKey("rmc_print_order").create())
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("rmc_edit_order").disabled("ui:isFormative").create())
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER, alert("orderDS.delete.alert")).permissionKey("rmc_delete_order").disabled("ui:isFormative").create())
                .create();
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(ORDER_DS, orderDS()).handler(orderDSHandler()))
                .addDataSource(selectDS(TYPE_REPRESENT_DS, typeRepresentDSHandler()))
                .addDataSource(selectDS(REASON_REPRESENT_DS, reasonRepresentDSHandler()))
                .addDataSource(selectDS(FORMATIVE_ORG_UNIT_DS).handler(formativeOrgUnitDSHandler()))
                .addDataSource(UniStudentManger.instance().courseDSConfig())
                .addDataSource(selectDS(STUDENT_COMBO_DS).handler(studentComboDSHandler()).addColumn(Student.person().identityCard().fullFio().s()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> orderDSHandler()
    {
        return new OrderDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> typeRepresentDSHandler()
    {
        return new TypeRepresentDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> reasonRepresentDSHandler()
    {
        return new ReasonRepresentDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> formativeOrgUnitDSHandler() {
        return new FormativeOrgUnitDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> studentComboDSHandler()
    {
        return new StudentComboDSHandler(getName());
    }


}
