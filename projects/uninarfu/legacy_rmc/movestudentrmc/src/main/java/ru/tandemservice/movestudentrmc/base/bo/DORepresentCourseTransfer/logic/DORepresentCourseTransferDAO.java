package ru.tandemservice.movestudentrmc.base.bo.DORepresentCourseTransfer.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentDAO;
import ru.tandemservice.movestudentrmc.docord.util.MoveStudentUtil;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.RepresentCourseTransfer;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntity;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class DORepresentCourseTransferDAO extends AbstractDORepresentDAO
{

    @Override
    public boolean doRollback(Representation represent, ErrorCollector error) {
        if (!super.doRollback(represent, error))
            return false;

        List<DocRepresentStudentBase> studentList = getStudentList(represent);
        Student student = studentList.get(0).getStudent();
        RepresentCourseTransfer rep = (RepresentCourseTransfer) represent;

        Course course = null;
        if (rep.getOldCourseId() != null)
            course = get(rep.getOldCourseId());
        if (course == null)
            error.add("Не найден старый курс");

        Group group = null;
        if (rep.getOldGroupId() != null) {
            group = get(rep.getOldGroupId());
            if (group == null)
                error.add("Не найдена старая группа");
        }

        CompensationType compensationType = get(rep.getOldCompensationTypeId());

        EducationOrgUnit eduOrgUnit = null;
        if (rep.getOldEducationOrgUnitId() != null)
            eduOrgUnit = get(rep.getOldEducationOrgUnitId());
        if (eduOrgUnit == null)
            error.add("Не найдены старые данные");

        if (!error.isHasFieldErrors()) {
            student.setCourse(course);
            student.setGroup(group);

            student.setCompensationType(compensationType);

            student.setEducationOrgUnit(eduOrgUnit);

            getSession().saveOrUpdate(student);

            doRollback(student, rep, getSession());

            rollbackStudentGrantsCancel(represent);
        }
        else
            return false;

        return true;
    }

    @Override
    public boolean doCommit(Representation represent, ErrorCollector error) {
        if (!super.doCommit(represent, error))
            return false;

        List<DocRepresentStudentBase> studentList = getStudentList(represent);
        Student student = studentList.get(0).getStudent();
        RepresentCourseTransfer rep = (RepresentCourseTransfer) represent;

        EducationOrgUnit educationOrgUnit = rep.getEducationOrgUnit() == null ? rep.getGroup().getEducationOrgUnit() : rep.getEducationOrgUnit();

        List<Student> lst = MoveStudentUtil.checkStudentList(Arrays.asList(student), null, educationOrgUnit.getDevelopForm());

        if (!lst.isEmpty()) {
            List<StudentGrantEntity> sgeList = MoveStudentUtil.getSrudentGrantEntityList(lst, represent.getStartDate());
            commitStudentGrantsCancel(represent, sgeList);
        }

        student.setGroup(rep.getGroup());
        student.setCourse(rep.getCourse());

        if (rep.getEducationOrgUnit() != null)
            student.setEducationOrgUnit(rep.getEducationOrgUnit());
        else
            student.setEducationOrgUnit(rep.getGroup().getEducationOrgUnit());

        getSession().saveOrUpdate(student);

        //проставляем новуе УП-РУП
        doCommit(student, rep, getSession());

        return true;
    }

    @Override
    public Representation createRepresentObject(RepresentationType type, List<Student> studentList) {
        RepresentCourseTransfer result = new RepresentCourseTransfer();
        result.setType(type);

        return result;
    }

    @Override
    public void buildBodyExtract(Representation representationBase, String itemFac, RtfDocument docCommon) {
        RepresentCourseTransfer represent = (RepresentCourseTransfer) representationBase;
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        UtilPrintSupport.setParNum(docCommon, itemFac);

        RtfInjectModifier modifier = new RtfInjectModifier();

        UtilPrintSupport.injectModifierExtract(modifier, docRepresent);

        injectModifier(modifier, represent, docRepresent);

        modifier.modify(docCommon);
    }

    @Override
    public void buildBodyRepresent(Representation representationBase, RtfDocument document, AtomicInteger parNumber, AtomicInteger extractNumber) {
        super.buildBodyRepresent(representationBase, document, parNumber, extractNumber);
        RepresentCourseTransfer represent = (RepresentCourseTransfer) representationBase;
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);
        Student student = docRepresent.getStudent();

        RtfInjectModifier modifier = new RtfInjectModifier();

        Course course = null;
        if (represent.getOldCourseId() != null)
            course = get(represent.getOldCourseId());

        Group group = null;
        if (represent.getOldGroupId() != null)
            group = get(represent.getOldGroupId());

        EducationOrgUnit eduOrgUnit = null;
        if (represent.getOldEducationOrgUnitId() != null)
            eduOrgUnit = get(represent.getOldEducationOrgUnitId());

        if (eduOrgUnit != null && course != null)
            UtilPrintSupport.injectModifierRepresent(modifier, docRepresent, student, eduOrgUnit, course, group);
        else
            UtilPrintSupport.injectModifierRepresent(modifier, docRepresent, student);
        
        injectModifier(modifier, represent, docRepresent);

        if (StringUtils.isEmpty(modifier.getStringValue("basics")))
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Arrays.asList("basics"), false, false);

        modifier.modify(document);
    }

    protected void injectModifier(RtfInjectModifier modifier, RepresentCourseTransfer represent, DocRepresentStudentBase docrepresent) {

        //modifier.put("studentTitle", PersonManager.instance().declinationDao().getDeclinationFIO(docrepresent.getStudent().getPerson().getIdentityCard(), GrammaCase.ACCUSATIVE));
        modifier.put("studentTitle", UtilPrintSupport.printSeparatedFIO(docrepresent.getStudent().getPerson().getIdentityCard(), GrammaCase.ACCUSATIVE, docrepresent.getStudent().getPerson().isMale()));

        modifier.put("studentSex", docrepresent.getStudent().getPerson().isMale() ? "студента" : "студентку");
        //-----------------------------------------------------------------------
        //modifier.put("developPayment", docrepresent.getCompensationTypeStr());
        //-----------------------------------------------------------------------

        modifier.put("newCourse", represent.getCourse().getTitle());
        RtfString reasonStr = new RtfString();
        if (represent.isConditionally()) {
            reasonStr.append("с условием ликвидации академической задолженности до ");
            UtilPrintSupport.getDateFormatterWithMonthStringAndYear(represent.getDeadlineDate(), reasonStr);
        }
        else {
            String str = represent.getReason().getTitle().toLowerCase();
            str = str.replace("_____ курса", docrepresent.getCourseStr() + " курса");
            reasonStr.append(str);
        }
        modifier.put("reason", reasonStr);
    }

}
