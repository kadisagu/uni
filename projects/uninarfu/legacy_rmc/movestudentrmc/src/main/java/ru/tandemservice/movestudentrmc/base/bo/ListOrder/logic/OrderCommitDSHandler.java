package ru.tandemservice.movestudentrmc.base.bo.ListOrder.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudentrmc.entity.ListOrdListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListOrder;
import ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class OrderCommitDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{

    public static final String NUMBER_ORDER_FILTER = "numberOrderFilter";
    public static final String DATE_FORMATIVE_FROM_FILTER = "dateFormativeFromFilter";
    public static final String DATE_COMMIT_FROM_FILTER = "dateCommitFromFilter";
    public static final String DATE_FORMATIVE_TO_FILTER = "dateFormativeToFilter";
    public static final String DATE_COMMIT_TO_FILTER = "dateCommitToFilter";
    public static final String DATE_START_FROM_FILTER = "dateStartFromFilter";
    public static final String DATE_START_TO_FILTER = "dateStartToFilter";

    public static final String ORDER_TITLE_COLUMN = "number";
    public static final String CREATE_DATE_COLUMN = "createDate";
    public static final String COMMIT_DATE_COLUMN = "commitDate";
    public static final String PRINT_COLUMN_NAME = "print";
    public static final String REPRESENTATION_TYPES = "representationTypes";
    public static final String REPRESENTATION_COUNT = "representationCount";
    public static final String STUDENT_COUNT = "studentCount";
    public static final String OPERATOR = "operator";

    public OrderCommitDSHandler(String ownerId)
    {
        super(ownerId);
        setPageable(false);
    }

    private DQLSelectBuilder builder;

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Date dateCommitFromFilter = context.get(DATE_COMMIT_FROM_FILTER);
        Date dateFormativeFromFilter = context.get(DATE_FORMATIVE_FROM_FILTER);
        Date dateCommitToFilter = getLastTime(context.get(DATE_COMMIT_TO_FILTER));
        Date dateFormativeToFilter = getLastTime(context.get(DATE_FORMATIVE_TO_FILTER));
        Date dateStartFromFilter = context.get(DATE_START_FROM_FILTER);
        Date dateStartToFilter = getLastTime(context.get(DATE_START_TO_FILTER));
        String numberOrderFilter = context.get(NUMBER_ORDER_FILTER);
        List<RepresentationType> types = context.get("typesRepresentFilter");
        List<OrgUnit> formativeOrgUnits = context.get("formativeOrgUnitsFilter");
        List<Course> courses = context.get("coursesFilter");
        List<Student> students = context.get("studentsFilter");
        String author = context.get("authorsFilter");
        List<RepresentationReason> reasons = context.get("reasonsRepresentFilter");


        builder = new DQLSelectBuilder().fromEntity(ListOrder.class, "o").column("o");

        builder.where(DQLExpressions.eq(
                DQLExpressions.property(ListOrder.state().code().fromAlias("o")),
                DQLExpressions.value("5")
        ));

        //Дата проведения
        builder.where(DQLExpressions.betweenDays(ListOrder.commitDateSystem().fromAlias("o"), dateCommitFromFilter, dateCommitToFilter));

        //Дата формирования
        builder.where(DQLExpressions.betweenDays(ListOrder.createDate().fromAlias("o"), dateFormativeFromFilter, dateFormativeToFilter));

        //Период вступления в силу
        builder.where(DQLExpressions.betweenDays(ListOrder.commitDate().fromAlias("o"), dateStartFromFilter, dateStartToFilter));

        if (numberOrderFilter != null)
            builder.where(DQLExpressions.like(
                    DQLExpressions.property(ListOrder.number().fromAlias("o")),
                    DQLExpressions.value(CoreStringUtils.escapeLike(numberOrderFilter, true))
            ));

        DQLSelectBuilder subBuilder = new DQLSelectBuilder()
                .fromEntity(ListOrdListRepresent.class, "lor")
                .column(DQLExpressions.property(ListOrdListRepresent.order().fromAlias("lor")))
                .joinEntity("lor", DQLJoinType.left, RelListRepresentStudents.class, "rel",
                        DQLExpressions.eq(
                                DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("rel")),
                                DQLExpressions.property(ListOrdListRepresent.representation().fromAlias("lor"))))
                .predicate(DQLPredicateType.distinct);

        FilterUtils.applySelectFilter(subBuilder, "lor", ListOrdListRepresent.representation().representationType(), types);
        FilterUtils.applySelectFilter(subBuilder, "rel", RelListRepresentStudents.student().educationOrgUnit().formativeOrgUnit(), formativeOrgUnits);
        FilterUtils.applySelectFilter(subBuilder, "rel", RelListRepresentStudents.student().course(), courses);
        FilterUtils.applySelectFilter(subBuilder, "rel", RelListRepresentStudents.student(), students);
        FilterUtils.applySelectFilter(subBuilder, "lor", ListOrdListRepresent.representation().representationReason(), reasons);

        builder.where(DQLExpressions.in(DQLExpressions.property(ListOrder.id().fromAlias("o")), subBuilder.buildQuery()));

        FilterUtils.applySimpleLikeFilter(builder, "o", ListOrder.operator().identityCard().fullFio().s(), author);

        if (input.getEntityOrder() != null)
        {
            if (CREATE_DATE_COLUMN.equals(input.getEntityOrder().getColumnName()))
            {
                builder.order(DQLExpressions.property(ListOrder.createDate().fromAlias("o")), input.getEntityOrder().getDirection());
            }
            if (ORDER_TITLE_COLUMN.equals(input.getEntityOrder().getColumnName()))
            {
                builder.order(DQLExpressions.property(ListOrder.commitDate().fromAlias("o")), input.getEntityOrder().getDirection());
            }
            if (COMMIT_DATE_COLUMN.equals(input.getEntityOrder().getColumnName()))
            {
                builder.order(DQLExpressions.property(ListOrder.commitDateSystem().fromAlias("o")), input.getEntityOrder().getDirection());
            }

        }

        return DQLSelectOutputBuilder.get(input, builder, getSession()).build();
    }

    public DQLSelectBuilder getBuilder()
    {
        return builder;
    }

    public Date getLastTime(Date date)
    {
        if (date != null)
        {
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            c.add(Calendar.DAY_OF_YEAR, 1);
            c.add(Calendar.MILLISECOND, -1);
            date = c.getTime();
        }
        return date;
    }
}
