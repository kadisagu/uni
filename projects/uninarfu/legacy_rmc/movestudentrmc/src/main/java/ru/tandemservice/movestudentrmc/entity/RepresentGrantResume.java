package ru.tandemservice.movestudentrmc.entity;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.movestudentrmc.entity.gen.RepresentGrantResumeGen;

/**
 * О возобновлении стипендии/выплаты
 */
public class RepresentGrantResume extends RepresentGrantResumeGen
{
    @Override
    public String getTitle() {
        return new StringBuilder()
                .append(getType().getTitle())
                .append(" (")
                .append(getGrantView().getShortTitle())
                .append(") с ")
                .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateResumePayment()))
                //---------------------------------------------------------------------
                //.append(", от ")
                //.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getCreateDate()))
                //---------------------------------------------------------------------
                .toString();
    }

    @Override
    public String getRepresentationTitle() {
        return new StringBuilder()
                .append(getType().getTitle())
                .append(" (")
                .append(getGrantView().getShortTitle())
                .append(") с ")
                .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateResumePayment()))
                .toString();
    }
}