package ru.tandemservice.movestudentrmc.base.bo.RelationSettings.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import ru.tandemservice.movestudentrmc.base.bo.RelationSettings.ui.AddEdit.RelationSettingsAddEdit;
import ru.tandemservice.movestudentrmc.base.bo.RelationSettings.ui.AddEdit.RelationSettingsAddEditUI;

public class RelationSettingsListUI extends UIPresenter {

    @Override
    public void onComponentRefresh()
    {
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegion(RelationSettingsAddEdit.class)
                .parameter(RelationSettingsAddEditUI.TYPE_REPRESENTATION_ID, getListenerParameterAsLong())
                .activate();
    }
}
