package ru.tandemservice.movestudentrmc.base.bo.ListRepresentQualificationThemes;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentQualificationThemes.logic.ListRepresentQualificationThemesManagerModifyDAO;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentQualificationThemes.logic.OrgUnitSelectedDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentQualificationThemes.ui.Edit.ListRepresentQualificationThemesEdit;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentQualificationThemes.ui.View.ListRepresentQualificationThemesView;
import ru.tandemservice.movestudentrmc.docord.IListObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IListObjectModifyDAO;


@Configuration
public class ListRepresentQualificationThemesManager extends BusinessObjectManager implements IListObjectByRepresentTypeManager {

    public static ListRepresentQualificationThemesManager instance()
    {
        return instance(ListRepresentQualificationThemesManager.class);
    }


    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return ListRepresentQualificationThemesEdit.class;
    }


    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return ListRepresentQualificationThemesView.class;
    }


    @Bean
    @Override
    public IListObjectModifyDAO getListObjectModifyDAO() {
        return new ListRepresentQualificationThemesManagerModifyDAO();
    }


    @Bean
    public DefaultComboDataSourceHandler getOrgUnitSelectedDSHandler() {
        return new OrgUnitSelectedDSHandler(getName());
    }
}
