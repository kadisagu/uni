package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;

public class MS_movestudentrmc_1x0x0_8to9 extends IndependentMigrationScript {
    @Override
    public void run(DBTool tool) throws Exception {

		/* Миграция для очищения базы в нужном порядке от всего,
         * что касается нового функционала по представлениям, приказам, стипендий
		 * справочникам для представлений, приказов, стипендий
		 * и их связей.
		 * RM #1648 
		 */

        if (tool.tableExists("RPRSNTTNADDTNLDCMNTSRLTN_T")) {
            tool.dropTable("RPRSNTTNADDTNLDCMNTSRLTN_T");
        }

        if (tool.tableExists("ADDITIONALDOCUMENT_T")) {
            tool.dropTable("ADDITIONALDOCUMENT_T");
        }

        if (tool.tableExists("BASEMENT_T")) {
            tool.dropTable("BASEMENT_T");
        }

        if (tool.tableExists("DOCORDREPRESENT_T")) {
            tool.dropTable("DOCORDREPRESENT_T");
        }

        if (tool.tableExists("DOCREPRESENTBASICS_T")) {
            tool.dropTable("DOCREPRESENTBASICS_T");
        }

        if (tool.tableExists("DOCREPRESENTGRANTS_T")) {
            tool.dropTable("DOCREPRESENTGRANTS_T");
        }

        if (tool.tableExists("DOCREPRESENTSTUDENTBASE_T")) {
            tool.dropTable("DOCREPRESENTSTUDENTBASE_T");
        }

        if (tool.tableExists("DOCREPRESENTSTUDENTDOCUMENTS_T")) {
            tool.dropTable("DOCREPRESENTSTUDENTDOCUMENTS_T");
        }

        if (tool.tableExists("RLRPRSNTTNBSMNTDCMNT_T")) {
            tool.dropTable("RLRPRSNTTNBSMNTDCMNT_T");
        }

        if (tool.tableExists("NARFUDOCUMENT_T")) {
            tool.dropTable("NARFUDOCUMENT_T");
        }

        if (tool.tableExists("RELDOCUMENTTYPEKIND_T")) {
            tool.dropTable("RELDOCUMENTTYPEKIND_T");
        }

        if (tool.tableExists("DOCUMENT_T")) {
            tool.dropTable("DOCUMENT_T");
        }

        if (tool.tableExists("DOCUMENTKIND_T")) {
            tool.dropTable("DOCUMENTKIND_T");
        }

        if (tool.tableExists("DOCUMENTORDER_T")) {
            tool.dropTable("DOCUMENTORDER_T");
        }

        if (tool.tableExists("DOCUMENTTYPE_T")) {
            tool.dropTable("DOCUMENTTYPE_T");
        }

        if (tool.tableExists("RELTYPEGRANTVIEW_T")) {
            tool.dropTable("RELTYPEGRANTVIEW_T");
        }

        if (tool.tableExists("GRANT_T")) {
            tool.dropTable("GRANT_T");
        }

        if (tool.tableExists("GRANTENTITY_T")) {
            tool.dropTable("GRANTENTITY_T");
        }

        if (tool.tableExists("GRANTTYPE_T")) {
            tool.dropTable("GRANTTYPE_T");
        }

        if (tool.tableExists("RELGRANTVIEWORGUNIT_T")) {
            tool.dropTable("RELGRANTVIEWORGUNIT_T");
        }

        if (tool.tableExists("STUDENTGRANTENTITY_T")) {
            tool.dropTable("STUDENTGRANTENTITY_T");
        }

        if (tool.tableExists("GRANTVIEW_T")) {
            tool.dropTable("GRANTVIEW_T");
        }

        if (tool.tableExists("RLORDRCTGRYRPRSNTTNTYP_T")) {
            tool.dropTable("RLORDRCTGRYRPRSNTTNTYP_T");
        }

        if (tool.tableExists("REPRESENTATION_T")) {
            if (tool.constraintExists("REPRESENTATION_T", "fk_basement_representation")) {
                tool.dropConstraint("REPRESENTATION_T", "fk_basement_representation");
            }
            if (tool.indexExists("REPRESENTATION_T", "idx_basement_representation")) {
                tool.dropIndex("REPRESENTATION_T", "idx_basement_representation");
            }
            if (tool.columnExists("REPRESENTATION_T", "BASEMENT_ID")) {
                tool.dropColumn("REPRESENTATION_T", "BASEMENT_ID");
            }
        }

        if (tool.tableExists("RPRSNTTNBSMNTRLTN_T")) {
            tool.dropTable("RPRSNTTNBSMNTRLTN_T");
        }

        if (tool.tableExists("REPRESENTATION_T")) {
            tool.dropTable("REPRESENTATION_T");
        }

        if (tool.tableExists("MOVESTUDENTEXTRACTSTATES_T")) {
            tool.dropTable("MOVESTUDENTEXTRACTSTATES_T");
        }

        if (tool.tableExists("MOVESTUDENTORDERSTATES_T")) {
            tool.dropTable("MOVESTUDENTORDERSTATES_T");
        }

        if (tool.tableExists("ORDERCATEGORY_T")) {
            tool.dropTable("ORDERCATEGORY_T");
        }

        if (tool.tableExists("PRINTTEMPLATE_T")) {
            tool.dropTable("PRINTTEMPLATE_T");
        }

        if (tool.tableExists("RELREPRESENTATIONREASONBASIC_T")) {
            tool.dropTable("RELREPRESENTATIONREASONBASIC_T");
        }

        if (tool.tableExists("RELREPRESENTATIONREASONOSSP_T")) {
            tool.dropTable("RELREPRESENTATIONREASONOSSP_T");
        }

        if (tool.tableExists("REPRESENTATIONBASEMENT_T")) {
            tool.dropTable("REPRESENTATIONBASEMENT_T");
        }

        if (tool.tableExists("REPRESENTATIONREASON_T")) {
            tool.dropTable("REPRESENTATIONREASON_T");
        }

        if (tool.tableExists("REPRESENTATIONTYPE_T")) {
            tool.dropTable("REPRESENTATIONTYPE_T");
        }

        if (tool.tableExists("TYPETEMPLATEREPRESENT_T")) {
            tool.dropTable("TYPETEMPLATEREPRESENT_T");
        }

        if (tool.tableExists("OSSPBASEMENT_T")) {
            tool.dropTable("OSSPBASEMENT_T");
        }

        if (tool.tableExists("OSSPGRANTS_T")) {
            tool.dropTable("OSSPGRANTS_T");
        }

        if (tool.tableExists("'OSSPGRANTSVIEW_T")) {
            tool.dropTable("'OSSPGRANTSVIEW_T");
        }

        if (tool.tableExists("OSSPPGO_T")) {
            tool.dropTable("OSSPPGO_T");
        }

        if (tool.tableExists("STUGRANTSTATUS_T")) {
            tool.dropTable("STUGRANTSTATUS_T");
        }

        if (tool.tableExists("REPRESENTEXCLUDE_T")) {
            tool.dropTable("REPRESENTEXCLUDE_T");
        }

    }
}
