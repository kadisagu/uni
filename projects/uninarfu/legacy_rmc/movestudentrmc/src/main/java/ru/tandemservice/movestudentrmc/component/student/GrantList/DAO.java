package ru.tandemservice.movestudentrmc.component.student.GrantList;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab.MonthWrapper;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntity;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.ArrayList;
import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        Student student = getNotNull(Student.class, model.getStudent().getId());
        model.setStudent(student);
        model.setEduYear(getNotNull(EducationYear.class, model.getEduYear().getId()));
        model.setMonthWrapper(new MonthWrapper(model.getMonth()));

        model.setListTitle("Стипендии " +
                                   PersonManager.instance().declinationDao().getDeclinationFIO(student.getPerson().getIdentityCard(), GrammaCase.GENITIVE) +
                                   " за " + model.getMonthWrapper().getTitle() + " " + model.getEduYear().getTitle());
    }

    @Override
    public void prepareListDataSource(Model model) {

        List<StudentGrantEntity> res = new ArrayList<StudentGrantEntity>();

        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(StudentGrantEntity.class, "sg")
                .addColumn("sg")
                .where(DQLExpressions.eq(DQLExpressions.property(StudentGrantEntity.student().fromAlias("sg")),
                                         DQLExpressions.value(model.getStudent())))
                .where(DQLExpressions.eq(DQLExpressions.property(StudentGrantEntity.eduYear().fromAlias("sg")),
                                         DQLExpressions.value(model.getEduYear())))
                .where(DQLExpressions.eq(DQLExpressions.property(StudentGrantEntity.month().fromAlias("sg")),
                                         DQLExpressions.value(model.getMonthWrapper().getStringValue(model.getEduYear()))))
                .where(DQLExpressions.or(DQLExpressions.eq(DQLExpressions.property(StudentGrantEntity.tmp().fromAlias("sg")),
                                                           DQLExpressions.value(Boolean.FALSE)),
                                         DQLExpressions.isNull(DQLExpressions.property(StudentGrantEntity.tmp().fromAlias("sg")))));

        res = builder.createStatement(getSession()).<StudentGrantEntity>list();

        UniUtils.createPage(model.getDataSource(), res);
    }

    @Override
    public void deleteRow(Long id) {
        StudentGrantEntity grant = get(StudentGrantEntity.class, id);
        if (grant != null)
            delete(grant);
    }

}
