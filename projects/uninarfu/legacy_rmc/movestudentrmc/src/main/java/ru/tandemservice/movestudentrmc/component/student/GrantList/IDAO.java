package ru.tandemservice.movestudentrmc.component.student.GrantList;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {
    void deleteRow(Long id);

}
