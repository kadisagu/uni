package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class RelListRepresentStudentsDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput> {

    protected DQLOrderDescriptionRegistry _orderDescriptionRegistry = new DQLOrderDescriptionRegistry(RelListRepresentStudents.class, "e");

    public RelListRepresentStudentsDSHandler(String s) {
        super(s);
    }


    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context) {

        DQLSelectBuilder builder = new DQLSelectBuilder();

        builder.fromEntity(RelListRepresentStudents.class, "e").column("e");

        Object listorderEditId = context.get("listorderEditId");

        builder.where(eq(property(RelListRepresentStudents.representation().fromAlias("e")), commonValue(listorderEditId)));

        this._orderDescriptionRegistry.applyOrder(builder, input.getEntityOrder());

        return DQLSelectOutputBuilder.get(input, builder, getSession()).build();
    }


}
