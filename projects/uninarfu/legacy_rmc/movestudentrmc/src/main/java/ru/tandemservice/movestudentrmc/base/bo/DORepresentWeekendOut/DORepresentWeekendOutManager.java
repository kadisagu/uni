package ru.tandemservice.movestudentrmc.base.bo.DORepresentWeekendOut;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentWeekendOut.logic.DORepresentWeekendOutDAO;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentWeekendOut.ui.Edit.DORepresentWeekendOutEdit;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentWeekendOut.ui.View.DORepresentWeekendOutView;
import ru.tandemservice.movestudentrmc.docord.IDOObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IDOObjectModifyDAO;

@Configuration
public class DORepresentWeekendOutManager extends BusinessObjectManager implements IDOObjectByRepresentTypeManager {

    public static DORepresentWeekendOutManager instance()
    {
        return instance(DORepresentWeekendOutManager.class);
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return DORepresentWeekendOutEdit.class;
    }

    @Override
    @Bean
    public IDOObjectModifyDAO getDOObjectModifyDAO() {
        return new DORepresentWeekendOutDAO();
    }

    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return DORepresentWeekendOutView.class;
    }
}
