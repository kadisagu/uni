package ru.tandemservice.movestudentrmc.base.bo.ListRepresentSocialGrant.ui.View;


import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.support.AbstractListRepresentViewUI;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentSocialGrant.logic.ListPrintDoc;
import ru.tandemservice.movestudentrmc.entity.ListRepresentSocialGrant;

import java.util.Collections;

public class ListRepresentSocialGrantViewUI extends AbstractListRepresentViewUI<ListRepresentSocialGrant> {

    @Override
    public ListRepresentSocialGrant getListRepresentObject() {
        return new ListRepresentSocialGrant();
    }

    @Override
    protected RtfDocument printRepresent() {
        return new ListPrintDoc().creationPrintDocOrder(Collections.singletonList(getListRepresent()));
    }


}
