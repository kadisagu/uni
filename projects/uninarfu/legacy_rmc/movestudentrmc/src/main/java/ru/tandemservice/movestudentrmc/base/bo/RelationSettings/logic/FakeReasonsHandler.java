package ru.tandemservice.movestudentrmc.base.bo.RelationSettings.logic;

import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationBasement;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;

import java.util.List;

public class FakeReasonsHandler {

    private RepresentationReason reason;
    private List<RepresentationBasement> basicsList;
    private Boolean edit = false;

    public Boolean getEdit() {
        return edit;
    }

    public void setEdit(Boolean edit) {
        this.edit = edit;
    }

    public RepresentationReason getReason() {
        return reason;
    }

    public void setReason(RepresentationReason reason) {
        this.reason = reason;
    }

    public List<RepresentationBasement> getBasicsList() {
        return basicsList;
    }

    public void setBasicsList(List<RepresentationBasement> basicsList) {
        this.basicsList = basicsList;
    }
}
