package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.support;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.util.RtfUtil;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ListRepresentBaseManager;
import ru.tandemservice.movestudentrmc.dao.CheckStateUtil;
import ru.tandemservice.movestudentrmc.docord.IListObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.catalog.MovestudentExtractStates;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentExtractStatesCodes;
import ru.tandemservice.movestudentrmc.util.CheckUtil;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.util.ReportRenderer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = AbstractListRepresentViewUI.LISTORDER_EDIT_ID),
})
public abstract class AbstractListRepresentViewUI<T extends ListRepresent> extends UIPresenter {
    public static final String LISTORDER_EDIT_ID = "listorderEditId";

    private Long listorderEditId;
    private T listRepresent;
    private boolean displayWarning = false;
    ////////////////////////определить в потомках//////////////////////////////////////////////////////////////

    //экземпляр объекта представления  напр. [return new ListRepresentGrantCancel()] 
    public abstract T getListRepresentObject();

    /////////////////////////////события формы/////////////////////////////////////////////////////////

    @Override
    public void onComponentRefresh() {
        this.listRepresent = (T) UniDaoFacade.getCoreDao().get(getListRepresentObject().getClass(), getListorderEditId());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        dataSource.put(LISTORDER_EDIT_ID, getListorderEditId());
    }

    public void onClickApprove() {

        CheckStateUtil.checkStateRepresent(listRepresent, Arrays.asList(MovestudentExtractStatesCodes.CODE_1), _uiSupport.getSession());

        List<String> errorList = new ArrayList<String>();

        boolean ok = CheckUtil.checkListRepresent(getListRepresent(), errorList);

        if (ok) {
            approve(true);
        }
        else {
            setDisplayWarning(true);

            RtfDocument errorReport = RtfBean.getElementFactory().createRtfDocument();
            IRtfControl par = RtfBean.getElementFactory().createRtfControl(IRtfData.PAR);
            for (String text : errorList) {
                IRtfText studText = RtfBean.getElementFactory().createRtfText(text);
                errorReport.addElement(studText);
                errorReport.getElementList().addAll(Arrays.asList(par, par));
            }
            BusinessComponentUtils.downloadDocument(new ReportRenderer("Ошибки.rtf", errorReport), true);
        }
    }

    public void onCancel() {
        getListRepresent().setCheck(false);
        ListRepresentBaseManager.instance().modifyDao().updateState(getListRepresent());
        setDisplayWarning(false);
    }

    public void onIgnoreWarning() {
        approve(false);
        setDisplayWarning(false);
    }

    private void approve(boolean check) {
        getListRepresent().setState(DataAccessServices.dao().get(MovestudentExtractStates.class, MovestudentExtractStates.code().s(), MovestudentExtractStatesCodes.CODE_2));
        getListRepresent().setCheck(check);
        ListRepresentBaseManager.instance().modifyDao().updateState(getListRepresent());
    }

    public void onClickFormative() {

        CheckStateUtil.checkStateRepresent(listRepresent, Arrays.asList(MovestudentExtractStatesCodes.CODE_2), _uiSupport.getSession());

        getListRepresent().setState(DataAccessServices.dao().get(MovestudentExtractStates.class, MovestudentExtractStates.code().s(), "1"));
        ListRepresentBaseManager.instance().modifyDao().updateState(getListRepresent());
    }

    public void onClickDoApprove() {
        CheckStateUtil.checkStateRepresent(listRepresent, Arrays.asList(MovestudentExtractStatesCodes.CODE_2), _uiSupport.getSession());
        getListRepresent().setState(DataAccessServices.dao().get(MovestudentExtractStates.class, MovestudentExtractStates.code().s(), "3"));

        //сохраним печатную форму
        final RtfDocument docMain = printRepresent();
        if (docMain != null) {
            getListRepresent().setDocument(RtfUtil.toByteArray(docMain));
        }

        ListRepresentBaseManager.instance().modifyDao().updateState(getListRepresent());
    }

    public void onClickNotApprove() {
        CheckStateUtil.checkStateRepresent(listRepresent, Arrays.asList(MovestudentExtractStatesCodes.CODE_3), _uiSupport.getSession());
        listRepresent.setState(DataAccessServices.dao().get(MovestudentExtractStates.class, MovestudentExtractStates.code().s(), "2"));
        getListRepresent().setDocument(null);
        ListRepresentBaseManager.instance().modifyDao().updateState(getListRepresent());
    }


    public void onClickPrint() {
        try {
            IDocumentRenderer doc = documentRenderer();
            if (doc != null)
                BusinessComponentUtils.downloadDocument(doc, true);
        }
        catch (Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public void onClickEdit() {
        IListObjectByRepresentTypeManager representManager = ListRepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(getListRepresent().getRepresentationType().getCode());
        _uiActivation.asCurrent(representManager.getEditComponentManager())
                .parameter(AbstractListRepresentEditUI.LIST_REPRESENT_ID, getListorderEditId())
                .activate();
    }

    //Вспомогательные функции

    protected IDocumentRenderer documentRenderer() {
        if (getListRepresent().getDocument() == null) {
            RtfDocument docMain = printRepresent();
            return new ReportRenderer("Списочное представление.rtf", docMain, false);
        }
        else
            return new ReportRenderer("Списочное представление.rtf", getListRepresent().getDocument(), false);
    }

    protected abstract RtfDocument printRepresent();
    ///////////////////////////////////////set && get///////////////////////////////////////////////

    public Long getListorderEditId() {
        return listorderEditId;
    }

    public void setListorderEditId(Long listorderEditId) {
        this.listorderEditId = listorderEditId;
    }

    public T getListRepresent() {
        return listRepresent;
    }

    public void setListRepresent(T listRepresent) {
        this.listRepresent = listRepresent;
    }

    public boolean isDisplayWarning() {
        return displayWarning;
    }

    public void setDisplayWarning(boolean displayWarning) {
        this.displayWarning = displayWarning;
    }
}
