package ru.tandemservice.movestudentrmc.component.settings.OrderCategoryToRepresentationSettingsPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudentrmc.entity.catalog.OrderCategory;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;


@State({@Bind(key = "firstId", binding = "firstId")})
public class Model {

    private Long firstId;
    private OrderCategory orderCategory;
    private DynamicListDataSource<RepresentationType> dataSource;

    public Long getFirstId() {
        return firstId;
    }

    public void setFirstId(Long firstId) {
        this.firstId = firstId;
    }

    public OrderCategory getOrderCategory() {
        return orderCategory;
    }

    public void setOrderCategory(OrderCategory orderCategory) {
        this.orderCategory = orderCategory;
    }

    public DynamicListDataSource<RepresentationType> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<RepresentationType> dataSource) {
        this.dataSource = dataSource;
    }

}
