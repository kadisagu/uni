package ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrant.ui.Edit;

import com.google.common.collect.ImmutableList;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.ui.View.PersonView;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.StudentDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.StudentSelectedDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrant.ListRepresentGrantManager;
import ru.tandemservice.movestudentrmc.dao.CheckStateUtil;
import ru.tandemservice.movestudentrmc.entity.DocListRepresentBasics;
import ru.tandemservice.movestudentrmc.entity.ListRepresentGrant;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentExtractStatesCodes;
import ru.tandemservice.movestudentrmc.entity.catalog.*;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.*;


@State({
        @Bind(key = ListRepresentGrantEditUI.LIST_REPRESENT_ID, binding = ListRepresentGrantEditUI.LIST_REPRESENT_ID),
        @Bind(key = "typeRepresentFilter", binding = "typeRepresentFilter"),
        @Bind(key = "reasonRepresentFilter", binding = "reasonRepresentFilter"),
        @Bind(key = "basementRepresentFilter", binding = "basementRepresentFilter"),
        @Bind(key = "representationBasementDate", binding = "representationBasementDate"),
        @Bind(key = "representationBasementNumber", binding = "representationBasementNumber")
})
public class ListRepresentGrantEditUI extends UIPresenter {

    public static final String LIST_REPRESENT_ID = "listRepresentId";
    public static final Long ACADEM = 0L;
    public static final Long SOCIAL = 1L;

    private Long listRepresentId;
    private RepresentationType typeRepresentFilter;
    private RepresentationReason reasonRepresentFilter;
    private RepresentationBasement basementRepresentFilter;

    private Date _representationBasementDate;     // Дата основания

    private String _representationBasementNumber;     // № основания


    private ListRepresentGrant listRepresent;

    private List<Student> studentSelectedList;
    //private ISelectModel sessionResults;

    private ISelectModel grantsModel;
    //    События компонента

    @Override
    public void onComponentPrepareRender() {
        Boolean isSuccessfullyHandOverSession = (Boolean) (_uiSettings.get("isSuccessfullyHandOverSession") == null ? false : _uiSettings.get("isSuccessfullyHandOverSession"));

        Boolean isConsiderSessionResults = (Boolean) (_uiSettings.get("isConsiderSessionResults") == null ? false : _uiSettings.get("isConsiderSessionResults"));

        if (!isSuccessfullyHandOverSession) {
            _uiSettings.set("yearDistPartFilter", null);
            _uiSettings.set("educationYearFilter", null);
        }

        if (!isConsiderSessionResults) {
            _uiSettings.set("sessionMarksResult", null);

            _uiSettings.set(StudentDSHandler.YEAR_DIST_SESSION_RESULT, null);
            _uiSettings.set(StudentDSHandler.YEAR_EDU_SESSION_RESULT, null);
        }

        //super.onComponentPrepareRender();
    }


    @Override
    public void onComponentRefresh()
    {
        clearSettings();

        if (getListRepresentId() != null) {
            listRepresent = DataAccessServices.dao().get(ListRepresentGrant.class, ListRepresentGrant.id().s(), getListRepresentId());
            //-------------------------------------------------------------------------
            //_uiSettings.set("educationYearField", listRepresent.getEducationYear());
            //-------------------------------------------------------------------------
            _uiSettings.set("grantViewField", listRepresent.getGrantView());
            _uiSettings.set("dateBeginingPaymentField", listRepresent.getDateBeginingPayment());
            _uiSettings.set("dateEndOfPaymentField", listRepresent.getDateEndOfPayment());
            _uiSettings.set("fullEducationPeriod", listRepresent.getFullEducationPeriod());

            studentSelectedList = ListRepresentGrantManager.instance().getListObjectModifyDAO().selectRepresent(listRepresent);
        }
        else {
            listRepresent = new ListRepresentGrant();

            listRepresent.setRepresentationType(getTypeRepresentFilter());
            listRepresent.setRepresentationReason(getReasonRepresentFilter());
            listRepresent.setRepresentationBasement(getBasementRepresentFilter());
            listRepresent.setState(DataAccessServices.dao().get(MovestudentExtractStates.class, MovestudentExtractStates.code().s(), "1"));
            if (studentSelectedList == null)
                studentSelectedList = new ArrayList<>();
        }

        setGrantsModel(new LazySimpleSelectModel<>(ImmutableList.of(
                new IdentifiableWrapper(ACADEM, "с академической стипендией"),
                new IdentifiableWrapper(SOCIAL, "без академической стипендии"))));

    }

    @Override
    public void onAfterDataSourceFetch(IUIDataSource iuidatasource) {
        super.onAfterDataSourceFetch(iuidatasource);
    }


    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        Map<String, Object> settingMap = _uiSettings.getAsMap(
                "educationYearFilter",
                "yearDistPartFilter",
                StudentDSHandler.YEAR_DIST_SESSION_RESULT,
                StudentDSHandler.YEAR_EDU_SESSION_RESULT,
                "territorialOrgUnitFilter",
                StudentDSHandler.FORMATIVE_ORG_UNIT_FILTER,
                "educationLevelsHighSchoolDSFilter",
                "educationOrgUnitFilter",
                "educationLevelFilter",
                StudentDSHandler.QUALIFICATION_FILTER,
                "courseFilter",
                "groupFilter",
                StudentDSHandler.DEVELOP_FORM_FILTER,
                "compensationTypeFilter",
                "benefitFilter",
                StudentDSHandler.STUDENT_FIO_FILTER,
                "educationYearField",
                "isSuccessfullyHandOverSession",
                "sessionMarksResult",
                "isConsiderSessionResults",
                "isforeignCitizens",
                "studentFilter",
                "userClickApply",
                "studentStatusFilter",
                "validityOfDocumentFilter",
                "grantsFilter",
                "monthFilter"
        );
        dataSource.putAll(settingMap);
        dataSource.put(StudentDSHandler.STUDENT_SELECTED_LIST, studentSelectedList);
        dataSource.put(StudentDSHandler.ORDER_ID, getListRepresentId());

    }

    //    События формы

    public void selectRepresent() {

        BaseSearchListDataSource representDS = (BaseSearchListDataSource) getConfig().getDataSource(ListRepresentGrantEdit.STUDENT_DS);

        Collection<IEntity> records = ((CheckboxColumn) representDS.getLegacyDataSource().getColumn(StudentDSHandler.CHECKBOX_COLUMN)).getSelectedObjects();

        for (IEntity record : records) {
            if (!studentSelectedList.contains((Student) record))
                studentSelectedList.add((Student) record);
        }

    }

    public void deleteSelectedRepresent() {

        BaseSearchListDataSource representDS = (BaseSearchListDataSource) getConfig().getDataSource(ListRepresentGrantEdit.STUDENT_SELECTED_DS);

        Collection<IEntity> records = ((CheckboxColumn) representDS.getLegacyDataSource().getColumn(StudentSelectedDSHandler.CHECKBOX_COLUMN)).getSelectedObjects();

        for (IEntity record : records) {
            studentSelectedList.remove((Student) record);
        }
    }

    public void onClickSave() {

        if (getListRepresentId() != null)
            CheckStateUtil.checkStateRepresent(getListRepresentGrant(), ImmutableList.of(MovestudentExtractStatesCodes.CODE_1), _uiSupport.getSession());

        if (studentSelectedList.isEmpty())
            getConfig().getUserContext().getErrorCollector().add(getConfig().getProperty("ui.alertEmptySelectedRepresent"));


        EducationYear educationYear = _uiSettings.get("educationYearField", EducationYear.class);
        Date dateBeginingPaymentField = _uiSettings.get("dateBeginingPaymentField", Date.class);
        Date dateEndOfPaymentField = _uiSettings.get("dateEndOfPaymentField", Date.class);
        GrantView grantView = _uiSettings.get("grantViewField", GrantView.class);
        //Boolean isFullEducationPeriod = _uiSettings.get("fullEducationPeriod", Boolean.class);

        //listRepresent.setFullEducationPeriod(isFullEducationPeriod);
        listRepresent.setCreateDate(getSupport().getCurrentDate());
        //-----------------------------------------------
        //listRepresent.setEducationYear(educationYear);
        //-----------------------------------------------
        listRepresent.setDateBeginingPayment(dateBeginingPaymentField);
        listRepresent.setDateEndOfPayment(dateEndOfPaymentField);
    /*	if(!isFullEducationPeriod)
			listRepresent.setDateEndOfPayment(dateEndOfPaymentField);
	*/
        listRepresent.setGrantView(grantView);
        if (getConfig().getUserContext().getErrorCollector().hasErrors())
            return;

        //if (getListRepresentId() == null)

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocListRepresentBasics.class, "b").addColumn("b");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(DocListRepresentBasics.listRepresent().fromAlias("b")),
                DQLExpressions.value(listRepresent)));

        List<DocListRepresentBasics> representOldBasicsList = builder.createStatement(getSupport().getSession()).list();

        DocListRepresentBasics representBasics;

        if (representOldBasicsList.size() > 0) {
            representBasics = representOldBasicsList.get(0);
        }
        else {
            representBasics = new DocListRepresentBasics();
            representBasics.setListRepresent(listRepresent);
            representBasics.setBasic(listRepresent.getRepresentationBasement());
            representBasics.setReason(listRepresent.getRepresentationReason());

            representBasics.setRepresentationBasementDate(_representationBasementDate);
            representBasics.setRepresentationBasementNumber(_representationBasementNumber);
        }

        ListRepresentGrantManager.instance().getListObjectModifyDAO().save(listRepresent, studentSelectedList, representBasics);
		
/*		else
			ListRepresentGrantManager.instance().getListObjectModifyDAO().update(listRepresent, studentSelectedList);
*/
        deactivate(2);
    }

    //    Вспомогательные функции


    //    Getters and Setters

    public void onClickView()
    {
        try {
            Student st = DataAccessServices.dao().get(Student.class, (Long) getListenerParameter());
            _uiActivation.asRegionDialog(PersonView.class)
                    .parameter("publisherId", st.getPerson().getId())
                    .activate();
        }
        catch (Exception e) {
            getUserContext().getErrorCollector().add(e.getLocalizedMessage());
        }


    }

    @Override
    public void saveSettings() {
        _uiSettings.set("userClickApply", true);
        super.saveSettings();
    }

    public ListRepresentGrant getListRepresentGrant() {
        return listRepresent;
    }

    public void setListRepresentGrant(ListRepresentGrant listRepresent) {
        this.listRepresent = listRepresent;
    }

    public void onViewRepresentationFromList() {
		/*
    	if (_orderId != null) {
        	List<DataWrapper> representSelect = ListRepresentGrantManager.instance().modifyDao().selectRepresent(_order);

        	String type = "";
        	for (DataWrapper representW : representSelect) {
        		Representation repr = (Representation) representW.getProperty("represent");
        		if (repr.getId() == getListenerParameterAsLong()) {
        			type = repr.getType().getCode();
        			break;
        		}        		
        	}
        	String componentName = "";
        	this.getUserContext().getCurrentComponent().getParentRegion().activate(new ComponentActivator(componentName, (new UniMap()).add("representationId", getListenerParameter())));
        } 
		 */
    }


    public boolean isSelectGrantsFilter() {
        return _uiSettings.get("grantsFilter") != null;
    }


    public boolean getDisableSessionResultFilters() {
        List<OrgUnit> fou = _uiSettings.get("formativeOrgUnitFilter");
        List<OrgUnit> el = _uiSettings.get("educationLevelsHighSchoolDSFilter");
        List<OrgUnit> tou = _uiSettings.get("territorialOrgUnitFilter");
        List<OrgUnit> eou = _uiSettings.get("educationOrgUnitFilter");
        List<Group> group = _uiSettings.get("groupFilter");
        if (
                (
                        fou != null && fou.size() > 0) ||
                        (tou != null && tou.size() > 0) ||
                        (el != null && el.size() > 0) ||
                        (eou != null && eou.size() > 0) ||
                        (group != null && group.size() > 0)
                )
        {
            return false;
        }
        else {
            _uiSettings.set("isSuccessfullyHandOverSession", false);
            _uiSettings.set("isConsiderSessionResults", false);
            return true;
        }

    }

    public RepresentationType getTypeRepresentFilter() {
        return typeRepresentFilter;
    }

    public void setTypeRepresentFilter(RepresentationType typeRepresentFilter) {
        this.typeRepresentFilter = typeRepresentFilter;
    }

    public RepresentationReason getReasonRepresentFilter() {
        return reasonRepresentFilter;
    }

    public void setReasonRepresentFilter(RepresentationReason reasonRepresentFilter) {
        this.reasonRepresentFilter = reasonRepresentFilter;
    }

    public RepresentationBasement getBasementRepresentFilter() {
        return basementRepresentFilter;
    }

    public void setBasementRepresentFilter(RepresentationBasement basementRepresentFilter) {
        this.basementRepresentFilter = basementRepresentFilter;
    }

    public Long getListRepresentId() {
        return listRepresentId;
    }

    public void setListRepresentId(Long listRepresentId) {
        this.listRepresentId = listRepresentId;
    }

    public Date getRepresentationBasementDate()
    {
        return _representationBasementDate;
    }

    public void setRepresentationBasementDate(Date representationBasementDate)
    {
        _representationBasementDate = representationBasementDate;
    }

    public String getRepresentationBasementNumber()
    {
        return _representationBasementNumber;
    }

    public void setRepresentationBasementNumber(String representationBasementNumber)
    {
        _representationBasementNumber = representationBasementNumber;
    }


    public ISelectModel getGrantsModel() {
        return grantsModel;
    }


    public void setGrantsModel(ISelectModel grantsModel) {
        this.grantsModel = grantsModel;
    }

}
