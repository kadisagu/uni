package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import ru.tandemservice.movestudentrmc.entity.IStudentPractice;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListRepresentPractice;
import ru.tandemservice.movestudentrmc.entity.catalog.PracticePaymentData;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Списочное представление о направлении на практику
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ListRepresentPracticeGen extends ListRepresent
 implements IStudentPractice{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.ListRepresentPractice";
    public static final String ENTITY_NAME = "listRepresentPractice";
    public static final int VERSION_HASH = 1588118484;
    private static IEntityMeta ENTITY_META;

    public static final String P_PRACTICE_TITLE = "practiceTitle";
    public static final String P_DATE_BEGINNING_PRACTICE = "dateBeginningPractice";
    public static final String P_DATE_END_OF_PRACTICE = "dateEndOfPractice";
    public static final String L_PAYMENT_DATA = "paymentData";
    public static final String L_REGISTRY_ELEMENT_ROW = "registryElementRow";
    public static final String L_SUPERVISOR = "supervisor";

    private String _practiceTitle;     // Наименование практики
    private Date _dateBeginningPractice;     // Дата начала практики
    private Date _dateEndOfPractice;     // Дата окончания практики
    private PracticePaymentData _paymentData;     // Данные об оплате
    private EppWorkPlanRow _registryElementRow;     // Мероприятие реестра
    private Employee _supervisor;     // Руководитель от университета

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Наименование практики. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPracticeTitle()
    {
        return _practiceTitle;
    }

    /**
     * @param practiceTitle Наименование практики. Свойство не может быть null.
     */
    public void setPracticeTitle(String practiceTitle)
    {
        dirty(_practiceTitle, practiceTitle);
        _practiceTitle = practiceTitle;
    }

    /**
     * @return Дата начала практики. Свойство не может быть null.
     */
    @NotNull
    public Date getDateBeginningPractice()
    {
        return _dateBeginningPractice;
    }

    /**
     * @param dateBeginningPractice Дата начала практики. Свойство не может быть null.
     */
    public void setDateBeginningPractice(Date dateBeginningPractice)
    {
        dirty(_dateBeginningPractice, dateBeginningPractice);
        _dateBeginningPractice = dateBeginningPractice;
    }

    /**
     * @return Дата окончания практики. Свойство не может быть null.
     */
    @NotNull
    public Date getDateEndOfPractice()
    {
        return _dateEndOfPractice;
    }

    /**
     * @param dateEndOfPractice Дата окончания практики. Свойство не может быть null.
     */
    public void setDateEndOfPractice(Date dateEndOfPractice)
    {
        dirty(_dateEndOfPractice, dateEndOfPractice);
        _dateEndOfPractice = dateEndOfPractice;
    }

    /**
     * @return Данные об оплате.
     */
    public PracticePaymentData getPaymentData()
    {
        return _paymentData;
    }

    /**
     * @param paymentData Данные об оплате.
     */
    public void setPaymentData(PracticePaymentData paymentData)
    {
        dirty(_paymentData, paymentData);
        _paymentData = paymentData;
    }

    /**
     * @return Мероприятие реестра.
     */
    public EppWorkPlanRow getRegistryElementRow()
    {
        return _registryElementRow;
    }

    /**
     * @param registryElementRow Мероприятие реестра.
     */
    public void setRegistryElementRow(EppWorkPlanRow registryElementRow)
    {
        dirty(_registryElementRow, registryElementRow);
        _registryElementRow = registryElementRow;
    }

    /**
     * @return Руководитель от университета.
     */
    public Employee getSupervisor()
    {
        return _supervisor;
    }

    /**
     * @param supervisor Руководитель от университета.
     */
    public void setSupervisor(Employee supervisor)
    {
        dirty(_supervisor, supervisor);
        _supervisor = supervisor;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ListRepresentPracticeGen)
        {
            setPracticeTitle(((ListRepresentPractice)another).getPracticeTitle());
            setDateBeginningPractice(((ListRepresentPractice)another).getDateBeginningPractice());
            setDateEndOfPractice(((ListRepresentPractice)another).getDateEndOfPractice());
            setPaymentData(((ListRepresentPractice)another).getPaymentData());
            setRegistryElementRow(((ListRepresentPractice)another).getRegistryElementRow());
            setSupervisor(((ListRepresentPractice)another).getSupervisor());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ListRepresentPracticeGen> extends ListRepresent.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ListRepresentPractice.class;
        }

        public T newInstance()
        {
            return (T) new ListRepresentPractice();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "practiceTitle":
                    return obj.getPracticeTitle();
                case "dateBeginningPractice":
                    return obj.getDateBeginningPractice();
                case "dateEndOfPractice":
                    return obj.getDateEndOfPractice();
                case "paymentData":
                    return obj.getPaymentData();
                case "registryElementRow":
                    return obj.getRegistryElementRow();
                case "supervisor":
                    return obj.getSupervisor();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "practiceTitle":
                    obj.setPracticeTitle((String) value);
                    return;
                case "dateBeginningPractice":
                    obj.setDateBeginningPractice((Date) value);
                    return;
                case "dateEndOfPractice":
                    obj.setDateEndOfPractice((Date) value);
                    return;
                case "paymentData":
                    obj.setPaymentData((PracticePaymentData) value);
                    return;
                case "registryElementRow":
                    obj.setRegistryElementRow((EppWorkPlanRow) value);
                    return;
                case "supervisor":
                    obj.setSupervisor((Employee) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "practiceTitle":
                        return true;
                case "dateBeginningPractice":
                        return true;
                case "dateEndOfPractice":
                        return true;
                case "paymentData":
                        return true;
                case "registryElementRow":
                        return true;
                case "supervisor":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "practiceTitle":
                    return true;
                case "dateBeginningPractice":
                    return true;
                case "dateEndOfPractice":
                    return true;
                case "paymentData":
                    return true;
                case "registryElementRow":
                    return true;
                case "supervisor":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "practiceTitle":
                    return String.class;
                case "dateBeginningPractice":
                    return Date.class;
                case "dateEndOfPractice":
                    return Date.class;
                case "paymentData":
                    return PracticePaymentData.class;
                case "registryElementRow":
                    return EppWorkPlanRow.class;
                case "supervisor":
                    return Employee.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ListRepresentPractice> _dslPath = new Path<ListRepresentPractice>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ListRepresentPractice");
    }
            

    /**
     * @return Наименование практики. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentPractice#getPracticeTitle()
     */
    public static PropertyPath<String> practiceTitle()
    {
        return _dslPath.practiceTitle();
    }

    /**
     * @return Дата начала практики. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentPractice#getDateBeginningPractice()
     */
    public static PropertyPath<Date> dateBeginningPractice()
    {
        return _dslPath.dateBeginningPractice();
    }

    /**
     * @return Дата окончания практики. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentPractice#getDateEndOfPractice()
     */
    public static PropertyPath<Date> dateEndOfPractice()
    {
        return _dslPath.dateEndOfPractice();
    }

    /**
     * @return Данные об оплате.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentPractice#getPaymentData()
     */
    public static PracticePaymentData.Path<PracticePaymentData> paymentData()
    {
        return _dslPath.paymentData();
    }

    /**
     * @return Мероприятие реестра.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentPractice#getRegistryElementRow()
     */
    public static EppWorkPlanRow.Path<EppWorkPlanRow> registryElementRow()
    {
        return _dslPath.registryElementRow();
    }

    /**
     * @return Руководитель от университета.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentPractice#getSupervisor()
     */
    public static Employee.Path<Employee> supervisor()
    {
        return _dslPath.supervisor();
    }

    public static class Path<E extends ListRepresentPractice> extends ListRepresent.Path<E>
    {
        private PropertyPath<String> _practiceTitle;
        private PropertyPath<Date> _dateBeginningPractice;
        private PropertyPath<Date> _dateEndOfPractice;
        private PracticePaymentData.Path<PracticePaymentData> _paymentData;
        private EppWorkPlanRow.Path<EppWorkPlanRow> _registryElementRow;
        private Employee.Path<Employee> _supervisor;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Наименование практики. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentPractice#getPracticeTitle()
     */
        public PropertyPath<String> practiceTitle()
        {
            if(_practiceTitle == null )
                _practiceTitle = new PropertyPath<String>(ListRepresentPracticeGen.P_PRACTICE_TITLE, this);
            return _practiceTitle;
        }

    /**
     * @return Дата начала практики. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentPractice#getDateBeginningPractice()
     */
        public PropertyPath<Date> dateBeginningPractice()
        {
            if(_dateBeginningPractice == null )
                _dateBeginningPractice = new PropertyPath<Date>(ListRepresentPracticeGen.P_DATE_BEGINNING_PRACTICE, this);
            return _dateBeginningPractice;
        }

    /**
     * @return Дата окончания практики. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentPractice#getDateEndOfPractice()
     */
        public PropertyPath<Date> dateEndOfPractice()
        {
            if(_dateEndOfPractice == null )
                _dateEndOfPractice = new PropertyPath<Date>(ListRepresentPracticeGen.P_DATE_END_OF_PRACTICE, this);
            return _dateEndOfPractice;
        }

    /**
     * @return Данные об оплате.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentPractice#getPaymentData()
     */
        public PracticePaymentData.Path<PracticePaymentData> paymentData()
        {
            if(_paymentData == null )
                _paymentData = new PracticePaymentData.Path<PracticePaymentData>(L_PAYMENT_DATA, this);
            return _paymentData;
        }

    /**
     * @return Мероприятие реестра.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentPractice#getRegistryElementRow()
     */
        public EppWorkPlanRow.Path<EppWorkPlanRow> registryElementRow()
        {
            if(_registryElementRow == null )
                _registryElementRow = new EppWorkPlanRow.Path<EppWorkPlanRow>(L_REGISTRY_ELEMENT_ROW, this);
            return _registryElementRow;
        }

    /**
     * @return Руководитель от университета.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentPractice#getSupervisor()
     */
        public Employee.Path<Employee> supervisor()
        {
            if(_supervisor == null )
                _supervisor = new Employee.Path<Employee>(L_SUPERVISOR, this);
            return _supervisor;
        }

        public Class getEntityClass()
        {
            return ListRepresentPractice.class;
        }

        public String getEntityName()
        {
            return "listRepresentPractice";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
