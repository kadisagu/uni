package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListRepresentExclude;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Списочное представление об отчислении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ListRepresentExcludeGen extends ListRepresent
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.ListRepresentExclude";
    public static final String ENTITY_NAME = "listRepresentExclude";
    public static final int VERSION_HASH = -428941901;
    private static IEntityMeta ENTITY_META;

    public static final String P_DATE_EXCLUDE = "dateExclude";

    private Date _dateExclude;     // Дата отчисления

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     */
    @NotNull
    public Date getDateExclude()
    {
        return _dateExclude;
    }

    /**
     * @param dateExclude Дата отчисления. Свойство не может быть null.
     */
    public void setDateExclude(Date dateExclude)
    {
        dirty(_dateExclude, dateExclude);
        _dateExclude = dateExclude;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ListRepresentExcludeGen)
        {
            setDateExclude(((ListRepresentExclude)another).getDateExclude());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ListRepresentExcludeGen> extends ListRepresent.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ListRepresentExclude.class;
        }

        public T newInstance()
        {
            return (T) new ListRepresentExclude();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "dateExclude":
                    return obj.getDateExclude();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "dateExclude":
                    obj.setDateExclude((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "dateExclude":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "dateExclude":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "dateExclude":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ListRepresentExclude> _dslPath = new Path<ListRepresentExclude>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ListRepresentExclude");
    }
            

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentExclude#getDateExclude()
     */
    public static PropertyPath<Date> dateExclude()
    {
        return _dslPath.dateExclude();
    }

    public static class Path<E extends ListRepresentExclude> extends ListRepresent.Path<E>
    {
        private PropertyPath<Date> _dateExclude;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentExclude#getDateExclude()
     */
        public PropertyPath<Date> dateExclude()
        {
            if(_dateExclude == null )
                _dateExclude = new PropertyPath<Date>(ListRepresentExcludeGen.P_DATE_EXCLUDE, this);
            return _dateExclude;
        }

        public Class getEntityClass()
        {
            return ListRepresentExclude.class;
        }

        public String getEntityName()
        {
            return "listRepresentExclude";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
