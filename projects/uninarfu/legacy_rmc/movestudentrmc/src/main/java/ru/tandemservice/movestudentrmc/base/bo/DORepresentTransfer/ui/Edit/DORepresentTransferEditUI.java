package ru.tandemservice.movestudentrmc.base.bo.DORepresentTransfer.ui.Edit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.*;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentEditUI;
import ru.tandemservice.movestudentrmc.docord.util.MoveStudentUtil;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.RepresentTransfer;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;

import java.io.IOException;
import java.util.List;

public class DORepresentTransferEditUI extends AbstractDORepresentEditUI
{

    private EducationOrgUnit eduOrgUnit = new EducationOrgUnit();
    private boolean showAllGroups = false;

    @Override
    protected void FetchStudents(Representation doc) {
        _studentDocList.clear();
        _studentList.clear();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocRepresentStudentBase.class, "stud").column("stud");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(DocRepresentStudentBase.representation().fromAlias("stud")),
                DQLExpressions.value(doc.getId())));

        List<DocRepresentStudentBase> studentList = builder.createStatement(getSupport().getSession()).list();
        for (DocRepresentStudentBase student : studentList) {

            _studentDocList.add(student);
            _studentList.add(student.getStudent());
        }

        _student = _studentList.get(0);
    }

    @Override
    protected void initDocument() {
        super.initDocument();
        setStudentStatusNewStr(_student.getStatus().getTitle());

        final RepresentTransfer rep = (RepresentTransfer) _representObj;

        if (rep.getId() != null && rep.getEducationOrgUnit() != null) {
            getEduOrgUnit().update(rep.getEducationOrgUnit());
            setShowAllGroups(!getEduOrgUnit().getEducationLevelHighSchool().equals(rep.getGroup().getEducationOrgUnit().getEducationLevelHighSchool()));
        }
        else {
            getEduOrgUnit().update(_student.getEducationOrgUnit());
            rep.setCompensationType(_student.getCompensationType());
        }
        //save old values
        rep.setOldEducationOrgUnitId(_student.getEducationOrgUnit().getId());
        if (_student.getCompensationType() != null)
            rep.setOldCompensationTypeId(_student.getCompensationType().getId());
        if (_student.getCourse() != null)
            rep.setOldCourseId(_student.getCourse().getId());
        if (_student.getGroup() != null)
            rep.setOldGroupId(_student.getGroup().getId());
    }

    @Override
    protected void syncPreCommitData(DocRepresentStudentBase studentDoc, Student student) {
        super.syncPreCommitData(studentDoc, student);

        String compensationTypeStr = "за счет средств федерального бюджета";
        if (UniDefines.COMPENSATION_TYPE_CONTRACT.equals(student.getCompensationType().getCode()))
            compensationTypeStr = "по договору";
        studentDoc.setCompensationTypeStr(compensationTypeStr);
    }


    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        super.onBeforeDataSourceFetch(dataSource);

        if (DORepresentTransferEdit.GROUP_DS.equals(dataSource.getName())) {
            dataSource.put(GroupDSHandler.SHOW_ALL_GROUPS, isShowAllGroups());
            dataSource.put(GroupDSHandler.NEW_FORMATIVE_ORG_UNIT, getEduOrgUnit().getFormativeOrgUnit());
            dataSource.put(GroupDSHandler.NEW_TERRITORIAL_ORG_UNIT, getEduOrgUnit().getTerritorialOrgUnit());
            dataSource.put(GroupDSHandler.EDUCATION_LEVELS_HIGH_SCHOOL, getEduOrgUnit().getEducationLevelHighSchool());
            dataSource.put(GroupDSHandler.DEVELOP_FORM, getEduOrgUnit().getDevelopForm());
            dataSource.put(GroupDSHandler.DEVELOP_CONDITION, getEduOrgUnit().getDevelopCondition());
            dataSource.put(GroupDSHandler.DEVELOP_PERIOD, getEduOrgUnit().getDevelopPeriod());
            dataSource.put(GroupDSHandler.DEVELOP_TECH, getEduOrgUnit().getDevelopTech());
            dataSource.put(GroupDSHandler.COURSE, getRepresentTransfer().getCourse());
        }

        if (DORepresentTransferEdit.EDU_LEVEL_DS.equals(dataSource.getName())) {
            dataSource.put(EducationLevelsHighSchoolDSHandler.FILTER_BY_DEVELOP, Boolean.FALSE);
            dataSource.put(EducationLevelsHighSchoolDSHandler.NEW_TERRITORIAL_ORG_UNIT, getEduOrgUnit().getTerritorialOrgUnit());
            dataSource.put(EducationLevelsHighSchoolDSHandler.NEW_FORMATIVE_ORG_UNIT, getEduOrgUnit().getFormativeOrgUnit());
        }

        if (DORepresentTransferEdit.DEVELOP_FORM_DS.equals(dataSource.getName())) {
            dataSource.put(DevelopFormDSHandler.NEW_FORMATIVE_ORG_UNIT, getEduOrgUnit().getFormativeOrgUnit());
            dataSource.put(DevelopFormDSHandler.NEW_TERRITORIAL_ORG_UNIT, getEduOrgUnit().getTerritorialOrgUnit());
            dataSource.put(DevelopFormDSHandler.EDUCATION_LEVELS_HIGH_SCHOOL, getEduOrgUnit().getEducationLevelHighSchool());
        }

        if (DORepresentTransferEdit.DEVELOP_CONDITION_DS.equals(dataSource.getName())) {
            dataSource.put(DevelopConditionDSHandler.NEW_FORMATIVE_ORG_UNIT, getEduOrgUnit().getFormativeOrgUnit());
            dataSource.put(DevelopConditionDSHandler.NEW_TERRITORIAL_ORG_UNIT, getEduOrgUnit().getTerritorialOrgUnit());
            dataSource.put(DevelopConditionDSHandler.EDUCATION_LEVELS_HIGH_SCHOOL, getEduOrgUnit().getEducationLevelHighSchool());
            dataSource.put(DevelopConditionDSHandler.DEVELOP_FORM, getEduOrgUnit().getDevelopForm());
        }

        if (DORepresentTransferEdit.DEVELOP_PERIOD_DS.equals(dataSource.getName())) {
            dataSource.put(DevelopPeriodDSHandler.NEW_FORMATIVE_ORG_UNIT, getEduOrgUnit().getFormativeOrgUnit());
            dataSource.put(DevelopPeriodDSHandler.NEW_TERRITORIAL_ORG_UNIT, getEduOrgUnit().getTerritorialOrgUnit());
            dataSource.put(DevelopPeriodDSHandler.EDUCATION_LEVELS_HIGH_SCHOOL, getEduOrgUnit().getEducationLevelHighSchool());
            dataSource.put(DevelopPeriodDSHandler.DEVELOP_FORM, getEduOrgUnit().getDevelopForm());
            dataSource.put(DevelopPeriodDSHandler.DEVELOP_CONDITION, getEduOrgUnit().getDevelopCondition());
        }

        if (DORepresentTransferEdit.DEVELOP_TECH_DS.equals(dataSource.getName())) {
            dataSource.put(DevelopTechDSHandler.NEW_FORMATIVE_ORG_UNIT, getEduOrgUnit().getFormativeOrgUnit());
            dataSource.put(DevelopTechDSHandler.NEW_TERRITORIAL_ORG_UNIT, getEduOrgUnit().getTerritorialOrgUnit());
            dataSource.put(DevelopTechDSHandler.EDUCATION_LEVELS_HIGH_SCHOOL, getEduOrgUnit().getEducationLevelHighSchool());
            dataSource.put(DevelopTechDSHandler.DEVELOP_FORM, getEduOrgUnit().getDevelopForm());
            dataSource.put(DevelopTechDSHandler.DEVELOP_CONDITION, getEduOrgUnit().getDevelopCondition());
            dataSource.put(DevelopTechDSHandler.DEVELOP_PERIOD, getEduOrgUnit().getDevelopPeriod());
        }
    }

    @Override
    public void onClickSave() throws IOException {
        EducationOrgUnit educationOrgUnit = MoveStudentUtil.getEducationOrgUnit(getEduOrgUnit());
        if (educationOrgUnit == null)
            throw new ApplicationException("Подходящее направление подготовки (специальность) подразделения не найдено.");
        final RepresentTransfer rep = (RepresentTransfer) _representObj;
        rep.setEducationOrgUnit(educationOrgUnit);

        boolean ok = MoveStudentUtil.isChecked(getStudentList(), rep.getCompensationType(), rep.getEducationOrgUnit().getDevelopForm());

        if ((ok && !isSave()) || (ok && isSave() && isWarning())) {
            String mess = MoveStudentUtil.checked(rep.getStartDate(), false);
            if (!StringUtils.isEmpty(mess)) {
                setSave(true);
                setWarning(true);
                setWarningMessage(mess);
                return;
            }
        }

        super.onClickSave();
    }

    public void onChangeGroup() {
        if (getRepresentTransfer().getCourse() == null)
            getRepresentTransfer().setCourse(getRepresentTransfer().getGroup().getCourse());
    }

    public RepresentTransfer getRepresentTransfer() {
        return (RepresentTransfer) this.getRepresentObj();
    }

    public EducationOrgUnit getEduOrgUnit() {
        return eduOrgUnit;
    }

    public void setEduOrgUnit(EducationOrgUnit eduOrgUnit) {
        this.eduOrgUnit = eduOrgUnit;
    }

    public boolean isShowAllGroups() {
        return showAllGroups;
    }

    public void setShowAllGroups(boolean showAllGroups) {
        this.showAllGroups = showAllGroups;
    }

}
