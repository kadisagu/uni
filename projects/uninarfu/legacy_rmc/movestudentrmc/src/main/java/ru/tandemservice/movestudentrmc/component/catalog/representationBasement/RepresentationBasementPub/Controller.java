package ru.tandemservice.movestudentrmc.component.catalog.representationBasement.RepresentationBasementPub;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationBasement;

public class Controller extends DefaultCatalogPubController<RepresentationBasement, Model, IDAO> {

    @Override
    protected DynamicListDataSource<RepresentationBasement> createListDataSource(IBusinessComponent context) {
        Model model = getModel(context);
        DynamicListDataSource<RepresentationBasement> ds = new DynamicListDataSource<RepresentationBasement>(context, this);
        ds.addColumn(new SimpleColumn("Наименование", "title").setClickable(false).setOrderable(true), 2);
        ds.addColumn(new SimpleColumn("Номер", "priority").setClickable(false).setOrderable(true), 3);
        ds.addColumn(new ToggleColumn("Печатать", Model.RepresentationBasementWrapper.PRINTABLE).setListener("onClickTogglePrintable"), 4);
        ds.addColumn(new ToggleColumn("Требуется примечание", Model.RepresentationBasementWrapper.NOTE_REQUIRED).setListener("onClickToggleNoteRequired"), 4);
        ds.addColumn(new ToggleColumn("Связан с документом", Model.RepresentationBasementWrapper.CONNECTED_WITH_DOCUMENT).setListener("onClickToggleConnectedWithDocument"), 4);
        ds.addColumn(new ToggleColumn("Подлежит объединению", Model.RepresentationBasementWrapper.MERGED).setListener("onClickToggleMerged"), 4);
        ds.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
        if (model.isUserCatalog())
            ds.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", ICatalogItem.CATALOG_ITEM_TITLE).setPermissionKey(model.getCatalogItemDelete()).setDisableHandler(model.getDisabledEntityHandler()));
        ds.setOrder("priority", OrderDirection.asc);
        return ds;
    }

    public void onClickTogglePrintable(IBusinessComponent component)
    {
        getDao().updatePrintable(getModel(component), (Long) component.getListenerParameter());
    }

    public void onClickToggleNoteRequired(IBusinessComponent component)
    {
        getDao().updateNoteRequired(getModel(component), (Long) component.getListenerParameter());
    }

    public void onClickToggleMerged(IBusinessComponent component)
    {
        getDao().updateMerged(getModel(component), (Long) component.getListenerParameter());
    }

    public void onClickToggleConnectedWithDocument(IBusinessComponent component)
    {
        getDao().updateConnectedWithDocument(getModel(component), (Long) component.getListenerParameter());
    }


}
