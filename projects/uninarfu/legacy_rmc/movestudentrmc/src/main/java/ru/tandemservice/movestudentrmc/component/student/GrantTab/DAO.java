package ru.tandemservice.movestudentrmc.component.student.GrantTab;

import ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab.MonthWrapper;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.ArrayList;
import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        model.setStudent(getNotNull(Student.class, model.getStudent().getId()));
        model.setEduYearList(getCatalogItemList(EducationYear.class));
        if (model.getEduYear() == null)
            model.setEduYear(get(EducationYear.class, EducationYear.current().s(), Boolean.valueOf(true)));

    }

    @Override
    public void prepareListDataSource(Model model) {
        List<MonthWrapper> months = new ArrayList<MonthWrapper>();
        for (int i = 0; i < 12; i++) {
            MonthWrapper wrapper = new MonthWrapper(i);
            months.add(wrapper);
        }

        UniUtils.createPage(model.getDataSource(), months);
    }


}
