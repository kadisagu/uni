package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListRepresentHistoryItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность хранит информацию для отката списочного представления
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ListRepresentHistoryItemGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.ListRepresentHistoryItem";
    public static final String ENTITY_NAME = "listRepresentHistoryItem";
    public static final int VERSION_HASH = -1738622790;
    private static IEntityMeta ENTITY_META;

    public static final String L_LIST_REPRESENT = "listRepresent";
    public static final String P_NUMBER = "number";
    public static final String P_FIELDNAME = "fieldname";
    public static final String P_VALUE = "value";

    private ListRepresent _listRepresent;     // Представление
    private long _number;     // Номер записи
    private String _fieldname;     // Имя поля
    private String _value;     // Значение поля

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Представление. Свойство не может быть null.
     */
    @NotNull
    public ListRepresent getListRepresent()
    {
        return _listRepresent;
    }

    /**
     * @param listRepresent Представление. Свойство не может быть null.
     */
    public void setListRepresent(ListRepresent listRepresent)
    {
        dirty(_listRepresent, listRepresent);
        _listRepresent = listRepresent;
    }

    /**
     * @return Номер записи. Свойство не может быть null.
     */
    @NotNull
    public long getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер записи. Свойство не может быть null.
     */
    public void setNumber(long number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Имя поля. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFieldname()
    {
        return _fieldname;
    }

    /**
     * @param fieldname Имя поля. Свойство не может быть null.
     */
    public void setFieldname(String fieldname)
    {
        dirty(_fieldname, fieldname);
        _fieldname = fieldname;
    }

    /**
     * @return Значение поля.
     */
    public String getValue()
    {
        return _value;
    }

    /**
     * @param value Значение поля.
     */
    public void setValue(String value)
    {
        dirty(_value, value);
        _value = value;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ListRepresentHistoryItemGen)
        {
            setListRepresent(((ListRepresentHistoryItem)another).getListRepresent());
            setNumber(((ListRepresentHistoryItem)another).getNumber());
            setFieldname(((ListRepresentHistoryItem)another).getFieldname());
            setValue(((ListRepresentHistoryItem)another).getValue());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ListRepresentHistoryItemGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ListRepresentHistoryItem.class;
        }

        public T newInstance()
        {
            return (T) new ListRepresentHistoryItem();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "listRepresent":
                    return obj.getListRepresent();
                case "number":
                    return obj.getNumber();
                case "fieldname":
                    return obj.getFieldname();
                case "value":
                    return obj.getValue();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "listRepresent":
                    obj.setListRepresent((ListRepresent) value);
                    return;
                case "number":
                    obj.setNumber((Long) value);
                    return;
                case "fieldname":
                    obj.setFieldname((String) value);
                    return;
                case "value":
                    obj.setValue((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "listRepresent":
                        return true;
                case "number":
                        return true;
                case "fieldname":
                        return true;
                case "value":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "listRepresent":
                    return true;
                case "number":
                    return true;
                case "fieldname":
                    return true;
                case "value":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "listRepresent":
                    return ListRepresent.class;
                case "number":
                    return Long.class;
                case "fieldname":
                    return String.class;
                case "value":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ListRepresentHistoryItem> _dslPath = new Path<ListRepresentHistoryItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ListRepresentHistoryItem");
    }
            

    /**
     * @return Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentHistoryItem#getListRepresent()
     */
    public static ListRepresent.Path<ListRepresent> listRepresent()
    {
        return _dslPath.listRepresent();
    }

    /**
     * @return Номер записи. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentHistoryItem#getNumber()
     */
    public static PropertyPath<Long> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Имя поля. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentHistoryItem#getFieldname()
     */
    public static PropertyPath<String> fieldname()
    {
        return _dslPath.fieldname();
    }

    /**
     * @return Значение поля.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentHistoryItem#getValue()
     */
    public static PropertyPath<String> value()
    {
        return _dslPath.value();
    }

    public static class Path<E extends ListRepresentHistoryItem> extends EntityPath<E>
    {
        private ListRepresent.Path<ListRepresent> _listRepresent;
        private PropertyPath<Long> _number;
        private PropertyPath<String> _fieldname;
        private PropertyPath<String> _value;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentHistoryItem#getListRepresent()
     */
        public ListRepresent.Path<ListRepresent> listRepresent()
        {
            if(_listRepresent == null )
                _listRepresent = new ListRepresent.Path<ListRepresent>(L_LIST_REPRESENT, this);
            return _listRepresent;
        }

    /**
     * @return Номер записи. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentHistoryItem#getNumber()
     */
        public PropertyPath<Long> number()
        {
            if(_number == null )
                _number = new PropertyPath<Long>(ListRepresentHistoryItemGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Имя поля. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentHistoryItem#getFieldname()
     */
        public PropertyPath<String> fieldname()
        {
            if(_fieldname == null )
                _fieldname = new PropertyPath<String>(ListRepresentHistoryItemGen.P_FIELDNAME, this);
            return _fieldname;
        }

    /**
     * @return Значение поля.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentHistoryItem#getValue()
     */
        public PropertyPath<String> value()
        {
            if(_value == null )
                _value = new PropertyPath<String>(ListRepresentHistoryItemGen.P_VALUE, this);
            return _value;
        }

        public Class getEntityClass()
        {
            return ListRepresentHistoryItem.class;
        }

        public String getEntityName()
        {
            return "listRepresentHistoryItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
