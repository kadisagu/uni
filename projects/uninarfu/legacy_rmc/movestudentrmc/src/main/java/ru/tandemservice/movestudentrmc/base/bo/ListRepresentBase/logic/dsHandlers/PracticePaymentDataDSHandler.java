package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import ru.tandemservice.movestudentrmc.entity.catalog.PracticePaymentData;

public class PracticePaymentDataDSHandler extends DefaultComboDataSourceHandler {

    public PracticePaymentDataDSHandler(String ownerId) {

        super(ownerId, PracticePaymentData.class, PracticePaymentData.P_TITLE);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {

    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
    {
        ep.dqlBuilder.order("e." + PracticePaymentData.title());
    }
}
