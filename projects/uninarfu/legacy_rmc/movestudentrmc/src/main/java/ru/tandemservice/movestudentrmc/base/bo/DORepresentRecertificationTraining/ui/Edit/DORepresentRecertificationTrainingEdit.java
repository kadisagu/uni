package ru.tandemservice.movestudentrmc.base.bo.DORepresentRecertificationTraining.ui.Edit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.block.BlockListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.ReasonDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.EditBasic.DORepresentBaseEditBasic;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentRecertificationTraining.logic.SessionTransferOutsideOperationDSHandler;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisession.entity.document.SessionTransferOutsideOperation;

@Configuration
public class DORepresentRecertificationTrainingEdit extends BusinessComponentManager {

    public static final String REASON_DS = "reasonDS";
    public static final String TRANSFER_DS1 = "transferDS1";
    public static final String TRANSFER_DS2 = "transferDS2";
    public static final String EDUYEAR_DS = "eduyearDS";


    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(REASON_DS, reasonDSHandler()))
                .addDataSource(selectDS(TRANSFER_DS1, sessionTransferOutsideOperationDSHandler())
                                       .addColumn("title", SessionTransferOutsideOperation.targetMark().slot().studentWpeCAction().studentWpe().sourceRow().displayableTitle().s())
                                       .addColumn("mark", SessionTransferOutsideOperation.targetMark().valueTitle().s())
                )
                .addDataSource(selectDS(TRANSFER_DS2, sessionTransferOutsideOperationDSHandler())
                                       .addColumn("title", SessionTransferOutsideOperation.targetMark().slot().studentWpeCAction().studentWpe().sourceRow().displayableTitle().s())
                                       .addColumn("mark", SessionTransferOutsideOperation.targetMark().valueTitle().s())
                )
                .addDataSource(selectDS(EDUYEAR_DS, educationYearDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler reasonDSHandler() {
        return new ReasonDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler educationYearDSHandler() {
        return EducationYear.defaultSelectDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler sessionTransferOutsideOperationDSHandler() {
        return new SessionTransferOutsideOperationDSHandler(getName());
    }

    @Bean
    public BlockListExtPoint editBasicBlockListExtPoint() {
        return blockListExtPointBuilder(DORepresentRecertificationTrainingEditUI.EDIT_BASIC_BLOCK_LIST)
                .addBlock(componentBlock(DORepresentRecertificationTrainingEditUI.EDIT_BASIC_BLOCK, DORepresentBaseEditBasic.class).parameters("ui:blockParam"))
                .create();

    }
}
