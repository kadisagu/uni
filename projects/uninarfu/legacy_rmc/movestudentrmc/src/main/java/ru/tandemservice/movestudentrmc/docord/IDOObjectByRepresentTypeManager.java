package ru.tandemservice.movestudentrmc.docord;

import org.tandemframework.caf.ui.config.BusinessComponentManager;

public interface IDOObjectByRepresentTypeManager {

    Class<? extends BusinessComponentManager> getEditComponentManager();

    IDOObjectModifyDAO getDOObjectModifyDAO();

    Class<? extends BusinessComponentManager> getViewComponentManager();
}
