package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudentrmc.entity.RepresentWeekend;
import ru.tandemservice.movestudentrmc.entity.Representation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * О предоставлении академического отпуска
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RepresentWeekendGen extends Representation
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.RepresentWeekend";
    public static final String ENTITY_NAME = "representWeekend";
    public static final int VERSION_HASH = 1258319794;
    private static IEntityMeta ENTITY_META;

    public static final String P_END_DATE = "endDate";
    public static final String P_WITH_PAYMENT = "withPayment";

    private Date _endDate;     // Дата окончания
    private boolean _withPayment = false;     // С назначением компенсационной выплаты

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата окончания. Свойство не может быть null.
     */
    @NotNull
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания. Свойство не может быть null.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return С назначением компенсационной выплаты. Свойство не может быть null.
     */
    @NotNull
    public boolean isWithPayment()
    {
        return _withPayment;
    }

    /**
     * @param withPayment С назначением компенсационной выплаты. Свойство не может быть null.
     */
    public void setWithPayment(boolean withPayment)
    {
        dirty(_withPayment, withPayment);
        _withPayment = withPayment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof RepresentWeekendGen)
        {
            setEndDate(((RepresentWeekend)another).getEndDate());
            setWithPayment(((RepresentWeekend)another).isWithPayment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RepresentWeekendGen> extends Representation.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RepresentWeekend.class;
        }

        public T newInstance()
        {
            return (T) new RepresentWeekend();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "endDate":
                    return obj.getEndDate();
                case "withPayment":
                    return obj.isWithPayment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "withPayment":
                    obj.setWithPayment((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "endDate":
                        return true;
                case "withPayment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "endDate":
                    return true;
                case "withPayment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "endDate":
                    return Date.class;
                case "withPayment":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RepresentWeekend> _dslPath = new Path<RepresentWeekend>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RepresentWeekend");
    }
            

    /**
     * @return Дата окончания. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekend#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return С назначением компенсационной выплаты. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekend#isWithPayment()
     */
    public static PropertyPath<Boolean> withPayment()
    {
        return _dslPath.withPayment();
    }

    public static class Path<E extends RepresentWeekend> extends Representation.Path<E>
    {
        private PropertyPath<Date> _endDate;
        private PropertyPath<Boolean> _withPayment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата окончания. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekend#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(RepresentWeekendGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return С назначением компенсационной выплаты. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekend#isWithPayment()
     */
        public PropertyPath<Boolean> withPayment()
        {
            if(_withPayment == null )
                _withPayment = new PropertyPath<Boolean>(RepresentWeekendGen.P_WITH_PAYMENT, this);
            return _withPayment;
        }

        public Class getEntityClass()
        {
            return RepresentWeekend.class;
        }

        public String getEntityName()
        {
            return "representWeekend";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
