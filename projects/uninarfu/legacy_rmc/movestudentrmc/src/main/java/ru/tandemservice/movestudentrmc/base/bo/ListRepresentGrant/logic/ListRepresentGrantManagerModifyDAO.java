package ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrant.logic;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.AbstractListRepresentDAO;
import ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab.MonthWrapper;
import ru.tandemservice.movestudentrmc.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.Grant;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.ReportRenderer;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.*;

//-----------------------------------------------------------------------------------------------------------
import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.*;
//-----------------------------------------------------------------------------------------------------------

public class ListRepresentGrantManagerModifyDAO extends AbstractListRepresentDAO {

    @Override
    public void save(ListRepresent listRepresent, List<Student> studentSelectedList, DocListRepresentBasics representBasics) {
        ListRepresentGrant listGrant = (ListRepresentGrant) listRepresent;

        if (CommonBaseDateUtil.isAfter(listGrant.getDateBeginingPayment(), listGrant.getDateEndOfPayment())) {
            throw new ApplicationException("Дата начала выплаты не может быть позже даты окончания выплаты");
        }
        
        //----------------------------------------------------------------------------------------------------------------------
        /*
        Calendar beginPaymentYearCalendar = Calendar.getInstance();
        beginPaymentYearCalendar.setTime(listGrant.getDateBeginingPayment());
        int beginPaymentYear = beginPaymentYearCalendar.get(Calendar.YEAR);
        
        Calendar endPaymentYearCalendar = Calendar.getInstance();
        endPaymentYearCalendar.setTime(listGrant.getDateEndOfPayment());
        int endPaymentYear = endPaymentYearCalendar.get(Calendar.YEAR);
        */
        
		DQLSelectBuilder eduYearBuilder = new DQLSelectBuilder()
                .fromEntity(EducationYear.class, "year")
                .where(or(between(valueDate(listGrant.getDateBeginingPayment())
                			, createTimestamp(property(EducationYear.intValue().fromAlias("year")), value(9), value(1), value(0), value(0), value(0))
                			, createTimestamp(plus(property(EducationYear.intValue().fromAlias("year")), value(1)), value(8), value(31), value(23), value(59), value(59)))
                		, between(valueDate(listGrant.getDateEndOfPayment())
                    			, createTimestamp(property(EducationYear.intValue().fromAlias("year")), value(9), value(1), value(0), value(0), value(0))
                    			, createTimestamp(plus(property(EducationYear.intValue().fromAlias("year")), value(1)), value(8), value(31), value(23), value(59), value(59)))

                		, and(lt(valueDate(listGrant.getDateBeginingPayment()), createTimestamp(property(EducationYear.intValue().fromAlias("year")), value(9), value(1), value(0), value(0), value(0)))
                			, gt(valueDate(listGrant.getDateEndOfPayment()), createTimestamp(plus(property(EducationYear.intValue().fromAlias("year")), value(1)), value(8), value(31), value(23), value(59), value(59)))) 
                		))
                ;
		
		List<EducationYear> eduYearList = eduYearBuilder.createStatement(getSession()).list();
		
		List<String> errorList = new ArrayList<>();
		
		for(EducationYear eduYear : eduYearList) {
			
			Date beginDate;
			Date endDate;
			
	        Calendar beginYear = Calendar.getInstance();
	        beginYear.set(eduYear.getIntValue(), Calendar.SEPTEMBER, 1, 0, 0, 0);
	        beginYear.set(Calendar.MILLISECOND, 0);
	        if(listGrant.getDateBeginingPayment().compareTo(beginYear.getTime()) > 0) {
	        	beginDate = listGrant.getDateBeginingPayment();
	        }
	        else {
	        	beginDate = beginYear.getTime();
	        }
	        
	        Calendar endYear = Calendar.getInstance();
	        endYear.set(eduYear.getIntValue() + 1, Calendar.AUGUST, 31, 23, 59, 59);
	        endYear.set(Calendar.MILLISECOND, 999);	
	        if(listGrant.getDateEndOfPayment().compareTo(endYear.getTime()) > 0) {
	        	endDate = endYear.getTime();
	        }
	        else {
	        	endDate = listGrant.getDateEndOfPayment();
	        }	        
	        
	        List<String> periodList = MonthWrapper.getMonthsList(beginDate, endDate, eduYear);
	        MoveStudentDaoFacade.getGrantDAO().checkStudentGrants(listGrant.getId(), listGrant.getGrantView(), studentSelectedList, periodList, errorList);
		}
		
        if (!errorList.isEmpty()) {
            RtfDocument errorReport = RtfBean.getElementFactory().createRtfDocument();
            IRtfControl par = RtfBean.getElementFactory().createRtfControl(IRtfData.PAR);
            for (String text : errorList) {
                IRtfText studText = RtfBean.getElementFactory().createRtfText(text);
                errorReport.addElement(studText);
                errorReport.getElementList().addAll(Arrays.asList(par, par));
            }
            BusinessComponentUtils.downloadDocument(new ReportRenderer("Ошибки.rtf", errorReport), true);

            throw new ApplicationException("Не удалось сохранить представление");
        }
        
        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(RelListRepresentStudents.class)
                .fromEntity(RelListRepresentStudents.class, "rls")
                .where(DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudents.representation()), DQLExpressions.value(listRepresent)));

        //Почистим старых студентов
        deleteBuilder.createStatement(getSession()).execute();

        //Сохраним оператора, изменившего представление
        IPrincipalContext context = ContextLocal.getUserContext().getPrincipalContext();
        Person person = PersonManager.instance().dao().getPerson(context);
        if (person != null)
            listGrant.setOperator(person);

        listGrant.setCreator(context);

        baseCreateOrUpdate(listRepresent);
        getSession().saveOrUpdate(representBasics);

        for (Student student : studentSelectedList) {
            RelListRepresentStudents relListRepresentGrantStudents = new RelListRepresentStudents();

            relListRepresentGrantStudents.setRepresentation(listRepresent);
            relListRepresentGrantStudents.setStudent(student);
            baseCreateOrUpdate(relListRepresentGrantStudents);
        }  

        MoveStudentDaoFacade.getGrantDAO().deleteStudentGrants(listGrant);

		for(EducationYear eduYear : eduYearList) {
			
			Date beginDate;
			Date endDate;
			
	        Calendar beginYear = Calendar.getInstance();
	        beginYear.set(eduYear.getIntValue(), Calendar.SEPTEMBER, 1, 0, 0, 0);
	        beginYear.set(Calendar.MILLISECOND, 0);
	        if(listGrant.getDateBeginingPayment().compareTo(beginYear.getTime()) > 0) {
	        	beginDate = listGrant.getDateBeginingPayment();
	        }
	        else {
	        	beginDate = beginYear.getTime();
	        }
	        
	        Calendar endYear = Calendar.getInstance();
	        endYear.set(eduYear.getIntValue() + 1, Calendar.AUGUST, 31, 23, 59, 59);
	        endYear.set(Calendar.MILLISECOND, 999);	
	        if(listGrant.getDateEndOfPayment().compareTo(endYear.getTime()) > 0) {
	        	endDate = endYear.getTime();
	        }
	        else {
	        	endDate = listGrant.getDateEndOfPayment();
	        }	        
	        
	        List<String> periodList = MonthWrapper.getMonthsList(beginDate, endDate, eduYear);
	        MoveStudentDaoFacade.getGrantDAO().saveStudentGrants(listGrant, listGrant.getGrantView(), studentSelectedList, periodList, eduYear, false);

		}        

		/*
        //период попадает в учебный год
        EducationYear eduYear = listGrant.getEducationYear();

        Calendar beginYear = Calendar.getInstance();
        beginYear.set(eduYear.getIntValue(), Calendar.SEPTEMBER, 1, 0, 0, 0);
        beginYear.set(Calendar.MILLISECOND, 0);
        Calendar endYear = Calendar.getInstance();
        endYear.set(eduYear.getIntValue() + 1, Calendar.AUGUST, 31, 23, 59, 59);
        endYear.set(Calendar.MILLISECOND, 999);

        if (!CommonBaseDateUtil.isBetween(listGrant.getDateBeginingPayment(), beginYear.getTime(), endYear.getTime())
                || !CommonBaseDateUtil.isBetween(listGrant.getDateEndOfPayment(), beginYear.getTime(), endYear.getTime()))
            throw new ApplicationException("Даты назначения стипендии/выплаты выходят за границы учебного года " + eduYear.getTitle());

        List<String> periodList = MonthWrapper.getMonthsList(listGrant.getDateBeginingPayment(), listGrant.getDateEndOfPayment(), listGrant.getEducationYear());
        List<String> errorList = new ArrayList<>();
        boolean good = MoveStudentDaoFacade.getGrantDAO().checkStudentGrants(listGrant.getId(), listGrant.getGrantView(), studentSelectedList, periodList, errorList);

        if (!good) {
            RtfDocument errorReport = RtfBean.getElementFactory().createRtfDocument();
            IRtfControl par = RtfBean.getElementFactory().createRtfControl(IRtfData.PAR);
            for (String text : errorList) {
                IRtfText studText = RtfBean.getElementFactory().createRtfText(text);
                errorReport.addElement(studText);
                errorReport.getElementList().addAll(Arrays.asList(par, par));
            }
            BusinessComponentUtils.downloadDocument(new ReportRenderer("Ошибки.rtf", errorReport), true);

            throw new ApplicationException("Не удалось сохранить представление");
        }

        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(RelListRepresentStudents.class)
                .fromEntity(RelListRepresentStudents.class, "rls")
                .where(DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudents.representation()), DQLExpressions.value(listRepresent)));

        //Почистим старых студентов
        deleteBuilder.createStatement(getSession()).execute();

        //Сохраним оператора, изменившего представление
        Person person = PersonManager.instance().dao().getPerson(ContextLocal.getUserContext().getPrincipalContext());
        if (person != null)
            listGrant.setOperator(person);

        IPrincipalContext context = ContextLocal.getUserContext().getPrincipalContext();
        listGrant.setCreator(context);

        baseCreateOrUpdate(listRepresent);
        getSession().saveOrUpdate(representBasics);

        for (Student student : studentSelectedList) {
            RelListRepresentStudents relListRepresentGrantStudents = new RelListRepresentStudents();

            relListRepresentGrantStudents.setRepresentation(listRepresent);
            relListRepresentGrantStudents.setStudent(student);
            baseCreateOrUpdate(relListRepresentGrantStudents);
        }

        MoveStudentDaoFacade.getGrantDAO().saveStudentGrants(listGrant, listGrant.getGrantView(), studentSelectedList, periodList, listGrant.getEducationYear());
		*/ 
        //----------------------------------------------------------------------------------------------------------------------

    }

    @Override
    public void buildBodyExtract(ListOrdListRepresent listOrdListRepresent,
                                 Student student, RtfDocument docExtract, Map<Student, OrderParagraphInfo> map) {
        ListRepresentGrant listRepresent = (ListRepresentGrant) listOrdListRepresent.getRepresentation();

        RtfInjectModifier im = new RtfInjectModifier();
        headerInjectModifier(im, listRepresent);
        im.modify(docExtract);
        super.buildBodyExtract(listOrdListRepresent, student, docExtract, map);


        new RtfInjectModifier()
                .put("st_date", UtilPrintSupport.getDateFormatterWithMonthString(listRepresent.getDateBeginingPayment()))
                .put("end_date", listRepresent.getDateEndOfPayment() != null ? UtilPrintSupport.getDateFormatterWithMonthStringAndYear(listRepresent.getDateEndOfPayment()) : new RtfString().append(" на весь период обучения"))
                .put("Spec_gr_Generive", listRepresent.getGrantView().getGenitive())
                .modify(docExtract);
    }

    @Override
    public Map<Student, OrderParagraphInfo> buildBodyRepresent(List<ListRepresent> representationBase, RtfDocument document) {
        ListPrintDoc listPrintDoc = new ListPrintDoc();
        RtfDocument rtfDocument = listPrintDoc.creationPrintDocOrder(representationBase);
        document.getElementList().addAll(rtfDocument.getElementList());
        return listPrintDoc.getParagInfomap();
    }

    @Override
    public boolean doRollback(ListRepresent represent, ErrorCollector error) {
        ListRepresentGrant listRepresentGrant = (ListRepresentGrant) represent;

        List<ListOrdListRepresent> orders = DataAccessServices.dao().getList(ListOrdListRepresent.class, ListOrdListRepresent.representation(), represent);
        ListOrder order = orders.get(0).getOrder();

        try {
            MoveStudentDaoFacade.getGrantDAO().rollbackStudentGrants(listRepresentGrant, order);
        } catch (Exception ex) {
            //TODO: а что тут надо делать?
            ex.printStackTrace();
            return false;
        }

        return true;
    }

    @Override
    public boolean doCommit(ListRepresent represent, ErrorCollector error) {
        ListRepresentGrant listRepresentGrant = (ListRepresentGrant) represent;

        List<ListOrdListRepresent> orders = DataAccessServices.dao().getList(ListOrdListRepresent.class, ListOrdListRepresent.representation(), represent);
        ListOrder order = orders.get(0).getOrder();

        try {
            MoveStudentDaoFacade.getGrantDAO().commitStudentGrants(listRepresentGrant, order);
        } catch (Exception ex) {
            //TODO: а что тут надо делать?
            ex.printStackTrace();
            return false;
        }
        return true;
    }


    @Override
    public List<Student> selectRepresent(ListRepresent listRepresent) {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(RelListRepresentStudents.class, "rls");

        builder.where(DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("rls")), DQLExpressions.value(listRepresent)));

        builder.column(DQLExpressions.property(RelListRepresentStudents.student().fromAlias("rls")));

        return builder.createStatement(getSession()).list();
    }

    public void saveSupport(ListRepresent listRepresent, List<DataWrapper> studentSelectedList, DocListRepresentBasics representBasics) {
    }

    protected void headerInjectModifier(RtfInjectModifier im,
                                        ListRepresentGrant listRepresent) {
        //Наименование стипендии
        GrantView grantView = listRepresent.getGrantView();
        List<RelTypeGrantView> relTypeGrantViews = DataAccessServices.dao().getList(RelTypeGrantView.class, RelTypeGrantView.view(), grantView);
        Grant grant;
        try {
            grant = relTypeGrantViews.get(0).getGrant();
        } catch (Exception e) {
            throw new ApplicationException("Распечатать приказ невозможно, т.к. не найдена настройка стипендия для вида : \"" +
                    (grantView.getShortTitle() != null ? grantView.getShortTitle() : grantView.getTitle()) + "\"");
        }

        String grantTitle = (grant.getGenitive() == null ? grant.getTitle() : grant.getGenitive());

        im.put("representTitle", "О назначении " + grantTitle);
    }

}
