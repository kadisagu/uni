package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import ru.tandemservice.movestudentrmc.entity.GrantEntity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

//#RT:2028, удаление дубликатов
public class MS_movestudentrmc_1x0x0_20to21 extends IndependentMigrationScript {
    @Override
    public void run(DBTool tool) throws Exception {
        IEntityMeta meta = EntityRuntime.getMeta(GrantEntity.class);
        if (!tool.tableExists(meta.getTableName()))
            return;

        String sql = "select ORGUNIT_ID, MONTH_P, VIEW_ID, COUNT(*) as aaa "
                + "from	GRANTENTITY_T "
                + "group by ORGUNIT_ID, MONTH_P, VIEW_ID "
                + "ORDER BY aaa desc ";
        ResultSet rs = tool.getConnection().createStatement().executeQuery(sql);

        PreparedStatement pst = tool.getConnection().prepareStatement("select id from GRANTENTITY_T "
                                                                              + "where "
                                                                              + "VIEW_ID=? "
                                                                              + "and MONTH_P=? "
                                                                              + "and ORGUNIT_ID=? ");

        PreparedStatement pst2 = tool.getConnection().prepareStatement("delete from GRANTENTITY_T where VIEW_ID=? and MONTH_P=? and ORGUNIT_ID=? and id<>?");
        while (rs.next()) {
            Long orgunitId = rs.getLong(1);
            String month = rs.getString(2);
            Long viewId = rs.getLong(3);
            Integer count = rs.getInt(4);

            if (count > 1) {
                pst.setLong(1, viewId);
                pst.setString(2, month);
                pst.setLong(3, orgunitId);
                ResultSet rs2 = pst.executeQuery();

                rs2.next();

                pst2.setLong(1, viewId);
                pst2.setString(2, month);
                pst2.setLong(3, orgunitId);
                pst2.setLong(4, rs2.getLong(1));
                pst2.executeUpdate();

                rs2.close();
            }
            else
                break;
        }

        rs.close();
    }


}
