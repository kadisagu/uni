package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.StudentPracticeData;
import ru.tandemservice.movestudentrmc.entity.catalog.PracticeBase;
import ru.tandemservice.uni.entity.employee.Student;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь Данные по практике для студента - списочное представление
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentPracticeDataGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.StudentPracticeData";
    public static final String ENTITY_NAME = "studentPracticeData";
    public static final int VERSION_HASH = -1680465140;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT = "student";
    public static final String L_INNER_ADVISOR = "innerAdvisor";
    public static final String L_PRACTICE_BASE = "practiceBase";
    public static final String P_OUTER_ADVISOR = "outerAdvisor";
    public static final String L_LIST_REPRESENT = "listRepresent";

    private Student _student;     // Студент
    private Employee _innerAdvisor;     // Руководитель от ОУ
    private PracticeBase _practiceBase;     // База практики
    private String _outerAdvisor;     // ФИО руководителя практики от предприятия
    private ListRepresent _listRepresent;     // Списочное представление

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Руководитель от ОУ.
     */
    public Employee getInnerAdvisor()
    {
        return _innerAdvisor;
    }

    /**
     * @param innerAdvisor Руководитель от ОУ.
     */
    public void setInnerAdvisor(Employee innerAdvisor)
    {
        dirty(_innerAdvisor, innerAdvisor);
        _innerAdvisor = innerAdvisor;
    }

    /**
     * @return База практики.
     */
    public PracticeBase getPracticeBase()
    {
        return _practiceBase;
    }

    /**
     * @param practiceBase База практики.
     */
    public void setPracticeBase(PracticeBase practiceBase)
    {
        dirty(_practiceBase, practiceBase);
        _practiceBase = practiceBase;
    }

    /**
     * @return ФИО руководителя практики от предприятия.
     */
    @Length(max=255)
    public String getOuterAdvisor()
    {
        return _outerAdvisor;
    }

    /**
     * @param outerAdvisor ФИО руководителя практики от предприятия.
     */
    public void setOuterAdvisor(String outerAdvisor)
    {
        dirty(_outerAdvisor, outerAdvisor);
        _outerAdvisor = outerAdvisor;
    }

    /**
     * @return Списочное представление. Свойство не может быть null.
     */
    @NotNull
    public ListRepresent getListRepresent()
    {
        return _listRepresent;
    }

    /**
     * @param listRepresent Списочное представление. Свойство не может быть null.
     */
    public void setListRepresent(ListRepresent listRepresent)
    {
        dirty(_listRepresent, listRepresent);
        _listRepresent = listRepresent;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentPracticeDataGen)
        {
            setStudent(((StudentPracticeData)another).getStudent());
            setInnerAdvisor(((StudentPracticeData)another).getInnerAdvisor());
            setPracticeBase(((StudentPracticeData)another).getPracticeBase());
            setOuterAdvisor(((StudentPracticeData)another).getOuterAdvisor());
            setListRepresent(((StudentPracticeData)another).getListRepresent());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentPracticeDataGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentPracticeData.class;
        }

        public T newInstance()
        {
            return (T) new StudentPracticeData();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "student":
                    return obj.getStudent();
                case "innerAdvisor":
                    return obj.getInnerAdvisor();
                case "practiceBase":
                    return obj.getPracticeBase();
                case "outerAdvisor":
                    return obj.getOuterAdvisor();
                case "listRepresent":
                    return obj.getListRepresent();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "innerAdvisor":
                    obj.setInnerAdvisor((Employee) value);
                    return;
                case "practiceBase":
                    obj.setPracticeBase((PracticeBase) value);
                    return;
                case "outerAdvisor":
                    obj.setOuterAdvisor((String) value);
                    return;
                case "listRepresent":
                    obj.setListRepresent((ListRepresent) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "student":
                        return true;
                case "innerAdvisor":
                        return true;
                case "practiceBase":
                        return true;
                case "outerAdvisor":
                        return true;
                case "listRepresent":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "student":
                    return true;
                case "innerAdvisor":
                    return true;
                case "practiceBase":
                    return true;
                case "outerAdvisor":
                    return true;
                case "listRepresent":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "student":
                    return Student.class;
                case "innerAdvisor":
                    return Employee.class;
                case "practiceBase":
                    return PracticeBase.class;
                case "outerAdvisor":
                    return String.class;
                case "listRepresent":
                    return ListRepresent.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentPracticeData> _dslPath = new Path<StudentPracticeData>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentPracticeData");
    }
            

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.StudentPracticeData#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Руководитель от ОУ.
     * @see ru.tandemservice.movestudentrmc.entity.StudentPracticeData#getInnerAdvisor()
     */
    public static Employee.Path<Employee> innerAdvisor()
    {
        return _dslPath.innerAdvisor();
    }

    /**
     * @return База практики.
     * @see ru.tandemservice.movestudentrmc.entity.StudentPracticeData#getPracticeBase()
     */
    public static PracticeBase.Path<PracticeBase> practiceBase()
    {
        return _dslPath.practiceBase();
    }

    /**
     * @return ФИО руководителя практики от предприятия.
     * @see ru.tandemservice.movestudentrmc.entity.StudentPracticeData#getOuterAdvisor()
     */
    public static PropertyPath<String> outerAdvisor()
    {
        return _dslPath.outerAdvisor();
    }

    /**
     * @return Списочное представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.StudentPracticeData#getListRepresent()
     */
    public static ListRepresent.Path<ListRepresent> listRepresent()
    {
        return _dslPath.listRepresent();
    }

    public static class Path<E extends StudentPracticeData> extends EntityPath<E>
    {
        private Student.Path<Student> _student;
        private Employee.Path<Employee> _innerAdvisor;
        private PracticeBase.Path<PracticeBase> _practiceBase;
        private PropertyPath<String> _outerAdvisor;
        private ListRepresent.Path<ListRepresent> _listRepresent;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.StudentPracticeData#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Руководитель от ОУ.
     * @see ru.tandemservice.movestudentrmc.entity.StudentPracticeData#getInnerAdvisor()
     */
        public Employee.Path<Employee> innerAdvisor()
        {
            if(_innerAdvisor == null )
                _innerAdvisor = new Employee.Path<Employee>(L_INNER_ADVISOR, this);
            return _innerAdvisor;
        }

    /**
     * @return База практики.
     * @see ru.tandemservice.movestudentrmc.entity.StudentPracticeData#getPracticeBase()
     */
        public PracticeBase.Path<PracticeBase> practiceBase()
        {
            if(_practiceBase == null )
                _practiceBase = new PracticeBase.Path<PracticeBase>(L_PRACTICE_BASE, this);
            return _practiceBase;
        }

    /**
     * @return ФИО руководителя практики от предприятия.
     * @see ru.tandemservice.movestudentrmc.entity.StudentPracticeData#getOuterAdvisor()
     */
        public PropertyPath<String> outerAdvisor()
        {
            if(_outerAdvisor == null )
                _outerAdvisor = new PropertyPath<String>(StudentPracticeDataGen.P_OUTER_ADVISOR, this);
            return _outerAdvisor;
        }

    /**
     * @return Списочное представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.StudentPracticeData#getListRepresent()
     */
        public ListRepresent.Path<ListRepresent> listRepresent()
        {
            if(_listRepresent == null )
                _listRepresent = new ListRepresent.Path<ListRepresent>(L_LIST_REPRESENT, this);
            return _listRepresent;
        }

        public Class getEntityClass()
        {
            return StudentPracticeData.class;
        }

        public String getEntityName()
        {
            return "studentPracticeData";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
