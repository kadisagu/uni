package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudentrmc.entity.IStudentGrant;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListRepresentBudgetTransfer;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Списочное представление о переводе на места финансируемые из средств федерального бюджета
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ListRepresentBudgetTransferGen extends ListRepresent
 implements IStudentGrant{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.ListRepresentBudgetTransfer";
    public static final String ENTITY_NAME = "listRepresentBudgetTransfer";
    public static final int VERSION_HASH = 917288916;
    private static IEntityMeta ENTITY_META;

    public static final String P_BEGIN_GRANT_DATE = "beginGrantDate";
    public static final String P_END_GRANT_DATE = "endGrantDate";
    public static final String P_DATE_BEGINING_PAYMENT = "dateBeginingPayment";
    public static final String P_DATE_BEGINING_TRANSFER = "dateBeginingTransfer";
    public static final String P_DATE_END_OF_PAYMENT = "dateEndOfPayment";
    public static final String L_EDUCATION_YEAR = "educationYear";
    public static final String L_GRANT_VIEW = "grantView";

    private Date _beginGrantDate;     // Дата назначения выплат
    private Date _endGrantDate;     // Дата окончания выплат
    private Date _dateBeginingPayment;     // Месяц начала выплаты
    private Date _dateBeginingTransfer;     // Дата перевода студентов
    private Date _dateEndOfPayment;     // Месяц окончания выплаты
    private EducationYear _educationYear;     // Учебный год
    private GrantView _grantView;     // Вид стипендии

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата назначения выплат. Свойство не может быть null.
     *
     * Это формула "dateBeginingPayment".
     */
    // @NotNull
    public Date getBeginGrantDate()
    {
        return _beginGrantDate;
    }

    /**
     * @param beginGrantDate Дата назначения выплат. Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setBeginGrantDate(Date beginGrantDate)
    {
        dirty(_beginGrantDate, beginGrantDate);
        _beginGrantDate = beginGrantDate;
    }

    /**
     * @return Дата окончания выплат.
     *
     * Это формула "dateEndOfPayment".
     */
    public Date getEndGrantDate()
    {
        return _endGrantDate;
    }

    /**
     * @param endGrantDate Дата окончания выплат.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setEndGrantDate(Date endGrantDate)
    {
        dirty(_endGrantDate, endGrantDate);
        _endGrantDate = endGrantDate;
    }

    /**
     * @return Месяц начала выплаты. Свойство не может быть null.
     */
    @NotNull
    public Date getDateBeginingPayment()
    {
        return _dateBeginingPayment;
    }

    /**
     * @param dateBeginingPayment Месяц начала выплаты. Свойство не может быть null.
     */
    public void setDateBeginingPayment(Date dateBeginingPayment)
    {
        dirty(_dateBeginingPayment, dateBeginingPayment);
        _dateBeginingPayment = dateBeginingPayment;
    }

    /**
     * @return Дата перевода студентов. Свойство не может быть null.
     */
    @NotNull
    public Date getDateBeginingTransfer()
    {
        return _dateBeginingTransfer;
    }

    /**
     * @param dateBeginingTransfer Дата перевода студентов. Свойство не может быть null.
     */
    public void setDateBeginingTransfer(Date dateBeginingTransfer)
    {
        dirty(_dateBeginingTransfer, dateBeginingTransfer);
        _dateBeginingTransfer = dateBeginingTransfer;
    }

    /**
     * @return Месяц окончания выплаты. Свойство не может быть null.
     */
    @NotNull
    public Date getDateEndOfPayment()
    {
        return _dateEndOfPayment;
    }

    /**
     * @param dateEndOfPayment Месяц окончания выплаты. Свойство не может быть null.
     */
    public void setDateEndOfPayment(Date dateEndOfPayment)
    {
        dirty(_dateEndOfPayment, dateEndOfPayment);
        _dateEndOfPayment = dateEndOfPayment;
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    /**
     * @param educationYear Учебный год. Свойство не может быть null.
     */
    public void setEducationYear(EducationYear educationYear)
    {
        dirty(_educationYear, educationYear);
        _educationYear = educationYear;
    }

    /**
     * @return Вид стипендии. Свойство не может быть null.
     */
    @NotNull
    public GrantView getGrantView()
    {
        return _grantView;
    }

    /**
     * @param grantView Вид стипендии. Свойство не может быть null.
     */
    public void setGrantView(GrantView grantView)
    {
        dirty(_grantView, grantView);
        _grantView = grantView;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ListRepresentBudgetTransferGen)
        {
            setBeginGrantDate(((ListRepresentBudgetTransfer)another).getBeginGrantDate());
            setEndGrantDate(((ListRepresentBudgetTransfer)another).getEndGrantDate());
            setDateBeginingPayment(((ListRepresentBudgetTransfer)another).getDateBeginingPayment());
            setDateBeginingTransfer(((ListRepresentBudgetTransfer)another).getDateBeginingTransfer());
            setDateEndOfPayment(((ListRepresentBudgetTransfer)another).getDateEndOfPayment());
            setEducationYear(((ListRepresentBudgetTransfer)another).getEducationYear());
            setGrantView(((ListRepresentBudgetTransfer)another).getGrantView());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ListRepresentBudgetTransferGen> extends ListRepresent.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ListRepresentBudgetTransfer.class;
        }

        public T newInstance()
        {
            return (T) new ListRepresentBudgetTransfer();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "beginGrantDate":
                    return obj.getBeginGrantDate();
                case "endGrantDate":
                    return obj.getEndGrantDate();
                case "dateBeginingPayment":
                    return obj.getDateBeginingPayment();
                case "dateBeginingTransfer":
                    return obj.getDateBeginingTransfer();
                case "dateEndOfPayment":
                    return obj.getDateEndOfPayment();
                case "educationYear":
                    return obj.getEducationYear();
                case "grantView":
                    return obj.getGrantView();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "beginGrantDate":
                    obj.setBeginGrantDate((Date) value);
                    return;
                case "endGrantDate":
                    obj.setEndGrantDate((Date) value);
                    return;
                case "dateBeginingPayment":
                    obj.setDateBeginingPayment((Date) value);
                    return;
                case "dateBeginingTransfer":
                    obj.setDateBeginingTransfer((Date) value);
                    return;
                case "dateEndOfPayment":
                    obj.setDateEndOfPayment((Date) value);
                    return;
                case "educationYear":
                    obj.setEducationYear((EducationYear) value);
                    return;
                case "grantView":
                    obj.setGrantView((GrantView) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "beginGrantDate":
                        return true;
                case "endGrantDate":
                        return true;
                case "dateBeginingPayment":
                        return true;
                case "dateBeginingTransfer":
                        return true;
                case "dateEndOfPayment":
                        return true;
                case "educationYear":
                        return true;
                case "grantView":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "beginGrantDate":
                    return true;
                case "endGrantDate":
                    return true;
                case "dateBeginingPayment":
                    return true;
                case "dateBeginingTransfer":
                    return true;
                case "dateEndOfPayment":
                    return true;
                case "educationYear":
                    return true;
                case "grantView":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "beginGrantDate":
                    return Date.class;
                case "endGrantDate":
                    return Date.class;
                case "dateBeginingPayment":
                    return Date.class;
                case "dateBeginingTransfer":
                    return Date.class;
                case "dateEndOfPayment":
                    return Date.class;
                case "educationYear":
                    return EducationYear.class;
                case "grantView":
                    return GrantView.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ListRepresentBudgetTransfer> _dslPath = new Path<ListRepresentBudgetTransfer>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ListRepresentBudgetTransfer");
    }
            

    /**
     * @return Дата назначения выплат. Свойство не может быть null.
     *
     * Это формула "dateBeginingPayment".
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentBudgetTransfer#getBeginGrantDate()
     */
    public static PropertyPath<Date> beginGrantDate()
    {
        return _dslPath.beginGrantDate();
    }

    /**
     * @return Дата окончания выплат.
     *
     * Это формула "dateEndOfPayment".
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentBudgetTransfer#getEndGrantDate()
     */
    public static PropertyPath<Date> endGrantDate()
    {
        return _dslPath.endGrantDate();
    }

    /**
     * @return Месяц начала выплаты. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentBudgetTransfer#getDateBeginingPayment()
     */
    public static PropertyPath<Date> dateBeginingPayment()
    {
        return _dslPath.dateBeginingPayment();
    }

    /**
     * @return Дата перевода студентов. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentBudgetTransfer#getDateBeginingTransfer()
     */
    public static PropertyPath<Date> dateBeginingTransfer()
    {
        return _dslPath.dateBeginingTransfer();
    }

    /**
     * @return Месяц окончания выплаты. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentBudgetTransfer#getDateEndOfPayment()
     */
    public static PropertyPath<Date> dateEndOfPayment()
    {
        return _dslPath.dateEndOfPayment();
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentBudgetTransfer#getEducationYear()
     */
    public static EducationYear.Path<EducationYear> educationYear()
    {
        return _dslPath.educationYear();
    }

    /**
     * @return Вид стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentBudgetTransfer#getGrantView()
     */
    public static GrantView.Path<GrantView> grantView()
    {
        return _dslPath.grantView();
    }

    public static class Path<E extends ListRepresentBudgetTransfer> extends ListRepresent.Path<E>
    {
        private PropertyPath<Date> _beginGrantDate;
        private PropertyPath<Date> _endGrantDate;
        private PropertyPath<Date> _dateBeginingPayment;
        private PropertyPath<Date> _dateBeginingTransfer;
        private PropertyPath<Date> _dateEndOfPayment;
        private EducationYear.Path<EducationYear> _educationYear;
        private GrantView.Path<GrantView> _grantView;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата назначения выплат. Свойство не может быть null.
     *
     * Это формула "dateBeginingPayment".
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentBudgetTransfer#getBeginGrantDate()
     */
        public PropertyPath<Date> beginGrantDate()
        {
            if(_beginGrantDate == null )
                _beginGrantDate = new PropertyPath<Date>(ListRepresentBudgetTransferGen.P_BEGIN_GRANT_DATE, this);
            return _beginGrantDate;
        }

    /**
     * @return Дата окончания выплат.
     *
     * Это формула "dateEndOfPayment".
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentBudgetTransfer#getEndGrantDate()
     */
        public PropertyPath<Date> endGrantDate()
        {
            if(_endGrantDate == null )
                _endGrantDate = new PropertyPath<Date>(ListRepresentBudgetTransferGen.P_END_GRANT_DATE, this);
            return _endGrantDate;
        }

    /**
     * @return Месяц начала выплаты. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentBudgetTransfer#getDateBeginingPayment()
     */
        public PropertyPath<Date> dateBeginingPayment()
        {
            if(_dateBeginingPayment == null )
                _dateBeginingPayment = new PropertyPath<Date>(ListRepresentBudgetTransferGen.P_DATE_BEGINING_PAYMENT, this);
            return _dateBeginingPayment;
        }

    /**
     * @return Дата перевода студентов. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentBudgetTransfer#getDateBeginingTransfer()
     */
        public PropertyPath<Date> dateBeginingTransfer()
        {
            if(_dateBeginingTransfer == null )
                _dateBeginingTransfer = new PropertyPath<Date>(ListRepresentBudgetTransferGen.P_DATE_BEGINING_TRANSFER, this);
            return _dateBeginingTransfer;
        }

    /**
     * @return Месяц окончания выплаты. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentBudgetTransfer#getDateEndOfPayment()
     */
        public PropertyPath<Date> dateEndOfPayment()
        {
            if(_dateEndOfPayment == null )
                _dateEndOfPayment = new PropertyPath<Date>(ListRepresentBudgetTransferGen.P_DATE_END_OF_PAYMENT, this);
            return _dateEndOfPayment;
        }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentBudgetTransfer#getEducationYear()
     */
        public EducationYear.Path<EducationYear> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new EducationYear.Path<EducationYear>(L_EDUCATION_YEAR, this);
            return _educationYear;
        }

    /**
     * @return Вид стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentBudgetTransfer#getGrantView()
     */
        public GrantView.Path<GrantView> grantView()
        {
            if(_grantView == null )
                _grantView = new GrantView.Path<GrantView>(L_GRANT_VIEW, this);
            return _grantView;
        }

        public Class getEntityClass()
        {
            return ListRepresentBudgetTransfer.class;
        }

        public String getEntityName()
        {
            return "listRepresentBudgetTransfer";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
