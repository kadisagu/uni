package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;

public class MS_movestudentrmc_1x0x0_56to57 extends IndependentMigrationScript {
    @Override
    public void run(DBTool tool) throws Exception {
        if (tool.tableExists("templatedocument_t")) {
            tool.executeUpdate("delete from templatedocument_t where code_p in ('decl.narfu.exclude', 'rep.narfu.exclude')");
        }
    }
}
