package ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab;

import ru.tandemservice.movestudentrmc.entity.GrantEntity;
import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {
    public void deleteRow(GrantEntity grant);
}
