package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;
import ru.tandemservice.movestudentrmc.entity.StudentVKRTheme;
import ru.tandemservice.uni.entity.employee.Student;

import java.sql.ResultSet;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_movestudentrmc_1x0x0_65to66 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.5.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.5.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность studentVKRTheme

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("studentvkrtheme_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("student_id", DBType.LONG).setNullable(false),
                                      new DBColumn("theme_p", DBType.createVarchar(1000))
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("studentVKRTheme");

        }

        IEntityMeta meta = EntityRuntime.getMeta(Student.class);
        IEntityMeta vkr = EntityRuntime.getMeta(StudentVKRTheme.class);
        short entityCode = vkr.getEntityCode().shortValue();
        ResultSet rs = tool.getConnection().createStatement().executeQuery("select id, finalqualifyingworktheme_p from " + meta.getTableName() + " where finalqualifyingworktheme_p IS NOT NULL");
        while (rs.next()) {
            Long id = rs.getLong("id");
            String theme = rs.getString("finalqualifyingworktheme_p");
            tool.prepareStatement("insert into studentvkrtheme_t (id, discriminator, student_id, theme_p) values (?,?,?,?)", EntityIDGenerator.generateNewId(entityCode).longValue(), entityCode, id, theme).execute();
        }


    }
}