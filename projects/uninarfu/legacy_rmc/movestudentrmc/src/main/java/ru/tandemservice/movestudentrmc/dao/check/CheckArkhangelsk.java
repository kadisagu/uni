package ru.tandemservice.movestudentrmc.dao.check;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.ArrayList;
import java.util.List;

public class CheckArkhangelsk extends AbstractCheckDAO {

    @Override
    public List<String> doCheck(List<Student> list, boolean value) {

        List<String> errorList = new ArrayList<String>();

        for (Student student : list) {
            AddressDetailed address = student.getEducationOrgUnit().getFormativeOrgUnit().getPostalAddress();
            String msg = "";
            if (address == null) {
                msg = new StringBuilder()
                        .append("Для ")
                        .append(StringUtils.capitalize(student.getEducationOrgUnit().getFormativeOrgUnit().getGenitiveCaseTitle()))
                        .append(" не указан почтовый адрес.").toString();
            }
            else {
                if (getCondition(student) != value) {
                    msg = getErrorMessage(student, value);
                }
            }
            if (!errorList.contains(msg) && !StringUtils.isEmpty(msg))
                errorList.add(msg);
        }

        return errorList;
    }

    @Override
    protected boolean getCondition(Student student) {
        AddressDetailed address = student.getEducationOrgUnit().getFormativeOrgUnit().getPostalAddress();
        AddressItem item = address.getSettlement();

        while (item != null) {
            if (getCityCode().equals(item.getCode()))
                return true;
            item = item.getParent();
        }

        return false;
    }

    protected String getCityCode() {
        return "2900000100000";
    }

    @Override
    protected String getErrorMessage(Student student, boolean value) {
        return new StringBuilder()
                .append("Студент ")
                .append(student.getPerson().getFullFio())
                .append(" ")
                .append(value ? "не " : "")
                .append("обучается в г.")
                .append(getCityStr())
                .append(".").toString();
    }

    protected String getCityStr() {
        return "Архангельск";
    }

}
