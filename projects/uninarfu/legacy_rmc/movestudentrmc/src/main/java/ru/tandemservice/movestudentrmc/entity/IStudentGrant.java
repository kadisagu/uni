package ru.tandemservice.movestudentrmc.entity;

import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.Date;

public interface IStudentGrant {

    Date getBeginGrantDate();

    GrantView getGrantView();

    //EducationYear getEducationYear();

    Date getEndGrantDate();

}
