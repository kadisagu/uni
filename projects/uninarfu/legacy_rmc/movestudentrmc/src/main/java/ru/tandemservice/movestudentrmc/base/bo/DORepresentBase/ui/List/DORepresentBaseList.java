package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.logic.TypeRepresentDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.RepresentationDSHandler;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.Representation;

/**
 * Created by IntelliJ IDEA.
 * User: artzab
 * Date: 12.12.11
 * Time: 14:55
 */

@Configuration
public class DORepresentBaseList extends BusinessComponentManager {

    public static final String REPRESENTATION_DS = "representationDS";
    private static final String TYPE_REPRESENT_DS = "typeRepresentDS";

    @Bean
    public ColumnListExtPoint representationDS()
    {
        return columnListExtPointBuilder(REPRESENTATION_DS)
        		//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                //.addColumn(publisherColumn(RepresentationDSHandler.REPRESENT_COLUMN, Representation.title().fromAlias(Representation.ENTITY_NAME)).permissionKey("rmc_view_represent").order().create())
        		.addColumn(publisherColumn(RepresentationDSHandler.REPRESENT_COLUMN, Representation.titleWithCreator().fromAlias(Representation.ENTITY_NAME)).permissionKey("rmc_view_represent").order().create())
                //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                .addColumn(textColumn(RepresentationDSHandler.STUDENT_COLUMN, DocRepresentStudentBase.student().person().identityCard().fullFio().fromAlias(DocRepresentStudentBase.ENTITY_NAME)).visible("ui:isOrgUnit").order().create())
                .addColumn(textColumn(RepresentationDSHandler.GROUP_COLUMN, DocRepresentStudentBase.student().group().title().fromAlias(DocRepresentStudentBase.ENTITY_NAME)).visible("ui:isOrgUnit").order().create())
                .addColumn(dateColumn(RepresentationDSHandler.CREATE_DATE_COLUMN, Representation.createDate().fromAlias(Representation.ENTITY_NAME)).order().create())
                .addColumn(textColumn(RepresentationDSHandler.STATE_COLUMN, Representation.state().title().fromAlias(Representation.ENTITY_NAME)).order().create())
                .addColumn(booleanColumn(RepresentationDSHandler.CHECK, Representation.check().fromAlias(Representation.ENTITY_NAME)).order().create())
                .addColumn(actionColumn(UtilPrintSupport.PRINT_REPRESENT_COLUMN_NAME, CommonDefines.ICON_PRINT, UtilPrintSupport.PRINT_REPRESENT_LISTENER).permissionKey("rmc_print_represent").displayHeader(true).create())
                        //.addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("docOrdRepresentListEdit").disabled("ui:isFormative").create())
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER, alert("representationDS.delete.alert")).permissionKey("rmc_delete_represent").disabled("ui:isFormative").create())
                .create();
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(REPRESENTATION_DS, representationDS()).handler(representationDSHandler()))
                .addDataSource(selectDS(TYPE_REPRESENT_DS, typeRepresentDSHandler()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> representationDSHandler()
    {
        return new RepresentationDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> typeRepresentDSHandler()
    {
        return new TypeRepresentDSHandler(getName());
    }
}