package ru.tandemservice.movestudentrmc.base.bo.ListRepresentTransfer.logic;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.AbstractListRepresentDAO;
import ru.tandemservice.movestudentrmc.docord.util.MoveStudentUtil;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentExtractStatesCodes;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentationTypeCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;

import static org.tandemframework.hibsupport.dql.DQLExpressions.and;
import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.not;
import static org.tandemframework.hibsupport.dql.DQLExpressions.or;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

import java.util.List;
import java.util.Map;

public class ListRepresentTransferManagerModifyDAO extends AbstractListRepresentDAO implements IListRepresentTransferManagerModifyDAO {


    @Override
    public void save(ListRepresent listRepresent,
                     List<Student> studentSelectedList,
                     DocListRepresentBasics representBasics)
    {

        //сохранение
        this.baseCreateOrUpdate(listRepresent);
        this.baseCreateOrUpdate(representBasics);

        //удалим старых студиков
        new DQLDeleteBuilder(RelListRepresentStudents.class)
                .where(DQLExpressions.eqValue(DQLExpressions.property(RelListRepresentStudents.representation().id()), listRepresent.getId()))
                .createStatement(getSession()).execute();

        //сохраним новых студентов
        for (Student st : studentSelectedList) {
            RelListRepresentStudentsOldData rel = new RelListRepresentStudentsOldData();
            rel.setRepresentation(listRepresent);
            rel.setStudent(st);
            rel.setOldEducationOrgUnit(st.getEducationOrgUnit());
            rel.setOldCourse(st.getCourse());
            rel.setOldGroup(st.getGroup());
            rel.setOldCompensationType(st.getCompensationType());
            this.baseCreateOrUpdate(rel);
        }

    }

    @Override
    public boolean doCommit(ListRepresent represent, ErrorCollector error) {
        if (!super.doCommit(represent, UserContext.getInstance().getErrorCollector()))
            return false;

        ListRepresentTransfer listRepresentTransfer = (ListRepresentTransfer) represent;

        List<RelListRepresentStudents> representList = getStudentList(represent);

        List<Student> students = CommonBaseEntityUtil.getPropertiesList(representList, RelListRepresentStudents.student().s());

        EducationOrgUnit educationOrgUnit = listRepresentTransfer.getEducationOrgUnit() != null ? listRepresentTransfer.getEducationOrgUnit() : listRepresentTransfer.getGroupNew().getEducationOrgUnit();

        List<Student> lst = MoveStudentUtil.checkStudentList(students, listRepresentTransfer.getCompensationTypeNew(), educationOrgUnit.getDevelopForm());

        if (!lst.isEmpty()) {
            List<StudentGrantEntity> sgeList = MoveStudentUtil.getSrudentGrantEntityList(lst, listRepresentTransfer.getDateTransfer());
            commitStudentGrantsCancel(listRepresentTransfer, sgeList);
        }

        for (RelListRepresentStudents representStudents : representList) {
            Student student = representStudents.getStudent();

            student.setEducationOrgUnit(educationOrgUnit);

            if (listRepresentTransfer.getCourseNew() != null)
                student.setCourse(listRepresentTransfer.getCourseNew());
            if (listRepresentTransfer.getGroupNew() != null)
                student.setGroup(listRepresentTransfer.getGroupNew());
            if (listRepresentTransfer.getCompensationTypeNew() != null)
                student.setCompensationType(listRepresentTransfer.getCompensationTypeNew());
            
            //-----------------------------------------------------------------------------------------------------------------
            if(listRepresentTransfer.isIndividualPlan()) {
            	student.setPersonalEducation(true);
            }
            //-----------------------------------------------------------------------------------------------------------------
            
            getSession().saveOrUpdate(student);
        }

        return true;
    }

    @Override
    public boolean doRollback(ListRepresent represent, ErrorCollector error) {
        if (!super.doRollback(represent, UserContext.getInstance().getErrorCollector()))
            return false;

        List<RelListRepresentStudents> representList = getStudentList(represent);

        for (RelListRepresentStudents representStudents : representList) {
            RelListRepresentStudentsOldData oldData = (RelListRepresentStudentsOldData) representStudents;
            Student student = oldData.getStudent();
            student.setEducationOrgUnit(oldData.getOldEducationOrgUnit());
            student.setCourse(oldData.getOldCourse());
            student.setGroup(oldData.getOldGroup());
            student.setCompensationType(oldData.getOldCompensationType());
            
            //-----------------------------------------------------------------------------------------------------------
            //Сбросить галочку "Индивидуальное образование", если не было других приказов об утверждении ИУП
            if(((ListRepresentTransfer)represent).isIndividualPlan()) {
            	
            	//Проверка списочных приказов "О переводе" с параметром "Утвердить ИУП"
            	DQLSelectBuilder checkBuilder = new DQLSelectBuilder()
            			.fromEntity(RelListRepresentStudents.class, "rel")
            			.joinEntity("rel", DQLJoinType.inner, ListRepresentTransfer.class, "rep", eq(RelListRepresentStudents.representation().id().fromAlias("rel"), ListRepresentTransfer.id().fromAlias("rep")) )
            			.where(eq(property(RelListRepresentStudents.student().id().fromAlias("rel")), value(student.getId())))
            			.where(not(eq(property(RelListRepresentStudents.representation().id().fromAlias("rel")), value(represent.getId()))))            			
            			.where(eq(property(RelListRepresentStudents.representation().state().code().fromAlias("rel")), value(MovestudentExtractStatesCodes.CODE_6)))
            			.where(eq(property(ListRepresentTransfer.individualPlan().fromAlias("rep")), value(true)))
            		    .column(property(RelListRepresentStudents.representation().id().fromAlias("rel")))
            			;
            	List<Long> checkListRepresentIdList = checkBuilder.createStatement(getSession()).list();
            	
            	//Проверка индивидуальных приказов "О переводе" с параметром "Утвердить ИУП", а также индивидуального приказа "Об утверждении ИУП"
            	DQLSelectBuilder builder = new DQLSelectBuilder()
            			.fromEntity(DocRepresentStudentBase.class, "doc")
            			.joinEntity("doc", DQLJoinType.left, RepresentTransfer.class, "rep", eq(DocRepresentStudentBase.representation().id().fromAlias("doc"), RepresentTransfer.id().fromAlias("rep")) )
            			.where(eq(property(DocRepresentStudentBase.student().id().fromAlias("doc")), value(student.getId())))
            			//.where(not(eq(property(DocRepresentStudentBase.representation().id().fromAlias("doc")), value(rep.getId()))))            			
            			.where(eq(property(DocRepresentStudentBase.representation().state().code().fromAlias("doc")), value(MovestudentExtractStatesCodes.CODE_6)))
            			.where(or(
            				eq(property(DocRepresentStudentBase.representation().type().code().fromAlias("doc")), value(RepresentationTypeCodes.PERSON_WORK_PLAN)), 
            				and(
            					eq(property(DocRepresentStudentBase.representation().type().code().fromAlias("doc")), value(RepresentationTypeCodes.TRANSFER)), 
            					eq(property(RepresentTransfer.individualPlan().fromAlias("rep")), value(true))
            				)
            				))
            		    .column(property(DocRepresentStudentBase.representation().id().fromAlias("doc")))
            			;
            	List<Long> checkRepresentIdList = builder.createStatement(getSession()).list();
            	
            	if(checkListRepresentIdList.size() == 0 && checkRepresentIdList.size() == 0) {
            		student.setPersonalEducation(false);		//Дополнительные данные студента "Индивидуальное образование"
            	}           	

            }            
            //-----------------------------------------------------------------------------------------------------------
            
            getSession().saveOrUpdate(student);
        }

        rollbackStudentGrantsCancel(represent);

        return true;
    }

    @Override
    public Map<Student, OrderParagraphInfo> buildBodyRepresent(
            List<ListRepresent> representationBase, RtfDocument document)
    {
        ListPrintDoc listPrintDoc = new ListPrintDoc();
        RtfDocument templateRepresent = listPrintDoc.creationPrintDocOrder(representationBase);

        document.setHeader(templateRepresent.getHeader());
        document.setSettings(templateRepresent.getSettings());
        document.getElementList().addAll(templateRepresent.getElementList());
        return listPrintDoc.getParagInfomap();
    }

    @Override
    public void buildBodyExtract(ListOrdListRepresent listOrdListRepresent,
                                 Student student, RtfDocument docExtract, Map<Student, OrderParagraphInfo> map)
    {
        super.buildBodyExtract(listOrdListRepresent, student, docExtract, map);

        ListRepresentTransfer listRepresent = (ListRepresentTransfer) listOrdListRepresent.getRepresentation();
        RtfInjectModifier im = new RtfInjectModifier();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RelListRepresentStudentsOldData.class, "rel")
                .where(DQLExpressions.eqValue(DQLExpressions.property(RelListRepresentStudentsOldData.representation().id().fromAlias("rel")), listRepresent.getId()))
                .where(DQLExpressions.eqValue(DQLExpressions.property(RelListRepresentStudentsOldData.student().id().fromAlias("rel")), student.getId()));

        RelListRepresentStudentsOldData oldData = builder.createStatement(getSession()).uniqueResult();

        ListPrintDoc.injectModifier(im, listRepresent, oldData);

        StringBuilder str = new StringBuilder().append(student.getPerson().getFullFio())
                .append(oldData.getOldCompensationType().isBudget() ? "" : " (по договору)");

        im.put("studentTitleWithCompType", str.toString());
        //----------------------------------------------------------------------------------------------------------
        im.put("studentNumber", String.valueOf(map.get(student).getStudentNumber()));
        //----------------------------------------------------------------------------------------------------------

        im.modify(docExtract);
    }

    @Override
    public List<Student> selectRepresent(ListRepresent listRepresent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void saveSupport(ListRepresent listRepresent,
                            List<DataWrapper> studentSelectedList,
                            DocListRepresentBasics representBasics)
    {
        // TODO Auto-generated method stub

    }

}
