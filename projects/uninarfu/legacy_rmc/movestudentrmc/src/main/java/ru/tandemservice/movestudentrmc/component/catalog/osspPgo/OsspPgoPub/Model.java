package ru.tandemservice.movestudentrmc.component.catalog.osspPgo.OsspPgoPub;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubModel;
import ru.tandemservice.movestudentrmc.entity.catalog.OsspPgo;

public class Model extends DefaultCatalogPubModel<OsspPgo> {
    public static class OsspPgoWrapper extends
            IdentifiableWrapper<OsspPgo>
    {
        private static final long serialVersionUID = -4301478784488247009L;
        public static final String PRINTABLE = "printable";

        private OsspPgo osspPgo;
        private boolean printable;

        public OsspPgoWrapper(
                OsspPgo osspPgo,
                boolean printable)
        {
            super(osspPgo.getId(), osspPgo
                    .getTitle());
            this.osspPgo = osspPgo;
            this.printable = printable;
        }

        public OsspPgo getOsspGrants() {
            return osspPgo;
        }

        public boolean isPrintable() {
            return printable;
        }

        public void setPrintable(boolean printable) {
            this.printable = printable;
        }

    }
}
