package ru.tandemservice.movestudentrmc.base.bo.DOOrder.ui.Edit;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.DOOrderManager;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.logic.RepresentDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.logic.RepresentSelectedDSHandler;
import ru.tandemservice.movestudentrmc.entity.DocumentOrder;
import ru.tandemservice.movestudentrmc.entity.catalog.MovestudentOrderStates;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;


@State({
        @Bind(key = DOOrderEditUI.ORDER_ID, binding = DOOrderEditUI.ORDER_ID)
})
public class DOOrderEditUI extends UIPresenter {

    public static final String ORDER_ID = "orderId";

    private Long _orderId;
    private DocumentOrder _order;
    private List<DataWrapper> _representSelectedList;

//    События компонента

    @Override
    public void onComponentRefresh()
    {
        if (_orderId != null) {
            _order = DataAccessServices.dao().get(DocumentOrder.class, DocumentOrder.id().s(), _orderId);
            _representSelectedList = DOOrderManager.instance().modifyDao().selectRepresent(_order);
        }
        else {
            _order = new DocumentOrder();
            _order.setCreateDate(getSupport().getCurrentDate());
            _order.setCommitDate(getSupport().getCurrentDate());
            _order.setCommitted(false);
            _order.setState(DataAccessServices.dao().get(MovestudentOrderStates.class, MovestudentOrderStates.code().s(), "1"));
            _representSelectedList = new ArrayList<DataWrapper>();
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (DOOrderEdit.REPRESENT_DS.equals(dataSource.getName())) {

            Map<String, Object> settingMap = _uiSettings.getAsMap(
                    RepresentDSHandler.DATE_REPRESENT_FILTER,
                    RepresentDSHandler.DEVELOP_FORM_FILTER,
                    RepresentDSHandler.FORMATIVE_ORG_UNIT_FILTER,
                    RepresentDSHandler.STUDENT_FIO_FILTER,
                    RepresentDSHandler.TYPE_REPRESENT_FILTER,
                    RepresentDSHandler.DECLARATION_FILTER
            );
            dataSource.putAll(settingMap);
            dataSource.put(RepresentDSHandler.REPRESENT_SELECTED_LIST, _representSelectedList);
            dataSource.put(RepresentDSHandler.ORDER_ID, _orderId);
        }
        else if (DOOrderEdit.REPRESENT_SELECTED_DS.equals(dataSource.getName())) {

            dataSource.put(RepresentSelectedDSHandler.REPRESENT_SELECTED_LIST, _representSelectedList);
        }
    }

    //    События формы

    public void selectRepresent() {

        BaseSearchListDataSource representDS = (BaseSearchListDataSource) getConfig().getDataSource(DOOrderEdit.REPRESENT_DS);

        Collection<IEntity> records = ((CheckboxColumn) representDS.getLegacyDataSource().getColumn(RepresentDSHandler.CHECKBOX_COLUMN)).getSelectedObjects();

        for (IEntity record : records) {
            if (!_representSelectedList.contains((DataWrapper) record))
                _representSelectedList.add((DataWrapper) record);
        }
    }

    public void deleteSelectedRepresent() {

        BaseSearchListDataSource representDS = (BaseSearchListDataSource) getConfig().getDataSource(DOOrderEdit.REPRESENT_SELECTED_DS);

        Collection<IEntity> records = ((CheckboxColumn) representDS.getLegacyDataSource().getColumn(RepresentSelectedDSHandler.CHECKBOX_COLUMN)).getSelectedObjects();

        for (IEntity record : records) {
            _representSelectedList.remove((DataWrapper) record);
        }
    }

    public void onClickSave() {

        if (_representSelectedList.isEmpty())
            getConfig().getUserContext().getErrorCollector().add(getConfig().getProperty("ui.alertEmptySelectedRepresent"));

        if (getConfig().getUserContext().getErrorCollector().hasErrors())
            return;

        if (_orderId == null)
            DOOrderManager.instance().modifyDao().save(_order, _representSelectedList);
        else
            DOOrderManager.instance().modifyDao().update(_order, _representSelectedList);

        deactivate();
    }

//    Вспомогательные функции


//    Getters and Setters

    public Long getOrderId() {
        return _orderId;
    }

    public void setOrderId(Long orderId) {
        _orderId = orderId;
    }

    public DocumentOrder getOrder() {
        return _order;
    }

    public void setOrder(DocumentOrder order) {
        _order = order;
    }
    /*
    public void onViewRepresentationFromList() {    	

    	if (_orderId != null) {
        	List<DataWrapper> representSelect = DOOrderManager.instance().modifyDao().selectRepresent(_order);
        	
        	String type = "";
        	for (DataWrapper representW : representSelect) {
        		Representation repr = (Representation) representW.getProperty("represent");
        		if (repr.getId() == getListenerParameterAsLong()) {
        			type = repr.getType().getCode();
        			break;
        		}        		
        	}
        	String componentName = RepresentationUtils.getStudentRepresentationViewComponent(type);
        	this.getUserContext().getCurrentComponent().getParentRegion().activate(new ComponentActivator(componentName, (new UniMap()).add("representationId", getListenerParameter())));
        } 
    }
	*/
}
