package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ui.Student;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.ListOrderListRepresentDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.ListRepresentDSHandler;
import ru.tandemservice.movestudentrmc.entity.ListOrder;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;


@Configuration
public class ListRepresentBaseStudent extends BusinessComponentManager {

    public static final String LIST_REPRESENT_DS = "listRepresentDS";

    @Bean
    public ColumnListExtPoint listRepresentDS()
    {

        return columnListExtPointBuilder(LIST_REPRESENT_DS)
                //.addColumn(textColumn(ListRepresentDSHandler.REPRESENT_TITLE_COLUMN,"represent."+ListRepresent.title()).width("400px").permissionKey("rmc_view_list_represent_Button").order().create())
//                .addColumn(actionColumn(ListRepresentDSHandler.REPRESENT_TITLE_COLUMN,"represent."+ListRepresent.title(),"onClickView").width("400px").permissionKey("rmc_view_list_represent_Button").order().create())
                .addColumn(publisherColumn(ListRepresentDSHandler.REPRESENT_TITLE_COLUMN, "represent." + ListRepresent.title()).width("400px").order().create())
                .addColumn(textColumn("createDate", "represent." + ListRepresent.createDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order().create())

                .addColumn(textColumn(ListRepresentDSHandler.STATE_COLUMN, "represent." + ListRepresent.state().title()).order().create())

                        //.addColumn(textColumn("orderNumber", "order."+ListOrder.number()).order().create())
                .addColumn(publisherColumn("orderNumber", "order." + ListOrder.number())
                                   .publisherLinkResolver(
                                           new IPublisherLinkResolver() {
                                               @Override
                                               public Object getParameters(IEntity entity) {
                                                   return new UniMap().add(PublisherActivator.PUBLISHER_ID_KEY, ((ListRepresent) ((DataWrapper) entity).getWrapped()).getOrder().getId());
                                               }

                                               @Override
                                               public String getComponentName(IEntity entity) {
                                                   return null;
                                               }
                                           }
                                   ).order().create())
                .addColumn(textColumn("orderState", "order." + ListOrder.state().title()).order().create())
                .addColumn(textColumn("orderСreateDate", "order." + ListOrder.createDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order().create())
                .addColumn(booleanColumn(ListRepresentDSHandler.CHECK, "represent." + ListRepresent.check()).order().create())
                        //.addColumn(actionColumn("printExtractColumn", CommonDefines.ICON_PRINT, "onClickPrintFromList").displayHeader(true).disabled(true).permissionKey("rmc_print_extracts_list_represent_Button").create())
                .addColumn(actionColumn("printRepresent", CommonDefines.ICON_PRINT, "onClickPrintFromList").displayHeader(true).permissionKey("rmc_print_list_represent_Button").create())
                .create();
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(LIST_REPRESENT_DS, listRepresentDS()).handler(listRepresentDSHandler()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> listRepresentDSHandler()
    {
        return new ListOrderListRepresentDSHandler(getName());
    }


}
