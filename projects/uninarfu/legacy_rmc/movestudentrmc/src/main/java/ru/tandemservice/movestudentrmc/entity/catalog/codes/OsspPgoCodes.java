package ru.tandemservice.movestudentrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "ПГО ОССП"
 * Имя сущности : osspPgo
 * Файл data.xml : movementstudent.data.xml
 */
public interface OsspPgoCodes
{
    /** Константа кода (code) элемента : ossppgo-1-1 (code). Название (title) : с зачислением на полное государственное обеспечение */
    String OSSPPGO_1_1 = "ossppgo-1-1";
    /** Константа кода (code) элемента : ossppgo-1-2 (code). Название (title) : с сохранением полного государственного обеспечения */
    String OSSPPGO_1_2 = "ossppgo-1-2";
    /** Константа кода (code) элемента : ossppgo-1-3 (code). Название (title) : со снятием с полного государственного обеспечения */
    String OSSPPGO_1_3 = "ossppgo-1-3";
    /** Константа кода (code) элемента : none (code). Название (title) : - */
    String NONE = "none";

    Set<String> CODES = ImmutableSet.of(OSSPPGO_1_1, OSSPPGO_1_2, OSSPPGO_1_3, NONE);
}
