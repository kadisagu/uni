package ru.tandemservice.movestudentrmc.base.bo.ListRepresentCourseTransfer.ui.View;

import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.support.AbstractListRepresentViewUI;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentCourseTransfer.logic.ListPrintDoc;
import ru.tandemservice.movestudentrmc.entity.ListRepresentCourseTransfer;

import java.util.Collections;

public class ListRepresentCourseTransferViewUI extends AbstractListRepresentViewUI<ListRepresentCourseTransfer>
{

    @Override
    public ListRepresentCourseTransfer getListRepresentObject() {
        return new ListRepresentCourseTransfer();
    }

    @Override
    protected RtfDocument printRepresent() {
        return new ListPrintDoc().creationPrintDocOrder(Collections.singletonList(getListRepresent()));
    }

}
