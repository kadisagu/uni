package ru.tandemservice.movestudentrmc.base.bo.GrantsRelationSettings.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.base.bo.GrantsRelationSettings.DOGrantsRelationSettingsManager;
import ru.tandemservice.movestudentrmc.base.bo.GrantsRelationSettings.logic.FakeGrantsHandler;
import ru.tandemservice.movestudentrmc.base.bo.GrantsRelationSettings.ui.EditRel.GrantsRelationSettingsEditRel;
import ru.tandemservice.movestudentrmc.base.bo.GrantsRelationSettings.ui.EditRel.GrantsRelationSettingsEditRelUI;
import ru.tandemservice.movestudentrmc.entity.RelTypeGrantView;
import ru.tandemservice.movestudentrmc.entity.catalog.Grant;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantType;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;

import java.util.ArrayList;
import java.util.List;

@Input(
        @Bind(key = GrantsRelationSettingsAddEditUI.GRANT_TYPE_ID, binding = GrantsRelationSettingsAddEditUI.GRANT_TYPE_ID)
)
public class GrantsRelationSettingsAddEditUI extends UIPresenter {

    public static final String GRANT_TYPE_ID = "grantTypeId";

    private Long grantTypeId;
    private GrantType grantType;
    private List<FakeGrantsHandler> grants;
    private List<Grant> grantsList;
    private FakeGrantsHandler grantRow;
    private String pageView;
    private int pageNum = 0;
    private int pageEnd = 1;
    private int countGrants = 0;
    private boolean firstRun = true;
    private boolean isAddForm = true;
    private boolean hasConfiguration = false;
    private List<RelTypeGrantView> listGrantView;

    @Override
    public void onComponentRefresh()
    {

        setPageView(getClass().getPackage().getName() + ".page" + (getPageNum()));

        if (!firstRun) {

            return;
        }

        if (null != grantTypeId)
            grantType = DataAccessServices.dao().getNotNull(GrantType.class, grantTypeId);
        else {
            grantType = new GrantType();
        }

        if (grantType.isConfigRelation()) {

            setPageNum(getPageNum() + 1);
            setPageView(getClass().getPackage().getName() + ".page" + (getPageNum()));
            isAddForm = false;
            hasConfiguration = true;
            fillArrayGrants();
        }
        else {
            setPageView(getClass().getPackage().getName() + ".page" + (getPageNum()));
            isAddForm = false;
            hasConfiguration = false;
            fillArrayGrants();

        }

        if (firstRun)
            firstRun = !firstRun;
    }

    private void fillArrayGrants() {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RelTypeGrantView.class, "rel").addColumn("rel");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(RelTypeGrantView.type().id().fromAlias("rel")),
                DQLExpressions.value(grantTypeId)));

        listGrantView = builder.createStatement(getSupport().getSession()).list();

        if (getGrantsList() == null)
            setGrantsList(new ArrayList<Grant>());
        else
            getGrantsList().clear();

        if (getGrants() == null)
            setGrants(new ArrayList<FakeGrantsHandler>());
        else
            getGrants().clear();

        for (RelTypeGrantView grantView : listGrantView) {

            Boolean isFind = false;
            for (FakeGrantsHandler fakeGrant : getGrants()) {

                if (fakeGrant.getGrant().equals(grantView.getGrant())) {
                    isFind = true;
                    fakeGrant.getViewList().add(grantView.getView());
                    break;
                }
            }

            if (!isFind) {

                FakeGrantsHandler fakeGrant = new FakeGrantsHandler();
                fakeGrant.setGrant(grantView.getGrant());
                fakeGrant.setEdit(true);
                fakeGrant.setViewList(new ArrayList<GrantView>());
                fakeGrant.getViewList().add(grantView.getView());
                getGrants().add(fakeGrant);
                if (!getGrantsList().contains(grantView.getView()))
                    getGrantsList().add(grantView.getGrant());
            }
        }
    }

    public void onClickEditRel() {

        _uiActivation.asRegion(GrantsRelationSettingsEditRel.class)
                .parameter(GrantsRelationSettingsEditRelUI.GRANT_ID, getListenerParameterAsLong())
                .parameter(GrantsRelationSettingsEditRelUI.GRANT_TYPE_ID, getGrantTypeId())
                .activate();
    }

    public void onClickApply()
    {
        if (isAddForm) {
            DOGrantsRelationSettingsManager.instance().modifyDao().saveGrantType(grantType, getGrants());
            isAddForm = false;
        }
        else
            DOGrantsRelationSettingsManager.instance().modifyDao().updateGrantType(grantType, getGrants(), listGrantView);

        fillArrayGrants();
        //deactivate();
    }

    public void onClickPrev() {

        setPageNum(getPageNum() - 1);
        setPageView(getClass().getPackage().getName() + ".page" + (getPageNum()));
    }

    public void onClickNext() {

        if (pageNum == 0) {
            mergeArraysGrants();
        }

        setPageNum(getPageNum() + 1);
        setPageView(getClass().getPackage().getName() + ".page" + (getPageNum()));
    }

    @Override
    public void onComponentPrepareRender() {

        super.onComponentPrepareRender();
    }

    public void mergeArraysGrants() {

        List<FakeGrantsHandler> isFound = new ArrayList<FakeGrantsHandler>();

        if (getGrants() == null)
            setGrants(new ArrayList<FakeGrantsHandler>());
        for (Grant grant : getGrantsList()) {
            Boolean isFind = false;
            for (FakeGrantsHandler fakeGrant : getGrants()) {
                if (fakeGrant.getGrant().equals(grant)) {
                    isFound.add(fakeGrant);
                    isFind = true;
                    break;
                }
            }

            if (!isFind) {
                FakeGrantsHandler fakeGrant = new FakeGrantsHandler();
                fakeGrant.setGrant(grant);
                isFound.add(fakeGrant);
            }
        }

        getGrants().clear();
        getGrants().addAll(isFound);
    }

//    Getters and Setters

    public List<Grant> getGrantsList() {
        return grantsList;
    }

    public void setGrantsList(List<Grant> grantsList) {
        this.grantsList = grantsList;
    }

    public List<FakeGrantsHandler> getGrants() {
        return grants;
    }

    public int getCountGrants() {
        this.countGrants = grants.size() + 1;
        return countGrants;
    }

    public void setGrants(List<FakeGrantsHandler> grants) {
        this.grants = grants;
    }

    public FakeGrantsHandler getGrantRow() {
        return grantRow;
    }

    public void setGrantRow(FakeGrantsHandler grantRow) {
        this.grantRow = grantRow;
    }

    public String getPageView() {
        return pageView;
    }

    public void setPageView(String pageView) {
        this.pageView = pageView;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageEnd() {
        return pageEnd;
    }

    public void setPageEnd(int pageEnd) {
        this.pageEnd = pageEnd;
    }

    public Long getGrantTypeId() {
        return grantTypeId;
    }

    public void setGrantTypeId(Long grantTypeId) {
        this.grantTypeId = grantTypeId;
    }

    public GrantType getGrantType() {
        return grantType;
    }

    public void setGrantType(GrantType grantType) {
        this.grantType = grantType;
    }
}
