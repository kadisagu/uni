package ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantCancelAndDestination.logic;

import ru.tandemservice.movestudentrmc.docord.IListObjectModifyDAO;
import ru.tandemservice.movestudentrmc.entity.DocRepresentOrderCancel;
import ru.tandemservice.movestudentrmc.entity.ListRepresentGrantCancelAndDestination;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntity;

import java.util.List;

public interface IListRepresentGrantCancelAndDestinationManagerModifyDAO extends IListObjectModifyDAO
{

    public void saveStudentGrantEntity(List<StudentGrantEntity> sgeList, ListRepresentGrantCancelAndDestination represent);

    public void deleteStudentGrantEntity(Long representId);

    public void saveOrderCancel(List<DocRepresentOrderCancel> ordersList);

    public void deleteOrderCancel(Long representId);
}
