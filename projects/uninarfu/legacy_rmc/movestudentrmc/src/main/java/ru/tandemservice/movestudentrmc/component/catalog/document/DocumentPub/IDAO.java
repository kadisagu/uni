package ru.tandemservice.movestudentrmc.component.catalog.document.DocumentPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;
import ru.tandemservice.movestudentrmc.entity.catalog.Document;


public interface IDAO extends IDefaultCatalogPubDAO<Document, Model> {

    public void change(Document document, String field, Boolean value);
}
