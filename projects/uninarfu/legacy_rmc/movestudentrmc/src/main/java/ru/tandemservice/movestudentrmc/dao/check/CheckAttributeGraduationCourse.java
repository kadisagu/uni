package ru.tandemservice.movestudentrmc.dao.check;

import ru.tandemservice.uni.entity.employee.Student;

public class CheckAttributeGraduationCourse extends AbstractCheckDAO {

    @Override
    protected boolean getCondition(Student student) {
        return (student.getCourse().getIntValue() == student
                .getEducationOrgUnit().getDevelopPeriod().getLastCourse());
    }

    @Override
    protected String getErrorMessage(Student student, boolean value) {
        return new StringBuilder()
                .append("Студент ")
                .append(student.getPerson().getFullFio())
                .append(" ")
                .append(value ? "не " : "")
                .append("обучается на последнем курсе (")
                .append(student.getCourse().getIntValue())
                .append(" курс).")
                .toString();
    }

}
