package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers;

import org.tandemframework.caf.command.BusinessCommand;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;

public class EducationOrgUnitDSHandler extends DefaultComboDataSourceHandler {

    public EducationOrgUnitDSHandler(String ownerId) {

        super(ownerId, OrgUnit.class);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        DQLSelectBuilder orgUnitBuiilder = new DQLSelectBuilder();
        orgUnitBuiilder.fromEntity(OrgUnitToKindRelation.class, "org");
        orgUnitBuiilder.where(DQLExpressions.and(
                DQLExpressions.eq(DQLExpressions.property(OrgUnitToKindRelation.orgUnit().archival().fromAlias("org")), DQLExpressions.value(false)),
                DQLExpressions.eq(DQLExpressions.property(OrgUnitToKindRelation.orgUnitKind().code().fromAlias("org")), DQLExpressions.value("1"))
        ));

        orgUnitBuiilder.addColumn(DQLExpressions.property(OrgUnitToKindRelation.orgUnit().fromAlias("org")));

        ep.dqlBuilder.where(DQLExpressions.in(DQLExpressions.property(OrgUnit.id().fromAlias("e")), orgUnitBuiilder.getQuery()));

    }

    @Override
    protected void doAfterExecute(BusinessCommand<DSInput, DSOutput> arg0,
                                  DSOutput arg1, ExecutionContext arg2)
    {
        super.doAfterExecute(arg0, arg1, arg2);
/*		List orgUnitList = UniDaoFacade.getOrgstructDao().getOrgUnitList("1");
        arg1.setRecordList(orgUnitList);*/

    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
    {
        //ep.dqlBuilder.order("e." + PersonBenefit.benefit().title());
    }

}
