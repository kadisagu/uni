package ru.tandemservice.movestudentrmc.component.represent.StudentAdmissionToPractice;


import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.catalog.PracticeBase;

import java.util.List;

@Input({
        @Bind(key = "list", binding = "list"),
        @Bind(key = "represent", binding = "represent")
})
public class Model {

    private List<DataWrapper> list;
    private ListRepresent _represent;

    private DynamicListDataSource<DataWrapper> dataSource;

    //-------------------------------------------------------
    //private List<PracticeBase> practiceBaseList;
    private ISelectModel practiceBaseModel;
    //-------------------------------------------------------
    private ISelectModel innerAdvisorModel;
    private Employee innerAdvisor;

    public Employee getInnerAdvisor() {
        return innerAdvisor;
    }

    public void setInnerAdvisor(Employee innerAdvisor) {
        this.innerAdvisor = innerAdvisor;
    }

    public ISelectModel getInnerAdvisorModel() {
        return innerAdvisorModel;
    }

    public void setInnerAdvisorModel(ISelectModel innerAdvisorModel) {
        this.innerAdvisorModel = innerAdvisorModel;
    }

    //-----------------------------------------------------------------------------------
    //public List<PracticeBase> getPracticeBaseList() {
        //return practiceBaseList;
    //}

    //public void setPracticeBaseList(List<PracticeBase> practiceBaseList) {
        //this.practiceBaseList = practiceBaseList;
    //}
    
    public ISelectModel getPracticeBaseModel() {
        return practiceBaseModel;
    }

    public void setPracticeBaseModel(ISelectModel practiceBaseModel) {
        this.practiceBaseModel = practiceBaseModel;
    }    
    //-----------------------------------------------------------------------------------

    public DynamicListDataSource<DataWrapper> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<DataWrapper> dataSource) {
        this.dataSource = dataSource;
    }

    public void setRepresent(ListRepresent represent)
    {
        this._represent = represent;
    }

    public ListRepresent getRepresent()
    {
        return this._represent;
    }

    public List<DataWrapper> getList() {
        return list;
    }

    public void setList(List<DataWrapper> list) {
        this.list = list;
    }

}
