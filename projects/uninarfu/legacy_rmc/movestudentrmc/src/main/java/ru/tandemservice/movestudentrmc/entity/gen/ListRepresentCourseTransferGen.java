package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListRepresentCourseTransfer;
import ru.tandemservice.uni.entity.catalog.Course;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Списочное представление О переводе на следующий курс
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ListRepresentCourseTransferGen extends ListRepresent
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.ListRepresentCourseTransfer";
    public static final String ENTITY_NAME = "listRepresentCourseTransfer";
    public static final int VERSION_HASH = -340862569;
    private static IEntityMeta ENTITY_META;

    public static final String P_DATE_BEGINING_TRANSFER = "dateBeginingTransfer";
    public static final String L_NEW_COURSE = "newCourse";
    public static final String P_CONDITIONALLY = "conditionally";
    public static final String P_DEADLINE_DATE = "deadlineDate";

    private Date _dateBeginingTransfer;     // Дата перевода
    private Course _newCourse;     // Новый курс
    private boolean _conditionally = false;     // Условный перевод
    private Date _deadlineDate;     // Дата ликвидации академической задолженности

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата перевода. Свойство не может быть null.
     */
    @NotNull
    public Date getDateBeginingTransfer()
    {
        return _dateBeginingTransfer;
    }

    /**
     * @param dateBeginingTransfer Дата перевода. Свойство не может быть null.
     */
    public void setDateBeginingTransfer(Date dateBeginingTransfer)
    {
        dirty(_dateBeginingTransfer, dateBeginingTransfer);
        _dateBeginingTransfer = dateBeginingTransfer;
    }

    /**
     * @return Новый курс. Свойство не может быть null.
     */
    @NotNull
    public Course getNewCourse()
    {
        return _newCourse;
    }

    /**
     * @param newCourse Новый курс. Свойство не может быть null.
     */
    public void setNewCourse(Course newCourse)
    {
        dirty(_newCourse, newCourse);
        _newCourse = newCourse;
    }

    /**
     * @return Условный перевод. Свойство не может быть null.
     */
    @NotNull
    public boolean isConditionally()
    {
        return _conditionally;
    }

    /**
     * @param conditionally Условный перевод. Свойство не может быть null.
     */
    public void setConditionally(boolean conditionally)
    {
        dirty(_conditionally, conditionally);
        _conditionally = conditionally;
    }

    /**
     * @return Дата ликвидации академической задолженности.
     */
    public Date getDeadlineDate()
    {
        return _deadlineDate;
    }

    /**
     * @param deadlineDate Дата ликвидации академической задолженности.
     */
    public void setDeadlineDate(Date deadlineDate)
    {
        dirty(_deadlineDate, deadlineDate);
        _deadlineDate = deadlineDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ListRepresentCourseTransferGen)
        {
            setDateBeginingTransfer(((ListRepresentCourseTransfer)another).getDateBeginingTransfer());
            setNewCourse(((ListRepresentCourseTransfer)another).getNewCourse());
            setConditionally(((ListRepresentCourseTransfer)another).isConditionally());
            setDeadlineDate(((ListRepresentCourseTransfer)another).getDeadlineDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ListRepresentCourseTransferGen> extends ListRepresent.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ListRepresentCourseTransfer.class;
        }

        public T newInstance()
        {
            return (T) new ListRepresentCourseTransfer();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "dateBeginingTransfer":
                    return obj.getDateBeginingTransfer();
                case "newCourse":
                    return obj.getNewCourse();
                case "conditionally":
                    return obj.isConditionally();
                case "deadlineDate":
                    return obj.getDeadlineDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "dateBeginingTransfer":
                    obj.setDateBeginingTransfer((Date) value);
                    return;
                case "newCourse":
                    obj.setNewCourse((Course) value);
                    return;
                case "conditionally":
                    obj.setConditionally((Boolean) value);
                    return;
                case "deadlineDate":
                    obj.setDeadlineDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "dateBeginingTransfer":
                        return true;
                case "newCourse":
                        return true;
                case "conditionally":
                        return true;
                case "deadlineDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "dateBeginingTransfer":
                    return true;
                case "newCourse":
                    return true;
                case "conditionally":
                    return true;
                case "deadlineDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "dateBeginingTransfer":
                    return Date.class;
                case "newCourse":
                    return Course.class;
                case "conditionally":
                    return Boolean.class;
                case "deadlineDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ListRepresentCourseTransfer> _dslPath = new Path<ListRepresentCourseTransfer>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ListRepresentCourseTransfer");
    }
            

    /**
     * @return Дата перевода. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentCourseTransfer#getDateBeginingTransfer()
     */
    public static PropertyPath<Date> dateBeginingTransfer()
    {
        return _dslPath.dateBeginingTransfer();
    }

    /**
     * @return Новый курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentCourseTransfer#getNewCourse()
     */
    public static Course.Path<Course> newCourse()
    {
        return _dslPath.newCourse();
    }

    /**
     * @return Условный перевод. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentCourseTransfer#isConditionally()
     */
    public static PropertyPath<Boolean> conditionally()
    {
        return _dslPath.conditionally();
    }

    /**
     * @return Дата ликвидации академической задолженности.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentCourseTransfer#getDeadlineDate()
     */
    public static PropertyPath<Date> deadlineDate()
    {
        return _dslPath.deadlineDate();
    }

    public static class Path<E extends ListRepresentCourseTransfer> extends ListRepresent.Path<E>
    {
        private PropertyPath<Date> _dateBeginingTransfer;
        private Course.Path<Course> _newCourse;
        private PropertyPath<Boolean> _conditionally;
        private PropertyPath<Date> _deadlineDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата перевода. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentCourseTransfer#getDateBeginingTransfer()
     */
        public PropertyPath<Date> dateBeginingTransfer()
        {
            if(_dateBeginingTransfer == null )
                _dateBeginingTransfer = new PropertyPath<Date>(ListRepresentCourseTransferGen.P_DATE_BEGINING_TRANSFER, this);
            return _dateBeginingTransfer;
        }

    /**
     * @return Новый курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentCourseTransfer#getNewCourse()
     */
        public Course.Path<Course> newCourse()
        {
            if(_newCourse == null )
                _newCourse = new Course.Path<Course>(L_NEW_COURSE, this);
            return _newCourse;
        }

    /**
     * @return Условный перевод. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentCourseTransfer#isConditionally()
     */
        public PropertyPath<Boolean> conditionally()
        {
            if(_conditionally == null )
                _conditionally = new PropertyPath<Boolean>(ListRepresentCourseTransferGen.P_CONDITIONALLY, this);
            return _conditionally;
        }

    /**
     * @return Дата ликвидации академической задолженности.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentCourseTransfer#getDeadlineDate()
     */
        public PropertyPath<Date> deadlineDate()
        {
            if(_deadlineDate == null )
                _deadlineDate = new PropertyPath<Date>(ListRepresentCourseTransferGen.P_DEADLINE_DATE, this);
            return _deadlineDate;
        }

        public Class getEntityClass()
        {
            return ListRepresentCourseTransfer.class;
        }

        public String getEntityName()
        {
            return "listRepresentCourseTransfer";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
