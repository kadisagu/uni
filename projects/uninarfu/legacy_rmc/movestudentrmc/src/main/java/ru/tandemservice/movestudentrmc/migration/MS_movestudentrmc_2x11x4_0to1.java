package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_movestudentrmc_2x11x4_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.4"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.11.4")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность listRepresentGrant

		// удалено свойство educationYear
		{
			// удалить колонку
			tool.dropColumn("listrepresentgrant_t", "educationyear_id");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность listRepresentGrantCancel

		// удалено свойство educationYear
		{
			// удалить колонку
			tool.dropColumn("listrepresentgrantcancel_t", "educationyear_id");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность listRepresentGrantCancelAndDestination

		// удалено свойство educationYear
		{
			// удалить колонку
			tool.dropColumn("lstrprsntgrntcnclanddstntn_t", "educationyear_id");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность listRepresentGrantResume

		// удалено свойство educationYear
		{
			// удалить колонку
			tool.dropColumn("listrepresentgrantresume_t", "educationyear_id");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность listRepresentGrantSuspend

		// удалено свойство educationYear
		{
			// удалить колонку
			tool.dropColumn("listrepresentgrantsuspend_t", "educationyear_id");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность listRepresentTransfer

		// создано обязательное свойство individualPlan
		{
			// создать колонку
			tool.createColumn("listrepresenttransfer_t", new DBColumn("individualplan_p", DBType.BOOLEAN));


			// задать значение по умолчанию
			tool.executeUpdate("update listrepresenttransfer_t set individualplan_p=? where individualplan_p is null", Boolean.FALSE);

			// сделать колонку NOT NULL
			tool.setColumnNullable("listrepresenttransfer_t", "individualplan_p", false);
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representation

		// создано свойство creator
		{
			// создать колонку
			tool.createColumn("representation_t", new DBColumn("creator_id", DBType.LONG));
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representGrant

		// удалено свойство educationYear
		{
			// удалить колонку
			tool.dropColumn("representgrant_t", "educationyear_id");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representGrantCancel

		// удалено свойство educationYear
		{
			// удалить колонку
			tool.dropColumn("representgrantcancel_t", "educationyear_id");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representGrantCancelAndDestination

		// удалено свойство educationYear
		{
			// удалить колонку
			tool.dropColumn("rprsntgrntcnclanddstntn_t", "educationyear_id");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representGrantResume

		// удалено свойство educationYear
		{
			// удалить колонку
			tool.dropColumn("representgrantresume_t", "educationyear_id");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representGrantSuspend

		// удалено свойство educationYear
		{
			// удалить колонку
			tool.dropColumn("representgrantsuspend_t", "educationyear_id");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representTransfer

		// создано обязательное свойство individualPlan
		{
			// создать колонку
			tool.createColumn("representtransfer_t", new DBColumn("individualplan_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			tool.executeUpdate("update representtransfer_t set individualplan_p=? where individualplan_p is null", Boolean.FALSE);

			// сделать колонку NOT NULL
			tool.setColumnNullable("representtransfer_t", "individualplan_p", false);
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representWeekend

		// создано обязательное свойство withPayment
		{
			// создать колонку
			tool.createColumn("representweekend_t", new DBColumn("withpayment_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			tool.executeUpdate("update representweekend_t set withpayment_p=? where withpayment_p is null", Boolean.FALSE);

			// сделать колонку NOT NULL
			tool.setColumnNullable("representweekend_t", "withpayment_p", false);
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representWeekendOut

		// создано обязательное свойство differenceLiquidation
		{
			// создать колонку
			tool.createColumn("representweekendout_t", new DBColumn("differenceliquidation_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			tool.executeUpdate("update representweekendout_t set differenceliquidation_p=? where differenceliquidation_p is null", Boolean.FALSE);

			// сделать колонку NOT NULL
			tool.setColumnNullable("representweekendout_t", "differenceliquidation_p", false);
		}

		// создано свойство differenceLiquidationDate
		{
			// создать колонку
			tool.createColumn("representweekendout_t", new DBColumn("differenceliquidationdate_p", DBType.TIMESTAMP));
		}

		// создано обязательное свойство debtLiquidation
		{
			// создать колонку
			tool.createColumn("representweekendout_t", new DBColumn("debtliquidation_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			tool.executeUpdate("update representweekendout_t set debtliquidation_p=? where debtliquidation_p is null", Boolean.FALSE);

			// сделать колонку NOT NULL
			tool.setColumnNullable("representweekendout_t", "debtliquidation_p", false);
		}

		// создано свойство debtLiquidationDate
		{
			// создать колонку
			tool.createColumn("representweekendout_t", new DBColumn("debtliquidationdate_p", DBType.TIMESTAMP));
		}
    }
}