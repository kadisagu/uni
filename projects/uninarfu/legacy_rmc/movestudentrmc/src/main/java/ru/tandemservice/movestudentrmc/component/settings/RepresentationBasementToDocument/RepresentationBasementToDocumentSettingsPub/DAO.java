package ru.tandemservice.movestudentrmc.component.settings.RepresentationBasementToDocument.RepresentationBasementToDocumentSettingsPub;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.movestudentrmc.entity.RelRepresentationBasementDocument;
import ru.tandemservice.uni.component.settings.abstractRelation.AbstractRelationPub.AbstractRelationPubDAO;

public class DAO extends AbstractRelationPubDAO<Model> implements IDAO {

    @Override
    public void prepare(Model model) {

        super.prepare(model);

        MQBuilder builder = new MQBuilder(RelRepresentationBasementDocument.ENTITY_NAME, "o")
                .add(MQExpression.eq("o", "first", model.getFirst()));

        model.setFirstExist(builder.getResultCount(getSession()) > 0 ? true : false);


    }

    @Override
    protected String getRelationEntityName() {
        return "ru.tandemservice.movestudentrmc.entity.RelRepresentationBasementDocument";
    }

}
