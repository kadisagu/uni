package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntity;
import ru.tandemservice.movestudentrmc.entity.StudentPracticeData;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StudentSelectedDSHandlerPractice extends AbstractSearchDataSourceHandler<DSInput, DSOutput> {

    public static final String DEVELOP_FORM_COLUMN = "developForm";
    public static final String FORMATIVE_ORG_UNIT_COLUMN = "formativeOrgUnit";
    public static final String TERRITORIAL_ORG_UNIT_COLUMN = "territorialOrgUnit";
    public static final String EDUCATION_LEVEL_HIGH_SCHOOL_COLUMN = "educationLevelHighSchool";
    public static final String EDUCATION_ORG_UNIT_COLUMN = "educationOrgUnit";
    public static final String TYPE_REPRESENT_COLUMN = "typeRepresent";
    public static final String COURSE_COLUMN = "course";
    public static final String GROUP_COLUMN = "group";
    public static final String BUDGET_COLUMN = "budget";
    public static final String DATE_REPRESENT_COLUMN = "dateRepresent";
    public static final String STUDENT_FIO_COLUMN = "studentFio";
    public static final String CHECKBOX_COLUMN = "checkbox";
    public static final String STATUS_COLUMN = "status";
    public static final String COMPENSATION_TYPE_COLUMN = "compensationType";
    public static final String DYPLOM_COLUMN = "dyplom";
    public static final String LIST_REPRESENT_ID = "listRepresentId";
    public static final String PRACTICE_COLUMN = "practiceData";


    public static final String STUDENT_SELECTED_LIST = "studentSelectList";

    public StudentSelectedDSHandlerPractice(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        List<String> errorList = new ArrayList<String>();
        List<DataWrapper> selectedList = context.get(STUDENT_SELECTED_LIST);

        Collections.sort(selectedList, new EntityComparator(input.getEntityOrder()));


        DQLSelectBuilder representDQL = new DQLSelectBuilder();
        representDQL.fromEntity(ListRepresent.class, "sp")
                .addColumn("sp")
                .where(DQLExpressions.eq(DQLExpressions.property(ListRepresent.id().fromAlias("sp")),
                                         DQLExpressions.commonValue(context.get(LIST_REPRESENT_ID))));
        ListRepresent represent = representDQL.createStatement(getSession()).uniqueResult();


        for (DataWrapper student : selectedList) {
            //    if (context.get("grantViewField") != null && context.get("educationYearField") != null && context.get("dateBeginingPaymentField") != null) {
            Student studentData = (Student) student.getProperty("student");
            StudentPracticeData stuPracticeData = (StudentPracticeData) student.getProperty("practiceData");

            if (stuPracticeData != null) {
                student.setProperty("practiceData", stuPracticeData);
                continue;
            }


            DQLSelectBuilder tmpPracticeData = new DQLSelectBuilder();
            tmpPracticeData.fromEntity(StudentPracticeData.class, "sp")
                    .addColumn("sp")
                    .where(DQLExpressions.eq(DQLExpressions.property(StudentGrantEntity.listRepresent().id().fromAlias("sp")),
                                             DQLExpressions.commonValue(context.get(LIST_REPRESENT_ID))))
                    .where(DQLExpressions.eq(DQLExpressions.property(StudentGrantEntity.student().fromAlias("sp")),
                                             DQLExpressions.value(studentData)))
            ;

            StudentPracticeData tmpStudentPracticeData = tmpPracticeData.createStatement(getSession()).uniqueResult();

            if (tmpStudentPracticeData != null) {
                student.setProperty("practiceData", tmpStudentPracticeData);
                continue;
            }

            StudentPracticeData studentPracticeData = new StudentPracticeData();
            studentPracticeData.setStudent(studentData);
            studentPracticeData.setListRepresent(represent);


            student.setProperty("practiceData", studentPracticeData);              //    }
        }


        //return ListOutputBuilder.get(input, resultList).pageable(true).build();

        return ListOutputBuilder.get(input, selectedList).pageable(true).build();
    }
}
