package ru.tandemservice.movestudentrmc.component.settings.checkOnOrderToGrantView.CheckOnOrderToGrantViewPub;

import org.tandemframework.core.component.State;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudentrmc.entity.catalog.CheckOnOrder;

@State(keys = "firstId", bindings = "check.id")
public class Model {

    public static final Long SHOW_USE_CODE = 0L;
    public static final Long SHOW_NON_USE_CODE = 1L;

    private ISelectModel usedModel;
    private CheckOnOrder check = new CheckOnOrder();
    private DynamicListDataSource<Wrapper> dataSource;
    private IDataSettings settings;

    public ISelectModel getUsedModel() {
        return usedModel;
    }

    public void setUsedModel(ISelectModel usedModel) {
        this.usedModel = usedModel;
    }

    public CheckOnOrder getCheck() {
        return check;
    }

    public void setCheck(CheckOnOrder check) {
        this.check = check;
    }

    public DynamicListDataSource<Wrapper> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<Wrapper> dataSource) {
        this.dataSource = dataSource;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

}
