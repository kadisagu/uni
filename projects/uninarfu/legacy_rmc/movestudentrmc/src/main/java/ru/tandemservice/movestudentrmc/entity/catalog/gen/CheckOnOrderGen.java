package ru.tandemservice.movestudentrmc.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.CheckOnOrder;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проверки по приказам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class CheckOnOrderGen extends EntityBase
 implements INaturalIdentifiable<CheckOnOrderGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.catalog.CheckOnOrder";
    public static final String ENTITY_NAME = "checkOnOrder";
    public static final int VERSION_HASH = 315168538;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_USED = "used";
    public static final String P_BEAN = "bean";
    public static final String P_TITLE = "title";
    public static final String P_CONFIG_RELATION = "configRelation";
    public static final String P_GRANT_CONFIG_RELATION = "grantConfigRelation";

    private String _code;     // Системный код
    private boolean _used = false;     // Использовать
    private String _bean;     // Бин проверки
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Использовать. Свойство не может быть null.
     */
    @NotNull
    public boolean isUsed()
    {
        return _used;
    }

    /**
     * @param used Использовать. Свойство не может быть null.
     */
    public void setUsed(boolean used)
    {
        dirty(_used, used);
        _used = used;
    }

    /**
     * @return Бин проверки.
     */
    @Length(max=255)
    public String getBean()
    {
        return _bean;
    }

    /**
     * @param bean Бин проверки.
     */
    public void setBean(String bean)
    {
        dirty(_bean, bean);
        _bean = bean;
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof CheckOnOrderGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((CheckOnOrder)another).getCode());
            }
            setUsed(((CheckOnOrder)another).isUsed());
            setBean(((CheckOnOrder)another).getBean());
            setTitle(((CheckOnOrder)another).getTitle());
        }
    }

    public INaturalId<CheckOnOrderGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<CheckOnOrderGen>
    {
        private static final String PROXY_NAME = "CheckOnOrderNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof CheckOnOrderGen.NaturalId) ) return false;

            CheckOnOrderGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends CheckOnOrderGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) CheckOnOrder.class;
        }

        public T newInstance()
        {
            return (T) new CheckOnOrder();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "used":
                    return obj.isUsed();
                case "bean":
                    return obj.getBean();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "used":
                    obj.setUsed((Boolean) value);
                    return;
                case "bean":
                    obj.setBean((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "used":
                        return true;
                case "bean":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "used":
                    return true;
                case "bean":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "used":
                    return Boolean.class;
                case "bean":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<CheckOnOrder> _dslPath = new Path<CheckOnOrder>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "CheckOnOrder");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.CheckOnOrder#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Использовать. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.CheckOnOrder#isUsed()
     */
    public static PropertyPath<Boolean> used()
    {
        return _dslPath.used();
    }

    /**
     * @return Бин проверки.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.CheckOnOrder#getBean()
     */
    public static PropertyPath<String> bean()
    {
        return _dslPath.bean();
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.CheckOnOrder#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.catalog.CheckOnOrder#getConfigRelation()
     */
    public static SupportedPropertyPath<Boolean> configRelation()
    {
        return _dslPath.configRelation();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.catalog.CheckOnOrder#getGrantConfigRelation()
     */
    public static SupportedPropertyPath<Boolean> grantConfigRelation()
    {
        return _dslPath.grantConfigRelation();
    }

    public static class Path<E extends CheckOnOrder> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<Boolean> _used;
        private PropertyPath<String> _bean;
        private PropertyPath<String> _title;
        private SupportedPropertyPath<Boolean> _configRelation;
        private SupportedPropertyPath<Boolean> _grantConfigRelation;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.CheckOnOrder#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(CheckOnOrderGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Использовать. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.CheckOnOrder#isUsed()
     */
        public PropertyPath<Boolean> used()
        {
            if(_used == null )
                _used = new PropertyPath<Boolean>(CheckOnOrderGen.P_USED, this);
            return _used;
        }

    /**
     * @return Бин проверки.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.CheckOnOrder#getBean()
     */
        public PropertyPath<String> bean()
        {
            if(_bean == null )
                _bean = new PropertyPath<String>(CheckOnOrderGen.P_BEAN, this);
            return _bean;
        }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.CheckOnOrder#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(CheckOnOrderGen.P_TITLE, this);
            return _title;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.catalog.CheckOnOrder#getConfigRelation()
     */
        public SupportedPropertyPath<Boolean> configRelation()
        {
            if(_configRelation == null )
                _configRelation = new SupportedPropertyPath<Boolean>(CheckOnOrderGen.P_CONFIG_RELATION, this);
            return _configRelation;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.catalog.CheckOnOrder#getGrantConfigRelation()
     */
        public SupportedPropertyPath<Boolean> grantConfigRelation()
        {
            if(_grantConfigRelation == null )
                _grantConfigRelation = new SupportedPropertyPath<Boolean>(CheckOnOrderGen.P_GRANT_CONFIG_RELATION, this);
            return _grantConfigRelation;
        }

        public Class getEntityClass()
        {
            return CheckOnOrder.class;
        }

        public String getEntityName()
        {
            return "checkOnOrder";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract Boolean getConfigRelation();

    public abstract Boolean getGrantConfigRelation();
}
