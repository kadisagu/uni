package ru.tandemservice.movestudentrmc.component.student.GrantAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntity;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import ru.tandemservice.movestudentrmc.entity.catalog.StuGrantStatus;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.List;

@State({
        @Bind(key = "studentId", binding = "student.id"),
        @Bind(key = "eduYearId", binding = "eduYear.id"),
        @Bind(key = "month", binding = "month"),
        @Bind(key = "studentGrantID", binding = "studentGrant.id")
})

public class Model {
    private Student student = new Student();
    private EducationYear eduYear = new EducationYear();
    private int month;
    private StudentGrantEntity studentGrant = new StudentGrantEntity();

    //private List<GrantEntity> grantEntityModel;
    private List<StuGrantStatus> statusModel;
    private List<GrantView> grantViewModel;

    private boolean updateMode;

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public EducationYear getEduYear() {
        return eduYear;
    }

    public void setEduYear(EducationYear eduYear) {
        this.eduYear = eduYear;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public StudentGrantEntity getStudentGrant() {
        return studentGrant;
    }

    public void setStudentGrant(StudentGrantEntity studentGrant) {
        this.studentGrant = studentGrant;
    }

    public void setStatusModel(List<StuGrantStatus> statusModel) {
        this.statusModel = statusModel;
    }

    public List<StuGrantStatus> getStatusModel() {
        return statusModel;
    }

    public void setUpdateMode(boolean updateMode) {
        this.updateMode = updateMode;
    }

    public boolean isUpdateMode() {
        return updateMode;
    }

    public void setGrantViewModel(List<GrantView> grantViewModel) {
        this.grantViewModel = grantViewModel;
    }

    public List<GrantView> getGrantViewModel() {
        return grantViewModel;
    }


}
