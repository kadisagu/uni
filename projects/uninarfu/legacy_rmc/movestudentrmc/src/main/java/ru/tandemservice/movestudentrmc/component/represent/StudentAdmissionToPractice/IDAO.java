package ru.tandemservice.movestudentrmc.component.represent.StudentAdmissionToPractice;


import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {
    public abstract void update(Model model);

    public void addNewPracticeBase(Model model, String filter, Long id);
}
