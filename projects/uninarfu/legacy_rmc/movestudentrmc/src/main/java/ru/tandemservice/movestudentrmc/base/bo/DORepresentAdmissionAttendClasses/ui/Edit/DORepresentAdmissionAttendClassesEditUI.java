package ru.tandemservice.movestudentrmc.base.bo.DORepresentAdmissionAttendClasses.ui.Edit;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.DevelopGridTermDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentEditUI;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.RepresentAdmissionAttendClasses;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;

import java.io.IOException;
import java.util.List;

public class DORepresentAdmissionAttendClassesEditUI extends AbstractDORepresentEditUI
{

    private RepresentAdmissionAttendClasses represent;

    @Override
    protected void FetchStudents(Representation doc) {
        _studentDocList.clear();
        _studentList.clear();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocRepresentStudentBase.class, "stud").addColumn("stud");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(DocRepresentStudentBase.representation().fromAlias("stud")),
                DQLExpressions.value(doc.getId())));

        List<DocRepresentStudentBase> studentList = builder.createStatement(getSupport().getSession()).list();

        for (DocRepresentStudentBase student : studentList) {

            _studentDocList.add(student);
            _studentList.add(student.getStudent());
        }
        _student = _studentList.get(0);

    }

    @Override
    protected void initDocument() {
        super.initDocument();
        represent = (RepresentAdmissionAttendClasses) _representObj;
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        super.onBeforeDataSourceFetch(dataSource);
        if ("developGridTermDS".equals(dataSource.getName())) {
            if (IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionRelation(_student.getId()) != null) {
                //если у студента есть УП, то получаем его и формируем список семестров в зависимости от курса и УП
                EppEduPlan eduPlan = IEppEduPlanDAO.instance.get()
                        .getActiveStudentEduPlanVersionRelation(_student.getId())
                        .getEduPlanVersion().getEduPlan();
                dataSource.put(DevelopGridTermDSHandler.COURSE, represent.getCourse());
                dataSource.put(DevelopGridTermDSHandler.DEVELOP_GRID, eduPlan.getDevelopGrid());
            }
        }
    }

    @Override
    public void onClickSave() throws IOException {
        //проверяем есть ли у текущего семестра РУП
        int termNumber = represent.getTerm().getTermNumber() - 1; //номер текущего семестра
        if (termNumber > 0) {
            Term term = UniDaoFacade.getCoreDao().get(Term.class, Term.intValue(), termNumber); //по номеру получаем семестр
            //проверяем есть ли у студента в текущем семестре РУП
            DQLSelectBuilder builder = new DQLSelectBuilder()
                    .fromEntity(EppStudent2WorkPlan.class, "swp")
                    .addColumn("swp")
                    .where(DQLExpressions.eq(DQLExpressions.property(EppStudent2WorkPlan.studentEduPlanVersion().student().fromAlias("swp")), DQLExpressions.value(_student)))
                    .where(DQLExpressions.eq(DQLExpressions.property(EppStudent2WorkPlan.cachedGridTerm().term().fromAlias("swp")), DQLExpressions.value(term)));

            List<EppStudent2WorkPlan> workPlanList = DataAccessServices.dao().getList(builder);

            if (workPlanList.isEmpty()) {
                //если РУПа нет, выводим сообщение
                throw new ApplicationException("Для студента не определен рабочий учебный план. Чтобы сформировать представление необходимо определить РУП для текущего студента.");
            }
        }
        //сохраняем представление
        super.onClickSave();
    }

    public Student getUndergrad() {
        return _student;
    }

    public Course getCourse() {
        return represent.getCourse();
    }

    public void setCourse(Course course) {
        represent.setCourse(course);
    }

    public DevelopGridTerm getTerm() {
        return represent.getTerm();
    }

    public void setTerm(DevelopGridTerm term) {
        represent.setTerm(term);
    }

}
