package ru.tandemservice.movestudentrmc.docord;

import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Map;

public interface IListPrintDoc {

    /**
     * Возвращает инфу о номере параграфа и номере студента в параграфе
     */
    public abstract Map<Student, OrderParagraphInfo> getParagInfomap();
}
