package ru.tandemservice.movestudentrmc.component.settings.RepresentationBasementToDocument.RepresentationBasementToDocumentSettingsAdd;

import ru.tandemservice.movestudentrmc.entity.RelRepresentationBasementDocument;
import ru.tandemservice.movestudentrmc.entity.catalog.Document;
import ru.tandemservice.uni.component.settings.abstractRelation.AbstractRelationAdd.AbstractRelationAddDAO;

import java.util.List;

public class DAO extends AbstractRelationAddDAO<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        super.prepare(model);

        String hql = (new StringBuilder())
                .append("from ")
                .append(getRelationSecondEntityClass().getName())
                .append(" b where b not in (select ")
                .append("second")
                .append(" from ")
                .append(getRelationEntityClass().getName())
                .append(") order by b.title")
                .toString();

		/*		new MQBuilder(RelRepresentationBasementDocument.ENTITY_CLASS,"o")
        .add(MQExpression.isNotNull("o", "first"))
		.getResultList(getSession());   */

        List list = getSession().createQuery(hql).list();

        model.setSecondList(list);
    }

    @Override
    protected Class<RelRepresentationBasementDocument> getRelationEntityClass() {
        return RelRepresentationBasementDocument.class;
    }

    @Override
    protected Class<Document> getRelationSecondEntityClass() {
        return Document.class;
    }

    @Override
    protected String getRelationSecondEntityClassSortProperty() {
        return "title";
    }

}
