package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.entity.catalog.Document;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.List;

public class DocumentDSHandler extends DefaultComboDataSourceHandler {

    public DocumentDSHandler(String ownerId) {

        super(ownerId, Document.class, Document.P_TITLE);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {

        DQLSelectBuilder builder = new DQLSelectBuilder();

        builder.fromEntity(Student.class, "s");
        builder.where(DQLExpressions.isNull(DQLExpressions.property(Student.group().fromAlias("s"))));

        List<Object> list = builder.createStatement(getSession()).list();
        int a;
        a = 2;
        /*List<String> markCodes =
    			Arrays.asList(SessionMarkGradeValueCatalogItemCodes.UDOVLETVORITELNO,
    					SessionMarkGradeValueCatalogItemCodes.HOROSHO,
    					SessionMarkGradeValueCatalogItemCodes.OTLICHNO,
    					SessionMarkGradeValueCatalogItemCodes.ZACHTENO);
    
    	String code = SessionMarkGradeValueCatalogItemCodes.HOROSHO;
    	
    	//Для сокращения поиска находим студентов у которых вообще есть такая оценка
    	DQLSelectBuilder markBuilder = new DQLSelectBuilder();

		markBuilder.fromEntity(SessionMark.class, "m");
		markBuilder.where(DQLExpressions.and(
				DQLExpressions.eq(DQLExpressions.property(SessionMark.cachedMarkValue().code().fromAlias("m")),
						DQLExpressions.value(code)),
						DQLExpressions.isNull(SessionMark.slot().student().removalDate().fromAlias("m"))));

		markBuilder.addColumn(DQLExpressions.property(SessionMark.slot().student().slot().student().student().id().fromAlias("m")));
		markBuilder.group(DQLExpressions.property(SessionMark.slot().student().slot().student().student().id().fromAlias("m")));

		List<Long> allStudents = markBuilder.createStatement(DataAccessServices.dao().getComponentSession()).list();

		List<Long> resultStudents = new ArrayList<Long>();			

		
		for(Long studentId : allStudents){
			
			List<EppStudentWpeCAction> slots = DataAccessServices.dao().getList(EppStudentWpeCAction.class,EppStudentWpeCAction.slot().student().student().id(),studentId);

			boolean addStudent = true;

			List<SessionSlotRegularMark> marks = new DQLSelectBuilder().fromEntity(SessionSlotRegularMark.class, "s")
					.where(DQLExpressions.and(
							DQLExpressions.in(DQLExpressions.property(SessionSlotRegularMark.slot().student().fromAlias("s")), DQLExpressions.value(slots)),
							DQLExpressions.eq(DQLExpressions.property(SessionSlotRegularMark.slot().student().slot().student().student().fromAlias("s")), DQLExpressions.value(studentId)),
							DQLExpressions.eq(DQLExpressions.property(SessionSlotRegularMark.slot().inSession().fromAlias("s")), DQLExpressions.value(true))))
							.createStatement(DataAccessServices.dao().getComponentSession()).list();
			if(slots.size() != marks.size())
				continue;
			
			for(SessionSlotRegularMark mark : marks){
				//Если находим оценку отличную от запрашиваемой - пропускаем студента
				if(!mark.getCachedMarkValue().getCode().equals(code) && !mark.getCachedMarkValue().getCode().equals(SessionMarkGradeValueCatalogItemCodes.ZACHTENO)){
					addStudent = false;
					break;
				}
			}
			if(addStudent)
				resultStudents.add(studentId);
		}


		List<Student> list = DataAccessServices.dao().getList(Student.class,resultStudents);
		list.get(3).getFio();
		
		int q;
		q=34;*/
    	/*DQLSelectBuilder students = new DQLSelectBuilder();

		students.fromEntity(Student.class, "s")
		.where(DQLExpressions.eq(property(Student.archival().fromAlias("s")),value(false)))
		.where(DQLExpressions.eq(property(Student.status().active().fromAlias("s")),value(true)));

*/
        //List<Object> list = markBuilder.createStatement(getSession()).list();

	/*	DQLSelectBuilder students = new DQLSelectBuilder();

		
		students.fromEntity(EppStudentWpeCAction.class, "s");
		students.joinPath(DQLJoinType.inner, EppStudentWpeCAction.slot().student().student().fromAlias("s"));
		
		students.addColumn(DQLExpressions.property(EppStudentWpeCAction.slot().student().student().id().fromAlias("s")));
		students.group(DQLExpressions.property(EppStudentWpeCAction.slot().student().student().fromAlias("s")));
		
		List<EppStudentWpeCAction> actionSlots = DataAccessServices.dao().getList(EppStudentWpeCAction.class,EppStudentWpeCAction.slot().student().student().id(),Long.valueOf("1404838040013109787"));


		DataAccessServices.dao().get(Student.class,Long.valueOf("1404838040013109787")).getFio();
		
			//1404836480296155675
		List<Long> fStuds = new ArrayList<Long>();
		

		
		for(Long student : students.createStatement(getSession()).<Long>list()) {
				Map sessionMapkMap = new HashMap();
				Map totalMapkMap = new HashMap();
				Map markCountMap = new HashMap();
				Map tutorsMap = new HashMap();

				
				
				DQLSelectBuilder slots = new DQLSelectBuilder()
				.fromEntity(EppStudentWpeCAction.class, "eppSlot")
				.where(DQLExpressions.eq(DQLExpressions.property(EppStudentWpeCAction.slot().student().student().id().fromAlias("eppSlot")), DQLExpressions.value(student)));
				List<Object> list = slots.createStatement(getSession()).list();
				
				DQLSelectBuilder markDql = new DQLSelectBuilder().fromEntity(SessionSlotRegularMark.class, "mark")
						.where(DQLExpressions.eq(DQLExpressions.property(SessionSlotRegularMark.slot().student().slot().student().student().id().fromAlias("mark")), DQLExpressions.value(student)))
						.order(DQLExpressions.property(SessionSlotRegularMark.id().fromAlias("mark")));
				
				List<SessionMark> marks = markDql.createStatement(getSession()).list();

				if(marks.size() == 0)
					continue;
				
				if(marks.size() != list.size())
					continue;
				
				fStuds.add(student);
				for (SessionMark mark :marks)
				{
					if ((mark.getSlot().getDocument() instanceof SessionStudentGradeBookDocument) && mark.isInSession())
					{
						sessionMapkMap.put(mark.getSlot().getStudent(), mark);
					}
				}



				for (ViewWrapper wrapper : wrappers)
				{
					wrapper.setViewProperty("sessionMark", sessionMapkMap.get(wrapper.getEntity()));
					wrapper.setViewProperty("totalMark", totalMapkMap.get(wrapper.getEntity()));
					wrapper.setViewProperty("tutors", tutorsMap.get(wrapper.getEntity()));

					MutableInt markCount = (MutableInt)markCountMap.get(wrapper.getEntity());
					wrapper.setViewProperty("markCount", null == markCount ? null : Integer.valueOf(markCount.intValue()));
				}
			}
*/
		
/*		BatchUtils.execute(students.createStatement(getSession()).<Student>list(), 256, new BatchUtils.Action<Student>(){

			@Override
			public void execute(Collection<Student> student) {
				Map sessionMapkMap = new HashMap();
				Map totalMapkMap = new HashMap();
				Map markCountMap = new HashMap();
				Map tutorsMap = new HashMap();

				
				DQLSelectBuilder slots = new DQLSelectBuilder()
				.fromEntity(EppStudentWpeCAction.class, "eppSlot")
				.addColumn("eppSlot")
				.where(DQLExpressions.eq(DQLExpressions.property(EppStudentWpeCAction.slot().student().student().fromAlias("eppSlot")), DQLExpressions.value(student)));
				
				
				DQLSelectBuilder markDql = new DQLSelectBuilder().fromEntity(SessionMark.class, "mark")
						.addColumn("mark")
						.fetchPath(DQLJoinType.inner, SessionMark.slot().fromAlias("mark"), "slot")
						.fetchPath(DQLJoinType.inner, SessionDocumentSlot.document().fromAlias("slot"), "document")
						.where(DQLExpressions.in(DQLExpressions.property(SessionMark.slot().student().fromAlias("mark")), slots.getQuery()))
						.order(DQLExpressions.property(SessionMark.id().fromAlias("mark")));

				for (SessionMark mark : markDql.createStatement(getSession()).<SessionMark>list())
				{
					if ((mark.getSlot().getDocument() instanceof SessionStudentGradeBookDocument))
					{
						(mark.isInSession() ? sessionMapkMap : totalMapkMap).put(mark.getSlot().getStudent(), mark);
					}

					if ((mark instanceof SessionSlotRegularMark))
					{
						((MutableInt)SafeMap.safeGet(markCountMap, mark.getSlot().getStudent(), MutableInt.class)).increment();
					}

				}



				for (ViewWrapper wrapper : wrappers)
				{
					wrapper.setViewProperty("sessionMark", sessionMapkMap.get(wrapper.getEntity()));
					wrapper.setViewProperty("totalMark", totalMapkMap.get(wrapper.getEntity()));
					wrapper.setViewProperty("tutors", tutorsMap.get(wrapper.getEntity()));

					MutableInt markCount = (MutableInt)markCountMap.get(wrapper.getEntity());
					wrapper.setViewProperty("markCount", null == markCount ? null : Integer.valueOf(markCount.intValue()));
				}
			}

	});*/

        //List<Long> allStudents = markBuilder.createStatement(DataAccessServices.dao().getComponentSession()).list();


    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
    {
        ep.dqlBuilder.order("e." + Document.title());
    }

}
