package ru.tandemservice.movestudentrmc.dao;

import org.tandemframework.core.runtime.ApplicationRuntime;
import ru.tandemservice.movestudentrmc.docord.util.title.ISpecificPrintTitle;
import ru.tandemservice.movestudent.dao.IMoveStudentDao;

// Referenced classes of package ru.tandemservice.movestudent.dao:
//            IMoveStudentDao
public class MoveStudentDaoFacade {
    public MoveStudentDaoFacade() {
    }

    public static IMoveStudentDao getMoveStudentDao() {
        if (_moveStudentDao == null)
            _moveStudentDao = (IMoveStudentDao) ApplicationRuntime.getBean("moveStudentDao");
        return _moveStudentDao;
    }

    public static IDocumentDAO getDocumentDAO() {
        if (_documentDAO == null)
            _documentDAO = (IDocumentDAO) ApplicationRuntime.getBean("documentDAO");
        return _documentDAO;
    }

    public static IGrantDAO getGrantDAO() {
        if (_grantDAO == null)
            _grantDAO = (IGrantDAO) ApplicationRuntime.getBean("grantDAO");
        return _grantDAO;
    }

    public static IHistoryGrantDAO getHistoryDAO() {
        if (_historyGrantDAO == null)
            _historyGrantDAO = (IHistoryGrantDAO) ApplicationRuntime.getBean("historyGrantDAO");
        return _historyGrantDAO;
    }

    public static ICustomStateDAO getCustomStateDAO() {
        if (_customStateDAO == null)
            _customStateDAO = (ICustomStateDAO) ApplicationRuntime.getBean("customStateDAO");
        return _customStateDAO;
    }

    public static IOrientationDAO getOrientationDAO() {
        if (_orientationDAO == null)
            _orientationDAO = (IOrientationDAO) ApplicationRuntime.getBean("orientationDAO");
        return _orientationDAO;
    }

    public static ISpecificPrintTitle getSpecificPrintTitle() {
        if (_specificPrintTitle == null)
            _specificPrintTitle = (ISpecificPrintTitle) ApplicationRuntime.getBean("specificPrintTitle");
        return _specificPrintTitle;
    }

    private static ISpecificPrintTitle _specificPrintTitle;
    private static IMoveStudentDao _moveStudentDao;
    private static IDocumentDAO _documentDAO;
    private static IGrantDAO _grantDAO;
    private static ICustomStateDAO _customStateDAO;
    private static IHistoryGrantDAO _historyGrantDAO;
    private static IOrientationDAO _orientationDAO;
}
