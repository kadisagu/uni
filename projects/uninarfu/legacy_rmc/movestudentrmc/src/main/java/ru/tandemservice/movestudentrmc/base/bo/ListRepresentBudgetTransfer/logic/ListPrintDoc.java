package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBudgetTransfer.logic;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.collections.map.MultiKeyMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import ru.tandemservice.movestudentrmc.docord.IListPrintDoc;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintListRep;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListRepresentBudgetTransfer;
import ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;

import java.util.*;
import java.util.Map.Entry;

public class ListPrintDoc implements IListPrintDoc
{

    private Map<Student, OrderParagraphInfo> hashMap = new HashMap<>();

    public RtfDocument creationPrintDocOrder(List<? extends ListRepresent> listOfRepresents)
    {

        //Получим список студентов из представлений
        DQLSelectBuilder builder1 = new DQLSelectBuilder()
                .fromEntity(RelListRepresentStudents.class, "rel")
                .where(DQLExpressions.in(DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("rel")), listOfRepresents))

                .joinEntity("rel", DQLJoinType.inner, ListRepresentBudgetTransfer.class, "lgrnt",
                            DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudents.representation().id().fromAlias("rel")),
                                DQLExpressions.property(ListRepresent.id().fromAlias("lgrnt"))))

                .column(DQLExpressions.property(RelListRepresentStudents.student().fromAlias("rel")))
                .column(DQLExpressions.property(RelListRepresentStudents.student().compensationType().id().fromAlias("rel")))
                .column(DQLExpressions.property(RelListRepresentStudents.student().educationOrgUnit().developForm().id().fromAlias("rel")))
                .column(DQLExpressions.property(RelListRepresentStudents.student().educationOrgUnit().formativeOrgUnit().id().fromAlias("rel")))

                .column(DQLExpressions.property("lgrnt"))

                .order(DQLExpressions.property(RelListRepresentStudents.student().educationOrgUnit().formativeOrgUnit().title().fromAlias("rel")))
                .order(DQLExpressions.property(ListRepresentBudgetTransfer.grantView().title().fromAlias("lgrnt")))
                .order(DQLExpressions.property(RelListRepresentStudents.student().educationOrgUnit().developForm().shortTitle().fromAlias("rel")))
                .order(DQLExpressions.property(RelListRepresentStudents.student().compensationType().shortTitle().fromAlias("rel")))

                .order(DQLExpressions.property(RelListRepresentStudents.student().course().intValue().fromAlias("rel")))
                .order(DQLExpressions.property(RelListRepresentStudents.student().person().identityCard().lastName().fromAlias("rel")));

        List<Object[]> students = builder1.createStatement(DataAccessServices.dao().getComponentSession()).list();


        //Параграф для шаблона
        UniecScriptItem scriptTemplate = new UniecScriptItem();
        scriptTemplate.setTemplatePath("movestudentrmc/templates/listRepresentaion/ListRepresentBudgetTransfer.rtf");
        RtfDocument template = new RtfReader().read(scriptTemplate.getTemplate());

        //Приложение для шаблона
        UniecScriptItem addonTemplate = new UniecScriptItem();
        addonTemplate.setTemplatePath("movestudentrmc/templates/listRepresentaion/addon_ListRepresentBudgetTransfer.rtf");
        RtfDocument tableAddon = new RtfReader().read(addonTemplate.getTemplate());


        //Параграф для шаблона
        UniecScriptItem paragTemplate = new UniecScriptItem();
        paragTemplate.setTemplatePath("movestudentrmc/templates/listRepresentaion/parag_ListRepresentBudgetTransfer.rtf");
        RtfDocument paragAddon = new RtfReader().read(paragTemplate.getTemplate());

        IRtfElement parag = UniRtfUtil.findElement(template.getElementList(), "PARAG");

        IRtfElement table = UniRtfUtil.findElement(template.getElementList(), "ADDON");
        template.getElementList().add(template.getElementList().indexOf(table), RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));

        RtfTableModifier tableModifier = new RtfTableModifier();

        List<String[]> tableData = new ArrayList<>();


        MultiKeyMap keyMap = new MultiKeyMap();

        //Сгруппируем данные
        for (Object[] data : students)
        {
            MultiKey key = new MultiKey(new Object[]{data[1], data[2], data[3], data[4]});

            if (keyMap.containsKey(key))
            {
                ((List) keyMap.get(key)).add(data[0]);
            } else
            {
                List<Object> arr = new ArrayList<>();
                arr.add(data[0]);
                keyMap.put(key, arr);
            }
        }


        int acNumb = 1;
        int orgUnits = 0;
        int j = 1;
        Long lastOrgUnit = null;

        for (Object value : keyMap.entrySet())
        {
            Entry<MultiKey, List> set = (Entry<MultiKey, List>) value;
            List<Student> v = (List<Student>) set.getValue();
            RtfInjectModifier injectModifier = new RtfInjectModifier();
            Student firstStudent = v.get(0);

            Long currentUnit = (Long) set.getKey().getKey(2);

            ListRepresentBudgetTransfer listRepresent = (ListRepresentBudgetTransfer) set.getKey().getKey(3);

            tableModifier = new RtfTableModifier();
            tableData.clear();


            if (!currentUnit.equals(lastOrgUnit))
            {
                template.getElementList().add(template.getElementList().indexOf(parag), paragAddon.getClone().getElementList().get(0));
                orgUnits++;
                j = 1;
            }
            template.getElementList().add(template.getElementList().indexOf(parag), paragAddon.getClone().getElementList().get(1));
            template.getElementList().addAll(template.getElementList().indexOf(table), tableAddon.getClone().getElementList());


            int i = 1;
            for (Student student : v)
            {
                if (student.getEducationOrgUnit().getDevelopForm().getCode().equals(DevelopFormCodes.FULL_TIME_FORM))
                {
                    tableData.add(fillTableRow(i++, student, listRepresent));

                    RtfString grantText = new RtfString();
                    grantText.append("с назначением государственной академической стипендии c ");
                    UtilPrintSupport.getDateFormatterWithMonthStringAndYear(listRepresent.getDateBeginingPayment(), grantText);
                    grantText.append(" выплаты стипендии по ");
                    UtilPrintSupport.getDateFormatterWithMonthStringAndYear(listRepresent.getDateEndOfPayment(), grantText);
                    grantText.append(" ")
                            .append(listRepresent.getGrantView().getGenitive() == null ? listRepresent.getGrantView().getTitle() : listRepresent.getGrantView().getGenitive());

                    injectModifier.put("grantText", grantText);

                } else
                {
                    tableData.add(fillTableRow(i++, student, null));
                    injectModifier.put("grantText", "");
                }
                hashMap.put(student, new OrderParagraphInfo(orgUnits, j));
            }

            if (v.size() <= 1)
            {
                injectModifier.put("nextEnding", "следующего");
                injectModifier.put("studentEnding", "студента");
            } else
            {
                injectModifier.put("nextEnding", "слудующих");
                injectModifier.put("studentEnding", "студентов");
            }

            injectModifier
                    .put("formativeOrgUnit", firstStudent.getEducationOrgUnit().getFormativeOrgUnit().getDativeCaseTitle())
                    .put("eduLevel", UtilPrintSupport.getHighLevelSchoolTypeString(firstStudent.getEducationOrgUnit().getEducationLevelHighSchool()))
                    .put("course", firstStudent.getCourse().getTitle())
                    .put("i", String.valueOf(orgUnits))
                    .put("j", String.valueOf(j++))
                    .put("develop_form", UtilPrintSupport.getDevelopFormGen(firstStudent.getEducationOrgUnit().getDevelopForm()))
                            //.put("dateBeginingPayment", DateFormattingUtil.MONTH_STRING_DATE_FORMAT.format(listRepresent.getDateBeginingPayment()))
                            //.put("dateEndPayment", DateFormattingUtil.MONTH_STRING_DATE_FORMAT.format(listRepresent.getDateEndOfPayment()))

                            //.put("ac_num", String.valueOf(acNumb++))
                    .put("representTitle", listRepresent.getRepresentationType().getTitle())
                    .put("representBasement", UtilPrintSupport.getListRepresentBasicTitle(listRepresent))
                    .put("represent_CreateDate", UtilPrintSupport.getDateFormatterWithMonthString(listRepresent.getCreateDate()))
                    .put("beginTransfer", UtilPrintSupport.getDateFormatterWithMonthString(listRepresent.getDateBeginingTransfer()))
                    .put("representReason", listRepresent.getRepresentationReason().getTitle())

            ;

            UtilPrintListRep.injectModifierRepresent(injectModifier, listRepresent);

            tableModifier.put("T", tableData.toArray(new String[tableData.size()][]));
            injectModifier.modify(template);
            tableModifier.modify(template);

            lastOrgUnit = (Long) set.getKey().getKey(2);

        }


		/*		RtfInjectModifier allModifier = new RtfInjectModifier();
        allModifier
		.put("dateBeginingPayment", DateFormattingUtil.MONTH_STRING_DATE_FORMAT.format(listRepresent.getDateBeginingPayment()))
		.put("dateEndPayment", DateFormattingUtil.MONTH_STRING_DATE_FORMAT.format(listRepresent.getDateEndOfPayment()))
		.put("grant_g", listRepresent.getGrantView().getGenitive() == null ? listRepresent.getGrantView().getTitle() : listRepresent.getGrantView().getGenitive())
		//.put("ac_num", String.valueOf(acNumb++))
		.put("representTitle", listRepresent.getRepresentationType().getTitle())
		.put("representBasement", listRepresent.getRepresentationReason().getTitle())
		.put("represent_CreateDate", DateFormattingUtil.MONTH_STRING_DATE_FORMAT.format(listRepresent.getCreateDate()))
		.put("beginTransfer", DateFormattingUtil.MONTH_STRING_DATE_FORMAT.format(listRepresent.getDateBeginingTransfer()))
		.put("representReason", listRepresent.getRepresentationReason().getTitle())
		;

		allModifier.modify(template);*/
        tableModifier.modify(template);

        SharedRtfUtil.removeParagraphsWithTagsRecursive(template, Arrays.asList("PARAG", "ADDON"), false, false);


        return template;
    }


    private String[] fillTableRow(int i, Student student, ListRepresentBudgetTransfer listRepresent)
    {
        List<String> result = new ArrayList<>();

        result.add(String.valueOf(i + "."));
        result.add(student.getPerson().getIdentityCard().getFullFio());
        result.add(listRepresent != null ? listRepresent.getGrantView().getTitle() : "");

        return result.toArray(new String[result.size()]);
    }

    @Override
    public Map<Student, OrderParagraphInfo> getParagInfomap()
    {
        // TODO Auto-generated method stub
        return hashMap;
    }
}
