package ru.tandemservice.movestudentrmc.base.bo.ListOrder;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.movestudentrmc.base.bo.ListOrder.logic.IListOrderManagerModifyDAO;
import ru.tandemservice.movestudentrmc.base.bo.ListOrder.logic.ListOrderManagerModifyDAO;

@Configuration
public class ListOrderManager extends BusinessObjectManager {

    public static ListOrderManager instance()
    {
        return instance(ListOrderManager.class);
    }

    @Bean
    public IListOrderManagerModifyDAO modifyDao()
    {
        return new ListOrderManagerModifyDAO();
    }
}
