package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.RepresentWeekendProlong;
import ru.tandemservice.movestudentrmc.entity.Representation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * О продлении академического отпуска
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RepresentWeekendProlongGen extends Representation
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.RepresentWeekendProlong";
    public static final String ENTITY_NAME = "representWeekendProlong";
    public static final int VERSION_HASH = -612856522;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof RepresentWeekendProlongGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RepresentWeekendProlongGen> extends Representation.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RepresentWeekendProlong.class;
        }

        public T newInstance()
        {
            return (T) new RepresentWeekendProlong();
        }
    }
    private static final Path<RepresentWeekendProlong> _dslPath = new Path<RepresentWeekendProlong>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RepresentWeekendProlong");
    }
            

    public static class Path<E extends RepresentWeekendProlong> extends Representation.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return RepresentWeekendProlong.class;
        }

        public String getEntityName()
        {
            return "representWeekendProlong";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
