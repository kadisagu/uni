package ru.tandemservice.movestudentrmc.component.catalog.grantType.GrantTypePub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantType;

public interface IDAO extends IDefaultCatalogPubDAO<GrantType, Model> {

    void updateAcadem(Model model, Long id);

    void updateSocial(Model model, Long id);
}
