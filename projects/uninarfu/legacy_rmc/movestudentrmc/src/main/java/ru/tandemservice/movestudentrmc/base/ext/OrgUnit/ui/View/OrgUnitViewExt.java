package ru.tandemservice.movestudentrmc.base.ext.OrgUnit.ui.View;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitView;

@Configuration
public class OrgUnitViewExt extends BusinessComponentExtensionManager {

    @Autowired
    public OrgUnitView orgUnitView;

    @Bean
    public TabPanelExtension tabPanelExtension() {
        return tabPanelExtensionBuilder(this.orgUnitView.orgUnitTabPanelExtPoint())
                .addTab(componentTab("orgUnitGrantEntityListTab", "ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab")
                                .parameters("mvel:['orgUnitId':presenter.orgUnit.id]")
                                .permissionKey("rmc_viewGrantEntityList")
                ).create();
    }
}
