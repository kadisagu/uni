package ru.tandemservice.movestudentrmc.base.bo.DOOrder.logic;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.sec.entity.Principal;
import ru.tandemservice.movestudentrmc.entity.Principal2Visa;

import java.util.Date;

public class OrderVisaDAO extends CommonDAO implements IOrderVisaDAO {

    @Override
    public Principal2Visa getPrincipal2Visa(Principal principal) {

        Principal2Visa principal2Visa = get(Principal2Visa.class, Principal2Visa.principal(), principal);

        if (principal2Visa == null) {
            principal2Visa = new Principal2Visa();
            principal2Visa.setPrincipal(principal);
            principal2Visa.setEditDate(new Date());
        }

        return principal2Visa;

    }


    @Override
    public void updateOrderVisa(Principal2Visa principal2Visa) {

        
/*        Map<String, IDQLExpression> map = new HashMap<String, IDQLExpression>();
        map.put("signPost", DQLExpressions.value(principal2Visa.getSignPost()));
        map.put("signFio", DQLExpressions.value(principal2Visa.getSignFio()));
        
        map.put("postFio1", DQLExpressions.value(principal2Visa.getPostFio1()));
        map.put("postTitle1", DQLExpressions.value(principal2Visa.getPostTitle1()));
        
        map.put("postFio2", DQLExpressions.value(principal2Visa.getPostFio2()));
        map.put("postTitle2", DQLExpressions.value(principal2Visa.getPostTitle2()));
        
        map.put("postFio3", DQLExpressions.value(principal2Visa.getPostFio3()));
        map.put("postTitle3", DQLExpressions.value(principal2Visa.getPostTitle3()));
        
        map.put("postFio4", DQLExpressions.value(principal2Visa.getPostFio4()));
        map.put("postTitle4", DQLExpressions.value(principal2Visa.getPostTitle4()));
        
        map.put("postFio5", DQLExpressions.value(principal2Visa.getPostFio5()));
        map.put("postTitle5", DQLExpressions.value(principal2Visa.getPostTitle5()));*/

        saveOrUpdate(principal2Visa);
        principal2Visa.setEditDate(new Date());
    }

    @Override
    public Principal2Visa getPrincipal2Visa() {
        IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();
        Principal principal = (Principal) principalContext.getPrincipal();

        return getPrincipal2Visa(principal);
    }


}
