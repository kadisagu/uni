package ru.tandemservice.movestudentrmc.component.catalog.osspPgo.OsspPgoPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.movestudentrmc.entity.catalog.OsspPgo;

public class DAO extends DefaultCatalogPubDAO<OsspPgo, Model> implements IDAO {

    public void updatePrintable(Model model, Long id) {
        OsspPgo osspPgo = getNotNull(OsspPgo.class, id);
        if (osspPgo.isPrintable()) {
            osspPgo.setPrintable(false);
        }
        else {
            osspPgo.setPrintable(true);
        }
        save(osspPgo);
    }
}
