package ru.tandemservice.movestudentrmc.component.catalog.osspGrants.OsspGrantsPub;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.movestudentrmc.entity.catalog.OsspGrants;

public class Controller extends DefaultCatalogPubController<OsspGrants, Model, IDAO> {

    protected DynamicListDataSource<OsspGrants> createListDataSource(IBusinessComponent context) {
        Model model = getModel(context);
        DynamicListDataSource<OsspGrants> ds = new DynamicListDataSource<OsspGrants>(context, this);
        ds.addColumn(new SimpleColumn("Название", "title").setClickable(false).setOrderable(true), 2);
        ds.addColumn(new SimpleColumn("Номер", "number").setClickable(false).setOrderable(true), 3);
        ds.addColumn(new ToggleColumn("Печатать", Model.OsspGrantsWrapper.PRINTABLE).setListener("onClickTogglePrintable"), 4);
        ds.addColumn(new ToggleColumn("Вид", Model.OsspGrantsWrapper.REQ_CLARIFICATION).setListener("onClickToggleReqClarification"), 4);
        ds.addColumn(new ToggleColumn("Период", Model.OsspGrantsWrapper.REQ_PERIOD).setListener("onClickToggleReqPeriod"), 4);
        ds.addColumn(new ToggleColumn("Размер", Model.OsspGrantsWrapper.REQ_SIZE).setListener("onClickToggleReqSize"), 4);
        ds.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
        if (model.isUserCatalog())
            ds.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", ICatalogItem.CATALOG_ITEM_TITLE).setPermissionKey(model.getCatalogItemDelete()).setDisableHandler(model.getDisabledEntityHandler()));
        ds.setOrder("number", OrderDirection.asc);
        return ds;
    }

    public void onClickTogglePrintable(IBusinessComponent component)
    {
        getDao().updatePrintable(getModel(component), (Long) component.getListenerParameter());
    }

    public void onClickToggleReqClarification(IBusinessComponent component)
    {
        getDao().updateReqClarification(getModel(component), (Long) component.getListenerParameter());
    }

    public void onClickToggleReqPeriod(IBusinessComponent component)
    {
        getDao().updateReqPeriod(getModel(component), (Long) component.getListenerParameter());
    }

    public void onClickToggleReqSize(IBusinessComponent component)
    {
        getDao().updateReqSize(getModel(component), (Long) component.getListenerParameter());
    }

}
