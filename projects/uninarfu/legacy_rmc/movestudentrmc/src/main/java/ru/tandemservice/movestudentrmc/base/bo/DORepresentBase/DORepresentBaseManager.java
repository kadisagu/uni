package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentAdmissionAttendClasses.DORepresentAdmissionAttendClassesManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.DORepresentBaseModifyDAO;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.IDORepresentBaseModifyDAO;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentChangeName.DORepresentChangeNameManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentChangeQualificationTheme.DORepresentChangeQualificationThemeManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentCourseTransfer.DORepresentCourseTransferManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentDiplomAndExclude.DORepresentDiplomAndExcludeManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentEnrollmentTransfer.DORepresentEnrollmentTransferManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentExclude.DORepresentExcludeManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentExcludeOut.DORepresentExcludeOutManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentExtensionSession.DORepresentExtensionSessionManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentGrant.DORepresentGrantManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentGrantCancel.DORepresentGrantCancelManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentGrantCancelAndDestination.DORepresentGrantCancelAndDestinationManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentGrantResume.DORepresentGrantResumeManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentGrantSuspend.DORepresentGrantSuspendManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentMakeReprimand.DORepresentMakeReprimandManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentPersonWorkPlan.DORepresentPersonWorkPlanManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentRecertificationTraining.DORepresentRecertificationTrainingManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentStudentTicket.DORepresentStudentTicketManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentSupport.DORepresentSupportManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentTransfer.DORepresentTransferManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentTravel.DORepresentTravelManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentWeekend.DORepresentWeekendManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentWeekendOut.DORepresentWeekendOutManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentWeekendProlong.DORepresentWeekendProlongManager;
import ru.tandemservice.movestudentrmc.docord.IDOObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentationTypeCodes;

@Configuration
public class DORepresentBaseManager extends BusinessObjectManager {

    public static DORepresentBaseManager instance()
    {
        return instance(DORepresentBaseManager.class);
    }

    @Bean
    public IDORepresentBaseModifyDAO modifyDao()
    {
        return new DORepresentBaseModifyDAO();
    }


    @Bean
    public ItemListExtPoint<? extends IDOObjectByRepresentTypeManager> doObjectByRepresentTypeExtPoint()
    {
        return itemList(IDOObjectByRepresentTypeManager.class).
                add(RepresentationTypeCodes.EXCLUDE, DORepresentExcludeManager.instance()). //Об отчислении
                add(RepresentationTypeCodes.WEEKEND, DORepresentWeekendManager.instance()). //О предоставлении ао
                add(RepresentationTypeCodes.TRANSFER, DORepresentTransferManager.instance()). //О переводе
                add(RepresentationTypeCodes.EXCLUDE_OUT, DORepresentExcludeOutManager.instance()). //О восстановлении
                add(RepresentationTypeCodes.WEEKEND_OUT, DORepresentWeekendOutManager.instance()). //О выходе из ао
                add(RepresentationTypeCodes.WEEKEND_PROLONG, DORepresentWeekendProlongManager.instance()). //О продлении ао
                add(RepresentationTypeCodes.COURSE_TRANSFER, DORepresentCourseTransferManager.instance()). //О переводе на следующий курс
                add(RepresentationTypeCodes.DIPLOM_AND_EXCLUDE, DORepresentDiplomAndExcludeManager.instance()). //О выдаче диплома и отчислении
                add(RepresentationTypeCodes.CHANGE_NAME, DORepresentChangeNameManager.instance()). //О смене имени
                add(RepresentationTypeCodes.ENROLLMENT_TRANSFER, DORepresentEnrollmentTransferManager.instance()). //О зачислении в порядке перевода
                add(RepresentationTypeCodes.ADMISSION_ATTEND_CLASSES, DORepresentAdmissionAttendClassesManager.instance()). //О допуске к посещению занятий
                add(RepresentationTypeCodes.EXTENSION_SESSION, DORepresentExtensionSessionManager.instance()). //О продлении сессии
                add(RepresentationTypeCodes.PERSON_WORK_PLAN, DORepresentPersonWorkPlanManager.instance()). //О предоставлении ИУП
                add(RepresentationTypeCodes.RECERTIFICATION_TRAINING, DORepresentRecertificationTrainingManager.instance()). //О перезачете/ переаттестации учебных дисциплин
                add(RepresentationTypeCodes.GRANT_CANCEL_INDIVIDUAL, DORepresentGrantCancelManager.instance()). //Об отмене стипендии/выплаты
                add(RepresentationTypeCodes.GRANT_INDIVIDUAL, DORepresentGrantManager.instance()). //О назначении стипендии/выплаты
                add(RepresentationTypeCodes.SOCIAL_SUPPORT_INDIVIDUAL, DORepresentSupportManager.instance()). //"О назначении социальной поддержки в связи с тяжелым материальным положением
                add(RepresentationTypeCodes.GRANT_SUSPEND_INDIVIDUAL, DORepresentGrantSuspendManager.instance()). //О приостановлении стипендии/выплаты
                add(RepresentationTypeCodes.GRANT_RESUME_INDIVIDUAL, DORepresentGrantResumeManager.instance()). //О возобновлении стипендии/выплаты
                add(RepresentationTypeCodes.DUPLICATE_STUDENT_TICKET_OR_EXAMS_BOOK, DORepresentStudentTicketManager.instance()). //О выдаче дубликата зачётной книжки, студенческого билета
                add(RepresentationTypeCodes.MAKE_REPRIMAND, DORepresentMakeReprimandManager.instance()). //Об объявлении выговора
                add(RepresentationTypeCodes.TRAVEL_INDIVIDUAL, DORepresentTravelManager.instance()). //О направлении в поездку
                add(RepresentationTypeCodes.GRANT_CANCEL_AND_DESTINATION_INDIVIDUAL, DORepresentGrantCancelAndDestinationManager.instance()). //Об отмене и назначении стипендии/выплаты
                add(RepresentationTypeCodes.CHANGE_QUALIFICATION_THEME, DORepresentChangeQualificationThemeManager.instance()). //О внесении изменений в приказ о назначении ВКР
                create();
    }
}
