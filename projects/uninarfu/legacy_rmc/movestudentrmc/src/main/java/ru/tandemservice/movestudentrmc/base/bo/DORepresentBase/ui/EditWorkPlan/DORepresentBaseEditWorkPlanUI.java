package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.EditWorkPlan;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.*;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;

import java.util.ArrayList;
import java.util.List;

@State(
        @Bind(key = "studentListId", binding = "studentListId")
)
@Input({
        @Bind(key = "represent", binding = "represent", required = true)
})
public class DORepresentBaseEditWorkPlanUI extends UIPresenter {

    private List<Long> studentListId;
    private Student student;
    private ITransferableRepresent represent;

    private boolean filterPlan = true;
    private EppEduPlan eduPlan;
    private EppEduPlanVersion planVersion;
    private EppYearEducationProcess year;
    private DevelopGridTerm term;

    private EppStudent2EduPlanVersion oldEduRel;


    private boolean firstLoad = true;

    @Override
    public void onComponentPrepareRender() {
        super.onComponentPrepareRender();
        if (this.oldEduRel == null || this.represent.getWorkPlan() == null)
            return;

        //сохраняем РУПы для отката

        MQBuilder builder = new MQBuilder(EppStudent2WorkPlan.ENTITY_CLASS, "rel")
                .add(MQExpression.eq("rel", EppStudent2WorkPlan.studentEduPlanVersion(), this.oldEduRel))
                .add(MQExpression.isNull("rel", EppStudent2WorkPlan.removalDate()));
        List<EppStudent2WorkPlan> workplanList = builder.getResultList(_uiSupport.getSession());

        List<String> oldIds = new ArrayList<String>();
        for (EppStudent2WorkPlan relPlan : workplanList) {
            oldIds.add(relPlan.getId().toString());
        }

        if (oldIds.isEmpty())
            this.represent.setOldWorkPlanRelIds(null);
        else
            this.represent.setOldWorkPlanRelIds(StringUtils.join(oldIds, ","));
    }

    @Override
    public void onComponentRefresh() {
        if (firstLoad) {
            this.student = UniDaoFacade.getCoreDao().get(Student.class, studentListId.get(0));

            if (represent.getId() != null && represent.getWorkPlan() != null) {
                this.eduPlan = represent.getWorkPlan().getEduPlan();
                this.planVersion = represent.getWorkPlan().getEduPlanVersion();
                this.term = represent.getWorkPlan().getGridTerm();
                this.year = represent.getWorkPlan().getYear();
                if (represent.getOldEduPlanRelId() != null)
                    this.oldEduRel = UniDaoFacade.getCoreDao().get(represent.getOldEduPlanRelId());
            }
            else {
                this.oldEduRel = IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionRelation(studentListId.get(0));

                if (this.oldEduRel != null) {
                    this.represent.setOldEduPlanRelId(this.oldEduRel.getId());//сохраняем старую связь УП

                    this.eduPlan = this.oldEduRel.getEduPlanVersion().getEduPlan();
                    this.planVersion = this.oldEduRel.getEduPlanVersion();
                }
            }

            firstLoad = false;
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        super.onBeforeDataSourceFetch(dataSource);

        if (DORepresentBaseEditWorkPlan.EDUPLAN_DS.equals(dataSource.getName()))
            dataSource.put(EduPlanDSHandler.GROUP, this.filterPlan ? this.represent.getGroup() : null);
        else if (DORepresentBaseEditWorkPlan.EDUPLANVERSION_DS.equals(dataSource.getName()))
            dataSource.put(EduPlanVersionDSHandler.EDU_PLAN, this.eduPlan);
        else if (DORepresentBaseEditWorkPlan.EPP_YEAR_DS.equals(dataSource.getName()))
            dataSource.put(EppYearDSHandler.PLAN_VERSION, this.planVersion);
        else if (DORepresentBaseEditWorkPlan.EPP_WORK_PLAN_DS.equals(dataSource.getName()))
            dataSource
                    .put(EppWorkPlanDSHandler.PLAN_VERSION, this.planVersion)
                    .put(EppWorkPlanDSHandler.TERM, this.term)
                    .put(EppWorkPlanDSHandler.EPP_YEAR, this.year);
        else if (DORepresentBaseEditWorkPlan.DEVELOP_GRID_TERM_DS.equals(dataSource.getName()))
            dataSource
                    .put(DevelopGridTermDSHandler.COURSE, represent.getCourse())
                    .put(DevelopGridTermDSHandler.DEVELOP_GRID, this.eduPlan != null ? this.eduPlan.getDevelopGrid() : null);
    }

    //ui
    public boolean isShowBlock() {
        return true;//TODO !!!
    }

    //set && get
    public List<Long> getStudentListId() {
        return studentListId;
    }

    public void setStudentListId(List<Long> studentListId) {
        this.studentListId = studentListId;
    }

    public boolean isFilterPlan() {
        return filterPlan;
    }

    public void setFilterPlan(boolean filterPlan) {
        this.filterPlan = filterPlan;
    }

    public EppEduPlan getEduPlan() {
        return eduPlan;
    }

    public void setEduPlan(EppEduPlan eduPlan) {
        this.eduPlan = eduPlan;
    }

    public EppEduPlanVersion getPlanVersion() {
        return planVersion;
    }

    public void setPlanVersion(EppEduPlanVersion planVersion) {
        this.planVersion = planVersion;
    }

    public EppYearEducationProcess getYear() {
        return year;
    }

    public void setYear(EppYearEducationProcess year) {
        this.year = year;
    }

    public ITransferableRepresent getRepresent() {
        return represent;
    }

    public void setRepresent(ITransferableRepresent represent) {
        this.represent = represent;
    }

    public DevelopGridTerm getTerm() {
        return term;
    }

    public void setTerm(DevelopGridTerm term) {
        this.term = term;
    }

}
