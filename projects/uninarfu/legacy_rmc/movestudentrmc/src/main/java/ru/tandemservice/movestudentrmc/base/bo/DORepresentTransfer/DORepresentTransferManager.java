package ru.tandemservice.movestudentrmc.base.bo.DORepresentTransfer;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentTransfer.logic.DORepresentTransferDAO;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentTransfer.ui.Edit.DORepresentTransferEdit;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentTransfer.ui.View.DORepresentTransferView;
import ru.tandemservice.movestudentrmc.docord.IDOObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IDOObjectModifyDAO;

@Configuration
public class DORepresentTransferManager extends BusinessObjectManager implements IDOObjectByRepresentTypeManager
{

    public static DORepresentTransferManager instance() {
        return instance(DORepresentTransferManager.class);
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return DORepresentTransferEdit.class;
    }

    @Bean
    public IDOObjectModifyDAO getDOObjectModifyDAO() {
        return new DORepresentTransferDAO();
    }

    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return DORepresentTransferView.class;
    }

}
