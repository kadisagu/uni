package ru.tandemservice.movestudentrmc.base.bo.DORepresentChangeQualificationTheme.logic;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentDAO;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.movestudentrmc.util.RepresentationUtil;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport.getDateFormatterWithMonthString;

public class DORepresentChangeQualificationThemeDAO extends AbstractDORepresentDAO
{
    public final static Integer RUSSIA_CODE = 0;

    @Override
    public Representation createRepresentObject(RepresentationType type, List<Student> studentList) {

        RepresentChangeQualificationTheme represent = new RepresentChangeQualificationTheme();
        represent.setType(type);
        return represent;
    }

    @Override
    public boolean doCommit(Representation represent, ErrorCollector error) {
        if (!super.doCommit(represent, error))
            return false;
        RepresentChangeQualificationTheme representation = (RepresentChangeQualificationTheme) represent;
        DocRepresentStudentBase representStudentBase = IUniBaseDao.instance.get().get(DocRepresentStudentBase.class, DocRepresentStudentBase.representation().s(), represent);
        if (representStudentBase != null) {

            Student student = representStudentBase.getStudent();
            StudentVKRTheme studentTheme = RepresentationUtil.getStudentVkrThemeLong(student);
            studentTheme.setTheme(representation.getNewTheme());
            baseUpdate(studentTheme);
            //сохраняем в студента название темы
            student.setFinalQualifyingWorkTheme(studentTheme.getTheme());
            baseUpdate(student);
        }

//    	StudentCustomStateCI state = UniDaoFacade.getCoreDao().get(StudentCustomStateCI.class, StudentCustomStateCI.code(), StudentCustomStateCICodes.TRIP);
//
        // MoveStudentDaoFacade.getCustomStateDAO().commitStudentCustomState(state, representation.getStartDate(), representation.get(), representation);

        return true;
    }

    @Override
    public boolean doRollback(Representation represent, ErrorCollector error) {
        if (!super.doRollback(represent, error))
            return false;

        RepresentChangeQualificationTheme representation = (RepresentChangeQualificationTheme) represent;
        DocRepresentStudentBase representStudentBase = IUniBaseDao.instance.get().get(DocRepresentStudentBase.class, DocRepresentStudentBase.representation().s(), represent);
        if (representStudentBase != null) {
        	
        	Student student = representStudentBase.getStudent();
	    	StudentVKRTheme studentTheme = RepresentationUtil.getStudentVkrThemeLong(student);
	            
	        //----------------------------------------------------------------------------------------------------------------------
	        if (representation.getDocumentOrder() != null) {
	        		
            	DQLSelectBuilder dql = new DQLSelectBuilder()
            			.fromEntity(DocRepresentStudentBase.class, "docrep")
            			.joinEntity("docrep", DQLJoinType.inner, DocOrdRepresent.class, "docordrep", DQLExpressions.eq(DQLExpressions.property(DocOrdRepresent.representation().id().fromAlias("docordrep")), DQLExpressions.property(DocRepresentStudentBase.representation().id().fromAlias("docrep"))))
                   		.where(DQLExpressions.eq(DQLExpressions.property(DocRepresentStudentBase.student().fromAlias("docrep")), DQLExpressions.value(student)))
                   		.where(DQLExpressions.eq(DQLExpressions.property(DocOrdRepresent.order().fromAlias("docordrep")), DQLExpressions.value(representation.getDocumentOrder())))
                   		.column(DQLExpressions.property(DocOrdRepresent.representation().fromAlias("docordrep")))
            	;
                List<RepresentChangeQualificationTheme> changeRepresents = IUniBaseDao.instance.get().getList(dql);
                if (!CollectionUtils.isEmpty(changeRepresents)) {
                   	RepresentChangeQualificationTheme changeRepresent = changeRepresents.get(0);
	                studentTheme.setTheme(changeRepresent.getNewTheme());
	                baseUpdate(studentTheme);
	
	                student.setFinalQualifyingWorkTheme(changeRepresent.getNewTheme());
	                baseUpdate(student);
                }
                else {
	                studentTheme.setTheme(null);
	                baseUpdate(studentTheme);
	
	                student.setFinalQualifyingWorkTheme(null);
	                baseUpdate(student);
                }

	        }
	        else {
	        //----------------------------------------------------------------------------------------------------------------------
		
	        	List<StudentQualificationThemes> list = IUniBaseDao.instance.get().getList(StudentQualificationThemes.class, StudentQualificationThemes.student().s(), student);
		        if (representation.getListOrder() == null || list.isEmpty()) {
		            studentTheme.setTheme(null);
		            baseUpdate(studentTheme);
		
		            student.setFinalQualifyingWorkTheme(null);
		            baseUpdate(student);
		            return true;
		        }
		        boolean changed = false;
		        for (StudentQualificationThemes theme : list) {
		            if (theme.getListRepresent() != null && theme.getListRepresent().getOrder() != null && theme.getListRepresent().getOrder().equals(representation.getListOrder())) {
		
		                changed = true;
		                studentTheme.setTheme(theme.getTheme());
		                baseUpdate(studentTheme);
		
		                student.setFinalQualifyingWorkTheme(theme.getTheme());
		                baseUpdate(student);
		                break;
	                }
	            }
	            if (!changed) {
	                studentTheme.setTheme(null);
	                baseUpdate(studentTheme);
		
	                student.setFinalQualifyingWorkTheme(null);
	                baseUpdate(student);
	            }
            //----------------------------------------------------------------------------------------------------------------
        	}
            //----------------------------------------------------------------------------------------------------------------

        }

        return true;
    }

    @Override
    public void buildBodyExtract(Representation representationBase, String itemFac, RtfDocument docCommon) {

        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        UtilPrintSupport.setParNum(docCommon, itemFac);

        RtfInjectModifier modifier = new RtfInjectModifier();

        UtilPrintSupport.injectModifierExtract(modifier, docRepresent);
        
        modifier.modify(docCommon);
    }

    @Override
    public void buildBodyRepresent(Representation representationBase, RtfDocument document, AtomicInteger parNumber, AtomicInteger extractNumber) {
        super.buildBodyRepresent(representationBase, document, parNumber, extractNumber);
        RepresentChangeQualificationTheme represent = (RepresentChangeQualificationTheme) representationBase;
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        Student student = UtilPrintSupport.getStudent(represent);


        RtfInjectModifier modifier = new RtfInjectModifier();

        UtilPrintSupport.injectModifierRepresent(modifier, docRepresent, student);
        if (parNumber != null && extractNumber != null) {
            StringBuilder str = new StringBuilder();
            str.append(parNumber.intValue())
                    .append(".")
                    .append(extractNumber.intValue())
                    .append(". ");

            modifier.put("parNumberNext", str.toString());
            extractNumber.getAndAdd(1);
        }
        else {
            modifier.put("parNumberNext", "");
        }

        String workType = "выпускной квалификационной работы";
        if (student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isMaster())
            workType = "магистерской диссертации";

        String levelType = "специальность ";
        if (student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isMaster() ||
                student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isBachelor())
            levelType = "направление подготовки ";
        String code = UtilPrintSupport.getEduLevelCodeWithQualification(student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel());
        String levelTitle = UtilPrintSupport.getParentTitleIfProfile(student.getEducationOrgUnit().getEducationLevelHighSchool());


        EducationYear educationYear = UniDaoFacade.getCoreDao().get(EducationYear.class, EducationYear.current().s(), true);

        modifier.put("qualificationWorkType", workType);
        //------------------------------------------------------------------------------
        modifier.put("eduYear", educationYear == null ? "" : educationYear.getTitle().replace("/", "-"));
        //------------------------------------------------------------------------------
        modifier.put("eduLevelTitle", levelType + code + levelTitle);
        //----------------------------------------------------------------------------------------------------------------------------
        /*
        modifier.put("orderDate", "");
        modifier.put("orderNumber", "");
        if (represent.getListOrder() != null) {
            if (represent.getListOrder().getCommitDate() != null) {
                RtfString rtfStr = UtilPrintSupport.getDateFormatterWithMonthString(represent.getListOrder().getCommitDate()).append(IRtfData.SYMBOL_TILDE).append("года");
                modifier.put("orderDate", rtfStr);
            }
            modifier.put("orderNumber", StringUtils.trimToEmpty(represent.getListOrder().getNumber()));

            List<ListRepresent> list = represent.getListOrder().getRepresentationList();

            DQLSelectBuilder selectBuilder = new DQLSelectBuilder().fromEntity(RelListRepresentStudents.class, "entity")
                    .where(DQLExpressions.eq(
                            DQLExpressions.property(RelListRepresentStudents.student().fromAlias("entity")),
                            DQLExpressions.value(student)
                    ))
                    .where(DQLExpressions.in(DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("entity")), list));

            List<RelListRepresentStudents> l = selectBuilder.createStatement(getSession()).list();
            if (l.size() != 0) {
                modifier.put("ordertitle", "«" + l.get(0).getRepresentation().getType().getTitle() + "»");
            }
            else {
                modifier.put("ordertitle", "");
            }

        }
        else {
            modifier.put("ordertitle", "");
        }
        */
        
        if(represent.getListOrder() != null || represent.getDocumentOrder() != null){
        	
        	RtfString rtfStr = new RtfString();
        	
        	if(represent.getListOrder() != null) {
        		
        		rtfStr.append("приказ");
        		if (represent.getListOrder().getCommitDate() != null) {
        			
        	        String day = RussianDateFormatUtils.getDayString(represent.getListOrder().getCommitDate(), true);
        	        String month = CommonBaseDateUtil.getMonthNameDeclined(represent.getListOrder().getCommitDate(), GrammaCase.GENITIVE);
        	        String year = RussianDateFormatUtils.getYearString(represent.getListOrder().getCommitDate(), false);
			
                    rtfStr.append(" от ").append(day).append(IRtfData.SYMBOL_TILDE).append(month).append(" ").append(year).append(IRtfData.SYMBOL_TILDE).append("года");
                }
        		
        		if(represent.getListOrder().getNumber() != null) {
        			rtfStr.append(" №").append(IRtfData.SYMBOL_TILDE).append(StringUtils.trimToEmpty(represent.getListOrder().getNumber()));
        		}
        		
                List<ListRepresent> list = represent.getListOrder().getRepresentationList();

                DQLSelectBuilder dql = new DQLSelectBuilder()
                		.fromEntity(RelListRepresentStudents.class, "entity")
                        .where(DQLExpressions.eq(
                                DQLExpressions.property(RelListRepresentStudents.student().fromAlias("entity")),
                                DQLExpressions.value(student)
                        ))
                        .where(DQLExpressions.in(DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("entity")), list))
                        .column(DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("entity")))
                ;

                List<ListRepresent> listRepresents = IUniBaseDao.instance.get().getList(dql);
                if(listRepresents.size() > 0) {
                	rtfStr.append(" «" + listRepresents.get(0).getType().getTitle() + "»");
                }
       		
                if(represent.getDocumentOrder() != null) {
                	rtfStr.append(", ");
                }
        	}
        	
        	if(represent.getDocumentOrder() != null) {
        		
        		rtfStr.append("приказ");
        		if (represent.getDocumentOrder().getCommitDate() != null) {
        			
        	        String day = RussianDateFormatUtils.getDayString(represent.getDocumentOrder().getCommitDate(), true);
        	        String month = CommonBaseDateUtil.getMonthNameDeclined(represent.getDocumentOrder().getCommitDate(), GrammaCase.GENITIVE);
        	        String year = RussianDateFormatUtils.getYearString(represent.getDocumentOrder().getCommitDate(), false);
 
                    rtfStr.append(" от ").append(day).append(IRtfData.SYMBOL_TILDE).append(month).append(" ").append(year).append(IRtfData.SYMBOL_TILDE).append("года");
        		}
        		
        		if(represent.getDocumentOrder().getNumber() != null) {
        			rtfStr.append(" №").append(IRtfData.SYMBOL_TILDE).append(StringUtils.trimToEmpty(represent.getDocumentOrder().getNumber()));
        		}
        		
            	DQLSelectBuilder dql = new DQLSelectBuilder()
            			.fromEntity(DocRepresentStudentBase.class, "docrep")
            			.joinEntity("docrep", DQLJoinType.inner, DocOrdRepresent.class, "docordrep", DQLExpressions.eq(DQLExpressions.property(DocOrdRepresent.representation().id().fromAlias("docordrep")), DQLExpressions.property(DocRepresentStudentBase.representation().id().fromAlias("docrep"))))
                   		.where(DQLExpressions.eq(DQLExpressions.property(DocRepresentStudentBase.student().fromAlias("docrep")), DQLExpressions.value(student)))
                   		.where(DQLExpressions.eq(DQLExpressions.property(DocOrdRepresent.order().fromAlias("docordrep")), DQLExpressions.value(represent.getDocumentOrder())))
                   		.column(DQLExpressions.property(DocOrdRepresent.representation().fromAlias("docordrep")))
            	;
            	
                List<RepresentChangeQualificationTheme> docRepresents = IUniBaseDao.instance.get().getList(dql);
                
                if(docRepresents.size() > 0) {
                	rtfStr.append(" «" + docRepresents.get(0).getType().getTitle() + "»");
                }                
        		
        	}
        	
        	modifier.put("changedOrders", rtfStr);
        	
        } 
        else {
            modifier.put("changedOrders", "");
        }
        
        //----------------------------------------------------------------------------------------------------------------------------
        modifier.put("studentFio", PersonManager.instance().declinationDao().getDeclinationFIO(student.getPerson().getIdentityCard(), GrammaCase.GENITIVE));

        RtfTableModifier tm = new RtfTableModifier();
        String[][] row = new String[1][4];
        row[0][0] = "1";
        row[0][1] = student.getFullFio();
        row[0][2] = StringUtils.trimToEmpty(represent.getNewTheme());
        row[0][3] = StringUtils.trimToEmpty(represent.getNewAdvisor());
        tm.put("T", row);
        tm.modify(document);
//        modifier.put("settlement", represent.getAddressItem() ==null ? "" : represent.getAddressItem().getFullTitle());
//        modifier.put("country", represent.getAddressCountry() == null ? "" : represent.getAddressCountry().getCode() == RUSSIA_CODE ? "" : " "+represent.getAddressCountry().getTitle());
//        modifier.put("organization", represent.getOrganization() ==null ? "" : "(" +represent.getOrganization() + ") ");
//        modifier.put("target", represent.getTarget() ==null ? "" : represent.getTarget());
//        modifier.put("days", represent.getDays() ==null ? "" : String.valueOf(represent.getDays()));
//        modifier.put("paymentData", represent.getChangeQualificationThemePaymentData() ==null ? "" : represent.getChangeQualificationThemePaymentData().getTitle());
//        modifier.put("dateBeginChangeQualificationTheme", represent.getBeginDate() == null? new RtfString(): UtilPrintSupport.getDateFormatterWithMonthString(represent.getBeginDate()).append(IRtfData.SYMBOL_TILDE).append("г"));
//        modifier.put("dateEndChangeQualificationTheme", represent.getEndDate() == null? new RtfString(): UtilPrintSupport.getDateFormatterWithMonthString(represent.getEndDate()).append(IRtfData.SYMBOL_TILDE).append("г"));
//        modifier.put("studentSex_D", student.getPerson().isMale() ? "студента" : "студентку");

        if (StringUtils.isEmpty(modifier.getStringValue("basics")))
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Arrays.asList("basics"), false, false);

        modifier.modify(document);
    }


}
