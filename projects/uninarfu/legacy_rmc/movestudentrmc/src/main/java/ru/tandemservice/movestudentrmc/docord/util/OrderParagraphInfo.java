package ru.tandemservice.movestudentrmc.docord.util;

public class OrderParagraphInfo {

    private int paragraphNumber;
    private int extractNumber;
    private int addonNumber;
    private int studentNumber;

    public OrderParagraphInfo(int paragraphNumber, int extractNumber, int addonNumber, int studentNumber) {
        this.setParagraphNumber(paragraphNumber);
        this.setExtractNumber(extractNumber);
        this.setAddonNumber(addonNumber);
        this.setStudentNumber(studentNumber);
    }

    public OrderParagraphInfo(int paragraphNumber, int extractNumber) {
        this.setParagraphNumber(paragraphNumber);
        this.setExtractNumber(extractNumber);
        this.setAddonNumber(0);
        this.setStudentNumber(0);
    }

    public OrderParagraphInfo() {
        this.setParagraphNumber(0);
        this.setExtractNumber(0);
        this.setAddonNumber(0);
        this.setStudentNumber(0);
    }

    public int getParagraphNumber() {
        return paragraphNumber;
    }

    public void setParagraphNumber(int paragraphNumber) {
        this.paragraphNumber = paragraphNumber;
    }

    public int getExtractNumber() {
        return extractNumber;
    }

    public void setExtractNumber(int extractNumber) {
        this.extractNumber = extractNumber;
    }

    public int getAddonNumber() {
        return addonNumber;
    }

    public void setAddonNumber(int addonNumber) {
        this.addonNumber = addonNumber;
    }

    public int getStudentNumber() {
        return studentNumber;
    }

    public void setStudentNumber(int studentNumber) {
        this.studentNumber = studentNumber;
    }


}
