package ru.tandemservice.movestudentrmc.base.bo.ListRepresentDiplomAndExclude.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfString;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.AbstractListRepresentDAO;
import ru.tandemservice.movestudentrmc.docord.util.MoveStudentUtil;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ListRepresentDiplomAndExcludeManagerModifyDAO extends AbstractListRepresentDAO
{

    @Override
    public void save(ListRepresent listRepresent, List<Student> studentSelectedList, DocListRepresentBasics representBasics) {

        baseCreate(listRepresent);

        baseCreateOrUpdate(representBasics);

        ListRepresentDiplomAndExclude diplomAndExclude = (ListRepresentDiplomAndExclude) listRepresent;

        for (Student student : studentSelectedList) {

            RelListRepresentStudentsOldData relListRepresentDiplomAndExcludeStudents = new RelListRepresentStudentsOldData();
            relListRepresentDiplomAndExcludeStudents.setRepresentation(diplomAndExclude);
            relListRepresentDiplomAndExcludeStudents.setStudent(student);
            relListRepresentDiplomAndExcludeStudents.setOldStatus(student.getStatus());
            baseCreateOrUpdate(relListRepresentDiplomAndExcludeStudents);
        }
    }

    @Override
    public void update(ListRepresent listRepresent, List<Student> studentSelectedList) {

        baseCreateOrUpdate(listRepresent);
        ListRepresentDiplomAndExclude diplomAndExclude = (ListRepresentDiplomAndExclude) listRepresent;
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(RelListRepresentStudents.class, "r").column("r");
        builder.where(DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("r")), DQLExpressions.value(listRepresent)));
        List<RelListRepresentStudents> representSelectedOldList = getStudentList(listRepresent);


        for (RelListRepresentStudents representSelectedOld : representSelectedOldList) {
            Boolean isFind = false;
            for (Student studentSelected : studentSelectedList) {
                if (representSelectedOld.getStudent().equals(studentSelected)) {
                    isFind = true;
                    studentSelectedList.remove(studentSelected);
                    break;
                }
            }
            if (!isFind) {
                ListRepresentDiplomAndExclude represent = (ListRepresentDiplomAndExclude) representSelectedOld.getRepresentation();
                baseCreateOrUpdate(represent);
                baseDelete(representSelectedOld);
            }
        }

        for (Student student : studentSelectedList) {
            RelListRepresentStudentsOldData relListRepresentDiplomAndExcludeStudents = new RelListRepresentStudentsOldData();
            relListRepresentDiplomAndExcludeStudents.setRepresentation(diplomAndExclude);
            relListRepresentDiplomAndExcludeStudents.setStudent(student);
            relListRepresentDiplomAndExcludeStudents.setOldStatus(student.getStatus());
            baseCreateOrUpdate(relListRepresentDiplomAndExcludeStudents);
        }
    }


    @Override
    public void buildBodyExtract(ListOrdListRepresent listOrdListRepresent,
                                 Student student, RtfDocument docExtract, Map<Student, OrderParagraphInfo> map)
    {
        super.buildBodyExtract(listOrdListRepresent, student, docExtract, map);

        ListRepresentDiplomAndExclude listRepresent = (ListRepresentDiplomAndExclude) listOrdListRepresent.getRepresentation();

        String qualification = "", prof = "", direction = "";
        if (student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isBachelor()) {
            qualification = "степень бакалавра";
            prof = " профиль ";
            direction = " направление подготовки ";
        }
        else if (student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isMaster()) {
            qualification = "степень магистра";
            prof = " магистерская программа ";
            direction = " направление подготовки ";
        }
        else {
            qualification = "квалификацию";
            prof = " специализация ";
            direction = " специальность ";
        }
        String kurs = (student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isMaster()) ? "курса магистратуры" : "курса";
//        String basics = " решение ГАК (протокол от "+ DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(listRepresent.getDateProtocol())+" года №"+listRepresent.getNumberProtocol()+")";

        RtfString basics = new RtfString();
        basics.append("Основание: решение ГАК (протокол от ");
        UtilPrintSupport.getDateFormatterWithMonthStringAndYear(listRepresent.getDateProtocol(), basics);
        basics.append(" №").append(IRtfData.SYMBOL_TILDE).append(listRepresent.getNumberProtocol() + ").");

        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(RelListRepresentStudents.class, "r");
        dql.where(
                DQLExpressions.and(
                        DQLExpressions.eq(
                                DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("r")),
                                DQLExpressions.value(listRepresent)
                        ),
                        DQLExpressions.eq(
                                DQLExpressions.property(RelListRepresentStudents.student().fromAlias("r")),
                                DQLExpressions.value(student)
                        )
                )
        );
        RelListRepresentStudents students = dql.createStatement(getSession()).uniqueResult();

        new RtfInjectModifier()
                .put("qualification", qualification)
                .put("diplomaQualification", StringUtils.uncapitalize(student.getEducationOrgUnit().getEducationLevelHighSchool().getQualificationTitleNullSafe()))
                .put("highSchool", UtilPrintSupport.getHighLevelSchoolTypeString(student.getEducationOrgUnit().getEducationLevelHighSchool()))
                //----------------------------------------------------------------------------------------------------
                .put("developPeriod", student.getEducationOrgUnit().getDevelopPeriod().getTitle())
                .put("studentNumber", String.valueOf(map.get(student).getStudentNumber()))
                //----------------------------------------------------------------------------------------------------
                .put("dateExclude", UtilPrintSupport.getDateFormatterWithMonthString(listRepresent.getDateExclude()))
                .put("kurs", kurs)
                .put("basics", basics)
                .put("diplomKind", students.getFive() ? "С отличием" : "Без отличия")
                .modify(docExtract);


    }

    @Override
    public Map<Student, OrderParagraphInfo> buildBodyRepresent(List<ListRepresent> representationBase, RtfDocument document) {
        ListPrintDoc listPrintDoc = new ListPrintDoc();
        RtfDocument templateRepresent = listPrintDoc.creationPrintDocOrder(representationBase);
        document.getElementList().addAll(templateRepresent.getElementList());
        return listPrintDoc.getParagInfomap();

    }


    @Override
    public boolean doRollback(ListRepresent represent, ErrorCollector error) {
        List<RelListRepresentStudents> representList = getStudentList(represent);

        for (RelListRepresentStudents representStudents : representList) {
            RelListRepresentStudentsOldData oldData = (RelListRepresentStudentsOldData) representStudents;
            Student student = oldData.getStudent();
            student.setStatus(oldData.getOldStatus());
            student.setFinishYear(oldData.getFinishedYear());
            getSession().saveOrUpdate(student);
        }

        rollbackStudentGrantsCancel(represent);

        return true;
    }

    @Override
    public boolean doCommit(ListRepresent represent, ErrorCollector error) {
        List<RelListRepresentStudents> representList = getStudentList(represent);
        List<Student> students = new ArrayList<>();
        for (RelListRepresentStudents representStudents : representList) {
            Student student = representStudents.getStudent();
            students.add(student);

            representStudents.setFinishedYear(student.getFinishYear());
            student.setFinishYear(CoreDateUtils.getYear(representStudents.getRepresentation().getOrder().getCommitDate()));
            student.setStatus(DataAccessServices.dao().get(StudentStatus.class, StudentStatus.code(), UniDefines.CATALOG_STUDENT_STATUS_GRADUATED_WITH_DIPLOMA));
            getSession().saveOrUpdate(student);
        }
        ListRepresentDiplomAndExclude listRepresent = (ListRepresentDiplomAndExclude) represent;

        List<StudentGrantEntity> sgeList = MoveStudentUtil.getSrudentGrantEntityList(students, listRepresent.getDateExclude());

        commitStudentGrantsCancel(listRepresent, sgeList);

        return true;
    }


    @Override
    public List<Student> selectRepresent(ListRepresent listRepresent) {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(RelListRepresentStudents.class, "rls");

        builder.where(DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("rls")), DQLExpressions.value(listRepresent)));

        builder.column(DQLExpressions.property(RelListRepresentStudents.student().fromAlias("rls")));

        return builder.createStatement(getSession()).list();
    }

    public void saveSupport(ListRepresent listRepresent, List<DataWrapper> studentSelectedList, DocListRepresentBasics representBasics) {
    }

}
