package ru.tandemservice.movestudentrmc.base.bo.GrantsRelationSettings.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantType;

public class GrantTypeDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput> {

    public static final String TYPE_COLUMN = "type";
    public static final String ISCONFIG_COLUMN = "isConfig";

    public GrantTypeDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(GrantType.class, "type").addColumn("type");

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).order(GrantType.class, "type").build();
    }
}
