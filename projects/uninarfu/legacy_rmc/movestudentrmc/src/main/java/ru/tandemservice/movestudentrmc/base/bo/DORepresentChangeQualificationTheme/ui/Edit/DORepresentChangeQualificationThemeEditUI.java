package ru.tandemservice.movestudentrmc.base.bo.DORepresentChangeQualificationTheme.ui.Edit;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentEditUI;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentQualificationThemes.logic.OrgUnitSelectedDSHandler;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.uni.dao.IUniBaseDao;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class DORepresentChangeQualificationThemeEditUI extends AbstractDORepresentEditUI
{

    // Логика по "Указать кафедру/ПЦК"
    public final static long CAF_ID = 0L;
    public final static long PCK_ID = 1L;
    public final static String CAF_TITLE = "Утверждено кафедрой";
    public final static String PCK_TITLE = "Утверждено ПЦК";
    private final static String PCK_PRINT_TITLE = "предметно-цикловой комиссии";
    private final static String CAF_PRINT_TITLE = "кафедры";
    public final static String SEPARATOR = ".";
    private static IdentifiableWrapper<IEntity> PCK = new IdentifiableWrapper<IEntity>(PCK_ID, PCK_TITLE);
    private static IdentifiableWrapper<IEntity> CAF = new IdentifiableWrapper<IEntity>(CAF_ID, CAF_TITLE);

    private List<IdentifiableWrapper<IEntity>> sourceFilterList; // Источник Кафедра/ПЦК
    private IdentifiableWrapper<IEntity> sourceFilter;
    private ISelectModel orgUnitSelectModel;
    private OrgUnit orgUnit;
    private boolean orgUnitVisible;

    @Override
    protected void initDocument() {
        super.initDocument();
//		_studentStatusNewStr = ((StudentStatus) UniDaoFacade.getCoreDao().getCatalogItem(StudentStatus.class, "5")).getTitle();
    }

    @Override
    public void onComponentPrepareRender() {
        super.onComponentPrepareRender();

        setSourceFilterList(Arrays.asList(
                CAF,
                PCK
        ));

        if (sourceFilter == null) {
            sourceFilter = CAF;
            this.orgUnitVisible = true;
            RepresentChangeQualificationTheme represent = (RepresentChangeQualificationTheme) getRepresentObj();

            this.orgUnit = represent.getAcceptOrgUnit();

            String code = represent.getAcceptOrgUnitType();

            if (code != null) {
                String id = StringUtils.substringBefore(code, SEPARATOR);
                try {
                    Long value = Long.valueOf(id);
                    if (value != CAF_ID) {
                        sourceFilter = PCK;
                        this.orgUnitVisible = false;
                    }
                }
                catch (Exception e) {

                }
            }
        }
    }

    @Override
    public void onComponentRefresh() {
        updateAdvisor();
//       setOrganizationSelectModel(new SingleSelectTextModel() {
//           @Override
//           public ListResult findValues(String filter) {
//
//               DQLSelectBuilder builder = new DQLSelectBuilder()
//                       .fromEntity(RepresentChangeQualificationTheme.class, "e")
//                       .column(DQLExpressions.property(RepresentChangeQualificationTheme.organization().fromAlias("e")))
//                       .setPredicate(DQLPredicateType.distinct)
//                       .where(DQLExpressions.likeUpper(DQLExpressions.property(RepresentChangeQualificationTheme.organization().fromAlias("e")), DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));
//
//               List list = builder.createStatement(getSupport().getSession()).setMaxResults(50).list();
//               Number number = (Number) builder.createCountStatement(new DQLExecutionContext(getSupport().getSession())).uniqueResult();
//
//               return new ListResult(list, number == null ? 0L : number.intValue());
//           }
//       });

        super.onComponentRefresh();
    }

    @Override
    protected void FetchStudents(Representation doc) {

        _studentDocList.clear();
        _studentList.clear();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocRepresentStudentBase.class, "stud").addColumn("stud");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(DocRepresentStudentBase.representation().fromAlias("stud")),
                DQLExpressions.value(doc.getId())));

        List<DocRepresentStudentBase> studentList = builder.createStatement(getSupport().getSession()).list();

        for (DocRepresentStudentBase student : studentList) {

            _studentDocList.add(student);
            _studentList.add(student.getStudent());
        }

        _student = _studentList.get(0);
    }

    public void onChangeOrder() {
        updateAdvisor();


    }

    private void updateAdvisor() {
        if (getRepresentObj() != null) {

            RepresentChangeQualificationTheme represent = (RepresentChangeQualificationTheme) getRepresentObj();
            if (represent.getListOrder() != null) {

                DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(RelListRepresentStudents.class, "r").joinEntity("r", DQLJoinType.inner, ListOrdListRepresent.class, "o", DQLExpressions.eq(DQLExpressions.property(ListOrdListRepresent.representation().fromAlias("o")), DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("r"))));
                dql.where(DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudents.student().fromAlias("r")), DQLExpressions.value(_student)));
                dql.where(DQLExpressions.eq(DQLExpressions.property(ListOrdListRepresent.order().fromAlias("o")), DQLExpressions.value(represent.getListOrder())));
                dql.column(DQLExpressions.property(ListOrdListRepresent.representation().fromAlias("o")));
                List<ListRepresentQualificationThemes> represents = IUniBaseDao.instance.get().getList(dql);
                if (!CollectionUtils.isEmpty(represents)) {
                    ListRepresentQualificationThemes listRepresent = represents.get(0);
                    DQLSelectBuilder dqlSelectBuilder = new DQLSelectBuilder().fromEntity(StudentQualificationThemes.class, "st");
                    dqlSelectBuilder.where(DQLExpressions.eq(DQLExpressions.property(StudentQualificationThemes.student().fromAlias("st")), DQLExpressions.value(_student)));
                    dqlSelectBuilder.where(DQLExpressions.eq(DQLExpressions.property(StudentQualificationThemes.listRepresent().fromAlias("st")), DQLExpressions.value(listRepresent)));
                    List<StudentQualificationThemes> qualificationThemes = IUniBaseDao.instance.get().getList(dqlSelectBuilder);
                    if (!CollectionUtils.isEmpty(qualificationThemes)) {
                        StudentQualificationThemes theme = qualificationThemes.get(0);
                        represent.setNewAdvisor(StringUtils.trimToEmpty(theme.getAdvisor()));
                        represent.setAcceptOrgUnit(listRepresent.getAcceptOrgUnit());
                        represent.setAcceptOrgUnitType(listRepresent.getAcceptOrgUnitType());

                    }
                }

            }
            
            //---------------------------------------------------------------------------------------------------------------------
            // Если выбран приказ О внесении изменений в приказ об утверждении тем ВКР
            if(represent.getDocumentOrder() != null) {
            	
            	if(represent.getListOrder() == null || represent.getDocumentOrder().getCommitDate().compareTo(represent.getListOrder().getCommitDate()) > 0) {
            		
            		DQLSelectBuilder dql = new DQLSelectBuilder()
            				.fromEntity(DocRepresentStudentBase.class, "docrep")
            				.joinEntity("docrep", DQLJoinType.inner, DocOrdRepresent.class, "docordrep", DQLExpressions.eq(DQLExpressions.property(DocOrdRepresent.representation().id().fromAlias("docordrep")), DQLExpressions.property(DocRepresentStudentBase.representation().id().fromAlias("docrep"))))
                      		.where(DQLExpressions.eq(DQLExpressions.property(DocRepresentStudentBase.student().fromAlias("docrep")), DQLExpressions.value(_student)))
                    		.where(DQLExpressions.eq(DQLExpressions.property(DocOrdRepresent.order().fromAlias("docordrep")), DQLExpressions.value(represent.getDocumentOrder())))
                    		.column(DQLExpressions.property(DocOrdRepresent.representation().fromAlias("docordrep")))
            		;
                    List<RepresentChangeQualificationTheme> represents = IUniBaseDao.instance.get().getList(dql);
                    if (!CollectionUtils.isEmpty(represents)) {
                    	RepresentChangeQualificationTheme changeRepresent = represents.get(0);
                        represent.setNewAdvisor(StringUtils.trimToEmpty(changeRepresent.getNewAdvisor()));
                        represent.setAcceptOrgUnit(changeRepresent.getAcceptOrgUnit());
                        represent.setAcceptOrgUnitType(changeRepresent.getAcceptOrgUnitType());
                    }

            	}
            	
            }
            //---------------------------------------------------------------------------------------------------------------------

        }

    }

    public void onChangeType()
    {
        setOrgUnitVisible(getSourceFilter().getId() == OrgUnitSelectedDSHandler.CAF_ID);
        if (getSourceFilter().getId() == OrgUnitSelectedDSHandler.PCK_ID)
            orgUnit = null;
    }

    @Override
    public void onClickSave() throws IOException {
        RepresentChangeQualificationTheme represent = (RepresentChangeQualificationTheme) getRepresentObj();
        represent.setAcceptOrgUnit(getOrgUnit());
        represent.setAcceptOrgUnitType(String.valueOf(sourceFilter.getId()) + SEPARATOR + sourceFilter.getTitle());
        // Вначале изменяем наше, а потом делаем сохранение
        super.onClickSave();
    }

    public boolean isOrgUnitVisible() {
        return orgUnitVisible;
    }

    public void setOrgUnitVisible(boolean orgUnitVisible) {
        this.orgUnitVisible = orgUnitVisible;
    }

    public OrgUnit getOrgUnit() {
        return orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit) {
        this.orgUnit = orgUnit;
    }

    public ISelectModel getOrgUnitSelectModel() {
        return orgUnitSelectModel;
    }

    public void setOrgUnitSelectModel(ISelectModel orgUnitSelectModel) {
        this.orgUnitSelectModel = orgUnitSelectModel;
    }

    public IdentifiableWrapper<IEntity> getSourceFilter() {
        return sourceFilter;
    }

    public void setSourceFilter(IdentifiableWrapper<IEntity> sourceFilter) {
        this.sourceFilter = sourceFilter;
    }

    public List<IdentifiableWrapper<IEntity>> getSourceFilterList() {
        return sourceFilterList;
    }

    public void setSourceFilterList(List<IdentifiableWrapper<IEntity>> sourceFilterList) {
        this.sourceFilterList = sourceFilterList;
    }
}
