package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_movestudentrmc_1x0x0_34to35 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.10"),
                        new ScriptDependency("org.tandemframework.shared", "1.1.3"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.1.3")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность principal2Visa

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("principal2visa_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("principal_id", DBType.LONG).setNullable(false),
                                      new DBColumn("signpost_p", DBType.createVarchar(255)),
                                      new DBColumn("signfio_p", DBType.createVarchar(255)),
                                      new DBColumn("posttitle1_p", DBType.createVarchar(255)),
                                      new DBColumn("postfio1_p", DBType.createVarchar(255)),
                                      new DBColumn("posttitle2_p", DBType.createVarchar(255)),
                                      new DBColumn("postfio2_p", DBType.createVarchar(255)),
                                      new DBColumn("posttitle3_p", DBType.createVarchar(255)),
                                      new DBColumn("postfio3_p", DBType.createVarchar(255)),
                                      new DBColumn("posttitle4_p", DBType.createVarchar(255)),
                                      new DBColumn("postfio4_p", DBType.createVarchar(255)),
                                      new DBColumn("posttitle5_p", DBType.createVarchar(255)),
                                      new DBColumn("postfio5_p", DBType.createVarchar(255))
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("principal2Visa");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность principal2DocumentOrderVisa

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("principal2documentordervisa_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("documentorder_id", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("principal2DocumentOrderVisa");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность principal2ListOrderVisa

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("principal2listordervisa_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("listorder_id", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("principal2ListOrderVisa");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность documentOrder

        // удалено свойство nacMetod
        {

            // удалить колонку
            tool.dropColumn("documentorder_t", "nacmetod_p");

        }

        // удалено свойство glavBuh
        {
            // удалить колонку
            tool.dropColumn("documentorder_t", "glavbuh_p");

        }

        // удалено свойство zam_Ekonom
        {
            // удалить колонку
            tool.dropColumn("documentorder_t", "zam_ekonom_p");

        }

        // удалено свойство director
        {

            // удалить колонку
            tool.dropColumn("documentorder_t", "director_p");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность listOrder

        // удалено свойство nacMetod
        {
            // удалить колонку
            tool.dropColumn("listorder_t", "nacmetod_p");

        }

        // удалено свойство glavBuh
        {
            // удалить колонку
            tool.dropColumn("listorder_t", "glavbuh_p");

        }

        // удалено свойство zam_Ekonom
        {
            // удалить колонку
            tool.dropColumn("listorder_t", "zam_ekonom_p");

        }

        // удалено свойство director
        {
            // удалить колонку
            tool.dropColumn("listorder_t", "director_p");

        }


    }
}