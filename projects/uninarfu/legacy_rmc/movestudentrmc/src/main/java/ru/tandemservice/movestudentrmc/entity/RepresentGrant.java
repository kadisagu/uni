package ru.tandemservice.movestudentrmc.entity;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.movestudentrmc.entity.gen.RepresentGrantGen;

/**
 * О назначении стипендии/выплаты
 */
public class RepresentGrant extends RepresentGrantGen
{
    @Override
    public String getTitle() {
        return new StringBuilder()
                .append(getType().getTitle())
                .append(" (")
                .append(getGrantView().getShortTitle())
                .append(") с ")
                .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getBeginDate()))
                .append(getEndDate() != null ? " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getEndDate()) : "")
                //---------------------------------------------------------------------
                //.append(", от ")
                //.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getCreateDate()))
                //---------------------------------------------------------------------
                .toString();
    }

    @Override
    public String getRepresentationTitle() {
        return new StringBuilder()
                .append(getType().getTitle())
                .append(" (")
                .append(getGrantView().getShortTitle())
                .append(") с ")
                .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getBeginDate()))
                .append(getEndDate() != null ? " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getEndDate()) : "")
                .toString();
    }
}