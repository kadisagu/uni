package ru.tandemservice.movestudentrmc.base.bo.GrantsRelationSettings.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.movestudentrmc.base.bo.GrantsRelationSettings.logic.GrantDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.GrantsRelationSettings.logic.GrantViewDSHandler;

@Configuration
public class GrantsRelationSettingsAddEdit extends BusinessComponentManager {

    public static final String GRANT_DS = "grantDS";
    public static final String VIEW_DS = "viewDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(GRANT_DS, grantDSHandler()))
                .addDataSource(selectDS(VIEW_DS, viewDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler grantDSHandler()
    {
        return (IDefaultComboDataSourceHandler) new GrantDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler viewDSHandler()
    {
        return (IDefaultComboDataSourceHandler) new GrantViewDSHandler(getName());
    }
}
