package ru.tandemservice.movestudentrmc.base.bo.ListRepresentTransfer.ui.View;

import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.support.AbstractListRepresentViewUI;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentTransfer.logic.ListPrintDoc;
import ru.tandemservice.movestudentrmc.entity.ListRepresentTransfer;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

import java.util.Arrays;

public class ListRepresentTransferViewUI extends AbstractListRepresentViewUI<ListRepresentTransfer>
{

    private EducationOrgUnit eduOrgUnit = new EducationOrgUnit();

    @Override
    public void onComponentRefresh() {
        super.onComponentRefresh();
        ListRepresentTransfer rep = (ListRepresentTransfer) getListRepresent();
        EducationOrgUnit educationOrgUnit = rep.getEducationOrgUnit() == null ? rep.getGroupNew().getEducationOrgUnit() : rep.getEducationOrgUnit();
        getEduOrgUnit().update(educationOrgUnit);
    }

    @Override
    public ListRepresentTransfer getListRepresentObject() {
        return new ListRepresentTransfer();
    }

    public EducationOrgUnit getEduOrgUnit() {
        return eduOrgUnit;
    }

    public void setEduOrgUnit(EducationOrgUnit eduOrgUnit) {
        this.eduOrgUnit = eduOrgUnit;
    }

    @Override
    protected RtfDocument printRepresent() {
        return new ListPrintDoc().creationPrintDocOrder(Arrays.asList(getListRepresent()));
    }


}
