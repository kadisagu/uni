package ru.tandemservice.movestudentrmc.component.catalog.representationBasement.RepresentationBasementPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationBasement;

public class DAO extends DefaultCatalogPubDAO<RepresentationBasement, Model> implements IDAO {

    public void updatePrintable(Model model, Long id) {
        RepresentationBasement representationBasement = getNotNull(RepresentationBasement.class, id);
        if (representationBasement.isPrintable()) {
            representationBasement.setPrintable(false);
        }
        else {
            representationBasement.setPrintable(true);
        }
        save(representationBasement);
    }

    public void updateNoteRequired(Model model, Long id) {
        RepresentationBasement representationBasement = getNotNull(RepresentationBasement.class, id);
        if (representationBasement.isNoteRequired()) {
            representationBasement.setNoteRequired(false);
        }
        else {
            representationBasement.setNoteRequired(true);
            representationBasement.setConnectedWithDocument(false);
        }
        save(representationBasement);
    }

    public void updateMerged(Model model, Long id) {
        RepresentationBasement representationBasement = getNotNull(RepresentationBasement.class, id);
        if (representationBasement.isMerged()) {
            representationBasement.setMerged(false);
        }
        else {
            representationBasement.setMerged(true);
        }
        save(representationBasement);
    }

    @Override
    public void updateConnectedWithDocument(Model model, Long id) {
        RepresentationBasement representationBasement = getNotNull(RepresentationBasement.class, id);
        if (representationBasement.isConnectedWithDocument()) {
            representationBasement.setConnectedWithDocument(false);
        }
        else {
            representationBasement.setConnectedWithDocument(true);
            representationBasement.setNoteRequired(false);
        }
        save(representationBasement);
    }
}
