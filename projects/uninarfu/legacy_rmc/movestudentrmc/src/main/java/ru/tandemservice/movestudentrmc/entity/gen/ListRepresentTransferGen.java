package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListRepresentTransfer;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Списочное представление о переводе
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ListRepresentTransferGen extends ListRepresent
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.ListRepresentTransfer";
    public static final String ENTITY_NAME = "listRepresentTransfer";
    public static final int VERSION_HASH = 1231834702;
    private static IEntityMeta ENTITY_META;

    public static final String P_DATE_TRANSFER = "dateTransfer";
    public static final String L_COMPENSATION_TYPE_NEW = "compensationTypeNew";
    public static final String L_COURSE_NEW = "courseNew";
    public static final String L_GROUP_NEW = "groupNew";
    public static final String L_EDUCATION_ORG_UNIT = "educationOrgUnit";
    public static final String P_DIFFERENCE_EDU_PLAN = "differenceEduPlan";
    public static final String P_DATE_LIQUIDATION = "dateLiquidation";
    public static final String P_INDIVIDUAL_PLAN = "individualPlan";

    private Date _dateTransfer;     // Дата перевода
    private CompensationType _compensationTypeNew;     // Тип возмещения затрат (новый)
    private Course _courseNew;     // Курс (новый)
    private Group _groupNew;     // Группа (новый)
    private EducationOrgUnit _educationOrgUnit;     // Направление подготовки (специальность)
    private boolean _differenceEduPlan = false;     // Разница в учебных планах
    private Date _dateLiquidation;     // Дата ликивидации разницы
    private boolean _individualPlan = false;     // Утвердить ИУП (индивидуальный учебный план)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата перевода. Свойство не может быть null.
     */
    @NotNull
    public Date getDateTransfer()
    {
        return _dateTransfer;
    }

    /**
     * @param dateTransfer Дата перевода. Свойство не может быть null.
     */
    public void setDateTransfer(Date dateTransfer)
    {
        dirty(_dateTransfer, dateTransfer);
        _dateTransfer = dateTransfer;
    }

    /**
     * @return Тип возмещения затрат (новый).
     */
    public CompensationType getCompensationTypeNew()
    {
        return _compensationTypeNew;
    }

    /**
     * @param compensationTypeNew Тип возмещения затрат (новый).
     */
    public void setCompensationTypeNew(CompensationType compensationTypeNew)
    {
        dirty(_compensationTypeNew, compensationTypeNew);
        _compensationTypeNew = compensationTypeNew;
    }

    /**
     * @return Курс (новый).
     */
    public Course getCourseNew()
    {
        return _courseNew;
    }

    /**
     * @param courseNew Курс (новый).
     */
    public void setCourseNew(Course courseNew)
    {
        dirty(_courseNew, courseNew);
        _courseNew = courseNew;
    }

    /**
     * @return Группа (новый).
     */
    public Group getGroupNew()
    {
        return _groupNew;
    }

    /**
     * @param groupNew Группа (новый).
     */
    public void setGroupNew(Group groupNew)
    {
        dirty(_groupNew, groupNew);
        _groupNew = groupNew;
    }

    /**
     * @return Направление подготовки (специальность).
     */
    public EducationOrgUnit getEducationOrgUnit()
    {
        return _educationOrgUnit;
    }

    /**
     * @param educationOrgUnit Направление подготовки (специальность).
     */
    public void setEducationOrgUnit(EducationOrgUnit educationOrgUnit)
    {
        dirty(_educationOrgUnit, educationOrgUnit);
        _educationOrgUnit = educationOrgUnit;
    }

    /**
     * @return Разница в учебных планах. Свойство не может быть null.
     */
    @NotNull
    public boolean isDifferenceEduPlan()
    {
        return _differenceEduPlan;
    }

    /**
     * @param differenceEduPlan Разница в учебных планах. Свойство не может быть null.
     */
    public void setDifferenceEduPlan(boolean differenceEduPlan)
    {
        dirty(_differenceEduPlan, differenceEduPlan);
        _differenceEduPlan = differenceEduPlan;
    }

    /**
     * @return Дата ликивидации разницы.
     */
    public Date getDateLiquidation()
    {
        return _dateLiquidation;
    }

    /**
     * @param dateLiquidation Дата ликивидации разницы.
     */
    public void setDateLiquidation(Date dateLiquidation)
    {
        dirty(_dateLiquidation, dateLiquidation);
        _dateLiquidation = dateLiquidation;
    }

    /**
     * @return Утвердить ИУП (индивидуальный учебный план). Свойство не может быть null.
     */
    @NotNull
    public boolean isIndividualPlan()
    {
        return _individualPlan;
    }

    /**
     * @param individualPlan Утвердить ИУП (индивидуальный учебный план). Свойство не может быть null.
     */
    public void setIndividualPlan(boolean individualPlan)
    {
        dirty(_individualPlan, individualPlan);
        _individualPlan = individualPlan;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ListRepresentTransferGen)
        {
            setDateTransfer(((ListRepresentTransfer)another).getDateTransfer());
            setCompensationTypeNew(((ListRepresentTransfer)another).getCompensationTypeNew());
            setCourseNew(((ListRepresentTransfer)another).getCourseNew());
            setGroupNew(((ListRepresentTransfer)another).getGroupNew());
            setEducationOrgUnit(((ListRepresentTransfer)another).getEducationOrgUnit());
            setDifferenceEduPlan(((ListRepresentTransfer)another).isDifferenceEduPlan());
            setDateLiquidation(((ListRepresentTransfer)another).getDateLiquidation());
            setIndividualPlan(((ListRepresentTransfer)another).isIndividualPlan());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ListRepresentTransferGen> extends ListRepresent.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ListRepresentTransfer.class;
        }

        public T newInstance()
        {
            return (T) new ListRepresentTransfer();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "dateTransfer":
                    return obj.getDateTransfer();
                case "compensationTypeNew":
                    return obj.getCompensationTypeNew();
                case "courseNew":
                    return obj.getCourseNew();
                case "groupNew":
                    return obj.getGroupNew();
                case "educationOrgUnit":
                    return obj.getEducationOrgUnit();
                case "differenceEduPlan":
                    return obj.isDifferenceEduPlan();
                case "dateLiquidation":
                    return obj.getDateLiquidation();
                case "individualPlan":
                    return obj.isIndividualPlan();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "dateTransfer":
                    obj.setDateTransfer((Date) value);
                    return;
                case "compensationTypeNew":
                    obj.setCompensationTypeNew((CompensationType) value);
                    return;
                case "courseNew":
                    obj.setCourseNew((Course) value);
                    return;
                case "groupNew":
                    obj.setGroupNew((Group) value);
                    return;
                case "educationOrgUnit":
                    obj.setEducationOrgUnit((EducationOrgUnit) value);
                    return;
                case "differenceEduPlan":
                    obj.setDifferenceEduPlan((Boolean) value);
                    return;
                case "dateLiquidation":
                    obj.setDateLiquidation((Date) value);
                    return;
                case "individualPlan":
                    obj.setIndividualPlan((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "dateTransfer":
                        return true;
                case "compensationTypeNew":
                        return true;
                case "courseNew":
                        return true;
                case "groupNew":
                        return true;
                case "educationOrgUnit":
                        return true;
                case "differenceEduPlan":
                        return true;
                case "dateLiquidation":
                        return true;
                case "individualPlan":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "dateTransfer":
                    return true;
                case "compensationTypeNew":
                    return true;
                case "courseNew":
                    return true;
                case "groupNew":
                    return true;
                case "educationOrgUnit":
                    return true;
                case "differenceEduPlan":
                    return true;
                case "dateLiquidation":
                    return true;
                case "individualPlan":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "dateTransfer":
                    return Date.class;
                case "compensationTypeNew":
                    return CompensationType.class;
                case "courseNew":
                    return Course.class;
                case "groupNew":
                    return Group.class;
                case "educationOrgUnit":
                    return EducationOrgUnit.class;
                case "differenceEduPlan":
                    return Boolean.class;
                case "dateLiquidation":
                    return Date.class;
                case "individualPlan":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ListRepresentTransfer> _dslPath = new Path<ListRepresentTransfer>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ListRepresentTransfer");
    }
            

    /**
     * @return Дата перевода. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentTransfer#getDateTransfer()
     */
    public static PropertyPath<Date> dateTransfer()
    {
        return _dslPath.dateTransfer();
    }

    /**
     * @return Тип возмещения затрат (новый).
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentTransfer#getCompensationTypeNew()
     */
    public static CompensationType.Path<CompensationType> compensationTypeNew()
    {
        return _dslPath.compensationTypeNew();
    }

    /**
     * @return Курс (новый).
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentTransfer#getCourseNew()
     */
    public static Course.Path<Course> courseNew()
    {
        return _dslPath.courseNew();
    }

    /**
     * @return Группа (новый).
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentTransfer#getGroupNew()
     */
    public static Group.Path<Group> groupNew()
    {
        return _dslPath.groupNew();
    }

    /**
     * @return Направление подготовки (специальность).
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentTransfer#getEducationOrgUnit()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
    {
        return _dslPath.educationOrgUnit();
    }

    /**
     * @return Разница в учебных планах. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentTransfer#isDifferenceEduPlan()
     */
    public static PropertyPath<Boolean> differenceEduPlan()
    {
        return _dslPath.differenceEduPlan();
    }

    /**
     * @return Дата ликивидации разницы.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentTransfer#getDateLiquidation()
     */
    public static PropertyPath<Date> dateLiquidation()
    {
        return _dslPath.dateLiquidation();
    }

    /**
     * @return Утвердить ИУП (индивидуальный учебный план). Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentTransfer#isIndividualPlan()
     */
    public static PropertyPath<Boolean> individualPlan()
    {
        return _dslPath.individualPlan();
    }

    public static class Path<E extends ListRepresentTransfer> extends ListRepresent.Path<E>
    {
        private PropertyPath<Date> _dateTransfer;
        private CompensationType.Path<CompensationType> _compensationTypeNew;
        private Course.Path<Course> _courseNew;
        private Group.Path<Group> _groupNew;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnit;
        private PropertyPath<Boolean> _differenceEduPlan;
        private PropertyPath<Date> _dateLiquidation;
        private PropertyPath<Boolean> _individualPlan;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата перевода. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentTransfer#getDateTransfer()
     */
        public PropertyPath<Date> dateTransfer()
        {
            if(_dateTransfer == null )
                _dateTransfer = new PropertyPath<Date>(ListRepresentTransferGen.P_DATE_TRANSFER, this);
            return _dateTransfer;
        }

    /**
     * @return Тип возмещения затрат (новый).
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentTransfer#getCompensationTypeNew()
     */
        public CompensationType.Path<CompensationType> compensationTypeNew()
        {
            if(_compensationTypeNew == null )
                _compensationTypeNew = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE_NEW, this);
            return _compensationTypeNew;
        }

    /**
     * @return Курс (новый).
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentTransfer#getCourseNew()
     */
        public Course.Path<Course> courseNew()
        {
            if(_courseNew == null )
                _courseNew = new Course.Path<Course>(L_COURSE_NEW, this);
            return _courseNew;
        }

    /**
     * @return Группа (новый).
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentTransfer#getGroupNew()
     */
        public Group.Path<Group> groupNew()
        {
            if(_groupNew == null )
                _groupNew = new Group.Path<Group>(L_GROUP_NEW, this);
            return _groupNew;
        }

    /**
     * @return Направление подготовки (специальность).
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentTransfer#getEducationOrgUnit()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
        {
            if(_educationOrgUnit == null )
                _educationOrgUnit = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT, this);
            return _educationOrgUnit;
        }

    /**
     * @return Разница в учебных планах. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentTransfer#isDifferenceEduPlan()
     */
        public PropertyPath<Boolean> differenceEduPlan()
        {
            if(_differenceEduPlan == null )
                _differenceEduPlan = new PropertyPath<Boolean>(ListRepresentTransferGen.P_DIFFERENCE_EDU_PLAN, this);
            return _differenceEduPlan;
        }

    /**
     * @return Дата ликивидации разницы.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentTransfer#getDateLiquidation()
     */
        public PropertyPath<Date> dateLiquidation()
        {
            if(_dateLiquidation == null )
                _dateLiquidation = new PropertyPath<Date>(ListRepresentTransferGen.P_DATE_LIQUIDATION, this);
            return _dateLiquidation;
        }

    /**
     * @return Утвердить ИУП (индивидуальный учебный план). Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentTransfer#isIndividualPlan()
     */
        public PropertyPath<Boolean> individualPlan()
        {
            if(_individualPlan == null )
                _individualPlan = new PropertyPath<Boolean>(ListRepresentTransferGen.P_INDIVIDUAL_PLAN, this);
            return _individualPlan;
        }

        public Class getEntityClass()
        {
            return ListRepresentTransfer.class;
        }

        public String getEntityName()
        {
            return "listRepresentTransfer";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
