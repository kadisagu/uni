package ru.tandemservice.movestudentrmc.dao;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.Grant;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentGrantType;
import ru.tandemservice.movestudentrmc.entity.catalog.StuGrantStatus;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentGrantTypeCodes;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.StuGrantStatusCodes;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.*;

public class GrantDAO extends UniBaseDao implements IGrantDAO {

    @Override
    public boolean checkStudentGrants(Long representId, GrantView grantView, List<Student> studentList, List<String> monthList, List<String> errorList) {
        Set<OrgUnit> fouSet = UniUtils.getPropertiesSet(studentList, Student.educationOrgUnit().formativeOrgUnit());


        //есть ли у формирующего подр связь на указанный в представлении GrantView
        MQBuilder builder = new MQBuilder(RelGrantViewOrgUnit.ENTITY_CLASS, "rel")
                .add(MQExpression.in("rel", RelGrantViewOrgUnit.second(), fouSet))
                .add(MQExpression.eq("rel", RelGrantViewOrgUnit.first(), grantView));
        builder.getSelectAliasList().clear();
        builder.addSelect(RelGrantViewOrgUnit.second().fromAlias("rel").s());
        List<OrgUnit> goodList = this.getList(builder);

        Set<OrgUnit> badSet = new HashSet<OrgUnit>(fouSet);
        badSet.removeAll(goodList);

        for (OrgUnit ou : badSet) {
            StringBuilder sb = new StringBuilder()
                    .append(ou.getShortTitle())
                    .append(", не имеет связи со стипендией: ")
                    .append(grantView.getShortTitle() != null ? grantView.getShortTitle() : grantView.getTitle());

            errorList.add(sb.toString());
        }


        //проверка выставлена ли сумма стипендии на каждый месяц для каждого формирующего подр.
        for (String month : monthList) {
            badSet = new HashSet<OrgUnit>(fouSet);

            builder = new MQBuilder(GrantEntity.ENTITY_CLASS, "ge")
                    .add(MQExpression.in("ge", GrantEntity.orgUnit(), fouSet))
                    .add(MQExpression.eq("ge", GrantEntity.view(), grantView))
                    .add(MQExpression.eq("ge", GrantEntity.month(), month));
            builder.getSelectAliasList().clear();
            builder.addSelect(GrantEntity.orgUnit().fromAlias("ge").s());

            badSet.removeAll(this.<OrgUnit>getList(builder));
            for (OrgUnit ou : badSet) {
                StringBuilder sb = new StringBuilder()
                        .append(ou.getShortTitle())
                        .append(", не настроена сумма выплаты на ")
                        .append(month)
                        .append(" для стипендии: ")
                        .append(grantView.getShortTitle() != null ? grantView.getShortTitle() : grantView.getTitle());

                errorList.add(sb.toString());
            }
        }


        //проверка каждого студента и для каждого месяца нет блокирующей GrantView (по матрице стипендий)
        RelTypeGrantView rel = this.get(RelTypeGrantView.class, RelTypeGrantView.view(), grantView);

        //не настроена связь тип выплаты и вид выплаты
        if (rel == null || rel.getGrant() == null || rel.getGrant().getId() == null) {
            StringBuilder sb = new StringBuilder()
                    .append("У вида стипендии/выплаты \"")
                    .append(grantView.getShortTitle() != null ? grantView.getShortTitle() : grantView.getTitle())
                    .append("\" не настроена связь с типом стипендии/выплаты\"");

            errorList.add(sb.toString());

            return errorList.isEmpty();
        }

        Grant grantObject = this.get(Grant.class, rel.getGrant().getId());

        //выгрузка из матрицы стипендий блокирующих Grant
        builder = new MQBuilder(Grant2Grant.ENTITY_CLASS, "g2g")
                .add(MQExpression.eq("g2g", Grant2Grant.first(), grantObject));
        builder.getSelectAliasList().clear();
        builder.addSelect(Grant2Grant.second().fromAlias("g2g").s());
        List<Grant> blockedGrantList = this.getList(builder);

        builder = new MQBuilder(Grant2Grant.ENTITY_CLASS, "g2g")
                .add(MQExpression.eq("g2g", Grant2Grant.second(), grantObject));
        builder.getSelectAliasList().clear();
        builder.addSelect(Grant2Grant.first().fromAlias("g2g").s());
        blockedGrantList.addAll(this.<Grant>getList(builder));

        //получение списка блокирующих GrantView
        builder = new MQBuilder(RelTypeGrantView.ENTITY_CLASS, "rel")
                .add(MQExpression.in("rel", RelTypeGrantView.grant(), blockedGrantList));
        builder.getSelectAliasList().clear();
        builder.addSelect(RelTypeGrantView.view().fromAlias("rel").s());

        Set<GrantView> blockedViewSet = new HashSet<GrantView>(this.<GrantView>getList(builder));

        //получение списка StudentGrantEntity записей которые блокируют начисление стипендий из представления
        builder = new MQBuilder(StudentGrantEntity.ENTITY_CLASS, "sg")
                .add(MQExpression.in("sg", StudentGrantEntity.student(), studentList))
                .add(MQExpression.in("sg", StudentGrantEntity.view(), blockedViewSet))
                .add(MQExpression.in("sg", StudentGrantEntity.month(), monthList))
            /*
			.add(MQExpression.or( //приказ проведен
				MQExpression.eq("sg", StudentGrantEntity.tmp(), Boolean.FALSE),
				MQExpression.isNull("sg", StudentGrantEntity.tmp())
			))
			*/
                .add(MQExpression.eq("sg", StudentGrantEntity.status().code(), StuGrantStatusCodes.CODE_1)); //выплата

        if (representId != null) //это не само наше представление
            builder.add(MQExpression.or(
                    MQExpression.notEq("sg", StudentGrantEntity.listRepresent().id(), representId),
                    MQExpression.notEq("sg", StudentGrantEntity.representation().id(), representId)
            ));

        List<StudentGrantEntity> blockedList = this.getList(builder);
        for (StudentGrantEntity sge : blockedList) {
            StringBuilder sb = new StringBuilder()
                    .append(sge.getStudent().getFio())
                    .append(", имеет блокирующую выплату ")
                    .append(sge.getView().getShortTitle() != null ? sge.getView().getShortTitle() : sge.getView().getTitle())
                    .append(" за ")
                    .append(sge.getMonth())
                    .append(" для стипендии: ")
                    .append(grantView.getShortTitle() != null ? grantView.getShortTitle() : grantView.getTitle());

            if (sge.isTmp() && sge.getListRepresent() != null)
                sb.append(" (представление в работе:")
                        .append(sge.getListRepresent().getTitle())
                        .append("");

            if (sge.isTmp() && sge.getRepresentation() != null)
                sb.append(" (представление в работе:")
                        .append(sge.getRepresentation().getTitle())
                        .append("");

            errorList.add(sb.toString());
        }
		
/*
		//проверка всех студентов что по ним нет представлений(стипендий) в работе
		builder = new MQBuilder(StudentGrantEntity.ENTITY_CLASS, "sg")
			.add(MQExpression.in("sg", StudentGrantEntity.student(), studentList))
			.add(MQExpression.in("sg", StudentGrantEntity.month(), monthList))
			.add(MQExpression.eq("sg", StudentGrantEntity.tmp(), Boolean.TRUE)) //в работе
			.add(MQExpression.eq("sg", StudentGrantEntity.status().code(), StuGrantStatusCodes.CODE_1)); //выплата
		
		if (representId != null) //это не само наше представление
			builder.add(MQExpression.notEq("sg", StudentGrantEntity.listRepresent().id(), representId));
		
		blockedList = this.getList(builder);
		for (StudentGrantEntity sge : blockedList) {
			StringBuilder sb = new StringBuilder()
				.append(sge.getStudent().getFio())
				.append(", имеет не проведенное представление ")
				.append(sge.getListRepresent() != null ? sge.getListRepresent().getTitle() : "")
				.append(" которое начисляет другую выплату за ")
				.append(sge.getMonth());
		
			errorList.add(sb.toString());
		}
*/
        return errorList.isEmpty();
    }

    @Override
    public void saveStudentGrants(ListRepresent represent, GrantView grantView, List<Student> studentList, List<String> monthList, EducationYear educationYear) {
        deleteStudentGrants(represent);

        Set<OrgUnit> fouSet = UniUtils.getPropertiesSet(studentList, Student.educationOrgUnit().formativeOrgUnit());
        for (String month : monthList) {
            MQBuilder builder = new MQBuilder(GrantEntity.ENTITY_CLASS, "ge")
                    .add(MQExpression.in("ge", GrantEntity.orgUnit(), fouSet))
                    .add(MQExpression.eq("ge", GrantEntity.month(), month))
                    .add(MQExpression.eq("ge", GrantEntity.eduYear(), educationYear))
                    .add(MQExpression.eq("ge", GrantEntity.view(), grantView));
            List<GrantEntity> list = this.getList(builder);

            Map<OrgUnit, Double> sumMap = new HashMap<OrgUnit, Double>();
            for (GrantEntity ge : list)
                sumMap.put(ge.getOrgUnit(), ge.getSum());

            Map<Student, Double> map = new HashMap<Student, Double>();
            for (Student student : studentList)
                map.put(student, sumMap.get(student.getEducationOrgUnit().getFormativeOrgUnit()));

            saveStudentGrants(represent, grantView, map, month, educationYear);
        }
    }

    //---------------------------------------------------------------------------------------------------------------------------
    //Без предварительного удаления выплат (для случаев, когда период выплат затрагивает более одного учебного года)
    public void saveStudentGrants(ListRepresent represent, GrantView grantView, List<Student> studentList, List<String> monthList, EducationYear educationYear, boolean deleteGrants) {
    	
    	if(deleteGrants) {
    		deleteStudentGrants(represent);		
    	}

        Set<OrgUnit> fouSet = UniUtils.getPropertiesSet(studentList, Student.educationOrgUnit().formativeOrgUnit());
        for (String month : monthList) {
            MQBuilder builder = new MQBuilder(GrantEntity.ENTITY_CLASS, "ge")
                    .add(MQExpression.in("ge", GrantEntity.orgUnit(), fouSet))
                    .add(MQExpression.eq("ge", GrantEntity.month(), month))
                    .add(MQExpression.eq("ge", GrantEntity.eduYear(), educationYear))
                    .add(MQExpression.eq("ge", GrantEntity.view(), grantView));
            List<GrantEntity> list = this.getList(builder);

            Map<OrgUnit, Double> sumMap = new HashMap<OrgUnit, Double>();
            for (GrantEntity ge : list)
                sumMap.put(ge.getOrgUnit(), ge.getSum());

            Map<Student, Double> map = new HashMap<Student, Double>();
            for (Student student : studentList)
                map.put(student, sumMap.get(student.getEducationOrgUnit().getFormativeOrgUnit()));

            saveStudentGrants(represent, grantView, map, month, educationYear);
        }
    }    
    //---------------------------------------------------------------------------------------------------------------------------

    @Override
    public void saveStudentGrants(ListRepresent represent, GrantView grantView, Map<Student, Double> studentMap, String month, EducationYear educationYear) {
        MQBuilder builder = new MQBuilder(StudentGrantEntity.ENTITY_CLASS, "sge")
                .add(MQExpression.eq("sge", StudentGrantEntity.listRepresent(), represent))
                .add(MQExpression.eq("sge", StudentGrantEntity.month(), month))
                .add(MQExpression.eq("sge", StudentGrantEntity.eduYear(), educationYear));
        List<StudentGrantEntity> sgeList = this.getList(builder);
        for (StudentGrantEntity sge : sgeList)
            this.delete(sge);

        StuGrantStatus grantStatus = this.getCatalogItem(StuGrantStatus.class, StuGrantStatusCodes.CODE_1);

        for (Student student : studentMap.keySet()) {
            StudentGrantEntity sge = new StudentGrantEntity();
            sge.setStudent(student);
            sge.setMonth(month);
            sge.setEduYear(educationYear);
            sge.setListRepresent(represent);
            sge.setRepresentation(null);
            sge.setView(grantView);
            sge.setStatus(grantStatus);
            sge.setTmp(Boolean.TRUE);

            sge.setSum(studentMap.get(student));

            this.save(sge);
        }

    }

    @Override
    public void commitStudentGrants(ListRepresent represent, ListOrder order) {
        List<StudentGrantEntity> list = getList(StudentGrantEntity.class, StudentGrantEntity.listRepresent(), represent);
        commit(list, order.getNumber(), order.getCommitDate());
        saveGrantHistory(list, represent, RepresentGrantTypeCodes.DESTINATION);
    }

    @Override
    public void rollbackStudentGrants(ListRepresent represent, ListOrder order) {
        List<StudentGrantEntity> list = getList(StudentGrantEntity.class, StudentGrantEntity.listRepresent(), represent);
        rollback(list);
        deleteGrantHistory(represent);
    }

    @Override
    public void deleteStudentGrants(ListRepresent represent) {
        List<StudentGrantEntity> sgeList = this.getList(StudentGrantEntity.class, StudentGrantEntity.listRepresent(), represent);
        for (StudentGrantEntity sge : sgeList)
            this.delete(sge);
    }

    @Override
    public void saveStudentGrants(Representation represent, GrantView grantView, Student student, List<String> monthList, EducationYear educationYear) {
        deleteStudentGrants(represent);

        for (String month : monthList) {
            MQBuilder builder = new MQBuilder(GrantEntity.ENTITY_CLASS, "ge")
                    .add(MQExpression.eq("ge", GrantEntity.orgUnit(), student.getEducationOrgUnit().getFormativeOrgUnit()))
                    .add(MQExpression.eq("ge", GrantEntity.month(), month))
                    .add(MQExpression.eq("ge", GrantEntity.eduYear(), educationYear))
                    .add(MQExpression.eq("ge", GrantEntity.view(), grantView));
            GrantEntity ge = (GrantEntity) builder.uniqueResult(getSession());

            saveStudentGrants(represent, grantView, student, ge.getSum(), month, educationYear);
        }
    }
    
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //Без предварительного удаления выплат (для случаев, когда период выплат затрагивает более одного учебного года)
    public void saveStudentGrants(Representation represent, GrantView grantView, Student student, List<String> monthList, EducationYear educationYear, boolean deleteGrants) {
    	
    	if(deleteGrants) {
    		deleteStudentGrants(represent);
    	}

        for (String month : monthList) {
            MQBuilder builder = new MQBuilder(GrantEntity.ENTITY_CLASS, "ge")
                    .add(MQExpression.eq("ge", GrantEntity.orgUnit(), student.getEducationOrgUnit().getFormativeOrgUnit()))
                    .add(MQExpression.eq("ge", GrantEntity.month(), month))
                    .add(MQExpression.eq("ge", GrantEntity.eduYear(), educationYear))
                    .add(MQExpression.eq("ge", GrantEntity.view(), grantView));
            GrantEntity ge = (GrantEntity) builder.uniqueResult(getSession());

            saveStudentGrants(represent, grantView, student, ge.getSum(), month, educationYear);
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    @Override
    public void saveStudentGrants(Representation represent, GrantView grantView, Student student, double sum, String month, EducationYear educationYear) {
        StuGrantStatus grantStatus = this.getCatalogItem(StuGrantStatus.class, StuGrantStatusCodes.CODE_1);

        StudentGrantEntity sge = new StudentGrantEntity();
        sge.setStudent(student);
        sge.setMonth(month);
        sge.setEduYear(educationYear);
        sge.setRepresentation(represent);
        sge.setListRepresent(null);
        sge.setView(grantView);
        sge.setStatus(grantStatus);
        sge.setTmp(Boolean.TRUE);

        sge.setSum(sum);

        this.save(sge);
    }

    @Override
    public void commitStudentGrants(Representation represent, DocumentOrder order) {
        List<StudentGrantEntity> list = getList(StudentGrantEntity.class, StudentGrantEntity.representation(), represent);
        commit(list, order.getNumber(), order.getCommitDate());
        saveGrantHistory(list, represent, RepresentGrantTypeCodes.DESTINATION);
    }

    public void rollback(List<StudentGrantEntity> list) {
        for (StudentGrantEntity sge : list) {
            sge.setTmp(Boolean.TRUE);
            sge.setOrderNumber(null);
            sge.setOrderDate(null);

            update(sge);
        }
    }

    public void commit(List<StudentGrantEntity> list, String orderNumber, Date orderDate) {
        for (StudentGrantEntity sge : list) {
            sge.setTmp(Boolean.FALSE);
            sge.setOrderNumber(orderNumber);
            sge.setOrderDate(orderDate);

            update(sge);
        }
    }

    @Override
    public void saveGrantHistory(List<StudentGrantEntity> list, IAbstractRepresentation represent, String typeCode) {

        RepresentGrantType type = get(RepresentGrantType.class, RepresentGrantType.code(), typeCode);
        Date current = new Date();

        for (StudentGrantEntity sge : list) {
            StudentGrantEntityHistory history = new StudentGrantEntityHistory();
            history.setStudentGrantEntity(sge);
            history.setRepresentation(represent);
            history.setRepresentGrantType(type);
            history.setCreateDate(current);

            getSession().saveOrUpdate(history);

        }
    }

    @Override
    public void deleteGrantHistory(IAbstractRepresentation represent) {
        new DQLDeleteBuilder(StudentGrantEntityHistory.class)
                .where(DQLExpressions.eqValue(DQLExpressions.property(StudentGrantEntityHistory.representation()), represent))
                .createStatement(getSession()).execute();
    }

    @Override
    public void rollbackStudentGrants(Representation represent, DocumentOrder order) {
        List<StudentGrantEntity> list = getList(StudentGrantEntity.class, StudentGrantEntity.representation(), represent);
        rollback(list);
        deleteGrantHistory(represent);
    }

    @Override
    public void deleteStudentGrants(Representation represent) {
        List<StudentGrantEntity> sgeList = this.getList(StudentGrantEntity.class, StudentGrantEntity.representation(), represent);
        for (StudentGrantEntity sge : sgeList)
            this.delete(sge);
    }

    @Override
    //можно ли назначить стипендию студенту
    public boolean canAddGrant(GrantView newGrantView, Student student, String month) {
        RelTypeGrantView newRel = this.getUnique(RelTypeGrantView.class, RelTypeGrantView.view().s(), newGrantView);

        //настройки нет, а значит и в матрице нет
        if (newRel == null)
            return true;

        MQBuilder builder = new MQBuilder(StudentGrantEntity.ENTITY_CLASS, "g")
                .add(MQExpression.eq("g", StudentGrantEntity.student(), student))
                .add(MQExpression.eq("g", StudentGrantEntity.status().code(), StuGrantStatusCodes.CODE_1)) //к выплате
                .add(MQExpression.eq("g", StudentGrantEntity.month(), month));

        builder.getSelectAliasList().clear();
        builder.addSelect(StudentGrantEntity.view().fromAlias("g").s());
        List<GrantView> existedList = builder.getResultList(getSession());

        for (GrantView existedView : existedList) {
            RelTypeGrantView existedRel = this.getUnique(RelTypeGrantView.class, RelTypeGrantView.view().s(), existedView);
            if (existedRel == null)
                continue;

            if (!canAddGrant(newRel.getGrant(), existedRel.getGrant()))
                return false;
        }

        return true;
    }

    @Override
    //матрица стипендий
    public boolean canAddGrant(Grant grant1, Grant grant2) {
        MQBuilder builder = new MQBuilder(Grant2Grant.ENTITY_CLASS, "rel")
                .add(MQExpression.or(
                        MQExpression.and(
                                MQExpression.eq("rel", Grant2Grant.first(), grant1),
                                MQExpression.eq("rel", Grant2Grant.second(), grant2)
                        ),
                        MQExpression.and(
                                MQExpression.eq("rel", Grant2Grant.first(), grant2),
                                MQExpression.eq("rel", Grant2Grant.second(), grant1)
                        )
                ));

        if (builder.getResultCount(getSession()) > 0)
            return false;

        return true;
    }

}
