package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.fias.base.entity.Address;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudentrmc.entity.PersonNARFU;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Доп атрибуты студента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PersonNARFUGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.PersonNARFU";
    public static final String ENTITY_NAME = "personNARFU";
    public static final int VERSION_HASH = 192388894;
    private static IEntityMeta ENTITY_META;

    public static final String L_PERSON = "person";
    public static final String P_NEED_SOCIAL_PAYMENT = "needSocialPayment";
    public static final String P_NEED_DORMITORY_ON_RECEIPT_PERIOD = "needDormitoryOnReceiptPeriod";
    public static final String P_DOCUMENT_EXPIRED_DATE = "documentExpiredDate";
    public static final String P_AVERAGE_FAMILY_CAPITAL = "averageFamilyCapital";
    public static final String L_ADDRESS_TEMP_REGISTRATION = "addressTempRegistration";
    public static final String P_ADDRESS_TEMP_REGISTRATION_START_DATE = "addressTempRegistrationStartDate";
    public static final String P_ADDRESS_TEMP_REGISTRATION_END_DATE = "addressTempRegistrationEndDate";
    public static final String P_ADDRESS_TEMP_REGISTRATION_INFO = "addressTempRegistrationInfo";
    public static final String P_ADDRESS_REGISTRATION_INFO = "addressRegistrationInfo";
    public static final String P_ADDRESS_INFO = "addressInfo";
    public static final String P_RESIDENT = "resident";

    private Person _person;     // Персона
    private boolean _needSocialPayment;     // Представитель малочисленных народов Севера
    private boolean _needDormitoryOnReceiptPeriod;     // Нуждается в общежитии на период поступления
    private Date _documentExpiredDate;     // Срок действия документа
    private Integer _averageFamilyCapital;     // Среднедушевой доход семьи
    private Address _addressTempRegistration;     // Адрес временной регистрации
    private Date _addressTempRegistrationStartDate;     // Дата начала временной регистрации
    private Date _addressTempRegistrationEndDate;     // Дата окончания временной регистрации
    private String _addressTempRegistrationInfo;     // Доп. информация к адресу временной регистрации
    private String _addressRegistrationInfo;     // Доп. информация к адресу регистрации
    private String _addressInfo;     // Доп. информация к адресу проживания
    private Boolean _resident;     // Нуждается в общежитии на период поступления

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Персона. Свойство должно быть уникальным.
     */
    public Person getPerson()
    {
        return _person;
    }

    /**
     * @param person Персона. Свойство должно быть уникальным.
     */
    public void setPerson(Person person)
    {
        dirty(_person, person);
        _person = person;
    }

    /**
     * @return Представитель малочисленных народов Севера. Свойство не может быть null.
     */
    @NotNull
    public boolean isNeedSocialPayment()
    {
        return _needSocialPayment;
    }

    /**
     * @param needSocialPayment Представитель малочисленных народов Севера. Свойство не может быть null.
     */
    public void setNeedSocialPayment(boolean needSocialPayment)
    {
        dirty(_needSocialPayment, needSocialPayment);
        _needSocialPayment = needSocialPayment;
    }

    /**
     * @return Нуждается в общежитии на период поступления. Свойство не может быть null.
     */
    @NotNull
    public boolean isNeedDormitoryOnReceiptPeriod()
    {
        return _needDormitoryOnReceiptPeriod;
    }

    /**
     * @param needDormitoryOnReceiptPeriod Нуждается в общежитии на период поступления. Свойство не может быть null.
     */
    public void setNeedDormitoryOnReceiptPeriod(boolean needDormitoryOnReceiptPeriod)
    {
        dirty(_needDormitoryOnReceiptPeriod, needDormitoryOnReceiptPeriod);
        _needDormitoryOnReceiptPeriod = needDormitoryOnReceiptPeriod;
    }

    /**
     * @return Срок действия документа.
     */
    public Date getDocumentExpiredDate()
    {
        return _documentExpiredDate;
    }

    /**
     * @param documentExpiredDate Срок действия документа.
     */
    public void setDocumentExpiredDate(Date documentExpiredDate)
    {
        dirty(_documentExpiredDate, documentExpiredDate);
        _documentExpiredDate = documentExpiredDate;
    }

    /**
     * @return Среднедушевой доход семьи.
     */
    public Integer getAverageFamilyCapital()
    {
        return _averageFamilyCapital;
    }

    /**
     * @param averageFamilyCapital Среднедушевой доход семьи.
     */
    public void setAverageFamilyCapital(Integer averageFamilyCapital)
    {
        dirty(_averageFamilyCapital, averageFamilyCapital);
        _averageFamilyCapital = averageFamilyCapital;
    }

    /**
     * @return Адрес временной регистрации.
     */
    public Address getAddressTempRegistration()
    {
        return _addressTempRegistration;
    }

    /**
     * @param addressTempRegistration Адрес временной регистрации.
     */
    public void setAddressTempRegistration(Address addressTempRegistration)
    {
        dirty(_addressTempRegistration, addressTempRegistration);
        _addressTempRegistration = addressTempRegistration;
    }

    /**
     * @return Дата начала временной регистрации.
     */
    public Date getAddressTempRegistrationStartDate()
    {
        return _addressTempRegistrationStartDate;
    }

    /**
     * @param addressTempRegistrationStartDate Дата начала временной регистрации.
     */
    public void setAddressTempRegistrationStartDate(Date addressTempRegistrationStartDate)
    {
        dirty(_addressTempRegistrationStartDate, addressTempRegistrationStartDate);
        _addressTempRegistrationStartDate = addressTempRegistrationStartDate;
    }

    /**
     * @return Дата окончания временной регистрации.
     */
    public Date getAddressTempRegistrationEndDate()
    {
        return _addressTempRegistrationEndDate;
    }

    /**
     * @param addressTempRegistrationEndDate Дата окончания временной регистрации.
     */
    public void setAddressTempRegistrationEndDate(Date addressTempRegistrationEndDate)
    {
        dirty(_addressTempRegistrationEndDate, addressTempRegistrationEndDate);
        _addressTempRegistrationEndDate = addressTempRegistrationEndDate;
    }

    /**
     * @return Доп. информация к адресу временной регистрации.
     */
    @Length(max=255)
    public String getAddressTempRegistrationInfo()
    {
        return _addressTempRegistrationInfo;
    }

    /**
     * @param addressTempRegistrationInfo Доп. информация к адресу временной регистрации.
     */
    public void setAddressTempRegistrationInfo(String addressTempRegistrationInfo)
    {
        dirty(_addressTempRegistrationInfo, addressTempRegistrationInfo);
        _addressTempRegistrationInfo = addressTempRegistrationInfo;
    }

    /**
     * @return Доп. информация к адресу регистрации.
     */
    @Length(max=255)
    public String getAddressRegistrationInfo()
    {
        return _addressRegistrationInfo;
    }

    /**
     * @param addressRegistrationInfo Доп. информация к адресу регистрации.
     */
    public void setAddressRegistrationInfo(String addressRegistrationInfo)
    {
        dirty(_addressRegistrationInfo, addressRegistrationInfo);
        _addressRegistrationInfo = addressRegistrationInfo;
    }

    /**
     * @return Доп. информация к адресу проживания.
     */
    @Length(max=255)
    public String getAddressInfo()
    {
        return _addressInfo;
    }

    /**
     * @param addressInfo Доп. информация к адресу проживания.
     */
    public void setAddressInfo(String addressInfo)
    {
        dirty(_addressInfo, addressInfo);
        _addressInfo = addressInfo;
    }

    /**
     * @return Нуждается в общежитии на период поступления.
     */
    public Boolean getResident()
    {
        return _resident;
    }

    /**
     * @param resident Нуждается в общежитии на период поступления.
     */
    public void setResident(Boolean resident)
    {
        dirty(_resident, resident);
        _resident = resident;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PersonNARFUGen)
        {
            setPerson(((PersonNARFU)another).getPerson());
            setNeedSocialPayment(((PersonNARFU)another).isNeedSocialPayment());
            setNeedDormitoryOnReceiptPeriod(((PersonNARFU)another).isNeedDormitoryOnReceiptPeriod());
            setDocumentExpiredDate(((PersonNARFU)another).getDocumentExpiredDate());
            setAverageFamilyCapital(((PersonNARFU)another).getAverageFamilyCapital());
            setAddressTempRegistration(((PersonNARFU)another).getAddressTempRegistration());
            setAddressTempRegistrationStartDate(((PersonNARFU)another).getAddressTempRegistrationStartDate());
            setAddressTempRegistrationEndDate(((PersonNARFU)another).getAddressTempRegistrationEndDate());
            setAddressTempRegistrationInfo(((PersonNARFU)another).getAddressTempRegistrationInfo());
            setAddressRegistrationInfo(((PersonNARFU)another).getAddressRegistrationInfo());
            setAddressInfo(((PersonNARFU)another).getAddressInfo());
            setResident(((PersonNARFU)another).getResident());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PersonNARFUGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PersonNARFU.class;
        }

        public T newInstance()
        {
            return (T) new PersonNARFU();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "person":
                    return obj.getPerson();
                case "needSocialPayment":
                    return obj.isNeedSocialPayment();
                case "needDormitoryOnReceiptPeriod":
                    return obj.isNeedDormitoryOnReceiptPeriod();
                case "documentExpiredDate":
                    return obj.getDocumentExpiredDate();
                case "averageFamilyCapital":
                    return obj.getAverageFamilyCapital();
                case "addressTempRegistration":
                    return obj.getAddressTempRegistration();
                case "addressTempRegistrationStartDate":
                    return obj.getAddressTempRegistrationStartDate();
                case "addressTempRegistrationEndDate":
                    return obj.getAddressTempRegistrationEndDate();
                case "addressTempRegistrationInfo":
                    return obj.getAddressTempRegistrationInfo();
                case "addressRegistrationInfo":
                    return obj.getAddressRegistrationInfo();
                case "addressInfo":
                    return obj.getAddressInfo();
                case "resident":
                    return obj.getResident();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "person":
                    obj.setPerson((Person) value);
                    return;
                case "needSocialPayment":
                    obj.setNeedSocialPayment((Boolean) value);
                    return;
                case "needDormitoryOnReceiptPeriod":
                    obj.setNeedDormitoryOnReceiptPeriod((Boolean) value);
                    return;
                case "documentExpiredDate":
                    obj.setDocumentExpiredDate((Date) value);
                    return;
                case "averageFamilyCapital":
                    obj.setAverageFamilyCapital((Integer) value);
                    return;
                case "addressTempRegistration":
                    obj.setAddressTempRegistration((Address) value);
                    return;
                case "addressTempRegistrationStartDate":
                    obj.setAddressTempRegistrationStartDate((Date) value);
                    return;
                case "addressTempRegistrationEndDate":
                    obj.setAddressTempRegistrationEndDate((Date) value);
                    return;
                case "addressTempRegistrationInfo":
                    obj.setAddressTempRegistrationInfo((String) value);
                    return;
                case "addressRegistrationInfo":
                    obj.setAddressRegistrationInfo((String) value);
                    return;
                case "addressInfo":
                    obj.setAddressInfo((String) value);
                    return;
                case "resident":
                    obj.setResident((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "person":
                        return true;
                case "needSocialPayment":
                        return true;
                case "needDormitoryOnReceiptPeriod":
                        return true;
                case "documentExpiredDate":
                        return true;
                case "averageFamilyCapital":
                        return true;
                case "addressTempRegistration":
                        return true;
                case "addressTempRegistrationStartDate":
                        return true;
                case "addressTempRegistrationEndDate":
                        return true;
                case "addressTempRegistrationInfo":
                        return true;
                case "addressRegistrationInfo":
                        return true;
                case "addressInfo":
                        return true;
                case "resident":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "person":
                    return true;
                case "needSocialPayment":
                    return true;
                case "needDormitoryOnReceiptPeriod":
                    return true;
                case "documentExpiredDate":
                    return true;
                case "averageFamilyCapital":
                    return true;
                case "addressTempRegistration":
                    return true;
                case "addressTempRegistrationStartDate":
                    return true;
                case "addressTempRegistrationEndDate":
                    return true;
                case "addressTempRegistrationInfo":
                    return true;
                case "addressRegistrationInfo":
                    return true;
                case "addressInfo":
                    return true;
                case "resident":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "person":
                    return Person.class;
                case "needSocialPayment":
                    return Boolean.class;
                case "needDormitoryOnReceiptPeriod":
                    return Boolean.class;
                case "documentExpiredDate":
                    return Date.class;
                case "averageFamilyCapital":
                    return Integer.class;
                case "addressTempRegistration":
                    return Address.class;
                case "addressTempRegistrationStartDate":
                    return Date.class;
                case "addressTempRegistrationEndDate":
                    return Date.class;
                case "addressTempRegistrationInfo":
                    return String.class;
                case "addressRegistrationInfo":
                    return String.class;
                case "addressInfo":
                    return String.class;
                case "resident":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PersonNARFU> _dslPath = new Path<PersonNARFU>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PersonNARFU");
    }
            

    /**
     * @return Персона. Свойство должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.PersonNARFU#getPerson()
     */
    public static Person.Path<Person> person()
    {
        return _dslPath.person();
    }

    /**
     * @return Представитель малочисленных народов Севера. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.PersonNARFU#isNeedSocialPayment()
     */
    public static PropertyPath<Boolean> needSocialPayment()
    {
        return _dslPath.needSocialPayment();
    }

    /**
     * @return Нуждается в общежитии на период поступления. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.PersonNARFU#isNeedDormitoryOnReceiptPeriod()
     */
    public static PropertyPath<Boolean> needDormitoryOnReceiptPeriod()
    {
        return _dslPath.needDormitoryOnReceiptPeriod();
    }

    /**
     * @return Срок действия документа.
     * @see ru.tandemservice.movestudentrmc.entity.PersonNARFU#getDocumentExpiredDate()
     */
    public static PropertyPath<Date> documentExpiredDate()
    {
        return _dslPath.documentExpiredDate();
    }

    /**
     * @return Среднедушевой доход семьи.
     * @see ru.tandemservice.movestudentrmc.entity.PersonNARFU#getAverageFamilyCapital()
     */
    public static PropertyPath<Integer> averageFamilyCapital()
    {
        return _dslPath.averageFamilyCapital();
    }

    /**
     * @return Адрес временной регистрации.
     * @see ru.tandemservice.movestudentrmc.entity.PersonNARFU#getAddressTempRegistration()
     */
    public static Address.Path<Address> addressTempRegistration()
    {
        return _dslPath.addressTempRegistration();
    }

    /**
     * @return Дата начала временной регистрации.
     * @see ru.tandemservice.movestudentrmc.entity.PersonNARFU#getAddressTempRegistrationStartDate()
     */
    public static PropertyPath<Date> addressTempRegistrationStartDate()
    {
        return _dslPath.addressTempRegistrationStartDate();
    }

    /**
     * @return Дата окончания временной регистрации.
     * @see ru.tandemservice.movestudentrmc.entity.PersonNARFU#getAddressTempRegistrationEndDate()
     */
    public static PropertyPath<Date> addressTempRegistrationEndDate()
    {
        return _dslPath.addressTempRegistrationEndDate();
    }

    /**
     * @return Доп. информация к адресу временной регистрации.
     * @see ru.tandemservice.movestudentrmc.entity.PersonNARFU#getAddressTempRegistrationInfo()
     */
    public static PropertyPath<String> addressTempRegistrationInfo()
    {
        return _dslPath.addressTempRegistrationInfo();
    }

    /**
     * @return Доп. информация к адресу регистрации.
     * @see ru.tandemservice.movestudentrmc.entity.PersonNARFU#getAddressRegistrationInfo()
     */
    public static PropertyPath<String> addressRegistrationInfo()
    {
        return _dslPath.addressRegistrationInfo();
    }

    /**
     * @return Доп. информация к адресу проживания.
     * @see ru.tandemservice.movestudentrmc.entity.PersonNARFU#getAddressInfo()
     */
    public static PropertyPath<String> addressInfo()
    {
        return _dslPath.addressInfo();
    }

    /**
     * @return Нуждается в общежитии на период поступления.
     * @see ru.tandemservice.movestudentrmc.entity.PersonNARFU#getResident()
     */
    public static PropertyPath<Boolean> resident()
    {
        return _dslPath.resident();
    }

    public static class Path<E extends PersonNARFU> extends EntityPath<E>
    {
        private Person.Path<Person> _person;
        private PropertyPath<Boolean> _needSocialPayment;
        private PropertyPath<Boolean> _needDormitoryOnReceiptPeriod;
        private PropertyPath<Date> _documentExpiredDate;
        private PropertyPath<Integer> _averageFamilyCapital;
        private Address.Path<Address> _addressTempRegistration;
        private PropertyPath<Date> _addressTempRegistrationStartDate;
        private PropertyPath<Date> _addressTempRegistrationEndDate;
        private PropertyPath<String> _addressTempRegistrationInfo;
        private PropertyPath<String> _addressRegistrationInfo;
        private PropertyPath<String> _addressInfo;
        private PropertyPath<Boolean> _resident;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Персона. Свойство должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.PersonNARFU#getPerson()
     */
        public Person.Path<Person> person()
        {
            if(_person == null )
                _person = new Person.Path<Person>(L_PERSON, this);
            return _person;
        }

    /**
     * @return Представитель малочисленных народов Севера. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.PersonNARFU#isNeedSocialPayment()
     */
        public PropertyPath<Boolean> needSocialPayment()
        {
            if(_needSocialPayment == null )
                _needSocialPayment = new PropertyPath<Boolean>(PersonNARFUGen.P_NEED_SOCIAL_PAYMENT, this);
            return _needSocialPayment;
        }

    /**
     * @return Нуждается в общежитии на период поступления. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.PersonNARFU#isNeedDormitoryOnReceiptPeriod()
     */
        public PropertyPath<Boolean> needDormitoryOnReceiptPeriod()
        {
            if(_needDormitoryOnReceiptPeriod == null )
                _needDormitoryOnReceiptPeriod = new PropertyPath<Boolean>(PersonNARFUGen.P_NEED_DORMITORY_ON_RECEIPT_PERIOD, this);
            return _needDormitoryOnReceiptPeriod;
        }

    /**
     * @return Срок действия документа.
     * @see ru.tandemservice.movestudentrmc.entity.PersonNARFU#getDocumentExpiredDate()
     */
        public PropertyPath<Date> documentExpiredDate()
        {
            if(_documentExpiredDate == null )
                _documentExpiredDate = new PropertyPath<Date>(PersonNARFUGen.P_DOCUMENT_EXPIRED_DATE, this);
            return _documentExpiredDate;
        }

    /**
     * @return Среднедушевой доход семьи.
     * @see ru.tandemservice.movestudentrmc.entity.PersonNARFU#getAverageFamilyCapital()
     */
        public PropertyPath<Integer> averageFamilyCapital()
        {
            if(_averageFamilyCapital == null )
                _averageFamilyCapital = new PropertyPath<Integer>(PersonNARFUGen.P_AVERAGE_FAMILY_CAPITAL, this);
            return _averageFamilyCapital;
        }

    /**
     * @return Адрес временной регистрации.
     * @see ru.tandemservice.movestudentrmc.entity.PersonNARFU#getAddressTempRegistration()
     */
        public Address.Path<Address> addressTempRegistration()
        {
            if(_addressTempRegistration == null )
                _addressTempRegistration = new Address.Path<Address>(L_ADDRESS_TEMP_REGISTRATION, this);
            return _addressTempRegistration;
        }

    /**
     * @return Дата начала временной регистрации.
     * @see ru.tandemservice.movestudentrmc.entity.PersonNARFU#getAddressTempRegistrationStartDate()
     */
        public PropertyPath<Date> addressTempRegistrationStartDate()
        {
            if(_addressTempRegistrationStartDate == null )
                _addressTempRegistrationStartDate = new PropertyPath<Date>(PersonNARFUGen.P_ADDRESS_TEMP_REGISTRATION_START_DATE, this);
            return _addressTempRegistrationStartDate;
        }

    /**
     * @return Дата окончания временной регистрации.
     * @see ru.tandemservice.movestudentrmc.entity.PersonNARFU#getAddressTempRegistrationEndDate()
     */
        public PropertyPath<Date> addressTempRegistrationEndDate()
        {
            if(_addressTempRegistrationEndDate == null )
                _addressTempRegistrationEndDate = new PropertyPath<Date>(PersonNARFUGen.P_ADDRESS_TEMP_REGISTRATION_END_DATE, this);
            return _addressTempRegistrationEndDate;
        }

    /**
     * @return Доп. информация к адресу временной регистрации.
     * @see ru.tandemservice.movestudentrmc.entity.PersonNARFU#getAddressTempRegistrationInfo()
     */
        public PropertyPath<String> addressTempRegistrationInfo()
        {
            if(_addressTempRegistrationInfo == null )
                _addressTempRegistrationInfo = new PropertyPath<String>(PersonNARFUGen.P_ADDRESS_TEMP_REGISTRATION_INFO, this);
            return _addressTempRegistrationInfo;
        }

    /**
     * @return Доп. информация к адресу регистрации.
     * @see ru.tandemservice.movestudentrmc.entity.PersonNARFU#getAddressRegistrationInfo()
     */
        public PropertyPath<String> addressRegistrationInfo()
        {
            if(_addressRegistrationInfo == null )
                _addressRegistrationInfo = new PropertyPath<String>(PersonNARFUGen.P_ADDRESS_REGISTRATION_INFO, this);
            return _addressRegistrationInfo;
        }

    /**
     * @return Доп. информация к адресу проживания.
     * @see ru.tandemservice.movestudentrmc.entity.PersonNARFU#getAddressInfo()
     */
        public PropertyPath<String> addressInfo()
        {
            if(_addressInfo == null )
                _addressInfo = new PropertyPath<String>(PersonNARFUGen.P_ADDRESS_INFO, this);
            return _addressInfo;
        }

    /**
     * @return Нуждается в общежитии на период поступления.
     * @see ru.tandemservice.movestudentrmc.entity.PersonNARFU#getResident()
     */
        public PropertyPath<Boolean> resident()
        {
            if(_resident == null )
                _resident = new PropertyPath<Boolean>(PersonNARFUGen.P_RESIDENT, this);
            return _resident;
        }

        public Class getEntityClass()
        {
            return PersonNARFU.class;
        }

        public String getEntityName()
        {
            return "personNARFU";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
