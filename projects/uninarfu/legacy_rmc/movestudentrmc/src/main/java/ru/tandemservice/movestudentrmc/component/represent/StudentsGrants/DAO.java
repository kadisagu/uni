package ru.tandemservice.movestudentrmc.component.represent.StudentsGrants;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntity;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.List;
import java.util.Map;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void update(Model model) {
        List<DataWrapper> itemList = model.getList();

        UserContext.getInstance().setAttribute("popupGrant", true);
        Map<Long, Object> grantMap = ((IValueMapHolder) model.getDataSource().getColumn("grant")).getValueMap();

        for (DataWrapper w : itemList) {
            StudentGrantEntity sGrantEntity = (StudentGrantEntity) w.getProperty("grant");
            Student student = (Student) w.getProperty("student");
            double sum = 0.0;

            if (null != grantMap.get(student.getId()))
                try {
                    sum = Double.parseDouble((String) grantMap.get(student.getId()));
                }
                catch (NumberFormatException e) {
                    throw new ApplicationException("Значение " + (String) grantMap.get(student.getId()) + " в поле \"Сумма выплаты\" для студента(студентки) " + student.getPerson().getIdentityCard().getFullFio() + " введено в неправильном формате");
                }

            sGrantEntity.setSum(sum);
            //grantMap.put(student.getId(),  String.valueOf(sGrantEntity.getSum()));
        }

        getSession().clear();
    }

    @Override
    public void prepareListDataSource(Model model) {
        Map<Long, Object> grantMap = ((IValueMapHolder) model.getDataSource().getColumn("grant")).getValueMap();

        List<DataWrapper> itemList = model.getList();

        for (DataWrapper w : itemList) {
            StudentGrantEntity sGrantEntity = (StudentGrantEntity) w.getProperty("grant");
            Student student = (Student) w.getProperty("student");
            double sum = 0.0;

            if (null != grantMap.get(student.getId()))
                try {
                    sum = Double.parseDouble((String) grantMap.get(student.getId()));
                }
                catch (NumberFormatException e) {
                    sum = sGrantEntity.getSum();
                }

            sGrantEntity.setSum(sum);

            //grantMap.put(student.getId(),  String.valueOf(sGrantEntity.getSum()));
        }

        for (DataWrapper dataW : model.getList()) {
            Student student = (Student) dataW.getProperty("student");
            StudentGrantEntity grantEntity = (StudentGrantEntity) dataW.getProperty("grant");
            if (grantEntity != null) {
                grantMap.put(student.getId(), String.valueOf(grantEntity.getSum()));
            }
            else {
                grantMap.put(student.getId(), "");
            }
        }

        ((BlockColumn) model.getDataSource().getColumn("grant")).setValueMap(grantMap);

        model.setDataSource(model.getDataSource());

        UniUtils.createPage(model.getDataSource(), model.getList());
    }
}
