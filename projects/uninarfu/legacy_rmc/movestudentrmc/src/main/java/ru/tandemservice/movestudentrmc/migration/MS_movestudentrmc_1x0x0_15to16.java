package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Map;


public class MS_movestudentrmc_1x0x0_15to16 extends IndependentMigrationScript {
    private Map<String, Object> map = new UniMap()
            .add("representationtype3", "transfer")
            .add("representationtype4", "excludeOut")
            .add("representationtype5", "weekendOut")
            .add("representationtype6", "weekendProlong")
            .add("representationtype7", "recertificationTraining")
            .add("representationtype8", "courseTransfer")
            .add("representationtype9", "diplomAndExclude")
            .add("representationtype10", "changeName")
            .add("representationtype11", "extensionSession")
            .add("representationtype12", "personWorkPlan")
            .add("representationtype13", "admissionAttendClasses")
            .add("representationtype17", "enrollmentTransfer");

    @Override
    public void run(DBTool tool) throws Exception {
        IEntityMeta meta = EntityRuntime.getMeta(RepresentationType.class);

        if (!tool.tableExists(meta.getTableName()))
            return;

        PreparedStatement pst = create(tool, "select * from " + meta.getTableName() + " where code_p=?");

        pst.setString(1, "excludeOut");
        ResultSet rs = pst.executeQuery();
        if (rs.next()) {
            doCorrection(tool);
            doChangeCodes(tool);
        }
        else
            doChangeCodes(tool);

        rs.close();
    }

    private void doCorrection(DBTool tool) throws Exception {
        IEntityMeta meta = EntityRuntime.getMeta(RepresentationType.class);
        IEntityMeta meta2 = EntityRuntime.getMeta(PrintTemplate.class);

        //переключаем печатные шаблоны на старые записи
        PreparedStatement pst = tool.getConnection().prepareStatement("select id from " + meta.getTableName() + " where code_p=?");
        PreparedStatement pst2 = tool.getConnection().prepareStatement("update " + meta2.getTableName() + " set TYPE_ID=? where TYPE_ID=?");

        for (Map.Entry<String, Object> entry : map.entrySet()) {
            pst.setString(1, entry.getValue().toString());
            ResultSet rs = pst.executeQuery();

            if (rs.next()) {
                Long newId = rs.getLong(1);

                pst.setString(1, entry.getKey());
                ResultSet rs2 = pst.executeQuery();
                rs2.next();
                Long oldId = rs2.getLong(1);
                rs2.close();

                pst2.setLong(1, oldId);
                pst2.setLong(2, newId);
                pst2.executeUpdate();
            }

            rs.close();
        }

        pst.close();
        pst2.close();


        //грохаем левые записи
        pst = create(tool, "delete from " + meta.getTableName() + " where code_p=?");
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            pst.setString(1, entry.getValue().toString());
            pst.executeUpdate();
        }
        pst.close();
    }

    private void doChangeCodes(DBTool tool) throws Exception {
        IEntityMeta meta = EntityRuntime.getMeta(RepresentationType.class);

        PreparedStatement pst = create(tool, "update " + meta.getTableName() + " set code_p=? where code_p=?");

        for (Map.Entry<String, Object> entry : map.entrySet()) {
            pst.setString(1, entry.getValue().toString());
            pst.setString(2, entry.getKey());
            pst.executeUpdate();
        }
        pst.close();
    }

    private PreparedStatement create(DBTool tool, String query) throws Exception {
        return tool.getConnection().prepareStatement(query);
    }
}