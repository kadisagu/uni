package ru.tandemservice.movestudentrmc.base.bo.ListRepresentDiplomAndExclude.ui.Edit;


import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.bo.Person.ui.View.PersonView;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ListRepresentBaseManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.StudentDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.StudentSelectedDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentDiplomAndExclude.ListRepresentDiplomAndExcludeManager;
import ru.tandemservice.movestudentrmc.dao.CheckStateUtil;
import ru.tandemservice.movestudentrmc.docord.util.MoveStudentUtil;
import ru.tandemservice.movestudentrmc.entity.DocListRepresentBasics;
import ru.tandemservice.movestudentrmc.entity.ListRepresentDiplomAndExclude;
import ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents;
import ru.tandemservice.movestudentrmc.entity.catalog.MovestudentExtractStates;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationBasement;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentExtractStatesCodes;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.*;


@State({
        @Bind(key = ListRepresentDiplomAndExcludeEditUI.LIST_REPRESENT_ID, binding = ListRepresentDiplomAndExcludeEditUI.LIST_REPRESENT_ID),
        @Bind(key = "typeRepresentFilter", binding = "typeRepresentFilter"),
        @Bind(key = "reasonRepresentFilter", binding = "reasonRepresentFilter"),
        @Bind(key = "basementRepresentFilter", binding = "basementRepresentFilter"),
        @Bind(key = "representationBasementDate", binding = "representationBasementDate"),
        @Bind(key = "representationBasementNumber", binding = "representationBasementNumber")
})
public class ListRepresentDiplomAndExcludeEditUI extends UIPresenter
{

    public static final String LIST_REPRESENT_ID = "listRepresentId";

    private Long listRepresentId;
    private RepresentationType typeRepresentFilter;
    private RepresentationReason reasonRepresentFilter;
    private RepresentationBasement basementRepresentFilter;

    private Date _representationBasementDate;// Дата основания
    private String _representationBasementNumber;// № основания
    private ListRepresentDiplomAndExclude listRepresent;

    private List<Student> studentSelectedList;
    private List<Student> studentChecked;

    private boolean warning = false;
    private boolean save = false;


    @Override
    public void saveSettings()
    {
        super.saveSettings();
    }

    //    События компонента
    public boolean isWarning()
    {
        return warning;
    }

    public void setWarning(boolean warning)
    {
        this.warning = warning;
    }

    @Override
    public void onComponentRefresh()
    {
        clearSettings();
        if (getListRepresentId() != null)
        {
            listRepresent = DataAccessServices.dao().get(ListRepresentDiplomAndExclude.class, ListRepresentDiplomAndExclude.id().s(), getListRepresentId());
            _uiSettings.set("dateExclude", listRepresent.getDateExclude());
            _uiSettings.set("dateProtocol", listRepresent.getDateProtocol());
            _uiSettings.set("numberProtocol", listRepresent.getNumberProtocol());
            studentSelectedList = ListRepresentDiplomAndExcludeManager.instance().getListObjectModifyDAO().selectRepresent(listRepresent);
            List<RelListRepresentStudents> listRepresentStudents = UniDaoFacade.getCoreDao().getList(RelListRepresentStudents.class, RelListRepresentStudents.representation(), listRepresent, new String[]{});
            studentChecked = new ArrayList<>();
            for (RelListRepresentStudents representStudents : listRepresentStudents)
            {
                if (representStudents.getFive())
                    studentChecked.add(representStudents.getStudent());
            }
        } else
        {
            listRepresent = new ListRepresentDiplomAndExclude();

            listRepresent.setRepresentationType(getTypeRepresentFilter());
            listRepresent.setRepresentationReason(getReasonRepresentFilter());
            listRepresent.setRepresentationBasement(getBasementRepresentFilter());
            listRepresent.setState(DataAccessServices.dao().get(MovestudentExtractStates.class, MovestudentExtractStates.code().s(), "1"));
            if (studentSelectedList == null)
                studentSelectedList = new ArrayList<>();
            _uiSettings.set("dateExclude", new Date());
            _uiSettings.set("dateProtocol", getRepresentationBasementDate());
            _uiSettings.set("numberProtocol", getRepresentationBasementNumber());
            _uiSettings.set("warning", warning);
        }
    }


    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        Map<String, Object> settingMap = _uiSettings.getAsMap(
                "educationYearFilter",
                "yearDistPartFilter",
                "territorialOrgUnitFilter",
                StudentDSHandler.FORMATIVE_ORG_UNIT_FILTER,
                "educationLevelsHighSchoolDSFilter",
                "educationOrgUnitFilter",
                "educationLevelFilter",
                StudentDSHandler.QUALIFICATION_FILTER,
                "courseFilter",
                "groupFilter",
                StudentDSHandler.DEVELOP_FORM_FILTER,
                "compensationTypeFilter",
                "benefitFilter",
                StudentDSHandler.STUDENT_FIO_FILTER,
                "educationYearField",
                "isSuccessfullyHandOverSession",
                "sessionMarksResult",
                "isConsiderSessionResults",
                "isforeignCitizens",
                "studentFilter",
                "userClickApply",
                "groupField",
                "educationLevelsHighSchoolDSField",
                "studentStatusFilter"
        );

        dataSource.putAll(settingMap);
        dataSource.put(StudentDSHandler.STUDENT_SELECTED_LIST, studentSelectedList);
        dataSource.put(StudentDSHandler.ORDER_ID, getListRepresentId());

    }

    @Override
    public void onAfterDataSourceFetch(IUIDataSource datasource)
    {
        super.onAfterDataSourceFetch(datasource);
        if ((datasource.getName().equals(ListRepresentDiplomAndExcludeEdit.STUDENT_SELECTED_DS)) && (studentChecked != null))
        {

            Collection<IEntity> records = new ArrayList<>();
            for (Student cheked : studentChecked)
            {
                records.add(cheked);
            }
            BaseSearchListDataSource representDS = getConfig().getDataSource(ListRepresentDiplomAndExcludeEdit.STUDENT_SELECTED_DS);
            ((CheckboxColumn) representDS.getLegacyDataSource().getColumn("five")).setSelectedObjects(records);
            studentChecked = null;
        }
    }

    //    События формы

    public void selectRepresent()
    {
        BaseSearchListDataSource representDS = getConfig().getDataSource(ListRepresentDiplomAndExcludeEdit.STUDENT_DS);
        Collection<IEntity> records = ((CheckboxColumn) representDS.getLegacyDataSource().getColumn(StudentDSHandler.CHECKBOX_COLUMN)).getSelectedObjects();
        List<Student> selectList = new ArrayList<>();
        for (IEntity record : records)
        {
            selectList.add((Student) record);
        }
        if (selectList.isEmpty())
            ContextLocal.getErrorCollector().add("Список выбранных студентов пуст");

        if (getTypeRepresentFilter() != null)
            ListRepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(getTypeRepresentFilter().getCode()).getListObjectModifyDAO().checkSelectStudent(this.listRepresent, selectList);
        int b = 0;
        int m = 0;
        int s = 0;
        List<Student> resultStatusList = new ArrayList<>();
        for (Student student : selectList)
        {
            if (student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isBachelor())
            {
                b = 1;
            }//бакалавр
            else if (student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isMaster())
            {
                m = 1;
            }//магистр
            else
            {
                s = 1;
            }

            if (!student.getStatus().getCode().equals(UniDefines.CATALOG_STUDENT_STATUS_ACTIVE))
            {
                resultStatusList.add(student);
            }
        }
        //проверка статуса студента
        if (!resultStatusList.isEmpty())
        {
            String str = "";
            setWarning(true);
            _uiSettings.set("warning", warning);
            for (Student student : resultStatusList)
            {
                str += ", " + student.getPerson().getFullFio();
            }
            String st = (resultStatusList.size() > 1) ? " студентов находящихся" : " студента находящегося";
            String warningMes = "Представление формируется для " + st + " в статусе неактивный: " + str.replaceFirst(", ", "");
            _uiSettings.set("warningMes", warningMes);
            selectList.clear();
        }
        //проверка представлений студента
        if (!isWarning())
        {
            if ((b + m + s) != 1)
            {
                ContextLocal.getErrorCollector().add("Выбраны обучающиеся разных квалификаций. Чтобы сформировать представление выберите обучающихся одной квалификации");
                selectList.clear();
            }
        }
        studentSelectedList.addAll(selectList);
    }

    public void onCancelRepresent()
    {
        setWarning(false);
        setSave(false);
        _uiSettings.set("warning", warning);
    }

    public void onIgnoreWarning()
    {
        if (isSave())
        {
            setWarning(false);
            _uiSettings.set("warning", warning);
            onClickSave();
        } else
        {
            setWarning(false);
            _uiSettings.set("warning", warning);
            int b = 0;
            int m = 0;
            int s = 0;
            BaseSearchListDataSource representDS = getConfig().getDataSource(ListRepresentDiplomAndExcludeEdit.STUDENT_DS);

            Collection<IEntity> records = ((CheckboxColumn) representDS.getLegacyDataSource().getColumn(StudentDSHandler.CHECKBOX_COLUMN)).getSelectedObjects();
            List<Student> selectList = new ArrayList<>();
            for (IEntity record : records)
            {
                selectList.add((Student) record);
            }

            for (Student student : selectList)
            {

                if (student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isBachelor())
                {
                    b = 1;
                }//бакалавр
                else if (student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isMaster())
                {
                    m = 1;
                }//магистр
                else
                {
                    s = 1;//специалист
                }
            }

            //		проверка представлений студента
            List<Student> result = new ArrayList<>();//WTF????
            if (!result.isEmpty())
            {
                String str = "";
                for (Student student : result)
                {
                    str += ", " + student.getPerson().getFullFio();
                }
                String errors = "Для " + str.replaceFirst(", ", " ") + " уже сфрмированы представления." + "\r\n" + "Чтобы сформировать очередные представления необходимо провести по приказу ранее созданные.";
                ContextLocal.getErrorCollector().add(errors);
                selectList.clear();
            }
            if ((b + m + s) != 1)
            {
                ContextLocal.getErrorCollector().add("Выбраны обучающиеся разных квалификаций. Чтобы сформировать представление выберите обучающихся одной квалификации");
                selectList.clear();
            }
            studentSelectedList.addAll(selectList);
        }
    }


    public void deleteSelectedRepresent()
    {

        BaseSearchListDataSource representDS = getConfig().getDataSource(ListRepresentDiplomAndExcludeEdit.STUDENT_SELECTED_DS);
        Collection<IEntity> records = ((CheckboxColumn) representDS.getLegacyDataSource().getColumn(StudentSelectedDSHandler.CHECKBOX_COLUMN)).getSelectedObjects();

        for (IEntity record : records)
        {
            studentSelectedList.remove(record);
        }
    }

    public void onClickSave()
    {

        if (listRepresentId != null)
            CheckStateUtil.checkStateRepresent(listRepresent, Collections.singletonList(MovestudentExtractStatesCodes.CODE_1), _uiSupport.getSession());

        boolean ok = MoveStudentUtil.isChecked(studentSelectedList, null, null);

        if ((ok && !isSave()) || (ok && isWarning()))
        {
            String mess = MoveStudentUtil.checked(_uiSettings.get("dateExclude", Date.class), true);
            if (!StringUtils.isEmpty(mess))
            {
                setSave(true);
                setWarning(true);
                _uiSettings.set("warning", warning);
                _uiSettings.set("warningMes", mess);
                return;
            }
        }

        if (studentSelectedList.isEmpty())
            getConfig().getUserContext().getErrorCollector().add(getConfig().getProperty("ui.alertEmptySelectedRepresent"));

        listRepresent.setDateExclude(_uiSettings.get("dateExclude", Date.class));
        listRepresent.setDateProtocol(_uiSettings.get("dateProtocol", Date.class));
        listRepresent.setNumberProtocol(_uiSettings.get("numberProtocol"));
        listRepresent.setCreateDate(getSupport().getCurrentDate());

        if (getConfig().getUserContext().getErrorCollector().hasErrors())
            return;

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocListRepresentBasics.class, "b").column("b");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(DocListRepresentBasics.listRepresent().fromAlias("b")),
                DQLExpressions.value(listRepresent)));

        List<DocListRepresentBasics> representOldBasicsList = builder.createStatement(getSupport().getSession()).list();

        DocListRepresentBasics representBasics;

        if (representOldBasicsList.size() > 0)
        {
            representBasics = representOldBasicsList.get(0);
        } else
        {
            representBasics = new DocListRepresentBasics();
            representBasics.setListRepresent(listRepresent);
            representBasics.setBasic(listRepresent.getRepresentationBasement());
            representBasics.setReason(listRepresent.getRepresentationReason());

            representBasics.setRepresentationBasementDate(_representationBasementDate);
            representBasics.setRepresentationBasementNumber(_representationBasementNumber);
        }
        IPrincipalContext context = ContextLocal.getUserContext().getPrincipalContext();
        Person person = PersonManager.instance().dao().getPerson(context);
        listRepresent.setOperator(person);
        listRepresent.setCreator(context);

        if (getListRepresentId() == null)
            ListRepresentDiplomAndExcludeManager.instance().getListObjectModifyDAO().save(listRepresent, studentSelectedList, representBasics);
        else
            ListRepresentDiplomAndExcludeManager.instance().getListObjectModifyDAO().update(listRepresent, studentSelectedList);

        List<RelListRepresentStudents> list = UniDaoFacade.getCoreDao().getList(RelListRepresentStudents.class, RelListRepresentStudents.representation(), listRepresent, new String[]{});

        BaseSearchListDataSource representDS = getConfig().getDataSource(ListRepresentDiplomAndExcludeEdit.STUDENT_SELECTED_DS);
        Collection<IEntity> records = ((CheckboxColumn) representDS.getLegacyDataSource().getColumn("five")).getSelectedObjects();
        List<Student> fiveList = new ArrayList<>();
        for (IEntity record : records)
        {
            fiveList.add((Student) record);
        }
        for (RelListRepresentStudents rel : list)
        {
            if (fiveList.contains(rel.getStudent()))
                rel.setFive(true);
            else
                rel.setFive(false);
            UniDaoFacade.getCoreDao().saveOrUpdate(rel);
        }

        deactivate(2);

    }

    //    Вспомогательные функции
    public void onClickView()
    {
        try
        {
            Student st = DataAccessServices.dao().get(Student.class, (Long) getListenerParameter());
            _uiActivation.asRegionDialog(PersonView.class)
                    .parameter("publisherId", st.getPerson().getId())
                    .activate();
        } catch (Exception e)
        {
            getUserContext().getErrorCollector().add(e.getLocalizedMessage());
        }
    }

    //    Getters and Setters
    public boolean isSave()
    {
        return save;
    }

    public void setSave(boolean save)
    {
        this.save = save;
    }

    public ListRepresentDiplomAndExclude getListRepresentDiplomAndExcluder()
    {
        return listRepresent;
    }

    public void setListRepresentDiplomAndExclude(ListRepresentDiplomAndExclude listRepresent)
    {
        this.listRepresent = listRepresent;
    }

    public void onViewRepresentationFromList()
    {

    }

    public RepresentationType getTypeRepresentFilter()
    {
        return typeRepresentFilter;
    }

    public void setTypeRepresentFilter(RepresentationType typeRepresentFilter)
    {
        this.typeRepresentFilter = typeRepresentFilter;
    }

    public RepresentationReason getReasonRepresentFilter()
    {
        return reasonRepresentFilter;
    }

    public void setReasonRepresentFilter(RepresentationReason reasonRepresentFilter)
    {
        this.reasonRepresentFilter = reasonRepresentFilter;
    }

    public RepresentationBasement getBasementRepresentFilter()
    {
        return basementRepresentFilter;
    }

    public void setBasementRepresentFilter(RepresentationBasement basementRepresentFilter)
    {
        this.basementRepresentFilter = basementRepresentFilter;
    }

    public Long getListRepresentId()
    {
        return listRepresentId;
    }

    public void setListRepresentId(Long listRepresentId)
    {
        this.listRepresentId = listRepresentId;
    }

    public Date getRepresentationBasementDate()
    {
        return _representationBasementDate;
    }

    public void setRepresentationBasementDate(Date representationBasementDate)
    {
        _representationBasementDate = representationBasementDate;
    }

    public String getRepresentationBasementNumber()
    {
        return _representationBasementNumber;
    }

    public void setRepresentationBasementNumber(String representationBasementNumber)
    {
        _representationBasementNumber = representationBasementNumber;
    }

}
