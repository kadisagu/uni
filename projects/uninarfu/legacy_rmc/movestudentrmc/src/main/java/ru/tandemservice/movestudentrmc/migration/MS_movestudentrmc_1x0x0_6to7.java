package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.BooleanDBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;

import java.sql.PreparedStatement;

public class MS_movestudentrmc_1x0x0_6to7 extends IndependentMigrationScript {

    public void run(DBTool tool) throws Exception {
        if (!tool.tableExists("representationtype_t"))
            return;

        if (!tool.table("representationtype_t").columnExists("printattachment_p")) {
            tool.createColumn("representationtype_t", new BooleanDBColumn("printattachment_p").setNullable(false));
            PreparedStatement pst = tool.getConnection().prepareStatement("update representationtype_t SET printattachment_p=?");
            pst.setBoolean(1, false);
            pst.execute();
        }
    }

}
