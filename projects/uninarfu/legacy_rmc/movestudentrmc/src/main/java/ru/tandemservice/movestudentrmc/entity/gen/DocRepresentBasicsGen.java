package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudentrmc.entity.DocRepresentBasics;
import ru.tandemservice.movestudentrmc.entity.NarfuDocument;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationBasement;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность-связь 'Представление'-'Основание'
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DocRepresentBasicsGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.DocRepresentBasics";
    public static final String ENTITY_NAME = "docRepresentBasics";
    public static final int VERSION_HASH = 941315158;
    private static IEntityMeta ENTITY_META;

    public static final String L_REPRESENTATION = "representation";
    public static final String L_REASON = "reason";
    public static final String L_BASIC = "basic";
    public static final String P_TEXT_BASIC = "textBasic";
    public static final String P_REPRESENTATION_BASEMENT_DATE = "representationBasementDate";
    public static final String P_REPRESENTATION_BASEMENT_NUMBER = "representationBasementNumber";
    public static final String L_DOCUMENT = "document";

    private Representation _representation;     // Представление
    private RepresentationReason _reason;     // Причина
    private RepresentationBasement _basic;     // Основание
    private String _textBasic;     // Текст основания
    private Date _representationBasementDate;     // Дата основания
    private String _representationBasementNumber;     // № основания
    private NarfuDocument _document;     // Документ основания

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Представление. Свойство не может быть null.
     */
    @NotNull
    public Representation getRepresentation()
    {
        return _representation;
    }

    /**
     * @param representation Представление. Свойство не может быть null.
     */
    public void setRepresentation(Representation representation)
    {
        dirty(_representation, representation);
        _representation = representation;
    }

    /**
     * @return Причина.
     */
    public RepresentationReason getReason()
    {
        return _reason;
    }

    /**
     * @param reason Причина.
     */
    public void setReason(RepresentationReason reason)
    {
        dirty(_reason, reason);
        _reason = reason;
    }

    /**
     * @return Основание. Свойство не может быть null.
     */
    @NotNull
    public RepresentationBasement getBasic()
    {
        return _basic;
    }

    /**
     * @param basic Основание. Свойство не может быть null.
     */
    public void setBasic(RepresentationBasement basic)
    {
        dirty(_basic, basic);
        _basic = basic;
    }

    /**
     * @return Текст основания.
     */
    @Length(max=255)
    public String getTextBasic()
    {
        return _textBasic;
    }

    /**
     * @param textBasic Текст основания.
     */
    public void setTextBasic(String textBasic)
    {
        dirty(_textBasic, textBasic);
        _textBasic = textBasic;
    }

    /**
     * @return Дата основания.
     */
    public Date getRepresentationBasementDate()
    {
        return _representationBasementDate;
    }

    /**
     * @param representationBasementDate Дата основания.
     */
    public void setRepresentationBasementDate(Date representationBasementDate)
    {
        dirty(_representationBasementDate, representationBasementDate);
        _representationBasementDate = representationBasementDate;
    }

    /**
     * @return № основания.
     */
    @Length(max=255)
    public String getRepresentationBasementNumber()
    {
        return _representationBasementNumber;
    }

    /**
     * @param representationBasementNumber № основания.
     */
    public void setRepresentationBasementNumber(String representationBasementNumber)
    {
        dirty(_representationBasementNumber, representationBasementNumber);
        _representationBasementNumber = representationBasementNumber;
    }

    /**
     * @return Документ основания.
     */
    public NarfuDocument getDocument()
    {
        return _document;
    }

    /**
     * @param document Документ основания.
     */
    public void setDocument(NarfuDocument document)
    {
        dirty(_document, document);
        _document = document;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DocRepresentBasicsGen)
        {
            setRepresentation(((DocRepresentBasics)another).getRepresentation());
            setReason(((DocRepresentBasics)another).getReason());
            setBasic(((DocRepresentBasics)another).getBasic());
            setTextBasic(((DocRepresentBasics)another).getTextBasic());
            setRepresentationBasementDate(((DocRepresentBasics)another).getRepresentationBasementDate());
            setRepresentationBasementNumber(((DocRepresentBasics)another).getRepresentationBasementNumber());
            setDocument(((DocRepresentBasics)another).getDocument());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DocRepresentBasicsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DocRepresentBasics.class;
        }

        public T newInstance()
        {
            return (T) new DocRepresentBasics();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "representation":
                    return obj.getRepresentation();
                case "reason":
                    return obj.getReason();
                case "basic":
                    return obj.getBasic();
                case "textBasic":
                    return obj.getTextBasic();
                case "representationBasementDate":
                    return obj.getRepresentationBasementDate();
                case "representationBasementNumber":
                    return obj.getRepresentationBasementNumber();
                case "document":
                    return obj.getDocument();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "representation":
                    obj.setRepresentation((Representation) value);
                    return;
                case "reason":
                    obj.setReason((RepresentationReason) value);
                    return;
                case "basic":
                    obj.setBasic((RepresentationBasement) value);
                    return;
                case "textBasic":
                    obj.setTextBasic((String) value);
                    return;
                case "representationBasementDate":
                    obj.setRepresentationBasementDate((Date) value);
                    return;
                case "representationBasementNumber":
                    obj.setRepresentationBasementNumber((String) value);
                    return;
                case "document":
                    obj.setDocument((NarfuDocument) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "representation":
                        return true;
                case "reason":
                        return true;
                case "basic":
                        return true;
                case "textBasic":
                        return true;
                case "representationBasementDate":
                        return true;
                case "representationBasementNumber":
                        return true;
                case "document":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "representation":
                    return true;
                case "reason":
                    return true;
                case "basic":
                    return true;
                case "textBasic":
                    return true;
                case "representationBasementDate":
                    return true;
                case "representationBasementNumber":
                    return true;
                case "document":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "representation":
                    return Representation.class;
                case "reason":
                    return RepresentationReason.class;
                case "basic":
                    return RepresentationBasement.class;
                case "textBasic":
                    return String.class;
                case "representationBasementDate":
                    return Date.class;
                case "representationBasementNumber":
                    return String.class;
                case "document":
                    return NarfuDocument.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DocRepresentBasics> _dslPath = new Path<DocRepresentBasics>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DocRepresentBasics");
    }
            

    /**
     * @return Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentBasics#getRepresentation()
     */
    public static Representation.Path<Representation> representation()
    {
        return _dslPath.representation();
    }

    /**
     * @return Причина.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentBasics#getReason()
     */
    public static RepresentationReason.Path<RepresentationReason> reason()
    {
        return _dslPath.reason();
    }

    /**
     * @return Основание. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentBasics#getBasic()
     */
    public static RepresentationBasement.Path<RepresentationBasement> basic()
    {
        return _dslPath.basic();
    }

    /**
     * @return Текст основания.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentBasics#getTextBasic()
     */
    public static PropertyPath<String> textBasic()
    {
        return _dslPath.textBasic();
    }

    /**
     * @return Дата основания.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentBasics#getRepresentationBasementDate()
     */
    public static PropertyPath<Date> representationBasementDate()
    {
        return _dslPath.representationBasementDate();
    }

    /**
     * @return № основания.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentBasics#getRepresentationBasementNumber()
     */
    public static PropertyPath<String> representationBasementNumber()
    {
        return _dslPath.representationBasementNumber();
    }

    /**
     * @return Документ основания.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentBasics#getDocument()
     */
    public static NarfuDocument.Path<NarfuDocument> document()
    {
        return _dslPath.document();
    }

    public static class Path<E extends DocRepresentBasics> extends EntityPath<E>
    {
        private Representation.Path<Representation> _representation;
        private RepresentationReason.Path<RepresentationReason> _reason;
        private RepresentationBasement.Path<RepresentationBasement> _basic;
        private PropertyPath<String> _textBasic;
        private PropertyPath<Date> _representationBasementDate;
        private PropertyPath<String> _representationBasementNumber;
        private NarfuDocument.Path<NarfuDocument> _document;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentBasics#getRepresentation()
     */
        public Representation.Path<Representation> representation()
        {
            if(_representation == null )
                _representation = new Representation.Path<Representation>(L_REPRESENTATION, this);
            return _representation;
        }

    /**
     * @return Причина.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentBasics#getReason()
     */
        public RepresentationReason.Path<RepresentationReason> reason()
        {
            if(_reason == null )
                _reason = new RepresentationReason.Path<RepresentationReason>(L_REASON, this);
            return _reason;
        }

    /**
     * @return Основание. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentBasics#getBasic()
     */
        public RepresentationBasement.Path<RepresentationBasement> basic()
        {
            if(_basic == null )
                _basic = new RepresentationBasement.Path<RepresentationBasement>(L_BASIC, this);
            return _basic;
        }

    /**
     * @return Текст основания.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentBasics#getTextBasic()
     */
        public PropertyPath<String> textBasic()
        {
            if(_textBasic == null )
                _textBasic = new PropertyPath<String>(DocRepresentBasicsGen.P_TEXT_BASIC, this);
            return _textBasic;
        }

    /**
     * @return Дата основания.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentBasics#getRepresentationBasementDate()
     */
        public PropertyPath<Date> representationBasementDate()
        {
            if(_representationBasementDate == null )
                _representationBasementDate = new PropertyPath<Date>(DocRepresentBasicsGen.P_REPRESENTATION_BASEMENT_DATE, this);
            return _representationBasementDate;
        }

    /**
     * @return № основания.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentBasics#getRepresentationBasementNumber()
     */
        public PropertyPath<String> representationBasementNumber()
        {
            if(_representationBasementNumber == null )
                _representationBasementNumber = new PropertyPath<String>(DocRepresentBasicsGen.P_REPRESENTATION_BASEMENT_NUMBER, this);
            return _representationBasementNumber;
        }

    /**
     * @return Документ основания.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentBasics#getDocument()
     */
        public NarfuDocument.Path<NarfuDocument> document()
        {
            if(_document == null )
                _document = new NarfuDocument.Path<NarfuDocument>(L_DOCUMENT, this);
            return _document;
        }

        public Class getEntityClass()
        {
            return DocRepresentBasics.class;
        }

        public String getEntityName()
        {
            return "docRepresentBasics";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
