package ru.tandemservice.movestudentrmc.docord.util.title;

import org.tandemframework.rtf.util.RtfString;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;

/**
 * Интерфейс для вывода специфических названий объектов
 * по требованию заказчика
 *
 * @author Viktor
 */
public interface ISpecificPrintTitle {

    public RtfString getSpecificPrintTitle(EducationLevelsHighSchool levelsHighSchool);
}
