package ru.tandemservice.movestudentrmc.component.settings.OrderCategoryToRepresentationSettingsAdd;

import ru.tandemservice.movestudentrmc.entity.RelOrderCategoryRepresentationType;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.uni.component.settings.abstractRelation.AbstractRelationAdd.AbstractRelationAddDAO;

import java.util.List;

public class DAO extends AbstractRelationAddDAO<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        // TODO Auto-generated method stub
        super.prepare(model);

        String hql = (new StringBuilder()).append("from ").append(getRelationSecondEntityClass().getName()).append(" b where b not in (select ").append("second").append(" from ").append(getRelationEntityClass().getName()).append(") order by b.title").toString();
        List list = getSession().createQuery(hql).list();

        model.setSecondList(list);
    }

    @Override
    protected Class<RelOrderCategoryRepresentationType> getRelationEntityClass() {
        // TODO Auto-generated method stub
        return RelOrderCategoryRepresentationType.class;
    }

    @Override
    protected Class<RepresentationType> getRelationSecondEntityClass() {
        // TODO Auto-generated method stub
        return RepresentationType.class;

    }

    @Override
    protected String getRelationSecondEntityClassSortProperty() {
        // TODO Auto-generated method stub
        return "title";
    }


}
