package ru.tandemservice.movestudentrmc.component.student.GrantTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab.MonthWrapper;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.List;

@State({
        @Bind(key = "studentId", binding = "student.id")
})
public class Model {
    private Student student = new Student();

    private List<EducationYear> eduYearList;
    private EducationYear eduYear;

    private MonthWrapper month;
    private DynamicListDataSource<MonthWrapper> dataSource;

    public void setEduYearList(List<EducationYear> eduYearList) {
        this.eduYearList = eduYearList;
    }

    public List<EducationYear> getEduYearList() {
        return eduYearList;
    }

    public void setEduYear(EducationYear eduYear) {
        this.eduYear = eduYear;
    }

    public EducationYear getEduYear() {
        return eduYear;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Student getStudent() {
        return student;
    }

    public void setMonth(MonthWrapper month) {
        this.month = month;
    }

    public MonthWrapper getMonth() {
        return month;
    }

    public void setDataSource(DynamicListDataSource<MonthWrapper> dataSource) {
        this.dataSource = dataSource;
    }

    public DynamicListDataSource<MonthWrapper> getDataSource() {
        return dataSource;
    }

}
