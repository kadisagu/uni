package ru.tandemservice.movestudentrmc.entity;

import ru.tandemservice.movestudentrmc.entity.catalog.MovestudentOrderStates;

import java.util.Date;

public interface IAbstractOrder {

    public Date getCommitDate();

    public String getNumber();

    public Long getOrderID();

    public MovestudentOrderStates getState();

    public Date getCreateDate();
}
