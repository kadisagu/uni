package ru.tandemservice.movestudentrmc.base.bo.DocumentSettings.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.movestudentrmc.base.bo.DocumentSettings.DocumentSettingsManager;
import ru.tandemservice.movestudentrmc.entity.catalog.DocumentKind;
import ru.tandemservice.movestudentrmc.entity.catalog.DocumentType;

import java.util.ArrayList;
import java.util.List;

@Input(
        @Bind(key = DocumentSettingsAddEditUI.TYPE_ID, binding = "typeId")
)
public class DocumentSettingsAddEditUI extends UIPresenter {

    public static final String TYPE_ID = "typeId";
    public static final String TYPE_REPRESENTATION_ID = "typeRepresentationId";

    private Long typeId;
    private DocumentType documentType;

    private List<DocumentKind> kindList;

    private Integer pageNum = null;
    private int pageEnd = 1;

    private List<Wrapper> wrapperList;
    private Wrapper curWrapper;

    @Override
    public void onComponentRefresh() {
        if (null != typeId)
            documentType = DataAccessServices.dao().getNotNull(DocumentType.class, typeId);
        else {
            documentType = new DocumentType();
        }

        if (pageNum == null) {
            kindList = DocumentSettingsManager.instance().dao().getKinds(documentType);

            if (kindList.isEmpty())
                setPageNum(0);
            else {
                setPageNum(1);
            }
        }

    }

    private void fillArray() {
        wrapperList = new ArrayList<Wrapper>();
        for (DocumentKind kind : this.kindList) {
            Wrapper wrapper = new Wrapper();
            wrapper.setKind(kind);
            wrapper.setDocuments(DocumentSettingsManager.instance().dao().getDocuments(documentType, kind));

            wrapperList.add(wrapper);
        }
    }

    public void onClickApply() {
        List<DocumentKind> oldKinds = DocumentSettingsManager.instance().dao().getKinds(documentType);
        for (Wrapper wrapper : wrapperList) {
            DocumentSettingsManager.instance().dao().saveRelations(documentType, wrapper.getKind(), wrapper.getDocuments());
            oldKinds.remove(wrapper.getKind());
        }

        //удаляем старые связи (эти виды уже не используются)
        for (DocumentKind oldKind : oldKinds)
            DocumentSettingsManager.instance().dao().saveRelations(documentType, oldKind, null);

        deactivate();
    }

    public void onClickPrev() {
        setPageNum(0);
    }

    public void onClickNext() {
        setPageNum(1);
    }

    @Override
    public void onComponentPrepareRender() {
        super.onComponentPrepareRender();
    }

//    Getters and Setters

    public String getPageView() {
        return getClass().getPackage().getName() + ".page" + (getPageNum());
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;

        if (pageNum == 1)
            fillArray();
    }

    public int getPageEnd() {
        return pageEnd;
    }

    public void setPageEnd(int pageEnd) {
        this.pageEnd = pageEnd;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public DocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentType documentType) {
        this.documentType = documentType;
    }

    public List<DocumentKind> getKindList() {
        return this.kindList;
    }

    public void setKindList(List<DocumentKind> kindList) {
        this.kindList = kindList;
    }

    public Wrapper getCurWrapper() {
        return curWrapper;
    }

    public void setCurWrapper(Wrapper curWrapper) {
        this.curWrapper = curWrapper;
    }

    public List<Wrapper> getWrapperList() {
        return wrapperList;
    }

    public void setWrapperList(List<Wrapper> wrapperList) {
        this.wrapperList = wrapperList;
    }

}
