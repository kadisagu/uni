package ru.tandemservice.movestudentrmc.entity;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.movestudentrmc.entity.gen.ListRepresentSocialGrantGen;

/**
 * Списочное О назначении государственной социальной стипендии
 */
public class ListRepresentSocialGrant extends ListRepresentSocialGrantGen
{

    @Override
    public String getTitle() {
        String shortTitle = getOrgUnitShortTitle();
        return getRepresentationType().getTitle()
                + " (" + getGrantView().getShortTitle() + ") c "
                + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateBeginingPayment())
                + (getDateEndOfPayment() != null ? " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateEndOfPayment()) : "-")
                + (getOperator() != null ? ", " + getOperator().getFullFio() : "")
                + (shortTitle.isEmpty() ? "" : ", " + shortTitle)
                + ", от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getCreateDate());
    }

    @Override
    public String getRepresentationTitle() {
        String shortTitle = getOrgUnitShortTitle();
        return getRepresentationType().getTitle()
                + " (" + getGrantView().getShortTitle() + ") c "
                + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateBeginingPayment())
                + (getDateEndOfPayment() != null ? " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateEndOfPayment()) : "-")
                + (shortTitle.isEmpty() ? "" : ", " + shortTitle)
                ;
    }
}