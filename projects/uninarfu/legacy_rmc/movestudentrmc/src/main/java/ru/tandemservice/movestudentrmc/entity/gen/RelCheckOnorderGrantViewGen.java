package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.RelCheckOnorderGrantView;
import ru.tandemservice.movestudentrmc.entity.catalog.CheckOnOrder;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность-связь 'Проверки по приказам'-'Виды стипендий/выплат'
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RelCheckOnorderGrantViewGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.RelCheckOnorderGrantView";
    public static final String ENTITY_NAME = "relCheckOnorderGrantView";
    public static final int VERSION_HASH = -542205959;
    private static IEntityMeta ENTITY_META;

    public static final String L_CHECK = "check";
    public static final String L_VIEW = "view";
    public static final String P_VALUE = "value";

    private CheckOnOrder _check;     // Проверка по приказам
    private GrantView _view;     // Виды стипендий/выплат
    private boolean _value = false;     // Значение

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Проверка по приказам. Свойство не может быть null.
     */
    @NotNull
    public CheckOnOrder getCheck()
    {
        return _check;
    }

    /**
     * @param check Проверка по приказам. Свойство не может быть null.
     */
    public void setCheck(CheckOnOrder check)
    {
        dirty(_check, check);
        _check = check;
    }

    /**
     * @return Виды стипендий/выплат. Свойство не может быть null.
     */
    @NotNull
    public GrantView getView()
    {
        return _view;
    }

    /**
     * @param view Виды стипендий/выплат. Свойство не может быть null.
     */
    public void setView(GrantView view)
    {
        dirty(_view, view);
        _view = view;
    }

    /**
     * @return Значение. Свойство не может быть null.
     */
    @NotNull
    public boolean isValue()
    {
        return _value;
    }

    /**
     * @param value Значение. Свойство не может быть null.
     */
    public void setValue(boolean value)
    {
        dirty(_value, value);
        _value = value;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RelCheckOnorderGrantViewGen)
        {
            setCheck(((RelCheckOnorderGrantView)another).getCheck());
            setView(((RelCheckOnorderGrantView)another).getView());
            setValue(((RelCheckOnorderGrantView)another).isValue());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RelCheckOnorderGrantViewGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RelCheckOnorderGrantView.class;
        }

        public T newInstance()
        {
            return (T) new RelCheckOnorderGrantView();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "check":
                    return obj.getCheck();
                case "view":
                    return obj.getView();
                case "value":
                    return obj.isValue();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "check":
                    obj.setCheck((CheckOnOrder) value);
                    return;
                case "view":
                    obj.setView((GrantView) value);
                    return;
                case "value":
                    obj.setValue((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "check":
                        return true;
                case "view":
                        return true;
                case "value":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "check":
                    return true;
                case "view":
                    return true;
                case "value":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "check":
                    return CheckOnOrder.class;
                case "view":
                    return GrantView.class;
                case "value":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RelCheckOnorderGrantView> _dslPath = new Path<RelCheckOnorderGrantView>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RelCheckOnorderGrantView");
    }
            

    /**
     * @return Проверка по приказам. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelCheckOnorderGrantView#getCheck()
     */
    public static CheckOnOrder.Path<CheckOnOrder> check()
    {
        return _dslPath.check();
    }

    /**
     * @return Виды стипендий/выплат. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelCheckOnorderGrantView#getView()
     */
    public static GrantView.Path<GrantView> view()
    {
        return _dslPath.view();
    }

    /**
     * @return Значение. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelCheckOnorderGrantView#isValue()
     */
    public static PropertyPath<Boolean> value()
    {
        return _dslPath.value();
    }

    public static class Path<E extends RelCheckOnorderGrantView> extends EntityPath<E>
    {
        private CheckOnOrder.Path<CheckOnOrder> _check;
        private GrantView.Path<GrantView> _view;
        private PropertyPath<Boolean> _value;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Проверка по приказам. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelCheckOnorderGrantView#getCheck()
     */
        public CheckOnOrder.Path<CheckOnOrder> check()
        {
            if(_check == null )
                _check = new CheckOnOrder.Path<CheckOnOrder>(L_CHECK, this);
            return _check;
        }

    /**
     * @return Виды стипендий/выплат. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelCheckOnorderGrantView#getView()
     */
        public GrantView.Path<GrantView> view()
        {
            if(_view == null )
                _view = new GrantView.Path<GrantView>(L_VIEW, this);
            return _view;
        }

    /**
     * @return Значение. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelCheckOnorderGrantView#isValue()
     */
        public PropertyPath<Boolean> value()
        {
            if(_value == null )
                _value = new PropertyPath<Boolean>(RelCheckOnorderGrantViewGen.P_VALUE, this);
            return _value;
        }

        public Class getEntityClass()
        {
            return RelCheckOnorderGrantView.class;
        }

        public String getEntityName()
        {
            return "relCheckOnorderGrantView";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
