package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudentrmc.entity.IDocRepresentWithNewGroup;
import ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * О выходе из академического отпуска
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RepresentWeekendOutGen extends Representation
 implements IDocRepresentWithNewGroup{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut";
    public static final String ENTITY_NAME = "representWeekendOut";
    public static final int VERSION_HASH = 1908649521;
    private static IEntityMeta ENTITY_META;

    public static final String P_AHEAD = "ahead";
    public static final String L_NEW_FORMATIVE_ORG_UNIT = "newFormativeOrgUnit";
    public static final String L_NEW_COURSE = "newCourse";
    public static final String L_NEW_GROUP = "newGroup";
    public static final String L_NEW_EDUCATION_LEVELS_HIGH_SCHOOL = "newEducationLevelsHighSchool";
    public static final String L_NEW_DEVELOP_FORM = "newDevelopForm";
    public static final String L_EDUCATION_ORG_UNIT = "educationOrgUnit";
    public static final String L_FORMATIVE_ORG_UNIT_OLD = "formativeOrgUnitOld";
    public static final String L_COURSE_OLD = "courseOld";
    public static final String L_GROUP_OLD = "groupOld";
    public static final String L_EDUCATION_LEVELS_HIGH_SCHOOL_OLD = "educationLevelsHighSchoolOld";
    public static final String L_DEVELOP_FORM_OLD = "developFormOld";
    public static final String L_EDUCATION_ORG_UNIT_OLD = "educationOrgUnitOld";
    public static final String P_DIFFERENCE_LIQUIDATION = "differenceLiquidation";
    public static final String P_DIFFERENCE_LIQUIDATION_DATE = "differenceLiquidationDate";
    public static final String P_DEBT_LIQUIDATION = "debtLiquidation";
    public static final String P_DEBT_LIQUIDATION_DATE = "debtLiquidationDate";

    private Boolean _ahead = false;     // Досрочно
    private OrgUnit _newFormativeOrgUnit;     // Новое формирующее подразделение
    private Course _newCourse;     // Новый курс
    private Group _newGroup;     // Новая группа
    private EducationLevelsHighSchool _newEducationLevelsHighSchool;     // Новое направление подготовки (специальность)
    private DevelopForm _newDevelopForm;     // Новая форма обучения
    private EducationOrgUnit _educationOrgUnit;     // Новое направление подготовки (специальность)
    private OrgUnit _formativeOrgUnitOld;     // Старое формирующее подразделение
    private Course _courseOld;     // Старый курс
    private Group _groupOld;     // Старая группа
    private EducationLevelsHighSchool _educationLevelsHighSchoolOld;     // Старое направление подготовки (специальность)
    private DevelopForm _developFormOld;     // Старая форма обучения
    private EducationOrgUnit _educationOrgUnitOld;     // Старое направление подготовки (специальность)
    private boolean _differenceLiquidation = false;     // С ликвидацией разницы в учебных планах
    private Date _differenceLiquidationDate;     // Дата ликвидации разницы в учебных планах
    private boolean _debtLiquidation = false;     // С ликвидацией академической задолженности
    private Date _debtLiquidationDate;     // Дата ликвидации академической задолженности

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Досрочно.
     */
    public Boolean getAhead()
    {
        return _ahead;
    }

    /**
     * @param ahead Досрочно.
     */
    public void setAhead(Boolean ahead)
    {
        dirty(_ahead, ahead);
        _ahead = ahead;
    }

    /**
     * @return Новое формирующее подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getNewFormativeOrgUnit()
    {
        return _newFormativeOrgUnit;
    }

    /**
     * @param newFormativeOrgUnit Новое формирующее подразделение. Свойство не может быть null.
     */
    public void setNewFormativeOrgUnit(OrgUnit newFormativeOrgUnit)
    {
        dirty(_newFormativeOrgUnit, newFormativeOrgUnit);
        _newFormativeOrgUnit = newFormativeOrgUnit;
    }

    /**
     * @return Новый курс.
     */
    public Course getNewCourse()
    {
        return _newCourse;
    }

    /**
     * @param newCourse Новый курс.
     */
    public void setNewCourse(Course newCourse)
    {
        dirty(_newCourse, newCourse);
        _newCourse = newCourse;
    }

    /**
     * @return Новая группа. Свойство не может быть null.
     */
    @NotNull
    public Group getNewGroup()
    {
        return _newGroup;
    }

    /**
     * @param newGroup Новая группа. Свойство не может быть null.
     */
    public void setNewGroup(Group newGroup)
    {
        dirty(_newGroup, newGroup);
        _newGroup = newGroup;
    }

    /**
     * @return Новое направление подготовки (специальность).
     */
    public EducationLevelsHighSchool getNewEducationLevelsHighSchool()
    {
        return _newEducationLevelsHighSchool;
    }

    /**
     * @param newEducationLevelsHighSchool Новое направление подготовки (специальность).
     */
    public void setNewEducationLevelsHighSchool(EducationLevelsHighSchool newEducationLevelsHighSchool)
    {
        dirty(_newEducationLevelsHighSchool, newEducationLevelsHighSchool);
        _newEducationLevelsHighSchool = newEducationLevelsHighSchool;
    }

    /**
     * @return Новая форма обучения.
     */
    public DevelopForm getNewDevelopForm()
    {
        return _newDevelopForm;
    }

    /**
     * @param newDevelopForm Новая форма обучения.
     */
    public void setNewDevelopForm(DevelopForm newDevelopForm)
    {
        dirty(_newDevelopForm, newDevelopForm);
        _newDevelopForm = newDevelopForm;
    }

    /**
     * @return Новое направление подготовки (специальность).
     */
    public EducationOrgUnit getEducationOrgUnit()
    {
        return _educationOrgUnit;
    }

    /**
     * @param educationOrgUnit Новое направление подготовки (специальность).
     */
    public void setEducationOrgUnit(EducationOrgUnit educationOrgUnit)
    {
        dirty(_educationOrgUnit, educationOrgUnit);
        _educationOrgUnit = educationOrgUnit;
    }

    /**
     * @return Старое формирующее подразделение.
     */
    public OrgUnit getFormativeOrgUnitOld()
    {
        return _formativeOrgUnitOld;
    }

    /**
     * @param formativeOrgUnitOld Старое формирующее подразделение.
     */
    public void setFormativeOrgUnitOld(OrgUnit formativeOrgUnitOld)
    {
        dirty(_formativeOrgUnitOld, formativeOrgUnitOld);
        _formativeOrgUnitOld = formativeOrgUnitOld;
    }

    /**
     * @return Старый курс.
     */
    public Course getCourseOld()
    {
        return _courseOld;
    }

    /**
     * @param courseOld Старый курс.
     */
    public void setCourseOld(Course courseOld)
    {
        dirty(_courseOld, courseOld);
        _courseOld = courseOld;
    }

    /**
     * @return Старая группа.
     */
    public Group getGroupOld()
    {
        return _groupOld;
    }

    /**
     * @param groupOld Старая группа.
     */
    public void setGroupOld(Group groupOld)
    {
        dirty(_groupOld, groupOld);
        _groupOld = groupOld;
    }

    /**
     * @return Старое направление подготовки (специальность).
     */
    public EducationLevelsHighSchool getEducationLevelsHighSchoolOld()
    {
        return _educationLevelsHighSchoolOld;
    }

    /**
     * @param educationLevelsHighSchoolOld Старое направление подготовки (специальность).
     */
    public void setEducationLevelsHighSchoolOld(EducationLevelsHighSchool educationLevelsHighSchoolOld)
    {
        dirty(_educationLevelsHighSchoolOld, educationLevelsHighSchoolOld);
        _educationLevelsHighSchoolOld = educationLevelsHighSchoolOld;
    }

    /**
     * @return Старая форма обучения.
     */
    public DevelopForm getDevelopFormOld()
    {
        return _developFormOld;
    }

    /**
     * @param developFormOld Старая форма обучения.
     */
    public void setDevelopFormOld(DevelopForm developFormOld)
    {
        dirty(_developFormOld, developFormOld);
        _developFormOld = developFormOld;
    }

    /**
     * @return Старое направление подготовки (специальность).
     */
    public EducationOrgUnit getEducationOrgUnitOld()
    {
        return _educationOrgUnitOld;
    }

    /**
     * @param educationOrgUnitOld Старое направление подготовки (специальность).
     */
    public void setEducationOrgUnitOld(EducationOrgUnit educationOrgUnitOld)
    {
        dirty(_educationOrgUnitOld, educationOrgUnitOld);
        _educationOrgUnitOld = educationOrgUnitOld;
    }

    /**
     * @return С ликвидацией разницы в учебных планах. Свойство не может быть null.
     */
    @NotNull
    public boolean isDifferenceLiquidation()
    {
        return _differenceLiquidation;
    }

    /**
     * @param differenceLiquidation С ликвидацией разницы в учебных планах. Свойство не может быть null.
     */
    public void setDifferenceLiquidation(boolean differenceLiquidation)
    {
        dirty(_differenceLiquidation, differenceLiquidation);
        _differenceLiquidation = differenceLiquidation;
    }

    /**
     * @return Дата ликвидации разницы в учебных планах.
     */
    public Date getDifferenceLiquidationDate()
    {
        return _differenceLiquidationDate;
    }

    /**
     * @param differenceLiquidationDate Дата ликвидации разницы в учебных планах.
     */
    public void setDifferenceLiquidationDate(Date differenceLiquidationDate)
    {
        dirty(_differenceLiquidationDate, differenceLiquidationDate);
        _differenceLiquidationDate = differenceLiquidationDate;
    }

    /**
     * @return С ликвидацией академической задолженности. Свойство не может быть null.
     */
    @NotNull
    public boolean isDebtLiquidation()
    {
        return _debtLiquidation;
    }

    /**
     * @param debtLiquidation С ликвидацией академической задолженности. Свойство не может быть null.
     */
    public void setDebtLiquidation(boolean debtLiquidation)
    {
        dirty(_debtLiquidation, debtLiquidation);
        _debtLiquidation = debtLiquidation;
    }

    /**
     * @return Дата ликвидации академической задолженности.
     */
    public Date getDebtLiquidationDate()
    {
        return _debtLiquidationDate;
    }

    /**
     * @param debtLiquidationDate Дата ликвидации академической задолженности.
     */
    public void setDebtLiquidationDate(Date debtLiquidationDate)
    {
        dirty(_debtLiquidationDate, debtLiquidationDate);
        _debtLiquidationDate = debtLiquidationDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof RepresentWeekendOutGen)
        {
            setAhead(((RepresentWeekendOut)another).getAhead());
            setNewFormativeOrgUnit(((RepresentWeekendOut)another).getNewFormativeOrgUnit());
            setNewCourse(((RepresentWeekendOut)another).getNewCourse());
            setNewGroup(((RepresentWeekendOut)another).getNewGroup());
            setNewEducationLevelsHighSchool(((RepresentWeekendOut)another).getNewEducationLevelsHighSchool());
            setNewDevelopForm(((RepresentWeekendOut)another).getNewDevelopForm());
            setEducationOrgUnit(((RepresentWeekendOut)another).getEducationOrgUnit());
            setFormativeOrgUnitOld(((RepresentWeekendOut)another).getFormativeOrgUnitOld());
            setCourseOld(((RepresentWeekendOut)another).getCourseOld());
            setGroupOld(((RepresentWeekendOut)another).getGroupOld());
            setEducationLevelsHighSchoolOld(((RepresentWeekendOut)another).getEducationLevelsHighSchoolOld());
            setDevelopFormOld(((RepresentWeekendOut)another).getDevelopFormOld());
            setEducationOrgUnitOld(((RepresentWeekendOut)another).getEducationOrgUnitOld());
            setDifferenceLiquidation(((RepresentWeekendOut)another).isDifferenceLiquidation());
            setDifferenceLiquidationDate(((RepresentWeekendOut)another).getDifferenceLiquidationDate());
            setDebtLiquidation(((RepresentWeekendOut)another).isDebtLiquidation());
            setDebtLiquidationDate(((RepresentWeekendOut)another).getDebtLiquidationDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RepresentWeekendOutGen> extends Representation.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RepresentWeekendOut.class;
        }

        public T newInstance()
        {
            return (T) new RepresentWeekendOut();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "ahead":
                    return obj.getAhead();
                case "newFormativeOrgUnit":
                    return obj.getNewFormativeOrgUnit();
                case "newCourse":
                    return obj.getNewCourse();
                case "newGroup":
                    return obj.getNewGroup();
                case "newEducationLevelsHighSchool":
                    return obj.getNewEducationLevelsHighSchool();
                case "newDevelopForm":
                    return obj.getNewDevelopForm();
                case "educationOrgUnit":
                    return obj.getEducationOrgUnit();
                case "formativeOrgUnitOld":
                    return obj.getFormativeOrgUnitOld();
                case "courseOld":
                    return obj.getCourseOld();
                case "groupOld":
                    return obj.getGroupOld();
                case "educationLevelsHighSchoolOld":
                    return obj.getEducationLevelsHighSchoolOld();
                case "developFormOld":
                    return obj.getDevelopFormOld();
                case "educationOrgUnitOld":
                    return obj.getEducationOrgUnitOld();
                case "differenceLiquidation":
                    return obj.isDifferenceLiquidation();
                case "differenceLiquidationDate":
                    return obj.getDifferenceLiquidationDate();
                case "debtLiquidation":
                    return obj.isDebtLiquidation();
                case "debtLiquidationDate":
                    return obj.getDebtLiquidationDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "ahead":
                    obj.setAhead((Boolean) value);
                    return;
                case "newFormativeOrgUnit":
                    obj.setNewFormativeOrgUnit((OrgUnit) value);
                    return;
                case "newCourse":
                    obj.setNewCourse((Course) value);
                    return;
                case "newGroup":
                    obj.setNewGroup((Group) value);
                    return;
                case "newEducationLevelsHighSchool":
                    obj.setNewEducationLevelsHighSchool((EducationLevelsHighSchool) value);
                    return;
                case "newDevelopForm":
                    obj.setNewDevelopForm((DevelopForm) value);
                    return;
                case "educationOrgUnit":
                    obj.setEducationOrgUnit((EducationOrgUnit) value);
                    return;
                case "formativeOrgUnitOld":
                    obj.setFormativeOrgUnitOld((OrgUnit) value);
                    return;
                case "courseOld":
                    obj.setCourseOld((Course) value);
                    return;
                case "groupOld":
                    obj.setGroupOld((Group) value);
                    return;
                case "educationLevelsHighSchoolOld":
                    obj.setEducationLevelsHighSchoolOld((EducationLevelsHighSchool) value);
                    return;
                case "developFormOld":
                    obj.setDevelopFormOld((DevelopForm) value);
                    return;
                case "educationOrgUnitOld":
                    obj.setEducationOrgUnitOld((EducationOrgUnit) value);
                    return;
                case "differenceLiquidation":
                    obj.setDifferenceLiquidation((Boolean) value);
                    return;
                case "differenceLiquidationDate":
                    obj.setDifferenceLiquidationDate((Date) value);
                    return;
                case "debtLiquidation":
                    obj.setDebtLiquidation((Boolean) value);
                    return;
                case "debtLiquidationDate":
                    obj.setDebtLiquidationDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "ahead":
                        return true;
                case "newFormativeOrgUnit":
                        return true;
                case "newCourse":
                        return true;
                case "newGroup":
                        return true;
                case "newEducationLevelsHighSchool":
                        return true;
                case "newDevelopForm":
                        return true;
                case "educationOrgUnit":
                        return true;
                case "formativeOrgUnitOld":
                        return true;
                case "courseOld":
                        return true;
                case "groupOld":
                        return true;
                case "educationLevelsHighSchoolOld":
                        return true;
                case "developFormOld":
                        return true;
                case "educationOrgUnitOld":
                        return true;
                case "differenceLiquidation":
                        return true;
                case "differenceLiquidationDate":
                        return true;
                case "debtLiquidation":
                        return true;
                case "debtLiquidationDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "ahead":
                    return true;
                case "newFormativeOrgUnit":
                    return true;
                case "newCourse":
                    return true;
                case "newGroup":
                    return true;
                case "newEducationLevelsHighSchool":
                    return true;
                case "newDevelopForm":
                    return true;
                case "educationOrgUnit":
                    return true;
                case "formativeOrgUnitOld":
                    return true;
                case "courseOld":
                    return true;
                case "groupOld":
                    return true;
                case "educationLevelsHighSchoolOld":
                    return true;
                case "developFormOld":
                    return true;
                case "educationOrgUnitOld":
                    return true;
                case "differenceLiquidation":
                    return true;
                case "differenceLiquidationDate":
                    return true;
                case "debtLiquidation":
                    return true;
                case "debtLiquidationDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "ahead":
                    return Boolean.class;
                case "newFormativeOrgUnit":
                    return OrgUnit.class;
                case "newCourse":
                    return Course.class;
                case "newGroup":
                    return Group.class;
                case "newEducationLevelsHighSchool":
                    return EducationLevelsHighSchool.class;
                case "newDevelopForm":
                    return DevelopForm.class;
                case "educationOrgUnit":
                    return EducationOrgUnit.class;
                case "formativeOrgUnitOld":
                    return OrgUnit.class;
                case "courseOld":
                    return Course.class;
                case "groupOld":
                    return Group.class;
                case "educationLevelsHighSchoolOld":
                    return EducationLevelsHighSchool.class;
                case "developFormOld":
                    return DevelopForm.class;
                case "educationOrgUnitOld":
                    return EducationOrgUnit.class;
                case "differenceLiquidation":
                    return Boolean.class;
                case "differenceLiquidationDate":
                    return Date.class;
                case "debtLiquidation":
                    return Boolean.class;
                case "debtLiquidationDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RepresentWeekendOut> _dslPath = new Path<RepresentWeekendOut>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RepresentWeekendOut");
    }
            

    /**
     * @return Досрочно.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#getAhead()
     */
    public static PropertyPath<Boolean> ahead()
    {
        return _dslPath.ahead();
    }

    /**
     * @return Новое формирующее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#getNewFormativeOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> newFormativeOrgUnit()
    {
        return _dslPath.newFormativeOrgUnit();
    }

    /**
     * @return Новый курс.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#getNewCourse()
     */
    public static Course.Path<Course> newCourse()
    {
        return _dslPath.newCourse();
    }

    /**
     * @return Новая группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#getNewGroup()
     */
    public static Group.Path<Group> newGroup()
    {
        return _dslPath.newGroup();
    }

    /**
     * @return Новое направление подготовки (специальность).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#getNewEducationLevelsHighSchool()
     */
    public static EducationLevelsHighSchool.Path<EducationLevelsHighSchool> newEducationLevelsHighSchool()
    {
        return _dslPath.newEducationLevelsHighSchool();
    }

    /**
     * @return Новая форма обучения.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#getNewDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> newDevelopForm()
    {
        return _dslPath.newDevelopForm();
    }

    /**
     * @return Новое направление подготовки (специальность).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#getEducationOrgUnit()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
    {
        return _dslPath.educationOrgUnit();
    }

    /**
     * @return Старое формирующее подразделение.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#getFormativeOrgUnitOld()
     */
    public static OrgUnit.Path<OrgUnit> formativeOrgUnitOld()
    {
        return _dslPath.formativeOrgUnitOld();
    }

    /**
     * @return Старый курс.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#getCourseOld()
     */
    public static Course.Path<Course> courseOld()
    {
        return _dslPath.courseOld();
    }

    /**
     * @return Старая группа.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#getGroupOld()
     */
    public static Group.Path<Group> groupOld()
    {
        return _dslPath.groupOld();
    }

    /**
     * @return Старое направление подготовки (специальность).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#getEducationLevelsHighSchoolOld()
     */
    public static EducationLevelsHighSchool.Path<EducationLevelsHighSchool> educationLevelsHighSchoolOld()
    {
        return _dslPath.educationLevelsHighSchoolOld();
    }

    /**
     * @return Старая форма обучения.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#getDevelopFormOld()
     */
    public static DevelopForm.Path<DevelopForm> developFormOld()
    {
        return _dslPath.developFormOld();
    }

    /**
     * @return Старое направление подготовки (специальность).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#getEducationOrgUnitOld()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitOld()
    {
        return _dslPath.educationOrgUnitOld();
    }

    /**
     * @return С ликвидацией разницы в учебных планах. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#isDifferenceLiquidation()
     */
    public static PropertyPath<Boolean> differenceLiquidation()
    {
        return _dslPath.differenceLiquidation();
    }

    /**
     * @return Дата ликвидации разницы в учебных планах.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#getDifferenceLiquidationDate()
     */
    public static PropertyPath<Date> differenceLiquidationDate()
    {
        return _dslPath.differenceLiquidationDate();
    }

    /**
     * @return С ликвидацией академической задолженности. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#isDebtLiquidation()
     */
    public static PropertyPath<Boolean> debtLiquidation()
    {
        return _dslPath.debtLiquidation();
    }

    /**
     * @return Дата ликвидации академической задолженности.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#getDebtLiquidationDate()
     */
    public static PropertyPath<Date> debtLiquidationDate()
    {
        return _dslPath.debtLiquidationDate();
    }

    public static class Path<E extends RepresentWeekendOut> extends Representation.Path<E>
    {
        private PropertyPath<Boolean> _ahead;
        private OrgUnit.Path<OrgUnit> _newFormativeOrgUnit;
        private Course.Path<Course> _newCourse;
        private Group.Path<Group> _newGroup;
        private EducationLevelsHighSchool.Path<EducationLevelsHighSchool> _newEducationLevelsHighSchool;
        private DevelopForm.Path<DevelopForm> _newDevelopForm;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnit;
        private OrgUnit.Path<OrgUnit> _formativeOrgUnitOld;
        private Course.Path<Course> _courseOld;
        private Group.Path<Group> _groupOld;
        private EducationLevelsHighSchool.Path<EducationLevelsHighSchool> _educationLevelsHighSchoolOld;
        private DevelopForm.Path<DevelopForm> _developFormOld;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnitOld;
        private PropertyPath<Boolean> _differenceLiquidation;
        private PropertyPath<Date> _differenceLiquidationDate;
        private PropertyPath<Boolean> _debtLiquidation;
        private PropertyPath<Date> _debtLiquidationDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Досрочно.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#getAhead()
     */
        public PropertyPath<Boolean> ahead()
        {
            if(_ahead == null )
                _ahead = new PropertyPath<Boolean>(RepresentWeekendOutGen.P_AHEAD, this);
            return _ahead;
        }

    /**
     * @return Новое формирующее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#getNewFormativeOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> newFormativeOrgUnit()
        {
            if(_newFormativeOrgUnit == null )
                _newFormativeOrgUnit = new OrgUnit.Path<OrgUnit>(L_NEW_FORMATIVE_ORG_UNIT, this);
            return _newFormativeOrgUnit;
        }

    /**
     * @return Новый курс.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#getNewCourse()
     */
        public Course.Path<Course> newCourse()
        {
            if(_newCourse == null )
                _newCourse = new Course.Path<Course>(L_NEW_COURSE, this);
            return _newCourse;
        }

    /**
     * @return Новая группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#getNewGroup()
     */
        public Group.Path<Group> newGroup()
        {
            if(_newGroup == null )
                _newGroup = new Group.Path<Group>(L_NEW_GROUP, this);
            return _newGroup;
        }

    /**
     * @return Новое направление подготовки (специальность).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#getNewEducationLevelsHighSchool()
     */
        public EducationLevelsHighSchool.Path<EducationLevelsHighSchool> newEducationLevelsHighSchool()
        {
            if(_newEducationLevelsHighSchool == null )
                _newEducationLevelsHighSchool = new EducationLevelsHighSchool.Path<EducationLevelsHighSchool>(L_NEW_EDUCATION_LEVELS_HIGH_SCHOOL, this);
            return _newEducationLevelsHighSchool;
        }

    /**
     * @return Новая форма обучения.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#getNewDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> newDevelopForm()
        {
            if(_newDevelopForm == null )
                _newDevelopForm = new DevelopForm.Path<DevelopForm>(L_NEW_DEVELOP_FORM, this);
            return _newDevelopForm;
        }

    /**
     * @return Новое направление подготовки (специальность).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#getEducationOrgUnit()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
        {
            if(_educationOrgUnit == null )
                _educationOrgUnit = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT, this);
            return _educationOrgUnit;
        }

    /**
     * @return Старое формирующее подразделение.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#getFormativeOrgUnitOld()
     */
        public OrgUnit.Path<OrgUnit> formativeOrgUnitOld()
        {
            if(_formativeOrgUnitOld == null )
                _formativeOrgUnitOld = new OrgUnit.Path<OrgUnit>(L_FORMATIVE_ORG_UNIT_OLD, this);
            return _formativeOrgUnitOld;
        }

    /**
     * @return Старый курс.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#getCourseOld()
     */
        public Course.Path<Course> courseOld()
        {
            if(_courseOld == null )
                _courseOld = new Course.Path<Course>(L_COURSE_OLD, this);
            return _courseOld;
        }

    /**
     * @return Старая группа.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#getGroupOld()
     */
        public Group.Path<Group> groupOld()
        {
            if(_groupOld == null )
                _groupOld = new Group.Path<Group>(L_GROUP_OLD, this);
            return _groupOld;
        }

    /**
     * @return Старое направление подготовки (специальность).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#getEducationLevelsHighSchoolOld()
     */
        public EducationLevelsHighSchool.Path<EducationLevelsHighSchool> educationLevelsHighSchoolOld()
        {
            if(_educationLevelsHighSchoolOld == null )
                _educationLevelsHighSchoolOld = new EducationLevelsHighSchool.Path<EducationLevelsHighSchool>(L_EDUCATION_LEVELS_HIGH_SCHOOL_OLD, this);
            return _educationLevelsHighSchoolOld;
        }

    /**
     * @return Старая форма обучения.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#getDevelopFormOld()
     */
        public DevelopForm.Path<DevelopForm> developFormOld()
        {
            if(_developFormOld == null )
                _developFormOld = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM_OLD, this);
            return _developFormOld;
        }

    /**
     * @return Старое направление подготовки (специальность).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#getEducationOrgUnitOld()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitOld()
        {
            if(_educationOrgUnitOld == null )
                _educationOrgUnitOld = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT_OLD, this);
            return _educationOrgUnitOld;
        }

    /**
     * @return С ликвидацией разницы в учебных планах. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#isDifferenceLiquidation()
     */
        public PropertyPath<Boolean> differenceLiquidation()
        {
            if(_differenceLiquidation == null )
                _differenceLiquidation = new PropertyPath<Boolean>(RepresentWeekendOutGen.P_DIFFERENCE_LIQUIDATION, this);
            return _differenceLiquidation;
        }

    /**
     * @return Дата ликвидации разницы в учебных планах.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#getDifferenceLiquidationDate()
     */
        public PropertyPath<Date> differenceLiquidationDate()
        {
            if(_differenceLiquidationDate == null )
                _differenceLiquidationDate = new PropertyPath<Date>(RepresentWeekendOutGen.P_DIFFERENCE_LIQUIDATION_DATE, this);
            return _differenceLiquidationDate;
        }

    /**
     * @return С ликвидацией академической задолженности. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#isDebtLiquidation()
     */
        public PropertyPath<Boolean> debtLiquidation()
        {
            if(_debtLiquidation == null )
                _debtLiquidation = new PropertyPath<Boolean>(RepresentWeekendOutGen.P_DEBT_LIQUIDATION, this);
            return _debtLiquidation;
        }

    /**
     * @return Дата ликвидации академической задолженности.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut#getDebtLiquidationDate()
     */
        public PropertyPath<Date> debtLiquidationDate()
        {
            if(_debtLiquidationDate == null )
                _debtLiquidationDate = new PropertyPath<Date>(RepresentWeekendOutGen.P_DEBT_LIQUIDATION_DATE, this);
            return _debtLiquidationDate;
        }

        public Class getEntityClass()
        {
            return RepresentWeekendOut.class;
        }

        public String getEntityName()
        {
            return "representWeekendOut";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
