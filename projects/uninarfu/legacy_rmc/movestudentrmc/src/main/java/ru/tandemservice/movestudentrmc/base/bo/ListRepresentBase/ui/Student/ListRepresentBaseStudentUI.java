package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ui.Student;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.RepresentationDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.List.DORepresentBaseListUI;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ListRepresentBaseManager;
import ru.tandemservice.movestudentrmc.docord.IListObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.uni.util.ReportRenderer;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@State({
        @Bind(key = DORepresentBaseListUI.STUDENT_LIST_ID, binding = DORepresentBaseListUI.STUDENT_LIST_ID)
})
public class ListRepresentBaseStudentUI extends UIPresenter {


    private List<Long> studentListId;


    @Override
    public void onComponentRefresh()
    {

    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {

        if (ListRepresentBaseStudent.LIST_REPRESENT_DS.equals(dataSource.getName())) {

            if (studentListId != null && !studentListId.isEmpty())
                dataSource.put(RepresentationDSHandler.STUDENT_ID_FILTER, studentListId.get(0));

            Map<String, Object> settingMap = _uiSettings.getAsMap(
                    "dateFormativeFromFilter",
                    "dateFormativeToFilter",
                    "dateStartFromFilter",
                    "dateStartToFilter"
            );
            dataSource.putAll(settingMap);
        }
    }

    @Override
    public void saveSettings() {
        /*
		        DateFormattingUtil.validateDatesPeriod(getSettings(), OrderDSHandler.DATE_FORMATIVE_FROM_FILTER, OrderDSHandler.DATE_FORMATIVE_TO_FILTER);
        DateFormattingUtil.validateDatesPeriod(getSettings(), OrderDSHandler.DATE_COMMIT_FROM_FILTER, OrderDSHandler.DATE_COMMIT_TO_FILTER);
        DateFormattingUtil.validateDatesPeriod(getSettings(), OrderDSHandler.DATE_START_FROM_FILTER, OrderDSHandler.DATE_START_TO_FILTER);
		 */
        super.saveSettings();
    }

    public void onClickPrintFromList() {

        try {
            IDocumentRenderer doc = documentRenderer();
            if (doc != null)
                BusinessComponentUtils.downloadDocument(doc, true);
        }
        catch (Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }


    public Boolean getIsFormative() {

        BaseSearchListDataSource representDS = (BaseSearchListDataSource) getConfig().getDataSource(ListRepresentBaseStudent.LIST_REPRESENT_DS);

        IEntity e = representDS.getCurrentEntity();
        ListRepresent listRepresent = ((ListRepresent) e);
        return !listRepresent.getState().getCode().equals("1");
    }

    private IDocumentRenderer documentRenderer() {
        ListRepresent listRepresent = DataAccessServices.dao().get(getListenerParameterAsLong());
        if (listRepresent.getDocument() == null) {
            IListObjectByRepresentTypeManager manager = ListRepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(listRepresent.getRepresentationType().getCode());

            RtfDocument docMain = RtfBean.getElementFactory().createRtfDocument();
            manager.getListObjectModifyDAO().buildBodyRepresent(Collections.singletonList(listRepresent), docMain);

            return new ReportRenderer("Списочное представление.rtf", docMain, false);
        }
        else
            return new ReportRenderer("Списочное представление.rtf", listRepresent.getDocument(), false);
    }

    public List<Long> getStudentListId() {
        return studentListId;
    }

    public void setStudentListId(List<Long> studentListId) {
        this.studentListId = studentListId;
    }

}
