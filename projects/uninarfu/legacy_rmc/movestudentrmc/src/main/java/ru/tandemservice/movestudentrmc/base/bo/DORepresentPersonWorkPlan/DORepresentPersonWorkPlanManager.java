package ru.tandemservice.movestudentrmc.base.bo.DORepresentPersonWorkPlan;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentPersonWorkPlan.logic.DORepresentPersonWorkPlanDAO;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentPersonWorkPlan.ui.Edit.DORepresentPersonWorkPlanEdit;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentPersonWorkPlan.ui.View.DORepresentPersonWorkPlanView;
import ru.tandemservice.movestudentrmc.docord.IDOObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IDOObjectModifyDAO;

@Configuration
public class DORepresentPersonWorkPlanManager extends BusinessObjectManager implements IDOObjectByRepresentTypeManager {

    public static DORepresentPersonWorkPlanManager instance()
    {
        return instance(DORepresentPersonWorkPlanManager.class);
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return DORepresentPersonWorkPlanEdit.class;
    }

    @Bean
    public IDOObjectModifyDAO getDOObjectModifyDAO() {
        return new DORepresentPersonWorkPlanDAO();
    }

    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return DORepresentPersonWorkPlanView.class;
    }
}
