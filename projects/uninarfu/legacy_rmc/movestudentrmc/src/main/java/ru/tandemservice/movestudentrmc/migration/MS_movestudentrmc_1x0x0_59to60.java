package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_movestudentrmc_1x0x0_59to60 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.5.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.5.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность listRepresentQualificationThemes

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("listrepresentqualthemes_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey()
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("listRepresentQualificationThemes");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность studentQualificationThemes

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("studentqualificationthemes_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("student_id", DBType.LONG).setNullable(false),
                                      new DBColumn("advisor_p", DBType.createVarchar(255)),
                                      new DBColumn("theme_p", DBType.TEXT),
                                      new DBColumn("listrepresent_id", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("studentQualificationThemes");

        }


    }
}