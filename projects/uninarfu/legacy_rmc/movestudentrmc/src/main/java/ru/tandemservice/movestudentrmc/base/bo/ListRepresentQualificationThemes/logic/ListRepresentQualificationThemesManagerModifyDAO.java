package ru.tandemservice.movestudentrmc.base.bo.ListRepresentQualificationThemes.logic;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.AbstractListRepresentDAO;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.util.RepresentationUtil;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

//-------------------------------------------------------------------
import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
//-------------------------------------------------------------------

public class ListRepresentQualificationThemesManagerModifyDAO extends AbstractListRepresentDAO
{

    public void save(ListRepresent listRepresent, List<Student> studentSelectedList, DocListRepresentBasics representBasics) {
    }

    @Override
    public boolean doCommit(ListRepresent represent, ErrorCollector error) {
        if (!super.doCommit(represent, UserContext.getInstance().getErrorCollector()))
            return false;

        ListRepresentQualificationThemes listRepresent = (ListRepresentQualificationThemes) represent;
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(StudentQualificationThemes.class, "s");
        dql.where(DQLExpressions.eq(DQLExpressions.property(StudentQualificationThemes.listRepresent().fromAlias("s")), DQLExpressions.value(listRepresent)));
        List<StudentQualificationThemes> list = IUniBaseDao.instance.get().getList(dql);
        for (StudentQualificationThemes theme : list) {
            theme.setOldTheme(theme.getStudent().getFinalQualifyingWorkTheme());
            baseUpdate(theme);
            StudentVKRTheme studentTheme = RepresentationUtil.getStudentVkrThemeLong(theme.getStudent());
            studentTheme.setTheme(theme.getTheme());
            baseUpdate(studentTheme);
            //сохраняем тему в студенте
            Student student = theme.getStudent();
            student.setFinalQualifyingWorkTheme(theme.getTheme());
            baseUpdate(student);
        }

        return true;
    }

    @Override
    public boolean doRollback(ListRepresent represent, ErrorCollector error) {
        if (!super.doRollback(represent, UserContext.getInstance().getErrorCollector()))
            return false;
        ListRepresentQualificationThemes listRepresent = (ListRepresentQualificationThemes) represent;
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(StudentQualificationThemes.class, "s");
        dql.where(DQLExpressions.eq(DQLExpressions.property(StudentQualificationThemes.listRepresent().fromAlias("s")), DQLExpressions.value(listRepresent)));
        List<StudentQualificationThemes> list = IUniBaseDao.instance.get().getList(dql);
        for (StudentQualificationThemes theme : list) {
            StudentVKRTheme studentTheme = RepresentationUtil.getStudentVkrThemeLong(theme.getStudent());
            studentTheme.setTheme(theme.getOldTheme());
            baseUpdate(studentTheme);

            //откатываем студенту тему
            Student student = theme.getStudent();
            student.setFinalQualifyingWorkTheme(theme.getOldTheme());
            baseUpdate(student);
        }
        //удалим статус, который назначили этим представлением
        return true;
    }

    @Transactional
    public void saveSupport(ListRepresent listRepresent, List<DataWrapper> studentSelectedList, DocListRepresentBasics representBasics) {

        ListRepresentQualificationThemes listQualificationThemes = (ListRepresentQualificationThemes) listRepresent;

        List<Student> stuSelectedList = new ArrayList<>();
        List<StudentQualificationThemes> qualificationThemesDataList = new ArrayList<>();
        for (DataWrapper dataW : studentSelectedList) {
            Student student = (Student) dataW.getProperty("student");
            StudentQualificationThemes StudentQualificationThemes = (StudentQualificationThemes) dataW.getProperty("qualificationThemes");
            StudentQualificationThemes.setListRepresent(listRepresent);
            qualificationThemesDataList.add(StudentQualificationThemes);
            stuSelectedList.add(student);
        }

        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(RelListRepresentStudents.class)
                .fromEntity(RelListRepresentStudents.class, "rls")
                .where(DQLExpressions.eq
                        (DQLExpressions.property(RelListRepresentStudents.representation()), DQLExpressions.value(listRepresent)));

        //Почистим старых студентов
        deleteBuilder.createStatement(getSession()).execute();

        //Сохраним оператора, изменившего представление
        Person person = PersonManager.instance().dao().getPerson(ContextLocal.getUserContext().getPrincipalContext());
        if (person != null)
            listQualificationThemes.setOperator(person);

        IPrincipalContext context = ContextLocal.getUserContext().getPrincipalContext();
        listQualificationThemes.setCreator(context);

        baseCreateOrUpdate(listRepresent);
        baseCreateOrUpdate(representBasics);
        for (Student student : stuSelectedList) {
            RelListRepresentStudents relListRepresentGrantStudents = new RelListRepresentStudents();
            relListRepresentGrantStudents.setRepresentation(listRepresent);
            relListRepresentGrantStudents.setStudent(student);
            baseCreateOrUpdate(relListRepresentGrantStudents);
        }

        deleteStudentQualificationThemes(listQualificationThemes);
        saveStudentQualificationThemes(qualificationThemesDataList);
    }

    public void deleteStudentQualificationThemes(ListRepresent represent) {
        List<StudentQualificationThemes> dataList = DataAccessServices.dao().getList(StudentQualificationThemes.class, StudentQualificationThemes.listRepresent(), represent);
        for (StudentQualificationThemes data : dataList)
            DataAccessServices.dao().delete(data);
    }

    public void saveStudentQualificationThemes(List<StudentQualificationThemes> qualificationThemesDataList) {
        for (StudentQualificationThemes data : qualificationThemesDataList)
            DataAccessServices.dao().save(data);
    }

    @Override
    public void buildBodyExtract(ListOrdListRepresent listOrdListRepresent, Student student, RtfDocument docExtract, Map<Student, OrderParagraphInfo> map) {
        super.buildBodyExtract(listOrdListRepresent, student, docExtract, map);

        RtfInjectModifier im = new RtfInjectModifier();
        ListPrintDoc.injectModifier(im, student);
        
        //---------------------------------------------------------------------------------------------------------------------
        DQLSelectBuilder dql = new DQLSelectBuilder()
        		.fromEntity(StudentQualificationThemes.class, "spd")
        		.where(eq(property(StudentQualificationThemes.listRepresent().fromAlias("spd")), value(listOrdListRepresent.getRepresentation())))
        		.where(eq(property(StudentQualificationThemes.student().fromAlias("spd")),value(student)));
        List<StudentQualificationThemes> qualificationThemesList = UniDaoFacade.getCoreDao().getList(dql);
        
        
        RtfTableModifier tm = new RtfTableModifier();
        List<String[]> tableList = new ArrayList<>();
        String[] row = new String[4];    
        row[0] = "" + map.get(student).getStudentNumber();
        row[1] = student.getPerson().getFullFio();
        if (qualificationThemesList.get(0) != null)
        {
            row[2] = StringUtils.trimToEmpty(qualificationThemesList.get(0).getTheme());
            row[3] = StringUtils.trimToEmpty(qualificationThemesList.get(0).getAdvisor());
        }        
        tableList.add(row);
        tm.put("T", tableList.toArray(new String[][]{}));
        tm.modify(docExtract);
        //---------------------------------------------------------------------------------------------------------------------
        
        im.modify(docExtract);
    }

    @Override
    public Map<Student, OrderParagraphInfo> buildBodyRepresent(List<ListRepresent> representationBase, RtfDocument document) {

        ListPrintDoc listPrintDoc = new ListPrintDoc();
        RtfDocument rtfDocument = listPrintDoc.creationPrintDocOrder(representationBase);

        document.getElementList().addAll(rtfDocument.getElementList());

        return listPrintDoc.getParagInfomap();
    }

    @Override
    public List<Student> selectRepresent(ListRepresent listRepresent) {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(RelListRepresentStudents.class, "rls");
        builder.where(DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("rls")), DQLExpressions.value(listRepresent)));
        builder.column(DQLExpressions.property(RelListRepresentStudents.student().fromAlias("rls")));

        return builder.createStatement(getSession()).list();
    }
}
