package ru.tandemservice.movestudentrmc.docord;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

//import ru.tandemservice.movestudentrmc.entity.DocumentRepresentationBase;

public interface IDOObjectModifyDAO {

    Representation createRepresentObject(RepresentationType type, List<Student> student);

    boolean doCommit(Representation represent, ErrorCollector error);

    boolean doRollback(Representation represent, ErrorCollector error);

    void buildBodyExtract(Representation representationBase, String itemFac, RtfDocument docCommon);

    void buildBodyRepresent(Representation representationBase, RtfDocument document);

    void buildBodyRepresent(Representation representationBase, RtfDocument document, AtomicInteger parNumber, AtomicInteger extractNumber);

    void buildBodyDeclaration(Representation representationBase, RtfDocument document);

    RtfDocument getTemplateExtract(ICatalogItem type);

    RtfDocument getTemplateRepresent(ICatalogItem type);

    RtfDocument getTemplateDeclaration(ICatalogItem type);

    GrammaCase getGrammaCase();

}
