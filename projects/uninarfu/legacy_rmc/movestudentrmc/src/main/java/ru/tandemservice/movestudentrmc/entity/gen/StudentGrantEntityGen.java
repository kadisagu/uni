package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntity;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import ru.tandemservice.movestudentrmc.entity.catalog.StuGrantStatus;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Стипендия/выплата студенту
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentGrantEntityGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.StudentGrantEntity";
    public static final String ENTITY_NAME = "studentGrantEntity";
    public static final int VERSION_HASH = -1190354247;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT = "student";
    public static final String L_VIEW = "view";
    public static final String L_EDU_YEAR = "eduYear";
    public static final String P_MONTH = "month";
    public static final String P_SUM = "sum";
    public static final String L_STATUS = "status";
    public static final String P_ORDER_NUMBER = "orderNumber";
    public static final String P_ORDER_DATE = "orderDate";
    public static final String P_TMP = "tmp";
    public static final String L_LIST_REPRESENT = "listRepresent";
    public static final String L_REPRESENTATION = "representation";

    private Student _student;     // Студент
    private GrantView _view;     // Вид стипендии
    private EducationYear _eduYear;     // Учебный год
    private String _month;     // Месяц
    private double _sum;     // Сумма
    private StuGrantStatus _status;     // Статус
    private String _orderNumber;     // Номер приказа
    private Date _orderDate;     // Дата приказа
    private boolean _tmp;     // Временный(скрытый)
    private ListRepresent _listRepresent;     // Списочное представление, которое назначает эту стипендию
    private Representation _representation;     // Индивидуальное представление, которое назначает эту стипендию

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Вид стипендии. Свойство не может быть null.
     */
    @NotNull
    public GrantView getView()
    {
        return _view;
    }

    /**
     * @param view Вид стипендии. Свойство не может быть null.
     */
    public void setView(GrantView view)
    {
        dirty(_view, view);
        _view = view;
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getEduYear()
    {
        return _eduYear;
    }

    /**
     * @param eduYear Учебный год. Свойство не может быть null.
     */
    public void setEduYear(EducationYear eduYear)
    {
        dirty(_eduYear, eduYear);
        _eduYear = eduYear;
    }

    /**
     * @return Месяц. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getMonth()
    {
        return _month;
    }

    /**
     * @param month Месяц. Свойство не может быть null.
     */
    public void setMonth(String month)
    {
        dirty(_month, month);
        _month = month;
    }

    /**
     * @return Сумма. Свойство не может быть null.
     */
    @NotNull
    public double getSum()
    {
        return _sum;
    }

    /**
     * @param sum Сумма. Свойство не может быть null.
     */
    public void setSum(double sum)
    {
        dirty(_sum, sum);
        _sum = sum;
    }

    /**
     * @return Статус. Свойство не может быть null.
     */
    @NotNull
    public StuGrantStatus getStatus()
    {
        return _status;
    }

    /**
     * @param status Статус. Свойство не может быть null.
     */
    public void setStatus(StuGrantStatus status)
    {
        dirty(_status, status);
        _status = status;
    }

    /**
     * @return Номер приказа.
     */
    @Length(max=255)
    public String getOrderNumber()
    {
        return _orderNumber;
    }

    /**
     * @param orderNumber Номер приказа.
     */
    public void setOrderNumber(String orderNumber)
    {
        dirty(_orderNumber, orderNumber);
        _orderNumber = orderNumber;
    }

    /**
     * @return Дата приказа.
     */
    public Date getOrderDate()
    {
        return _orderDate;
    }

    /**
     * @param orderDate Дата приказа.
     */
    public void setOrderDate(Date orderDate)
    {
        dirty(_orderDate, orderDate);
        _orderDate = orderDate;
    }

    /**
     * @return Временный(скрытый). Свойство не может быть null.
     */
    @NotNull
    public boolean isTmp()
    {
        return _tmp;
    }

    /**
     * @param tmp Временный(скрытый). Свойство не может быть null.
     */
    public void setTmp(boolean tmp)
    {
        dirty(_tmp, tmp);
        _tmp = tmp;
    }

    /**
     * @return Списочное представление, которое назначает эту стипендию.
     */
    public ListRepresent getListRepresent()
    {
        return _listRepresent;
    }

    /**
     * @param listRepresent Списочное представление, которое назначает эту стипендию.
     */
    public void setListRepresent(ListRepresent listRepresent)
    {
        dirty(_listRepresent, listRepresent);
        _listRepresent = listRepresent;
    }

    /**
     * @return Индивидуальное представление, которое назначает эту стипендию.
     */
    public Representation getRepresentation()
    {
        return _representation;
    }

    /**
     * @param representation Индивидуальное представление, которое назначает эту стипендию.
     */
    public void setRepresentation(Representation representation)
    {
        dirty(_representation, representation);
        _representation = representation;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentGrantEntityGen)
        {
            setStudent(((StudentGrantEntity)another).getStudent());
            setView(((StudentGrantEntity)another).getView());
            setEduYear(((StudentGrantEntity)another).getEduYear());
            setMonth(((StudentGrantEntity)another).getMonth());
            setSum(((StudentGrantEntity)another).getSum());
            setStatus(((StudentGrantEntity)another).getStatus());
            setOrderNumber(((StudentGrantEntity)another).getOrderNumber());
            setOrderDate(((StudentGrantEntity)another).getOrderDate());
            setTmp(((StudentGrantEntity)another).isTmp());
            setListRepresent(((StudentGrantEntity)another).getListRepresent());
            setRepresentation(((StudentGrantEntity)another).getRepresentation());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentGrantEntityGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentGrantEntity.class;
        }

        public T newInstance()
        {
            return (T) new StudentGrantEntity();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "student":
                    return obj.getStudent();
                case "view":
                    return obj.getView();
                case "eduYear":
                    return obj.getEduYear();
                case "month":
                    return obj.getMonth();
                case "sum":
                    return obj.getSum();
                case "status":
                    return obj.getStatus();
                case "orderNumber":
                    return obj.getOrderNumber();
                case "orderDate":
                    return obj.getOrderDate();
                case "tmp":
                    return obj.isTmp();
                case "listRepresent":
                    return obj.getListRepresent();
                case "representation":
                    return obj.getRepresentation();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "view":
                    obj.setView((GrantView) value);
                    return;
                case "eduYear":
                    obj.setEduYear((EducationYear) value);
                    return;
                case "month":
                    obj.setMonth((String) value);
                    return;
                case "sum":
                    obj.setSum((Double) value);
                    return;
                case "status":
                    obj.setStatus((StuGrantStatus) value);
                    return;
                case "orderNumber":
                    obj.setOrderNumber((String) value);
                    return;
                case "orderDate":
                    obj.setOrderDate((Date) value);
                    return;
                case "tmp":
                    obj.setTmp((Boolean) value);
                    return;
                case "listRepresent":
                    obj.setListRepresent((ListRepresent) value);
                    return;
                case "representation":
                    obj.setRepresentation((Representation) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "student":
                        return true;
                case "view":
                        return true;
                case "eduYear":
                        return true;
                case "month":
                        return true;
                case "sum":
                        return true;
                case "status":
                        return true;
                case "orderNumber":
                        return true;
                case "orderDate":
                        return true;
                case "tmp":
                        return true;
                case "listRepresent":
                        return true;
                case "representation":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "student":
                    return true;
                case "view":
                    return true;
                case "eduYear":
                    return true;
                case "month":
                    return true;
                case "sum":
                    return true;
                case "status":
                    return true;
                case "orderNumber":
                    return true;
                case "orderDate":
                    return true;
                case "tmp":
                    return true;
                case "listRepresent":
                    return true;
                case "representation":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "student":
                    return Student.class;
                case "view":
                    return GrantView.class;
                case "eduYear":
                    return EducationYear.class;
                case "month":
                    return String.class;
                case "sum":
                    return Double.class;
                case "status":
                    return StuGrantStatus.class;
                case "orderNumber":
                    return String.class;
                case "orderDate":
                    return Date.class;
                case "tmp":
                    return Boolean.class;
                case "listRepresent":
                    return ListRepresent.class;
                case "representation":
                    return Representation.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentGrantEntity> _dslPath = new Path<StudentGrantEntity>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentGrantEntity");
    }
            

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.StudentGrantEntity#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Вид стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.StudentGrantEntity#getView()
     */
    public static GrantView.Path<GrantView> view()
    {
        return _dslPath.view();
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.StudentGrantEntity#getEduYear()
     */
    public static EducationYear.Path<EducationYear> eduYear()
    {
        return _dslPath.eduYear();
    }

    /**
     * @return Месяц. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.StudentGrantEntity#getMonth()
     */
    public static PropertyPath<String> month()
    {
        return _dslPath.month();
    }

    /**
     * @return Сумма. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.StudentGrantEntity#getSum()
     */
    public static PropertyPath<Double> sum()
    {
        return _dslPath.sum();
    }

    /**
     * @return Статус. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.StudentGrantEntity#getStatus()
     */
    public static StuGrantStatus.Path<StuGrantStatus> status()
    {
        return _dslPath.status();
    }

    /**
     * @return Номер приказа.
     * @see ru.tandemservice.movestudentrmc.entity.StudentGrantEntity#getOrderNumber()
     */
    public static PropertyPath<String> orderNumber()
    {
        return _dslPath.orderNumber();
    }

    /**
     * @return Дата приказа.
     * @see ru.tandemservice.movestudentrmc.entity.StudentGrantEntity#getOrderDate()
     */
    public static PropertyPath<Date> orderDate()
    {
        return _dslPath.orderDate();
    }

    /**
     * @return Временный(скрытый). Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.StudentGrantEntity#isTmp()
     */
    public static PropertyPath<Boolean> tmp()
    {
        return _dslPath.tmp();
    }

    /**
     * @return Списочное представление, которое назначает эту стипендию.
     * @see ru.tandemservice.movestudentrmc.entity.StudentGrantEntity#getListRepresent()
     */
    public static ListRepresent.Path<ListRepresent> listRepresent()
    {
        return _dslPath.listRepresent();
    }

    /**
     * @return Индивидуальное представление, которое назначает эту стипендию.
     * @see ru.tandemservice.movestudentrmc.entity.StudentGrantEntity#getRepresentation()
     */
    public static Representation.Path<Representation> representation()
    {
        return _dslPath.representation();
    }

    public static class Path<E extends StudentGrantEntity> extends EntityPath<E>
    {
        private Student.Path<Student> _student;
        private GrantView.Path<GrantView> _view;
        private EducationYear.Path<EducationYear> _eduYear;
        private PropertyPath<String> _month;
        private PropertyPath<Double> _sum;
        private StuGrantStatus.Path<StuGrantStatus> _status;
        private PropertyPath<String> _orderNumber;
        private PropertyPath<Date> _orderDate;
        private PropertyPath<Boolean> _tmp;
        private ListRepresent.Path<ListRepresent> _listRepresent;
        private Representation.Path<Representation> _representation;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.StudentGrantEntity#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Вид стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.StudentGrantEntity#getView()
     */
        public GrantView.Path<GrantView> view()
        {
            if(_view == null )
                _view = new GrantView.Path<GrantView>(L_VIEW, this);
            return _view;
        }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.StudentGrantEntity#getEduYear()
     */
        public EducationYear.Path<EducationYear> eduYear()
        {
            if(_eduYear == null )
                _eduYear = new EducationYear.Path<EducationYear>(L_EDU_YEAR, this);
            return _eduYear;
        }

    /**
     * @return Месяц. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.StudentGrantEntity#getMonth()
     */
        public PropertyPath<String> month()
        {
            if(_month == null )
                _month = new PropertyPath<String>(StudentGrantEntityGen.P_MONTH, this);
            return _month;
        }

    /**
     * @return Сумма. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.StudentGrantEntity#getSum()
     */
        public PropertyPath<Double> sum()
        {
            if(_sum == null )
                _sum = new PropertyPath<Double>(StudentGrantEntityGen.P_SUM, this);
            return _sum;
        }

    /**
     * @return Статус. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.StudentGrantEntity#getStatus()
     */
        public StuGrantStatus.Path<StuGrantStatus> status()
        {
            if(_status == null )
                _status = new StuGrantStatus.Path<StuGrantStatus>(L_STATUS, this);
            return _status;
        }

    /**
     * @return Номер приказа.
     * @see ru.tandemservice.movestudentrmc.entity.StudentGrantEntity#getOrderNumber()
     */
        public PropertyPath<String> orderNumber()
        {
            if(_orderNumber == null )
                _orderNumber = new PropertyPath<String>(StudentGrantEntityGen.P_ORDER_NUMBER, this);
            return _orderNumber;
        }

    /**
     * @return Дата приказа.
     * @see ru.tandemservice.movestudentrmc.entity.StudentGrantEntity#getOrderDate()
     */
        public PropertyPath<Date> orderDate()
        {
            if(_orderDate == null )
                _orderDate = new PropertyPath<Date>(StudentGrantEntityGen.P_ORDER_DATE, this);
            return _orderDate;
        }

    /**
     * @return Временный(скрытый). Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.StudentGrantEntity#isTmp()
     */
        public PropertyPath<Boolean> tmp()
        {
            if(_tmp == null )
                _tmp = new PropertyPath<Boolean>(StudentGrantEntityGen.P_TMP, this);
            return _tmp;
        }

    /**
     * @return Списочное представление, которое назначает эту стипендию.
     * @see ru.tandemservice.movestudentrmc.entity.StudentGrantEntity#getListRepresent()
     */
        public ListRepresent.Path<ListRepresent> listRepresent()
        {
            if(_listRepresent == null )
                _listRepresent = new ListRepresent.Path<ListRepresent>(L_LIST_REPRESENT, this);
            return _listRepresent;
        }

    /**
     * @return Индивидуальное представление, которое назначает эту стипендию.
     * @see ru.tandemservice.movestudentrmc.entity.StudentGrantEntity#getRepresentation()
     */
        public Representation.Path<Representation> representation()
        {
            if(_representation == null )
                _representation = new Representation.Path<Representation>(L_REPRESENTATION, this);
            return _representation;
        }

        public Class getEntityClass()
        {
            return StudentGrantEntity.class;
        }

        public String getEntityName()
        {
            return "studentGrantEntity";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
