package ru.tandemservice.movestudentrmc.entity;

import org.tandemframework.shared.commonbase.dao.IEntityRelation;
import ru.tandemservice.movestudentrmc.entity.catalog.OrderCategory;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.movestudentrmc.entity.gen.RelOrderCategoryRepresentationTypeGen;

/**
 * Сущность-связь 'Категорией приказа'-'Типами представлений'
 */
public class RelOrderCategoryRepresentationType extends RelOrderCategoryRepresentationTypeGen implements IEntityRelation<OrderCategory, RepresentationType> {
}