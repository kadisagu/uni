package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListRepresentTravel;
import ru.tandemservice.movestudentrmc.entity.catalog.TravelPaymentData;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Списочное представление о направлении в поездку
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ListRepresentTravelGen extends ListRepresent
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.ListRepresentTravel";
    public static final String ENTITY_NAME = "listRepresentTravel";
    public static final int VERSION_HASH = 2108909414;
    private static IEntityMeta ENTITY_META;

    public static final String L_ADDRESS_COUNTRY = "addressCountry";
    public static final String L_ADDRESS_ITEM = "addressItem";
    public static final String P_ORGANIZATION = "organization";
    public static final String P_DAYS = "days";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_TARGET = "target";
    public static final String L_TRAVEL_PAYMENT_DATA = "travelPaymentData";

    private AddressCountry _addressCountry;     // Место поездки – страна
    private AddressItem _addressItem;     // Место поездки – населенный пункт
    private String _organization;     // Место поездки – организация
    private Integer _days;     // Количество дней
    private Date _beginDate;     // Дата начала поездки
    private Date _endDate;     // Дата окончания поездки
    private String _target;     // Цель поездки
    private TravelPaymentData _travelPaymentData;     // Данные об оплате проезда

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Место поездки – страна. Свойство не может быть null.
     */
    @NotNull
    public AddressCountry getAddressCountry()
    {
        return _addressCountry;
    }

    /**
     * @param addressCountry Место поездки – страна. Свойство не может быть null.
     */
    public void setAddressCountry(AddressCountry addressCountry)
    {
        dirty(_addressCountry, addressCountry);
        _addressCountry = addressCountry;
    }

    /**
     * @return Место поездки – населенный пункт.
     */
    public AddressItem getAddressItem()
    {
        return _addressItem;
    }

    /**
     * @param addressItem Место поездки – населенный пункт.
     */
    public void setAddressItem(AddressItem addressItem)
    {
        dirty(_addressItem, addressItem);
        _addressItem = addressItem;
    }

    /**
     * @return Место поездки – организация. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getOrganization()
    {
        return _organization;
    }

    /**
     * @param organization Место поездки – организация. Свойство не может быть null.
     */
    public void setOrganization(String organization)
    {
        dirty(_organization, organization);
        _organization = organization;
    }

    /**
     * @return Количество дней.
     */
    public Integer getDays()
    {
        return _days;
    }

    /**
     * @param days Количество дней.
     */
    public void setDays(Integer days)
    {
        dirty(_days, days);
        _days = days;
    }

    /**
     * @return Дата начала поездки. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала поездки. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания поездки. Свойство не может быть null.
     */
    @NotNull
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания поездки. Свойство не может быть null.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Цель поездки. Свойство не может быть null.
     */
    @NotNull
    @Length(max=1024)
    public String getTarget()
    {
        return _target;
    }

    /**
     * @param target Цель поездки. Свойство не может быть null.
     */
    public void setTarget(String target)
    {
        dirty(_target, target);
        _target = target;
    }

    /**
     * @return Данные об оплате проезда.
     */
    public TravelPaymentData getTravelPaymentData()
    {
        return _travelPaymentData;
    }

    /**
     * @param travelPaymentData Данные об оплате проезда.
     */
    public void setTravelPaymentData(TravelPaymentData travelPaymentData)
    {
        dirty(_travelPaymentData, travelPaymentData);
        _travelPaymentData = travelPaymentData;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ListRepresentTravelGen)
        {
            setAddressCountry(((ListRepresentTravel)another).getAddressCountry());
            setAddressItem(((ListRepresentTravel)another).getAddressItem());
            setOrganization(((ListRepresentTravel)another).getOrganization());
            setDays(((ListRepresentTravel)another).getDays());
            setBeginDate(((ListRepresentTravel)another).getBeginDate());
            setEndDate(((ListRepresentTravel)another).getEndDate());
            setTarget(((ListRepresentTravel)another).getTarget());
            setTravelPaymentData(((ListRepresentTravel)another).getTravelPaymentData());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ListRepresentTravelGen> extends ListRepresent.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ListRepresentTravel.class;
        }

        public T newInstance()
        {
            return (T) new ListRepresentTravel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "addressCountry":
                    return obj.getAddressCountry();
                case "addressItem":
                    return obj.getAddressItem();
                case "organization":
                    return obj.getOrganization();
                case "days":
                    return obj.getDays();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "target":
                    return obj.getTarget();
                case "travelPaymentData":
                    return obj.getTravelPaymentData();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "addressCountry":
                    obj.setAddressCountry((AddressCountry) value);
                    return;
                case "addressItem":
                    obj.setAddressItem((AddressItem) value);
                    return;
                case "organization":
                    obj.setOrganization((String) value);
                    return;
                case "days":
                    obj.setDays((Integer) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "target":
                    obj.setTarget((String) value);
                    return;
                case "travelPaymentData":
                    obj.setTravelPaymentData((TravelPaymentData) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "addressCountry":
                        return true;
                case "addressItem":
                        return true;
                case "organization":
                        return true;
                case "days":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "target":
                        return true;
                case "travelPaymentData":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "addressCountry":
                    return true;
                case "addressItem":
                    return true;
                case "organization":
                    return true;
                case "days":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "target":
                    return true;
                case "travelPaymentData":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "addressCountry":
                    return AddressCountry.class;
                case "addressItem":
                    return AddressItem.class;
                case "organization":
                    return String.class;
                case "days":
                    return Integer.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "target":
                    return String.class;
                case "travelPaymentData":
                    return TravelPaymentData.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ListRepresentTravel> _dslPath = new Path<ListRepresentTravel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ListRepresentTravel");
    }
            

    /**
     * @return Место поездки – страна. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentTravel#getAddressCountry()
     */
    public static AddressCountry.Path<AddressCountry> addressCountry()
    {
        return _dslPath.addressCountry();
    }

    /**
     * @return Место поездки – населенный пункт.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentTravel#getAddressItem()
     */
    public static AddressItem.Path<AddressItem> addressItem()
    {
        return _dslPath.addressItem();
    }

    /**
     * @return Место поездки – организация. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentTravel#getOrganization()
     */
    public static PropertyPath<String> organization()
    {
        return _dslPath.organization();
    }

    /**
     * @return Количество дней.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentTravel#getDays()
     */
    public static PropertyPath<Integer> days()
    {
        return _dslPath.days();
    }

    /**
     * @return Дата начала поездки. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentTravel#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания поездки. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentTravel#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Цель поездки. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentTravel#getTarget()
     */
    public static PropertyPath<String> target()
    {
        return _dslPath.target();
    }

    /**
     * @return Данные об оплате проезда.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentTravel#getTravelPaymentData()
     */
    public static TravelPaymentData.Path<TravelPaymentData> travelPaymentData()
    {
        return _dslPath.travelPaymentData();
    }

    public static class Path<E extends ListRepresentTravel> extends ListRepresent.Path<E>
    {
        private AddressCountry.Path<AddressCountry> _addressCountry;
        private AddressItem.Path<AddressItem> _addressItem;
        private PropertyPath<String> _organization;
        private PropertyPath<Integer> _days;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<String> _target;
        private TravelPaymentData.Path<TravelPaymentData> _travelPaymentData;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Место поездки – страна. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentTravel#getAddressCountry()
     */
        public AddressCountry.Path<AddressCountry> addressCountry()
        {
            if(_addressCountry == null )
                _addressCountry = new AddressCountry.Path<AddressCountry>(L_ADDRESS_COUNTRY, this);
            return _addressCountry;
        }

    /**
     * @return Место поездки – населенный пункт.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentTravel#getAddressItem()
     */
        public AddressItem.Path<AddressItem> addressItem()
        {
            if(_addressItem == null )
                _addressItem = new AddressItem.Path<AddressItem>(L_ADDRESS_ITEM, this);
            return _addressItem;
        }

    /**
     * @return Место поездки – организация. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentTravel#getOrganization()
     */
        public PropertyPath<String> organization()
        {
            if(_organization == null )
                _organization = new PropertyPath<String>(ListRepresentTravelGen.P_ORGANIZATION, this);
            return _organization;
        }

    /**
     * @return Количество дней.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentTravel#getDays()
     */
        public PropertyPath<Integer> days()
        {
            if(_days == null )
                _days = new PropertyPath<Integer>(ListRepresentTravelGen.P_DAYS, this);
            return _days;
        }

    /**
     * @return Дата начала поездки. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentTravel#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(ListRepresentTravelGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания поездки. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentTravel#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(ListRepresentTravelGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Цель поездки. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentTravel#getTarget()
     */
        public PropertyPath<String> target()
        {
            if(_target == null )
                _target = new PropertyPath<String>(ListRepresentTravelGen.P_TARGET, this);
            return _target;
        }

    /**
     * @return Данные об оплате проезда.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentTravel#getTravelPaymentData()
     */
        public TravelPaymentData.Path<TravelPaymentData> travelPaymentData()
        {
            if(_travelPaymentData == null )
                _travelPaymentData = new TravelPaymentData.Path<TravelPaymentData>(L_TRAVEL_PAYMENT_DATA, this);
            return _travelPaymentData;
        }

        public Class getEntityClass()
        {
            return ListRepresentTravel.class;
        }

        public String getEntityName()
        {
            return "listRepresentTravel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
