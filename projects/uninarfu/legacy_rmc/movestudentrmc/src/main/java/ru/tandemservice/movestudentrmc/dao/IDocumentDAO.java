package ru.tandemservice.movestudentrmc.dao;

import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentDocuments;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentIC;
import ru.tandemservice.movestudentrmc.entity.NarfuDocument;
import ru.tandemservice.movestudentrmc.entity.catalog.Document;
import ru.tandemservice.movestudentrmc.entity.catalog.DocumentKind;
import ru.tandemservice.movestudentrmc.entity.catalog.DocumentType;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Collection;
import java.util.List;

public interface IDocumentDAO extends IUniBaseDao {

    public List<DocumentKind> getKinds(DocumentType documentType);

    public List<Document> getDocuments(DocumentType documentType, DocumentKind documentKind);

    public void saveRelations(DocumentType documentType, DocumentKind documentKind, List<Document> documents);

    public List<DocumentType> getUsedTypes(Student student);

    public List<DocumentKind> getUsedKinds(Student student, DocumentType documentType);

    public List<Document> getUsedDocuments(Student student, DocumentType documentType, DocumentKind documentKind);

    public List<NarfuDocument> getUsedStudentDocuments(Student student, DocumentType documentType, DocumentKind documentKind, Document document);

    public List<NarfuDocument> getAllStudentDocuments(Student student);

    public List<DocRepresentStudentDocuments> getDocuments(Long representId);

    public List<Long> getIds(Collection<? extends IEntity> list);

    public List<DocRepresentStudentIC> getIdentityCards(Long representId);
}