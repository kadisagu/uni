package ru.tandemservice.movestudentrmc.base.bo.DORepresentTransfer.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentDAO;
import ru.tandemservice.movestudentrmc.docord.util.MoveStudentUtil;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.ListRepresentTransfer;
import ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents;
import ru.tandemservice.movestudentrmc.entity.RepresentTransfer;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntity;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.TypeTemplateRepresentCodes;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.codes.DevelopConditionCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

//--------------------------------------------------------------
import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentationTypeCodes;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentExtractStatesCodes;
//--------------------------------------------------------------

public class DORepresentTransferDAO extends AbstractDORepresentDAO
{

    @Override
    public boolean doRollback(Representation represent, ErrorCollector error) {
        if (!super.doRollback(represent, error))
            return false;

        List<DocRepresentStudentBase> studentList = getStudentList(represent);
        Student student = studentList.get(0).getStudent();
        RepresentTransfer rep = (RepresentTransfer) represent;

        Course course = null;
        if (rep.getOldCourseId() != null)
            course = get(rep.getOldCourseId());
        if (course == null)
            error.add("Не найден старый курс");

        Group group = null;
        if (rep.getOldGroupId() != null) {
            group = get(rep.getOldGroupId());
            if (group == null)
                error.add("Не найдена старая группа");
        }

        CompensationType compensationType = get(rep.getOldCompensationTypeId());

        EducationOrgUnit eduOrgUnit = null;
        if (rep.getOldEducationOrgUnitId() != null)
            eduOrgUnit = get(rep.getOldEducationOrgUnitId());
        if (eduOrgUnit == null)
            error.add("Не найдены старые данные");

        if (!error.isHasFieldErrors()) {
            student.setCourse(course);
            student.setGroup(group);

            student.setEducationOrgUnit(eduOrgUnit);
            student.setCompensationType(compensationType);
            
            //----------------------------------------------------------------------------------------------------
            //Сбросить галочку "Индивидуальное образование", если не было других приказов об утверждении ИУП
            if(rep.isIndividualPlan()) {
            	
            	//Проверка списочных приказов "О переводе" с параметром "Утвердить ИУП"
            	DQLSelectBuilder checkBuilder = new DQLSelectBuilder()
            			.fromEntity(RelListRepresentStudents.class, "rel")
            			.joinEntity("rel", DQLJoinType.inner, ListRepresentTransfer.class, "rep", eq(RelListRepresentStudents.representation().id().fromAlias("rel"), ListRepresentTransfer.id().fromAlias("rep")) )
            			.where(eq(property(RelListRepresentStudents.student().id().fromAlias("rel")), value(student.getId())))
            			//.where(not(eq(property(RelListRepresentStudents.representation().id().fromAlias("rel")), value(represent.getId()))))            			
            			.where(eq(property(RelListRepresentStudents.representation().state().code().fromAlias("rel")), value(MovestudentExtractStatesCodes.CODE_6)))
            			.where(eq(property(ListRepresentTransfer.individualPlan().fromAlias("rep")), value(true)))
            		    .column(property(RelListRepresentStudents.representation().id().fromAlias("rel")))
            			;
            	List<Long> checkListRepresentIdList = checkBuilder.createStatement(getSession()).list();
            	
            	//Проверка индивидуальных приказов "О переводе" с параметром "Утвердить ИУП", а также индивидуального приказа "Об утверждении ИУП"
            	DQLSelectBuilder builder = new DQLSelectBuilder()
            			.fromEntity(DocRepresentStudentBase.class, "doc")
            			.joinEntity("doc", DQLJoinType.left, RepresentTransfer.class, "rep", eq(DocRepresentStudentBase.representation().id().fromAlias("doc"), RepresentTransfer.id().fromAlias("rep")) )
            			.where(eq(property(DocRepresentStudentBase.student().id().fromAlias("doc")), value(student.getId())))
            			.where(not(eq(property(DocRepresentStudentBase.representation().id().fromAlias("doc")), value(rep.getId()))))            			
            			.where(eq(property(DocRepresentStudentBase.representation().state().code().fromAlias("doc")), value(MovestudentExtractStatesCodes.CODE_6)))
            			.where(or(
            				eq(property(DocRepresentStudentBase.representation().type().code().fromAlias("doc")), value(RepresentationTypeCodes.PERSON_WORK_PLAN)), 
            				and(
            					eq(property(DocRepresentStudentBase.representation().type().code().fromAlias("doc")), value(RepresentationTypeCodes.TRANSFER)), 
            					eq(property(RepresentTransfer.individualPlan().fromAlias("rep")), value(true))
            				)
            				))
            		    .column(property(DocRepresentStudentBase.representation().id().fromAlias("doc")));
            	List<Long> checkRepresentIdList = builder.createStatement(getSession()).list();
            	
            	if(checkListRepresentIdList.size() == 0 && checkRepresentIdList.size() == 0) {
            		student.setPersonalEducation(false);		//Дополнительные данные студента "Индивидуальное образование"
            	}              	
            }
            //----------------------------------------------------------------------------------------------------            

            getSession().saveOrUpdate(student);

            doRollback(student, rep, getSession());

            rollbackStudentGrantsCancel(represent);
        }
        else
            return false;

        return true;
    }

    @Override
    public boolean doCommit(Representation represent, ErrorCollector error) {
        if (!super.doCommit(represent, error))
            return false;

        List<DocRepresentStudentBase> studentList = getStudentList(represent);
        Student student = studentList.get(0).getStudent();
        RepresentTransfer rep = (RepresentTransfer) represent;

        EducationOrgUnit educationOrgUnit = rep.getEducationOrgUnit() == null ? rep.getGroup().getEducationOrgUnit() : rep.getEducationOrgUnit();

        List<Student> lst = MoveStudentUtil.checkStudentList(Arrays.asList(student), rep.getCompensationType(), educationOrgUnit.getDevelopForm());

        if (!lst.isEmpty()) {
            List<StudentGrantEntity> sgeList = MoveStudentUtil.getSrudentGrantEntityList(lst, represent.getStartDate());
            commitStudentGrantsCancel(represent, sgeList);
        }

        if (rep.getEducationOrgUnit() != null)
            student.setEducationOrgUnit(rep.getEducationOrgUnit());
        else
            student.setEducationOrgUnit(rep.getGroup().getEducationOrgUnit());
        student.setGroup(rep.getGroup());
        student.setCourse(rep.getCourse());
        student.setCompensationType(rep.getCompensationType());

        //----------------------------------------------------------------------------------------------------
        if(rep.isIndividualPlan()) {
        	student.setPersonalEducation(true);		//Дополнительные данные студента "Индивидуальное образование"
        }
        //----------------------------------------------------------------------------------------------------        

        getSession().saveOrUpdate(student);

        //проставляем новуе УП-РУП
        doCommit(student, rep, getSession());

        return true;
    }

    @Override
    public Representation createRepresentObject(RepresentationType type, List<Student> studentList) {
        RepresentTransfer result = new RepresentTransfer();
        result.setType(type);

        return result;
    }

    @Override
    public void buildBodyDeclaration(Representation representationBase, RtfDocument document) {
        super.buildBodyDeclaration(representationBase, document);
        RepresentTransfer rep = (RepresentTransfer) representationBase;

        RtfInjectModifier im = new RtfInjectModifier();
        if (rep.getEndDate() != null) {
            im.put("raznica", "Разница в учебных планах приведена в Приложении 1");

            RtfString str = new RtfString().append("Установить срок ликвидации разницы в учебных планах до ");

            im.put("endDate", UtilPrintSupport.getDateFormatterWithMonthStringAndYear(rep.getEndDate(), str));

            RtfDocument firstPage = document.getClone();
            document.getElementList().clear();

            document.getElementList().addAll(firstPage.getElementList());
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
            document.getElementList().addAll(this.createDeclarationAttachment(rep).getElementList());
        }
        else {
            im.put("raznica", "Разницы в учебных планах нет");
            im.put("endDate", "");
        }

        im.modify(document);
    }

    private RtfDocument createDeclarationAttachment(RepresentTransfer representation) {
        RtfDocument doc = UtilPrintSupport.getTemplate("3.21", TypeTemplateRepresentCodes.ATTACHMENT);

        RtfInjectModifier im = new RtfInjectModifier();
        im.put("endDate2", UtilPrintSupport.getDateFormatterWithMonthStringAndYear(representation.getEndDate()));
        im.modify(doc);

        return doc;
    }

    @Override
    public void buildBodyExtract(Representation representationBase, String itemFac, RtfDocument docCommon) {
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        UtilPrintSupport.setParNum(docCommon, itemFac);

        RtfInjectModifier modifier = new RtfInjectModifier();

        UtilPrintSupport.injectModifierExtract(modifier, docRepresent);

        injectModifier(modifier, docRepresent);

        modifier.modify(docCommon);
    }

    @Override
    public void buildBodyRepresent(Representation representationBase, RtfDocument document, AtomicInteger parNumber, AtomicInteger extractNumber) {
        super.buildBodyRepresent(representationBase, document, parNumber, extractNumber);
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);
        Student student = docRepresent.getStudent();

        RtfInjectModifier modifier = new RtfInjectModifier();

        RepresentTransfer representation = (RepresentTransfer) docRepresent.getRepresentation();
        EducationOrgUnit oldEducationOrgUnit = get(representation.getOldEducationOrgUnitId());
        Group oldGroup = null;
        if (representation.getOldGroupId() != null)
            oldGroup = get(representation.getOldGroupId());
        Course oldCourse = get(representation.getOldCourseId());
        /** В параграф должны выводиться старые данные **/
        if (oldEducationOrgUnit != null && oldCourse != null)
            UtilPrintSupport.injectModifierRepresent(modifier, docRepresent, student, oldEducationOrgUnit, oldCourse, oldGroup);
        else
            UtilPrintSupport.injectModifierRepresent(modifier, docRepresent, student);

        injectModifier(modifier, docRepresent);

        if (StringUtils.isEmpty(modifier.getStringValue("basics")))
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Arrays.asList("basics"), false, false);

        modifier.modify(document);
    }

    @Override
    protected void injectModifier(RtfInjectModifier modifier, DocRepresentStudentBase docrepresent) {
        RepresentTransfer represent = (RepresentTransfer) docrepresent.getRepresentation();

        EducationOrgUnit oldEducationOrgUnit = get(represent.getOldEducationOrgUnitId());
        /*Group oldGroup = null;
		if (represent.getOldGroupId() != null)
			oldGroup = get(represent.getOldGroupId());
		Course oldCourse = get(represent.getOldCourseId());
		*/
        EducationOrgUnit eduOrgUnit = represent.getEducationOrgUnit() != null ? represent.getEducationOrgUnit() : represent.getGroup().getEducationOrgUnit();

        //modifier.put("studentTitle", PersonManager.instance().declinationDao().getDeclinationFIO(docrepresent.getStudent().getPerson().getIdentityCard(), GrammaCase.ACCUSATIVE));
        modifier.put("studentTitle", UtilPrintSupport.printSeparatedFIO(docrepresent.getStudent().getPerson().getIdentityCard(), GrammaCase.ACCUSATIVE, docrepresent.getStudent().getPerson().isMale()));

        modifier.put("studentSex", docrepresent.getStudent().getPerson().isMale() ? "студента" : "студентку");
        modifier.put("newCourse", represent.getCourse().getTitle());
        modifier.put("newGroup", represent.getGroup().getTitle());

        modifier.put("newForm", UtilPrintSupport.getDevelopFormGen(eduOrgUnit.getDevelopForm()));
        modifier.put("newDevelopForm", eduOrgUnit.getDevelopForm().equals(oldEducationOrgUnit.getDevelopForm()) ? "" : " " + UtilPrintSupport.getDevelopFormGen(eduOrgUnit.getDevelopForm()) + " формы");

        if (eduOrgUnit.getEducationLevelHighSchool().equals(oldEducationOrgUnit.getEducationLevelHighSchool())) {
            modifier.put("separator", "");
            modifier.put("newHighSchool", "");
        }
        else {
            modifier.put("separator", ", ");
            modifier.put("newHighSchool", UtilPrintSupport.getHighLevelSchoolTypeString(eduOrgUnit.getEducationLevelHighSchool()));
        }

        modifier.put("newFormOrgUnit", eduOrgUnit.getFormativeOrgUnit().equals(oldEducationOrgUnit.getFormativeOrgUnit()) ? "" : " " + eduOrgUnit.getFormativeOrgUnit().getGenitiveCaseTitle());
        modifier.put("newInst", eduOrgUnit.getFormativeOrgUnit().getGenitiveCaseTitle());
        modifier.put("newInst2", eduOrgUnit.getFormativeOrgUnit().getGenitiveCaseTitle());

        modifier.put("newSpecType", UtilPrintSupport.getHighLevelSchoolTypeString(eduOrgUnit.getEducationLevelHighSchool()));
        modifier.put("newSpec", eduOrgUnit.getTitle());
        modifier.put("newBase", represent.getCompensationType().getShortTitle());

        modifier.put("newGrant", "");
        modifier.put("newPGO", "");

        if (eduOrgUnit.getFormativeOrgUnit().getHead() != null)
            modifier.put("fioDirNewInst", ((EmployeePost) eduOrgUnit.getFormativeOrgUnit().getHead()).getFio());
        else
            modifier.put("fioDirNewInst", "");

        if (represent.getEndDate() != null) {

        	//----------------------------------------------------------------------------------------------
            //RtfString str = new RtfString().append(" с условием ликвидации разницы в учебных планах до ");
            RtfString str = new RtfString().append(", с условием ликвидации разницы в учебных планах до ");	//добавлена запятая
            //----------------------------------------------------------------------------------------------

            modifier.put("dateEnd", UtilPrintSupport.getDateFormatterWithMonthStringAndYear(represent.getEndDate(), str));

            //modifier.put("dateEnd", " до " + DateFormattingUtil.MONTH_STRING_DATE_FORMAT.format(represent.getEndDate()) + " года");
            str = new RtfString().append("Установить срок ликвидации разницы в учебных планах до ");
            modifier.put("endDate", UtilPrintSupport.getDateFormatterWithMonthStringAndYear(represent.getEndDate(), str));
        }
        else {
            modifier.put("dateEnd", "");
            modifier.put("endDate", "");
        }

        modifier.put("developPayment", "для обучения " + UtilPrintSupport.getCompensationTypeStr(represent.getCompensationType()));
        //-------------------------------------------------------------------------------------------------------------------------
        //Новые условия освоения
        String developTermCode;
        switch (eduOrgUnit.getDevelopCondition().getCode()){
            case DevelopConditionCodes.REDUCED_PERIOD:
                developTermCode = " в сокращенные сроки " + eduOrgUnit.getDevelopPeriod().getTitle();
                break;
            case DevelopConditionCodes.ACCELERATED_PERIOD:
                developTermCode = " в ускоренные сроки";
                break;
            case DevelopConditionCodes.REDUCED_ACCELERATED_PERIOD:
                developTermCode = " в сокращенные ускоренные сроки " + eduOrgUnit.getDevelopPeriod().getTitle();
                break;
            default:
                developTermCode = "";
        }
        modifier.put("developTermCode", developTermCode);
        
        //Утвердить ИУП
        modifier.put("transfer", represent.isIndividualPlan() ? "Утвердить индивидуальный учебный план и перевести" : "Перевести");
        //-------------------------------------------------------------------------------------------------------------------------

        String reasonStr = represent.getReason().getTitle().toLowerCase();
        reasonStr = reasonStr.replace("_____ курса", docrepresent.getCourseStr() + " курса");
        modifier.put("reason", reasonStr);
    }

}
