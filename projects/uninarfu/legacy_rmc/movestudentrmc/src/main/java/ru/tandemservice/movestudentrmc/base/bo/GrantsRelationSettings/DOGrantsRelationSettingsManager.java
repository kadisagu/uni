package ru.tandemservice.movestudentrmc.base.bo.GrantsRelationSettings;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.movestudentrmc.base.bo.GrantsRelationSettings.logic.IRelationSettingsModifyDAO;
import ru.tandemservice.movestudentrmc.base.bo.GrantsRelationSettings.logic.RelationSettingsModifyDAO;

@Configuration
public class DOGrantsRelationSettingsManager extends BusinessObjectManager {
    public static DOGrantsRelationSettingsManager instance()
    {
        return instance(DOGrantsRelationSettingsManager.class);
    }

    @Bean
    public IRelationSettingsModifyDAO modifyDao()
    {
        return new RelationSettingsModifyDAO();
    }
}
