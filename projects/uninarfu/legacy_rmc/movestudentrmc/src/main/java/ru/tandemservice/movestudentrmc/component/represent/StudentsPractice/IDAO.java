package ru.tandemservice.movestudentrmc.component.represent.StudentsPractice;

import ru.tandemservice.uni.dao.IUniDao;

public abstract interface IDAO extends IUniDao<Model> {

    public abstract void update(Model model);

    public void addNewPracticeBase(Model model, String filter, Long id);

}
