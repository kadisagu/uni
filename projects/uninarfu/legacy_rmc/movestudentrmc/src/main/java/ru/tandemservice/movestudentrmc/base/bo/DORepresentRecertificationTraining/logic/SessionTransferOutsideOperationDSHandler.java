package ru.tandemservice.movestudentrmc.base.bo.DORepresentRecertificationTraining.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisession.entity.document.SessionTransferOutsideOperation;

import java.util.List;

public class SessionTransferOutsideOperationDSHandler extends DefaultComboDataSourceHandler {

    public SessionTransferOutsideOperationDSHandler(String ownerId) {
        super(ownerId, SessionTransferOutsideOperation.class);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep) {
        Long studentId = ep.context.get("studentId");
        EducationYear eduYear = ep.context.get("eduYear");
        List<SessionTransferOutsideOperation> list = ep.context.get("exceptList");

        ep.dqlBuilder.where(DQLExpressions.eq(
                DQLExpressions.property(SessionTransferOutsideOperation.targetMark().slot().studentWpeCAction().studentWpe().year().educationYear().fromAlias("e")),
                DQLExpressions.value(eduYear)
        ));

        ep.dqlBuilder.where(DQLExpressions.eq(
                DQLExpressions.property(SessionTransferOutsideOperation.targetMark().slot().studentWpeCAction().studentWpe().student().id().fromAlias("e")),
                DQLExpressions.value(studentId)
        ));

        if (list != null && !list.isEmpty())
            ep.dqlBuilder.where(DQLExpressions.notIn(
                    DQLExpressions.property(SessionTransferOutsideOperation.id().fromAlias("e")),
                    list
            ));

    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep) {
    }

}
