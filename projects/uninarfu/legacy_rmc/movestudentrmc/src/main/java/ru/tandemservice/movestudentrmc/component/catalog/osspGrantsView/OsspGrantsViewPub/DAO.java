package ru.tandemservice.movestudentrmc.component.catalog.osspGrantsView.OsspGrantsViewPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.movestudentrmc.entity.catalog.OsspGrantsView;

public class DAO extends DefaultCatalogPubDAO<OsspGrantsView, Model> implements IDAO {

    public void updatePrintable(Model model, Long id) {
        OsspGrantsView osspGrants = getNotNull(OsspGrantsView.class, id);
        if (osspGrants.isPrintable()) {
            osspGrants.setPrintable(false);
        }
        else {
            osspGrants.setPrintable(true);
        }
        save(osspGrants);
    }

}
