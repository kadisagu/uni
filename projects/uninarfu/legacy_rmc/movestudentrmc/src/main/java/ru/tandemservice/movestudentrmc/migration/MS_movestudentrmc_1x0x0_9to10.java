package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import ru.tandemservice.movestudentrmc.entity.RelTypeGrantView;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantType;

public class MS_movestudentrmc_1x0x0_9to10 extends IndependentMigrationScript {
    @Override
    public void run(DBTool tool) throws Exception {

        if (tool.tableExists(EntityRuntime.getMeta(RelTypeGrantView.class).getTableName())) {
            tool.executeUpdate("delete from " + EntityRuntime.getMeta(RelTypeGrantView.class).getTableName());
        }

        // в GranType записи добавятся из xml

        if (tool.tableExists(EntityRuntime.getMeta(GrantType.class).getTableName())) {
            tool.executeUpdate("delete from " + EntityRuntime.getMeta(GrantType.class).getTableName());
        }
    }
}