package ru.tandemservice.movestudentrmc.dao.check;

import org.apache.commons.lang.StringUtils;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.Student;

public abstract class AbstractCheckStudentStatus extends AbstractCheckDAO {

    protected abstract String getStatusCode();

    @Override
    protected boolean getCondition(Student student) {
        return student.getStatus().getCode().equals(getStatusCode());
    }

    @Override
    protected String getErrorMessage(Student student, boolean value) {
        StudentStatus status = get(StudentStatus.class, StudentStatus.code().s(), getStatusCode());
        StringBuilder sb = new StringBuilder()
                .append("Студент ")
                .append(student.getPerson().getFullFio())
                .append(" ")
                .append(value ? "не " : "")
                .append("находится в состоянии «")
                .append(StringUtils.capitalize(status.getTitle()))
                .append("»");
        return sb.toString();
    }
}
