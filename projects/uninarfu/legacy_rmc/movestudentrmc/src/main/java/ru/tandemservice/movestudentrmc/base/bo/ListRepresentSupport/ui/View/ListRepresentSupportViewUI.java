package ru.tandemservice.movestudentrmc.base.bo.ListRepresentSupport.ui.View;


import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.support.AbstractListRepresentViewUI;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentSupport.logic.ListPrintDoc;
import ru.tandemservice.movestudentrmc.entity.ListRepresentSupport;

import java.util.Collections;

public class ListRepresentSupportViewUI extends AbstractListRepresentViewUI<ListRepresentSupport> {

    @Override
    public ListRepresentSupport getListRepresentObject() {
        return new ListRepresentSupport();
    }

    @Override
    protected RtfDocument printRepresent() {
        return new ListPrintDoc().creationPrintDocOrder(Collections.singletonList(getListRepresent()));
    }


}
