package ru.tandemservice.movestudentrmc.dao.check;

import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.catalog.codes.EppPlanStructureCodes;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvStructureRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.*;

// RM#5035
public abstract class AbstractCheckSessionMark extends AbstractCheckDAO {

    protected Map<Long, List<EppRegistryElement>> eppEpvRegistryElementMap;
    /**
     * факультативные дисциплины
     */
    protected Set<Long> facultativeRegistryElements;
    /**
     * Локальный кэш оценок студента в последнюю сессию
     */
    protected Map<Long, List<SessionMark>> marksMap;
    /**
     * Map id-студента, последний семестр студента
     */
    protected Map<Long, Long> termMap;

    /**
     * Map id-студента, количество мероприятий в последнюю сессию
     */
    protected Map<Long, Long> totalDisciplineMap;

    protected boolean inSession;

    @Override
    public List<String> doCheck(List<Student> list, boolean value) {
        prepareParam();
        prepareLocalDataCaches(list, inSession);

        return super.doCheck(list, value);
    }

    protected abstract boolean checkCondition(Student student);

    protected abstract String createError(Student student, boolean value);

    protected abstract void prepareParam();

    protected void prepareLocalDataCaches(List<Student> students, boolean inSession) {
        prepareStudentTermAndTotalMap(students, inSession);
        this.eppEpvRegistryElementMap = getEppRegistryElementMap(students);
        this.marksMap = getSessionMarkMap(students, inSession);
    }

    @Override
    protected boolean getCondition(Student student) {
        return checkCondition(student);
    }

    @Override
    protected String getErrorMessage(Student student, boolean value) {
        return createError(student, value);
    }

    private Map<Long, List<SessionMark>> getSessionMarkMap(List<Student> students, boolean inSession) {
        Map<Long, List<SessionMark>> sessionMarkMap = new HashMap<>();

        List<Object[]> sessionMarkData = getSessionMarkData(students, inSession);
        for (Object[] item : sessionMarkData) {
            SessionMark sessionMark = (SessionMark) item[0];
            EppRegistryElement eppRegistryElement = (EppRegistryElement) item[1];
            Student student = (Student) item[2];
            Long term = ((Integer) item[3]).longValue();

            // проверим является ли дисциплина факультативной
            if (eppEpvRegistryElementMap.containsKey(student.getId())) {
                // факультативная дисциплина - нет в кэше обычных и есть в списке факультативных
                if (!eppEpvRegistryElementMap.get(student.getId()).contains(eppRegistryElement) && facultativeRegistryElements.contains(eppRegistryElement.getId())) {
                    // отнимем факультативную дисциплину из общего количества
                    Long total = this.totalDisciplineMap.get(student.getId());
                    if (total != null) {
                        total--;
                        this.totalDisciplineMap.put(student.getId(), total);
                    }
                    continue;
                }
                // иначе даже если нет в списке обычных дисциплин, возможно ее добавили вручную в индивидульный
                // РУП студента (в УП ее в таком случае не будет)
                else // запоминаем только дисциплины за последний семестр
                    if (term.equals(termMap.get(student.getId()))) {
                        if (sessionMarkMap.containsKey(student.getId())) {
                            sessionMarkMap.get(student.getId()).add(sessionMark);
                        }
                        else {
                            List<SessionMark> lst = new ArrayList<>();
                            lst.add(sessionMark);
                            sessionMarkMap.put(student.getId(), lst);
                        }
                    }
            }
        }

        return sessionMarkMap;
    }

    public List<Object[]> getSessionMarkData(List<Student> students, boolean inSession) {

        DQLSelectBuilder actualActionSlotBuilder = getActualActionSlotBuilder(students);
        actualActionSlotBuilder.column("slot");

        // достаем итоговые оценки по контрольным мероприятиям (КМ)
        DQLSelectBuilder totalMarkBuilder = new DQLSelectBuilder().fromEntity(SessionMark.class, "mark")
                .joinPath(DQLJoinType.inner, SessionMark.slot().fromAlias("mark"), "slot")
                .joinEntity("slot", DQLJoinType.inner, SessionStudentGradeBookDocument.class, "book",
                            DQLExpressions.eq(DQLExpressions.property(SessionDocumentSlot.document().fromAlias("slot")), DQLExpressions.property("book")))
                .where(DQLExpressions.in(DQLExpressions.property(SessionMark.slot().studentWpeCAction().fromAlias("mark")), actualActionSlotBuilder.buildQuery()))
                .where(DQLExpressions.eqValue(DQLExpressions.property(SessionMark.slot().inSession().fromAlias("mark")), inSession))
                .column("mark")
                .column(SessionMark.slot().studentWpeCAction().studentWpe().registryElementPart().registryElement().fromAlias("mark").s())
                .column(SessionMark.slot().studentWpeCAction().studentWpe().student().fromAlias("mark").s())
                .column(SessionMark.slot().studentWpeCAction().studentWpe().term().intValue().fromAlias("mark").s())
                .order(DQLExpressions.property(SessionMark.slot().studentWpeCAction().studentWpe().term().intValue().fromAlias("mark")), OrderDirection.desc);

        return getList(totalMarkBuilder);
    }

    /**
     * Запрос выборки актуальных мероприятий студентов
     */
    public DQLSelectBuilder getActualActionSlotBuilder(List<Student> students) {
        // актуальные мероприятия студента
        return new DQLSelectBuilder().fromEntity(EppStudentWpeCAction.class, "slot")
                .joinPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().registryElementPart().registryElement().fromAlias("slot"), "d")
                .joinEntity(
                        "d",
                        DQLJoinType.inner,
                        EppRegistryDiscipline.class,
                        "disc",
                        DQLExpressions.eq(
                                DQLExpressions.property("disc.id"),
                                DQLExpressions.property("d.id")
                        )
                )
                .where(DQLExpressions.in(DQLExpressions.property(EppStudentWpeCAction.studentWpe().student().fromAlias("slot")), ids(students)))
                .where(DQLExpressions.isNull(DQLExpressions.property(EppStudentWpeCAction.studentWpe().removalDate().fromAlias("slot"))));
    }

    private DQLSelectBuilder getEppEpvRegistryRowData(List<Student> students) {

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EppEpvRegistryRow.class, "eerr")
                .joinPath(DQLJoinType.inner, EppEpvRegistryRow.owner().fromAlias("eerr"))
                .joinDataSource("eerr", DQLJoinType.inner, getEduPlanVersionDS(students).buildQuery(), "ds",
                                DQLExpressions.eq(
                                        DQLExpressions.property(EppEpvRegistryRow.owner().eduPlanVersion().id().fromAlias("eerr")),
                                        DQLExpressions.property("ds.eduPlanVersionId")))
                .where(DQLExpressions.in(
                        DQLExpressions.property(EppEpvRegistryRow.owner().eduPlanVersion().fromAlias("eerr")),
                        getEduPlanVersionDQL(students).buildQuery()
                ))
                .column("eerr")
                .column("ds.student");

        return builder;
    }

    private DQLSelectBuilder getEduPlanVersionDQL(List<Student> students) {

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EppStudent2EduPlanVersion.class, "s2epv")
                .where(DQLExpressions.isNull(
                        DQLExpressions.property(EppStudent2EduPlanVersion.removalDate().fromAlias("s2epv"))))
                .where(DQLExpressions.in(
                        DQLExpressions.property(EppStudent2EduPlanVersion.student().id().fromAlias("s2epv")),
                        ids(students)))
                .column(EppStudent2EduPlanVersion.eduPlanVersion().fromAlias("s2epv").s());

        return builder;
    }

    private DQLSelectBuilder getEduPlanVersionDS(List<Student> students) {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EppStudent2EduPlanVersion.class, "s2epv")
                .where(DQLExpressions.isNull(
                        DQLExpressions.property(EppStudent2EduPlanVersion.removalDate().fromAlias("s2epv"))))
                .where(DQLExpressions.in(
                        DQLExpressions.property(EppStudent2EduPlanVersion.student().id().fromAlias("s2epv")),
                        ids(students)))
                .column(EppStudent2EduPlanVersion.eduPlanVersion().id().fromAlias("s2epv").s(), "eduPlanVersionId")
                .column(EppStudent2EduPlanVersion.student().fromAlias("s2epv").s(), "student");

        return builder;
    }

    private Map<Long, Long> getStudentTermMap(List<Student> students, boolean inSession) {
        Map<Long, Long> termMap = new HashMap<>();

        List<Object[]> termData = getList(getStudentTermDQL(students, inSession));

        for (Object[] item : termData)
            termMap.put((Long) item[0], (Long) ((Integer) item[1]).longValue());

        return termMap;
    }

    private Map<Long, List<EppRegistryElement>> getEppRegistryElementMap(List<Student> students) {
        Map<Long, List<EppRegistryElement>> eppRegistryElementMap = new HashMap<>();
        facultativeRegistryElements = new HashSet<>();
        List<Object[]> data = getList(getEppEpvRegistryRowData(students));
        for (Object[] item : data) {
            EppEpvRegistryRow eppEpvRegistryRow = (EppEpvRegistryRow) item[0];
            Student student = (Student) item[1];

            if (isFacultative(eppEpvRegistryRow)) {
                // добавляем в список факультативных дисциплин
                facultativeRegistryElements.add(eppEpvRegistryRow.getRegistryElement().getId());
                continue;
            }

            if (eppRegistryElementMap.containsKey(student.getId())) {
                eppRegistryElementMap.get(student.getId()).add(eppEpvRegistryRow.getRegistryElement());
            }
            else {
                List<EppRegistryElement> lst = new ArrayList<>();
                lst.add(eppEpvRegistryRow.getRegistryElement());
                eppRegistryElementMap.put(student.getId(), lst);
            }
        }
        return eppRegistryElementMap;
    }

    /**
     * Запрос для извлечения семестра для студента - последнего заполненного
     */
    private DQLSelectBuilder getStudentTermDQL(List<Student> students, boolean inSession) {

        DQLSelectBuilder actualActionSlotBuilder = getActualActionSlotBuilder(students);
        actualActionSlotBuilder.column("slot");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SessionMark.class, "mark")
                .joinPath(DQLJoinType.inner, SessionMark.slot().fromAlias("mark"), "slot")
                .joinEntity("slot", DQLJoinType.inner, SessionStudentGradeBookDocument.class, "book",
                            DQLExpressions.eq(DQLExpressions.property(SessionDocumentSlot.document().fromAlias("slot")), DQLExpressions.property("book")))
                .where(DQLExpressions.in(DQLExpressions.property(SessionMark.slot().studentWpeCAction().fromAlias("mark")), actualActionSlotBuilder.buildQuery()))
                .where(DQLExpressions.eqValue(DQLExpressions.property(SessionMark.slot().inSession().fromAlias("mark")), inSession))
                .column(SessionMark.slot().studentWpeCAction().studentWpe().student().id().fromAlias("mark").s(), "studentId")
                .column(DQLFunctions.max(DQLExpressions.property(SessionMark.slot().studentWpeCAction().studentWpe().term().intValue().fromAlias("mark"))), "term")
                .group(SessionMark.slot().studentWpeCAction().studentWpe().student().id().fromAlias("mark").s());

        return builder;
    }

    /**
     * Количество мероприятий сессию для сдачи (группировка по студенту и
     * семестру, поэтому возможен ответ с данными за несколько семестров, отсеиваем в {@link #prepareLocalDataCaches(List, boolean)})
     */
    private DQLSelectBuilder getStudentTotalDisciplineDQL(List<Student> students) {
        return getActualActionSlotBuilder(students)
                .column(EppStudentWpeCAction.studentWpe().student().id().fromAlias("slot").s(), "studentId")
                .column(DQLFunctions.max(DQLExpressions.property(EppStudentWpeCAction.studentWpe().term().intValue().fromAlias("slot"))), "term")
                .column(DQLFunctions.count("slot"), "total")
                .group(EppStudentWpeCAction.studentWpe().student().id().fromAlias("slot").s())
                .group(EppStudentWpeCAction.studentWpe().term().id().fromAlias("slot").s());
    }

    private void prepareStudentTermAndTotalMap(List<Student> students, boolean inSession) {

        this.termMap = getStudentTermMap(students, inSession);

        Map<Long, Long> totalDataMap = new HashMap<>();

        // заполним данные по количеству дисциплин
        List<Object[]> totalData = getList(getStudentTotalDisciplineDQL(students));
        for (Object[] item : totalData) {

            Long lastTerm = this.termMap.get((Long) item[0]);
            Long term = ((Integer) item[1]).longValue();

            if (lastTerm != term) {
                continue;
            }

            totalDataMap.put((Long) item[0], (Long) item[2]);
        }

        this.totalDisciplineMap = totalDataMap;
    }

    public boolean isFacultative(EppEpvRegistryRow eppEpvRegistryRow) {

        if (!(eppEpvRegistryRow.getHierarhyParent() instanceof EppEpvStructureRow))
            return false;

        EppEpvStructureRow cycle = getRoot((EppEpvStructureRow) eppEpvRegistryRow.getHierarhyParent());

        return EppPlanStructureCodes.FGOS_CYCLES_SPECIAL_OPTIONAL_DISCIPLINES.equals(cycle.getValue().getCode())
                || EppPlanStructureCodes.GOS_CYCLES_OPTIONAL_DISCIPLINES.equals(cycle.getValue().getCode())
                || EppPlanStructureCodes.GOS_CYCLES_SPECIAL_OPTIONAL_DISCIPLINES.equals(cycle.getValue().getCode());
    }

    /**
     * Корневая строка УПа
     */
    private EppEpvStructureRow getRoot(EppEpvStructureRow eppEpvStructureRow) {
        EppEpvStructureRow root = eppEpvStructureRow;
        while (root.getHierarhyParent() != null)
            root = root.getHierarhyParent();

        return root;
    }

}
