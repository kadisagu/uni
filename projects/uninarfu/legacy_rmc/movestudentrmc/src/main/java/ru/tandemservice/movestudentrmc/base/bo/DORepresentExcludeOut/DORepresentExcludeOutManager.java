package ru.tandemservice.movestudentrmc.base.bo.DORepresentExcludeOut;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentExcludeOut.logic.DORepresentExcludeOutDAO;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentExcludeOut.ui.Edit.DORepresentExcludeOutEdit;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentExcludeOut.ui.View.DORepresentExcludeOutView;
import ru.tandemservice.movestudentrmc.docord.IDOObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IDOObjectModifyDAO;

@Configuration
public class DORepresentExcludeOutManager extends BusinessObjectManager implements IDOObjectByRepresentTypeManager {

    public static DORepresentExcludeOutManager instance() {
        return instance(DORepresentExcludeOutManager.class);
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return DORepresentExcludeOutEdit.class;
    }

    @Bean
    public IDOObjectModifyDAO getDOObjectModifyDAO() {
        return new DORepresentExcludeOutDAO();
    }

    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return DORepresentExcludeOutView.class;
    }

}
