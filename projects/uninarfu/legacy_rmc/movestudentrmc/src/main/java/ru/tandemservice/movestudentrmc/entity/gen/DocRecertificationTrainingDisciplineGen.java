package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.DocRecertificationTrainingDiscipline;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.unisession.entity.document.SessionTransferOutsideOperation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность-связь 'Представление о перезачете'-'Перезачтенное мероприятие из другого ОУ'
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DocRecertificationTrainingDisciplineGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.DocRecertificationTrainingDiscipline";
    public static final String ENTITY_NAME = "docRecertificationTrainingDiscipline";
    public static final int VERSION_HASH = -1057554590;
    private static IEntityMeta ENTITY_META;

    public static final String L_REPRESENTATION = "representation";
    public static final String L_SESSION_TRANSFER_OUTSIDE_OPERATION = "sessionTransferOutsideOperation";
    public static final String P_RECERTIFICATION = "recertification";

    private Representation _representation;     // Представление
    private SessionTransferOutsideOperation _sessionTransferOutsideOperation;     // Перезачтенное мероприятие из другого ОУ
    private boolean _recertification;     // Переатестация (иначе перезачет)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Представление. Свойство не может быть null.
     */
    @NotNull
    public Representation getRepresentation()
    {
        return _representation;
    }

    /**
     * @param representation Представление. Свойство не может быть null.
     */
    public void setRepresentation(Representation representation)
    {
        dirty(_representation, representation);
        _representation = representation;
    }

    /**
     * @return Перезачтенное мероприятие из другого ОУ. Свойство не может быть null.
     */
    @NotNull
    public SessionTransferOutsideOperation getSessionTransferOutsideOperation()
    {
        return _sessionTransferOutsideOperation;
    }

    /**
     * @param sessionTransferOutsideOperation Перезачтенное мероприятие из другого ОУ. Свойство не может быть null.
     */
    public void setSessionTransferOutsideOperation(SessionTransferOutsideOperation sessionTransferOutsideOperation)
    {
        dirty(_sessionTransferOutsideOperation, sessionTransferOutsideOperation);
        _sessionTransferOutsideOperation = sessionTransferOutsideOperation;
    }

    /**
     * @return Переатестация (иначе перезачет). Свойство не может быть null.
     */
    @NotNull
    public boolean isRecertification()
    {
        return _recertification;
    }

    /**
     * @param recertification Переатестация (иначе перезачет). Свойство не может быть null.
     */
    public void setRecertification(boolean recertification)
    {
        dirty(_recertification, recertification);
        _recertification = recertification;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DocRecertificationTrainingDisciplineGen)
        {
            setRepresentation(((DocRecertificationTrainingDiscipline)another).getRepresentation());
            setSessionTransferOutsideOperation(((DocRecertificationTrainingDiscipline)another).getSessionTransferOutsideOperation());
            setRecertification(((DocRecertificationTrainingDiscipline)another).isRecertification());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DocRecertificationTrainingDisciplineGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DocRecertificationTrainingDiscipline.class;
        }

        public T newInstance()
        {
            return (T) new DocRecertificationTrainingDiscipline();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "representation":
                    return obj.getRepresentation();
                case "sessionTransferOutsideOperation":
                    return obj.getSessionTransferOutsideOperation();
                case "recertification":
                    return obj.isRecertification();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "representation":
                    obj.setRepresentation((Representation) value);
                    return;
                case "sessionTransferOutsideOperation":
                    obj.setSessionTransferOutsideOperation((SessionTransferOutsideOperation) value);
                    return;
                case "recertification":
                    obj.setRecertification((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "representation":
                        return true;
                case "sessionTransferOutsideOperation":
                        return true;
                case "recertification":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "representation":
                    return true;
                case "sessionTransferOutsideOperation":
                    return true;
                case "recertification":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "representation":
                    return Representation.class;
                case "sessionTransferOutsideOperation":
                    return SessionTransferOutsideOperation.class;
                case "recertification":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DocRecertificationTrainingDiscipline> _dslPath = new Path<DocRecertificationTrainingDiscipline>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DocRecertificationTrainingDiscipline");
    }
            

    /**
     * @return Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRecertificationTrainingDiscipline#getRepresentation()
     */
    public static Representation.Path<Representation> representation()
    {
        return _dslPath.representation();
    }

    /**
     * @return Перезачтенное мероприятие из другого ОУ. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRecertificationTrainingDiscipline#getSessionTransferOutsideOperation()
     */
    public static SessionTransferOutsideOperation.Path<SessionTransferOutsideOperation> sessionTransferOutsideOperation()
    {
        return _dslPath.sessionTransferOutsideOperation();
    }

    /**
     * @return Переатестация (иначе перезачет). Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRecertificationTrainingDiscipline#isRecertification()
     */
    public static PropertyPath<Boolean> recertification()
    {
        return _dslPath.recertification();
    }

    public static class Path<E extends DocRecertificationTrainingDiscipline> extends EntityPath<E>
    {
        private Representation.Path<Representation> _representation;
        private SessionTransferOutsideOperation.Path<SessionTransferOutsideOperation> _sessionTransferOutsideOperation;
        private PropertyPath<Boolean> _recertification;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRecertificationTrainingDiscipline#getRepresentation()
     */
        public Representation.Path<Representation> representation()
        {
            if(_representation == null )
                _representation = new Representation.Path<Representation>(L_REPRESENTATION, this);
            return _representation;
        }

    /**
     * @return Перезачтенное мероприятие из другого ОУ. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRecertificationTrainingDiscipline#getSessionTransferOutsideOperation()
     */
        public SessionTransferOutsideOperation.Path<SessionTransferOutsideOperation> sessionTransferOutsideOperation()
        {
            if(_sessionTransferOutsideOperation == null )
                _sessionTransferOutsideOperation = new SessionTransferOutsideOperation.Path<SessionTransferOutsideOperation>(L_SESSION_TRANSFER_OUTSIDE_OPERATION, this);
            return _sessionTransferOutsideOperation;
        }

    /**
     * @return Переатестация (иначе перезачет). Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRecertificationTrainingDiscipline#isRecertification()
     */
        public PropertyPath<Boolean> recertification()
        {
            if(_recertification == null )
                _recertification = new PropertyPath<Boolean>(DocRecertificationTrainingDisciplineGen.P_RECERTIFICATION, this);
            return _recertification;
        }

        public Class getEntityClass()
        {
            return DocRecertificationTrainingDiscipline.class;
        }

        public String getEntityName()
        {
            return "docRecertificationTrainingDiscipline";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
