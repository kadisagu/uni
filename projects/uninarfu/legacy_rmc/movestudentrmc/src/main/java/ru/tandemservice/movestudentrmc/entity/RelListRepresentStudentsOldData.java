package ru.tandemservice.movestudentrmc.entity;

import ru.tandemservice.movestudentrmc.entity.gen.RelListRepresentStudentsOldDataGen;

/**
 * Списочное представление-Студенты для хранения старых данных для студентов при откате списочного приказа
 */
public class RelListRepresentStudentsOldData extends RelListRepresentStudentsOldDataGen
{
}