package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.RelRepresentationBasementDocument;
import ru.tandemservice.movestudentrmc.entity.catalog.Document;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationBasement;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность-связь Основание представления-Документ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RelRepresentationBasementDocumentGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.RelRepresentationBasementDocument";
    public static final String ENTITY_NAME = "relRepresentationBasementDocument";
    public static final int VERSION_HASH = 2012679305;
    private static IEntityMeta ENTITY_META;

    public static final String L_FIRST = "first";
    public static final String L_SECOND = "second";

    private RepresentationBasement _first;     // Основания представлений
    private Document _second;     // Документ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Основания представлений. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public RepresentationBasement getFirst()
    {
        return _first;
    }

    /**
     * @param first Основания представлений. Свойство не может быть null и должно быть уникальным.
     */
    public void setFirst(RepresentationBasement first)
    {
        dirty(_first, first);
        _first = first;
    }

    /**
     * @return Документ. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public Document getSecond()
    {
        return _second;
    }

    /**
     * @param second Документ. Свойство не может быть null и должно быть уникальным.
     */
    public void setSecond(Document second)
    {
        dirty(_second, second);
        _second = second;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RelRepresentationBasementDocumentGen)
        {
            setFirst(((RelRepresentationBasementDocument)another).getFirst());
            setSecond(((RelRepresentationBasementDocument)another).getSecond());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RelRepresentationBasementDocumentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RelRepresentationBasementDocument.class;
        }

        public T newInstance()
        {
            return (T) new RelRepresentationBasementDocument();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "first":
                    return obj.getFirst();
                case "second":
                    return obj.getSecond();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "first":
                    obj.setFirst((RepresentationBasement) value);
                    return;
                case "second":
                    obj.setSecond((Document) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "first":
                        return true;
                case "second":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "first":
                    return true;
                case "second":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "first":
                    return RepresentationBasement.class;
                case "second":
                    return Document.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RelRepresentationBasementDocument> _dslPath = new Path<RelRepresentationBasementDocument>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RelRepresentationBasementDocument");
    }
            

    /**
     * @return Основания представлений. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.RelRepresentationBasementDocument#getFirst()
     */
    public static RepresentationBasement.Path<RepresentationBasement> first()
    {
        return _dslPath.first();
    }

    /**
     * @return Документ. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.RelRepresentationBasementDocument#getSecond()
     */
    public static Document.Path<Document> second()
    {
        return _dslPath.second();
    }

    public static class Path<E extends RelRepresentationBasementDocument> extends EntityPath<E>
    {
        private RepresentationBasement.Path<RepresentationBasement> _first;
        private Document.Path<Document> _second;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Основания представлений. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.RelRepresentationBasementDocument#getFirst()
     */
        public RepresentationBasement.Path<RepresentationBasement> first()
        {
            if(_first == null )
                _first = new RepresentationBasement.Path<RepresentationBasement>(L_FIRST, this);
            return _first;
        }

    /**
     * @return Документ. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.RelRepresentationBasementDocument#getSecond()
     */
        public Document.Path<Document> second()
        {
            if(_second == null )
                _second = new Document.Path<Document>(L_SECOND, this);
            return _second;
        }

        public Class getEntityClass()
        {
            return RelRepresentationBasementDocument.class;
        }

        public String getEntityName()
        {
            return "relRepresentationBasementDocument";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
