package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class GroupDSHandler extends DefaultComboDataSourceHandler {

    public GroupDSHandler(String ownerId) {

        super(ownerId, Group.class, Group.P_TITLE);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        List<OrgUnit> formativeOrgUnitFilter = ep.context.get("formativeOrgUnitFilter");
        List<DevelopForm> developFormFilter = ep.context.get("developFormFilter");
        EducationOrgUnit educationOrgUnit = ep.context.get("orientationFilter");

        String filter = ep.input.getComboFilterByValue();
        if (StringUtils.isNotBlank(filter)) {
            ep.dqlBuilder.where(like(DQLFunctions.upper(property(Group.title().fromAlias("e"))), value(CoreStringUtils.escapeLike(filter, true))));
        }

//    	FilterUtils.applySelectFilter(ep.dqlBuilder, "e", Group.course(), ep.context.get("courseFilter"));

        FilterUtils.applySelectFilter(ep.dqlBuilder, "e", Group.educationOrgUnit().formativeOrgUnit(), formativeOrgUnitFilter);
        FilterUtils.applySelectFilter(ep.dqlBuilder, "e", Group.educationOrgUnit().developForm(), developFormFilter);
        if (educationOrgUnit != null) {
            FilterUtils.applySelectFilter(ep.dqlBuilder, "e", Group.educationOrgUnit(), educationOrgUnit);
        }
        // данное поле удалено <TODO> для Ситека
        // FilterUtils.applySelectFilter(ep.dqlBuilder,"e", Group.compensationType(), ep.context.get("compensationTypeFilter"));
        //FilterUtils.applySelectFilter(ep.dqlBuilder, "e",Group.educationOrgUnit().educationLevelHighSchool().orgUnit(), ep.context.get("educationOrgUnitFilter"));

        //------------------------------------------------------------------------------------------------------------------------------------------------
        //Исключены архивные группы
        ep.dqlBuilder.where(eq(property(Group.archival().fromAlias("e")), value(false)));
        //------------------------------------------------------------------------------------------------------------------------------------------------
    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
    {
        ep.dqlBuilder.order("e." + Group.title());
    }
}
