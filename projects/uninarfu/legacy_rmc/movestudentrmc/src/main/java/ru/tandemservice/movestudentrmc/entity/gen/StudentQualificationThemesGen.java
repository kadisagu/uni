package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.StudentQualificationThemes;
import ru.tandemservice.uni.entity.employee.Student;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь Студент - тема ВКР - списочное представление о назначении темы ВКР
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentQualificationThemesGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.StudentQualificationThemes";
    public static final String ENTITY_NAME = "studentQualificationThemes";
    public static final int VERSION_HASH = 2034707866;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT = "student";
    public static final String P_ADVISOR = "advisor";
    public static final String P_THEME = "theme";
    public static final String P_OLD_THEME = "oldTheme";
    public static final String L_LIST_REPRESENT = "listRepresent";

    private Student _student;     // Студент
    private String _advisor;     // Руководитель
    private String _theme;     // Тема ВКР
    private String _oldTheme;     // Предыдущая тема ВКР
    private ListRepresent _listRepresent;     // Списочное представление

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Руководитель.
     */
    @Length(max=255)
    public String getAdvisor()
    {
        return _advisor;
    }

    /**
     * @param advisor Руководитель.
     */
    public void setAdvisor(String advisor)
    {
        dirty(_advisor, advisor);
        _advisor = advisor;
    }

    /**
     * @return Тема ВКР.
     */
    public String getTheme()
    {
        return _theme;
    }

    /**
     * @param theme Тема ВКР.
     */
    public void setTheme(String theme)
    {
        dirty(_theme, theme);
        _theme = theme;
    }

    /**
     * @return Предыдущая тема ВКР.
     */
    public String getOldTheme()
    {
        return _oldTheme;
    }

    /**
     * @param oldTheme Предыдущая тема ВКР.
     */
    public void setOldTheme(String oldTheme)
    {
        dirty(_oldTheme, oldTheme);
        _oldTheme = oldTheme;
    }

    /**
     * @return Списочное представление. Свойство не может быть null.
     */
    @NotNull
    public ListRepresent getListRepresent()
    {
        return _listRepresent;
    }

    /**
     * @param listRepresent Списочное представление. Свойство не может быть null.
     */
    public void setListRepresent(ListRepresent listRepresent)
    {
        dirty(_listRepresent, listRepresent);
        _listRepresent = listRepresent;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentQualificationThemesGen)
        {
            setStudent(((StudentQualificationThemes)another).getStudent());
            setAdvisor(((StudentQualificationThemes)another).getAdvisor());
            setTheme(((StudentQualificationThemes)another).getTheme());
            setOldTheme(((StudentQualificationThemes)another).getOldTheme());
            setListRepresent(((StudentQualificationThemes)another).getListRepresent());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentQualificationThemesGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentQualificationThemes.class;
        }

        public T newInstance()
        {
            return (T) new StudentQualificationThemes();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "student":
                    return obj.getStudent();
                case "advisor":
                    return obj.getAdvisor();
                case "theme":
                    return obj.getTheme();
                case "oldTheme":
                    return obj.getOldTheme();
                case "listRepresent":
                    return obj.getListRepresent();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "advisor":
                    obj.setAdvisor((String) value);
                    return;
                case "theme":
                    obj.setTheme((String) value);
                    return;
                case "oldTheme":
                    obj.setOldTheme((String) value);
                    return;
                case "listRepresent":
                    obj.setListRepresent((ListRepresent) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "student":
                        return true;
                case "advisor":
                        return true;
                case "theme":
                        return true;
                case "oldTheme":
                        return true;
                case "listRepresent":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "student":
                    return true;
                case "advisor":
                    return true;
                case "theme":
                    return true;
                case "oldTheme":
                    return true;
                case "listRepresent":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "student":
                    return Student.class;
                case "advisor":
                    return String.class;
                case "theme":
                    return String.class;
                case "oldTheme":
                    return String.class;
                case "listRepresent":
                    return ListRepresent.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentQualificationThemes> _dslPath = new Path<StudentQualificationThemes>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentQualificationThemes");
    }
            

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.StudentQualificationThemes#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Руководитель.
     * @see ru.tandemservice.movestudentrmc.entity.StudentQualificationThemes#getAdvisor()
     */
    public static PropertyPath<String> advisor()
    {
        return _dslPath.advisor();
    }

    /**
     * @return Тема ВКР.
     * @see ru.tandemservice.movestudentrmc.entity.StudentQualificationThemes#getTheme()
     */
    public static PropertyPath<String> theme()
    {
        return _dslPath.theme();
    }

    /**
     * @return Предыдущая тема ВКР.
     * @see ru.tandemservice.movestudentrmc.entity.StudentQualificationThemes#getOldTheme()
     */
    public static PropertyPath<String> oldTheme()
    {
        return _dslPath.oldTheme();
    }

    /**
     * @return Списочное представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.StudentQualificationThemes#getListRepresent()
     */
    public static ListRepresent.Path<ListRepresent> listRepresent()
    {
        return _dslPath.listRepresent();
    }

    public static class Path<E extends StudentQualificationThemes> extends EntityPath<E>
    {
        private Student.Path<Student> _student;
        private PropertyPath<String> _advisor;
        private PropertyPath<String> _theme;
        private PropertyPath<String> _oldTheme;
        private ListRepresent.Path<ListRepresent> _listRepresent;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.StudentQualificationThemes#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Руководитель.
     * @see ru.tandemservice.movestudentrmc.entity.StudentQualificationThemes#getAdvisor()
     */
        public PropertyPath<String> advisor()
        {
            if(_advisor == null )
                _advisor = new PropertyPath<String>(StudentQualificationThemesGen.P_ADVISOR, this);
            return _advisor;
        }

    /**
     * @return Тема ВКР.
     * @see ru.tandemservice.movestudentrmc.entity.StudentQualificationThemes#getTheme()
     */
        public PropertyPath<String> theme()
        {
            if(_theme == null )
                _theme = new PropertyPath<String>(StudentQualificationThemesGen.P_THEME, this);
            return _theme;
        }

    /**
     * @return Предыдущая тема ВКР.
     * @see ru.tandemservice.movestudentrmc.entity.StudentQualificationThemes#getOldTheme()
     */
        public PropertyPath<String> oldTheme()
        {
            if(_oldTheme == null )
                _oldTheme = new PropertyPath<String>(StudentQualificationThemesGen.P_OLD_THEME, this);
            return _oldTheme;
        }

    /**
     * @return Списочное представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.StudentQualificationThemes#getListRepresent()
     */
        public ListRepresent.Path<ListRepresent> listRepresent()
        {
            if(_listRepresent == null )
                _listRepresent = new ListRepresent.Path<ListRepresent>(L_LIST_REPRESENT, this);
            return _listRepresent;
        }

        public Class getEntityClass()
        {
            return StudentQualificationThemes.class;
        }

        public String getEntityName()
        {
            return "studentQualificationThemes";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
