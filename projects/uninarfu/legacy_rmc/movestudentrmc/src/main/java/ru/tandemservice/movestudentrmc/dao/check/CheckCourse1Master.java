package ru.tandemservice.movestudentrmc.dao.check;

import ru.tandemservice.uni.entity.employee.Student;

public class CheckCourse1Master extends CheckMaster {

    @Override
    protected boolean getCondition(Student student) {
        return super.getCondition(student) && student.getCourse().getCode().equals(getCourseCode());
    }

    protected String getCourseCode() {
        return "1";
    }

    @Override
    protected String getErrorMessage(Student student, boolean value) {
        return new StringBuilder()
                .append("Студент ")
                .append(student.getPerson().getFullFio())
                .append(" ")
                .append(value ? "не " : "")
                .append("обучается на ")
                .append(getCourseCode())
                .append(" курсе по программам магистратуры.")
                .toString();
    }

}
