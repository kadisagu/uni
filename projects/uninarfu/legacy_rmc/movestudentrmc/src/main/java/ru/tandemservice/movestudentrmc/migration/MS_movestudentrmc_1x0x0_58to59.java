package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.OrderCategoryCodes;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentationTypeCodes;


/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_movestudentrmc_1x0x0_58to59 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.5.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.5.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность representationType

        // удалено свойство checkMove
        {
            // удалить колонку
            tool.dropColumn("representationtype_t", "checkmove_p");

        }

        // удалено свойство checkGrant
        {
            // удалить колонку
            tool.dropColumn("representationtype_t", "checkgrant_p");

        }

        // создано обязательное свойство check
        {
            // создать колонку
            tool.createColumn("representationtype_t", new DBColumn("check_p", DBType.BOOLEAN));

            // задать значение по умолчанию
            java.lang.Boolean defaultCheck = false;        // TODO: правильно?
            tool.executeUpdate("update representationtype_t set check_p=? where check_p is null", defaultCheck);

            // сделать колонку NOT NULL
            tool.setColumnNullable("representationtype_t", "check_p", false);

        }

        // создано обязательное свойство orderCategory
        {
            // создать колонку
            tool.createColumn("representationtype_t", new DBColumn("ordercategory_id", DBType.LONG));

            tool.executeUpdate("update representationtype_t set ordercategory_id = (select id from ordercategory_t where code_p = ?) where code_p in (?,?,?,?,?,?,?,?,?,?,?,?) and ordercategory_id is null",
                               OrderCategoryCodes.GRANTS,
                               RepresentationTypeCodes.APPOINT_GRANT,
                               RepresentationTypeCodes.GRANT_CANCEL,
                               RepresentationTypeCodes.GRANT_CANCEL_AND_DESTINATION_INDIVIDUAL,
                               RepresentationTypeCodes.GRANT_CANCEL_AND_DESTINATION_LIST,
                               RepresentationTypeCodes.GRANT_CANCEL_INDIVIDUAL,
                               RepresentationTypeCodes.GRANT_RESUME,
                               RepresentationTypeCodes.GRANT_RESUME_INDIVIDUAL,
                               RepresentationTypeCodes.GRANT_SUSPEND,
                               RepresentationTypeCodes.GRANT_SUSPEND_INDIVIDUAL,
                               RepresentationTypeCodes.SOCIAL_GRANT_LIST,
                               RepresentationTypeCodes.SOCIAL_SUPPORT,
                               RepresentationTypeCodes.SOCIAL_SUPPORT_INDIVIDUAL);
            tool.executeUpdate("update representationtype_t set ordercategory_id = (select id from ordercategory_t where code_p = ?) where ordercategory_id is null", OrderCategoryCodes.MOVEMENTSTUDENT);

            // сделать колонку NOT NULL
            tool.setColumnNullable("representationtype_t", "ordercategory_id", false);

        }
        //удалим ненужные категории
        {
            //удалим возсожные связи категорий приказов и типов представлений
            tool.executeUpdate("delete from rlordrctgryrprsnttntyp_t where id in (select rel.id from rlordrctgryrprsnttntyp_t rel, ordercategory_t oc where rel.first_id = oc.id and oc.code_p in ('costs', 'employee'))");
            //удалим ненужные категории
            tool.executeUpdate("delete from ordercategory_t where code_p in ('costs', 'employee')");
        }


    }
}