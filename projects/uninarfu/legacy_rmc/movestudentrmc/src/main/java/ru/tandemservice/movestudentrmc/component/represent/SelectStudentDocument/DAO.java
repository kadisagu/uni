package ru.tandemservice.movestudentrmc.component.represent.SelectStudentDocument;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudentrmc.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudentrmc.entity.NarfuDocument;
import ru.tandemservice.movestudentrmc.entity.catalog.Document;
import ru.tandemservice.movestudentrmc.entity.catalog.DocumentKind;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.ArrayList;
import java.util.List;

public class DAO extends UniDao<Model> {

    @Override
    public void prepare(final Model model) {
        final Student student = getNotNull(Student.class, model.getStudent().getId());
        model.setStudent(student);

        model.setTypeList(MoveStudentDaoFacade.getDocumentDAO().getUsedTypes(student));

        model.setKindModel(new BaseSingleSelectModel() {
            @Override
            public ListResult<DocumentKind> findValues(String arg0) {
                List<DocumentKind> list = MoveStudentDaoFacade.getDocumentDAO().getUsedKinds(student, model.getDocumentType());
                return new ListResult<DocumentKind>(list);
            }

            @Override
            public Object getValue(Object arg0) {
                return get(DocumentKind.class, (Long) arg0);
            }
        });

        model.setDocsettModel(new BaseSingleSelectModel() {
            @Override
            public ListResult<Document> findValues(String arg0) {
                List<Document> list = MoveStudentDaoFacade.getDocumentDAO().getUsedDocuments(student, model.getDocumentType(), model.getDocumentKind());
                return new ListResult<Document>(list);
            }

            @Override
            public Object getValue(Object arg0) {
                return get(DocumentKind.class, (Long) arg0);
            }
        });

        model.setDocumentList(new BaseSingleSelectModel() {
            @Override
            public ListResult<NarfuDocument> findValues(String filter) {
                List<NarfuDocument> list = MoveStudentDaoFacade.getDocumentDAO().getUsedStudentDocuments(student, model.getDocumentType(), model.getDocumentKind(), model.getDocsett());
                if (filter != null && !StringUtils.isEmpty(filter)) {
                    List<NarfuDocument> resultList = new ArrayList<NarfuDocument>();
                    for (NarfuDocument doc : list)
                        if (doc.getTitle().toLowerCase().startsWith(filter.toLowerCase()))
                            resultList.add(doc);

                    return new ListResult<NarfuDocument>(resultList);
                }
                else
                    return new ListResult<NarfuDocument>(list);
            }

            @Override
            public Object getValue(Object arg0) {
                return get(NarfuDocument.class, (Long) arg0);
            }
        });
    }
}
