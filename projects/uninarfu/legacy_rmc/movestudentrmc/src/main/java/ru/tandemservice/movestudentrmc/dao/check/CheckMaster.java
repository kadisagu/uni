package ru.tandemservice.movestudentrmc.dao.check;

import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.employee.Student;

public class CheckMaster extends AbstractCheckDAO {

    @Override
    protected boolean getCondition(Student student) {
        return student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getQualification().getCode().equals(QualificationsCodes.MAGISTR);
    }

    @Override
    protected String getErrorMessage(Student student, boolean value) {
        return new StringBuilder()
                .append("Студент ")
                .append(student.getPerson().getFullFio())
                .append(" ")
                .append(value ? "не " : "")
                .append("обучается по программам магистратуры.").toString();
    }

}
