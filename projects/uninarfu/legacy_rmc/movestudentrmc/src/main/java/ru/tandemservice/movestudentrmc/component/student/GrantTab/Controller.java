package ru.tandemservice.movestudentrmc.component.student.GrantTab;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab.MonthWrapper;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    public void onRefreshComponent(IBusinessComponent component) {
        ((IDAO) getDao()).prepare(getModel(component));

        prepareDataSource(component);
    }

    private void _makeDataSourse(IBusinessComponent component, final Model model)
    {
        DynamicListDataSource<MonthWrapper> dataSource = new DynamicListDataSource<>(component, arg0 -> {
            ((IDAO) getDao()).prepareListDataSource(model);
        });

        model.setDataSource(dataSource);


    }

    private void _createColumn(final Model model)
    {

        PublisherLinkColumn col = new PublisherLinkColumn("Месяц", "title");
        col.setClickable(true).setOrderable(false);

        col.setResolver(new IPublisherLinkResolver() {

            @Override
            public Object getParameters(IEntity entity) {
                MonthWrapper month = (MonthWrapper) entity;
                ParametersMap params = new ParametersMap();
                params.put("month", month.getMonth());
                params.put("eduYearId", model.getEduYear().getId());
                return params;
            }

            @Override
            public String getComponentName(IEntity arg0) {
                return "ru.tandemservice.movestudentrmc.component.student.GrantList";
            }
        });


        model.getDataSource().addColumn(col);
    }

    private void prepareDataSource(IBusinessComponent component) {
        final Model model = getModel(component);
        if (model.getDataSource() != null)
            return;

        _makeDataSourse(component, model);
        _createColumn(model);

    }

    public void onClickMonthDetail(IBusinessComponent component)
    {
        Model model = getModel(component);
        Long id = component.getListenerParameter();
    }

    public void onSearchParamsChange(IBusinessComponent component)
    {
        Model model = getModel(component);
        // _createColumn(model);
        model.getDataSource().refresh();
    }
}
