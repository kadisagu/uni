package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.movestudentrmc.entity.RepresentChangeName;
import ru.tandemservice.movestudentrmc.entity.Representation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * О смене имени
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RepresentChangeNameGen extends Representation
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.RepresentChangeName";
    public static final String ENTITY_NAME = "representChangeName";
    public static final int VERSION_HASH = -1836948111;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT_I_C_OLD = "studentICOld";

    private IdentityCard _studentICOld;     // Сущность УЛ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сущность УЛ.
     */
    public IdentityCard getStudentICOld()
    {
        return _studentICOld;
    }

    /**
     * @param studentICOld Сущность УЛ.
     */
    public void setStudentICOld(IdentityCard studentICOld)
    {
        dirty(_studentICOld, studentICOld);
        _studentICOld = studentICOld;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof RepresentChangeNameGen)
        {
            setStudentICOld(((RepresentChangeName)another).getStudentICOld());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RepresentChangeNameGen> extends Representation.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RepresentChangeName.class;
        }

        public T newInstance()
        {
            return (T) new RepresentChangeName();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "studentICOld":
                    return obj.getStudentICOld();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "studentICOld":
                    obj.setStudentICOld((IdentityCard) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "studentICOld":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "studentICOld":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "studentICOld":
                    return IdentityCard.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RepresentChangeName> _dslPath = new Path<RepresentChangeName>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RepresentChangeName");
    }
            

    /**
     * @return Сущность УЛ.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentChangeName#getStudentICOld()
     */
    public static IdentityCard.Path<IdentityCard> studentICOld()
    {
        return _dslPath.studentICOld();
    }

    public static class Path<E extends RepresentChangeName> extends Representation.Path<E>
    {
        private IdentityCard.Path<IdentityCard> _studentICOld;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сущность УЛ.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentChangeName#getStudentICOld()
     */
        public IdentityCard.Path<IdentityCard> studentICOld()
        {
            if(_studentICOld == null )
                _studentICOld = new IdentityCard.Path<IdentityCard>(L_STUDENT_I_C_OLD, this);
            return _studentICOld;
        }

        public Class getEntityClass()
        {
            return RepresentChangeName.class;
        }

        public String getEntityName()
        {
            return "representChangeName";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
