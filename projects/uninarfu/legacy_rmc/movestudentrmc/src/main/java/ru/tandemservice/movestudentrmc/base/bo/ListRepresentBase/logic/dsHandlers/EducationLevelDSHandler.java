package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;

public class EducationLevelDSHandler extends DefaultComboDataSourceHandler {

    public EducationLevelDSHandler(String ownerId) {
        super(ownerId, StructureEducationLevels.class);

    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep) {
        ep.dqlBuilder.where(DQLExpressions.isNull(DQLExpressions.property(StructureEducationLevels.parent().fromAlias("e"))));
    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep) {
        ep.dqlBuilder.order("e." + StructureEducationLevels.priority());
    }

}
