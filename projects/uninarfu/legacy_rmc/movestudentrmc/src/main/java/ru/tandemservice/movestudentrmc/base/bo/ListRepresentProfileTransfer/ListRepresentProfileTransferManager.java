package ru.tandemservice.movestudentrmc.base.bo.ListRepresentProfileTransfer;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentProfileTransfer.logic.ListRepresentProfileTransferManagerModifyDAO;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentProfileTransfer.ui.Edit.ListRepresentProfileTransferEdit;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentProfileTransfer.ui.View.ListRepresentProfileTransferView;
import ru.tandemservice.movestudentrmc.docord.IListObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IListObjectModifyDAO;


@Configuration
public class ListRepresentProfileTransferManager extends BusinessObjectManager implements IListObjectByRepresentTypeManager {

    public static ListRepresentProfileTransferManager instance()
    {
        return instance(ListRepresentProfileTransferManager.class);
    }


    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return ListRepresentProfileTransferEdit.class;
    }


    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return ListRepresentProfileTransferView.class;
    }


    @Bean
    @Override
    public IListObjectModifyDAO getListObjectModifyDAO() {
        return new ListRepresentProfileTransferManagerModifyDAO();
    }
}
