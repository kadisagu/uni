package ru.tandemservice.movestudentrmc.base.bo.DORepresentTravel.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentDAO;
import ru.tandemservice.movestudentrmc.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.RepresentTravel;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.StudentCustomStateCICodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class DORepresentTravelDAO extends AbstractDORepresentDAO {
    public final static Integer RUSSIA_CODE = 0;

    @Override
    public Representation createRepresentObject(RepresentationType type, List<Student> studentList) {

        RepresentTravel represent = new RepresentTravel();
        represent.setType(type);
        return represent;
    }

    @Override
    public boolean doCommit(Representation represent, ErrorCollector error) {
        if (!super.doCommit(represent, error))
            return false;

        RepresentTravel representation = (RepresentTravel) represent;

        StudentCustomStateCI state = UniDaoFacade.getCoreDao().get(StudentCustomStateCI.class, StudentCustomStateCI.code(), StudentCustomStateCICodes.TRIP);

        MoveStudentDaoFacade.getCustomStateDAO().commitStudentCustomState(state, representation.getBeginDate(), representation.getEndDate(), representation);

        return true;
    }

    @Override
    public boolean doRollback(Representation represent, ErrorCollector error) {
        if (!super.doRollback(represent, error))
            return false;

        MoveStudentDaoFacade.getCustomStateDAO().rollbackStudentCustomState(represent);

        return true;
    }

    @Override
    public void buildBodyExtract(Representation representationBase, String itemFac, RtfDocument docCommon) {

        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        UtilPrintSupport.setParNum(docCommon, itemFac);

        RtfInjectModifier modifier = new RtfInjectModifier();

        UtilPrintSupport.injectModifierExtract(modifier, docRepresent);

        modifier.modify(docCommon);
    }

    @Override
    public void buildBodyRepresent(Representation representationBase, RtfDocument document, AtomicInteger parNumber, AtomicInteger extractNumber) {
        super.buildBodyRepresent(representationBase, document, parNumber, extractNumber);
        RepresentTravel represent = (RepresentTravel) representationBase;
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        Student student = UtilPrintSupport.getStudent(represent);

        RtfInjectModifier modifier = new RtfInjectModifier();

        UtilPrintSupport.injectModifierRepresent(modifier, docRepresent, student);
        modifier.put("studentTitle", UtilPrintSupport.printSeparatedFIO(student.getPerson().getIdentityCard(), GrammaCase.ACCUSATIVE, student.getPerson().isMale()));
        modifier.put("settlement", represent.getAddressItem() == null ? "" : represent.getAddressItem().getFullTitle());
        //---------------------------------------------------------------------------------------------------------------------------------------
        //modifier.put("country", represent.getAddressCountry() == null ? "" : represent.getAddressCountry().getCode() == RUSSIA_CODE ? "" : " " + represent.getAddressCountry().getTitle());
        modifier.put("country", represent.getAddressCountry() == null ? "" : represent.getAddressCountry().getCode() == RUSSIA_CODE ? "" : ", " + represent.getAddressCountry().getFullTitle());
        //---------------------------------------------------------------------------------------------------------------------------------------
        modifier.put("organization", represent.getOrganization() == null ? "" : "(" + represent.getOrganization() + ") ");
        modifier.put("target", represent.getTarget() == null ? "" : represent.getTarget());
        //------------------------------------------------------------------------------------------------------------------------
        //modifier.put("days", represent.getDays() == null ? "" : String.valueOf(represent.getDays()));
        //modifier.put("paymentData", represent.getTravelPaymentData() == null ? "" : represent.getTravelPaymentData().getTitle());
        //modifier.put("dateBeginTravel", represent.getBeginDate() == null ? new RtfString() : UtilPrintSupport.getDateFormatterWithMonthString(represent.getBeginDate()).append(IRtfData.SYMBOL_TILDE).append("г"));
        //modifier.put("dateEndTravel", represent.getEndDate() == null ? new RtfString() : UtilPrintSupport.getDateFormatterWithMonthString(represent.getEndDate()).append(IRtfData.SYMBOL_TILDE).append("г"));

        String stip = student.getCompensationType().isBudget() ? " с сохранением стипендии" : "";
        modifier.put("paymentData", represent.getTravelPaymentData() == null ? "" : represent.getTravelPaymentData().getTitle() + stip);
        modifier.put("dateBeginTravel", represent.getBeginDate() == null ? new RtfString() : UtilPrintSupport.getDateFormatterWithMonthString(represent.getBeginDate()).append(IRtfData.SYMBOL_TILDE).append("года"));
        modifier.put("dateEndTravel", represent.getEndDate() == null ? new RtfString() : UtilPrintSupport.getDateFormatterWithMonthString(represent.getEndDate()).append(IRtfData.SYMBOL_TILDE).append("года"));
        
        StringBuilder days = new StringBuilder();

        if (represent.getDays() != null)
        {
            days.append("сроком на ")
                    .append(String.valueOf(represent.getDays()))
                    .append(" ")
                    .append(UtilPrintSupport.getDaysStr(represent.getDays()));
        }

        modifier.put("daysStr", days.toString());        
        //------------------------------------------------------------------------------------------------------------------------
        modifier.put("studentSex_D", student.getPerson().isMale() ? "студента" : "студентку");

        if (StringUtils.isEmpty(modifier.getStringValue("basics")))
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Arrays.asList("basics"), false, false);

        modifier.modify(document);
    }

    @Override
    public void buildBodyDeclaration(Representation representationBase, RtfDocument document) {
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        RtfInjectModifier im = new RtfInjectModifier();

        RtfTableModifier tm = new RtfTableModifier();
        UtilPrintSupport.injectModifierDeclaration(im, tm, docRepresent);

        this.injectModifier(im, docRepresent);

        im.modify(document);
        tm.modify(document);
    }
}
