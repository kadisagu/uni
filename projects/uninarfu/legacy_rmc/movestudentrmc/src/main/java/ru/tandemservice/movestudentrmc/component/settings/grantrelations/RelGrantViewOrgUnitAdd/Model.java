package ru.tandemservice.movestudentrmc.component.settings.grantrelations.RelGrantViewOrgUnitAdd;

import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.component.settings.abstractRelation.AbstractRelationAdd.AbstractRelationAddModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.List;

public class Model extends AbstractRelationAddModel implements ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil.Model, IEnrollmentCampaignSelectModel {

    private IPrincipalContext _principalContext;
    private ISelectModel _formativeOrgUnitListModel;
    private ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil.Parameters _parameters;
    private List _formativeOrgUnitList;
    private OrgUnit _orgUnit;

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }


    public ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil.Parameters getParameters()
    {
        return _parameters;
    }

    public void setParameters(ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil.Parameters parameters)
    {
        _parameters = parameters;
    }

    public IPrincipalContext getPrincipalContext()
    {
        return _principalContext;
    }

    public void setPrincipalContext(IPrincipalContext principalContext)
    {
        _principalContext = principalContext;
    }

    public ISelectModel getFormativeOrgUnitListModel()
    {
        return _formativeOrgUnitListModel;
    }

    public void setFormativeOrgUnitListModel(ISelectModel formativeOrgUnitListModel)
    {
        _formativeOrgUnitListModel = formativeOrgUnitListModel;
    }

    @Override
    public List<DevelopCondition> getSelectedDevelopConditionList() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<DevelopForm> getSelectedDevelopFormList() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<DevelopPeriod> getSelectedDevelopPeriodList() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<DevelopTech> getSelectedDevelopTechList() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<EducationLevelsHighSchool> getSelectedEducationLevelHighSchoolList() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<EnrollmentCampaign> getSelectedEnrollmentCampaignList() {
        // TODO Auto-generated method stub
        return null;
    }

    public List getFormativeOrgUnitList()
    {
        return _formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List formativeOrgUnitList)
    {
        _formativeOrgUnitList = formativeOrgUnitList;
    }

    @Override
    public List<OrgUnit> getSelectedTerritorialOrgUnitList() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IDataSettings getSettings() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public List<OrgUnit> getSelectedFormativeOrgUnitList() {
        // TODO Auto-generated method stub
        return null;
    }


}
