package ru.tandemservice.movestudentrmc.base.bo.RelationSettingsOSSP.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import ru.tandemservice.movestudentrmc.base.bo.RelationSettingsOSSP.logic.TypeRepresentationDSHandler;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;


@Configuration
public class RelationSettingsOSSPList extends BusinessComponentManager {

    public static final String TYPE_DS = "typeRepresentationDS";

    @Bean
    public ColumnListExtPoint typeDS()
    {
        return columnListExtPointBuilder(TYPE_DS)
                .addColumn(textColumn(TypeRepresentationDSHandler.TYPE_COLUMN, RepresentationType.title()).order().create())
                .addColumn(booleanColumn(TypeRepresentationDSHandler.ISCONFIG_COLUMN, RepresentationType.configOSSP()).create())
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).create())
                .create();
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(TYPE_DS, typeDS()).handler(typeRepresentationDSHandler()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> typeRepresentationDSHandler()
    {
        return new TypeRepresentationDSHandler(getName());
    }
}
