package ru.tandemservice.movestudentrmc.dao.check;

import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.Student;

public class CheckStatusAcademicalVacationOnMedicalStatementOrWithoutVisits extends AbstractCheckStudentAdditionalStatus {

    @Override
    protected void prepareConditionData() {
        this.studentCustomStateCode = CheckStatusAcademicalVacationOnMedicalStatement.ACADEMICAL_VACATION_ON_MEDICAL_STATEMENT_CODE;
        this.studentCustomStateCI = getCatalogItem(StudentCustomStateCI.class, studentCustomStateCode);
        this.studentStatus = getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_ACADEM);
    }

    @Override
    protected boolean hasStudentCustomState(Student student) {
        return super.hasStudentCustomState(student) ||
                student.getStatus().getCode().equals(UniDefines.CATALOG_STUDENT_STATUS_ACADEM);
    }
}
