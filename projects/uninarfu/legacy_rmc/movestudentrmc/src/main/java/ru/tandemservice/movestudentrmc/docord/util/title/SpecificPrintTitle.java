package ru.tandemservice.movestudentrmc.docord.util.title;

import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.util.RtfString;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;

public class SpecificPrintTitle extends UniBaseDao implements ISpecificPrintTitle {

    /**
     * Алгоритм RM#1858
     * "направление подготовки" / "специальность"
     * ПРОБЕЛ код_направления_подготовки
     * ПРОБЕЛ наименование_направления_подготовки {_если имеется профиль, то далее_}
     * ЗАПЯТАЯ ПРОБЕЛ
     * "профиль" / "магистерская программа " / "специализация"
     * ОТКРЫВАЮЩАЯ КОВЫЧКА наименование_профиля ЗАКРЫВАЮЩАЯ КОВЫЧКА
     */
    @Override
    public RtfString getSpecificPrintTitle(EducationLevelsHighSchool edLevel) {
        EducationLevels level = edLevel.getEducationLevel();

        RtfString result = new RtfString();

        boolean isHasProfile = edLevel.getEducationLevel().getLevelType().isSpecialization()
                || edLevel.getEducationLevel().getLevelType().isProfile();

        result
                .append(edLevel.isSpeciality() || edLevel.isSpecialization() ? "специальность" : "направление подготовки")
                //----------------------------------------------------------------------------------------------------
                //.append(" ").append(UtilPrintSupport.getEduLevelCodeWithQualification(edLevel.getEducationLevel()))
                .append(" ").append(UtilPrintSupport.getEduLevelCodeWithQualification(edLevel.getEducationLevel()).trim())
                //----------------------------------------------------------------------------------------------------
                .append(IRtfData.SYMBOL_TILDE).append(isHasProfile ? level.getParentLevel().getTitle() : edLevel.getTitle());

        if (isHasProfile) {
        	//--------------------------------------------------------------------------
        	String levelTypeTitle = "";
        	if(edLevel.getEducationLevel().getLevelType().isMaster()){
        		levelTypeTitle = "магистерская программа";
        	}
        	else if(edLevel.getEducationLevel().getLevelType().isBachelor()){
        		levelTypeTitle = "профиль";
        	}
        	else if(edLevel.getEducationLevel().getLevelType().isSpecialization()){
        		levelTypeTitle = "специализация";
        	}
        	else{
        		levelTypeTitle = edLevel.getEducationLevel().getLevelType().getTitle();
        	}
        	//--------------------------------------------------------------------------
            result
                    .append(", ")
                    //-----------------------------------------------------------------------------------------------------------------------------------------------
                    //.append(edLevel.getEducationLevel().getLevelType().isMaster() ? "магистерская программа" : edLevel.getEducationLevel().getLevelType().getTitle())
                    .append(levelTypeTitle)
                    //-----------------------------------------------------------------------------------------------------------------------------------------------
                    .append(" ")
                    .append("«" + edLevel.getTitle() + "»");

        }

        return result;
    }


}
