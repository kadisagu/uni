package ru.tandemservice.movestudentrmc.base.bo.ListRepresentSupport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentSupport.logic.ListRepresentSupportManagerModifyDAO;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentSupport.ui.Edit.ListRepresentSupportEdit;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentSupport.ui.View.ListRepresentSupportView;
import ru.tandemservice.movestudentrmc.docord.IListObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IListObjectModifyDAO;


@Configuration
public class ListRepresentSupportManager extends BusinessObjectManager implements IListObjectByRepresentTypeManager {

    public static ListRepresentSupportManager instance()
    {
        return instance(ListRepresentSupportManager.class);
    }


    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return ListRepresentSupportEdit.class;
    }


    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return ListRepresentSupportView.class;
    }


    @Bean
    @Override
    public IListObjectModifyDAO getListObjectModifyDAO() {
        return new ListRepresentSupportManagerModifyDAO();
    }
}
