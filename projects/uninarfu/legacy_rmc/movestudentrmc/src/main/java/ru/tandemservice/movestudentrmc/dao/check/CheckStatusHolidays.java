package ru.tandemservice.movestudentrmc.dao.check;

import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;

public class CheckStatusHolidays extends AbstractCheckStudentAdditionalStatus {

    private final static String HOLIDAYS_CODE = "movestudentrmc.holidays";

    @Override
    protected void prepareConditionData() {
        this.studentCustomStateCode = HOLIDAYS_CODE;
        this.studentCustomStateCI = getCatalogItem(StudentCustomStateCI.class, studentCustomStateCode);
    }

}
