package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.StudentTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.List.DORepresentBaseList;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.ListCommit.DORepresentBaseListCommit;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ui.Student.ListRepresentBaseStudent;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ui.StudentCommit.ListRepresentBaseStudentCommit;

@Configuration
public class DORepresentBaseStudentTab extends BusinessComponentManager {

    //Панель вкладок
    public static final String TAB_PANEL = "tabPanel";

    //Вкладки
    public static final String REPRESENT_BASE_TAB = "representBaseTab";
    public static final String REPRESENT_BASE_COMMIT_TAB = "representBaseCommitTab";

    public static final String LIST_REPRESENT_BASE_TAB = "listRepresentBaseTab";
    public static final String LIST_REPRESENT_BASE_COMMIT_TAB = "listRepresentBaseCommitTab";

    @Bean
    public TabPanelExtPoint tabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(TAB_PANEL)
                .addTab(componentTab(REPRESENT_BASE_TAB, DORepresentBaseList.class).parameters("ui:paramsRepresentBaseTab").permissionKey("rmc_view_represent_tab"))
                .addTab(componentTab(REPRESENT_BASE_COMMIT_TAB, DORepresentBaseListCommit.class).parameters("ui:paramsRepresentBaseTab").permissionKey("rmc_view_commited_represent_tab"))

                .addTab(componentTab(LIST_REPRESENT_BASE_TAB, ListRepresentBaseStudent.class).parameters("ui:paramsRepresentBaseTab").permissionKey("rmc_view_list_represent_tab"))
                .addTab(componentTab(LIST_REPRESENT_BASE_COMMIT_TAB, ListRepresentBaseStudentCommit.class).parameters("ui:paramsRepresentBaseTab").permissionKey("rmc_view_list_commited_represent_tab"))
                .create();
    }

}
