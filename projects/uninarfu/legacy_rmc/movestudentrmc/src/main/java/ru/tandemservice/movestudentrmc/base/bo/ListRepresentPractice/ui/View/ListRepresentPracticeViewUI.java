package ru.tandemservice.movestudentrmc.base.bo.ListRepresentPractice.ui.View;


import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.util.RtfUtil;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ListRepresentBaseManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.support.AbstractListRepresentViewUI;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentPractice.logic.ListPrintDoc;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentPractice.logic.ListRepresentPracticePrintDoc;
import ru.tandemservice.movestudentrmc.dao.CheckStateUtil;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListRepresentPractice;
import ru.tandemservice.movestudentrmc.entity.catalog.MovestudentExtractStates;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentExtractStatesCodes;
import ru.tandemservice.uni.util.ReportRenderer;

import java.util.Collections;

public class ListRepresentPracticeViewUI extends AbstractListRepresentViewUI<ListRepresentPractice>
{

    @Override
    public ListRepresentPractice getListRepresentObject()
    {
        return new ListRepresentPractice();
    }

    @Override
    protected RtfDocument printRepresent()
    {
        return new ListPrintDoc().creationPrintDocOrder(Collections.singletonList(getListRepresent()));
    }

    @Override
    public void onClickPrint()
    {
        IDocumentRenderer doc;
        ListRepresent listRepresent = getListRepresent();
        if (listRepresent.getDocument() == null)
        {
            ListRepresentPracticePrintDoc printDoc = new ListRepresentPracticePrintDoc();
            RtfDocument docMain = printDoc.createDocument(Collections.singletonList(listRepresent));
            doc = new ReportRenderer("Списочное представление.rtf", docMain, false);
        }
        else
            doc = new ReportRenderer("Списочное представление.rtf", listRepresent.getDocument(), false);

        BusinessComponentUtils.downloadDocument(doc, true);

    }

    @Override
    public void onClickDoApprove()
    {
        ListRepresent listRepresent = getListRepresent();

        CheckStateUtil.checkStateRepresent(listRepresent,
                                           Collections.singletonList(MovestudentExtractStatesCodes.CODE_2),
                                           getSupport().getSession());
        listRepresent.setState(DataAccessServices.dao().get(MovestudentExtractStates.class, MovestudentExtractStates.code().s(), "3"));
        ListRepresentPracticePrintDoc printDoc = new ListRepresentPracticePrintDoc();
        RtfDocument docMain = printDoc.createDocument(Collections.singletonList(listRepresent));
        if (docMain != null)
            listRepresent.setDocument(RtfUtil.toByteArray(docMain));

        ListRepresentBaseManager.instance().modifyDao().updateState(listRepresent);
    }
}
