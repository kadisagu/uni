package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class TypeRepresentDSHandler extends DefaultComboDataSourceHandler {

    public TypeRepresentDSHandler(String ownerId) {
        super(ownerId, RepresentationType.class, RepresentationType.P_TITLE);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        String filter = ep.input.getComboFilterByValue();
        if (StringUtils.isNotBlank(filter)) {
            ep.dqlBuilder.where(like(DQLFunctions.upper(property(RepresentationType.title().fromAlias("e"))), value(CoreStringUtils.escapeLike(filter, true))));
        }
        ep.dqlBuilder.where(eq(DQLFunctions.upper(property(RepresentationType.listRepresentation().fromAlias("e"))), value(true)));
    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
    {
        ep.dqlBuilder.order("e." + RepresentationType.title());
    }
}
