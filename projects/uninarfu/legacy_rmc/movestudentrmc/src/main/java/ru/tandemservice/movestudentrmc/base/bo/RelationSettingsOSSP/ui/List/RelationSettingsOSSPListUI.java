package ru.tandemservice.movestudentrmc.base.bo.RelationSettingsOSSP.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import ru.tandemservice.movestudentrmc.base.bo.RelationSettingsOSSP.ui.Edit.RelationSettingsOSSPEdit;
import ru.tandemservice.movestudentrmc.base.bo.RelationSettingsOSSP.ui.Edit.RelationSettingsOSSPEditUI;

public class RelationSettingsOSSPListUI extends UIPresenter {

    @Override
    public void onComponentRefresh()
    {
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegion(RelationSettingsOSSPEdit.class)
                .parameter(RelationSettingsOSSPEditUI.TYPE_REPRESENTATION_ID, getListenerParameterAsLong())
                .activate();
    }
}
