package ru.tandemservice.movestudentrmc.tapestry.translators;

import org.apache.tapestry.IMarkupWriter;
import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.form.FormComponentContributorContext;
import org.apache.tapestry.form.IFormComponent;
import org.apache.tapestry.form.ValidationMessages;
import org.apache.tapestry.form.translator.AbstractTranslator;
import org.apache.tapestry.valid.ValidatorException;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.Locale;

public class RMCTranslator extends AbstractTranslator {

    public RMCTranslator() {
    }

    @Override
    public void renderContribution(IMarkupWriter writer, IRequestCycle cycle, FormComponentContributorContext context, IFormComponent field) {
        int a = 5;
        super.renderContribution(writer, cycle, context, field);
    }

    @Override
    public String format(IFormComponent field, Locale locale, Object object) {
        int a = 5;
        return super.format(field, locale, object);
    }

    @Override
    protected String formatObject(IFormComponent field, Locale locale, Object object) {
        int a = 5;
        if (object instanceof Group)
            return ((Group) object).getFullTitle();
        else if (object != null)
            return object.toString();
        else
            return null;
    }

    @Override
    public Object parse(IFormComponent field, ValidationMessages messages, String text) throws ValidatorException {
        int a = 5;
        return super.parse(field, messages, text);
    }

    @Override
    protected Object parseText(IFormComponent field, ValidationMessages messages, String text) throws ValidatorException {
        int a = 5;
        return null;
    }

}
