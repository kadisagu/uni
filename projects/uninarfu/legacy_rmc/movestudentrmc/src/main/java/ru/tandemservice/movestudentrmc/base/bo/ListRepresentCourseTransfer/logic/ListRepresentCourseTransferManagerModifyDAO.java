package ru.tandemservice.movestudentrmc.base.bo.ListRepresentCourseTransfer.logic;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;

import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.AbstractListRepresentDAO;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListRepresentCourseTransferManagerModifyDAO extends AbstractListRepresentDAO {
    private Map<Student, Integer> studentNumber = new HashMap<>();

    @Override
    public void save(ListRepresent listRepresent, List<Student> studentSelectedList, DocListRepresentBasics representBasics) {

        baseCreate(listRepresent);

        baseCreateOrUpdate(representBasics);

        ListRepresentCourseTransfer courseTransfer = (ListRepresentCourseTransfer) listRepresent;

        for (Student student : studentSelectedList) {

            RelListRepresentStudentsOldData relListRepresentCourseTransferStudents = new RelListRepresentStudentsOldData();
            relListRepresentCourseTransferStudents.setRepresentation(courseTransfer);
            relListRepresentCourseTransferStudents.setStudent(student);
            relListRepresentCourseTransferStudents.setOldCourse(student.getCourse());
            baseCreateOrUpdate(relListRepresentCourseTransferStudents);
        }
    }

    @Override
    public void update(ListRepresent listRepresent, List<Student> studentSelectedList) {

        baseCreateOrUpdate(listRepresent);
        ListRepresentCourseTransfer courseTransfer = (ListRepresentCourseTransfer) listRepresent;
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(RelListRepresentStudents.class, "r").column("r");
        builder.where(DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("r")), DQLExpressions.value(listRepresent)));
        List<RelListRepresentStudents> representSelectedOldList = getStudentList(listRepresent);


        for (RelListRepresentStudents representSelectedOld : representSelectedOldList) {
            Boolean isFind = false;
            for (Student studentSelected : studentSelectedList) {
                if (representSelectedOld.getStudent().equals(studentSelected)) {
                    isFind = true;
                    studentSelectedList.remove(studentSelected);
                    break;
                }
            }
            if (!isFind) {
                ListRepresentCourseTransfer represent = (ListRepresentCourseTransfer) representSelectedOld.getRepresentation();
                baseCreateOrUpdate(represent);
                baseDelete(representSelectedOld);
            }
        }

        for (Student student : studentSelectedList) {
            RelListRepresentStudentsOldData relListRepresentCourseTransferStudents = new RelListRepresentStudentsOldData();
            relListRepresentCourseTransferStudents.setRepresentation(courseTransfer);
            relListRepresentCourseTransferStudents.setStudent(student);
            relListRepresentCourseTransferStudents.setOldCourse(student.getCourse());
            baseCreateOrUpdate(relListRepresentCourseTransferStudents);
        }
    }

    @Override
    public void buildBodyExtract(ListOrdListRepresent listOrdListRepresent,
                                 Student student, RtfDocument docExtract, Map<Student, OrderParagraphInfo> map)
    {
        super.buildBodyExtract(listOrdListRepresent, student, docExtract, map);


        String compType = student.getCompensationType().isBudget() ? "" : " (по договору)";

        ListRepresentCourseTransfer listRepresent = (ListRepresentCourseTransfer) listOrdListRepresent.getRepresentation();
        //-----------------------------------------------------------------------------------------------------------------
        RtfString withConditionString = new RtfString();
        
        if(!listRepresent.isConditionally()) {
        	withConditionString.append(" как полностью выполнивших учебный план ").append(student.getCourse().getTitle()).append(" курса");
        }
        else {	//условный перевод
            String day = RussianDateFormatUtils.getDayString(listRepresent.getDeadlineDate(), true);
            String month = CommonBaseDateUtil.getMonthNameDeclined(listRepresent.getDeadlineDate(), GrammaCase.GENITIVE);
            String year = RussianDateFormatUtils.getYearString(listRepresent.getDeadlineDate(), false);
            withConditionString.append(" и установить срок ликвидации академической задолженности до ").append(day).append(IRtfData.SYMBOL_TILDE).append(month).append(" ").append(year).append(IRtfData.SYMBOL_TILDE).append("года");
        }
        //-----------------------------------------------------------------------------------------------------------------
        new RtfInjectModifier()
                .put("studentTitle", student.getPerson().getFullFio() + compType)
                .put("data", UtilPrintSupport.getDateFormatterWithMonthString(listRepresent.getDateBeginingTransfer()))
                .put("courseNew", listRepresent.getNewCourse().getTitle())
                .put("highSchoolTitle", UtilPrintSupport.getHighLevelSchoolTypeString(student.getEducationOrgUnit().getEducationLevelHighSchool()))
                .put("group", student.getGroup() == null ? "" : "группа " + student.getGroup().getTitle())
                .put("k", studentNumber.get(student) == null ? "1" : studentNumber.get(student).toString())
                //--------------------------------------------------------------------------------------------------------------------------------------------------
                .put("conditionally", listRepresent.isConditionally() ? " условно" : "")
                .put("withCondition", withConditionString)
                //--------------------------------------------------------------------------------------------------------------------------------------------------
                .modify(docExtract);
    }

    @Override
    public Map<Student, OrderParagraphInfo> buildBodyRepresent(List<ListRepresent> representationBase, RtfDocument document) {
        ListPrintDoc listPrintDoc = new ListPrintDoc();
        RtfDocument templateRepresent = listPrintDoc.creationPrintDocOrder(representationBase);

        document.setHeader(templateRepresent.getHeader());
        document.setSettings(templateRepresent.getSettings());

        document.getElementList().addAll(templateRepresent.getElementList());
        setStudentNumber(listPrintDoc.getStudentNumberMap());
        return listPrintDoc.getParagInfomap();

    }


    @Override
    public boolean doRollback(ListRepresent represent, ErrorCollector error) {
        List<RelListRepresentStudents> representList = getStudentList(represent);

        for (RelListRepresentStudents representStudents : representList) {
            RelListRepresentStudentsOldData oldData = (RelListRepresentStudentsOldData) representStudents;
            Student student = oldData.getStudent();
            student.setCourse(oldData.getOldCourse());
            getSession().saveOrUpdate(student);
        }

        return true;
    }

    @Override
    public boolean doCommit(ListRepresent represent, ErrorCollector error) {
        ListRepresentCourseTransfer сourseTransfer = (ListRepresentCourseTransfer) represent;
        List<RelListRepresentStudents> representList = getStudentList(represent);

        for (RelListRepresentStudents representStudents : representList) {
            Student student = representStudents.getStudent();

            student.setCourse(сourseTransfer.getNewCourse());
            getSession().saveOrUpdate(student);
        }

        return true;
    }


    @Override
    public List<Student> selectRepresent(ListRepresent listRepresent) {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(RelListRepresentStudents.class, "rls");

        builder.where(DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("rls")), DQLExpressions.value(listRepresent)));

        builder.column(DQLExpressions.property(RelListRepresentStudents.student().fromAlias("rls")));

        return builder.createStatement(getSession()).list();
    }

    public void saveSupport(ListRepresent listRepresent, List<DataWrapper> studentSelectedList, DocListRepresentBasics representBasics) {
    }

    public Map<Student, Integer> getStudentNumber() {
        return studentNumber;
    }

    public void setStudentNumber(Map<Student, Integer> studentNumber) {
        this.studentNumber = studentNumber;
    }
}
