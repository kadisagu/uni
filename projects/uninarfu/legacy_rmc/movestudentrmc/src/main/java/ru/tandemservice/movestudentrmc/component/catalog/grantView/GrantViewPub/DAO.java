package ru.tandemservice.movestudentrmc.component.catalog.grantView.GrantViewPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;

public class DAO extends DefaultCatalogPubDAO<GrantView, Model> implements IDAO {

    public void updateInUse(Model model, Long id) {
        GrantView grantView = getNotNull(GrantView.class, id);
        grantView.setUse(!grantView.isUse());
        saveOrUpdate(grantView);
    }

    public void updateInSuspend(Model model, Long id) {
        GrantView grantView = getNotNull(GrantView.class, id);
        grantView.setSuspend(!grantView.isSuspend());
        saveOrUpdate(grantView);
    }
}
