package ru.tandemservice.movestudentrmc.dao.check;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.*;

public abstract class AbstractCheckStudentAdditionalStatus extends
        AbstractCheckDAO
{

    protected String studentCustomStateCode;
    protected StudentCustomStateCI studentCustomStateCI;
    protected StudentStatus studentStatus;
    protected Map<Long, List<StudentCustomState>> studentCustomStateMap;

    /**
     * Инициализация критериев проверки (в частности studentCustomStateCode и
     * studentCustomStateCI)
     */
    protected abstract void prepareConditionData();

    protected void prepareStudentCustomStateCache(List<Student> students) {
        this.studentCustomStateMap = getStudentCustomStatesMap(students);
    }

    @Override
    public List<String> doCheck(List<Student> list, boolean value) {
        prepareConditionData();
        prepareStudentCustomStateCache(list);

        return super.doCheck(list, value);
    }

    @Override
    protected boolean getCondition(Student student) {
        return hasStudentCustomState(student);
    }

    /**
     * Проверка актуальности дополнительного статуса студента
     *
     * @param studentCustomState
     *
     * @return
     */
    protected boolean isActualPeriod(StudentCustomState studentCustomState) {

        if (studentCustomState == null)
            return false;

        boolean isActualPeriod = false;

        Date currentDate = Calendar.getInstance().getTime();
        Date beginActiveDate = studentCustomState.getBeginDate();
        Date endActiveDate = studentCustomState.getEndDate();

        if ((beginActiveDate == null && endActiveDate == null)
                || (beginActiveDate == null && endActiveDate != null && currentDate.before(endActiveDate))
                || (beginActiveDate != null && endActiveDate == null && currentDate.after(beginActiveDate))
                || (beginActiveDate != null && endActiveDate != null && currentDate.after(beginActiveDate) && currentDate.before(endActiveDate)))
            isActualPeriod = true;

        else isActualPeriod = false;

        return isActualPeriod;
    }

    @Override
    protected String getErrorMessage(Student student, boolean value) {
        StringBuilder sb = new StringBuilder();
        if (!studentCustomStateMap.containsKey(student.getId())) {
            sb.append("Для студента ").append(student.getPerson().getFullFio())
                    .append(" ").append("не указан дополнительный статус");
        }
        else {
            sb.append("Студент ")
                    .append(student.getPerson().getFullFio())
                    .append(" ")
                    .append(value ? "не " : "")
                    .append("находится в состоянии «")
                    .append(StringUtils.capitalize(studentCustomStateCI.getTitle()))
                    .append("»");
            if (!value && studentStatus != null)
                sb.append(" или «")
                        .append(StringUtils.capitalize(studentStatus.getTitle()))
                        .append("»");
        }
        return sb.toString();
    }

    protected boolean hasStudentCustomState(Student student) {

        if (studentCustomStateCI == null)
            return false;

        if (studentCustomStateMap.containsKey(student.getId())) {
            List<StudentCustomState> studentCustomStates = studentCustomStateMap.get(student.getId());
            for (StudentCustomState studentCustomState : studentCustomStates) {
                if (studentCustomState.getCustomState().getCode().equals(studentCustomStateCode) && isActualPeriod(studentCustomState))
                    return true;
            }
        }

        return false;
    }

    /**
     * Локальный кэш для дополнительных статусов тех студентов, которых нужно
     * проверять
     *
     * @param studentList
     *
     * @return
     */
    private Map<Long, List<StudentCustomState>> getStudentCustomStatesMap(List<Student> studentList) {
        Map<Long, List<StudentCustomState>> studentStatesMap = new HashMap<>();

        if (!studentList.isEmpty()) {
            List<Long> studentIds = new ArrayList<>();

            for (Student student : studentList)
                studentIds.add(student.getId());

            DQLSelectBuilder builder = new DQLSelectBuilder()
                    .fromEntity(StudentCustomState.class, "scs")
                    .where(DQLExpressions.in(DQLExpressions.property(StudentCustomState.student().id().fromAlias("scs")),
                                             studentIds));

            List<StudentCustomState> customStates = getList(builder);

            for (StudentCustomState state : customStates) {
                Long studentId = state.getStudent().getId();
                if (studentStatesMap.containsKey(studentId)) {
                    studentStatesMap.get(studentId).add(state);
                }
                else {
                    List<StudentCustomState> lst = new ArrayList<>();
                    lst.add(state);
                    studentStatesMap.put(studentId, lst);
                }
            }
        }

        return studentStatesMap;
    }
}
