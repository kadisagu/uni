package ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.DateColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        ((IDAO) getDao()).prepare(getModel(component));
        prepareDataSource(component);
    }

    protected void prepareDataSource(IBusinessComponent component) {
        final Model model = getModel(component);
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<GrantWrapper> dataSource = new DynamicListDataSource<>(component, arg0 -> {
            ((IDAO) getDao()).prepareListDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("Вид стипендии", "grant.view.title").setClickable(false));
        dataSource.addColumn(new SimpleColumn("Сумма (руб)", "ruble").setClickable(false));
        dataSource.addColumn(new SimpleColumn("Сумма (коп)", "kopec").setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Номер приказа", "grant.orderNumber").setClickable(false));
        dataSource.addColumn(new DateColumn("Дата приказа", "grant.orderDate").setClickable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit").setPermissionKey("rmc_editGrantEntity"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete").setPermissionKey("rmc_deleteGrantEntity"));

        model.setDataSource(dataSource);
    }

    public void onClickEdit(IBusinessComponent component) {
        Model model = getModel(component);

        Long id = component.getListenerParameter();
        GrantWrapper wrapper = model.getDataSource().getRecordById(id);

        activate(
                component,
                new ComponentActivator("ru.tandemservice.movestudentrmc.component.orgUnit.GrantAddEdit", new ParametersMap().add("orgUnitId", model.getOrgUnit().getId()).add("grantEntity", wrapper.getGrant()))
        );

    }

    public void onClickDelete(IBusinessComponent component) {
        Model model = getModel(component);

        Long id = component.getListenerParameter();
        GrantWrapper wrapper = model.getDataSource().getRecordById(id);
        if (wrapper.getGrant().getId() != null)
            ((IDAO) getDao()).deleteRow(wrapper.getGrant());
    }

    public void onClickSearch(IBusinessComponent component) {
        Model model = getModel(component);
        model.getDataSource().refresh();
    }
}
