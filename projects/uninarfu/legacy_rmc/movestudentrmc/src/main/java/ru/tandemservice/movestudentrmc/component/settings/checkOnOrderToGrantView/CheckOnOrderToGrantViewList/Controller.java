package ru.tandemservice.movestudentrmc.component.settings.checkOnOrderToGrantView.CheckOnOrderToGrantViewList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.BooleanColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudentrmc.entity.catalog.CheckOnOrder;
import ru.tandemservice.uni.component.settings.abstractRelation.AbstractRelationList.AbstractRelationListController;

public class Controller extends AbstractRelationListController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        super.onRefreshComponent(component);
        Model model = (Model) getModel(component);

        DynamicListDataSource<CheckOnOrder> dataSource = model.getDataSource();

        if (dataSource.getColumn("grantConfigRelation") == null) {
            dataSource.addColumn(new BooleanColumn("Настроено", CheckOnOrder.grantConfigRelation()));
            model.setDataSource(dataSource);
        }
    }

    @Override
    protected String getRelationPubComponent() {
        return "ru.tandemservice.movestudentrmc.component.settings.checkOnOrderToGrantView.CheckOnOrderToGrantViewPub";
    }

}
