package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.EditBasic;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.*;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.movestudentrmc.docord.EditBasicGroupList;
import ru.tandemservice.movestudentrmc.entity.DocRepresentBasics;
import ru.tandemservice.movestudentrmc.entity.NarfuDocument;
import ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonBasic;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationBasement;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.movestudentrmc.util.ListenerWrapper;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@State(
        @Bind(key = DORepresentBaseEditBasicUI.STUDENT_LIST_ID, binding = DORepresentBaseEditBasicUI.STUDENT_LIST_ID)
)
@Input({
        @Bind(key = DORepresentBaseEditBasicUI.REPRESENT_TYPE, binding = DORepresentBaseEditBasicUI.REPRESENT_TYPE, required = true),
        @Bind(key = DORepresentBaseEditBasicUI.REPRESENT_BASIC_LIST, binding = DORepresentBaseEditBasicUI.REPRESENT_BASIC_LIST, required = true),
        @Bind(key = DORepresentBaseEditBasicUI.REPRESENT, binding = DORepresentBaseEditBasicUI.REPRESENT, required = true),
        @Bind(key = DORepresentBaseEditBasicUI.REASON, binding = DORepresentBaseEditBasicUI.REASON)
})
public class DORepresentBaseEditBasicUI extends UIPresenter implements ListenerWrapper.IListener<NarfuDocument>
{
    public static final String STUDENT_LIST_ID = "studentListId";

    public static final String REPRESENT_TYPE = "representType";
    public static final String REPRESENT_BASIC_LIST = "representBasicList";
    public static final String REPRESENT = "represent";
    public static final String REASON = "reason";

    private List<Long> _studentListId;

    private RepresentationType _representType;
    private Representation _represent;
    private RepresentationReason _reason;
    private List<DocRepresentBasics> _representBasicList;
    private DocRepresentBasics _representBasicRow;

    private RepresentationReason _oldReason;
    private List<EditBasicGroupList> _basicGroupList = new ArrayList<EditBasicGroupList>();
    private EditBasicGroupList _basicGroupRow;
    private List<RepresentationBasement> _basicRequiredList = new ArrayList<RepresentationBasement>();
    private List<RepresentationBasement> _basicNoRequiredList = new ArrayList<RepresentationBasement>();
    private List<RepresentationBasement> _basicNoRequiredListSelected = new ArrayList<RepresentationBasement>();

//    События компонента

    @Override
    public void onComponentRefresh() {

        if (_representBasicList.isEmpty())
            onChangeReason();
        else
            basicsNotEmpty(_represent);
    }

//    События формы компонента

    public void onChangeBasic() {

        fillBasicCommentList();
    }

    public void onChangeGrants() {

        fillGrantsAdditionalList();
    }

//    Вспомогательные функции

    public void onChangeReason() {

        fillBasicList();
    }

    //работа с документами
    public void onClickAddDocument() {
        Student student = UniDaoFacade.getCoreDao().get(Student.class, getStudentListId().get(0));

        ListenerWrapper<NarfuDocument> listener = new ListenerWrapper<NarfuDocument>(this);
        listener.setParam(getListenerParameter());


        Activator activator = new ComponentActivator(
                "ru.tandemservice.movestudentrmc.component.represent.SelectStudentDocument",
                new UniMap().add("studentId", student.getId()).add("listener", listener)
        );

        ContextLocal.createDesktop("PersonShellDialog", activator);
    }

    public void onClickEditDocument() {
        Student student = UniDaoFacade.getCoreDao().get(Student.class, getStudentListId().get(0));
        DocRepresentBasics basics = getListenerParameter();
        ListenerWrapper<NarfuDocument> listener = new ListenerWrapper<NarfuDocument>(this);
        listener.setParam(basics);


        Activator activator = new ComponentActivator(
                "ru.tandemservice.movestudentrmc.component.student.DocumentAddEdit",
                new UniMap().add("studentId", student.getId()).add("listener", listener).add("documentId", basics.getDocument().getId())
        );

        ContextLocal.createDesktop("PersonShellDialog", activator);
    }

    @Override
    public void listen(ListenerWrapper<NarfuDocument> wrapper) {
        if (wrapper.isOk()) {
            NarfuDocument doc = wrapper.getResult();
            DocRepresentBasics basics = (DocRepresentBasics) wrapper.getParam();
            basics.setDocument(doc);
        }
    }

    //    Заполнение списков основаниями
    protected void fillBasicList() {

        if (_reason == null)
            return;

        if (_oldReason == null)
            _oldReason = _reason;
        else if (_oldReason != _reason) {

            clearRepresentBasicsList();

            _oldReason = _reason;
        }
        else
            return;

        _basicGroupList.clear();
        _basicRequiredList.clear();
        _basicNoRequiredList.clear();
        _basicNoRequiredListSelected.clear();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RelRepresentationReasonBasic.class, "rel").addColumn("rel");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(RelRepresentationReasonBasic.type().id().fromAlias("rel")),
                DQLExpressions.value(_represent.getType().getId())));
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(RelRepresentationReasonBasic.reason().fromAlias("rel")),
                DQLExpressions.value(_reason)));


        List<RelRepresentationReasonBasic> listReasonBasic = builder.createStatement(getSupport().getSession()).list();

        for (RelRepresentationReasonBasic reasonBasic : listReasonBasic) {

            if (reasonBasic.isRequired() && !reasonBasic.isJoinGroup()) {
                _basicRequiredList.add(reasonBasic.getBasic());
            }
            else if (reasonBasic.isRequired() && reasonBasic.isJoinGroup()) {
                Boolean isFind = false;
                for (EditBasicGroupList basicGroup : _basicGroupList) {

                    if (basicGroup.getGroupName().equals(reasonBasic.getGroupName())) {
                        isFind = true;
                        basicGroup.getBasicList().add(reasonBasic.getBasic());
                    }
                }
                if (!isFind) {
                    EditBasicGroupList basicGroup = new EditBasicGroupList();
                    basicGroup.setGroupName(reasonBasic.getGroupName());
                    basicGroup.setBasicList(new ArrayList<RepresentationBasement>());
                    basicGroup.getBasicList().add(reasonBasic.getBasic());
                    basicGroup.setGroupName(reasonBasic.getGroupName());
                    basicGroup.setGroupTitle("Группа оснований: " + reasonBasic.getGroupName());
                    _basicGroupList.add(basicGroup);
                }
            }
            else if (!reasonBasic.isRequired()) {
                _basicNoRequiredList.add(reasonBasic.getBasic());
            }
        }

        fillBasicCommentList();
    }

    private void clearRepresentBasicsList() {

        if (!_representBasicList.isEmpty()) {

            for (Iterator<DocRepresentBasics> representBasicsIter = _representBasicList.iterator(); representBasicsIter.hasNext(); ) {
                DocRepresentBasics representBasics = representBasicsIter.next();

                if (_oldReason.equals(representBasics.getReason()) || _reason.equals(representBasics.getReason())) {
                    representBasicsIter.remove();
                }
            }
        }
    }

    //    Заполнение списка для комментирования оснваний
    private void fillBasicCommentList() {

        List<RepresentationBasement> basicList = new ArrayList<RepresentationBasement>();
        List<DocRepresentBasics> representBasicsList = new ArrayList<DocRepresentBasics>();
        basicList.addAll(_basicRequiredList);
        basicList.addAll(_basicNoRequiredListSelected);
        for (EditBasicGroupList editBasicGroup : _basicGroupList) {
            if (editBasicGroup.getSelected() != null)
                basicList.add(editBasicGroup.getSelected());
        }

        for (RepresentationBasement basic : basicList) {
            Boolean isFind = false;
            for (DocRepresentBasics representBasics : _representBasicList) {
                if (basic.equals(representBasics.getBasic()) && _reason.equals(representBasics.getReason())) {
                    isFind = true;
                    representBasicsList.add(representBasics);
                    break;
                }
            }
            if (!isFind) {
                DocRepresentBasics representBasics = new DocRepresentBasics();
                representBasics.setRepresentation(_represent);
                representBasics.setBasic(basic);
                representBasics.setReason(_reason);
                representBasicsList.add(representBasics);
            }
        }

        clearRepresentBasicsList();
        _representBasicList.addAll(representBasicsList);
    }

    //Заполнение списка для дополнительных полей стипендий

    private void fillGrantsAdditionalList() {

    }

    //    Если список оснований пришел не пустой
    private void basicsNotEmpty(Representation doc) {

        List<DocRepresentBasics> representBasicList = new ArrayList<DocRepresentBasics>();
        representBasicList.addAll(_representBasicList);
        fillBasicList();
        _representBasicList.clear();
        _representBasicList.addAll(representBasicList);

        for (DocRepresentBasics representBasics : representBasicList) {

            if (_basicNoRequiredList.contains(representBasics.getBasic())) {

                _basicNoRequiredListSelected.add(representBasics.getBasic());

            }
            else {
                for (EditBasicGroupList basicGroup : _basicGroupList) {

                    if (basicGroup.getBasicList().contains(representBasics.getBasic())) {
                        basicGroup.setSelected(representBasics.getBasic());
                        break;
                    }
                }
            }
        }
    }

//    Getters

    public Boolean getDoShowComment() {
        return (_representBasicRow.getBasic().isNoteRequired() && _representBasicRow.getReason().equals(_reason));
    }

    public Boolean getDoShowDocument() {
        return (_representBasicRow.getBasic().isConnectedWithDocument() && _representBasicRow.getReason().equals(_reason));
    }

    public Boolean getDoHasDocument() {
        return (_representBasicRow.getDocument() != null && _representBasicRow.getReason().equals(_reason));
    }

    public Boolean getDoTmpDocument() {
        return (_representBasicRow.getDocument() != null && Boolean.TRUE.equals(_representBasicRow.getDocument().getTmp()) && _representBasicRow.getReason().equals(_reason));
    }

    public Boolean getIsReasonNull() {

        return (_reason == null);
    }

    public Boolean getNotComments() {

        Boolean isFind = true;
        for (DocRepresentBasics representBasics : _representBasicList) {
            if (representBasics.getReason().equals(_reason)) {
                isFind = false;
            }
        }

        return isFind;
    }

//    Getters and setters

    public RepresentationReason getReason() {
        return _reason;
    }

    public void setReason(RepresentationReason reason) {
        _reason = reason;
    }

    public List<EditBasicGroupList> getBasicGroupList() {
        return _basicGroupList;
    }

    public void setBasicGroupList(List<EditBasicGroupList> basicGroupList) {
        _basicGroupList = basicGroupList;
    }

    public List<RepresentationBasement> getBasicRequiredList() {
        return _basicRequiredList;
    }

    public void setBasicRequiredList(List<RepresentationBasement> basicRequiredList) {
        _basicRequiredList = basicRequiredList;
    }

    public List<RepresentationBasement> getBasicNoRequiredList() {
        return _basicNoRequiredList;
    }

    public void setBasicNoRequiredList(List<RepresentationBasement> basicNoRequiredList) {
        _basicNoRequiredList = basicNoRequiredList;
    }

    public List<RepresentationBasement> getBasicNoRequiredListSelected() {
        return _basicNoRequiredListSelected;
    }

    public void setBasicNoRequiredListSelected(List<RepresentationBasement> basicNoRequiredListSelected) {
        _basicNoRequiredListSelected = basicNoRequiredListSelected;
    }

    public Representation getRepresent() {
        return _represent;
    }

    public void setRepresent(Representation represent) {
        _represent = represent;
    }

    public List<DocRepresentBasics> getRepresentBasicList() {
        return _representBasicList;
    }

    public void setRepresentBasicList(List<DocRepresentBasics> representBasicList) {
        _representBasicList = representBasicList;
    }

    public RepresentationType getRepresentType() {
        return _representType;
    }

    public void setRepresentType(RepresentationType representType) {
        _representType = representType;
    }

    public DocRepresentBasics getRepresentBasicRow() {
        return _representBasicRow;
    }

    public void setRepresentBasicRow(DocRepresentBasics representBasicRow) {
        _representBasicRow = representBasicRow;
    }

    public EditBasicGroupList getBasicGroupRow() {
        return _basicGroupRow;
    }

    public void setBasicGroupRow(EditBasicGroupList basicGroupRow) {
        _basicGroupRow = basicGroupRow;
    }

    public List<Long> getStudentListId() {
        return _studentListId;
    }

    public void setStudentListId(List<Long> studentListId) {
        _studentListId = studentListId;
    }
}
