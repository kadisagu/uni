package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.RelDocumentTypeKind;
import ru.tandemservice.movestudentrmc.entity.catalog.Document;
import ru.tandemservice.movestudentrmc.entity.catalog.DocumentKind;
import ru.tandemservice.movestudentrmc.entity.catalog.DocumentType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связи Тип - Вид - Документ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RelDocumentTypeKindGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.RelDocumentTypeKind";
    public static final String ENTITY_NAME = "relDocumentTypeKind";
    public static final int VERSION_HASH = -1259076178;
    private static IEntityMeta ENTITY_META;

    public static final String L_TYPE = "type";
    public static final String L_KIND = "kind";
    public static final String L_DOCUMENT = "document";

    private DocumentType _type;     // Тип Документа
    private DocumentKind _kind;     // Вид Документа
    private Document _document;     // Документ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип Документа. Свойство не может быть null.
     */
    @NotNull
    public DocumentType getType()
    {
        return _type;
    }

    /**
     * @param type Тип Документа. Свойство не может быть null.
     */
    public void setType(DocumentType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Вид Документа. Свойство не может быть null.
     */
    @NotNull
    public DocumentKind getKind()
    {
        return _kind;
    }

    /**
     * @param kind Вид Документа. Свойство не может быть null.
     */
    public void setKind(DocumentKind kind)
    {
        dirty(_kind, kind);
        _kind = kind;
    }

    /**
     * @return Документ. Свойство не может быть null.
     */
    @NotNull
    public Document getDocument()
    {
        return _document;
    }

    /**
     * @param document Документ. Свойство не может быть null.
     */
    public void setDocument(Document document)
    {
        dirty(_document, document);
        _document = document;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RelDocumentTypeKindGen)
        {
            setType(((RelDocumentTypeKind)another).getType());
            setKind(((RelDocumentTypeKind)another).getKind());
            setDocument(((RelDocumentTypeKind)another).getDocument());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RelDocumentTypeKindGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RelDocumentTypeKind.class;
        }

        public T newInstance()
        {
            return (T) new RelDocumentTypeKind();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "type":
                    return obj.getType();
                case "kind":
                    return obj.getKind();
                case "document":
                    return obj.getDocument();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "type":
                    obj.setType((DocumentType) value);
                    return;
                case "kind":
                    obj.setKind((DocumentKind) value);
                    return;
                case "document":
                    obj.setDocument((Document) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "type":
                        return true;
                case "kind":
                        return true;
                case "document":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "type":
                    return true;
                case "kind":
                    return true;
                case "document":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "type":
                    return DocumentType.class;
                case "kind":
                    return DocumentKind.class;
                case "document":
                    return Document.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RelDocumentTypeKind> _dslPath = new Path<RelDocumentTypeKind>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RelDocumentTypeKind");
    }
            

    /**
     * @return Тип Документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelDocumentTypeKind#getType()
     */
    public static DocumentType.Path<DocumentType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Вид Документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelDocumentTypeKind#getKind()
     */
    public static DocumentKind.Path<DocumentKind> kind()
    {
        return _dslPath.kind();
    }

    /**
     * @return Документ. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelDocumentTypeKind#getDocument()
     */
    public static Document.Path<Document> document()
    {
        return _dslPath.document();
    }

    public static class Path<E extends RelDocumentTypeKind> extends EntityPath<E>
    {
        private DocumentType.Path<DocumentType> _type;
        private DocumentKind.Path<DocumentKind> _kind;
        private Document.Path<Document> _document;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип Документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelDocumentTypeKind#getType()
     */
        public DocumentType.Path<DocumentType> type()
        {
            if(_type == null )
                _type = new DocumentType.Path<DocumentType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Вид Документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelDocumentTypeKind#getKind()
     */
        public DocumentKind.Path<DocumentKind> kind()
        {
            if(_kind == null )
                _kind = new DocumentKind.Path<DocumentKind>(L_KIND, this);
            return _kind;
        }

    /**
     * @return Документ. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelDocumentTypeKind#getDocument()
     */
        public Document.Path<Document> document()
        {
            if(_document == null )
                _document = new Document.Path<Document>(L_DOCUMENT, this);
            return _document;
        }

        public Class getEntityClass()
        {
            return RelDocumentTypeKind.class;
        }

        public String getEntityName()
        {
            return "relDocumentTypeKind";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
