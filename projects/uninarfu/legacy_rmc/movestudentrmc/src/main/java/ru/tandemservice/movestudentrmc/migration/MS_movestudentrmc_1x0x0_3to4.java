package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;

public class MS_movestudentrmc_1x0x0_3to4 extends IndependentMigrationScript {
    @Override
    public void run(DBTool tool) throws Exception {


        //<TODO> vch забыли таблицу 2012/07/06
        if (tool.tableExists("rlordrctgryrprsnttntyp_t")) {
            tool.executeUpdate("delete from rlordrctgryrprsnttntyp_t");
        }

        if (tool.tableExists("rprsnttnbsmntrltn_t")) {
            tool.executeUpdate("delete from rprsnttnbsmntrltn_t");
        }
        if (tool.tableExists("rprsnttnaddtnldcmntsrltn_t")) {
            tool.executeUpdate("delete from rprsnttnaddtnldcmntsrltn_t");
        }
        if (tool.tableExists("docrepresentstudentbase_t")) {
            tool.executeUpdate("delete from docrepresentstudentbase_t");
        }
        if (tool.tableExists("docrepresentbasics_t")) {
            tool.executeUpdate("delete from docrepresentbasics_t");
        }
        if (tool.tableExists("docordrepresent_t")) {
            tool.executeUpdate("delete from docordrepresent_t");
        }
        if (tool.tableExists("representation_t")) {
            tool.executeUpdate("delete from representation_t");
        }
        if (tool.tableExists("printtemplate_t")) {
            tool.executeUpdate("delete from printtemplate_t");
        }
        if (tool.tableExists("relrepresentationreasonossp_t")) {
            tool.executeUpdate("delete from relrepresentationreasonossp_t");
        }
        if (tool.tableExists("relrepresentationreasonbasic_t")) {
            tool.executeUpdate("delete from relrepresentationreasonbasic_t");
        }
        if (tool.tableExists("representationtype_t")) {
            tool.executeUpdate("delete from representationtype_t");
        }
    }
}