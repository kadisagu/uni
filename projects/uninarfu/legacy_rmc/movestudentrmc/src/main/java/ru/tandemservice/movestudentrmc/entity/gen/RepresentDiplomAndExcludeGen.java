package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudentrmc.entity.RepresentDiplomAndExclude;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * О выдаче диплома и отчислении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RepresentDiplomAndExcludeGen extends Representation
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.RepresentDiplomAndExclude";
    public static final String ENTITY_NAME = "representDiplomAndExclude";
    public static final int VERSION_HASH = -1788672029;
    private static IEntityMeta ENTITY_META;

    public static final String L_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String L_EDUCATION_LEVELS_HIGH_SCHOOL = "educationLevelsHighSchool";
    public static final String P_HAS_DIPLOMA_WITH_HONORS = "hasDiplomaWithHonors";

    private OrgUnit _formativeOrgUnit;     // Формирующее подразделение
    private EducationLevelsHighSchool _educationLevelsHighSchool;     // Направление подготовки (специальность)
    private boolean _hasDiplomaWithHonors;     // Закончил с отличием

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Формирующее подразделение.
     */
    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подразделение.
     */
    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * @return Направление подготовки (специальность).
     */
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    /**
     * @param educationLevelsHighSchool Направление подготовки (специальность).
     */
    public void setEducationLevelsHighSchool(EducationLevelsHighSchool educationLevelsHighSchool)
    {
        dirty(_educationLevelsHighSchool, educationLevelsHighSchool);
        _educationLevelsHighSchool = educationLevelsHighSchool;
    }

    /**
     * @return Закончил с отличием. Свойство не может быть null.
     */
    @NotNull
    public boolean isHasDiplomaWithHonors()
    {
        return _hasDiplomaWithHonors;
    }

    /**
     * @param hasDiplomaWithHonors Закончил с отличием. Свойство не может быть null.
     */
    public void setHasDiplomaWithHonors(boolean hasDiplomaWithHonors)
    {
        dirty(_hasDiplomaWithHonors, hasDiplomaWithHonors);
        _hasDiplomaWithHonors = hasDiplomaWithHonors;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof RepresentDiplomAndExcludeGen)
        {
            setFormativeOrgUnit(((RepresentDiplomAndExclude)another).getFormativeOrgUnit());
            setEducationLevelsHighSchool(((RepresentDiplomAndExclude)another).getEducationLevelsHighSchool());
            setHasDiplomaWithHonors(((RepresentDiplomAndExclude)another).isHasDiplomaWithHonors());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RepresentDiplomAndExcludeGen> extends Representation.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RepresentDiplomAndExclude.class;
        }

        public T newInstance()
        {
            return (T) new RepresentDiplomAndExclude();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "educationLevelsHighSchool":
                    return obj.getEducationLevelsHighSchool();
                case "hasDiplomaWithHonors":
                    return obj.isHasDiplomaWithHonors();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((OrgUnit) value);
                    return;
                case "educationLevelsHighSchool":
                    obj.setEducationLevelsHighSchool((EducationLevelsHighSchool) value);
                    return;
                case "hasDiplomaWithHonors":
                    obj.setHasDiplomaWithHonors((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "formativeOrgUnit":
                        return true;
                case "educationLevelsHighSchool":
                        return true;
                case "hasDiplomaWithHonors":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "formativeOrgUnit":
                    return true;
                case "educationLevelsHighSchool":
                    return true;
                case "hasDiplomaWithHonors":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "formativeOrgUnit":
                    return OrgUnit.class;
                case "educationLevelsHighSchool":
                    return EducationLevelsHighSchool.class;
                case "hasDiplomaWithHonors":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RepresentDiplomAndExclude> _dslPath = new Path<RepresentDiplomAndExclude>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RepresentDiplomAndExclude");
    }
            

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentDiplomAndExclude#getFormativeOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return Направление подготовки (специальность).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentDiplomAndExclude#getEducationLevelsHighSchool()
     */
    public static EducationLevelsHighSchool.Path<EducationLevelsHighSchool> educationLevelsHighSchool()
    {
        return _dslPath.educationLevelsHighSchool();
    }

    /**
     * @return Закончил с отличием. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentDiplomAndExclude#isHasDiplomaWithHonors()
     */
    public static PropertyPath<Boolean> hasDiplomaWithHonors()
    {
        return _dslPath.hasDiplomaWithHonors();
    }

    public static class Path<E extends RepresentDiplomAndExclude> extends Representation.Path<E>
    {
        private OrgUnit.Path<OrgUnit> _formativeOrgUnit;
        private EducationLevelsHighSchool.Path<EducationLevelsHighSchool> _educationLevelsHighSchool;
        private PropertyPath<Boolean> _hasDiplomaWithHonors;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentDiplomAndExclude#getFormativeOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new OrgUnit.Path<OrgUnit>(L_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return Направление подготовки (специальность).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentDiplomAndExclude#getEducationLevelsHighSchool()
     */
        public EducationLevelsHighSchool.Path<EducationLevelsHighSchool> educationLevelsHighSchool()
        {
            if(_educationLevelsHighSchool == null )
                _educationLevelsHighSchool = new EducationLevelsHighSchool.Path<EducationLevelsHighSchool>(L_EDUCATION_LEVELS_HIGH_SCHOOL, this);
            return _educationLevelsHighSchool;
        }

    /**
     * @return Закончил с отличием. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentDiplomAndExclude#isHasDiplomaWithHonors()
     */
        public PropertyPath<Boolean> hasDiplomaWithHonors()
        {
            if(_hasDiplomaWithHonors == null )
                _hasDiplomaWithHonors = new PropertyPath<Boolean>(RepresentDiplomAndExcludeGen.P_HAS_DIPLOMA_WITH_HONORS, this);
            return _hasDiplomaWithHonors;
        }

        public Class getEntityClass()
        {
            return RepresentDiplomAndExclude.class;
        }

        public String getEntityName()
        {
            return "representDiplomAndExclude";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
