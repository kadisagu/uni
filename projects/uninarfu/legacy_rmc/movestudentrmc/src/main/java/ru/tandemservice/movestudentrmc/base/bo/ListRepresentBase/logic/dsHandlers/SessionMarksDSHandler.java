package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers;

import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.core.entity.IdentifiableWrapper;

import java.util.Arrays;

public class SessionMarksDSHandler extends SimpleTitledComboDataSourceHandler {

    public SessionMarksDSHandler(String ownerId) {
        super(ownerId);
        filtered(true);
        addAll(Arrays.asList(new IdentifiableWrapper[]{
                new IdentifiableWrapper(MAKR_4, "сессия на 4"),
                new IdentifiableWrapper(MAKR_4_5, "сессия на 4 и 5"),
                new IdentifiableWrapper(MAKR_5, "сессия на 5")
        }));
    }

    public static final Long MAKR_4 = 0L;
    public static final Long MAKR_4_5 = 1L;
    public static final Long MAKR_5 = 2L;


}
