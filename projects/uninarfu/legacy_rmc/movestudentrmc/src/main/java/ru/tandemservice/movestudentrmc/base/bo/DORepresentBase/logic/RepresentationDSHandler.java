package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.uni.util.FilterUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RepresentationDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput> {

    public static final String REPRESENT_COLUMN = "type.title";
    public static final String CREATE_DATE_COLUMN = "createDate";
    public static final String STATE_COLUMN = "state";
    public static final String GROUP_COLUMN = "group";
    public static final String STUDENT_COLUMN = "student";
    public static final String CHECK = "check";

    public static final String TYPE_REPRESENT_FILTER = "typeRepresentFilter";
    public static final String DATE_FORMATIVE_FROM_FILTER = "dateFormativeFromFilter";
    public static final String DATE_FORMATIVE_TO_FILTER = "dateFormativeToFilter";
    public static final String STUDENT_ID_FILTER = "studentId";
    public static final String ORG_UNIT_ID_FILTER = "orgUnitId";

    public RepresentationDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {

        Long studentId = (Long) context.get(STUDENT_ID_FILTER);
        Long orgUnitId = (Long) context.get(ORG_UNIT_ID_FILTER);
        RepresentationType typeRepresentFilter = (RepresentationType) context.get(TYPE_REPRESENT_FILTER);
        Date dateFormativeFromFilter = (Date) context.get(DATE_FORMATIVE_FROM_FILTER);
        Date dateFormativeToFilter = (Date) context.get(DATE_FORMATIVE_TO_FILTER);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Representation.class, "type").addColumn("type");
        builder.joinEntity("type", DQLJoinType.left, DocRepresentStudentBase.class, "stud",
                           DQLExpressions.eq(DQLExpressions.property(Representation.id().fromAlias("type")),
                                             DQLExpressions.property(DocRepresentStudentBase.representation().fromAlias("stud"))));
        builder.addColumn("stud");


        if (studentId != null) {
            builder.where(DQLExpressions.eq(DQLExpressions.property(DocRepresentStudentBase.student().id().fromAlias("stud")), DQLExpressions.value(studentId)));
        }

        if (orgUnitId != null) {
            OrgUnit orgUnit = DataAccessServices.dao().get(OrgUnit.class, orgUnitId);

            IDQLExpression expression = FilterUtils.getEducationOrgUnitFilter(DocRepresentStudentBase.student().educationOrgUnit().fromAlias("stud").s(), orgUnit);
            builder.where(null == expression ? DQLExpressions.eq(DQLExpressions.property("stud.id"), DQLExpressions.value(-1L)) : expression);
        }

        builder.where(DQLExpressions.ne(
                DQLExpressions.property(Representation.state().code().fromAlias("type")),
                DQLExpressions.value("6")
        ));

        if (typeRepresentFilter != null)
            builder.where(DQLExpressions.eq(
                    DQLExpressions.property(Representation.type().fromAlias("type")),
                    DQLExpressions.value(typeRepresentFilter)
            ));

        //Период создания
        if (dateFormativeFromFilter != null && dateFormativeToFilter == null)
            builder.where(DQLExpressions.ge(
                    DQLExpressions.property(Representation.createDate().fromAlias("type")),
                    DQLExpressions.valueDate(dateFormativeFromFilter)
            ));

        if (dateFormativeFromFilter == null && dateFormativeToFilter != null)
            builder.where(DQLExpressions.le(
                    DQLExpressions.property(Representation.createDate().fromAlias("type")),
                    DQLExpressions.valueDate(dateFormativeToFilter)
            ));

        if (dateFormativeFromFilter != null && dateFormativeToFilter != null)
            builder.where(DQLExpressions.between(
                    DQLExpressions.property(Representation.createDate().fromAlias("type")),
                    DQLExpressions.valueDate(dateFormativeFromFilter),
                    DQLExpressions.valueDate(dateFormativeToFilter)
            ));

        if (input.getEntityOrder() != null)
            builder.order(DQLExpressions.property("type", input.getEntityOrder().getColumnName()), input.getEntityOrder().getDirection());

        List<Object[]> rowList = builder.createStatement(context.getSession()).list();
        List<DataWrapper> resultList = new ArrayList<DataWrapper>();

        for (Object[] row : rowList) {
            Representation representData = (Representation) row[0];
            DocRepresentStudentBase studentData = (DocRepresentStudentBase) row[1];

            DataWrapper w = new DataWrapper(representData.getId(), representData.getTitle(), representData);

            w.setProperty(Representation.ENTITY_NAME, representData);
            w.setProperty(DocRepresentStudentBase.ENTITY_NAME, studentData);

            resultList.add(w);
        }

        return ListOutputBuilder.get(input, resultList).pageable(true).build();
    }
}
