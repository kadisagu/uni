package ru.tandemservice.movestudentrmc.base.bo.ListOrder.logic;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.DOOrderManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ListRepresentBaseManager;
import ru.tandemservice.movestudentrmc.dao.CheckStateUtil;
import ru.tandemservice.movestudentrmc.docord.IListObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.MovestudentExtractStates;
import ru.tandemservice.movestudentrmc.entity.catalog.MovestudentOrderStates;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentExtractStatesCodes;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentOrderStatesCodes;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.*;

public class ListOrderManagerModifyDAO extends CommonDAO implements IListOrderManagerModifyDAO {

    @Override
    public void save(ListOrder order, List<DataWrapper> representSelectedList) {

        Principal2Visa principal2Visa = DOOrderManager.instance().visaDao().getPrincipal2Visa();
        if (principal2Visa != null && order.getCreateDate().after(principal2Visa.getEditDate())) {
            order.setSignFio(principal2Visa.getSignFio());
            order.setSignPost(principal2Visa.getSignPost());
            //-------------------------------------------------------------
            order.setSignPostGenitive(principal2Visa.getSignPostGenitive());
            //-------------------------------------------------------------

            order.setPostFio1(principal2Visa.getPostFio1());
            order.setPostTitle1(principal2Visa.getPostTitle1());

            order.setPostFio2(principal2Visa.getPostFio2());
            order.setPostTitle2(principal2Visa.getPostTitle2());

            order.setPostFio3(principal2Visa.getPostFio3());
            order.setPostTitle3(principal2Visa.getPostTitle3());

            order.setPostFio4(principal2Visa.getPostFio4());
            order.setPostTitle4(principal2Visa.getPostTitle4());

            order.setPostFio5(principal2Visa.getPostFio5());
            order.setPostTitle5(principal2Visa.getPostTitle5());

            order.setPostFio6(principal2Visa.getPostFio6());
            order.setPostTitle6(principal2Visa.getPostTitle6());
        }


        Person person = PersonManager.instance().dao().getPerson(ContextLocal.getUserContext().getPrincipalContext());
        if (person != null)
            order.setOperator(person);
        save(order);

        for (DataWrapper representSelected : representSelectedList) {

            ListOrdListRepresent orderRepresent = new ListOrdListRepresent();
            ListRepresent represent = (ListRepresent) representSelected.getWrapped();
            represent.setState(get(MovestudentExtractStates.class, MovestudentExtractStates.code().s(), MovestudentExtractStatesCodes.CODE_5));
            saveOrUpdate(represent);
            orderRepresent.setOrder(order);
            orderRepresent.setRepresentation((ListRepresent) representSelected.getWrapped());
            saveOrUpdate(orderRepresent);
        }

    }

    @Override
    public void update(ListOrder order, List<DataWrapper> representSelectedList) {

        CheckStateUtil.checkStateOrder(order, Arrays.asList(MovestudentOrderStatesCodes.CODE_1), getSession());

        Person person = PersonManager.instance().dao().getPerson(ContextLocal.getUserContext().getPrincipalContext());
        if (person != null)
            order.setOperator(person);
        saveOrUpdate(order);
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(ListOrdListRepresent.class, "r").column("r");
        builder.where(DQLExpressions.eq(DQLExpressions.property(ListOrdListRepresent.order().fromAlias("r")), DQLExpressions.value(order)));
        List<ListOrdListRepresent> representSelectedOldList = getRepresentList(order);

        for (ListOrdListRepresent representSelectedOld : representSelectedOldList) {
            Boolean isFind = false;
            for (DataWrapper representSelected : representSelectedList) {
                if (representSelectedOld.getRepresentation().equals(representSelected.getWrapped())) {
                    isFind = true;
                    representSelectedList.remove(representSelected);
                    break;
                }
            }
            if (!isFind) {
                ListRepresent represent = representSelectedOld.getRepresentation();
                represent.setState(get(MovestudentExtractStates.class, MovestudentExtractStates.code().s(), "3"));
                saveOrUpdate(represent);
                delete(representSelectedOld);
            }
        }

        for (DataWrapper representSelected : representSelectedList) {

            ListOrdListRepresent orderRepresent = new ListOrdListRepresent();
            ListRepresent represent = (ListRepresent) representSelected.getWrapped();
            represent.setState(get(MovestudentExtractStates.class, MovestudentExtractStates.code().s(), MovestudentExtractStatesCodes.CODE_5));
            saveOrUpdate(represent);
            orderRepresent.setOrder(order);
            orderRepresent.setRepresentation(represent);
            saveOrUpdate(orderRepresent);
        }

    }

    public List<DataWrapper> selectRepresent(ListOrder order) {

    	
/*        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(ListOrdListRepresent.class, "o").addColumn("o");
        builder.joinEntity("o", DQLJoinType.inner, ListRepresent.class,"r",
                DQLExpressions.eq(DQLExpressions.property(ListOrdListRepresent.representation().id().fromAlias("o")),
                        DQLExpressions.property(ListRepresent.id().fromAlias("r"))));
        builder.addColumn("r");
        builder.where(DQLExpressions.eq(DQLExpressions.property(ListOrdListRepresent.order().fromAlias("o")), DQLExpressions.value(order)));

        List<ListRepresent> representList = builder.createStatement(getSession()).list();
        
        List<DataWrapper> resultList = new ArrayList<DataWrapper>();
*/
        List<ListOrdListRepresent> listOrdListRepresents = DataAccessServices.dao().getList(ListOrdListRepresent.class, ListOrdListRepresent.order(), order);

        List<DataWrapper> resultList = new ArrayList<DataWrapper>();

        for (ListOrdListRepresent represent : listOrdListRepresents) {
            //ListOrdListRepresent representData = (ListOrdListRepresent) represent[0];
            //RelListRepresentStudents studentData = (RelListRepresentStudents) represent[1];

            int count = DataAccessServices.dao().getCount(RelListRepresentStudents.class, RelListRepresentStudents.representation().s(), represent.getRepresentation());

            DataWrapper w = new DataWrapper(represent.getRepresentation().getId(), represent.getRepresentation().getTitle(), represent.getRepresentation());
            w.setProperty("count", count);
            w.setProperty("represent", represent.getRepresentation());
            resultList.add(w);
        }

        return resultList;
    }

    public void updateState(ListOrder order) {

        getSession().saveOrUpdate(order);
    }

    public void doCommit(ListOrder order, ErrorCollector error) {

        List<ListOrdListRepresent> representList = getList(ListOrdListRepresent.class, ListOrdListRepresent.order(), order);

        Boolean isComplete = true;

        
/*        final RtfDocument docMain = BuildPrintDocV2.creationPrintDocOrder(order, error, true);
        if (docMain != null)
            order.setDocument(RtfUtil.toByteArray(docMain));
        else
            isComplete = false;

        if (!isComplete) {
            error.add("Ошибка в формировании печатной формы приказа!");
        }*/

        for (ListOrdListRepresent represent : representList) {
/*            ListRepresent reprData = (ListRepresent) represent[1];
            RelListRepresentStudents studentData = (RelListRepresentStudents) represent[2];    */
            ListRepresent reprData = represent.getRepresentation();
            if (!isComplete)
                break;

            if (reprData.isCommitted())
                return;


            String type = reprData.getRepresentationType().getCode();
            IListObjectByRepresentTypeManager representManager = ListRepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(type);

            boolean doCommit = representManager.getListObjectModifyDAO().doCommit(reprData, error);

            if (doCommit) {
/*            	if (reprData.getRepresentationType().getCode().equals(RepresentationTypeCodes.APPOINT_GRANT)) {
                    studentData.getStudent().setStatus(DataAccessServices.dao().get(StudentStatus.class, StudentStatus.code().s(), "8"));
            	}*/
                reprData.setState(get(MovestudentExtractStates.class, MovestudentExtractStates.code().s(), MovestudentExtractStatesCodes.CODE_6));
                reprData.setCommitted(true);

                saveOrUpdate(reprData);
            }
            else {
                error.add("Ошибка в проведении представлений приказа!");
                isComplete = false;
                //Откатываем весь приказ
                //representManager.getListObjectModifyDAO().doRollback(reprData, error);
                break;
            }
        }

        if (isComplete) {

            order.setState(get(MovestudentOrderStates.class, MovestudentOrderStates.code().s(), "5"));
            getSession().saveOrUpdate(order);
        }

    }

    public void doRollback(ListOrder order, ErrorCollector error) {
        List<ListOrdListRepresent> representList = getRepresentList(order);

        Boolean isComplete = true;
        for (ListOrdListRepresent represent : representList) {

 /*       	if (!represent.getRepresentation().isCommitted())
                return;*/

            String type = represent.getRepresentation().getRepresentationType().getCode();
            IListObjectByRepresentTypeManager representManager = ListRepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(type);

            if (representManager.getListObjectModifyDAO().doRollback(represent.getRepresentation(), error)) {

                represent.getRepresentation().setCommitted(false);
                represent.getRepresentation().setState(get(MovestudentExtractStates.class, MovestudentExtractStates.code().s(), MovestudentExtractStatesCodes.CODE_5));

                saveOrUpdate(represent.getRepresentation());
            }
            else {
                isComplete = false;
                break;
            }
        }

        if (isComplete) {
            order.setState(get(MovestudentOrderStates.class, MovestudentOrderStates.code().s(), "3"));
            order.setDocument(null);
            saveOrUpdate(order);
        }

    }

    public void saveDocOrder(ListOrder order, RtfDocument docMain) {

        order.setDocument(RtfUtil.toByteArray(docMain));
        getSession().saveOrUpdate(order);
    }

    private List<ListOrdListRepresent> getRepresentList(ListOrder order) {

        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(ListOrdListRepresent.class, "r").column("r");
        builder.where(DQLExpressions.eq(DQLExpressions.property(ListOrdListRepresent.order().fromAlias("r")), DQLExpressions.value(order)));
        return builder.createStatement(getSession()).list();
    }

    public void deleteExtractsText(ListOrder order) {
        new DQLDeleteBuilder(ExtractTextRelation.class)
                .where(DQLExpressions.eqValue(DQLExpressions.property(ExtractTextRelation.order().order().id()), order.getId()))
                .createStatement(getSession()).execute();
    }

    public void saveExtractsText(ListOrder order) {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(ListOrdListRepresent.class, "rel")
                .joinEntity("rel", DQLJoinType.inner, RelListRepresentStudents.class, "stud",
                            DQLExpressions.eq(
                                    DQLExpressions.property(ListOrdListRepresent.representation().id().fromAlias("rel")),
                                    DQLExpressions.property(RelListRepresentStudents.representation().id().fromAlias("stud"))
                            )
                )
                .where(DQLExpressions.eqValue(DQLExpressions.property(ListOrdListRepresent.order().id().fromAlias("rel")), order.getId()))
                .column("rel")
                .order(DQLExpressions.property(ListOrdListRepresent.order().number().fromAlias("rel")))
                .joinPath(DQLJoinType.inner, ListOrdListRepresent.order().fromAlias("rel"), "ord")
                .joinPath(DQLJoinType.inner, ListOrdListRepresent.representation().fromAlias("rel"), "repres")
                .column(DQLExpressions.property(RelListRepresentStudents.student().fromAlias("stud")));

        List<Object[]> lst = getList(builder);

        Map<Long, RtfDocument> templMap = new HashMap<Long, RtfDocument>();

        for (Object[] obj : lst) {
            ListOrdListRepresent rel = (ListOrdListRepresent) obj[0];
            Student student = (Student) obj[1];

            ListRepresent listRepresent = rel.getRepresentation();

            IListObjectByRepresentTypeManager manager = ListRepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(listRepresent.getRepresentationType().getCode());

            Map<Student, OrderParagraphInfo> infoParagMap = manager.getListObjectModifyDAO().buildBodyRepresent(rel.getOrder().getRepresentationList(), RtfBean.getElementFactory().createRtfDocument());

            //Шаблон выписки
            RtfDocument templateExtract = null;
            if (templMap.get(listRepresent.getRepresentationType().getId()) == null) {
                RtfDocument template = manager.getListObjectModifyDAO().getTemplateExtract(listRepresent.getRepresentationType());
                templMap.put(listRepresent.getRepresentationType().getId(), template);
                templateExtract = template.getClone();
            }
            else
                templateExtract = templMap.get(listRepresent.getRepresentationType().getId()).getClone();

            manager.getListObjectModifyDAO().buildBodyExtract(rel, student, templateExtract, infoParagMap);

            ExtractTextRelation relation = new ExtractTextRelation();
            relation.setOrder(rel);
            relation.setStudent(student);
            relation.setText(RtfUtil.toByteArray(templateExtract));
            getSession().save(relation);
        }
    }

}
