package ru.tandemservice.movestudentrmc.component.orgUnit.GrantAddEdit;

import ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab.GrantWrapper;
import ru.tandemservice.movestudentrmc.entity.GrantEntity;
import ru.tandemservice.uni.dao.UniDao;

public class DAO extends UniDao<Model> implements IDAO {

    public void prepare(Model model) {
        double sum = model.getGrantEntity().getSum();
        model.setSumR(GrantWrapper.getRubles(sum));
        model.setSumK(GrantWrapper.getKopecs(sum));
    }

    public void update(Model model) {
        GrantEntity grant = model.getGrantEntity();
        double sum = model.getSumR();
        sum += model.getSumK() / 100.0;
        grant.setSum(sum);

        saveOrUpdate(grant);
    }

}
