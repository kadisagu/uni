package ru.tandemservice.movestudentrmc.base.bo.DocumentSettings.logic;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dao.CommonDAO;
import ru.tandemservice.movestudentrmc.entity.RelDocumentTypeKind;
import ru.tandemservice.movestudentrmc.entity.catalog.Document;
import ru.tandemservice.movestudentrmc.entity.catalog.DocumentKind;
import ru.tandemservice.movestudentrmc.entity.catalog.DocumentType;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DocumentSettingsModifyDAO extends CommonDAO implements IDocumentSettingsModifyDAO {

    @Override
    public List<DocumentKind> getKinds(DocumentType documentType) {
        MQBuilder builder = new MQBuilder(RelDocumentTypeKind.ENTITY_CLASS, "r")
                .add(MQExpression.eq("r", RelDocumentTypeKind.type(), documentType));

        builder.getSelectAliasList().clear();
        builder.addSelect(RelDocumentTypeKind.kind().s());
        builder.setNeedDistinct(true);

        return builder.getResultList(getSession());
    }

    @Override
    public List<Document> getDocuments(DocumentType documentType, DocumentKind documentKind) {
        MQBuilder builder = new MQBuilder(RelDocumentTypeKind.ENTITY_CLASS, "r");

        if (documentType != null)
            builder.add(MQExpression.eq("r", RelDocumentTypeKind.type(), documentType));
        if (documentKind != null)
            builder.add(MQExpression.eq("r", RelDocumentTypeKind.kind(), documentKind));

        builder.getSelectAliasList().clear();
        builder.addSelect(RelDocumentTypeKind.document().s());
        builder.setNeedDistinct(true);
        return builder.getResultList(getSession());
    }

    @Override
    public void saveRelations(DocumentType documentType, DocumentKind documentKind, List<Document> documents) {
        if (documents == null)
            documents = Collections.emptyList();

        MQBuilder builder = new MQBuilder(RelDocumentTypeKind.ENTITY_CLASS, "r")
                .add(MQExpression.eq("r", RelDocumentTypeKind.type(), documentType))
                .add(MQExpression.eq("r", RelDocumentTypeKind.kind(), documentKind));
        List<RelDocumentTypeKind> oldRels = builder.getResultList(getSession());

        Map<Document, RelDocumentTypeKind> map = new HashMap<Document, RelDocumentTypeKind>();
        for (RelDocumentTypeKind rel : oldRels)
            map.put(rel.getDocument(), rel);

        //удаляем
        for (Document doc : map.keySet())
            if (!documents.contains(doc))
                this.delete(map.get(doc));

        //вставляем новые
        for (Document doc : documents)
            if (!map.containsKey(doc)) {
                RelDocumentTypeKind rel = new RelDocumentTypeKind();
                rel.setType(documentType);
                rel.setKind(documentKind);
                rel.setDocument(doc);

                this.saveOrUpdate(rel);
            }
    }


}
