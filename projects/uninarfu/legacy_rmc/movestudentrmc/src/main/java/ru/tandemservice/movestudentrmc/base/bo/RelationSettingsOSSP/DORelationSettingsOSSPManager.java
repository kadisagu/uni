package ru.tandemservice.movestudentrmc.base.bo.RelationSettingsOSSP;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.movestudentrmc.base.bo.RelationSettingsOSSP.logic.IRelationSettingsOSSPManagerModifyDAO;
import ru.tandemservice.movestudentrmc.base.bo.RelationSettingsOSSP.logic.RelationSettingsOSSPManagerModifyDAO;

@Configuration
public class DORelationSettingsOSSPManager extends BusinessObjectManager {

    public static DORelationSettingsOSSPManager instance()
    {
        return instance(DORelationSettingsOSSPManager.class);
    }

    @Bean
    public IRelationSettingsOSSPManagerModifyDAO modifyDao()
    {
        return new RelationSettingsOSSPManagerModifyDAO();
    }
}
