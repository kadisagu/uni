package ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrant;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrant.logic.ListRepresentGrantManagerModifyDAO;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrant.ui.Edit.ListRepresentGrantEdit;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrant.ui.View.ListRepresentGrantView;
import ru.tandemservice.movestudentrmc.docord.IListObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IListObjectModifyDAO;


@Configuration
public class ListRepresentGrantManager extends BusinessObjectManager implements IListObjectByRepresentTypeManager {

    public static ListRepresentGrantManager instance()
    {
        return instance(ListRepresentGrantManager.class);
    }


    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return ListRepresentGrantEdit.class;
    }


    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return ListRepresentGrantView.class;
    }


    @Bean
    @Override
    public IListObjectModifyDAO getListObjectModifyDAO() {
        return new ListRepresentGrantManagerModifyDAO();
    }
}
