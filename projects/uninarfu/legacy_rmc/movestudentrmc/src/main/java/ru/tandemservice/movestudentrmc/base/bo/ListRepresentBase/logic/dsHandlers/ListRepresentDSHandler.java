package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import ru.tandemservice.movestudentrmc.entity.catalog.MovestudentExtractStates;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.codes.EduLevelCodes;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class ListRepresentDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String HIGH = "high";
    public static final String MIDDLE = "middle";
    public static final String POSTGRADUATE = "postgraduate";
    public static final String MIDDLE_BRANCH = "middleBranch";
    public static final String HIGH_BRANCH = "highBranch";
    public static final String STATE_COLUMN = "state";
    public static final String DATE_BEGINING_PAYMENT = "createDate";
    public static final String REPRESENT_TITLE_COLUMN = "listRepresentTitle";
    public static final String ORDER_TITLE_COLUMN = "orderTitle";
    public static final String COUNT_STUDENT_COLUMN = "countStudent";
    public static final String CHECK = "check";

    public ListRepresentDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        DQLSelectBuilder builder = getListrepresentBuilder(context);

        DQLExecutionContext qdlContext = new DQLExecutionContext(context.getSession());
        Number rawCount = builder.createCountStatement(qdlContext).uniqueResult();

        int count = rawCount == null ? 0 : rawCount.intValue();
        int startCount = input.getStartRecord();
        int pageSize = input.getCountRecord();
        if (startCount < 0 || startCount > count)
        {
            long lastPageCount = count % pageSize;
            startCount = new Long(count - lastPageCount).intValue();
        }
        if ((startCount + pageSize) > count)
            pageSize = count - startCount;

        IDQLStatement stmt = builder.createStatement(qdlContext);
        stmt.setFirstResult(startCount);
        stmt.setMaxResults(pageSize);
        List<Long> represents = stmt.<Object[]>list().stream().map(row -> (Long) row[0]).collect(Collectors.toList());

        DQLSelectBuilder resultBuilder = new DQLSelectBuilder()
                .fromEntity(ListRepresent.class, "rep")
                .where(in(property("rep", ListRepresent.id()), represents));

        DSOutput output = DQLSelectOutputBuilder.get(input, resultBuilder, getSession()).build();
        output.setTotalSize(count);

        return output;
    }

    protected DQLSelectBuilder getListrepresentBuilder(ExecutionContext context)
    {
        Date dateFormativeFromFilter = context.get("dateFormativeFromFilter");
        Date dateFormativeToFilter = getLastTime(context.get("dateFormativeToFilter"));
        Date dateStartFromFilter = context.get("dateStartFromFilter");
        Date dateStartToFilter = getLastTime(context.get("dateStartToFilter"));
        List<RepresentationType> types = context.get("typeRepresentFilter");
        List<MovestudentExtractStates> states = context.get("stateViewFilter");
        List<OrgUnit> formativeOrgUnits = context.get("formativeOrgUnitFilter");
        List<Course> courses = context.get("courseFilter");
        List<Student> students = context.get("studentFilter");
        String operator = context.get("authorFilter");
        List<GrantView> grantViews = context.get("grantViewField");
        Boolean high = context.get(HIGH);
        Boolean middle = context.get(MIDDLE);
        Boolean postgraduate = context.get(POSTGRADUATE);
        Boolean highBranch = context.get(HIGH_BRANCH);
        Boolean middleBranch = context.get(MIDDLE_BRANCH);

        // мапа <orgunit, orgunit.parent>
        Map<OrgUnit, OrgUnit> orgUnitRelationMap = getOrgUnitRelationMap();
        Set<OrgUnit> branches = getBranchesWithParents(orgUnitRelationMap);

        String alias = "rel";
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(RelListRepresentStudents.class, alias)
                .column(property(alias, RelListRepresentStudents.representation().id()))
                .column(property(alias, RelListRepresentStudents.representation().state().code()))
                .distinct();

        FilterUtils.applyBetweenFilter(builder, alias, RelListRepresentStudents.representation().createDate().s(), dateFormativeFromFilter, dateFormativeToFilter);
        FilterUtils.applyBetweenFilter(builder, alias, RelListRepresentStudents.representation().startDate().s(), dateStartFromFilter, dateStartToFilter);
        FilterUtils.applySelectFilter(builder, alias, RelListRepresentStudents.representation().representationType(), types);
        FilterUtils.applySelectFilter(builder, alias, RelListRepresentStudents.representation().state(), states);


        builder.joinPath(DQLJoinType.inner, RelListRepresentStudents.student().fromAlias(alias), "stu")
                .joinPath(DQLJoinType.inner, Student.educationOrgUnit().fromAlias("stu"), "eou")
                .joinPath(DQLJoinType.inner, EducationOrgUnit.formativeOrgUnit().fromAlias("eou"), "fou")
                .joinPath(DQLJoinType.inner, EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSubject().subjectIndex().programKind().eduLevel().fromAlias("eou"), "lvl");

        FilterUtils.applySelectFilter(builder, "fou", "", formativeOrgUnits);
        FilterUtils.applySelectFilter(builder, "stu", Student.course(), courses);
        FilterUtils.applySelectFilter(builder, "stu", "", students);

        if (postgraduate)
            builder.where(eq(property("lvl", EduLevel.code()), value(EduLevelCodes.VYSSHEE_OBRAZOVANIE_PODGOTOVKA_KADROV_VYSSHEY_KVALIFIKATSII)));

        IDQLExpression inBranch = in(property("fou"), branches);
        IDQLExpression inHigh = or(eq(property("lvl", EduLevel.code()), value(EduLevelCodes.VYSSHEE_OBRAZOVANIE_BAKALAVRIAT)),
                                   eq(property("lvl", EduLevel.code()), value(EduLevelCodes.VYSSHEE_OBRAZOVANIE_SPETSIALITET_MAGISTRATURA)));
        IDQLExpression inMiddle = eq(property("lvl", EduLevel.code()), value(EduLevelCodes.SREDNEE_PROFESSIONALNOE_OBRAZOVANIE));

        if (middle || middleBranch) builder.where(inMiddle);
        if (high || highBranch) builder.where(inHigh);
        if (middle || high) builder.where(not(inBranch));
        if (middleBranch || highBranch) builder.where(inBranch);

        FilterUtils.applySimpleLikeFilter(builder, alias, RelListRepresentStudents.representation().operator().identityCard().fullFio().s(), operator);

        if (grantViews != null && !grantViews.isEmpty())
            builder.where(in(property(alias, RelListRepresentStudents.representation().id()), getGrantViewRepresents(grantViews)));

        builder.order(property(RelListRepresentStudents.representation().state().code().fromAlias(alias)));

        return builder;
    }

    /**
     * Выгрузка данных по формирующим подразделениям
     */
    public Map<OrgUnit, OrgUnit> getOrgUnitRelationMap()
    {
        Map<OrgUnit, OrgUnit> orgUnitRelationMap = new HashMap<>();

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(OrgUnit.class, "ou")
                .where(in(
                        property(OrgUnit.id().fromAlias("ou")),
                        new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "eou").column(EducationOrgUnit.formativeOrgUnit().id().fromAlias("eou").s()).predicate(DQLPredicateType.distinct).buildQuery())
                );

        List<OrgUnit> orgUnits = IUniBaseDao.instance.get().getList(builder);

        for (OrgUnit orgUnit : orgUnits)
        {
            orgUnitRelationMap.put(orgUnit, orgUnit.getParent());
        }

        return orgUnitRelationMap;
    }

    /**
     * Список подразделений, которые являются филиалами, если родительское
     * подразделение - филиал, то дочернее тоже относим к разряду филиалов
     */
    public Set<OrgUnit> getBranchesWithParents(Map<OrgUnit, OrgUnit> orgUnitRelationMap)
    {
        Set<OrgUnit> branchesWithParents = new HashSet<>();

        for (Map.Entry<OrgUnit, OrgUnit> entry : orgUnitRelationMap.entrySet())
        {
            OrgUnit currentNode = entry.getKey();
            Set<OrgUnit> possibleBranchesNodes = new HashSet<>(); // возможные филиалы
            if (currentNode.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.BRANCH))
            {
                branchesWithParents.add(currentNode);
            }
            else
            {
                possibleBranchesNodes.add(currentNode);
            }

            // для определения относится ли формирующее к филиалам проверим, есть ли среди родительских подразделений филиалы
            while (currentNode.getParent() != null)
            {
                currentNode = currentNode.getParent();
                if (currentNode.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.BRANCH))
                {
                    possibleBranchesNodes.add(currentNode);
                    branchesWithParents.addAll(possibleBranchesNodes);
                    possibleBranchesNodes = new HashSet<>();
                }
                else
                {
                    possibleBranchesNodes.add(currentNode);
                }
            }

            // проверим корень
            if (currentNode.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.BRANCH))
            {
                branchesWithParents.addAll(possibleBranchesNodes);
            }
        }

        return branchesWithParents;
    }

    public List<Long> getGrantViewRepresents(List<GrantView> list)
    {

        IUniBaseDao dao = IUniBaseDao.instance.get();

        DQLSelectBuilder grantBuilder = new DQLSelectBuilder()
                .fromEntity(ListRepresentGrant.class, "lg")
                .column(property(ListRepresentGrant.id().fromAlias("lg")))
                .where(in(property(ListRepresentGrant.grantView().fromAlias("lg")), list));

        DQLSelectBuilder supportBuilder = new DQLSelectBuilder()
                .fromEntity(ListRepresentSupport.class, "ls")
                .column(property(ListRepresentSupport.id().fromAlias("ls")))
                .where(in(property(ListRepresentSupport.grantView().fromAlias("ls")), list));

        grantBuilder.union(supportBuilder.buildSelectRule());

        DQLSelectBuilder grantCancelBuilder = new DQLSelectBuilder()
                .fromEntity(ListRepresentGrantCancel.class, "lgc")
                .column(property(ListRepresentGrantCancel.id().fromAlias("lgc")))
                .where(in(property(ListRepresentGrantCancel.grantView().fromAlias("lgc")), list));

        DQLSelectBuilder grantResumeBuilder = new DQLSelectBuilder()
                .fromEntity(ListRepresentGrantResume.class, "lgr")
                .column(property(ListRepresentGrantResume.id().fromAlias("lgr")))
                .where(in(property(ListRepresentGrantResume.grantView().fromAlias("lgr")), list));

        grantCancelBuilder.union(grantResumeBuilder.buildSelectRule());

        DQLSelectBuilder grantSuspendBuilder = new DQLSelectBuilder()
                .fromEntity(ListRepresentGrantSuspend.class, "lgs")
                .column(property(ListRepresentGrantSuspend.id().fromAlias("lgs")))
                .where(in(property(ListRepresentGrantSuspend.grantView().fromAlias("lgs")), list));

        List<Long> result = new ArrayList<>();
        result.addAll(dao.getList(grantBuilder));
        result.addAll(dao.getList(grantCancelBuilder));
        result.addAll(dao.getList(grantSuspendBuilder));

        return result;
    }

    public Date getLastTime(Date date)
    {
        if (date != null)
        {
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            c.add(Calendar.DAY_OF_YEAR, 1);
            c.add(Calendar.MILLISECOND, -1);
            date = c.getTime();
        }
        return date;
    }
}
