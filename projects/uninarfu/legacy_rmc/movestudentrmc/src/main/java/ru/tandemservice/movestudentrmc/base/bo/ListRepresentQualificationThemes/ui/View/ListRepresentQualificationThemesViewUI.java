package ru.tandemservice.movestudentrmc.base.bo.ListRepresentQualificationThemes.ui.View;


import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.support.AbstractListRepresentViewUI;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentQualificationThemes.logic.ListPrintDoc;
import ru.tandemservice.movestudentrmc.entity.ListRepresentQualificationThemes;

import java.util.Collections;

public class ListRepresentQualificationThemesViewUI extends AbstractListRepresentViewUI<ListRepresentQualificationThemes>
{

    @Override
    public ListRepresentQualificationThemes getListRepresentObject() {
        return new ListRepresentQualificationThemes();
    }

    @Override
    protected RtfDocument printRepresent() {
        return new ListPrintDoc().creationPrintDocOrder(Collections.singletonList(getListRepresent()));
    }

}
