package ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab;

import org.tandemframework.core.entity.EntityBase;
import ru.tandemservice.movestudentrmc.entity.GrantEntity;

public class GrantWrapper extends EntityBase {

    private GrantEntity grant;

    public GrantWrapper(GrantEntity grant) {
        this.grant = grant;
    }

    public GrantEntity getGrant() {
        return grant;
    }

    public void setGrant(GrantEntity grant) {
        this.grant = grant;
    }

    public String getRuble() {
        if (this.grant.getId() != null) {
            return "" + getRubles(this.grant.getSum());
        }
        else
            return "-";
    }

    public String getKopec() {
        if (this.grant.getId() != null) {
            return "" + getKopecs(this.grant.getSum());
        }
        else
            return "-";
    }

    public static int getRubles(double sum) {
        Double d = Double.valueOf(sum);
        return d.intValue();
    }

    public static int getKopecs(double sum) {
        double d = sum - getRubles(sum);
        return (int) Math.round(d * 100);
    }
}
