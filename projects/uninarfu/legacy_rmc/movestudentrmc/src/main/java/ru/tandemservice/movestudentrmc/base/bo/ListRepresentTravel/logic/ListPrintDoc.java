package ru.tandemservice.movestudentrmc.base.bo.ListRepresentTravel.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.movestudentrmc.docord.IListPrintDoc;
import ru.tandemservice.movestudentrmc.docord.util.MoveStudentUtil;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListRepresentTravel;
import ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents;
import ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.*;

public class ListPrintDoc implements IListPrintDoc
{

    private Map<Student, OrderParagraphInfo> hashMap = new HashMap<>();

    public static final int RUSSIA_CODE = 0;
    public final String TEMPLATE_HEADER = "list.rep.travel.header";
    public final String TEMPLATE_PARAG = "list.rep.travel.parag";


    public RtfDocument creationPrintDocOrder(List<? extends ListRepresent> listOfRepresents)
    {
        PrintTemplate templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(PrintTemplate.class, TEMPLATE_HEADER);
        RtfDocument templateHeader = new RtfReader().read(templateDocument.getContent());

        templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(PrintTemplate.class, TEMPLATE_PARAG);
        RtfDocument templateParag = new RtfReader().read(templateDocument.getContent());

        Map<OrgUnit, Map<String, List<Object[]>>> dataMap = getStudentData(listOfRepresents);
        //сортировка формирующих по алфавиту
        List<OrgUnit> orgUnitList = new ArrayList<>(dataMap.keySet());
        Collections.sort(orgUnitList, new EntityComparator<>(new EntityOrder(OrgUnit.title())));

        RtfDocument result = templateHeader.getClone();
        result.setSettings(templateParag.getSettings());
        IRtfElement paragTemp = UniRtfUtil.findElement(result.getElementList(), "parag");
        new RtfInjectModifier().put("representTitle", listOfRepresents.get(0).getRepresentationType().getTitle()).modify(result);

        RtfDocument res = RtfBean.getElementFactory().createRtfDocument();
        res.setHeader(templateParag.getHeader());
        res.setSettings(templateParag.getSettings());
        //------------------------------------------------------------------------------------
        //res.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.TAB));
        //------------------------------------------------------------------------------------

        int formCounter = 0;
        //Сортировка студентов внутри параграфа

        for (OrgUnit formOU : orgUnitList)
        {
            Map<String, List<Object[]>> formMap = dataMap.get(formOU);

            formCounter++;
            res.getElementList().add(RtfBean.getElementFactory().createRtfText(String.valueOf(formCounter).trim() + "."));
            res.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.TAB));
            insertRow(res, "По " + formOU.getDativeCaseTitle());
            List<String> parNumbers = new ArrayList<>();
            int counter = 0;
            for (List<Object[]> studentList : formMap.values())
            {
                counter++;
                parNumbers.add("п. " + formCounter + "." + counter + ".");
                RtfDocument par = getParagraph(templateParag, formCounter, counter, studentList);
                res.getElementList().addAll(par.getElementList());
            }
            counter++;
            //------------------------------------------------------------------------------------
            //res.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.TAB));
            //------------------------------------------------------------------------------------
            res.getElementList().add(RtfBean.getElementFactory().createRtfText(String.valueOf(formCounter).trim() + "."));
            res.getElementList().add(RtfBean.getElementFactory().createRtfText(String.valueOf(counter).trim() + "."));

            RtfString str = new RtfString();
            str.append(IRtfData.TAB)
                    .append("Контроль за исполнением ")
                    .append(StringUtils.join(parNumbers, ", "))
                    .append(" приказа возложить на директора ")
                    .append(formOU.getGenitiveCaseTitle());
            //----------------------------------------------------------------------------------------------------------------------------
            /*
            if (EmployeeManager.instance().dao().getHead(formOU) != null)
            {
                str.append(" ")
                .append(MoveStudentUtil.getDeclinationIOF(EmployeeManager.instance().dao().getHead(formOU).getPerson().getIdentityCard(), GrammaCase.ACCUSATIVE));
                res.getElementList().addAll(MoveStudentUtil.getDeclinationIO_F(EmployeeManager.instance().dao().getHead(formOU).getPerson().getIdentityCard(), GrammaCase.ACCUSATIVE).toList());
                
            }
            str.append(".");
            res.getElementList().addAll(str.toList());
            */
            
            if (EmployeeManager.instance().dao().getHead(formOU) != null)
            {
                str.append(" ");
                res.getElementList().addAll(str.toList());
                res.getElementList().addAll(MoveStudentUtil.getDeclinationIO_F(EmployeeManager.instance().dao().getHead(formOU).getPerson().getIdentityCard(), GrammaCase.ACCUSATIVE).toList());
                res.getElementList().add(RtfBean.getElementFactory().createRtfText("."));
            }
            //----------------------------------------------------------------------------------------------------------------------------
            res.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));

            res.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
        }

        //основания
//        res.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
        insertRow(res, "Основание: " + UtilPrintSupport.getBasementTitles(listOfRepresents));
        result.getElementList().addAll(result.getElementList().indexOf(paragTemp), res.getElementList());

        //---------------------------------------------------------------------------------------
        //result.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
        //---------------------------------------------------------------------------------------

        SharedRtfUtil.removeParagraphsWithTagsRecursive(result, Collections.singletonList("parag"), false, false);
        return result;
    }

    private RtfDocument getParagraph(RtfDocument template, int formCounter, int counter, List<Object[]> studentList)
    {
        RtfDocument result = template.getClone();
        ListRepresentTravel listRepresent = (ListRepresentTravel) studentList.get(0)[1];
        Student student = (Student) studentList.get(0)[0];
        for (Object[] o : studentList)
        {
            Student s = (Student) o[0];
            hashMap.put(s, new OrderParagraphInfo(formCounter, counter));
        }
        RtfInjectModifier im = new RtfInjectModifier();

        im.put("i", String.valueOf(formCounter).trim());
        im.put("j", String.valueOf(counter).trim());
        injectModifier(im, listRepresent, student);
        im.modify(result);

        final Map<Integer, RtfString> map = new HashMap<>();

        List<String[]> tableList = new ArrayList<>();
        int rowCounter = 1;
        for (Object[] arr : studentList)
        {
            Student stu = (Student) arr[0];
            RtfString studentData = getStudentInfo(stu);
            String[] row = new String[2];
            IdentityCard card = stu.getPerson().getIdentityCard();
            //-------------------------------------------------------
            hashMap.get(stu).setStudentNumber(rowCounter);
            //-------------------------------------------------------
            StringBuilder sb = new StringBuilder()
                    .append(rowCounter).append(") ")
                    .append(PersonManager.instance().declinationDao().getDeclinationLastName(card.getLastName(), GrammaCase.ACCUSATIVE, stu.getPerson().isMale()))
                    .append("\\n")
                    .append(PersonManager.instance().declinationDao().getDeclinationFirstName(card.getFirstName(), GrammaCase.ACCUSATIVE, stu.getPerson().isMale()));
            if (stu.getPerson().getIdentityCard().getMiddleName() != null)
                sb
                        .append(" ")
                        .append(PersonManager.instance().declinationDao().getDeclinationMiddleName(card.getMiddleName(), GrammaCase.ACCUSATIVE, stu.getPerson().isMale()));
            row[0] = sb.toString();
            //------------------------------------------------------------------------------------------------
            studentData.append(studentList.indexOf(arr) == studentList.size()-1 ? "." : ";");
            //------------------------------------------------------------------------------------------------
            row[1] = studentData.toString();
            tableList.add(row);
            map.put(rowCounter - 1, studentData);
            rowCounter++;
        }

        RtfTableModifier tm = new RtfTableModifier();
        tm.put("T", tableList.toArray(new String[][]{}));
        tm.put("T", new RtfRowIntercepterBase()
        {
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                if (colIndex == 0)
                {
                    int separatorIndex = value.lastIndexOf("\\n");

                    return new RtfString().append(value.substring(0, separatorIndex)).append(IRtfData.PAR).append(value.substring(separatorIndex + 2)).toList();
                }
                if (colIndex == 1)
                {
                    return map.get(rowIndex).toList();
                } else
                    return null;
            }
        });
        tm.modify(result);
        return result;
    }

    public static void injectModifier(RtfInjectModifier im, ListRepresentTravel listRepresent, Student student)
    {
        im.put("settlement", listRepresent.getAddressItem() == null ? "" : listRepresent.getAddressItem().getFullTitle());
        //---------------------------------------------------------------------------------------------------------------------------------------
        //im.put("country", listRepresent.getAddressCountry().getCode() == RUSSIA_CODE ? "" : ", " + listRepresent.getAddressCountry().getTitle());
        im.put("country", listRepresent.getAddressCountry().getCode() == RUSSIA_CODE ? "" : ", " + listRepresent.getAddressCountry().getFullTitle());
        //---------------------------------------------------------------------------------------------------------------------------------------
        im.put("organization", "(" + listRepresent.getOrganization() + ")");
        im.put("target", listRepresent.getTarget());

        StringBuilder days = new StringBuilder();

        if (listRepresent.getDays() != null)
        {
            days.append(" сроком на ")
                    .append(String.valueOf(listRepresent.getDays()))
                    .append(" ")
                    .append(UtilPrintSupport.getDaysStr(listRepresent.getDays().intValue()));
        }

        im.put("daysStr", days.toString());
        String stip = student.getCompensationType().isBudget() ? " с сохранением стипендии" : "";
        im.put("paymentData", listRepresent.getTravelPaymentData() == null ? "" : listRepresent.getTravelPaymentData().getTitle() + stip);
        im.put("dateBeginTravel", UtilPrintSupport.getDateFormatterWithMonthString(listRepresent.getBeginDate()));
        im.put("dateEndTravel", UtilPrintSupport.getDateFormatterWithMonthString(listRepresent.getEndDate()));
        im.put("reprBasic", UtilPrintSupport.getListRepresentBasicTitle(listRepresent));
    }


    private void insertRow(RtfDocument document, String text)
    {
        document.getElementList().add(RtfBean.getElementFactory().createRtfText(text));
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
    }

    private String getKey(Student student, ListRepresentTravel represent)
    {
        return new StringBuilder()
                .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(represent.getBeginDate()))
                .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(represent.getEndDate()))
                .append(represent.getAddressCountry().getTitle())
                .append(represent.getAddressItem() == null ? "" : represent.getAddressItem().getTitle())
                .append(represent.getOrganization())
                .append(represent.getTarget())
                .append(represent.getTravelPaymentData() == null ? "" : represent.getTravelPaymentData().getTitle())
                .append(student.getCompensationType().getId())
                .toString();
    }

    private Map<OrgUnit, Map<String, List<Object[]>>> getStudentData(List<? extends ListRepresent> listOfRepresents)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(RelListRepresentStudents.class, "rel")
                .where(DQLExpressions.in(
                        DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("rel")),
                        listOfRepresents
                ))
                .joinEntity(
                        "rel",
                        DQLJoinType.inner,
                        ListRepresentTravel.class,
                        "rep",
                        DQLExpressions.eq(
                                DQLExpressions.property(ListRepresentTravel.id().fromAlias("rep")),
                                DQLExpressions.property(RelListRepresentStudents.representation().id().fromAlias("rel"))
                        ))
                .column(DQLExpressions.property(RelListRepresentStudents.student().fromAlias("rel")))
                .column("rep")
                .order(DQLExpressions.property(RelListRepresentStudents.student().person().identityCard().fullFio().fromAlias("rel")));

        List<Object[]> studentDataList = UniDaoFacade.getCoreDao().getList(dql);

        Map<OrgUnit, Map<String, List<Object[]>>> dataMap = new HashMap<>();
        for (Object[] arr : studentDataList)
        {
            Student student = (Student) arr[0];
            ListRepresentTravel represent = (ListRepresentTravel) arr[1];

            OrgUnit formOU = student.getEducationOrgUnit().getFormativeOrgUnit();
            if (dataMap.get(formOU) == null)
                dataMap.put(formOU, new HashMap<>());

            String key = getKey(student, represent);
            if (dataMap.get(formOU).get(key) == null)
                dataMap.get(formOU).put(key, new ArrayList<>());

            dataMap.get(formOU).get(key).add(arr);
        }

        return dataMap;
    }


    @Override
    public Map<Student, OrderParagraphInfo> getParagInfomap()
    {
        return hashMap;
    }

    public static RtfString getStudentInfo(Student student)
    {
        String studentSex_D = student.getPerson().isMale() ? "студента" : "студентку";
        String course = student.getCourse().getTitle();
        String group = student.getGroup() == null ? "" : student.getGroup().getTitle() + " группы ";
        String developForm = UtilPrintSupport.getDevelopFormGen(student.getEducationOrgUnit().getDevelopForm());
        RtfString highSchool = UtilPrintSupport.getHighLevelSchoolTypeString(student.getEducationOrgUnit().getEducationLevelHighSchool());
        String developTermCode = student.getEducationOrgUnit().getDevelopCondition().getCode();
        if ("1".equals(developTermCode))
        {
            developTermCode = "";
        } else if ("2".equals(developTermCode))
        {
            developTermCode = " в сокращенные сроки " + student.getEducationOrgUnit().getDevelopPeriod().getTitle();
        } else if ("3".equals(developTermCode))
        {
            developTermCode = " в ускоренные сроки " + student.getEducationOrgUnit().getDevelopPeriod().getTitle();
        } else if ("4".equals(developTermCode))
        {
            developTermCode = " в сокращенные ускоренные сроки " + student.getEducationOrgUnit().getDevelopPeriod().getTitle();
        } else
        {
            developTermCode = "";
        }
        String genderBasedDevelop = "обучающегося";
        if (!student.getPerson().isMale())
        {
            genderBasedDevelop = "обучающуюся";
        }
        String developPayment = genderBasedDevelop + " " + (student.getCompensationType().getCode().equals(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET) ? "за счет средств федерального бюджета" : "с полным возмещением затрат");

        RtfString str = new RtfString();

        str.append("– " + studentSex_D + " "
                + course + " курса "
                + group
                + developForm + " формы, ");

        List<IRtfElement> lst = highSchool.toList();

        str.toList().addAll(lst);

        str.append(", "
        		//----------------------
                //+ developPayment + " "
                + developPayment
                //----------------------
                + developTermCode);

        return str;

    }
}
