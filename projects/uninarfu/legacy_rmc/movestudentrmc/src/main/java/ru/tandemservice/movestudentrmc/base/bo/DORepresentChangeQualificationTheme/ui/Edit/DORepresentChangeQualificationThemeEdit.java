package ru.tandemservice.movestudentrmc.base.bo.DORepresentChangeQualificationTheme.ui.Edit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.block.BlockListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

//-----------------------------------------------------------------------------------------------------------
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.QualificationThemeChangeOrderDSHandler;
//-----------------------------------------------------------------------------------------------------------
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.QualificationThemeOrderDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.ReasonDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.EditBasic.DORepresentBaseEditBasic;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentQualificationThemes.logic.OrgUnitSelectedDSHandler;

@Configuration
public class DORepresentChangeQualificationThemeEdit extends BusinessComponentManager {

    public static final String REASON_DS = "reasonDS";
    public static final String ORDER_DS = "orderDS";
    //---------------------------------------------------------
    public static final String CHANGEORDER_DS = "changeOrderDS";
    //---------------------------------------------------------
    public static final String ORGUNIT_DATA_SOURCE = "orgUDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(REASON_DS, reasonDSHandler()))
                .addDataSource(selectDS(ORDER_DS, orderDSHandler()))
                //----------------------------------------------------------------------
                .addDataSource(selectDS(CHANGEORDER_DS, changeOrderDSHandler()))
                //----------------------------------------------------------------------
                .addDataSource(selectDS(ORGUNIT_DATA_SOURCE, orgUnitSelectDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler reasonDSHandler()
    {
        return (IDefaultComboDataSourceHandler) new ReasonDSHandler(getName());
    }


    @Bean
    public IDefaultComboDataSourceHandler orderDSHandler()
    {
        return new QualificationThemeOrderDSHandler(getName());
    }

    //----------------------------------------------------------------
    @Bean
    public IDefaultComboDataSourceHandler changeOrderDSHandler()
    {
        return new QualificationThemeChangeOrderDSHandler(getName());
    }
    //----------------------------------------------------------------

    @Bean
    public BlockListExtPoint editBasicBlockListExtPoint()
    {
        return blockListExtPointBuilder(DORepresentChangeQualificationThemeEditUI.EDIT_BASIC_BLOCK_LIST)
                .addBlock(componentBlock(DORepresentChangeQualificationThemeEditUI.EDIT_BASIC_BLOCK, DORepresentBaseEditBasic.class).parameters("ui:blockParam"))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> orgUnitSelectDSHandler() {
        return new OrgUnitSelectedDSHandler(getName());
    }
}
