package ru.tandemservice.movestudentrmc.base.bo.DORepresentGrantCancelAndDestination;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentGrantCancelAndDestination.logic.DORepresentGrantCancelAndDestinationDAO;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentGrantCancelAndDestination.ui.Edit.DORepresentGrantCancelAndDestinationEdit;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentGrantCancelAndDestination.ui.View.DORepresentGrantCancelAndDestinationView;
import ru.tandemservice.movestudentrmc.docord.IDOObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IDOObjectModifyDAO;

@Configuration
public class DORepresentGrantCancelAndDestinationManager extends BusinessObjectManager implements IDOObjectByRepresentTypeManager {

    public static DORepresentGrantCancelAndDestinationManager instance() {
        return instance(DORepresentGrantCancelAndDestinationManager.class);
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return DORepresentGrantCancelAndDestinationEdit.class;
    }

    @Bean
    public IDOObjectModifyDAO getDOObjectModifyDAO() {
        return new DORepresentGrantCancelAndDestinationDAO();
    }

    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return DORepresentGrantCancelAndDestinationView.class;
    }

}
