package ru.tandemservice.movestudentrmc.base.bo.ListRepresentSupport.logic;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.collections.map.MultiKeyMap;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;

import ru.tandemservice.movestudentrmc.docord.IListPrintDoc;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintListRep;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;

import java.util.*;
import java.util.Map.Entry;
//---------------------------------------------------------------------------
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
//---------------------------------------------------------------------------

public class ListPrintDoc implements IListPrintDoc
{

    private Map<Student, OrderParagraphInfo> hashMap = new HashMap<>();

    public RtfDocument creationPrintDocOrder(List<? extends ListRepresent> listOfRepresents)
    {
        //Получим список студентов из представлений
        DQLSelectBuilder builder1 = new DQLSelectBuilder()
                .fromEntity(RelListRepresentStudents.class, "rel")
                .where(DQLExpressions.in(DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("rel")), listOfRepresents))
                .joinEntity(
                        "rel",
                        DQLJoinType.inner,
                        ListRepresentSupport.class,
                        "lgrnt",
                        DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudents.representation().id().fromAlias("rel")),
                                DQLExpressions.property(ListRepresentSupport.id().fromAlias("lgrnt")))
                )

                .column(DQLExpressions.property(RelListRepresentStudents.student().fromAlias("rel")))
                .column(DQLExpressions.property(RelListRepresentStudents.student().compensationType().id().fromAlias("rel")))
                .column(DQLExpressions.property(RelListRepresentStudents.student().educationOrgUnit().developForm().id().fromAlias("rel")))
                .column(DQLExpressions.property(RelListRepresentStudents.student().educationOrgUnit().formativeOrgUnit().id().fromAlias("rel")))
                .column(DQLExpressions.property("lgrnt"))

                .order(DQLExpressions.property(RelListRepresentStudents.student().educationOrgUnit().formativeOrgUnit().title().fromAlias("rel")))
                .order(DQLExpressions.property(ListRepresentSupport.grantView().title().fromAlias("lgrnt")))
                .order(DQLExpressions.property(RelListRepresentStudents.student().educationOrgUnit().developForm().shortTitle().fromAlias("rel")))
                .order(DQLExpressions.property(RelListRepresentStudents.student().compensationType().shortTitle().fromAlias("rel")))
                .order(DQLExpressions.property(RelListRepresentStudents.student().course().intValue().fromAlias("rel")))
                .order(DQLExpressions.property(RelListRepresentStudents.student().person().identityCard().fullFio().fromAlias("rel")));

        List<Object[]> students = builder1.createStatement(DataAccessServices.dao().getComponentSession()).list();

        //Параграф для шаблона
        //-----------------------------------------------------------------------------------------------------
        //UniecScriptItem scriptTemplate = new UniecScriptItem();
        //scriptTemplate.setTemplatePath("movestudentrmc/templates/listRepresentaion/listRepresentSupport.rtf");
        //RtfDocument template = new RtfReader().read(scriptTemplate.getTemplate());
        
        TemplateDocument templateDocument;
        templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(TemplateDocument.class, "list.rep.support.header");
        RtfDocument template = new RtfReader().read(templateDocument.getContent());
        //-----------------------------------------------------------------------------------------------------

        //Приложение для шаблона
        UniecScriptItem addonTemplate = new UniecScriptItem();
        //-----------------------------------------------------------------------------------------------------------
        //addonTemplate.setTemplatePath("movestudentrmc/templates/representation/addon_ListRepresentSupport.rtf");
        //RtfDocument tableAddon = new RtfReader().read(addonTemplate.getTemplate());
        
        templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(TemplateDocument.class, "list.rep.support.addon");
        RtfDocument tableAddon = new RtfReader().read(templateDocument.getContent());
        //-----------------------------------------------------------------------------------------------------------        

        //Параграф для шаблона
        UniecScriptItem paragTemplate = new UniecScriptItem();
        //----------------------------------------------------------------------------------------------------------
        //paragTemplate.setTemplatePath("movestudentrmc/templates/representation/parag_ListRepresentSupport.rtf");
        //RtfDocument paragAddon = new RtfReader().read(paragTemplate.getTemplate());
 
        templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(TemplateDocument.class, "list.rep.support.parag");
        RtfDocument paragAddon = new RtfReader().read(templateDocument.getContent());
        //----------------------------------------------------------------------------------------------------------        

        IRtfElement parag = UniRtfUtil.findElement(template.getElementList(), "PARAG");

        IRtfElement table = UniRtfUtil.findElement(template.getElementList(), "ADDON");

        template.getElementList().add(template.getElementList().indexOf(table), RtfBean.getElementFactory()
                .createRtfControl(IRtfData.PAGE));

        RtfTableModifier tableModifier = new RtfTableModifier();

        List<String[]> tableData = new ArrayList<>();


        //Наименование стипендии
        GrantView grantView = ((ListRepresentSupport) listOfRepresents.get(0)).getGrantView();
        List<RelTypeGrantView> relTypeSupportViews = DataAccessServices.dao().getList(RelTypeGrantView.class, RelTypeGrantView.view(),
                                                                                      grantView);

        if (relTypeSupportViews.isEmpty())
            throw new ApplicationException("Не найдено настройки 'Тип стипендии'-'Наименование стипендии'-'Вид стипедии' для вида: " + grantView.getTitle());
        String grantTitle = (relTypeSupportViews.get(0).getGrant().getGenitive() == null ? relTypeSupportViews.get(0).getGrant().getTitle() : relTypeSupportViews.get(0).getGrant().getGenitive());

/*		SortedMap test = new TreeMap(new EntityComparator<IEntity>(){
            @Override
			public int compare(IEntity a, IEntity b) {
				
				return super.compare(a, b);
			}
		});*/

        MultiKeyMap keyMap = new MultiKeyMap();

        //Сгруппируем данные
        for (Object[] data : students)
        {
            //MultiKey key = new MultiKey(new Object[]{data[1],data[2],data[3],data[4]});

            ListRepresentSupport rep = (ListRepresentSupport) data[4];
            GrantView view = rep.getGrantView();
            String month = CommonBaseDateUtil.getMonthNameDeclined(rep.getDateBeginingPayment(), GrammaCase.PREPOSITIONAL);
            int year = CoreDateUtils.getYear(rep.getDateBeginingPayment());

            MultiKey key = new MultiKey(new Object[]{
                    data[1], //compensationType 0
                    data[2], //developForm 1
                    data[3], //formOU 2
                    view, //3
                    month, //4
                    year //5
            });

            if (keyMap.containsKey(key))
            {
                ((List<Object[]>) keyMap.get(key)).add(data);
            } else
            {
                List<Object[]> arr = new ArrayList<>();
                arr.add(data);
                keyMap.put(key, arr);
            }
        }


        int acNumb = 1;
        int orgUnits = 0;
        int j = 1;
        Long lastOrgUnit = null;

        Set<String> baseList = new HashSet<>();
        for (ListRepresent rep : listOfRepresents)
        {
            String s = UtilPrintSupport.getListRepresentBasicTitle(rep);
            baseList.add(s);
        }
        String baseStr = StringUtils.join(baseList, ", ");

        for (Object value : keyMap.entrySet())
        {
            Entry<MultiKey, List<Object[]>> set = (Entry<MultiKey, List<Object[]>>) value;
            List<Object[]> list = set.getValue();
            Student firstStudent = (Student) list.get(0)[0];
            Long currentUnit = (Long) set.getKey().getKey(2);
            ListRepresentSupport listRepresent = (ListRepresentSupport) list.get(0)[4];

            tableModifier = new RtfTableModifier();
            tableData.clear();

            if (!currentUnit.equals(lastOrgUnit))
            {
                template.getElementList().add(template.getElementList().indexOf(parag), paragAddon.getClone().getElementList().get(0));
                orgUnits++;
                j = 1;
            }
            template.getElementList().add(template.getElementList().indexOf(parag), paragAddon.getClone().getElementList().get(1));

            template.getElementList().addAll(template.getElementList().indexOf(table), tableAddon.getClone().getElementList());
            template.getElementList().add(template.getElementList().indexOf(table), RtfBean.getElementFactory()
                    .createRtfControl(IRtfData.PAGE));

            GrantView view = (GrantView) set.getKey().getKey(3);
            String month = (String) set.getKey().getKey(4);
            int year = (Integer) set.getKey().getKey(5);

            RtfInjectModifier injectModifier = new RtfInjectModifier();

            injectModifier
                    .put("f_unit_Dative", firstStudent.getEducationOrgUnit().getFormativeOrgUnit().getDativeCaseTitle().toLowerCase())
                    .put("f_unit_Genetive", firstStudent.getEducationOrgUnit().getFormativeOrgUnit().getGenitiveCaseTitle().toLowerCase())
                    .put("ed_form_Genetive", UtilPrintSupport.getDevelopFormGen(firstStudent.getEducationOrgUnit().getDevelopForm()).toLowerCase())
                    .put("st_date", month + " " + String.valueOf(year))
                    .put("ac_num", String.valueOf(acNumb++))
                    .put("pay", firstStudent.getCompensationType().getCode().equals(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET) ? "за счет средств федерального бюджета" : firstStudent.getCompensationType().getShortTitle())
                            //.put("base", UtilPrintSupport.getListRepresentBasicTitle(listRepresent))
                    .put("base", baseStr)
                    .put("Spec_gr_Generive", view.getGenitive())
                    .put("i", String.valueOf(orgUnits))
                    .put("j", String.valueOf(j++));

            UtilPrintListRep.injectModifierRepresent(injectModifier, listRepresent);

            int i = 1;
            for (Object[] arr : list)
            {
                Student student = (Student) arr[0];
                ListRepresentSupport rep = (ListRepresentSupport) arr[4];
                hashMap.put(student, new OrderParagraphInfo(orgUnits, j - 1));
                tableData.add(fillTableRow(i++, student, rep));
            }
            tableModifier.put("T", tableData.toArray(new String[tableData.size()][]));
            injectModifier.modify(template);
            tableModifier.modify(template);

            lastOrgUnit = (Long) set.getKey().getKey(2);

        }


        String month = CommonBaseDateUtil.getMonthNameDeclined(((ListRepresentSupport) listOfRepresents.get(0)).getDateBeginingPayment(), GrammaCase.PREPOSITIONAL);
        int year = CoreDateUtils.getYear(((ListRepresentSupport) listOfRepresents.get(0)).getDateBeginingPayment());

        RtfInjectModifier allModifier = new RtfInjectModifier();
        allModifier
                .put("Grant_name_Generive", grantTitle + " в " + month + " " + String.valueOf(year) + " года")
                .put("cause", listOfRepresents.get(0).getRepresentationReason().getTitle());

        allModifier.modify(template);
        tableModifier.modify(template);

        SharedRtfUtil.removeParagraphsWithTagsRecursive(template, Arrays.asList("PARAG", "ADDON"), false, false);
        template.getElementList().remove(template.getElementList().size() - 1);
        return template;
    }

    private static String[] fillTableRow(int i, Student student, ListRepresent listRepresent)
    {
        DQLSelectBuilder tmpGrantsBuilder = new DQLSelectBuilder();
        tmpGrantsBuilder.fromEntity(StudentGrantEntity.class, "sg")
                .column("sg")
                .where(DQLExpressions.eq(
                        DQLExpressions.property(StudentGrantEntity.student().fromAlias("sg")),
                        DQLExpressions.value(student)
                ))
                .where(DQLExpressions.eq(
                        DQLExpressions.property(StudentGrantEntity.listRepresent().fromAlias("sg")),
                        DQLExpressions.value(listRepresent)
                ));

        StudentGrantEntity tmpStudentgrantEntity = tmpGrantsBuilder.createStatement(DataAccessServices.dao().getComponentSession()).uniqueResult();

        List<String> result = new ArrayList<>();

        result.add(String.valueOf(i));
        result.add(student.getBookNumber());
        result.add(student.getPerson().getIdentityCard().getFullFio());
        result.add(String.valueOf(student.getCourse().getIntValue()));
        result.add(String.valueOf(tmpStudentgrantEntity != null ? tmpStudentgrantEntity.getSum() : ""));

        return result.toArray(new String[result.size()]);
    }

    @Override
    public Map<Student, OrderParagraphInfo> getParagInfomap()
    {
        return hashMap;
    }
}
