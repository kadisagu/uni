package ru.tandemservice.movestudentrmc.component.student.GrantList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntity;


public class Controller extends AbstractBusinessController<IDAO, Model> {

    public void onRefreshComponent(IBusinessComponent component) {
        ((IDAO) getDao()).prepare(getModel(component));

        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component) {
        final Model model = getModel(component);
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<StudentGrantEntity> dataSource = new DynamicListDataSource<>(component, arg0 -> {
            ((IDAO) getDao()).prepareListDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("Стипендия", StudentGrantEntity.view().title().s()));
        dataSource.addColumn(new SimpleColumn("Сумма", StudentGrantEntity.sum().s(), DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED));
        dataSource.addColumn(new SimpleColumn("Статус", StudentGrantEntity.status().title().s()));
        dataSource.addColumn(new SimpleColumn("Приказ", StudentGrantEntity.orderNumber().s()));
        dataSource.addColumn(new SimpleColumn("Дата приказа", StudentGrantEntity.orderDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER));

        //dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit").setPermissionKey("editStuGrantEntity"));
        //dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить привязку стипендии?").setPermissionKey("delStuGrantEntity"));

        model.setDataSource(dataSource);
    }

    public void onClickEdit(IBusinessComponent component) {
        Model model = getModel(component);

        Long id = component.getListenerParameter();

        activate(
                component,
                new ComponentActivator("ru.tandemservice.movestudentrmc.component.student.GrantAddEdit",
                                       new ParametersMap().add("studentGrantID", id)
                                               .add("studentId", model.getStudent().getId())
                                               .add("eduYearId", model.getEduYear().getId())
                                               .add("month", model.getMonth()))
        );

    }

    public void onClickDelete(IBusinessComponent component) {
        Long id = component.getListenerParameter();
        ((IDAO) getDao()).deleteRow(id);
    }
}
