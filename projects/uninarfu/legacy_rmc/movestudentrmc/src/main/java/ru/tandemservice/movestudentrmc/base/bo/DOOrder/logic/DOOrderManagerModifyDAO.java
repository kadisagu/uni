package ru.tandemservice.movestudentrmc.base.bo.DOOrder.logic;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.DOOrderManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.DORepresentBaseManager;
import ru.tandemservice.movestudentrmc.dao.CheckStateUtil;
import ru.tandemservice.movestudentrmc.docord.IDOObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.MovestudentExtractStates;
import ru.tandemservice.movestudentrmc.entity.catalog.MovestudentOrderStates;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentOrderStatesCodes;

import java.util.*;

public class DOOrderManagerModifyDAO extends CommonDAO implements IDOOrderManagerModifyDAO {

    @Override
    public void save(DocumentOrder order, List<DataWrapper> representSelectedList) {

        Principal2Visa principal2Visa = DOOrderManager.instance().visaDao().getPrincipal2Visa();
        if (principal2Visa != null && order.getCreateDate().after(principal2Visa.getEditDate())) {
            order.setSignFio(principal2Visa.getSignFio());
            order.setSignPost(principal2Visa.getSignPost());
            //-------------------------------------------------------------
            order.setSignPostGenitive(principal2Visa.getSignPostGenitive());
            //-------------------------------------------------------------

            order.setPostFio1(principal2Visa.getPostFio1());
            order.setPostTitle1(principal2Visa.getPostTitle1());

            order.setPostFio2(principal2Visa.getPostFio2());
            order.setPostTitle2(principal2Visa.getPostTitle2());

            order.setPostFio3(principal2Visa.getPostFio3());
            order.setPostTitle3(principal2Visa.getPostTitle3());

            order.setPostFio4(principal2Visa.getPostFio4());
            order.setPostTitle4(principal2Visa.getPostTitle4());

            order.setPostFio5(principal2Visa.getPostFio5());
            order.setPostTitle5(principal2Visa.getPostTitle5());

            order.setPostFio6(principal2Visa.getPostFio6());
            order.setPostTitle6(principal2Visa.getPostTitle6());
        }

        Person person = PersonManager.instance().dao().getPerson(ContextLocal.getUserContext().getPrincipalContext());
        if (person != null)
            order.setOperator(person);
        getSession().save(order);

        for (DataWrapper representSelected : representSelectedList) {

            DocOrdRepresent orderRepresent = new DocOrdRepresent();
            Representation represent = (Representation) representSelected.getWrapped();
            represent.setState(get(MovestudentExtractStates.class, MovestudentExtractStates.code().s(), "5"));
            getSession().saveOrUpdate(represent);
            orderRepresent.setOrder(order);
            orderRepresent.setRepresentation((Representation) representSelected.getWrapped());
            getSession().saveOrUpdate(orderRepresent);
        }
    }

    @Override
    public void update(DocumentOrder order, List<DataWrapper> representSelectedList) {

        CheckStateUtil.checkStateOrder(order, Arrays.asList(MovestudentOrderStatesCodes.CODE_1), getSession());

        Person person = PersonManager.instance().dao().getPerson(ContextLocal.getUserContext().getPrincipalContext());
        if (person != null)
            order.setOperator(person);
        getSession().saveOrUpdate(order);
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(DocOrdRepresent.class, "r").column("r");
        builder.where(DQLExpressions.eq(DQLExpressions.property(DocOrdRepresent.order().fromAlias("r")), DQLExpressions.value(order)));
        List<DocOrdRepresent> representSelectedOldList = getRepresentList(order);

        for (DocOrdRepresent representSelectedOld : representSelectedOldList) {
            Boolean isFind = false;
            for (DataWrapper representSelected : representSelectedList) {
                if (representSelectedOld.getRepresentation().equals(representSelected.getWrapped())) {
                    isFind = true;
                    representSelectedList.remove(representSelected);
                    break;
                }
            }
            if (!isFind) {
                Representation represent = representSelectedOld.getRepresentation();
                represent.setState(get(MovestudentExtractStates.class, MovestudentExtractStates.code().s(), "3"));
                getSession().saveOrUpdate(represent);
                getSession().delete(representSelectedOld);
            }
        }

        for (DataWrapper representSelected : representSelectedList) {

            DocOrdRepresent orderRepresent = new DocOrdRepresent();
            Representation represent = (Representation) representSelected.getWrapped();
            represent.setState(get(MovestudentExtractStates.class, MovestudentExtractStates.code().s(), "5"));
            getSession().saveOrUpdate(represent);
            orderRepresent.setOrder(order);
            orderRepresent.setRepresentation(represent);
            getSession().saveOrUpdate(orderRepresent);
        }
    }

    @Override
    public List<DataWrapper> selectRepresent(DocumentOrder order) {

        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(DocOrdRepresent.class, "o").column("o");
        builder.joinEntity("o", DQLJoinType.inner, Representation.class, "r",
                           DQLExpressions.eq(DQLExpressions.property(DocOrdRepresent.representation().id().fromAlias("o")),
                                             DQLExpressions.property(Representation.id().fromAlias("r"))));
        builder.column("r");
        builder.joinEntity("r", DQLJoinType.inner, DocRepresentStudentBase.class, "s",
                           DQLExpressions.eq(DQLExpressions.property(Representation.id().fromAlias("r")),
                                             DQLExpressions.property(DocRepresentStudentBase.representation().id().fromAlias("s"))));
        builder.column("s");
        builder.where(DQLExpressions.eq(DQLExpressions.property(DocOrdRepresent.order().fromAlias("o")), DQLExpressions.value(order)));

        List<Object[]> representList = builder.createStatement(getSession()).list();
        List<DataWrapper> resultList = new ArrayList<DataWrapper>();

        for (Object[] represent : representList) {
            Representation reprData = (Representation) represent[1];
            DocRepresentStudentBase studentData = (DocRepresentStudentBase) represent[2];

            DataWrapper w = new DataWrapper(reprData.getId(), reprData.getTitle(), reprData);
            w.setProperty("student", studentData.getStudent());
            w.setProperty("represent", reprData);
            resultList.add(w);
        }

        return resultList;
    }

    @Override
    public void updateState(DocumentOrder order) {

        getSession().saveOrUpdate(order);
    }

    @Override
    public void doCommit(DocumentOrder order, ErrorCollector error) {

        DQLSelectBuilder builder = new DQLSelectBuilder();

        builder.fromEntity(DocOrdRepresent.class, "o").column("o");

        builder.joinEntity("o", DQLJoinType.inner, Representation.class, "r",
                           DQLExpressions.eq(DQLExpressions.property(DocOrdRepresent.representation().id().fromAlias("o")),
                                             DQLExpressions.property(Representation.id().fromAlias("r"))));
        builder.column("r");

        builder.joinEntity("r", DQLJoinType.inner, DocRepresentStudentBase.class, "s",
                           DQLExpressions.eq(DQLExpressions.property(Representation.id().fromAlias("r")),
                                             DQLExpressions.property(DocRepresentStudentBase.representation().id().fromAlias("s"))));
        builder.column("s");

        builder.where(DQLExpressions.eq(DQLExpressions.property(DocOrdRepresent.order().fromAlias("o")), DQLExpressions.value(order)));

        List<Object[]> representList = builder.createStatement(getSession()).list();

        //List<DocOrdRepresent> representList = getRepresentList(order);

        Boolean isComplete = true;

        /*
        final RtfDocument docMain = BuildPrintDocV2.creationPrintDocOrder(order, error, true);
        if (docMain != null)
            order.setDocument(RtfUtil.toByteArray(docMain));
        else
            isComplete = false;

        if (!isComplete) {
            error.add("Ошибка в формировании печатной формы приказа!");
        }
		*/
        for (Object[] represent : representList) {
            Representation reprData = (Representation) represent[1];
            DocRepresentStudentBase studentData = (DocRepresentStudentBase) represent[2];

            if (!isComplete)
                break;

            if (reprData.isCommitted())
                return;


            String type = reprData.getType().getCode();
            IDOObjectByRepresentTypeManager representManager = DORepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(type);
            
            /*
            reprData.setDocRepresent(RtfUtil.toByteArray(UtilPrintSupport.representRendererRtf(reprData)));
			*/

            if (representManager.getDOObjectModifyDAO().doCommit(reprData, error)) {
                reprData.setState(get(MovestudentExtractStates.class, MovestudentExtractStates.code().s(), "6"));
                reprData.setCommitted(true);

                DQLSelectBuilder docsBuilder = new DQLSelectBuilder().fromEntity(NarfuDocument.class, "docs").column("docs");
                docsBuilder.where(DQLExpressions.eq(
                        DQLExpressions.property(NarfuDocument.student().fromAlias("docs")),
                        DQLExpressions.value(studentData.getStudent().getId())));

                List<NarfuDocument> docsList = DataAccessServices.dao().getList(docsBuilder);
                for (NarfuDocument doc : docsList) {
                    doc.setTmp(false);
                    getSession().saveOrUpdate(doc);
                }
                getSession().saveOrUpdate(reprData);
            }
            else {
                //error.add("Ошибка в проведении представлений приказа!");
                throw new ApplicationException("Ошибка в проведении представлений приказа!");
                //isComplete = false;
                //break;
            }
        }

        if (isComplete) {

            order.setState(get(MovestudentOrderStates.class, MovestudentOrderStates.code().s(), "5"));
            getSession().saveOrUpdate(order);
        }

    }

    @Override
    public void doRollback(DocumentOrder order, ErrorCollector error) {
        List<DocOrdRepresent> representList = getRepresentList(order);

        Boolean isComplete = true;
        for (DocOrdRepresent represent : representList) {

            if (!represent.getRepresentation().isCommitted())
                continue;

            String type = represent.getRepresentation().getType().getCode();
            IDOObjectByRepresentTypeManager representManager = DORepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(type);

            if (representManager.getDOObjectModifyDAO().doRollback(represent.getRepresentation(), error)) {

                represent.getRepresentation().setCommitted(false);
                //--------------------------------------------------------------------------------------------------------------------
                //represent.getRepresentation().setState(get(MovestudentExtractStates.class, MovestudentExtractStates.code().s(), "3"));
                represent.getRepresentation().setState(get(MovestudentExtractStates.class, MovestudentExtractStates.code().s(), "5"));
                //--------------------------------------------------------------------------------------------------------------------
                represent.getRepresentation().setDocExtract(null);
                represent.getRepresentation().setDocRepresent(null);
                represent.getRepresentation().setNumber(null);

                getSession().saveOrUpdate(represent.getRepresentation());
            }
            else {
                isComplete = false;
                break;
            }
        }

        if (isComplete) {
            order.setState(get(MovestudentOrderStates.class, MovestudentOrderStates.code().s(), "3"));
            order.setDocument(null);
            getSession().saveOrUpdate(order);
        }

    }

    @Override
    public void saveDocOrder(DocumentOrder order, RtfDocument docMain) {

        order.setDocument(RtfUtil.toByteArray(docMain));
        getSession().saveOrUpdate(order);
    }

    private List<DocOrdRepresent> getRepresentList(DocumentOrder order) {

        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(DocOrdRepresent.class, "r").column("r");
        builder.where(DQLExpressions.eq(DQLExpressions.property(DocOrdRepresent.order().fromAlias("r")), DQLExpressions.value(order)));
        return builder.createStatement(getSession()).list();
    }

    @Override
    public void saveExtractsText(DocumentOrder order) {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocOrdRepresent.class, "rel")
                .joinEntity("rel", DQLJoinType.inner, DocRepresentStudentBase.class, "stud",
                            DQLExpressions.eq(
                                    DQLExpressions.property(DocOrdRepresent.representation().id().fromAlias("rel")),
                                    DQLExpressions.property(DocRepresentStudentBase.representation().id().fromAlias("stud"))
                            )
                )
                .where(DQLExpressions.eqValue(DQLExpressions.property(DocOrdRepresent.order().id().fromAlias("rel")), order.getId()))
                .column("rel")
                .order(DQLExpressions.property(DocOrdRepresent.order().number().fromAlias("rel")))
                .joinPath(DQLJoinType.inner, DocOrdRepresent.order().fromAlias("rel"), "ord")
                .joinPath(DQLJoinType.inner, DocOrdRepresent.representation().fromAlias("rel"), "repres")
                .column("stud");

        List<Object[]> lst = getList(builder);

        Map<Long, RtfDocument> templMap = new HashMap<Long, RtfDocument>();
        RtfDocument commonExtractTemplate = UtilPrintSupport.getTemplate("modularExtractCommonTemplate", "common");

        for (Object[] obj : lst) {
            RtfDocument extract = commonExtractTemplate.getClone();
            DocOrdRepresent rel = (DocOrdRepresent) obj[0];
            DocRepresentStudentBase student = (DocRepresentStudentBase) obj[1];

            Representation represent = rel.getRepresentation();

            IDOObjectByRepresentTypeManager representManager = DORepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(represent.getType().getCode());

            OrderPrint.createPrintDocOrder(rel.getOrder(), new ErrorCollector(), false);
            OrderParagraphInfo paragraphInfo = OrderPrint.studentParagInfoMap.get(student.getId());

            //Шаблон выписки
            RtfDocument templateRepresent = null;
            if (templMap.get(represent.getType().getId()) == null) {
                RtfDocument template = representManager.getDOObjectModifyDAO().getTemplateRepresent(represent.getType());
                templMap.put(represent.getType().getId(), template);
                templateRepresent = template.getClone();
            }
            else
                templateRepresent = templMap.get(represent.getType().getId()).getClone();

            extract = UtilPrintSupport.extractRendererRtf(represent, rel.getOrder(), extract, templateRepresent, paragraphInfo);

            ExtractTextRelation relation = new ExtractTextRelation();
            relation.setOrder(rel);
            relation.setStudent(student.getStudent());
            relation.setText(RtfUtil.toByteArray(extract));
            getSession().save(relation);

        }

    }

    @Override
    public void deleteExtractsText(DocumentOrder order) {
        new DQLDeleteBuilder(ExtractTextRelation.class)
                .where(DQLExpressions.eqValue(DQLExpressions.property(ExtractTextRelation.order().order().id()), order.getId()))
                .createStatement(getSession()).execute();

    }
}
