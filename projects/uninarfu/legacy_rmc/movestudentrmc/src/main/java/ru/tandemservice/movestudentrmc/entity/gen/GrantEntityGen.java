package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudentrmc.entity.GrantEntity;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность стипендия/выплата
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class GrantEntityGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.GrantEntity";
    public static final String ENTITY_NAME = "grantEntity";
    public static final int VERSION_HASH = -33436400;
    private static IEntityMeta ENTITY_META;

    public static final String L_VIEW = "view";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_EDU_YEAR = "eduYear";
    public static final String P_MONTH = "month";
    public static final String P_SUM = "sum";
    public static final String P_ORDER_NUMBER = "orderNumber";
    public static final String P_ORDER_DATE = "orderDate";

    private GrantView _view;     // Вид стипендии
    private OrgUnit _orgUnit;     // Формирующее подразделение
    private EducationYear _eduYear;     // Учебный год
    private String _month;     // Месяц
    private double _sum;     // Сумма
    private String _orderNumber;     // Номер приказа
    private Date _orderDate;     // Дата приказа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Вид стипендии. Свойство не может быть null.
     */
    @NotNull
    public GrantView getView()
    {
        return _view;
    }

    /**
     * @param view Вид стипендии. Свойство не может быть null.
     */
    public void setView(GrantView view)
    {
        dirty(_view, view);
        _view = view;
    }

    /**
     * @return Формирующее подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Формирующее подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getEduYear()
    {
        return _eduYear;
    }

    /**
     * @param eduYear Учебный год. Свойство не может быть null.
     */
    public void setEduYear(EducationYear eduYear)
    {
        dirty(_eduYear, eduYear);
        _eduYear = eduYear;
    }

    /**
     * @return Месяц. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getMonth()
    {
        return _month;
    }

    /**
     * @param month Месяц. Свойство не может быть null.
     */
    public void setMonth(String month)
    {
        dirty(_month, month);
        _month = month;
    }

    /**
     * @return Сумма. Свойство не может быть null.
     */
    @NotNull
    public double getSum()
    {
        return _sum;
    }

    /**
     * @param sum Сумма. Свойство не может быть null.
     */
    public void setSum(double sum)
    {
        dirty(_sum, sum);
        _sum = sum;
    }

    /**
     * @return Номер приказа.
     */
    @Length(max=255)
    public String getOrderNumber()
    {
        return _orderNumber;
    }

    /**
     * @param orderNumber Номер приказа.
     */
    public void setOrderNumber(String orderNumber)
    {
        dirty(_orderNumber, orderNumber);
        _orderNumber = orderNumber;
    }

    /**
     * @return Дата приказа.
     */
    public Date getOrderDate()
    {
        return _orderDate;
    }

    /**
     * @param orderDate Дата приказа.
     */
    public void setOrderDate(Date orderDate)
    {
        dirty(_orderDate, orderDate);
        _orderDate = orderDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof GrantEntityGen)
        {
            setView(((GrantEntity)another).getView());
            setOrgUnit(((GrantEntity)another).getOrgUnit());
            setEduYear(((GrantEntity)another).getEduYear());
            setMonth(((GrantEntity)another).getMonth());
            setSum(((GrantEntity)another).getSum());
            setOrderNumber(((GrantEntity)another).getOrderNumber());
            setOrderDate(((GrantEntity)another).getOrderDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends GrantEntityGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) GrantEntity.class;
        }

        public T newInstance()
        {
            return (T) new GrantEntity();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "view":
                    return obj.getView();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "eduYear":
                    return obj.getEduYear();
                case "month":
                    return obj.getMonth();
                case "sum":
                    return obj.getSum();
                case "orderNumber":
                    return obj.getOrderNumber();
                case "orderDate":
                    return obj.getOrderDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "view":
                    obj.setView((GrantView) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "eduYear":
                    obj.setEduYear((EducationYear) value);
                    return;
                case "month":
                    obj.setMonth((String) value);
                    return;
                case "sum":
                    obj.setSum((Double) value);
                    return;
                case "orderNumber":
                    obj.setOrderNumber((String) value);
                    return;
                case "orderDate":
                    obj.setOrderDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "view":
                        return true;
                case "orgUnit":
                        return true;
                case "eduYear":
                        return true;
                case "month":
                        return true;
                case "sum":
                        return true;
                case "orderNumber":
                        return true;
                case "orderDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "view":
                    return true;
                case "orgUnit":
                    return true;
                case "eduYear":
                    return true;
                case "month":
                    return true;
                case "sum":
                    return true;
                case "orderNumber":
                    return true;
                case "orderDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "view":
                    return GrantView.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "eduYear":
                    return EducationYear.class;
                case "month":
                    return String.class;
                case "sum":
                    return Double.class;
                case "orderNumber":
                    return String.class;
                case "orderDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<GrantEntity> _dslPath = new Path<GrantEntity>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "GrantEntity");
    }
            

    /**
     * @return Вид стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.GrantEntity#getView()
     */
    public static GrantView.Path<GrantView> view()
    {
        return _dslPath.view();
    }

    /**
     * @return Формирующее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.GrantEntity#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.GrantEntity#getEduYear()
     */
    public static EducationYear.Path<EducationYear> eduYear()
    {
        return _dslPath.eduYear();
    }

    /**
     * @return Месяц. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.GrantEntity#getMonth()
     */
    public static PropertyPath<String> month()
    {
        return _dslPath.month();
    }

    /**
     * @return Сумма. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.GrantEntity#getSum()
     */
    public static PropertyPath<Double> sum()
    {
        return _dslPath.sum();
    }

    /**
     * @return Номер приказа.
     * @see ru.tandemservice.movestudentrmc.entity.GrantEntity#getOrderNumber()
     */
    public static PropertyPath<String> orderNumber()
    {
        return _dslPath.orderNumber();
    }

    /**
     * @return Дата приказа.
     * @see ru.tandemservice.movestudentrmc.entity.GrantEntity#getOrderDate()
     */
    public static PropertyPath<Date> orderDate()
    {
        return _dslPath.orderDate();
    }

    public static class Path<E extends GrantEntity> extends EntityPath<E>
    {
        private GrantView.Path<GrantView> _view;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private EducationYear.Path<EducationYear> _eduYear;
        private PropertyPath<String> _month;
        private PropertyPath<Double> _sum;
        private PropertyPath<String> _orderNumber;
        private PropertyPath<Date> _orderDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Вид стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.GrantEntity#getView()
     */
        public GrantView.Path<GrantView> view()
        {
            if(_view == null )
                _view = new GrantView.Path<GrantView>(L_VIEW, this);
            return _view;
        }

    /**
     * @return Формирующее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.GrantEntity#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.GrantEntity#getEduYear()
     */
        public EducationYear.Path<EducationYear> eduYear()
        {
            if(_eduYear == null )
                _eduYear = new EducationYear.Path<EducationYear>(L_EDU_YEAR, this);
            return _eduYear;
        }

    /**
     * @return Месяц. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.GrantEntity#getMonth()
     */
        public PropertyPath<String> month()
        {
            if(_month == null )
                _month = new PropertyPath<String>(GrantEntityGen.P_MONTH, this);
            return _month;
        }

    /**
     * @return Сумма. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.GrantEntity#getSum()
     */
        public PropertyPath<Double> sum()
        {
            if(_sum == null )
                _sum = new PropertyPath<Double>(GrantEntityGen.P_SUM, this);
            return _sum;
        }

    /**
     * @return Номер приказа.
     * @see ru.tandemservice.movestudentrmc.entity.GrantEntity#getOrderNumber()
     */
        public PropertyPath<String> orderNumber()
        {
            if(_orderNumber == null )
                _orderNumber = new PropertyPath<String>(GrantEntityGen.P_ORDER_NUMBER, this);
            return _orderNumber;
        }

    /**
     * @return Дата приказа.
     * @see ru.tandemservice.movestudentrmc.entity.GrantEntity#getOrderDate()
     */
        public PropertyPath<Date> orderDate()
        {
            if(_orderDate == null )
                _orderDate = new PropertyPath<Date>(GrantEntityGen.P_ORDER_DATE, this);
            return _orderDate;
        }

        public Class getEntityClass()
        {
            return GrantEntity.class;
        }

        public String getEntityName()
        {
            return "grantEntity";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
