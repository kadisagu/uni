package ru.tandemservice.movestudentrmc.component.student.DocumentAddEdit;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {

    public void deleteFile(Model model);
}
