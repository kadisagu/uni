package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.IAbstractOrder;
import ru.tandemservice.movestudentrmc.entity.IAbstractRepresentation;
import ru.tandemservice.movestudentrmc.entity.IRepresentOrder;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import ru.tandemservice.movestudentrmc.entity.IRepresentOrder;

@SuppressWarnings({"all", "unchecked", "unused"})
public abstract class IRepresentOrderGen extends InterfaceStubBase
 implements IRepresentOrder{
    public static final int VERSION_HASH = 984587942;

    public static final String L_ORDER = "order";
    public static final String L_REPRESENTATION = "representation";

    private IAbstractOrder _order;
    private IAbstractRepresentation _representation;

    @NotNull

    public IAbstractOrder getOrder()
    {
        return _order;
    }

    public void setOrder(IAbstractOrder order)
    {
        _order = order;
    }

    @NotNull

    public IAbstractRepresentation getRepresentation()
    {
        return _representation;
    }

    public void setRepresentation(IAbstractRepresentation representation)
    {
        _representation = representation;
    }

    private static final Path<IRepresentOrder> _dslPath = new Path<IRepresentOrder>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ru.tandemservice.movestudentrmc.entity.IRepresentOrder");
    }
            

    /**
     * @return Приказ. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.IRepresentOrder#getOrder()
     */
    public static IAbstractOrderGen.Path<IAbstractOrder> order()
    {
        return _dslPath.order();
    }

    /**
     * @return Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.IRepresentOrder#getRepresentation()
     */
    public static IAbstractRepresentationGen.Path<IAbstractRepresentation> representation()
    {
        return _dslPath.representation();
    }

    public static class Path<E extends IRepresentOrder> extends EntityPath<E>
    {
        private IAbstractOrderGen.Path<IAbstractOrder> _order;
        private IAbstractRepresentationGen.Path<IAbstractRepresentation> _representation;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приказ. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.IRepresentOrder#getOrder()
     */
        public IAbstractOrderGen.Path<IAbstractOrder> order()
        {
            if(_order == null )
                _order = new IAbstractOrderGen.Path<IAbstractOrder>(L_ORDER, this);
            return _order;
        }

    /**
     * @return Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.IRepresentOrder#getRepresentation()
     */
        public IAbstractRepresentationGen.Path<IAbstractRepresentation> representation()
        {
            if(_representation == null )
                _representation = new IAbstractRepresentationGen.Path<IAbstractRepresentation>(L_REPRESENTATION, this);
            return _representation;
        }

        public Class getEntityClass()
        {
            return IRepresentOrder.class;
        }

        public String getEntityName()
        {
            return "ru.tandemservice.movestudentrmc.entity.IRepresentOrder";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
