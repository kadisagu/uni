package ru.tandemservice.movestudentrmc.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Тип стипендии/выплаты
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class GrantTypeGen extends EntityBase
 implements INaturalIdentifiable<GrantTypeGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.catalog.GrantType";
    public static final String ENTITY_NAME = "grantType";
    public static final int VERSION_HASH = 2030182468;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_TITLE = "title";
    public static final String P_ACADEM = "academ";
    public static final String P_SOCIAL = "social";
    public static final String P_CONFIG_RELATION = "configRelation";

    private String _code;     // Системный код
    private String _title; 
    private boolean _academ = false;     // Cчитать академической
    private boolean _social = false;     // Cчитать социальной

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return  Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title  Свойство не может быть null и должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Cчитать академической. Свойство не может быть null.
     */
    @NotNull
    public boolean isAcadem()
    {
        return _academ;
    }

    /**
     * @param academ Cчитать академической. Свойство не может быть null.
     */
    public void setAcadem(boolean academ)
    {
        dirty(_academ, academ);
        _academ = academ;
    }

    /**
     * @return Cчитать социальной. Свойство не может быть null.
     */
    @NotNull
    public boolean isSocial()
    {
        return _social;
    }

    /**
     * @param social Cчитать социальной. Свойство не может быть null.
     */
    public void setSocial(boolean social)
    {
        dirty(_social, social);
        _social = social;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof GrantTypeGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((GrantType)another).getCode());
            }
            setTitle(((GrantType)another).getTitle());
            setAcadem(((GrantType)another).isAcadem());
            setSocial(((GrantType)another).isSocial());
        }
    }

    public INaturalId<GrantTypeGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<GrantTypeGen>
    {
        private static final String PROXY_NAME = "GrantTypeNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof GrantTypeGen.NaturalId) ) return false;

            GrantTypeGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends GrantTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) GrantType.class;
        }

        public T newInstance()
        {
            return (T) new GrantType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "title":
                    return obj.getTitle();
                case "academ":
                    return obj.isAcadem();
                case "social":
                    return obj.isSocial();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "academ":
                    obj.setAcadem((Boolean) value);
                    return;
                case "social":
                    obj.setSocial((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "title":
                        return true;
                case "academ":
                        return true;
                case "social":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "title":
                    return true;
                case "academ":
                    return true;
                case "social":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "title":
                    return String.class;
                case "academ":
                    return Boolean.class;
                case "social":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<GrantType> _dslPath = new Path<GrantType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "GrantType");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.GrantType#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return  Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.GrantType#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Cчитать академической. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.GrantType#isAcadem()
     */
    public static PropertyPath<Boolean> academ()
    {
        return _dslPath.academ();
    }

    /**
     * @return Cчитать социальной. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.GrantType#isSocial()
     */
    public static PropertyPath<Boolean> social()
    {
        return _dslPath.social();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.catalog.GrantType#getConfigRelation()
     */
    public static SupportedPropertyPath<Boolean> configRelation()
    {
        return _dslPath.configRelation();
    }

    public static class Path<E extends GrantType> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _title;
        private PropertyPath<Boolean> _academ;
        private PropertyPath<Boolean> _social;
        private SupportedPropertyPath<Boolean> _configRelation;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.GrantType#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(GrantTypeGen.P_CODE, this);
            return _code;
        }

    /**
     * @return  Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.GrantType#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(GrantTypeGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Cчитать академической. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.GrantType#isAcadem()
     */
        public PropertyPath<Boolean> academ()
        {
            if(_academ == null )
                _academ = new PropertyPath<Boolean>(GrantTypeGen.P_ACADEM, this);
            return _academ;
        }

    /**
     * @return Cчитать социальной. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.GrantType#isSocial()
     */
        public PropertyPath<Boolean> social()
        {
            if(_social == null )
                _social = new PropertyPath<Boolean>(GrantTypeGen.P_SOCIAL, this);
            return _social;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.catalog.GrantType#getConfigRelation()
     */
        public SupportedPropertyPath<Boolean> configRelation()
        {
            if(_configRelation == null )
                _configRelation = new SupportedPropertyPath<Boolean>(GrantTypeGen.P_CONFIG_RELATION, this);
            return _configRelation;
        }

        public Class getEntityClass()
        {
            return GrantType.class;
        }

        public String getEntityName()
        {
            return "grantType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract Boolean getConfigRelation();
}
