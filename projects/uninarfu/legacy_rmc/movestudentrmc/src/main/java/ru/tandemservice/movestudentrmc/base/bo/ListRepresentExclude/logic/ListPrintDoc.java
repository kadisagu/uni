package ru.tandemservice.movestudentrmc.base.bo.ListRepresentExclude.logic;

import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfString;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.AbstractListPrintDoc;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListRepresentExclude;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.*;

public class ListPrintDoc extends AbstractListPrintDoc<ListRepresentExclude>
{

    public final String TEMPLATE_HEADER = "list.rep.exclude.header";
    public final String TEMPLATE_PARAG = "list.rep.exclude.parag";

    @Override
    protected String getTemplateHeader()
    {
        return TEMPLATE_HEADER;
    }

    @Override
    protected String getTemplateParag()
    {
        return TEMPLATE_PARAG;
    }

    @Override
    protected String getTemplateAddon()
    {
        return null;
    }

    @Override
    protected void paragraphInjectModifier(RtfInjectModifier im,
                                           ListRepresentExclude listRepresent, Student student)
    {
        injectModifier(im, listRepresent, student);
    }

    @Override
    protected Class<? extends ListRepresent> getListRepresentClass()
    {
        return ListRepresentExclude.class;
    }

    @Override
    protected String getKey(Student student, ListRepresentExclude represent)
    {
        return new StringBuilder()
                .append(student.getEducationOrgUnit().getDevelopForm().getId())
                .append(represent.getDateExclude().getTime())
                .toString();
    }

    public static void injectModifier(RtfInjectModifier im, ListRepresentExclude listRepresent, Student student)
    {
        im.put("dateExclude", UtilPrintSupport.getDateFormatterWithMonthString(listRepresent.getDateExclude()));
        im.put("course", student.getCourse().getTitle());
        im.put("developForm", UtilPrintSupport.getDevelopFormGen(student.getEducationOrgUnit().getDevelopForm()));
        im.put("highSchool", UtilPrintSupport.getHighLevelSchoolTypeString(student.getEducationOrgUnit().getEducationLevelHighSchool()));
        im.put("reason", listRepresent.getRepresentationReason().getTitle());
    }

    @Override
    protected RtfDocument getParagraph(RtfDocument template, int formCounter,
                                       int counter, int addonCounter, List<Object[]> studentList)
    {
        RtfDocument result = template.getClone();
        ListRepresentExclude listRepresent = (ListRepresentExclude) studentList.get(0)[1];
        Student student = (Student) studentList.get(0)[0];

        Map<Course, List<Student>> courseMap = new HashMap<>();

        for (Object[] o : studentList)
        {
            Student s = (Student) o[0];
            Course key = s.getCourse();
            getParagInfomap().put(s, new OrderParagraphInfo(formCounter, counter));

            List<Student> lst = courseMap.get(key);
            if (lst == null)
                lst = new ArrayList<>();
            lst.add(s);

            courseMap.put(key, lst);
        }
        RtfInjectModifier im = new RtfInjectModifier();
        injectModifier(im, listRepresent, student);
        im.put("i", String.valueOf(formCounter).trim());
        im.put("j", String.valueOf(counter).trim());

        List<Course> courseList = new ArrayList<>(courseMap.keySet());

        Collections.sort(courseList,
                new EntityComparator<>(new EntityOrder(Course.intValue())));

        for (Course course : courseList)
        {
            List<Student> studList = courseMap.get(course);
            insertRow(result, course.getTitle() + " курс");

            Map<EducationLevelsHighSchool, List<Student>> levelMap = new HashMap<>();

            for (Student s : studList)
            {
                EducationLevelsHighSchool key = s.getEducationOrgUnit().getEducationLevelHighSchool();

                List<Student> lst = levelMap.get(key);
                if (lst == null)
                    lst = new ArrayList<>();
                lst.add(s);

                levelMap.put(key, lst);
            }

            List<EducationLevelsHighSchool> eduLevelList = new ArrayList<>(levelMap.keySet());

            Collections.sort(eduLevelList,
                    new EntityComparator<>(new EntityOrder(EducationLevelsHighSchool.displayableTitle())));

            for (EducationLevelsHighSchool eduLevel : eduLevelList)
            {
                insertRow(result, UtilPrintSupport.getHighLevelSchoolTypeString(eduLevel));
                studList = levelMap.get(eduLevel);

                Map<String, List<Student>> groupMap = new HashMap<>();

                for (Student s : studList)
                {
                    String key = s.getGroup() != null ? s.getGroup().getTitle() : "";

                    List<Student> lst = groupMap.get(key);
                    if (lst == null)
                        lst = new ArrayList<>();
                    lst.add(s);

                    groupMap.put(key, lst);
                }

                List<String> groupList = new ArrayList<>(groupMap.keySet());

                Collections.sort(groupList);

                for (String group : groupList)
                {
                    if (!group.isEmpty())
                        insertRow(result, ("группа " + group));
                    studList = groupMap.get(group);
                    Collections.sort(studList,
                            new EntityComparator<>(new EntityOrder(Student.person().identityCard().fullFio())));
                    int n = 1;
                    for (Student std : studList)
                    {
                        result.getElementList().addAll(new RtfString().append(n++ + ")").append(IRtfData.TAB)
                                .append(std.getPerson().getFullFio())
                                .append(std.getCompensationType().isBudget() ? "" : " (по договору)")
                                .par().toList());
                    }
                    result.addElement(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
                }
            }

        }
        im.modify(result);
        return result;
    }
}
