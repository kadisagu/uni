package ru.tandemservice.movestudentrmc.base.bo.DocumentSettings.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.entity.catalog.DocumentType;

public class DocumentTypeDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput> {

    public static final String TYPE_COLUMN = "type";

    public DocumentTypeDSHandler(String ownerId) {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context) {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocumentType.class, "type").addColumn("type");

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).order(DocumentType.class, "type").build();
    }
}
