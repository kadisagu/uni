package ru.tandemservice.movestudentrmc.base.bo.DORepresentStudentTicket.ui.Edit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.block.BlockListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.DocumentTypeDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.ReasonDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.EditBasic.DORepresentBaseEditBasic;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentExclude.ui.Edit.DORepresentExcludeEditUI;

@Configuration
public class DORepresentStudentTicketEdit extends BusinessComponentManager {

    public static final String REASON_DS = "reasonDS";
    public static final String DOCUMENT_TYPE_DS = "documentTypeForRepresentDS";


    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(REASON_DS, reasonDSHandler()))
                .addDataSource(selectDS(DOCUMENT_TYPE_DS, documentTypeDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler reasonDSHandler()
    {
        return new ReasonDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler documentTypeDSHandler()
    {
        return new DocumentTypeDSHandler(getName());
    }


    @Bean
    public BlockListExtPoint editBasicBlockListExtPoint()
    {
        return blockListExtPointBuilder(DORepresentExcludeEditUI.EDIT_BASIC_BLOCK_LIST)
                .addBlock(componentBlock(DORepresentExcludeEditUI.EDIT_BASIC_BLOCK, DORepresentBaseEditBasic.class).parameters("ui:blockParam"))
                .create();
    }
}
