package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonBasic;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;
import ru.tandemservice.uni.util.FilterUtils;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class ReasonRepresentDSHandler extends DefaultComboDataSourceHandler {

    public ReasonRepresentDSHandler(String ownerId) {
        super(ownerId, RepresentationReason.class);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        String filter = ep.input.getComboFilterByValue();
        if (StringUtils.isNotBlank(filter)) {
            ep.dqlBuilder.where(like(DQLFunctions.upper(property(RepresentationReason.title().fromAlias("e"))), value(CoreStringUtils.escapeLike(filter, true))));
        }

        ep.dqlBuilder.joinEntity("e", DQLJoinType.inner, RelRepresentationReasonBasic.class, "r",
                                 eq(property(RelRepresentationReasonBasic.reason().id().fromAlias("r")),
                                    property(RepresentationReason.id().fromAlias("e"))));

        FilterUtils.applySelectFilter(ep.dqlBuilder, RelRepresentationReasonBasic.type().fromAlias("r"), ep.context.get("typeRepresentFilter"));
    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
    {
        ep.dqlBuilder.order("e." + RepresentationReason.title());
    }
}
