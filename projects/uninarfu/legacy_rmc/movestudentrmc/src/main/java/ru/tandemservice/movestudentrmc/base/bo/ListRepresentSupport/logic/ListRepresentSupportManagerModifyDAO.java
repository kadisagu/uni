package ru.tandemservice.movestudentrmc.base.bo.ListRepresentSupport.logic;

import com.ibm.icu.util.Calendar;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.AbstractListRepresentDAO;
import ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab.MonthWrapper;
import ru.tandemservice.movestudentrmc.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudentrmc.docord.IListPrintDoc;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.ReportRenderer;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.*;

public class ListRepresentSupportManagerModifyDAO extends AbstractListRepresentDAO
{

    public void save(ListRepresent listRepresent, List<Student> studentSelectedList, DocListRepresentBasics representBasics) {
    }

    @Transactional
    public void saveSupport(ListRepresent listRepresent, List<DataWrapper> studentSelectedList, DocListRepresentBasics representBasics) {

        ListRepresentSupport listGrant = (ListRepresentSupport) listRepresent;

        //период попадает в учебный год
        EducationYear eduYear = listGrant.getEducationYear();

        Calendar beginYear = Calendar.getInstance();
        beginYear.set(eduYear.getIntValue(), 8, 1, 0, 0, 0);
        beginYear.set(14, 0);
        Calendar endYear = Calendar.getInstance();
        endYear.set(eduYear.getIntValue() + 1, 7, 31, 23, 59, 59);
        endYear.set(14, 999);

        if (!CommonBaseDateUtil.isBetween(listGrant.getDateBeginingPayment(), beginYear.getTime(), endYear.getTime()))
            throw new ApplicationException("Даты назначения стипендии/выплаты выходят за границы учебного года " + eduYear.getTitle());

        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(RelListRepresentStudents.class)
                .fromEntity(RelListRepresentStudents.class, "rls")
                .where(DQLExpressions.eq
                        (DQLExpressions.property(RelListRepresentStudents.representation()), DQLExpressions.value(listRepresent)));

        //Почистим старых студентов
        deleteBuilder.createStatement(getSession()).execute();

        //Сохраним оператора, изменившего представление
        Person person = PersonManager.instance().dao().getPerson(ContextLocal.getUserContext().getPrincipalContext());
        if (person != null)
            listGrant.setOperator(person);

        IPrincipalContext context = ContextLocal.getUserContext().getPrincipalContext();
        listRepresent.setCreator(context);
        Map<Student, Double> grantMap = new HashMap<Student, Double>();
        List<Student> stuSelectedList = new ArrayList<Student>();
        for (DataWrapper dataW : studentSelectedList) {
            Student student = (Student) dataW.getProperty("student");
            StudentGrantEntity studentGrantEntity = (StudentGrantEntity) dataW.getProperty("grant");

            grantMap.put(student, studentGrantEntity.getSum());
            stuSelectedList.add(student);
        }


        List<String> periodList = MonthWrapper.getMonthsList(listGrant.getDateBeginingPayment(), listGrant.getDateBeginingPayment(), listGrant.getEducationYear());
        String month = periodList.get(0);
        List<String> errorList = new ArrayList<String>();

        boolean good = MoveStudentDaoFacade.getGrantDAO().checkStudentGrants(listGrant.getId(), listGrant.getGrantView(), stuSelectedList, periodList, errorList);

        if (!good) {
            RtfDocument errorReport = RtfBean.getElementFactory().createRtfDocument();
            IRtfControl par = RtfBean.getElementFactory().createRtfControl(IRtfData.PAR);
            for (String text : errorList) {
                IRtfText studText = RtfBean.getElementFactory().createRtfText(text);
                errorReport.addElement(studText);
                errorReport.getElementList().addAll(Arrays.asList(par, par));
            }
            BusinessComponentUtils.downloadDocument(new ReportRenderer("Ошибки.rtf", errorReport), true);

			/*
			if (this.getSession().getTransaction() != null) {
				this.getSession().getTransaction().rollback();
				this.getSession().beginTransaction();
			}
			*/
            throw new ApplicationException("Не удалось сохранить представление");
        }

        baseCreateOrUpdate(listRepresent);
        baseCreateOrUpdate(representBasics);
        for (Student student : stuSelectedList) {
            RelListRepresentStudents relListRepresentGrantStudents = new RelListRepresentStudents();
            relListRepresentGrantStudents.setRepresentation(listRepresent);
            relListRepresentGrantStudents.setStudent(student);
            baseCreateOrUpdate(relListRepresentGrantStudents);
        }

        MoveStudentDaoFacade.getGrantDAO().deleteStudentGrants(listGrant);
        MoveStudentDaoFacade.getGrantDAO().saveStudentGrants(listGrant, listGrant.getGrantView(), grantMap, month, listGrant.getEducationYear());
    }


    @Override
    public void buildBodyExtract(ListOrdListRepresent listOrdListRepresent,
                                 Student student, RtfDocument docExtract, Map<Student, OrderParagraphInfo> map)
    {
        super.buildBodyExtract(listOrdListRepresent, student, docExtract, map);

        ListRepresentSupport representation = (ListRepresentSupport) listOrdListRepresent.getRepresentation();
        RtfInjectModifier im = new RtfInjectModifier();

        im.put("ed_form_Genetive", UtilPrintSupport.getDevelopFormGen(student.getEducationOrgUnit().getDevelopForm()).toLowerCase());
        im.put("Spec_gr_Generive", representation.getGrantView().getGenitive());
        im.put("st_date", UtilPrintSupport.getDateFormatterWithMonthString(representation.getDateBeginingPayment()));
        im.put("end_date", representation.getDateEndOfPayment() != null ? UtilPrintSupport.getDateFormatterWithMonthString(representation.getDateEndOfPayment()) : new RtfString().append(""));
        im.put("pay", student.getCompensationType().getCode().equals(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET) ? "за счет средств федерального бюджета" : student.getCompensationType().getShortTitle());
        im.modify(docExtract);
    }

    @Override
    public Map<Student, OrderParagraphInfo> buildBodyRepresent(List<ListRepresent> representationBase, RtfDocument document) {

        ListPrintDoc listPrintDoc = new ListPrintDoc();
        RtfDocument rtfDocument = listPrintDoc.creationPrintDocOrder(representationBase);

        document.getElementList().addAll(rtfDocument.getElementList());

        return ((IListPrintDoc) listPrintDoc).getParagInfomap();
    }


    @Override
    public boolean doRollback(ListRepresent represent, ErrorCollector error) {
        ListRepresentSupport listRepresentGrant = (ListRepresentSupport) represent;

        List<ListOrdListRepresent> orders = DataAccessServices.dao().getList(ListOrdListRepresent.class, ListOrdListRepresent.representation(), represent);
        ListOrder order = orders.get(0).getOrder();

        try {
            MoveStudentDaoFacade.getGrantDAO().rollbackStudentGrants(listRepresentGrant, order);
        }
        catch (Exception ex) {
            //TODO: а что тут надо делать?
            ex.printStackTrace();
            return false;
        }

        return true;
    }

    @Override
    public void update(ListRepresent listRepresent, List<Student> studentSelectedList) {
    }

    @Override
    public boolean doCommit(ListRepresent represent, ErrorCollector error) {
        ListRepresentSupport listRepresentGrant = (ListRepresentSupport) represent;


        List<ListOrdListRepresent> orders = DataAccessServices.dao().getList(ListOrdListRepresent.class, ListOrdListRepresent.representation(), represent);
        ListOrder order = orders.get(0).getOrder();

        try {
            MoveStudentDaoFacade.getGrantDAO().commitStudentGrants(listRepresentGrant, order);
        }
        catch (Exception ex) {
            //TODO: а что тут надо делать?
            ex.printStackTrace();
            return false;
        }
        return true;
    }


    @Override
    public List<Student> selectRepresent(ListRepresent listRepresent) {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(RelListRepresentStudents.class, "rls");

        builder.where(DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("rls")), DQLExpressions.value(listRepresent)));

        builder.addColumn(DQLExpressions.property(RelListRepresentStudents.student().fromAlias("rls")));

        return builder.createStatement(getSession()).list();
    }
}
