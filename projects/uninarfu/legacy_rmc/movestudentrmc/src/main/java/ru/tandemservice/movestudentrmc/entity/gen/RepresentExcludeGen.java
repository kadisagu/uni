package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.catalog.entity.EduInstitution;
import ru.tandemservice.movestudentrmc.entity.RepresentExclude;
import ru.tandemservice.movestudentrmc.entity.Representation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Об отчислении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RepresentExcludeGen extends Representation
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.RepresentExclude";
    public static final String ENTITY_NAME = "representExclude";
    public static final int VERSION_HASH = 1650778335;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_INSTITUTION = "eduInstitution";

    private EduInstitution _eduInstitution;     // ВУЗ, в который переводится студент

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ВУЗ, в который переводится студент.
     */
    public EduInstitution getEduInstitution()
    {
        return _eduInstitution;
    }

    /**
     * @param eduInstitution ВУЗ, в который переводится студент.
     */
    public void setEduInstitution(EduInstitution eduInstitution)
    {
        dirty(_eduInstitution, eduInstitution);
        _eduInstitution = eduInstitution;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof RepresentExcludeGen)
        {
            setEduInstitution(((RepresentExclude)another).getEduInstitution());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RepresentExcludeGen> extends Representation.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RepresentExclude.class;
        }

        public T newInstance()
        {
            return (T) new RepresentExclude();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "eduInstitution":
                    return obj.getEduInstitution();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "eduInstitution":
                    obj.setEduInstitution((EduInstitution) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "eduInstitution":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "eduInstitution":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "eduInstitution":
                    return EduInstitution.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RepresentExclude> _dslPath = new Path<RepresentExclude>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RepresentExclude");
    }
            

    /**
     * @return ВУЗ, в который переводится студент.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentExclude#getEduInstitution()
     */
    public static EduInstitution.Path<EduInstitution> eduInstitution()
    {
        return _dslPath.eduInstitution();
    }

    public static class Path<E extends RepresentExclude> extends Representation.Path<E>
    {
        private EduInstitution.Path<EduInstitution> _eduInstitution;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ВУЗ, в который переводится студент.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentExclude#getEduInstitution()
     */
        public EduInstitution.Path<EduInstitution> eduInstitution()
        {
            if(_eduInstitution == null )
                _eduInstitution = new EduInstitution.Path<EduInstitution>(L_EDU_INSTITUTION, this);
            return _eduInstitution;
        }

        public Class getEntityClass()
        {
            return RepresentExclude.class;
        }

        public String getEntityName()
        {
            return "representExclude";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
