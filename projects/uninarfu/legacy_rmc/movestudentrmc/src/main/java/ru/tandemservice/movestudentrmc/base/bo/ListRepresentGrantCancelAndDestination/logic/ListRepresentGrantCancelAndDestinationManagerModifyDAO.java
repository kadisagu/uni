package ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantCancelAndDestination.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.AbstractListRepresentDAO;
import ru.tandemservice.movestudentrmc.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.StuGrantStatus;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentGrantTypeCodes;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.StuGrantStatusCodes;
import ru.tandemservice.movestudentrmc.entity.gen.IRepresentOrderGen;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class ListRepresentGrantCancelAndDestinationManagerModifyDAO extends AbstractListRepresentDAO implements IListRepresentGrantCancelAndDestinationManagerModifyDAO {

    @Override
    public boolean doCommit(ListRepresent represent, ErrorCollector error) {

        if (!super.doCommit(represent, error))
            return false;

        ListRepresentGrantCancelAndDestination listRepresent = (ListRepresentGrantCancelAndDestination) represent;
        ListOrder order = getOrder(listRepresent);

        StuGrantStatus status = UniDaoFacade.getCoreDao().get(StuGrantStatus.class, StuGrantStatus.code(), StuGrantStatusCodes.CODE_3);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RelRepresentGrants.class, "sge")
                .where(DQLExpressions.eqValue(DQLExpressions.property(RelRepresentGrants.representation().fromAlias("sge")), listRepresent))
                .column(DQLExpressions.property(RelRepresentGrants.studentGrantEntity().fromAlias("sge")));

        List<StudentGrantEntity> sgelist = IUniBaseDao.instance.get().getList(builder);

        for (StudentGrantEntity sge : sgelist) {
            //сохраняем данные этой стипендии
            HistoryItem item = new HistoryItem(listRepresent, sge.getId());
            item.addProperty("orderDate", sge.getOrderDate() != null ? "" + sge.getOrderDate().getTime() : null);
            item.addProperty("orderNumber", sge.getOrderNumber());

            List<ListRepresentHistoryItem> histryItemList = item.create();
            for (ListRepresentHistoryItem historyItem : histryItemList)
                getSession().saveOrUpdate(historyItem);

            sge.setOrderDate(order.getCommitDate());
            sge.setOrderNumber(order.getNumber());
            sge.setStatus(status);
            getSession().saveOrUpdate(sge);
        }

        MoveStudentDaoFacade.getGrantDAO().saveGrantHistory(sgelist, listRepresent, RepresentGrantTypeCodes.CANCEL);

        MoveStudentDaoFacade.getGrantDAO().commitStudentGrants(listRepresent, order);

        return true;
    }

    @Override
    public boolean doRollback(ListRepresent represent, ErrorCollector error) {

        if (!super.doRollback(represent, error))
            return false;

        ListOrder order = getOrder(represent);

        StuGrantStatus status = UniDaoFacade.getCoreDao().get(StuGrantStatus.class, StuGrantStatus.code(), StuGrantStatusCodes.CODE_1);

        ListRepresentGrantCancelAndDestination listRepresent = (ListRepresentGrantCancelAndDestination) represent;
        Map<Long, HistoryItem> historyItemMap = getHistoryMap(listRepresent);

        List<StudentGrantEntity> sgeList = UniDaoFacade.getCoreDao().getList(StudentGrantEntity.class, CommonBaseEntityUtil.getIdList(historyItemMap.values()));
        for (StudentGrantEntity sge : sgeList) {
            HistoryItem historyItem = historyItemMap.get(sge.getId());

            Date orderDate = historyItem.getPropertyValue("orderDate") != null ? new Date(Long.parseLong(historyItem.getPropertyValue("orderDate"))) : null;
            String orderNumber = historyItem.getPropertyValue("orderNumber");

            sge.setOrderDate(orderDate);
            sge.setOrderNumber(orderNumber);
            sge.setStatus(status);

            getSession().saveOrUpdate(sge);
        }

        deleteHistory(listRepresent);

        MoveStudentDaoFacade.getGrantDAO().deleteGrantHistory(listRepresent);

        MoveStudentDaoFacade.getGrantDAO().rollbackStudentGrants(listRepresent, order);

        return true;
    }

    @Override
    public Map<Student, OrderParagraphInfo> buildBodyRepresent(
            List<ListRepresent> representationBase, RtfDocument document)
    {
        ListPrintDoc listPrintDoc = new ListPrintDoc();

        RtfDocument templateRepresent = listPrintDoc.creationPrintDocOrder(representationBase);

        document.setHeader(templateRepresent.getHeader());
        document.setSettings(templateRepresent.getSettings());
        document.getElementList().addAll(templateRepresent.getElementList());

        return listPrintDoc.getParagInfomap();
    }

    @Override
    public void buildBodyExtract(ListOrdListRepresent listOrdListRepresent,
                                 Student student, RtfDocument docCommon,
                                 Map<Student, OrderParagraphInfo> map)
    {
        ListRepresentGrantCancelAndDestination represent = (ListRepresentGrantCancelAndDestination) listOrdListRepresent.getRepresentation();
        new RtfInjectModifier().put("representTitle", "О назначениии " + represent.getGrant().getGenitive()).modify(docCommon);

        super.buildBodyExtract(listOrdListRepresent, student, docCommon, map);

        ListOrder order = listOrdListRepresent.getOrder();

        RtfInjectModifier im = new RtfInjectModifier();
        RtfTableModifier tm = new RtfTableModifier();

        int studentNumder = 0;

        if (map != null && map.containsKey(student)) {
            OrderParagraphInfo orderParagraphInfo = map.get(student);
            im.put("k", (orderParagraphInfo.getParagraphNumber() + 1) + "");
            im.put("addonNumber", orderParagraphInfo.getAddonNumber() + "");
            studentNumder = orderParagraphInfo.getStudentNumber();
        }
        else {
            im.put("k", "0");
            im.put("addonNumber", "0");
        }

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocRepresentOrderCancel.class, "o")
                .column("o")
                .where(DQLExpressions.eqValue(DQLExpressions.property(DocRepresentOrderCancel.representation().id().fromAlias("o")), represent.getId()))
                .joinEntity("o", DQLJoinType.inner, IRepresentOrder.class, "rel",
                            DQLExpressions.eq(DQLExpressions.property(DocRepresentOrderCancel.order().id().fromAlias("o")),
                                              DQLExpressions.property(IRepresentOrderGen.order().id().fromAlias("rel"))))
                .joinEntity("o", DQLJoinType.inner, StudentGrantEntityHistory.class, "h",
                            DQLExpressions.eq(
                                    DQLExpressions.property(StudentGrantEntityHistory.representation().id().fromAlias("h")),
                                    DQLExpressions.property(IRepresentOrderGen.representation().id().fromAlias("rel"))))
                .where(DQLExpressions.eqValue(DQLExpressions.property(StudentGrantEntityHistory.studentGrantEntity().student().id().fromAlias("h")), student.getId()))
                .distinct();

        List<DocRepresentOrderCancel> orders = UniDaoFacade.getCoreDao().getList(builder);

        im.put("orders", ListPrintDoc.getOrderStr(orders));

        im.put("grantView", StringUtils.capitalize(represent.getGrantView().getTitle()));
        im.put("orderDate", order != null ? UtilPrintSupport.getDateFormatterWithMonthStringAndYear(order.getCommitDate()) : new RtfString().append(""));
        im.put("orderNumber", order != null ? order.getNumber() : "");
        ListPrintDoc.injectModifier(im, represent, student);

        List<String[]> tableList = new ArrayList<>();
        String[] row = new String[5];

        row[0] = "" + studentNumder;
        row[1] = student.getBookNumber() != null ? student.getBookNumber() : "";
        row[2] = student.getPerson().getFullFio();
        row[3] = student.getCourse().getTitle();
        row[4] = student.getEducationOrgUnit().getFormativeOrgUnit().getTitle();

        tableList.add(row);

        tm.put("T", tableList.toArray(new String[][]{}));
        tm.modify(docCommon);
        im.modify(docCommon);
    }

    @Override
    public List<Student> selectRepresent(ListRepresent listRepresent) {
        return null;
    }

    @Override
    public void saveSupport(ListRepresent listRepresent,
                            List<DataWrapper> studentSelectedList,
                            DocListRepresentBasics representBasics)
    {
    }

    @Override
    public void saveStudentGrantEntity(List<StudentGrantEntity> sgeList,
                                       ListRepresentGrantCancelAndDestination represent)
    {
        for (StudentGrantEntity sge : sgeList) {
            RelRepresentGrants rel = new RelRepresentGrants();
            rel.setRepresentation(represent);
            rel.setStudentGrantEntity(sge);

            getSession().saveOrUpdate(rel);
        }

    }

    @Override
    public void deleteStudentGrantEntity(Long representId) {
        new DQLDeleteBuilder(RelRepresentGrants.class)
                .where(DQLExpressions.eqValue(DQLExpressions.property(RelRepresentGrants.representation().id()), representId))
                .createStatement(getSession()).execute();
    }

    @Override
    public void saveOrderCancel(List<DocRepresentOrderCancel> ordersList) {
        for (DocRepresentOrderCancel rel : ordersList) {
            getSession().saveOrUpdate(rel);
        }
    }

    @Override
    public void deleteOrderCancel(Long representId) {
        new DQLDeleteBuilder(DocRepresentOrderCancel.class)
                .where(DQLExpressions.eqValue(DQLExpressions.property(DocRepresentOrderCancel.representation()), representId))
                .createStatement(getSession()).execute();
    }

}
