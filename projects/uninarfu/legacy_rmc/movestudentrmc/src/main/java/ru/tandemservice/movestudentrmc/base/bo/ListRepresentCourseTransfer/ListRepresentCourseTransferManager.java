package ru.tandemservice.movestudentrmc.base.bo.ListRepresentCourseTransfer;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentCourseTransfer.logic.ListRepresentCourseTransferManagerModifyDAO;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentCourseTransfer.ui.Edit.ListRepresentCourseTransferEdit;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentCourseTransfer.ui.View.ListRepresentCourseTransferView;
import ru.tandemservice.movestudentrmc.docord.IListObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IListObjectModifyDAO;


@Configuration
public class ListRepresentCourseTransferManager extends BusinessObjectManager implements IListObjectByRepresentTypeManager {

    public static ListRepresentCourseTransferManager instance()
    {
        return instance(ListRepresentCourseTransferManager.class);
    }


    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return ListRepresentCourseTransferEdit.class;
    }


    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return ListRepresentCourseTransferView.class;
    }


    @Bean
    @Override
    public IListObjectModifyDAO getListObjectModifyDAO() {
        return new ListRepresentCourseTransferManagerModifyDAO();
    }
}
