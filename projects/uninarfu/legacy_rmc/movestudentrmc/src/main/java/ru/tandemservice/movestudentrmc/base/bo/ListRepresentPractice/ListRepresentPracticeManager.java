package ru.tandemservice.movestudentrmc.base.bo.ListRepresentPractice;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentPractice.logic.ListRepresentPracticeManagerModifyDAO;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentPractice.ui.Edit.ListRepresentPracticeEdit;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentPractice.ui.View.ListRepresentPracticeView;
import ru.tandemservice.movestudentrmc.docord.IListObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IListObjectModifyDAO;


@Configuration
public class ListRepresentPracticeManager extends BusinessObjectManager implements IListObjectByRepresentTypeManager {

    public static ListRepresentPracticeManager instance()
    {
        return instance(ListRepresentPracticeManager.class);
    }


    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return ListRepresentPracticeEdit.class;
    }


    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return ListRepresentPracticeView.class;
    }


    @Bean
    @Override
    public IListObjectModifyDAO getListObjectModifyDAO() {
        return new ListRepresentPracticeManagerModifyDAO();
    }
}
