package ru.tandemservice.movestudentrmc.base.bo.DORepresentEnrollmentTransfer.ui.View;

import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentViewUI;
import ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

public class DORepresentEnrollmentTransferViewUI extends AbstractDORepresentViewUI {

    public EducationOrgUnit getEducationOrgUnit() {
        RepresentEnrollmentTransfer rep = (RepresentEnrollmentTransfer) this._represent;
        if (rep.getEducationOrgUnit() != null)
            return rep.getEducationOrgUnit();
        else
            return rep.getNewGroup().getEducationOrgUnit();
    }
}
