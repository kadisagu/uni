package ru.tandemservice.movestudentrmc.base.bo.ListRepresentProfileTransfer.logic;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.AbstractListRepresentDAO;
import ru.tandemservice.movestudentrmc.docord.util.MoveStudentUtil;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class ListRepresentProfileTransferManagerModifyDAO extends AbstractListRepresentDAO
{

    @Override
    public void save(ListRepresent listRepresent, List<Student> studentSelectedList, DocListRepresentBasics representBasics) {
        if (listRepresent.getTitle() == null)
            listRepresent.setTitle(listRepresent.getRepresentationType().getTitle());
        baseCreate(listRepresent);

        baseCreateOrUpdate(representBasics);

        ListRepresentProfileTransfer profileTransfer = (ListRepresentProfileTransfer) listRepresent;

        for (Student student : studentSelectedList) {

            RelListRepresentStudentsOldData relListRepresentProfileTransferStudents = new RelListRepresentStudentsOldData();
            relListRepresentProfileTransferStudents.setRepresentation(profileTransfer);
            relListRepresentProfileTransferStudents.setStudent(student);

            relListRepresentProfileTransferStudents.setOldEducationOrgUnit(student.getEducationOrgUnit());
            //relListRepresentProfileTransferStudents.setOldEducationLevelsHighSchool(student.getEducationOrgUnit().getEducationLevelHighSchool());
            relListRepresentProfileTransferStudents.setOldGroup(student.getGroup());
            baseCreateOrUpdate(relListRepresentProfileTransferStudents);

        }


    }

    @Override
    public void update(ListRepresent listRepresent, List<Student> studentSelectedList) {

        baseCreateOrUpdate(listRepresent);
        ListRepresentProfileTransfer profileTransfer = (ListRepresentProfileTransfer) listRepresent;
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(RelListRepresentStudents.class, "r").addColumn("r");
        builder.where(DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("r")), DQLExpressions.value(listRepresent)));
        List<RelListRepresentStudents> representSelectedOldList = getStudentList(listRepresent);

        for (RelListRepresentStudents representSelectedOld : representSelectedOldList) {
            Boolean isFind = false;
            for (Student studentSelected : studentSelectedList) {
                if (representSelectedOld.getStudent().equals(studentSelected)) {
                    isFind = true;
                    studentSelectedList.remove(studentSelected);
                    break;
                }
            }
            if (!isFind) {
                ListRepresentProfileTransfer represent = (ListRepresentProfileTransfer) representSelectedOld.getRepresentation();
                //represent.setState(DataAccessServices.dao().get(MovestudentExtractStates.class, MovestudentExtractStates.code().s(),"3"));
                baseCreateOrUpdate(represent);
                baseDelete(representSelectedOld);
            }
        }

        for (Student student : studentSelectedList) {

            RelListRepresentStudentsOldData relListRepresentProfileTransferStudents = new RelListRepresentStudentsOldData();
            relListRepresentProfileTransferStudents.setRepresentation(profileTransfer);
            relListRepresentProfileTransferStudents.setStudent(student);

            relListRepresentProfileTransferStudents.setOldEducationOrgUnit(student.getEducationOrgUnit());
            //relListRepresentProfileTransferStudents.setOldEducationLevelsHighSchool(student.getEducationOrgUnit().getEducationLevelHighSchool());
            relListRepresentProfileTransferStudents.setOldGroup(student.getGroup());
            baseCreateOrUpdate(relListRepresentProfileTransferStudents);

        }

    }


    @Override
    public void buildBodyExtract(ListOrdListRepresent listOrdListRepresent,
                                 Student student, RtfDocument docExtract, Map<Student, OrderParagraphInfo> map)
    {
        super.buildBodyExtract(listOrdListRepresent, student, docExtract, map);

        ListRepresentProfileTransfer listRepresent = (ListRepresentProfileTransfer) listOrdListRepresent.getRepresentation();
        new RtfInjectModifier()
                .put("represent_CreateDate", UtilPrintSupport.getDateFormatterWithMonthString(listRepresent.getCreateDate()))
                .put("represent_StartDate", UtilPrintSupport.getDateFormatterWithMonthString(listRepresent.getDateBeginingTransfer()))
                .put("representEduLevelFullTitle", UtilPrintSupport.getHighLevelSchoolTypeString(listRepresent.getNewEducationOrgUnit().getEducationLevelHighSchool()))
                .put("newGroup", listRepresent.getNewGroup().getTitle())
                .put("k", getStudentNumber(Arrays.asList(new ListRepresent[]{listRepresent})).get(student).toString())
                .modify(docExtract);

    }

    @Override
    public Map<Student, OrderParagraphInfo> buildBodyRepresent(List<ListRepresent> representationBase, RtfDocument document) {
        ListPrintDoc listPrintDoc = new ListPrintDoc();
        RtfDocument templateRepresent = listPrintDoc.creationPrintDocOrder(representationBase);
        document.getElementList().addAll(templateRepresent.getElementList());
        return listPrintDoc.getParagInfomap();
    }

    public Map<Student, Integer> getStudentNumber(List<ListRepresent> representationBase) {
        ListPrintDoc listPrintDoc = new ListPrintDoc();
        RtfDocument templateRepresent = listPrintDoc.creationPrintDocOrder(representationBase);

        return listPrintDoc.getStudentNumberMap();
    }


    @Override
    public boolean doRollback(ListRepresent represent, ErrorCollector error) {

        List<RelListRepresentStudents> representList = getStudentList(represent);

        for (RelListRepresentStudents representStudents : representList) {
            RelListRepresentStudentsOldData oldData = (RelListRepresentStudentsOldData) representStudents;
            Student student = oldData.getStudent();
            student.setGroup(oldData.getOldGroup());
            student.setEducationOrgUnit(oldData.getOldEducationOrgUnit());
            getSession().saveOrUpdate(student);
        }

        rollbackStudentGrantsCancel(represent);
        return true;
    }

    @Override
    public boolean doCommit(ListRepresent represent, ErrorCollector error) {

        ListRepresentProfileTransfer profileTransfer = (ListRepresentProfileTransfer) represent;

        List<RelListRepresentStudents> representList = getStudentList(represent);

        List<Student> students = CommonBaseEntityUtil.getPropertiesList(representList, RelListRepresentStudents.student().s());

        List<Student> lst = MoveStudentUtil.checkStudentList(students, null, profileTransfer.getNewEducationOrgUnit().getDevelopForm());

        if (!lst.isEmpty()) {
            List<StudentGrantEntity> sgeList = MoveStudentUtil.getSrudentGrantEntityList(lst, profileTransfer.getDateBeginingTransfer());
            commitStudentGrantsCancel(profileTransfer, sgeList);
        }

        for (RelListRepresentStudents representStudents : representList) {
            Student student = representStudents.getStudent();
            student.setGroup(profileTransfer.getNewGroup());
            student.setEducationOrgUnit(profileTransfer.getNewEducationOrgUnit());
            //student.getEducationOrgUnit().setEducationLevelHighSchool(profileTransfer.getNewEducationLevelsHighSchool());
            getSession().saveOrUpdate(student);
        }

        return true;

    }


    @Override
    public List<Student> selectRepresent(ListRepresent listRepresent) {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(RelListRepresentStudents.class, "rls");

        builder.where(DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("rls")), DQLExpressions.value(listRepresent)));

        builder.column(DQLExpressions.property(RelListRepresentStudents.student().fromAlias("rls")));

        return builder.createStatement(getSession()).list();
    }

    public void saveSupport(ListRepresent listRepresent, List<DataWrapper> studentSelectedList, DocListRepresentBasics representBasics) {
    }
}
