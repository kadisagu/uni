package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonBasic;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationBasement;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class BasementRepresentDSHandler extends DefaultComboDataSourceHandler {

    public BasementRepresentDSHandler(String ownerId) {
        super(ownerId, RepresentationBasement.class);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {


/*		String filter = ep.input.getComboFilterByValue();
        if (StringUtils.isNotBlank(filter))
		{
			ep.dqlBuilder.where(like(DQLFunctions.upper(property(RepresentationType.title().fromAlias("e"))), value(CoreStringUtils.escapeLike(filter, true))));
		}*/
        RepresentationType typeRepresentFilter = ep.context.get("typeRepresentFilter");
        RepresentationReason reasonRepresentFilter = ep.context.get("reasonRepresentFilter");

        ep.dqlBuilder.joinEntity("e", DQLJoinType.inner, RelRepresentationReasonBasic.class, "r",
                                 eq(property(RelRepresentationReasonBasic.basic().id().fromAlias("r")),
                                    DQLExpressions.property(RepresentationBasement.id().fromAlias("e"))));

        ep.dqlBuilder.where(eq(property(RelRepresentationReasonBasic.reason().fromAlias("r")), value(reasonRepresentFilter)));
        ep.dqlBuilder.where(eq(property(RelRepresentationReasonBasic.type().fromAlias("r")), value(typeRepresentFilter)));
    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
    {
        //ep.dqlBuilder.order("e." + RepresentationType.title());
    }
}
