package ru.tandemservice.movestudentrmc.base.bo.DORepresentExclude.ui.Edit;

import org.apache.axis.utils.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.person.catalog.entity.EduInstitution;
import org.tandemframework.shared.person.catalog.entity.codes.EducationalInstitutionTypeKindCodes;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentEditUI;
import ru.tandemservice.movestudentrmc.docord.util.MoveStudentUtil;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.NarfuDocument;
import ru.tandemservice.movestudentrmc.entity.RepresentExclude;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.util.ListenerWrapper;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.StudentStatus;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DORepresentExcludeEditUI extends AbstractDORepresentEditUI implements ListenerWrapper.IListener<NarfuDocument>
{

    private ISelectModel eduInstitutionModel;
    private boolean showEduInstitution = false;

    @Override
    public void onComponentRefresh() {
        super.onComponentRefresh();

        this.setEduInstitutionModel(new BaseSingleSelectModel(EduInstitution.title().s()) {

            @Override
            public ListResult<EduInstitution> findValues(String filter) {

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EduInstitution.class, "e")
                        .where(DQLExpressions.isNotNull(DQLExpressions.property(EduInstitution.eduInstitutionKind().parent().fromAlias("e"))))
                        .where(DQLExpressions.in(DQLExpressions.property(EduInstitution.eduInstitutionKind().parent().code().fromAlias("e")), Arrays.asList(EducationalInstitutionTypeKindCodes.SCHOOL_PRO_HIGH, EducationalInstitutionTypeKindCodes.SCHOOL_ARMY_PRO_HIGH)));

                if (filter != null)
                    builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(EduInstitution.title().fromAlias("e"))), DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));

                List<EduInstitution> lst = IUniBaseDao.instance.get().getList(builder);

                return new ListResult<>(lst);
            }

            @Override
            public Object getValue(Object primaryKey) {
                return IUniBaseDao.instance.get().get((Long) primaryKey);
            }
        });

    }

    @Override
    public void onChangeReason() {
        super.onChangeReason();
        if (this.getRepresentObj().getReason().getCode().equals("narfu-1.3"))
            setShowEduInstitution(true);
        else {
            setShowEduInstitution(false);
            ((RepresentExclude) this.getRepresentObj()).setEduInstitution(null);
        }

    }

    @Override
    public void onComponentBindReturnParameters(String childRegionName,
                                                Map<String, Object> returnedData)
    {
        if (returnedData != null && returnedData.get("createdEduInstitutionId") != null) {
            Long id = (Long) returnedData.get("createdEduInstitutionId");
            ((RepresentExclude) this.getRepresentObj()).setEduInstitution((EduInstitution) IUniBaseDao.instance.get().get(id));
        }
    }

    public void onClickAddEduInstitution() {
        Map<String, Object> params = new HashMap<>();
        params.put("catalogCode", "eduInstitution");
        params.put("existanceCheckingType", "returnExisting");
        params.put("personRoleName", "Student");

        this.getConfig().getBusinessComponent().createDefaultChildRegion(new ComponentActivator(CommonManager.catalogComponentProvider().getAddEditComponent(EduInstitution.class), params));
    }

    @Override
    protected void initDocument() {
        super.initDocument();
        _studentStatusNewStr = ((StudentStatus) UniDaoFacade.getCoreDao().getCatalogItem(ru.tandemservice.uni.entity.catalog.StudentStatus.class, "8")).getTitle();
        if (this.getRepresentId() != null && this.getRepresentObj().getReason().getCode().equals("narfu-1.3"))
            setShowEduInstitution(true);
        else
            setShowEduInstitution(false);
    }

    @Override
    protected void FetchStudents(Representation doc) {

        _studentDocList.clear();
        _studentList.clear();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocRepresentStudentBase.class, "stud").column("stud");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(DocRepresentStudentBase.representation().fromAlias("stud")),
                DQLExpressions.value(doc.getId())));

        List<DocRepresentStudentBase> studentList = builder.createStatement(getSupport().getSession()).list();

        for (DocRepresentStudentBase student : studentList) {

            _studentDocList.add(student);
            _studentList.add(student.getStudent());
        }

        _student = _studentList.get(0);
    }

    @Override
    public void onClickSave() throws IOException {

        boolean ok = MoveStudentUtil.isChecked(getStudentList(), null, null);

        if ((ok && !isSave()) || (ok && isSave() && isWarning())) {
            String mess = MoveStudentUtil.checked(getRepresentObj().getStartDate(), true);
            if (!StringUtils.isEmpty(mess)) {
                setSave(true);
                setWarning(true);
                setWarningMessage(mess);
                return;
            }
        }

        super.onClickSave();
    }

    public ISelectModel getEduInstitutionModel() {
        return eduInstitutionModel;
    }

    public void setEduInstitutionModel(ISelectModel eduInstitutionModel) {
        this.eduInstitutionModel = eduInstitutionModel;
    }

    public boolean isShowEduInstitution() {
        return showEduInstitution;
    }

    public void setShowEduInstitution(boolean showEduInstitution) {
        this.showEduInstitution = showEduInstitution;
    }

}
