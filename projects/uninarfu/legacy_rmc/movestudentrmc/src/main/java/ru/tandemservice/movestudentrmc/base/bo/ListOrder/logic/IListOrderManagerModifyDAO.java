package ru.tandemservice.movestudentrmc.base.bo.ListOrder.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.movestudentrmc.entity.ListOrder;

import java.util.List;

public interface IListOrderManagerModifyDAO extends INeedPersistenceSupport {

    void save(ListOrder order, List<DataWrapper> representSelectedList);

    void update(ListOrder order, List<DataWrapper> representSelectedList);

    List<DataWrapper> selectRepresent(ListOrder order);

    void updateState(ListOrder order);

    void doCommit(ListOrder order, ErrorCollector error);

    void doRollback(ListOrder order, ErrorCollector error);

    void saveDocOrder(ListOrder order, RtfDocument docMain);

    public void saveExtractsText(ListOrder order);

    public void deleteExtractsText(ListOrder order);

}
