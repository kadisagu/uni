package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.OrgUnitTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.List.DORepresentBaseList;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.ListCommit.DORepresentBaseListCommit;

@Configuration
public class DORepresentBaseOrgUnitTab extends BusinessComponentManager {

    //Панель вкладок
    public static final String TAB_PANEL = "tabPanel";

    //Вкладки
    public static final String REPRESENT_BASE_TAB = "representBaseTab";
    public static final String REPRESENT_BASE_COMMIT_TAB = "representBaseCommitTab";

    @Bean
    public TabPanelExtPoint tabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(TAB_PANEL)
                .addTab(componentTab(REPRESENT_BASE_TAB, DORepresentBaseList.class).parameters("ui:paramsRepresentBaseTab").permissionKey("data:viewRepresent"))
                .addTab(componentTab(REPRESENT_BASE_COMMIT_TAB, DORepresentBaseListCommit.class).parameters("ui:paramsRepresentBaseTab").permissionKey("data:viewRepresentCommit"))
                .create();
    }

}
