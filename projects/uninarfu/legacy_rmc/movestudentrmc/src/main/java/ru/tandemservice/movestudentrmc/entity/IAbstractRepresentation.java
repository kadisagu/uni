package ru.tandemservice.movestudentrmc.entity;

import ru.tandemservice.movestudentrmc.entity.catalog.MovestudentExtractStates;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;

public interface IAbstractRepresentation {

    public Long getRepresentID();

    public MovestudentExtractStates getState();

    public RepresentationType getType();

}
