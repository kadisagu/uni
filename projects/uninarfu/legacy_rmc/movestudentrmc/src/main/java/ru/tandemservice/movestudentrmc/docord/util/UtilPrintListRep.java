package ru.tandemservice.movestudentrmc.docord.util;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.movestudentrmc.entity.ListOrdListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListOrder;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;

import java.util.List;


public class UtilPrintListRep {


    public static void injectModifierRepresent(RtfInjectModifier modifier, ListRepresent listRepresent) {

        ListOrder listOrder = getOrderByRepresent(listRepresent);

        if (listOrder != null) {
            modifier.put("date_or", listOrder.getCommitDate() != null ? UtilPrintSupport.getDateFormatterWithMonthString(listOrder.getCommitDate()) : UtilPrintSupport.getDateFormatterWithMonthString(listOrder.getCreateDate()));
            modifier.put("num_or", listOrder.getNumber());

        }
        else {
            modifier.put("date_or", "");
            modifier.put("num_or", "");
            modifier.put("footer", "");
        }
        //--------------------------------------------------------------------------------------------------------------------------------------
        modifier.put("signPostGenitive", listOrder != null ? listOrder.getSignPostGenitive() : "");
        //--------------------------------------------------------------------------------------------------------------------------------------

    }

    public static ListOrder getOrderByRepresent(ListRepresent listRepresent) {
        List<ListOrdListRepresent> listOrderRel = DataAccessServices.dao().getList(ListOrdListRepresent.class, ListOrdListRepresent.representation(), listRepresent);
        ListOrder listOrder = null;
        if (!listOrderRel.isEmpty())
            listOrder = listOrderRel.get(0).getOrder();
        return listOrder;
    }

}
