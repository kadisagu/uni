package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.dao.BaseModifyAggregateDAO;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudentrmc.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudentrmc.docord.IListObjectModifyDAO;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.StuGrantStatus;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentGrantTypeCodes;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.StuGrantStatusCodes;
import ru.tandemservice.movestudentrmc.util.CheckUtil;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.*;

@Transactional
public abstract class AbstractListRepresentDAO extends BaseModifyAggregateDAO<ListRepresent> implements IListObjectModifyDAO
{

    @Override
    @Deprecated
    public void update(ListRepresent listRepresent, List<Student> studentSelectedList)
    {
        DocListRepresentBasics representBasics = UniDaoFacade.getCoreDao().get(DocListRepresentBasics.class, DocListRepresentBasics.listRepresent(), listRepresent);
        this.save(listRepresent, studentSelectedList, representBasics);
    }

    @Override
    public void save(ListRepresent listRepresent, List<Student> studentSelectedList, DocListRepresentBasics representBasics)
    {
        /*
        List<String> badList = checkExistingRepresents(listRepresent, studentSelectedList);
		if (!badList.isEmpty()) {
			StringBuilder sb = new StringBuilder()
				.append("Для студента(ов) ")
				.append(StringUtils.join(badList, ", "))
				.append(" уже есть сформированные представления");
			throw new ApplicationException(sb.toString());
		}
		 */
        //сохранение
        this.baseCreateOrUpdate(listRepresent);
        this.baseCreateOrUpdate(representBasics);

        //удалим старых студиков
        List<RelListRepresentStudents> list = new DQLSelectBuilder().fromEntity(RelListRepresentStudents.class, "e")
                .where(DQLExpressions.eq(
                        DQLExpressions.property(RelListRepresentStudents.representation().id().fromAlias("e")),
                        DQLExpressions.value(listRepresent.getId())
                ))
                .column("e")
                .createStatement(getSession())
                .list();

        for (RelListRepresentStudents rel : list)
            getSession().delete(rel);

        //сохраним новых студентов
        for (Student st : studentSelectedList) {
            RelListRepresentStudents rel = new RelListRepresentStudents();
            rel.setRepresentation(listRepresent);
            rel.setStudent(st);

            this.baseCreateOrUpdate(rel);
        }
    }

    @Override
    public boolean doCommit(ListRepresent represent, ErrorCollector error)
    {

        List<RelListRepresentStudents> studentList = getStudentList(represent);

        if (studentList.isEmpty()) {
            error.add("Представление содержит пустой список студентов!");
            return false;
        }
        return true;
    }

    @Override
    public boolean doRollback(ListRepresent represent, ErrorCollector error)
    {

        List<RelListRepresentStudents> studentList = getStudentList(represent);

        if (studentList.isEmpty()) {
            error.add("Представление содержит пустой список студентов!");
            return false;
        }
        return true;
    }

    @Override
    public void buildBodyExtract(ListOrdListRepresent listOrdListRepresent, Student student, RtfDocument docCommon,
                                 Map<Student, OrderParagraphInfo> map
    )
    {
        ListRepresent listRepresent = listOrdListRepresent.getRepresentation();

        ListOrder listOrder = listOrdListRepresent.getOrder();

        RtfInjectModifier im = new RtfInjectModifier();

        String baseTitle = StringUtils.join(UtilPrintSupport.getBasement(listRepresent), ", ");
        String representBasementTitle = UtilPrintSupport.getListRepresentBasicTitle(listRepresent);
        String formOrgUdativeCaseTitle = student.getEducationOrgUnit().getFormativeOrgUnit().getDativeCaseTitle();
        String formOrgUgenitiveCaseTitle = student.getEducationOrgUnit().getFormativeOrgUnit().getGenitiveCaseTitle();

        String developFormlowerCase = UtilPrintSupport.getDevelopFormGen(student.getEducationOrgUnit().getDevelopForm());
        String compType = student.getCompensationType().getCode().equals(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET) ? "за счет средств федерального бюджета" : student.getCompensationType().getShortTitle();
        String representReason = listRepresent.getRepresentationReason().getTitle();
        RtfString orderDate = listOrder != null ? UtilPrintSupport.getDateFormatterWithMonthString(listOrder.getCommitDate()) : new RtfString().append("-");
        String OrderNumber = listOrder != null ? listOrder.getNumber() : "-";

        String course = student.getCourse().getTitle();
        im.put("studentTitle", student.getPerson().getFullFio());
        im.put("date_or", orderDate);
        im.put("num_or", OrderNumber);
        im.put("orderDate", orderDate);
        im.put("orderNumber", OrderNumber);

        im.put("base", baseTitle);
        im.put("representBasement", representBasementTitle);
        im.put("representReason", representReason);
        im.put("reason", representReason);
        im.put("representTitle", listRepresent.getRepresentationType().getTitle());
        im.put("formativeOrgUnit", formOrgUdativeCaseTitle);
        im.put("f_unit_Dative", StringUtils.uncapitalize(formOrgUdativeCaseTitle));
        im.put("ed_form_Genetive", developFormlowerCase.toLowerCase());
        im.put("form", developFormlowerCase.toLowerCase());
        im.put("developForm_G", developFormlowerCase);
        im.put("develop_form", developFormlowerCase);
        im.put("eduLevel", UtilPrintSupport.getHighLevelSchoolTypeString(student.getEducationOrgUnit().getEducationLevelHighSchool()));

        im.put("pay", compType);
        im.put("course_Num", course);
        im.put("course", course);

        im.put("formOU", formOrgUgenitiveCaseTitle);
        im.put("developForm", developFormlowerCase);
        im.put("compensationType", compType);

        if (map != null && map.containsKey(student)) {
            OrderParagraphInfo orderParagraphInfo = map.get(student);
            im.put("i", String.valueOf(orderParagraphInfo.getParagraphNumber()));
            im.put("j", String.valueOf(orderParagraphInfo.getExtractNumber()));
        } else {
            im.put("i", "0");
            im.put("j", "0");
        }

        im.modify(docCommon);
        UtilPrintSupport.injectVisaTable(docCommon, listOrder);
        //------------------------------------------------------------
        //UtilPrintSupport.injectExtractsExecutorTable(docCommon);
        //------------------------------------------------------------
    }

    @Override
    public void buildBodyExtract(ListOrdListRepresent listOrdListRepresent, Student student, RtfDocument docExtract)
    {
        ListRepresent listRepresent = listOrdListRepresent.getRepresentation();

        ListOrder order = listRepresent.getOrder();

        Map<Student, OrderParagraphInfo> infoParagMap = buildBodyRepresent(order.getRepresentationList(), RtfBean.getElementFactory().createRtfDocument());

        buildBodyExtract(listOrdListRepresent, student, docExtract, infoParagMap);
    }

    @Override
    public RtfDocument getTemplateExtract(ICatalogItem type)
    {
        RtfDocument template = UtilPrintSupport.getTemplate(type, "extract");
        if (template != null && !template.getElementList().isEmpty()) {

            //Получим общий шаблон выписки
            RtfDocument commonTemplateExtract = getCommonTemplateExtract();

            RtfSearchResult rtfMark = UniRtfUtil.findRtfMark(commonTemplateExtract, "paragraph");
            if (!rtfMark.isFound())
                throw new ApplicationException("В общем шаблоне выписки не найден элемент \"paragraph\"");
//			commonTemplateExtract.getElementList().remove(rtfMark.getIndex()-1);
            commonTemplateExtract.getElementList().addAll(rtfMark.getIndex() - 1, template.getElementList());
            SharedRtfUtil.removeParagraphsWithTagsRecursive(commonTemplateExtract, Collections.singletonList("paragraph"), false, true);
            return commonTemplateExtract;
        } else
            throw new ApplicationException("Для списочного представления \"" + type.getTitle() + " не найдено шаблона выписки");
    }

    private RtfDocument getCommonTemplateExtract()
    {
        return UtilPrintSupport.getTemplate("listExtractCommonTemplate", "common");
    }

    @Override
    public RtfDocument getTemplateRepresent(ICatalogItem type)
    {
        return UtilPrintSupport.getTemplate(type, "represent");
    }

    @Override
    public GrammaCase getGrammaCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    public List<RelListRepresentStudents> getStudentList(ListRepresent listRepresent)
    {
        return DataAccessServices.dao().getList(RelListRepresentStudents.class, RelListRepresentStudents.representation(), listRepresent);
    }

    public void checkSelectStudent(ListRepresent listRepresent, List<Student> selectList)
    {
        List<String> badList = CheckUtil.checkExistingRepresents(listRepresent, selectList);
        if (!badList.isEmpty()) {
            throw new ApplicationException("Для " + StringUtils.join(badList, ", ") +
                    " уже сфрмированы представления.Чтобы сформировать очередные представления необходимо провести по приказу ранее созданные.");
        }

    }

    //////////////////////////////////////////////////////////////////////////////////////
    protected Map<Long, HistoryItem> getHistoryMap(ListRepresent listRepresent)
    {
        Map<Long, HistoryItem> historyMap = new HashMap<>();

        Map<Long, Map<String, String>> valuesMap = new HashMap<>();
        List<ListRepresentHistoryItem> itemList = UniDaoFacade.getCoreDao().getList(ListRepresentHistoryItem.class, ListRepresentHistoryItem.listRepresent(), listRepresent);
        for (ListRepresentHistoryItem item : itemList) {
            Long number = item.getNumber();
            if (valuesMap.get(number) == null)
                valuesMap.put(number, new HashMap<String, String>());

            valuesMap.get(number).put(item.getFieldname(), item.getValue());
        }

        for (Map.Entry<Long, Map<String, String>> entry : valuesMap.entrySet()) {
            HistoryItem historyItem = new HistoryItem(listRepresent, entry.getKey(), entry.getValue());
            historyMap.put(historyItem.getId(), historyItem);
        }
        return historyMap;
    }

    protected void deleteHistory(ListRepresent listRepresent)
    {
        //DQLDeleteBuilder не проходит так как валится транзация
        List<ListRepresentHistoryItem> itemList = UniDaoFacade.getCoreDao().getList(ListRepresentHistoryItem.class, ListRepresentHistoryItem.listRepresent(), listRepresent);
        for (ListRepresentHistoryItem item : itemList)
            getSession().delete(item);
    }

    public void commitStudentGrantsCancel(ListRepresent represent, List<StudentGrantEntity> sgeList)
    {
        ListOrder order = represent.getOrder();
        StuGrantStatus status = IUniBaseDao.instance.get().getCatalogItem(StuGrantStatus.class, StuGrantStatusCodes.CODE_3);

        for (StudentGrantEntity sge : sgeList) {
            //сохраняем данные этой стипендии
            HistoryItem item = new HistoryItem(represent, sge.getId());
            //item.addProperty("id", sge.getId().toString());
            item.addProperty("orderDate", sge.getOrderDate() != null ? "" + sge.getOrderDate().getTime() : null);
            item.addProperty("orderNumber", sge.getOrderNumber());

            List<ListRepresentHistoryItem> histryItemList = item.create();
            for (ListRepresentHistoryItem historyItem : histryItemList)
                getSession().saveOrUpdate(historyItem);

            sge.setOrderDate(order.getCommitDate());
            sge.setOrderNumber(order.getNumber());
            sge.setStatus(status);
            getSession().saveOrUpdate(sge);
        }

        MoveStudentDaoFacade.getGrantDAO().saveGrantHistory(sgeList, represent, RepresentGrantTypeCodes.CANCEL);
    }

    public void rollbackStudentGrantsCancel(ListRepresent represent)
    {
        StuGrantStatus status = IUniBaseDao.instance.get().getCatalogItem(StuGrantStatus.class, StuGrantStatusCodes.CODE_1);

        Map<Long, HistoryItem> historyItemMap = getHistoryMap(represent);

        List<StudentGrantEntity> sgeList = IUniBaseDao.instance.get().getList(StudentGrantEntity.class, UniBaseUtils.getIdList(historyItemMap.values()));
        for (StudentGrantEntity sge : sgeList) {
            HistoryItem historyItem = historyItemMap.get(sge.getId());

            Date orderDate = historyItem.getPropertyValue("orderDate") != null ? new Date(Long.parseLong(historyItem.getPropertyValue("orderDate"))) : null;
            String orderNumber = historyItem.getPropertyValue("orderNumber");

            sge.setOrderDate(orderDate);
            sge.setOrderNumber(orderNumber);
            sge.setStatus(status);

            getSession().saveOrUpdate(sge);
        }

        MoveStudentDaoFacade.getGrantDAO().deleteGrantHistory(represent);
    }

    protected ListOrder getOrder(ListRepresent listRepresent)
    {
        List<ListOrdListRepresent> orderList = UniDaoFacade.getCoreDao().getList(ListOrdListRepresent.class, ListOrdListRepresent.representation(), listRepresent);

        if (!orderList.isEmpty())
            return orderList.get(0).getOrder();
        else
            return null;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static class HistoryItem implements IEntity
    {

        private ListRepresent listRepresent;
        private long number;
        private Map<String, String> properties = new HashMap<>();

        public HistoryItem(ListRepresent listRepresent, long number)
        {
            this.listRepresent = listRepresent;
            this.number = number;
        }

        public HistoryItem(ListRepresent listRepresent, long number, Map<String, String> properties)
        {
            this(listRepresent, number);
            this.properties = properties != null ? properties : new HashMap<String, String>();
        }

        public void addProperty(String propertyName, String propertyValue)
        {
            this.properties.put(propertyName, propertyValue);
        }

        public String getPropertyValue(String propertyName)
        {
            return this.properties.get(propertyName);
        }

        public List<ListRepresentHistoryItem> create()
        {
            List<ListRepresentHistoryItem> itemList = new ArrayList<>();

            for (Map.Entry<String, String> entry : this.properties.entrySet()) {
                ListRepresentHistoryItem item = new ListRepresentHistoryItem();
                item.setListRepresent(this.listRepresent);
                item.setNumber(this.number);

                item.setFieldname(entry.getKey());
                item.setValue(entry.getValue());

                itemList.add(item);
            }
            return itemList;
        }

        public ListRepresent getListRepresent()
        {
            return listRepresent;
        }

        public void setListRepresent(ListRepresent listRepresent)
        {
            this.listRepresent = listRepresent;
        }

        public long getNumber()
        {
            return number;
        }

        public void setNumber(long number)
        {
            this.number = number;
        }

        /////////////// IEntity ////////////////////
        @Override
        public Long getId()
        {
            return getNumber();
        }

        @Override
        public Object getProperty(Object key)
        {
            return this.properties.get(key.toString());
        }

        @Override
        public void setId(Long id)
        {
            setNumber(id);
        }

        @Override
        public void setProperty(String key, Object value)
        {
            this.properties.put(key, value != null ? value.toString() : null);
        }
    }

}
