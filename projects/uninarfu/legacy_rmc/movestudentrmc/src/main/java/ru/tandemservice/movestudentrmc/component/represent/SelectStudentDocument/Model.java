package ru.tandemservice.movestudentrmc.component.represent.SelectStudentDocument;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.State;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.movestudentrmc.entity.NarfuDocument;
import ru.tandemservice.movestudentrmc.entity.catalog.Document;
import ru.tandemservice.movestudentrmc.entity.catalog.DocumentKind;
import ru.tandemservice.movestudentrmc.entity.catalog.DocumentType;
import ru.tandemservice.movestudentrmc.util.ListenerWrapper;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.List;

@State(keys = {"studentId"}, bindings = {"student.id"})
@Input(keys = {"listener"}, bindings = {"listener"})
public class Model {

    private Student student = new Student();
    private ListenerWrapper<NarfuDocument> listener;

    private List<DocumentType> typeList;
    private DocumentType documentType;

    private ISingleSelectModel kindModel;
    private DocumentKind documentKind;

    private ISingleSelectModel docsettModel;
    private Document docsett;

    private ISingleSelectModel documentList;
    private NarfuDocument document;

    private boolean selected = false;
    private boolean newDocument = false;

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public List<DocumentType> getTypeList() {
        return typeList;
    }

    public void setTypeList(List<DocumentType> typeList) {
        this.typeList = typeList;
    }

    public DocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentType documentType) {
        this.documentType = documentType;
    }

    public ISingleSelectModel getKindModel() {
        return kindModel;
    }

    public void setKindModel(ISingleSelectModel kindModel) {
        this.kindModel = kindModel;
    }

    public DocumentKind getDocumentKind() {
        return documentKind;
    }

    public void setDocumentKind(DocumentKind documentKind) {
        this.documentKind = documentKind;
    }

    public ISingleSelectModel getDocumentList() {
        return documentList;
    }

    public void setDocumentList(ISingleSelectModel documentList) {
        this.documentList = documentList;
    }

    public NarfuDocument getDocument() {
        return document;
    }

    public void setDocument(NarfuDocument document) {
        this.document = document;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public ISingleSelectModel getDocsettModel() {
        return docsettModel;
    }

    public void setDocsettModel(ISingleSelectModel docsettModel) {
        this.docsettModel = docsettModel;
    }

    public Document getDocsett() {
        return docsett;
    }

    public void setDocsett(Document docsett) {
        this.docsett = docsett;
    }

    public ListenerWrapper<NarfuDocument> getListener() {
        return listener;
    }

    public void setListener(ListenerWrapper<NarfuDocument> listener) {
        this.listener = listener;
    }

    public boolean isNewDocument()
    {
        return newDocument;
    }

    public void setNewDocument(boolean newDocument)
    {
        this.newDocument = newDocument;
    }
}
