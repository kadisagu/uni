package ru.tandemservice.movestudentrmc.component.student.GrantAddEdit;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab.MonthWrapper;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntity;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import ru.tandemservice.movestudentrmc.entity.catalog.StuGrantStatus;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.StuGrantStatusCodes;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        model.setStudent(getNotNull(Student.class, model.getStudent().getId()));
        model.setEduYear(getNotNull(EducationYear.class, model.getEduYear().getId()));
        MonthWrapper monthWrapper = new MonthWrapper(model.getMonth());
        String monthStr = monthWrapper.getStringValue(model.getEduYear());

        if (model.getStudentGrant().getId() == null) {  //new record
            model.setUpdateMode(false);
            model.getStudentGrant().setStudent(model.getStudent());
            model.getStudentGrant().setStatus(getCatalogItem(StuGrantStatus.class, StuGrantStatusCodes.CODE_1));
            model.getStudentGrant().setEduYear(model.getEduYear());
            model.getStudentGrant().setMonth(monthStr);
        }
        else {
            model.setUpdateMode(true);
            model.setStudentGrant(getNotNull(StudentGrantEntity.class, model.getStudentGrant().getId()));
        }


//		DQLSelectBuilder builder = new DQLSelectBuilder();
//		builder.fromEntity(GrantEntity.class, "g")
//		.addColumn("g")
//		.where(DQLExpressions.eq(DQLExpressions.property(GrantEntity.orgUnit().fromAlias("g")), 
//				                 DQLExpressions.value(model.getStudent().getEducationOrgUnit().getFormativeOrgUnit())))
//		.where(DQLExpressions.eq(DQLExpressions.property(GrantEntity.eduYear().fromAlias("g")),
//                                 DQLExpressions.value(model.getEduYear())))
//		.where(DQLExpressions.eq(DQLExpressions.property(GrantEntity.month().fromAlias("g")),
//                                 DQLExpressions.value(monthStr)))
//		.where(DQLExpressions.notIn(DQLExpressions.property(GrantEntity.id().fromAlias("g")),
//                                 DQLExpressions.value(getExistsGrants(model, monthStr))))
//                                 ;
//		model.setGrantEntityModel(builder.createStatement(getSession()).<GrantEntity>list());


        model.setGrantViewModel(getList(GrantView.class));
        model.setStatusModel(getList(StuGrantStatus.class));
    }

    @Override
    public void update(Model model) {

        saveOrUpdate(model.getStudentGrant());

    }

    private List<Long> getExistsGrants(Model model, String monthStr) {
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(StudentGrantEntity.class, "sg")
                .addColumn("sg.grantEntity.id")
                .where(DQLExpressions.eq(DQLExpressions.property(StudentGrantEntity.student().fromAlias("sg")),
                                         DQLExpressions.value(model.getStudent())))
                .where(DQLExpressions.eq(DQLExpressions.property(StudentGrantEntity.eduYear().fromAlias("sg")),
                                         DQLExpressions.value(model.getEduYear())))
                .where(DQLExpressions.eq(DQLExpressions.property(StudentGrantEntity.month().fromAlias("sg")),
                                         DQLExpressions.value(monthStr)))
        ;

        return builder.createStatement(getSession()).<Long>list();
    }

}
