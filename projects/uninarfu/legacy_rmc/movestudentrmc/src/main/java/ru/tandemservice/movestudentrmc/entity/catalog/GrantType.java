package ru.tandemservice.movestudentrmc.entity.catalog;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.entity.RelTypeGrantView;
import ru.tandemservice.movestudentrmc.entity.catalog.gen.GrantTypeGen;
import ru.tandemservice.uni.dao.UniDaoFacade;

/**
 * Тип стипендии/выплаты
 */
public class GrantType extends GrantTypeGen {
    @EntityDSLSupport(parts = {P_TITLE})
    public Boolean isConfigRelation() {

        Boolean result;

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RelTypeGrantView.class, "rel").addColumn("rel");
        builder.where(DQLExpressions.eq(DQLExpressions.property(RelTypeGrantView.type().id().fromAlias("rel")), DQLExpressions.value(getId())));

        result = UniDaoFacade.getCoreDao().getCount(builder) > 0;

        return result;
    }

    @Override
    public Boolean getConfigRelation() {
        return isConfigRelation();
    }
}