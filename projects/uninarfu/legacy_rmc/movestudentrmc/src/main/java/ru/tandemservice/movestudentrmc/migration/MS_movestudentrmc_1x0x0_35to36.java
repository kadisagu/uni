package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_movestudentrmc_1x0x0_35to36 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.10"),
                        new ScriptDependency("org.tandemframework.shared", "1.1.3"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.1.3")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {

        if (tool.tableExists("listrepresent_t") && tool.tableExists("listrepresentgrant_t")
                && tool.tableExists("listrepresentgrantcancel_t") && tool.tableExists("listrepresentgrantresume_t")
                && tool.tableExists("listrepresentgrantsuspend_t") && tool.tableExists("listrepresentsupport_t"))
        {
            // сущность listRepresent
            // создано свойство operator
            // создать колонку
            tool.createColumn("listrepresent_t", new DBColumn("operator_id", DBType.LONG));

            Statement stmt = tool.getConnection().createStatement();
            stmt.execute("(select lg.ID, lg.OPERATOR_ID from LISTREPRESENTGRANT_T lg where lg.OPERATOR_ID is not null) " +
                                 "union all " +
                                 "(select lgc.ID, lgc.OPERATOR_ID from listrepresentgrantcancel_t lgc where lgc.OPERATOR_ID is not null) " +
                                 "union all " +
                                 "(select lgt.ID, lgt.OPERATOR_ID from listrepresentgrantresume_t lgt where lgt.OPERATOR_ID is not null) " +
                                 "union all " +
                                 "(select lgs.ID, lgs.OPERATOR_ID from listrepresentgrantsuspend_t lgs where lgs.OPERATOR_ID is not null) " +
                                 "union all " +
                                 "(select ls.ID, ls.OPERATOR_ID from listrepresentsupport_t ls where ls.OPERATOR_ID is not null)"
            );

            ResultSet rs = stmt.getResultSet();
            while (rs.next()) {
                Long representID = rs.getLong(1);
                Long operatorID = rs.getLong(2);
                tool.executeUpdate("update listrepresent_t set OPERATOR_ID=? where ID=?", operatorID, representID);
            }

            ////////////////////////////////////////////////////////////////////////////////
            // сущность listRepresentGrant
            // удалено свойство operator
            // удалить колонку
            tool.dropColumn("listrepresentgrant_t", "operator_id");
            ////////////////////////////////////////////////////////////////////////////////
            // сущность listRepresentGrantCancel
            // удалено свойство operator
            // удалить колонку
            tool.dropColumn("listrepresentgrantcancel_t", "operator_id");
            ////////////////////////////////////////////////////////////////////////////////
            // сущность listRepresentGrantResume
            // удалено свойство operator
            // удалить колонку
            tool.dropColumn("listrepresentgrantresume_t", "operator_id");
            ////////////////////////////////////////////////////////////////////////////////
            // сущность listRepresentGrantSuspend
            // удалено свойство operator
            // удалить колонку
            tool.dropColumn("listrepresentgrantsuspend_t", "operator_id");
            ////////////////////////////////////////////////////////////////////////////////
            // сущность listRepresentSupport
            // удалено свойство operator
            // удалить колонку
            tool.dropColumn("listrepresentsupport_t", "operator_id");
        }

    }
}