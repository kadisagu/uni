package ru.tandemservice.movestudentrmc.component.person.IdentityCardEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.State;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentIC;
import ru.tandemservice.movestudentrmc.entity.Representation;

import java.util.List;

@State({
        @Bind(key = "publisherId", binding = "publisherId")
})
@Input({
        @Bind(key = "list", binding = "list"),
        @Bind(key = "represent", binding = "represent")
})
public class Model extends org.tandemframework.shared.person.base.bo.Person.ui.IdentityCardEditInline.Model {

    private DocRepresentStudentIC _representIC = new DocRepresentStudentIC();
    private Representation _representation;
    private List<DocRepresentStudentIC> list;

    private boolean _represent;

    public void setRepresent(boolean represent)
    {
        this._represent = represent;
    }

    public boolean getRepresent()
    {
        return this._represent;
    }

    public DocRepresentStudentIC getRepresentIC()
    {
        return this._representIC;
    }

    public void setRepresentIC(DocRepresentStudentIC representIC)
    {
        this._representIC = representIC;
    }

    public Representation getRepresentation()
    {
        return this._representation;
    }

    public void setRepresentation(Representation representation)
    {
        this._representation = representation;
    }

    public List<DocRepresentStudentIC> getList() {
        return list;
    }

    public void setList(List<DocRepresentStudentIC> list) {
        this.list = list;
    }

}
