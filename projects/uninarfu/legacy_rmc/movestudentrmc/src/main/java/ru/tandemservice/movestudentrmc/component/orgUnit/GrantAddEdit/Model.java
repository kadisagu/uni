package ru.tandemservice.movestudentrmc.component.orgUnit.GrantAddEdit;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.State;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudentrmc.entity.GrantEntity;

@State({@org.tandemframework.core.component.Bind(key = "orgUnitId", binding = "orgUnit.id")})
@Input({@org.tandemframework.core.component.Bind(key = "grantEntity", binding = "grantEntity")})
public class Model {
    private OrgUnit orgUnit;
    private GrantEntity grantEntity;

    private int sumR;
    private int sumK;

    public GrantEntity getGrantEntity() {
        return grantEntity;
    }

    public void setGrantEntity(GrantEntity grantEntity) {
        this.grantEntity = grantEntity;
    }

    public int getSumR() {
        return sumR;
    }

    public void setSumR(int sumR) {
        this.sumR = sumR;
    }

    public int getSumK() {
        return sumK;
    }

    public void setSumK(int sumK) {
        this.sumK = sumK;
    }

    public OrgUnit getOrgUnit() {
        return orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit) {
        this.orgUnit = orgUnit;
    }

}
