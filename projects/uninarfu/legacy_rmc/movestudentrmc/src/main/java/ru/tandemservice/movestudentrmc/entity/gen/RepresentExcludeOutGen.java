package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudentrmc.entity.RepresentExcludeOut;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * О восстановлении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RepresentExcludeOutGen extends Representation
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.RepresentExcludeOut";
    public static final String ENTITY_NAME = "representExcludeOut";
    public static final int VERSION_HASH = -1147827273;
    private static IEntityMeta ENTITY_META;

    public static final String P_END_DATE = "endDate";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String L_COURSE = "course";
    public static final String L_GROUP = "group";
    public static final String L_EDUCATION_ORG_UNIT = "educationOrgUnit";
    public static final String P_OLD_STUDENT_STATUS_ID = "oldStudentStatusId";
    public static final String P_OLD_COMPENSATION_TYPE_ID = "oldCompensationTypeId";
    public static final String P_OLD_EDUCATION_ORG_UNIT_ID = "oldEducationOrgUnitId";
    public static final String P_OLD_COURSE_ID = "oldCourseId";
    public static final String P_OLD_GROUP_ID = "oldGroupId";
    public static final String L_WORK_PLAN = "workPlan";
    public static final String L_WORK_PLAN_REL = "workPlanRel";
    public static final String P_OLD_EDU_PLAN_REL_ID = "oldEduPlanRelId";
    public static final String P_OLD_WORK_PLAN_REL_IDS = "oldWorkPlanRelIds";

    private Date _endDate;     // Дата ликвидации разницы в учебных планах
    private CompensationType _compensationType;     // Тип возмещения затрат
    private Course _course;     // Курс
    private Group _group;     // Группа
    private EducationOrgUnit _educationOrgUnit;     // Направление подготовки (специальность)
    private Long _oldStudentStatusId;     // Статус (старый)
    private Long _oldCompensationTypeId;     // Тип возмещения затрат (старый)
    private Long _oldEducationOrgUnitId; 
    private Long _oldCourseId;     // Курс (старый)
    private Long _oldGroupId;     // Группа (старая)
    private EppWorkPlan _workPlan;     // РУП
    private EppStudent2WorkPlan _workPlanRel;     // Связь студент - РУП
    private Long _oldEduPlanRelId;     // Связь студент - УП (старая)
    private String _oldWorkPlanRelIds;     // Активные связи студент - РУП (старая)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата ликвидации разницы в учебных планах.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата ликвидации разницы в учебных планах.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Тип возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Тип возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Направление подготовки (специальность).
     */
    public EducationOrgUnit getEducationOrgUnit()
    {
        return _educationOrgUnit;
    }

    /**
     * @param educationOrgUnit Направление подготовки (специальность).
     */
    public void setEducationOrgUnit(EducationOrgUnit educationOrgUnit)
    {
        dirty(_educationOrgUnit, educationOrgUnit);
        _educationOrgUnit = educationOrgUnit;
    }

    /**
     * @return Статус (старый).
     */
    public Long getOldStudentStatusId()
    {
        return _oldStudentStatusId;
    }

    /**
     * @param oldStudentStatusId Статус (старый).
     */
    public void setOldStudentStatusId(Long oldStudentStatusId)
    {
        dirty(_oldStudentStatusId, oldStudentStatusId);
        _oldStudentStatusId = oldStudentStatusId;
    }

    /**
     * @return Тип возмещения затрат (старый).
     */
    public Long getOldCompensationTypeId()
    {
        return _oldCompensationTypeId;
    }

    /**
     * @param oldCompensationTypeId Тип возмещения затрат (старый).
     */
    public void setOldCompensationTypeId(Long oldCompensationTypeId)
    {
        dirty(_oldCompensationTypeId, oldCompensationTypeId);
        _oldCompensationTypeId = oldCompensationTypeId;
    }

    /**
     * @return 
     */
    public Long getOldEducationOrgUnitId()
    {
        return _oldEducationOrgUnitId;
    }

    /**
     * @param oldEducationOrgUnitId 
     */
    public void setOldEducationOrgUnitId(Long oldEducationOrgUnitId)
    {
        dirty(_oldEducationOrgUnitId, oldEducationOrgUnitId);
        _oldEducationOrgUnitId = oldEducationOrgUnitId;
    }

    /**
     * @return Курс (старый).
     */
    public Long getOldCourseId()
    {
        return _oldCourseId;
    }

    /**
     * @param oldCourseId Курс (старый).
     */
    public void setOldCourseId(Long oldCourseId)
    {
        dirty(_oldCourseId, oldCourseId);
        _oldCourseId = oldCourseId;
    }

    /**
     * @return Группа (старая).
     */
    public Long getOldGroupId()
    {
        return _oldGroupId;
    }

    /**
     * @param oldGroupId Группа (старая).
     */
    public void setOldGroupId(Long oldGroupId)
    {
        dirty(_oldGroupId, oldGroupId);
        _oldGroupId = oldGroupId;
    }

    /**
     * @return РУП.
     */
    public EppWorkPlan getWorkPlan()
    {
        return _workPlan;
    }

    /**
     * @param workPlan РУП.
     */
    public void setWorkPlan(EppWorkPlan workPlan)
    {
        dirty(_workPlan, workPlan);
        _workPlan = workPlan;
    }

    /**
     * @return Связь студент - РУП.
     */
    public EppStudent2WorkPlan getWorkPlanRel()
    {
        return _workPlanRel;
    }

    /**
     * @param workPlanRel Связь студент - РУП.
     */
    public void setWorkPlanRel(EppStudent2WorkPlan workPlanRel)
    {
        dirty(_workPlanRel, workPlanRel);
        _workPlanRel = workPlanRel;
    }

    /**
     * @return Связь студент - УП (старая).
     */
    public Long getOldEduPlanRelId()
    {
        return _oldEduPlanRelId;
    }

    /**
     * @param oldEduPlanRelId Связь студент - УП (старая).
     */
    public void setOldEduPlanRelId(Long oldEduPlanRelId)
    {
        dirty(_oldEduPlanRelId, oldEduPlanRelId);
        _oldEduPlanRelId = oldEduPlanRelId;
    }

    /**
     * @return Активные связи студент - РУП (старая).
     */
    @Length(max=255)
    public String getOldWorkPlanRelIds()
    {
        return _oldWorkPlanRelIds;
    }

    /**
     * @param oldWorkPlanRelIds Активные связи студент - РУП (старая).
     */
    public void setOldWorkPlanRelIds(String oldWorkPlanRelIds)
    {
        dirty(_oldWorkPlanRelIds, oldWorkPlanRelIds);
        _oldWorkPlanRelIds = oldWorkPlanRelIds;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof RepresentExcludeOutGen)
        {
            setEndDate(((RepresentExcludeOut)another).getEndDate());
            setCompensationType(((RepresentExcludeOut)another).getCompensationType());
            setCourse(((RepresentExcludeOut)another).getCourse());
            setGroup(((RepresentExcludeOut)another).getGroup());
            setEducationOrgUnit(((RepresentExcludeOut)another).getEducationOrgUnit());
            setOldStudentStatusId(((RepresentExcludeOut)another).getOldStudentStatusId());
            setOldCompensationTypeId(((RepresentExcludeOut)another).getOldCompensationTypeId());
            setOldEducationOrgUnitId(((RepresentExcludeOut)another).getOldEducationOrgUnitId());
            setOldCourseId(((RepresentExcludeOut)another).getOldCourseId());
            setOldGroupId(((RepresentExcludeOut)another).getOldGroupId());
            setWorkPlan(((RepresentExcludeOut)another).getWorkPlan());
            setWorkPlanRel(((RepresentExcludeOut)another).getWorkPlanRel());
            setOldEduPlanRelId(((RepresentExcludeOut)another).getOldEduPlanRelId());
            setOldWorkPlanRelIds(((RepresentExcludeOut)another).getOldWorkPlanRelIds());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RepresentExcludeOutGen> extends Representation.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RepresentExcludeOut.class;
        }

        public T newInstance()
        {
            return (T) new RepresentExcludeOut();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "endDate":
                    return obj.getEndDate();
                case "compensationType":
                    return obj.getCompensationType();
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
                case "educationOrgUnit":
                    return obj.getEducationOrgUnit();
                case "oldStudentStatusId":
                    return obj.getOldStudentStatusId();
                case "oldCompensationTypeId":
                    return obj.getOldCompensationTypeId();
                case "oldEducationOrgUnitId":
                    return obj.getOldEducationOrgUnitId();
                case "oldCourseId":
                    return obj.getOldCourseId();
                case "oldGroupId":
                    return obj.getOldGroupId();
                case "workPlan":
                    return obj.getWorkPlan();
                case "workPlanRel":
                    return obj.getWorkPlanRel();
                case "oldEduPlanRelId":
                    return obj.getOldEduPlanRelId();
                case "oldWorkPlanRelIds":
                    return obj.getOldWorkPlanRelIds();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "educationOrgUnit":
                    obj.setEducationOrgUnit((EducationOrgUnit) value);
                    return;
                case "oldStudentStatusId":
                    obj.setOldStudentStatusId((Long) value);
                    return;
                case "oldCompensationTypeId":
                    obj.setOldCompensationTypeId((Long) value);
                    return;
                case "oldEducationOrgUnitId":
                    obj.setOldEducationOrgUnitId((Long) value);
                    return;
                case "oldCourseId":
                    obj.setOldCourseId((Long) value);
                    return;
                case "oldGroupId":
                    obj.setOldGroupId((Long) value);
                    return;
                case "workPlan":
                    obj.setWorkPlan((EppWorkPlan) value);
                    return;
                case "workPlanRel":
                    obj.setWorkPlanRel((EppStudent2WorkPlan) value);
                    return;
                case "oldEduPlanRelId":
                    obj.setOldEduPlanRelId((Long) value);
                    return;
                case "oldWorkPlanRelIds":
                    obj.setOldWorkPlanRelIds((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "endDate":
                        return true;
                case "compensationType":
                        return true;
                case "course":
                        return true;
                case "group":
                        return true;
                case "educationOrgUnit":
                        return true;
                case "oldStudentStatusId":
                        return true;
                case "oldCompensationTypeId":
                        return true;
                case "oldEducationOrgUnitId":
                        return true;
                case "oldCourseId":
                        return true;
                case "oldGroupId":
                        return true;
                case "workPlan":
                        return true;
                case "workPlanRel":
                        return true;
                case "oldEduPlanRelId":
                        return true;
                case "oldWorkPlanRelIds":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "endDate":
                    return true;
                case "compensationType":
                    return true;
                case "course":
                    return true;
                case "group":
                    return true;
                case "educationOrgUnit":
                    return true;
                case "oldStudentStatusId":
                    return true;
                case "oldCompensationTypeId":
                    return true;
                case "oldEducationOrgUnitId":
                    return true;
                case "oldCourseId":
                    return true;
                case "oldGroupId":
                    return true;
                case "workPlan":
                    return true;
                case "workPlanRel":
                    return true;
                case "oldEduPlanRelId":
                    return true;
                case "oldWorkPlanRelIds":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "endDate":
                    return Date.class;
                case "compensationType":
                    return CompensationType.class;
                case "course":
                    return Course.class;
                case "group":
                    return Group.class;
                case "educationOrgUnit":
                    return EducationOrgUnit.class;
                case "oldStudentStatusId":
                    return Long.class;
                case "oldCompensationTypeId":
                    return Long.class;
                case "oldEducationOrgUnitId":
                    return Long.class;
                case "oldCourseId":
                    return Long.class;
                case "oldGroupId":
                    return Long.class;
                case "workPlan":
                    return EppWorkPlan.class;
                case "workPlanRel":
                    return EppStudent2WorkPlan.class;
                case "oldEduPlanRelId":
                    return Long.class;
                case "oldWorkPlanRelIds":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RepresentExcludeOut> _dslPath = new Path<RepresentExcludeOut>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RepresentExcludeOut");
    }
            

    /**
     * @return Дата ликвидации разницы в учебных планах.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentExcludeOut#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Тип возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentExcludeOut#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentExcludeOut#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentExcludeOut#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Направление подготовки (специальность).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentExcludeOut#getEducationOrgUnit()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
    {
        return _dslPath.educationOrgUnit();
    }

    /**
     * @return Статус (старый).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentExcludeOut#getOldStudentStatusId()
     */
    public static PropertyPath<Long> oldStudentStatusId()
    {
        return _dslPath.oldStudentStatusId();
    }

    /**
     * @return Тип возмещения затрат (старый).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentExcludeOut#getOldCompensationTypeId()
     */
    public static PropertyPath<Long> oldCompensationTypeId()
    {
        return _dslPath.oldCompensationTypeId();
    }

    /**
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.RepresentExcludeOut#getOldEducationOrgUnitId()
     */
    public static PropertyPath<Long> oldEducationOrgUnitId()
    {
        return _dslPath.oldEducationOrgUnitId();
    }

    /**
     * @return Курс (старый).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentExcludeOut#getOldCourseId()
     */
    public static PropertyPath<Long> oldCourseId()
    {
        return _dslPath.oldCourseId();
    }

    /**
     * @return Группа (старая).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentExcludeOut#getOldGroupId()
     */
    public static PropertyPath<Long> oldGroupId()
    {
        return _dslPath.oldGroupId();
    }

    /**
     * @return РУП.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentExcludeOut#getWorkPlan()
     */
    public static EppWorkPlan.Path<EppWorkPlan> workPlan()
    {
        return _dslPath.workPlan();
    }

    /**
     * @return Связь студент - РУП.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentExcludeOut#getWorkPlanRel()
     */
    public static EppStudent2WorkPlan.Path<EppStudent2WorkPlan> workPlanRel()
    {
        return _dslPath.workPlanRel();
    }

    /**
     * @return Связь студент - УП (старая).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentExcludeOut#getOldEduPlanRelId()
     */
    public static PropertyPath<Long> oldEduPlanRelId()
    {
        return _dslPath.oldEduPlanRelId();
    }

    /**
     * @return Активные связи студент - РУП (старая).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentExcludeOut#getOldWorkPlanRelIds()
     */
    public static PropertyPath<String> oldWorkPlanRelIds()
    {
        return _dslPath.oldWorkPlanRelIds();
    }

    public static class Path<E extends RepresentExcludeOut> extends Representation.Path<E>
    {
        private PropertyPath<Date> _endDate;
        private CompensationType.Path<CompensationType> _compensationType;
        private Course.Path<Course> _course;
        private Group.Path<Group> _group;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnit;
        private PropertyPath<Long> _oldStudentStatusId;
        private PropertyPath<Long> _oldCompensationTypeId;
        private PropertyPath<Long> _oldEducationOrgUnitId;
        private PropertyPath<Long> _oldCourseId;
        private PropertyPath<Long> _oldGroupId;
        private EppWorkPlan.Path<EppWorkPlan> _workPlan;
        private EppStudent2WorkPlan.Path<EppStudent2WorkPlan> _workPlanRel;
        private PropertyPath<Long> _oldEduPlanRelId;
        private PropertyPath<String> _oldWorkPlanRelIds;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата ликвидации разницы в учебных планах.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentExcludeOut#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(RepresentExcludeOutGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Тип возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentExcludeOut#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentExcludeOut#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentExcludeOut#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Направление подготовки (специальность).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentExcludeOut#getEducationOrgUnit()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
        {
            if(_educationOrgUnit == null )
                _educationOrgUnit = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT, this);
            return _educationOrgUnit;
        }

    /**
     * @return Статус (старый).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentExcludeOut#getOldStudentStatusId()
     */
        public PropertyPath<Long> oldStudentStatusId()
        {
            if(_oldStudentStatusId == null )
                _oldStudentStatusId = new PropertyPath<Long>(RepresentExcludeOutGen.P_OLD_STUDENT_STATUS_ID, this);
            return _oldStudentStatusId;
        }

    /**
     * @return Тип возмещения затрат (старый).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentExcludeOut#getOldCompensationTypeId()
     */
        public PropertyPath<Long> oldCompensationTypeId()
        {
            if(_oldCompensationTypeId == null )
                _oldCompensationTypeId = new PropertyPath<Long>(RepresentExcludeOutGen.P_OLD_COMPENSATION_TYPE_ID, this);
            return _oldCompensationTypeId;
        }

    /**
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.RepresentExcludeOut#getOldEducationOrgUnitId()
     */
        public PropertyPath<Long> oldEducationOrgUnitId()
        {
            if(_oldEducationOrgUnitId == null )
                _oldEducationOrgUnitId = new PropertyPath<Long>(RepresentExcludeOutGen.P_OLD_EDUCATION_ORG_UNIT_ID, this);
            return _oldEducationOrgUnitId;
        }

    /**
     * @return Курс (старый).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentExcludeOut#getOldCourseId()
     */
        public PropertyPath<Long> oldCourseId()
        {
            if(_oldCourseId == null )
                _oldCourseId = new PropertyPath<Long>(RepresentExcludeOutGen.P_OLD_COURSE_ID, this);
            return _oldCourseId;
        }

    /**
     * @return Группа (старая).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentExcludeOut#getOldGroupId()
     */
        public PropertyPath<Long> oldGroupId()
        {
            if(_oldGroupId == null )
                _oldGroupId = new PropertyPath<Long>(RepresentExcludeOutGen.P_OLD_GROUP_ID, this);
            return _oldGroupId;
        }

    /**
     * @return РУП.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentExcludeOut#getWorkPlan()
     */
        public EppWorkPlan.Path<EppWorkPlan> workPlan()
        {
            if(_workPlan == null )
                _workPlan = new EppWorkPlan.Path<EppWorkPlan>(L_WORK_PLAN, this);
            return _workPlan;
        }

    /**
     * @return Связь студент - РУП.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentExcludeOut#getWorkPlanRel()
     */
        public EppStudent2WorkPlan.Path<EppStudent2WorkPlan> workPlanRel()
        {
            if(_workPlanRel == null )
                _workPlanRel = new EppStudent2WorkPlan.Path<EppStudent2WorkPlan>(L_WORK_PLAN_REL, this);
            return _workPlanRel;
        }

    /**
     * @return Связь студент - УП (старая).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentExcludeOut#getOldEduPlanRelId()
     */
        public PropertyPath<Long> oldEduPlanRelId()
        {
            if(_oldEduPlanRelId == null )
                _oldEduPlanRelId = new PropertyPath<Long>(RepresentExcludeOutGen.P_OLD_EDU_PLAN_REL_ID, this);
            return _oldEduPlanRelId;
        }

    /**
     * @return Активные связи студент - РУП (старая).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentExcludeOut#getOldWorkPlanRelIds()
     */
        public PropertyPath<String> oldWorkPlanRelIds()
        {
            if(_oldWorkPlanRelIds == null )
                _oldWorkPlanRelIds = new PropertyPath<String>(RepresentExcludeOutGen.P_OLD_WORK_PLAN_REL_IDS, this);
            return _oldWorkPlanRelIds;
        }

        public Class getEntityClass()
        {
            return RepresentExcludeOut.class;
        }

        public String getEntityName()
        {
            return "representExcludeOut";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
