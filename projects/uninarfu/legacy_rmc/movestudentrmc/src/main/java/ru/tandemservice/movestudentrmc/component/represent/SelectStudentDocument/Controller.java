package ru.tandemservice.movestudentrmc.component.represent.SelectStudentDocument;

import org.tandemframework.core.component.Activator;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.movestudentrmc.entity.NarfuDocument;
import ru.tandemservice.movestudentrmc.util.ListenerWrapper;

public class Controller extends AbstractBusinessController<IDAO, Model> implements ListenerWrapper.IListener<NarfuDocument>
{

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        ((IDAO) getDao()).prepare(getModel(component));
    }

    public void onClickSelect(IBusinessComponent component) {
        Model model = component.getModel();
        deactivate(component);

        if (model.getListener() != null)
            model.getListener().process(true, model.getDocument());
    }

    public void onClickAddNew(IBusinessComponent component) {
        Model model = getModel(component);
        ListenerWrapper<NarfuDocument> listener = new ListenerWrapper<NarfuDocument>(component);
        Activator activator = new ComponentActivator(
                "ru.tandemservice.movestudentrmc.component.student.DocumentAddEdit",
                new ParametersMap().add("studentId", model.getStudent().getId()).add("tmp", Boolean.TRUE).add("listener", listener));
        component.createChildRegion("addNewDocumentRegion", activator);
        ((Model)component.getModel()).setNewDocument(true);
    }

    @Override
    public void listen(ListenerWrapper<NarfuDocument> wrapper) {
        if (wrapper.isOk()) {
            IBusinessComponent component = (IBusinessComponent) wrapper.getListener();
            Model model = getModel(component);
            model.setDocument(wrapper.getResult());

            if (model.getListener() != null) {
                deactivate(component);
                model.getListener().process(true, wrapper.getResult());
            }
        }
    }
}
