package ru.tandemservice.movestudentrmc.base.bo.ListRepresentAdmissionToPractice;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentAdmissionToPractice.logic.ListRepresentAdmissionToPracticeManagerModifyDAO;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentAdmissionToPractice.ui.Edit.ListRepresentAdmissionToPracticeEdit;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentAdmissionToPractice.ui.View.ListRepresentAdmissionToPracticeView;
import ru.tandemservice.movestudentrmc.docord.IListObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IListObjectModifyDAO;

@Configuration
public class ListRepresentAdmissionToPracticeManager extends BusinessObjectManager implements IListObjectByRepresentTypeManager {

    public static ListRepresentAdmissionToPracticeManager instance() {
        return instance(ListRepresentAdmissionToPracticeManager.class);
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return ListRepresentAdmissionToPracticeEdit.class;
    }

    @Bean
    @Override
    public IListObjectModifyDAO getListObjectModifyDAO() {
        return new ListRepresentAdmissionToPracticeManagerModifyDAO();
    }

    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return ListRepresentAdmissionToPracticeView.class;
    }
}
