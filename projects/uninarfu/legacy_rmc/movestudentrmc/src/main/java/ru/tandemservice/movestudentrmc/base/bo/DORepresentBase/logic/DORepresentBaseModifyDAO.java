package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic;

import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.dao.BaseModifyAggregateDAO;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudentrmc.dao.CheckStateUtil;
import ru.tandemservice.movestudentrmc.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentExtractStatesCodes;
import ru.tandemservice.uni.dao.IOrgstructDAO;
import ru.tandemservice.uni.dao.UniDaoFacade;

import java.util.Arrays;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

@Transactional
public class DORepresentBaseModifyDAO extends BaseModifyAggregateDAO<Representation>
        implements IDORepresentBaseModifyDAO
{

    @Override
    public void update(Representation document, List<DocRepresentStudentBase> studentList, List<DocRepresentBasics> representBasicsList, List<DocRepresentGrants> representGrantsList, List<DocRepresentStudentDocuments> representStudentDocumentsList, List<DocRepresentStudentIC> representStudentICsList) {

        if (document.getId() != null)
            CheckStateUtil.checkStateRepresent(document, Arrays.asList(MovestudentExtractStatesCodes.CODE_1, MovestudentExtractStatesCodes.CODE_2), getSession());

        baseCreateOrUpdate(document);

        for (DocRepresentStudentBase student : studentList) {

            baseCreateOrUpdate(student);
        }

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocRepresentBasics.class, "b").column(property("b"));
        builder.where(eq(
                property(DocRepresentBasics.representation().fromAlias("b")),
                value(document)));

        List<DocRepresentBasics> representOldBasicsList = builder.createStatement(getSession()).list();

        for (DocRepresentBasics representOldBasics : representOldBasicsList) {

            if (!representBasicsList.contains(representOldBasics))
                baseDelete(representOldBasics);
        }

        for (DocRepresentBasics representBasics : representBasicsList) {

            if (representBasics.getId() != null) {
                DocRepresentBasics updatedBasics = UniDaoFacade.getCoreDao().get(DocRepresentBasics.class, representBasics.getId());
                updatedBasics.update(representBasics);
                baseCreateOrUpdate(updatedBasics);
            }
            else {
                baseCreateOrUpdate(representBasics);
            }
        }

        /*Grants*/
        DQLSelectBuilder builderGrants = new DQLSelectBuilder().fromEntity(DocRepresentGrants.class, "g").addColumn("g");
        builderGrants.where(eq(
                property(DocRepresentGrants.representation().fromAlias("g")),
                value(document)));

        List<DocRepresentGrants> representOldGrantsList = builderGrants.createStatement(getSession()).list();

        for (DocRepresentGrants representOldGrants : representOldGrantsList) {

            if (!representGrantsList.contains(representOldGrants))
                baseDelete(representOldGrants);
        }

        for (DocRepresentGrants representGrants : representGrantsList) {

            baseCreateOrUpdate(representGrants);
        }
        /**/

        /*StudentDocuments*/
        List<DocRepresentStudentDocuments> oldDocumentList = MoveStudentDaoFacade.getDocumentDAO().getDocuments(document.getId());
        for (DocRepresentStudentDocuments rel : representStudentDocumentsList) {
            if (rel.getId() == null || rel.getId() < 0) {
                rel.setRepresentation(document);
                rel.setId(null);
            }
            else if (oldDocumentList.contains(rel)) {
                DocRepresentStudentDocuments oldDocument = oldDocumentList.get(oldDocumentList.indexOf(rel));
                oldDocumentList.remove(rel);

                oldDocument.update(rel);
                rel = oldDocument;
            }

            baseCreateOrUpdate(rel);
        }
        for (DocRepresentStudentDocuments rel : oldDocumentList)
            baseDelete(rel);
        /**/

        /*StudentICs*/
        List<DocRepresentStudentIC> oldICList = MoveStudentDaoFacade.getDocumentDAO().getIdentityCards(document.getId());
        for (DocRepresentStudentIC rel : representStudentICsList) {
            if (rel.getId() == null || rel.getId() < 0) {
                rel.setRepresentation(document);
                rel.setId(null);
            }
            else if (oldICList.contains(rel)) {
                DocRepresentStudentIC oldIC = oldICList.get(oldICList.indexOf(rel));
                oldICList.remove(rel);

                oldIC.update(rel);
                rel = oldIC;
            }

            baseCreateOrUpdate(rel);
        }
        for (DocRepresentStudentIC rel : oldICList)
            baseDelete(rel);
        /**/
    }

    @Override
    public void updateState(Representation document) {

        baseCreateOrUpdate(document);
    }

    @Override
    public void delete(Representation document) {

        baseDelete(document);
    }

    public boolean isOrgUnitDocOrdTabVisible(OrgUnit orgUnit)
    {
        return IOrgstructDAO.instance.get().isOrgUnitFormingOrTerritorial(orgUnit);
    }

}
