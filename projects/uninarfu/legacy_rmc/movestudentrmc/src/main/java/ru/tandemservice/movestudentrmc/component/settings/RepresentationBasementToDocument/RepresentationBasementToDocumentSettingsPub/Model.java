package ru.tandemservice.movestudentrmc.component.settings.RepresentationBasementToDocument.RepresentationBasementToDocumentSettingsPub;

import ru.tandemservice.uni.component.settings.abstractRelation.AbstractRelationPub.AbstractRelationPubModel;

public class Model extends AbstractRelationPubModel {

    private boolean firstExist;

    public boolean isFirstExist() {
        return firstExist;
    }

    public void setFirstExist(boolean firstExist) {
        this.firstExist = firstExist;
    }
}
