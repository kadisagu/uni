package ru.tandemservice.movestudentrmc.base.bo.ListOrder.ui.TabPanel;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import ru.tandemservice.movestudentrmc.base.bo.ListOrder.ui.List.ListOrderList;
import ru.tandemservice.movestudentrmc.base.bo.ListOrder.ui.ListCommit.ListOrderListCommit;

@Configuration
public class ListOrderTabPanel extends BusinessComponentManager {

    //Панель вкладок
    public static final String TAB_PANEL = "tabPanel";

    //Вкладки
    public static final String ORDER_WORK_TAB = "orderWorkTab";
    public static final String ORDER_COMMIT_TAB = "orderCommitTab";

    @Bean
    public TabPanelExtPoint tabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(TAB_PANEL)
                .addTab(componentTab(ORDER_WORK_TAB, ListOrderList.class).permissionKey("rmc_view_list_order_tab"))
                .addTab(componentTab(ORDER_COMMIT_TAB, ListOrderListCommit.class).permissionKey("rmc_view_commited_list_order_tab"))
                .create();
    }

}
