package ru.tandemservice.movestudentrmc.base.bo.DORepresentExclude.ui.Edit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.block.BlockListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.EditBasic.DORepresentBaseEditBasic;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.*;

@Configuration
public class DORepresentExcludeEdit extends BusinessComponentManager {

    public static final String REASON_DS = "reasonDS";
    public static final String STUDENT_DOCUMENTS_DS = "studentDocumentsDS";
    public static final String DOCUMENTS_TYPE_DS = "documentsTypeDS";
    public static final String DOCUMENTS_KIND_DS = "documentsKindDS";
    public static final String DOCUMENTS_DS = "documentsDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(REASON_DS, reasonDSHandler()))
                .addDataSource(selectDS(STUDENT_DOCUMENTS_DS, studentDocumentsDSHandler()))
                .addDataSource(selectDS(DOCUMENTS_TYPE_DS, documentsTypeDSHandler()))
                .addDataSource(selectDS(DOCUMENTS_KIND_DS, documentsKindDSHandler()))
                .addDataSource(selectDS(DOCUMENTS_DS, documentsDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler reasonDSHandler()
    {
        return (IDefaultComboDataSourceHandler) new ReasonDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler studentDocumentsDSHandler()
    {
        return (IDefaultComboDataSourceHandler) new StudentDocumentsDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler documentsTypeDSHandler()
    {
        return (IDefaultComboDataSourceHandler) new DocumentsTypeDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler documentsKindDSHandler()
    {
        return (IDefaultComboDataSourceHandler) new DocumentsKindDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler documentsDSHandler()
    {
        return (IDefaultComboDataSourceHandler) new DocumentsDSHandler(getName());
    }

    @Bean
    public BlockListExtPoint editBasicBlockListExtPoint()
    {
        return blockListExtPointBuilder(DORepresentExcludeEditUI.EDIT_BASIC_BLOCK_LIST)
                .addBlock(componentBlock(DORepresentExcludeEditUI.EDIT_BASIC_BLOCK, DORepresentBaseEditBasic.class).parameters("ui:blockParam"))
                .create();
    }
}
