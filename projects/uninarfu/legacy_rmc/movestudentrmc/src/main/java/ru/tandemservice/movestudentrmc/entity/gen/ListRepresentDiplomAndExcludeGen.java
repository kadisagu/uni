package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListRepresentDiplomAndExclude;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Списочное представление О выдаче диплома и отчислении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ListRepresentDiplomAndExcludeGen extends ListRepresent
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.ListRepresentDiplomAndExclude";
    public static final String ENTITY_NAME = "listRepresentDiplomAndExclude";
    public static final int VERSION_HASH = 547319248;
    private static IEntityMeta ENTITY_META;

    public static final String P_DATE_EXCLUDE = "dateExclude";
    public static final String P_DATE_PROTOCOL = "dateProtocol";
    public static final String P_NUMBER_PROTOCOL = "numberProtocol";

    private Date _dateExclude;     // Дата отчисления
    private Date _dateProtocol;     // Дата формирования протокола
    private String _numberProtocol;     // Номер протокола

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     */
    @NotNull
    public Date getDateExclude()
    {
        return _dateExclude;
    }

    /**
     * @param dateExclude Дата отчисления. Свойство не может быть null.
     */
    public void setDateExclude(Date dateExclude)
    {
        dirty(_dateExclude, dateExclude);
        _dateExclude = dateExclude;
    }

    /**
     * @return Дата формирования протокола. Свойство не может быть null.
     */
    @NotNull
    public Date getDateProtocol()
    {
        return _dateProtocol;
    }

    /**
     * @param dateProtocol Дата формирования протокола. Свойство не может быть null.
     */
    public void setDateProtocol(Date dateProtocol)
    {
        dirty(_dateProtocol, dateProtocol);
        _dateProtocol = dateProtocol;
    }

    /**
     * @return Номер протокола. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getNumberProtocol()
    {
        return _numberProtocol;
    }

    /**
     * @param numberProtocol Номер протокола. Свойство не может быть null.
     */
    public void setNumberProtocol(String numberProtocol)
    {
        dirty(_numberProtocol, numberProtocol);
        _numberProtocol = numberProtocol;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ListRepresentDiplomAndExcludeGen)
        {
            setDateExclude(((ListRepresentDiplomAndExclude)another).getDateExclude());
            setDateProtocol(((ListRepresentDiplomAndExclude)another).getDateProtocol());
            setNumberProtocol(((ListRepresentDiplomAndExclude)another).getNumberProtocol());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ListRepresentDiplomAndExcludeGen> extends ListRepresent.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ListRepresentDiplomAndExclude.class;
        }

        public T newInstance()
        {
            return (T) new ListRepresentDiplomAndExclude();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "dateExclude":
                    return obj.getDateExclude();
                case "dateProtocol":
                    return obj.getDateProtocol();
                case "numberProtocol":
                    return obj.getNumberProtocol();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "dateExclude":
                    obj.setDateExclude((Date) value);
                    return;
                case "dateProtocol":
                    obj.setDateProtocol((Date) value);
                    return;
                case "numberProtocol":
                    obj.setNumberProtocol((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "dateExclude":
                        return true;
                case "dateProtocol":
                        return true;
                case "numberProtocol":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "dateExclude":
                    return true;
                case "dateProtocol":
                    return true;
                case "numberProtocol":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "dateExclude":
                    return Date.class;
                case "dateProtocol":
                    return Date.class;
                case "numberProtocol":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ListRepresentDiplomAndExclude> _dslPath = new Path<ListRepresentDiplomAndExclude>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ListRepresentDiplomAndExclude");
    }
            

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentDiplomAndExclude#getDateExclude()
     */
    public static PropertyPath<Date> dateExclude()
    {
        return _dslPath.dateExclude();
    }

    /**
     * @return Дата формирования протокола. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentDiplomAndExclude#getDateProtocol()
     */
    public static PropertyPath<Date> dateProtocol()
    {
        return _dslPath.dateProtocol();
    }

    /**
     * @return Номер протокола. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentDiplomAndExclude#getNumberProtocol()
     */
    public static PropertyPath<String> numberProtocol()
    {
        return _dslPath.numberProtocol();
    }

    public static class Path<E extends ListRepresentDiplomAndExclude> extends ListRepresent.Path<E>
    {
        private PropertyPath<Date> _dateExclude;
        private PropertyPath<Date> _dateProtocol;
        private PropertyPath<String> _numberProtocol;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentDiplomAndExclude#getDateExclude()
     */
        public PropertyPath<Date> dateExclude()
        {
            if(_dateExclude == null )
                _dateExclude = new PropertyPath<Date>(ListRepresentDiplomAndExcludeGen.P_DATE_EXCLUDE, this);
            return _dateExclude;
        }

    /**
     * @return Дата формирования протокола. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentDiplomAndExclude#getDateProtocol()
     */
        public PropertyPath<Date> dateProtocol()
        {
            if(_dateProtocol == null )
                _dateProtocol = new PropertyPath<Date>(ListRepresentDiplomAndExcludeGen.P_DATE_PROTOCOL, this);
            return _dateProtocol;
        }

    /**
     * @return Номер протокола. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentDiplomAndExclude#getNumberProtocol()
     */
        public PropertyPath<String> numberProtocol()
        {
            if(_numberProtocol == null )
                _numberProtocol = new PropertyPath<String>(ListRepresentDiplomAndExcludeGen.P_NUMBER_PROTOCOL, this);
            return _numberProtocol;
        }

        public Class getEntityClass()
        {
            return ListRepresentDiplomAndExclude.class;
        }

        public String getEntityName()
        {
            return "listRepresentDiplomAndExclude";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
