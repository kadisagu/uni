package ru.tandemservice.movestudentrmc.base.bo.DORepresentGrant.ui.Edit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentEditUI;
import ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab.MonthWrapper;
import ru.tandemservice.movestudentrmc.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.GrantEntity;
import ru.tandemservice.movestudentrmc.entity.RepresentGrant;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.util.ReportRenderer;
import ru.tandemservice.uniedu.base.bo.EducationYear.EducationYearManager;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import static org.tandemframework.hibsupport.dql.DQLExpressions.and;
import static org.tandemframework.hibsupport.dql.DQLExpressions.between;
import static org.tandemframework.hibsupport.dql.DQLExpressions.gt;
import static org.tandemframework.hibsupport.dql.DQLExpressions.lt;
import static org.tandemframework.hibsupport.dql.DQLExpressions.or;
import static org.tandemframework.hibsupport.dql.DQLExpressions.plus;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;
import static org.tandemframework.hibsupport.dql.DQLExpressions.valueDate;
import static org.tandemframework.hibsupport.dql.DQLFunctions.createTimestamp;

import java.io.IOException;
import java.util.*;

public class DORepresentGrantEditUI extends AbstractDORepresentEditUI
{

    @Override
    protected void FetchStudents(Representation doc) {
        _studentDocList.clear();
        _studentList.clear();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocRepresentStudentBase.class, "stud").column("stud");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(DocRepresentStudentBase.representation().fromAlias("stud")),
                DQLExpressions.value(doc.getId())));

        List<DocRepresentStudentBase> studentList = builder.createStatement(getSupport().getSession()).list();
        for (DocRepresentStudentBase student : studentList) {

            _studentDocList.add(student);
            _studentList.add(student.getStudent());
        }

        _student = _studentList.get(0);
    }

    @Override
    protected void initDocument() {
        super.initDocument();

        //----------------------------------------------------------------------------------
        /*
        if (getRepresentId() == null) {
            RepresentGrant represent = (RepresentGrant) this.getRepresentObj();
            represent.setEducationYear(EducationYearManager.instance().dao().getCurrent());
        }
        */
        //----------------------------------------------------------------------------------
    }

    @Override
    public void onClickSave() throws IOException {

        RepresentGrant represent = (RepresentGrant) this.getRepresentObj();

        if (CommonBaseDateUtil.isAfter(represent.getBeginDate(), represent.getEndDate())) {
            throw new ApplicationException("Дата начала выплаты не может быть позже даты окончания выплаты");
        }
        
        //-----------------------------------------------------------------------------------------------------------------------------
		DQLSelectBuilder eduYearBuilder = new DQLSelectBuilder()
				.fromEntity(EducationYear.class, "year")
				.where(or(between(valueDate(represent.getBeginDate())
						, createTimestamp(property(EducationYear.intValue().fromAlias("year")), value(9), value(1), value(0), value(0), value(0))
						, createTimestamp(plus(property(EducationYear.intValue().fromAlias("year")), value(1)), value(8), value(31), value(23), value(59), value(59)))
						, between(valueDate(represent.getEndDate())
								, createTimestamp(property(EducationYear.intValue().fromAlias("year")), value(9), value(1), value(0), value(0), value(0))
								, createTimestamp(plus(property(EducationYear.intValue().fromAlias("year")), value(1)), value(8), value(31), value(23), value(59), value(59)))

						, and(lt(valueDate(represent.getBeginDate()), createTimestamp(property(EducationYear.intValue().fromAlias("year")), value(9), value(1), value(0), value(0), value(0)))
								, gt(valueDate(represent.getEndDate()), createTimestamp(plus(property(EducationYear.intValue().fromAlias("year")), value(1)), value(8), value(31), value(23), value(59), value(59))))
				));

		List<EducationYear> eduYearList = UniDaoFacade.getCoreDao().getList(eduYearBuilder);

		List<String> errorList = new ArrayList<>();

		for (EducationYear eduYear : eduYearList)
		{

			Date beginDate;
			Date endDate;

			Calendar beginYear = Calendar.getInstance();
			beginYear.set(eduYear.getIntValue(), Calendar.SEPTEMBER, 1, 0, 0, 0);
			beginYear.set(Calendar.MILLISECOND, 0);
			if (represent.getBeginDate().compareTo(beginYear.getTime()) > 0)
			{
				beginDate = represent.getBeginDate();
			}
			else
			{
				beginDate = beginYear.getTime();
			}

			Calendar endYear = Calendar.getInstance();
			endYear.set(eduYear.getIntValue() + 1, Calendar.AUGUST, 31, 23, 59, 59);
			endYear.set(Calendar.MILLISECOND, 999);
			if (represent.getEndDate().compareTo(endYear.getTime()) > 0)
			{
				endDate = endYear.getTime();
			}
			else
			{
				endDate = represent.getEndDate();
			}

			List<String> periodList = MonthWrapper.getMonthsList(beginDate, endDate, eduYear);

			MoveStudentDaoFacade.getGrantDAO().checkStudentGrants(represent.getId(), represent.getGrantView(), this.getStudentList(), periodList, errorList);

		}

		if (!errorList.isEmpty())
		{

			RtfDocument errorReport = RtfBean.getElementFactory().createRtfDocument();
			IRtfControl par = RtfBean.getElementFactory().createRtfControl(IRtfData.PAR);
			for (String text : errorList)
			{
				IRtfText studText = RtfBean.getElementFactory().createRtfText(text);
				errorReport.addElement(studText);
				errorReport.getElementList().addAll(Arrays.asList(par, par));
			}
			BusinessComponentUtils.downloadDocument(new ReportRenderer("Ошибки.rtf", errorReport), true);

			throw new ApplicationException("Не удалось сохранить представление");

		}

		// Вносим дату начала действия приказа для избежания nullPointException при печати приказа.
		_representObj.setStartDate(new Date());

		super.onClickSave();

		MoveStudentDaoFacade.getGrantDAO().deleteStudentGrants(represent);

		for (EducationYear eduYear : eduYearList)
		{

			Date beginDate;
			Date endDate;

			Calendar beginYear = Calendar.getInstance();
			beginYear.set(eduYear.getIntValue(), Calendar.SEPTEMBER, 1, 0, 0, 0);
			beginYear.set(Calendar.MILLISECOND, 0);
			if (represent.getBeginDate().compareTo(beginYear.getTime()) > 0)
			{
				beginDate = represent.getBeginDate();
			}
			else
			{
				beginDate = beginYear.getTime();
			}

			Calendar endYear = Calendar.getInstance();
			endYear.set(eduYear.getIntValue() + 1, Calendar.AUGUST, 31, 23, 59, 59);
			endYear.set(Calendar.MILLISECOND, 999);
			if (represent.getEndDate().compareTo(endYear.getTime()) > 0)
			{
				endDate = endYear.getTime();
			}
			else
			{
				endDate = represent.getEndDate();
			}

			List<String> periodList = MonthWrapper.getMonthsList(beginDate, endDate, eduYear);

			MoveStudentDaoFacade.getGrantDAO().saveStudentGrants(represent, represent.getGrantView(), _student, periodList, eduYear, false);

		}


        /*
        //период попадает в учебный год
        EducationYear eduYear = represent.getEducationYear();

        Calendar beginYear = Calendar.getInstance();
        beginYear.set(eduYear.getIntValue(), 8, 1, 0, 0, 0);
        beginYear.set(14, 0);
        Calendar endYear = Calendar.getInstance();
        endYear.set(eduYear.getIntValue() + 1, 7, 31, 23, 59, 59);
        endYear.set(14, 999);

        if (!CommonBaseDateUtil.isBetween(represent.getBeginDate(), beginYear.getTime(), endYear.getTime())
                || !CommonBaseDateUtil.isBetween(represent.getEndDate(), beginYear.getTime(), endYear.getTime()))
            throw new ApplicationException("Даты назначения стипендии/выплаты выходят за границы учебного года " + eduYear.getTitle());

        List<String> periodList = MonthWrapper.getMonthsList(represent.getBeginDate(), represent.getEndDate(), represent.getEducationYear());
        List<String> errorList = new ArrayList<>();

        boolean good = MoveStudentDaoFacade.getGrantDAO().checkStudentGrants(represent.getId(), represent.getGrantView(), this.getStudentList(), periodList, errorList);

        if (!good) {
            RtfDocument errorReport = RtfBean.getElementFactory().createRtfDocument();
            IRtfControl par = RtfBean.getElementFactory().createRtfControl(IRtfData.PAR);
            for (String text : errorList) {
                IRtfText studText = RtfBean.getElementFactory().createRtfText(text);
                errorReport.addElement(studText);
                errorReport.getElementList().addAll(Arrays.asList(par, par));
            }
            BusinessComponentUtils.downloadDocument(new ReportRenderer("Ошибки.rtf", errorReport), true);

            throw new ApplicationException("Не удалось сохранить представление");
        }

        List<String> months = new ArrayList<>();

        for (String month : periodList) {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(GrantEntity.class, "ge")
                    .where(DQLExpressions.eqValue(DQLExpressions.property(GrantEntity.orgUnit().fromAlias("ge")), _student.getEducationOrgUnit().getFormativeOrgUnit()))
                    .where(DQLExpressions.eqValue(DQLExpressions.property(GrantEntity.month().fromAlias("ge")), month))
                    .where(DQLExpressions.eqValue(DQLExpressions.property(GrantEntity.eduYear().fromAlias("ge")), represent.getEducationYear()))
                    .where(DQLExpressions.eqValue(DQLExpressions.property(GrantEntity.view().fromAlias("ge")), represent.getGrantView()));
            List<GrantEntity> lst = UniDaoFacade.getCoreDao().getList(builder);

            GrantEntity ge = lst.get(0);

            if (ge.getSum() == 0) {
                months.add(getMonthName(month));
            }

        }
        if (!months.isEmpty())
            ContextLocal.getInfoCollector().add("У выбранного вида стипендии сумма за " + StringUtils.join(months, ", ") + " равна 0.");

        // Вносим дату начала действия приказа для избежания nullPointException при печати приказа.
        _representObj.setStartDate(new Date());

        super.onClickSave();

        MoveStudentDaoFacade.getGrantDAO().saveStudentGrants(represent, represent.getGrantView(), _student, periodList, represent.getEducationYear());
        */
        //------------------------------------------------------------------------------------------------------------------------------
    }

    private String getMonthName(String month) {
        return RussianDateFormatUtils.getMonthName(Integer.parseInt(month.substring(0, month.indexOf("."))), true);
    }


}
