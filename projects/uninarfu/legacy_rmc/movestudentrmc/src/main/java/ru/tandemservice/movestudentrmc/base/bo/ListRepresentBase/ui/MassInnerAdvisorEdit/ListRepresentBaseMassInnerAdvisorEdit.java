package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ui.MassInnerAdvisorEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author Andrey Andreev
 * @since 19.10.2015
 */
@Configuration
public class ListRepresentBaseMassInnerAdvisorEdit extends BusinessComponentManager
{

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder().create();
    }

}