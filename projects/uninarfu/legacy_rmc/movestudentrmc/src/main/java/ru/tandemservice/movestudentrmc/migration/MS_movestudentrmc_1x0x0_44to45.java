package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_movestudentrmc_1x0x0_44to45 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.12"),
                        new ScriptDependency("org.tandemframework.shared", "1.2.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.2.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность listRepresentPractice

        // создана новая сущность
        if (!tool.tableExists("listrepresentpractice_t")) {
            // создать таблицу
            DBTable dbt = new DBTable("listrepresentpractice_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("practicetitle_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("datebeginningpractice_p", DBType.TIMESTAMP).setNullable(false),
                                      new DBColumn("dateendofpractice_p", DBType.TIMESTAMP).setNullable(false),
                                      new DBColumn("paymentdata_id", DBType.LONG)
            );
            tool.createTable(dbt);
            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("listRepresentPractice");
        }
    }
}