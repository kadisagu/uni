package ru.tandemservice.movestudentrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип изменения стипендии"
 * Имя сущности : representGrantType
 * Файл data.xml : movementstudent.data.xml
 */
public interface RepresentGrantTypeCodes
{
    /** Константа кода (code) элемента : destination (code). Название (title) : Назначение */
    String DESTINATION = "destination";
    /** Константа кода (code) элемента : cancel (code). Название (title) : Отмена */
    String CANCEL = "cancel";
    /** Константа кода (code) элемента : suspend (code). Название (title) : Приостановление */
    String SUSPEND = "suspend";
    /** Константа кода (code) элемента : resume (code). Название (title) : Возобновление */
    String RESUME = "resume";

    Set<String> CODES = ImmutableSet.of(DESTINATION, CANCEL, SUSPEND, RESUME);
}
