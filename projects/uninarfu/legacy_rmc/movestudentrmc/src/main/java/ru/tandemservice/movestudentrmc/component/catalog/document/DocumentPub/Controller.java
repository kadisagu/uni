package ru.tandemservice.movestudentrmc.component.catalog.document.DocumentPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.movestudentrmc.entity.catalog.Document;


public class Controller extends DefaultCatalogPubController<Document, Model, IDAO> {

    @Override
    protected DynamicListDataSource<Document> createListDataSource(IBusinessComponent component) {
        Model model = getModel(component);

        DynamicListDataSource<Document> dataSource = new DynamicListDataSource<Document>(component, this);
        dataSource.addColumn(getCatalogItemLinkColumn(model, "Название", "title"));
        dataSource.addColumn(new ToggleColumn("Отображать название", "showName").toggleOnListener("onToggleOnShowName").toggleOffListener("onToggleOffShowName"));

        dataSource.addColumn(new ToggleColumn("Доп. название", "additionalNameExist").toggleOnListener("onToggleOnAdditionalNameExist").toggleOffListener("onToggleOffAdditionalNameExist"));

        dataSource.addColumn(new ToggleColumn("Серия", "seriaExist").toggleOnListener("onToggleOnSeriaExist").toggleOffListener("onToggleOffSeriaExist"));
        dataSource.addColumn(new ToggleColumn("Обяз. серия", "seriaRequired").toggleOnListener("onToggleOnSeriaRequired").toggleOffListener("onToggleOffSeriaRequired"));
        dataSource.addColumn(new SimpleColumn("Мин. серия", "seriaMin"));
        dataSource.addColumn(new SimpleColumn("Макс. серия", "seriaMax"));

        dataSource.addColumn(new ToggleColumn("Номер", "numberExist").toggleOnListener("onToggleOnNumberExist").toggleOffListener("onToggleOffNumberExist"));
        dataSource.addColumn(new ToggleColumn("Обяз. номера", "numberRequired").toggleOnListener("onToggleOnNumberRequired").toggleOffListener("onToggleOffNumberRequired"));
        dataSource.addColumn(new SimpleColumn("Мин. номер", "numberMin"));
        dataSource.addColumn(new SimpleColumn("Макс. номер", "numberMax"));

        dataSource.addColumn(new ToggleColumn("Дата выдачи", "issuanceDateExist").toggleOnListener("onToggleOnIssuanceDateExist").toggleOffListener("onToggleOffIssuanceDateExist"));
        dataSource.addColumn(new ToggleColumn("Обяз. дата выдачи", "issuanceDateRequired").toggleOnListener("onToggleOnIssuanceDateRequired").toggleOffListener("onToggleOffIssuanceDateRequired"));

        dataSource.addColumn(new ToggleColumn("Период действия", "periodDateExist").toggleOnListener("onToggleOnPeriodDateExist").toggleOffListener("onToggleOffPeriodDateExist"));
        dataSource.addColumn(new ToggleColumn("Обяз. периода", "periodDateRequired").toggleOnListener("onToggleOnPeriodDateRequired").toggleOffListener("onToggleOffPeriodDateRequired"));

        if (model.isUserCatalog())
            dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));

        dataSource.addColumn(new ActionColumn(
                                     "Удалить",
                                     "delete",
                                     "onClickDeleteItem",
                                     "Удалить элемент «{0}» из справочника?",
                                     new Object[]{"title"}
                             )
                                     .setPermissionKey(model.getCatalogItemDelete())
                                     .setDisableHandler(model.getDisabledEntityHandler())
        );

        return dataSource;
    }

    public void onToggleOnShowName(IBusinessComponent component) {
        change(component, "showName", true);
    }

    public void onToggleOffShowName(IBusinessComponent component) {
        change(component, "showName", false);
    }

    public void onToggleOnAdditionalNameExist(IBusinessComponent component) {
        change(component, "additionalNameExist", true);
    }

    public void onToggleOffAdditionalNameExist(IBusinessComponent component) {
        change(component, "additionalNameExist", false);
    }

    public void onToggleOnSeriaExist(IBusinessComponent component) {
        change(component, "seriaExist", true);
    }

    public void onToggleOffSeriaExist(IBusinessComponent component) {
        change(component, "seriaExist", false);
    }

    public void onToggleOnSeriaRequired(IBusinessComponent component) {
        change(component, "seriaRequired", true);
    }

    public void onToggleOffSeriaRequired(IBusinessComponent component) {
        change(component, "seriaRequired", false);
    }

    public void onToggleOnNumberExist(IBusinessComponent component) {
        change(component, "numberExist", true);
    }

    public void onToggleOffNumberExist(IBusinessComponent component) {
        change(component, "numberExist", false);
    }

    public void onToggleOnNumberRequired(IBusinessComponent component) {
        change(component, "numberRequired", true);
    }

    public void onToggleOffNumberRequired(IBusinessComponent component) {
        change(component, "numberRequired", false);
    }

    public void onToggleOnIssuanceDateExist(IBusinessComponent component) {
        change(component, "issuanceDateExist", true);
    }

    public void onToggleOffIssuanceDateExist(IBusinessComponent component) {
        change(component, "issuanceDateExist", false);
    }

    public void onToggleOnIssuanceDateRequired(IBusinessComponent component) {
        change(component, "issuanceDateRequired", true);
    }

    public void onToggleOffIssuanceDateRequired(IBusinessComponent component) {
        change(component, "issuanceDateRequired", false);
    }

    public void onToggleOnPeriodDateExist(IBusinessComponent component) {
        change(component, "periodDateExist", true);
    }

    public void onToggleOffPeriodDateExist(IBusinessComponent component) {
        change(component, "periodDateExist", false);
    }

    public void onToggleOnPeriodDateRequired(IBusinessComponent component) {
        change(component, "periodDateRequired", true);
    }

    public void onToggleOffPeriodDateRequired(IBusinessComponent component) {
        change(component, "periodDateRequired", false);
    }


    private void change(IBusinessComponent component, String field, Boolean value) {
        Model model = component.getModel();
        Document document = model.getDataSource().getRecordById((Long) component.getListenerParameter());

        getDao().change(document, field, value);
    }
}
