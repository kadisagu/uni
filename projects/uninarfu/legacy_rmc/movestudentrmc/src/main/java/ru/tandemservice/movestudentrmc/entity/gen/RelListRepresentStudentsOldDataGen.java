package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents;
import ru.tandemservice.movestudentrmc.entity.RelListRepresentStudentsOldData;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Списочное представление-Студенты для хранения старых данных для студентов при откате списочного приказа
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RelListRepresentStudentsOldDataGen extends RelListRepresentStudents
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.RelListRepresentStudentsOldData";
    public static final String ENTITY_NAME = "relListRepresentStudentsOldData";
    public static final int VERSION_HASH = -655265153;
    private static IEntityMeta ENTITY_META;

    public static final String L_OLD_GROUP = "oldGroup";
    public static final String L_OLD_EDUCATION_ORG_UNIT = "oldEducationOrgUnit";
    public static final String L_OLD_COURSE = "oldCourse";
    public static final String L_OLD_STATUS = "oldStatus";
    public static final String L_OLD_COMPENSATION_TYPE = "oldCompensationType";

    private Group _oldGroup;     // Старая группа
    private EducationOrgUnit _oldEducationOrgUnit;     // Старое направление подготовки (ОУ)
    private Course _oldCourse;     // Старый курс
    private StudentStatus _oldStatus;     // Старый статус
    private CompensationType _oldCompensationType;     // Старый вид возмещения затрат

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Старая группа.
     */
    public Group getOldGroup()
    {
        return _oldGroup;
    }

    /**
     * @param oldGroup Старая группа.
     */
    public void setOldGroup(Group oldGroup)
    {
        dirty(_oldGroup, oldGroup);
        _oldGroup = oldGroup;
    }

    /**
     * @return Старое направление подготовки (ОУ).
     */
    public EducationOrgUnit getOldEducationOrgUnit()
    {
        return _oldEducationOrgUnit;
    }

    /**
     * @param oldEducationOrgUnit Старое направление подготовки (ОУ).
     */
    public void setOldEducationOrgUnit(EducationOrgUnit oldEducationOrgUnit)
    {
        dirty(_oldEducationOrgUnit, oldEducationOrgUnit);
        _oldEducationOrgUnit = oldEducationOrgUnit;
    }

    /**
     * @return Старый курс.
     */
    public Course getOldCourse()
    {
        return _oldCourse;
    }

    /**
     * @param oldCourse Старый курс.
     */
    public void setOldCourse(Course oldCourse)
    {
        dirty(_oldCourse, oldCourse);
        _oldCourse = oldCourse;
    }

    /**
     * @return Старый статус.
     */
    public StudentStatus getOldStatus()
    {
        return _oldStatus;
    }

    /**
     * @param oldStatus Старый статус.
     */
    public void setOldStatus(StudentStatus oldStatus)
    {
        dirty(_oldStatus, oldStatus);
        _oldStatus = oldStatus;
    }

    /**
     * @return Старый вид возмещения затрат.
     */
    public CompensationType getOldCompensationType()
    {
        return _oldCompensationType;
    }

    /**
     * @param oldCompensationType Старый вид возмещения затрат.
     */
    public void setOldCompensationType(CompensationType oldCompensationType)
    {
        dirty(_oldCompensationType, oldCompensationType);
        _oldCompensationType = oldCompensationType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof RelListRepresentStudentsOldDataGen)
        {
            setOldGroup(((RelListRepresentStudentsOldData)another).getOldGroup());
            setOldEducationOrgUnit(((RelListRepresentStudentsOldData)another).getOldEducationOrgUnit());
            setOldCourse(((RelListRepresentStudentsOldData)another).getOldCourse());
            setOldStatus(((RelListRepresentStudentsOldData)another).getOldStatus());
            setOldCompensationType(((RelListRepresentStudentsOldData)another).getOldCompensationType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RelListRepresentStudentsOldDataGen> extends RelListRepresentStudents.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RelListRepresentStudentsOldData.class;
        }

        public T newInstance()
        {
            return (T) new RelListRepresentStudentsOldData();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "oldGroup":
                    return obj.getOldGroup();
                case "oldEducationOrgUnit":
                    return obj.getOldEducationOrgUnit();
                case "oldCourse":
                    return obj.getOldCourse();
                case "oldStatus":
                    return obj.getOldStatus();
                case "oldCompensationType":
                    return obj.getOldCompensationType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "oldGroup":
                    obj.setOldGroup((Group) value);
                    return;
                case "oldEducationOrgUnit":
                    obj.setOldEducationOrgUnit((EducationOrgUnit) value);
                    return;
                case "oldCourse":
                    obj.setOldCourse((Course) value);
                    return;
                case "oldStatus":
                    obj.setOldStatus((StudentStatus) value);
                    return;
                case "oldCompensationType":
                    obj.setOldCompensationType((CompensationType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "oldGroup":
                        return true;
                case "oldEducationOrgUnit":
                        return true;
                case "oldCourse":
                        return true;
                case "oldStatus":
                        return true;
                case "oldCompensationType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "oldGroup":
                    return true;
                case "oldEducationOrgUnit":
                    return true;
                case "oldCourse":
                    return true;
                case "oldStatus":
                    return true;
                case "oldCompensationType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "oldGroup":
                    return Group.class;
                case "oldEducationOrgUnit":
                    return EducationOrgUnit.class;
                case "oldCourse":
                    return Course.class;
                case "oldStatus":
                    return StudentStatus.class;
                case "oldCompensationType":
                    return CompensationType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RelListRepresentStudentsOldData> _dslPath = new Path<RelListRepresentStudentsOldData>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RelListRepresentStudentsOldData");
    }
            

    /**
     * @return Старая группа.
     * @see ru.tandemservice.movestudentrmc.entity.RelListRepresentStudentsOldData#getOldGroup()
     */
    public static Group.Path<Group> oldGroup()
    {
        return _dslPath.oldGroup();
    }

    /**
     * @return Старое направление подготовки (ОУ).
     * @see ru.tandemservice.movestudentrmc.entity.RelListRepresentStudentsOldData#getOldEducationOrgUnit()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> oldEducationOrgUnit()
    {
        return _dslPath.oldEducationOrgUnit();
    }

    /**
     * @return Старый курс.
     * @see ru.tandemservice.movestudentrmc.entity.RelListRepresentStudentsOldData#getOldCourse()
     */
    public static Course.Path<Course> oldCourse()
    {
        return _dslPath.oldCourse();
    }

    /**
     * @return Старый статус.
     * @see ru.tandemservice.movestudentrmc.entity.RelListRepresentStudentsOldData#getOldStatus()
     */
    public static StudentStatus.Path<StudentStatus> oldStatus()
    {
        return _dslPath.oldStatus();
    }

    /**
     * @return Старый вид возмещения затрат.
     * @see ru.tandemservice.movestudentrmc.entity.RelListRepresentStudentsOldData#getOldCompensationType()
     */
    public static CompensationType.Path<CompensationType> oldCompensationType()
    {
        return _dslPath.oldCompensationType();
    }

    public static class Path<E extends RelListRepresentStudentsOldData> extends RelListRepresentStudents.Path<E>
    {
        private Group.Path<Group> _oldGroup;
        private EducationOrgUnit.Path<EducationOrgUnit> _oldEducationOrgUnit;
        private Course.Path<Course> _oldCourse;
        private StudentStatus.Path<StudentStatus> _oldStatus;
        private CompensationType.Path<CompensationType> _oldCompensationType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Старая группа.
     * @see ru.tandemservice.movestudentrmc.entity.RelListRepresentStudentsOldData#getOldGroup()
     */
        public Group.Path<Group> oldGroup()
        {
            if(_oldGroup == null )
                _oldGroup = new Group.Path<Group>(L_OLD_GROUP, this);
            return _oldGroup;
        }

    /**
     * @return Старое направление подготовки (ОУ).
     * @see ru.tandemservice.movestudentrmc.entity.RelListRepresentStudentsOldData#getOldEducationOrgUnit()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> oldEducationOrgUnit()
        {
            if(_oldEducationOrgUnit == null )
                _oldEducationOrgUnit = new EducationOrgUnit.Path<EducationOrgUnit>(L_OLD_EDUCATION_ORG_UNIT, this);
            return _oldEducationOrgUnit;
        }

    /**
     * @return Старый курс.
     * @see ru.tandemservice.movestudentrmc.entity.RelListRepresentStudentsOldData#getOldCourse()
     */
        public Course.Path<Course> oldCourse()
        {
            if(_oldCourse == null )
                _oldCourse = new Course.Path<Course>(L_OLD_COURSE, this);
            return _oldCourse;
        }

    /**
     * @return Старый статус.
     * @see ru.tandemservice.movestudentrmc.entity.RelListRepresentStudentsOldData#getOldStatus()
     */
        public StudentStatus.Path<StudentStatus> oldStatus()
        {
            if(_oldStatus == null )
                _oldStatus = new StudentStatus.Path<StudentStatus>(L_OLD_STATUS, this);
            return _oldStatus;
        }

    /**
     * @return Старый вид возмещения затрат.
     * @see ru.tandemservice.movestudentrmc.entity.RelListRepresentStudentsOldData#getOldCompensationType()
     */
        public CompensationType.Path<CompensationType> oldCompensationType()
        {
            if(_oldCompensationType == null )
                _oldCompensationType = new CompensationType.Path<CompensationType>(L_OLD_COMPENSATION_TYPE, this);
            return _oldCompensationType;
        }

        public Class getEntityClass()
        {
            return RelListRepresentStudentsOldData.class;
        }

        public String getEntityName()
        {
            return "relListRepresentStudentsOldData";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
