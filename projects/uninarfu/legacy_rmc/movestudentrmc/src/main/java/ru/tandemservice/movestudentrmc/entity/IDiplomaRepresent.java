package ru.tandemservice.movestudentrmc.entity;

import org.tandemframework.shared.organization.base.entity.OrgUnit;

public interface IDiplomaRepresent {
    public String getAcceptOrgUnitType();

    public OrgUnit getAcceptOrgUnit();

    public String getTypePrintTitle();
}
