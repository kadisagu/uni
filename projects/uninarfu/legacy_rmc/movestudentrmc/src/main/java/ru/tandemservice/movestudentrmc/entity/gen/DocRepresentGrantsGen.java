package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudentrmc.entity.DocRepresentGrants;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.entity.catalog.OsspGrants;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность-связь 'Представление'-'Стипендии'
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DocRepresentGrantsGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.DocRepresentGrants";
    public static final String ENTITY_NAME = "docRepresentGrants";
    public static final int VERSION_HASH = 693254171;
    private static IEntityMeta ENTITY_META;

    public static final String L_REPRESENTATION = "representation";
    public static final String L_REASON = "reason";
    public static final String L_OSSP_GRANTS = "osspGrants";
    public static final String P_TEXT_GRANTS = "textGrants";
    public static final String P_GRANT_START_DATE = "grantStartDate";
    public static final String P_GRANT_END_DATE = "grantEndDate";
    public static final String P_GRANT_SIZE = "grantSize";

    private Representation _representation;     // Представление
    private RepresentationReason _reason;     // Причина
    private OsspGrants _osspGrants;     // Стипендии ОССП
    private String _textGrants;     // Текст стипендии
    private Date _grantStartDate;     // Дата начала
    private Date _grantEndDate;     // Дата окончания
    private String _grantSize;     // Размер

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Представление. Свойство не может быть null.
     */
    @NotNull
    public Representation getRepresentation()
    {
        return _representation;
    }

    /**
     * @param representation Представление. Свойство не может быть null.
     */
    public void setRepresentation(Representation representation)
    {
        dirty(_representation, representation);
        _representation = representation;
    }

    /**
     * @return Причина.
     */
    public RepresentationReason getReason()
    {
        return _reason;
    }

    /**
     * @param reason Причина.
     */
    public void setReason(RepresentationReason reason)
    {
        dirty(_reason, reason);
        _reason = reason;
    }

    /**
     * @return Стипендии ОССП.
     */
    public OsspGrants getOsspGrants()
    {
        return _osspGrants;
    }

    /**
     * @param osspGrants Стипендии ОССП.
     */
    public void setOsspGrants(OsspGrants osspGrants)
    {
        dirty(_osspGrants, osspGrants);
        _osspGrants = osspGrants;
    }

    /**
     * @return Текст стипендии.
     */
    @Length(max=255)
    public String getTextGrants()
    {
        return _textGrants;
    }

    /**
     * @param textGrants Текст стипендии.
     */
    public void setTextGrants(String textGrants)
    {
        dirty(_textGrants, textGrants);
        _textGrants = textGrants;
    }

    /**
     * @return Дата начала.
     */
    public Date getGrantStartDate()
    {
        return _grantStartDate;
    }

    /**
     * @param grantStartDate Дата начала.
     */
    public void setGrantStartDate(Date grantStartDate)
    {
        dirty(_grantStartDate, grantStartDate);
        _grantStartDate = grantStartDate;
    }

    /**
     * @return Дата окончания.
     */
    public Date getGrantEndDate()
    {
        return _grantEndDate;
    }

    /**
     * @param grantEndDate Дата окончания.
     */
    public void setGrantEndDate(Date grantEndDate)
    {
        dirty(_grantEndDate, grantEndDate);
        _grantEndDate = grantEndDate;
    }

    /**
     * @return Размер.
     */
    @Length(max=255)
    public String getGrantSize()
    {
        return _grantSize;
    }

    /**
     * @param grantSize Размер.
     */
    public void setGrantSize(String grantSize)
    {
        dirty(_grantSize, grantSize);
        _grantSize = grantSize;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DocRepresentGrantsGen)
        {
            setRepresentation(((DocRepresentGrants)another).getRepresentation());
            setReason(((DocRepresentGrants)another).getReason());
            setOsspGrants(((DocRepresentGrants)another).getOsspGrants());
            setTextGrants(((DocRepresentGrants)another).getTextGrants());
            setGrantStartDate(((DocRepresentGrants)another).getGrantStartDate());
            setGrantEndDate(((DocRepresentGrants)another).getGrantEndDate());
            setGrantSize(((DocRepresentGrants)another).getGrantSize());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DocRepresentGrantsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DocRepresentGrants.class;
        }

        public T newInstance()
        {
            return (T) new DocRepresentGrants();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "representation":
                    return obj.getRepresentation();
                case "reason":
                    return obj.getReason();
                case "osspGrants":
                    return obj.getOsspGrants();
                case "textGrants":
                    return obj.getTextGrants();
                case "grantStartDate":
                    return obj.getGrantStartDate();
                case "grantEndDate":
                    return obj.getGrantEndDate();
                case "grantSize":
                    return obj.getGrantSize();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "representation":
                    obj.setRepresentation((Representation) value);
                    return;
                case "reason":
                    obj.setReason((RepresentationReason) value);
                    return;
                case "osspGrants":
                    obj.setOsspGrants((OsspGrants) value);
                    return;
                case "textGrants":
                    obj.setTextGrants((String) value);
                    return;
                case "grantStartDate":
                    obj.setGrantStartDate((Date) value);
                    return;
                case "grantEndDate":
                    obj.setGrantEndDate((Date) value);
                    return;
                case "grantSize":
                    obj.setGrantSize((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "representation":
                        return true;
                case "reason":
                        return true;
                case "osspGrants":
                        return true;
                case "textGrants":
                        return true;
                case "grantStartDate":
                        return true;
                case "grantEndDate":
                        return true;
                case "grantSize":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "representation":
                    return true;
                case "reason":
                    return true;
                case "osspGrants":
                    return true;
                case "textGrants":
                    return true;
                case "grantStartDate":
                    return true;
                case "grantEndDate":
                    return true;
                case "grantSize":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "representation":
                    return Representation.class;
                case "reason":
                    return RepresentationReason.class;
                case "osspGrants":
                    return OsspGrants.class;
                case "textGrants":
                    return String.class;
                case "grantStartDate":
                    return Date.class;
                case "grantEndDate":
                    return Date.class;
                case "grantSize":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DocRepresentGrants> _dslPath = new Path<DocRepresentGrants>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DocRepresentGrants");
    }
            

    /**
     * @return Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentGrants#getRepresentation()
     */
    public static Representation.Path<Representation> representation()
    {
        return _dslPath.representation();
    }

    /**
     * @return Причина.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentGrants#getReason()
     */
    public static RepresentationReason.Path<RepresentationReason> reason()
    {
        return _dslPath.reason();
    }

    /**
     * @return Стипендии ОССП.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentGrants#getOsspGrants()
     */
    public static OsspGrants.Path<OsspGrants> osspGrants()
    {
        return _dslPath.osspGrants();
    }

    /**
     * @return Текст стипендии.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentGrants#getTextGrants()
     */
    public static PropertyPath<String> textGrants()
    {
        return _dslPath.textGrants();
    }

    /**
     * @return Дата начала.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentGrants#getGrantStartDate()
     */
    public static PropertyPath<Date> grantStartDate()
    {
        return _dslPath.grantStartDate();
    }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentGrants#getGrantEndDate()
     */
    public static PropertyPath<Date> grantEndDate()
    {
        return _dslPath.grantEndDate();
    }

    /**
     * @return Размер.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentGrants#getGrantSize()
     */
    public static PropertyPath<String> grantSize()
    {
        return _dslPath.grantSize();
    }

    public static class Path<E extends DocRepresentGrants> extends EntityPath<E>
    {
        private Representation.Path<Representation> _representation;
        private RepresentationReason.Path<RepresentationReason> _reason;
        private OsspGrants.Path<OsspGrants> _osspGrants;
        private PropertyPath<String> _textGrants;
        private PropertyPath<Date> _grantStartDate;
        private PropertyPath<Date> _grantEndDate;
        private PropertyPath<String> _grantSize;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentGrants#getRepresentation()
     */
        public Representation.Path<Representation> representation()
        {
            if(_representation == null )
                _representation = new Representation.Path<Representation>(L_REPRESENTATION, this);
            return _representation;
        }

    /**
     * @return Причина.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentGrants#getReason()
     */
        public RepresentationReason.Path<RepresentationReason> reason()
        {
            if(_reason == null )
                _reason = new RepresentationReason.Path<RepresentationReason>(L_REASON, this);
            return _reason;
        }

    /**
     * @return Стипендии ОССП.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentGrants#getOsspGrants()
     */
        public OsspGrants.Path<OsspGrants> osspGrants()
        {
            if(_osspGrants == null )
                _osspGrants = new OsspGrants.Path<OsspGrants>(L_OSSP_GRANTS, this);
            return _osspGrants;
        }

    /**
     * @return Текст стипендии.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentGrants#getTextGrants()
     */
        public PropertyPath<String> textGrants()
        {
            if(_textGrants == null )
                _textGrants = new PropertyPath<String>(DocRepresentGrantsGen.P_TEXT_GRANTS, this);
            return _textGrants;
        }

    /**
     * @return Дата начала.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentGrants#getGrantStartDate()
     */
        public PropertyPath<Date> grantStartDate()
        {
            if(_grantStartDate == null )
                _grantStartDate = new PropertyPath<Date>(DocRepresentGrantsGen.P_GRANT_START_DATE, this);
            return _grantStartDate;
        }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentGrants#getGrantEndDate()
     */
        public PropertyPath<Date> grantEndDate()
        {
            if(_grantEndDate == null )
                _grantEndDate = new PropertyPath<Date>(DocRepresentGrantsGen.P_GRANT_END_DATE, this);
            return _grantEndDate;
        }

    /**
     * @return Размер.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentGrants#getGrantSize()
     */
        public PropertyPath<String> grantSize()
        {
            if(_grantSize == null )
                _grantSize = new PropertyPath<String>(DocRepresentGrantsGen.P_GRANT_SIZE, this);
            return _grantSize;
        }

        public Class getEntityClass()
        {
            return DocRepresentGrants.class;
        }

        public String getEntityName()
        {
            return "docRepresentGrants";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
