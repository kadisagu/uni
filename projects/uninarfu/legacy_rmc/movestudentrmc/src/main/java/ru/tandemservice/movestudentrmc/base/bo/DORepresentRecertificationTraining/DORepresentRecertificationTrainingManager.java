package ru.tandemservice.movestudentrmc.base.bo.DORepresentRecertificationTraining;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentRecertificationTraining.logic.DORepresentRecertificationTrainingDAO;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentRecertificationTraining.ui.Edit.DORepresentRecertificationTrainingEdit;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentRecertificationTraining.ui.View.DORepresentRecertificationTrainingView;
import ru.tandemservice.movestudentrmc.docord.IDOObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IDOObjectModifyDAO;

@Configuration
public class DORepresentRecertificationTrainingManager extends BusinessObjectManager implements IDOObjectByRepresentTypeManager {

    public static DORepresentRecertificationTrainingManager instance()
    {
        return instance(DORepresentRecertificationTrainingManager.class);
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return DORepresentRecertificationTrainingEdit.class;
    }

    @Override
    @Bean
    public IDOObjectModifyDAO getDOObjectModifyDAO() {
        return new DORepresentRecertificationTrainingDAO();
    }

    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return DORepresentRecertificationTrainingView.class;
    }
}
