package ru.tandemservice.movestudentrmc.base.bo.RelationSettings.ui.EditRel;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.base.bo.RelationSettings.DORelationSettingsManager;
import ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonBasic;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;

import java.util.List;

@Input({
        @Bind(key = RelationSettingsEditRelUI.REASON_ID, binding = RelationSettingsEditRelUI.REASON_ID),
        @Bind(key = RelationSettingsEditRelUI.TYPE_REPRESENTATION_ID, binding = RelationSettingsEditRelUI.TYPE_REPRESENTATION_ID)
})
public class RelationSettingsEditRelUI extends UIPresenter {

    public static final String REASON_ID = "reasonId";
    public static final String TYPE_REPRESENTATION_ID = "typeRepresentationId";

    private Long reasonId;
    private Long typeRepresentationId;
    private RepresentationReason reason;
    private RepresentationType typeRepresentation;
    private boolean firstRun = true;
    private List<RelRepresentationReasonBasic> relList;
    private RelRepresentationReasonBasic relRow;

    @Override
    public void onComponentRefresh()
    {

        if (!firstRun) {

            return;
        }

        if (null != reasonId)
            reason = DataAccessServices.dao().getNotNull(RepresentationReason.class, reasonId);
        else
            reason = new RepresentationReason();

        if (null != typeRepresentationId)
            typeRepresentation = DataAccessServices.dao().getNotNull(RepresentationType.class, typeRepresentationId);
        else
            typeRepresentation = new RepresentationType();

        fillArrayRel();

        if (firstRun)
            firstRun = !firstRun;
    }

    private void fillArrayRel() {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RelRepresentationReasonBasic.class, "rel").addColumn("rel");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(RelRepresentationReasonBasic.type().id().fromAlias("rel")),
                DQLExpressions.value(typeRepresentationId)))
                .where(DQLExpressions.eq(
                        DQLExpressions.property(RelRepresentationReasonBasic.reason().id().fromAlias("rel")),
                        DQLExpressions.value(reasonId)));
        ;

        relList = builder.createStatement(getSupport().getSession()).list();
    }

    public void onClickApply()
    {

        DORelationSettingsManager.instance().modifyDao().updateRelations(typeRepresentation, reason, relList);
        //fillArrayRel();
        deactivate();
    }

    public List<RelRepresentationReasonBasic> getRelList() {
        return relList;
    }

    public void setRelList(List<RelRepresentationReasonBasic> relList) {
        this.relList = relList;
    }

    public RelRepresentationReasonBasic getRelRow() {
        return relRow;
    }

    public void setRelRow(RelRepresentationReasonBasic relRow) {
        this.relRow = relRow;
    }

    public Long getReasonId() {
        return reasonId;
    }

    public void setReasonId(Long reasonId) {
        this.reasonId = reasonId;
    }

    public Long getTypeRepresentationId() {
        return typeRepresentationId;
    }

    public void setTypeRepresentationId(Long typeRepresentationId) {
        this.typeRepresentationId = typeRepresentationId;
    }

    public RepresentationReason getReason() {
        return reason;
    }

    public void setReason(RepresentationReason reason) {
        this.reason = reason;
    }

    public RepresentationType getTypeRepresentation() {
        return typeRepresentation;
    }

    public void setTypeRepresentation(RepresentationType typeRepresentation) {
        this.typeRepresentation = typeRepresentation;
    }
}
