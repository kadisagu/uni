package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListRepresentProfileTransfer;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Списочное представление о распределении обучаемых по профилям и группам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ListRepresentProfileTransferGen extends ListRepresent
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.ListRepresentProfileTransfer";
    public static final String ENTITY_NAME = "listRepresentProfileTransfer";
    public static final int VERSION_HASH = -766619888;
    private static IEntityMeta ENTITY_META;

    public static final String L_NEW_GROUP = "newGroup";
    public static final String L_NEW_EDUCATION_ORG_UNIT = "newEducationOrgUnit";
    public static final String P_DATE_BEGINING_TRANSFER = "dateBeginingTransfer";

    private Group _newGroup;     // Новая группа
    private EducationOrgUnit _newEducationOrgUnit;     // Новое направление подготовки ОУ
    private Date _dateBeginingTransfer;     // Дата перевода

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Новая группа. Свойство не может быть null.
     */
    @NotNull
    public Group getNewGroup()
    {
        return _newGroup;
    }

    /**
     * @param newGroup Новая группа. Свойство не может быть null.
     */
    public void setNewGroup(Group newGroup)
    {
        dirty(_newGroup, newGroup);
        _newGroup = newGroup;
    }

    /**
     * @return Новое направление подготовки ОУ.
     */
    public EducationOrgUnit getNewEducationOrgUnit()
    {
        return _newEducationOrgUnit;
    }

    /**
     * @param newEducationOrgUnit Новое направление подготовки ОУ.
     */
    public void setNewEducationOrgUnit(EducationOrgUnit newEducationOrgUnit)
    {
        dirty(_newEducationOrgUnit, newEducationOrgUnit);
        _newEducationOrgUnit = newEducationOrgUnit;
    }

    /**
     * @return Дата перевода. Свойство не может быть null.
     */
    @NotNull
    public Date getDateBeginingTransfer()
    {
        return _dateBeginingTransfer;
    }

    /**
     * @param dateBeginingTransfer Дата перевода. Свойство не может быть null.
     */
    public void setDateBeginingTransfer(Date dateBeginingTransfer)
    {
        dirty(_dateBeginingTransfer, dateBeginingTransfer);
        _dateBeginingTransfer = dateBeginingTransfer;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ListRepresentProfileTransferGen)
        {
            setNewGroup(((ListRepresentProfileTransfer)another).getNewGroup());
            setNewEducationOrgUnit(((ListRepresentProfileTransfer)another).getNewEducationOrgUnit());
            setDateBeginingTransfer(((ListRepresentProfileTransfer)another).getDateBeginingTransfer());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ListRepresentProfileTransferGen> extends ListRepresent.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ListRepresentProfileTransfer.class;
        }

        public T newInstance()
        {
            return (T) new ListRepresentProfileTransfer();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "newGroup":
                    return obj.getNewGroup();
                case "newEducationOrgUnit":
                    return obj.getNewEducationOrgUnit();
                case "dateBeginingTransfer":
                    return obj.getDateBeginingTransfer();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "newGroup":
                    obj.setNewGroup((Group) value);
                    return;
                case "newEducationOrgUnit":
                    obj.setNewEducationOrgUnit((EducationOrgUnit) value);
                    return;
                case "dateBeginingTransfer":
                    obj.setDateBeginingTransfer((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "newGroup":
                        return true;
                case "newEducationOrgUnit":
                        return true;
                case "dateBeginingTransfer":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "newGroup":
                    return true;
                case "newEducationOrgUnit":
                    return true;
                case "dateBeginingTransfer":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "newGroup":
                    return Group.class;
                case "newEducationOrgUnit":
                    return EducationOrgUnit.class;
                case "dateBeginingTransfer":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ListRepresentProfileTransfer> _dslPath = new Path<ListRepresentProfileTransfer>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ListRepresentProfileTransfer");
    }
            

    /**
     * @return Новая группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentProfileTransfer#getNewGroup()
     */
    public static Group.Path<Group> newGroup()
    {
        return _dslPath.newGroup();
    }

    /**
     * @return Новое направление подготовки ОУ.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentProfileTransfer#getNewEducationOrgUnit()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> newEducationOrgUnit()
    {
        return _dslPath.newEducationOrgUnit();
    }

    /**
     * @return Дата перевода. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentProfileTransfer#getDateBeginingTransfer()
     */
    public static PropertyPath<Date> dateBeginingTransfer()
    {
        return _dslPath.dateBeginingTransfer();
    }

    public static class Path<E extends ListRepresentProfileTransfer> extends ListRepresent.Path<E>
    {
        private Group.Path<Group> _newGroup;
        private EducationOrgUnit.Path<EducationOrgUnit> _newEducationOrgUnit;
        private PropertyPath<Date> _dateBeginingTransfer;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Новая группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentProfileTransfer#getNewGroup()
     */
        public Group.Path<Group> newGroup()
        {
            if(_newGroup == null )
                _newGroup = new Group.Path<Group>(L_NEW_GROUP, this);
            return _newGroup;
        }

    /**
     * @return Новое направление подготовки ОУ.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentProfileTransfer#getNewEducationOrgUnit()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> newEducationOrgUnit()
        {
            if(_newEducationOrgUnit == null )
                _newEducationOrgUnit = new EducationOrgUnit.Path<EducationOrgUnit>(L_NEW_EDUCATION_ORG_UNIT, this);
            return _newEducationOrgUnit;
        }

    /**
     * @return Дата перевода. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentProfileTransfer#getDateBeginingTransfer()
     */
        public PropertyPath<Date> dateBeginingTransfer()
        {
            if(_dateBeginingTransfer == null )
                _dateBeginingTransfer = new PropertyPath<Date>(ListRepresentProfileTransferGen.P_DATE_BEGINING_TRANSFER, this);
            return _dateBeginingTransfer;
        }

        public Class getEntityClass()
        {
            return ListRepresentProfileTransfer.class;
        }

        public String getEntityName()
        {
            return "listRepresentProfileTransfer";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
