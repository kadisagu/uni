package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;

public class MS_movestudentrmc_1x0x0_47to48 extends IndependentMigrationScript {

    @Override
    public void run(DBTool tool) throws Exception {

        if (tool.tableExists("CHECKONORDER_T")) {
            // могут быть связи с relCheckOnorderRepresentType и relCheckOnorderGrantView
            // сначало уничтожим связанные записи
            String sqlDeleteRelation_1
                    = "delete from relCheckOnorderRepresentType_t where " +
                    "relCheckOnorderRepresentType_t.CHECK_ID in " +
                    "(select id from CHECKONORDER_T where CODE_P not like 'movestudentrmc%')";

            String sqlDeleteRelation_2
                    = "delete from relCheckOnorderGrantView_t where " +
                    "relCheckOnorderGrantView_t.CHECK_ID in " +
                    "(select id from CHECKONORDER_T where CODE_P not like 'movestudentrmc%')";

            tool.executeUpdate(sqlDeleteRelation_1);
            tool.executeUpdate(sqlDeleteRelation_2);

            tool.executeUpdate("delete from CHECKONORDER_T where CODE_P not like 'movestudentrmc%'");
        }
    }

}
