package ru.tandemservice.movestudentrmc.base.bo.ListOrder.ui.View;


import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.util.RtfUtil;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.DOOrderManager;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.ui.VisaEdit.DOOrderVisaEdit;
import ru.tandemservice.movestudentrmc.base.bo.ListOrder.ListOrderManager;
import ru.tandemservice.movestudentrmc.base.bo.ListOrder.logic.ListPrintDoc;
import ru.tandemservice.movestudentrmc.base.bo.ListOrder.logic.RepresentOrderDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListOrder.ui.Edit.ListOrderEdit;
import ru.tandemservice.movestudentrmc.base.bo.ListOrder.ui.Edit.ListOrderEditUI;
import ru.tandemservice.movestudentrmc.dao.CheckStateUtil;
import ru.tandemservice.movestudentrmc.entity.ListOrder;
import ru.tandemservice.movestudentrmc.entity.Principal2Visa;
import ru.tandemservice.movestudentrmc.entity.catalog.MovestudentOrderStates;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentOrderStatesCodes;
import ru.tandemservice.uni.util.ReportRenderer;

import java.util.Collections;

@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = ListOrderViewUI.ORDER_ID)
})
public class ListOrderViewUI extends UIPresenter
{

    public static final String ORDER_ID = "orderId";

    private Long _orderId;
    private ListOrder _order;

    //    События компонента
    @Override
    public void onComponentRefresh()
    {
        _order = DataAccessServices.dao().get(ListOrder.class, ListOrder.id().s(), _orderId);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (ListOrderView.REPRESENT_ORDER_DS.equals(dataSource.getName()))
        {

            dataSource.put(RepresentOrderDSHandler.ORDER_ID, _orderId);
        }
    }

//    События формы

    public void onClickApprove()
    {
        if (_order.getCommitDate() == null)
            getConfig().getUserContext().getErrorCollector().add(getConfig().getProperty("ui.alertEmptyCommitedDate"));

        if (_order.getNumber() == null)
            getConfig().getUserContext().getErrorCollector().add(getConfig().getProperty("ui.alertEmptyOrderNumber"));

        if (getConfig().getUserContext().getErrorCollector().hasErrors())
            return;

        CheckStateUtil.checkStateOrder(_order, Collections.singletonList(MovestudentOrderStatesCodes.CODE_1), _uiSupport.getSession());

        _order.setState(DataAccessServices.dao().get(MovestudentOrderStates.class, MovestudentOrderStates.code().s(), "2"));

        ListOrderManager.instance().modifyDao().updateState(_order);
    }

    public void onClickFormative()
    {

        CheckStateUtil.checkStateOrder(_order, Collections.singletonList(MovestudentOrderStatesCodes.CODE_2), _uiSupport.getSession());

        _order.setState(DataAccessServices.dao().get(MovestudentOrderStates.class, MovestudentOrderStates.code().s(), "1"));

        ListOrderManager.instance().modifyDao().updateState(_order);
    }

    public void onClickDoApprove()
    {

        CheckStateUtil.checkStateOrder(_order, Collections.singletonList(MovestudentOrderStatesCodes.CODE_2), _uiSupport.getSession());

        _order.setState(DataAccessServices.dao().get(MovestudentOrderStates.class, MovestudentOrderStates.code().s(), "3"));

        //при согласовании сохраним печатную форму приказа
        final RtfDocument docMain = ListPrintDoc.creationListPrintDocOrder(getOrder());
        if (docMain != null)
        {
            _order.setDocument(RtfUtil.toByteArray(docMain));
        }

        ListOrderManager.instance().modifyDao().updateState(_order);
        //сохраним печатные формы выписок
        ListOrderManager.instance().modifyDao().saveExtractsText(_order);
    }

    public void onClickNotApprove()
    {

        CheckStateUtil.checkStateOrder(_order, Collections.singletonList(MovestudentOrderStatesCodes.CODE_3), _uiSupport.getSession());

        _order.setState(DataAccessServices.dao().get(MovestudentOrderStates.class, MovestudentOrderStates.code().s(), "2"));
        //отменили согласование - удаляем печатную форму
        _order.setDocument(null);
        ListOrderManager.instance().modifyDao().updateState(_order);
        //удалим печатные формы выписок
        ListOrderManager.instance().modifyDao().deleteExtractsText(_order);
    }

    public void onClickCommit()
    {

        CheckStateUtil.checkStateOrder(_order, Collections.singletonList(MovestudentOrderStatesCodes.CODE_3), _uiSupport.getSession());

        ErrorCollector error = getConfig().getUserContext().getErrorCollector();

        _order.setCommitDateSystem(getSupport().getCurrentDate());

        ListOrderManager.instance().modifyDao().doCommit(_order, error);
    }

    public void onClickRollback()
    {

        CheckStateUtil.checkStateOrder(_order, Collections.singletonList(MovestudentOrderStatesCodes.CODE_5), _uiSupport.getSession());

        ErrorCollector error = getConfig().getUserContext().getErrorCollector();

        ListOrderManager.instance().modifyDao().doRollback(_order, error);
    }

    public void onClickPrint()
    {
        try
        {
            IDocumentRenderer doc = documentRenderer();
            if (doc != null)
                BusinessComponentUtils.downloadDocument(doc, true);
        } catch (Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    /**
     * Вызываем компонент редактирования данных полей визирующих лиц
     */
    public void onEditVisaFields()
    {
        _uiActivation.asRegionDialog(DOOrderVisaEdit.class)
                .parameter("orderId", _orderId)
                .activate();
    }

    public void onClickEdit()
    {
        _uiActivation.asCurrent(ListOrderEdit.class)
                .parameter(ListOrderEditUI.ORDER_ID, _orderId)
                .activate();
    }

    //    Вспомогательные функции
    private IDocumentRenderer documentRenderer()
    {
        if (_order.getDocument() == null)
        {

            final RtfDocument docMain = ListPrintDoc.creationListPrintDocOrder(getOrder());
            if (docMain != null)
            {
                return new ReportRenderer("Списочный приказ.rtf", docMain, false);
            } else
            {
                return null;
            }
        } else
        {
            return new ReportRenderer("Списочный приказ.rtf", _order.getDocument(), false);
        }
    }

    //    Getters and Setters
    public Principal2Visa getPrincipal2Visa()
    {
        return DOOrderManager.instance().visaDao().getPrincipal2Visa();
    }

    public Long getOrderId()
    {
        return _orderId;
    }

    public void setOrderId(Long orderId)
    {
        _orderId = orderId;
    }

    public ListOrder getOrder()
    {
        return _order;
    }

    public void setOrder(ListOrder order)
    {
        _order = order;
    }
}
