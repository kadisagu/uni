/* $Id$ */
package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic;

import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Multimap;
import com.google.common.collect.TreeMultimap;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.shared.commonbase.base.util.CommonBaseFilterUtil;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudentrmc.docord.IListPrintDoc;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListRepresentAdmissionToPractice;
import ru.tandemservice.movestudentrmc.entity.ListRepresentPractice;
import ru.tandemservice.movestudentrmc.entity.StudentPracticeData;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Andrey Andreev
 * @since 22.10.2015
 */
public class ListRepresentPracticePrintDoc implements IListPrintDoc
{
    private Map<Student, OrderParagraphInfo> hashMap = new HashMap<>();

    private RtfDocument templateParag;

    private final String headerCode;
    private final String paragraphCode;

    public ListRepresentPracticePrintDoc(String headerCode, String paragraphCode)
    {
        this.headerCode = headerCode;
        this.paragraphCode = paragraphCode;
    }

    public RtfDocument createDocument(List<? extends ListRepresent> listOfRepresents)
    {
        TemplateDocument templateDocument;

        templateDocument = IUniBaseDao.instance.get().getCatalogItem(TemplateDocument.class, paragraphCode);
        templateParag = new RtfReader().read(templateDocument.getContent());

        Multimap<OrgUnit, StudentPracticeData> studentsByOrgUnits = getStudentDataByOrgUnits(listOfRepresents);
        int formCounter = 1;
        List<IRtfElement> paragraphsList = new ArrayList<>();
        for (OrgUnit orgUnit : studentsByOrgUnits.keySet()) {
            paragraphsList.addAll(printByOrgUnit(formCounter++, orgUnit, studentsByOrgUnits.get(orgUnit)).getElementList());
        }


        templateDocument = IUniBaseDao.instance.get().getCatalogItem(TemplateDocument.class, headerCode);
        RtfDocument document = new RtfReader().read(templateDocument.getContent());
        document.setSettings(templateParag.getSettings());

        headerModify(new RtfInjectModifier()).put("representTitle", listOfRepresents.get(0).getRepresentationType().getTitle()).modify(document);


        int paragLabelIndex = document.getElementList().indexOf(UniRtfUtil.findElement(document.getElementList(), "parag"));
        document.getElementList().addAll(paragLabelIndex, paragraphsList);
        document.addElement(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));

        SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Arrays.asList("parag", "tables", "footer", "tables"), false, false);
        return document;
    }

    private RtfDocument printByOrgUnit(int formCounter, OrgUnit orgUnit, Collection<StudentPracticeData> studentsByOrgUnit)
    {

        RtfDocument orgUnitParagraph = RtfBean.getElementFactory().createRtfDocument();
        orgUnitParagraph.setHeader(templateParag.getHeader());
        orgUnitParagraph.setSettings(templateParag.getSettings());

        orgUnitParagraph.addElement(RtfBean.getElementFactory().createRtfControl(IRtfData.TAB));
        orgUnitParagraph.addElement(RtfBean.getElementFactory().createRtfText(String.valueOf(formCounter) + "."));
        orgUnitParagraph.addElement(RtfBean.getElementFactory().createRtfControl(IRtfData.TAB));
        orgUnitParagraph.addElement(RtfBean.getElementFactory().createRtfText("По " + orgUnit.getDativeCaseTitle()));
        orgUnitParagraph.addElement(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));


        Multimap<Key, StudentPracticeData> studentsByData = TreeMultimap.create(
                Key.COMPARATOR,
                (Comparator<StudentPracticeData>) (o1, o2) -> ComparisonChain.start()
                        .compare(o1.getStudent().getFullFio(), o2.getStudent().getFullFio())
                        .compare(o1.getId(), o2.getId())
                        .result());
        studentsByOrgUnit.stream().forEach(data -> studentsByData.put(new Key(data), data));
        int count = 1;
        for (Key key : studentsByData.keySet()) {
            orgUnitParagraph.getElementList().addAll(printParagraph(formCounter, count++, studentsByData.get(key)).getElementList());
        }

        return orgUnitParagraph;
    }

    private RtfDocument printParagraph(int formCounter, int count, Collection<StudentPracticeData> studentPracticeDatas)
    {
        studentPracticeDatas.stream().forEach(data -> hashMap.put(data.getStudent(), new OrderParagraphInfo(formCounter, count)));

        RtfDocument paragraph = templateParag.getClone();

        StudentPracticeData first = studentPracticeDatas.iterator().next();
        Student student = first.getStudent();
        ListRepresent represent = first.getListRepresent();
        RtfInjectModifier im = new RtfInjectModifier()
                .put("i", String.valueOf(formCounter).trim())
                .put("j", String.valueOf(count).trim());

        paragraphModify(im, represent, student).modify(paragraph);

        List<String[]> rowList = new ArrayList<>();
        int rowCount = 0;
        for (StudentPracticeData data : studentPracticeDatas) {
            rowList.add(getStudentRow(data, ++rowCount));
            //--------------------------------------------------------------------------
            //Порядковый номер студента в таблице параграфа
            hashMap.get(data.getStudent()).setStudentNumber(rowCount);
            //--------------------------------------------------------------------------
        }

        new RtfTableModifier().put("T", rowList.toArray(new String[][]{})).modify(paragraph);

        return paragraph;
    }


    private Multimap<OrgUnit, StudentPracticeData> getStudentDataByOrgUnits(List<? extends ListRepresent> listOfRepresents)
    {
        String spd_alias = "spd";
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(StudentPracticeData.class, spd_alias, true)
                .column(property(spd_alias, StudentPracticeData.student().educationOrgUnit().formativeOrgUnit()))
                .column(property(spd_alias));
        CommonBaseFilterUtil.applySelectFilter(dql, spd_alias, StudentPracticeData.listRepresent().id(), listOfRepresents);
        List<Object[]> studentDataRawList = IUniBaseDao.instance.get().getList(dql);

        Multimap<OrgUnit, StudentPracticeData> data = TreeMultimap.create(
                (o1, o2) -> ComparisonChain.start().compare(o1.getTitle(), o2.getTitle()).compare(o1.getId(), o2.getId()).result(),
                (o1, o2) -> o1.getId().compareTo(o2.getId()));
        studentDataRawList.stream()
                .filter(raw -> (((StudentPracticeData) raw[1]).getListRepresent() instanceof ListRepresentPractice ||
                        ((StudentPracticeData) raw[1]).getListRepresent() instanceof ListRepresentAdmissionToPractice))
                .forEach(raw -> data.put((OrgUnit) raw[0], (StudentPracticeData) raw[1]));

        return data;
    }

    private String[] getStudentRow(StudentPracticeData data, int rowCounter)
    {
        String[] row = new String[6];
        row[0] = "" + rowCounter;
        row[1] = data.getStudent().getFullFio();
        row[2] = UtilPrintSupport.getCompensationTypeStr(data.getStudent().getCompensationType());
        row[3] = data.getInnerAdvisor() == null ? "" : data.getInnerAdvisor().getFullFio();
        row[4] = data.getPracticeBase() == null ? "" : data.getPracticeBase().getTitle();
        row[5] = data.getOuterAdvisor();

        return row;
    }

    private static class Key
    {
        private final Long devFormID;
        private final Long courseID;
        private final Long eduLvlID;
        private final Long representID;

        public Key(StudentPracticeData data)
        {
            Student student = data.getStudent();

            this.devFormID = student.getEducationOrgUnit().getDevelopForm().getId();
            this.courseID = student.getCourse().getId();
            this.eduLvlID = student.getEducationOrgUnit().getEducationLevelHighSchool().getId();
            this.representID = data.getListRepresent().getId();
        }

        public static Comparator<Key> COMPARATOR = (o1, o2) ->
                ComparisonChain.start()
                        .compare(o1.devFormID, o2.devFormID)
                        .compare(o1.courseID, o2.courseID)
                        .compare(o1.eduLvlID, o2.eduLvlID)
                        .compare(o1.representID, o2.representID)
                        .result();
    }

    protected RtfInjectModifier headerModify(RtfInjectModifier im)
    {
        return im;
    }

    protected RtfInjectModifier paragraphModify(RtfInjectModifier im, ListRepresent represent, Student student)
    {
        return im;
    }


    @Override
    public Map<Student, OrderParagraphInfo> getParagInfomap()
    {
        return hashMap;
    }
}