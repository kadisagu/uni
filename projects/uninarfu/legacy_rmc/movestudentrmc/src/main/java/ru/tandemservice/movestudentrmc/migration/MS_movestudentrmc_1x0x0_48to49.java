package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_movestudentrmc_1x0x0_48to49 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.12"),
                        new ScriptDependency("org.tandemframework.shared", "1.2.4"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.2.4")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность listRepresentProfileTransfer

        // создано обязательное свойство dateBeginingTransfer
        {
            // создать колонку
            tool.createColumn("listrepresentprofiletransfer_t", new DBColumn("datebeginingtransfer_p", DBType.TIMESTAMP));

            // задать значение по умолчанию
            Timestamp defaultDateBeginingTransfer = new Timestamp(new Date().getTime());
            tool.executeUpdate("update listrepresentprofiletransfer_t set datebeginingtransfer_p=? where datebeginingtransfer_p is null", defaultDateBeginingTransfer);

            // сделать колонку NOT NULL
            tool.setColumnNullable("listrepresentprofiletransfer_t", "datebeginingtransfer_p", false);

        }


    }
}