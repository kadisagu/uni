package ru.tandemservice.movestudentrmc.base.bo.DORepresentAdmissionAttendClasses;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentAdmissionAttendClasses.logic.DORepresentAdmissionAttendClassesDAO;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentAdmissionAttendClasses.ui.Edit.DORepresentAdmissionAttendClassesEdit;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentAdmissionAttendClasses.ui.View.DORepresentAdmissionAttendClassesView;
import ru.tandemservice.movestudentrmc.docord.IDOObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IDOObjectModifyDAO;

@Configuration
public class DORepresentAdmissionAttendClassesManager extends BusinessObjectManager implements IDOObjectByRepresentTypeManager {

    public static DORepresentAdmissionAttendClassesManager instance() {
        return instance(DORepresentAdmissionAttendClassesManager.class);
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return DORepresentAdmissionAttendClassesEdit.class;
    }

    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return DORepresentAdmissionAttendClassesView.class;
    }

    @Bean
    public IDOObjectModifyDAO getDOObjectModifyDAO() {
        return new DORepresentAdmissionAttendClassesDAO();
    }
}
