package ru.tandemservice.movestudentrmc.component.settings.OrderCategoryToRepresentationSettings;

import ru.tandemservice.uni.component.settings.abstractRelation.AbstractRelationList.AbstractRelationListDAO;

public class DAO extends AbstractRelationListDAO<Model> implements IDAO {

    @Override
    protected String getFirstObjectEntityName() {
        return "ru.tandemservice.movestudentrmc.entity.catalog.OrderCategory";
    }

}
