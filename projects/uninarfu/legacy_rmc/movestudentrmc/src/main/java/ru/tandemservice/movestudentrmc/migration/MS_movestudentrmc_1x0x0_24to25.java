package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

//RT:2342
public class MS_movestudentrmc_1x0x0_24to25 extends IndependentMigrationScript {
    private DBTool tool;
    private IEntityMeta meta;

    @Override
    public void run(DBTool tool) throws Exception {
        meta = EntityRuntime.getMeta(RepresentWeekendOut.class);
        if (!tool.tableExists(meta.getTableName()))
            return;

        this.tool = tool;
        process();
    }

/*
select
	rep.id,
	rep.COURSEOLD_ID,
	rel.COURSESTR_P,
	c.id
from
	REPRESENTWEEKENDOUT_T rep
	inner join DOCREPRESENTSTUDENTBASE_T rel on rel.REPRESENTATION_ID=rep.ID
	inner join course_t c on c.intvalue_p=rel.COURSESTR_P
where
	rep.COURSEOLD_ID is null
 */

    private void process() throws Exception {
        String sql = ""
                + " select"
                + " rep.id,"
                + " rep.COURSEOLD_ID,"
                + " rel.COURSESTR_P,"
                + " c.id"
                + " from"
                + " REPRESENTWEEKENDOUT_T rep"
                + " inner join DOCREPRESENTSTUDENTBASE_T rel on rel.REPRESENTATION_ID=rep.ID"
                + " inner join course_t c on c.intvalue_p=rel.COURSESTR_P"
                + " where"
                + " rep.COURSEOLD_ID is null";

        PreparedStatement pst = this.tool.getConnection().prepareStatement("update " + this.meta.getTableName() + " set COURSEOLD_ID=? where ID=?");
        ResultSet rs = this.tool.getConnection().createStatement().executeQuery(sql);

        while (rs.next()) {
            Long id = rs.getLong(1);
            Long courseId = rs.getLong(4);

            pst.setLong(1, courseId);
            pst.setLong(2, id);
            pst.executeUpdate();
        }
        rs.close();
    }
}
