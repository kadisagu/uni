package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.RepresentRecertificationTraining;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * О перезачете/переаттестации учебных дисциплин
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RepresentRecertificationTrainingGen extends Representation
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.RepresentRecertificationTraining";
    public static final String ENTITY_NAME = "representRecertificationTraining";
    public static final int VERSION_HASH = 351289080;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDUCATION_YEAR = "educationYear";
    public static final String P_INSTITUTE = "institute";

    private EducationYear _educationYear;     // Учебный год
    private String _institute;     // Образовательное учреждение, в котором обучался

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    /**
     * @param educationYear Учебный год. Свойство не может быть null.
     */
    public void setEducationYear(EducationYear educationYear)
    {
        dirty(_educationYear, educationYear);
        _educationYear = educationYear;
    }

    /**
     * @return Образовательное учреждение, в котором обучался. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getInstitute()
    {
        return _institute;
    }

    /**
     * @param institute Образовательное учреждение, в котором обучался. Свойство не может быть null.
     */
    public void setInstitute(String institute)
    {
        dirty(_institute, institute);
        _institute = institute;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof RepresentRecertificationTrainingGen)
        {
            setEducationYear(((RepresentRecertificationTraining)another).getEducationYear());
            setInstitute(((RepresentRecertificationTraining)another).getInstitute());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RepresentRecertificationTrainingGen> extends Representation.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RepresentRecertificationTraining.class;
        }

        public T newInstance()
        {
            return (T) new RepresentRecertificationTraining();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                    return obj.getEducationYear();
                case "institute":
                    return obj.getInstitute();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "educationYear":
                    obj.setEducationYear((EducationYear) value);
                    return;
                case "institute":
                    obj.setInstitute((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                        return true;
                case "institute":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                    return true;
                case "institute":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                    return EducationYear.class;
                case "institute":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RepresentRecertificationTraining> _dslPath = new Path<RepresentRecertificationTraining>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RepresentRecertificationTraining");
    }
            

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentRecertificationTraining#getEducationYear()
     */
    public static EducationYear.Path<EducationYear> educationYear()
    {
        return _dslPath.educationYear();
    }

    /**
     * @return Образовательное учреждение, в котором обучался. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentRecertificationTraining#getInstitute()
     */
    public static PropertyPath<String> institute()
    {
        return _dslPath.institute();
    }

    public static class Path<E extends RepresentRecertificationTraining> extends Representation.Path<E>
    {
        private EducationYear.Path<EducationYear> _educationYear;
        private PropertyPath<String> _institute;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentRecertificationTraining#getEducationYear()
     */
        public EducationYear.Path<EducationYear> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new EducationYear.Path<EducationYear>(L_EDUCATION_YEAR, this);
            return _educationYear;
        }

    /**
     * @return Образовательное учреждение, в котором обучался. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentRecertificationTraining#getInstitute()
     */
        public PropertyPath<String> institute()
        {
            if(_institute == null )
                _institute = new PropertyPath<String>(RepresentRecertificationTrainingGen.P_INSTITUTE, this);
            return _institute;
        }

        public Class getEntityClass()
        {
            return RepresentRecertificationTraining.class;
        }

        public String getEntityName()
        {
            return "representRecertificationTraining";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
