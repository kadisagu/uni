package ru.tandemservice.movestudentrmc.dao.check;

public class CheckStatusVacationWithVisits extends AbstractCheckStudentStatus {

    /**
     * Статус студента - отпуск с посещениями
     */
    private final static String STUDENT_STATUS_VACATION_WITH_VISITS = "7";

    @Override
    protected String getStatusCode() {
        return STUDENT_STATUS_VACATION_WITH_VISITS;
    }

}
