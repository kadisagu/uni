package ru.tandemservice.movestudentrmc.component.settings.grantrelations.RelGrantViewOrgUnitPub;

import ru.tandemservice.uni.component.settings.abstractRelation.AbstractRelationPub.AbstractRelationPubController;

public class Controller extends AbstractRelationPubController<IDAO, Model> {

    @Override
    protected String getDeleteRowPermissionKey() {
        return null;
    }

    @Override
    protected String getRelationAddingComponentName() {

        return "ru.tandemservice.movestudentrmc.component.settings.grantrelations.RelGrantViewOrgUnitAdd";
    }

}
