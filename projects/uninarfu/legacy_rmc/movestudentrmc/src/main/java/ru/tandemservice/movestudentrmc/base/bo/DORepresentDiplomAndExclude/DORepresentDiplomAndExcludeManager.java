package ru.tandemservice.movestudentrmc.base.bo.DORepresentDiplomAndExclude;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentDiplomAndExclude.logic.DORepresentDiplomAndExcludeDAO;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentDiplomAndExclude.ui.Edit.DORepresentDiplomAndExcludeEdit;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentDiplomAndExclude.ui.View.DORepresentDiplomAndExcludeView;
import ru.tandemservice.movestudentrmc.docord.IDOObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IDOObjectModifyDAO;

@Configuration
public class DORepresentDiplomAndExcludeManager extends BusinessObjectManager implements IDOObjectByRepresentTypeManager {

    public static DORepresentDiplomAndExcludeManager instance()
    {
        return instance(DORepresentDiplomAndExcludeManager.class);
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return DORepresentDiplomAndExcludeEdit.class;
    }

    @Bean
    public IDOObjectModifyDAO getDOObjectModifyDAO() {
        return new DORepresentDiplomAndExcludeDAO();
    }

    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return DORepresentDiplomAndExcludeView.class;
    }
}
