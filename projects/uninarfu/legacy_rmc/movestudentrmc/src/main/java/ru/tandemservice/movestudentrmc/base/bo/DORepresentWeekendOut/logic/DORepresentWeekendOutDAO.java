package ru.tandemservice.movestudentrmc.base.bo.DORepresentWeekendOut.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;

import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentDAO;
import ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab.MonthWrapper;
import ru.tandemservice.movestudentrmc.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.DocumentOrder;
import ru.tandemservice.movestudentrmc.entity.RelStudentCustomStateRepresent;
import ru.tandemservice.movestudentrmc.entity.RepresentGrantsOld;
import ru.tandemservice.movestudentrmc.entity.RepresentWeekend;
import ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntity;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.movestudentrmc.entity.catalog.StuGrantStatus;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentGrantTypeCodes;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.StuGrantStatusCodes;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.StudentCustomStateCICodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

//import java.sql.Date;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
//------------------------------------------------------------------
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import org.tandemframework.rtf.util.RtfString;
//------------------------------------------------------------------

public class DORepresentWeekendOutDAO extends AbstractDORepresentDAO
{

    @Override
    public Representation createRepresentObject(RepresentationType type, List<Student> studentList) {

        Student student = studentList.get(0);
        RepresentWeekendOut represent = new RepresentWeekendOut();
        represent.setType(type);

        represent.setEducationOrgUnitOld(student.getEducationOrgUnit());
        represent.setGroupOld(student.getGroup());
        represent.setCourseOld(student.getCourse());

        return represent;
    }

    @Override
    public boolean doCommit(Representation represent, ErrorCollector error) {
        if (!super.doCommit(represent, error))
            return false;
        RepresentWeekendOut rep = (RepresentWeekendOut) represent;
        List<DocRepresentStudentBase> studentList = getStudentList(represent);
        Student student = studentList.get(0).getStudent();

        student.setStatus(DataAccessServices.dao().get(StudentStatus.class, StudentStatus.code().s(), "1"));

        student.setGroup(((RepresentWeekendOut) represent).getNewGroup());
        student.setCourse(((RepresentWeekendOut) represent).getNewCourse());

        if (rep.getEducationOrgUnit() != null)
            student.setEducationOrgUnit(rep.getEducationOrgUnit());
        else
            student.setEducationOrgUnit(rep.getNewGroup().getEducationOrgUnit());

        baseUpdate(student);

        //--------------------------------------------------------------------------------------------------------------------------------
        //При досрочном выходе из академического отпуска
        if (rep.getAhead())
        {

            //При досрочном выходе из академического отпуска изменяем дату окончания дополнительного статуса Академический отпуск по мед. показаниям
            DQLSelectBuilder builder = new DQLSelectBuilder()
                    .fromEntity(StudentCustomState.class, "st")
                    .where(eq(property(StudentCustomState.student().id().fromAlias("st")), value(student.getId())))
                    .where(eq(property(StudentCustomState.customState().code().fromAlias("st")), value(StudentCustomStateCICodes.WEEKEND_HONEY_INDICATIONS)))
                    .where(ge(property(StudentCustomState.endDate().fromAlias("st")), valueDate(rep.getStartDate())))
                    .where(le(property(StudentCustomState.beginDate().fromAlias("st")), valueDate(rep.getStartDate())));

            List<StudentCustomState> customStateList = builder.createStatement(getSession()).list();

            for (StudentCustomState customState : customStateList)
            {

                Calendar endWeekendDate = Calendar.getInstance();
                endWeekendDate.setTime(represent.getStartDate());
                endWeekendDate.add(Calendar.DAY_OF_YEAR, -1);

                customState.setEndDate(endWeekendDate.getTime());
                baseUpdate(customState);
            }

            //При досрочном выходе из академического отпуска отменяем ежемесячные компенсационные выплаты
            StuGrantStatus status = UniDaoFacade.getCoreDao().get(StuGrantStatus.class, StuGrantStatus.code(), StuGrantStatusCodes.CODE_3);

            DocumentOrder order = getOrder(represent);

            GrantView grant = UniDaoFacade.getCoreDao().getByCode(GrantView.class, "206");    //ежемесячная компенсационная выплата студентам, находящимся в академическом отпуске по медицинским показаниям

            //Последний учебный год назначения выплат
            DQLSelectBuilder lastYearBuilder = new DQLSelectBuilder()
                    .fromEntity(StudentGrantEntity.class, "sge")
                    .where(eq(property(StudentGrantEntity.status().code().fromAlias("sge")), value(StuGrantStatusCodes.CODE_1)))
                    .where(eq(property(StudentGrantEntity.student().fromAlias("sge")), value(student)))
                    .where(eq(property(StudentGrantEntity.view().fromAlias("sge")), value(grant)))
                    .column(property(StudentGrantEntity.eduYear().intValue().fromAlias("sge")))
                    .order(property(StudentGrantEntity.eduYear().intValue().fromAlias("sge")), OrderDirection.desc)
                    .top(1);
            int lastYear = lastYearBuilder.createStatement(getSession()).uniqueResult();

            //Последний день последнего учебного года назначения выплат
            Calendar lastYearDate = Calendar.getInstance();
            lastYearDate.set(lastYear + 1, Calendar.AUGUST, 31, 0, 0, 0);    //31.08.year+1
            List<String> monthList = MonthWrapper.getMonthsList(rep.getStartDate(), lastYearDate.getTime());

            DQLSelectBuilder grantBuilder = new DQLSelectBuilder().fromEntity(StudentGrantEntity.class, "sge")
                    .where(DQLExpressions.eq(DQLExpressions.property(StudentGrantEntity.view().fromAlias("sge")),
                                             DQLExpressions.value(grant)))
                    .where(DQLExpressions.eq(DQLExpressions.property(StudentGrantEntity.status().code().fromAlias("sge")),
                                             DQLExpressions.value(StuGrantStatusCodes.CODE_1)))
                    .where(DQLExpressions.eq(DQLExpressions.property(StudentGrantEntity.student().id().fromAlias("sge")),
                                             DQLExpressions.value(student.getId())))
                    .where(DQLExpressions.in(DQLExpressions.property(StudentGrantEntity.month().fromAlias("sge")), monthList))
                    .where(DQLExpressions.or(DQLExpressions.eq(DQLExpressions.property(StudentGrantEntity.tmp().fromAlias("sge")),
                                                               DQLExpressions.value(Boolean.FALSE)),
                                             DQLExpressions.isNull(DQLExpressions.property(StudentGrantEntity.tmp().fromAlias("sge")))));

            List<StudentGrantEntity> sgelist = UniDaoFacade.getCoreDao().getList(grantBuilder);

            for (StudentGrantEntity sge : sgelist)
            {
                //сохраним старые данные для отката
                RepresentGrantsOld grantsOld = new RepresentGrantsOld();
                grantsOld.setRepresentation(represent);
                grantsOld.setStudentGrantEntityId(sge.getId());
                grantsOld.setOrderDateOld(sge.getOrderDate());
                grantsOld.setOrderNumberOld(sge.getOrderNumber());
                getSession().saveOrUpdate(grantsOld);
                //сохраним данные приказа
                sge.setStatus(status);
                sge.setOrderDate(order.getCommitDate());
                sge.setOrderNumber(order.getNumber());
                getSession().saveOrUpdate(sge);
            }

            MoveStudentDaoFacade.getGrantDAO().saveGrantHistory(sgelist, represent, RepresentGrantTypeCodes.CANCEL);

        }

        //--------------------------------------------------------------------------------------------------------------------------------
        
        return true;
    }

    @Override
    public boolean doRollback(Representation represent, ErrorCollector error) {
        if (!super.doRollback(represent, error))
            return false;

        RepresentWeekendOut rep = (RepresentWeekendOut) represent;

        List<DocRepresentStudentBase> studentList = getStudentList(represent);
        Student student = studentList.get(0).getStudent();

        if (represent.getStatusOld() == null)
            error.add("Не найден старый статус студента");

        Group group = null;
        if (((RepresentWeekendOut) represent).getGroupOld() != null) {
            group = ((RepresentWeekendOut) represent).getGroupOld();
            if (group == null) {
                error.add("Не найдена старая группа");
                return false;
            }
        }

        if (((RepresentWeekendOut) represent).getCourseOld() == null)
            error.add("Не найден старый курс");

        EducationOrgUnit edu = null;
        if (rep.getEducationOrgUnitOld() != null)
            edu = rep.getEducationOrgUnitOld();
        else
            error.add("Не найдены старые данные");

        if (!error.isHasFieldErrors()) {
            student.setStatus(represent.getStatusOld());
            student.setGroup(((RepresentWeekendOut) represent).getGroupOld());
            student.setCourse(((RepresentWeekendOut) represent).getCourseOld());

            student.setEducationOrgUnit(edu);

            getSession().saveOrUpdate(student);

            //------------------------------------------------------------------------------------------------------------------------------
            //При откате предствалений с досрочным выходом из академического отпуска
            if (rep.getAhead())
            {

                //Восстанавливаем дату окончания дополнительного статуса Академический отпуск по мед. показаниям
                Calendar endWeekendDate = Calendar.getInstance();
                endWeekendDate.setTime(rep.getStartDate());
                endWeekendDate.add(Calendar.DAY_OF_YEAR, -1);

                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(StudentCustomState.class, "st")
                        .joinEntity("st", DQLJoinType.inner, RelStudentCustomStateRepresent.class, "rel", eq(property(StudentCustomState.id().fromAlias("st")), property(RelStudentCustomStateRepresent.studentCustomState().id().fromAlias("rel"))))
                        .joinEntity("rel", DQLJoinType.inner, RepresentWeekend.class, "rep", eq(property(RepresentWeekend.id().fromAlias("rep")), property(RelStudentCustomStateRepresent.representation().id().fromAlias("rel"))))
                        .where(eq(property(StudentCustomState.student().id().fromAlias("st")), value(student.getId())))
                        .where(eq(property(StudentCustomState.customState().code().fromAlias("st")), value(StudentCustomStateCICodes.WEEKEND_HONEY_INDICATIONS)))
                        .where(le(property(StudentCustomState.beginDate().fromAlias("st")), valueDate(rep.getStartDate())))
                        .where(eq(property(StudentCustomState.endDate().fromAlias("st")), valueDate(endWeekendDate.getTime())))
                        .column(property(StudentCustomState.id().fromAlias("st")))
                        .column(property(RepresentWeekend.id().fromAlias("rep")));

                List<Object[]> idsList = builder.createStatement(getSession()).list();

                for (Object[] ids : idsList)
                {

                    StudentCustomState customState = DataAccessServices.dao().get(StudentCustomState.class, (Long) ids[0]);
                    RepresentWeekend repWeekend = DataAccessServices.dao().get(RepresentWeekend.class, (Long) ids[1]);
                    customState.setEndDate(repWeekend.getEndDate());

                    baseUpdate(customState);

                }

                //Восстанавливаем назначение ежемесячных компенсационных выплат
                StuGrantStatus status = UniDaoFacade.getCoreDao().get(StuGrantStatus.class, StuGrantStatus.code(), StuGrantStatusCodes.CODE_1);

                List<RepresentGrantsOld> grantsOlds = UniDaoFacade.getCoreDao().getList(RepresentGrantsOld.class, RepresentGrantsOld.representation(), represent);

                for (RepresentGrantsOld grantOld : grantsOlds)
                {
                    StudentGrantEntity sge = UniDaoFacade.getCoreDao().get(grantOld.getStudentGrantEntityId());

                    sge.setStatus(status);
                    sge.setOrderDate(grantOld.getOrderDateOld());
                    sge.setOrderNumber(grantOld.getOrderNumberOld());

                    getSession().saveOrUpdate(sge);

                    getSession().delete(grantOld);
                }

                MoveStudentDaoFacade.getGrantDAO().deleteGrantHistory(represent);

            }
            
            //------------------------------------------------------------------------------------------------------------------------------
        }
        else
            return false;

        return true;
    }

    @Override
    public void buildBodyExtract(Representation representationBase, String itemFac, RtfDocument docCommon) {

        RepresentWeekendOut represent = (RepresentWeekendOut) representationBase;


        RtfInjectModifier modifier = new RtfInjectModifier();

        UtilPrintSupport.injectParNumModifier(modifier, itemFac);

        UtilPrintSupport.injectModifierExtract(modifier, represent);

        modifier.put("NewGroupNumber", represent.getNewGroup().getTitle());
        modifier.put("NewOKSO", UtilPrintSupport.getEduLevelCodeWithQualification(represent.getNewGroup().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel()));

        if (represent.getAhead())
            modifier.put("Before", "досрочно ");
        else
            modifier.put("Before", "");

        RtfSearchResult searchResult = UniRtfUtil.findRtfMark(docCommon, "Ending");
        if (searchResult.isFound()) {
            IRtfText text = RtfBean.getElementFactory().createRtfText("\\fs28\\f0 им");
            text.setRaw(true);
            searchResult.getElementList().set(searchResult.getIndex(), text);
        }

        modifier.modify(docCommon);
    }

    @Override
    public void buildBodyRepresent(Representation representationBase, RtfDocument document, AtomicInteger parNumber, AtomicInteger extractNumber) {
        super.buildBodyRepresent(representationBase, document, parNumber, extractNumber);
        RepresentWeekendOut represent = (RepresentWeekendOut) representationBase;

        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        Student student = UtilPrintSupport.getStudent(represent);
        RtfInjectModifier modifier = new RtfInjectModifier();

        boolean isMaleSex = student.getPerson().isMale();

        UtilPrintSupport.injectModifierRepresent(modifier, docRepresent, student, represent.getEducationOrgUnitOld(), represent.getCourseOld(), represent.getGroupOld());

        //modifier.put("studentTitle", PersonManager.instance().declinationDao().getDeclinationFIO(student.getPerson().getIdentityCard(), GrammaCase.ACCUSATIVE));
        modifier.put("studentTitle", UtilPrintSupport.printSeparatedFIO(student.getPerson().getIdentityCard(), GrammaCase.ACCUSATIVE, student.getPerson().isMale()));

        modifier.put("studentSex", student.getPerson().isMale() ? "студента" : "студентку");

        modifier.put("courseNew", represent.getNewCourse().getTitle());
        modifier.put("groupNew", represent.getNewGroup().getTitle());
        modifier.put("highSchoolNew", UtilPrintSupport.getHighLevelSchoolTypeString(represent.getNewEducationLevelsHighSchool()));

        //modifier.put("NewOKSO", represent.getNewGroup().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getInheritedOksoPrefix());
        if (represent.getAhead())
            modifier.put("Before", "досрочно ");
        else
            modifier.put("Before", "");

        String maleSex = "";

        if (isMaleSex)
            maleSex = "\\fs28\\f0 им";
        else
            maleSex = "\\fs28\\f0 ей";

        RtfSearchResult searchResult = UniRtfUtil.findRtfMark(document, "Ending");
        if (searchResult.isFound()) {
            IRtfText text = RtfBean.getElementFactory().createRtfText(maleSex);
            text.setRaw(true);
            searchResult.getElementList().set(searchResult.getIndex(), text);
        }

        //------------------------------------------------------------------------------------------------------------------------------------
        //Условия ликвидации академической задолженности или разницы в учебных планах
        RtfString liquidationString = new RtfString();

        if (represent.isDebtLiquidation())
        {

            String day = RussianDateFormatUtils.getDayString(represent.getDebtLiquidationDate(), true);
            String month = CommonBaseDateUtil.getMonthNameDeclined(represent.getDebtLiquidationDate(), GrammaCase.GENITIVE);
            String year = RussianDateFormatUtils.getYearString(represent.getDebtLiquidationDate(), false);

            liquidationString.append(", с ликвидацией академической задолженности");

            if (represent.getDebtLiquidationDate() != null)
            {
                liquidationString
                        .append(" до ")
                        .append(day)
                        .append(IRtfData.SYMBOL_TILDE)
                        .append(month)
                        .append(" ")
                        .append(year)
                        .append(IRtfData.SYMBOL_TILDE)
                        .append("года");
            }

        }

        if (represent.isDifferenceLiquidation())
        {

            String day = RussianDateFormatUtils.getDayString(represent.getDifferenceLiquidationDate(), true);
            String month = CommonBaseDateUtil.getMonthNameDeclined(represent.getDifferenceLiquidationDate(), GrammaCase.GENITIVE);
            String year = RussianDateFormatUtils.getYearString(represent.getDifferenceLiquidationDate(), false);

            liquidationString.append(", с ликвидацией разницы в учебных планах");

            if (represent.getDifferenceLiquidationDate() != null)
            {
                liquidationString
                        .append(" до ")
                        .append(day)
                        .append(IRtfData.SYMBOL_TILDE)
                        .append(month)
                        .append(" ")
                        .append(year)
                        .append(IRtfData.SYMBOL_TILDE)
                        .append("года");
            }

        }

        modifier.put("debtLiquidation", liquidationString);
        //------------------------------------------------------------------------------------------------------------------------------------

        if (StringUtils.isEmpty(modifier.getStringValue("basics")))
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Arrays.asList("basics"), false, false);

        modifier.modify(document);
    }

    @Override
    public void buildBodyDeclaration(Representation representationBase, RtfDocument document) {
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        RepresentWeekendOut represent = (RepresentWeekendOut) representationBase;

        Student student = UtilPrintSupport.getStudent(represent);

        boolean isMaleSex = student.getPerson().isMale();

        RtfInjectModifier im = new RtfInjectModifier();
        RtfTableModifier tm = new RtfTableModifier();
        UtilPrintSupport.injectModifierDeclaration(im, tm, docRepresent);

        this.injectModifier(im, docRepresent);

        if (represent.getAhead())
            im.put("Before", "досрочно ");
        else
            im.put("Before", "");

        String maleSex = "";

        if (isMaleSex)
            maleSex = "\\fs26\\f0 им";
        else
            maleSex = "\\fs26\\f0 ей";

        RtfSearchResult searchResult = UniRtfUtil.findRtfMark(document, "Ending");
        if (searchResult.isFound()) {
            IRtfText text = RtfBean.getElementFactory().createRtfText(maleSex);
            text.setRaw(true);
            searchResult.getElementList().set(searchResult.getIndex(), text);
        }

        im.modify(document);
        tm.modify(document);
    }
}
