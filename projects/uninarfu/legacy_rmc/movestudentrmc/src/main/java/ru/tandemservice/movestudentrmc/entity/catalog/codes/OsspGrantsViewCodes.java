package ru.tandemservice.movestudentrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Вид стипендии ОССП"
 * Имя сущности : osspGrantsView
 * Файл data.xml : movementstudent.data.xml
 */
public interface OsspGrantsViewCodes
{
    /** Константа кода (code) элемента : grantView-1-1 (code). Название (title) : социальная стипендия */
    String GRANT_VIEW_1_1 = "grantView-1-1";
    /** Константа кода (code) элемента : grantView-1-2 (code). Название (title) : государственная стипендия студентам из числа граждан, проходивших военную службу */
    String GRANT_VIEW_1_2 = "grantView-1-2";
    /** Константа кода (code) элемента : grantView-1-3 (code). Название (title) : социальная поддержка */
    String GRANT_VIEW_1_3 = "grantView-1-3";
    /** Константа кода (code) элемента : grantView-1-4 (code). Название (title) : ежегодное пособие детям-сиротам и детям, оставшихся без попечения родителей на приобретение учебной литературы и письменных принадлежностей */
    String GRANT_VIEW_1_4 = "grantView-1-4";
    /** Константа кода (code) элемента : grantView-1-5 (code). Название (title) : стипендия Президента Российской Федерации */
    String GRANT_VIEW_1_5 = "grantView-1-5";
    /** Константа кода (code) элемента : grantView-1-6 (code). Название (title) : стипендия Правительства Российской Федерации */
    String GRANT_VIEW_1_6 = "grantView-1-6";
    /** Константа кода (code) элемента : grantView-1-7 (code). Название (title) : стипендия Губернатора Архангельской области */
    String GRANT_VIEW_1_7 = "grantView-1-7";
    /** Константа кода (code) элемента : grantView-1-8 (code). Название (title) : стипендия имени Е.Т. Гайдара */
    String GRANT_VIEW_1_8 = "grantView-1-8";
    /** Константа кода (code) элемента : grantView-1-9 (code). Название (title) : стипендия имени А.А. Собчака */
    String GRANT_VIEW_1_9 = "grantView-1-9";
    /** Константа кода (code) элемента : grantView-1-10 (code). Название (title) : стипендия имени Д.С. Лихачева */
    String GRANT_VIEW_1_10 = "grantView-1-10";
    /** Константа кода (code) элемента : grantView-1-11 (code). Название (title) : стипендия имени А.И. Солженицына */
    String GRANT_VIEW_1_11 = "grantView-1-11";
    /** Константа кода (code) элемента : grantView-1-12 (code). Название (title) : выплату малочисленным народам Севера */
    String GRANT_VIEW_1_12 = "grantView-1-12";
    /** Константа кода (code) элемента : grantView-1-13 (code). Название (title) : повышенная стипендия I степени */
    String GRANT_VIEW_1_13 = "grantView-1-13";
    /** Константа кода (code) элемента : grantView-1-14 (code). Название (title) : повышенная стипендия II степени */
    String GRANT_VIEW_1_14 = "grantView-1-14";
    /** Константа кода (code) элемента : grantView-1-15 (code). Название (title) : ежегодное пособие аспирантам и докторантам на приобретения научной литературы */
    String GRANT_VIEW_1_15 = "grantView-1-15";
    /** Константа кода (code) элемента : grantView-1-16 (code). Название (title) : стипендия аспирантам и докторантам, на период их болезни продолжительностью свыше одного месяца (очная форма обучения) */
    String GRANT_VIEW_1_16 = "grantView-1-16";

    Set<String> CODES = ImmutableSet.of(GRANT_VIEW_1_1, GRANT_VIEW_1_2, GRANT_VIEW_1_3, GRANT_VIEW_1_4, GRANT_VIEW_1_5, GRANT_VIEW_1_6, GRANT_VIEW_1_7, GRANT_VIEW_1_8, GRANT_VIEW_1_9, GRANT_VIEW_1_10, GRANT_VIEW_1_11, GRANT_VIEW_1_12, GRANT_VIEW_1_13, GRANT_VIEW_1_14, GRANT_VIEW_1_15, GRANT_VIEW_1_16);
}
