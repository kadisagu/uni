package ru.tandemservice.movestudentrmc.base.bo.ListRepresentProfileTransfer.ui.View;


import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.support.AbstractListRepresentViewUI;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentProfileTransfer.logic.ListPrintDoc;
import ru.tandemservice.movestudentrmc.entity.ListRepresentProfileTransfer;

import java.util.Collections;

public class ListRepresentProfileTransferViewUI extends AbstractListRepresentViewUI<ListRepresentProfileTransfer> {

    @Override
    public ListRepresentProfileTransfer getListRepresentObject() {
        return new ListRepresentProfileTransfer();
    }

    @Override
    protected RtfDocument printRepresent() {
        return new ListPrintDoc().creationPrintDocOrder(Collections.singletonList(getListRepresent()));
    }

}
