package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_movestudentrmc_2x11x0_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.1"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.11.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность documentOrder

		// создано свойство signPostGenitive
		{
			// создать колонку
			tool.createColumn("documentorder_t", new DBColumn("signpostgenitive_p", DBType.createVarchar(255)));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность listOrder

		// создано свойство signPostGenitive
		{
			// создать колонку
			tool.createColumn("listorder_t", new DBColumn("signpostgenitive_p", DBType.createVarchar(255)));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность listRepresentCourseTransfer

		// удалено свойство educationYear
		{
			// удалить колонку
			tool.dropColumn("listrepresentcoursetransfer_t", "educationyear_id");

		}

		// создано обязательное свойство conditionally
		{
			// создать колонку
			tool.createColumn("listrepresentcoursetransfer_t", new DBColumn("conditionally_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			tool.executeUpdate("update listrepresentcoursetransfer_t set conditionally_p=? where conditionally_p is null", false);

			// сделать колонку NOT NULL
			tool.setColumnNullable("listrepresentcoursetransfer_t", "conditionally_p", false);

		}

		// создано свойство deadlineDate
		{
			// создать колонку
			tool.createColumn("listrepresentcoursetransfer_t", new DBColumn("deadlinedate_p", DBType.TIMESTAMP));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность principal2Visa

		// создано свойство signPostGenitive
		{
			// создать колонку
			tool.createColumn("principal2visa_t", new DBColumn("signpostgenitive_p", DBType.createVarchar(255)));
		}
    }
}