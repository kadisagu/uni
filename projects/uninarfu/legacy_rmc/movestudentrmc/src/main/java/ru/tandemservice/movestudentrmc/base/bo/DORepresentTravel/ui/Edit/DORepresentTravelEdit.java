package ru.tandemservice.movestudentrmc.base.bo.DORepresentTravel.ui.Edit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.block.BlockListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.ReasonDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.TravelPaymentDataDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.EditBasic.DORepresentBaseEditBasic;

@Configuration
public class DORepresentTravelEdit extends BusinessComponentManager {

    public static final String REASON_DS = "reasonDS";
    public static final String PAYMENT_DATA_DS = "travelPaymentDataDS";
    public static final String COUNTRY_DS = "countryDS";
    public static final String SETTLEMENT_DS = "settlementDS";
    public static final String ORGANIZATION_DS = "organizationDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(REASON_DS, reasonDSHandler()))
                .addDataSource(selectDS(PAYMENT_DATA_DS, travelPaymentDataDSHandler()))
                .addDataSource(selectDS(COUNTRY_DS, countryDSHandler()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(SETTLEMENT_DS, getName(), AddressItem.settlementComboDSHandler(getName())))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler reasonDSHandler()
    {
        return new ReasonDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler travelPaymentDataDSHandler()
    {
        return new TravelPaymentDataDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler countryDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), AddressCountry.class);
    }

    @Bean
    public BlockListExtPoint editBasicBlockListExtPoint()
    {
        return blockListExtPointBuilder(DORepresentTravelEditUI.EDIT_BASIC_BLOCK_LIST)
                .addBlock(componentBlock(DORepresentTravelEditUI.EDIT_BASIC_BLOCK, DORepresentBaseEditBasic.class).parameters("ui:blockParam"))
                .create();
    }
}
