package ru.tandemservice.movestudentrmc.base.bo.DORepresentMakeReprimand;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentMakeReprimand.logic.DORepresentMakeReprimandDAO;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentMakeReprimand.ui.Edit.DORepresentMakeReprimandEdit;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentMakeReprimand.ui.View.DORepresentMakeReprimandView;
import ru.tandemservice.movestudentrmc.docord.IDOObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IDOObjectModifyDAO;

@Configuration
public class DORepresentMakeReprimandManager extends BusinessObjectManager implements IDOObjectByRepresentTypeManager {

    public static DORepresentMakeReprimandManager instance()
    {
        return instance(DORepresentMakeReprimandManager.class);
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return DORepresentMakeReprimandEdit.class;
    }

    @Bean
    public IDOObjectModifyDAO getDOObjectModifyDAO() {
        return new DORepresentMakeReprimandDAO();
    }

    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return DORepresentMakeReprimandView.class;
    }
}
