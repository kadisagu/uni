package ru.tandemservice.movestudentrmc.dao.check;

import ru.tandemservice.movestudentrmc.entity.IStudentGrant;
import ru.tandemservice.movestudentrmc.entity.NarfuDocument;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Date;

public class CheckStudentSocialGrant extends AbstractCheckStudentBenefit {

    /**
     * Код - Член малоимущей семьи
     */
    private final static String INDIGENT_BENEFIT_CODE = "10";
    /**
     * Код документа о льготах - Справка о статусе малоимущего
     */
    private final static String DOCUMENT_BENEFIT_CODE = "10";

    @Override
    protected boolean checkCondition(Student student) {
        Long personId = student.getPerson().getId();

        if (!isIStudentGrantInstance()
                || !containBenefit(personId, INDIGENT_BENEFIT_CODE)
                || !containDocument(student.getId(), DOCUMENT_BENEFIT_CODE))
            return false;

        NarfuDocument document = getDocument(student.getId(), DOCUMENT_BENEFIT_CODE);
        Date beginDate = document.getPeriodDateEnd() != null ? document.getPeriodDateEnd() : null;

        if (beginDate != null
                && ((IStudentGrant) represent).getBeginGrantDate().before(beginDate))
            return true;

        return false;
    }

    protected boolean isIStudentGrantInstance() {
        return (represent instanceof IStudentGrant);
    }

    @Override
    protected String createError(Student student, boolean value) {
        StringBuilder sb = new StringBuilder()
                .append("Студент ")
                .append(student.getPerson().getFullFio())
                .append(" ")
                .append(value ? "не " : "")
                .append("выполняется условие: начало периода назначения стипендии студенту, являющемуся членом малоимущей семьи, должно быть не больше даты окончания срока действия справки о статусе малоимущей семьи");
        return sb.toString();
    }

}
