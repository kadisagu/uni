package ru.tandemservice.movestudentrmc.base.bo.DORepresentTransfer.ui.View;

import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentViewUI;
import ru.tandemservice.movestudentrmc.entity.RepresentTransfer;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

public class DORepresentTransferViewUI extends AbstractDORepresentViewUI {

    public EducationOrgUnit getEducationOrgUnit() {
        RepresentTransfer rep = (RepresentTransfer) this._represent;
        if (rep.getEducationOrgUnit() != null)
            return rep.getEducationOrgUnit();
        else
            return rep.getGroup().getEducationOrgUnit();
    }

}
