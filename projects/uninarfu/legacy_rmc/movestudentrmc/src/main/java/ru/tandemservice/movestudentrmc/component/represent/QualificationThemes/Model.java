package ru.tandemservice.movestudentrmc.component.represent.QualificationThemes;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.StudentQualificationThemes;

import java.util.List;

@Input({
        @Bind(key = "list", binding = "list"),
        @Bind(key = "represent", binding = "represent")
})


public class Model {

    private List<DataWrapper> list;
    private ListRepresent _represent;

    private DynamicListDataSource<DataWrapper> dataSource;

    private StudentQualificationThemes qualificationThemes;

    public DynamicListDataSource<DataWrapper> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<DataWrapper> dataSource) {
        this.dataSource = dataSource;
    }

    public void setRepresent(ListRepresent represent)
    {
        this._represent = represent;
    }

    public ListRepresent getRepresent()
    {
        return this._represent;
    }

    public List<DataWrapper> getList() {
        return list;
    }

    public void setList(List<DataWrapper> list) {
        this.list = list;
    }

}
