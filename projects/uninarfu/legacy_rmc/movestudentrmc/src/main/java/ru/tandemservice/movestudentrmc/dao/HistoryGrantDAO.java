package ru.tandemservice.movestudentrmc.dao;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentGrantType;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentGrantTypeCodes;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentationTypeCodes;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.StuGrantStatusCodes;
import ru.tandemservice.uni.dao.UniBaseDao;

import java.util.*;


public class HistoryGrantDAO extends UniBaseDao implements IHistoryGrantDAO {

    /**
     * регистрируем демона
     */
    public static final SyncDaemon DAEMON = new SyncDaemon(HistoryGrantDAO.class.getName(), 1) {
        @Override
        protected void main()
        {
            if (!FORSE_RUN)
                return;

            try {
                if (!IS_BEAZY) {
                    MoveStudentDaoFacade.getHistoryDAO().fillHistory();
                }
            }
            catch (Exception e) {
                this.logger.error(e.getMessage(), e);
                Debug.exception(e.getMessage(), e);
            }
        }
    };

    public static boolean FORSE_RUN = false;
    public static boolean IS_BEAZY = false;
    public static String LOG_STRING = "";

    public void fillHistory() {

        IS_BEAZY = true;

        Map<Long, IAbstractOrder> destinationOrderMap = getOrderMap();

        Map<Long, List<ListOrdListRepresent>> listCancelOrderMap = getListOrderMap(Arrays.asList(RepresentationTypeCodes.GRANT_CANCEL, RepresentationTypeCodes.DISTRIBUTION_ACCORDING_TO_PROFILES,
                                                                                                 RepresentationTypeCodes.DIPLOM_AND_EXCLUDE_LIST, RepresentationTypeCodes.EXCLUDE_LIST, RepresentationTypeCodes.TRANSFER_LIST));
        Map<Long, List<DocOrdRepresent>> indivCancelOrderMap = getIndivOrderMap(Arrays.asList(RepresentationTypeCodes.GRANT_CANCEL_AND_DESTINATION_INDIVIDUAL, RepresentationTypeCodes.GRANT_CANCEL_INDIVIDUAL,
                                                                                              RepresentationTypeCodes.COURSE_TRANSFER, RepresentationTypeCodes.DIPLOM_AND_EXCLUDE, RepresentationTypeCodes.EXCLUDE,
                                                                                              RepresentationTypeCodes.TRANSFER));

        indivCancelOrderMap.putAll(getIndivOrderMap());

        Map<Long, List<ListOrdListRepresent>> listSuspendOrderMap = getListOrderMap(Arrays.asList(RepresentationTypeCodes.GRANT_SUSPEND));
        Map<Long, List<DocOrdRepresent>> indivSuspendOrderMap = getIndivOrderMap(Arrays.asList(RepresentationTypeCodes.GRANT_SUSPEND_INDIVIDUAL));

        Map<Long, List<ListOrdListRepresent>> listResumeOrderMap = getListOrderMap(Arrays.asList(RepresentationTypeCodes.GRANT_RESUME));
        Map<Long, List<DocOrdRepresent>> indivResumeOrderMap = getIndivOrderMap(Arrays.asList(RepresentationTypeCodes.GRANT_RESUME_INDIVIDUAL));

        Set<String> grantHistorySet = getGrantHistory();

        List<StudentGrantEntity> sgeList = getList(StudentGrantEntity.class, StudentGrantEntity.tmp(), Boolean.FALSE);

        RepresentGrantType destination = get(RepresentGrantType.class, RepresentGrantType.code(), RepresentGrantTypeCodes.DESTINATION);
        RepresentGrantType cancel = get(RepresentGrantType.class, RepresentGrantType.code(), RepresentGrantTypeCodes.CANCEL);
        RepresentGrantType suspend = get(RepresentGrantType.class, RepresentGrantType.code(), RepresentGrantTypeCodes.SUSPEND);
        RepresentGrantType resume = get(RepresentGrantType.class, RepresentGrantType.code(), RepresentGrantTypeCodes.RESUME);

        for (StudentGrantEntity sge : sgeList) {
            //назначение стипендии
            IAbstractRepresentation representation = sge.getListRepresent() == null ? sge.getRepresentation() : sge.getListRepresent();
            IAbstractOrder order = destinationOrderMap.get(sge.getId());

            String key = sge.getId().toString() + representation.getRepresentID().toString();
            if (!grantHistorySet.contains(key)) {
                createHistoryItem(sge, representation, order, destination);
                grantHistorySet.add(key);
            }

            //об отмене
            if (listCancelOrderMap.containsKey(sge.getId()) || indivCancelOrderMap.containsKey(sge.getId())) {

                if (listCancelOrderMap.containsKey(sge.getId())) {
                    List<ListOrdListRepresent> lst = listCancelOrderMap.get(sge.getId());

                    for (ListOrdListRepresent rel : lst) {
                        key = sge.getId().toString() + rel.getRepresentation().getId().toString();
                        if (!grantHistorySet.contains(key)) {
                            createHistoryItem(sge, rel.getRepresentation(), rel.getOrder(), cancel);
                            grantHistorySet.add(key);
                        }
                    }
                }
                else {
                    List<DocOrdRepresent> lst = indivCancelOrderMap.get(sge.getId());

                    for (DocOrdRepresent rel : lst) {
                        key = sge.getId().toString() + rel.getRepresentation().getId().toString();
                        if (!grantHistorySet.contains(key)) {
                            createHistoryItem(sge, rel.getRepresentation(), rel.getOrder(), cancel);
                            grantHistorySet.add(key);
                        }
                    }
                }
            }

            //о приостановлении
            if (listSuspendOrderMap.containsKey(sge.getId()) || indivSuspendOrderMap.containsKey(sge.getId())) {
                if (listSuspendOrderMap.containsKey(sge.getId())) {
                    List<ListOrdListRepresent> lst = listSuspendOrderMap.get(sge.getId());

                    for (ListOrdListRepresent rel : lst) {
                        key = sge.getId().toString() + rel.getRepresentation().getId().toString();
                        if (!grantHistorySet.contains(key)) {
                            createHistoryItem(sge, rel.getRepresentation(), rel.getOrder(), suspend);
                            grantHistorySet.add(key);
                        }
                    }
                }
                else {
                    List<DocOrdRepresent> lst = indivSuspendOrderMap.get(sge.getId());

                    for (DocOrdRepresent rel : lst) {
                        key = sge.getId().toString() + rel.getRepresentation().getId().toString();
                        if (!grantHistorySet.contains(key)) {
                            createHistoryItem(sge, rel.getRepresentation(), rel.getOrder(), suspend);
                            grantHistorySet.add(key);
                        }
                    }
                }
            }

            //о возобновлении
            if (listResumeOrderMap.containsKey(sge.getId()) || indivResumeOrderMap.containsKey(sge.getId())) {
                if (listResumeOrderMap.containsKey(sge.getId())) {
                    List<ListOrdListRepresent> lst = listResumeOrderMap.get(sge.getId());

                    for (ListOrdListRepresent rel : lst) {
                        key = sge.getId().toString() + rel.getRepresentation().getId().toString();
                        if (!grantHistorySet.contains(key)) {
                            createHistoryItem(sge, rel.getRepresentation(), rel.getOrder(), resume);
                            grantHistorySet.add(key);
                        }
                    }
                }
                else {
                    List<DocOrdRepresent> lst = indivResumeOrderMap.get(sge.getId());

                    for (DocOrdRepresent rel : lst) {
                        key = sge.getId().toString() + rel.getRepresentation().getId().toString();
                        if (!grantHistorySet.contains(key)) {
                            createHistoryItem(sge, rel.getRepresentation(), rel.getOrder(), resume);
                            grantHistorySet.add(key);
                        }
                    }
                }
            }

            LOG_STRING = " Обработано " + Integer.toString(sgeList.indexOf(sge) + 1) + " из " + Integer.toString(sgeList.size());
        }

        IS_BEAZY = false;
        FORSE_RUN = false;

    }


    private void createHistoryItem(StudentGrantEntity sge, IAbstractRepresentation representation, IAbstractOrder order, RepresentGrantType type) {

        StudentGrantEntityHistory history = new StudentGrantEntityHistory();
        history.setStudentGrantEntity(sge);
        history.setRepresentation(representation);
        history.setRepresentGrantType(type);
        history.setCreateDate(order.getCommitDate());

        getSession().saveOrUpdate(history);
    }

    private Set<String> getGrantHistory() {

        Set<String> set = new HashSet<String>();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentGrantEntityHistory.class, "h")
                .column(DQLExpressions.property(StudentGrantEntityHistory.studentGrantEntity().id().fromAlias("h")))
                .column(DQLExpressions.property(StudentGrantEntityHistory.representation().representID().fromAlias("h")));


        List<Object[]> lst = getList(builder);

        for (Object[] obj : lst) {
            String grantId = ((Long) obj[0]).toString();
            String representId = ((Long) obj[1]).toString();

            set.add(grantId + representId);
        }

        return set;
    }

    private Map<Long, List<ListOrdListRepresent>> getListOrderMap(List<String> codes) {
        DQLSelectBuilder listOrderBuilder = new DQLSelectBuilder().fromEntity(ListRepresentHistoryItem.class, "hi")
                .where(DQLExpressions.in(DQLExpressions.property(ListRepresentHistoryItem.listRepresent().representationType().code().fromAlias("hi")), codes))
                .joinEntity("hi", DQLJoinType.inner, ListOrdListRepresent.class, "lo", DQLExpressions.eq(DQLExpressions.property("hi", ListRepresentHistoryItem.listRepresent()), DQLExpressions.property("lo", ListOrdListRepresent.representation())))
                .joinPath(DQLJoinType.inner, ListOrdListRepresent.order().fromAlias("lo"), "listorder")
                .joinPath(DQLJoinType.inner, ListOrdListRepresent.representation().fromAlias("lo"), "listrep")
                .order("listorder.commitDate")
                .column("hi.number")
                .column("lo");


        List<Object[]> listOrder = getList(listOrderBuilder);

        Map<Long, List<ListOrdListRepresent>> listOrderMap = new HashMap<Long, List<ListOrdListRepresent>>();
        for (Object[] obj : listOrder) {
            Long id = (Long) obj[0];
            ListOrdListRepresent order = (ListOrdListRepresent) obj[1];

            List<ListOrdListRepresent> lst = listOrderMap.get(id);
            if (lst == null)
                lst = new ArrayList<ListOrdListRepresent>();
            lst.add(order);

            listOrderMap.put(id, lst);
        }

        return listOrderMap;
    }

    private Map<Long, List<DocOrdRepresent>> getIndivOrderMap(List<String> codes) {
        DQLSelectBuilder indivOrderBuilder = new DQLSelectBuilder().fromEntity(RepresentGrantsOld.class, "hi")
                .where(DQLExpressions.in(DQLExpressions.property(RepresentGrantsOld.representation().type().code().fromAlias("hi")), codes))
                .joinEntity("hi", DQLJoinType.inner, DocOrdRepresent.class, "io", DQLExpressions.eq(DQLExpressions.property("hi", RepresentGrantsOld.representation()), DQLExpressions.property("io", DocOrdRepresent.representation())))
                .joinPath(DQLJoinType.inner, DocOrdRepresent.order().fromAlias("io"), "indorder")
                .joinPath(DQLJoinType.inner, DocOrdRepresent.representation().fromAlias("io"), "indrep")
                .order("indorder.commitDate")
                .column(DQLExpressions.property(RepresentGrantsOld.studentGrantEntityId().fromAlias("hi")))
                .column("io");

        List<Object[]> indivOrder = getList(indivOrderBuilder);

        Map<Long, List<DocOrdRepresent>> indivOrderMap = new HashMap<Long, List<DocOrdRepresent>>();
        for (Object[] obj : indivOrder) {
            Long id = (Long) obj[0];
            DocOrdRepresent order = (DocOrdRepresent) obj[1];

            List<DocOrdRepresent> lst = indivOrderMap.get(id);
            if (lst == null)
                lst = new ArrayList<DocOrdRepresent>();
            lst.add(order);

            indivOrderMap.put(id, lst);
        }

        return indivOrderMap;
    }

    private Map<Long, List<DocOrdRepresent>> getIndivOrderMap() {

        DQLSelectBuilder cancelBuilder = new DQLSelectBuilder().fromEntity(StudentGrantEntity.class, "s")
                .where(DQLExpressions.eqValue(DQLExpressions.property(StudentGrantEntity.tmp().fromAlias("s")), Boolean.FALSE))
                .where(DQLExpressions.eqValue(DQLExpressions.property(StudentGrantEntity.status().code().fromAlias("s")), StuGrantStatusCodes.CODE_3))
                .where(DQLExpressions.notIn(DQLExpressions.property(StudentGrantEntity.id().fromAlias("s")), getCancelBuilder().getQuery()));

        Map<Long, List<DocOrdRepresent>> indivOrderMap = new HashMap<Long, List<DocOrdRepresent>>();
        List<StudentGrantEntity> cancelIds = getList(cancelBuilder);
        for (StudentGrantEntity sge : cancelIds) {
            Long id = sge.getId();
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocOrdRepresent.class, "rel")
                    .where(DQLExpressions.like(DQLExpressions.property(DocOrdRepresent.representation().grantsOld().fromAlias("rel")), DQLExpressions.value(CoreStringUtils.escapeLike(String.valueOf(id), true))));


            List<DocOrdRepresent> indivOrder = getList(builder);

            for (DocOrdRepresent order : indivOrder) {
                List<DocOrdRepresent> lst = indivOrderMap.get(id);
                if (lst == null)
                    lst = new ArrayList<DocOrdRepresent>();
                lst.add(order);

                indivOrderMap.put(id, lst);
            }
        }

        return indivOrderMap;
    }

    private DQLSelectBuilder getCancelBuilder() {
        DQLSelectBuilder listOrderBuilder = new DQLSelectBuilder().fromEntity(ListRepresentHistoryItem.class, "hi")
                .where(DQLExpressions.in(DQLExpressions.property(ListRepresentHistoryItem.listRepresent().representationType().code().fromAlias("hi")),
                                         Arrays.asList(RepresentationTypeCodes.GRANT_CANCEL, RepresentationTypeCodes.DISTRIBUTION_ACCORDING_TO_PROFILES,
                                                       RepresentationTypeCodes.DIPLOM_AND_EXCLUDE_LIST, RepresentationTypeCodes.EXCLUDE_LIST, RepresentationTypeCodes.TRANSFER_LIST)))
                .column("hi.number");

        DQLSelectBuilder indivOrderBuilder = new DQLSelectBuilder().fromEntity(RepresentGrantsOld.class, "hi")
                .where(DQLExpressions.in(DQLExpressions.property(RepresentGrantsOld.representation().type().code().fromAlias("hi")),
                                         Arrays.asList(RepresentationTypeCodes.GRANT_CANCEL_AND_DESTINATION_INDIVIDUAL, RepresentationTypeCodes.GRANT_CANCEL_INDIVIDUAL,
                                                       RepresentationTypeCodes.COURSE_TRANSFER, RepresentationTypeCodes.DIPLOM_AND_EXCLUDE, RepresentationTypeCodes.EXCLUDE, RepresentationTypeCodes.TRANSFER)))
                .column(DQLExpressions.property(RepresentGrantsOld.studentGrantEntityId().fromAlias("hi")));

        listOrderBuilder.union(indivOrderBuilder.getSelectRule());
        return listOrderBuilder;
    }

    private Map<Long, IAbstractOrder> getOrderMap() {

        DQLSelectBuilder indivOrderBuilder = new DQLSelectBuilder().fromEntity(StudentGrantEntity.class, "grants")
                .joinEntity("grants", DQLJoinType.inner, DocOrdRepresent.class, "io",
                            DQLExpressions.eq(
                                    DQLExpressions.property("grants", StudentGrantEntity.representation()),
                                    DQLExpressions.property("io", DocOrdRepresent.representation()))
                )
                .where(DQLExpressions.eqValue(DQLExpressions.property(StudentGrantEntity.tmp().fromAlias("grants")), Boolean.FALSE))
                .column("grants.id")
                .column("io.order");

        DQLSelectBuilder listOrderBuilder = new DQLSelectBuilder().fromEntity(StudentGrantEntity.class, "grants")
                .joinEntity("grants", DQLJoinType.inner, ListOrdListRepresent.class, "lo",
                            DQLExpressions.eq(
                                    DQLExpressions.property("grants", StudentGrantEntity.listRepresent()),
                                    DQLExpressions.property("lo", ListOrdListRepresent.representation()))
                )
                .column("grants.id")
                .column("lo.order")
                .where(DQLExpressions.eqValue(DQLExpressions.property(StudentGrantEntity.tmp().fromAlias("grants")), Boolean.FALSE));

        List<Object[]> indivOrder = getList(indivOrderBuilder);

        Map<Long, IAbstractOrder> map = new HashMap<Long, IAbstractOrder>();
        for (Object[] obj : indivOrder) {
            Long id = (Long) obj[0];
            IAbstractOrder order = (IAbstractOrder) obj[1];

            map.put(id, order);
        }

        List<Object[]> listOrder = getList(listOrderBuilder);

        for (Object[] obj : listOrder) {
            Long id = (Long) obj[0];
            IAbstractOrder order = (IAbstractOrder) obj[1];

            map.put(id, order);
        }

        return map;
    }
}
