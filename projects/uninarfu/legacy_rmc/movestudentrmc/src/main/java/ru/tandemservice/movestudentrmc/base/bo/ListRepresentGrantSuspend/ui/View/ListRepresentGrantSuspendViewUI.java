package ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantSuspend.ui.View;

import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.support.AbstractListRepresentViewUI;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantSuspend.logic.ListPrintDoc;
import ru.tandemservice.movestudentrmc.entity.ListRepresentGrantSuspend;

import java.util.Arrays;

public class ListRepresentGrantSuspendViewUI extends AbstractListRepresentViewUI<ListRepresentGrantSuspend> {

    @Override
    public ListRepresentGrantSuspend getListRepresentObject() {
        return new ListRepresentGrantSuspend();
    }

    @Override
    protected RtfDocument printRepresent() {
        return new ListPrintDoc().creationPrintDocOrder(Arrays.asList(getListRepresent()));
    }
}
