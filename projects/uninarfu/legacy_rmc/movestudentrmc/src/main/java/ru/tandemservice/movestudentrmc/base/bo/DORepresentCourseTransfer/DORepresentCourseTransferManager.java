package ru.tandemservice.movestudentrmc.base.bo.DORepresentCourseTransfer;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentCourseTransfer.logic.DORepresentCourseTransferDAO;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentCourseTransfer.ui.Edit.DORepresentCourseTransferEdit;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentCourseTransfer.ui.View.DORepresentCourseTransferView;
import ru.tandemservice.movestudentrmc.docord.IDOObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IDOObjectModifyDAO;

@Configuration
public class DORepresentCourseTransferManager extends BusinessObjectManager implements IDOObjectByRepresentTypeManager {

    public static DORepresentCourseTransferManager instance() {
        return instance(DORepresentCourseTransferManager.class);
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return DORepresentCourseTransferEdit.class;
    }

    @Bean
    public IDOObjectModifyDAO getDOObjectModifyDAO() {
        return new DORepresentCourseTransferDAO();
    }

    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return DORepresentCourseTransferView.class;
    }

}
