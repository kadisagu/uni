package ru.tandemservice.movestudentrmc.dao.check;

import org.tandemframework.shared.person.base.entity.PersonBenefit;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.List;

public class CheckStudentBenefitChildrenOrphan extends AbstractCheckStudentBenefit {

    private final static String CHILDREN_ORPHAN_CODE = "5";

    @Override
    protected boolean checkCondition(Student student) {
        Long personId = student.getPerson().getId();

        if (!personBenefitMap.containsKey(personId))
            return false;

        List<PersonBenefit> benefits = personBenefitMap.get(personId);
        for (PersonBenefit personBenefit : benefits)
            if (personBenefit.getBenefit().getCode().equals(CHILDREN_ORPHAN_CODE))
                return true;

        return false;
    }

    @Override
    protected String createError(Student student, boolean value) {
        StringBuilder sb = new StringBuilder()
                .append("Студент ")
                .append(student.getPerson().getFullFio())
                .append(" ")
                .append(value ? "не " : "")
                .append("имеет льготу «дети-сироты, дети, оставшиеся без попечения родителей и лица из их числа»");
        return sb.toString();
    }

}
