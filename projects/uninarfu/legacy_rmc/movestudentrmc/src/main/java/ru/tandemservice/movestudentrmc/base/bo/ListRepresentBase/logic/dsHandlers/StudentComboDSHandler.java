package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers;


import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import org.tandemframework.shared.person.catalog.entity.codes.IdentityCardTypeCodes;
import ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;

import java.util.List;


public class StudentComboDSHandler extends DefaultComboDataSourceHandler {

    public StudentComboDSHandler(String ownerId) {
        super(ownerId, Student.class, Student.person().identityCard().fullFio());
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        String userInput = ep.input.getComboFilterByValue();
        List<Object> territorialOrgUnitFilter = ep.context.get("territorialOrgUnitFilter");
        List<Object> formativeOrgUnitFilter = ep.context.get("formativeOrgUnitFilter");

        List<Object> educationLevelsHighSchoolDSFilter = ep.context.get("educationLevelsHighSchoolDSFilter");
        Object educationLevelFilter = ep.context.get("educationLevelFilter");
        List<Object> courseFilter = ep.context.get("courseFilter");
        List<Object> groupFilter = ep.context.get("groupFilter");
        List<Object> developFormFilter = ep.context.get("developFormFilter");
        Object compensationTypeFilter = ep.context.get("compensationTypeFilter");

        List<Object> educationOrgUnitFilter = ep.context.get("educationOrgUnitFilter");

        List<Object> benefitFilter = ep.context.get("benefitFilter");
        List<Object> studentFilter = ep.context.get("studentFilter");

        Boolean isSuccessfullyHandOverSession = (Boolean) (ep.context.get("isSuccessfullyHandOverSession") == null ? false : ep.context.get("isSuccessfullyHandOverSession"));
        List<Object> educationYearFilter = ep.context.get("educationYearFilter");
        List<Object> yearDistPartFilter = ep.context.get("yearDistPartFilter");

        Boolean isConsiderSessionResults = (Boolean) (ep.context.get("isConsiderSessionResults") == null ? false : ep.context.get("isConsiderSessionResults"));

        Boolean isforeignCitizens = (Boolean) (ep.context.get("isforeignCitizens") == null ? false : ep.context.get("isforeignCitizens"));

        Boolean isNoGroup = (Boolean) (ep.context.get("isNoGroup") == null ? false : ep.context.get("isNoGroup"));

        Object studentStatusFilter = ep.context.get("studentStatusFilter");
        List<IdentifiableWrapper> sessionMarksResult = ep.context.get("sessionMarksResult");

        List<RepresentationType> typeRepresentFilter = ep.context.get("typeRepresentFilter");


        if (isforeignCitizens) {
            FilterUtils.applySelectFilter(ep.dqlBuilder, "e", Student.compensationType().code(), CompensationTypeCodes.COMPENSATION_TYPE_BUDGET);
            ep.dqlBuilder.where(DQLExpressions.ne(DQLExpressions.property(Student.person().identityCard().cardType().code().fromAlias("e")), DQLExpressions.value(IdentityCardTypeCodes.PASPORT_GRAJDANINA_ROSSIYSKOY_FEDERATSII)));
        }

        if (benefitFilter != null && !benefitFilter.isEmpty()) {
            DQLSelectBuilder benefitBuilder = new DQLSelectBuilder();

            benefitBuilder.fromEntity(PersonBenefit.class, "b");
            FilterUtils.applySelectFilter(benefitBuilder, "b", PersonBenefit.benefit(), benefitFilter);
            benefitBuilder.addColumn(DQLExpressions.property(PersonBenefit.person().fromAlias("b")));

            ep.dqlBuilder.where(DQLExpressions.in(DQLExpressions.property(Student.person().fromAlias("e")), benefitBuilder.getQuery()));
        }

        if (isNoGroup)
            ep.dqlBuilder.where(DQLExpressions.isNull(DQLExpressions.property(Student.group().fromAlias("e"))));

        FilterUtils.applySelectFilter(ep.dqlBuilder, "e", Student.educationOrgUnit().formativeOrgUnit(), formativeOrgUnitFilter);

        FilterUtils.applySelectFilter(ep.dqlBuilder, "e", Student.educationOrgUnit().educationLevelHighSchool().orgUnit(), educationOrgUnitFilter);

        FilterUtils.applySelectFilter(ep.dqlBuilder, "e", Student.course(), courseFilter);

        FilterUtils.applySelectFilter(ep.dqlBuilder, "e", Student.group(), groupFilter);

        FilterUtils.applySelectFilter(ep.dqlBuilder, "e", Student.compensationType(), compensationTypeFilter);


        FilterUtils.applySelectFilter(ep.dqlBuilder, "e", Student.educationOrgUnit().territorialOrgUnit(), territorialOrgUnitFilter);

        FilterUtils.applySelectFilter(ep.dqlBuilder, "e", Student.educationOrgUnit().developForm(), developFormFilter);

        FilterUtils.applySelectFilter(ep.dqlBuilder, "e", Student.educationOrgUnit().educationLevelHighSchool(), educationLevelsHighSchoolDSFilter);
        FilterUtils.applySelectFilter(ep.dqlBuilder, "e", Student.status(), studentStatusFilter);

        FilterUtils.applySimpleLikeFilter(ep.dqlBuilder, "e", Student.person().identityCard().fullFio(), userInput);

        if (educationLevelFilter != null)
            ep.dqlBuilder.where(DQLExpressions.or(
                    DQLExpressions.eq(DQLExpressions.property(Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().fromAlias("e")), DQLExpressions.commonValue(educationLevelFilter)),
                    DQLExpressions.eq(DQLExpressions.property(Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().parent().fromAlias("e")), DQLExpressions.commonValue(educationLevelFilter)),
                    DQLExpressions.eq(DQLExpressions.property(Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().parent().parent().fromAlias("e")), DQLExpressions.commonValue(educationLevelFilter))
            ));

        if (typeRepresentFilter != null)
            applyTypeRepresentFilter(ep.dqlBuilder, typeRepresentFilter);

        if (studentFilter != null && !studentFilter.isEmpty())
            FilterUtils.applySelectFilter(ep.dqlBuilder, "e", Student.id(), studentFilter);


    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
    {
        ep.dqlBuilder.order("e." + Student.person().identityCard().fullFio());
    }

    public void applyTypeRepresentFilter(DQLSelectBuilder builder, List<RepresentationType> listTypes) {
        DQLSelectBuilder subBuilder = new DQLSelectBuilder()
                .fromEntity(RelListRepresentStudents.class, "rrs")
                .column(DQLExpressions.property(RelListRepresentStudents.student().id().fromAlias("rrs")))
                .predicate(DQLPredicateType.distinct);
        FilterUtils.applySelectFilter(subBuilder, "rrs", RelListRepresentStudents.representation().representationType(), listTypes);

        builder.where(DQLExpressions.in(DQLExpressions.property(Student.id().fromAlias("e")), subBuilder.getQuery()));
    }

}
