package ru.tandemservice.movestudentrmc.base.bo.ListRepresentWeekend.ui.View;

import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.support.AbstractListRepresentViewUI;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentWeekend.logic.ListPrintDoc;
import ru.tandemservice.movestudentrmc.entity.ListRepresentWeekend;

import java.util.Arrays;

public class ListRepresentWeekendViewUI extends AbstractListRepresentViewUI<ListRepresentWeekend> {

    @Override
    public ListRepresentWeekend getListRepresentObject() {
        return new ListRepresentWeekend();
    }

    @Override
    protected RtfDocument printRepresent() {
        return new ListPrintDoc().creationPrintDocOrder(Arrays.asList(getListRepresent()));
    }

}
