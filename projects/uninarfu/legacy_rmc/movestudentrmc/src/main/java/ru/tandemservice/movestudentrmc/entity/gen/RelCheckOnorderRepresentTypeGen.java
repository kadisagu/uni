package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.RelCheckOnorderRepresentType;
import ru.tandemservice.movestudentrmc.entity.catalog.CheckOnOrder;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность-связь 'Проверки по приказам'-'Типы представлений'
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RelCheckOnorderRepresentTypeGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.RelCheckOnorderRepresentType";
    public static final String ENTITY_NAME = "relCheckOnorderRepresentType";
    public static final int VERSION_HASH = -448150296;
    private static IEntityMeta ENTITY_META;

    public static final String L_CHECK = "check";
    public static final String L_TYPE = "type";
    public static final String P_VALUE = "value";

    private CheckOnOrder _check;     // Проверка по приказам
    private RepresentationType _type;     // Тип Представления
    private boolean _value = false;     // Значение

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Проверка по приказам. Свойство не может быть null.
     */
    @NotNull
    public CheckOnOrder getCheck()
    {
        return _check;
    }

    /**
     * @param check Проверка по приказам. Свойство не может быть null.
     */
    public void setCheck(CheckOnOrder check)
    {
        dirty(_check, check);
        _check = check;
    }

    /**
     * @return Тип Представления. Свойство не может быть null.
     */
    @NotNull
    public RepresentationType getType()
    {
        return _type;
    }

    /**
     * @param type Тип Представления. Свойство не может быть null.
     */
    public void setType(RepresentationType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Значение. Свойство не может быть null.
     */
    @NotNull
    public boolean isValue()
    {
        return _value;
    }

    /**
     * @param value Значение. Свойство не может быть null.
     */
    public void setValue(boolean value)
    {
        dirty(_value, value);
        _value = value;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RelCheckOnorderRepresentTypeGen)
        {
            setCheck(((RelCheckOnorderRepresentType)another).getCheck());
            setType(((RelCheckOnorderRepresentType)another).getType());
            setValue(((RelCheckOnorderRepresentType)another).isValue());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RelCheckOnorderRepresentTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RelCheckOnorderRepresentType.class;
        }

        public T newInstance()
        {
            return (T) new RelCheckOnorderRepresentType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "check":
                    return obj.getCheck();
                case "type":
                    return obj.getType();
                case "value":
                    return obj.isValue();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "check":
                    obj.setCheck((CheckOnOrder) value);
                    return;
                case "type":
                    obj.setType((RepresentationType) value);
                    return;
                case "value":
                    obj.setValue((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "check":
                        return true;
                case "type":
                        return true;
                case "value":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "check":
                    return true;
                case "type":
                    return true;
                case "value":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "check":
                    return CheckOnOrder.class;
                case "type":
                    return RepresentationType.class;
                case "value":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RelCheckOnorderRepresentType> _dslPath = new Path<RelCheckOnorderRepresentType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RelCheckOnorderRepresentType");
    }
            

    /**
     * @return Проверка по приказам. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelCheckOnorderRepresentType#getCheck()
     */
    public static CheckOnOrder.Path<CheckOnOrder> check()
    {
        return _dslPath.check();
    }

    /**
     * @return Тип Представления. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelCheckOnorderRepresentType#getType()
     */
    public static RepresentationType.Path<RepresentationType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Значение. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelCheckOnorderRepresentType#isValue()
     */
    public static PropertyPath<Boolean> value()
    {
        return _dslPath.value();
    }

    public static class Path<E extends RelCheckOnorderRepresentType> extends EntityPath<E>
    {
        private CheckOnOrder.Path<CheckOnOrder> _check;
        private RepresentationType.Path<RepresentationType> _type;
        private PropertyPath<Boolean> _value;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Проверка по приказам. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelCheckOnorderRepresentType#getCheck()
     */
        public CheckOnOrder.Path<CheckOnOrder> check()
        {
            if(_check == null )
                _check = new CheckOnOrder.Path<CheckOnOrder>(L_CHECK, this);
            return _check;
        }

    /**
     * @return Тип Представления. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelCheckOnorderRepresentType#getType()
     */
        public RepresentationType.Path<RepresentationType> type()
        {
            if(_type == null )
                _type = new RepresentationType.Path<RepresentationType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Значение. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelCheckOnorderRepresentType#isValue()
     */
        public PropertyPath<Boolean> value()
        {
            if(_value == null )
                _value = new PropertyPath<Boolean>(RelCheckOnorderRepresentTypeGen.P_VALUE, this);
            return _value;
        }

        public Class getEntityClass()
        {
            return RelCheckOnorderRepresentType.class;
        }

        public String getEntityName()
        {
            return "relCheckOnorderRepresentType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
