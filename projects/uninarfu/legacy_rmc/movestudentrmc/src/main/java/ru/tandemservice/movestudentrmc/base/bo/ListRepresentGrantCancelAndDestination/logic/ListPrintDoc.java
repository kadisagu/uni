package ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantCancelAndDestination.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import ru.tandemservice.movestudentrmc.docord.IListPrintDoc;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.catalog.Grant;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.*;

public class ListPrintDoc implements IListPrintDoc
{

    public final String TEMPLATE_HEADER = "list.rep.grantCancelAndDist.header";
    public final String TEMPLATE_PARAG = "list.rep.grantCancelAndDist.parag";
    public final String TEMPLATE_ADDON = "list.rep.grantCancelAndDist.addon";
    public final String TEMPLATE_ADDON_PARAG = "list.rep.grantCancelAndDist.addon.parag";

    private Map<Student, OrderParagraphInfo> hashMap = new HashMap<>();

    public RtfDocument creationPrintDocOrder(
            List<? extends ListRepresent> listOfRepresents)
    {

        PrintTemplate templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(PrintTemplate.class, TEMPLATE_HEADER);
        RtfDocument templateHeader = new RtfReader().read(templateDocument.getContent());

        templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(PrintTemplate.class, TEMPLATE_PARAG);
        RtfDocument templateParag = new RtfReader().read(templateDocument.getContent());

        templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(PrintTemplate.class, TEMPLATE_ADDON);
        RtfDocument templateAddon = new RtfReader().read(templateDocument.getContent());

        ListOrder order = listOfRepresents.get(0).getOrder();

        Map<Grant, Map<String, List<Object[]>>> dataMap = getStudentData(listOfRepresents);

        List<Grant> grantList = new ArrayList<>(dataMap.keySet());
        Collections.sort(grantList, new EntityComparator<>(new EntityOrder(Grant.title())));

        RtfDocument result = templateHeader.getClone();
        result.setSettings(templateParag.getSettings());
        IRtfElement paragTemp = UniRtfUtil.findElement(result.getElementList(), "parag");

        new RtfInjectModifier().put("grantNameGen", ((ListRepresentGrantCancelAndDestination) listOfRepresents.get(0)).getGrant().getGenitive()).modify(result);

        RtfDocument res = RtfBean.getElementFactory().createRtfDocument();
        res.setHeader(templateParag.getHeader());
        res.setSettings(templateParag.getSettings());
        res.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.TAB));

        int formCounter = 1;
        int addonCounter = 0;
        List<RtfDocument> addonList = new ArrayList<>();

        for (Grant grant : grantList)
        {
            Map<String, List<Object[]>> grantMap = dataMap.get(grant);
//			formCounter++;
            for (List<Object[]> studentList : grantMap.values())
            {
                addonCounter++;
                RtfDocument par = getParagraph(templateParag, formCounter++, formCounter++, addonCounter, studentList);
                res.getElementList().addAll(par.getElementList());
                addonList.add(getAddon(templateAddon, addonCounter, studentList, order));
            }
        }

        //основания
//		res.getElementList().add( RtfBean.getElementFactory().createRtfControl(IRtfData.PAR) );
        insertRow(res, "Основание: " + UtilPrintSupport.getBasementTitles(listOfRepresents));
        result.getElementList().addAll(result.getElementList().indexOf(paragTemp), res.getElementList());

        result.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));

        for (int i = 0; i < addonList.size(); i++)
        {
            result.getElementList().addAll(addonList.get(i).getElementList());
            if (i + 1 < addonList.size())
                result.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
        }

        SharedRtfUtil.removeParagraphsWithTagsRecursive(result, Collections.singletonList("parag"), false, false);

        return result;
    }

    private RtfDocument getParagraph(RtfDocument template, int formCounter, int counter, int addonCounter, List<Object[]> studentList)
    {
        RtfDocument result = template.getClone();
        ListRepresentGrantCancelAndDestination listRepresent = (ListRepresentGrantCancelAndDestination) studentList.get(0)[1];
        Student student = (Student) studentList.get(0)[0];
        List<Long> representIds = new ArrayList<>();
        for (Object[] o : studentList)
        {
            Student s = (Student) o[0];
            ListRepresentGrantCancelAndDestination rep = (ListRepresentGrantCancelAndDestination) o[1];
            hashMap.put(s, new OrderParagraphInfo(formCounter, 0, addonCounter, 0));
            representIds.add(rep.getId());
        }
        RtfInjectModifier im = new RtfInjectModifier();

        im.put("i", "" + formCounter);
        im.put("j", "" + counter);
        im.put("addonNumber", "" + addonCounter);

        injectModifier(im, listRepresent, student);

        List<DocRepresentOrderCancel> orders = IUniBaseDao.instance.get().getList(DocRepresentOrderCancel.class, DocRepresentOrderCancel.representation().id(), representIds);

        im.put("orders", getOrderStr(orders));

        im.modify(result);
        return result;
    }

    public static RtfString getOrderStr(List<DocRepresentOrderCancel> orders)
    {
        Map<String, RtfString> map = new HashMap<>();
        Set<IAbstractOrder> usedOrders = new HashSet<>();
        for (DocRepresentOrderCancel rel : orders)
        {
            if (usedOrders.contains(rel.getOrder()))
                continue;
            String key = rel.getTitle();
            RtfString str = map.get(key);
            if (str != null)
                str.append(",");
            else
                str = new RtfString();
            str.toList().addAll(getOrderTitle(rel.getOrder()).toList());
            usedOrders.add(rel.getOrder());
            map.put(key, str);
        }

        RtfString ordersStr = new RtfString();

        if (orders.size() > 1)
            ordersStr.append("приказы ");
        else
            ordersStr.append("приказ ");

        boolean first = true;

        for (String str : map.keySet())
        {
            if (!first)
                ordersStr.append(", ");
            ordersStr.append("«")
                    .append(str)
                    .append("»");
            ordersStr.toList().addAll(map.get(str).toList());
            first = false;
        }

        return ordersStr;
    }

    private static RtfString getOrderTitle(IAbstractOrder order)
    {

        RtfString title = new RtfString();

        title.append(" от")
                .append(IRtfData.SYMBOL_TILDE);

        UtilPrintSupport.getDateFormatterWithMonthStringAndYear(order.getCommitDate(), title);

        title.append(" №")
                .append(IRtfData.SYMBOL_TILDE)
                .append(order.getNumber())
        ;

        return title;
    }

    private RtfDocument getAddon(RtfDocument template, int addonCounter, List<Object[]> studentList, ListOrder order)
    {
        RtfDocument result = template.getClone();

        ListRepresentGrantCancelAndDestination listRepresent = (ListRepresentGrantCancelAndDestination) studentList.get(0)[1];
        Student firstStudent = (Student) studentList.get(0)[0];

        RtfInjectModifier im = new RtfInjectModifier();
        im.put("addonNumber", "" + addonCounter);
        im.put("orderDate", order != null ? UtilPrintSupport.getDateFormatterWithMonthStringAndYear(order.getCommitDate()) : new RtfString().append(""));
        im.put("orderNumber", order != null ? order.getNumber() : "");
        //--------------------------------------------------------------------------------------------------------------------------------------
        im.put("signPostGenitive", order != null ? order.getSignPostGenitive() : "");
        //--------------------------------------------------------------------------------------------------------------------------------------

        injectModifier(im, listRepresent, firstStudent);

        im.modify(result);

        PrintTemplate templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(PrintTemplate.class, TEMPLATE_ADDON_PARAG);
        RtfDocument templateAddonParag = new RtfReader().read(templateDocument.getContent());

        RtfDocument parag = templateAddonParag.getClone();
        parag.setSettings(templateAddonParag.getSettings());

        Map<GrantView, List<Student>> map = new HashMap<>();

        for (Object[] obj : studentList)
        {
            Student student = (Student) obj[0];
            ListRepresentGrantCancelAndDestination rep = (ListRepresentGrantCancelAndDestination) obj[1];

            List<Student> lst = map.get(rep.getGrantView());

            if (lst == null)
                lst = new ArrayList<>();

            lst.add(student);
            map.put(rep.getGrantView(), lst);
        }

        List<GrantView> grantViewList = new ArrayList<>(map.keySet());
        Collections.sort(grantViewList, new EntityComparator<>(new EntityOrder(GrantView.title())));

        int parCounter = 0;

        for (GrantView grantView : grantViewList)
        {
            List<Student> studList = map.get(grantView);

            RtfDocument par = parag.getClone();
            parCounter++;

            new RtfInjectModifier()
                    .put("i", "" + parCounter)
                    .put("grantView", StringUtils.capitalize(grantView.getTitle())).modify(par);

            List<String[]> tableList = new ArrayList<>();
            int counter = 1;
            for (Student student : studList)
            {
                String[] row = new String[5];

                OrderParagraphInfo info = hashMap.get(student);
                if (info != null)
                {
                    info.setExtractNumber(parCounter);
                    info.setStudentNumber(counter);
                }
                row[0] = "" + counter++;
                row[1] = student.getBookNumber() != null ? student.getBookNumber() : "";
                row[2] = student.getPerson().getFullFio();
                row[3] = student.getCourse().getTitle();
                row[4] = student.getEducationOrgUnit().getFormativeOrgUnit().getTitle();

                tableList.add(row);
            }

            RtfTableModifier tm = new RtfTableModifier();
            tm.put("T", tableList.toArray(new String[][]{}));
            tm.modify(par);

            result.getElementList().addAll(par.getElementList());
        }

        return result;
    }


    private void insertRow(RtfDocument document, String text)
    {
        document.getElementList().add(RtfBean.getElementFactory().createRtfText(text));
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
    }

    private Map<Grant, Map<String, List<Object[]>>> getStudentData(List<? extends ListRepresent> listOfRepresents)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(RelListRepresentStudents.class, "rel")
                .where(DQLExpressions.in(
                        DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("rel")),
                        listOfRepresents
                ))
                .joinEntity(
                        "rel",
                        DQLJoinType.inner,
                        ListRepresentGrantCancelAndDestination.class,
                        "rep",
                        DQLExpressions.eq(
                                DQLExpressions.property(ListRepresentGrantCancelAndDestination.id().fromAlias("rep")),
                                DQLExpressions.property(RelListRepresentStudents.representation().id().fromAlias("rel"))
                        ))
                .column(DQLExpressions.property(RelListRepresentStudents.student().fromAlias("rel")))
                .column("rep")
                .order(DQLExpressions.property(ListRepresentGrantCancelAndDestination.grantView().title().fromAlias("rep")))
                .order(DQLExpressions.property(RelListRepresentStudents.student().educationOrgUnit().formativeOrgUnit().title().fromAlias("rel")))
                .order(DQLExpressions.property(RelListRepresentStudents.student().course().intValue().fromAlias("rel")))
                .order(DQLExpressions.property(RelListRepresentStudents.student().person().identityCard().fullFio().fromAlias("rel")));

        List<Object[]> studentDataList = UniDaoFacade.getCoreDao().getList(dql);

        Map<Grant, Map<String, List<Object[]>>> dataMap = new HashMap<>();
        for (Object[] arr : studentDataList)
        {
            Student student = (Student) arr[0];
            ListRepresentGrantCancelAndDestination represent = (ListRepresentGrantCancelAndDestination) arr[1];

            Grant grant = represent.getGrant();
            if (dataMap.get(grant) == null)
                dataMap.put(grant, new HashMap<>());

            String key = getKey(student, represent);
            if (dataMap.get(grant).get(key) == null)
                dataMap.get(grant).put(key, new ArrayList<>());

            dataMap.get(grant).get(key).add(arr);
        }

        return dataMap;
    }

    protected static void injectModifier(RtfInjectModifier im,
                                         ListRepresentGrantCancelAndDestination listRepresent,
                                         Student student)
    {
        im.put("developForm", UtilPrintSupport.getDevelopFormGen(student.getEducationOrgUnit().getDevelopForm()));
        im.put("compType", student.getCompensationType().getCode().equals(UniDefines.COMPENSATION_TYPE_BUDGET) ? "за счет средств федерального бюджета" : "с полным возмещением затрат");
        im.put("grantGen", listRepresent.getGrant().getGenitive());
        im.put("beginDate", UtilPrintSupport.getDateFormatterWithMonthStringAndYear(listRepresent.getBeginGrantDate()));
        im.put("endDate", UtilPrintSupport.getDateFormatterWithMonthStringAndYear(listRepresent.getEndGrantDate()));
    }

    protected String getKey(Student student,
                            ListRepresentGrantCancelAndDestination represent)
    {
        return new StringBuilder()
                .append(student.getEducationOrgUnit().getDevelopForm().getId())
                .append(student.getCompensationType().getId())
                .append(represent.getBeginGrantDate().getTime())
                .append(represent.getEndGrantDate().getTime())
                .toString();
    }

    @Override
    public Map<Student, OrderParagraphInfo> getParagInfomap()
    {
        return hashMap;
    }

}
