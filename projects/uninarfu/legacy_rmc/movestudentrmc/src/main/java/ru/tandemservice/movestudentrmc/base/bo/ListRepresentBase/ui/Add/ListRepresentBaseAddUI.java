package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ListRepresentBaseManager;
import ru.tandemservice.movestudentrmc.docord.IListObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationBasement;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentationTypeCodes;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

@State(
        @Bind(key = ListRepresentBaseAddUI.STUDENT_LIST_ID, binding = ListRepresentBaseAddUI.STUDENT_LIST_ID)
)
public class ListRepresentBaseAddUI extends UIPresenter {

    public static final String STUDENT_LIST_ID = "studentListId";
    private List<Long> _studentListId;

    private RepresentationType representType;
    private RepresentationReason representReason;
    private RepresentationBasement representBasement;

    private Date _representationBasementDate;     // Дата основания
    private String _representationBasementNumber;     // № основания
    private GrantView grantView;

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        Map<String, Object> settingMap = new UniMap()
                .add("typeRepresentFilter", this.getRepresentType())
                .add("reasonRepresentFilter", this.getRepresentReason())
                .add("basementRepresentFilter", this.getRepresentBasement())
                .add("grantViewCanSuspendFilter", this.getGrantCanSuspend());
        dataSource.putAll(settingMap);
    }

    public void onClickCreate() {
        IListObjectByRepresentTypeManager representManager = ListRepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(this.getRepresentType().getCode());
        try {
            _uiActivation.asRegion(representManager.getEditComponentManager())
                    .parameter("representationBasementDate", _representationBasementDate)
                    .parameter("representationBasementNumber", _representationBasementNumber)
                    .parameter("typeRepresentFilter", this.getRepresentType())
                    .parameter("reasonRepresentFilter", this.getRepresentReason())
                    .parameter("basementRepresentFilter", this.getRepresentBasement())
                    .parameter("grantViewFilter", this.getShowGrantBlock() ? this.getGrantView() : null)
                    .activate();
        }
        catch (Exception e) {
            ContextLocal.getErrorCollector().add("Невозможно создать представление для выбранного типа.");
            e.printStackTrace();
            // vch ну режет глаза
            // ну не надо так, если нет БК для такого представления, значит null, значит можно написать человеческую ошибку
        }
    }

    public boolean getShowGrantBlock() {
        if (getRepresentType() == null)
            return false;

        List<String> grantList = Arrays.asList(
                //RepresentationTypeCodes.SOCIAL_GRANT_LIST,
                RepresentationTypeCodes.GRANT_CANCEL,
                RepresentationTypeCodes.GRANT_SUSPEND,
                RepresentationTypeCodes.GRANT_RESUME);

        return grantList.contains(getRepresentType().getCode());
    }

    public Boolean getGrantCanSuspend() {
        if (!getShowGrantBlock())
            return null;

        List<String> suspendList = Arrays.asList(
                RepresentationTypeCodes.GRANT_SUSPEND,
                RepresentationTypeCodes.GRANT_RESUME);

        return suspendList.contains(getRepresentType().getCode());
    }

    public RepresentationType getRepresentType() {
        return representType;
    }

    public void setRepresentType(RepresentationType representType) {
        this.representType = representType;
    }

    public RepresentationReason getRepresentReason() {
        return representReason;
    }

    public void setRepresentReason(RepresentationReason representReason) {
        this.representReason = representReason;
    }

    public RepresentationBasement getRepresentBasement() {
        return representBasement;
    }

    public void setRepresentBasement(RepresentationBasement representBasement) {
        this.representBasement = representBasement;
    }

    public List<Long> getStudentListId() {
        return _studentListId;
    }

    public void setStudentListId(List<Long> studentListId) {
        _studentListId = studentListId;
    }

    public Boolean getHasAdditional() {
        if (this.getRepresentBasement() != null)
            return this.getRepresentBasement().isNoteRequired();
        else
            return false;
    }

    public Date getRepresentationBasementDate()
    {
        return _representationBasementDate;
    }

    public void setRepresentationBasementDate(Date representationBasementDate)
    {
        _representationBasementDate = representationBasementDate;
    }

    public String getRepresentationBasementNumber()
    {
        return _representationBasementNumber;
    }

    public void setRepresentationBasementNumber(String representationBasementNumber)
    {
        _representationBasementNumber = representationBasementNumber;
    }

    public GrantView getGrantView() {
        return grantView;
    }

    public void setGrantView(GrantView grantView) {
        this.grantView = grantView;
    }

}
