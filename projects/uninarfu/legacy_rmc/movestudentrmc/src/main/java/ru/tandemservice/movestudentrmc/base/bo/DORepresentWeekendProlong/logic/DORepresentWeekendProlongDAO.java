package ru.tandemservice.movestudentrmc.base.bo.DORepresentWeekendProlong.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentDAO;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.RepresentWeekendProlong;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class DORepresentWeekendProlongDAO extends AbstractDORepresentDAO
{

    @Override
    public Representation createRepresentObject(RepresentationType type, List<Student> studentList) {

        RepresentWeekendProlong represent = new RepresentWeekendProlong();
        represent.setType(type);
        return represent;
    }

    @Override
    public boolean doCommit(Representation represent, ErrorCollector error) {
        if (!super.doCommit(represent, error))
            return false;

        List<DocRepresentStudentBase> studentList = getStudentList(represent);
        Student student = studentList.get(0).getStudent();
        student.setStatus(DataAccessServices.dao().get(StudentStatus.class, StudentStatus.code().s(), "5"));
        baseUpdate(student);

        return true;
    }

    @Override
    public boolean doRollback(Representation represent, ErrorCollector error) {
        if (!super.doRollback(represent, error))
            return false;

        List<DocRepresentStudentBase> studentList = getStudentList(represent);
        Student student = studentList.get(0).getStudent();
        student.setStatus(represent.getStatusOld());

        baseUpdate(student);
        return true;
    }

    @Override
    public void buildBodyExtract(Representation representationBase, String itemFac, RtfDocument docCommon) {
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        UtilPrintSupport.setParNum(docCommon, itemFac);

        RtfInjectModifier modifier = new RtfInjectModifier();

        UtilPrintSupport.injectModifierExtract(modifier, docRepresent);

        modifier.modify(docCommon);
    }

    @Override
    public void buildBodyRepresent(Representation representationBase, RtfDocument document, AtomicInteger parNumber, AtomicInteger extractNumber) {
        super.buildBodyRepresent(representationBase, document, parNumber, extractNumber);
        RepresentWeekendProlong represent = (RepresentWeekendProlong) representationBase;
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        Student student = UtilPrintSupport.getStudent(represent);


        RtfInjectModifier modifier = new RtfInjectModifier();

        UtilPrintSupport.injectModifierRepresent(modifier, docRepresent, student);
        //modifier.put("studentTitle", PersonManager.instance().declinationDao().getDeclinationFIO(student.getPerson().getIdentityCard(), GrammaCase.DATIVE));
        modifier.put("studentTitle", UtilPrintSupport.printSeparatedFIO(student.getPerson().getIdentityCard(), GrammaCase.DATIVE, student.getPerson().isMale()));

        modifier.put("studentSex", student.getPerson().isMale() ? "студенту" : "студентке");

        if (StringUtils.isEmpty(modifier.getStringValue("basics")))
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Arrays.asList("basics"), false, false);

        modifier.modify(document);
    }

    @Override
    public void buildBodyDeclaration(Representation representationBase, RtfDocument document) {
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        RtfInjectModifier im = new RtfInjectModifier();

        RtfTableModifier tm = new RtfTableModifier();
        UtilPrintSupport.injectModifierDeclaration(im, tm, docRepresent);

        this.injectModifier(im, docRepresent);

        im.modify(document);
        tm.modify(document);
    }

    @Override
    public GrammaCase getGrammaCase() {
        return GrammaCase.DATIVE;
    }
}
