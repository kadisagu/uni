package ru.tandemservice.movestudentrmc.dao.check;

import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;

public class CheckStatusPractice extends AbstractCheckStudentAdditionalStatus {

    private final static String PRACTICE_CODE = "movestudentrmc.practice";

    @Override
    protected void prepareConditionData() {
        this.studentCustomStateCode = PRACTICE_CODE;
        this.studentCustomStateCI = getCatalogItem(StudentCustomStateCI.class, studentCustomStateCode);
    }

}
