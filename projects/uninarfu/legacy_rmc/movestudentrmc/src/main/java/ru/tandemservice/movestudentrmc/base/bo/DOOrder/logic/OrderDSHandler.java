package ru.tandemservice.movestudentrmc.base.bo.DOOrder.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudentrmc.entity.DocOrdRepresent;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.DocumentOrder;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class OrderDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{

    public static final String NUMBER_ORDER_FILTER = "numberOrderFilter";
    public static final String DATE_FORMATIVE_FROM_FILTER = "dateFormativeFromFilter";
    public static final String DATE_COMMIT_FROM_FILTER = "dateCommitFromFilter";
    public static final String DATE_FORMATIVE_TO_FILTER = "dateFormativeToFilter";
    public static final String DATE_COMMIT_TO_FILTER = "dateCommitToFilter";
    public static final String DATE_START_FROM_FILTER = "dateStartFromFilter";
    public static final String DATE_START_TO_FILTER = "dateStartToFilter";
    public static final String ORDER_TITLE_COLUMN = "number";
    public static final String STATE_COLUMN = "state";
    public static final String CREATE_DATE_COLUMN = "createDate";
    public static final String COMMIT_DATE_COLUMN = "commitDate";
    public static final String REPRESENTATION_TYPES = "representationTypes";
    public static final String REPRESENTATION_COUNT = "representationCount";
    public static final String OPERATOR = "operator";

    public OrderDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Date dateCommitFromFilter = context.get(DATE_COMMIT_FROM_FILTER);
        Date dateFormativeFromFilter = context.get(DATE_FORMATIVE_FROM_FILTER);//from
        Date dateCommitToFilter = getLastTime(context.get(DATE_COMMIT_TO_FILTER));//to
        Date dateFormativeToFilter = getLastTime(context.get(DATE_FORMATIVE_TO_FILTER));
        Date dateStartFromFilter = context.get(DATE_START_FROM_FILTER);
        Date dateStartToFilter = getLastTime(context.get(DATE_START_TO_FILTER));
        String numberOrderFilter = context.get(NUMBER_ORDER_FILTER);
        List<RepresentationType> types = context.get("typeRepresentFilter");
        List<OrgUnit> formativeOrgUnits = context.get("formativeOrgUnitFilter");
        List<Course> courses = context.get("courseFilter");
        List<Student> students = context.get("filterStudent");
        String author = context.get("authorFilter");
        List<RepresentationReason> reasons = context.get("reasonRepresentFilter");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocumentOrder.class, "o").column("o");
        builder.where(DQLExpressions.ne(
                DQLExpressions.property(DocumentOrder.state().code().fromAlias("o")),
                DQLExpressions.value("5")
        ));

        //Период проведения
        builder.where(DQLExpressions.betweenDays(DocumentOrder.commitDateSystem().fromAlias("o"), dateCommitFromFilter, dateCommitToFilter));

        //Период создания
        builder.where(DQLExpressions.betweenDays(DocumentOrder.createDate().fromAlias("o"), dateFormativeFromFilter, dateFormativeToFilter));

        //Период вступления в силу
        builder.where(DQLExpressions.betweenDays(DocumentOrder.commitDate().fromAlias("o"), dateStartFromFilter, dateStartToFilter));

        if (numberOrderFilter != null)
            builder.where(DQLExpressions.like(
                    DQLExpressions.property(DocumentOrder.number().fromAlias("o")),
                    DQLExpressions.value(CoreStringUtils.escapeLike(numberOrderFilter, true))
            ));

        DQLSelectBuilder subBuilder = new DQLSelectBuilder()
                .fromEntity(DocOrdRepresent.class, "dor")
                .column(DQLExpressions.property(DocOrdRepresent.order().id().fromAlias("dor")))
                .joinEntity("dor", DQLJoinType.left, DocRepresentStudentBase.class, "drs",
                            DQLExpressions.eq(
                                DQLExpressions.property(DocRepresentStudentBase.representation().id().fromAlias("drs")),
                                DQLExpressions.property(DocOrdRepresent.representation().id().fromAlias("dor"))))
                .predicate(DQLPredicateType.distinct);

        FilterUtils.applySelectFilter(subBuilder, "dor", DocOrdRepresent.representation().type(), types);
        FilterUtils.applySelectFilter(subBuilder, "drs", DocRepresentStudentBase.student().educationOrgUnit().formativeOrgUnit(), formativeOrgUnits);
        FilterUtils.applySelectFilter(subBuilder, "drs", DocRepresentStudentBase.student().course(), courses);
        FilterUtils.applySelectFilter(subBuilder, "drs", DocRepresentStudentBase.student(), students);
        FilterUtils.applySelectFilter(subBuilder, "dor", DocOrdRepresent.representation().reason(), reasons);

        builder.where(DQLExpressions.in(DQLExpressions.property(DocumentOrder.id().fromAlias("o")), subBuilder.buildQuery()));

        FilterUtils.applySimpleLikeFilter(builder, "o", DocumentOrder.operator().identityCard().fullFio().s(), author);

        if (input.getEntityOrder() != null)
        {
            builder.order(DQLExpressions.property("o", input.getEntityOrder().getColumnName()), input.getEntityOrder().getDirection());
            if (input.getEntityOrder().getColumnName().equals("number"))
            {
                builder.order(DQLExpressions.property("o", "commitDate"), input.getEntityOrder().getDirection());
            }
        }

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).build();
    }

    public Date getLastTime(Date date)
    {
        if (date != null)
        {
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            c.add(Calendar.DAY_OF_YEAR, 1);
            c.add(Calendar.MILLISECOND, -1);
            date = c.getTime();
        }
        return date;
    }
}
