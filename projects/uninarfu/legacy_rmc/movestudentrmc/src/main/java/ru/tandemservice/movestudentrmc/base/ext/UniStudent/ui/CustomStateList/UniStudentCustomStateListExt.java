package ru.tandemservice.movestudentrmc.base.ext.UniStudent.ui.CustomStateList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtension;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.movestudentrmc.entity.RelStudentCustomStateRepresent;
import ru.tandemservice.uni.base.bo.UniStudent.ui.CustomStateList.UniStudentCustomStateList;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.education.StudentCustomState;

@Configuration
public class UniStudentCustomStateListExt extends BusinessComponentExtensionManager {

    @Autowired
    public UniStudentCustomStateList parent;

    @Bean
    public ColumnListExtension customStateListCL()
    {
        return columnListExtensionBuilder(parent.customStateListCL())
                .replaceColumn(publisherColumn("desc", StudentCustomState.description()).publisherLinkResolver(new IPublisherLinkResolver() {
                    @Override
                    public Object getParameters(IEntity entity) {
                        Long id = null;
                        RelStudentCustomStateRepresent rel = IUniBaseDao.instance.get().get(RelStudentCustomStateRepresent.class, RelStudentCustomStateRepresent.studentCustomState().id(), entity.getId());
                        if (rel == null)
                            return null;
                        else {
                            if (rel.getListRepresent() != null)
                                id = rel.getListRepresent().getOrder().getId();
                            if (rel.getRepresentation() != null)
                                id = rel.getRepresentation().getOrder().getId();
                        }
                        return new UniMap().add(PublisherActivator.PUBLISHER_ID_KEY, id);
                    }

                    @Override
                    public String getComponentName(IEntity entity) {
                        return null;
                    }
                }))
                .overwriteColumn(actionColumn("delete").disabled("ui:visible"))
                .overwriteColumn(actionColumn("edit").disabled("ui:visible"))
                .create()
                ;
    }

}
