package ru.tandemservice.movestudentrmc.base.bo.DORepresentRecertificationTraining.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentDAO;
import ru.tandemservice.movestudentrmc.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.DocRecertificationTrainingDiscipline;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.RepresentRecertificationTraining;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unisession.entity.document.SessionTransferOutsideOperation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class DORepresentRecertificationTrainingDAO extends AbstractDORepresentDAO
{

    @Override
    public Representation createRepresentObject(RepresentationType type, List<Student> studentList) {
        RepresentRecertificationTraining represent = new RepresentRecertificationTraining();
        represent.setType(type);
        return represent;
    }

    @Override
    public boolean doCommit(Representation represent, ErrorCollector error) {
        if (!super.doCommit(represent, error))
            return false;
        return true;
    }

    @Override
    public boolean doRollback(Representation represent, ErrorCollector error) {
        if (!super.doRollback(represent, error))
            return false;

        return true;
    }

    @Override
    public void buildBodyExtract(Representation representationBase, String itemFac, RtfDocument docCommon) {
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        RtfInjectModifier modifier = new RtfInjectModifier();

        UtilPrintSupport.setParNum(docCommon, itemFac);

        UtilPrintSupport.injectModifierExtract(modifier, docRepresent);

        modifier.modify(docCommon);
    }

    @Override
    public void buildBodyRepresent(Representation representationBase, RtfDocument document, AtomicInteger parNumber, AtomicInteger extractNumber) {
        super.buildBodyRepresent(representationBase, document, parNumber, extractNumber);
        RepresentRecertificationTraining represent = (RepresentRecertificationTraining) representationBase;
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        Student student = UtilPrintSupport.getStudent(represent);
        RtfInjectModifier modifier = new RtfInjectModifier();

        injectModifier(modifier, represent, getList(represent, false), document);
        injectModifier(modifier, represent, getList(represent, true), document);

        UtilPrintSupport.injectModifierRepresent(modifier, docRepresent, student);

        String genderBasedDevelop = "обучающегося";
        if (!student.getPerson().isMale()) {
            genderBasedDevelop = "обучающуюся";
        }

        modifier.put("developPayment", genderBasedDevelop + " " + docRepresent.getCompensationTypeStr());
        modifier.put("highSchool", UtilPrintSupport.getHighLevelSchoolTypeString(student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel()) + " " + student.getEducationOrgUnit().getEducationLevelHighSchool().getDisplayableShortTitle());
        modifier.put("studentTitle", UtilPrintSupport.printSeparatedFIO(student.getPerson().getIdentityCard(), GrammaCase.DATIVE, student.getPerson().isMale()));

        modifier.put("studentSex", student.getPerson().isMale() ? "студенту" : "студентке");

        document.getElementList().remove(document.getElementList().indexOf(UniRtfUtil.findElement(document.getElementList(), "ADDON")) - 1);
        document.getElementList().remove(document.getElementList().indexOf(UniRtfUtil.findElement(document.getElementList(), "ADDON")));

        if (StringUtils.isEmpty(modifier.getStringValue("basics")))
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Arrays.asList("basics"), false, false);

        modifier.modify(document);

    }

    public List<DocRecertificationTrainingDiscipline> getList(RepresentRecertificationTraining represent, boolean flag) {
        MQBuilder builder = new MQBuilder(DocRecertificationTrainingDiscipline.ENTITY_CLASS, "e")
                .add(MQExpression.eq("e", DocRecertificationTrainingDiscipline.representation(), represent))
                .add(MQExpression.eq("e", DocRecertificationTrainingDiscipline.recertification(), flag));

        return MoveStudentDaoFacade.getGrantDAO().getList(builder);
    }

    protected void injectModifier(RtfInjectModifier modifier, RepresentRecertificationTraining represent, List<DocRecertificationTrainingDiscipline> list, RtfDocument document) {
        PrintTemplate templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(PrintTemplate.class, "14.1.1");
        RtfDocument addonTemplate = new RtfReader().read(templateDocument.getContent());
        RtfTableModifier tableModifier = new RtfTableModifier();

        IRtfElement addon = UniRtfUtil.findElement(document.getElementList(), "ADDON");
        if (list != null && !list.isEmpty()) {
            document.getElementList().addAll(document.getElementList().indexOf(addon), addonTemplate.getClone().getElementList());

            modifier.put("recertification", list.get(0).isRecertification() ? "Переаттестовать" : "Перезачесть");
            modifier.put("educationYear", represent.getEducationYear().getTitle());

            List<String[]> tableData = new ArrayList<String[]>();
            for (DocRecertificationTrainingDiscipline docRecertificationTrainingDiscipline : list) {
                tableData.add(fillTableRow(docRecertificationTrainingDiscipline.getSessionTransferOutsideOperation()));
            }
            tableModifier.put("T", tableData.toArray(new String[tableData.size()][]));
        }
        modifier.modify(document);
        tableModifier.modify(document);

    }

    private static String[] fillTableRow(SessionTransferOutsideOperation transferOutsideOperation)
    {
        List<String> result = new ArrayList<String>();

        result.add(transferOutsideOperation.getTargetMark().getSlot().getStudentWpeCAction().getStudentWpe().getSourceRow().getTitle());
        result.add(StringUtils.split(transferOutsideOperation.getTargetMark().getSlot().getStudentWpeCAction().getStudentWpe().getSourceRow().getFormattedLoad(), " ")[0]);
        result.add(transferOutsideOperation.getTargetMark().getValueTitle().toLowerCase());

        return (String[]) result.toArray(new String[result.size()]);
    }

    @Override
    public void buildBodyDeclaration(Representation representationBase, RtfDocument document) {
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        RepresentRecertificationTraining represent = (RepresentRecertificationTraining) representationBase;

        Student student = UtilPrintSupport.getStudent(represent);

        RtfInjectModifier im = new RtfInjectModifier();
        RtfTableModifier tm = new RtfTableModifier();
        UtilPrintSupport.injectModifierDeclaration(im, tm, docRepresent);


        im.put("highSchool", UtilPrintSupport.getDisplayableShortTitle(student.getEducationOrgUnit().getEducationLevelHighSchool()));
        im.put("institute", represent.getInstitute());

        List<DocRecertificationTrainingDiscipline> list = getList(represent, false);
        list.addAll(getList(represent, true));
        List<String[]> tableData = new ArrayList<String[]>();
        for (DocRecertificationTrainingDiscipline docRecertificationTrainingDiscipline : list) {
            tableData.add(fillTableRowDeclaretion(docRecertificationTrainingDiscipline.getSessionTransferOutsideOperation()));
        }
        tm.put("T", tableData.toArray(new String[tableData.size()][]));
        im.modify(document);
        tm.modify(document);
    }

    private static String[] fillTableRowDeclaretion(SessionTransferOutsideOperation transferOutsideOperation)
    {
        List<String> result = new ArrayList<String>();

        result.add(transferOutsideOperation.getTargetMark().getSlot().getStudentWpeCAction().getStudentWpe().getSourceRow().getTitle());
        result.add(StringUtils.split(transferOutsideOperation.getTargetMark().getSlot().getStudentWpeCAction().getStudentWpe().getSourceRow().getFormattedLoad(), " ")[0]);
        result.add(transferOutsideOperation.getControlAction().getTitle().toLowerCase());

        return (String[]) result.toArray(new String[result.size()]);
    }

    @Override
    public GrammaCase getGrammaCase() {
        return GrammaCase.DATIVE;
    }
}
