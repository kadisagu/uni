package ru.tandemservice.movestudentrmc.base.bo.DORepresentWeekendOut.ui.View;

import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentViewUI;
import ru.tandemservice.movestudentrmc.entity.RepresentWeekendOut;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

public class DORepresentWeekendOutViewUI extends AbstractDORepresentViewUI {

    public EducationOrgUnit getEducationOrgUnit() {
        RepresentWeekendOut rep = (RepresentWeekendOut) this._represent;
        if (rep.getEducationOrgUnit() != null)
            return rep.getEducationOrgUnit();
        else
            return rep.getNewGroup().getEducationOrgUnit();
    }
}
