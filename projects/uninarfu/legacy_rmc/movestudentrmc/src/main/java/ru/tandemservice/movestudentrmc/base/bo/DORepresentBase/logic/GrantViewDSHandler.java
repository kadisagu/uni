package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab.MonthWrapper;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntity;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.StuGrantStatusCodes;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class GrantViewDSHandler extends DefaultComboDataSourceHandler {

    public static final String GRANT_VIEW_DS = "grantViewDS";
    public static final String STUDENT_ID = "studentId";
    public static final String EDU_YEAR = "eduYear";
    public static final String MONTH = "month";
    public static final String SUSPEND = "suspend";
    public static final String STATUS = "status";

    public GrantViewDSHandler(String ownerId) {

        super(ownerId, GrantView.class);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep) {

        Long studentId = (Long) ep.context.get(STUDENT_ID);
        //--------------------------------------------------------------------
        //EducationYear eduYear = ep.context.get(EDU_YEAR);
        //--------------------------------------------------------------------
        Date date = (Date) ep.context.get(MONTH);
        Boolean suspend = (Boolean) ep.context.get(SUSPEND);
        String status = (String) ep.context.get(STATUS);

        if (status == null)
            status = StuGrantStatusCodes.CODE_1;

        //----------------------------------------------------------------------------------------------------------------------
        /*
        if (date != null && eduYear != null && studentId != null) {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentGrantEntity.class, "sge")
                    .column(DQLExpressions.property(StudentGrantEntity.view().id().fromAlias("sge")))
                    .where(DQLExpressions.eq(DQLExpressions.property(StudentGrantEntity.status().code().fromAlias("sge")),
                                             DQLExpressions.value(status)))
                    .where(DQLExpressions.eq(DQLExpressions.property(StudentGrantEntity.student().id().fromAlias("sge")),
                                             DQLExpressions.value(studentId)))
                    .where(DQLExpressions.eq(DQLExpressions.property(StudentGrantEntity.eduYear().fromAlias("sge")),
                                             DQLExpressions.value(eduYear)))
                    .where(DQLExpressions.eq(DQLExpressions.property(StudentGrantEntity.month().fromAlias("sge")),
                                             DQLExpressions.value(getStringValue(date, eduYear))))
                    .where(DQLExpressions.or(DQLExpressions.eq(DQLExpressions.property(StudentGrantEntity.tmp().fromAlias("sge")),
                                                               DQLExpressions.value(Boolean.FALSE)),
                                             DQLExpressions.isNull(DQLExpressions.property(StudentGrantEntity.tmp().fromAlias("sge")))));

            ep.dqlBuilder.where(DQLExpressions.in(DQLExpressions.property(GrantView.id().fromAlias("e")), builder.getQuery()));
        }
        */
        
        if (date != null && studentId != null) {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentGrantEntity.class, "sge")
                    .column(DQLExpressions.property(StudentGrantEntity.view().id().fromAlias("sge")))
                    .where(DQLExpressions.eq(DQLExpressions.property(StudentGrantEntity.status().code().fromAlias("sge")),
                                             DQLExpressions.value(status)))
                    .where(DQLExpressions.eq(DQLExpressions.property(StudentGrantEntity.student().id().fromAlias("sge")),
                                             DQLExpressions.value(studentId)))
                    .where(DQLExpressions.in(DQLExpressions.property(StudentGrantEntity.month().fromAlias("sge")),
                                             getMonthList(date, studentId)))
                    .where(DQLExpressions.or(DQLExpressions.eq(DQLExpressions.property(StudentGrantEntity.tmp().fromAlias("sge")),
                                                               DQLExpressions.value(Boolean.FALSE)),
                                             DQLExpressions.isNull(DQLExpressions.property(StudentGrantEntity.tmp().fromAlias("sge")))));

            ep.dqlBuilder.where(DQLExpressions.in(DQLExpressions.property(GrantView.id().fromAlias("e")), builder.getQuery()));
        }        
        //----------------------------------------------------------------------------------------------------------------------

        if (suspend != null && suspend) {
            ep.dqlBuilder.where(DQLExpressions.eq(
                    DQLExpressions.property(GrantView.suspend().fromAlias("e")),
                    DQLExpressions.value(suspend)
            ));
        }

        ep.dqlBuilder.where(eq(property(GrantView.use().fromAlias("e")), value(true)));

        String filter = ep.input.getComboFilterByValue();

        if (StringUtils.isNotBlank(filter)) {
            ep.dqlBuilder.where(like(DQLFunctions.upper(property(GrantView.title().fromAlias("e"))), value(CoreStringUtils.escapeLike(filter, true))));
        }
    }

    private String getStringValue(Date date, EducationYear year) {
        return new StringBuilder()
                .append(CoreDateUtils.getMonth(date) + 1)
                .append(".")
                .append(year.getTitle())
                .toString();

    }
    
    //-------------------------------------------------------------------------------------------------------------------------
    private List<String> getMonthList(Date date, Long studentId) {

        //Последний учебный год назначения выплат
        DQLSelectBuilder lastYearBuilder = new DQLSelectBuilder()
        		.fromEntity(StudentGrantEntity.class, "sge")
        		.where(eq(property(StudentGrantEntity.status().code().fromAlias("sge")), value(StuGrantStatusCodes.CODE_1)))
        		.where(eq(property(StudentGrantEntity.student().id().fromAlias("sge")), value(studentId)))
        		.column(property(StudentGrantEntity.eduYear().intValue().fromAlias("sge")))
        		.order(property(StudentGrantEntity.eduYear().intValue().fromAlias("sge")), OrderDirection.desc)
        		.top(1)
        		;
        int lastYear = lastYearBuilder.createStatement(getSession()).uniqueResult();
        
        //Последний день последнего учебного года назначения выплат
        Calendar lastYearDate = Calendar.getInstance();
        lastYearDate.set(lastYear + 1, Calendar.AUGUST, 31, 0, 0, 0);	//31.08.year+1

        return MonthWrapper.getMonthsList(date, lastYearDate.getTime());

    }    
    //-------------------------------------------------------------------------------------------------------------------------

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
    {
        ep.dqlBuilder.order("e." + GrantView.shortTitle());
    }
}
