package ru.tandemservice.movestudentrmc.component.represent.StudentsGrants;

import ru.tandemservice.uni.dao.IUniDao;

public abstract interface IDAO extends IUniDao<Model> {

    public abstract void update(Model model);

}
