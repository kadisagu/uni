package ru.tandemservice.movestudentrmc.component.catalog.representationType.RepresentationTypePub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;

public class Controller extends DefaultCatalogPubController<RepresentationType, Model, IDAO> {

    @Override
    protected DynamicListDataSource<RepresentationType> createListDataSource(IBusinessComponent context) {
        Model model = context.getModel();
        DynamicListDataSource<RepresentationType> ds = new DynamicListDataSource<RepresentationType>(context, this);
        ds.addColumn(new SimpleColumn("Наименование", "title").setClickable(false), 1);
        ds.addColumn(new SimpleColumn("Номер", "priority").setClickable(false), 2);
        ds.addColumn(new ToggleColumn("Групповой", Model.RepresentationWrapper.GROUPING).setListener("onClickToggleGrouping"), 3);
        ds.addColumn(new ToggleColumn("Использовать", Model.RepresentationWrapper.USE).setListener("onClickToggleUse"), 4);
        ds.addColumn(new ToggleColumn("Отнести к распоряжениям", Model.RepresentationWrapper.INSTRUCTION).setListener("onClickToggleInstruction"), 5);
        ds.addColumn(new ToggleColumn("Списочное представление", Model.RepresentationWrapper.LIST_REPRESENTATION).setListener("onClickToggleListRepresentation"), 6);
        ds.addColumn(new ToggleColumn("Печать с приложением", RepresentationType.printAttachment().s()).setListener("onClickTogglePrintAttachment").setEnabledProperty(RepresentationType.listRepresentation().s()), 7);
        ds.addColumn(new ToggleColumn("Печать заявления", RepresentationType.printDeclaration().s()).setListener("onClickTogglePrintDeclaration"), 8);
//		ds.addColumn(new ToggleColumn("Учитывать сохранение стипендий/выплат", RepresentationType.useGrant().s()).setListener("onClickToggleUseGrant"), 9);
        ds.addColumn(new ToggleColumn("Выполнять проверку", RepresentationType.check().s()).setListener("onClickToggleCheck"), 10);
        ds.addColumn(new ActionColumn("Редактировать", "edit", "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
        ds.setOrder("priority", OrderDirection.asc);
        return ds;
    }

    public void onClickToggleCheck(IBusinessComponent component)
    {
        getDao().updateCheck(getModel(component), (Long) component.getListenerParameter());
    }

    public void onClickToggleGrouping(IBusinessComponent component)
    {
        getDao().updateGrouping(getModel(component), (Long) component.getListenerParameter());
    }

    public void onClickToggleUse(IBusinessComponent component)
    {
        getDao().updateInUse(getModel(component), (Long) component.getListenerParameter());
    }

    public void onClickToggleInstruction(IBusinessComponent component)
    {
        getDao().updateInstruction(getModel(component), (Long) component.getListenerParameter());
    }

    public void onClickToggleListRepresentation(IBusinessComponent component)
    {
        getDao().updateListRepresentation(getModel(component), (Long) component.getListenerParameter());
    }

    public void onClickTogglePrintAttachment(IBusinessComponent component) {
        getDao().updatePrintAttachment(getModel(component), (Long) component.getListenerParameter());
    }

    public void onClickTogglePrintDeclaration(IBusinessComponent component) {
        getDao().updatePrintDeclaration(getModel(component), (Long) component.getListenerParameter());
    }
    /*
	public void onClickToggleUseGrant(IBusinessComponent component)  {
        getDao().updateUseGrant(getModel(component), (Long) component.getListenerParameter());
    }
    */
}
