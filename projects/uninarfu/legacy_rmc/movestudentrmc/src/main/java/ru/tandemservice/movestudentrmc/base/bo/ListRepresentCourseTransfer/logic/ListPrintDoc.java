package ru.tandemservice.movestudentrmc.base.bo.ListRepresentCourseTransfer.logic;

import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;

import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.AbstractListPrintDoc;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListRepresentCourseTransfer;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.employee.Student;
//-----------------------------------------------------------------------
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
//-----------------------------------------------------------------------

import java.util.*;

public class ListPrintDoc extends AbstractListPrintDoc<ListRepresentCourseTransfer>
{

    private Map<Student, Integer> studentNumber = new HashMap<>();

    @Override
    protected String getTemplateHeader()
    {
        return "list.rep.course.transfer";
    }

    @Override
    protected String getTemplateParag()
    {
        return "list.rep.course.transfer.parag";
    }

    @Override
    protected String getTemplateAddon()
    {
        return null;
    }

    @Override
    protected void paragraphInjectModifier(RtfInjectModifier im,
                                           ListRepresentCourseTransfer listRepresent, Student student)
    {
        injectModifier(im, listRepresent, student);
    }

    @Override
    protected Class<? extends ListRepresent> getListRepresentClass()
    {
        return ListRepresentCourseTransfer.class;
    }

    @Override
    protected String getKey(Student student,
                            ListRepresentCourseTransfer represent)
    {
        return new StringBuilder()
        		//------------------------------------------------------------------------------------
                //.append(student.getEducationOrgUnit().getDevelopForm().getId())
                //.append(represent.getDateBeginingTransfer().getTime())
        		
        		.append(student.getEducationOrgUnit().getDevelopForm().getCode())
        		.append(represent.getNewCourse().getCode())
        		.append(student.getCourse().getCode())
        		.append(represent.getDateBeginingTransfer().getTime())
        		.append(represent.isConditionally() ? "1" : "0")
        		.append(represent.getDeadlineDate()!=null ? represent.getDeadlineDate().getTime() : 0)
        		//------------------------------------------------------------------------------------
                .toString();
    }

    @Override
    protected RtfDocument getParagraph(RtfDocument template, int formCounter,
                                       int counter, int addonCounter, List<Object[]> studentList)
    {
        RtfDocument result = template.getClone();
        ListRepresentCourseTransfer listRepresent = (ListRepresentCourseTransfer) studentList.get(0)[1];
        Student student = (Student) studentList.get(0)[0];

        //---------------------------------------------------------------------------
        /*
        Map<Course, List<Student>> courseMap = new HashMap<>();

        for (Object[] o : studentList)
        {
            Student s = (Student) o[0];
            ListRepresentCourseTransfer repr = (ListRepresentCourseTransfer) o[1];
            Course key = repr.getNewCourse();
            getParagInfomap().put(s, new OrderParagraphInfo(formCounter, counter));

            List<Student> lst = courseMap.get(key);
            if (lst == null)
                lst = new ArrayList<>();
            lst.add(s);

            courseMap.put(key, lst);
        }
        */
        //---------------------------------------------------------------------------
        
        RtfInjectModifier im = new RtfInjectModifier();
        injectModifier(im, listRepresent, student);
        im.put("i", String.valueOf(formCounter).trim());
        im.put("j", String.valueOf(counter).trim());
        
        //----------------------------------------------------------------------------------------------------------------------------------------
        //Изменены формирование и сортировка абзацев приказа
        Map<EduProgramKind, List<Student>> eduProgramKindMap = new HashMap<>();        
        
        for (Object[] o : studentList) {
            Student s = (Student) o[0];
            //ListRepresentCourseTransfer repr = (ListRepresentCourseTransfer) o[1];
            getParagInfomap().put(s, new OrderParagraphInfo(formCounter, counter));
            EduProgramKind key = s.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject().getEduProgramKind();
            List<Student> lst = eduProgramKindMap.get(key);
            if (lst == null)
                lst = new ArrayList<>();            
            lst.add(s);
            eduProgramKindMap.put(key, lst);
        }
        
        List<EduProgramKind> keyList = new ArrayList<>(eduProgramKindMap.keySet());
        Collections.sort(keyList, new EntityComparator<>(new EntityOrder(EduProgramKind.code(), OrderDirection.asc)));
        
        for (EduProgramKind kind : keyList) {
        	Map<EducationLevelsHighSchool, List<Student>> levelMap = new HashMap<>();
            List<Student> studList = eduProgramKindMap.get(kind);
            for (Student s : studList) {
            	EducationLevelsHighSchool key = s.getEducationOrgUnit().getEducationLevelHighSchool();
                List<Student> lst = levelMap.get(key);
                if (lst == null)
                    lst = new ArrayList<>();
                lst.add(s);
                levelMap.put(key, lst);
            }
            
            List<EducationLevelsHighSchool> eduLevelList = new ArrayList<>(levelMap.keySet());
            Collections.sort(eduLevelList, new EntityComparator<>(new EntityOrder(EducationLevelsHighSchool.displayableTitle())));

            for (EducationLevelsHighSchool eduLevel : eduLevelList)
            {
                insertRow(result, UtilPrintSupport.getHighLevelSchoolTypeString(eduLevel));
                studList = levelMap.get(eduLevel);

                Map<String, List<Student>> groupMap = new HashMap<>();

                for (Student s : studList)
                {
                    String key = s.getGroup() != null ? s.getGroup().getTitle() : "";

                    List<Student> lst = groupMap.get(key);
                    if (lst == null)
                        lst = new ArrayList<>();
                    lst.add(s);

                    groupMap.put(key, lst);
                }

                List<String> groupList = new ArrayList<>(groupMap.keySet());

                Collections.sort(groupList);

                for (String group : groupList)
                {
                    if (!group.isEmpty())
                        insertRow(result, "группа " + group);
                    studList = groupMap.get(group);
                    Collections.sort(studList, new EntityComparator<>(new EntityOrder(Student.person().identityCard().fullFio())));
                    int n = 1;
                    for (Student std : studList)
                    {
                        result.getElementList().addAll(new RtfString().append(n + ")").append(IRtfData.TAB)
                                .append(std.getPerson().getFullFio())
                                .append(std.getCompensationType().isBudget() ? "" : " (по договору)")
                                .par().toList());
                        studentNumber.put(std, n);
                        n++;
                    }
                    //result.addElement(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
                }
            }
        	
        }
        result.addElement(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));

        /*
        List<Course> keyList = new ArrayList<>(courseMap.keySet());

        Collections.sort(keyList,
                new EntityComparator<>(new EntityOrder(Course.intValue(), OrderDirection.desc)));

        for (Course corse : keyList)
        {

            insertRow(result, "на " + corse.getTitle() + " курс");

            Map<EducationLevelsHighSchool, List<Student>> levelMap = new HashMap<>();
            List<Student> studList = getSortQualificationList(courseMap.get(corse));

            for (Student s : studList)
            {
                EducationLevelsHighSchool key = s.getEducationOrgUnit().getEducationLevelHighSchool();

                List<Student> lst = levelMap.get(key);
                if (lst == null)
                    lst = new ArrayList<>();
                lst.add(s);

                levelMap.put(key, lst);
            }

            List<EducationLevelsHighSchool> eduLevelList = new ArrayList<>(levelMap.keySet());

            Collections.sort(eduLevelList,
                    new EntityComparator<>(new EntityOrder(EducationLevelsHighSchool.displayableTitle())));

            for (EducationLevelsHighSchool eduLevel : eduLevelList)
            {
                insertRow(result, UtilPrintSupport.getHighLevelSchoolTypeString(eduLevel));
                studList = levelMap.get(eduLevel);

                Map<String, List<Student>> groupMap = new HashMap<>();

                for (Student s : studList)
                {
                    String key = s.getGroup() != null ? s.getGroup().getTitle() : "";

                    List<Student> lst = groupMap.get(key);
                    if (lst == null)
                        lst = new ArrayList<>();
                    lst.add(s);

                    groupMap.put(key, lst);
                }

                List<String> groupList = new ArrayList<>(groupMap.keySet());

                Collections.sort(groupList);

                for (String group : groupList)
                {
                    if (!group.isEmpty())
                        insertRow(result, "группа " + group);
                    studList = groupMap.get(group);
                    Collections.sort(studList,
                            new EntityComparator<>(new EntityOrder(Student.person().identityCard().fullFio())));
                    int n = 1;
                    for (Student std : studList)
                    {
                        result.getElementList().addAll(new RtfString().append(n + ")").append(IRtfData.TAB)
                                .append(std.getPerson().getFullFio())
                                .append(std.getCompensationType().isBudget() ? "" : " (по договору)")
                                .par().toList());
                        studentNumber.put(std, n);
                        n++;
                    }
                    result.addElement(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
                }
            }

        }
        */
        //----------------------------------------------------------------------------------------------------------------------------------------

        im.modify(result);
        return result;
    }

    public Map<Student, Integer> getStudentNumberMap()
    {
        return studentNumber;
    }

    public static void injectModifier(RtfInjectModifier im, ListRepresentCourseTransfer listRepresent, Student student)
    {
        im.put("data", UtilPrintSupport.getDateFormatterWithMonthString(listRepresent.getDateBeginingTransfer()));
        im.put("form", UtilPrintSupport.getDevelopFormGen(student.getEducationOrgUnit().getDevelopForm()));
        //--------------------------------------------------------------------------------------------------------------------------------------------------
        im.put("conditionally", listRepresent.isConditionally() ? " условно" : "");
        im.put("newCourse", listRepresent.getNewCourse().getTitle());
        
        RtfString withConditionString = new RtfString();
        
        if(!listRepresent.isConditionally()) {
        	withConditionString.append(" как полностью выполнивших учебный план ").append(student.getCourse().getTitle()).append(" курса");
        }
        else {	//условный перевод
            String day = RussianDateFormatUtils.getDayString(listRepresent.getDeadlineDate(), true);
            String month = CommonBaseDateUtil.getMonthNameDeclined(listRepresent.getDeadlineDate(), GrammaCase.GENITIVE);
            String year = RussianDateFormatUtils.getYearString(listRepresent.getDeadlineDate(), false);
            withConditionString.append(" и установить срок ликвидации академической задолженности до ").append(day).append(IRtfData.SYMBOL_TILDE).append(month).append(" ").append(year).append(IRtfData.SYMBOL_TILDE).append("года");
        }
        im.put("withCondition", withConditionString);
        //--------------------------------------------------------------------------------------------------------------------------------------------------
    }

    //-----------------------------------------------------------------------------------------------------------------------------------
    /*
    public List<Student> getSortQualificationList(List<Student> list)
    {
        //сортируем студентов
        List<Student> result = new ArrayList<Student>();
        for (Student student : list)
        {
            //в первую очередь идут студенты без группы
            if (student.getGroup() == null)
                result.add(student);
        }
        for (Student student : list)
        {
            //затем бакалавры
            if (student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isBachelor() && (student.getGroup() != null))
                result.add(student);
        }
        for (Student student : list)
        {
            //затем специалисты
            if ((student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isSpecialization() || student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isSpecialty()) && (student.getGroup() != null))
                result.add(student);
        }
        for (Student student : list)
        {
            //затем магистры
            if (student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isMaster() && (student.getGroup() != null))
                result.add(student);
        }
        return result;
    }
    */
    //----------------------------------------------------------------------------------------------------------------------------------

}
