package ru.tandemservice.movestudentrmc.base.bo.ListRepresentSocialGrant.logic;

import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.AbstractListRepresentDAO;
import ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab.MonthWrapper;
import ru.tandemservice.movestudentrmc.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudentrmc.docord.IListPrintDoc;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.ReportRenderer;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.*;

public class ListRepresentSocialGrantManagerModifyDAO extends AbstractListRepresentDAO implements IListRepresentSocialGrantManagerModifyDAO {

    @Transactional
    public void saveSupport(ListRepresent listRepresent, List<DataWrapper> studentSelectedList, DocListRepresentBasics representBasics) {

        ListRepresentSocialGrant listGrant = (ListRepresentSocialGrant) listRepresent;

        if (!inOneMonth(listGrant.getDateBeginingPayment(), listGrant.getDateEndOfPayment())) {
            throw new ApplicationException("Поля \"Дата начала выплаты\", и \"Дата окончания выплаты\" должны быть в одном месяце");
        }

        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(RelListRepresentStudents.class)
                .fromEntity(RelListRepresentStudents.class, "rls")
                .where(DQLExpressions.eq
                        (DQLExpressions.property(RelListRepresentStudents.representation()), DQLExpressions.value(listRepresent)));

        //Почистим старых студентов
        deleteBuilder.createStatement(getSession()).execute();

        //Сохраним оператора, изменившего представление
        Person person = PersonManager.instance().dao().getPerson(ContextLocal.getUserContext().getPrincipalContext());
        if (person != null)
            listGrant.setOperator(person);


        Map<Student, Double> grantMap = new HashMap<Student, Double>();
        List<Student> stuSelectedList = new ArrayList<Student>();
        for (DataWrapper dataW : studentSelectedList) {
            Student student = (Student) dataW.getProperty("student");
            StudentGrantEntity studentGrantEntity = (StudentGrantEntity) dataW.getProperty("grant");

            grantMap.put(student, studentGrantEntity.getSum());
            stuSelectedList.add(student);
        }


        EducationYear eduYear = getByDate(listGrant.getDateBeginingPayment());
        List<String> periodList = MonthWrapper.getMonthsList(listGrant.getDateBeginingPayment(), listGrant.getDateEndOfPayment(), eduYear);
        String month = periodList.get(0);
        List<String> errorList = new ArrayList<String>();

        boolean good = MoveStudentDaoFacade.getGrantDAO().checkStudentGrants(listGrant.getId(), listGrant.getGrantView(), stuSelectedList, periodList, errorList);

        if (!good) {
            RtfDocument errorReport = RtfBean.getElementFactory().createRtfDocument();
            IRtfControl par = RtfBean.getElementFactory().createRtfControl(IRtfData.PAR);
            for (String text : errorList) {
                IRtfText studText = RtfBean.getElementFactory().createRtfText(text);
                errorReport.addElement(studText);
                errorReport.getElementList().addAll(Arrays.asList(par, par));
            }
            BusinessComponentUtils.downloadDocument(new ReportRenderer("Ошибки.rtf", errorReport), true);

            throw new ApplicationException("Не удалось сохранить представление");
        }

        baseCreateOrUpdate(listRepresent);
        baseCreateOrUpdate(representBasics);
        for (Student student : stuSelectedList) {
            RelListRepresentStudents relListRepresentGrantStudents = new RelListRepresentStudents();
            relListRepresentGrantStudents.setRepresentation(listRepresent);
            relListRepresentGrantStudents.setStudent(student);
            baseCreateOrUpdate(relListRepresentGrantStudents);
        }

        MoveStudentDaoFacade.getGrantDAO().deleteStudentGrants(listGrant);
        MoveStudentDaoFacade.getGrantDAO().saveStudentGrants(listGrant, listGrant.getGrantView(), grantMap, month, eduYear);
    }

    //-------------------------------------------------------------------------------------------------------------------
    /*
    @Override
    public void buildBodyExtract(ListOrdListRepresent listOrdListRepresent, Student student, RtfDocument docExtract) {

        super.buildBodyExtract(listOrdListRepresent, student, docExtract);

        ListRepresentSocialGrant representation = (ListRepresentSocialGrant) listOrdListRepresent.getRepresentation();
        RtfInjectModifier im = new RtfInjectModifier();

        im.put("ed_form_Genetive", student.getEducationOrgUnit().getDevelopForm().getGenCaseTitle().toLowerCase());
        im.put("Spec_gr_Generive", representation.getGrantView().getGenitive());
//		im.put("st_date", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(representation.getDateBeginingPayment()));
//		im.put("end_date",representation.getDateEndOfPayment() !=null ?  DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(representation.getDateEndOfPayment()) : "");
        im.put("pay", student.getCompensationType().getCode().equals(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET) ? "за счет средств федерального бюджета" : student.getCompensationType().getShortTitle());
        im.modify(docExtract);
    }
    */
    
    @Override
    public void buildBodyExtract(ListOrdListRepresent listOrdListRepresent, Student student, RtfDocument docExtract,
                Map<Student, OrderParagraphInfo> map) 	{    	

    	super.buildBodyExtract(listOrdListRepresent, student, docExtract, map);

        ListRepresentSocialGrant representation = (ListRepresentSocialGrant) listOrdListRepresent.getRepresentation();
        RtfInjectModifier im = new RtfInjectModifier();

        im.put("ed_form_Genetive", student.getEducationOrgUnit().getDevelopForm().getGenCaseTitle().toLowerCase());
        im.put("Spec_gr_Generive", representation.getGrantView().getGenitive());
//		im.put("st_date", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(representation.getDateBeginingPayment()));
//		im.put("end_date",representation.getDateEndOfPayment() !=null ?  DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(representation.getDateEndOfPayment()) : "");
        im.put("pay", student.getCompensationType().getCode().equals(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET) ? "за счет средств федерального бюджета" : student.getCompensationType().getShortTitle());
        im.modify(docExtract);
    }  
    //-------------------------------------------------------------------------------------------------------------------
    

    @Override
    public Map<Student, OrderParagraphInfo> buildBodyRepresent(List<ListRepresent> representationBase, RtfDocument document) {
        ListPrintDoc listPrintDoc = new ListPrintDoc();
        RtfDocument rtfDocument = listPrintDoc.creationPrintDocOrder(representationBase);
        document.getElementList().addAll(rtfDocument.getElementList());

        return ((IListPrintDoc) listPrintDoc).getParagInfomap();
    }


    @Override
    public boolean doRollback(ListRepresent represent, ErrorCollector error) {
        ListRepresentSocialGrant listRepresentGrant = (ListRepresentSocialGrant) represent;

        List<ListOrdListRepresent> orders = DataAccessServices.dao().getList(ListOrdListRepresent.class, ListOrdListRepresent.representation(), represent);
        ListOrder order = orders.get(0).getOrder();

        try {
            MoveStudentDaoFacade.getGrantDAO().rollbackStudentGrants(listRepresentGrant, order);
        }
        catch (Exception ex) {
            //TODO: а что тут надо делать?
            ex.printStackTrace();
            return false;
        }

        return true;
    }

    @Override
    public void update(ListRepresent listRepresent, List<Student> studentSelectedList) {
    }

    @Override
    public boolean doCommit(ListRepresent represent, ErrorCollector error) {
        ListRepresentSocialGrant listRepresentGrant = (ListRepresentSocialGrant) represent;


        List<ListOrdListRepresent> orders = DataAccessServices.dao().getList(ListOrdListRepresent.class, ListOrdListRepresent.representation(), represent);
        ListOrder order = orders.get(0).getOrder();

        try {
            MoveStudentDaoFacade.getGrantDAO().commitStudentGrants(listRepresentGrant, order);
        }
        catch (Exception ex) {
            //TODO: а что тут надо делать?
            ex.printStackTrace();
            return false;
        }
        return true;
    }


    @Override
    public List<Student> selectRepresent(ListRepresent listRepresent) {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(RelListRepresentStudents.class, "rls");

        builder.where(DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("rls")), DQLExpressions.value(listRepresent)));

        builder.column(DQLExpressions.property(RelListRepresentStudents.student().fromAlias("rls")));

        return builder.createStatement(getSession()).list();
    }

    @Override
    public EducationYear getByDate(Date date) {
        if (date == null) return null;

        int year = CoreDateUtils.getYear(date);
        if (CoreDateUtils.getMonth(date) <= Calendar.AUGUST)
            year -= 1;

        return IUniBaseDao.instance.get().get(EducationYear.class, EducationYear.intValue(), year);
    }

    @Override
    public boolean inOneMonth(Date date1, Date date2) {
        if (date1 == null || date2 == null)
            return true;

        if (CoreDateUtils.getMonth(date1) != CoreDateUtils.getMonth(date2))
            return false;
        else if (CoreDateUtils.getYear(date1) != CoreDateUtils.getYear(date2))
            return false;

        return true;
    }
}
