package ru.tandemservice.movestudentrmc.base.bo.DOOrder.ui.ForeignerVisaEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;

public class DOOrderForeignerVisaEditUI extends UIPresenter {

    private String postTitle;
    private String postFio;

    @Override
    public void onComponentRefresh() {
        IDataSettings settings = DataSettingsFacade.getSettings("foreignerVisa");
        Object post = settings.get("postTitle");
        Object fio = settings.get("postFio");

        if (post instanceof String)
            setPostTitle(String.valueOf(post));

        if (fio instanceof String)
            setPostFio(String.valueOf(fio));

    }

    public void onClickSave() {

        IDataSettings settings = DataSettingsFacade.getSettings("foreignerVisa");

        settings.set("postTitle", String.valueOf(getPostTitle()));
        settings.set("postFio", String.valueOf(getPostFio()));

        DataSettingsFacade.saveSettings(settings);

        deactivate();
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostFio() {
        return postFio;
    }

    public void setPostFio(String postFio) {
        this.postFio = postFio;
    }

}
