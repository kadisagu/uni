package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab.MonthWrapper;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntity;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MS_movestudentrmc_1x0x0_23to24 extends IndependentMigrationScript {
    private DBTool tool;
    private IEntityMeta meta;

    @Override
    public void run(DBTool tool) throws Exception {
        this.meta = EntityRuntime.getMeta(StudentGrantEntity.class);
        if (!tool.tableExists(meta.getTableName()))
            return;

        this.tool = tool;
        process();
    }

/*
select
	rep.ID,
	repgr.DATEBEGININGPAYMENT_P,
	repgr.DATEENDOFPAYMENT_P,
	ey.intvalue_p,
	ey.title_p
from
	LISTREPRESENT_T rep
	inner join LISTREPRESENTGRANT_T repgr on repgr.ID=rep.ID
	inner join educationyear_t ey on ey.id=repgr.EDUCATIONYEAR_ID	
 */

    private void process() throws Exception {
        String sql = ""
                + " select"
                + " rep.ID,"
                + " repgr.DATEBEGININGPAYMENT_P,"
                + " repgr.DATEENDOFPAYMENT_P,"
                + " ey.intvalue_p,"
                + " ey.title_p"
                + " from"
                + " LISTREPRESENT_T rep"
                + " inner join LISTREPRESENTGRANT_T repgr on repgr.ID=rep.ID"
                + " inner join educationyear_t ey on ey.id=repgr.EDUCATIONYEAR_ID";
        ResultSet rs = this.tool.getConnection().createStatement().executeQuery(sql);

        PreparedStatement pst = tool.getConnection().prepareStatement("delete from " + this.meta.getTableName() + " where MONTH_P=? and LISTREPRESENT_ID=?");
        while (rs.next()) {
            RepresentData data = new RepresentData(rs);
            List<String> badPeriods = data.getBadPeriods();
            for (String bad : badPeriods) {
                int count = removeBad(data, bad, pst);
                //if (count > 0)
                //	System.out.println("id:" + data.id + " month:" + bad + ", deleted: " + count);
            }
        }
        rs.close();
    }

    private int removeBad(RepresentData data, String badMonth, PreparedStatement pst) throws Exception {
        pst.setString(1, badMonth);
        pst.setLong(2, data.id);

        return pst.executeUpdate();
    }

    private static class RepresentData {
        private Long id;
        private Date startDate;
        private Date endDate;
        private int year;
        private String title;

        private RepresentData(ResultSet rs) throws Exception {
            this.id = rs.getLong(1);
            this.startDate = rs.getDate(2);
            this.endDate = rs.getDate(3);
            this.year = rs.getInt(4);
            this.title = rs.getString(5);
        }

        private List<String> getBadPeriods() {
            EducationYear educationYear = new EducationYear();
            educationYear.setIntValue(this.year);
            educationYear.setTitle(this.title);

            List<String> totalPeriods = MonthWrapper.getMonthsList(this.startDate, this.endDate, educationYear);

            Calendar beginYear = Calendar.getInstance();
            beginYear.set(this.year, 8, 1, 0, 0, 0);
            beginYear.set(14, 0);
            Calendar endYear = Calendar.getInstance();
            endYear.set(this.year + 1, 7, 31, 23, 59, 59);
            endYear.set(14, 999);

            Calendar c = Calendar.getInstance();
            c.setTime(this.startDate);
            if (c.before(beginYear))
                c.setTime(beginYear.getTime());
            if (c.after(endYear))
                c.setTime(endYear.getTime());
            this.startDate = c.getTime();

            c = Calendar.getInstance();
            c.setTime(this.endDate);
            if (c.before(beginYear))
                c.setTime(beginYear.getTime());
            if (c.after(endYear))
                c.setTime(endYear.getTime());
            this.endDate = c.getTime();

            List<String> okPeriods = MonthWrapper.getMonthsList(this.startDate, this.endDate, educationYear);

            totalPeriods.removeAll(okPeriods);
            return totalPeriods;
        }
    }

}
