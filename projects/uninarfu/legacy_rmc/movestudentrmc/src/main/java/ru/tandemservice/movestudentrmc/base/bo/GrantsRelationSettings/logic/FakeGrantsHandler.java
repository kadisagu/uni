package ru.tandemservice.movestudentrmc.base.bo.GrantsRelationSettings.logic;

import ru.tandemservice.movestudentrmc.entity.catalog.Grant;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;

import java.util.List;

public class FakeGrantsHandler {

    private Grant grant;
    private List<GrantView> viewList;
    private Boolean edit = false;

    public Boolean getEdit() {
        return edit;
    }

    public void setEdit(Boolean edit) {
        this.edit = edit;
    }

    public Grant getGrant() {
        return grant;
    }

    public void setGrant(Grant grant) {
        this.grant = grant;
    }

    public List<GrantView> getViewList() {
        return viewList;
    }

    public void setViewList(List<GrantView> viewList) {
        this.viewList = viewList;
    }
}
