package ru.tandemservice.movestudentrmc.base.bo.ListOrder.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.core.entity.EntityComparator;

import java.util.Collections;
import java.util.List;

public class RepresentSelectedDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput> {

    public static final String DEVELOP_FORM_COLUMN = "developForm";
    public static final String FORMATIVE_ORG_UNIT_COLUMN = "formativeOrgUnit";
    public static final String TERRITORIAL_ORG_UNIT_COLUMN = "territorialOrgUnit";
    public static final String EDUCATION_LEVEL_HIGH_SCHOOL_COLUMN = "educationLevelHighSchool";
    public static final String EDUCATION_ORG_UNIT_COLUMN = "educationOrgUnit";
    public static final String TYPE_REPRESENT_COLUMN = "typeRepresent";
    public static final String COURSE_COLUMN = "course";
    public static final String GROUP_COLUMN = "group";
    public static final String BUDGET_COLUMN = "budget";
    public static final String DATE_REPRESENT_COLUMN = "dateRepresent";
    public static final String STUDENT_FIO_COLUMN = "studentFio";
    public static final String CHECKBOX_COLUMN = "checkbox";

    public static final String REPRESENT_SELECTED_LIST = "representSelectedList";

    public RepresentSelectedDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Collections.sort(context.<List>get(REPRESENT_SELECTED_LIST), new EntityComparator(input.getEntityOrder()));
        return ListOutputBuilder.get(input, context.<List>get(REPRESENT_SELECTED_LIST)).build();
    }
}
