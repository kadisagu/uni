package ru.tandemservice.movestudentrmc.base.bo.ListRepresentDiplomAndExclude.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.RtfText;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudentrmc.docord.IListPrintDoc;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListRepresentDiplomAndExclude;
import ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents;
import ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate;
//---------------------------------------------------------------------------
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
//---------------------------------------------------------------------------
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.*;

public class ListPrintDoc implements IListPrintDoc
{

    private Map<Student, OrderParagraphInfo> hashMap = new HashMap<>();

    //	private  RtfDocument template;
//	private  RtfDocument templateClone;
    private int i;
    private int j;
    private int size; //количество представлений в приказе
//	private  boolean firstLoad;

    public synchronized RtfDocument creationPrintDocOrder(List<? extends ListRepresent> listOfRepresents)
    {

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(RelListRepresentStudents.class, "rls")
                .where(DQLExpressions.in(DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("rls")),
                        listOfRepresents));


        List<RelListRepresentStudents> listRepresentStudents = builder.createStatement(DataAccessServices.dao().getComponentSession()).list();
        size = listOfRepresents.size();
        List<Student> fiveStudentList = new ArrayList<>();
        //формируем список студентов с отличием
        for (RelListRepresentStudents representStudents : listRepresentStudents)
        {
            if (representStudents.getFive())
                fiveStudentList.add(representStudents.getStudent());
        }
        //загружаем шаблон документа
        //-----------------------------------------------------------------------------------------------------------------------------
        //PrintTemplate templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(PrintTemplate.class, "list.rep.diplom.and.exclude");
        TemplateDocument templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(TemplateDocument.class, "list.rep.diplom.and.exclude");
        //-----------------------------------------------------------------------------------------------------------------------------
        RtfDocument template = new RtfReader().read(templateDocument.getContent());
//		templateClone = template.getClone();
        //--------------------------------------------------------------------------------------------------------------------
        //templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(PrintTemplate.class, "list.rep.diplom.and.exclude.parag");
        templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(TemplateDocument.class, "list.rep.diplom.and.exclude.parag");
        //--------------------------------------------------------------------------------------------------------------------
        RtfDocument paragTamplate = new RtfReader().read(templateDocument.getContent());

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        ListRepresentDiplomAndExclude listRepresent = (ListRepresentDiplomAndExclude) listOfRepresents.get(0);
        i = 0;

        RtfDocument res = RtfBean.getElementFactory().createRtfDocument();
        res.setHeader(paragTamplate.getHeader());
        res.setSettings(paragTamplate.getSettings());
        res.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.TAB));

        IRtfElement paragTemp = UniRtfUtil.findElement(template.getElementList(), "parag");

        //группируем студентов по институту
        HashMap<OrgUnit, List<RelListRepresentStudents>> orgUnitMap = new HashMap<>();
        List<RelListRepresentStudents> orgUnitList;
        for (RelListRepresentStudents represent : listRepresentStudents)
        {
            orgUnitList = orgUnitMap.get(represent.getStudent().getEducationOrgUnit().getFormativeOrgUnit());
            if (orgUnitList == null)
            {
                orgUnitList = new ArrayList<>();
                orgUnitList.add(represent);
            } else
                orgUnitList.add(represent);
            orgUnitMap.put(represent.getStudent().getEducationOrgUnit().getFormativeOrgUnit(), orgUnitList);
        }

        List<OrgUnit> sortOrgUnitList = new ArrayList<>();
        for (OrgUnit orgUnit : orgUnitMap.keySet())
        {
            sortOrgUnitList.add(orgUnit);
        }
        //сортируем интституты в алфавитном порядке
        Collections.sort(sortOrgUnitList,
                new EntityComparator<>(new EntityOrder(OrgUnit.title().s()))
        );

        for (OrgUnit orgUnit : sortOrgUnitList)
        {
            //выводим институт
            insertRow(String.valueOf(++i) + ". По " + orgUnit.getDativeCaseTitle(), res);
            //группируем направления подготовки внутри института
            HashMap<EducationLevelsHighSchool, List<RelListRepresentStudents>> educationLevelsMap = new HashMap<>();
            List<RelListRepresentStudents> educationLevelsList;
            for (RelListRepresentStudents represent : orgUnitMap.get(orgUnit))
            {
                educationLevelsList = educationLevelsMap.get(represent.getStudent().getEducationOrgUnit().getEducationLevelHighSchool());
                if (educationLevelsList == null)
                {
                    educationLevelsList = new ArrayList<>();
                    educationLevelsList.add(represent);
                } else
                    educationLevelsList.add(represent);
                educationLevelsMap.put(represent.getStudent().getEducationOrgUnit().getEducationLevelHighSchool(), educationLevelsList);
            }
            //сортируем по наименованию направления
            List<EducationLevelsHighSchool> sortEducationLevelsList = new ArrayList<>();
            for (EducationLevelsHighSchool educationLevels : educationLevelsMap.keySet())
            {
                sortEducationLevelsList.add(educationLevels);
            }
            Collections.sort(sortEducationLevelsList,
                    new EntityComparator<>(new EntityOrder(EducationLevelsHighSchool.fullTitle().s()))
            );

            for (EducationLevelsHighSchool educationLevels : getSortQualificationList(sortEducationLevelsList))
            {
                //группируем студентов по направлению подготовки, курсу, форме обучения, квалификации, дате отчисления, номеру протокола
                HashMap<String, List<RelListRepresentStudents>> studentMap = new HashMap<>();
                List<RelListRepresentStudents> studentList;
                for (RelListRepresentStudents represent : educationLevelsMap.get(educationLevels))
                {
                    studentList = studentMap.get(getKey(represent));
                    if (studentList == null)
                    {
                        studentList = new ArrayList<>();
                        studentList.add(represent);
                    } else
                        studentList.add(represent);
                    studentMap.put(getKey(represent), studentList);
                }

                RtfDocument par = printDoc(studentMap, paragTamplate);
                res.getElementList().addAll(par.getElementList());
            }
            j = 0;
        }

        if (size == 1)
        {
            RtfString str = new RtfString();
            str.append("Основание: ")
                    .append(listRepresent.getRepresentationBasement().getTitle())
                    .append(" (протокол от ")
            ;
            UtilPrintSupport.getDateFormatterWithMonthStringAndYear(listRepresent.getDateProtocol(), str);
            str.append(" №").append(IRtfData.SYMBOL_TILDE).append(listRepresent.getNumberProtocol() + ").");
            //если представление одно, выводим основание
            insertRow(str, res);

        }

        template.getElementList().addAll(template.getElementList().indexOf(paragTemp), res.getElementList());

        injectModifier
                .put("representTitle", listRepresent.getRepresentationType().getTitle());
        injectModifier.modify(template);

        SharedRtfUtil.removeParagraphsWithTagsRecursive(template, Collections.singletonList("parag"), false, false);

        return template;
    }

    public String getKey(RelListRepresentStudents represent)
    {
        // формируем ключ, для группировки студентов
        return new StringBuilder()
                .append(represent.getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle())
                .append(represent.getStudent().getCourse().getTitle())
                .append(represent.getStudent().getEducationOrgUnit().getDevelopForm().getGenCaseTitle())
                .append(represent.getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getQualificationTitle())
                .append(DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(((ListRepresentDiplomAndExclude) represent.getRepresentation()).getDateExclude()))
                .append(((ListRepresentDiplomAndExclude) represent.getRepresentation()).getNumberProtocol())
                .append(((ListRepresentDiplomAndExclude) represent.getRepresentation()).getDateProtocol().getTime())
                .toString();
    }

    public List<EducationLevelsHighSchool> getSortQualificationList(List<EducationLevelsHighSchool> list)
    {
        //сортируем направления подготовки
        List<EducationLevelsHighSchool> result = new ArrayList<>();
        for (EducationLevelsHighSchool educationLevels : list)
        {
            //сначала по бакалаврам
            if (educationLevels.getEducationLevel().getLevelType().isBachelor())
                result.add(educationLevels);
        }
        for (EducationLevelsHighSchool educationLevels : list)
        {
            //затем специалисты
            if (educationLevels.getEducationLevel().getLevelType().isSpecialization() || educationLevels.getEducationLevel().getLevelType().isSpecialty())
                result.add(educationLevels);
        }
        for (EducationLevelsHighSchool educationLevels : list)
        {
            //потом магистры
            if (educationLevels.getEducationLevel().getLevelType().isMaster())
                result.add(educationLevels);
        }
        return result;
    }

    public RtfDocument printDoc(HashMap<String, List<RelListRepresentStudents>> map, RtfDocument document)
    {
        RtfInjectModifier injectModifier = new RtfInjectModifier();
        RtfDocument result = RtfBean.getElementFactory().createRtfDocument();
        result.setHeader(document.getHeader());
        result.setSettings(document.getSettings());
        for (String key : map.keySet())
        {
            //загружаем параграф для шаблона и заполняем
            RtfDocument par = document.getClone();
            List<RelListRepresentStudents> studentList = map.get(key);
            String qualification = "", prof = "", direction = "";
            if (studentList.get(0).getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isBachelor())
            {
                qualification = "степень";
                prof = " профиль ";
                direction = " направление подготовки ";
            } else if (studentList.get(0).getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isMaster())
            {
                qualification = "степень";
                prof = " магистерская программа ";
                direction = " направление подготовки ";
            } else
            {
                qualification = "квалификацию";
                prof = " специализация ";
                direction = " специальность ";
            }
            String kurs = (studentList.get(0).getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isMaster()) ? "курса магистратуры" : "курса";

            injectModifier
                    .put("i", String.valueOf(i))
                    .put("j", String.valueOf(++j))
                    .put("qualification", qualification)
                    .put("course", studentList.get(0).getStudent().getCourse().getTitle())
                    .put("kurs", kurs)
                    .put("diplomaQualification", StringUtils.uncapitalize(studentList.get(0).getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getQualificationTitleNullSafe()))
                            //.put("highSchool", (studentList.get(0).getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isSpecialty()) ? studentList.get(0).getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle()  : direction+studentList.get(0).getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getParentLevel().getDisplayableTitle()+","+prof+"«"+ studentList.get(0).getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getTitle()+"»")
                    .put("highSchool", UtilPrintSupport.getHighLevelSchoolTypeString(studentList.get(0).getStudent().getEducationOrgUnit().getEducationLevelHighSchool()))
                    //--------------------------------------------------------------------------------------------------------------------------------------------------------
                    .put("developPeriod", studentList.get(0).getStudent().getEducationOrgUnit().getDevelopPeriod().getTitle())
                    //--------------------------------------------------------------------------------------------------------------------------------------------------------
                    .put("dateExclude", UtilPrintSupport.getDateFormatterWithMonthString(((ListRepresentDiplomAndExclude) studentList.get(0).getRepresentation()).getDateExclude()))
                    .put("form", UtilPrintSupport.getDevelopFormGen(studentList.get(0).getStudent().getEducationOrgUnit().getDevelopForm()));

            List<RelListRepresentStudents> fiveList = new ArrayList<>();
            List<RelListRepresentStudents> studList = new ArrayList<>();
            //формируем списки студентов с отличием и без
            for (RelListRepresentStudents represent : studentList)
            {
                if (represent.getFive())
                    fiveList.add(represent);
                else
                    studList.add(represent);
                hashMap.put(represent.getStudent(), new OrderParagraphInfo(i, j));
            }
            if (!fiveList.isEmpty())
            {
                //выводим студентов с отличием
                printStudentlist(fiveList, "C отличием", par);
            }
            if (!studList.isEmpty())
            {
                //выводим студентов без отличия
                printStudentlist(studList, "Без отличия", par);
            }
            if (size > 1)
            {
                RtfString str = new RtfString();
                str.append("Основание: ")
                        .append(studentList.get(0).getRepresentation().getRepresentationBasement().getTitle())
                        .append(" (протокол от ")
                ;
                UtilPrintSupport.getDateFormatterWithMonthStringAndYear(((ListRepresentDiplomAndExclude) studentList.get(0).getRepresentation()).getDateProtocol(), str);
                str.append(" №").append(IRtfData.SYMBOL_TILDE).append(((ListRepresentDiplomAndExclude) studentList.get(0).getRepresentation()).getNumberProtocol() + ").");
                //если представлений несколько, то после каждого подпункта выводим основание
                insertRow(str, par);
            }
            injectModifier.modify(par);

            result.getElementList().addAll(par.getElementList());
        }

        return result;
    }

    public void insertRow(String row, RtfDocument document)
    {
        //добавление строки с текстом row в шаблон документа
        RtfText element = new RtfText();
        element.setString(row);
        document.addElement(element);
        document.addElement(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
    }

    protected void insertRow(RtfString text, RtfDocument document)
    {
        document.getElementList().addAll(text.toList());
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
    }

    public void printStudentlist(List<RelListRepresentStudents> list, String five, RtfDocument document)
    {
        //сортируем и выводим список студентов
        Collections.sort(list,
                new EntityComparator<>(new EntityOrder(RelListRepresentStudents.student().person().fullFio().s()))
        );
        insertRow(five, document);
        for (RelListRepresentStudents represent : list)
        {
            String str = represent.getStudent().getCompensationType().getCode().equals(UniDefines.COMPENSATION_TYPE_CONTRACT) ? " (по договору)" : "";
            insertRow(String.valueOf(list.indexOf(represent) + 1 + ") ") + represent.getStudent().getPerson().getFullFio() + str, document);
            //------------------------------------------------------------------------------------------------------------------------------
            hashMap.get(represent.getStudent()).setStudentNumber(list.indexOf(represent) + 1);
            //------------------------------------------------------------------------------------------------------------------------------
        }
        insertRow("", document);
    }

    @Override
    public Map<Student, OrderParagraphInfo> getParagInfomap()
    {
        return hashMap;
    }

}
