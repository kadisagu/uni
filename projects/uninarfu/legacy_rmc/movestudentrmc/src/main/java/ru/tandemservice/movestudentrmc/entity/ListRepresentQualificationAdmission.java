package ru.tandemservice.movestudentrmc.entity;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentQualificationThemes.logic.OrgUnitSelectedDSHandler;
import ru.tandemservice.movestudentrmc.entity.gen.ListRepresentQualificationAdmissionGen;

/**
 * Списочное представление о допуске к мероприятиям итоговой аттестации
 */
public class ListRepresentQualificationAdmission extends ListRepresentQualificationAdmissionGen implements IDiplomaRepresent {

    @EntityDSLSupport(parts = {P_ACCEPT_ORG_UNIT_TYPE})
    @Override
    public String getTypePrintTitle()
    {
        String title = StringUtils.substringAfter(getAcceptOrgUnitType(), OrgUnitSelectedDSHandler.SEPARATOR);
        if (title == null)
            return "";
        return title;
    }
}