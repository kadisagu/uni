package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListOrder.logic.TypeRepresentDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentAdmissionToPractice.ListRepresentAdmissionToPracticeManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBudgetTransfer.ListRepresentBudgetTransferManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentCourseTransfer.ListRepresentCourseTransferManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentDiplomAndExclude.ListRepresentDiplomAndExcludeManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentExclude.ListRepresentExcludeManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrant.ListRepresentGrantManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantCancel.ListRepresentGrantCancelManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantCancelAndDestination.ListRepresentGrantCancelAndDestinationManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantResume.ListRepresentGrantResumeManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantSuspend.ListRepresentGrantSuspendManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentPractice.ListRepresentPracticeManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentProfileTransfer.ListRepresentProfileTransferManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentQualificationAdmission.ListRepresentQualificationAdmissionManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentQualificationThemes.ListRepresentQualificationThemesManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentSocialGrant.ListRepresentSocialGrantManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentSupport.ListRepresentSupportManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentTransfer.ListRepresentTransferManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentTravel.ListRepresentTravelManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentWeekend.ListRepresentWeekendManager;
import ru.tandemservice.movestudentrmc.docord.IListObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentationTypeCodes;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.*;

@Configuration
public class ListRepresentBaseManager extends BusinessObjectManager {

    public static ListRepresentBaseManager instance()
    {
        return instance(ListRepresentBaseManager.class);
    }


    @Bean
    public IListRepresentBaseModifyDAO modifyDao()
    {
        return new ListRepresentBaseModifyDAO();
    }


    @Bean
    public ItemListExtPoint<? extends IListObjectByRepresentTypeManager> doObjectByRepresentTypeExtPoint()
    {
        // vch
        // добавляем бк для работы со списочными представлениями
        // а может можно как то без ввода руками (через мету)? <TODO>
        return itemList(IListObjectByRepresentTypeManager.class)
                .add(RepresentationTypeCodes.APPOINT_GRANT, ListRepresentGrantManager.instance())
                .add(RepresentationTypeCodes.DISTRIBUTION_ACCORDING_TO_PROFILES, ListRepresentProfileTransferManager.instance())
                .add(RepresentationTypeCodes.DISTRIBUTION_TO_BUDGET, ListRepresentBudgetTransferManager.instance())
                .add(RepresentationTypeCodes.SOCIAL_SUPPORT, ListRepresentSupportManager.instance())
                .add(RepresentationTypeCodes.COURSE_TRANSFER_LIST, ListRepresentCourseTransferManager.instance())
                .add(RepresentationTypeCodes.DIPLOM_AND_EXCLUDE_LIST, ListRepresentDiplomAndExcludeManager.instance())
                .add(RepresentationTypeCodes.GRANT_CANCEL, ListRepresentGrantCancelManager.instance())
                .add(RepresentationTypeCodes.GRANT_SUSPEND, ListRepresentGrantSuspendManager.instance())
                .add(RepresentationTypeCodes.GRANT_RESUME, ListRepresentGrantResumeManager.instance())
                .add(RepresentationTypeCodes.EXCLUDE_LIST, ListRepresentExcludeManager.instance())
                .add(RepresentationTypeCodes.TRANSFER_LIST, ListRepresentTransferManager.instance())
                .add(RepresentationTypeCodes.WEEKEND_LIST, ListRepresentWeekendManager.instance())
                .add(RepresentationTypeCodes.PRACTICE_LIST, ListRepresentPracticeManager.instance())
                .add(RepresentationTypeCodes.ADMISSION_TO_PRACTICE, ListRepresentAdmissionToPracticeManager.instance())
                .add(RepresentationTypeCodes.TRAVEL_LIST, ListRepresentTravelManager.instance())
                .add(RepresentationTypeCodes.SOCIAL_GRANT_LIST, ListRepresentSocialGrantManager.instance())
                .add(RepresentationTypeCodes.GRANT_CANCEL_AND_DESTINATION_LIST, ListRepresentGrantCancelAndDestinationManager.instance())
                .add(RepresentationTypeCodes.QUALIFICATION_THEMES_LIST, ListRepresentQualificationThemesManager.instance())
                .add(RepresentationTypeCodes.QUALIFICATION_ADMISSION_LIST, ListRepresentQualificationAdmissionManager.instance())
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> listRepresentDSHandler()
    {
        return new ListRepresentDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> formativeOrgUnitDSHandler()
    {
        return new FormativeOrgUnitDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> grantViewDSHandler()
    {
        return new GrantViewDSHandler(getName(), "shortTitle");
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> studentComboDSHandler()
    {
        return new StudentComboDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> typeRepresentDSHandler()
    {
        return new TypeRepresentDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> stateViewDSHandler() {
        return new StateViewDSHandler(getName());
    }
}
