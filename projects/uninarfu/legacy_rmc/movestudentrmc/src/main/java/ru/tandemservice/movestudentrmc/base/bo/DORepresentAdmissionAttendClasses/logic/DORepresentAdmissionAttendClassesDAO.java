package ru.tandemservice.movestudentrmc.base.bo.DORepresentAdmissionAttendClasses.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfString;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentDAO;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.RepresentAdmissionAttendClasses;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class DORepresentAdmissionAttendClassesDAO extends AbstractDORepresentDAO
{

    @Override
    public Representation createRepresentObject(RepresentationType type, List<Student> student) {

        RepresentAdmissionAttendClasses represent = new RepresentAdmissionAttendClasses();
        represent.setType(type);
        return represent;
    }

    @Override
    public void buildBodyExtract(Representation representationBase, String itemFac, RtfDocument docCommon) {
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        UtilPrintSupport.setParNum(docCommon, itemFac);

        RtfInjectModifier modifier = new RtfInjectModifier();

        UtilPrintSupport.injectModifierExtract(modifier, docRepresent);

        injectModifier(modifier, docRepresent);

        modifier.modify(docCommon);
    }

    @Override
    public void buildBodyRepresent(Representation representationBase, RtfDocument document, AtomicInteger parNumber, AtomicInteger extractNumber) {
        super.buildBodyRepresent(representationBase, document, parNumber, extractNumber);
        //формируем печатную форму представления
        RepresentAdmissionAttendClasses represent = (RepresentAdmissionAttendClasses) representationBase;

        DocRepresentStudentBase docRepresent = getStudentList(represent).get(0);

        Student student = docRepresent.getStudent();

        RtfInjectModifier modifier = new RtfInjectModifier();
        //заполняем общими данными представления
        UtilPrintSupport.injectModifierRepresent(modifier, docRepresent, student);

        injectModifier(modifier, represent);

        if (StringUtils.isEmpty(modifier.getStringValue("basics")))
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Arrays.asList("basics"), false, false);

        modifier.modify(document);
    }

    protected void injectModifier(RtfInjectModifier modifier, RepresentAdmissionAttendClasses represent) {
        //заполняем данными данного представления
        modifier.put("new_course", represent.getCourse().getTitle());
        modifier.put("new_sem", represent.getTerm().getTerm().getTitle());
        modifier.put("date_pay", UtilPrintSupport.getDateFormatterWithMonthStringAndYear(represent.getExamsDate()));
    }

    @Override
    public void buildBodyDeclaration(Representation representationBase,
                                     RtfDocument document)
    {
        //формируем печатную форму заявления
        RepresentAdmissionAttendClasses represent = (RepresentAdmissionAttendClasses) representationBase;

        DocRepresentStudentBase docRepresent = getStudentList(represent).get(0);

        RtfInjectModifier im = new RtfInjectModifier();

        RtfTableModifier tm = new RtfTableModifier();
        UtilPrintSupport.injectModifierDeclaration(im, tm, docRepresent);
        injectModifier(im, represent);
        //если студент обучается на договорной основе, то выводим соотв. сообщение
        if (docRepresent.getStudent().getCompensationType().getCode().equals(UniDefines.COMPENSATION_TYPE_CONTRACT)) {
            RtfString str = new RtfString().append("Обязаюсь оплатить дальнейшее обучение в установленный срок до ");
            UtilPrintSupport.getDateFormatterWithMonthString(represent.getExamsDate(), str);
            str.append(".");
            im.put("label", str);
        }
        else
            //иначе удаляем метку
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Arrays.asList("label"), false, false);

        this.injectModifier(im, docRepresent);

        im.modify(document);
        tm.modify(document);
    }

}
