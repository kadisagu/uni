package ru.tandemservice.movestudentrmc.component.catalog.osspGrants.OsspGrantsPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;
import ru.tandemservice.movestudentrmc.entity.catalog.OsspGrants;

public interface IDAO extends IDefaultCatalogPubDAO<OsspGrants, Model> {
    void updatePrintable(Model model, Long id);

    void updateReqClarification(Model model, Long id);

    void updateReqPeriod(Model model, Long id);

    void updateReqSize(Model model, Long id);
}
