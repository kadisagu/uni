package ru.tandemservice.movestudentrmc.base.bo.ListRepresentQualificationThemes.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.util.FilterUtils;

public class OrgUnitSelectedDSHandler extends DefaultComboDataSourceHandler {
    public final static long CAF_ID = 0L;
    public final static long PCK_ID = 1L;
    public final static String SEPARATOR = ".";
    //--------------------------------------------------------------------------
    //private final static String PCK_PRINT_TITLE = "предметно-цикловой комиссии";
    private final static String PCK_PRINT_TITLE = "предметной (цикловой) комиссии";
    //--------------------------------------------------------------------------
    private final static String CAF_PRINT_TITLE = "кафедры";

    public OrgUnitSelectedDSHandler(String ownerId) {
        super(ownerId, OrgUnit.class);
    }


    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep) {
        String filter = ep.input.getComboFilterByValue();

        ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property(OrgUnit.orgUnitType().code().fromAlias("e")), DQLExpressions.value("cathedra")));
        FilterUtils.applySimpleLikeFilter(ep.dqlBuilder, "e", OrgUnit.title(), filter);
    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> dsInputDSOutputExecutionParameters) {
        super.prepareOrders(dsInputDSOutputExecutionParameters);
    }


    public static String getBasicTitlePostfix(String title, OrgUnit orgUnit)
    {
        String code = StringUtils.substringBefore(title, SEPARATOR);
        Long codeLong = null;
        try {
            codeLong = Long.valueOf(code);
        }
        catch (Exception e) {
            return "";
        }
        if (codeLong == CAF_ID) {
            if (orgUnit != null)
                return orgUnit.getGenitiveCaseTitle();
            return CAF_PRINT_TITLE;
        }
        if (codeLong == PCK_ID) {
            return PCK_PRINT_TITLE;
        }
        return "";

    }
}
