package ru.tandemservice.movestudentrmc.component.person.IdentityCardEdit;

import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentIC;

import java.util.List;

public class DAO extends org.tandemframework.shared.person.base.bo.Person.ui.IdentityCardEditInline.DAO {

    @Override
    public void update(org.tandemframework.shared.person.base.bo.Person.ui.IdentityCardEditInline.Model model) {
        Model myModel = (Model) model;
        if (!myModel.getRepresent()) {
            super.update(model);
        }
        else {
            ErrorCollector errors = ContextLocal.getErrorCollector();
            PersonManager.instance().dao().checkIdentityCard(model.getIdentityCard(), errors);
            if (errors.hasErrors()) {
                return;
            }
            else {
                DocRepresentStudentIC representIC = new DocRepresentStudentIC();

                Person person = getPerson(model);
                IdentityCard identityCard = model.getIdentityCard();
                getSession().save(identityCard);

                identityCard.setPerson(person);
                update(((org.tandemframework.core.entity.IEntity) (person.getIdentityCard())));


                DocRepresentStudentIC representIdentityCard = myModel.getRepresentIC();

                representIdentityCard.setStudentIC(identityCard);

                List<DocRepresentStudentIC> resultList = myModel.getList();

                resultList.add(representIdentityCard);
                representIdentityCard.setId(new Long("-1"));
                //myModel.setList(resultList);
                //getSession().save(representIdentityCard);

                //update(((org.tandemframework.core.entity.IEntity) (representIdentityCard)));


                //person.setIdentityCard(identityCard);
                //update(((org.tandemframework.core.entity.IEntity) (person)));
                //identityCard.setPerson(person);
                //update(((org.tandemframework.core.entity.IEntity) (person.getIdentityCard())));
                return;
            }
        }
    }
}
