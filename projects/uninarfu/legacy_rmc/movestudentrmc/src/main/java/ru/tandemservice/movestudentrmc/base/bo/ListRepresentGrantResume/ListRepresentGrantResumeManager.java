package ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantResume;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantResume.logic.ListRepresentGrantResumeManagerModifyDAO;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantResume.ui.Edit.ListRepresentGrantResumeEdit;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantResume.ui.View.ListRepresentGrantResumeView;
import ru.tandemservice.movestudentrmc.docord.IListObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IListObjectModifyDAO;

@Configuration
public class ListRepresentGrantResumeManager extends BusinessObjectManager implements IListObjectByRepresentTypeManager {

    public static ListRepresentGrantResumeManager instance() {
        return instance(ListRepresentGrantResumeManager.class);
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return ListRepresentGrantResumeEdit.class;
    }

    @Bean
    @Override
    public IListObjectModifyDAO getListObjectModifyDAO() {
        return new ListRepresentGrantResumeManagerModifyDAO();
    }

    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return ListRepresentGrantResumeView.class;
    }
}
