package ru.tandemservice.movestudentrmc.component.settings.grantrelations.RelGrantViewOrgUnitAdd;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudentrmc.entity.RelGrantViewOrgUnit;
import ru.tandemservice.uni.component.settings.abstractRelation.AbstractRelationAdd.AbstractRelationAddDAO;

import java.util.List;

public class DAO extends AbstractRelationAddDAO<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        // TODO Auto-generated method stub
        super.prepare(model);

        String hql = (new StringBuilder()).append("from ").append(getRelationSecondEntityClass().getName()).append(" b where b not in (select ").append("second").append(" from ").append(getRelationEntityClass().getName()).append(") order by b.title").toString();
        List list = getSession().createQuery(hql).list();

        model.setFormativeOrgUnitListModel(new BaseSingleSelectModel() {

                                               public ListResult findValues(String filter)
                                               {
                                                   MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "o");
                                                   builder.add(MQExpression.like("o", "title", CoreStringUtils.escapeLike(filter)));
                                                   builder.addOrder("o", "title");
                                                   return new ListResult(builder.getResultList(getSession()));
                                               }

                                               public Object getValue(Object primaryKey)
                                               {
                                                   org.tandemframework.core.entity.IEntity entity = get(OrgUnit.class, (Long) primaryKey);
                                                   if (findValues("").getObjects().contains(entity))
                                                       return entity;
                                                   else
                                                       return null;
                                               }

                                               public String getLabelFor(Object value, int columnIndex)
                                               {
                                                   return ((OrgUnit) value).getFullTitle();
                                               }

                                           }
        );
        model.setSecondList(list);
    }

    @Override
    protected Class<RelGrantViewOrgUnit> getRelationEntityClass() {
        // TODO Auto-generated method stub
        return RelGrantViewOrgUnit.class;
    }

    @Override
    protected Class<OrgUnit> getRelationSecondEntityClass() {
        // TODO Auto-generated method stub
        return OrgUnit.class;

    }

    @Override
    protected String getRelationSecondEntityClassSortProperty() {
        // TODO Auto-generated method stub
        return "title";
    }


}
