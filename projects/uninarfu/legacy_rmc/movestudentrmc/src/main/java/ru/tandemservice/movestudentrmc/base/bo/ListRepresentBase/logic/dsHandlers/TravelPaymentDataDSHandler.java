package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers;

import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import ru.tandemservice.movestudentrmc.entity.catalog.TravelPaymentData;

public class TravelPaymentDataDSHandler extends DefaultComboDataSourceHandler {

    public TravelPaymentDataDSHandler(String ownerId) {
        super(ownerId, TravelPaymentData.class);
    }

}