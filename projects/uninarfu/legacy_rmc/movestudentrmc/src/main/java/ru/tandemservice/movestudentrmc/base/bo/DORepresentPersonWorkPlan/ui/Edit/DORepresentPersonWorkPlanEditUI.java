package ru.tandemservice.movestudentrmc.base.bo.DORepresentPersonWorkPlan.ui.Edit;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentEditUI;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.NarfuDocument;
import ru.tandemservice.movestudentrmc.entity.RepresentPersonWorkPlan;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.util.ListenerWrapper;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DORepresentPersonWorkPlanEditUI extends AbstractDORepresentEditUI implements ListenerWrapper.IListener<NarfuDocument>
{

    @Override
    protected void FetchStudents(Representation doc) {

        _studentDocList.clear();
        _studentList.clear();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocRepresentStudentBase.class, "stud").addColumn("stud");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(DocRepresentStudentBase.representation().fromAlias("stud")),
                DQLExpressions.value(doc.getId())));

        List<DocRepresentStudentBase> studentList = builder.createStatement(getSupport().getSession()).list();

        for (DocRepresentStudentBase student : studentList) {

            _studentDocList.add(student);
            _studentList.add(student.getStudent());
        }

        _student = _studentList.get(0);
    }


    public void onClickBeginDate() {
        RepresentPersonWorkPlan representObj = (RepresentPersonWorkPlan) getRepresentObj();

        Date currentDate = representObj.getBeginPersonWorkPlanDate();
        int currentDateYear = CoreDateUtils.getYear(currentDate);

        Calendar beginPeriod1 = Calendar.getInstance();
        beginPeriod1.setTime(currentDate);
        Calendar endPeriod1 = Calendar.getInstance();

        beginPeriod1.set(Calendar.MONTH, Calendar.SEPTEMBER);
        beginPeriod1.set(Calendar.DAY_OF_MONTH, 1);

        endPeriod1.set(Calendar.MONTH, Calendar.JANUARY);
        endPeriod1.set(Calendar.YEAR, currentDateYear + 1);
        endPeriod1.set(Calendar.DAY_OF_MONTH, 30);

        /**
         * Если Дата начала попадает в период [01.09 – 30.01],
         * то для Дата окончания должно автоматически подставиться значение 31.01 соответствующего года
         */
        if (CommonBaseDateUtil.isBetween(currentDate, beginPeriod1.getTime(), endPeriod1.getTime())) {
            endPeriod1.set(currentDateYear + 1, Calendar.JANUARY, 31);
            representObj.setEndPersonWorkPlanDate(endPeriod1.getTime());
        }
        else {

            /**
             * если Дата начала попадает в период [01.02 – 30.08],
             * то для Дата окончания должно отображаться значение 31.08 соответствующего года
             */
            endPeriod1.set(currentDateYear, Calendar.AUGUST, 31);
            representObj.setEndPersonWorkPlanDate(endPeriod1.getTime());

        }
    }


    @Override
    public void onClickSave() throws IOException {
        RepresentPersonWorkPlan representObj = (RepresentPersonWorkPlan) getRepresentObj();

        if (CommonBaseDateUtil.isAfter(representObj.getBeginPersonWorkPlanDate(), representObj.getEndPersonWorkPlanDate()))
            throw new ApplicationException("Дата начала должна быть раньше даты окончания.");

        super.onClickSave();
    }

}
