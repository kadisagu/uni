package ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantCancel;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantCancel.logic.ListRepresentGrantCancelManagerModifyDAO;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantCancel.ui.Edit.ListRepresentGrantCancelEdit;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantCancel.ui.View.ListRepresentGrantCancelView;
import ru.tandemservice.movestudentrmc.docord.IListObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IListObjectModifyDAO;

@Configuration
public class ListRepresentGrantCancelManager extends BusinessObjectManager implements IListObjectByRepresentTypeManager {

    public static ListRepresentGrantCancelManager instance() {
        return instance(ListRepresentGrantCancelManager.class);
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return ListRepresentGrantCancelEdit.class;
    }

    @Bean
    @Override
    public IListObjectModifyDAO getListObjectModifyDAO() {
        return new ListRepresentGrantCancelManagerModifyDAO();
    }

    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return ListRepresentGrantCancelView.class;
    }
}
