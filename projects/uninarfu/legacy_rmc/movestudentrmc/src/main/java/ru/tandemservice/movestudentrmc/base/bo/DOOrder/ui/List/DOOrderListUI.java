package ru.tandemservice.movestudentrmc.base.bo.DOOrder.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.logic.OrderDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.logic.OrderPrint;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.ui.Edit.DOOrderEdit;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.ui.Edit.DOOrderEditUI;
import ru.tandemservice.movestudentrmc.entity.DocOrdRepresent;
import ru.tandemservice.movestudentrmc.entity.DocumentOrder;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.entity.catalog.MovestudentExtractStates;
import ru.tandemservice.uni.util.ReportRenderer;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.List;
import java.util.Map;

public class DOOrderListUI extends UIPresenter
{

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        Map<String, Object> settingMap = _uiSettings.getAsMap(
                OrderDSHandler.NUMBER_ORDER_FILTER,
                OrderDSHandler.DATE_COMMIT_FROM_FILTER,
                OrderDSHandler.DATE_FORMATIVE_FROM_FILTER,
                OrderDSHandler.DATE_COMMIT_TO_FILTER,
                OrderDSHandler.DATE_FORMATIVE_TO_FILTER,
                OrderDSHandler.DATE_START_FROM_FILTER,
                OrderDSHandler.DATE_START_TO_FILTER,
                "typeRepresentFilter",
                "reasonRepresentFilter",
                "formativeOrgUnitFilter",
                "courseFilter",
                "filterStudent",
                "authorFilter"
        );

        dataSource.putAll(settingMap);
    }

    @Override
    public void saveSettings()
    {
        UniBaseUtils.validateDatesPeriod(getSettings(), OrderDSHandler.DATE_FORMATIVE_FROM_FILTER, OrderDSHandler.DATE_FORMATIVE_TO_FILTER);
        UniBaseUtils.validateDatesPeriod(getSettings(), OrderDSHandler.DATE_COMMIT_FROM_FILTER, OrderDSHandler.DATE_COMMIT_TO_FILTER);
        UniBaseUtils.validateDatesPeriod(getSettings(), OrderDSHandler.DATE_START_FROM_FILTER, OrderDSHandler.DATE_START_TO_FILTER);

        super.saveSettings();
    }

    public void onClickAdd()
    {
        _uiActivation.asRegion(DOOrderEdit.class)
                .activate();
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asCurrent(DOOrderEdit.class)
                .parameter(DOOrderEditUI.ORDER_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onClickPrintFromList()
    {
        try
        {
            IDocumentRenderer doc = documentRenderer();
            if (doc != null)
                BusinessComponentUtils.downloadDocument(doc, true);
        } catch (Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public void onDeleteEntityFromList()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(DocOrdRepresent.class, "r").column("r");
        builder.where(DQLExpressions.eq(DQLExpressions.property(DocOrdRepresent.order().id().fromAlias("r")), DQLExpressions.value(getListenerParameterAsLong())));
        List<DocOrdRepresent> representSelectedOldList = builder.createStatement(getSupport().getSession()).list();

        for (DocOrdRepresent representSelectedOld : representSelectedOldList)
        {

            Representation represent = representSelectedOld.getRepresentation();
            represent.setState(DataAccessServices.dao().get(MovestudentExtractStates.class, MovestudentExtractStates.code().s(), "3"));
            DataAccessServices.dao().save(represent);
        }
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public Boolean getIsFormative()
    {
        BaseSearchListDataSource representDS = getConfig().getDataSource(DOOrderList.ORDER_DS);

        IEntity e = representDS.getCurrent();
        DocumentOrder order = ((DocumentOrder) e);
        return !order.getState().getCode().equals("1");
    }

    private IDocumentRenderer documentRenderer()
    {
        ErrorCollector error = getConfig().getUserContext().getErrorCollector();

        DocumentOrder order = DataAccessServices.dao().get(getListenerParameterAsLong());

        if (order.getDocument() == null)
        {
            final RtfDocument docMain = OrderPrint.createPrintDocOrder(order, error, false);
            if (docMain != null)
            {
                return new ReportRenderer("Order(draft).rtf", docMain, false);
            } else
            {
                return null;
            }
        } else
        {
            return new ReportRenderer("Order.rtf", order.getDocument(), false);
        }
    }
}
