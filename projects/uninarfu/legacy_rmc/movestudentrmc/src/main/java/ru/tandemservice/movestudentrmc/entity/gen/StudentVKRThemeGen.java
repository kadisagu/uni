package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.StudentVKRTheme;
import ru.tandemservice.uni.entity.employee.Student;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Увеличенное поле темы ВКР студента 
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentVKRThemeGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.StudentVKRTheme";
    public static final String ENTITY_NAME = "studentVKRTheme";
    public static final int VERSION_HASH = -1150054117;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT = "student";
    public static final String P_THEME = "theme";

    private Student _student;     // Студент
    private String _theme;     // Тема ВКР

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Студент. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null и должно быть уникальным.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Тема ВКР.
     */
    @Length(max=1000)
    public String getTheme()
    {
        return _theme;
    }

    /**
     * @param theme Тема ВКР.
     */
    public void setTheme(String theme)
    {
        dirty(_theme, theme);
        _theme = theme;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentVKRThemeGen)
        {
            setStudent(((StudentVKRTheme)another).getStudent());
            setTheme(((StudentVKRTheme)another).getTheme());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentVKRThemeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentVKRTheme.class;
        }

        public T newInstance()
        {
            return (T) new StudentVKRTheme();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "student":
                    return obj.getStudent();
                case "theme":
                    return obj.getTheme();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "theme":
                    obj.setTheme((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "student":
                        return true;
                case "theme":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "student":
                    return true;
                case "theme":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "student":
                    return Student.class;
                case "theme":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentVKRTheme> _dslPath = new Path<StudentVKRTheme>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentVKRTheme");
    }
            

    /**
     * @return Студент. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.StudentVKRTheme#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Тема ВКР.
     * @see ru.tandemservice.movestudentrmc.entity.StudentVKRTheme#getTheme()
     */
    public static PropertyPath<String> theme()
    {
        return _dslPath.theme();
    }

    public static class Path<E extends StudentVKRTheme> extends EntityPath<E>
    {
        private Student.Path<Student> _student;
        private PropertyPath<String> _theme;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Студент. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.StudentVKRTheme#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Тема ВКР.
     * @see ru.tandemservice.movestudentrmc.entity.StudentVKRTheme#getTheme()
     */
        public PropertyPath<String> theme()
        {
            if(_theme == null )
                _theme = new PropertyPath<String>(StudentVKRThemeGen.P_THEME, this);
            return _theme;
        }

        public Class getEntityClass()
        {
            return StudentVKRTheme.class;
        }

        public String getEntityName()
        {
            return "studentVKRTheme";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
