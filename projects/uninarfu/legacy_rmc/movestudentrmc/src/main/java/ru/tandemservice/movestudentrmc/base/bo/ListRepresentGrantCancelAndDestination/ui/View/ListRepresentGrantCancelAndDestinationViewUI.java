package ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantCancelAndDestination.ui.View;

import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.support.AbstractListRepresentViewUI;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantCancelAndDestination.logic.ListPrintDoc;
import ru.tandemservice.movestudentrmc.entity.ListRepresentGrantCancelAndDestination;

import java.util.Collections;

public class ListRepresentGrantCancelAndDestinationViewUI extends AbstractListRepresentViewUI<ListRepresentGrantCancelAndDestination>
{

    @Override
    public ListRepresentGrantCancelAndDestination getListRepresentObject()
    {
        return new ListRepresentGrantCancelAndDestination();
    }

    @Override
    protected RtfDocument printRepresent()
    {
        return new ListPrintDoc().creationPrintDocOrder(Collections.singletonList(getListRepresent()));
    }

}
