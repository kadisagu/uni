package ru.tandemservice.movestudentrmc.base.bo.DORepresentExcludeOut.ui.View;

import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentViewUI;
import ru.tandemservice.movestudentrmc.entity.RepresentExcludeOut;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

public class DORepresentExcludeOutViewUI extends AbstractDORepresentViewUI
{

    public EducationOrgUnit getEducationOrgUnit() {
        RepresentExcludeOut rep = (RepresentExcludeOut) this._represent;
        if (rep.getEducationOrgUnit() != null)
            return rep.getEducationOrgUnit();
        else
            return rep.getGroup().getEducationOrgUnit();
    }
}
