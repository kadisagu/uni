package ru.tandemservice.movestudentrmc.base.bo.DOOrder.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudentrmc.entity.DocOrdRepresent;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RepresentDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput> {

    public static final String DEVELOP_FORM_FILTER = "developFormFilter";
    public static final String DECLARATION_FILTER = "declarationFilter";
    public static final String FORMATIVE_ORG_UNIT_FILTER = "formativeOrgUnitFilter";
    public static final String TYPE_REPRESENT_FILTER = "typeRepresentFilter";
    public static final String DATE_REPRESENT_FILTER = "dateRepresentFilter";
    public static final String STUDENT_FIO_FILTER = "studentFioFilter";

    public static final String DEVELOP_FORM_COLUMN = "developForm";
    public static final String FORMATIVE_ORG_UNIT_COLUMN = "formativeOrgUnit";
    public static final String TERRITORIAL_ORG_UNIT_COLUMN = "territorialOrgUnit";
    public static final String EDUCATION_LEVEL_HIGH_SCHOOL_COLUMN = "educationLevelHighSchool";
    public static final String EDUCATION_ORG_UNIT_COLUMN = "educationOrgUnit";
    public static final String TYPE_REPRESENT_COLUMN = "typeRepresent";
    public static final String COURSE_COLUMN = "course";
    public static final String GROUP_COLUMN = "group";
    public static final String BUDGET_COLUMN = "budget";
    public static final String DATE_REPRESENT_COLUMN = "dateRepresent";
    public static final String STUDENT_FIO_COLUMN = "studentFio";
    public static final String CHECKBOX_COLUMN = "checkbox";
    public static final String REPRESENT_SELECTED_LIST = "representSelectedList";
    public static final String ORDER_ID = "orderId";

    public RepresentDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        DevelopForm developFormFilter = (DevelopForm) context.get(DEVELOP_FORM_FILTER);
        Boolean declarationFilter = (Boolean) context.get(DECLARATION_FILTER);
        OrgUnit formativeOrgUnitFilter = (OrgUnit) context.get(FORMATIVE_ORG_UNIT_FILTER);
        RepresentationType typeRepresentFilter = (RepresentationType) context.get(TYPE_REPRESENT_FILTER);
        Date dateRepresentFilter = (Date) context.get(DATE_REPRESENT_FILTER);
        String studentFioFilter = (String) context.get(STUDENT_FIO_FILTER);
        Long orderId = (Long) context.get(ORDER_ID);

        DQLSelectBuilder subBuilder = new DQLSelectBuilder();
        if (orderId != null) {
            subBuilder.fromEntity(DocOrdRepresent.class, "o").addColumn("o.representation.id");
            subBuilder.where(DQLExpressions.eq(
                    DQLExpressions.property(DocOrdRepresent.order().id().fromAlias("o")), DQLExpressions.value(orderId)
            ));
        }

        DQLSelectBuilder builder = new DQLSelectBuilder();

        builder.fromEntity(Representation.class, "r").addColumn("r");

        builder.joinEntity("r", DQLJoinType.inner, DocRepresentStudentBase.class, "s",
                           DQLExpressions.eq(DQLExpressions.property(Representation.id().fromAlias("r")),
                                             DQLExpressions.property(DocRepresentStudentBase.representation().fromAlias("s"))));
        builder.addColumn("s");

        builder.joinEntity("r", DQLJoinType.inner, RepresentationType.class, "t",
                           DQLExpressions.eq(DQLExpressions.property(Representation.type().id().fromAlias("r")),
                                             DQLExpressions.property(RepresentationType.id().fromAlias("t"))));

        builder.addColumn("t");
        
        /*        
        DQLSelectBuilder builder = new DQLSelectBuilder();

        builder.fromEntity(Representation.class, "r").addColumn("r");
        
        builder.joinEntity("r", DQLJoinType.inner, Student.class,"s",
                DQLExpressions.eq(DQLExpressions.property(Student.id().fromAlias("s")),
                        DQLExpressions.property(Representation.representationStudent().id().fromAlias("r"))));
        builder.addColumn("s");
        */

        if (orderId != null)
            builder.where(
                    DQLExpressions.or(
                            DQLExpressions.eq(DQLExpressions.property(Representation.state().code().fromAlias("r")), DQLExpressions.value("3")),
                            DQLExpressions.and(
                                    DQLExpressions.eq(DQLExpressions.property(Representation.state().code().fromAlias("r")), DQLExpressions.value("5")),
                                    DQLExpressions.in(DQLExpressions.property(Representation.id().fromAlias("r")), subBuilder.getQuery())
                            )
                    )
            );
        else
            builder.where(DQLExpressions.eq(DQLExpressions.property(Representation.state().code().fromAlias("r")), DQLExpressions.value("3")));

        if (developFormFilter != null)
            builder.where(DQLExpressions.eq(DQLExpressions.property(DocRepresentStudentBase.student().educationOrgUnit().developForm().fromAlias("s")), DQLExpressions.value(developFormFilter)));

        if (formativeOrgUnitFilter != null)
            builder.where(DQLExpressions.eq(DQLExpressions.property(DocRepresentStudentBase.student().educationOrgUnit().formativeOrgUnit().fromAlias("s")), DQLExpressions.value(formativeOrgUnitFilter)));

        if (typeRepresentFilter != null)
            builder.where(DQLExpressions.eq(DQLExpressions.property(Representation.type().fromAlias("r")), DQLExpressions.value(typeRepresentFilter)));

        if (dateRepresentFilter != null)
            builder.where(DQLExpressions.eq(DQLExpressions.property(Representation.startDate().fromAlias("r")), DQLExpressions.valueDate(dateRepresentFilter)));

        if (studentFioFilter != null)
            builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(DocRepresentStudentBase.student().person().identityCard().fullFio().fromAlias("s"))), DQLExpressions.value(CoreStringUtils.escapeLike(studentFioFilter, true))));

        if (declarationFilter != null)
            builder.where(DQLExpressions.eq(DQLExpressions.property(RepresentationType.instruction().fromAlias("t")), DQLExpressions.value(declarationFilter)));

        List<Object[]> representList = builder.createStatement(context.getSession()).list();
        List<DataWrapper> resultList = new ArrayList<DataWrapper>();
        List<DataWrapper> selectedList = context.get(REPRESENT_SELECTED_LIST);
        for (Object[] represent : representList) {
            Representation representData = (Representation) represent[0];
            DocRepresentStudentBase studentData = (DocRepresentStudentBase) represent[1];

            DataWrapper w = new DataWrapper(representData.getId(), representData.getTitle(), representData);
            w.setProperty("student", studentData.getStudent());
            w.setProperty("represent", representData);
            if (selectedList != null)
                if (selectedList.contains(w))
                    continue;

            resultList.add(w);
        }

        return ListOutputBuilder.get(input, resultList).pageable(true).build();
    }
}
