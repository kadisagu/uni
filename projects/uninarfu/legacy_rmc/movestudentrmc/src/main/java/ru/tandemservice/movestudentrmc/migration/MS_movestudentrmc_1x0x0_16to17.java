package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.BooleanDBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;


public class MS_movestudentrmc_1x0x0_16to17 extends IndependentMigrationScript {

    private static final String COLUMN = "usegrant_p";

    @Override
    public void run(DBTool tool) throws Exception {
        IEntityMeta meta = EntityRuntime.getMeta(RepresentationType.class);

        if (!tool.tableExists(meta.getTableName()))
            return;

        BooleanDBColumn col = new BooleanDBColumn(COLUMN);
        col.setNullable(true);
        col.setDefaultExpression("0");

        tool.table(meta.getTableName()).createColumn(col);

        tool.executeUpdate("update " + meta.getTableName() + " set " + COLUMN + "=0");

        tool.table(meta.getTableName()).column(COLUMN).changeNullable(false);

    }
}