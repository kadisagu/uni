package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;

public class MS_movestudentrmc_1x0x0_33to34 extends IndependentMigrationScript {

    @Override
    public void run(DBTool tool) throws Exception {
        /*
		 * Переименование названия типа представления RM#2708
		 */
        if (tool.tableExists("REPRESENTATIONTYPE_T")) {
            tool.executeUpdate("update REPRESENTATIONTYPE_T set TITLE_P='О приостановлении стипендии/выплаты' where CODE_p='grantSuspend'");
        }

    }

}
