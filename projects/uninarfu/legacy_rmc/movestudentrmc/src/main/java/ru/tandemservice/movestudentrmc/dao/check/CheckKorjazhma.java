package ru.tandemservice.movestudentrmc.dao.check;

public class CheckKorjazhma extends CheckArkhangelsk {

    @Override
    protected String getCityCode() {
        return "2900000500000";
    }

    @Override
    protected String getCityStr() {
        return "Коряжма";
    }
}
