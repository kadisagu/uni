package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.constraints.UniqueDBConstraint;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashSet;
import java.util.Set;

public class MS_movestudentrmc_1x0x0_11to12 extends IndependentMigrationScript {
    @Override
    public void run(DBTool tool) throws Exception {
        IEntityMeta meta = EntityRuntime.getMeta(RepresentationReason.class);
        Set<Integer> used = new HashSet<Integer>();

        String sql = "select max(priority_p) from " + meta.getTableName();
        ResultSet rs = tool.getConnection().createStatement().executeQuery(sql);

        int max = 0;
        if (rs.next())
            max = rs.getInt(1);

        rs.close();

        rs = tool.getConnection().createStatement().executeQuery("select id, priority_p from " + meta.getTableName());
        PreparedStatement pst = tool.getConnection().prepareStatement("update " + meta.getTableName().toString() + " set priority_p=? where id=?");

        while (rs.next()) {
            long id = rs.getLong(1);
            Integer priority = Integer.valueOf(rs.getInt(2));

            if (used.contains(priority)) {
                pst.setInt(1, max + 1);
                pst.setLong(2, id);
                pst.executeUpdate();
                max++;
            }
            else
                used.add(priority);
        }
        rs.close();
        //tool.table(EntityRuntime.getMeta(RepresentationReason.class).getTableName()).createIndex(new DBIndex(null, "priority_p"));
        UniqueDBConstraint c = new UniqueDBConstraint("uk_priority", "priority_p");
        tool.table(meta.getTableName()).createConstraint(c);
    }
}