package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_movestudentrmc_1x0x0_30to31 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.10"),
                        new ScriptDependency("org.tandemframework.shared", "1.1.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.1.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность representEnrollmentTransfer

        // создано свойство orderDateOld
        {
            // создать колонку
            tool.createColumn("representenrollmenttransfer_t", new DBColumn("orderdateold_p", DBType.DATE));

        }

        // создано свойство orderNumberOld
        {
            // создать колонку
            tool.createColumn("representenrollmenttransfer_t", new DBColumn("ordernumberold_p", DBType.DATE));

        }

        // создано свойство orderDateEnrOld
        {
            // создать колонку
            tool.createColumn("representenrollmenttransfer_t", new DBColumn("orderdateenrold_p", DBType.DATE));

        }


    }
}