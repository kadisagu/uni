package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;

public class EduPlanDSHandler extends DefaultComboDataSourceHandler {
    public static final String GROUP = "group";

    public EduPlanDSHandler(String s) {
        super(s, EppEduPlan.class, EppEduPlan.number());
        setOrderByProperty(EppEduPlan.number().s());
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep) {
        super.prepareConditions(ep);

        Group group = ep.context.get(GROUP);
        if (group != null) {
            EducationOrgUnit eou = group.getEducationOrgUnit();
//TODO DEV-6870
//			ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property(EppEduPlan.developForm().fromAlias("e")), DQLExpressions.value(eou.getDevelopForm())));
            ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property(EppEduPlan.developCondition().fromAlias("e")), DQLExpressions.value(eou.getDevelopCondition())));
//TODO DEV-6870
//			ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property(EppEduPlan.developTech().fromAlias("e")), DQLExpressions.value(eou.getDevelopTech())));
//			ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property(EppEduPlan.developTech().fromAlias("e")), DQLExpressions.value(eou.getDevelopTech())));
//			ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property(EppEduPlan.educationLevelHighSchool().fromAlias("e")), DQLExpressions.value(eou.getEducationLevelHighSchool())));

            ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property(EppEduPlan.developGrid().developPeriod().fromAlias("e")), DQLExpressions.value(eou.getDevelopPeriod())));


        }
    }

}
