package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentationTypeCodes;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

//RT:2445
public class MS_movestudentrmc_1x0x0_25to26 extends IndependentMigrationScript {
    private IEntityMeta meta;
    private DBTool tool;
    private Long supportTypeId = null;

    @Override
    public void run(DBTool tool) throws Exception {
        this.meta = EntityRuntime.getMeta(ListRepresent.class);

        if (!tool.tableExists(meta.getTableName()))
            return;

        this.tool = tool;

        //load RepresentationTypeCodes.SOCIAL_SUPPORT id
        PreparedStatement pst = this.tool.getConnection().prepareStatement("select ID from REPRESENTATIONTYPE_T where CODE_P=?");
        pst.setString(1, RepresentationTypeCodes.SOCIAL_SUPPORT);
        ResultSet rs = pst.executeQuery();
        rs.next();
        this.supportTypeId = rs.getLong(1);
        rs.close();

        process();
    }

    /*
    select
      lr.ID
    from
      LISTREPRESENT_T lr
      inner join REPRESENTATIONTYPE_T rt on rt.ID=lr.REPRESENTATIONTYPE_ID
      inner join LISTREPRESENTSUPPORT_T ls on ls.id=lr.ID
    where
      rt.CODE_P<>'socialSupport'
     */
    private void process() throws Exception {
        String sql = ""
                + " select"
                + "   lr.ID"
                + " from"
                + "   LISTREPRESENT_T lr"
                + "   inner join REPRESENTATIONTYPE_T rt on rt.ID=lr.REPRESENTATIONTYPE_ID"
                + "   inner join LISTREPRESENTSUPPORT_T ls on ls.id=lr.ID"
                + " where"
                + "   rt.CODE_P<>?";

        PreparedStatement pst = this.tool.getConnection().prepareStatement(sql);
        pst.setString(1, "socialSupport");
        ResultSet rs = pst.executeQuery();

        pst = this.tool.getConnection().prepareStatement("update " + this.meta.getTableName() + " set REPRESENTATIONTYPE_ID=?, TITLE_P=? where ID=?");
        while (rs.next()) {
            Long id = rs.getLong(1);

            pst.setLong(1, this.supportTypeId);
            pst.setString(2, "О назначении социальной  поддержки в связи с тяжелым материальным положением");
            pst.setLong(3, id);
            pst.executeUpdate();
        }
        rs.close();
    }
}
