package ru.tandemservice.movestudentrmc.component.catalog.representationBasement.RepresentationBasementAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationBasement;

public interface IDAO extends IDefaultCatalogAddEditDAO<RepresentationBasement, Model> {

}
