package ru.tandemservice.movestudentrmc.component.catalog.printTemplate.PrintTemplateAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultPrintCatalogAddEdit.IDefaultPrintCatalogAddEditDAO;
import ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate;

/**
 * @author AutoGenerator
 *         Created on 07.12.2011
 */
public interface IDAO extends IDefaultPrintCatalogAddEditDAO<PrintTemplate, Model> {
}