package ru.tandemservice.movestudentrmc.dao.check;

public class CheckStatusDeferProtection extends AbstractCheckStudentStatus {

    @Override
    protected String getStatusCode() {
        return "4";
    }

}
