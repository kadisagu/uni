package ru.tandemservice.movestudentrmc.component.catalog.grant.GrantPub;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.movestudentrmc.entity.catalog.Grant;

public class Controller extends DefaultCatalogPubController<Grant, Model, IDAO> {

    @Override
    protected DynamicListDataSource<Grant> createListDataSource(IBusinessComponent context) {
        Model model = getModel(context);
        DynamicListDataSource<Grant> ds = new DynamicListDataSource<Grant>(context, this);
        ds.addColumn(new SimpleColumn("Наименование стипендии/выплаты", "title").setClickable(false).setOrderable(true), 2);
        ds.addColumn(new SimpleColumn("Краткое наименование группы стипендий", "shortTitle").setClickable(false).setOrderable(true), 3);
        ds.addColumn(new SimpleColumn("Родительный падеж", "genitive").setClickable(false).setOrderable(true), 4);
        ds.addColumn(new SimpleColumn("Дательный падеж", "dative").setClickable(false).setOrderable(true), 5);
        ds.addColumn(new SimpleColumn("Винительный падеж", "accusative").setClickable(false).setOrderable(true), 6);
        ds.addColumn(new SimpleColumn("Творительный падеж", "instrumental").setClickable(false).setOrderable(true), 7);
        ds.addColumn(new SimpleColumn("Предложный падеж", "prepositional").setClickable(false).setOrderable(true), 8);
//		ds.addColumn(new SimpleColumn("Hаименование группы стипендий", "group").setClickable(false).setOrderable(true), 9);
        ds.addColumn(new SimpleColumn("Номер", "priority").setClickable(false).setOrderable(true), 10);
        ds.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
        if (model.isUserCatalog())
            ds.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", ICatalogItem.CATALOG_ITEM_TITLE).setPermissionKey(model.getCatalogItemDelete()).setDisableHandler(model.getDisabledEntityHandler()));
        ds.setOrder("priority", OrderDirection.asc);
        return ds;
    }

}
