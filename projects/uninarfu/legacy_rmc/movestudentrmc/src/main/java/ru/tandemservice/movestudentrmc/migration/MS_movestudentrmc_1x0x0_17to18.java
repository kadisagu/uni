package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;

public class MS_movestudentrmc_1x0x0_17to18 extends IndependentMigrationScript {
    @Override
    public void run(DBTool tool) throws Exception {

		/* Миграция для удаления ненужных полей в представлении "О смене имени" 
         * RM #1828
		 */

        if (tool.tableExists("REPRESENTCHANGENAME_T")) {
            if (tool.columnExists("REPRESENTCHANGENAME_T", "FIRSTNAME_P")) {
                tool.dropColumn("REPRESENTCHANGENAME_T", "FIRSTNAME_P");
            }
            if (tool.columnExists("REPRESENTCHANGENAME_T", "LASTNAME_P")) {
                tool.dropColumn("REPRESENTCHANGENAME_T", "LASTNAME_P");
            }
            if (tool.columnExists("REPRESENTCHANGENAME_T", "MIDDLENAME_P")) {
                tool.dropColumn("REPRESENTCHANGENAME_T", "MIDDLENAME_P");
            }
            if (tool.columnExists("REPRESENTCHANGENAME_T", "FIRSTNAMEOLD_P")) {
                tool.dropColumn("REPRESENTCHANGENAME_T", "FIRSTNAMEOLD_P");
            }
            if (tool.columnExists("REPRESENTCHANGENAME_T", "LASTNAMEOLD_P")) {
                tool.dropColumn("REPRESENTCHANGENAME_T", "LASTNAMEOLD_P");
            }
            if (tool.columnExists("REPRESENTCHANGENAME_T", "MIDDLENAMEOLD_P")) {
                tool.dropColumn("REPRESENTCHANGENAME_T", "MIDDLENAMEOLD_P");
            }
        }
    }
}
