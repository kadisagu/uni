package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

public class EduPlanVersionDSHandler extends DefaultComboDataSourceHandler {
    public static final String EDU_PLAN = "eduPlan";

    public EduPlanVersionDSHandler(String s) {
        super(s, EppEduPlanVersion.class, EppEduPlanVersion.eduPlan().number());
        setOrderByProperty(EppEduPlanVersion.number().s());
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep) {
        super.prepareConditions(ep);

        EppEduPlan plan = ep.context.get(EDU_PLAN);
        ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property(EppEduPlanVersion.eduPlan().fromAlias("e")), DQLExpressions.value(plan)));
    }

}
