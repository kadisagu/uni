package ru.tandemservice.movestudentrmc.base.bo.DORepresentChangeName.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentDAO;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentIC;
import ru.tandemservice.movestudentrmc.entity.RepresentChangeName;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class DORepresentChangeNameDAO extends AbstractDORepresentDAO {

    @Override
    public Representation createRepresentObject(RepresentationType type, List<Student> studentList) {

        Student student = studentList.get(0);
        IdentityCard ic = student.getPerson().getIdentityCard();
        RepresentChangeName represent = new RepresentChangeName();
        represent.setType(type);

        //represent.setFirstName(ic.getFirstName());
        //represent.setLastName(ic.getLastName());
        //represent.setMiddleName(ic.getMiddleName());

        //represent.setFirstNameOld(ic.getFirstName());
        //represent.setLastNameOld(ic.getLastName());
        //represent.setMiddleNameOld(ic.getMiddleName());

        represent.setStudentICOld(ic);

        return represent;
    }

    @Override
    public boolean doCommit(Representation representationBase, ErrorCollector error) {
        if (!super.doCommit(representationBase, error))
            return false;

        RepresentChangeName represent = (RepresentChangeName) representationBase;

        DQLSelectBuilder icBuilder = new DQLSelectBuilder().fromEntity(DocRepresentStudentIC.class, "ic").column("ic");
        icBuilder.where(DQLExpressions.eq(
                DQLExpressions.property(DocRepresentStudentIC.representation().fromAlias("ic")),
                DQLExpressions.value(represent.getId())));

        List<DocRepresentStudentIC> icList = DataAccessServices.dao().getList(icBuilder);

        if (icList.size() > 0) {
            IdentityCard icNew = (IdentityCard) icList.get(0).getStudentIC();

            icNew.getPerson().setIdentityCard(icNew);
            baseUpdate(icNew.getPerson());
        }

        /*
        person.setIdentityCard(icNew);
        baseUpdate(person);        
        icNew.setPerson(person);
        baseUpdate(icNew);
        */
        return true;
    }

    @Override
    public boolean doRollback(Representation representationBase, ErrorCollector error) {
        if (!super.doRollback(representationBase, error))
            return false;

        RepresentChangeName represent = (RepresentChangeName) representationBase;

        IdentityCard icOld = represent.getStudentICOld();

        icOld.getPerson().setIdentityCard(icOld);
        baseUpdate(icOld.getPerson());

        /*
        person.setIdentityCard(icOld);
        baseUpdate(person);        
        icOld.setPerson(person);
        baseUpdate(icOld);
        */

        return true;
    }

    @Override
    public void buildBodyExtract(Representation representationBase, String itemFac, RtfDocument docCommon) {
        //TODO: Функционал по выпискамж
    }

    @Override
    public void buildBodyRepresent(Representation representationBase, RtfDocument document, AtomicInteger parNumber, AtomicInteger extractNumber) {
        super.buildBodyRepresent(representationBase, document, parNumber, extractNumber);
        RepresentChangeName represent = (RepresentChangeName) representationBase;
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        Student student = UtilPrintSupport.getStudent(represent);

        RtfInjectModifier modifier = new RtfInjectModifier();

        UtilPrintSupport.injectModifierRepresent(modifier, docRepresent, student);

        IdentityCard identityCard = student.getPerson().getIdentityCard();
        boolean isMaleSex = identityCard.getSex().isMale();

        DQLSelectBuilder icBuilder = new DQLSelectBuilder().fromEntity(DocRepresentStudentIC.class, "ic").column("ic");
        icBuilder.where(DQLExpressions.eq(
                DQLExpressions.property(DocRepresentStudentIC.representation().fromAlias("ic")),
                DQLExpressions.value(represent.getId())));

        List<DocRepresentStudentIC> icList = DataAccessServices.dao().getList(icBuilder);

        IdentityCard icNew = (IdentityCard) icList.get(0).getStudentIC();

        StringBuilder str = new StringBuilder(PersonManager.instance().declinationDao().getDeclinationLastName(icNew.getLastName() != null ? icNew.getLastName() : identityCard.getLastName(), GrammaCase.INSTRUMENTAL, isMaleSex));
        str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationFirstName(icNew.getFirstName() != null ? icNew.getFirstName() : identityCard.getFirstName(), GrammaCase.INSTRUMENTAL, isMaleSex));
        //------------------------------------------------------------------------------------------------------------------------------
        //if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
        //    str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationMiddleName(icNew.getMiddleName() != null ? icNew.getMiddleName() : identityCard.getMiddleName(), GrammaCase.INSTRUMENTAL, isMaleSex));
        if(icNew.getMiddleName() != null) {
        	str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationMiddleName(icNew.getMiddleName(), GrammaCase.INSTRUMENTAL, isMaleSex));
        }
        else if(identityCard.getMiddleName() != null) {
        	str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationMiddleName(identityCard.getMiddleName(), GrammaCase.INSTRUMENTAL, isMaleSex));
        }
        //------------------------------------------------------------------------------------------------------------------------------

        /*For old representations without old identity card*/
        IdentityCard olcIC = null;
        if (represent.getStudentICOld() != null) {
            olcIC = represent.getStudentICOld();
        }
        else {
            olcIC = identityCard;
        }
        /**/
        StringBuilder strOld = new StringBuilder(PersonManager.instance().declinationDao().getDeclinationLastName(olcIC.getLastName() != null ? olcIC.getLastName() : identityCard.getLastName(), GrammaCase.ACCUSATIVE, isMaleSex));
        strOld.append(" ").append(PersonManager.instance().declinationDao().getDeclinationFirstName(olcIC.getFirstName() != null ? olcIC.getFirstName() : identityCard.getFirstName(), GrammaCase.ACCUSATIVE, isMaleSex));
        if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
            strOld.append(" ").append(PersonManager.instance().declinationDao().getDeclinationMiddleName(olcIC.getMiddleName() != null ? olcIC.getMiddleName() : identityCard.getMiddleName(), GrammaCase.ACCUSATIVE, isMaleSex));

        modifier.put("studentSex", isMaleSex ? "студента" : "студентку");
        modifier.put("newName", str.toString());

        modifier.put("studentTitle", UtilPrintSupport.printSeparatedFIO(olcIC, GrammaCase.ACCUSATIVE, isMaleSex));

        if (StringUtils.isEmpty(modifier.getStringValue("basics")))
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Arrays.asList("basics"), false, false);

        modifier.modify(document);
    }

    @Override
    public void buildBodyDeclaration(Representation representationBase, RtfDocument document) {
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        RepresentChangeName represent = (RepresentChangeName) representationBase;

        Student student = UtilPrintSupport.getStudent(represent);

        IdentityCard identityCard = student.getPerson().getIdentityCard();

        RtfInjectModifier im = new RtfInjectModifier();
        RtfTableModifier tm = new RtfTableModifier();
        UtilPrintSupport.injectModifierDeclaration(im, tm, docRepresent);

        this.injectModifier(im, docRepresent);

        DQLSelectBuilder icBuilder = new DQLSelectBuilder().fromEntity(DocRepresentStudentIC.class, "ic").column("ic");
        icBuilder.where(DQLExpressions.eq(
                DQLExpressions.property(DocRepresentStudentIC.representation().fromAlias("ic")),
                DQLExpressions.value(represent.getId())));

        List<DocRepresentStudentIC> icList = DataAccessServices.dao().getList(icBuilder);

        IdentityCard icNew = (IdentityCard) icList.get(0).getStudentIC();

        StringBuilder str = new StringBuilder(icNew.getLastName() != null ? icNew.getLastName() : identityCard.getLastName());
        str.append(" ").append(icNew.getFirstName() != null ? icNew.getFirstName() : identityCard.getFirstName());
        if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
            str.append(" ").append(icNew.getMiddleName() != null ? icNew.getMiddleName() : identityCard.getMiddleName());

        im.put("newName", str.toString());

        im.modify(document);
        tm.modify(document);
    }
}
