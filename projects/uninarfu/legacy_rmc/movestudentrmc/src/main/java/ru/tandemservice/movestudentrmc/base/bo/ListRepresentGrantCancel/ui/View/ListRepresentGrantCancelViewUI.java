package ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantCancel.ui.View;

import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.support.AbstractListRepresentViewUI;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantCancel.logic.ListPrintDoc;
import ru.tandemservice.movestudentrmc.entity.ListRepresentGrantCancel;

import java.util.Arrays;

public class ListRepresentGrantCancelViewUI extends AbstractListRepresentViewUI<ListRepresentGrantCancel> {

    @Override
    public ListRepresentGrantCancel getListRepresentObject() {
        return new ListRepresentGrantCancel();
    }

    @Override
    protected RtfDocument printRepresent() {
        return new ListPrintDoc().creationPrintDocOrder(Arrays.asList(getListRepresent()));
    }
}
