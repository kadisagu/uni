package ru.tandemservice.movestudentrmc.base.bo.DORepresentGrantCancel;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentGrantCancel.logic.DORepresentGrantCancelDAO;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentGrantCancel.ui.Edit.DORepresentGrantCancelEdit;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentGrantCancel.ui.View.DORepresentGrantCancelView;
import ru.tandemservice.movestudentrmc.docord.IDOObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IDOObjectModifyDAO;

@Configuration
public class DORepresentGrantCancelManager extends BusinessObjectManager implements IDOObjectByRepresentTypeManager {

    public static DORepresentGrantCancelManager instance() {
        return instance(DORepresentGrantCancelManager.class);
    }


    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return DORepresentGrantCancelEdit.class;
    }

    @Bean
    public IDOObjectModifyDAO getDOObjectModifyDAO() {
        return new DORepresentGrantCancelDAO();
    }

    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return DORepresentGrantCancelView.class;
    }

}
