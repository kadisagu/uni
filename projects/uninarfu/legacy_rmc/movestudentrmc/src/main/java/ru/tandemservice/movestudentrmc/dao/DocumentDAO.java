package ru.tandemservice.movestudentrmc.dao;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentDocuments;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentIC;
import ru.tandemservice.movestudentrmc.entity.NarfuDocument;
import ru.tandemservice.movestudentrmc.entity.RelDocumentTypeKind;
import ru.tandemservice.movestudentrmc.entity.catalog.Document;
import ru.tandemservice.movestudentrmc.entity.catalog.DocumentKind;
import ru.tandemservice.movestudentrmc.entity.catalog.DocumentType;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.*;

public class DocumentDAO extends UniBaseDao implements IDocumentDAO {

    @Override
    public List<DocumentKind> getKinds(DocumentType documentType) {
        MQBuilder builder = new MQBuilder(RelDocumentTypeKind.ENTITY_CLASS, "r")
                .add(MQExpression.eq("r", RelDocumentTypeKind.type(), documentType));

        builder.getSelectAliasList().clear();
        builder.addSelect(RelDocumentTypeKind.kind().s());
        builder.setNeedDistinct(true);

        return builder.getResultList(getSession());
    }

    @Override
    public List<Document> getDocuments(DocumentType documentType, DocumentKind documentKind) {
        MQBuilder builder = new MQBuilder(RelDocumentTypeKind.ENTITY_CLASS, "r");

        if (documentType != null)
            builder.add(MQExpression.eq("r", RelDocumentTypeKind.type(), documentType));
        if (documentKind != null)
            builder.add(MQExpression.eq("r", RelDocumentTypeKind.kind(), documentKind));

        builder.getSelectAliasList().clear();
        builder.addSelect(RelDocumentTypeKind.document().s());
        builder.setNeedDistinct(true);

        return builder.getResultList(getSession());
    }

    @Override
    public void saveRelations(DocumentType documentType, DocumentKind documentKind, List<Document> documents) {
        if (documents == null)
            documents = Collections.emptyList();

        MQBuilder builder = new MQBuilder(RelDocumentTypeKind.ENTITY_CLASS, "r")
                .add(MQExpression.eq("r", RelDocumentTypeKind.type(), documentType))
                .add(MQExpression.eq("r", RelDocumentTypeKind.kind(), documentKind));
        List<RelDocumentTypeKind> oldRels = builder.getResultList(getSession());

        Map<Document, RelDocumentTypeKind> map = new HashMap<Document, RelDocumentTypeKind>();
        for (RelDocumentTypeKind rel : oldRels)
            map.put(rel.getDocument(), rel);

        //удаляем
        for (Document doc : map.keySet())
            if (!documents.contains(doc))
                this.delete(map.get(doc));

        //вставляем новые
        for (Document doc : documents)
            if (!map.containsKey(doc)) {
                RelDocumentTypeKind rel = new RelDocumentTypeKind();
                rel.setType(documentType);
                rel.setKind(documentKind);
                rel.setDocument(doc);

                this.saveOrUpdate(rel);
            }
    }

    @Override
    public List<DocumentType> getUsedTypes(Student student) {
        MQBuilder builder = new MQBuilder(NarfuDocument.ENTITY_CLASS, "d")
                .add(MQExpression.or(
                        MQExpression.isNull("d", NarfuDocument.tmp()),
                        MQExpression.eq("d", NarfuDocument.tmp(), Boolean.FALSE)
                ))
                .add(MQExpression.eq("d", NarfuDocument.student(), student))
                .addJoin("d", NarfuDocument.settings(), "sett")
                .addJoin("sett", RelDocumentTypeKind.type(), "t");

        builder.getSelectAliasList().clear();
        builder.addSelect("t");
        builder.setNeedDistinct(true);

        return builder.getResultList(getSession());
    }

    @Override
    public List<DocumentKind> getUsedKinds(Student student, DocumentType documentType) {
        MQBuilder builder = new MQBuilder(NarfuDocument.ENTITY_CLASS, "d")
                .add(MQExpression.or(
                        MQExpression.isNull("d", NarfuDocument.tmp()),
                        MQExpression.eq("d", NarfuDocument.tmp(), Boolean.FALSE)
                ))
                .add(MQExpression.eq("d", NarfuDocument.student(), student))
                .addJoin("d", NarfuDocument.settings(), "sett")
                .add(MQExpression.eq("sett", RelDocumentTypeKind.type(), documentType))
                .addJoin("sett", RelDocumentTypeKind.kind(), "k");

        builder.getSelectAliasList().clear();
        builder.addSelect("k");
        builder.setNeedDistinct(true);

        return builder.getResultList(getSession());
    }

    @Override
    public List<Document> getUsedDocuments(Student student, DocumentType documentType, DocumentKind documentKind) {
        MQBuilder builder = new MQBuilder(NarfuDocument.ENTITY_CLASS, "d")
                .add(MQExpression.or(
                        MQExpression.isNull("d", NarfuDocument.tmp()),
                        MQExpression.eq("d", NarfuDocument.tmp(), Boolean.FALSE)
                ))
                .add(MQExpression.eq("d", NarfuDocument.student(), student))
                .addJoin("d", NarfuDocument.settings(), "sett")
                .add(MQExpression.eq("sett", RelDocumentTypeKind.type(), documentType))
                .add(MQExpression.eq("sett", RelDocumentTypeKind.kind(), documentKind))
                .addJoin("sett", RelDocumentTypeKind.document(), "dd");

        builder.getSelectAliasList().clear();
        builder.addSelect("dd");
        builder.setNeedDistinct(true);

        return builder.getResultList(getSession());
    }

    @Override
    public List<Long> getIds(Collection<? extends IEntity> list) {
        if (list == null)
            list = Collections.emptyList();

        List<Long> resultList = new ArrayList<Long>();
        for (IEntity en : list)
            resultList.add(en.getId());

        return resultList;
    }

    @Override
    public List<NarfuDocument> getUsedStudentDocuments(Student student, DocumentType documentType, DocumentKind documentKind, Document document) {
        MQBuilder builder = new MQBuilder(NarfuDocument.ENTITY_CLASS, "d")
                .add(MQExpression.or(
                        MQExpression.isNull("d", NarfuDocument.tmp()),
                        MQExpression.eq("d", NarfuDocument.tmp(), Boolean.FALSE)
                ))
                .add(MQExpression.eq("d", NarfuDocument.student(), student))
                .add(MQExpression.eq("d", NarfuDocument.settings().type(), documentType))
                .add(MQExpression.eq("d", NarfuDocument.settings().kind(), documentKind))
                .add(MQExpression.eq("d", NarfuDocument.settings().document(), document));

        return builder.getResultList(getSession());
    }

    @Override
    public List<NarfuDocument> getAllStudentDocuments(Student student) {
        MQBuilder builder = new MQBuilder(NarfuDocument.ENTITY_CLASS, "d")
                .add(MQExpression.or(
                        MQExpression.isNull("d", NarfuDocument.tmp()),
                        MQExpression.eq("d", NarfuDocument.tmp(), Boolean.FALSE)
                ))
                .add(MQExpression.eq("d", NarfuDocument.student(), student));

        return builder.getResultList(getSession());
    }

    @Override
    public List<DocRepresentStudentDocuments> getDocuments(Long representId) {
        MQBuilder builder = new MQBuilder(DocRepresentStudentDocuments.ENTITY_CLASS, "rel")
                .add(MQExpression.eq("rel", DocRepresentStudentDocuments.representation().id(), representId));

        return builder.getResultList(getSession());
    }

    @Override
    public List<DocRepresentStudentIC> getIdentityCards(Long representId) {
        MQBuilder builder = new MQBuilder(DocRepresentStudentIC.ENTITY_CLASS, "rel")
                .add(MQExpression.eq("rel", DocRepresentStudentIC.representation().id(), representId));

        return builder.getResultList(getSession());
    }

}
