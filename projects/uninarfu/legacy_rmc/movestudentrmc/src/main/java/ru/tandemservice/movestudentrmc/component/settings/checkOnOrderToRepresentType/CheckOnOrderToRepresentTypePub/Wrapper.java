package ru.tandemservice.movestudentrmc.component.settings.checkOnOrderToRepresentType.CheckOnOrderToRepresentTypePub;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.EntityBase;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;

public class Wrapper extends EntityBase implements ITitled {

    public static final String USED = "used";
    public static final String VALUE = "value";
    public static final String TITLE = "type.titleWithType";

    private RepresentationType type;
    private boolean used;
    private boolean value;

    public Wrapper(RepresentationType type, boolean used, boolean value) {
        this.type = type;
        this.used = used;
        this.value = value;
    }

    @Override
    public Long getId() {
        return type.getId();
    }

    public RepresentationType getType() {
        return type;
    }

    public void setType(RepresentationType type) {
        this.type = type;
    }

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    public boolean isValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    @Override
    public String getTitle() {
        return type.getTitleWithType();
    }

}
