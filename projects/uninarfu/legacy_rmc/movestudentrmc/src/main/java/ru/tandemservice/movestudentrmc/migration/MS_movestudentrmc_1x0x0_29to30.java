package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_movestudentrmc_1x0x0_29to30 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.10"),
                        new ScriptDependency("org.tandemframework.shared", "1.1.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.1.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception {
        if (!tool.columnExists("grantview_t", "suspend_p")) {
            tool.createColumn("grantview_t", new DBColumn("suspend_p", DBType.BOOLEAN));

            // задать значение по умолчанию
            java.lang.Boolean defaultSuspend = false;
            tool.executeUpdate("update grantview_t set suspend_p=? where suspend_p is null", defaultSuspend);

            // сделать колонку NOT NULL
            tool.setColumnNullable("grantview_t", "suspend_p", false);

        }
    }

}