package ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrant.logic;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfString;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.AbstractListPrintDoc;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.ListRepresentGrant;
import ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents;
import ru.tandemservice.movestudentrmc.entity.RelTypeGrantView;
import ru.tandemservice.movestudentrmc.entity.catalog.Grant;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.ArrayList;
import java.util.List;

public class ListPrintDoc extends AbstractListPrintDoc<ListRepresentGrant>
{

    public final String TEMPLATE_HEADER = "list.rep.grant.header";
    public final String TEMPLATE_PARAG = "list.rep.grant.parag";
    public final String TEMPLATE_ADDON = "list.rep.grant.addon";

    @Override
    protected String getTemplateHeader()
    {
        return TEMPLATE_HEADER;
    }

    @Override
    protected String getTemplateParag()
    {
        return TEMPLATE_PARAG;
    }

    @Override
    protected String getTemplateAddon()
    {
        return TEMPLATE_ADDON;
    }

    @Override
    protected Class<ListRepresentGrant> getListRepresentClass()
    {
        return ListRepresentGrant.class;
    }

    @Override
    protected void paragraphInjectModifier(RtfInjectModifier im, ListRepresentGrant listRepresent, Student student)
    {
        injectModifier(im, listRepresent, student);
    }

    private void injectModifier(RtfInjectModifier im, ListRepresentGrant listRepresent, Student student)
    {
        im.put("ed_form_Genetive", student.getEducationOrgUnit().getDevelopForm().getGenCaseTitle().toLowerCase());
        im.put("st_date", UtilPrintSupport.getDateFormatterWithMonthString(listRepresent.getDateBeginingPayment()));
        im.put("end_date", listRepresent.getDateEndOfPayment() != null ? UtilPrintSupport.getDateFormatterWithMonthStringAndYear(listRepresent.getDateEndOfPayment()) : new RtfString().append(" на весь период обучения"));
        im.put("pay", student.getCompensationType().getCode().equals(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET) ? "за счет средств федерального бюджета" : student.getCompensationType().getShortTitle());
        im.put("Spec_gr_Generive", listRepresent.getGrantView().getGenitive());
    }

    @Override
    protected void addonInjectModifier(RtfInjectModifier im, ListRepresentGrant listRepresent, Student student)
    {
        injectModifier(im, listRepresent, student);
        im.put("f_unit_Genetive", student.getEducationOrgUnit().getFormativeOrgUnit().getGenitiveCaseTitle().toLowerCase());
    }

    @Override
    protected String getKey(Student student, ListRepresentGrant represent)
    {
        return String.valueOf(student.getEducationOrgUnit().getDevelopForm().getId()) +
                student.getCompensationType().getId() +
                represent.getGrantView().getId() +
                represent.getDateBeginingPayment().getTime() +
                (represent.getDateEndOfPayment() != null ? represent.getDateEndOfPayment().getTime() : "");
    }

    @Override
    protected List<String[]> getTableData(List<Student> studentList, List<Object[]> additionalList)
    {
        List<String[]> tableList = new ArrayList<>();
        int counter = 1;
        for (Student student : studentList)
        {
            String[] row = new String[4];
            row[0] = "" + counter++;
            row[1] = student.getBookNumber() != null ? student.getBookNumber() : "";
            row[2] = student.getPerson().getFullFio();
            row[3] = student.getCourse().getTitle();

            tableList.add(row);
        }

        return tableList;
    }

    @Override
    protected void headerInjectModifier(RtfInjectModifier im,
                                        ListRepresentGrant listRepresent)
    {
        //Наименование стипендии
        GrantView grantView = listRepresent.getGrantView();
        List<RelTypeGrantView> relTypeGrantViews = DataAccessServices.dao().getList(RelTypeGrantView.class, RelTypeGrantView.view(),
                                                                                    grantView);
        Grant grant;
        try
        {
            grant = relTypeGrantViews.get(0).getGrant();
        } catch (Exception e)
        {
            throw new ApplicationException("Распечатать приказ невозможно, т.к. не найдена настройка стипендия для вида : \"" +
                    (grantView.getShortTitle() != null ? grantView.getShortTitle() : grantView.getTitle()) + "\"");
        }

        String grantTitle = (grant.getGenitive() == null ? grant.getTitle() : grant.getGenitive());

        im.put("Grant_name_Generive", grantTitle);
    }

    @Override
    protected void applyOrders(DQLSelectBuilder builder)
    {
        super.applyOrders(builder);
        builder.order(DQLExpressions.property(ListRepresentGrant.grantView().title().fromAlias("rep")))
                .order(DQLExpressions.property(RelListRepresentStudents.student().educationOrgUnit().developForm().shortTitle().fromAlias("rel")))
                .order(DQLExpressions.property(RelListRepresentStudents.student().compensationType().shortTitle().fromAlias("rel")))
                .order(DQLExpressions.property(RelListRepresentStudents.student().course().intValue().fromAlias("rel")));
    }

}
