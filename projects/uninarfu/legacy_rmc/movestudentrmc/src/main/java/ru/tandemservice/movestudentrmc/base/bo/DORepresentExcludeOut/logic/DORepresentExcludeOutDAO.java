package ru.tandemservice.movestudentrmc.base.bo.DORepresentExcludeOut.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentDAO;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.RepresentExcludeOut;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentationTypeCodes;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.catalog.codes.DevelopConditionCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class DORepresentExcludeOutDAO extends AbstractDORepresentDAO implements IDORepresentExcludeOutDAO {

    @Override
    public boolean doRollback(Representation represent, ErrorCollector error) {
        if (!super.doRollback(represent, error))
            return false;

        List<DocRepresentStudentBase> studentList = getStudentList(represent);
        Student student = studentList.get(0).getStudent();
        RepresentExcludeOut rep = (RepresentExcludeOut) represent;

        StudentStatus status = null;
        if (rep.getOldStudentStatusId() != null)
            status = get(rep.getOldStudentStatusId());
        if (status == null)
            error.add("Не найден старый статус студента");

        Course course = null;
        if (rep.getOldCourseId() != null)
            course = get(rep.getOldCourseId());
        if (course == null)
            error.add("Не найден старый курс");

        Group group = null;
        if (rep.getOldGroupId() != null) {
            group = get(rep.getOldGroupId());
            if (group == null)
                error.add("Не найдена старая группа");
        }

        CompensationType compensationType = get(rep.getOldCompensationTypeId());

        EducationOrgUnit eduOrgUnit = null;
        if (rep.getOldEducationOrgUnitId() != null)
            eduOrgUnit = get(rep.getOldEducationOrgUnitId());
        if (eduOrgUnit == null)
            error.add("Не найдены старые данные");

        if (!error.isHasFieldErrors()) {
            student.setStatus(status);
            student.setCourse(course);
            student.setGroup(group);

            student.setEducationOrgUnit(eduOrgUnit);
            student.setCompensationType(compensationType);

            getSession().saveOrUpdate(student);

            doRollback(student, rep, getSession());
        }
        else
            return false;

        return true;
    }

    @Override
    public boolean doCommit(Representation represent, ErrorCollector error) {
        if (!super.doCommit(represent, error))
            return false;

        List<DocRepresentStudentBase> studentList = getStudentList(represent);
        Student student = studentList.get(0).getStudent();
        RepresentExcludeOut rep = (RepresentExcludeOut) represent;

        if (rep.getEducationOrgUnit() != null)
            student.setEducationOrgUnit(rep.getEducationOrgUnit());
        else
            student.setEducationOrgUnit(rep.getGroup().getEducationOrgUnit());
        student.setGroup(rep.getGroup());
        student.setCourse(rep.getCourse());
        student.setCompensationType(rep.getCompensationType());
        student.setStatus(UniDaoFacade.getCoreDao().getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_ACTIVE));

        getSession().saveOrUpdate(student);

        //проставляем новуе УП-РУП
        doCommit(student, rep, getSession());

        return true;
    }

    @Override
    public Representation createRepresentObject(RepresentationType type, List<Student> studentList) {
        RepresentExcludeOut result = new RepresentExcludeOut();
        result.setType(type);

        return result;
    }

    @Override
    public void buildBodyExtract(Representation representationBase, String itemFac, RtfDocument docCommon) {
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);
        UtilPrintSupport.setParNum(docCommon, itemFac);

        RtfInjectModifier modifier = new RtfInjectModifier();

        UtilPrintSupport.injectModifierExtract(modifier, docRepresent);
        injectModifier(modifier, docRepresent);

        modifier.modify(docCommon);
    }

    @Override
    public void buildBodyRepresent(Representation representationBase, RtfDocument document, AtomicInteger parNumber, AtomicInteger extractNumber) {
        super.buildBodyRepresent(representationBase, document, parNumber, extractNumber);
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);
        Student student = docRepresent.getStudent();

        RtfInjectModifier modifier = new RtfInjectModifier();

        RepresentExcludeOut representation = (RepresentExcludeOut) docRepresent.getRepresentation();

        EducationOrgUnit oldEducationOrgUnit = get(representation.getOldEducationOrgUnitId());
        Group oldGroup = null;
        if (representation.getOldGroupId() != null)
            oldGroup = get(representation.getOldGroupId());
        Course oldCourse = get(representation.getOldCourseId());
        /** В параграф должны выводиться старые данные **/
        if (oldEducationOrgUnit != null && oldGroup != null && oldCourse != null)
            UtilPrintSupport.injectModifierRepresent(modifier, docRepresent, student, oldEducationOrgUnit, oldCourse, oldGroup);
        else
            UtilPrintSupport.injectModifierRepresent(modifier, docRepresent, student);

        //modifier.put("studentTitle", PersonManager.instance().declinationDao().getDeclinationFIO(student.getPerson().getIdentityCard(), GrammaCase.ACCUSATIVE));
        modifier.put("studentTitle", UtilPrintSupport.printSeparatedFIO(student.getPerson().getIdentityCard(), GrammaCase.ACCUSATIVE, student.getPerson().isMale()));

        injectModifier(modifier, docRepresent);

        if (StringUtils.isEmpty(modifier.getStringValue("basics")))
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Arrays.asList("basics"), false, false);

        modifier.modify(document);
    }

    @Override
    protected void injectModifier(RtfInjectModifier modifier, DocRepresentStudentBase docrepresent) {
        RepresentExcludeOut represent = (RepresentExcludeOut) docrepresent.getRepresentation();

        if (docrepresent.getStudent().getPerson().isMale()) {
            modifier.put("studentSex", "обучавшегося");
            modifier.put("studentSex2", "проживающего");
        }
        else {
            modifier.put("studentSex", "обучавшуюся");
            modifier.put("studentSex2", "проживающей");
        }

        if ("1".equals(represent.getCompensationType().getCode()))
            modifier.put("developPayment", "для обучения за счет средств федерального бюджета");
        else
            modifier.put("developPayment", "для обучения с полным возмещением затрат");

        modifier.put("newCourse", represent.getCourse().getTitle());

        String groupStr = "";

        if (represent.getGroup() != null)
            groupStr = "в " + represent.getGroup().getTitle() + " группу ";

        modifier.put("newGroup", groupStr);

        modifier.put("newForm", UtilPrintSupport.getDevelopFormGen(represent.getGroup().getEducationOrgUnit().getDevelopForm()));
        
        //-------------------------------------------------------------------------------------------------------------------------------
        //modifier.put("newDevelopForm", UtilPrintSupport.getDevelopFormGen(represent.getGroup().getEducationOrgUnit().getDevelopForm()));
        //modifier.put("newHighSchool", UtilPrintSupport.getHighLevelSchoolTypeString(represent.getGroup().getEducationOrgUnit().getEducationLevelHighSchool()));
        //modifier.put("newFormOrgUnit", represent.getGroup().getEducationOrgUnit().getFormativeOrgUnit().getGenitiveCaseTitle());

        EducationOrgUnit eduOrgUnit = represent.getEducationOrgUnit();
        // Для старых приказов, чтобы не выпадала ошибка при печати старых выписок
        if(eduOrgUnit == null) {
        	eduOrgUnit = represent.getGroup().getEducationOrgUnit();	//для старых приказов
        }
        
        modifier.put("newDevelopForm", UtilPrintSupport.getDevelopFormGen(eduOrgUnit.getDevelopForm()));
        modifier.put("newHighSchool", UtilPrintSupport.getHighLevelSchoolTypeString(eduOrgUnit.getEducationLevelHighSchool()));

        String developTermCode;
        switch (eduOrgUnit.getDevelopCondition().getCode()){
            case DevelopConditionCodes.REDUCED_PERIOD:
                developTermCode = " в сокращенные сроки " + eduOrgUnit.getDevelopPeriod().getTitle();
                break;
            case DevelopConditionCodes.ACCELERATED_PERIOD:
                developTermCode = " в ускоренные сроки";
                break;
            case DevelopConditionCodes.REDUCED_ACCELERATED_PERIOD:
                developTermCode = " в сокращенные ускоренные сроки " + eduOrgUnit.getDevelopPeriod().getTitle();
                break;
            default:
                developTermCode = "";
        }
        modifier.put("developTermCode", developTermCode);
        
        modifier.put("newFormOrgUnit", eduOrgUnit.getFormativeOrgUnit().equals(((EducationOrgUnit)get(represent.getOldEducationOrgUnitId())).getFormativeOrgUnit()) ? "" : " " + eduOrgUnit.getFormativeOrgUnit().getGenitiveCaseTitle());
        //-------------------------------------------------------------------------------------------------------------------------------

        modifier.put("newInst", represent.getGroup().getEducationOrgUnit().getFormativeOrgUnit().getGenitiveCaseTitle());
        modifier.put("newInst2", represent.getGroup().getEducationOrgUnit().getFormativeOrgUnit().getGenitiveCaseTitle());

        modifier.put("newSpecType", UtilPrintSupport.getHighLevelSchoolTypeString(represent.getGroup().getEducationOrgUnit().getEducationLevelHighSchool()));
        modifier.put("newSpec", represent.getGroup().getEducationOrgUnit().getTitle());
        modifier.put("newBase", represent.getCompensationType().getShortTitle());

        modifier.put("newGrant", "");
        modifier.put("newPGO", "");

        if (represent.getGroup().getEducationOrgUnit().getFormativeOrgUnit().getHead() != null)
            modifier.put("fioDirNewInst", ((EmployeePost) represent.getGroup().getEducationOrgUnit().getFormativeOrgUnit().getHead()).getFio());
        else
            modifier.put("fioDirNewInst", "");

        if (represent.getEndDate() != null) {
        	//----------------------------------------------------------------------------------------------
            //RtfString str = new RtfString().append(" c условием ликвидации разницы в учебных планах до ");
            RtfString str = new RtfString().append(", c условием ликвидации разницы в учебных планах до ");		//добавлена запятая
            //----------------------------------------------------------------------------------------------
            UtilPrintSupport.getDateFormatterWithMonthStringAndYear(represent.getEndDate(), str);
            modifier.put("dateEnd", str);

            //modifier.put("dateEnd", " до " + DateFormattingUtil.MONTH_STRING_DATE_FORMAT.format(represent.getEndDate()) + " года");
            str = new RtfString().append("Установить срок ликвидации разницы в учебных планах до ");
            UtilPrintSupport.getDateFormatterWithMonthStringAndYear(represent.getEndDate(), str);
            modifier.put("endDate", str);
            modifier.put("raznica", "Разница в учебных планах:");
        }
        else {
            modifier.put("dateEnd", "");
            modifier.put("endDate", "");
            modifier.put("raznica", "Разницы в учебных планах нет");
        }


        modifier.put("birth", docrepresent.getStudent().getPerson().getBirthDateStr());

        if (docrepresent.getStudent().getPerson().getAddress() != null) {
            modifier.put("address", docrepresent.getStudent().getPerson().getAddress().getTitleWithFlat());
            modifier.put("phoneHome", docrepresent.getStudent().getPerson().getContactData() != null ? docrepresent.getStudent().getPerson().getContactData().getPhoneFact() : null);
            modifier.put("phoneMob", docrepresent.getStudent().getPerson().getContactData() != null ? docrepresent.getStudent().getPerson().getContactData().getPhoneMobile() : null);
        }
        else {
            modifier.put("address", "");
            modifier.put("phoneHome", "");
            modifier.put("phoneMob", "");
        }

        if (docrepresent.getStudent().getPerson().getWorkPlacePosition() != null)
            modifier.put("workPost", docrepresent.getStudent().getPerson().getWorkPlacePosition());
        else
            modifier.put("workPost", "не работаю");

        Representation lastExtract = findLastExclude(docrepresent.getStudent(), docrepresent.getRepresentation().getStartDate());
        if (lastExtract != null) {
            StringBuilder sb = new StringBuilder()
                    .append("Был")
                    .append(!docrepresent.getStudent().getPerson().isMale() ? "а" : "")
                    .append(" отчислен")
                    .append(!docrepresent.getStudent().getPerson().isMale() ? "а" : "")
                    .append(" ")
                    .append(lastExtract.getReason().getTitle().toLowerCase());
            modifier.put("reasonExclude", sb.toString());
        }
        else
            modifier.put("reasonExclude", "");

        String reasonStr = represent.getReason().getTitle().toLowerCase();
        reasonStr = reasonStr.replace("_____ курса", docrepresent.getCourseStr() + " курса");
        //--------------------------------------
        //modifier.put("reason", reasonStr);
        modifier.put("reason", reasonStr.equalsIgnoreCase("продолжение обучения") ? "" : ", " + reasonStr);
        //--------------------------------------
    }

    private Representation findLastExclude(Student student, Date currDate) {
        MQBuilder builder = new MQBuilder(DocRepresentStudentBase.ENTITY_CLASS, "dr")
                .add(MQExpression.eq("dr", DocRepresentStudentBase.student(), student))
                .add(MQExpression.eq("dr", DocRepresentStudentBase.representation().committed(), Boolean.TRUE))
                .add(MQExpression.eq("dr", DocRepresentStudentBase.representation().type().code(), RepresentationTypeCodes.EXCLUDE))
                .add(MQExpression.less("dr", DocRepresentStudentBase.representation().startDate(), currDate))
                .addOrder("dr", DocRepresentStudentBase.representation().startDate().s(), OrderDirection.desc);

        List<DocRepresentStudentBase> list = builder.getResultList(getSession());
        if (list.isEmpty())
            return null;
        else
            return list.get(0).getRepresentation();
    }
}
