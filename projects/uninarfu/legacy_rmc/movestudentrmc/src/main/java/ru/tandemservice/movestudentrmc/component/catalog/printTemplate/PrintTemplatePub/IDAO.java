package ru.tandemservice.movestudentrmc.component.catalog.printTemplate.PrintTemplatePub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultPrintCatalogPub.IDefaultPrintCatalogPubDAO;
import ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate;

/**
 * @author AutoGenerator
 *         Created on 07.12.2011
 */
public interface IDAO extends IDefaultPrintCatalogPubDAO<PrintTemplate, Model> {
}