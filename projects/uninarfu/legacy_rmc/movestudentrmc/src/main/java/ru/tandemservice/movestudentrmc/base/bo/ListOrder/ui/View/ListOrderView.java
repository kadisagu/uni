package ru.tandemservice.movestudentrmc.base.bo.ListOrder.ui.View;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.movestudentrmc.base.bo.ListOrder.logic.RepresentDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListOrder.logic.RepresentOrderDSHandler;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;

@Configuration
public class ListOrderView extends BusinessComponentManager {

    public static final String REPRESENT_ORDER_DS = "representOrderDS";

    @Bean
    public ColumnListExtPoint representOrderDS()
    {
        return columnListExtPointBuilder(REPRESENT_ORDER_DS)
                //.addColumn(textColumn(RepresentDSHandler.GRANT_COLUMN, ListRepresent.grantView().title()).order().create())               
                //.addColumn(textColumn(RepresentDSHandler.TYPE_REPRESENT_COLUMN,  "represent."+ListRepresent.representationType().title()).order().create())
                .addColumn(publisherColumn(RepresentDSHandler.TYPE_REPRESENT_COLUMN, "represent." + ListRepresent.title()).order().create())
                .addColumn(textColumn(RepresentDSHandler.COUNT_COLUMN, "count").order().create())
                .addColumn(booleanColumn(RepresentDSHandler.CHECK, "represent." + ListRepresent.check()).order().create())
                             
  /*              .addColumn(textColumn(RepresentDSHandler.DEVELOP_FORM_COLUMN, "student." + Student.educationOrgUnit().developForm().title()).order().create())               
                .addColumn(textColumn(RepresentDSHandler.FORMATIVE_ORG_UNIT_COLUMN, "student." +  Student.educationOrgUnit().formativeOrgUnit().title()).order().create())
                .addColumn(textColumn(RepresentDSHandler.TERRITORIAL_ORG_UNIT_COLUMN, "student." +  Student.educationOrgUnit().territorialOrgUnit().fullTitle()).order().create())
                .addColumn(textColumn(RepresentDSHandler.EDUCATION_LEVEL_HIGH_SCHOOL_COLUMN, "student." +  Student.educationOrgUnit().educationLevelHighSchool().orgUnit().fullTitle()).order().create())
                .addColumn(textColumn(RepresentDSHandler.EDUCATION_ORG_UNIT_COLUMN,"student." +  Student.educationOrgUnit().title()).order().create())
                .addColumn(textColumn(RepresentDSHandler.TYPE_REPRESENT_COLUMN, "represent." +  Representation.type().title()).order().create())
                //.addColumn(actionColumn(RepresentDSHandler.TYPE_REPRESENT_COLUMN, "represent." +  Representation.type().title(), "onViewRepresentationFromList").order().create())
                .addColumn(dateColumn(RepresentDSHandler.DATE_REPRESENT_COLUMN, "represent." + Representation.startDate()).order().create())
                .addColumn(textColumn(RepresentDSHandler.COURSE_COLUMN, "student." +  Student.course().title()).order().create())
                .addColumn(textColumn(RepresentDSHandler.GROUP_COLUMN, "student." +  Student.group().title()).order().create())
                .addColumn(textColumn(RepresentDSHandler.BUDGET_COLUMN, "student." +  Student.compensationType().shortTitle()).order().create())
                .addColumn(textColumn(RepresentDSHandler.STUDENT_FIO_COLUMN, "student." +  Student.person().identityCard().fullFio()).order().create())
*/.create();

    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(REPRESENT_ORDER_DS, representOrderDS()).handler(representOrderDSHandler()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> representOrderDSHandler()
    {
        return new RepresentOrderDSHandler(getName());
    }

}
