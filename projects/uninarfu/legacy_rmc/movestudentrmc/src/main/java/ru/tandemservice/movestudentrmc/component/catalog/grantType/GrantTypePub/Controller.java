package ru.tandemservice.movestudentrmc.component.catalog.grantType.GrantTypePub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantType;

public class Controller extends DefaultCatalogPubController<GrantType, Model, IDAO> {

    @Override
    protected DynamicListDataSource<GrantType> createListDataSource(
            IBusinessComponent context)
    {
        Model model = (Model) getModel(context);

        DynamicListDataSource<GrantType> ds = new DynamicListDataSource<GrantType>(context, this);

        ds.addColumn(getCatalogItemLinkColumn(model, "Наименование", "title"));
        ds.addColumn(new ToggleColumn("Считать академической", GrantType.academ()).setListener("onClickToggleAcadem"), 2);
        ds.addColumn(new ToggleColumn("Считать социальной", GrantType.social()).setListener("onClickToggleSocial"), 3);
        ds.addColumn(new ActionColumn("Редактировать", "edit", "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));

        return ds;
    }

    public void onClickToggleAcadem(IBusinessComponent component)
    {
        getDao().updateAcadem(getModel(component), (Long) component.getListenerParameter());
    }

    public void onClickToggleSocial(IBusinessComponent component)
    {
        getDao().updateSocial(getModel(component), (Long) component.getListenerParameter());
    }
}
