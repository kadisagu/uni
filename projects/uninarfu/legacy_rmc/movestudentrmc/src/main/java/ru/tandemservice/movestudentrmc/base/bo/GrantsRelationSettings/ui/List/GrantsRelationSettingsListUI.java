package ru.tandemservice.movestudentrmc.base.bo.GrantsRelationSettings.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import ru.tandemservice.movestudentrmc.base.bo.GrantsRelationSettings.ui.AddEdit.GrantsRelationSettingsAddEdit;
import ru.tandemservice.movestudentrmc.base.bo.GrantsRelationSettings.ui.AddEdit.GrantsRelationSettingsAddEditUI;

public class GrantsRelationSettingsListUI extends UIPresenter {

    @Override
    public void onComponentRefresh()
    {
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegion(GrantsRelationSettingsAddEdit.class)
                .parameter(GrantsRelationSettingsAddEditUI.GRANT_TYPE_ID, getListenerParameterAsLong())
                .activate();
    }
}
