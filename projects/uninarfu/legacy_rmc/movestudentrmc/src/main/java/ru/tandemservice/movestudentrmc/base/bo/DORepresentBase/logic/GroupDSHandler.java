package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.orgstruct.Group;

public class GroupDSHandler extends DefaultComboDataSourceHandler {

    public static final String NEW_FORMATIVE_ORG_UNIT = "formOrgUnit";
    public static final String NEW_TERRITORIAL_ORG_UNIT = "terrOrgUnit";
    public static final String EDUCATION_LEVELS_HIGH_SCHOOL = "educationLevelsHighSchool";
    public static final String DEVELOP_FORM = "developForm";
    public static final String DEVELOP_CONDITION = "developCondition";
    public static final String DEVELOP_PERIOD = "developPeriod";
    public static final String DEVELOP_TECH = "developTech";
    public static final String COURSE = "course";
    public static final String SHOW_ALL_GROUPS = "showAllGroups";

    public GroupDSHandler(String ownerId) {
        super(ownerId, Group.class);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep) {

        String str = ep.input.getComboFilterByValue();
        if (StringUtils.isNotBlank(str))
            ep.dqlBuilder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property("e", Group.title())), DQLExpressions.value(CoreStringUtils.escapeLike(str, true))));

        Course course = ep.context.get(COURSE);
        OrgUnit formOrgUnit = ep.context.get(NEW_FORMATIVE_ORG_UNIT);
        OrgUnit terrOrgUnit = ep.context.get(NEW_TERRITORIAL_ORG_UNIT);
        EducationLevelsHighSchool educationLevelsHighSchool = ep.context.get(EDUCATION_LEVELS_HIGH_SCHOOL);
        DevelopForm developForm = (DevelopForm) ep.context.get(DEVELOP_FORM);
        DevelopCondition developCondition = (DevelopCondition) ep.context.get(DEVELOP_CONDITION);
        DevelopPeriod developPeriod = (DevelopPeriod) ep.context.get(DEVELOP_PERIOD);
        DevelopTech developTech = (DevelopTech) ep.context.get(DEVELOP_TECH);
        boolean showAllGroups = (boolean) ep.context.get(SHOW_ALL_GROUPS);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Group.class, "g")
                .where(DQLExpressions.eqValue(DQLExpressions.property(Group.educationOrgUnit().formativeOrgUnit().fromAlias("g")), formOrgUnit))
                .column(DQLExpressions.property(Group.id().fromAlias("g")));


        if (!showAllGroups) {
            builder
                    .where(DQLExpressions.eqValue(DQLExpressions.property(Group.course().fromAlias("g")), course))
                    .where(DQLExpressions.eqValue(DQLExpressions.property(Group.educationOrgUnit().territorialOrgUnit().fromAlias("g")), terrOrgUnit))
                    .where(DQLExpressions.eqValue(DQLExpressions.property(Group.educationOrgUnit().educationLevelHighSchool().fromAlias("g")), educationLevelsHighSchool))
                    .where(DQLExpressions.eqValue(DQLExpressions.property(Group.educationOrgUnit().developForm().fromAlias("g")), developForm))
                    .where(DQLExpressions.eqValue(DQLExpressions.property(Group.educationOrgUnit().developCondition().fromAlias("g")), developCondition))
                    .where(DQLExpressions.eqValue(DQLExpressions.property(Group.educationOrgUnit().developPeriod().fromAlias("g")), developPeriod))
                    .where(DQLExpressions.eqValue(DQLExpressions.property(Group.educationOrgUnit().developTech().fromAlias("g")), developTech));
            //FilterUtils.applySelectFilter(builder, "g", Group.educationOrgUnit().formativeOrgUnit(), formOrgUnit);
        } /*else {

			FilterUtils.applySelectFilter(builder, "g", Group.course(), course);
			//FilterUtils.applySelectFilter(builder, "g", Group.educationOrgUnit().formativeOrgUnit(), formOrgUnit);
			FilterUtils.applySelectFilter(builder, "g", Group.educationOrgUnit().territorialOrgUnit(), terrOrgUnit);
			FilterUtils.applySelectFilter(builder, "g", Group.educationOrgUnit().educationLevelHighSchool(), educationLevelsHighSchool);
			FilterUtils.applySelectFilter(builder, "g", Group.educationOrgUnit().developForm(), developForm);
			FilterUtils.applySelectFilter(builder, "g", Group.educationOrgUnit().developCondition(), developCondition);
			FilterUtils.applySelectFilter(builder, "g", Group.educationOrgUnit().developPeriod(), developPeriod);
			FilterUtils.applySelectFilter(builder, "g", Group.educationOrgUnit().developTech(), developTech);
		}*/
		/*
		MQBuilder builder = new MQBuilder(Group.ENTITY_CLASS, "g")
			.add(MQExpression.eq("g", Group.course(), course))
			.add(MQExpression.eq("g", Group.educationOrgUnit().formativeOrgUnit(), formOrgUnit))
			.add(MQExpression.eq("g", Group.educationOrgUnit().territorialOrgUnit(), terrOrgUnit));
		builder.getSelectAliasList().clear();
		builder.addSelect("g.id");
		
		List<Long> list = builder.getResultList(getSession());
		*/
        ep.dqlBuilder.where(DQLExpressions.in(DQLExpressions.property(Group.id().fromAlias("e")), builder.getQuery()));
    }

}

