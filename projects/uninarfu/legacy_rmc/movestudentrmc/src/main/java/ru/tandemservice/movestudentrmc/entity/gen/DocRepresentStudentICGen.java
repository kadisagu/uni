package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentIC;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность-связь 'Представление'-'УЛ Студента'
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DocRepresentStudentICGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.DocRepresentStudentIC";
    public static final String ENTITY_NAME = "docRepresentStudentIC";
    public static final int VERSION_HASH = -614419054;
    private static IEntityMeta ENTITY_META;

    public static final String L_REPRESENTATION = "representation";
    public static final String L_REASON = "reason";
    public static final String L_STUDENT_I_C = "studentIC";

    private Representation _representation;     // Представление
    private RepresentationReason _reason;     // Причина
    private IdentityCard _studentIC;     // Сущность УЛ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Представление. Свойство не может быть null.
     */
    @NotNull
    public Representation getRepresentation()
    {
        return _representation;
    }

    /**
     * @param representation Представление. Свойство не может быть null.
     */
    public void setRepresentation(Representation representation)
    {
        dirty(_representation, representation);
        _representation = representation;
    }

    /**
     * @return Причина.
     */
    public RepresentationReason getReason()
    {
        return _reason;
    }

    /**
     * @param reason Причина.
     */
    public void setReason(RepresentationReason reason)
    {
        dirty(_reason, reason);
        _reason = reason;
    }

    /**
     * @return Сущность УЛ.
     */
    public IdentityCard getStudentIC()
    {
        return _studentIC;
    }

    /**
     * @param studentIC Сущность УЛ.
     */
    public void setStudentIC(IdentityCard studentIC)
    {
        dirty(_studentIC, studentIC);
        _studentIC = studentIC;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DocRepresentStudentICGen)
        {
            setRepresentation(((DocRepresentStudentIC)another).getRepresentation());
            setReason(((DocRepresentStudentIC)another).getReason());
            setStudentIC(((DocRepresentStudentIC)another).getStudentIC());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DocRepresentStudentICGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DocRepresentStudentIC.class;
        }

        public T newInstance()
        {
            return (T) new DocRepresentStudentIC();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "representation":
                    return obj.getRepresentation();
                case "reason":
                    return obj.getReason();
                case "studentIC":
                    return obj.getStudentIC();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "representation":
                    obj.setRepresentation((Representation) value);
                    return;
                case "reason":
                    obj.setReason((RepresentationReason) value);
                    return;
                case "studentIC":
                    obj.setStudentIC((IdentityCard) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "representation":
                        return true;
                case "reason":
                        return true;
                case "studentIC":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "representation":
                    return true;
                case "reason":
                    return true;
                case "studentIC":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "representation":
                    return Representation.class;
                case "reason":
                    return RepresentationReason.class;
                case "studentIC":
                    return IdentityCard.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DocRepresentStudentIC> _dslPath = new Path<DocRepresentStudentIC>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DocRepresentStudentIC");
    }
            

    /**
     * @return Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentIC#getRepresentation()
     */
    public static Representation.Path<Representation> representation()
    {
        return _dslPath.representation();
    }

    /**
     * @return Причина.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentIC#getReason()
     */
    public static RepresentationReason.Path<RepresentationReason> reason()
    {
        return _dslPath.reason();
    }

    /**
     * @return Сущность УЛ.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentIC#getStudentIC()
     */
    public static IdentityCard.Path<IdentityCard> studentIC()
    {
        return _dslPath.studentIC();
    }

    public static class Path<E extends DocRepresentStudentIC> extends EntityPath<E>
    {
        private Representation.Path<Representation> _representation;
        private RepresentationReason.Path<RepresentationReason> _reason;
        private IdentityCard.Path<IdentityCard> _studentIC;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentIC#getRepresentation()
     */
        public Representation.Path<Representation> representation()
        {
            if(_representation == null )
                _representation = new Representation.Path<Representation>(L_REPRESENTATION, this);
            return _representation;
        }

    /**
     * @return Причина.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentIC#getReason()
     */
        public RepresentationReason.Path<RepresentationReason> reason()
        {
            if(_reason == null )
                _reason = new RepresentationReason.Path<RepresentationReason>(L_REASON, this);
            return _reason;
        }

    /**
     * @return Сущность УЛ.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentIC#getStudentIC()
     */
        public IdentityCard.Path<IdentityCard> studentIC()
        {
            if(_studentIC == null )
                _studentIC = new IdentityCard.Path<IdentityCard>(L_STUDENT_I_C, this);
            return _studentIC;
        }

        public Class getEntityClass()
        {
            return DocRepresentStudentIC.class;
        }

        public String getEntityName()
        {
            return "docRepresentStudentIC";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
