package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.DocOrdRepresent;
import ru.tandemservice.movestudentrmc.entity.DocumentOrder;
import ru.tandemservice.movestudentrmc.entity.IRepresentOrder;
import ru.tandemservice.movestudentrmc.entity.Representation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность-связь 'Приказ'-'Представление'
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DocOrdRepresentGen extends EntityBase
 implements IRepresentOrder{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.DocOrdRepresent";
    public static final String ENTITY_NAME = "docOrdRepresent";
    public static final int VERSION_HASH = 1728827741;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORDER = "order";
    public static final String L_REPRESENTATION = "representation";

    private DocumentOrder _order;     // Приказ
    private Representation _representation;     // Представление

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приказ. Свойство не может быть null.
     */
    @NotNull
    public DocumentOrder getOrder()
    {
        return _order;
    }

    /**
     * @param order Приказ. Свойство не может быть null.
     */
    public void setOrder(DocumentOrder order)
    {
        dirty(_order, order);
        _order = order;
    }

    /**
     * @return Представление. Свойство не может быть null.
     */
    @NotNull
    public Representation getRepresentation()
    {
        return _representation;
    }

    /**
     * @param representation Представление. Свойство не может быть null.
     */
    public void setRepresentation(Representation representation)
    {
        dirty(_representation, representation);
        _representation = representation;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DocOrdRepresentGen)
        {
            setOrder(((DocOrdRepresent)another).getOrder());
            setRepresentation(((DocOrdRepresent)another).getRepresentation());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DocOrdRepresentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DocOrdRepresent.class;
        }

        public T newInstance()
        {
            return (T) new DocOrdRepresent();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "order":
                    return obj.getOrder();
                case "representation":
                    return obj.getRepresentation();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "order":
                    obj.setOrder((DocumentOrder) value);
                    return;
                case "representation":
                    obj.setRepresentation((Representation) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "order":
                        return true;
                case "representation":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "order":
                    return true;
                case "representation":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "order":
                    return DocumentOrder.class;
                case "representation":
                    return Representation.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DocOrdRepresent> _dslPath = new Path<DocOrdRepresent>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DocOrdRepresent");
    }
            

    /**
     * @return Приказ. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocOrdRepresent#getOrder()
     */
    public static DocumentOrder.Path<DocumentOrder> order()
    {
        return _dslPath.order();
    }

    /**
     * @return Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocOrdRepresent#getRepresentation()
     */
    public static Representation.Path<Representation> representation()
    {
        return _dslPath.representation();
    }

    public static class Path<E extends DocOrdRepresent> extends EntityPath<E>
    {
        private DocumentOrder.Path<DocumentOrder> _order;
        private Representation.Path<Representation> _representation;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приказ. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocOrdRepresent#getOrder()
     */
        public DocumentOrder.Path<DocumentOrder> order()
        {
            if(_order == null )
                _order = new DocumentOrder.Path<DocumentOrder>(L_ORDER, this);
            return _order;
        }

    /**
     * @return Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocOrdRepresent#getRepresentation()
     */
        public Representation.Path<Representation> representation()
        {
            if(_representation == null )
                _representation = new Representation.Path<Representation>(L_REPRESENTATION, this);
            return _representation;
        }

        public Class getEntityClass()
        {
            return DocOrdRepresent.class;
        }

        public String getEntityName()
        {
            return "docOrdRepresent";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
