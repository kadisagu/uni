package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.*;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.DORepresentBaseManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.ReasonDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.StudentDocumentsDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.EditBasic.DORepresentBaseEditBasicUI;
import ru.tandemservice.movestudentrmc.dao.CheckStateUtil;
import ru.tandemservice.movestudentrmc.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudentrmc.docord.IDOObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.*;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentExtractStatesCodes;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.OsspGrantsCodes;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.OsspPgoCodes;
import ru.tandemservice.movestudentrmc.util.ListenerWrapper;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unibase.UniBaseUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

@Input({
        @Bind(key = AbstractDORepresentEditUI.REPRESENTATION_ID, binding = AbstractDORepresentEditUI.REPRESENTATION_ID),
        @Bind(key = AbstractDORepresentEditUI.TYPE_ID, binding = AbstractDORepresentEditUI.TYPE_ID, required = true),
        @Bind(key = AbstractDORepresentEditUI.STUDENT_LIST_ID, binding = AbstractDORepresentEditUI.STUDENT_LIST_ID, required = true)
})
public abstract class AbstractDORepresentEditUI extends UIPresenter implements ListenerWrapper.IListener<NarfuDocument> {

    public static final String REPRESENTATION_ID = "representId";
    public static final String TYPE_ID = "typeId";
    public static final String STUDENT_LIST_ID = "studentListId";

    public static final String EDIT_BASIC_BLOCK_LIST = "editBasicBlockListExtPoint";
    public static final String EDIT_BASIC_BLOCK = "editBasicBlock";


    protected Long _representId;
    protected Long _typeId;
    protected RepresentationType _type;
    protected Representation _representObj;
    protected List<Long> _studentListId;
    protected List<Student> _studentList = new ArrayList<>();
    protected Student _student;
    protected String _studentStatusNewStr;
    private RepresentationReason _oldReason;
    protected List<DocRepresentBasics> _representBasicsList = new ArrayList<>();

    protected List<DevelopCondition> _developConditionList = new ArrayList<>();
    protected List<OsspGrants> _grantsOSSPList = new ArrayList<>();
    protected List<NarfuDocument> _documentsList = new ArrayList<>();
    protected List<OsspPgo> _pgoOSSPList = new ArrayList<>();
    protected List<DocRepresentStudentBase> _studentDocList = new ArrayList<>();
    //private final OsspGrants _noneGrants = DataAccessServices.dao().get(OsspGrants.class, OsspGrants.code(), OsspGrantsCodes.NONE);
    //private final OsspPgo _nonePgo = DataAccessServices.dao().get(OsspPgo.class, OsspPgo.code(), OsspPgoCodes.NONE);

    protected List<OsspGrantsView> _grantsOSSPViewList = new ArrayList<>();

    private List<DocRepresentGrants> _representGrantsList = new ArrayList<>();
    private DocRepresentGrants _representGrantRow;

    private List<DocRepresentStudentDocuments> _representStudentDocumentsList = new ArrayList<>();
    //private DocRepresentStudentDocuments _representStudentDocumentsRow;

    private List<NarfuDocument> _studentDocumentsNoRequiredList = new ArrayList<>();
    private List<NarfuDocument> _studentDocumentsNoRequiredListSelected = new ArrayList<>();


    //private List<RepresentationAdditionalDocumentsRelation> _representDocumentsList = new ArrayList<>();

    private List<OsspGrants> _grantNoRequiredList = new ArrayList<>();
    private List<OsspGrants> _grantNoRequiredListSelected = new ArrayList<>();

    private byte[] _additionalDocFile;     // Документ
    IUploadFile _uploadFile;

    private DocumentType _documentType;
    private DocumentKind _documentKind;
    private Document _document;

    private DynamicListDataSource<DocRepresentStudentDocuments> representDocumentDataSource;
    private List<DocRepresentStudentDocuments> representDocumentList;

    private DynamicListDataSource<DocRepresentStudentIC> representIdentityCardDataSource;
    private List<DocRepresentStudentIC> representIdentityCardList = new ArrayList<>();
    private boolean warning = false;
    private boolean save = false;
    private String warningMessage;

    @Override
    public void onComponentRefresh() {
        super.onComponentRefresh();

        if (_representObj == null) {
            initDocument();
        }
        _representId = _representObj.getId();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        if (ReasonDSHandler.REASON_DS.equals(dataSource.getName())) {
            dataSource.put(ReasonDSHandler.TYPE_REASON, _type);
        }
        dataSource.put(StudentDocumentsDSHandler.STUDENT_ID, _student.getId());
        dataSource.put("representId", _representId);
    }

    private void fillOSSPList() {

        if (_representObj.getReason() == null)
            return;

        if (_oldReason == null)
            _oldReason = _representObj.getReason();
        else if (_oldReason != _representObj.getReason())
            _oldReason = _representObj.getReason();
        else
            return;

        _developConditionList.clear();
        _grantsOSSPList.clear();
        _pgoOSSPList.clear();

        _grantNoRequiredList.clear();
        _grantNoRequiredListSelected.clear();

        _documentsList.clear();
        _studentDocumentsNoRequiredList.clear();
        _studentDocumentsNoRequiredListSelected.clear();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RelRepresentationReasonOSSP.class, "rel").column(property("rel"));
        builder.where(eq(
                property(RelRepresentationReasonOSSP.type().id().fromAlias("rel")),
                value(_representObj.getType().getId())));
        builder.where(eq(
                property(RelRepresentationReasonOSSP.reason().fromAlias("rel")),
                value(_representObj.getReason())));
        builder.where(eq(
                property(RelRepresentationReasonOSSP.budget().fromAlias("rel")),
                value(_student.getCompensationType().isBudget())));

        List<RelRepresentationReasonOSSP> listRelOSSP = builder.createStatement(getSupport().getSession()).list();


        DQLSelectBuilder developConditionBuilder = new DQLSelectBuilder().fromEntity(DevelopCondition.class, "rel").column(property("rel"));
        _developConditionList = developConditionBuilder.createStatement(getSupport().getSession()).list();

        DQLSelectBuilder studentDocumentsbuilder = new DQLSelectBuilder().fromEntity(NarfuDocument.class, "rel").column(property("rel"));
        studentDocumentsbuilder.where(eq(
                property(NarfuDocument.student().id().fromAlias("rel")),
                value(_studentListId.get(0))));

        List<NarfuDocument> studentDocumentslist = studentDocumentsbuilder.createStatement(getSupport().getSession()).list();

        for (RelRepresentationReasonOSSP ossp : listRelOSSP) {


            if (ossp.getGrantsOSSP() != null)
                if (!_grantsOSSPList.contains(ossp.getGrantsOSSP())) {
                    _grantsOSSPList.add(ossp.getGrantsOSSP());
                }

            if (ossp.getPgoOSSP() != null)
                if (!_pgoOSSPList.contains(ossp.getPgoOSSP())) {
                    _pgoOSSPList.add(ossp.getPgoOSSP());
                }
        }

        if (_representObj.getOsspGrants() == null) {
            OsspGrants _noneGrants = DataAccessServices.dao().get(OsspGrants.class, OsspGrants.code(), OsspGrantsCodes.NONE);
            if (_grantsOSSPList.size() == 1)
                _representObj.setOsspGrants(_grantsOSSPList.get(0));
            if (_grantsOSSPList.size() > 1 && _grantsOSSPList.contains(_noneGrants))
                _representObj.setOsspGrants(_noneGrants);
        }
        if (_representObj.getOsspPgo() == null) {
            OsspPgo _nonePgo = DataAccessServices.dao().get(OsspPgo.class, OsspPgo.code(), OsspPgoCodes.NONE);
            if (_pgoOSSPList.size() == 1)
                _representObj.setOsspPgo(_pgoOSSPList.get(0));
            if (_pgoOSSPList.size() > 1 && _pgoOSSPList.contains(_nonePgo))
                _representObj.setOsspPgo(_nonePgo);
        }

        _studentDocumentsNoRequiredList.addAll(studentDocumentslist);
    }

    public void onChangeStudentsDocumentFilter() {

        _studentDocumentsNoRequiredList.clear();
        _studentDocumentsNoRequiredListSelected.clear();
        _representStudentDocumentsList.clear();
        DQLSelectBuilder studentDocumentsbuilder = new DQLSelectBuilder().fromEntity(NarfuDocument.class, "rel").column(property("rel"));
        studentDocumentsbuilder.where(eq(
                property(NarfuDocument.student().id().fromAlias("rel")),
                value(_studentListId.get(0))));
        if (_documentType != null) {
            studentDocumentsbuilder.where(eq(
                    property(NarfuDocument.settings().type().id().fromAlias("rel")),
                    value(_documentType)));
        }

        if (_documentKind != null) {
            studentDocumentsbuilder.where(eq(
                    property(NarfuDocument.settings().kind().id().fromAlias("rel")),
                    value(_documentKind)));
        }

        if (_document != null) {
            studentDocumentsbuilder.where(eq(
                    property(NarfuDocument.settings().document().id().fromAlias("rel")),
                    value(_document)));
        }

        List<NarfuDocument> studentDocumentslist = studentDocumentsbuilder.createStatement(getSupport().getSession()).list();

        _studentDocumentsNoRequiredList.addAll(studentDocumentslist);

        FetchStudentDocuments(_representObj);
        studentDocumentsNotEmpty(_representObj);

        //onChangeStudentDocuments();

    }

    protected void initDocument() {
        _type = DataAccessServices.dao().get(RepresentationType.class, RepresentationType.id().s(), _typeId);

        if (_representId == null) {

            for (Long studentId : _studentListId) {

                _student = DataAccessServices.dao().get(Student.class, studentId);
                _studentList.add(_student);
            }

            _student = _studentList.get(0);

            IDOObjectByRepresentTypeManager representManager = DORepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(_type.getCode());
            _representObj = representManager.getDOObjectModifyDAO().createRepresentObject(_type, _studentList);
            _representObj.setCreateDate(getSupport().getCurrentDate());
            _representObj.setState(DataAccessServices.dao().get(MovestudentExtractStates.class, MovestudentExtractStates.code().s(), "1"));
            _representObj.setStatusOld(_student.getStatus());
            _representObj.setDevelopCondition(_student.getEducationOrgUnit().getDevelopCondition());

        }
        else {

            _representObj = DataAccessServices.dao().get(Representation.class, Representation.id().s(), _representId);

            FetchStudents(_representObj);
            FetchBasics(_representObj);
            FetchGrants(_representObj);
            FetchStudentDocuments(_representObj);
        }

        //в типе указано что стипендии у студента не трогаем
//        if (!_representObj.getType().isUseGrant())
        _representObj.setSaveGrants(true);


        _grantsOSSPViewList = UniDaoFacade.getCoreDao().getCatalogItemList(OsspGrantsView.class);
        grantsNotEmpty(_representObj);
        studentDocumentsNotEmpty(_representObj);
        //grantsNotEmpty();
        //fillOSSPList();
        //_studentStatusNewStr = ((StudentStatus) UniDaoFacade.getCoreDao().getCatalogItem(ru.tandemservice.uni.entity.catalog.StudentStatus.class, "8")).getTitle();
        _studentStatusNewStr = _student.getStatus().getTitle();

        //список документов представления
        if (representDocumentDataSource == null) {
            representDocumentDataSource = new DynamicListDataSource<>(this, new IListDataSourceDelegate()
            {
                private boolean loaded = false;

                @Override
                public void updateListDataSource(IBusinessComponent arg0)
                {
                    List<DocRepresentStudentDocuments> list;
                    if (loaded)
                        list = representDocumentList;
                    else
                    {
                        list = MoveStudentDaoFacade.getDocumentDAO().getDocuments(_representObj.getId());
                        representDocumentList = list;
                    }

                    UniBaseUtils.createPage(representDocumentDataSource, list);
                    loaded = true;
                }
            });

            representDocumentDataSource.addColumn(new SimpleColumn("Наименование", DocRepresentStudentDocuments.studentDocuments().s() + ".title"));
            representDocumentDataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditDocument"));
            representDocumentDataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteDocument"));
        }

        prepareRepresentIdentityCardDataSource();


    }

    public void onIgnoreWarning() throws IOException {
        setWarning(false);
        onClickApply();
    }

    public void onCancelRepresent() {
        setWarning(false);
        setSave(false);
    }

    protected void prepareRepresentIdentityCardDataSource() {
        //список УЛ представления
        if (representIdentityCardDataSource == null) {
            representIdentityCardDataSource = new DynamicListDataSource<>(this, new IListDataSourceDelegate()
            {
                private boolean loaded = false;

                @Override
                public void updateListDataSource(IBusinessComponent arg0)
                {
                    List<DocRepresentStudentIC> list;
                    if (loaded)
                        list = representIdentityCardList;
                    else
                    {
                        list = MoveStudentDaoFacade.getDocumentDAO().getIdentityCards(_representObj.getId());
                        representIdentityCardList = list;
                    }

                    UniBaseUtils.createPage(representIdentityCardDataSource, list);
                    loaded = true;
                }
            });

            representIdentityCardDataSource.addColumn(new SimpleColumn("Наименование", DocRepresentStudentIC.studentIC().cardType().s() + ".title"));
            //representIdentityCardDataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditIC"));
            representIdentityCardDataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteIC"));
        }
    }

    private void FetchBasics(Representation doc) {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocRepresentBasics.class, "b").column(property("b"));
        builder.where(eq(
                property(DocRepresentBasics.representation().fromAlias("b")),
                value(doc)));

        _representBasicsList = builder.createStatement(getSupport().getSession()).list();
    }

    private void FetchGrants(Representation doc) {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocRepresentGrants.class, "g").column(property("g"));
        builder.where(eq(
                property(DocRepresentGrants.representation().fromAlias("g")),
                value(doc)));

        _representGrantsList = builder.createStatement(getSupport().getSession()).list();
    }

    private void FetchStudentDocuments(Representation doc) {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocRepresentStudentDocuments.class, "g").column(property("g"));
        builder.where(eq(
                property(DocRepresentStudentDocuments.representation().fromAlias("g")),
                value(doc)));

        _representStudentDocumentsList = builder.createStatement(getSupport().getSession()).list();

    }

    protected abstract void FetchStudents(Representation doc);

    public void onClickApply() throws IOException {
        if (getRepresentObj().getId() != null)
            CheckStateUtil.checkStateRepresent(getRepresentObj(), Arrays.asList(MovestudentExtractStatesCodes.CODE_1, MovestudentExtractStatesCodes.CODE_2), _uiSupport.getSession());
        onClickSave();
    }

    public void onClickSave() throws IOException
    {
        if (_uploadFile != null) {
            _additionalDocFile = getBytesFromFile();
            //_representObj.setAdditionalDocFile(_additionalDocFile);
        }
        if (_studentDocList.isEmpty()) {

            _studentDocList = new ArrayList<>();

            //IDOObjectByRepresentTypeManager representManager = DORepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(_type.getCode());

            for (Student student : _studentList) {

                DocRepresentStudentBase studentDoc = new DocRepresentStudentBase();
                studentDoc.setRepresentation(_representObj);
                studentDoc.setStudent(student);
                syncPreCommitData(studentDoc, student);
                _studentDocList.add(studentDoc);
            }
        }

        DORepresentBaseManager.instance().modifyDao().update(_representObj, _studentDocList, _representBasicsList, _representGrantsList, representDocumentList, representIdentityCardList);
        deactivate(2);
    }

    public void onDelete()
    {

    }

    //для печати можно перекрывать этот метод
    protected void syncPreCommitData(DocRepresentStudentBase studentDoc, Student student) {
        student = UniDaoFacade.getCoreDao().get(student.getId());

        studentDoc.setStudentTitleStr(student.getPerson().getFullFio());
        studentDoc.setStudentTitleGenetiveStr(PersonManager.instance().declinationDao().getDeclinationFIO(student.getPerson().getIdentityCard(), GrammaCase.GENITIVE));
        studentDoc.setStudentStatusStr(student.getStatus().getTitle());
        studentDoc.setPersonalNumberStr(student.getPersonalNumber());
        studentDoc.setCourseStr(student.getCourse().getTitle());
        studentDoc.setGroupStr(student.getGroup() == null ? "" : student.getGroup().getTitle());

        if ("1".equals(student.getCompensationType().getCode()))
            studentDoc.setCompensationTypeStr("за счет средств федерального бюджета");
        else
            studentDoc.setCompensationTypeStr("с полным возмещением затрат");

        studentDoc.setFormativeOrgUnitStr(student.getEducationOrgUnit().getFormativeOrgUnit().getTitle());
        studentDoc.setFormativeOrgUnitGenetiveStr(student.getEducationOrgUnit().getFormativeOrgUnit().getGenitiveCaseTitle());

        OrgUnit formativeOrgUnit = student.getEducationOrgUnit().getFormativeOrgUnit();
        EmployeePost head = EmployeeManager.instance().dao().getHead(formativeOrgUnit);

/*        List<EmployeePost> list  = EmployeeManager.instance().dao().getHeaderEmployeePostList(
        	formativeOrgUnit
        );*/
        if (head == null)
            studentDoc.setHeadTitleStr("-");
        else
            studentDoc.setHeadTitleStr(head.getFio());


        studentDoc.setTerritorialOrgUnitStr(student.getEducationOrgUnit().getTerritorialOrgUnit().getTerritorialShortTitle());

        studentDoc.setEducationLevelHighSchoolStr(student.getEducationOrgUnit().getEducationLevelHighSchool().getDisplayableTitle());
        studentDoc.setEducationLevelHighSchoolTypeStr(student.getEducationOrgUnit().getEducationLevelHighSchool().getDisplayableTitle());

        studentDoc.setDevelopFormStr(student.getEducationOrgUnit().getDevelopForm().getTitle().toLowerCase());
        studentDoc.setDevelopConditionStr(student.getEducationOrgUnit().getDevelopCondition().getTitle());
        studentDoc.setDevelopTechStr(student.getEducationOrgUnit().getDevelopTech().getTitle());
        studentDoc.setDevelopPeriodStr(student.getEducationOrgUnit().getDevelopPeriod().getTitle());
    }

//    Getters

    public String getStudent() {
        if (_student == null)
            return null;

        IDOObjectByRepresentTypeManager representManager = DORepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(_type.getCode());
        IdentityCard identityCard = _student.getPerson().getIdentityCard();
        GrammaCase rusCase = representManager.getDOObjectModifyDAO().getGrammaCase();
        boolean isMaleSex = identityCard.getSex().isMale();

        StringBuilder str = new StringBuilder(PersonManager.instance().declinationDao().getDeclinationLastName(identityCard.getLastName(), rusCase, isMaleSex).toUpperCase());
        str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationFirstName(identityCard.getFirstName(), rusCase, isMaleSex));
        if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
            str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationMiddleName(identityCard.getMiddleName(), rusCase, isMaleSex));

        return str.toString();
    }

    public Map<String, Object> getBlockParam() {

        Map<String, Object> params = new Hashtable<>();
        params.put(DORepresentBaseEditBasicUI.REPRESENT_TYPE, _type);
        params.put(DORepresentBaseEditBasicUI.REPRESENT_BASIC_LIST, _representBasicsList);
        params.put(DORepresentBaseEditBasicUI.REPRESENT, _representObj);
        if (_representObj.getReason() != null)
            params.put(DORepresentBaseEditBasicUI.REASON, _representObj.getReason());
        return params;
    }

    public void onChangeReason() {
        DORepresentBaseEditBasicUI basicEdit = (DORepresentBaseEditBasicUI) _uiSupport.getChildrenUI().get(EDIT_BASIC_BLOCK_LIST + "_" + EDIT_BASIC_BLOCK);
        basicEdit.setReason(_representObj.getReason());
        basicEdit.onChangeReason();

        fillOSSPList();
    }

    /*
    public void onChangeGrants() {
        DORepresentBaseEditBasicUI basicEdit = (DORepresentBaseEditBasicUI) _uiSupport.getChildrenUI().get(EDIT_BASIC_BLOCK_LIST + "_" + EDIT_BASIC_BLOCK);
        basicEdit.setReason(_representObj.getReason());
        basicEdit.onChangeReason();
    }
     */

    public void onChangeGrants() {

        fillGrantsAdditionalList();
    }

    public void onChangeStudentDocuments() {

        fillStudentDocumentsAdditionalList();
    }

    private void clearRepresentGrantsList() {

        if (!_representGrantsList.isEmpty()) {

            for (Iterator<DocRepresentGrants> representGrantsIter = _representGrantsList.iterator(); representGrantsIter.hasNext(); ) {
                DocRepresentGrants representGrants = representGrantsIter.next();

                if (_oldReason.equals(representGrants.getReason()) || _representObj.getReason().equals(representGrants.getReason())) {
                    representGrantsIter.remove();
                }
            }
        }
    }

    private void clearRepresentStudentDocumentsList() {

        if (!_representStudentDocumentsList.isEmpty()) {

            for (Iterator<DocRepresentStudentDocuments> representStudentDocumentsIter = _representStudentDocumentsList.iterator(); representStudentDocumentsIter.hasNext(); ) {
                DocRepresentStudentDocuments representStudentDocuments = representStudentDocumentsIter.next();

                if (_oldReason.equals(representStudentDocuments.getReason()) || _representObj.getReason().equals(representStudentDocuments.getReason())) {
                    representStudentDocumentsIter.remove();
                }
            }
        }
    }

    //    Заполнение списка для дополнительных полей стипендий
    private void fillGrantsAdditionalList() {

        List<OsspGrants> grantsList = new ArrayList<>();
        List<DocRepresentGrants> representGrantsList = new ArrayList<>();
        grantsList.addAll(_grantNoRequiredListSelected);

        for (OsspGrants grant : grantsList) {
            Boolean isFind = false;
            for (DocRepresentGrants representGrants : _representGrantsList) {
                if (grant.equals(representGrants.getOsspGrants()) && _representObj.getReason().equals(representGrants.getReason())) {
                    isFind = true;
                    representGrantsList.add(representGrants);
                    break;
                }
            }
            if (!isFind) {
                DocRepresentGrants representGrants = new DocRepresentGrants();
                representGrants.setRepresentation(_representObj);
                representGrants.setOsspGrants(grant);
                representGrants.setReason(_representObj.getReason());
                representGrantsList.add(representGrants);
            }
        }

        clearRepresentGrantsList();
        _representGrantsList.addAll(representGrantsList);
    }

    public void onClickAddDocument() {
        ListenerWrapper<NarfuDocument> listener = new ListenerWrapper<>(this);
        Activator activator = new ComponentActivator(
                "ru.tandemservice.movestudentrmc.component.represent.SelectStudentDocument",
                new ParametersMap().add("studentId", _student.getId()).add("owner", _student).add("listener", listener)
        );
        ContextLocal.createDesktop("PersonShellDialog", activator);
    }

    public void onClickAddIdentityCard() {
        if (representIdentityCardList.size() > 0) {
            ContextLocal.getInfoCollector().add("Нельзя добавить в представление более одного Удостоверения Личности.");
        }
        else {
            Activator activator = new ComponentActivator(
                    "org.tandemframework.shared.person.base.bo.Person.ui.IdentityCardAddEdit",
                    new ParametersMap().add("publisherId", _student.getPerson().getId()).add("represent", true).add("list", representIdentityCardList)
            );

            UserContext.getInstance().activateDesktop("PersonShellDialog", activator);
            representIdentityCardDataSource.refresh();
        }
    }

    @Override
    public void listen(ListenerWrapper<NarfuDocument> wrapper) {
        if (wrapper.isOk()) {
            NarfuDocument doc = wrapper.getResult();
            DocRepresentStudentDocuments rel;
            if (wrapper.getParam() == null) {
                rel = new DocRepresentStudentDocuments();
                rel.setId(new Long(-1 - representDocumentList.size()));
                representDocumentList.add(rel);
            }
            else
                rel = (DocRepresentStudentDocuments) wrapper.getParam();

            rel.setStudentDocuments(doc);
        }
    }

    public void onClickEditDocument() {
        Long id = getListenerParameterAsLong();
        DocRepresentStudentDocuments rel = representDocumentDataSource.getRecordById(id);
        if (!rel.getStudentDocuments().isTemporary())
            return;

        ListenerWrapper<NarfuDocument> listener = new ListenerWrapper<>(this);
        listener.setParam(rel);
        Activator activator = new ComponentActivator(
                "ru.tandemservice.movestudentrmc.component.student.DocumentAddEdit",
                new ParametersMap().add("studentId", _student.getId()).add("listener", listener).add("documentId", rel.getStudentDocuments().getId())
        );
        ContextLocal.createDesktop("PersonShellDialog", activator);
    }

    public void onClickDeleteDocument() {
        Long id = getListenerParameterAsLong();
        DocRepresentStudentDocuments rel = representDocumentDataSource.getRecordById(id);
        representDocumentList.remove(rel);
    }

    public void onClickEditIC() {
        Long id = getListenerParameterAsLong();
        DocRepresentStudentIC rel = representIdentityCardDataSource.getRecordById(id);
        Activator activator = new ComponentActivator(
                "org.tandemframework.shared.person.base.bo.Person.ui.IdentityCardAddEdit",
                new ParametersMap().add("identityCardId", rel.getStudentIC().getId())
                        .add("publisherId", _student.getPerson().getId())
                        .add("birthDateCheck", Boolean.TRUE)
                        .add("represent", true).add("list", representIdentityCardList)
        );
        UserContext.getInstance().activateDesktop("PersonShellDialog", activator);
    }

    public void onClickDeleteIC() {
        Long id = getListenerParameterAsLong();
        DocRepresentStudentIC rel = representIdentityCardDataSource.getRecordById(id);
        PersonManager.instance().dao().deleteIdentityCard(rel.getStudentIC().getId());
        representIdentityCardList.remove(rel);
        representIdentityCardDataSource.refresh();
    }

    private void fillStudentDocumentsAdditionalList() {

        List<NarfuDocument> studentDocumentsList = new ArrayList<>();
        List<DocRepresentStudentDocuments> representStudentDocumentsList = new ArrayList<>();
        studentDocumentsList.addAll(_studentDocumentsNoRequiredListSelected);

        for (NarfuDocument studentDocument : studentDocumentsList) {
            Boolean isFind = false;
            for (DocRepresentStudentDocuments representStudentDocuments : _representStudentDocumentsList) {
                if (studentDocument.equals(representStudentDocuments.getStudentDocuments()) && _representObj.getReason().equals(representStudentDocuments.getReason())) {
                    isFind = true;
                    representStudentDocumentsList.add(representStudentDocuments);
                    break;
                }
            }
            if (!isFind) {
                DocRepresentStudentDocuments representStudentDocuments = new DocRepresentStudentDocuments();
                representStudentDocuments.setRepresentation(_representObj);
                representStudentDocuments.setStudentDocuments(studentDocument);
                representStudentDocuments.setReason(_representObj.getReason());
                representStudentDocumentsList.add(representStudentDocuments);
            }
        }

        clearRepresentStudentDocumentsList();
        _representStudentDocumentsList.addAll(representStudentDocumentsList);
    }

    public Boolean getIsReasonNull() {

        return _representObj.getReason() == null;
    }

    public byte[] getBytesFromFile() throws IOException {
        InputStream is = getUploadFile().getStream();
        File file = new File(getUploadFile().getFilePath());
        long length = getUploadFile().getSize();
        if (length > Integer.MAX_VALUE) {
            return null;
        }

        byte[] bytes = new byte[(int) length];

        // Read in the bytes
        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }

        // Ensure all the bytes have been read in
        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " + file.getName());
        }

        // Close the input stream and return bytes
        is.close();
        return bytes;
    }


    //    Если список стипендий пришел не пустой
    private void grantsNotEmpty(Representation doc) {

        List<DocRepresentGrants> representGrantsList = new ArrayList<>();
        if (getRepresentGrantsList() != null) {
            representGrantsList.addAll(getRepresentGrantsList());
            fillOSSPList();
            _representGrantsList.clear();
            _representGrantsList.addAll(representGrantsList);

            for (DocRepresentGrants representGrants : representGrantsList) {

                if (_grantNoRequiredList.contains(representGrants.getOsspGrants())) {

                    _grantNoRequiredListSelected.add(representGrants.getOsspGrants());

                }
            }
        }
        else {
            fillOSSPList();
        }
    }

    //    Если список документов пришел не пустой
    private void studentDocumentsNotEmpty(Representation doc) {

        List<DocRepresentStudentDocuments> representStudentDocumentsList = new ArrayList<>();
        if (getRepresentStudentDocumentsList() != null) {
            representStudentDocumentsList.addAll(getRepresentStudentDocumentsList());
            _representStudentDocumentsList.clear();
            _representStudentDocumentsList.addAll(representStudentDocumentsList);

            for (DocRepresentStudentDocuments representStudentDocuments : representStudentDocumentsList) {

                if (_studentDocumentsNoRequiredList.contains(representStudentDocuments.getStudentDocuments())) {

                    _studentDocumentsNoRequiredListSelected.add(representStudentDocuments.getStudentDocuments());

                }
            }
        }
        else {
            fillOSSPList();
        }
    }


    public Boolean getDoShowGrantsAdditionalOrSize() {

        return (getDoShowGrantsAdditional() || getDoShowGrantsSize());
    }

    public Boolean getDoShowGrantsAdditional() {

        return (_representGrantRow.getOsspGrants().isReqPeriod());
        //return (_representObj.getOsspGrants().isReqPeriod());
    }

    public Boolean getDoShowGrantsSize() {

        return (_representGrantRow.getOsspGrants().isReqSize());
        //return (_representObj.getOsspGrants().isReqSize());
    }

    public Boolean getOneGrants() {

        return _grantsOSSPList.size() == 1;
    }

    public Boolean getNotEmptyGrants() {

        return !_grantsOSSPList.isEmpty();
    }

    public Boolean getOnePgo() {

        return _pgoOSSPList.size() == 1;
    }

    public Boolean getNotEmptyPgo() {

        return !_pgoOSSPList.isEmpty();
    }

    public Boolean getEditBasementVal() {


        return null;
    }

//    Getters and Setters field


    public Boolean getNotGrants() {
        Boolean isFind = true;
        for (DocRepresentGrants representGrants : _representGrantsList) {
            if (representGrants.getReason().equals(_representObj.getReason())) {
                isFind = false;
            }
        }
        return isFind;
    }

    public List<Student> getStudentList() {
        return _studentList;
    }

    public void setStudentList(List<Student> studentList) {
        _studentList = studentList;
    }

    public List<Long> getStudentListId() {
        return _studentListId;
    }

    public void setStudentListId(List<Long> studentListId) {
        _studentListId = studentListId;
    }

    public List<DevelopCondition> getDevelopConditionList() {
        return _developConditionList;
    }

    public void setDevelopConditionList(List<DevelopCondition> developConditionList) {
        _developConditionList = developConditionList;
    }

    public List<OsspGrants> getGrantsOSSPList() {
        return _grantsOSSPList;
    }

    public void setGrantsOSSPList(List<OsspGrants> grantsOSSPList) {
        _grantsOSSPList = grantsOSSPList;
    }

    public List<OsspPgo> getPgoOSSPList() {
        return _pgoOSSPList;
    }

    public void setPgoOSSPList(List<OsspPgo> pgoOSSPList) {
        _pgoOSSPList = pgoOSSPList;
    }

    public List<OsspGrantsView> getGrantsOSSPViewList() {
        return _grantsOSSPViewList;
    }

    public void setGrantsOSSPViewList(List<OsspGrantsView> grantsOSSPViewList) {
        _grantsOSSPViewList = grantsOSSPViewList;
    }

    public List<DocRepresentBasics> getRepresentBasicsList() {
        return _representBasicsList;
    }

    public void setRepresentBasicsList(List<DocRepresentBasics> representBasicsList) {
        _representBasicsList = representBasicsList;
    }

    public List<DocRepresentGrants> getRepresentGrantsList() {
        return _representGrantsList;
    }

    public void setRepresentGrantsList(List<DocRepresentGrants> representGrantsList) {
        _representGrantsList = representGrantsList;
    }

    public List<DocRepresentStudentDocuments> getRepresentStudentDocumentsList() {
        return _representStudentDocumentsList;
    }

    public void setRepresentStudentDocumentsList(List<DocRepresentStudentDocuments> representStudentDocumentsList) {
        _representStudentDocumentsList = representStudentDocumentsList;
    }

    public Long getTypeId() {
        return _typeId;
    }

    public void setTypeId(Long typeId) {
        _typeId = typeId;
    }

    public RepresentationType getType() {
        return _type;
    }

    public void setType(RepresentationType type) {
        _type = type;
    }

    public Long getRepresentId() {
        return _representId;
    }

    public void setRepresentId(Long representId) {
        this._representId = representId;
    }

    public Representation getRepresentObj() {
        return _representObj;
    }

    public void setRepresentObj(Representation representObj) {
        this._representObj = representObj;
    }

    public DocRepresentGrants getRepresentGrantRow() {
        return _representGrantRow;
    }

    public void setRepresentGrantRow(DocRepresentGrants representGrantRow) {
        _representGrantRow = representGrantRow;
    }

    public List<OsspGrants> getGrantNoRequiredList() {
        return _grantNoRequiredList;
    }

    public void setGrantNoRequiredList(List<OsspGrants> grantNoRequiredList) {
        _grantNoRequiredList = grantNoRequiredList;
    }

    public List<OsspGrants> getGrantNoRequiredListSelected() {
        return _grantNoRequiredListSelected;
    }

    public void setGrantNoRequiredListSelected(List<OsspGrants> grantNoRequiredListSelected) {
        _grantNoRequiredListSelected = grantNoRequiredListSelected;
    }

    public List<NarfuDocument> getStudentDocumentsNoRequiredList() {
        return _studentDocumentsNoRequiredList;
    }

    public void setStudentDocumentsNoRequiredList(List<NarfuDocument> studentDocumentsNoRequiredList) {
        _studentDocumentsNoRequiredList = studentDocumentsNoRequiredList;
    }

    public List<NarfuDocument> getStudentDocumentsNoRequiredListSelected() {
        return _studentDocumentsNoRequiredListSelected;
    }

    public void setStudentDocumentsNoRequiredListSelected(List<NarfuDocument> studentDocumentsNoRequiredListSelected) {
        _studentDocumentsNoRequiredListSelected = studentDocumentsNoRequiredListSelected;
    }

    public String getStudentStatusNewStr() {
        return _studentStatusNewStr;
    }

    public void setStudentStatusNewStr(String studentStatusNewStr) {
        _studentStatusNewStr = studentStatusNewStr;
    }

    public byte[] getAdditionalDocFile() {
        return _additionalDocFile;
    }

    public void setAdditionalDocFile(byte[] additionalDocFile) {
        _additionalDocFile = additionalDocFile;
    }

    public IUploadFile getUploadFile() {
        return _uploadFile;
    }

    public void setUploadFile(IUploadFile uploadFile) {
        _uploadFile = uploadFile;
    }

    public DocumentType getDocumentType() {
        return _documentType;
    }

    public void setDocumentType(DocumentType documentType) {
        _documentType = documentType;
    }

    public DocumentKind getDocumentKind() {
        return _documentKind;
    }

    public void setDocumentKind(DocumentKind documentKind) {
        _documentKind = documentKind;
    }

    public Document getDocument() {
        return _document;
    }

    public void setDocument(Document document) {
        _document = document;
    }

    public DynamicListDataSource<DocRepresentStudentDocuments> getRepresentDocumentDataSource() {
        return representDocumentDataSource;
    }

    public void setRepresentDocumentDataSource(DynamicListDataSource<DocRepresentStudentDocuments> representDocumentDataSource) {
        this.representDocumentDataSource = representDocumentDataSource;
    }

    public DynamicListDataSource<DocRepresentStudentIC> getRepresentIdentityCardDataSource() {
        return representIdentityCardDataSource;
    }

    public void setRepresentIdentityCardDataSource(DynamicListDataSource<DocRepresentStudentIC> representIdentityCardDataSource) {
        this.representIdentityCardDataSource = representIdentityCardDataSource;
    }

    public List<DocRepresentStudentIC> getRepresentIdentityCardList() {
        return representIdentityCardList;
    }

    public void setRepresentIdentityCardListList(List<DocRepresentStudentIC> representIdentityCardList) {
        this.representIdentityCardList = representIdentityCardList;
    }

    public boolean isOneIc() {
        return representIdentityCardList.size() > 0;
    }

    public boolean isWarning() {
        return warning;
    }

    public void setWarning(boolean warning) {
        this.warning = warning;
    }

    public boolean isSave() {
        return save;
    }

    public void setSave(boolean save) {
        this.save = save;
    }

    public String getWarningMessage() {
        return warningMessage;
    }

    public void setWarningMessage(String warningMessage) {
        this.warningMessage = warningMessage;
    }
}
