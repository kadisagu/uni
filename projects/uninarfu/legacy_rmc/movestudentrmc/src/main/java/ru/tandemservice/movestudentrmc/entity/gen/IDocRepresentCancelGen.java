package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.DocumentOrder;
import ru.tandemservice.movestudentrmc.entity.IDocRepresentCancel;
import ru.tandemservice.movestudentrmc.entity.Representation;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import ru.tandemservice.movestudentrmc.entity.IDocRepresentCancel;

@SuppressWarnings({"all", "unchecked", "unused"})
public abstract class IDocRepresentCancelGen extends InterfaceStubBase
 implements IDocRepresentCancel{
    public static final int VERSION_HASH = -98779545;

    public static final String L_CANCEL_ORDER = "cancelOrder";
    public static final String L_CANCEL_REPRESENT = "cancelRepresent";

    private DocumentOrder _cancelOrder;
    private Representation _cancelRepresent;

    @NotNull

    public DocumentOrder getCancelOrder()
    {
        return _cancelOrder;
    }

    public void setCancelOrder(DocumentOrder cancelOrder)
    {
        _cancelOrder = cancelOrder;
    }

    @NotNull

    public Representation getCancelRepresent()
    {
        return _cancelRepresent;
    }

    public void setCancelRepresent(Representation cancelRepresent)
    {
        _cancelRepresent = cancelRepresent;
    }

    private static final Path<IDocRepresentCancel> _dslPath = new Path<IDocRepresentCancel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ru.tandemservice.movestudentrmc.entity.IDocRepresentCancel");
    }
            

    /**
     * @return Приказ. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.IDocRepresentCancel#getCancelOrder()
     */
    public static DocumentOrder.Path<DocumentOrder> cancelOrder()
    {
        return _dslPath.cancelOrder();
    }

    /**
     * @return Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.IDocRepresentCancel#getCancelRepresent()
     */
    public static Representation.Path<Representation> cancelRepresent()
    {
        return _dslPath.cancelRepresent();
    }

    public static class Path<E extends IDocRepresentCancel> extends EntityPath<E>
    {
        private DocumentOrder.Path<DocumentOrder> _cancelOrder;
        private Representation.Path<Representation> _cancelRepresent;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приказ. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.IDocRepresentCancel#getCancelOrder()
     */
        public DocumentOrder.Path<DocumentOrder> cancelOrder()
        {
            if(_cancelOrder == null )
                _cancelOrder = new DocumentOrder.Path<DocumentOrder>(L_CANCEL_ORDER, this);
            return _cancelOrder;
        }

    /**
     * @return Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.IDocRepresentCancel#getCancelRepresent()
     */
        public Representation.Path<Representation> cancelRepresent()
        {
            if(_cancelRepresent == null )
                _cancelRepresent = new Representation.Path<Representation>(L_CANCEL_REPRESENT, this);
            return _cancelRepresent;
        }

        public Class getEntityClass()
        {
            return IDocRepresentCancel.class;
        }

        public String getEntityName()
        {
            return "ru.tandemservice.movestudentrmc.entity.IDocRepresentCancel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
