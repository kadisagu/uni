package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class FormativeOrgUnitDSHandler extends ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.FormativeOrgUnitDSHandler
{

    public FormativeOrgUnitDSHandler(String ownerId) {

        super(ownerId);
    }


    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        super.prepareConditions(ep);
/*    	Arrays.asList(OrgUnitTypeCodes.INSTITUTE,OrgUnitTypeCodes.BRANCH,OrgUnitTypeCodes.)
        String filter = ep.input.getComboFilterByValue();
        ep.dqlBuilder.where(
        		DQLExpressions.or(
        		eq(property(OrgUnit.orgUnitType().code().fromAlias("e")), value(OrgUnitTypeCodes.INSTITUTE)),
        		eq(property(OrgUnit.orgUnitType().code().fromAlias("e")), value(OrgUnitTypeCodes.BRANCH)),
        		eq(property(OrgUnit.orgUnitType().code().fromAlias("e")), value("institute"))));
        
        */

        String filter = ep.input.getComboFilterByValue();
        if (StringUtils.isNotBlank(filter)) {
            ep.dqlBuilder.where(like(DQLFunctions.upper(property(OrgUnit.title().fromAlias("e"))), value(CoreStringUtils.escapeLike(filter, true))));
        }

    }


    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
    {
        ep.dqlBuilder.order("e." + OrgUnit.title());
    }
}
