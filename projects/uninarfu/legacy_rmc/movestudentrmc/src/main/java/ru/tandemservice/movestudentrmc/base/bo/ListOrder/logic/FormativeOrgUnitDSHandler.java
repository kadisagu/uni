package ru.tandemservice.movestudentrmc.base.bo.ListOrder.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.shared.organization.base.entity.OrgUnit;


public class FormativeOrgUnitDSHandler extends ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.FormativeOrgUnitDSHandler
{

    public FormativeOrgUnitDSHandler(String ownerId) {

        super(ownerId);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        String filter = ep.input.getComboFilterByValue();
        super.prepareConditions(ep);
        if (StringUtils.isNotBlank(filter)) {
            ep.dqlBuilder.where(DQLExpressions.like(
                    DQLFunctions.upper(DQLExpressions.property(OrgUnit.title().fromAlias("e"))),
                    DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));
        }
    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
    {
        ep.dqlBuilder.order("e." + OrgUnit.title());
    }
}
