package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudentrmc.entity.IStudentGrant;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListRepresentGrantCancel;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Списочное представление об отмене стиендии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ListRepresentGrantCancelGen extends ListRepresent
 implements IStudentGrant{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.ListRepresentGrantCancel";
    public static final String ENTITY_NAME = "listRepresentGrantCancel";
    public static final int VERSION_HASH = 2076735831;
    private static IEntityMeta ENTITY_META;

    public static final String P_BEGIN_GRANT_DATE = "beginGrantDate";
    public static final String P_END_GRANT_DATE = "endGrantDate";
    public static final String P_DATE_CANCEL_PAYMENT = "dateCancelPayment";
    public static final String L_GRANT_VIEW = "grantView";

    private Date _beginGrantDate;     // Дата назначения выплат
    private Date _endGrantDate;     // Дата окончания выплат
    private Date _dateCancelPayment;     // Месяц отмены выплаты
    private GrantView _grantView;     // Вид стипендии

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата назначения выплат. Свойство не может быть null.
     *
     * Это формула "dateCancelPayment".
     */
    // @NotNull
    public Date getBeginGrantDate()
    {
        return _beginGrantDate;
    }

    /**
     * @param beginGrantDate Дата назначения выплат. Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setBeginGrantDate(Date beginGrantDate)
    {
        dirty(_beginGrantDate, beginGrantDate);
        _beginGrantDate = beginGrantDate;
    }

    /**
     * @return Дата окончания выплат.
     *
     * Это формула "null".
     */
    public Date getEndGrantDate()
    {
        return _endGrantDate;
    }

    /**
     * @param endGrantDate Дата окончания выплат.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setEndGrantDate(Date endGrantDate)
    {
        dirty(_endGrantDate, endGrantDate);
        _endGrantDate = endGrantDate;
    }

    /**
     * @return Месяц отмены выплаты. Свойство не может быть null.
     */
    @NotNull
    public Date getDateCancelPayment()
    {
        return _dateCancelPayment;
    }

    /**
     * @param dateCancelPayment Месяц отмены выплаты. Свойство не может быть null.
     */
    public void setDateCancelPayment(Date dateCancelPayment)
    {
        dirty(_dateCancelPayment, dateCancelPayment);
        _dateCancelPayment = dateCancelPayment;
    }

    /**
     * @return Вид стипендии. Свойство не может быть null.
     */
    @NotNull
    public GrantView getGrantView()
    {
        return _grantView;
    }

    /**
     * @param grantView Вид стипендии. Свойство не может быть null.
     */
    public void setGrantView(GrantView grantView)
    {
        dirty(_grantView, grantView);
        _grantView = grantView;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ListRepresentGrantCancelGen)
        {
            setBeginGrantDate(((ListRepresentGrantCancel)another).getBeginGrantDate());
            setEndGrantDate(((ListRepresentGrantCancel)another).getEndGrantDate());
            setDateCancelPayment(((ListRepresentGrantCancel)another).getDateCancelPayment());
            setGrantView(((ListRepresentGrantCancel)another).getGrantView());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ListRepresentGrantCancelGen> extends ListRepresent.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ListRepresentGrantCancel.class;
        }

        public T newInstance()
        {
            return (T) new ListRepresentGrantCancel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "beginGrantDate":
                    return obj.getBeginGrantDate();
                case "endGrantDate":
                    return obj.getEndGrantDate();
                case "dateCancelPayment":
                    return obj.getDateCancelPayment();
                case "grantView":
                    return obj.getGrantView();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "beginGrantDate":
                    obj.setBeginGrantDate((Date) value);
                    return;
                case "endGrantDate":
                    obj.setEndGrantDate((Date) value);
                    return;
                case "dateCancelPayment":
                    obj.setDateCancelPayment((Date) value);
                    return;
                case "grantView":
                    obj.setGrantView((GrantView) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "beginGrantDate":
                        return true;
                case "endGrantDate":
                        return true;
                case "dateCancelPayment":
                        return true;
                case "grantView":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "beginGrantDate":
                    return true;
                case "endGrantDate":
                    return true;
                case "dateCancelPayment":
                    return true;
                case "grantView":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "beginGrantDate":
                    return Date.class;
                case "endGrantDate":
                    return Date.class;
                case "dateCancelPayment":
                    return Date.class;
                case "grantView":
                    return GrantView.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ListRepresentGrantCancel> _dslPath = new Path<ListRepresentGrantCancel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ListRepresentGrantCancel");
    }
            

    /**
     * @return Дата назначения выплат. Свойство не может быть null.
     *
     * Это формула "dateCancelPayment".
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentGrantCancel#getBeginGrantDate()
     */
    public static PropertyPath<Date> beginGrantDate()
    {
        return _dslPath.beginGrantDate();
    }

    /**
     * @return Дата окончания выплат.
     *
     * Это формула "null".
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentGrantCancel#getEndGrantDate()
     */
    public static PropertyPath<Date> endGrantDate()
    {
        return _dslPath.endGrantDate();
    }

    /**
     * @return Месяц отмены выплаты. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentGrantCancel#getDateCancelPayment()
     */
    public static PropertyPath<Date> dateCancelPayment()
    {
        return _dslPath.dateCancelPayment();
    }

    /**
     * @return Вид стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentGrantCancel#getGrantView()
     */
    public static GrantView.Path<GrantView> grantView()
    {
        return _dslPath.grantView();
    }

    public static class Path<E extends ListRepresentGrantCancel> extends ListRepresent.Path<E>
    {
        private PropertyPath<Date> _beginGrantDate;
        private PropertyPath<Date> _endGrantDate;
        private PropertyPath<Date> _dateCancelPayment;
        private GrantView.Path<GrantView> _grantView;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата назначения выплат. Свойство не может быть null.
     *
     * Это формула "dateCancelPayment".
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentGrantCancel#getBeginGrantDate()
     */
        public PropertyPath<Date> beginGrantDate()
        {
            if(_beginGrantDate == null )
                _beginGrantDate = new PropertyPath<Date>(ListRepresentGrantCancelGen.P_BEGIN_GRANT_DATE, this);
            return _beginGrantDate;
        }

    /**
     * @return Дата окончания выплат.
     *
     * Это формула "null".
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentGrantCancel#getEndGrantDate()
     */
        public PropertyPath<Date> endGrantDate()
        {
            if(_endGrantDate == null )
                _endGrantDate = new PropertyPath<Date>(ListRepresentGrantCancelGen.P_END_GRANT_DATE, this);
            return _endGrantDate;
        }

    /**
     * @return Месяц отмены выплаты. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentGrantCancel#getDateCancelPayment()
     */
        public PropertyPath<Date> dateCancelPayment()
        {
            if(_dateCancelPayment == null )
                _dateCancelPayment = new PropertyPath<Date>(ListRepresentGrantCancelGen.P_DATE_CANCEL_PAYMENT, this);
            return _dateCancelPayment;
        }

    /**
     * @return Вид стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentGrantCancel#getGrantView()
     */
        public GrantView.Path<GrantView> grantView()
        {
            if(_grantView == null )
                _grantView = new GrantView.Path<GrantView>(L_GRANT_VIEW, this);
            return _grantView;
        }

        public Class getEntityClass()
        {
            return ListRepresentGrantCancel.class;
        }

        public String getEntityName()
        {
            return "listRepresentGrantCancel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
