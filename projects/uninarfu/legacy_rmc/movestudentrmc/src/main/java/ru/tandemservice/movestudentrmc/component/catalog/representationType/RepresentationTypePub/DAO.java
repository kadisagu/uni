package ru.tandemservice.movestudentrmc.component.catalog.representationType.RepresentationTypePub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;

public class DAO extends DefaultCatalogPubDAO<RepresentationType, Model> implements IDAO {

    public void updateInUse(Model model, Long id) {
        RepresentationType representation = getNotNull(RepresentationType.class, id);
        if (representation.isUse()) {
            representation.setUse(false);
        }
        else {
            representation.setUse(true);
        }
        save(representation);
    }

    public void updateInstruction(Model model, Long id) {
        RepresentationType representation = getNotNull(RepresentationType.class, id);
        if (representation.isInstruction()) {
            representation.setInstruction(false);
        }
        else {
            representation.setInstruction(true);
        }
        save(representation);

    }

    public void updateGrouping(Model model, Long id) {
        RepresentationType representation = getNotNull(RepresentationType.class, id);
        if (representation.isGrouping()) {
            representation.setGrouping(false);
        }
        else {
            representation.setGrouping(true);
        }
        save(representation);
    }

    public void updateCheck(Model model, Long id) {
        RepresentationType representation = getNotNull(RepresentationType.class, id);
        if (representation.isCheck()) {
            representation.setCheck(false);
        }
        else {
            representation.setCheck(true);
        }
        save(representation);
    }

    @Override
    public void updateListRepresentation(Model model, Long id) {
        RepresentationType representation = getNotNull(RepresentationType.class, id);
        if (representation.isListRepresentation()) {
            representation.setListRepresentation(false);
            representation.setPrintAttachment(false);//!!!
        }
        else {
            representation.setListRepresentation(true);
        }
        save(representation);
    }

    @Override
    public void updatePrintAttachment(Model model, Long id) {
        RepresentationType representation = getNotNull(RepresentationType.class, id);
        if (!representation.isListRepresentation())
            return;

        representation.setPrintAttachment(!representation.isPrintAttachment());
        save(representation);
    }

    @Override
    public void updatePrintDeclaration(Model model, Long id) {
        RepresentationType representation = getNotNull(RepresentationType.class, id);

        representation.setPrintDeclaration(!representation.isPrintDeclaration());
        save(representation);
    }
    /*
	@Override
	public void updateUseGrant(Model model, Long id) {
		RepresentationType representation = getNotNull(RepresentationType.class, id);
		
		representation.setUseGrant(!representation.isUseGrant());
		save(representation);
	}
	*/
}
