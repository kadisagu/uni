package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.support;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ListRepresentBaseManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ui.Add.ListRepresentBaseAdd;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ui.List.ListRepresentBaseList;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrant.logic.ListPrintDoc;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrant.ui.Edit.ListRepresentGrantEditUI;
import ru.tandemservice.movestudentrmc.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudentrmc.docord.IListObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.uni.util.ReportRenderer;

import java.util.Collections;
import java.util.Map;

public abstract class AbstractListRepresentBaseListUI extends UIPresenter {

    @Override
    public void onComponentRefresh()
    {
        _uiSettings.getSettings();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {

        if (dataSource.getName().equals("listRepresentDS")) {

            Map<String, Object> settingMap = _uiSettings.getAsMap(
                    "dateFormativeFromFilter",
                    "dateFormativeToFilter",
                    "dateStartFromFilter",
                    "dateStartToFilter",
                    "typeRepresentFilter",
                    "stateViewFilter",
                    "formativeOrgUnitFilter",
                    "courseFilter",
                    "studentFilter",
                    "authorFilter",
                    "grantViewField"
            );
            dataSource.putAll(settingMap);
            dataSource.putAll(getParams());
        }
    }

    protected abstract Map<String, Object> getParams();

    @Override
    public void saveSettings() {
/*
                DateFormattingUtil.validateDatesPeriod(getSettings(), OrderDSHandler.DATE_FORMATIVE_FROM_FILTER, OrderDSHandler.DATE_FORMATIVE_TO_FILTER);
        DateFormattingUtil.validateDatesPeriod(getSettings(), OrderDSHandler.DATE_COMMIT_FROM_FILTER, OrderDSHandler.DATE_COMMIT_TO_FILTER);
        DateFormattingUtil.validateDatesPeriod(getSettings(), OrderDSHandler.DATE_START_FROM_FILTER, OrderDSHandler.DATE_START_TO_FILTER);
		 */
        super.saveSettings();
    }

    public void onClickAdd()
    {
        _uiActivation.asRegion(ListRepresentBaseAdd.class)
                //			.parameters(_uiSettings.getAsMap("typeRepresentFilter","reasonRepresentFilter","basementRepresentFilter"))
                .activate();

    }

    public void onEditEntityFromList()
    {
        ListRepresent listRepresent = getEntityByListenerParameterAsLong();
        IListObjectByRepresentTypeManager representManager = ListRepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(listRepresent.getRepresentationType().getCode());

        _uiActivation.asCurrent(representManager.getEditComponentManager())
                .parameter(ListRepresentGrantEditUI.LIST_REPRESENT_ID, getListenerParameterAsLong())
                .activate();
    }

    @Override
    public void onComponentDeactivate() {

		/*		if(true){

			getUserContext().getErrorCollector().add("Выберите Тип представления");
			getUserContext().getErrorCollector().add("Выберите Причину");
			getUserContext().getErrorCollector().add("Выберите Основание");
		}
		else*/
        super.onComponentDeactivate();
    }

    public void onClickPrintFromList() {

        try {
            IDocumentRenderer doc = documentRenderer();
            if (doc != null)
                BusinessComponentUtils.downloadDocument(doc, true);
        }
        catch (Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public void onDeleteEntityFromList() {
        Long representId = getListenerParameterAsLong();
        ListRepresent represent = DataAccessServices.dao().get(ListRepresent.class, representId);

        MoveStudentDaoFacade.getGrantDAO().deleteStudentGrants(represent);
        DataAccessServices.dao().delete(represent);

        //не понимаю что тут делается, поэтому оставляю
		/*
		DQLSelectBuilder builder = new DQLSelectBuilder();
		builder.fromEntity(ListOrdListRepresent.class, "r").addColumn("r");
		builder.where(DQLExpressions.eq(DQLExpressions.property(ListOrdListRepresent.order().id().fromAlias("r")), DQLExpressions.value(getListenerParameterAsLong())));
		List<ListOrdListRepresent> representSelectedOldList = builder.createStatement(getSupport().getSession()).list();

		for (ListOrdListRepresent representSelectedOld : representSelectedOldList){

			ListRepresent represent = representSelectedOld.getRepresentation();
			represent.setState(DataAccessServices.dao().get(MovestudentExtractStates.class, MovestudentExtractStates.code().s(), "3"));
			DataAccessServices.dao().save(represent);
		}
		DataAccessServices.dao().delete(getListenerParameterAsLong());
		*/
    }

    public Boolean getIsFormative() {

        BaseSearchListDataSource representDS = (BaseSearchListDataSource) getConfig().getDataSource(ListRepresentBaseList.LIST_REPRESENT_DS);

        IEntity e = representDS.getCurrentEntity();
        ListRepresent listRepresent = ((ListRepresent) e);
        return !listRepresent.getState().getCode().equals("1");
    }

    private IDocumentRenderer documentRenderer() {

        ErrorCollector error = getConfig().getUserContext().getErrorCollector();

        ListRepresent listRepresent = DataAccessServices.dao().get(getListenerParameterAsLong());


        final RtfDocument docMain = new ListPrintDoc().creationPrintDocOrder(Collections.singletonList(listRepresent));

        return new ReportRenderer("Списочное представление.rtf", docMain);
    }
}
