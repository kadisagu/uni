package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic;


import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import ru.tandemservice.movestudentrmc.entity.catalog.DocumentTypeForRepresent;

public class DocumentTypeDSHandler extends DefaultComboDataSourceHandler {

    public DocumentTypeDSHandler(String ownerId)
    {
        super(ownerId, DocumentTypeForRepresent.class, DocumentTypeForRepresent.P_TITLE);
    }
}
