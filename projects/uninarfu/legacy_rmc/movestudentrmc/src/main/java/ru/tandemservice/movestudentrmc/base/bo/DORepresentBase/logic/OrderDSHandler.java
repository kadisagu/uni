package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.entity.DocumentOrder;

public class OrderDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput> {

    public static final String ORDER_TITLE_COLUMN = "orderTitle";

    public OrderDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocumentOrder.class, "o").column(DQLExpressions.property("o"));

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).order(DocumentOrder.class, "o").build();
    }
}
