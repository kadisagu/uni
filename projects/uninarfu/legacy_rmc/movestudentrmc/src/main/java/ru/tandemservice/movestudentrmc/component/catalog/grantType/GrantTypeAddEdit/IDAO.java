package ru.tandemservice.movestudentrmc.component.catalog.grantType.GrantTypeAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantType;

public interface IDAO extends IDefaultCatalogAddEditDAO<GrantType, Model> {

}
