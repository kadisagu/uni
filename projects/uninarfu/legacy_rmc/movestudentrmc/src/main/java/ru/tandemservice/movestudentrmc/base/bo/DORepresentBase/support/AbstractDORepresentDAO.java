package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.caf.logic.dao.BaseModifyAggregateDAO;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.ITransferableRepresent;
import ru.tandemservice.movestudentrmc.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudentrmc.docord.IDOObjectModifyDAO;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.StuGrantStatus;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentGrantTypeCodes;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.StuGrantStatusCodes;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
//----------------------------------------------------------------
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;
//----------------------------------------------------------------

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class AbstractDORepresentDAO extends BaseModifyAggregateDAO<Representation> implements IDOObjectModifyDAO {

    @Override
    public void buildBodyDeclaration(Representation representationBase, RtfDocument document) {
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        RtfInjectModifier im = new RtfInjectModifier();
        RtfTableModifier tm = new RtfTableModifier();
        UtilPrintSupport.injectModifierDeclaration(im, tm, docRepresent);

        this.injectModifier(im, docRepresent);

        im.modify(document);
        tm.modify(document);
    }

    @Override
    public void buildBodyRepresent(Representation representationBase,
                                   RtfDocument document)
    {
        buildBodyRepresent(representationBase, document, null, null);
    }

    @Override
    public void buildBodyRepresent(Representation representationBase,
                                   RtfDocument document, AtomicInteger parNumber,
                                   AtomicInteger extractNumber)
    {

        RtfInjectModifier im = new RtfInjectModifier();
        if (parNumber != null && extractNumber != null) {
            StringBuilder str = new StringBuilder();
            str.append(parNumber.intValue())
                    .append(".")
                    .append(extractNumber.intValue())
                    .append(". ");

            im.put("parN", String.valueOf(parNumber.intValue()) + ". ");
            im.put("parNumber", str.toString());
            extractNumber.getAndAdd(1);
        }
        else {
            im.put("parN", "");
            im.put("parNumber", "");
        }
        im.modify(document);
    }

    @Override
    public boolean doCommit(Representation represent, ErrorCollector error) {
        List<DocRepresentStudentBase> studentList = getStudentList(represent);
        if (studentList.isEmpty()) {
            error.add("Представление содержит пустой список студентов!");
            return false;
        }

//		return doCommitGrants(represent, error);
        return true;
    }

    @Override
    public boolean doRollback(Representation represent, ErrorCollector error) {
        List<DocRepresentStudentBase> studentList = getStudentList(represent);
        if (studentList.isEmpty()) {
            error.add("Представление содержит пустой список студентов!");
            return false;
        }

        return doRollbackGrants(represent, error);
    }

    @Override
    public RtfDocument getTemplateExtract(ICatalogItem type) {

        return UtilPrintSupport.getTemplate(type, "extract");
    }

    @Override
    public RtfDocument getTemplateRepresent(ICatalogItem type) {

        return UtilPrintSupport.getTemplate(type, "represent");
    }

    @Override
    public RtfDocument getTemplateDeclaration(ICatalogItem type) {
        return UtilPrintSupport.getTemplate(type, "declaration");
    }

    @Override
    public GrammaCase getGrammaCase() {
        return GrammaCase.ACCUSATIVE;
    }

    protected List<DocRepresentStudentBase> getStudentList(Representation represent) {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocRepresentStudentBase.class, "stud").column("stud");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(DocRepresentStudentBase.representation().fromAlias("stud")),
                DQLExpressions.value(represent.getId())));

        return builder.createStatement(getSession()).list();
    }

    protected void injectModifier(RtfInjectModifier modifier, DocRepresentStudentBase docrepresent) {

    }

    //назначение новых УП-РУП
    protected void doCommit(Student student, ITransferableRepresent rep, Session session) {
        if (rep.getWorkPlan() == null)
            return;

        //RT:#2730, студенту ктото поставил этот же workPlan
        if (!check(student, rep.getWorkPlan()))
            return;

        Date commitDate = new Date();

        EppStudent2EduPlanVersion oldEduRel = null;
        if (rep.getOldEduPlanRelId() != null)
            oldEduRel = get(rep.getOldEduPlanRelId());

        EppStudent2EduPlanVersion newEduRel = null;
        EppStudent2WorkPlan newWorkRel = null;

        if (oldEduRel != null) {//есть старый УП
            if (oldEduRel.getEduPlanVersion().equals(rep.getWorkPlan().getEduPlanVersion())) {//старый и новый УП совпадают
                newEduRel = oldEduRel;
            }
            else {
                oldEduRel.setRemovalDate(commitDate); //деактивируем старый УП
                session.saveOrUpdate(oldEduRel);

                newEduRel = new EppStudent2EduPlanVersion(student, rep.getWorkPlan().getEduPlanVersion());
                session.saveOrUpdate(newEduRel);//создаем новую связь на УП
            }

            List<String> oldWorkPlanRelIds = new ArrayList<String>();
            if (rep.getOldWorkPlanRelIds() != null)
                oldWorkPlanRelIds.addAll(Arrays.asList(rep.getOldWorkPlanRelIds().split(",")));

            if (oldWorkPlanRelIds.contains(rep.getWorkPlan().getId().toString())) {//старый и новый РУП совпадают
                //ничего делать не надо
            }
            else {
                for (String oldId : oldWorkPlanRelIds) {
                    EppStudent2WorkPlan oldRel = get(Long.parseLong(oldId));
                    //если УП изменилось то РУП погасятся ранее: oldEduRel.setRemovalDate(commitDate)
                    if (oldEduRel.equals(newEduRel) && oldRel.getGridTerm().getTerm().equals(rep.getWorkPlan().getGridTerm().getTerm())) {// гасим старый РУП на этот период
                        oldRel.setRemovalDate(commitDate);
                        session.saveOrUpdate(oldRel);
                    }
                }
                newWorkRel = new EppStudent2WorkPlan(newEduRel, rep.getWorkPlan());
                session.saveOrUpdate(newWorkRel);

                rep.setWorkPlanRel(newWorkRel);
                session.saveOrUpdate(rep);
            }
        }
        else {//нет старого УП

            newEduRel = getEppStudent2EduPlanVersion(student, rep.getWorkPlan().getEduPlanVersion());
            session.saveOrUpdate(newEduRel);

            newWorkRel = getEppStudent2WorkPlan(newEduRel, rep.getWorkPlan());
            session.saveOrUpdate(newWorkRel);

            rep.setWorkPlanRel(newWorkRel);
            session.saveOrUpdate(rep);
        }
    }

    private EppStudent2WorkPlan getEppStudent2WorkPlan(EppStudent2EduPlanVersion eduRel, EppWorkPlan workPlan) {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppStudent2WorkPlan.class, "rel")
                .where(DQLExpressions.eqValue(DQLExpressions.property(EppStudent2WorkPlan.studentEduPlanVersion().fromAlias("rel")), eduRel))
                .where(DQLExpressions.eqValue(DQLExpressions.property(EppStudent2WorkPlan.workPlan().fromAlias("rel")), workPlan));

        EppStudent2WorkPlan workRel = builder.createStatement(getSession()).uniqueResult();

        if (workRel == null)
            workRel = new EppStudent2WorkPlan(eduRel, workPlan);
        else
            workRel.setRemovalDate(null);

        return workRel;
    }

    private EppStudent2EduPlanVersion getEppStudent2EduPlanVersion(Student student, EppEduPlanVersion eduPlanVersion) {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppStudent2EduPlanVersion.class, "rel")
                .where(DQLExpressions.eqValue(DQLExpressions.property(EppStudent2EduPlanVersion.student().id().fromAlias("rel")), student.getId()))
                .where(DQLExpressions.eqValue(DQLExpressions.property(EppStudent2EduPlanVersion.eduPlanVersion().id().fromAlias("rel")), eduPlanVersion.getId()));

        EppStudent2EduPlanVersion eduRel = builder.createStatement(getSession()).uniqueResult();

        if (eduRel == null)
            eduRel = new EppStudent2EduPlanVersion(student, eduPlanVersion);
        else
            eduRel.setRemovalDate(null);

        return eduRel;
    }

    //стоит ли на студенте данный EppWorkPlan
    private boolean check(Student student, EppWorkPlan plan) {
    	
    	//------------------------------------------------------------------------------
    	//Список версий выбранного РУП
        MQBuilder subquery = new MQBuilder(EppWorkPlanVersion.ENTITY_CLASS, "e")
                .add(MQExpression.eq("e", EppWorkPlanVersion.parent(), plan));
        //------------------------------------------------------------------------------
    	
        MQBuilder builder = new MQBuilder(EppStudent2WorkPlan.ENTITY_CLASS, "e")
                .add(MQExpression.eq("e", EppStudent2WorkPlan.studentEduPlanVersion().student(), student))
                //-------------------------------------------------------------------
                //.add(MQExpression.eq("e", EppStudent2WorkPlan.workPlan(), plan))
                .add(MQExpression.or(MQExpression.eq("e", EppStudent2WorkPlan.workPlan(), plan), MQExpression.in("e", EppStudent2WorkPlan.workPlan(), subquery)))
                //-------------------------------------------------------------------
                .add(MQExpression.or(
                        MQExpression.isNull("e", EppStudent2WorkPlan.removalDate()),
                        MQExpression.great("e", EppStudent2WorkPlan.removalDate(), new Date())
                ));

        return builder.getResultCount(getSession()) == 0;
    }

    //возврат старых УП-РУП
    protected void doRollback(Student student, ITransferableRepresent rep, Session session) {
        if (rep.getWorkPlanRel() == null)
            return;

        EppStudent2WorkPlan workRel = rep.getWorkPlanRel();
        EppStudent2EduPlanVersion eduRel = workRel.getStudentEduPlanVersion();

        List<String> oldWorkPlanRelIds = new ArrayList<String>();
        if (rep.getOldWorkPlanRelIds() != null)
            oldWorkPlanRelIds.addAll(Arrays.asList(rep.getOldWorkPlanRelIds().split(",")));

        if (!eduRel.getId().equals(rep.getOldEduPlanRelId())) {//старый и новый УП не совпадают
            session.delete(workRel);
            session.delete(eduRel);

            if (rep.getOldEduPlanRelId() != null) {
                eduRel = get(rep.getOldEduPlanRelId());
                eduRel.setRemovalDate(null);
                session.saveOrUpdate(eduRel);
            }
        }
        else {
            session.delete(workRel);
        }

        for (String oldId : oldWorkPlanRelIds) {
            workRel = get(Long.parseLong(oldId));
            workRel.setRemovalDate(null);
            session.saveOrUpdate(workRel);
        }

        rep.setWorkPlanRel(null);
        session.saveOrUpdate(rep);
    }

    /*
     protected boolean doCommitGrants(Representation representation, ErrorCollector errors) {
         if (representation.isSaveGrants())
             return true;

         DocRepresentStudentBase doc = this.getStudentList(representation).get(0);
         StuGrantStatus grantStatus = UniDaoFacade.getCoreDao().getCatalogItem(StuGrantStatus.class, StuGrantStatusCodes.CODE_3);

         MQBuilder builder = new MQBuilder(StudentGrantEntity.ENTITY_CLASS, "g")
             .add(MQExpression.eq("g", StudentGrantEntity.student(), doc.getStudent()))
             .add(MQExpression.notEq("g", StudentGrantEntity.tmp(), Boolean.TRUE))
             .add(MQExpression.eq("g", StudentGrantEntity.status().code(), StuGrantStatusCodes.CODE_1));

         List<StudentGrantEntity> studentGrants = builder.getResultList(this.getSession());
         List<Long> idList = new ArrayList<Long>();
         for (StudentGrantEntity grant : studentGrants) {
             grant.setStatus(grantStatus);
             getSession().saveOrUpdate(grant);

             idList.add(grant.getId());
         }

         String idsStr = StringUtils.join(idList, ",");
         representation.setGrantsOld(idsStr);
         getSession().saveOrUpdate(representation);

         return true;
     }
     */
    public DocumentOrder getOrder(Representation representation) {

        DocOrdRepresent docOrdRepresent = UniDaoFacade.getCoreDao().get(DocOrdRepresent.class, DocOrdRepresent.representation(), representation);

        if (docOrdRepresent != null) {
            return docOrdRepresent.getOrder();
        }
        else
            return null;
    }

    protected boolean doRollbackGrants(Representation representation, ErrorCollector errors) {
        if (representation.isSaveGrants() || StringUtils.isEmpty(representation.getGrantsOld()))
            return true;

        List<Long> idList = new ArrayList<Long>();
        for (String id : representation.getGrantsOld().split(","))
            idList.add(Long.parseLong(id));

        MQBuilder builder = new MQBuilder(StudentGrantEntity.ENTITY_CLASS, "g")
                .add(MQExpression.in("g", StudentGrantEntity.id(), idList));
        List<StudentGrantEntity> grantList = builder.getResultList(getSession());

        StuGrantStatus grantStatus = UniDaoFacade.getCoreDao().getCatalogItem(StuGrantStatus.class, StuGrantStatusCodes.CODE_1);

        for (StudentGrantEntity grant : grantList)
            if (!MoveStudentDaoFacade.getGrantDAO().canAddGrant(grant.getView(), grant.getStudent(), grant.getMonth())) {
                StringBuilder sb = new StringBuilder()
                        .append("Стипендия ")
                        .append(grant.getView().getTitle())
                        .append(" для студента ")
                        .append(PersonManager.instance().declinationDao().getDeclinationFIO(grant.getStudent().getPerson().getIdentityCard(), GrammaCase.GENITIVE))
                        .append(" за ")
                        .append(grant.getMonth())
                        .append(" не может быть восстановлена: ")
                        .append(" Запрещено по матрице стипендий.");
                errors.add(sb.toString());
                return false;
            }

        for (StudentGrantEntity grant : grantList) {
            grant.setStatus(grantStatus);
            getSession().saveOrUpdate(grant);
        }

        return true;
    }

    public void commitStudentGrantsCancel(Representation represent,
                                          List<StudentGrantEntity> sgeList)
    {

        StuGrantStatus status = IUniBaseDao.instance.get().get(StuGrantStatus.class, StuGrantStatus.code(), StuGrantStatusCodes.CODE_3);

        DocumentOrder order = represent.getOrder();

        for (StudentGrantEntity sge : sgeList) {
            //сохраним старые данные для отката
            RepresentGrantsOld grantsOld = new RepresentGrantsOld();
            grantsOld.setRepresentation(represent);
            grantsOld.setStudentGrantEntityId(sge.getId());
            grantsOld.setOrderDateOld(sge.getOrderDate());
            grantsOld.setOrderNumberOld(sge.getOrderNumber());
            getSession().saveOrUpdate(grantsOld);
            //сохраним данные приказа
            sge.setStatus(status);
            sge.setOrderDate(order.getCommitDate());
            sge.setOrderNumber(order.getNumber());
            getSession().saveOrUpdate(sge);
        }

        MoveStudentDaoFacade.getGrantDAO().saveGrantHistory(sgeList, represent, RepresentGrantTypeCodes.CANCEL);
    }

    public void rollbackStudentGrantsCancel(Representation represent) {

        StuGrantStatus status = IUniBaseDao.instance.get().get(StuGrantStatus.class, StuGrantStatus.code(), StuGrantStatusCodes.CODE_1);

        List<RepresentGrantsOld> grantsOlds = IUniBaseDao.instance.get().getList(RepresentGrantsOld.class, RepresentGrantsOld.representation(), represent);

        if (grantsOlds == null || grantsOlds.isEmpty())
            return;

        for (RepresentGrantsOld grantOld : grantsOlds) {
            StudentGrantEntity sge = get(grantOld.getStudentGrantEntityId());

            sge.setStatus(status);
            sge.setOrderDate(grantOld.getOrderDateOld());
            sge.setOrderNumber(grantOld.getOrderNumberOld());

            getSession().saveOrUpdate(sge);

            getSession().delete(grantOld);
        }

        MoveStudentDaoFacade.getGrantDAO().deleteGrantHistory(represent);
    }

}
