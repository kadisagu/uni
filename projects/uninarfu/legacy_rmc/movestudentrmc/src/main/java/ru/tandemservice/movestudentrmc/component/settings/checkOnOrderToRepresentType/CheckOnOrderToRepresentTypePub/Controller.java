package ru.tandemservice.movestudentrmc.component.settings.checkOnOrderToRepresentType.CheckOnOrderToRepresentTypePub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.UniUtils;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        model.setSettings(component.getSettings());
        ((IDAO) getDao()).prepare(model);

        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component) {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<Wrapper> dataSource = UniUtils.createDataSource(component, getDao());

        dataSource.addColumn(new SimpleColumn("Наименование", Wrapper.TITLE));
        dataSource.addColumn(new ToggleColumn("Использовать", Wrapper.USED).setListener("onClickToggleCheckUse"));
        dataSource.addColumn(new ToggleColumn("Значение", Wrapper.VALUE).setListener("onClickToggleCheckValue").setEnabledProperty(Wrapper.USED));

        model.setDataSource(dataSource);
    }

    public void onClickToggleCheckUse(IBusinessComponent component)
    {
        getDao().updateInUse(getModel(component), (Long) component.getListenerParameter());
    }

    public void onClickToggleCheckValue(IBusinessComponent component)
    {
        getDao().updateValue(getModel(component), (Long) component.getListenerParameter());
    }

    public void onClickSearch(final IBusinessComponent context)
    {
        final Model model = getModel(context);
        model.getDataSource().refresh();
        DataSettingsFacade.saveSettings(model.getSettings());
    }

    public void onClickClear(final IBusinessComponent context)
    {
        getModel(context).getSettings().clear();
        onClickSearch(context);
    }
}
