package ru.tandemservice.movestudentrmc.base.bo.ListRepresentTransfer.ui.Edit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.DevelopConditionDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.DevelopFormDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.DevelopPeriodDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.DevelopTechDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.support.AbstractListRepresentEditUI;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentTransfer.logic.EducationLevelsHighSchoolTransferDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentTransfer.logic.GroupTransferDSHandler;
import ru.tandemservice.movestudentrmc.dao.CheckStateUtil;
import ru.tandemservice.movestudentrmc.docord.util.MoveStudentUtil;
import ru.tandemservice.movestudentrmc.entity.ListRepresentTransfer;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentExtractStatesCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Arrays;
import java.util.Date;

@State({
        @Bind(key = AbstractListRepresentEditUI.LIST_REPRESENT_ID, binding = AbstractListRepresentEditUI.LIST_REPRESENT_ID),
        @Bind(key = "typeRepresentFilter", binding = "typeRepresentFilter"),
        @Bind(key = "reasonRepresentFilter", binding = "reasonRepresentFilter"),
        @Bind(key = "basementRepresentFilter", binding = "basementRepresentFilter"),
        @Bind(key = "representationBasementDate", binding = "representationBasementDate"),
        @Bind(key = "representationBasementNumber", binding = "representationBasementNumber"),
})

public class ListRepresentTransferEditUI extends AbstractListRepresentEditUI<ListRepresentTransfer> {

    private ISelectModel formSelectModel;
    private EducationOrgUnit eduOrgUnit = new EducationOrgUnit();
    private boolean save = false;
    private boolean showAllGroups = false;

    @Override
    public void onComponentRefresh() {
        super.onComponentRefresh();

        final ListRepresentTransfer rep = (ListRepresentTransfer) getListRepresent();

        if (rep.getEducationOrgUnit() != null) {
            getEduOrgUnit().update(rep.getEducationOrgUnit());
            if (rep.getGroupNew() != null)
                setShowAllGroups(!getEduOrgUnit().getEducationLevelHighSchool().equals(rep.getGroupNew().getEducationOrgUnit().getEducationLevelHighSchool()));
        }
        else {
            if (!this.getStudentSelectedList().isEmpty() && getEduOrgUnit().getFormativeOrgUnit() == null) {
                Student firstStudent = this.getStudentSelectedList().get(0);
                getEduOrgUnit().update(firstStudent.getEducationOrgUnit());
            }
        }

        if (this.getListRepresentId() == null)
            this.getListRepresent().setDateTransfer(new Date());
    }

    @Override
    public ListRepresentTransfer getListRepresentObject() {
        return new ListRepresentTransfer();
    }

    @Override
    public void selectRepresent(boolean check) {
        super.selectRepresent(check);

        if (getEduOrgUnit().getFormativeOrgUnit() == null && !getStudentSelectedList().isEmpty()) {
            Student firstStudent = this.getStudentSelectedList().get(0);
            getEduOrgUnit().update(firstStudent.getEducationOrgUnit());
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        super.onBeforeDataSourceFetch(dataSource);

        if (ListRepresentTransferEdit.GROUP_DS2.equals(dataSource.getName())) {
            dataSource.put(GroupTransferDSHandler.SHOW_ALL_GROUPS, isShowAllGroups());
            dataSource.put(GroupTransferDSHandler.NEW_FORMATIVE_ORG_UNIT, getEduOrgUnit().getFormativeOrgUnit());
            dataSource.put(GroupTransferDSHandler.NEW_TERRITORIAL_ORG_UNIT, getEduOrgUnit().getTerritorialOrgUnit());
            dataSource.put(GroupTransferDSHandler.EDUCATION_LEVELS_HIGH_SCHOOL, getEduOrgUnit().getEducationLevelHighSchool());
            dataSource.put(GroupTransferDSHandler.DEVELOP_FORM, getEduOrgUnit().getDevelopForm());
            dataSource.put(GroupTransferDSHandler.DEVELOP_CONDITION, getEduOrgUnit().getDevelopCondition());
            dataSource.put(GroupTransferDSHandler.DEVELOP_PERIOD, getEduOrgUnit().getDevelopPeriod());
            dataSource.put(GroupTransferDSHandler.DEVELOP_TECH, getEduOrgUnit().getDevelopTech());
            dataSource.put(GroupTransferDSHandler.COURSE, getListRepresentTransfer().getCourseNew());
        }

        if (ListRepresentTransferEdit.EDU_LEVEL_DS.equals(dataSource.getName())) {
            dataSource.put(EducationLevelsHighSchoolTransferDSHandler.FILTER_BY_DEVELOP, Boolean.FALSE);
            dataSource.put(EducationLevelsHighSchoolTransferDSHandler.NEW_TERRITORIAL_ORG_UNIT, getEduOrgUnit().getTerritorialOrgUnit());
            dataSource.put(EducationLevelsHighSchoolTransferDSHandler.NEW_FORMATIVE_ORG_UNIT, getEduOrgUnit().getFormativeOrgUnit());
        }

        if (ListRepresentTransferEdit.DEVELOP_FORM_DS2.equals(dataSource.getName())) {
            dataSource.put(DevelopFormDSHandler.NEW_FORMATIVE_ORG_UNIT, getEduOrgUnit().getFormativeOrgUnit());
            dataSource.put(DevelopFormDSHandler.NEW_TERRITORIAL_ORG_UNIT, getEduOrgUnit().getTerritorialOrgUnit());
            dataSource.put(DevelopFormDSHandler.EDUCATION_LEVELS_HIGH_SCHOOL, getEduOrgUnit().getEducationLevelHighSchool());
        }

        if (ListRepresentTransferEdit.DEVELOP_CONDITION_DS.equals(dataSource.getName())) {
            dataSource.put(DevelopConditionDSHandler.NEW_FORMATIVE_ORG_UNIT, getEduOrgUnit().getFormativeOrgUnit());
            dataSource.put(DevelopConditionDSHandler.NEW_TERRITORIAL_ORG_UNIT, getEduOrgUnit().getTerritorialOrgUnit());
            dataSource.put(DevelopConditionDSHandler.EDUCATION_LEVELS_HIGH_SCHOOL, getEduOrgUnit().getEducationLevelHighSchool());
            dataSource.put(DevelopConditionDSHandler.DEVELOP_FORM, getEduOrgUnit().getDevelopForm());
        }

        if (ListRepresentTransferEdit.DEVELOP_PERIOD_DS.equals(dataSource.getName())) {
            dataSource.put(DevelopPeriodDSHandler.NEW_FORMATIVE_ORG_UNIT, getEduOrgUnit().getFormativeOrgUnit());
            dataSource.put(DevelopPeriodDSHandler.NEW_TERRITORIAL_ORG_UNIT, getEduOrgUnit().getTerritorialOrgUnit());
            dataSource.put(DevelopPeriodDSHandler.EDUCATION_LEVELS_HIGH_SCHOOL, getEduOrgUnit().getEducationLevelHighSchool());
            dataSource.put(DevelopPeriodDSHandler.DEVELOP_FORM, getEduOrgUnit().getDevelopForm());
            dataSource.put(DevelopPeriodDSHandler.DEVELOP_CONDITION, getEduOrgUnit().getDevelopCondition());
        }

        if (ListRepresentTransferEdit.DEVELOP_TECH_DS.equals(dataSource.getName())) {
            dataSource.put(DevelopTechDSHandler.NEW_FORMATIVE_ORG_UNIT, getEduOrgUnit().getFormativeOrgUnit());
            dataSource.put(DevelopTechDSHandler.NEW_TERRITORIAL_ORG_UNIT, getEduOrgUnit().getTerritorialOrgUnit());
            dataSource.put(DevelopTechDSHandler.EDUCATION_LEVELS_HIGH_SCHOOL, getEduOrgUnit().getEducationLevelHighSchool());
            dataSource.put(DevelopTechDSHandler.DEVELOP_FORM, getEduOrgUnit().getDevelopForm());
            dataSource.put(DevelopTechDSHandler.DEVELOP_CONDITION, getEduOrgUnit().getDevelopCondition());
            dataSource.put(DevelopTechDSHandler.DEVELOP_PERIOD, getEduOrgUnit().getDevelopPeriod());
        }

    }

    public ListRepresentTransfer getListRepresentTransfer() {
        return (ListRepresentTransfer) this.getListRepresent();
    }

    @Override
    public void selectRepresent() {
        super.selectRepresent();
        if (!this.getStudentSelectedList().isEmpty() && getEduOrgUnit().getFormativeOrgUnit() == null) {
            Student firstStudent = this.getStudentSelectedList().get(0);
            setEduOrgUnit(firstStudent.getEducationOrgUnit());
        }
    }

    @Override
    public void onClickSave() {

        if (getListRepresentId() != null)
            CheckStateUtil.checkStateRepresent(getListRepresent(), Arrays.asList(MovestudentExtractStatesCodes.CODE_1), _uiSupport.getSession());

        EducationOrgUnit educationOrgUnit = MoveStudentUtil.getEducationOrgUnit(getEduOrgUnit());
        if (educationOrgUnit == null)
            throw new ApplicationException("Подходящее направление подготовки (специальность) подразделения не найдено.");
        getListRepresentTransfer().setEducationOrgUnit(educationOrgUnit);

        boolean ok = MoveStudentUtil.isChecked(getStudentSelectedList(), getListRepresent().getCompensationTypeNew(), getEduOrgUnit().getDevelopForm());

        if ((ok && !isSave()) || (ok && isSave() && isWarning())) {
            String mess = MoveStudentUtil.checked(getListRepresent().getDateTransfer(), false);
            if (!StringUtils.isEmpty(mess)) {
                setSave(true);
                setWarning(true);
                setWarningMessage(mess);
                return;
            }
        }

        super.onClickSave();

    }

    @Override
    public void onIgnoreWarning() {

        if (isSave()) {
            setWarning(false);
            onClickSave();
        }
        else
            super.onIgnoreWarning();
    }

    @Override
    public void onCancelRepresent() {
        setWarning(false);
        setSave(false);
    }

    public ISelectModel getFormSelectModel() {
        return formSelectModel;
    }

    public void setFormSelectModel(ISelectModel formSelectModel) {
        this.formSelectModel = formSelectModel;
    }

    public EducationOrgUnit getEduOrgUnit() {
        return eduOrgUnit;
    }

    public void setEduOrgUnit(EducationOrgUnit eduOrgUnit) {
        this.eduOrgUnit = eduOrgUnit;
    }

    public boolean isSave() {
        return save;
    }

    public void setSave(boolean save) {
        this.save = save;
    }

    public boolean isShowAllGroups() {
        return showAllGroups;
    }

    public void setShowAllGroups(boolean showAllGroups) {
        this.showAllGroups = showAllGroups;
    }
}
