package ru.tandemservice.movestudentrmc.base.bo.DORepresentGrantSuspend.ui.Edit;

import org.apache.commons.lang.time.DateUtils;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.GrantViewDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentEditUI;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.RepresentGrantSuspend;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.uniedu.base.bo.EducationYear.EducationYearManager;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DORepresentGrantSuspendEditUI extends AbstractDORepresentEditUI
{

    @Override
    protected void FetchStudents(Representation doc) {
        _studentDocList.clear();
        _studentList.clear();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocRepresentStudentBase.class, "stud").column("stud");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(DocRepresentStudentBase.representation().fromAlias("stud")),
                DQLExpressions.value(doc.getId())));

        List<DocRepresentStudentBase> studentList = builder.createStatement(getSupport().getSession()).list();
        for (DocRepresentStudentBase student : studentList) {

            _studentDocList.add(student);
            _studentList.add(student.getStudent());
        }

        _student = _studentList.get(0);
    }

    @Override
    protected void initDocument() {
        super.initDocument();

        if (getRepresentId() == null) {
            RepresentGrantSuspend represent = (RepresentGrantSuspend) this.getRepresentObj();
            //------------------------------------------------------------------------------
            //represent.setEducationYear(EducationYearManager.instance().dao().getCurrent());
            //------------------------------------------------------------------------------
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(DateUtils.addMonths(new Date(), 1));
            represent.setDateSuspendPayment(CoreDateUtils.getMonthFirstTimeMoment(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH)));
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        super.onBeforeDataSourceFetch(dataSource);

        if (GrantViewDSHandler.GRANT_VIEW_DS.equals(dataSource.getName())) {
            dataSource.put(GrantViewDSHandler.STUDENT_ID, _student.getId());
            //----------------------------------------------------------------------------------------------------------------
            //dataSource.put(GrantViewDSHandler.EDU_YEAR, ((RepresentGrantSuspend) this.getRepresentObj()).getEducationYear());
            //----------------------------------------------------------------------------------------------------------------
            dataSource.put(GrantViewDSHandler.MONTH, ((RepresentGrantSuspend) this.getRepresentObj()).getDateSuspendPayment());
            dataSource.put(GrantViewDSHandler.SUSPEND, Boolean.TRUE);
        }
    }

}
