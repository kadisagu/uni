package ru.tandemservice.movestudentrmc.entity;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.movestudentrmc.entity.gen.ListRepresentProfileTransferGen;

/**
 * Списочное представление о распределении обучаемых по профилям и группам
 */
public class ListRepresentProfileTransfer extends
        ListRepresentProfileTransferGen
{
    @Override
    public String getTitle() {

        return getRepresentationType().getTitle()
                + " в группу " + getNewGroup().getTitle()
                + " на профиль " + (getNewEducationOrgUnit() != null ? getNewEducationOrgUnit().getTitle() : "")
                + (getOperator() != null ? ", " + getOperator().getFullFio() : "")
                + ", от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getCreateDate());
    }

    @Override
    public String getRepresentationTitle() {
        return getRepresentationType().getTitle()
                + " в группу " + getNewGroup().getTitle()
                + " на профиль " + (getNewEducationOrgUnit() != null ? getNewEducationOrgUnit().getTitle() : "")
                ;
    }
}