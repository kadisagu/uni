package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_movestudentrmc_1x0x0_64to65 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.5.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.5.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность relRepresentationReasonOSSP

        // удалено свойство basementOSSP
        {
            // TODO: программист должен подтвердить это действие
//			if( true )
//				throw new UnsupportedOperationException("Confirm me: delete column");

            // удалить колонку
            tool.dropColumn("relrepresentationreasonossp_t", "basementossp_id");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность representation

        // удалено свойство osspBasement
        {
            // TODO: программист должен подтвердить это действие
//			if( true )
//				throw new UnsupportedOperationException("Confirm me: delete column");

            // удалить колонку
            tool.dropColumn("representation_t", "osspbasement_id");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность osspBasement

        // сущность была удалена
        {
            // TODO: программист должен подтвердить это действие
//            if( true )
//                throw new UnsupportedOperationException("Confirm me: delete table");

            // удалить таблицу
            tool.dropTable("osspbasement_t", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("osspBasement");

        }


    }
}