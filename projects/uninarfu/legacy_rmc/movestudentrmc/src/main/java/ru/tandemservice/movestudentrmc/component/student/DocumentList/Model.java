package ru.tandemservice.movestudentrmc.component.student.DocumentList;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudentrmc.entity.NarfuDocument;
import ru.tandemservice.uni.entity.employee.Student;

@Input(keys = {"studentId"}, bindings = {"student.id"})
public class Model {

    private Student student = new Student();

    private DynamicListDataSource<NarfuDocument> dataSource;

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public DynamicListDataSource<NarfuDocument> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<NarfuDocument> dataSource) {
        this.dataSource = dataSource;
    }


}
