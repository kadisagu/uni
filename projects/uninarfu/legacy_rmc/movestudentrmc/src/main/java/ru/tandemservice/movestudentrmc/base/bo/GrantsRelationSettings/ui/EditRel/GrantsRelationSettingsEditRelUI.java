package ru.tandemservice.movestudentrmc.base.bo.GrantsRelationSettings.ui.EditRel;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.base.bo.GrantsRelationSettings.DOGrantsRelationSettingsManager;
import ru.tandemservice.movestudentrmc.entity.RelTypeGrantView;
import ru.tandemservice.movestudentrmc.entity.catalog.Grant;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantType;

import java.util.List;

@Input({
        @Bind(key = GrantsRelationSettingsEditRelUI.GRANT_ID, binding = GrantsRelationSettingsEditRelUI.GRANT_ID),
        @Bind(key = GrantsRelationSettingsEditRelUI.GRANT_TYPE_ID, binding = GrantsRelationSettingsEditRelUI.GRANT_TYPE_ID)
})
public class GrantsRelationSettingsEditRelUI extends UIPresenter {

    public static final String GRANT_ID = "grantId";
    public static final String GRANT_TYPE_ID = "grantTypeId";

    private Long grantId;
    private Long grantTypeId;
    private Grant grant;
    private GrantType grantType;
    private boolean firstRun = true;
    private List<RelTypeGrantView> relList;
    private RelTypeGrantView relRow;

    @Override
    public void onComponentRefresh()
    {

        if (!firstRun) {

            return;
        }

        if (null != grantId)
            grant = DataAccessServices.dao().getNotNull(Grant.class, grantId);
        else
            grant = new Grant();

        if (null != grantTypeId)
            grantType = DataAccessServices.dao().getNotNull(GrantType.class, grantTypeId);
        else
            grantType = new GrantType();

        fillArrayRel();

        if (firstRun)
            firstRun = !firstRun;
    }

    private void fillArrayRel() {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RelTypeGrantView.class, "rel").addColumn("rel");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(RelTypeGrantView.type().id().fromAlias("rel")),
                DQLExpressions.value(grantTypeId)))
                .where(DQLExpressions.eq(
                        DQLExpressions.property(RelTypeGrantView.grant().id().fromAlias("rel")),
                        DQLExpressions.value(grantId)));
        ;

        relList = builder.createStatement(getSupport().getSession()).list();
    }

    public void onClickApply()
    {

        DOGrantsRelationSettingsManager.instance().modifyDao().updateRelations(grantType, grant, relList);
        //fillArrayRel();
        deactivate();
    }

    public List<RelTypeGrantView> getRelList() {
        return relList;
    }

    public void setRelList(List<RelTypeGrantView> relList) {
        this.relList = relList;
    }

    public RelTypeGrantView getRelRow() {
        return relRow;
    }

    public void setRelRow(RelTypeGrantView relRow) {
        this.relRow = relRow;
    }

    public Long getGrantId() {
        return grantId;
    }

    public void setGrantId(Long grantId) {
        this.grantId = grantId;
    }

    public Long getGrantTypeId() {
        return grantTypeId;
    }

    public void setGrantTypeId(Long grantTypeId) {
        this.grantTypeId = grantTypeId;
    }

    public Grant getGrant() {
        return grant;
    }

    public void setGrant(Grant grant) {
        this.grant = grant;
    }

    public GrantType getGrantType() {
        return grantType;
    }

    public void setGrantType(GrantType grantType) {
        this.grantType = grantType;
    }
}
