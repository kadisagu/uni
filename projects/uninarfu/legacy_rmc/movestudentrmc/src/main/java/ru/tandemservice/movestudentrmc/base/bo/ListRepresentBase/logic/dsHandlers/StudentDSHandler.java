package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.datasource.output.IDQLSelectOutputBuilder;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import org.tandemframework.shared.person.catalog.entity.codes.IdentityCardTypeCodes;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrant.ui.Edit.ListRepresentGrantEditUI;
import ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab.MonthWrapper;
import ru.tandemservice.movestudentrmc.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudentrmc.entity.PersonNARFU;
import ru.tandemservice.movestudentrmc.entity.RelTypeGrantView;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntity;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.StuGrantStatusCodes;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkGradeValueCatalogItemCodes;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

public class StudentDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput> {

    public static final String MARK_OTHER = "other";

    public static final String DEVELOP_FORM_FILTER = "developFormFilter";
    public static final String DECLARATION_FILTER = "declarationFilter";
    public static final String FORMATIVE_ORG_UNIT_FILTER = "formativeOrgUnitFilter";
    public static final String TYPE_REPRESENT_FILTER = "typeRepresentFilter";
    public static final String DATE_REPRESENT_FILTER = "dateRepresentFilter";
    public static final String STUDENT_FIO_FILTER = "studentFioFilter";
    public static final String EDUCATION_LEVELS_HIGH_SHOOL_FILTER = "educationLevelsHighSchoolDSFilter";
    public static final String QUALIFICATION_FILTER = "qualificationFilter";
    public static final String EDUCATION_YEAR_FILTER = "educationYearFilter";

    public static final String DEVELOP_FORM_COLUMN = "developForm";
    public static final String FORMATIVE_ORG_UNIT_COLUMN = "formativeOrgUnit";
    public static final String TERRITORIAL_ORG_UNIT_COLUMN = "territorialOrgUnit";
    public static final String EDUCATION_LEVEL_HIGH_SCHOOL_COLUMN = "educationLevelHighSchool";
    public static final String EDUCATION_ORG_UNIT_COLUMN = "educationOrgUnit";
    public static final String TYPE_REPRESENT_COLUMN = "typeRepresent";
    public static final String COURSE_COLUMN = "course";
    public static final String GROUP_COLUMN = "group";
    public static final String BUDGET_COLUMN = "budget";
    public static final String DATE_REPRESENT_COLUMN = "dateRepresent";
    public static final String STUDENT_FIO_COLUMN = "studentFio";
    public static final String CHECKBOX_COLUMN = "checkbox";
    public static final String STUDENT_SELECTED_LIST = "studentSelectedList";
    public static final String ORDER_ID = "orderId";
    public static final String COMPENSATION_TYPE_COLUMN = "compensationType";
    public static final String DYPLOM_COLUMN = "dyplom";
    public static final String STATUS_COLUMN = "status";

    public static final String YEAR_DIST_SESSION_RESULT = "yearDistPartFilterFotConsiderSessionResults";
    public static final String YEAR_EDU_SESSION_RESULT = "educationYearFilterFotConsiderSessionResults";

    public static final String VALID_ABSENT = "state.1";

    public static final List<String> USPESNO_SDAVSZIE_SESSIU =
            Arrays.asList(
                    SessionMarkGradeValueCatalogItemCodes.HOROSHO,
                    SessionMarkGradeValueCatalogItemCodes.OTLICHNO,
                    SessionMarkGradeValueCatalogItemCodes.ZACHTENO);

    public StudentDSHandler(String ownerId)
    {
        super(ownerId, Student.class);
    }

    @Override
    protected synchronized DSOutput execute(DSInput input, ExecutionContext context)
    {
        List<Object> territorialOrgUnitFilter = context.get("territorialOrgUnitFilter");
        List<Object> formativeOrgUnitFilter = context.get("formativeOrgUnitFilter");

        List<Object> educationLevelsHighSchoolDSFilter = context.get("educationLevelsHighSchoolDSFilter");
        Object educationLevelFilter = context.get("educationLevelFilter");
        List<Object> qualificationFilter = context.get(QUALIFICATION_FILTER);
        List<Object> courseFilter = context.get("courseFilter");
        List<Object> groupFilter = context.get("groupFilter");
        List<Object> developFormFilter = context.get("developFormFilter");
        Object compensationTypeFilter = context.get("compensationTypeFilter");

        List<Object> educationOrgUnitFilter = context.get("educationOrgUnitFilter");

        List<Object> benefitFilter = context.get("benefitFilter");
        List<Object> studentFilter = context.get("studentFilter");

        Boolean isSuccessfullyHandOverSession = (Boolean) (context.get("isSuccessfullyHandOverSession") == null ? false : context.get("isSuccessfullyHandOverSession"));

        List<Object> educationYearFilter = context.get("educationYearFilter");
        List<Object> yearDistPartFilter = context.get("yearDistPartFilter");
        List<Object> educationYearFilterForSessionResult = context.get(YEAR_EDU_SESSION_RESULT);
        List<Object> yearDistPartFilterForSessionResult = context.get(YEAR_DIST_SESSION_RESULT);

        List<Object> studentStatusFilter = context.get("studentStatusFilter");

        Boolean isConsiderSessionResults = (Boolean) (context.get("isConsiderSessionResults") == null ? false : context.get("isConsiderSessionResults"));
        Date validityOfDocumentFilterResult = context.get("validityOfDocumentFilter");

        Boolean isforeignCitizens = (Boolean) (context.get("isforeignCitizens") == null ? false : context.get("isforeignCitizens"));

        Boolean isNoGroup = (Boolean) (context.get("isNoGroup") == null ? false : context.get("isNoGroup"));


        List<IdentifiableWrapper> sessionMarksResult = context.get("sessionMarksResult");

        GrantView grantView = context.get("grantViewFilter");
        String studentGrantViewStatus = context.get("studentGrantViewStatusCodeFilter");
        IdentifiableWrapper grantsFilter = context.get("grantsFilter");
        Date monthGrantFilter = context.get("monthFilter");

        EducationOrgUnit orientation = context.get("orientationFilter");
        EducationYear educationYear = context.get("eduYearFilter");

        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(Student.class, "s");
        builder.column("s");


        if (isforeignCitizens) {
            FilterUtils.applySelectFilter(builder, "s", Student.compensationType().code(), CompensationTypeCodes.COMPENSATION_TYPE_BUDGET);
            builder.where(ne(property(Student.person().identityCard().cardType().code().fromAlias("s")), value(IdentityCardTypeCodes.PASPORT_GRAJDANINA_ROSSIYSKOY_FEDERATSII)));
        }

        if (benefitFilter != null && !benefitFilter.isEmpty()) {
            DQLSelectBuilder benefitBuilder = new DQLSelectBuilder();

            benefitBuilder.fromEntity(PersonBenefit.class, "b");
            FilterUtils.applySelectFilter(benefitBuilder, "b", PersonBenefit.benefit(), benefitFilter);
            benefitBuilder.column(property(PersonBenefit.person().fromAlias("b")));

            builder.where(in(property(Student.person().fromAlias("s")), benefitBuilder.buildQuery()));
        }
        builder.where(eq(property(Student.archival().fromAlias("s")), value(false)));

        if (isNoGroup)
            builder.where(isNull(property(Student.group().fromAlias("s"))));

        FilterUtils.applySelectFilter(builder, "s", Student.educationOrgUnit().formativeOrgUnit(), formativeOrgUnitFilter);

        FilterUtils.applySelectFilter(builder, "s", Student.educationOrgUnit().educationLevelHighSchool().orgUnit(), educationOrgUnitFilter);

        FilterUtils.applySelectFilter(builder, "s", Student.course(), courseFilter);

        FilterUtils.applySelectFilter(builder, "s", Student.group(), groupFilter);

        FilterUtils.applySelectFilter(builder, "s", Student.compensationType(), compensationTypeFilter);


        FilterUtils.applySelectFilter(builder, "s", Student.educationOrgUnit().territorialOrgUnit(), territorialOrgUnitFilter);

        FilterUtils.applySelectFilter(builder, "s", Student.educationOrgUnit().developForm(), developFormFilter);

        FilterUtils.applySelectFilter(builder, "s", Student.educationOrgUnit().educationLevelHighSchool(), educationLevelsHighSchoolDSFilter);

        FilterUtils.applySelectFilter(builder, "s", Student.status(), studentStatusFilter);

        FilterUtils.applySelectFilter(builder, "s", Student.educationOrgUnit().educationLevelHighSchool().educationLevel().qualification(), qualificationFilter);

        if (educationLevelFilter != null)
            builder.where(in(
                    property(Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().fromAlias("s")),
                    getByRoot((StructureEducationLevels) educationLevelFilter)
            ));

        if (studentFilter != null && !studentFilter.isEmpty())
            FilterUtils.applySelectFilter(builder, "s", Student.id(), studentFilter);

        if (validityOfDocumentFilterResult != null) {
            DQLSelectBuilder dateBuilder = new DQLSelectBuilder()
                    .fromEntity(PersonNARFU.class, "pn")
                    .column(PersonNARFU.person().fromAlias("pn").s())
                    .where(gt(
                            property(PersonNARFU.documentExpiredDate().fromAlias("pn")),
                            valueDate(validityOfDocumentFilterResult)));
            if (UniDaoFacade.getCoreDao().getCount(dateBuilder) == 0)
                return ListOutputBuilder.get(input, new ArrayList<Student>()).pageable(isPageable()).build();

            builder.where(in(
                    property(Student.person().fromAlias("s")),
                    dateBuilder.buildQuery()));
        }

        //для представления о распределении по профилям
        //фильтр по подписке студента на профиль в учебном году
        if (orientation != null || educationYear != null) {
            builder.where(in(
                    property(Student.id().fromAlias("s")),
                    MoveStudentDaoFacade.getOrientationDAO().getStudentBuilder(orientation, educationYear).buildQuery()));
        }

        if (grantView != null) {
            //искать со статусом выплата если не указано иное
            if (StringUtils.isEmpty(studentGrantViewStatus))
                studentGrantViewStatus = StuGrantStatusCodes.CODE_1;

            DQLSelectBuilder subDQL = new DQLSelectBuilder().fromEntity(StudentGrantEntity.class, "sge")
                    .where(eq(
                            property(StudentGrantEntity.view().fromAlias("sge")),
                            value(grantView)
                    ))
                    .where(or(
                            eq(property(StudentGrantEntity.eduYear().current().fromAlias("sge")), value(Boolean.TRUE)),
                            eq(property(StudentGrantEntity.eduYear().intValue().fromAlias("sge")),
                               value(EducationYear.getCurrentRequired().getIntValue() - 1))
                            //-------------------------------------------------------------------------------------------
                            , gt(property(StudentGrantEntity.eduYear().intValue().fromAlias("sge")),
                            		value(EducationYear.getCurrentRequired().getIntValue()))
                            //-------------------------------------------------------------------------------------------
                    ))
                    .where(eq(
                            property(StudentGrantEntity.status().code().fromAlias("sge")),
                            value(studentGrantViewStatus)
                    ))
                    .where(or(
                            eq(property(StudentGrantEntity.tmp().fromAlias("sge")), value(Boolean.FALSE)),
                            isNull(property(StudentGrantEntity.tmp().fromAlias("sge")))
                    ))
                    .column(property(StudentGrantEntity.student().id().fromAlias("sge")))
                    .predicate(DQLPredicateType.distinct);

            builder.where(in(
                    property(Student.id().fromAlias("s")),
                    subDQL.buildQuery()
            ));
        }

        if (grantsFilter != null && monthGrantFilter != null) {

            MonthWrapper.Data data = MonthWrapper.getInstance(monthGrantFilter);
            String month = data.monthWrapper.getStringValue(data.educationYear);
            boolean isAcadem = grantsFilter.getId().equals(ListRepresentGrantEditUI.ACADEM);
            //все академические стипендии, назначенные в данном месяце
            DQLSelectBuilder subDQL = new DQLSelectBuilder().fromEntity(StudentGrantEntity.class, "sge")
                    .where(or(
                            eq(property(StudentGrantEntity.tmp().fromAlias("sge")), value(Boolean.FALSE)),
                            isNull(property(StudentGrantEntity.tmp().fromAlias("sge")))
                    ))
                    .where(eqValue(property(StudentGrantEntity.month().fromAlias("sge")), month))
                    .where(eqValue(property(StudentGrantEntity.eduYear().fromAlias("sge")), data.educationYear))
                    .joinPath(DQLJoinType.inner, StudentGrantEntity.view().fromAlias("sge"), "view")
                    .joinEntity("view", DQLJoinType.inner, RelTypeGrantView.class, "rel",
                                eq(
                                        property(GrantView.id().fromAlias("view")),
                                        property(RelTypeGrantView.view().fromAlias("rel"))))
                    .where(eqValue(property(RelTypeGrantView.type().academ().fromAlias("rel")), Boolean.TRUE))
                    .column(property(StudentGrantEntity.student().id().fromAlias("sge")))
                    .distinct();

            if (isAcadem)
                builder.where(in(
                        property(Student.id().fromAlias("s")),
                        subDQL.buildQuery()
                ));
            else
                builder.where(notIn(
                        property(Student.id().fromAlias("s")),
                        subDQL.buildQuery()
                ));
        }

        // те студенты, которые уже выделены
        List<Student> selectedList = context.get(STUDENT_SELECTED_LIST);
        if (selectedList != null && selectedList.size() > 0)
            builder.where(notIn(property(Student.id().fromAlias("s")), selectedList));

        //Поиск среди успешно сдавших сессию, без указания - учитывать результат сессии
        //#6818 попросили убрать условие "без указания - учитывать результат сессии"
        if (isSuccessfullyHandOverSession) {
            /*
        	DQLSelectBuilder allMarks = getStudentMarks(builder, yearDistPartFilter, educationYearFilter);
            List<SessionMark> marks = UniDaoFacade.getCoreDao().getList(allMarks);//allMarks.createStatement(getSession()).list();
            Map<Student,Map<EppStudentWpeCAction,String>> sessionMarkMap = successfullyHandOverSession(marks);
            List<Student> students =  new ArrayList<>(sessionMarkMap.keySet());
            Collections.sort(students, new EntityComparator(input.getEntityOrder()));
            return ListOutputBuilder.get(input, students).pageable(isPageable()).build();
            */
            DQLSelectBuilder bedMarkDQL = getStudentNegativeMarks(yearDistPartFilter, educationYearFilter);
            builder.where(notIn(
                    property(Student.id().fromAlias("s")),
                    bedMarkDQL.buildQuery()));


            // студенты, по которым в принципе есть оценки
            DQLSelectBuilder dqlHasMark = getStudentHasMarks(yearDistPartFilter, educationYearFilter);
            builder.where(in(
                    property(Student.id().fromAlias("s")),
                    dqlHasMark.buildQuery()));
        }

        //Учитывать результаты сессии
        if (isConsiderSessionResults && sessionMarksResult != null && !sessionMarksResult.isEmpty()) {

//            //Получим оценки всех студентов
//            DQLSelectBuilder markDql = getStudentMarks(builder, yearDistPartFilterForSessionResult, educationYearFilterForSessionResult);
//            List<SessionMark> marks = UniDaoFacade.getCoreDao().getList(markDql);//markDql.createStatement(getSession()).list();
//           
//            //Отбросим тех, у кого есть 3-ки или меньше
//            Map<Student,Map<EppStudentWpeCAction,String>> sessionMarkMap = successfullyHandOverSession(marks);
//
//            //Обработаем фильтры по оценкам. Общий результат складывается из результатов каждого фильтра
//            List<Student> students = new ArrayList<>();
//            for(IdentifiableWrapper w : sessionMarksResult){
//				if (w.getId().equals(Long.valueOf(1L))) {
//                    List<Student> lst = considerSessionResults_4_5(sessionMarkMap);
//                   students.addAll(lst);
//				}
//				else{
//					if(w.getId().equals(Long.valueOf(0L))){
//                      List<Student> lst = considerSessionResults(sessionMarkMap, SessionMarkGradeValueCatalogItemCodes.HOROSHO);
//                      students.addAll(lst);
//                    }
//
//					if (w.getId().equals(Long.valueOf(2L))) {
//                        List<Student> lst = considerSessionResults(sessionMarkMap, SessionMarkGradeValueCatalogItemCodes.OTLICHNO);
//                        students.addAll(lst);
//                    }
//				}
//			}
//            Collections.sort(students, new EntityComparator(input.getEntityOrder()));
//            //Передать результат в итоговый DQLSelectOutputBuilder не получится, так как in в DQL-запросе не съедает списки с >2000 элементов
//            return ListOutputBuilder.get(input, students).pageable(isPageable()).build();

            List<Integer> minMarkPriority = new ArrayList<>();
            List<Integer> maxMarkPriority = new ArrayList<>();
            for (IdentifiableWrapper w : sessionMarksResult) {
                if (w.getId().equals(1L)) // 4 и 5
                {
                    if (!minMarkPriority.contains(4))
                        minMarkPriority.add(4);
                    if (!maxMarkPriority.contains(5))
                        maxMarkPriority.add(5);
                }
                else {
                    if (w.getId().equals(0L)) {
                        if (!minMarkPriority.contains(4))
                            minMarkPriority.add(4);
                        if (!maxMarkPriority.contains(4))
                            maxMarkPriority.add(4);
                    }
                    if (w.getId().equals(2L)) {
                        if (!minMarkPriority.contains(5))
                            minMarkPriority.add(5);
                        if (!maxMarkPriority.contains(5))
                            maxMarkPriority.add(5);
                    }
                }
            }

            DQLSelectBuilder subDql = getStudentPositiveMarks(yearDistPartFilterForSessionResult, educationYearFilterForSessionResult, minMarkPriority, maxMarkPriority);
            // обойдемся без in, просто джойним
            // markPositive.includeId
            builder.joinDataSource("s",
                                   DQLJoinType.inner,
                                   subDql.buildQuery(),
                                   "ds",
                                   eq(
                                           property(Student.id().fromAlias("s")),
                                           property("ds.includeId")));

            DQLSelectBuilder bedMarkDQL = getStudentNegativeMarks(yearDistPartFilterForSessionResult, educationYearFilterForSessionResult);
            builder.where(notIn(
                    property(Student.id().fromAlias("s")),
                    bedMarkDQL.buildQuery()));

        }

        // Жесть полная, Ситек - что это?
		/*
		 List<Student> studentList = builder.createStatement(getSession()).list();
		 List<Student> resultList = new ArrayList<Student>();
		 List<Student> selectedList = context.get(STUDENT_SELECTED_LIST);
		 for (Student student : studentList) {
			if (selectedList !=null)
				if(selectedList.contains(student))
					continue;
			resultList.add(student);
		 }
		return ListOutputBuilder.get(input, resultList).pageable(true).build();
		*/

        //if (input.getEntityOrder() != null)
        //    builder.order(DQLExpressions.property("s", input.getEntityOrder().getKeyString()), input.getEntityOrder().getDirection());

        /*
        List<Student> lst = IUniBaseDao.instance.get().getList(builder);
        System.out.print(lst.size());
        return ListOutputBuilder.get(input, lst).pageable(true).build();
        */

        //input.getStartRecord();
        //input.getCountRecord();
        // input.setEntityOrder(null);

        if (input.getEntityOrder() != null) {
            DQLOrderDescriptionRegistry order = new DQLOrderDescriptionRegistry(Student.class, "s");
            order.applyOrderWithLeftJoins(builder, input.getEntityOrder());
        }
        IDQLSelectOutputBuilder dout = DQLSelectOutputBuilder.get(input, builder, getSession());
        dout.pageable(isPageable());

        return dout.build();

//		return DQLSelectOutputBuilder.get(input, builder, getSession()).pageable(isPageable()).build();
    }

//    private List<Student> considerSessionResults_4_5(Map<Student, Map<EppStudentWpeCAction, String>> map)
//    {
//        Map<Student, Map<EppStudentWpeCAction, String>> markMap = new HashMap<>();
//        List<Student> students = new ArrayList<>();
//        //Выбор тех, у кого есть пятерки
//        for (Map.Entry<Student, Map<EppStudentWpeCAction, String>> item : map.entrySet()) {
//            for (Map.Entry<EppStudentWpeCAction, String> sessionMark : item.getValue().entrySet()) {
//                String code = sessionMark.getValue();
//                if (code.equals(SessionMarkGradeValueCatalogItemCodes.OTLICHNO)) {
//                    markMap.put(item.getKey(), item.getValue());
//                    break;
//                }
//            }
//        }
//        //Выбор тех, у кого есть 4-ки
//        for (Map.Entry<Student, Map<EppStudentWpeCAction, String>> item : markMap.entrySet()) {
//            for (Map.Entry<EppStudentWpeCAction, String> sessionMark : item.getValue().entrySet()) {
//                String code = sessionMark.getValue();
//                if (code.equals(SessionMarkGradeValueCatalogItemCodes.HOROSHO)) {
//                    students.add(item.getKey());
//                    break;
//                }
//            }
//        }
//        return students;
//
//    }

//    private List<Student> considerSessionResults(Map<Student, Map<EppStudentWpeCAction, String>> map, String markCode)
//    {
//        List<Student> students = new ArrayList<>(map.keySet());
//        //Убираем всех с оценками, отличными от запрашиваемой
//        for (Map.Entry<Student, Map<EppStudentWpeCAction, String>> item : map.entrySet()) {
//            for (Map.Entry<EppStudentWpeCAction, String> sessionMark : item.getValue().entrySet()) {
//                String code = sessionMark.getValue();
//                if (code.equals(SessionMarkGradeValueCatalogItemCodes.ZACHTENO))
//                    continue;
//                if (!code.equals(markCode)) {
//                    students.remove(item.getKey());
//                    break;
//                }
//            }
//        }
//        return students;
//
//    }

    /**
     * Стуженты с неудачными оченками
     * Таких просто исключаем
     */
    private DQLSelectBuilder getStudentNegativeMarks(final List<Object> yearDistPartFilter, final List<Object> educationYearFilter) {

        DQLSelectBuilder markDql = new DQLSelectBuilder()
                .fromEntity(SessionMark.class, "markExclude")
                .column(SessionMark.slot().studentWpeCAction().studentWpe().student().id().fromAlias("markExclude").s(), "excludeId")

                        // есть плохие оценки
                .where(eq(property(SessionMark.cachedMarkPositiveStatus().fromAlias("markExclude")), value(false)))

                        // в сессию
                .where(eq(property(SessionMark.slot().inSession().fromAlias("markExclude")), value(true)))
                .predicate(DQLPredicateType.distinct)

                .joinPath(DQLJoinType.inner, SessionMark.slot().document().fromAlias("markExclude"), "document")

                        // берем оценки только из зачетки
                        // зачетная книжка SessionStudentGradeBookDocument
                .joinEntity("document", DQLJoinType.inner, SessionStudentGradeBookDocument.class, "gradebook", eq
                                    (
                                            property("gradebook.id"),
                                            property("document.id")
                                    )
                );


        //Фильтры по году и семестру
        if (educationYearFilter != null && !educationYearFilter.isEmpty())
            FilterUtils.applySelectFilter(markDql, SessionMark.slot().studentWpeCAction().studentWpe().year().educationYear().fromAlias("markExclude"), educationYearFilter);
        if (yearDistPartFilter != null && !yearDistPartFilter.isEmpty())
            FilterUtils.applySelectFilter(markDql, SessionMark.slot().studentWpeCAction().studentWpe().part().fromAlias("markExclude"), yearDistPartFilter);
        return markDql;
    }


    /**
     * Студенты, по которым в принципе есть оценки, любые
     */
    private DQLSelectBuilder getStudentHasMarks(final List<Object> yearDistPartFilter, final List<Object> educationYearFilter)
    {
        DQLSelectBuilder markDql = new DQLSelectBuilder()
                .fromEntity(SessionMark.class, "markHas")
                .column(SessionMark.slot().studentWpeCAction().studentWpe().student().id().fromAlias("markHas").s(), "excludeId")
                        // в сессию
                .where(eq(property(SessionMark.slot().inSession().fromAlias("markHas")), value(true)))
                .predicate(DQLPredicateType.distinct);

        //Фильтры по году и семестру
        if (educationYearFilter != null && !educationYearFilter.isEmpty())
            FilterUtils.applySelectFilter(markDql, SessionMark.slot().studentWpeCAction().studentWpe().year().educationYear().fromAlias("markHas"), educationYearFilter);
        if (yearDistPartFilter != null && !yearDistPartFilter.isEmpty())
            FilterUtils.applySelectFilter(markDql, SessionMark.slot().studentWpeCAction().studentWpe().part().fromAlias("markHas"), yearDistPartFilter);
        return markDql;
    }


    /**
     * Стуженты с неудачными оченками
     * Таких просто исключаем
     */
    private DQLSelectBuilder getStudentPositiveMarks(final List<Object> yearDistPartFilter, final List<Object> educationYearFilter, List<Integer> minPriority, List<Integer> maxPriority)
    {
        DQLSelectBuilder markDql = new DQLSelectBuilder()
                .fromEntity(SessionMark.class, "markPositive")
                .column(SessionMark.slot().studentWpeCAction().studentWpe().student().id().fromAlias("markPositive").s(), "includeId")
                        // в сессию
                .where(eq(property(SessionMark.slot().inSession().fromAlias("markPositive")), value(true)))
                .joinPath(DQLJoinType.inner, SessionMark.cachedMarkValue().fromAlias("markPositive"), "smci")
                .joinEntity("smci", DQLJoinType.inner, SessionMarkGradeValueCatalogItem.class, "smgv", eq
                                    (
                                            property("smci.id"),
                                            property("smgv.id")
                                    )
                )

                        // документ - основание
                .joinPath(DQLJoinType.inner, SessionMark.slot().document().fromAlias("markPositive"), "document")

                        // берем оценки только из зачетки
                        // зачетная книжка SessionStudentGradeBookDocument
                .joinEntity("document", DQLJoinType.inner, SessionStudentGradeBookDocument.class, "gradebook", eq
                                    (
                                            property("gradebook.id"),
                                            property("document.id")
                                    )
                )
                        //смотрим только оценки по пятибальной шкале
                .where(ne(property(SessionMarkGradeValueCatalogItem.scale().code().fromAlias("smgv")), "scale2"))

                        // вынимаем максимальный и минимальный приоритет оценки
                .column(DQLFunctions.min(property(SessionMarkGradeValueCatalogItem.priority().fromAlias("smgv"))))
                .column(DQLFunctions.max(property(SessionMarkGradeValueCatalogItem.priority().fromAlias("smgv"))))
                .group(SessionMark.slot().studentWpeCAction().studentWpe().student().id().fromAlias("markPositive").s())

                .having(in(DQLFunctions.min(property(SessionMarkGradeValueCatalogItem.priority().fromAlias("smgv"))), minPriority))
                .having(in(DQLFunctions.max(property(SessionMarkGradeValueCatalogItem.priority().fromAlias("smgv"))), maxPriority))

                        //.predicate(DQLPredicateType.distinct)
                .where(eq(property(SessionMark.cachedMarkPositiveStatus().fromAlias("markPositive")), value(true)));


        //Фильтры по году и семестру
        if (educationYearFilter != null && !educationYearFilter.isEmpty())
            FilterUtils.applySelectFilter(markDql, SessionMark.slot().studentWpeCAction().studentWpe().year().educationYear().fromAlias("markPositive"), educationYearFilter);
        if (yearDistPartFilter != null && !yearDistPartFilter.isEmpty())
            FilterUtils.applySelectFilter(markDql, SessionMark.slot().studentWpeCAction().studentWpe().part().fromAlias("markPositive"), yearDistPartFilter);

        return markDql;
    }


//    private DQLSelectBuilder getStudentMarks(DQLSelectBuilder studentBuilder, final List<Object> yearDistPartFilter, final List<Object> educationYearFilter) {
//
//        DQLSelectBuilder markDql = new DQLSelectBuilder()
//                .fromEntity(SessionMark.class, "mark")
//                .column("mark")
//                        // fetchPath работает дольше, чем прямое извлечение сущности Student из SessionMark
//                        //       .fetchPath(DQLJoinType.inner, SessionMark.slot().student().slot().student().student().fromAlias("mark"), "student")
//                .where(DQLExpressions.eq(DQLExpressions.property(SessionMark.slot().inSession().fromAlias("mark")), value(true)))
//                .where(DQLExpressions.in(DQLExpressions.property(SessionMark.slot().studentWpeCAction().studentWpe().student().fromAlias("mark")), studentBuilder.buildQuery()))
//                .order(DQLExpressions.property(SessionMark.id().fromAlias("mark")));
//        //Фильтры по году и семестру
//        if (educationYearFilter != null && !educationYearFilter.isEmpty())
//            FilterUtils.applySelectFilter(markDql, SessionMark.slot().studentWpeCAction().studentWpe().year().educationYear().fromAlias("mark"), educationYearFilter);
//        if (yearDistPartFilter != null && !yearDistPartFilter.isEmpty())
//            FilterUtils.applySelectFilter(markDql, SessionMark.slot().studentWpeCAction().studentWpe().part().fromAlias("mark"), yearDistPartFilter);
//        return markDql;
//
//    }

//    private Map<Student, Map<EppStudentWpeCAction, String>> successfullyHandOverSession(List<SessionMark> marks)
//    {
//        Map<Student, Map<EppStudentWpeCAction, String>> sessionMarkMap = new HashMap<>();
//        //Получим итоговые оценки
//        Set<Student> toRemove = new HashSet<>();
//        for (SessionMark mark : marks) {
//            SessionDocumentSlot slot = mark.getSlot();
//            if ((slot.getDocument() instanceof SessionStudentGradeBookDocument)) {
//                Student student = slot.getStudentWpeCAction().getStudentWpe().getStudent();
//                SessionMark regularMark = (mark instanceof SessionSlotLinkMark) ? ((SessionSlotLinkMark) mark).getTarget() : mark;
//                String markCode = regularMark.getCachedMarkValue().getCode();
//                if (!USPESNO_SDAVSZIE_SESSIU.contains(markCode)) {
//                    toRemove.add(student);
//                    continue;
//                }
//                if (!sessionMarkMap.containsKey(student))
//                    sessionMarkMap.put(student, new HashMap<EppStudentWpeCAction, String>());
//                sessionMarkMap.get(student).put(slot.getStudentWpeCAction(), markCode);
//            }
//        }
//
//        for (Student item : toRemove)
//            sessionMarkMap.remove(item);
//
//        return sessionMarkMap;
//    }

    /**
     * Получить количество оценок
     */
//    private Map<String, Integer> countMark(List<SessionSlotRegularMark> marks) {
//
//        int ne_zachteno = 0,
//                neudovletvoritelno = 0, udovletvoritelno = 0,
//                horosho = 0, otlichno = 0,
//                zachteno = 0, other = 0;
//
//        for (SessionSlotRegularMark mark : marks) {
//            String markCode = mark.getCachedMarkValue().getCode();
//            if (markCode.equals(SessionMarkGradeValueCatalogItemCodes.NE_ZACHTENO))
//                ne_zachteno++;
//            else if (markCode.equals(SessionMarkGradeValueCatalogItemCodes.NEUDOVLETVORITELNO))
//                neudovletvoritelno++;
//            else if (markCode.equals(SessionMarkGradeValueCatalogItemCodes.UDOVLETVORITELNO))
//                udovletvoritelno++;
//            else if (markCode.equals(SessionMarkGradeValueCatalogItemCodes.HOROSHO))
//                horosho++;
//            else if (markCode.equals(SessionMarkGradeValueCatalogItemCodes.OTLICHNO))
//                otlichno++;
//            else if (markCode.equals(SessionMarkGradeValueCatalogItemCodes.ZACHTENO))
//                zachteno++;
//            else
//                other++;
//        }
//
//        HashMap<String, Integer> result = new HashMap<String, Integer>();
//
//        result.put(SessionMarkGradeValueCatalogItemCodes.NE_ZACHTENO, ne_zachteno);
//        result.put(SessionMarkGradeValueCatalogItemCodes.NEUDOVLETVORITELNO, neudovletvoritelno);
//        result.put(SessionMarkGradeValueCatalogItemCodes.UDOVLETVORITELNO, udovletvoritelno);
//        result.put(SessionMarkGradeValueCatalogItemCodes.HOROSHO, horosho);
//        result.put(SessionMarkGradeValueCatalogItemCodes.OTLICHNO, otlichno);
//        result.put(SessionMarkGradeValueCatalogItemCodes.ZACHTENO, zachteno);
//        result.put(MARK_OTHER, other);
//
//        return result;
//    }

    public static PersonNARFU getPersonNARFU(Person person) {
        PersonNARFU personNARFU = UniDaoFacade.getCoreDao().get(PersonNARFU.class, PersonNARFU.L_PERSON, person);
        if (personNARFU != null) {
            return personNARFU;
        }

        personNARFU = new PersonNARFU();
        personNARFU.setPerson(person);
        personNARFU.setNeedDormitoryOnReceiptPeriod(false);
        personNARFU.setAverageFamilyCapital(0);
        personNARFU.setDocumentExpiredDate(new Date());
        UniDaoFacade.getCoreDao().saveOrUpdate(personNARFU);
        return personNARFU;
    }

    private Set<StructureEducationLevels> getByRoot(StructureEducationLevels root) {
        if (root == null)
            return null;

        Set<StructureEducationLevels> resultSet = new HashSet<>();
        resultSet.add(root);

        int counter = 0;
        while (counter < resultSet.size()) {
            counter = resultSet.size();

            List<StructureEducationLevels> list = IUniBaseDao.instance.get().getList(StructureEducationLevels.class, StructureEducationLevels.parent(), resultSet);
            resultSet.addAll(list);
        }

        return resultSet;
    }
}
