package ru.tandemservice.movestudentrmc.base.bo.DOOrder.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.entity.DocOrdRepresent;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;

import java.util.ArrayList;
import java.util.List;

//import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;

public class RepresentOrderDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput> {

    public static final String DEVELOP_FORM_COLUMN = "developForm";
    public static final String FORMATIVE_ORG_UNIT_COLUMN = "formativeOrgUnit";
    public static final String TERRITORIAL_ORG_UNIT_COLUMN = "territorialOrgUnit";
    public static final String EDUCATION_LEVEL_HIGH_SCHOOL_COLUMN = "educationLevelHighSchool";
    public static final String EDUCATION_ORG_UNIT_COLUMN = "educationOrgUnit";
    public static final String TYPE_REPRESENT_COLUMN = "typeRepresent";
    public static final String COURSE_COLUMN = "course";
    public static final String GROUP_COLUMN = "group";
    public static final String BUDGET_COLUMN = "budget";
    public static final String DATE_REPRESENT_COLUMN = "dateRepresent";
    public static final String STUDENT_FIO_COLUMN = "studentFio";
    public static final String CHECK = "check";
    public static final String CHECKBOX_COLUMN = "checkbox";
    public static final String ORDER_ID = "orderId";

    public RepresentOrderDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long orderId = (Long) context.get(ORDER_ID);

        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(DocOrdRepresent.class, "r").column(DQLExpressions.property("r"));
        builder.joinEntity("r", DQLJoinType.inner, DocRepresentStudentBase.class, "s",
                           DQLExpressions.eq(DQLExpressions.property(DocOrdRepresent.representation().fromAlias("r")),
                                             DQLExpressions.property(DocRepresentStudentBase.representation().fromAlias("s"))));
        builder.column(DQLExpressions.property("s"));
        builder.where(DQLExpressions.eq(DQLExpressions.property(DocOrdRepresent.order().id().fromAlias("r")), DQLExpressions.value(orderId)));

        List<Object[]> representList = builder.createStatement(context.getSession()).list();
        List<DataWrapper> resultList = new ArrayList<DataWrapper>();
        for (Object[] represent : representList) {
            DocOrdRepresent representData = (DocOrdRepresent) represent[0];
            DocRepresentStudentBase studentData = (DocRepresentStudentBase) represent[1];

            DataWrapper w = new DataWrapper(representData.getRepresentation().getId(), representData.getRepresentation().getTitle(), representData.getRepresentation());
            w.setProperty("student", studentData.getStudent());
            w.setProperty("represent", representData.getRepresentation());
            resultList.add(w);
        }

        return ListOutputBuilder.get(input, resultList).pageable(true).build();

    }
}
