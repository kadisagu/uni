package ru.tandemservice.movestudentrmc.dao.check;

import ru.tandemservice.uni.UniDefines;

public class CheckStatusDeducted extends AbstractCheckStudentStatus {

    @Override
    protected String getStatusCode() {
        return UniDefines.CATALOG_STUDENT_STATUS_EXCLUDED;
    }

}
