package ru.tandemservice.movestudentrmc.entity;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.movestudentrmc.entity.gen.RepresentSupportGen;

/**
 * О назначении социальной поддержки в связи с тяжелым материальным положением
 */
public class RepresentSupport extends RepresentSupportGen
{
    @Override
    public String getTitle() {
        return new StringBuilder()
                .append(getType().getTitle())
                .append(" (")
                .append(getGrantView().getShortTitle())
                .append(") с ")
                .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getBeginDate()))
                //---------------------------------------------------------------------
                //.append(", от ")
                //.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getCreateDate()))
                //---------------------------------------------------------------------
                .toString();
    }

    @Override
    public String getRepresentationTitle() {
        return new StringBuilder()
                .append(getType().getTitle())
                .append(" (")
                .append(getGrantView().getShortTitle())
                .append(") с ")
                .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getBeginDate()))
                .toString();
    }

}