package ru.tandemservice.movestudentrmc.base.bo.DORepresentSupport.ui.Edit;

import org.apache.commons.lang.time.DateUtils;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfText;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentEditUI;
import ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab.MonthWrapper;
import ru.tandemservice.movestudentrmc.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.GrantEntity;
import ru.tandemservice.movestudentrmc.entity.RepresentSupport;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.util.ReportRenderer;
import ru.tandemservice.uniedu.base.bo.EducationYear.EducationYearManager;

import java.io.IOException;
import java.util.*;

public class DORepresentSupportEditUI extends AbstractDORepresentEditUI
{

    @Override
    protected void FetchStudents(Representation doc) {
        _studentDocList.clear();
        _studentList.clear();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocRepresentStudentBase.class, "stud").column("stud");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(DocRepresentStudentBase.representation().fromAlias("stud")),
                DQLExpressions.value(doc.getId())));

        List<DocRepresentStudentBase> studentList = builder.createStatement(getSupport().getSession()).list();
        for (DocRepresentStudentBase student : studentList) {

            _studentDocList.add(student);
            _studentList.add(student.getStudent());
        }

        _student = _studentList.get(0);
    }

    @Override
    protected void initDocument() {
        super.initDocument();

        if (getRepresentId() == null) {
            RepresentSupport represent = (RepresentSupport) this.getRepresentObj();
            represent.setEducationYear(EducationYearManager.instance().dao().getCurrent());
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(DateUtils.addMonths(new Date(), 1));
            represent.setBeginDate(CoreDateUtils.getMonthFirstTimeMoment(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH)));
        }
    }

    @Override
    public void onClickSave() throws IOException {

        RepresentSupport represent = (RepresentSupport) this.getRepresentObj();

        List<String> periodList = MonthWrapper.getMonthsList(represent.getBeginDate(), represent.getBeginDate(), represent.getEducationYear());
        List<String> errorList = new ArrayList<>();

        boolean good = MoveStudentDaoFacade.getGrantDAO().checkStudentGrants(represent.getId(), represent.getGrantView(), this.getStudentList(), periodList, errorList);

        if (!good) {
            RtfDocument errorReport = RtfBean.getElementFactory().createRtfDocument();
            IRtfControl par = RtfBean.getElementFactory().createRtfControl(IRtfData.PAR);
            for (String text : errorList) {
                IRtfText studText = RtfBean.getElementFactory().createRtfText(text);
                errorReport.addElement(studText);
                errorReport.getElementList().addAll(Arrays.asList(par, par));
            }
            BusinessComponentUtils.downloadDocument(new ReportRenderer("Ошибки.rtf", errorReport), true);

            throw new ApplicationException("Не удалось сохранить представление");
        }

        // Вносим дату начала действия приказа для избежания nullPointException при печати приказа.
        _representObj.setStartDate(new Date());

        super.onClickSave();

        MoveStudentDaoFacade.getGrantDAO().deleteStudentGrants(represent);
        MoveStudentDaoFacade.getGrantDAO().saveStudentGrants(represent, represent.getGrantView(), _student, represent.getSum(), periodList.get(0), represent.getEducationYear());
    }

    public void onChangeSum() {
        RepresentSupport represent = (RepresentSupport) this.getRepresentObj();
        if (represent.getEducationYear() != null && represent.getGrantView() != null && represent.getBeginDate() != null) {
            String month = MonthWrapper.getMonthsList(represent.getBeginDate(), represent.getBeginDate(), represent.getEducationYear()).get(0);

            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(GrantEntity.class, "ge")
                    .where(DQLExpressions.eqValue(DQLExpressions.property(GrantEntity.orgUnit().fromAlias("ge")), _student.getEducationOrgUnit().getFormativeOrgUnit()))
                    .where(DQLExpressions.eqValue(DQLExpressions.property(GrantEntity.month().fromAlias("ge")), month))
                    .where(DQLExpressions.eqValue(DQLExpressions.property(GrantEntity.eduYear().fromAlias("ge")), represent.getEducationYear()))
                    .where(DQLExpressions.eqValue(DQLExpressions.property(GrantEntity.view().fromAlias("ge")), represent.getGrantView()));
            List<GrantEntity> lst = UniDaoFacade.getCoreDao().getList(builder);

            if (!lst.isEmpty()) {
                GrantEntity ge = lst.get(0);
                represent.setSum(ge.getSum());
            }
            else {
                represent.setSum(0);
            }
        }
    }


}
