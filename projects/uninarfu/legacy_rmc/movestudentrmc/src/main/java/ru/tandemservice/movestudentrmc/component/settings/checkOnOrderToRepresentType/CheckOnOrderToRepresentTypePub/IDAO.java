package ru.tandemservice.movestudentrmc.component.settings.checkOnOrderToRepresentType.CheckOnOrderToRepresentTypePub;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {

    void updateInUse(Model model, Long id);

    void updateValue(Model model, Long id);
}
