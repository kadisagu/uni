package ru.tandemservice.movestudentrmc.base.bo.ListOrder.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.movestudentrmc.entity.ListOrdListRepresent;
import ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;

public class RepresentOrderDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput> {

    public static final String DEVELOP_FORM_COLUMN = "developForm";
    public static final String FORMATIVE_ORG_UNIT_COLUMN = "formativeOrgUnit";
    public static final String TERRITORIAL_ORG_UNIT_COLUMN = "territorialOrgUnit";
    public static final String EDUCATION_LEVEL_HIGH_SCHOOL_COLUMN = "educationLevelHighSchool";
    public static final String EDUCATION_ORG_UNIT_COLUMN = "educationOrgUnit";
    public static final String TYPE_REPRESENT_COLUMN = "typeRepresent";
    public static final String COURSE_COLUMN = "course";
    public static final String GROUP_COLUMN = "group";
    public static final String BUDGET_COLUMN = "budget";
    public static final String DATE_REPRESENT_COLUMN = "dateRepresent";
    public static final String STUDENT_FIO_COLUMN = "studentFio";
    public static final String CHECKBOX_COLUMN = "checkbox";
    public static final String ORDER_ID = "orderId";

    public RepresentOrderDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long orderId = (Long) context.get(ORDER_ID);

        List<ListOrdListRepresent> listOrdListRepresents = DataAccessServices.dao().getList(ListOrdListRepresent.class, ListOrdListRepresent.order().id(), orderId);

    /*    DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(ListOrdListRepresent.class, "r").addColumn("r");
        builder.joinEntity("r", DQLJoinType.inner, RelListRepresentStudents.class,"s",
                DQLExpressions.eq(DQLExpressions.property(ListOrdListRepresent.representation().fromAlias("r")),
                        DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("s"))));
        builder.addColumn("s");
        builder.where(DQLExpressions.eq(DQLExpressions.property(ListOrdListRepresent.order().id().fromAlias("r")), DQLExpressions.value(orderId)));

        List<Object[]> representList = builder.createStatement(getSession()).list();
        List<DataWrapper> resultList = new ArrayList<DataWrapper>();
*/
        List<DataWrapper> resultList = new ArrayList<DataWrapper>();

        for (ListOrdListRepresent represent : listOrdListRepresents) {
            //ListOrdListRepresent representData = (ListOrdListRepresent) represent[0];
            //RelListRepresentStudents studentData = (RelListRepresentStudents) represent[1];

            int count = DataAccessServices.dao().getCount(RelListRepresentStudents.class, RelListRepresentStudents.representation().s(), represent.getRepresentation());

            DataWrapper w = new DataWrapper(represent.getRepresentation().getId(), represent.getRepresentation().getTitle(), represent.getRepresentation());
            w.setProperty("count", count);
            w.setProperty("represent", represent.getRepresentation());
            resultList.add(w);
        }

        Collections.sort(resultList, new EntityComparator(input.getEntityOrder()));

        return ListOutputBuilder.get(input, resultList).pageable(true).build();

    }
}
