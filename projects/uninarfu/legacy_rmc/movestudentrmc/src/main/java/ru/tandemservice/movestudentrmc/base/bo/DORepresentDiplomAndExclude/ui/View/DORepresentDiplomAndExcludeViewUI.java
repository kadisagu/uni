package ru.tandemservice.movestudentrmc.base.bo.DORepresentDiplomAndExclude.ui.View;

import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentViewUI;
import ru.tandemservice.movestudentrmc.entity.RepresentDiplomAndExclude;

public class DORepresentDiplomAndExcludeViewUI extends AbstractDORepresentViewUI
{
    public String getHasDiplomaWithHonors() {
        RepresentDiplomAndExclude represent = (RepresentDiplomAndExclude) getRepresent();

        return represent.isHasDiplomaWithHonors() ? "с отличием" : "без отличия";

    }
}
