package ru.tandemservice.movestudentrmc.base.bo.DOOrder.ui.CommonVisaEdit;

import org.tandemframework.caf.ui.UIPresenter;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.DOOrderManager;
import ru.tandemservice.movestudentrmc.entity.Principal2Visa;


public class DOOrderCommonVisaEditUI extends UIPresenter {


    private Principal2Visa principal2Visa;

    @Override
    public void onComponentRefresh() {

        setPrincipal2Visa(DOOrderManager.instance().visaDao().getPrincipal2Visa());

    }


    public void onClickSave() {
        DOOrderManager.instance().visaDao().updateOrderVisa(getPrincipal2Visa());
        deactivate();
    }


    public Principal2Visa getPrincipal2Visa() {
        return principal2Visa;
    }


    public void setPrincipal2Visa(Principal2Visa principal2Visa) {
        this.principal2Visa = principal2Visa;
    }


}
