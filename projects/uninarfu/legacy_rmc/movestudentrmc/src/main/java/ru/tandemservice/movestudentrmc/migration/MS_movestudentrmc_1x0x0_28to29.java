package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_movestudentrmc_1x0x0_28to29 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.10"),
                        new ScriptDependency("org.tandemframework.shared", "1.1.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.1.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность representDiplomAndExclude

        // создано обязательное свойство hasdiplomawithhonors
        {
            // создать колонку
            if (!tool.columnExists("representdiplomandexclude_t", "hasdiplomawithhonors_p")) {
                tool.createColumn("representdiplomandexclude_t", new DBColumn("hasdiplomawithhonors_p", DBType.BOOLEAN));

                // задать значение по умолчанию
                java.lang.Boolean defaulthasdiplomawithhonors = Boolean.FALSE;        // TODO: задайте NOT NULL значение!
                tool.executeUpdate("update representdiplomandexclude_t set hasdiplomawithhonors_p=? where hasdiplomawithhonors_p is null", defaulthasdiplomawithhonors);

                // сделать колонку NOT NULL
                tool.setColumnNullable("representdiplomandexclude_t", "hasdiplomawithhonors_p", false);
            }

        }


    }
}