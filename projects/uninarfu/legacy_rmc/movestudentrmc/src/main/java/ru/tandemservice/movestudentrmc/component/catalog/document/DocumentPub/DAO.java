package ru.tandemservice.movestudentrmc.component.catalog.document.DocumentPub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.movestudentrmc.entity.catalog.Document;

import java.lang.reflect.Method;

public class DAO extends DefaultCatalogPubDAO<Document, Model> implements IDAO {

    @Override
    public void change(Document document, String fieldName, Boolean value) {
        try {
            Method method = document.getClass().getMethod("set" + StringUtils.capitalise(fieldName), new Class[]{boolean.class});
            method.invoke(document, new Object[]{value});
            saveOrUpdate(document);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

}
