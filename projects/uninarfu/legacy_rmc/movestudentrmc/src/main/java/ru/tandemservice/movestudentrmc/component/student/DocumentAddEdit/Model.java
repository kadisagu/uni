package ru.tandemservice.movestudentrmc.component.student.DocumentAddEdit;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.State;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.movestudentrmc.entity.NarfuDocument;
import ru.tandemservice.movestudentrmc.entity.RelDocumentTypeKind;
import ru.tandemservice.movestudentrmc.entity.catalog.DocumentKind;
import ru.tandemservice.movestudentrmc.entity.catalog.DocumentType;
import ru.tandemservice.movestudentrmc.util.ListenerWrapper;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.List;

@State(keys = {"studentId"}, bindings = {"student.id"})
@Input(keys = {"documentId", "tmp", "listener"}, bindings = {"document.id", "document.tmp", "listener"})
public class Model {

    private Student student = new Student();
    private ListenerWrapper<NarfuDocument> listener;
    private boolean addForm = true;

    private List<DocumentType> typeList;
    private DocumentType documentType;

    private ISingleSelectModel kindModel;
    private DocumentKind documentKind;

    private ISingleSelectModel relModel;
    private RelDocumentTypeKind documentRel;

    private NarfuDocument document = new NarfuDocument();
    private IUploadFile file;

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public List<DocumentType> getTypeList() {
        return typeList;
    }

    public void setTypeList(List<DocumentType> typeList) {
        this.typeList = typeList;
    }

    public DocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentType documentType) {
        this.documentType = documentType;
    }


    public DocumentKind getDocumentKind() {
        return documentKind;
    }

    public void setDocumentKind(DocumentKind documentKind) {
        this.documentKind = documentKind;
    }

    public RelDocumentTypeKind getDocumentRel() {
        return documentRel;
    }

    public void setDocumentRel(RelDocumentTypeKind documentRel) {
        this.documentRel = documentRel;
    }

    public NarfuDocument getDocument() {
        return document;
    }

    public void setDocument(NarfuDocument document) {
        this.document = document;
    }

    public ISingleSelectModel getKindModel() {
        return kindModel;
    }

    public void setKindModel(ISingleSelectModel kindModel) {
        this.kindModel = kindModel;
    }

    public ISingleSelectModel getRelModel() {
        return relModel;
    }

    public void setRelModel(ISingleSelectModel relModel) {
        this.relModel = relModel;
    }

    public boolean isAddForm() {
        return addForm;
    }

    public void setAddForm(boolean addForm) {
        this.addForm = addForm;
    }


    public IUploadFile getFile() {
        return file;
    }

    public void setFile(IUploadFile file) {
        this.file = file;
    }

    public boolean isCanSave() {
        return this.documentRel != null;
    }

    public ListenerWrapper<NarfuDocument> getListener() {
        return listener;
    }

    public void setListener(ListenerWrapper<NarfuDocument> listener) {
        this.listener = listener;
    }

}
