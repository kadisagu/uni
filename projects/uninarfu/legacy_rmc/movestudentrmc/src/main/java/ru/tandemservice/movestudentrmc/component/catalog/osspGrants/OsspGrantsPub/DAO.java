package ru.tandemservice.movestudentrmc.component.catalog.osspGrants.OsspGrantsPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.movestudentrmc.entity.catalog.OsspGrants;

public class DAO extends DefaultCatalogPubDAO<OsspGrants, Model> implements IDAO {

    public void updatePrintable(Model model, Long id) {
        OsspGrants osspGrants = getNotNull(OsspGrants.class, id);
        if (osspGrants.isPrintable()) {
            osspGrants.setPrintable(false);
        }
        else {
            osspGrants.setPrintable(true);
        }
        save(osspGrants);
    }

    public void updateReqClarification(Model model, Long id) {
        OsspGrants osspGrants = getNotNull(OsspGrants.class, id);
        if (osspGrants.isReqClarification()) {
            osspGrants.setReqClarification(false);
        }
        else {
            osspGrants.setReqClarification(true);
        }
        save(osspGrants);
    }

    public void updateReqPeriod(Model model, Long id) {
        OsspGrants osspGrants = getNotNull(OsspGrants.class, id);
        if (osspGrants.isReqPeriod()) {
            osspGrants.setReqPeriod(false);
        }
        else {
            osspGrants.setReqPeriod(true);
        }
        save(osspGrants);
    }

    public void updateReqSize(Model model, Long id) {
        OsspGrants osspGrants = getNotNull(OsspGrants.class, id);
        if (osspGrants.isReqSize()) {
            osspGrants.setReqSize(false);
        }
        else {
            osspGrants.setReqSize(true);
        }
        save(osspGrants);
    }
}
