package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.support;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ListRepresentBaseManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.StudentDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.StudentSelectedDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantCancel.ui.Edit.ListRepresentGrantCancelEdit;
import ru.tandemservice.movestudentrmc.docord.IListObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IListObjectModifyDAO;
import ru.tandemservice.movestudentrmc.entity.DocListRepresentBasics;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents;
import ru.tandemservice.movestudentrmc.entity.catalog.MovestudentExtractStates;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationBasement;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentExtractStatesCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.*;

@State({
        @Bind(key = AbstractListRepresentEditUI.LIST_REPRESENT_ID, binding = AbstractListRepresentEditUI.LIST_REPRESENT_ID),
        @Bind(key = "typeRepresentFilter", binding = "typeRepresentFilter"),
        @Bind(key = "reasonRepresentFilter", binding = "reasonRepresentFilter"),
        @Bind(key = "basementRepresentFilter", binding = "basementRepresentFilter"),
        @Bind(key = "representationBasementDate", binding = "representationBasementDate"),
        @Bind(key = "representationBasementNumber", binding = "representationBasementNumber")
})
public abstract class AbstractListRepresentEditUI<T extends ListRepresent> extends UIPresenter {
    public static final String LIST_REPRESENT_ID = "listRepresentId";

    //из биндинга
    private Long listRepresentId;
    private RepresentationType typeRepresentFilter;
    private RepresentationReason reasonRepresentFilter;
    private RepresentationBasement basementRepresentFilter;
    private Date representationBasementDate;     // Дата основания
    private String representationBasementNumber;     // № основания

    //данные представления
    private T listRepresent;
    private DocListRepresentBasics representBasics;

    //работа со списками студентов
    private List<Student> studentSelectedList = new ArrayList<>();
    private List<Student> studentChecked = new ArrayList<>();
    private boolean warning = false;
    private String warningMessage;

    ////////////////////////определить в потомках//////////////////////////////////////////////////////////////

    //экземпляр объекта представления  напр. [return new ListRepresentGrantCancel()] 
    public abstract T getListRepresentObject();

    /////////////////////////////события формы/////////////////////////////////////////////////////////

    @Override
    public void onComponentRefresh() {
        this._uiSettings.clear();

        if (this.getListRepresentId() != null) {
            this.listRepresent = (T) DataAccessServices.dao().get(this.getListRepresentObject().getClass(), this.getListRepresentId());
            this.representBasics = DataAccessServices.dao().get(DocListRepresentBasics.class, DocListRepresentBasics.listRepresent(), this.listRepresent);

            List<RelListRepresentStudents> list = UniDaoFacade.getCoreDao().getList(RelListRepresentStudents.class, RelListRepresentStudents.representation(), this.listRepresent);
            this.studentSelectedList = CommonBaseEntityUtil.getPropertiesList(list, RelListRepresentStudents.student());

            this.typeRepresentFilter = this.listRepresent.getRepresentationType();
            this.reasonRepresentFilter = this.listRepresent.getRepresentationReason();
            this.basementRepresentFilter = this.listRepresent.getRepresentationBasement();

        }
        else {
            this.listRepresent = getListRepresentObject();
            this.listRepresent.setState(UniDaoFacade.getCoreDao().getCatalogItem(MovestudentExtractStates.class, MovestudentExtractStatesCodes.CODE_1));
            this.listRepresent.setRepresentationType(this.getTypeRepresentFilter());
            this.listRepresent.setRepresentationReason(this.getReasonRepresentFilter());
            this.listRepresent.setRepresentationBasement(this.getBasementRepresentFilter());

            this.representBasics = new DocListRepresentBasics();
            this.representBasics.setListRepresent(this.listRepresent);
            this.representBasics.setReason(this.listRepresent.getRepresentationReason());
            this.representBasics.setBasic(this.listRepresent.getRepresentationBasement());

            this.representBasics.setRepresentationBasementDate(this.getRepresentationBasementDate());
            this.representBasics.setRepresentationBasementNumber(this.getRepresentationBasementNumber() == null ? "" : this.getRepresentationBasementNumber());
        }
    }

    public void selectRepresent() {
        if (this.isWarning())
            return;

        selectRepresent(true);
    }

    public void selectRepresent(boolean check) {
        BaseSearchListDataSource representDS = getConfig().getDataSource(ListRepresentGrantCancelEdit.STUDENT_DS);
        Collection<IEntity> records = ((CheckboxColumn) representDS.getLegacyDataSource().getColumn(StudentDSHandler.CHECKBOX_COLUMN)).getSelectedObjects();

        List<Student> selectList = new ArrayList<>();
        for (IEntity entity : records)
            selectList.add((Student) entity);

        if (selectList.isEmpty()) {
            ContextLocal.getErrorCollector().add("Список выбранных студентов пуст");
            return;
        }

        //в потомках переопредеяются доп. проверки на добавление студентов
        checkSelectStudent(selectList);

        List<String> badList = getBadSelectedStudentList(selectList);
        if (check && !badList.isEmpty()) {
            this.warning = true;

            StringBuilder sb = new StringBuilder()
                    .append("Представление формируется для студента(ов) находящегося(ихся) в статусе неактивный: ")
                    .append(StringUtils.join(badList, ", "));
            this.warningMessage = sb.toString();

            return;
        }

        this.studentSelectedList.addAll(selectList);
    }

    public void onIgnoreWarning() {
        this.warning = false;
        this.selectRepresent(false);
    }

    public void checkSelectStudent(List<Student> selectList) {
        getDao().checkSelectStudent(getListRepresent(), selectList);
    }


    public void deleteSelectedRepresent() {
        BaseSearchListDataSource representDS = getConfig().getDataSource(ListRepresentGrantCancelEdit.STUDENT_SELECTED_DS);
        Collection<IEntity> records = ((CheckboxColumn) representDS.getLegacyDataSource().getColumn(StudentSelectedDSHandler.CHECKBOX_COLUMN)).getSelectedObjects();

        this.getStudentSelectedList().removeAll(records);
    }

    public void onClickSave() {

        if (getStudentSelectedList().isEmpty())
            getConfig().getUserContext().getErrorCollector().add(getConfig().getProperty("ui.alertEmptySelectedRepresent"));

        getListRepresent().setCreateDate(getSupport().getCurrentDate());

        IPrincipalContext context = ContextLocal.getUserContext().getPrincipalContext();
        Person person = PersonManager.instance().dao().getPerson(context);

        getListRepresent().setCreator(context);
        getListRepresent().setOperator(person);

        getDao().save(getListRepresent(), getStudentSelectedList(), getRepresentBasics());

        deactivate(2);
    }

    public void onCancelRepresent() {
        deactivate();
    }

    public void saveSettings() {
        this._uiSettings.save();
    }

    public void clearSettings() {
        Object grantViewFilter = _uiSettings.get("grantViewFilter");
        this._uiSettings.clear();
        _uiSettings.set("grantViewFilter", grantViewFilter);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        Map<String, Object> settingMap = _uiSettings.getAsMap(
                "educationYearFilter",
                "territorialOrgUnitFilter",
                StudentDSHandler.FORMATIVE_ORG_UNIT_FILTER,
                "educationLevelsHighSchoolDSFilter",
                "educationOrgUnitFilter",
                "educationLevelFilter",
                StudentDSHandler.QUALIFICATION_FILTER,
                "courseFilter",
                "groupFilter",
                StudentDSHandler.DEVELOP_FORM_FILTER,
                "compensationTypeFilter",
                "benefitFilter",
                StudentDSHandler.STUDENT_FIO_FILTER,
                "educationYearField",
                "isforeignCitizens",
                "studentFilter",
                "userClickApply",
                "groupField",
                "educationLevelsHighSchoolDSField",
                "studentStatusFilter",
                "grantViewFilter"
        );
        dataSource.putAll(settingMap);

        dataSource.put(StudentDSHandler.STUDENT_SELECTED_LIST, this.getStudentSelectedList());
        dataSource.put(StudentDSHandler.ORDER_ID, getListRepresentId());
    }
    ///////////////////////////////////////служебные методы//////////////////////////////////////////

    /*
     * возвращает список ФИО 'плохих' студентов при переносе в нижнюю таблицу
     */
    protected List<String> getBadSelectedStudentList(List<Student> studentList) {
        List<String> badList = new ArrayList<>();
        for (Student st : studentList)
            if (!st.getStatus().isActive())
                badList.add(st.getPerson().getIdentityCard().getFullFio());

        return badList;
    }

    protected IListObjectModifyDAO getDao() {
        IListObjectByRepresentTypeManager representManager = ListRepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(getTypeRepresentFilter().getCode());
        return representManager.getListObjectModifyDAO();
    }

    ///////////////////////////////////////set && get///////////////////////////////////////////////

    public Long getListRepresentId() {
        return listRepresentId;
    }

    public void setListRepresentId(Long listRepresentId) {
        this.listRepresentId = listRepresentId;
    }

    public RepresentationType getTypeRepresentFilter() {
        return typeRepresentFilter;
    }

    public void setTypeRepresentFilter(RepresentationType typeRepresentFilter) {
        this.typeRepresentFilter = typeRepresentFilter;
    }

    public RepresentationReason getReasonRepresentFilter() {
        return reasonRepresentFilter;
    }

    public void setReasonRepresentFilter(RepresentationReason reasonRepresentFilter) {
        this.reasonRepresentFilter = reasonRepresentFilter;
    }

    public RepresentationBasement getBasementRepresentFilter() {
        return basementRepresentFilter;
    }

    public void setBasementRepresentFilter(
            RepresentationBasement basementRepresentFilter)
    {
        this.basementRepresentFilter = basementRepresentFilter;
    }

    public Date getRepresentationBasementDate() {
        return representationBasementDate;
    }

    public void setRepresentationBasementDate(Date representationBasementDate) {
        this.representationBasementDate = representationBasementDate;
    }

    public String getRepresentationBasementNumber() {
        return representationBasementNumber;
    }

    public void setRepresentationBasementNumber(String representationBasementNumber) {
        this.representationBasementNumber = representationBasementNumber;
    }

    public T getListRepresent() {
        return listRepresent;
    }

    public void setListRepresent(T listRepresent) {
        this.listRepresent = listRepresent;
    }

    public DocListRepresentBasics getRepresentBasics() {
        return representBasics;
    }

    public void setRepresentBasics(DocListRepresentBasics representBasics) {
        this.representBasics = representBasics;
    }

    public List<Student> getStudentSelectedList() {
        return studentSelectedList;
    }

    public void setStudentSelectedList(List<Student> studentSelectedList) {
        this.studentSelectedList = studentSelectedList;
    }

    public List<Student> getStudentChecked() {
        return studentChecked;
    }

    public void setStudentChecked(List<Student> studentChecked) {
        this.studentChecked = studentChecked;
    }

    public boolean isWarning() {
        return warning;
    }

    public void setWarning(boolean warning) {
        this.warning = warning;
    }

    public String getWarningMessage() {
        return warningMessage;
    }

    public void setWarningMessage(String warningMessage) {
        this.warningMessage = warningMessage;
    }

}
