package ru.tandemservice.movestudentrmc.component.menu.PaymentsList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.SimpleMergeIdResolver;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntity;
import ru.tandemservice.unibase.UniBaseUtils;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = (Model) getModel(component);

        model.setSettings(component.getSettings());

        ((IDAO) getDao()).prepare(model);

        prepareDataSource(component);
    }

    protected void prepareDataSource(IBusinessComponent component) {
        final Model model = (Model) getModel(component);

        if (model.getDataSource() != null) return;

        DynamicListDataSource<Wrapper> dataSource = UniBaseUtils.createDataSource(component, ((IDAO) getDao()));

        SimpleMergeIdResolver merge = new SimpleMergeIdResolver("mergeKey");

        dataSource.addColumn(new PublisherLinkColumn("ФИО", "grantEntity." + StudentGrantEntity.student().person().identityCard().fullFio()).setResolver(new IPublisherLinkResolver() {
            @Override
            public String getComponentName(IEntity ientity) {
                return null;
            }

            @Override
            public Object getParameters(IEntity entity) {
                return new UniMap().add(PublisherActivator.PUBLISHER_ID_KEY, ((Wrapper) entity).getGrantEntity().getStudent().getId());
            }
        }).setMergeRowIdResolver(merge));
        dataSource.addColumn(new SimpleColumn("Вид выплаты", "grantEntity." + StudentGrantEntity.view().shortTitle()).setMergeRowIdResolver(merge));
        dataSource.addColumn(new PublisherLinkColumn("Назначение", "grantOrder").setResolver(new IPublisherLinkResolver() {
            @Override
            public String getComponentName(IEntity ientity) {
                return null;
            }

            @Override
            public Object getParameters(IEntity entity) {
                return new UniMap().add(PublisherActivator.PUBLISHER_ID_KEY, ((Wrapper) entity).getGrantOrderId());
            }
        }).setOrderable(false).setMergeRowIdResolver(merge));
        dataSource.addColumn(new PublisherLinkColumn("Отмена", "grantCancelOrder").setResolver(new IPublisherLinkResolver() {
            @Override
            public String getComponentName(IEntity ientity) {
                return null;
            }

            @Override
            public Object getParameters(IEntity entity) {
                return new UniMap().add(PublisherActivator.PUBLISHER_ID_KEY, ((Wrapper) entity).getGrantCancelOrderId());
            }
        }).setOrderable(false).setMergeRowIdResolver(merge));
        dataSource.addColumn(new PublisherLinkColumn("Приостановление", "grantSuspendOrder").setResolver(new IPublisherLinkResolver() {
            @Override
            public String getComponentName(IEntity ientity) {
                return null;
            }

            @Override
            public Object getParameters(IEntity entity) {
                return new UniMap().add(PublisherActivator.PUBLISHER_ID_KEY, ((Wrapper) entity).getGrantSuspendOrderId());
            }
        }).setOrderable(false));
        dataSource.addColumn(new PublisherLinkColumn("Возобновление ", "grantResumeOrder").setResolver(new IPublisherLinkResolver() {
            @Override
            public String getComponentName(IEntity ientity) {
                return null;
            }

            @Override
            public Object getParameters(IEntity entity) {
                return new UniMap().add(PublisherActivator.PUBLISHER_ID_KEY, ((Wrapper) entity).getGrantResumeOrderId());
            }
        }).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("№ зачетной книжки", "grantEntity." + StudentGrantEntity.student().bookNumber()).setMergeRowIdResolver(merge));
        dataSource.addColumn(new SimpleColumn("Год приема", "grantEntity." + StudentGrantEntity.student().entranceYear()).setMergeRowIdResolver(merge));
        dataSource.addColumn(new SimpleColumn("Состояние", "grantEntity." + StudentGrantEntity.student().status().title()).setMergeRowIdResolver(merge));
        dataSource.addColumn(new SimpleColumn("Курс", "grantEntity." + StudentGrantEntity.student().course().title()).setMergeRowIdResolver(merge));
        dataSource.addColumn(new SimpleColumn("Вид затрат", "grantEntity." + StudentGrantEntity.student().compensationType().shortTitle()).setMergeRowIdResolver(merge));
        dataSource.addColumn(new SimpleColumn("Группа", "grantEntity." + StudentGrantEntity.student().group().title()).setMergeRowIdResolver(merge));
        dataSource.addColumn(new SimpleColumn("Категория обучаемого", "grantEntity." + StudentGrantEntity.student().studentCategory().title()).setMergeRowIdResolver(merge));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", "grantEntity." + StudentGrantEntity.student().educationOrgUnit().formativeOrgUnit().title()).setMergeRowIdResolver(merge));
        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", "grantEntity." + StudentGrantEntity.student().educationOrgUnit().educationLevelHighSchool().educationLevel().programSubjectWithCodeIndexAndGenTitle()).setMergeRowIdResolver(merge));
        dataSource.addColumn(new SimpleColumn("Направленность", "grantEntity." + StudentGrantEntity.student().educationOrgUnit().educationLevelHighSchool().educationLevel().programSpecializationTitle()).setMergeRowIdResolver(merge));
        dataSource.addColumn(new SimpleColumn("Квалификация", "grantEntity." + StudentGrantEntity.student().educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().title()).setMergeRowIdResolver(merge));
        dataSource.addColumn(new SimpleColumn("Форма освоения", "grantEntity." + StudentGrantEntity.student().educationOrgUnit().developForm().title()).setMergeRowIdResolver(merge));
        dataSource.addColumn(new SimpleColumn("Условие освоения", "grantEntity." + StudentGrantEntity.student().educationOrgUnit().developCondition().title()).setMergeRowIdResolver(merge));
        dataSource.addColumn(new SimpleColumn("Срок освоения", "grantEntity." + StudentGrantEntity.student().educationOrgUnit().developPeriod().title()).setMergeRowIdResolver(merge));

        model.setDataSource(dataSource);
    }

    public void onClickSearch(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        component.saveSettings();
        model.getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        model.getSettings().clear();
        ((IDAO) getDao()).prepare(model);
        onClickSearch(component);
    }
}
