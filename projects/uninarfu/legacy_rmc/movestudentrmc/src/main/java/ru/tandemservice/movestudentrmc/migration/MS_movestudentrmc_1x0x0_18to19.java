package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;

public class MS_movestudentrmc_1x0x0_18to19 extends IndependentMigrationScript {
    @Override
    public void run(DBTool tool) throws Exception {

		/* Миграция для проставления персоны в УЛ для непроведенных представлений "О смене имени" 
         * RM #2031
		 */

        tool.executeUpdate("UPDATE identitycard_t " +
                                   "SET person_id = pr.person_id " +
                                   "from REPRESENTATION_T r " +
                                   "inner join REPRESENTATIONTYPE_T t " +
                                   "on r.TYPE_ID=t.ID " +
                                   "inner join DOCREPRESENTSTUDENTBASE_T drsb " +
                                   "on drsb.REPRESENTATION_ID=r.ID " +
                                   "inner join personrole_t pr " +
                                   "on pr.id=drsb.STUDENT_ID " +
                                   "inner join DOCREPRESENTSTUDENTIC_T drsic " +
                                   "on drsic.REPRESENTATION_ID=r.id " +
                                   "inner join identitycard_t i " +
                                   "on i.id=drsic.STUDENTIC_ID " +
                                   "where t.CODE_P='changeName' "
        );

		/*зачистка identitycard от строк с person_id=null*/
        if (tool.tableExists("identitycard_t"))
            tool.executeUpdate("delete from identitycard_t where person_id is null");

    }
}
