package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_movestudentrmc_1x0x0_66to67 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.5.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.5.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность listRepresentQualificationAdmission

        // создано свойство acceptOrgUnitType
        {
            // создать колонку
            tool.createColumn("listrepresentqualadm_t", new DBColumn("acceptorgunittype_p", DBType.createVarchar(255)));

        }

        // создано свойство acceptOrgUnit
        {
            // создать колонку
            tool.createColumn("listrepresentqualadm_t", new DBColumn("acceptorgunit_id", DBType.LONG));

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность listRepresentQualificationThemes

        // создано свойство acceptOrgUnitType
        {
            // создать колонку
            tool.createColumn("listrepresentqualthemes_t", new DBColumn("acceptorgunittype_p", DBType.createVarchar(255)));

        }

        // создано свойство acceptOrgUnit
        {
            // создать колонку
            tool.createColumn("listrepresentqualthemes_t", new DBColumn("acceptorgunit_id", DBType.LONG));

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность representChangeQualificationTheme

        // создано свойство acceptOrgUnitType
        {
            // создать колонку
            tool.createColumn("representchngqualtheme_t", new DBColumn("acceptorgunittype_p", DBType.createVarchar(255)));

        }

        // создано свойство acceptOrgUnit
        {
            // создать колонку
            tool.createColumn("representchngqualtheme_t", new DBColumn("acceptorgunit_id", DBType.LONG));

        }


    }
}