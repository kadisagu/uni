package ru.tandemservice.movestudentrmc.dao.check;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import ru.tandemservice.movestudentrmc.entity.NarfuDocument;
import ru.tandemservice.movestudentrmc.entity.PersonNARFU;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractCheckStudentBenefit extends AbstractCheckDAO {

    protected Map<Long, List<PersonBenefit>> personBenefitMap;
    protected Map<Long, PersonNARFU> personNarfuMap;
    protected Map<Long, List<NarfuDocument>> studentDocumentMap;

    protected abstract boolean checkCondition(Student student);

    protected abstract String createError(Student student, boolean value);

    @Override
    public List<String> doCheck(List<Student> list, boolean value) {
        prepareLocalDataCache(list);
        return super.doCheck(list, value);
    }

    protected boolean containDocument(Long studentId, String code) {
        if (!studentDocumentMap.containsKey(studentId))
            return false;

        for (NarfuDocument doc : studentDocumentMap.get(studentId))
            if (doc.getSettings().getDocument().getCode().equals(code))
                return true;

        return false;
    }

    protected NarfuDocument getDocument(Long studentId, String code) {
        if (!studentDocumentMap.containsKey(studentId))
            return null;

        for (NarfuDocument doc : studentDocumentMap.get(studentId))
            if (doc.getSettings().getDocument().getCode().equals(code))
                return doc;

        return null;
    }

    protected boolean containBenefit(Long personId, String code) {
        if (!personBenefitMap.containsKey(personId))
            return false;

        for (PersonBenefit personBenefit : personBenefitMap.get(personId))
            if (personBenefit.getBenefit().getCode().equals(code))
                return true;

        return false;
    }

    protected void prepareLocalDataCache(List<Student> students) {
        personBenefitMap = new HashMap<Long, List<PersonBenefit>>();
        personNarfuMap = new HashMap<>();
        studentDocumentMap = new HashMap<>();

        List<PersonBenefit> benefits = getList(getPersonBenefitDQL(students));

        for (PersonBenefit personBenefit : benefits) {
            Long id = personBenefit.getPerson().getId();
            if (personBenefitMap.containsKey(id)) {
                personBenefitMap.get(id).add(personBenefit);
            }
            else {
                List<PersonBenefit> lst = new ArrayList<>();
                lst.add(personBenefit);
                personBenefitMap.put(id, lst);
            }
        }

        List<PersonNARFU> personsNarfu = getList(getPersonNarfuDQL(students));
        for (PersonNARFU personNARFU : personsNarfu)
            personNarfuMap.put(personNARFU.getPerson().getId(), personNARFU);

        List<NarfuDocument> documents = getList(getDocumentDQL(students));
        for (NarfuDocument doc : documents) {
            Long studentId = doc.getStudent().getId();
            if (studentDocumentMap.containsKey(studentId))
                studentDocumentMap.get(studentId).add(doc);
            else {
                List<NarfuDocument> lst = new ArrayList<>();
                lst.add(doc);
                studentDocumentMap.put(studentId, lst);
            }
        }
    }

    @Override
    protected boolean getCondition(Student student) {
        return checkCondition(student);
    }

    @Override
    protected String getErrorMessage(Student student, boolean value) {
        return createError(student, value);
    }

    private DQLSelectBuilder getPersonBenefitDQL(List<Student> students) {
        List<Long> personIds = new ArrayList<>();

        for (Student student : students)
            personIds.add(student.getPerson().getId());

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(PersonBenefit.class, "pb")
                .where(DQLExpressions.in(
                        DQLExpressions.property(PersonBenefit.person().id().fromAlias("pb")),
                        personIds))
                .column("pb");

        return builder;
    }

    private DQLSelectBuilder getPersonNarfuDQL(List<Student> students) {
        List<Long> personIds = new ArrayList<>();

        for (Student student : students)
            personIds.add(student.getPerson().getId());

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(PersonNARFU.class, "p")
                .where(DQLExpressions.in(
                        DQLExpressions.property(PersonNARFU.person().id().fromAlias("p")),
                        personIds))
                .column("p");

        return builder;
    }

    private DQLSelectBuilder getDocumentDQL(List<Student> students) {

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(NarfuDocument.class, "nd")
                .where(DQLExpressions.in(
                        DQLExpressions.property(NarfuDocument.student().id().fromAlias("nd")),
                        ids(students)))
                .where(DQLExpressions.or(
                        DQLExpressions.isNull(NarfuDocument.tmp().fromAlias("nd")),
                        DQLExpressions.eqValue(
                                DQLExpressions.property(NarfuDocument.tmp().fromAlias("nd")),
                                Boolean.FALSE)));

        return builder;
    }

}