package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.ListCommit;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.DORepresentBaseManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.RepresentationCommitDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentEditUI;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.Add.DORepresentBaseAdd;
import ru.tandemservice.movestudentrmc.docord.IDOObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.DocOrdRepresent;
import ru.tandemservice.movestudentrmc.entity.Representation;

import java.util.List;
import java.util.Map;

@State({
        @Bind(key = DORepresentBaseListCommitUI.STUDENT_LIST_ID, binding = DORepresentBaseListCommitUI.STUDENT_LIST_ID),
        @Bind(key = DORepresentBaseListCommitUI.ORG_UNIT_ID, binding = DORepresentBaseListCommitUI.ORG_UNIT_ID)
})
public class DORepresentBaseListCommitUI extends UIPresenter {

    public static final String STUDENT_LIST_ID = "studentListId";
    public static final String ORG_UNIT_ID = "orgUnitId";

    private BaseSearchListDataSource _contentDS;
    private List<Long> _studentListId;
    private Long _orgUnitId;

    @Override
    public void onComponentRefresh()
    {
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource iuiDataSource) {

        if (DORepresentBaseListCommit.REPRESENTATION_COMMIT_DS.equals(iuiDataSource.getName())) {
            if (_studentListId != null && !_studentListId.isEmpty())
                iuiDataSource.put(RepresentationCommitDSHandler.STUDENT_ID_FILTER, _studentListId.get(0));

            if (_orgUnitId != null)
                iuiDataSource.put(RepresentationCommitDSHandler.ORG_UNIT_ID_FILTER, _orgUnitId);

            Map<String, Object> settingMap = _uiSettings.getAsMap(
                    RepresentationCommitDSHandler.TYPE_REPRESENT_FILTER,
                    RepresentationCommitDSHandler.DATE_COMMIT_FROM_FILTER,
                    RepresentationCommitDSHandler.DATE_FORMATIVE_FROM_FILTER,
                    RepresentationCommitDSHandler.DATE_COMMIT_TO_FILTER,
                    RepresentationCommitDSHandler.DATE_FORMATIVE_TO_FILTER,
                    RepresentationCommitDSHandler.DATE_START_FROM_FILTER,
                    RepresentationCommitDSHandler.DATE_START_TO_FILTER
            );
            iuiDataSource.putAll(settingMap);
        }
    }

    @Override
    public void onAfterDataSourceFetch(IUIDataSource iuiDataSource) {
        super.onAfterDataSourceFetch(iuiDataSource);    //To change body of overridden methods use File | Settings | File Templates.
    }

    public BaseSearchListDataSource getContentDS()
    {
        if (_contentDS == null)
            _contentDS = getConfig().getDataSource(DORepresentBaseListCommit.REPRESENTATION_COMMIT_DS);
        return _contentDS;
    }

    public void onClickAdd()
    {
        _uiActivation.asRegion(DORepresentBaseAdd.class)
                .parameter(AbstractDORepresentEditUI.STUDENT_LIST_ID, _studentListId)
                .activate();
    }

    public void onEditEntityFromList()
    {
        DataWrapper e = getContentDS().getRecordById(getListenerParameterAsLong());
        Representation representBase = ((DocOrdRepresent) e.getWrapped()).getRepresentation();
        String type = representBase.getType().getCode();
        IDOObjectByRepresentTypeManager representManager = DORepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(type);
        _uiActivation.asCurrent(representManager.getEditComponentManager())
                .parameter(AbstractDORepresentEditUI.TYPE_ID, representBase.getType().getId())
                .parameter(AbstractDORepresentEditUI.REPRESENTATION_ID, representBase.getId())
                .parameter(AbstractDORepresentEditUI.STUDENT_LIST_ID, _studentListId)
                .activate();
    }

    public void onPrintExtract()
    {
        DataWrapper e = getContentDS().getRecordById(getListenerParameterAsLong());
        Representation representBase = e.getWrapped();
        Long studentId = getStudentListId().get(0);
        try {

            IDocumentRenderer doc = UtilPrintSupport.extractRenderer(representBase, studentId);
            if (doc != null)
                BusinessComponentUtils.downloadDocument(doc, true);
        }
        catch (Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public void onPrintRepresent()
    {
        DataWrapper e = getContentDS().getRecordById(getListenerParameterAsLong());
        Representation representBase = e.getWrapped();
        try {
            IDocumentRenderer doc = UtilPrintSupport.representRenderer(representBase);
            if (doc != null)
                BusinessComponentUtils.downloadDocument(doc, true);
        }
        catch (Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public Boolean getIsFormative() {

        DataWrapper e = getContentDS().getCurrent();
        Representation representBase = e.getWrapped();
        return !representBase.getState().getCode().equals("1");
    }

    public Boolean getIsOrgUnit() {

        return _orgUnitId != null;
    }

    public Boolean getIsPrintExtract() {

        DataWrapper e = getContentDS().getCurrent();
        Representation representBase = e.getWrapped();
        return representBase.getDocExtract() == null;
    }


    public List<Long> getStudentListId() {
        return _studentListId;
    }

    public void setStudentListId(List<Long> studentListId) {
        _studentListId = studentListId;
    }

    public Long getOrgUnitId() {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId) {
        _orgUnitId = orgUnitId;
    }


}
