package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import ru.tandemservice.movestudentrmc.entity.DocOrdRepresent;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.DocumentOrder;

import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentationTypeCodes;

public class QualificationThemeChangeOrderDSHandler extends DefaultComboDataSourceHandler {
	
	public static final String STUDENT_ID = "studentId";

    public QualificationThemeChangeOrderDSHandler(String ownerId) {

        super(ownerId, DocumentOrder.class, DocumentOrder.number());
    }

    /*
    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep) {
    	
        String filter = ep.input.getComboFilterByValue();
        Long studentId = ep.context.get(STUDENT_ID);

        DQLSelectBuilder dql = new DQLSelectBuilder()
        		.fromEntity(DocRepresentStudentBase.class, "docrep")
        		.joinEntity("docrep", DQLJoinType.inner, DocOrdRepresent.class, "docordrep", eq(property(DocOrdRepresent.representation().id().fromAlias("docordrep")), property(DocRepresentStudentBase.representation().id().fromAlias("docrep"))))
        		.where(eq(property(DocRepresentStudentBase.student().id().fromAlias("docrep")), value(studentId)))
        		.where(eq(property(DocRepresentStudentBase.representation().type().code().fromAlias("docrep")),value(RepresentationTypeCodes.CHANGE_QUALIFICATION_THEME)))
        		.where(eq(property(DocOrdRepresent.order().state().code().fromAlias("docordrep")), value("5")))
        		.column(property(DocOrdRepresent.order().fromAlias("docordrep")))
   		;
        
        ep.dqlBuilder.where(in(DQLExpressions.property(DocumentOrder.id().fromAlias("e")), dql.buildQuery()));

    }
    */
    
    @Override
    protected ExecutionParameters<DSInput, DSOutput> prepareExecutionParameters(DSInput input, ExecutionContext context) {
		Class entityClass = this.getEntityClass(context);
		DSOutput output = new DSOutput(input);
		DQLSelectBuilder builder = new DQLSelectBuilder();
		builder.column(DQLExpressions.property("e"));
		builder.fromEntity(entityClass.getName(), "e");
		
		Long studentId = context.get(STUDENT_ID);
		
        DQLSelectBuilder dql = new DQLSelectBuilder()
        		.fromEntity(DocRepresentStudentBase.class, "docrep")
        		.joinEntity("docrep", DQLJoinType.inner, DocOrdRepresent.class, "docordrep", eq(property(DocOrdRepresent.representation().id().fromAlias("docordrep")), property(DocRepresentStudentBase.representation().id().fromAlias("docrep"))))
        		.where(eq(property(DocRepresentStudentBase.student().id().fromAlias("docrep")), value(studentId)))
        		.where(eq(property(DocRepresentStudentBase.representation().type().code().fromAlias("docrep")), value(RepresentationTypeCodes.CHANGE_QUALIFICATION_THEME)))
        		.where(eq(property(DocOrdRepresent.order().state().code().fromAlias("docordrep")), value("5")))
        		.column(property(DocOrdRepresent.order().fromAlias("docordrep")))
   		;
        
        builder.where(in(DQLExpressions.property(DocumentOrder.id().fromAlias("e")), dql.buildQuery()));
		
		return new ExecutionParameters(input, output, context, builder);
    }
}
