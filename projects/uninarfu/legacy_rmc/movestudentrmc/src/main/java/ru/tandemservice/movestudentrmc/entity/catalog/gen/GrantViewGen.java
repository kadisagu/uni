package ru.tandemservice.movestudentrmc.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Виды стипендий/выплат
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class GrantViewGen extends EntityBase
 implements INaturalIdentifiable<GrantViewGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.catalog.GrantView";
    public static final String ENTITY_NAME = "grantView";
    public static final int VERSION_HASH = -477681038;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_TITLE = "title";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_GENITIVE = "genitive";
    public static final String P_DATIVE = "dative";
    public static final String P_ACCUSATIVE = "accusative";
    public static final String P_INSTRUMENTAL = "instrumental";
    public static final String P_PREPOSITIONAL = "prepositional";
    public static final String P_USE = "use";
    public static final String P_SUSPEND = "suspend";

    private String _code;     // Системный код
    private String _title; 
    private String _shortTitle; 
    private String _genitive; 
    private String _dative; 
    private String _accusative; 
    private String _instrumental; 
    private String _prepositional; 
    private boolean _use; 
    private boolean _suspend = false;     // Можно приостанавливать выплату

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return  Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title  Свойство не может быть null и должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle 
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getGenitive()
    {
        return _genitive;
    }

    /**
     * @param genitive 
     */
    public void setGenitive(String genitive)
    {
        dirty(_genitive, genitive);
        _genitive = genitive;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getDative()
    {
        return _dative;
    }

    /**
     * @param dative 
     */
    public void setDative(String dative)
    {
        dirty(_dative, dative);
        _dative = dative;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getAccusative()
    {
        return _accusative;
    }

    /**
     * @param accusative 
     */
    public void setAccusative(String accusative)
    {
        dirty(_accusative, accusative);
        _accusative = accusative;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getInstrumental()
    {
        return _instrumental;
    }

    /**
     * @param instrumental 
     */
    public void setInstrumental(String instrumental)
    {
        dirty(_instrumental, instrumental);
        _instrumental = instrumental;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getPrepositional()
    {
        return _prepositional;
    }

    /**
     * @param prepositional 
     */
    public void setPrepositional(String prepositional)
    {
        dirty(_prepositional, prepositional);
        _prepositional = prepositional;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public boolean isUse()
    {
        return _use;
    }

    /**
     * @param use  Свойство не может быть null.
     */
    public void setUse(boolean use)
    {
        dirty(_use, use);
        _use = use;
    }

    /**
     * @return Можно приостанавливать выплату. Свойство не может быть null.
     */
    @NotNull
    public boolean isSuspend()
    {
        return _suspend;
    }

    /**
     * @param suspend Можно приостанавливать выплату. Свойство не может быть null.
     */
    public void setSuspend(boolean suspend)
    {
        dirty(_suspend, suspend);
        _suspend = suspend;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof GrantViewGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((GrantView)another).getCode());
            }
            setTitle(((GrantView)another).getTitle());
            setShortTitle(((GrantView)another).getShortTitle());
            setGenitive(((GrantView)another).getGenitive());
            setDative(((GrantView)another).getDative());
            setAccusative(((GrantView)another).getAccusative());
            setInstrumental(((GrantView)another).getInstrumental());
            setPrepositional(((GrantView)another).getPrepositional());
            setUse(((GrantView)another).isUse());
            setSuspend(((GrantView)another).isSuspend());
        }
    }

    public INaturalId<GrantViewGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<GrantViewGen>
    {
        private static final String PROXY_NAME = "GrantViewNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof GrantViewGen.NaturalId) ) return false;

            GrantViewGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends GrantViewGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) GrantView.class;
        }

        public T newInstance()
        {
            return (T) new GrantView();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "title":
                    return obj.getTitle();
                case "shortTitle":
                    return obj.getShortTitle();
                case "genitive":
                    return obj.getGenitive();
                case "dative":
                    return obj.getDative();
                case "accusative":
                    return obj.getAccusative();
                case "instrumental":
                    return obj.getInstrumental();
                case "prepositional":
                    return obj.getPrepositional();
                case "use":
                    return obj.isUse();
                case "suspend":
                    return obj.isSuspend();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "genitive":
                    obj.setGenitive((String) value);
                    return;
                case "dative":
                    obj.setDative((String) value);
                    return;
                case "accusative":
                    obj.setAccusative((String) value);
                    return;
                case "instrumental":
                    obj.setInstrumental((String) value);
                    return;
                case "prepositional":
                    obj.setPrepositional((String) value);
                    return;
                case "use":
                    obj.setUse((Boolean) value);
                    return;
                case "suspend":
                    obj.setSuspend((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "title":
                        return true;
                case "shortTitle":
                        return true;
                case "genitive":
                        return true;
                case "dative":
                        return true;
                case "accusative":
                        return true;
                case "instrumental":
                        return true;
                case "prepositional":
                        return true;
                case "use":
                        return true;
                case "suspend":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "title":
                    return true;
                case "shortTitle":
                    return true;
                case "genitive":
                    return true;
                case "dative":
                    return true;
                case "accusative":
                    return true;
                case "instrumental":
                    return true;
                case "prepositional":
                    return true;
                case "use":
                    return true;
                case "suspend":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "title":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "genitive":
                    return String.class;
                case "dative":
                    return String.class;
                case "accusative":
                    return String.class;
                case "instrumental":
                    return String.class;
                case "prepositional":
                    return String.class;
                case "use":
                    return Boolean.class;
                case "suspend":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<GrantView> _dslPath = new Path<GrantView>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "GrantView");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.GrantView#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return  Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.GrantView#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.catalog.GrantView#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.catalog.GrantView#getGenitive()
     */
    public static PropertyPath<String> genitive()
    {
        return _dslPath.genitive();
    }

    /**
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.catalog.GrantView#getDative()
     */
    public static PropertyPath<String> dative()
    {
        return _dslPath.dative();
    }

    /**
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.catalog.GrantView#getAccusative()
     */
    public static PropertyPath<String> accusative()
    {
        return _dslPath.accusative();
    }

    /**
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.catalog.GrantView#getInstrumental()
     */
    public static PropertyPath<String> instrumental()
    {
        return _dslPath.instrumental();
    }

    /**
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.catalog.GrantView#getPrepositional()
     */
    public static PropertyPath<String> prepositional()
    {
        return _dslPath.prepositional();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.GrantView#isUse()
     */
    public static PropertyPath<Boolean> use()
    {
        return _dslPath.use();
    }

    /**
     * @return Можно приостанавливать выплату. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.GrantView#isSuspend()
     */
    public static PropertyPath<Boolean> suspend()
    {
        return _dslPath.suspend();
    }

    public static class Path<E extends GrantView> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _title;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<String> _genitive;
        private PropertyPath<String> _dative;
        private PropertyPath<String> _accusative;
        private PropertyPath<String> _instrumental;
        private PropertyPath<String> _prepositional;
        private PropertyPath<Boolean> _use;
        private PropertyPath<Boolean> _suspend;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.GrantView#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(GrantViewGen.P_CODE, this);
            return _code;
        }

    /**
     * @return  Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.GrantView#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(GrantViewGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.catalog.GrantView#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(GrantViewGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.catalog.GrantView#getGenitive()
     */
        public PropertyPath<String> genitive()
        {
            if(_genitive == null )
                _genitive = new PropertyPath<String>(GrantViewGen.P_GENITIVE, this);
            return _genitive;
        }

    /**
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.catalog.GrantView#getDative()
     */
        public PropertyPath<String> dative()
        {
            if(_dative == null )
                _dative = new PropertyPath<String>(GrantViewGen.P_DATIVE, this);
            return _dative;
        }

    /**
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.catalog.GrantView#getAccusative()
     */
        public PropertyPath<String> accusative()
        {
            if(_accusative == null )
                _accusative = new PropertyPath<String>(GrantViewGen.P_ACCUSATIVE, this);
            return _accusative;
        }

    /**
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.catalog.GrantView#getInstrumental()
     */
        public PropertyPath<String> instrumental()
        {
            if(_instrumental == null )
                _instrumental = new PropertyPath<String>(GrantViewGen.P_INSTRUMENTAL, this);
            return _instrumental;
        }

    /**
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.catalog.GrantView#getPrepositional()
     */
        public PropertyPath<String> prepositional()
        {
            if(_prepositional == null )
                _prepositional = new PropertyPath<String>(GrantViewGen.P_PREPOSITIONAL, this);
            return _prepositional;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.GrantView#isUse()
     */
        public PropertyPath<Boolean> use()
        {
            if(_use == null )
                _use = new PropertyPath<Boolean>(GrantViewGen.P_USE, this);
            return _use;
        }

    /**
     * @return Можно приостанавливать выплату. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.GrantView#isSuspend()
     */
        public PropertyPath<Boolean> suspend()
        {
            if(_suspend == null )
                _suspend = new PropertyPath<Boolean>(GrantViewGen.P_SUSPEND, this);
            return _suspend;
        }

        public Class getEntityClass()
        {
            return GrantView.class;
        }

        public String getEntityName()
        {
            return "grantView";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
