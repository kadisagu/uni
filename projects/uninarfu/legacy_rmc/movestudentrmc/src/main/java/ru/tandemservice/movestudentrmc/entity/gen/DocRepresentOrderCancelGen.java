package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.DocRepresentOrderCancel;
import ru.tandemservice.movestudentrmc.entity.IAbstractOrder;
import ru.tandemservice.movestudentrmc.entity.IAbstractRepresentation;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность-связь 'Представление'-'Отменяемый приказ'
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DocRepresentOrderCancelGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.DocRepresentOrderCancel";
    public static final String ENTITY_NAME = "docRepresentOrderCancel";
    public static final int VERSION_HASH = 1888207435;
    private static IEntityMeta ENTITY_META;

    public static final String L_REPRESENTATION = "representation";
    public static final String L_ORDER = "order";
    public static final String P_TITLE = "title";
    public static final String L_GRANT_VIEW = "grantView";

    private IAbstractRepresentation _representation;     // Представление
    private IAbstractOrder _order;     // Приказ
    private String _title;     // Название приказа
    private GrantView _grantView;     // Вид стипендии

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Представление. Свойство не может быть null.
     */
    @NotNull
    public IAbstractRepresentation getRepresentation()
    {
        return _representation;
    }

    /**
     * @param representation Представление. Свойство не может быть null.
     */
    public void setRepresentation(IAbstractRepresentation representation)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && representation!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IAbstractRepresentation.class);
            IEntityMeta actual =  representation instanceof IEntity ? EntityRuntime.getMeta((IEntity) representation) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_representation, representation);
        _representation = representation;
    }

    /**
     * @return Приказ.
     */
    public IAbstractOrder getOrder()
    {
        return _order;
    }

    /**
     * @param order Приказ.
     */
    public void setOrder(IAbstractOrder order)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && order!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IAbstractOrder.class);
            IEntityMeta actual =  order instanceof IEntity ? EntityRuntime.getMeta((IEntity) order) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_order, order);
        _order = order;
    }

    /**
     * @return Название приказа.
     */
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название приказа.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Вид стипендии.
     */
    public GrantView getGrantView()
    {
        return _grantView;
    }

    /**
     * @param grantView Вид стипендии.
     */
    public void setGrantView(GrantView grantView)
    {
        dirty(_grantView, grantView);
        _grantView = grantView;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DocRepresentOrderCancelGen)
        {
            setRepresentation(((DocRepresentOrderCancel)another).getRepresentation());
            setOrder(((DocRepresentOrderCancel)another).getOrder());
            setTitle(((DocRepresentOrderCancel)another).getTitle());
            setGrantView(((DocRepresentOrderCancel)another).getGrantView());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DocRepresentOrderCancelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DocRepresentOrderCancel.class;
        }

        public T newInstance()
        {
            return (T) new DocRepresentOrderCancel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "representation":
                    return obj.getRepresentation();
                case "order":
                    return obj.getOrder();
                case "title":
                    return obj.getTitle();
                case "grantView":
                    return obj.getGrantView();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "representation":
                    obj.setRepresentation((IAbstractRepresentation) value);
                    return;
                case "order":
                    obj.setOrder((IAbstractOrder) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "grantView":
                    obj.setGrantView((GrantView) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "representation":
                        return true;
                case "order":
                        return true;
                case "title":
                        return true;
                case "grantView":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "representation":
                    return true;
                case "order":
                    return true;
                case "title":
                    return true;
                case "grantView":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "representation":
                    return IAbstractRepresentation.class;
                case "order":
                    return IAbstractOrder.class;
                case "title":
                    return String.class;
                case "grantView":
                    return GrantView.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DocRepresentOrderCancel> _dslPath = new Path<DocRepresentOrderCancel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DocRepresentOrderCancel");
    }
            

    /**
     * @return Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentOrderCancel#getRepresentation()
     */
    public static IAbstractRepresentationGen.Path<IAbstractRepresentation> representation()
    {
        return _dslPath.representation();
    }

    /**
     * @return Приказ.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentOrderCancel#getOrder()
     */
    public static IAbstractOrderGen.Path<IAbstractOrder> order()
    {
        return _dslPath.order();
    }

    /**
     * @return Название приказа.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentOrderCancel#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Вид стипендии.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentOrderCancel#getGrantView()
     */
    public static GrantView.Path<GrantView> grantView()
    {
        return _dslPath.grantView();
    }

    public static class Path<E extends DocRepresentOrderCancel> extends EntityPath<E>
    {
        private IAbstractRepresentationGen.Path<IAbstractRepresentation> _representation;
        private IAbstractOrderGen.Path<IAbstractOrder> _order;
        private PropertyPath<String> _title;
        private GrantView.Path<GrantView> _grantView;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentOrderCancel#getRepresentation()
     */
        public IAbstractRepresentationGen.Path<IAbstractRepresentation> representation()
        {
            if(_representation == null )
                _representation = new IAbstractRepresentationGen.Path<IAbstractRepresentation>(L_REPRESENTATION, this);
            return _representation;
        }

    /**
     * @return Приказ.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentOrderCancel#getOrder()
     */
        public IAbstractOrderGen.Path<IAbstractOrder> order()
        {
            if(_order == null )
                _order = new IAbstractOrderGen.Path<IAbstractOrder>(L_ORDER, this);
            return _order;
        }

    /**
     * @return Название приказа.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentOrderCancel#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(DocRepresentOrderCancelGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Вид стипендии.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentOrderCancel#getGrantView()
     */
        public GrantView.Path<GrantView> grantView()
        {
            if(_grantView == null )
                _grantView = new GrantView.Path<GrantView>(L_GRANT_VIEW, this);
            return _grantView;
        }

        public Class getEntityClass()
        {
            return DocRepresentOrderCancel.class;
        }

        public String getEntityName()
        {
            return "docRepresentOrderCancel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
