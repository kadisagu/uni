package ru.tandemservice.movestudentrmc.component.settings.RepresentationBasementToDocument.RepresentationBasementToDocumentSettingsPub;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.uni.component.settings.abstractRelation.AbstractRelationPub.AbstractRelationPubController;

public class Controller extends AbstractRelationPubController<IDAO, Model> {

    @Override
    public void onClickDeleteRelation(IBusinessComponent component) {
        super.onClickDeleteRelation(component);
        component.refresh();
    }


    @Override
    protected String getDeleteRowPermissionKey() {
        return null;
    }

    @Override
    protected String getRelationAddingComponentName() {
        return "ru.tandemservice.movestudentrmc.component.settings.RepresentationBasementToDocument.RepresentationBasementToDocumentSettingsAdd";
    }

}
