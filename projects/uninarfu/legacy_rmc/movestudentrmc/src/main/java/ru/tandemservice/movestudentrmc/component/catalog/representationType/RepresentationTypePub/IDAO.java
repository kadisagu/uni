package ru.tandemservice.movestudentrmc.component.catalog.representationType.RepresentationTypePub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;

public interface IDAO extends IDefaultCatalogPubDAO<RepresentationType, Model> {
    void updateInUse(Model model, Long id);

    void updateInstruction(Model model, Long id);

    void updateGrouping(Model model, Long id);

    void updateListRepresentation(Model model, Long id);

    void updatePrintAttachment(Model model, Long id);

    void updatePrintDeclaration(Model model, Long id);

//	void updateUseGrant(Model model, Long id);

    void updateCheck(Model model, Long id);

}
