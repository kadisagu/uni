package ru.tandemservice.movestudentrmc.base.bo.DORepresentChangeQualificationTheme;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentChangeQualificationTheme.logic.DORepresentChangeQualificationThemeDAO;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentChangeQualificationTheme.ui.Edit.DORepresentChangeQualificationThemeEdit;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentChangeQualificationTheme.ui.View.DORepresentChangeQualificationThemeView;
import ru.tandemservice.movestudentrmc.docord.IDOObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IDOObjectModifyDAO;

@Configuration
public class DORepresentChangeQualificationThemeManager extends BusinessObjectManager implements IDOObjectByRepresentTypeManager {

    public static DORepresentChangeQualificationThemeManager instance()
    {
        return instance(DORepresentChangeQualificationThemeManager.class);
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return DORepresentChangeQualificationThemeEdit.class;
    }

    @Bean
    public IDOObjectModifyDAO getDOObjectModifyDAO() {
        return new DORepresentChangeQualificationThemeDAO();
    }

    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return DORepresentChangeQualificationThemeView.class;
    }
}
