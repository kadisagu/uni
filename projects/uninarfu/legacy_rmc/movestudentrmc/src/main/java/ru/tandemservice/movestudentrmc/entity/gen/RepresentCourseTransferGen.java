package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudentrmc.entity.RepresentCourseTransfer;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * О переводе на след курс
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RepresentCourseTransferGen extends Representation
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.RepresentCourseTransfer";
    public static final String ENTITY_NAME = "representCourseTransfer";
    public static final int VERSION_HASH = -385277464;
    private static IEntityMeta ENTITY_META;

    public static final String L_COURSE = "course";
    public static final String L_GROUP = "group";
    public static final String L_WORK_PLAN = "workPlan";
    public static final String L_WORK_PLAN_REL = "workPlanRel";
    public static final String L_EDUCATION_ORG_UNIT = "educationOrgUnit";
    public static final String P_OLD_COURSE_ID = "oldCourseId";
    public static final String P_OLD_GROUP_ID = "oldGroupId";
    public static final String P_OLD_EDUCATION_ORG_UNIT_ID = "oldEducationOrgUnitId";
    public static final String P_OLD_COMPENSATION_TYPE_ID = "oldCompensationTypeId";
    public static final String P_OLD_EDU_PLAN_REL_ID = "oldEduPlanRelId";
    public static final String P_OLD_WORK_PLAN_REL_IDS = "oldWorkPlanRelIds";
    public static final String P_CONDITIONALLY = "conditionally";
    public static final String P_DEADLINE_DATE = "deadlineDate";

    private Course _course;     // Курс
    private Group _group;     // Группа
    private EppWorkPlan _workPlan;     // РУП
    private EppStudent2WorkPlan _workPlanRel;     // Связь студент - РУП
    private EducationOrgUnit _educationOrgUnit;     // Новое направление подготовки (специальность)
    private Long _oldCourseId;     // Курс (старый)
    private Long _oldGroupId;     // Группа (старая)
    private Long _oldEducationOrgUnitId; 
    private Long _oldCompensationTypeId;     // Тип возмещения затрат (старый)
    private Long _oldEduPlanRelId;     // Связь студент - УП (старая)
    private String _oldWorkPlanRelIds;     // Активные связи студент - РУП (старая)
    private boolean _conditionally = false;     // Условный перевод
    private Date _deadlineDate;     // Дата ликвидации академической задолженности

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return РУП.
     */
    public EppWorkPlan getWorkPlan()
    {
        return _workPlan;
    }

    /**
     * @param workPlan РУП.
     */
    public void setWorkPlan(EppWorkPlan workPlan)
    {
        dirty(_workPlan, workPlan);
        _workPlan = workPlan;
    }

    /**
     * @return Связь студент - РУП.
     */
    public EppStudent2WorkPlan getWorkPlanRel()
    {
        return _workPlanRel;
    }

    /**
     * @param workPlanRel Связь студент - РУП.
     */
    public void setWorkPlanRel(EppStudent2WorkPlan workPlanRel)
    {
        dirty(_workPlanRel, workPlanRel);
        _workPlanRel = workPlanRel;
    }

    /**
     * @return Новое направление подготовки (специальность).
     */
    public EducationOrgUnit getEducationOrgUnit()
    {
        return _educationOrgUnit;
    }

    /**
     * @param educationOrgUnit Новое направление подготовки (специальность).
     */
    public void setEducationOrgUnit(EducationOrgUnit educationOrgUnit)
    {
        dirty(_educationOrgUnit, educationOrgUnit);
        _educationOrgUnit = educationOrgUnit;
    }

    /**
     * @return Курс (старый).
     */
    public Long getOldCourseId()
    {
        return _oldCourseId;
    }

    /**
     * @param oldCourseId Курс (старый).
     */
    public void setOldCourseId(Long oldCourseId)
    {
        dirty(_oldCourseId, oldCourseId);
        _oldCourseId = oldCourseId;
    }

    /**
     * @return Группа (старая).
     */
    public Long getOldGroupId()
    {
        return _oldGroupId;
    }

    /**
     * @param oldGroupId Группа (старая).
     */
    public void setOldGroupId(Long oldGroupId)
    {
        dirty(_oldGroupId, oldGroupId);
        _oldGroupId = oldGroupId;
    }

    /**
     * @return 
     */
    public Long getOldEducationOrgUnitId()
    {
        return _oldEducationOrgUnitId;
    }

    /**
     * @param oldEducationOrgUnitId 
     */
    public void setOldEducationOrgUnitId(Long oldEducationOrgUnitId)
    {
        dirty(_oldEducationOrgUnitId, oldEducationOrgUnitId);
        _oldEducationOrgUnitId = oldEducationOrgUnitId;
    }

    /**
     * @return Тип возмещения затрат (старый).
     */
    public Long getOldCompensationTypeId()
    {
        return _oldCompensationTypeId;
    }

    /**
     * @param oldCompensationTypeId Тип возмещения затрат (старый).
     */
    public void setOldCompensationTypeId(Long oldCompensationTypeId)
    {
        dirty(_oldCompensationTypeId, oldCompensationTypeId);
        _oldCompensationTypeId = oldCompensationTypeId;
    }

    /**
     * @return Связь студент - УП (старая).
     */
    public Long getOldEduPlanRelId()
    {
        return _oldEduPlanRelId;
    }

    /**
     * @param oldEduPlanRelId Связь студент - УП (старая).
     */
    public void setOldEduPlanRelId(Long oldEduPlanRelId)
    {
        dirty(_oldEduPlanRelId, oldEduPlanRelId);
        _oldEduPlanRelId = oldEduPlanRelId;
    }

    /**
     * @return Активные связи студент - РУП (старая).
     */
    @Length(max=255)
    public String getOldWorkPlanRelIds()
    {
        return _oldWorkPlanRelIds;
    }

    /**
     * @param oldWorkPlanRelIds Активные связи студент - РУП (старая).
     */
    public void setOldWorkPlanRelIds(String oldWorkPlanRelIds)
    {
        dirty(_oldWorkPlanRelIds, oldWorkPlanRelIds);
        _oldWorkPlanRelIds = oldWorkPlanRelIds;
    }

    /**
     * @return Условный перевод. Свойство не может быть null.
     */
    @NotNull
    public boolean isConditionally()
    {
        return _conditionally;
    }

    /**
     * @param conditionally Условный перевод. Свойство не может быть null.
     */
    public void setConditionally(boolean conditionally)
    {
        dirty(_conditionally, conditionally);
        _conditionally = conditionally;
    }

    /**
     * @return Дата ликвидации академической задолженности.
     */
    public Date getDeadlineDate()
    {
        return _deadlineDate;
    }

    /**
     * @param deadlineDate Дата ликвидации академической задолженности.
     */
    public void setDeadlineDate(Date deadlineDate)
    {
        dirty(_deadlineDate, deadlineDate);
        _deadlineDate = deadlineDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof RepresentCourseTransferGen)
        {
            setCourse(((RepresentCourseTransfer)another).getCourse());
            setGroup(((RepresentCourseTransfer)another).getGroup());
            setWorkPlan(((RepresentCourseTransfer)another).getWorkPlan());
            setWorkPlanRel(((RepresentCourseTransfer)another).getWorkPlanRel());
            setEducationOrgUnit(((RepresentCourseTransfer)another).getEducationOrgUnit());
            setOldCourseId(((RepresentCourseTransfer)another).getOldCourseId());
            setOldGroupId(((RepresentCourseTransfer)another).getOldGroupId());
            setOldEducationOrgUnitId(((RepresentCourseTransfer)another).getOldEducationOrgUnitId());
            setOldCompensationTypeId(((RepresentCourseTransfer)another).getOldCompensationTypeId());
            setOldEduPlanRelId(((RepresentCourseTransfer)another).getOldEduPlanRelId());
            setOldWorkPlanRelIds(((RepresentCourseTransfer)another).getOldWorkPlanRelIds());
            setConditionally(((RepresentCourseTransfer)another).isConditionally());
            setDeadlineDate(((RepresentCourseTransfer)another).getDeadlineDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RepresentCourseTransferGen> extends Representation.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RepresentCourseTransfer.class;
        }

        public T newInstance()
        {
            return (T) new RepresentCourseTransfer();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
                case "workPlan":
                    return obj.getWorkPlan();
                case "workPlanRel":
                    return obj.getWorkPlanRel();
                case "educationOrgUnit":
                    return obj.getEducationOrgUnit();
                case "oldCourseId":
                    return obj.getOldCourseId();
                case "oldGroupId":
                    return obj.getOldGroupId();
                case "oldEducationOrgUnitId":
                    return obj.getOldEducationOrgUnitId();
                case "oldCompensationTypeId":
                    return obj.getOldCompensationTypeId();
                case "oldEduPlanRelId":
                    return obj.getOldEduPlanRelId();
                case "oldWorkPlanRelIds":
                    return obj.getOldWorkPlanRelIds();
                case "conditionally":
                    return obj.isConditionally();
                case "deadlineDate":
                    return obj.getDeadlineDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "workPlan":
                    obj.setWorkPlan((EppWorkPlan) value);
                    return;
                case "workPlanRel":
                    obj.setWorkPlanRel((EppStudent2WorkPlan) value);
                    return;
                case "educationOrgUnit":
                    obj.setEducationOrgUnit((EducationOrgUnit) value);
                    return;
                case "oldCourseId":
                    obj.setOldCourseId((Long) value);
                    return;
                case "oldGroupId":
                    obj.setOldGroupId((Long) value);
                    return;
                case "oldEducationOrgUnitId":
                    obj.setOldEducationOrgUnitId((Long) value);
                    return;
                case "oldCompensationTypeId":
                    obj.setOldCompensationTypeId((Long) value);
                    return;
                case "oldEduPlanRelId":
                    obj.setOldEduPlanRelId((Long) value);
                    return;
                case "oldWorkPlanRelIds":
                    obj.setOldWorkPlanRelIds((String) value);
                    return;
                case "conditionally":
                    obj.setConditionally((Boolean) value);
                    return;
                case "deadlineDate":
                    obj.setDeadlineDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                        return true;
                case "group":
                        return true;
                case "workPlan":
                        return true;
                case "workPlanRel":
                        return true;
                case "educationOrgUnit":
                        return true;
                case "oldCourseId":
                        return true;
                case "oldGroupId":
                        return true;
                case "oldEducationOrgUnitId":
                        return true;
                case "oldCompensationTypeId":
                        return true;
                case "oldEduPlanRelId":
                        return true;
                case "oldWorkPlanRelIds":
                        return true;
                case "conditionally":
                        return true;
                case "deadlineDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return true;
                case "group":
                    return true;
                case "workPlan":
                    return true;
                case "workPlanRel":
                    return true;
                case "educationOrgUnit":
                    return true;
                case "oldCourseId":
                    return true;
                case "oldGroupId":
                    return true;
                case "oldEducationOrgUnitId":
                    return true;
                case "oldCompensationTypeId":
                    return true;
                case "oldEduPlanRelId":
                    return true;
                case "oldWorkPlanRelIds":
                    return true;
                case "conditionally":
                    return true;
                case "deadlineDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return Course.class;
                case "group":
                    return Group.class;
                case "workPlan":
                    return EppWorkPlan.class;
                case "workPlanRel":
                    return EppStudent2WorkPlan.class;
                case "educationOrgUnit":
                    return EducationOrgUnit.class;
                case "oldCourseId":
                    return Long.class;
                case "oldGroupId":
                    return Long.class;
                case "oldEducationOrgUnitId":
                    return Long.class;
                case "oldCompensationTypeId":
                    return Long.class;
                case "oldEduPlanRelId":
                    return Long.class;
                case "oldWorkPlanRelIds":
                    return String.class;
                case "conditionally":
                    return Boolean.class;
                case "deadlineDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RepresentCourseTransfer> _dslPath = new Path<RepresentCourseTransfer>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RepresentCourseTransfer");
    }
            

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentCourseTransfer#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentCourseTransfer#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return РУП.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentCourseTransfer#getWorkPlan()
     */
    public static EppWorkPlan.Path<EppWorkPlan> workPlan()
    {
        return _dslPath.workPlan();
    }

    /**
     * @return Связь студент - РУП.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentCourseTransfer#getWorkPlanRel()
     */
    public static EppStudent2WorkPlan.Path<EppStudent2WorkPlan> workPlanRel()
    {
        return _dslPath.workPlanRel();
    }

    /**
     * @return Новое направление подготовки (специальность).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentCourseTransfer#getEducationOrgUnit()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
    {
        return _dslPath.educationOrgUnit();
    }

    /**
     * @return Курс (старый).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentCourseTransfer#getOldCourseId()
     */
    public static PropertyPath<Long> oldCourseId()
    {
        return _dslPath.oldCourseId();
    }

    /**
     * @return Группа (старая).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentCourseTransfer#getOldGroupId()
     */
    public static PropertyPath<Long> oldGroupId()
    {
        return _dslPath.oldGroupId();
    }

    /**
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.RepresentCourseTransfer#getOldEducationOrgUnitId()
     */
    public static PropertyPath<Long> oldEducationOrgUnitId()
    {
        return _dslPath.oldEducationOrgUnitId();
    }

    /**
     * @return Тип возмещения затрат (старый).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentCourseTransfer#getOldCompensationTypeId()
     */
    public static PropertyPath<Long> oldCompensationTypeId()
    {
        return _dslPath.oldCompensationTypeId();
    }

    /**
     * @return Связь студент - УП (старая).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentCourseTransfer#getOldEduPlanRelId()
     */
    public static PropertyPath<Long> oldEduPlanRelId()
    {
        return _dslPath.oldEduPlanRelId();
    }

    /**
     * @return Активные связи студент - РУП (старая).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentCourseTransfer#getOldWorkPlanRelIds()
     */
    public static PropertyPath<String> oldWorkPlanRelIds()
    {
        return _dslPath.oldWorkPlanRelIds();
    }

    /**
     * @return Условный перевод. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentCourseTransfer#isConditionally()
     */
    public static PropertyPath<Boolean> conditionally()
    {
        return _dslPath.conditionally();
    }

    /**
     * @return Дата ликвидации академической задолженности.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentCourseTransfer#getDeadlineDate()
     */
    public static PropertyPath<Date> deadlineDate()
    {
        return _dslPath.deadlineDate();
    }

    public static class Path<E extends RepresentCourseTransfer> extends Representation.Path<E>
    {
        private Course.Path<Course> _course;
        private Group.Path<Group> _group;
        private EppWorkPlan.Path<EppWorkPlan> _workPlan;
        private EppStudent2WorkPlan.Path<EppStudent2WorkPlan> _workPlanRel;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnit;
        private PropertyPath<Long> _oldCourseId;
        private PropertyPath<Long> _oldGroupId;
        private PropertyPath<Long> _oldEducationOrgUnitId;
        private PropertyPath<Long> _oldCompensationTypeId;
        private PropertyPath<Long> _oldEduPlanRelId;
        private PropertyPath<String> _oldWorkPlanRelIds;
        private PropertyPath<Boolean> _conditionally;
        private PropertyPath<Date> _deadlineDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentCourseTransfer#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentCourseTransfer#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return РУП.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentCourseTransfer#getWorkPlan()
     */
        public EppWorkPlan.Path<EppWorkPlan> workPlan()
        {
            if(_workPlan == null )
                _workPlan = new EppWorkPlan.Path<EppWorkPlan>(L_WORK_PLAN, this);
            return _workPlan;
        }

    /**
     * @return Связь студент - РУП.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentCourseTransfer#getWorkPlanRel()
     */
        public EppStudent2WorkPlan.Path<EppStudent2WorkPlan> workPlanRel()
        {
            if(_workPlanRel == null )
                _workPlanRel = new EppStudent2WorkPlan.Path<EppStudent2WorkPlan>(L_WORK_PLAN_REL, this);
            return _workPlanRel;
        }

    /**
     * @return Новое направление подготовки (специальность).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentCourseTransfer#getEducationOrgUnit()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
        {
            if(_educationOrgUnit == null )
                _educationOrgUnit = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT, this);
            return _educationOrgUnit;
        }

    /**
     * @return Курс (старый).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentCourseTransfer#getOldCourseId()
     */
        public PropertyPath<Long> oldCourseId()
        {
            if(_oldCourseId == null )
                _oldCourseId = new PropertyPath<Long>(RepresentCourseTransferGen.P_OLD_COURSE_ID, this);
            return _oldCourseId;
        }

    /**
     * @return Группа (старая).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentCourseTransfer#getOldGroupId()
     */
        public PropertyPath<Long> oldGroupId()
        {
            if(_oldGroupId == null )
                _oldGroupId = new PropertyPath<Long>(RepresentCourseTransferGen.P_OLD_GROUP_ID, this);
            return _oldGroupId;
        }

    /**
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.RepresentCourseTransfer#getOldEducationOrgUnitId()
     */
        public PropertyPath<Long> oldEducationOrgUnitId()
        {
            if(_oldEducationOrgUnitId == null )
                _oldEducationOrgUnitId = new PropertyPath<Long>(RepresentCourseTransferGen.P_OLD_EDUCATION_ORG_UNIT_ID, this);
            return _oldEducationOrgUnitId;
        }

    /**
     * @return Тип возмещения затрат (старый).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentCourseTransfer#getOldCompensationTypeId()
     */
        public PropertyPath<Long> oldCompensationTypeId()
        {
            if(_oldCompensationTypeId == null )
                _oldCompensationTypeId = new PropertyPath<Long>(RepresentCourseTransferGen.P_OLD_COMPENSATION_TYPE_ID, this);
            return _oldCompensationTypeId;
        }

    /**
     * @return Связь студент - УП (старая).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentCourseTransfer#getOldEduPlanRelId()
     */
        public PropertyPath<Long> oldEduPlanRelId()
        {
            if(_oldEduPlanRelId == null )
                _oldEduPlanRelId = new PropertyPath<Long>(RepresentCourseTransferGen.P_OLD_EDU_PLAN_REL_ID, this);
            return _oldEduPlanRelId;
        }

    /**
     * @return Активные связи студент - РУП (старая).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentCourseTransfer#getOldWorkPlanRelIds()
     */
        public PropertyPath<String> oldWorkPlanRelIds()
        {
            if(_oldWorkPlanRelIds == null )
                _oldWorkPlanRelIds = new PropertyPath<String>(RepresentCourseTransferGen.P_OLD_WORK_PLAN_REL_IDS, this);
            return _oldWorkPlanRelIds;
        }

    /**
     * @return Условный перевод. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentCourseTransfer#isConditionally()
     */
        public PropertyPath<Boolean> conditionally()
        {
            if(_conditionally == null )
                _conditionally = new PropertyPath<Boolean>(RepresentCourseTransferGen.P_CONDITIONALLY, this);
            return _conditionally;
        }

    /**
     * @return Дата ликвидации академической задолженности.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentCourseTransfer#getDeadlineDate()
     */
        public PropertyPath<Date> deadlineDate()
        {
            if(_deadlineDate == null )
                _deadlineDate = new PropertyPath<Date>(RepresentCourseTransferGen.P_DEADLINE_DATE, this);
            return _deadlineDate;
        }

        public Class getEntityClass()
        {
            return RepresentCourseTransfer.class;
        }

        public String getEntityName()
        {
            return "representCourseTransfer";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
