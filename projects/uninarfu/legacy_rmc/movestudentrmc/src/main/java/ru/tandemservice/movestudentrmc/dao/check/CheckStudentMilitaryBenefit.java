package ru.tandemservice.movestudentrmc.dao.check;

import org.tandemframework.shared.person.base.entity.PersonBenefit;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.List;

public class CheckStudentMilitaryBenefit extends AbstractCheckStudentBenefit {

    private final static String MILITARY_BENEFIT_CODE = "2";

    @Override
    protected boolean checkCondition(Student student) {
        Long personId = student.getPerson().getId();

        if (!personBenefitMap.containsKey(personId))
            return false;

        List<PersonBenefit> benefits = personBenefitMap.get(personId);
        for (PersonBenefit personBenefit : benefits)
            if (personBenefit.getBenefit().getCode().equals(MILITARY_BENEFIT_CODE))
                return true;

        return false;
    }

    @Override
    protected String createError(Student student, boolean value) {
        StringBuilder sb = new StringBuilder()
                .append("Студент ")
                .append(student.getPerson().getFullFio())
                .append(" ")
                .append(value ? "не " : "")
                .append("имеет льготу «граждане, проходившие не менее 3х лет военную службу по контракту в Вооруженных силах РФ и уволенные с военной службы»");
        return sb.toString();
    }

}
