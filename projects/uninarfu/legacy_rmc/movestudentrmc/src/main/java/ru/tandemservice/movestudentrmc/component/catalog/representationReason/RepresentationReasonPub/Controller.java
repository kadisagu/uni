package ru.tandemservice.movestudentrmc.component.catalog.representationReason.RepresentationReasonPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.info.InfoCollector;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;

public class Controller extends DefaultCatalogPubController<RepresentationReason, Model, IDAO> {

    @Override
    protected DynamicListDataSource<RepresentationReason> createListDataSource(IBusinessComponent context) {
        Model model = getModel(context);
        DynamicListDataSource<RepresentationReason> ds = new DynamicListDataSource<RepresentationReason>(context, this);
        ds.addColumn(new SimpleColumn("Наименование", "title").setClickable(false).setOrderable(true), 2);
        ds.addColumn(new SimpleColumn("Номер", "priority").setClickable(false).setOrderable(true), 3);
        ds.addColumn(new ToggleColumn("Печатать", Model.RepresentationReasonWrapper.PRINTABLE).setListener("onClickTogglePrintable"), 4);
        ds.addColumn(new ToggleColumn("Использовать", RepresentationReason.used().s()).setListener("onClickToggleUsed"), 5);
        ds.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
        //if (model.isUserCatalog()) {
        String addtionalInfo = "";
        //    Boolean hasRelation = getDao().checkRelationReason((Long) context.getListenerParameter());        
        //    if (hasRelation) {
        //    	addtionalInfo = "Данная причина задействована в настройке «Тип-причина-основание». Изменения в справочнике повлекут за собой изменения в настройке.\n";
        //    }	


        //#1720
//        	ds.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", addtionalInfo + "Удалить элемент «{0}» из справочника?", ICatalogItem.CATALOG_ITEM_TITLE).setPermissionKey(model.getCatalogItemDelete()).setDisableHandler(model.getDisabledEntityHandler()));
        //}


        ds.setOrder("priority", OrderDirection.asc);
        return ds;
    }

    public void onClickTogglePrintable(IBusinessComponent component)
    {
        getDao().updatePrintable(getModel(component), (Long) component.getListenerParameter());
    }

    public void onClickToggleUsed(IBusinessComponent component)
    {
        getDao().updateUsed(getModel(component), (Long) component.getListenerParameter());
    }

    public void onClickDeleteItem(IBusinessComponent component)
    {
        Boolean hasRelation = getDao().checkRelationReason((Long) component.getListenerParameter());
        if (hasRelation) {
            InfoCollector info = ContextLocal.getInfoCollector();
            info.add("Данная причина задействована в настройке «Тип-причина-основание». Изменения в справочнике повлекут за собой изменения в настройке.");
        }
        getDao().deleteRow(component);
    }

}
