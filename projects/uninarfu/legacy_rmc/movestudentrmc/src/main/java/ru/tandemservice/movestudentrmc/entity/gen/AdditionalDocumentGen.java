package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.AdditionalDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Дополнтельные документы
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class AdditionalDocumentGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.AdditionalDocument";
    public static final String ENTITY_NAME = "additionalDocument";
    public static final int VERSION_HASH = 1776102997;
    private static IEntityMeta ENTITY_META;

    public static final String P_ADDITIONAL_DOC_FILE = "additionalDocFile";
    public static final String P_ADDITIONAL_DOC_FILE_NAME = "additionalDocFileName";

    private byte[] _additionalDocFile;     // Документ
    private String _additionalDocFileName;     // Название документа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Документ.
     */
    public byte[] getAdditionalDocFile()
    {
        return _additionalDocFile;
    }

    /**
     * @param additionalDocFile Документ.
     */
    public void setAdditionalDocFile(byte[] additionalDocFile)
    {
        dirty(_additionalDocFile, additionalDocFile);
        _additionalDocFile = additionalDocFile;
    }

    /**
     * @return Название документа.
     */
    @Length(max=255)
    public String getAdditionalDocFileName()
    {
        return _additionalDocFileName;
    }

    /**
     * @param additionalDocFileName Название документа.
     */
    public void setAdditionalDocFileName(String additionalDocFileName)
    {
        dirty(_additionalDocFileName, additionalDocFileName);
        _additionalDocFileName = additionalDocFileName;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof AdditionalDocumentGen)
        {
            setAdditionalDocFile(((AdditionalDocument)another).getAdditionalDocFile());
            setAdditionalDocFileName(((AdditionalDocument)another).getAdditionalDocFileName());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends AdditionalDocumentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) AdditionalDocument.class;
        }

        public T newInstance()
        {
            return (T) new AdditionalDocument();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "additionalDocFile":
                    return obj.getAdditionalDocFile();
                case "additionalDocFileName":
                    return obj.getAdditionalDocFileName();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "additionalDocFile":
                    obj.setAdditionalDocFile((byte[]) value);
                    return;
                case "additionalDocFileName":
                    obj.setAdditionalDocFileName((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "additionalDocFile":
                        return true;
                case "additionalDocFileName":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "additionalDocFile":
                    return true;
                case "additionalDocFileName":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "additionalDocFile":
                    return byte[].class;
                case "additionalDocFileName":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<AdditionalDocument> _dslPath = new Path<AdditionalDocument>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "AdditionalDocument");
    }
            

    /**
     * @return Документ.
     * @see ru.tandemservice.movestudentrmc.entity.AdditionalDocument#getAdditionalDocFile()
     */
    public static PropertyPath<byte[]> additionalDocFile()
    {
        return _dslPath.additionalDocFile();
    }

    /**
     * @return Название документа.
     * @see ru.tandemservice.movestudentrmc.entity.AdditionalDocument#getAdditionalDocFileName()
     */
    public static PropertyPath<String> additionalDocFileName()
    {
        return _dslPath.additionalDocFileName();
    }

    public static class Path<E extends AdditionalDocument> extends EntityPath<E>
    {
        private PropertyPath<byte[]> _additionalDocFile;
        private PropertyPath<String> _additionalDocFileName;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Документ.
     * @see ru.tandemservice.movestudentrmc.entity.AdditionalDocument#getAdditionalDocFile()
     */
        public PropertyPath<byte[]> additionalDocFile()
        {
            if(_additionalDocFile == null )
                _additionalDocFile = new PropertyPath<byte[]>(AdditionalDocumentGen.P_ADDITIONAL_DOC_FILE, this);
            return _additionalDocFile;
        }

    /**
     * @return Название документа.
     * @see ru.tandemservice.movestudentrmc.entity.AdditionalDocument#getAdditionalDocFileName()
     */
        public PropertyPath<String> additionalDocFileName()
        {
            if(_additionalDocFileName == null )
                _additionalDocFileName = new PropertyPath<String>(AdditionalDocumentGen.P_ADDITIONAL_DOC_FILE_NAME, this);
            return _additionalDocFileName;
        }

        public Class getEntityClass()
        {
            return AdditionalDocument.class;
        }

        public String getEntityName()
        {
            return "additionalDocument";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
