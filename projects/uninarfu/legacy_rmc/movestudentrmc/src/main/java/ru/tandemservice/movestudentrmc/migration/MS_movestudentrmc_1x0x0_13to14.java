package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.BooleanDBColumn;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.ddl.schema.columns.VarcharDBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentationTypeCodes;

import java.sql.PreparedStatement;

public class MS_movestudentrmc_1x0x0_13to14 extends IndependentMigrationScript {
    private static final String COLUMN_NAME = "PRINTDECLARATION_P";

    @Override
    public void run(DBTool tool) throws Exception {
        updateRepresentationType(tool);
        updateDocRepresentStudentBase(tool);
    }

    private void updateDocRepresentStudentBase(DBTool tool) throws Exception {
        String[] columns = new String[]{
                "studentTitleGenetiveStr_p",
                "formativeOrgUnitGenetiveStr_p",
                "headTitleStr_p",
                "DCTNLVLHGHSCHLTYPSTR_P"
        };

        IEntityMeta meta = EntityRuntime.getMeta(DocRepresentStudentBase.class);
        if (!tool.tableExists(meta.getTableName()) || tool.columnExists(meta.getTableName(), columns[0]))
            return;

        for (String colName : columns) {
            DBColumn column = new VarcharDBColumn(colName, 255);
            column.setNullable(true);
            tool.table(meta.getTableName()).createColumn(column);
        }

        tool.executeUpdate(
                "UPDATE " + meta.getTableName() + " SET studentTitleGenetiveStr_p=studentTitleStr_p, formativeOrgUnitGenetiveStr_p=formativeOrgUnitStr_p, headTitleStr_p=?, DCTNLVLHGHSCHLTYPSTR_P=?",
                new Object[]{"-", "направление подготовки/специальность"}
        );

        for (String colName : columns)
            tool.table(meta.getTableName()).column(colName).changeNullable(false);
    }

    private void updateRepresentationType(DBTool tool) throws Exception {
        IEntityMeta meta = EntityRuntime.getMeta(RepresentationType.class);
        if (!tool.tableExists(meta.getTableName()))
            return;

        PreparedStatement pst;

        if (!tool.table(meta.getTableName()).columnExists(COLUMN_NAME)) {
            //создаем колонку
            DBColumn column = new BooleanDBColumn(COLUMN_NAME);
            column.setDefaultExpression("0");
            column.setNullable(true);
            tool.table(meta.getTableName()).createColumn(column);

            //апдейтим все в false
            pst = tool.getConnection().prepareStatement(" UPDATE " + meta.getTableName() + " SET " + COLUMN_NAME + " = ?");
            pst.setBoolean(1, Boolean.FALSE);
            pst.executeUpdate();
            pst.close();

            //меняем на NOT NULL, DEFAULT FALSE
            tool.table(meta.getTableName()).column(COLUMN_NAME).changeDefault("0");
            tool.table(meta.getTableName()).column(COLUMN_NAME).changeNullable(false);
        }

        pst = tool.getConnection().prepareStatement(" UPDATE " + meta.getTableName() + " SET " + COLUMN_NAME + " = ? WHERE CODE_P = ?");
        pst.setBoolean(1, Boolean.TRUE);
        pst.setString(2, RepresentationTypeCodes.TRANSFER); //О переводе
        pst.executeUpdate();
        pst.close();
    }
}
