package ru.tandemservice.movestudentrmc.base.bo.ListRepresentWeekend.logic;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.AbstractListRepresentDAO;
import ru.tandemservice.movestudentrmc.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.StudentCustomStateCICodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.List;
import java.util.Map;

public class ListRepresentWeekendManagerModifyDAO extends AbstractListRepresentDAO implements IListRepresentWeekendManagerModifyDAO {

    @Override
    public void save(ListRepresent listRepresent,
                     List<Student> studentSelectedList,
                     DocListRepresentBasics representBasics)
    {
        //сохранение
        this.baseCreateOrUpdate(listRepresent);
        this.baseCreateOrUpdate(representBasics);

        //удалим старых студиков
        new DQLDeleteBuilder(RelListRepresentStudents.class)
                .where(DQLExpressions.eqValue(DQLExpressions.property(RelListRepresentStudents.representation().id()), listRepresent.getId()))
                .createStatement(getSession()).execute();

        //сохраним новых студентов
        for (Student st : studentSelectedList) {
            RelListRepresentStudentsOldData rel = new RelListRepresentStudentsOldData();
            rel.setRepresentation(listRepresent);
            rel.setStudent(st);
            rel.setOldStatus(st.getStatus());
            this.baseCreateOrUpdate(rel);
        }
    }

    @Override
    public boolean doCommit(ListRepresent represent, ErrorCollector error) {
        if (!super.doCommit(represent, UserContext.getInstance().getErrorCollector()))
            return false;

        ListRepresentWeekend listRepresent = (ListRepresentWeekend) represent;

        StudentCustomStateCI state = UniDaoFacade.getCoreDao().get(StudentCustomStateCI.class, StudentCustomStateCI.code(), StudentCustomStateCICodes.HOLIDAYS);

        MoveStudentDaoFacade.getCustomStateDAO().commitStudentCustomState(state, listRepresent.getDateBeginningWeekend(), listRepresent.getDateEndOfWeekend(), listRepresent);

        return true;
    }

    @Override
    public boolean doRollback(ListRepresent represent, ErrorCollector error) {
        if (!super.doRollback(represent, UserContext.getInstance().getErrorCollector()))
            return false;

        //удалим статус, который назначили этим представлением
        MoveStudentDaoFacade.getCustomStateDAO().rollbackStudentCustomState(represent);

        return true;
    }

    @Override
    public Map<Student, OrderParagraphInfo> buildBodyRepresent(
            List<ListRepresent> representationBase, RtfDocument document)
    {
        ListPrintDoc listPrintDoc = new ListPrintDoc();
        RtfDocument templateRepresent = listPrintDoc.creationPrintDocOrder(representationBase);

        document.setHeader(templateRepresent.getHeader());
        document.setSettings(templateRepresent.getSettings());
        document.getElementList().addAll(templateRepresent.getElementList());
        return listPrintDoc.getParagInfomap();
    }

    @Override
    public void buildBodyExtract(ListOrdListRepresent listOrdListRepresent,
                                 Student student, RtfDocument docExtract, Map<Student, OrderParagraphInfo> map)
    {
        super.buildBodyExtract(listOrdListRepresent, student, docExtract, map);

        ListRepresentWeekend listRepresent = (ListRepresentWeekend) listOrdListRepresent.getRepresentation();
        RtfInjectModifier im = new RtfInjectModifier();

        ListPrintDoc.injectModifier(im, listRepresent, student);

        im.modify(docExtract);
    }

    @Override
    public List<Student> selectRepresent(ListRepresent listRepresent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void saveSupport(ListRepresent listRepresent,
                            List<DataWrapper> studentSelectedList,
                            DocListRepresentBasics representBasics)
    {
        // TODO Auto-generated method stub

    }

}
