package ru.tandemservice.movestudentrmc.component.student.GrantAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    public void onRefreshComponent(IBusinessComponent component) {
        ((IDAO) getDao()).prepare(getModel(component));

    }

    public void onClickApply(IBusinessComponent component) {
        ((IDAO) getDao()).update(getModel(component));
        deactivate(component);
    }


}
