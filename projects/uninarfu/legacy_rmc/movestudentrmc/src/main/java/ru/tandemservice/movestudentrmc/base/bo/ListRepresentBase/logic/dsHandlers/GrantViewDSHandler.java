package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.entity.RelTypeGrantView;
import ru.tandemservice.movestudentrmc.entity.catalog.Grant;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class GrantViewDSHandler extends DefaultComboDataSourceHandler {

    public static final String SHORT_TITLE = "shortTitle";
    public static final String GRANT = "grant";

    public GrantViewDSHandler(String ownerId) {

        super(ownerId, GrantView.class);
    }

    public GrantViewDSHandler(String ownerId, String filterByProperty) {

        super(ownerId, GrantView.class);
        this._filterByProperty = filterByProperty;
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        //проверяем установлено ли поле по которому фильтруем
        if (this._filterByProperty.isEmpty()) {
            String filter = ep.input.getComboFilterByValue();
            ep.dqlBuilder.where(eq(DQLExpressions.property(GrantView.use().fromAlias("e")), value(true)));
            if (StringUtils.isNotBlank(filter)) {
                ep.dqlBuilder.where(like(DQLFunctions.upper(DQLExpressions.property(GrantView.title().fromAlias("e"))), value(CoreStringUtils.escapeLike(filter, true))));
            }
        }
        //если фильтр по shortTitle
        else {
            if (this._filterByProperty.equals(SHORT_TITLE)) {
                String filter = ep.input.getComboFilterByValue();
                ep.dqlBuilder.where(eq(DQLExpressions.property(GrantView.use().fromAlias("e")), value(true)));
                if (StringUtils.isNotBlank(filter)) {
                    ep.dqlBuilder.where(like(DQLFunctions.upper(DQLExpressions.property(GrantView.shortTitle().fromAlias("e"))), value(CoreStringUtils.escapeLike(filter, true))));
                }
            }
        }

        Boolean suspend = ep.context.get("grantViewCanSuspendFilter");
        if (suspend != null && suspend)
            ep.dqlBuilder.where(DQLExpressions.eq(
                    DQLExpressions.property(GrantView.suspend().fromAlias("e")),
                    DQLExpressions.value(true)
            ));

        Grant grant = ep.context.get(GRANT);

        if (grant != null) {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RelTypeGrantView.class, "rel")
                    .column(DQLExpressions.property(RelTypeGrantView.view().id().fromAlias("rel")))
                    .where(DQLExpressions.eqValue(DQLExpressions.property(RelTypeGrantView.grant().id().fromAlias("rel")), grant.getId()));

            ep.dqlBuilder.where(DQLExpressions.in(DQLExpressions.property(GrantView.id().fromAlias("e")), builder.buildQuery()));
        }


    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
    {
        ep.dqlBuilder.order("e." + GrantView.shortTitle());
    }
}
