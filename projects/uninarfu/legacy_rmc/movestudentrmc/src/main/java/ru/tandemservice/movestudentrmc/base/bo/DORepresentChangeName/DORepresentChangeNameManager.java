package ru.tandemservice.movestudentrmc.base.bo.DORepresentChangeName;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentChangeName.logic.DORepresentChangeNameDAO;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentChangeName.ui.Edit.DORepresentChangeNameEdit;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentChangeName.ui.View.DORepresentChangeNameView;
import ru.tandemservice.movestudentrmc.docord.IDOObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IDOObjectModifyDAO;

@Configuration
public class DORepresentChangeNameManager extends BusinessObjectManager implements IDOObjectByRepresentTypeManager {

    public static DORepresentChangeNameManager instance()
    {
        return instance(DORepresentChangeNameManager.class);
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return DORepresentChangeNameEdit.class;
    }

    @Override
    @Bean
    public IDOObjectModifyDAO getDOObjectModifyDAO() {
        return new DORepresentChangeNameDAO();
    }

    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return DORepresentChangeNameView.class;
    }
}
