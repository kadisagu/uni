package ru.tandemservice.movestudentrmc.component.settings.RepresentationBasementToDocument.RepresentationBasementToDocumentSettings;

import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.entity.IEntity;

public class Wrapper extends EntityBase {

    private IEntity first;

    private boolean configured;

    public Wrapper(IEntity first, boolean isConfigured) {
        this.first = first;
        this.configured = isConfigured;
    }

    public IEntity getFirst() {
        return first;
    }

    public void setFirst(IEntity first) {
        this.first = first;
    }

    public boolean isConfigured() {
        return configured;
    }

    public void setConfigured(boolean configured) {
        this.configured = configured;
    }

    @Override
    public Long getId() {
        return first.getId();
    }


}
