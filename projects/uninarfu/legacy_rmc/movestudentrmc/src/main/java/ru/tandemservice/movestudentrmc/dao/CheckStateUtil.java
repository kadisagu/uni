package ru.tandemservice.movestudentrmc.dao;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.entity.IAbstractOrder;
import ru.tandemservice.movestudentrmc.entity.IAbstractRepresentation;
import ru.tandemservice.movestudentrmc.entity.catalog.MovestudentExtractStates;
import ru.tandemservice.movestudentrmc.entity.catalog.MovestudentOrderStates;
import ru.tandemservice.movestudentrmc.entity.gen.IAbstractOrderGen;
import ru.tandemservice.movestudentrmc.entity.gen.IAbstractRepresentationGen;
import ru.tandemservice.uni.dao.IUniBaseDao;

import java.util.ArrayList;
import java.util.List;


public class CheckStateUtil {

    public static void checkStateOrder(IAbstractOrder order, List<String> codes, Session session) {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(IAbstractOrder.class, "o")
                .column(DQLExpressions.property(IAbstractOrderGen.state().code().fromAlias("o")))
                .where(DQLExpressions.eqValue(DQLExpressions.property("o.id"), order.getOrderID()));

        String code = builder.createStatement(session).uniqueResult();

        if (!codes.contains(code))
            throw new ApplicationException(getErrorMessage(codes, true));
    }

    public static void checkStateRepresent(IAbstractRepresentation represent, List<String> codes, Session session) {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(IAbstractRepresentation.class, "r")
                .column(DQLExpressions.property(IAbstractRepresentationGen.state().code().fromAlias("r")))
                .where(DQLExpressions.eqValue(DQLExpressions.property("r.id"), represent.getRepresentID()));

        String code = builder.createStatement(session).uniqueResult();

        if (!codes.contains(code))
            throw new ApplicationException(getErrorMessage(codes, false));
    }

    private static String getErrorMessage(List<String> codes, boolean isOrder) {

        StringBuilder msg = new StringBuilder("Нельзя изменять ");
        msg.append(isOrder ? "приказ" : "представление")
                .append(". Изменение ")
                .append(isOrder ? "приказа" : "представления")
                .append(" возможно только в ")
                .append(codes.size() == 1 ? "состоянии " : "состояниях ")
                .append(StringUtils.join(getStateTitle(codes, isOrder), ", "))
                .append(".");

        return msg.toString();
    }

    private static List<String> getStateTitle(List<String> codes, boolean isOrder) {

        List<String> retVal = new ArrayList<String>();
        if (isOrder)
            for (String code : codes) {
                MovestudentOrderStates state = IUniBaseDao.instance.get().get(MovestudentOrderStates.class, MovestudentOrderStates.code(), code);
                retVal.add("«" + state.getTitle() + "»");
            }
        else
            for (String code : codes) {
                MovestudentExtractStates state = IUniBaseDao.instance.get().get(MovestudentExtractStates.class, MovestudentExtractStates.code(), code);
                retVal.add("«" + state.getTitle() + "»");
            }

        return retVal;
    }

}
