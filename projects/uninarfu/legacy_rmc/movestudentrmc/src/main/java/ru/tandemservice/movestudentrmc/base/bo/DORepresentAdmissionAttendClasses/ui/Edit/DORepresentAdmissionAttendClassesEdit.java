package ru.tandemservice.movestudentrmc.base.bo.DORepresentAdmissionAttendClasses.ui.Edit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.block.BlockListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.DevelopGridTermDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.ReasonDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.EditBasic.DORepresentBaseEditBasic;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;

@Configuration
public class DORepresentAdmissionAttendClassesEdit extends BusinessComponentManager {

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(selectDS("reasonDS", reasonDSHandler()))
                .addDataSource(UniStudentManger.instance().courseDSConfig())
                .addDataSource(selectDS("developGridTermDS", developGridTermDSHandler())
                                       .addColumn("title", null, new DevelopGridTermDSHandler.Formatter()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler reasonDSHandler() {
        return new ReasonDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler developGridTermDSHandler() {
        return new DevelopGridTermDSHandler(getName());
    }

    @Bean
    public BlockListExtPoint editBasicBlockListExtPoint() {
        return blockListExtPointBuilder(DORepresentAdmissionAttendClassesEditUI.EDIT_BASIC_BLOCK_LIST)
                .addBlock(componentBlock(
                                  DORepresentAdmissionAttendClassesEditUI.EDIT_BASIC_BLOCK,
                                  DORepresentBaseEditBasic.class).parameters("ui:blockParam")
                )
                .create();
    }
}
