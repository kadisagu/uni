package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import ru.tandemservice.movestudentrmc.entity.IStudentPractice;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListRepresentAdmissionToPractice;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Списочное представление о допуске к практике
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ListRepresentAdmissionToPracticeGen extends ListRepresent
 implements IStudentPractice{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.ListRepresentAdmissionToPractice";
    public static final String ENTITY_NAME = "listRepresentAdmissionToPractice";
    public static final int VERSION_HASH = 850611037;
    private static IEntityMeta ENTITY_META;

    public static final String P_TITLE_PRACTICE = "titlePractice";
    public static final String P_DATE_BEGINNING_PRACTICE = "dateBeginningPractice";
    public static final String P_DATE_END_OF_PRACTICE = "dateEndOfPractice";
    public static final String L_REGISTRY_ELEMENT_ROW = "registryElementRow";
    public static final String L_SUPERVISOR = "supervisor";

    private String _titlePractice;     // Наименование практики
    private Date _dateBeginningPractice;     // Дата начала практики
    private Date _dateEndOfPractice;     // Дата окончания практики
    private EppWorkPlanRow _registryElementRow;     // Мероприятие реестра
    private Employee _supervisor;     // Руководитель от университета

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Наименование практики.
     */
    @Length(max=255)
    public String getTitlePractice()
    {
        return _titlePractice;
    }

    /**
     * @param titlePractice Наименование практики.
     */
    public void setTitlePractice(String titlePractice)
    {
        dirty(_titlePractice, titlePractice);
        _titlePractice = titlePractice;
    }

    /**
     * @return Дата начала практики.
     */
    public Date getDateBeginningPractice()
    {
        return _dateBeginningPractice;
    }

    /**
     * @param dateBeginningPractice Дата начала практики.
     */
    public void setDateBeginningPractice(Date dateBeginningPractice)
    {
        dirty(_dateBeginningPractice, dateBeginningPractice);
        _dateBeginningPractice = dateBeginningPractice;
    }

    /**
     * @return Дата окончания практики.
     */
    public Date getDateEndOfPractice()
    {
        return _dateEndOfPractice;
    }

    /**
     * @param dateEndOfPractice Дата окончания практики.
     */
    public void setDateEndOfPractice(Date dateEndOfPractice)
    {
        dirty(_dateEndOfPractice, dateEndOfPractice);
        _dateEndOfPractice = dateEndOfPractice;
    }

    /**
     * @return Мероприятие реестра.
     */
    public EppWorkPlanRow getRegistryElementRow()
    {
        return _registryElementRow;
    }

    /**
     * @param registryElementRow Мероприятие реестра.
     */
    public void setRegistryElementRow(EppWorkPlanRow registryElementRow)
    {
        dirty(_registryElementRow, registryElementRow);
        _registryElementRow = registryElementRow;
    }

    /**
     * @return Руководитель от университета.
     */
    public Employee getSupervisor()
    {
        return _supervisor;
    }

    /**
     * @param supervisor Руководитель от университета.
     */
    public void setSupervisor(Employee supervisor)
    {
        dirty(_supervisor, supervisor);
        _supervisor = supervisor;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ListRepresentAdmissionToPracticeGen)
        {
            setTitlePractice(((ListRepresentAdmissionToPractice)another).getTitlePractice());
            setDateBeginningPractice(((ListRepresentAdmissionToPractice)another).getDateBeginningPractice());
            setDateEndOfPractice(((ListRepresentAdmissionToPractice)another).getDateEndOfPractice());
            setRegistryElementRow(((ListRepresentAdmissionToPractice)another).getRegistryElementRow());
            setSupervisor(((ListRepresentAdmissionToPractice)another).getSupervisor());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ListRepresentAdmissionToPracticeGen> extends ListRepresent.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ListRepresentAdmissionToPractice.class;
        }

        public T newInstance()
        {
            return (T) new ListRepresentAdmissionToPractice();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "titlePractice":
                    return obj.getTitlePractice();
                case "dateBeginningPractice":
                    return obj.getDateBeginningPractice();
                case "dateEndOfPractice":
                    return obj.getDateEndOfPractice();
                case "registryElementRow":
                    return obj.getRegistryElementRow();
                case "supervisor":
                    return obj.getSupervisor();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "titlePractice":
                    obj.setTitlePractice((String) value);
                    return;
                case "dateBeginningPractice":
                    obj.setDateBeginningPractice((Date) value);
                    return;
                case "dateEndOfPractice":
                    obj.setDateEndOfPractice((Date) value);
                    return;
                case "registryElementRow":
                    obj.setRegistryElementRow((EppWorkPlanRow) value);
                    return;
                case "supervisor":
                    obj.setSupervisor((Employee) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "titlePractice":
                        return true;
                case "dateBeginningPractice":
                        return true;
                case "dateEndOfPractice":
                        return true;
                case "registryElementRow":
                        return true;
                case "supervisor":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "titlePractice":
                    return true;
                case "dateBeginningPractice":
                    return true;
                case "dateEndOfPractice":
                    return true;
                case "registryElementRow":
                    return true;
                case "supervisor":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "titlePractice":
                    return String.class;
                case "dateBeginningPractice":
                    return Date.class;
                case "dateEndOfPractice":
                    return Date.class;
                case "registryElementRow":
                    return EppWorkPlanRow.class;
                case "supervisor":
                    return Employee.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ListRepresentAdmissionToPractice> _dslPath = new Path<ListRepresentAdmissionToPractice>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ListRepresentAdmissionToPractice");
    }
            

    /**
     * @return Наименование практики.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentAdmissionToPractice#getTitlePractice()
     */
    public static PropertyPath<String> titlePractice()
    {
        return _dslPath.titlePractice();
    }

    /**
     * @return Дата начала практики.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentAdmissionToPractice#getDateBeginningPractice()
     */
    public static PropertyPath<Date> dateBeginningPractice()
    {
        return _dslPath.dateBeginningPractice();
    }

    /**
     * @return Дата окончания практики.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentAdmissionToPractice#getDateEndOfPractice()
     */
    public static PropertyPath<Date> dateEndOfPractice()
    {
        return _dslPath.dateEndOfPractice();
    }

    /**
     * @return Мероприятие реестра.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentAdmissionToPractice#getRegistryElementRow()
     */
    public static EppWorkPlanRow.Path<EppWorkPlanRow> registryElementRow()
    {
        return _dslPath.registryElementRow();
    }

    /**
     * @return Руководитель от университета.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentAdmissionToPractice#getSupervisor()
     */
    public static Employee.Path<Employee> supervisor()
    {
        return _dslPath.supervisor();
    }

    public static class Path<E extends ListRepresentAdmissionToPractice> extends ListRepresent.Path<E>
    {
        private PropertyPath<String> _titlePractice;
        private PropertyPath<Date> _dateBeginningPractice;
        private PropertyPath<Date> _dateEndOfPractice;
        private EppWorkPlanRow.Path<EppWorkPlanRow> _registryElementRow;
        private Employee.Path<Employee> _supervisor;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Наименование практики.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentAdmissionToPractice#getTitlePractice()
     */
        public PropertyPath<String> titlePractice()
        {
            if(_titlePractice == null )
                _titlePractice = new PropertyPath<String>(ListRepresentAdmissionToPracticeGen.P_TITLE_PRACTICE, this);
            return _titlePractice;
        }

    /**
     * @return Дата начала практики.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentAdmissionToPractice#getDateBeginningPractice()
     */
        public PropertyPath<Date> dateBeginningPractice()
        {
            if(_dateBeginningPractice == null )
                _dateBeginningPractice = new PropertyPath<Date>(ListRepresentAdmissionToPracticeGen.P_DATE_BEGINNING_PRACTICE, this);
            return _dateBeginningPractice;
        }

    /**
     * @return Дата окончания практики.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentAdmissionToPractice#getDateEndOfPractice()
     */
        public PropertyPath<Date> dateEndOfPractice()
        {
            if(_dateEndOfPractice == null )
                _dateEndOfPractice = new PropertyPath<Date>(ListRepresentAdmissionToPracticeGen.P_DATE_END_OF_PRACTICE, this);
            return _dateEndOfPractice;
        }

    /**
     * @return Мероприятие реестра.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentAdmissionToPractice#getRegistryElementRow()
     */
        public EppWorkPlanRow.Path<EppWorkPlanRow> registryElementRow()
        {
            if(_registryElementRow == null )
                _registryElementRow = new EppWorkPlanRow.Path<EppWorkPlanRow>(L_REGISTRY_ELEMENT_ROW, this);
            return _registryElementRow;
        }

    /**
     * @return Руководитель от университета.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentAdmissionToPractice#getSupervisor()
     */
        public Employee.Path<Employee> supervisor()
        {
            if(_supervisor == null )
                _supervisor = new Employee.Path<Employee>(L_SUPERVISOR, this);
            return _supervisor;
        }

        public Class getEntityClass()
        {
            return ListRepresentAdmissionToPractice.class;
        }

        public String getEntityName()
        {
            return "listRepresentAdmissionToPractice";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
