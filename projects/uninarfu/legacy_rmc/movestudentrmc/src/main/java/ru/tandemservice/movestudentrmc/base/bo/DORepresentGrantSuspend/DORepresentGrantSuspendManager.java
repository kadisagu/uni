package ru.tandemservice.movestudentrmc.base.bo.DORepresentGrantSuspend;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentGrantSuspend.logic.DORepresentGrantSuspendDAO;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentGrantSuspend.ui.Edit.DORepresentGrantSuspendEdit;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentGrantSuspend.ui.View.DORepresentGrantSuspendView;
import ru.tandemservice.movestudentrmc.docord.IDOObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IDOObjectModifyDAO;

@Configuration
public class DORepresentGrantSuspendManager extends BusinessObjectManager implements IDOObjectByRepresentTypeManager {

    public static DORepresentGrantSuspendManager instance() {
        return instance(DORepresentGrantSuspendManager.class);
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return DORepresentGrantSuspendEdit.class;
    }

    @Bean
    public IDOObjectModifyDAO getDOObjectModifyDAO() {
        return new DORepresentGrantSuspendDAO();
    }

    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return DORepresentGrantSuspendView.class;
    }


}
