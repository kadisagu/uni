package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBudgetTransfer.logic;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.AbstractListRepresentDAO;
import ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab.MonthWrapper;
import ru.tandemservice.movestudentrmc.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.ReportRenderer;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.*;

public class ListRepresentBudgetTransferManagerModifyDAO extends AbstractListRepresentDAO {

    @Override
    public void save(ListRepresent listRepresent, List<Student> studentSelectedList, DocListRepresentBasics representBasics) {

        ListRepresentBudgetTransfer profileTransfer = (ListRepresentBudgetTransfer) listRepresent;

        if (CommonBaseDateUtil.isAfter(profileTransfer.getDateBeginingPayment(), profileTransfer.getDateEndOfPayment())) {
            throw new ApplicationException("Дата начала выплаты не может быть позже даты окончания выплаты");
        }

        //период попадает в учебный год
        EducationYear eduYear = profileTransfer.getEducationYear();

        Calendar beginYear = Calendar.getInstance();
        beginYear.set(eduYear.getIntValue(), 8, 1, 0, 0, 0);
        beginYear.set(14, 0);
        Calendar endYear = Calendar.getInstance();
        endYear.set(eduYear.getIntValue() + 1, 7, 31, 23, 59, 59);
        endYear.set(14, 999);

        if (!CommonBaseDateUtil.isBetween(profileTransfer.getDateBeginingPayment(), beginYear.getTime(), endYear.getTime())
                || !CommonBaseDateUtil.isBetween(profileTransfer.getDateEndOfPayment(), beginYear.getTime(), endYear.getTime()))
            throw new ApplicationException("Даты назначения стипендии/выплаты выходят за границы учебного года " + eduYear.getTitle());

        List<Student> internalStudentList = new ArrayList<Student>();
        for (Student student : studentSelectedList) {
            /** Сформируем список очников **/
            if (student.getEducationOrgUnit().getDevelopForm().getCode().equals(DevelopFormCodes.FULL_TIME_FORM))
                internalStudentList.add(student);
        }

        /** Проверим стипендии для очников **/
        List<String> periodList = MonthWrapper.getMonthsList(profileTransfer.getDateBeginingPayment(), profileTransfer.getDateEndOfPayment(), profileTransfer.getEducationYear());
        List<String> errorList = new ArrayList<String>();
        boolean good = MoveStudentDaoFacade.getGrantDAO().checkStudentGrants(profileTransfer.getId(), profileTransfer.getGrantView(), internalStudentList, periodList, errorList);

        if (!good) {
            RtfDocument errorReport = RtfBean.getElementFactory().createRtfDocument();
            IRtfControl par = RtfBean.getElementFactory().createRtfControl(IRtfData.PAR);
            for (String text : errorList) {
                IRtfText studText = RtfBean.getElementFactory().createRtfText(text);
                errorReport.addElement(studText);
                errorReport.getElementList().addAll(Arrays.asList(par, par));
            }
            BusinessComponentUtils.downloadDocument(new ReportRenderer("Ошибки.rtf", errorReport), true);

            throw new ApplicationException("Не удалось сохранить представление");
        }

        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(RelListRepresentStudents.class)
                .fromEntity(RelListRepresentStudents.class, "rls")
                .where(DQLExpressions.eq
                        (DQLExpressions.property(RelListRepresentStudents.representation()), DQLExpressions.value(listRepresent)));

        //Почистим старых студентов
        deleteBuilder.createStatement(getSession()).execute();

        baseCreateOrUpdate(listRepresent);
        getSession().saveOrUpdate(representBasics);

        for (Student student : studentSelectedList) {
            RelListRepresentStudents relListRepresentGrantStudents = new RelListRepresentStudents();

            relListRepresentGrantStudents.setRepresentation(listRepresent);
            relListRepresentGrantStudents.setStudent(student);
            baseCreateOrUpdate(relListRepresentGrantStudents);
        }

        /** Назначаем стипендии только для очников **/
        MoveStudentDaoFacade.getGrantDAO().saveStudentGrants(profileTransfer, profileTransfer.getGrantView(), internalStudentList, periodList, profileTransfer.getEducationYear());
    }

    @Override
    public void update(ListRepresent listRepresent, List<Student> studentSelectedList) {

    }


    @Override
    public void buildBodyExtract(ListOrdListRepresent listOrdListRepresent,
                                 Student student, RtfDocument docExtract, Map<Student, OrderParagraphInfo> map)
    {
        super.buildBodyExtract(listOrdListRepresent, student, docExtract, map);

        ListRepresentBudgetTransfer listRepresent = (ListRepresentBudgetTransfer) listOrdListRepresent.getRepresentation();
        /*StringBuilder grantText = new StringBuilder();
		grantText
		.append("с назначением государственной академической стипендии c")
		.append(DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(listRepresent.getDateBeginingPayment()))
		.append(" года выплаты стипендии по ")
		.append(DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(listRepresent.getDateEndOfPayment()))
		.append(listRepresent.getGrantView().getGenitive() == null ? listRepresent.getGrantView().getTitle() : listRepresent.getGrantView().getGenitive());
		*/
        RtfString grantText = new RtfString();
        grantText.append("с назначением государственной академической стипендии c ");
        UtilPrintSupport.getDateFormatterWithMonthStringAndYear(listRepresent.getDateBeginingPayment(), grantText);
        grantText.append(" выплаты стипендии по ");
        UtilPrintSupport.getDateFormatterWithMonthStringAndYear(listRepresent.getDateEndOfPayment(), grantText);
        grantText.append(" ")
                .append(listRepresent.getGrantView().getGenitive() == null ? listRepresent.getGrantView().getTitle() : listRepresent.getGrantView().getGenitive());

        new RtfInjectModifier()
                .put("represent_CreateDate", UtilPrintSupport.getDateFormatterWithMonthString(listRepresent.getCreateDate()))
                .put("beginTransfer", UtilPrintSupport.getDateFormatterWithMonthString(listRepresent.getDateBeginingTransfer()))
                .put("nextEnding", "следующего")
                .put("studentEnding", "студента")
                .put("grantText", grantText)
                .put("develop_form", UtilPrintSupport.getDevelopFormGen(student.getEducationOrgUnit().getDevelopForm()))
                .modify(docExtract);

    }

    @Override
    public Map<Student, OrderParagraphInfo> buildBodyRepresent(List<ListRepresent> representationBase, RtfDocument document) {
        ListPrintDoc listPrintDoc = new ListPrintDoc();
        RtfDocument templateRepresent = listPrintDoc.creationPrintDocOrder(representationBase);
        document.getElementList().addAll(templateRepresent.getElementList());
        return listPrintDoc.getParagInfomap();

    }


    @Override
    public boolean doRollback(ListRepresent represent, ErrorCollector error) {

        List<RelListRepresentStudents> representList = getStudentList(represent);
        List<ListOrdListRepresent> orders = DataAccessServices.dao().getList(ListOrdListRepresent.class, ListOrdListRepresent.representation(), represent);
        ListOrder order = orders.get(0).getOrder();

        for (RelListRepresentStudents representStudents : representList) {

            Student student = representStudents.getStudent();
            /** Переведем по договору **/
            student.setCompensationType(DataAccessServices.dao().get(CompensationType.class, CompensationType.code(), CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT));

            getSession().saveOrUpdate(student);

            /**
             * Откатим стипендии
             */
            try {
                MoveStudentDaoFacade.getGrantDAO().rollbackStudentGrants(represent, order);
            }
            catch (Exception ex) {
                //TODO: а что тут надо делать?
                ex.printStackTrace();
                return false;
            }
        }


        return true;
    }

    @Override
    public boolean doCommit(ListRepresent represent, ErrorCollector error) {

        List<RelListRepresentStudents> representList = getStudentList(represent);


        for (RelListRepresentStudents st : representList) {
            Student student = st.getStudent();
            /** Переведем на бюджет **/
            student.setCompensationType(DataAccessServices.dao().get(CompensationType.class, CompensationType.code(), CompensationTypeCodes.COMPENSATION_TYPE_BUDGET));

            //Если очная форма - назначим стипендию
            if (student.getEducationOrgUnit().getDevelopForm().getCode().equals(DevelopFormCodes.FULL_TIME_FORM)) {
                List<ListOrdListRepresent> orders = DataAccessServices.dao().getList(ListOrdListRepresent.class, ListOrdListRepresent.representation(), represent);
                ListOrder order = orders.get(0).getOrder();

                try {
                    MoveStudentDaoFacade.getGrantDAO().commitStudentGrants(represent, order);
                }
                catch (Exception ex) {
                    //TODO: а что тут надо делать?
                    ex.printStackTrace();
                    return false;
                }

            }

            //Поиск активных договоров (пока что не подключен модуль unisc)
			/*	        MQBuilder builder = new MQBuilder("ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student", "a2s");
	        builder.add(MQExpression.eq("a2s", "student", student));
	        builder.addJoinFetch("a2s", "agreement", "agreement");
	        builder.addOrder("a2s", "active");
	        builder.addOrder("agreement", "formingDate");
	        builder.addOrder("agreement", "id");
	        Session session = getSession();
	        List result = new ArrayList();
	        List relations = builder.getResultList(session);
	        for(Iterator i$ = relations.iterator(); i$.hasNext();)
	        {
	            UniscEduAgreement2Student a2s = (UniscEduAgreement2Student)i$.next();
	            result.add(new Model.AgreementWrapper(a2s, a2s.isActive()));
	            MQBuilder b = new MQBuilder("ru.tandemservice.unisc.entity.agreements.UniscEduAdditAgreement", "agreement");
	            b.add(MQExpression.eq("agreement", "agreement.id", a2s.getAgreement().getId()));
	            b.addOrder("agreement", "formingDate");
	            b.addOrder("agreement", "id");
	            Iterator i$ = b.getResultList(session).iterator();
	            while(i$.hasNext()) 
	            {
	                UniscEduAdditAgreement a = (UniscEduAdditAgreement)i$.next();
	                result.add(new Model.AgreementWrapper(a, a2s.isActive()));
	            }
	        }	*/

        }


        return true;

    }


    @Override
    public List<Student> selectRepresent(ListRepresent listRepresent) {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(RelListRepresentStudents.class, "rls");

        builder.where(DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("rls")), DQLExpressions.value(listRepresent)));

        builder.addColumn(DQLExpressions.property(RelListRepresentStudents.student().fromAlias("rls")));

        return builder.createStatement(getSession()).list();
    }

    public void saveSupport(ListRepresent listRepresent, List<DataWrapper> studentSelectedList, DocListRepresentBasics representBasics) {
    }

}
