package ru.tandemservice.movestudentrmc.component.menu.PaymentsList;

import org.tandemframework.core.entity.EntityBase;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntity;

public class Wrapper extends EntityBase {

    private StudentGrantEntity grantEntity;

    private String grantOrder;
    private Long grantOrderId;

    private String grantCancelOrder;
    private Long grantCancelOrderId;

    private String grantResumeOrder;
    private Long grantResumeOrderId;

    private String grantSuspendOrder;
    private Long grantSuspendOrderId;

    public Wrapper(StudentGrantEntity grantEntity, String grantOrder, Long grantOrderId, String grantCancelOrder, Long grantCancelOrderId, String grantResumeOrder, Long grantResumeOrderId, String grantSuspendOrder, Long grantSuspendOrderId) {
        this.grantEntity = grantEntity;

        this.grantOrder = grantOrder;
        this.grantOrderId = grantOrderId;

        this.grantCancelOrder = grantCancelOrder;
        this.grantCancelOrderId = grantCancelOrderId;

        this.grantResumeOrder = grantResumeOrder;
        this.grantResumeOrderId = grantResumeOrderId;

        this.grantSuspendOrder = grantSuspendOrder;
        this.grantSuspendOrderId = grantSuspendOrderId;
    }

    public String getMergeKey() {
        Long id = this.grantEntity.getRepresentation() == null ? this.grantEntity.getListRepresent().getId() : this.grantEntity.getRepresentation().getId();
        return "" + this.grantEntity.getStudent().getId() + id;
    }

    @Override
    public Long getId() {
        return grantEntity.getId();
    }

    public StudentGrantEntity getGrantEntity() {
        return grantEntity;
    }

    public void setGrantEntity(StudentGrantEntity grantEntity) {
        this.grantEntity = grantEntity;
    }

    public String getGrantOrder() {
        return grantOrder;
    }

    public void setGrantOrder(String grantOrder) {
        this.grantOrder = grantOrder;
    }

    public Long getGrantOrderId() {
        return grantOrderId;
    }

    public void setGrantOrderId(Long grantOrderId) {
        this.grantOrderId = grantOrderId;
    }

    public String getGrantCancelOrder() {
        return grantCancelOrder;
    }

    public void setGrantCancelOrder(String grantCancelOrder) {
        this.grantCancelOrder = grantCancelOrder;
    }

    public Long getGrantCancelOrderId() {
        return grantCancelOrderId;
    }

    public void setGrantCancelOrderId(Long grantCancelOrderId) {
        this.grantCancelOrderId = grantCancelOrderId;
    }

    public String getGrantResumeOrder() {
        return grantResumeOrder;
    }

    public void setGrantResumeOrder(String grantResumeOrder) {
        this.grantResumeOrder = grantResumeOrder;
    }

    public Long getGrantResumeOrderId() {
        return grantResumeOrderId;
    }

    public void setGrantResumeOrderId(Long grantResumeOrderId) {
        this.grantResumeOrderId = grantResumeOrderId;
    }

    public String getGrantSuspendOrder() {
        return grantSuspendOrder;
    }

    public void setGrantSuspendOrder(String grantSuspendOrder) {
        this.grantSuspendOrder = grantSuspendOrder;
    }

    public Long getGrantSuspendOrderId() {
        return grantSuspendOrderId;
    }

    public void setGrantSuspendOrderId(Long grantSuspendOrderId) {
        this.grantSuspendOrderId = grantSuspendOrderId;
    }

}
