package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.EditWorkPlan;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.*;

@Configuration
public class DORepresentBaseEditWorkPlan extends BusinessComponentManager {

    public static final String EDUPLAN_DS = "eduplanDS";
    public static final String EDUPLANVERSION_DS = "eduplanversionDS";
    public static final String EPP_YEAR_DS = "eppYearDS";
    public static final String EPP_WORK_PLAN_DS = "eppWorkPlanDS";
    public static final String DEVELOP_GRID_TERM_DS = "developGridTermDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(EDUPLAN_DS, planDSHandler()))
                .addDataSource(selectDS(EDUPLANVERSION_DS, versionDSHandler()))
                .addDataSource(selectDS(EPP_YEAR_DS, eppYearDSHandler()))
                .addDataSource(selectDS(EPP_WORK_PLAN_DS, eppWorkPlanDSHandler()))
                .addDataSource(selectDS(DEVELOP_GRID_TERM_DS, developGridTermDSHandler()).addColumn("title", null, new DevelopGridTermDSHandler.Formatter()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler planDSHandler() {
        return new EduPlanDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler versionDSHandler() {
        return new EduPlanVersionDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler eppYearDSHandler() {
        return new EppYearDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler eppWorkPlanDSHandler() {
        return new EppWorkPlanDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler developGridTermDSHandler() {
        return new DevelopGridTermDSHandler(getName());
    }
}
