package ru.tandemservice.movestudentrmc.base.bo.DORepresentGrantCancelAndDestination.logic;

import ru.tandemservice.movestudentrmc.docord.IDOObjectModifyDAO;
import ru.tandemservice.movestudentrmc.entity.DocRepresentOrderCancel;
import ru.tandemservice.movestudentrmc.entity.RepresentGrantCancelAndDestination;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntity;

import java.util.List;

public interface IDORepresentGrantCancelAndDestinationDAO extends IDOObjectModifyDAO {

    public void saveStudentGrantEntity(List<StudentGrantEntity> sgeList, RepresentGrantCancelAndDestination represent);

    public void deleteStudentGrantEntity(Long representId);

    public void saveOrderCancel(List<DocRepresentOrderCancel> ordersLis);

    public void deleteOrderCancel(Long representId);
}
