package ru.tandemservice.movestudentrmc.component.settings.checkOnOrderToGrantView.CheckOnOrderToGrantViewPub;

import com.google.common.collect.ImmutableList;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.movestudentrmc.entity.RelCheckOnorderGrantView;
import ru.tandemservice.movestudentrmc.entity.RelTypeGrantView;
import ru.tandemservice.movestudentrmc.entity.catalog.CheckOnOrder;
import ru.tandemservice.movestudentrmc.entity.catalog.Grant;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        CheckOnOrder check = get(CheckOnOrder.class, model.getCheck().getId());
        if (check != null)
            model.setCheck(check);

        model.setUsedModel(new LazySimpleSelectModel<>(ImmutableList.of(
                new IdentifiableWrapper(Model.SHOW_USE_CODE, "Да"),
                new IdentifiableWrapper(Model.SHOW_NON_USE_CODE, "Нет"))));
    }

    @Override
    public void prepareListDataSource(Model model) {

        DQLSelectBuilder builder = createBuilder(model);

        List<Object[]> objs = getList(builder);
        List<Wrapper> lst = new ArrayList<>();

        IdentifiableWrapper used = (IdentifiableWrapper) model.getSettings().get("used");

        for (Object[] obj : objs) {
            GrantView view = (GrantView) obj[0];
            Grant grant = (Grant) obj[1];

            RelCheckOnorderGrantView rel = getCheckOnorderGrantView(model.getCheck(), view);

            boolean use = false;
            boolean value = false;

            if (rel != null) {
                use = true;
                value = rel.isValue();
            }

            if (used != null)
                if (!(use == used.getId().equals(Model.SHOW_USE_CODE)))
                    continue;

            lst.add(new Wrapper(grant, view, use, value));
        }

        model.getDataSource().setCountRow(lst.size());

        Collections.sort(lst, new EntityComparator<>(model.getDataSource().getEntityOrder()));
        UniUtils.createPage(model.getDataSource(), lst);
    }

    private RelCheckOnorderGrantView getCheckOnorderGrantView(CheckOnOrder check, GrantView view) {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RelCheckOnorderGrantView.class, "rel")
                .where(DQLExpressions.eqValue(DQLExpressions.property(RelCheckOnorderGrantView.check().fromAlias("rel")), check))
                .where(DQLExpressions.eqValue(DQLExpressions.property(RelCheckOnorderGrantView.view().fromAlias("rel")), view));

        return builder.createStatement(getSession()).uniqueResult();
    }

    private DQLSelectBuilder createBuilder(Model model) {

        String title = (String) model.getSettings().get("title");
        String grantView = (String) model.getSettings().get("grantView");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(GrantView.class, "gv")
                .column("gv")
                .column(DQLExpressions.property(RelTypeGrantView.grant().fromAlias("rel")))
                .joinEntity("gv", DQLJoinType.left, RelTypeGrantView.class, "rel",
                            DQLExpressions.eq(
                                    DQLExpressions.property(GrantView.id().fromAlias("gv")),
                                    DQLExpressions.property(RelTypeGrantView.view().id().fromAlias("rel"))))
                .order(DQLExpressions.property(RelTypeGrantView.grant().title().fromAlias("rel")));

        if (title != null)
            builder.where(DQLExpressions.like(DQLExpressions.property(RelTypeGrantView.grant().title().fromAlias("rel")),
                                              DQLExpressions.value(CoreStringUtils.escapeLike(title))));
        if (grantView != null)
            builder.where(DQLExpressions.like(DQLExpressions.property(GrantView.title().fromAlias("gv")),
                                              DQLExpressions.value(CoreStringUtils.escapeLike(grantView))));

        return builder;
    }

    @Override
    public void updateInUse(Model model, Long id) {
        GrantView view = getNotNull(id);
        RelCheckOnorderGrantView rel = getCheckOnorderGrantView(model.getCheck(), view);

        if (rel == null) {
            RelCheckOnorderGrantView entity = new RelCheckOnorderGrantView();
            entity.setCheck(model.getCheck());
            entity.setView(view);
            getSession().saveOrUpdate(entity);
        }
        else {
            new DQLDeleteBuilder(RelCheckOnorderGrantView.class)
                    .where(DQLExpressions.eqValue(DQLExpressions.property(RelCheckOnorderGrantView.check()), model.getCheck()))
                    .where(DQLExpressions.eqValue(DQLExpressions.property(RelCheckOnorderGrantView.view()), view))
                    .createStatement(getSession()).execute();
        }
    }

    @Override
    public void updateValue(Model model, Long id) {
        GrantView view = getNotNull(id);
        RelCheckOnorderGrantView rel = getCheckOnorderGrantView(model.getCheck(), view);

        if (rel != null) {
            if (rel.isValue())
                rel.setValue(false);
            else
                rel.setValue(true);

            getSession().saveOrUpdate(rel);
        }
    }


}
