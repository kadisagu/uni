package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.DORepresentBaseManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentEditUI;
import ru.tandemservice.movestudentrmc.docord.IDOObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.OrderCategoryCodes;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentationTypeCodes;
import ru.tandemservice.movestudentrmc.util.CheckUtil;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;

import java.util.Arrays;
import java.util.List;

@State(
        @Bind(key = DORepresentBaseAddUI.STUDENT_LIST_ID, binding = DORepresentBaseAddUI.STUDENT_LIST_ID)
)
public class DORepresentBaseAddUI extends UIPresenter {

    public static final String STUDENT_LIST_ID = "studentListId";
    private List<Long> _studentListId;
    private RepresentationType _type;

    @Override
    public void onComponentRefresh() {
        super.onComponentRefresh();
    }

    public void onClickCreate()
    {
        /**
         * Если статус студента неактивен, выведем warning
         */
        if (!isStudentActive()) {
            _uiSettings.set("displayWarning", true);
            _uiSettings.set("warningMes", "Представление формируется для студента находящегося в статусе \"" + getStudent().getStatus().getTitle() + "\"");
            ContextLocal.getInfoCollector().clear();
        }
        else {
            onClickAppyNext();
        }

    }

    public void onClickAppyNext() {

        if (hasOtherRepresentations(getStudentListId()) && !getType().getOrderCategory().getCode().equals(OrderCategoryCodes.GRANTS))
            throw new ApplicationException("Для " + PersonManager.instance().declinationDao().getDeclinationFIO(getStudent().getPerson().getIdentityCard(), GrammaCase.GENITIVE) + " уже сформировано одно представление. Чтобы сформировать очередное представление необходимо провести по приказу ранее созданное.");
        //для представления о допуске к посещению занятий проверяем есть ли у студента УП
        if (_type.getCode().equals(RepresentationTypeCodes.ADMISSION_ATTEND_CLASSES))
            if (IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionRelation(getStudent().getId()) == null)
                throw new ApplicationException("Для студента не определен учебный план. Чтобы сформировать представление необходимо определить УП для текущего студента.");

        IDOObjectByRepresentTypeManager representManager = DORepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(_type.getCode());

        try {
            _uiActivation.asRegion(representManager.getEditComponentManager())
                    .parameter(AbstractDORepresentEditUI.TYPE_ID, _type.getId())
                    .parameter(AbstractDORepresentEditUI.STUDENT_LIST_ID, _studentListId)
                    .activate();
        }
        catch (Exception e) {
            ContextLocal.getErrorCollector().add("Невозможно создать представление для выбранного типа.");
        }

    }


    /**
     * В случае если для выбранного студента существует представление, которое отправлено:
     * на согласование или согласовано, но не проведено в приказе,
     * то создание нового представления не должно предоставляться возможным.
     */
    public boolean hasOtherRepresentations(List<Long> studentIds) {

        Long stuId = getStudentListId().get(0);
        Student student = DataAccessServices.dao().get(Student.class, stuId);

        return !CheckUtil.checkExistingRepresents(null, Arrays.asList(student)).isEmpty();

    }

    /**
     * Является ли статус студента активным
     *
     * @return boolean status
     */
    public boolean isStudentActive() {
        if (!getStudentListId().isEmpty()) {
            Long stuId = getStudentListId().get(0);
            Student student = DataAccessServices.dao().get(Student.class, stuId);
            return student.getStatus().isActive();
            //return UniDefines.CATALOG_STUDENT_STATUS_ACTIVE.equals(student.getStatus().getCode());
        }
        return false;
    }

    public Student getStudent() {
        return DataAccessServices.dao().get(Student.class, getStudentListId().get(0));
    }

    public RepresentationType getType() {
        return _type;
    }

    public void setRepresentation(RepresentationType type) {
        _type = type;
    }

    public List<Long> getStudentListId() {
        return _studentListId;
    }

    public void setStudentListId(List<Long> studentListId) {
        _studentListId = studentListId;
    }
}
