package ru.tandemservice.movestudentrmc.dao;

import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;

import java.util.Date;

public interface ICustomStateDAO extends IUniBaseDao {
    @Transactional
    public void commitStudentCustomState(StudentCustomStateCI state, Date beginDate, Date endDate, ListRepresent represent);

    public void rollbackStudentCustomState(ListRepresent represent);

    @Transactional
    public void commitStudentCustomState(StudentCustomStateCI state, Date beginDate, Date endDate, Representation represent);

    public void rollbackStudentCustomState(Representation represent);
}
