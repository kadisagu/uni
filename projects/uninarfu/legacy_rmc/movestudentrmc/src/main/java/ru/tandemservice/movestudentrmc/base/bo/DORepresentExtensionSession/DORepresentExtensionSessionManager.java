package ru.tandemservice.movestudentrmc.base.bo.DORepresentExtensionSession;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentExtensionSession.logic.DORepresentExtensionSessionDAO;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentExtensionSession.ui.Edit.DORepresentExtensionSessionEdit;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentExtensionSession.ui.View.DORepresentExtensionSessionView;
import ru.tandemservice.movestudentrmc.docord.IDOObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IDOObjectModifyDAO;

@Configuration
public class DORepresentExtensionSessionManager extends BusinessObjectManager implements IDOObjectByRepresentTypeManager {

    public static DORepresentExtensionSessionManager instance()
    {
        return instance(DORepresentExtensionSessionManager.class);
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return DORepresentExtensionSessionEdit.class;
    }

    @Bean
    public IDOObjectModifyDAO getDOObjectModifyDAO() {
        return new DORepresentExtensionSessionDAO();
    }

    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return DORepresentExtensionSessionView.class;
    }
}
