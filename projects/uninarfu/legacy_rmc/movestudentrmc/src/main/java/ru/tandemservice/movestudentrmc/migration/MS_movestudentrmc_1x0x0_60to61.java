package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_movestudentrmc_1x0x0_60to61 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.5.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.5.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность representCourseTransfer

        // создано обязательное свойство conditionally
        {
            // создать колонку
            tool.createColumn("representcoursetransfer_t", new DBColumn("conditionally_p", DBType.BOOLEAN));

            // задать значение по умолчанию
            java.lang.Boolean defaultConditionally = false;        // TODO: правильно?
            tool.executeUpdate("update representcoursetransfer_t set conditionally_p=? where conditionally_p is null", defaultConditionally);

            // сделать колонку NOT NULL
            tool.setColumnNullable("representcoursetransfer_t", "conditionally_p", false);

        }


    }
}