package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.RepresentStudentTicket;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.entity.catalog.DocumentTypeForRepresent;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * О выдаче дубликата зачетной книжки/студенческого билета
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RepresentStudentTicketGen extends Representation
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.RepresentStudentTicket";
    public static final String ENTITY_NAME = "representStudentTicket";
    public static final int VERSION_HASH = -473921294;
    private static IEntityMeta ENTITY_META;

    public static final String P_REPRIMAND = "reprimand";
    public static final String L_DOCUMENT_TYPE = "documentType";

    private boolean _reprimand = false;     // С объявлением выговора
    private DocumentTypeForRepresent _documentType;     // Тип выдаваемого документа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return С объявлением выговора. Свойство не может быть null.
     */
    @NotNull
    public boolean isReprimand()
    {
        return _reprimand;
    }

    /**
     * @param reprimand С объявлением выговора. Свойство не может быть null.
     */
    public void setReprimand(boolean reprimand)
    {
        dirty(_reprimand, reprimand);
        _reprimand = reprimand;
    }

    /**
     * @return Тип выдаваемого документа. Свойство не может быть null.
     */
    @NotNull
    public DocumentTypeForRepresent getDocumentType()
    {
        return _documentType;
    }

    /**
     * @param documentType Тип выдаваемого документа. Свойство не может быть null.
     */
    public void setDocumentType(DocumentTypeForRepresent documentType)
    {
        dirty(_documentType, documentType);
        _documentType = documentType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof RepresentStudentTicketGen)
        {
            setReprimand(((RepresentStudentTicket)another).isReprimand());
            setDocumentType(((RepresentStudentTicket)another).getDocumentType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RepresentStudentTicketGen> extends Representation.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RepresentStudentTicket.class;
        }

        public T newInstance()
        {
            return (T) new RepresentStudentTicket();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "reprimand":
                    return obj.isReprimand();
                case "documentType":
                    return obj.getDocumentType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "reprimand":
                    obj.setReprimand((Boolean) value);
                    return;
                case "documentType":
                    obj.setDocumentType((DocumentTypeForRepresent) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "reprimand":
                        return true;
                case "documentType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "reprimand":
                    return true;
                case "documentType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "reprimand":
                    return Boolean.class;
                case "documentType":
                    return DocumentTypeForRepresent.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RepresentStudentTicket> _dslPath = new Path<RepresentStudentTicket>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RepresentStudentTicket");
    }
            

    /**
     * @return С объявлением выговора. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentStudentTicket#isReprimand()
     */
    public static PropertyPath<Boolean> reprimand()
    {
        return _dslPath.reprimand();
    }

    /**
     * @return Тип выдаваемого документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentStudentTicket#getDocumentType()
     */
    public static DocumentTypeForRepresent.Path<DocumentTypeForRepresent> documentType()
    {
        return _dslPath.documentType();
    }

    public static class Path<E extends RepresentStudentTicket> extends Representation.Path<E>
    {
        private PropertyPath<Boolean> _reprimand;
        private DocumentTypeForRepresent.Path<DocumentTypeForRepresent> _documentType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return С объявлением выговора. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentStudentTicket#isReprimand()
     */
        public PropertyPath<Boolean> reprimand()
        {
            if(_reprimand == null )
                _reprimand = new PropertyPath<Boolean>(RepresentStudentTicketGen.P_REPRIMAND, this);
            return _reprimand;
        }

    /**
     * @return Тип выдаваемого документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentStudentTicket#getDocumentType()
     */
        public DocumentTypeForRepresent.Path<DocumentTypeForRepresent> documentType()
        {
            if(_documentType == null )
                _documentType = new DocumentTypeForRepresent.Path<DocumentTypeForRepresent>(L_DOCUMENT_TYPE, this);
            return _documentType;
        }

        public Class getEntityClass()
        {
            return RepresentStudentTicket.class;
        }

        public String getEntityName()
        {
            return "representStudentTicket";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
