package ru.tandemservice.movestudentrmc.component.settings.checkOnOrderToGrantView.CheckOnOrderToGrantViewPub;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {

    void updateInUse(Model model, Long id);

    void updateValue(Model model, Long id);
}
