package ru.tandemservice.movestudentrmc.base.bo.RelationSettingsOSSP.ui.Edit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.base.bo.RelationSettingsOSSP.DORelationSettingsOSSPManager;
import ru.tandemservice.movestudentrmc.base.bo.RelationSettingsOSSP.logic.RepresentRelOSSP;
import ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonOSSP;
import ru.tandemservice.movestudentrmc.entity.catalog.OsspGrants;
import ru.tandemservice.movestudentrmc.entity.catalog.OsspPgo;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;

import java.util.ArrayList;
import java.util.List;

@Input(
        @Bind(key = RelationSettingsOSSPEditUI.TYPE_REPRESENTATION_ID, binding = RelationSettingsOSSPEditUI.TYPE_REPRESENTATION_ID)
)
public class RelationSettingsOSSPEditUI extends UIPresenter {

    public static final String TYPE_REPRESENTATION_ID = "typeRepresentationId";

    private Long typeRepresentationId;
    private RepresentationType typeRepresentation;
    private List<RepresentRelOSSP> reasons;
    private List<RepresentationReason> reasonsList;
    private RepresentRelOSSP reasonRow;
    private String pageView;
    private int pageNum = 0;
    private int pageEnd = 1;
    private boolean firstRun = true;
    private boolean isAddForm = true;
    private List<RelRepresentationReasonOSSP> osspList;

    @Override
    public void onComponentRefresh()
    {

        setPageView(getClass().getPackage().getName() + ".page" + (getPageNum()));

        if (!firstRun) {

            return;
        }

        if (null != typeRepresentationId)
            typeRepresentation = DataAccessServices.dao().getNotNull(RepresentationType.class, typeRepresentationId);
        else {
            typeRepresentation = new RepresentationType();
        }

        if (typeRepresentation.isConfigOSSP()) {
            setPageNum(1);
        }
        else {
            setPageNum(0);
        }

        setPageView(getClass().getPackage().getName() + ".page" + (getPageNum()));
        isAddForm = false;
        fillArrayRel();

        if (firstRun)
            firstRun = !firstRun;
    }

    private void fillArrayRel() {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RelRepresentationReasonOSSP.class, "rel").addColumn("rel");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(RelRepresentationReasonOSSP.type().id().fromAlias("rel")),
                DQLExpressions.value(typeRepresentationId)));

        osspList = builder.createStatement(getSupport().getSession()).list();

        if (getReasonsList() == null)
            setReasonsList(new ArrayList<RepresentationReason>());
        else
            getReasonsList().clear();

        if (getReasons() == null)
            setReasons(new ArrayList<RepresentRelOSSP>());
        else
            getReasons().clear();

        for (RelRepresentationReasonOSSP ossp : osspList) {

            if (!getReasonsList().contains(ossp.getReason()))
                getReasonsList().add(ossp.getReason());
            boolean isFind = false;
            for (RepresentRelOSSP relOSSP : getReasons()) {
                if (relOSSP.getReason().equals(ossp.getReason()) && relOSSP.isBudget() == ossp.isBudget()) {
                    isFind = true;
                    updateOSSPList(relOSSP, ossp);
                    break;
                }
            }

            if (!isFind) {
                RepresentRelOSSP relOSSP = new RepresentRelOSSP();
                relOSSP.setReason(ossp.getReason());
                relOSSP.setBudget(ossp.isBudget());
                relOSSP.setGrantsList(new ArrayList<OsspGrants>());
                relOSSP.setPgoList(new ArrayList<OsspPgo>());
                updateOSSPList(relOSSP, ossp);
                getReasons().add(relOSSP);
                relOSSP = new RepresentRelOSSP();
                relOSSP.setReason(ossp.getReason());
                relOSSP.setBudget(!ossp.isBudget());
                relOSSP.setGrantsList(new ArrayList<OsspGrants>());
                relOSSP.setPgoList(new ArrayList<OsspPgo>());
                getReasons().add(relOSSP);
            }
        }
    }

    private void updateOSSPList(RepresentRelOSSP relOSSP, RelRepresentationReasonOSSP ossp) {


        if (ossp.getGrantsOSSP() != null)
            if (!relOSSP.getGrantsList().contains(ossp.getGrantsOSSP())) {
                relOSSP.getGrantsList().add(ossp.getGrantsOSSP());
            }

        if (ossp.getPgoOSSP() != null)
            if (!relOSSP.getPgoList().contains(ossp.getPgoOSSP())) {
                relOSSP.getPgoList().add(ossp.getPgoOSSP());
            }
    }

    public void onClickApply()
    {
        if (isAddForm)
            DORelationSettingsOSSPManager.instance().modifyDao().save(getTypeRepresentation(), getReasons());
        else
            DORelationSettingsOSSPManager.instance().modifyDao().update(getTypeRepresentation(), getReasons(), osspList);

        deactivate();
    }

    public void onClickPrev() {

        setPageNum(getPageNum() - 1);
        setPageView(getClass().getPackage().getName() + ".page" + (getPageNum()));
    }

    public void onClickNext() {

        if (pageNum == 0) {
            mergeArraysRel();
        }

        setPageNum(getPageNum() + 1);
        setPageView(getClass().getPackage().getName() + ".page" + (getPageNum()));
    }

    public void mergeArraysRel() {

        List<RepresentRelOSSP> isFound = new ArrayList<RepresentRelOSSP>();

        if (getReasons() == null)
            setReasons(new ArrayList<RepresentRelOSSP>());
        for (RepresentationReason reason : getReasonsList()) {
            Boolean isFind = false;
            for (RepresentRelOSSP ossp : getReasons()) {
                if (ossp.getReason().equals(reason)) {
                    isFound.add(ossp);
                    isFind = true;
                }
            }

            if (!isFind) {
                RepresentRelOSSP ossp = new RepresentRelOSSP();
                ossp.setBudget(false);
                ossp.setReason(reason);
                isFound.add(ossp);
                ossp = new RepresentRelOSSP();
                ossp.setBudget(true);
                ossp.setReason(reason);
                isFound.add(ossp);

            }
        }


        getReasons().clear();
        getReasons().addAll(isFound);
    }

//    Getters and Setters

    public List<RepresentationReason> getReasonsList() {
        return reasonsList;
    }

    public void setReasonsList(List<RepresentationReason> reasonsList) {
        this.reasonsList = reasonsList;
    }

    public List<RepresentRelOSSP> getReasons() {
        return reasons;
    }

    public void setReasons(List<RepresentRelOSSP> reasons) {
        this.reasons = reasons;
    }

    public RepresentRelOSSP getReasonRow() {
        return reasonRow;
    }

    public void setReasonRow(RepresentRelOSSP reasonRow) {
        this.reasonRow = reasonRow;
    }

    public String getPageView() {
        return pageView;
    }

    public void setPageView(String pageView) {
        this.pageView = pageView;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageEnd() {
        return pageEnd;
    }

    public void setPageEnd(int pageEnd) {
        this.pageEnd = pageEnd;
    }

    public Long getTypeRepresentationId() {
        return typeRepresentationId;
    }

    public void setTypeRepresentationId(Long typeRepresentationId) {
        this.typeRepresentationId = typeRepresentationId;
    }

    public RepresentationType getTypeRepresentation() {
        return typeRepresentation;
    }

    public void setTypeRepresentation(RepresentationType typeRepresentation) {
        this.typeRepresentation = typeRepresentation;
    }
}
