package ru.tandemservice.movestudentrmc.component.student.DocumentList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.DateColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.movestudentrmc.entity.NarfuDocument;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;


public class Controller extends AbstractBusinessController<IDAO, Model> {

    public void onRefreshComponent(IBusinessComponent component) {
        getDao().prepare(getModel(component));

        prepareDataSource(component);
    }

    public void onClickAdd(IBusinessComponent component) {
        Model model = getModel(component);
        activate(
                component,
                new ComponentActivator("ru.tandemservice.movestudentrmc.component.student.DocumentAddEdit", new UniMap().add("studentId", model.getStudent().getId()))
        );
    }

    public void onClickEdit(IBusinessComponent component) {
        Model model = getModel(component);
        Long documentId = component.getListenerParameter();
        activate(
                component,
                new ComponentActivator("ru.tandemservice.movestudentrmc.component.student.DocumentAddEdit", new UniMap().add("studentId", model.getStudent().getId()).add("documentId", documentId))
        );
    }

    public void onClickDelete(IBusinessComponent component) {
        Long documentId = component.getListenerParameter();

        getDao().delete(documentId);
    }

    protected void prepareDataSource(IBusinessComponent component) {
        final Model model = getModel(component);
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<NarfuDocument> dataSource = new DynamicListDataSource<>(component, cmp -> getDao().prepareListDataSource(model));

        dataSource.addColumn(new SimpleColumn("Вид документа", NarfuDocument.settings().kind().title().s()).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Документ", NarfuDocument.settings().document().title().s()).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Название", NarfuDocument.name().s()).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Доп. название", NarfuDocument.additionalName().s()).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Номер документа", NarfuDocument.number().s()).setOrderable(true));
        dataSource.addColumn(new DateColumn("Дата выдачи", NarfuDocument.issuanceDate().s()).setOrderable(true));

        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickPrint", "Печатать файл").setEnabledProperty("attachmentExist").setPermissionKey("rmc_print_document"));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit").setPermissionKey("rmc_add_edit_document"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete").setPermissionKey("rmc_delete_document"));

        model.setDataSource(dataSource);
    }

    public void onClickPrint(IBusinessComponent component) {
        Model model = component.getModel();

        NarfuDocument doc = model.getDataSource().getRecordById((Long) component.getListenerParameter());

        byte[] content = doc.getFile().getContent();
        if (content == null)
            throw new ApplicationException("Файл документа пуст.");
        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(content, doc.getFile().getFilename());
        activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new UniMap().add("id", id)));
    }
}
