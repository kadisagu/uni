package ru.tandemservice.movestudentrmc.base.bo.DORepresentWeekend.ui.Edit;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;

import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentEditUI;
import ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab.MonthWrapper;
import ru.tandemservice.movestudentrmc.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.RepresentGrant;
import ru.tandemservice.movestudentrmc.entity.RepresentWeekend;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.StudentCustomStateCICodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.util.ReportRenderer;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import static org.tandemframework.hibsupport.dql.DQLExpressions.and;
import static org.tandemframework.hibsupport.dql.DQLExpressions.between;
import static org.tandemframework.hibsupport.dql.DQLExpressions.gt;
import static org.tandemframework.hibsupport.dql.DQLExpressions.lt;
import static org.tandemframework.hibsupport.dql.DQLExpressions.or;
import static org.tandemframework.hibsupport.dql.DQLExpressions.plus;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;
import static org.tandemframework.hibsupport.dql.DQLExpressions.valueDate;
import static org.tandemframework.hibsupport.dql.DQLFunctions.createTimestamp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DORepresentWeekendEditUI extends AbstractDORepresentEditUI
{
	//----------------------------------------------------------------------------------------------------------------------------------------
	public boolean isShowWithPayment() {
		
		//Видимость параметра «с назначением компенсационной выплаты» для причины «по медицинским показаниям»
		return getRepresentObj() != null && getRepresentObj().getReason() != null && getRepresentObj().getReason().getCode().equals("narfu-1.13");

	}
	//----------------------------------------------------------------------------------------------------------------------------------------

    @Override
    protected void initDocument() {
        super.initDocument();
        _studentStatusNewStr = UniDaoFacade.getCoreDao().getCatalogItem(StudentStatus.class, "5").getTitle();
    }

    @Override
    protected void FetchStudents(Representation doc) {

        _studentDocList.clear();
        _studentList.clear();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocRepresentStudentBase.class, "stud").addColumn("stud");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(DocRepresentStudentBase.representation().fromAlias("stud")),
                DQLExpressions.value(doc.getId())));

        List<DocRepresentStudentBase> studentList = builder.createStatement(getSupport().getSession()).list();

        for (DocRepresentStudentBase student : studentList) {

            _studentDocList.add(student);
            _studentList.add(student.getStudent());
        }

        _student = _studentList.get(0);
    }

	//------------------------------------------------------------------------------------------------------------------------------------------
	@Override
	public void onClickSave() throws IOException
	{

		RepresentWeekend represent = (RepresentWeekend) this.getRepresentObj();

		if (represent.isWithPayment())
		{

			//Сохраняем назначенные студенту ежемесячные компенсационные выплаты

			if (CommonBaseDateUtil.isAfter(represent.getStartDate(), represent.getEndDate()))
			{
				throw new ApplicationException("Дата начала академического отпуска не может быть позже даты его окончания");
			}

			GrantView grant = UniDaoFacade.getCoreDao().getByCode(GrantView.class, "206");    //ежемесячная компенсационная выплата студентам, находящимся в академическом отпуске по медицинским показаниям

			DQLSelectBuilder eduYearBuilder = new DQLSelectBuilder()
					.fromEntity(EducationYear.class, "year")
					.where(or(between(valueDate(represent.getStartDate())
							, createTimestamp(property(EducationYear.intValue().fromAlias("year")), value(9), value(1), value(0), value(0), value(0))
							, createTimestamp(plus(property(EducationYear.intValue().fromAlias("year")), value(1)), value(8), value(31), value(23), value(59), value(59)))
							, between(valueDate(represent.getEndDate())
									, createTimestamp(property(EducationYear.intValue().fromAlias("year")), value(9), value(1), value(0), value(0), value(0))
									, createTimestamp(plus(property(EducationYear.intValue().fromAlias("year")), value(1)), value(8), value(31), value(23), value(59), value(59)))

							, and(lt(valueDate(represent.getStartDate()), createTimestamp(property(EducationYear.intValue().fromAlias("year")), value(9), value(1), value(0), value(0), value(0)))
									, gt(valueDate(represent.getEndDate()), createTimestamp(plus(property(EducationYear.intValue().fromAlias("year")), value(1)), value(8), value(31), value(23), value(59), value(59))))
					));

			List<EducationYear> eduYearList = UniDaoFacade.getCoreDao().getList(eduYearBuilder);

			List<String> errorList = new ArrayList<>();

			for (EducationYear eduYear : eduYearList)
			{

				Date beginDate;
				Date endDate;

				Calendar beginYear = Calendar.getInstance();
				beginYear.set(eduYear.getIntValue(), Calendar.SEPTEMBER, 1, 0, 0, 0);
				beginYear.set(Calendar.MILLISECOND, 0);
				if (represent.getStartDate().compareTo(beginYear.getTime()) > 0)
				{
					beginDate = represent.getStartDate();
				}
				else
				{
					beginDate = beginYear.getTime();
				}

				Calendar endYear = Calendar.getInstance();
				endYear.set(eduYear.getIntValue() + 1, Calendar.AUGUST, 31, 23, 59, 59);
				endYear.set(Calendar.MILLISECOND, 999);
				if (represent.getEndDate().compareTo(endYear.getTime()) > 0)
				{
					endDate = endYear.getTime();
				}
				else
				{
					endDate = represent.getEndDate();
				}

				List<String> periodList = MonthWrapper.getMonthsList(beginDate, endDate, eduYear);

				MoveStudentDaoFacade.getGrantDAO().checkStudentGrants(represent.getId(), grant, this.getStudentList(), periodList, errorList);

			}

			if (!errorList.isEmpty())
			{

				RtfDocument errorReport = RtfBean.getElementFactory().createRtfDocument();
				IRtfControl par = RtfBean.getElementFactory().createRtfControl(IRtfData.PAR);
				for (String text : errorList)
				{
					IRtfText studText = RtfBean.getElementFactory().createRtfText(text);
					errorReport.addElement(studText);
					errorReport.getElementList().addAll(Arrays.asList(par, par));
				}
				BusinessComponentUtils.downloadDocument(new ReportRenderer("Ошибки.rtf", errorReport), true);

				throw new ApplicationException("Не удалось сохранить представление");

			}

			super.onClickSave();

			MoveStudentDaoFacade.getGrantDAO().deleteStudentGrants(represent);

			for (EducationYear eduYear : eduYearList)
			{

				Date beginDate;
				Date endDate;

				Calendar beginYear = Calendar.getInstance();
				beginYear.set(eduYear.getIntValue(), Calendar.SEPTEMBER, 1, 0, 0, 0);
				beginYear.set(Calendar.MILLISECOND, 0);
				if (represent.getStartDate().compareTo(beginYear.getTime()) > 0)
				{
					beginDate = represent.getStartDate();
				}
				else
				{
					beginDate = beginYear.getTime();
				}

				Calendar endYear = Calendar.getInstance();
				endYear.set(eduYear.getIntValue() + 1, Calendar.AUGUST, 31, 23, 59, 59);
				endYear.set(Calendar.MILLISECOND, 999);
				if (represent.getEndDate().compareTo(endYear.getTime()) > 0)
				{
					endDate = endYear.getTime();
				}
				else
				{
					endDate = represent.getEndDate();
				}

				List<String> periodList = MonthWrapper.getMonthsList(beginDate, endDate, eduYear);

				MoveStudentDaoFacade.getGrantDAO().saveStudentGrants(represent, grant, _student, periodList, eduYear, false);

			}
		}
		else
		{

			super.onClickSave();

			MoveStudentDaoFacade.getGrantDAO().deleteStudentGrants(represent);

		}
	}
	//------------------------------------------------------------------------------------------------------------------------------------------
}