package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudentrmc.entity.IStudentGrant;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import ru.tandemservice.movestudentrmc.entity.IStudentGrant;

@SuppressWarnings({"all", "unchecked", "unused"})
public abstract class IStudentGrantGen extends InterfaceStubBase
 implements IStudentGrant{
    public static final int VERSION_HASH = 1452461922;

    public static final String P_BEGIN_GRANT_DATE = "beginGrantDate";
    public static final String L_GRANT_VIEW = "grantView";
    public static final String P_END_GRANT_DATE = "endGrantDate";

    private Date _beginGrantDate;
    private GrantView _grantView;
    private Date _endGrantDate;

    @NotNull

    public Date getBeginGrantDate()
    {
        return _beginGrantDate;
    }

    public void setBeginGrantDate(Date beginGrantDate)
    {
        _beginGrantDate = beginGrantDate;
    }

    @NotNull

    public GrantView getGrantView()
    {
        return _grantView;
    }

    public void setGrantView(GrantView grantView)
    {
        _grantView = grantView;
    }


    public Date getEndGrantDate()
    {
        return _endGrantDate;
    }

    public void setEndGrantDate(Date endGrantDate)
    {
        _endGrantDate = endGrantDate;
    }

    private static final Path<IStudentGrant> _dslPath = new Path<IStudentGrant>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ru.tandemservice.movestudentrmc.entity.IStudentGrant");
    }
            

    /**
     * @return Дата назначения выплат. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.IStudentGrant#getBeginGrantDate()
     */
    public static PropertyPath<Date> beginGrantDate()
    {
        return _dslPath.beginGrantDate();
    }

    /**
     * @return Вид стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.IStudentGrant#getGrantView()
     */
    public static GrantView.Path<GrantView> grantView()
    {
        return _dslPath.grantView();
    }

    /**
     * @return Дата окончания выплат.
     * @see ru.tandemservice.movestudentrmc.entity.IStudentGrant#getEndGrantDate()
     */
    public static PropertyPath<Date> endGrantDate()
    {
        return _dslPath.endGrantDate();
    }

    public static class Path<E extends IStudentGrant> extends EntityPath<E>
    {
        private PropertyPath<Date> _beginGrantDate;
        private GrantView.Path<GrantView> _grantView;
        private PropertyPath<Date> _endGrantDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата назначения выплат. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.IStudentGrant#getBeginGrantDate()
     */
        public PropertyPath<Date> beginGrantDate()
        {
            if(_beginGrantDate == null )
                _beginGrantDate = new PropertyPath<Date>(IStudentGrantGen.P_BEGIN_GRANT_DATE, this);
            return _beginGrantDate;
        }

    /**
     * @return Вид стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.IStudentGrant#getGrantView()
     */
        public GrantView.Path<GrantView> grantView()
        {
            if(_grantView == null )
                _grantView = new GrantView.Path<GrantView>(L_GRANT_VIEW, this);
            return _grantView;
        }

    /**
     * @return Дата окончания выплат.
     * @see ru.tandemservice.movestudentrmc.entity.IStudentGrant#getEndGrantDate()
     */
        public PropertyPath<Date> endGrantDate()
        {
            if(_endGrantDate == null )
                _endGrantDate = new PropertyPath<Date>(IStudentGrantGen.P_END_GRANT_DATE, this);
            return _endGrantDate;
        }

        public Class getEntityClass()
        {
            return IStudentGrant.class;
        }

        public String getEntityName()
        {
            return "ru.tandemservice.movestudentrmc.entity.IStudentGrant";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
