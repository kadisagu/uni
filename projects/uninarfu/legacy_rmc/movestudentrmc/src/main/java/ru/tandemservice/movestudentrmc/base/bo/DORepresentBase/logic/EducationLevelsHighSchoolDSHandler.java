package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

public class EducationLevelsHighSchoolDSHandler extends DefaultComboDataSourceHandler {

    public static final String NEW_FORMATIVE_ORG_UNIT = "newFormativeOrgUnit";
    public static final String NEW_TERRITORIAL_ORG_UNIT = "newTerritorialOrgUnit";

    public static final String FILTER_BY_DEVELOP = "filterByDevelop";
    public static final String DEVELOP_FORM = "developForm";
    public static final String DEVELOP_CONDITION = "developCondition";
    public static final String DEVELOP_PERIOD = "developPeriod";
    public static final String DEVELOP_TECH = "developTech";

    public EducationLevelsHighSchoolDSHandler(String ownerId) {
        super(ownerId, EducationLevelsHighSchool.class);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep) {
        String str = ep.input.getComboFilterByValue();
        if (StringUtils.isNotBlank(str))
            ep.dqlBuilder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property("e", EducationLevelsHighSchool.fullTitle())), DQLExpressions.value(CoreStringUtils.escapeLike(str, true))));
        OrgUnit newFormativeOrgUnit = ep.context.get(NEW_FORMATIVE_ORG_UNIT);
        OrgUnit newTerritorialOrgUnit = ep.context.get(NEW_TERRITORIAL_ORG_UNIT);

        Boolean filterByDevelop = ep.context.get(FILTER_BY_DEVELOP);
        DevelopForm devForm = ep.context.get(DEVELOP_FORM);
        DevelopCondition devCondition = ep.context.get(DEVELOP_CONDITION);
        DevelopPeriod devPeriod = ep.context.get(DEVELOP_PERIOD);
        DevelopTech devTech = ep.context.get(DEVELOP_TECH);

        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(EducationOrgUnit.class, "eou");

        builder.column("eou." + EducationOrgUnit.educationLevelHighSchool().s());

        builder.where(DQLExpressions.eq(DQLExpressions.property(EducationOrgUnit.formativeOrgUnit().fromAlias("eou")), DQLExpressions.value(newFormativeOrgUnit)));
        if (newTerritorialOrgUnit != null)
            builder.where(DQLExpressions.eq(DQLExpressions.property(EducationOrgUnit.territorialOrgUnit().fromAlias("eou")), DQLExpressions.value(newTerritorialOrgUnit)));

        if (Boolean.TRUE.equals(filterByDevelop)) {
            builder.where(DQLExpressions.eq(DQLExpressions.property(EducationOrgUnit.developForm().fromAlias("eou")), DQLExpressions.value(devForm)));
            builder.where(DQLExpressions.eq(DQLExpressions.property(EducationOrgUnit.developCondition().fromAlias("eou")), DQLExpressions.value(devCondition)));
            builder.where(DQLExpressions.eq(DQLExpressions.property(EducationOrgUnit.developPeriod().fromAlias("eou")), DQLExpressions.value(devPeriod)));
            builder.where(DQLExpressions.eq(DQLExpressions.property(EducationOrgUnit.developTech().fromAlias("eou")), DQLExpressions.value(devTech)));
        }

        ep.dqlBuilder.where(DQLExpressions.in(DQLExpressions.property(EducationLevelsHighSchool.id().fromAlias("e")), builder.getQuery()));

    }


    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep) {

        ep.dqlBuilder.order(DQLExpressions.property(EducationLevelsHighSchool.educationLevel().inheritedOkso().fromAlias("e")));
        ep.dqlBuilder.order(DQLExpressions.property(EducationLevelsHighSchool.educationLevel().title().fromAlias("e")));

        //ep.dqlBuilder.order(DQLExpressions.property(EducationLevelsHighSchool.printTitle().fromAlias("e")));

    }
}