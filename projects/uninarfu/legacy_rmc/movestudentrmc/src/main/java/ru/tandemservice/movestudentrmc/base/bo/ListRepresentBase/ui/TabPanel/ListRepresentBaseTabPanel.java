package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ui.TabPanel;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ui.List.ListRepresentBaseList;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ui.ListBranch.ListRepresentBaseListBranch;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ui.ListMiddle.ListRepresentBaseListMiddle;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ui.ListMiddleBranch.ListRepresentBaseListMiddleBranch;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ui.ListPostgraduate.ListRepresentBaseListPostgraduate;

@Configuration
public class ListRepresentBaseTabPanel extends BusinessComponentManager {

    //Панель вкладок
    public static final String TAB_PANEL = "tabPanel";

    //Вкладки
    public static final String LIST_REPRESENT_HIGH_TAB = "highTab";
    public static final String LIST_REPRESENT_MIDDLE_TAB = "middleTab";
    public static final String LIST_REPRESENT_POSTGRADUATE_TAB = "postgraduateTab";
    public static final String LIST_REPRESENT_HIGH_BRANCH_TAB = "highBranchTab";
    public static final String LIST_REPRESENT_MIDDLE_BRANCH_TAB = "middleBranchTab";

    @Bean
    public TabPanelExtPoint tabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(TAB_PANEL)
                .addTab(componentTab(LIST_REPRESENT_HIGH_TAB, ListRepresentBaseList.class).permissionKey("rmc_view_list_represent_high_tab"))
                .addTab(componentTab(LIST_REPRESENT_MIDDLE_TAB, ListRepresentBaseListMiddle.class).permissionKey("rmc_view_list_represent_middle_tab"))
                .addTab(componentTab(LIST_REPRESENT_POSTGRADUATE_TAB, ListRepresentBaseListPostgraduate.class).permissionKey("rmc_view_list_represent_postgraduate_tab"))
                .addTab(componentTab(LIST_REPRESENT_HIGH_BRANCH_TAB, ListRepresentBaseListBranch.class).permissionKey("rmc_view_list_represent_high_branch_tab"))
                .addTab(componentTab(LIST_REPRESENT_MIDDLE_BRANCH_TAB, ListRepresentBaseListMiddleBranch.class).permissionKey("rmc_view_list_represent_middle_branch_tab"))
                .create();
    }
}
