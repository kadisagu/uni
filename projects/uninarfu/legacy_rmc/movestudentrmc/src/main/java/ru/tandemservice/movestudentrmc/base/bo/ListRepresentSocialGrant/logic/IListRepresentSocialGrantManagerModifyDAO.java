package ru.tandemservice.movestudentrmc.base.bo.ListRepresentSocialGrant.logic;

import ru.tandemservice.movestudentrmc.docord.IListObjectModifyDAO;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.Date;

public interface IListRepresentSocialGrantManagerModifyDAO extends IListObjectModifyDAO
{

    public EducationYear getByDate(Date date);

    public boolean inOneMonth(Date date1, Date date2);
}
