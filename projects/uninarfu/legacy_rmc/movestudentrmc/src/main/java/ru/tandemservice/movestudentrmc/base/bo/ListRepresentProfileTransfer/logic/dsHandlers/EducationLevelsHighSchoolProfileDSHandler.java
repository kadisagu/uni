package ru.tandemservice.movestudentrmc.base.bo.ListRepresentProfileTransfer.logic.dsHandlers;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;

public class EducationLevelsHighSchoolProfileDSHandler extends DefaultComboDataSourceHandler {

    public EducationLevelsHighSchoolProfileDSHandler(String ownerId) {
        super(ownerId, EducationOrgUnit.class, EducationOrgUnit.educationLevelHighSchool().educationLevel().title());
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep) {

        Group groupField = ep.context.get("groupField");

        if (groupField != null) {

            if (groupField.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isProfile()
                    || groupField.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isSpecialization())
            {
                FilterUtils.applySelectFilter(ep.dqlBuilder, "e", EducationOrgUnit.id(), groupField.getEducationOrgUnit().getId());
            }
            else {
                FilterUtils.applySelectFilter(ep.dqlBuilder, "e", EducationOrgUnit.territorialOrgUnit(), groupField.getEducationOrgUnit().getTerritorialOrgUnit());
                FilterUtils.applySelectFilter(ep.dqlBuilder, "e", EducationOrgUnit.formativeOrgUnit(), groupField.getEducationOrgUnit().getFormativeOrgUnit());
                FilterUtils.applySelectFilter(ep.dqlBuilder, "e", EducationOrgUnit.developTech(), groupField.getEducationOrgUnit().getDevelopTech());
                FilterUtils.applySelectFilter(ep.dqlBuilder, "e", EducationOrgUnit.developPeriod(), groupField.getEducationOrgUnit().getDevelopPeriod());
                FilterUtils.applySelectFilter(ep.dqlBuilder, "e", EducationOrgUnit.developForm(), groupField.getEducationOrgUnit().getDevelopForm());
                FilterUtils.applySelectFilter(ep.dqlBuilder, "e", EducationOrgUnit.developCondition(), groupField.getEducationOrgUnit().getDevelopCondition());

                FilterUtils.applySelectFilter(ep.dqlBuilder, "e", EducationOrgUnit.educationLevelHighSchool().educationLevel().parentLevel(), groupField.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel());
                if (groupField.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isMaster() ||
                        groupField.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isBachelor())
                    FilterUtils.applySelectFilter(ep.dqlBuilder, "e", EducationOrgUnit.educationLevelHighSchool().educationLevel().levelType().profile(), true);
                else if (groupField.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isSpecialty())
                    FilterUtils.applySelectFilter(ep.dqlBuilder, "e", EducationOrgUnit.educationLevelHighSchool().educationLevel().levelType().specialization(), true);
            }
        }


    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep) {
        ep.dqlBuilder.order("e." + EducationOrgUnit.educationLevelHighSchool().title());
    }

}
