package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.List;

public class TerritorialOrgUnitDSHandler extends DefaultComboDataSourceHandler {

    public TerritorialOrgUnitDSHandler(String ownerId) {
        super(ownerId, OrgUnit.class, OrgUnit.P_TERRITORIAL_TITLE);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep) {
        MQBuilder builder = new MQBuilder(Student.ENTITY_CLASS, "s")
                .addJoin("s", Student.educationOrgUnit(), "edu")
                .addJoin("edu", EducationOrgUnit.territorialOrgUnit(), "tou");
        builder.getSelectAliasList().clear();
        builder.addSelect("tou.id");
        builder.setNeedDistinct(true);

        List<Long> list = builder.getResultList(ep.context.getSession());

        ep.dqlBuilder.where(DQLExpressions.in(DQLExpressions.property(OrgUnit.id().fromAlias("e")), list));
    }

}
