package ru.tandemservice.movestudentrmc.dao.check;

import ru.tandemservice.uni.UniDefines;

public class CheckStatusChildRearingLeaveWithVisits extends AbstractCheckStudentStatus {

    @Override
    protected String getStatusCode() {
        return UniDefines.CATALOG_STUDENT_STATUS_CHILD_WITH_ATTENDANCE;
    }

}
