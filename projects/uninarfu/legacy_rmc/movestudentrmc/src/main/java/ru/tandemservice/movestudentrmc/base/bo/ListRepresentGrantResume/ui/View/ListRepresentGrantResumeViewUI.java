package ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantResume.ui.View;

import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.support.AbstractListRepresentViewUI;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantResume.logic.ListPrintDoc;
import ru.tandemservice.movestudentrmc.entity.ListRepresentGrantResume;

import java.util.Arrays;

public class ListRepresentGrantResumeViewUI extends AbstractListRepresentViewUI<ListRepresentGrantResume> {

    @Override
    public ListRepresentGrantResume getListRepresentObject() {
        return new ListRepresentGrantResume();
    }

    @Override
    protected RtfDocument printRepresent() {
        return new ListPrintDoc().creationPrintDocOrder(Arrays.asList(getListRepresent()));
    }
}
