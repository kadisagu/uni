package ru.tandemservice.movestudentrmc.base.bo.ListRepresentQualificationAdmission.ui.Edit;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Activator;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.bo.Person.ui.View.PersonView;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ListRepresentBaseManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.StudentDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.StudentSelectedDSHandlerPractice;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentQualificationAdmission.ListRepresentQualificationAdmissionManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentQualificationThemes.logic.OrgUnitSelectedDSHandler;
import ru.tandemservice.movestudentrmc.dao.CheckStateUtil;
import ru.tandemservice.movestudentrmc.entity.DocListRepresentBasics;
import ru.tandemservice.movestudentrmc.entity.ListRepresentQualificationAdmission;
import ru.tandemservice.movestudentrmc.entity.catalog.*;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentExtractStatesCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.*;


@State({
        @Bind(key = ListRepresentQualificationAdmissionEditUI.LIST_REPRESENT_ID, binding = ListRepresentQualificationAdmissionEditUI.LIST_REPRESENT_ID),
        @Bind(key = "typeRepresentFilter", binding = "typeRepresentFilter"),
        @Bind(key = "reasonRepresentFilter", binding = "reasonRepresentFilter"),
        @Bind(key = "basementRepresentFilter", binding = "basementRepresentFilter"),
        @Bind(key = "representationBasementDate", binding = "representationBasementDate"),
        @Bind(key = "representationBasementNumber", binding = "representationBasementNumber")
})
public class ListRepresentQualificationAdmissionEditUI extends UIPresenter {

    public static final String LIST_REPRESENT_ID = "listRepresentId";

    private Long listRepresentId;
    private RepresentationType typeRepresentFilter;
    private RepresentationReason reasonRepresentFilter;
    private RepresentationBasement basementRepresentFilter;


    private ISelectModel qualificationExamTypeModel;

    private Date _representationBasementDate;     // Дата основания

    private String _representationBasementNumber;     // № основания

    private ListRepresentQualificationAdmission listRepresent;

    private List<Student> studentSelectedList;


    // Логика по "Указать кафедру/ПЦК"
    public final static long CAF_ID = 0L;
    public final static long PCK_ID = 1L;
    public final static String CAF_TITLE = "Утверждено кафедрой";
    public final static String PCK_TITLE = "Утверждено ПЦК";
    private final static String PCK_PRINT_TITLE = "предметно-цикловой комиссии";
    private final static String CAF_PRINT_TITLE = "кафедры";
    public final static String SEPARATOR = ".";
    private static IdentifiableWrapper<IEntity> PCK = new IdentifiableWrapper<IEntity>(PCK_ID, PCK_TITLE);
    private static IdentifiableWrapper<IEntity> CAF = new IdentifiableWrapper<IEntity>(CAF_ID, CAF_TITLE);

    private List<IdentifiableWrapper<IEntity>> sourceFilterList; // Источник Кафедра/ПЦК
    private IdentifiableWrapper<IEntity> sourceFilter;
    private ISelectModel orgUnitSelectModel;
    private OrgUnit orgUnit;
    private boolean orgUnitVisible;

    //    События компонента

    @Override
    public void onComponentPrepareRender() {

        Boolean isSuccessfullyHandOverSession = (Boolean) (_uiSettings.get("isSuccessfullyHandOverSession") == null ? false : _uiSettings.get("isSuccessfullyHandOverSession"));
        if (!isSuccessfullyHandOverSession) {
            _uiSettings.set("yearDistPartFilter", null);
            _uiSettings.set("educationYearFilter", null);
        }
        if (getSettings().get("qualificationExamType") != null)
            listRepresent.setQualificationExamType((QualificationExamType) getSettings().get("qualificationExamType"));

        Boolean isConsiderSessionResults = (Boolean) (_uiSettings.get("isConsiderSessionResults") == null ? false : _uiSettings.get("isConsiderSessionResults"));

        if (!isConsiderSessionResults) {
            _uiSettings.set("sessionMarksResult", null);
        }

        setSourceFilterList(Arrays.asList(
                CAF,
                PCK
        ));

        if (sourceFilter == null) {
            sourceFilter = CAF;
            this.orgUnitVisible = true;
            this.orgUnit = listRepresent.getAcceptOrgUnit();

            String code = listRepresent.getAcceptOrgUnitType();

            if (code != null) {
                String id = StringUtils.substringBefore(code, SEPARATOR);
                try {
                    Long value = Long.valueOf(id);
                    if (value != CAF_ID) {
                        sourceFilter = PCK;
                        this.orgUnitVisible = false;
                    }
                }
                catch (Exception e) {

                }
            }
        }
    }


    @Override
    public void onComponentRefresh()
    {
        clearSettings();

        if (getListRepresentId() != null) {
            listRepresent = DataAccessServices.dao().get(ListRepresentQualificationAdmission.class, ListRepresentQualificationAdmission.id().s(), getListRepresentId());

            _uiSettings.set("qualificationExamType", listRepresent.getQualificationExamType());

            studentSelectedList = ListRepresentQualificationAdmissionManager.instance().getListObjectModifyDAO().selectRepresent(listRepresent);
            setTypeRepresentFilter(listRepresent.getRepresentationType());


        }
        else {

            if (listRepresent != null) {
            }

            if (listRepresent == null)
                listRepresent = new ListRepresentQualificationAdmission();

            listRepresent.setRepresentationType(getTypeRepresentFilter());
            listRepresent.setRepresentationReason(getReasonRepresentFilter());
            listRepresent.setRepresentationBasement(getBasementRepresentFilter());
            listRepresent.setState(DataAccessServices.dao().get(MovestudentExtractStates.class, MovestudentExtractStates.code().s(), "1"));


            if (studentSelectedList == null) {
                studentSelectedList = new ArrayList<Student>();
            }
        }
        setQualificationExamTypeModel(new LazySimpleSelectModel<>(QualificationExamType.class));

    }

    @Override
    public void onAfterDataSourceFetch(IUIDataSource iuidatasource) {
        super.onAfterDataSourceFetch(iuidatasource);
    }


    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        Map<String, Object> settingMap = _uiSettings.getAsMap(
                "educationYearFilter",
                "yearDistPartFilter",
                "territorialOrgUnitFilter",
                StudentDSHandler.FORMATIVE_ORG_UNIT_FILTER,
                "educationLevelsHighSchoolDSFilter",
                "educationOrgUnitFilter",
                "educationLevelFilter",
                StudentDSHandler.QUALIFICATION_FILTER,
                "courseFilter",
                "groupFilter",
                StudentDSHandler.DEVELOP_FORM_FILTER,
                "compensationTypeFilter",
                "benefitFilter",
                StudentDSHandler.STUDENT_FIO_FILTER,
                "isSuccessfullyHandOverSession",
                "sessionMarksResult",
                "isConsiderSessionResults",
                "isforeignCitizens",
                "studentFilter",
                "userClickApply",
                "studentStatusFilter",
                "qualificationExamType"
        );
        dataSource.putAll(settingMap);
        dataSource.put(StudentSelectedDSHandlerPractice.STUDENT_SELECTED_LIST, studentSelectedList);
        dataSource.put(StudentSelectedDSHandlerPractice.LIST_REPRESENT_ID, getListRepresentId());
        dataSource.put(StudentDSHandler.STUDENT_SELECTED_LIST, studentSelectedList);
        dataSource.put(StudentDSHandler.ORDER_ID, getListRepresentId());
    }

    //    События формы
    public boolean getDisableSessionResultFilters() {
        List<OrgUnit> fou = _uiSettings.get("formativeOrgUnitFilter");
        List<OrgUnit> el = _uiSettings.get("educationLevelsHighSchoolDSFilter");
        List<OrgUnit> tou = _uiSettings.get("territorialOrgUnitFilter");
        List<OrgUnit> eou = _uiSettings.get("educationOrgUnitFilter");
        List<Group> group = _uiSettings.get("groupFilter");
        if (
                (
                        fou != null && fou.size() > 0) ||
                        (tou != null && tou.size() > 0) ||
                        (el != null && el.size() > 0) ||
                        (eou != null && eou.size() > 0) ||
                        (group != null && group.size() > 0)
                )
        {
            return false;
        }
        else {
            _uiSettings.set("isSuccessfullyHandOverSession", false);
            _uiSettings.set("isConsiderSessionResults", false);
            return true;
        }

    }

    public void selectRepresent() {


        BaseSearchListDataSource representDS = (BaseSearchListDataSource) getConfig().getDataSource(ListRepresentQualificationAdmissionEdit.STUDENT_DS);

        Collection<IEntity> records = ((CheckboxColumn) representDS.getLegacyDataSource().getColumn(StudentDSHandler.CHECKBOX_COLUMN)).getSelectedObjects();
        List<Student> selectList = new ArrayList<>();
        boolean hasErrors = false;
        for (IEntity record : records) {
            selectList.add((Student) record);
        }
        if (!CollectionUtils.isEmpty(studentSelectedList)) {
            String key = getStringKey((Student) studentSelectedList.get(0));
            for (Student student : selectList) {
                if (!key.equals(getStringKey(student))) {
                    hasErrors = true;
                    ContextLocal.getErrorCollector().add(student.getFio() + ": в представление можно включить только студентов одного направления подготовки (специальности), курса, формы освоения и формирующего подразделения.");

                }
            }

        }
        else {
            String key = getStringKey(selectList.get(0));
            for (Student student : selectList) {
                if (!key.equals(getStringKey(student))) {
                    if (!hasErrors)
                        ContextLocal.getErrorCollector().add("В представление можно включить только студентов одного направления подготовки (специальности), курса, формы освоения и формирующего подразделения.");
                    hasErrors = true;
                }
            }

        }
        ListRepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(getTypeRepresentFilter().getCode()).getListObjectModifyDAO().checkSelectStudent(this.listRepresent, selectList);

        if (hasErrors)
            selectList.clear();
//        }

        for (IEntity record : selectList) {
            if (!studentSelectedList.contains((Student) record)) {
                studentSelectedList.add((Student) record);
            }
        }


    }

    private String getStringKey(Student student)
    {
        return student.getEducationOrgUnit().getEducationLevelHighSchool().getTitle()
                + student.getEducationOrgUnit().getDevelopForm().getTitle()
                + student.getCourse().getTitle()
                + student.getEducationOrgUnit().getFormativeOrgUnit().getTitle();
    }


    public void deleteSelectedRepresent() {

        BaseSearchListDataSource representDS = (BaseSearchListDataSource) getConfig().getDataSource(ListRepresentQualificationAdmissionEdit.STUDENT_SELECTED_DS);

        Collection<IEntity> records = ((CheckboxColumn) representDS.getLegacyDataSource().getColumn(StudentSelectedDSHandlerPractice.CHECKBOX_COLUMN)).getSelectedObjects();

        for (IEntity record : records) {
            studentSelectedList.remove(record);
        }
    }

    public void onClickSave() {

        if (getListRepresentId() != null)
            CheckStateUtil.checkStateRepresent(getListRepresentQualificationAdmission(), Arrays.asList(MovestudentExtractStatesCodes.CODE_1), _uiSupport.getSession());

        if (studentSelectedList.isEmpty())
            getConfig().getUserContext().getErrorCollector().add(getConfig().getProperty("ui.alertEmptySelectedRepresent"));

        listRepresent.setCreateDate(getSupport().getCurrentDate());

        if (getConfig().getUserContext().getErrorCollector().hasErrors())
            return;

        QualificationExamType qualificationExamType = _uiSettings.get("qualificationExamType", QualificationExamType.class);

        listRepresent.setQualificationExamType(qualificationExamType);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocListRepresentBasics.class, "b").addColumn("b");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(DocListRepresentBasics.listRepresent().fromAlias("b")),
                DQLExpressions.value(listRepresent)));

        List<DocListRepresentBasics> representOldBasicsList = builder.createStatement(getSupport().getSession()).list();

        DocListRepresentBasics representBasics = null;

        if (representOldBasicsList.size() > 0) {
            representBasics = representOldBasicsList.get(0);
        }
        else {
            representBasics = new DocListRepresentBasics();
            representBasics.setListRepresent(listRepresent);
            representBasics.setBasic(listRepresent.getRepresentationBasement());
            representBasics.setReason(listRepresent.getRepresentationReason());

            representBasics.setRepresentationBasementDate(_representationBasementDate);
            representBasics.setRepresentationBasementNumber(_representationBasementNumber);
        }

        listRepresent.setAcceptOrgUnit(orgUnit);
        listRepresent.setAcceptOrgUnitType(String.valueOf(sourceFilter.getId()) + SEPARATOR + sourceFilter.getTitle());
        
        //--------------------------------------------------------------------------------------------------------------
        //Оператор, сохранивший представление
        Person person = PersonManager.instance().dao().getPerson(ContextLocal.getUserContext().getPrincipalContext());
        if (person != null)
        	listRepresent.setOperator(person);

        IPrincipalContext context = ContextLocal.getUserContext().getPrincipalContext();
       	listRepresent.setCreator(context);
        //--------------------------------------------------------------------------------------------------------------

        ListRepresentQualificationAdmissionManager.instance().getListObjectModifyDAO().save(listRepresent, studentSelectedList, representBasics);

        deactivate(2);
    }

    //    Вспомогательные функции


    public void onClickView()
    {
        try {
            Student st = DataAccessServices.dao().get(Student.class, (Long) getListenerParameter());
            _uiActivation.asRegionDialog(PersonView.class)
                    .parameter("publisherId", st.getPerson().getId())
                    .activate();
        }
        catch (Exception e) {
            getUserContext().getErrorCollector().add(e.getLocalizedMessage());
        }


    }

    public void onClickEdit() {

        if (studentSelectedList.isEmpty()) {
            ContextLocal.getInfoCollector().add("Список выбранных студентов пуст");
            return;
        }
        else {
            Activator activator = new ComponentActivator(
                    "ru.tandemservice.movestudentrmc.component.represent.QualificationAdmission",
                    new UniMap().add("represent", listRepresent).add("list", studentSelectedList)
            );
            UserContext.getInstance().activateDesktop("PersonShellDialog", activator);
        }
    }


    public void onChangeType()
    {
        setOrgUnitVisible(getSourceFilter().getId() == CAF_ID);
        if (getSourceFilter().getId() == OrgUnitSelectedDSHandler.PCK_ID)
            orgUnit = null;
    }

    @Override
    public void saveSettings() {
        _uiSettings.set("userClickApply", true);
        super.saveSettings();
    }


    public ListRepresentQualificationAdmission getListRepresentQualificationAdmission() {
        return listRepresent;
    }

    public void setListRepresentQualificationAdmission(ListRepresentQualificationAdmission listRepresent) {
        this.listRepresent = listRepresent;
    }

    public RepresentationType getTypeRepresentFilter() {
        return typeRepresentFilter;
    }

    public void setTypeRepresentFilter(RepresentationType typeRepresentFilter) {
        this.typeRepresentFilter = typeRepresentFilter;
    }

    public RepresentationReason getReasonRepresentFilter() {
        return reasonRepresentFilter;
    }

    public void setReasonRepresentFilter(RepresentationReason reasonRepresentFilter) {
        this.reasonRepresentFilter = reasonRepresentFilter;
    }

    public RepresentationBasement getBasementRepresentFilter() {
        return basementRepresentFilter;
    }

    public void setBasementRepresentFilter(RepresentationBasement basementRepresentFilter) {
        this.basementRepresentFilter = basementRepresentFilter;
    }

    public Long getListRepresentId() {
        return listRepresentId;
    }

    public void setListRepresentId(Long listRepresentId) {
        this.listRepresentId = listRepresentId;
    }

    public Date getRepresentationBasementDate()
    {
        return _representationBasementDate;
    }

    public void setRepresentationBasementDate(Date representationBasementDate)
    {
        _representationBasementDate = representationBasementDate;
    }

    public String getRepresentationBasementNumber()
    {
        return _representationBasementNumber;
    }

    public void setRepresentationBasementNumber(String representationBasementNumber)
    {
        _representationBasementNumber = representationBasementNumber;
    }

    public ISelectModel getQualificationExamTypeModel() {
        return qualificationExamTypeModel;
    }

    public void setQualificationExamTypeModel(ISelectModel qualificationExamTypeModel) {
        this.qualificationExamTypeModel = qualificationExamTypeModel;
    }

    public boolean isOrgUnitVisible() {
        return orgUnitVisible;
    }

    public void setOrgUnitVisible(boolean orgUnitVisible) {
        this.orgUnitVisible = orgUnitVisible;
    }

    public OrgUnit getOrgUnit() {
        return orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit) {
        this.orgUnit = orgUnit;
    }

    public ISelectModel getOrgUnitSelectModel() {
        return orgUnitSelectModel;
    }

    public void setOrgUnitSelectModel(ISelectModel orgUnitSelectModel) {
        this.orgUnitSelectModel = orgUnitSelectModel;
    }

    public IdentifiableWrapper<IEntity> getSourceFilter() {
        return sourceFilter;
    }

    public void setSourceFilter(IdentifiableWrapper<IEntity> sourceFilter) {
        this.sourceFilter = sourceFilter;
    }

    public List<IdentifiableWrapper<IEntity>> getSourceFilterList() {
        return sourceFilterList;
    }

    public void setSourceFilterList(List<IdentifiableWrapper<IEntity>> sourceFilterList) {
        this.sourceFilterList = sourceFilterList;
    }
}
