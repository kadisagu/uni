package ru.tandemservice.movestudentrmc.entity;

import ru.tandemservice.movestudentrmc.entity.gen.DocRepresentBasicsGen;

/**
 * Сущность-связь 'Представление'-'Основание'
 */
public class DocRepresentBasics extends DocRepresentBasicsGen {

    public String getTitle() {
        if (this.getDocument() != null && this.getBasic().isConnectedWithDocument())
            return this.getDocument().getTitle();
        else if (this.getBasic().getTitle().contains("_______") && this.getBasic().isNoteRequired())
            return this.getBasic().getTitle().replace("_______",
                                                      this.getRepresentationBasementNumber() == null ?
                                                              "" :
                                                              ("№" + this.getRepresentationBasementNumber().toString())
            );

        return this.getBasic().getTitle();
    }
}