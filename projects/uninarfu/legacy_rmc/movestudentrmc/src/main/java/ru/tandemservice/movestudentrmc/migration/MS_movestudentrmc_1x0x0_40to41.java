package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_movestudentrmc_1x0x0_40to41 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.12"),
                        new ScriptDependency("org.tandemframework.shared", "1.2.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.2.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность representationType

        if (tool.tableExists("representationtype_t")) {
            // создать колонку
            tool.createColumn("representationtype_t", new DBColumn("checkmove_p", DBType.BOOLEAN));
            // задать значение по умолчанию
            java.lang.Boolean defaultCheckMove = false;
            tool.executeUpdate("update representationtype_t set checkmove_p=? where checkmove_p is null", defaultCheckMove);

            // сделать колонку NOT NULL
            tool.setColumnNullable("representationtype_t", "checkmove_p", false);

            // создать колонку
            tool.createColumn("representationtype_t", new DBColumn("checkgrant_p", DBType.BOOLEAN));
            // задать значение по умолчанию
            java.lang.Boolean defaultCheckGrant = false;
            tool.executeUpdate("update representationtype_t set checkgrant_p=? where checkgrant_p is null", defaultCheckGrant);

            // сделать колонку NOT NULL
            tool.setColumnNullable("representationtype_t", "checkgrant_p", false);

        }
    }

}