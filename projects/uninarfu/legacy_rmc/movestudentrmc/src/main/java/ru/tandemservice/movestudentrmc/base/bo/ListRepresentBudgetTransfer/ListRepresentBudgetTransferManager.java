package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBudgetTransfer;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBudgetTransfer.logic.ListRepresentBudgetTransferManagerModifyDAO;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBudgetTransfer.ui.Edit.ListRepresentBudgetTransferEdit;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBudgetTransfer.ui.View.ListRepresentBudgetTransferView;
import ru.tandemservice.movestudentrmc.docord.IListObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IListObjectModifyDAO;


@Configuration
public class ListRepresentBudgetTransferManager extends BusinessObjectManager implements IListObjectByRepresentTypeManager {

    public static ListRepresentBudgetTransferManager instance()
    {
        return instance(ListRepresentBudgetTransferManager.class);
    }


    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return ListRepresentBudgetTransferEdit.class;
    }


    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return ListRepresentBudgetTransferView.class;
    }


    @Bean
    @Override
    public IListObjectModifyDAO getListObjectModifyDAO() {
        return new ListRepresentBudgetTransferManagerModifyDAO();
    }
}
