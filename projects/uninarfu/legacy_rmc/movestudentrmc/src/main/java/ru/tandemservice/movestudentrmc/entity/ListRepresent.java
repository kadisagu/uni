package ru.tandemservice.movestudentrmc.entity;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.movestudentrmc.entity.gen.ListRepresentGen;
import ru.tandemservice.uni.dao.IUniBaseDao;

import java.util.List;


/**
 * Списочное представление
 */
public class ListRepresent extends ListRepresentGen
{
    public static final String P_STUDENT_COUNT = "studentCount";
    public static final String P_ORDER = "order";

    @Override
    public String getTitle() {
        String shortTitle = getOrgUnitShortTitle();
        return getRepresentationType().getTitle() +
                (getOperator() != null ? ", " + getOperator().getFullFio() : "")
                + (shortTitle.isEmpty() ? "" : ", " + shortTitle)
                + ", от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getCreateDate());
    }

    /**
     * Название типа представления без ФИО сотрудника и даты создания
     *
     * @return
     */
    public String getRepresentationTitle() {
        String shortTitle = getOrgUnitShortTitle();
        return getRepresentationType().getTitle() +
                (shortTitle.isEmpty() ? "" : ", " + shortTitle);
    }

    protected String getOrgUnitShortTitle() {
        if (this.getCreator() != null && this.getCreator() instanceof EmployeePost) {
            return ((EmployeePost) this.getCreator()).getOrgUnit().getShortTitle();
        }
        else
            return "";
    }

    public Integer getStudentCount() {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(RelListRepresentStudents.class, "rel")
                .where(DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("rel")), DQLExpressions.value(this)));
        return IUniBaseDao.instance.get().getCount(builder);
    }

    public ListOrder getOrder() {

        List<ListOrdListRepresent> list = DataAccessServices.dao().getList(ListOrdListRepresent.class, ListOrdListRepresent.representation(), this);

        if (!list.isEmpty())
            return list.get(0).getOrder();

        return null;
    }
}