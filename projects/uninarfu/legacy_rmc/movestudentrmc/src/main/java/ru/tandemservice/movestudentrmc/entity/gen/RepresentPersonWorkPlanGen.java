package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudentrmc.entity.RepresentPersonWorkPlan;
import ru.tandemservice.movestudentrmc.entity.Representation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * О предоставлении ИУП
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RepresentPersonWorkPlanGen extends Representation
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.RepresentPersonWorkPlan";
    public static final String ENTITY_NAME = "representPersonWorkPlan";
    public static final int VERSION_HASH = -124222147;
    private static IEntityMeta ENTITY_META;

    public static final String P_BEGIN_PERSON_WORK_PLAN_DATE = "beginPersonWorkPlanDate";
    public static final String P_END_PERSON_WORK_PLAN_DATE = "endPersonWorkPlanDate";

    private Date _beginPersonWorkPlanDate;     // Дата начала предоставлении ИУП
    private Date _endPersonWorkPlanDate;     // Дата окончания предоставлении ИУП

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата начала предоставлении ИУП. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginPersonWorkPlanDate()
    {
        return _beginPersonWorkPlanDate;
    }

    /**
     * @param beginPersonWorkPlanDate Дата начала предоставлении ИУП. Свойство не может быть null.
     */
    public void setBeginPersonWorkPlanDate(Date beginPersonWorkPlanDate)
    {
        dirty(_beginPersonWorkPlanDate, beginPersonWorkPlanDate);
        _beginPersonWorkPlanDate = beginPersonWorkPlanDate;
    }

    /**
     * @return Дата окончания предоставлении ИУП. Свойство не может быть null.
     */
    @NotNull
    public Date getEndPersonWorkPlanDate()
    {
        return _endPersonWorkPlanDate;
    }

    /**
     * @param endPersonWorkPlanDate Дата окончания предоставлении ИУП. Свойство не может быть null.
     */
    public void setEndPersonWorkPlanDate(Date endPersonWorkPlanDate)
    {
        dirty(_endPersonWorkPlanDate, endPersonWorkPlanDate);
        _endPersonWorkPlanDate = endPersonWorkPlanDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof RepresentPersonWorkPlanGen)
        {
            setBeginPersonWorkPlanDate(((RepresentPersonWorkPlan)another).getBeginPersonWorkPlanDate());
            setEndPersonWorkPlanDate(((RepresentPersonWorkPlan)another).getEndPersonWorkPlanDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RepresentPersonWorkPlanGen> extends Representation.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RepresentPersonWorkPlan.class;
        }

        public T newInstance()
        {
            return (T) new RepresentPersonWorkPlan();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "beginPersonWorkPlanDate":
                    return obj.getBeginPersonWorkPlanDate();
                case "endPersonWorkPlanDate":
                    return obj.getEndPersonWorkPlanDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "beginPersonWorkPlanDate":
                    obj.setBeginPersonWorkPlanDate((Date) value);
                    return;
                case "endPersonWorkPlanDate":
                    obj.setEndPersonWorkPlanDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "beginPersonWorkPlanDate":
                        return true;
                case "endPersonWorkPlanDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "beginPersonWorkPlanDate":
                    return true;
                case "endPersonWorkPlanDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "beginPersonWorkPlanDate":
                    return Date.class;
                case "endPersonWorkPlanDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RepresentPersonWorkPlan> _dslPath = new Path<RepresentPersonWorkPlan>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RepresentPersonWorkPlan");
    }
            

    /**
     * @return Дата начала предоставлении ИУП. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentPersonWorkPlan#getBeginPersonWorkPlanDate()
     */
    public static PropertyPath<Date> beginPersonWorkPlanDate()
    {
        return _dslPath.beginPersonWorkPlanDate();
    }

    /**
     * @return Дата окончания предоставлении ИУП. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentPersonWorkPlan#getEndPersonWorkPlanDate()
     */
    public static PropertyPath<Date> endPersonWorkPlanDate()
    {
        return _dslPath.endPersonWorkPlanDate();
    }

    public static class Path<E extends RepresentPersonWorkPlan> extends Representation.Path<E>
    {
        private PropertyPath<Date> _beginPersonWorkPlanDate;
        private PropertyPath<Date> _endPersonWorkPlanDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата начала предоставлении ИУП. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentPersonWorkPlan#getBeginPersonWorkPlanDate()
     */
        public PropertyPath<Date> beginPersonWorkPlanDate()
        {
            if(_beginPersonWorkPlanDate == null )
                _beginPersonWorkPlanDate = new PropertyPath<Date>(RepresentPersonWorkPlanGen.P_BEGIN_PERSON_WORK_PLAN_DATE, this);
            return _beginPersonWorkPlanDate;
        }

    /**
     * @return Дата окончания предоставлении ИУП. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentPersonWorkPlan#getEndPersonWorkPlanDate()
     */
        public PropertyPath<Date> endPersonWorkPlanDate()
        {
            if(_endPersonWorkPlanDate == null )
                _endPersonWorkPlanDate = new PropertyPath<Date>(RepresentPersonWorkPlanGen.P_END_PERSON_WORK_PLAN_DATE, this);
            return _endPersonWorkPlanDate;
        }

        public Class getEntityClass()
        {
            return RepresentPersonWorkPlan.class;
        }

        public String getEntityName()
        {
            return "representPersonWorkPlan";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
