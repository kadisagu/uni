package ru.tandemservice.movestudentrmc.component.catalog.representationReason.RepresentationReasonPub;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubModel;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;

public class Model extends DefaultCatalogPubModel<RepresentationReason> {
    private RepresentationReason representationReason;
    private RepresentationReason _representationReason = new RepresentationReason();

    public static class RepresentationReasonWrapper extends IdentifiableWrapper<RepresentationReason> {
        private static final long serialVersionUID = -4301478784488247007L;
        public static final String PRINTABLE = "printable";
        private RepresentationReason representationReason;
        private boolean printable;

        public RepresentationReasonWrapper(RepresentationReason representationReason, boolean printable)
        {
            super(representationReason.getId(), representationReason.getTitle());
            this.representationReason = representationReason;
            this.printable = printable;
        }


        public RepresentationReason getRepresentationReason()
        {
            return representationReason;
        }

        public boolean isPrintable()
        {
            return printable;
        }

        public void setPrintable(boolean printable)
        {
            this.printable = printable;
        }
    }

    public RepresentationReason getRepresentationReason()
    {
        return _representationReason;
    }

    public void setRepresentationReason(RepresentationReason representationReason)
    {
        _representationReason = representationReason;
    }
}
