package ru.tandemservice.movestudentrmc.base.bo.ListRepresentExclude;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentExclude.logic.ListRepresentExcludeManagerModifyDAO;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentExclude.ui.Edit.ListRepresentExcludeEdit;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentExclude.ui.View.ListRepresentExcludeView;
import ru.tandemservice.movestudentrmc.docord.IListObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IListObjectModifyDAO;

@Configuration
public class ListRepresentExcludeManager extends BusinessObjectManager implements IListObjectByRepresentTypeManager {

    public static ListRepresentExcludeManager instance() {
        return instance(ListRepresentExcludeManager.class);
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return ListRepresentExcludeEdit.class;
    }

    @Override
    @Bean
    public IListObjectModifyDAO getListObjectModifyDAO() {
        return new ListRepresentExcludeManagerModifyDAO();
    }

    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return ListRepresentExcludeView.class;
    }

}
