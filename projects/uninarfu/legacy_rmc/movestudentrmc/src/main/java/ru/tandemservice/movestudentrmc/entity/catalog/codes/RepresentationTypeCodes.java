package ru.tandemservice.movestudentrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Типы представлений"
 * Имя сущности : representationType
 * Файл data.xml : movementstudent.data.xml
 */
public interface RepresentationTypeCodes
{
    /** Константа кода (code) элемента : exclude (code). Название (title) : Об отчислении */
    String EXCLUDE = "exclude";
    /** Константа кода (code) элемента : weekend (code). Название (title) : О предоставлении академического отпуска */
    String WEEKEND = "weekend";
    /** Константа кода (code) элемента : transfer (code). Название (title) : О переводе */
    String TRANSFER = "transfer";
    /** Константа кода (code) элемента : excludeOut (code). Название (title) : О восстановлении */
    String EXCLUDE_OUT = "excludeOut";
    /** Константа кода (code) элемента : weekendOut (code). Название (title) : О выходе из ао */
    String WEEKEND_OUT = "weekendOut";
    /** Константа кода (code) элемента : weekendProlong (code). Название (title) : О продлении ао */
    String WEEKEND_PROLONG = "weekendProlong";
    /** Константа кода (code) элемента : recertificationTraining (code). Название (title) : О перезачете/переаттестации учебных дисциплин */
    String RECERTIFICATION_TRAINING = "recertificationTraining";
    /** Константа кода (code) элемента : courseTransfer (code). Название (title) : О переводе на следующий курс */
    String COURSE_TRANSFER = "courseTransfer";
    /** Константа кода (code) элемента : diplomAndExclude (code). Название (title) : О выдаче диплома и отчислении */
    String DIPLOM_AND_EXCLUDE = "diplomAndExclude";
    /** Константа кода (code) элемента : changeName (code). Название (title) : О смене имени */
    String CHANGE_NAME = "changeName";
    /** Константа кода (code) элемента : extensionSession (code). Название (title) : О продлении сессии */
    String EXTENSION_SESSION = "extensionSession";
    /** Константа кода (code) элемента : personWorkPlan (code). Название (title) : О предоставлении ИУП */
    String PERSON_WORK_PLAN = "personWorkPlan";
    /** Константа кода (code) элемента : admissionAttendClasses (code). Название (title) : О допуске к посещению занятий */
    String ADMISSION_ATTEND_CLASSES = "admissionAttendClasses";
    /** Константа кода (code) элемента : appointGrant (code). Название (title) : О назначении стипендии/выплаты */
    String APPOINT_GRANT = "appointGrant";
    /** Константа кода (code) элемента : distributionAccordingToProfiles (code). Название (title) : О распределении обучаемых по профилям и группам */
    String DISTRIBUTION_ACCORDING_TO_PROFILES = "distributionAccordingToProfiles";
    /** Константа кода (code) элемента : distributionToBudget (code). Название (title) : О переводе на места финансируемые из средств федерального бюджета */
    String DISTRIBUTION_TO_BUDGET = "distributionToBudget";
    /** Константа кода (code) элемента : enrollmentTransfer (code). Название (title) : О зачислении в порядке перевода */
    String ENROLLMENT_TRANSFER = "enrollmentTransfer";
    /** Константа кода (code) элемента : socialSupport (code). Название (title) : О назначении социальной  поддержки в связи с тяжелым материальным положением */
    String SOCIAL_SUPPORT = "socialSupport";
    /** Константа кода (code) элемента : courseTransferList (code). Название (title) : О переводе на следующий курс */
    String COURSE_TRANSFER_LIST = "courseTransferList";
    /** Константа кода (code) элемента : diplomAndExcludeList (code). Название (title) : О выдаче диплома и отчислении */
    String DIPLOM_AND_EXCLUDE_LIST = "diplomAndExcludeList";
    /** Константа кода (code) элемента : grantCancel (code). Название (title) : Об отмене стипендии/выплаты */
    String GRANT_CANCEL = "grantCancel";
    /** Константа кода (code) элемента : grantSuspend (code). Название (title) : О приостановлении стипендии/выплаты */
    String GRANT_SUSPEND = "grantSuspend";
    /** Константа кода (code) элемента : grantResume (code). Название (title) : О возобновлении стипендии/выплаты */
    String GRANT_RESUME = "grantResume";
    /** Константа кода (code) элемента : grantCancelIndividual (code). Название (title) : Об отмене стипендии/выплаты */
    String GRANT_CANCEL_INDIVIDUAL = "grantCancelIndividual";
    /** Константа кода (code) элемента : grantIndividual (code). Название (title) : О назначении стипендии/выплаты */
    String GRANT_INDIVIDUAL = "grantIndividual";
    /** Константа кода (code) элемента : socialSupportIndividual (code). Название (title) : О назначении социальной поддержки в связи с тяжелым материальным положением */
    String SOCIAL_SUPPORT_INDIVIDUAL = "socialSupportIndividual";
    /** Константа кода (code) элемента : grantSuspendIndividual (code). Название (title) : О приостановлении стипендии/выплаты */
    String GRANT_SUSPEND_INDIVIDUAL = "grantSuspendIndividual";
    /** Константа кода (code) элемента : grantResumeIndividual (code). Название (title) : О возобновлении стипендии/выплаты */
    String GRANT_RESUME_INDIVIDUAL = "grantResumeIndividual";
    /** Константа кода (code) элемента : excludeList (code). Название (title) : Об отчислении */
    String EXCLUDE_LIST = "excludeList";
    /** Константа кода (code) элемента : transferList (code). Название (title) : О переводе */
    String TRANSFER_LIST = "transferList";
    /** Константа кода (code) элемента : duplicateStudentTicketOrExamsBook (code). Название (title) : О выдаче дубликата зачетной книжки/студенческого билета */
    String DUPLICATE_STUDENT_TICKET_OR_EXAMS_BOOK = "duplicateStudentTicketOrExamsBook";
    /** Константа кода (code) элемента : makeReprimand (code). Название (title) : Об объявлении выговора */
    String MAKE_REPRIMAND = "makeReprimand";
    /** Константа кода (code) элемента : weekendList (code). Название (title) : О предоставлении каникул */
    String WEEKEND_LIST = "weekendList";
    /** Константа кода (code) элемента : admissionToPractice (code). Название (title) : О допуске к практике */
    String ADMISSION_TO_PRACTICE = "admissionToPractice";
    /** Константа кода (code) элемента : practiceList (code). Название (title) : О направлении на практику */
    String PRACTICE_LIST = "practiceList";
    /** Константа кода (code) элемента : travelIndividual (code). Название (title) : О направлении в поездку */
    String TRAVEL_INDIVIDUAL = "travelIndividual";
    /** Константа кода (code) элемента : travelList (code). Название (title) : О направлении в поездку */
    String TRAVEL_LIST = "travelList";
    /** Константа кода (code) элемента : socialGrantList (code). Название (title) : О назначении государственной социальной стипендии */
    String SOCIAL_GRANT_LIST = "socialGrantList";
    /** Константа кода (code) элемента : grantCancelAndDestinationIndividual (code). Название (title) : Об отмене и назначении стипендии/выплаты */
    String GRANT_CANCEL_AND_DESTINATION_INDIVIDUAL = "grantCancelAndDestinationIndividual";
    /** Константа кода (code) элемента : grantCancelAndDestinationList (code). Название (title) : Об отмене и назначении стипендии/выплаты */
    String GRANT_CANCEL_AND_DESTINATION_LIST = "grantCancelAndDestinationList";
    /** Константа кода (code) элемента : qualificationThemesList (code). Название (title) : Об утверждении тем ВКР */
    String QUALIFICATION_THEMES_LIST = "qualificationThemesList";
    /** Константа кода (code) элемента : qualificationAdmissionList (code). Название (title) : О допуске к мероприятиям итоговой аттестации */
    String QUALIFICATION_ADMISSION_LIST = "qualificationAdmissionList";
    /** Константа кода (code) элемента : changeQualificationTheme (code). Название (title) : О внесении изменений в приказ об утверждении тем ВКР */
    String CHANGE_QUALIFICATION_THEME = "changeQualificationTheme";

    Set<String> CODES = ImmutableSet.of(EXCLUDE, WEEKEND, TRANSFER, EXCLUDE_OUT, WEEKEND_OUT, WEEKEND_PROLONG, RECERTIFICATION_TRAINING, COURSE_TRANSFER, DIPLOM_AND_EXCLUDE, CHANGE_NAME, EXTENSION_SESSION, PERSON_WORK_PLAN, ADMISSION_ATTEND_CLASSES, APPOINT_GRANT, DISTRIBUTION_ACCORDING_TO_PROFILES, DISTRIBUTION_TO_BUDGET, ENROLLMENT_TRANSFER, SOCIAL_SUPPORT, COURSE_TRANSFER_LIST, DIPLOM_AND_EXCLUDE_LIST, GRANT_CANCEL, GRANT_SUSPEND, GRANT_RESUME, GRANT_CANCEL_INDIVIDUAL, GRANT_INDIVIDUAL, SOCIAL_SUPPORT_INDIVIDUAL, GRANT_SUSPEND_INDIVIDUAL, GRANT_RESUME_INDIVIDUAL, EXCLUDE_LIST, TRANSFER_LIST, DUPLICATE_STUDENT_TICKET_OR_EXAMS_BOOK, MAKE_REPRIMAND, WEEKEND_LIST, ADMISSION_TO_PRACTICE, PRACTICE_LIST, TRAVEL_INDIVIDUAL, TRAVEL_LIST, SOCIAL_GRANT_LIST, GRANT_CANCEL_AND_DESTINATION_INDIVIDUAL, GRANT_CANCEL_AND_DESTINATION_LIST, QUALIFICATION_THEMES_LIST, QUALIFICATION_ADMISSION_LIST, CHANGE_QUALIFICATION_THEME);
}
