/* $Id$ */
package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ui.MassInnerAdvisorEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentAdmissionToPractice.ui.Edit.ListRepresentAdmissionToPracticeEditUI;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.StudentSelectedDSHandlerPractice;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentPractice.ui.Edit.ListRepresentPracticeEdit;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentPractice.ui.Edit.ListRepresentPracticeEditUI;

import java.util.Collection;

/**
 * @author Andrey Andreev
 * @since 23.10.2015
 */
public class OnClickMassEditInnerAdvisor extends NamedUIAction
{
    public OnClickMassEditInnerAdvisor()
    {
        super("onClickMassEditInnerAdvisor");
    }

    @Override
    public void execute(IUIPresenter iuiPresenter)
    {
        if (!(iuiPresenter instanceof ListRepresentPracticeEditUI ||
                iuiPresenter instanceof ListRepresentAdmissionToPracticeEditUI)) return;

        BaseSearchListDataSource representDS = iuiPresenter.getConfig().getDataSource(ListRepresentPracticeEdit.STUDENT_SELECTED_DS);

        Collection<IEntity> records = ((CheckboxColumn) representDS.getLegacyDataSource()
                .getColumn(StudentSelectedDSHandlerPractice.CHECKBOX_COLUMN)).getSelectedObjects();

        if (records.isEmpty()) {
            ContextLocal.getInfoCollector().add("Нет выбранных студентов");
        } else {
            iuiPresenter.getActivationBuilder()
                    .asRegionDialog(ListRepresentBaseMassInnerAdvisorEdit.class)
                    .parameter(ListRepresentBaseMassInnerAdvisorEditUI.RECORDS, records)
                    .activate();
        }
    }
}