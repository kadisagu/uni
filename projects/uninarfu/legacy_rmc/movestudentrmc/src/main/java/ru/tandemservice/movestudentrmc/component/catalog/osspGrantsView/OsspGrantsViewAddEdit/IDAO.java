package ru.tandemservice.movestudentrmc.component.catalog.osspGrantsView.OsspGrantsViewAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.movestudentrmc.entity.catalog.OsspGrantsView;

public interface IDAO extends IDefaultCatalogAddEditDAO<OsspGrantsView, Model> {

}
