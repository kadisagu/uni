package ru.tandemservice.movestudentrmc.base.bo.ListRepresentSocialGrant.ui.Edit;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.component.Activator;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.ui.View.PersonView;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.StudentDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.StudentSelectedDSHandlerSupport;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentSocialGrant.ListRepresentSocialGrantManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentSocialGrant.logic.IListRepresentSocialGrantManagerModifyDAO;
import ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab.MonthWrapper;
import ru.tandemservice.movestudentrmc.dao.CheckStateUtil;
import ru.tandemservice.movestudentrmc.entity.DocListRepresentBasics;
import ru.tandemservice.movestudentrmc.entity.GrantEntity;
import ru.tandemservice.movestudentrmc.entity.ListRepresentSocialGrant;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntity;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentExtractStatesCodes;
import ru.tandemservice.movestudentrmc.entity.catalog.*;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.*;


@State({
        @Bind(key = ListRepresentSocialGrantEditUI.LIST_REPRESENT_ID, binding = ListRepresentSocialGrantEditUI.LIST_REPRESENT_ID),
        @Bind(key = "typeRepresentFilter", binding = "typeRepresentFilter"),
        @Bind(key = "reasonRepresentFilter", binding = "reasonRepresentFilter"),
        @Bind(key = "basementRepresentFilter", binding = "basementRepresentFilter"),
        @Bind(key = "representationBasementDate", binding = "representationBasementDate"),
        @Bind(key = "representationBasementNumber", binding = "representationBasementNumber"),
})
public class ListRepresentSocialGrantEditUI extends UIPresenter {

    public static final String LIST_REPRESENT_ID = "listRepresentId";

    private Long listRepresentId;
    private RepresentationType typeRepresentFilter;
    private RepresentationReason reasonRepresentFilter;
    private RepresentationBasement basementRepresentFilter;

    private Date _representationBasementDate;     // Дата основания
    private String _representationBasementNumber;     // № основания


    private ListRepresentSocialGrant listRepresent;
    private List<DataWrapper> studentSelectedList;
    private List<Student> stuSelectedList;


    //    События компонента

    @Override
    public void onComponentPrepareRender() {
        if (getSettings().get("grantViewField") != null)
            listRepresent.setGrantView((GrantView) getSettings().get("grantViewField"));
        if (getSettings().get("dateBeginingPaymentField") != null)
            listRepresent.setDateBeginingPayment((Date) getSettings().get("dateBeginingPaymentField"));
        if (getSettings().get("dateEndOfPaymentField") != null)
            listRepresent.setDateEndOfPayment((Date) getSettings().get("dateEndOfPaymentField"));

		/*
		Boolean isSuccessfullyHandOverSession = (Boolean) (_uiSettings.get("isSuccessfullyHandOverSession") == null ? false : _uiSettings.get("isSuccessfullyHandOverSession"));
		if(!isSuccessfullyHandOverSession){
			_uiSettings.set("yearDistPartFilter", null);
			_uiSettings.set("educationYearFilter", null);			
		}
		*/
        Boolean isConsiderSessionResults = (Boolean) (_uiSettings.get("isConsiderSessionResults") == null ? false : _uiSettings.get("isConsiderSessionResults"));

        if (!isConsiderSessionResults) {
            _uiSettings.set("sessionMarksResult", null);
        }
        //super.onComponentPrepareRender();
    }


    @Override
    public void onComponentRefresh()
    {
        clearSettings();

        if (getListRepresentId() != null) {
            listRepresent = DataAccessServices.dao().get(ListRepresentSocialGrant.class, ListRepresentSocialGrant.id().s(), getListRepresentId());
            _uiSettings.set("grantViewField", listRepresent.getGrantView());
            _uiSettings.set("dateBeginingPaymentField", listRepresent.getDateBeginingPayment());
            _uiSettings.set("dateEndOfPaymentField", listRepresent.getDateEndOfPayment());

            stuSelectedList = ListRepresentSocialGrantManager.instance().getListObjectModifyDAO().selectRepresent(listRepresent);

            if (studentSelectedList == null) {
                studentSelectedList = new ArrayList<DataWrapper>();
                for (Student student : stuSelectedList) {
                    DataWrapper dw = new DataWrapper(student.getId(), student.getTitle(), student);
                    dw.setProperty("student", student);
                    studentSelectedList.add(dw);
                }
            }

        }
        else {

            if (listRepresent != null) {
                _uiSettings.set("grantViewField", listRepresent.getGrantView());
                _uiSettings.set("dateBeginingPaymentField", listRepresent.getDateBeginingPayment());
                _uiSettings.set("dateEndOfPaymentField", listRepresent.getDateEndOfPayment());
            }

            if (listRepresent == null)
                listRepresent = new ListRepresentSocialGrant();

            listRepresent.setRepresentationType(getTypeRepresentFilter());
            listRepresent.setRepresentationReason(getReasonRepresentFilter());
            listRepresent.setRepresentationBasement(getBasementRepresentFilter());
            listRepresent.setState(DataAccessServices.dao().get(MovestudentExtractStates.class, MovestudentExtractStates.code().s(), "1"));


            if (studentSelectedList == null) {
                studentSelectedList = new ArrayList<DataWrapper>();
            }

            if (stuSelectedList == null)
                stuSelectedList = new ArrayList<Student>();
        }

    }

    @Override
    public void onAfterDataSourceFetch(IUIDataSource iuidatasource) {
        super.onAfterDataSourceFetch(iuidatasource);
    }


    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        Map<String, Object> settingMap = _uiSettings.getAsMap(
                //"educationYearFilter",
                "yearDistPartFilter",
                "territorialOrgUnitFilter",
                StudentDSHandler.FORMATIVE_ORG_UNIT_FILTER,
                "educationLevelsHighSchoolDSFilter",
                "educationOrgUnitFilter",
                "educationLevelFilter",
                StudentDSHandler.QUALIFICATION_FILTER,
                "courseFilter",
                "groupFilter",
                StudentDSHandler.DEVELOP_FORM_FILTER,
                "compensationTypeFilter",
                "benefitFilter",
                StudentDSHandler.STUDENT_FIO_FILTER,
                "grantViewField",
                "educationYearField",
                "dateBeginingPaymentField",
                "dateEndOfPaymentField",
                "popupGrant",
                "isSuccessfullyHandOverSession",
                "sessionMarksResult",
                "isConsiderSessionResults",
                "isforeignCitizens",
                "studentFilter",
                "userClickApply",
                "studentStatusFilter"
        );
        dataSource.putAll(settingMap);
        dataSource.put(StudentSelectedDSHandlerSupport.STUDENT_SELECTED_LIST, studentSelectedList);
        dataSource.put(StudentSelectedDSHandlerSupport.LIST_REPRESENT_ID, getListRepresentId());
        dataSource.put(StudentDSHandler.STUDENT_SELECTED_LIST, stuSelectedList);
        dataSource.put(StudentDSHandler.ORDER_ID, getListRepresentId());


        Date dateBeginingPaymentField = _uiSettings.get("dateEndOfPaymentField", Date.class);
        EducationYear year = getDao().getByDate(dateBeginingPaymentField);
        dataSource.put("educationYearField", year);
    }


    //    События формы

    public void selectRepresent() {

        BaseSearchListDataSource representDS = (BaseSearchListDataSource) getConfig().getDataSource(ListRepresentSocialGrantEdit.STUDENT_DS);

        Collection<IEntity> records = ((CheckboxColumn) representDS.getLegacyDataSource().getColumn(StudentDSHandler.CHECKBOX_COLUMN)).getSelectedObjects();

        for (IEntity record : records) {
            if (!stuSelectedList.contains((Student) record)) {
                stuSelectedList.add((Student) record);
                Student student = (Student) record;
                DataWrapper dw = new DataWrapper(student.getId(), student.getTitle(), student);
                dw.setProperty("student", student);
                studentSelectedList.add(dw);
            }
        }

    }

    public void deleteSelectedRepresent() {

        BaseSearchListDataSource representDS = (BaseSearchListDataSource) getConfig().getDataSource(ListRepresentSocialGrantEdit.STUDENT_SELECTED_DS);

        Collection<IEntity> records = ((CheckboxColumn) representDS.getLegacyDataSource().getColumn(StudentSelectedDSHandlerSupport.CHECKBOX_COLUMN)).getSelectedObjects();
        for (IEntity record : records) {
            studentSelectedList.remove((DataWrapper) record);
            stuSelectedList.remove(((DataWrapper) record).getProperty("student"));
        }
    }

    public void onClickSave() {

        if (getListRepresentId() != null)
            CheckStateUtil.checkStateRepresent(getListRepresentGrant(), Arrays.asList(MovestudentExtractStatesCodes.CODE_1), _uiSupport.getSession());

        if (studentSelectedList.isEmpty())
            getConfig().getUserContext().getErrorCollector().add(getConfig().getProperty("ui.alertEmptySelectedRepresent"));


        Date dateBeginingPaymentField = _uiSettings.get("dateBeginingPaymentField", Date.class);
        Date dateEndOfPaymentField = _uiSettings.get("dateEndOfPaymentField", Date.class);
        GrantView grantView = _uiSettings.get("grantViewField", GrantView.class);

        listRepresent.setCreateDate(getSupport().getCurrentDate());
        listRepresent.setDateBeginingPayment(dateBeginingPaymentField);
        listRepresent.setDateEndOfPayment(dateEndOfPaymentField);
        if (getConfig().getUserContext().getErrorCollector().hasErrors())
            return;

        List<DocListRepresentBasics> representOldBasicsList = IUniBaseDao.instance.get().getList(DocListRepresentBasics.class, DocListRepresentBasics.listRepresent().id(), listRepresent.getId());
        DocListRepresentBasics representBasics = null;
        if (representOldBasicsList.size() > 0) {
            representBasics = representOldBasicsList.get(0);
        }
        else {
            representBasics = new DocListRepresentBasics();
            representBasics.setListRepresent(listRepresent);
            representBasics.setBasic(listRepresent.getRepresentationBasement());
            representBasics.setReason(listRepresent.getRepresentationReason());
            representBasics.setRepresentationBasementDate(_representationBasementDate);
            representBasics.setRepresentationBasementNumber(_representationBasementNumber);
        }

        ListRepresentSocialGrantManager.instance().getListObjectModifyDAO().saveSupport(listRepresent, studentSelectedList, representBasics);
        deactivate(2);
    }

    //    Вспомогательные функции


    //    Getters and Setters

    public void onClickView()
    {
        try {
            Student st = DataAccessServices.dao().get(Student.class, (Long) getListenerParameter());
            _uiActivation.asRegionDialog(PersonView.class)
                    .parameter("publisherId", st.getPerson().getId())
                    .activate();
        }
        catch (Exception e) {
            getUserContext().getErrorCollector().add(e.getLocalizedMessage());
        }


    }

    public boolean getDisableSessionResultFilters() {
        List<OrgUnit> fou = _uiSettings.get("formativeOrgUnitFilter");
        List<OrgUnit> el = _uiSettings.get("educationLevelsHighSchoolDSFilter");
        List<OrgUnit> tou = _uiSettings.get("territorialOrgUnitFilter");
        List<OrgUnit> eou = _uiSettings.get("educationOrgUnitFilter");
        List<Group> group = _uiSettings.get("groupFilter");
        if (
                (fou != null && fou.size() > 0) ||
                        (tou != null && tou.size() > 0) ||
                        (el != null && el.size() > 0) ||
                        (eou != null && eou.size() > 0) ||
                        (group != null && group.size() > 0)
                )
        {
            return false;
        }
        else {
            _uiSettings.set("isSuccessfullyHandOverSession", false);
            _uiSettings.set("isConsiderSessionResults", false);
            return true;
        }

    }

    public void onClickAddGrant() {
        Date dateBeginingPaymentField = _uiSettings.get("dateBeginingPaymentField", Date.class);
        Date dateEndOfPaymentField = _uiSettings.get("dateEndOfPaymentField", Date.class);
        GrantView grantView = _uiSettings.get("grantViewField", GrantView.class);

        if (dateBeginingPaymentField == null || dateEndOfPaymentField == null || grantView == null) {
            ContextLocal.getErrorCollector().add("Поля \"Вид стипендии/выплаты\", и \"Даты\" обязательны для заполнения.");
            return;
        }
        if (!getDao().inOneMonth(dateBeginingPaymentField, dateEndOfPaymentField)) {
            ContextLocal.getErrorCollector().add("Поля \"Дата начала выплаты\", и \"Дата окончания выплаты\" должны быть в одном месяце");
            return;
        }

        if (studentSelectedList.isEmpty()) {
            ContextLocal.getInfoCollector().add("Список выбранных студентов пуст");
            return;
        }
        else {
            Activator activator = new ComponentActivator(
                    "ru.tandemservice.movestudentrmc.component.represent.StudentsGrants",
                    new UniMap().add("represent", true).add("list", studentSelectedList)
            );
            UserContext.getInstance().activateDesktop("PersonShellDialog", activator);
        }
    }

    public void onClickCalculateGrant() {
        Date dateBeginingPaymentField = _uiSettings.get("dateBeginingPaymentField", Date.class);
        Date dateEndOfPaymentField = _uiSettings.get("dateEndOfPaymentField", Date.class);
        GrantView grantView = _uiSettings.get("grantViewField", GrantView.class);

        if (dateBeginingPaymentField == null || dateEndOfPaymentField == null || grantView == null) {
            ContextLocal.getErrorCollector().add("Поля \"Вид стипендии/выплаты\", и \"Даты\" обязательны для заполнения.");
            return;
        }
        if (!getDao().inOneMonth(dateBeginingPaymentField, dateEndOfPaymentField)) {
            ContextLocal.getErrorCollector().add("Поля \"Дата начала выплаты\", и \"Дата окончания выплаты\" должны быть в одном месяце");
            return;
        }
        if (studentSelectedList.isEmpty()) {
            ContextLocal.getInfoCollector().add("Список выбранных студентов пуст");
            return;
        }

        double k = 1.0;
        Calendar c = Calendar.getInstance();
        c.setTime(dateEndOfPaymentField);
        if (CoreDateUtils.getDayOfMonth(dateBeginingPaymentField) != 1 || CoreDateUtils.getDayOfMonth(dateEndOfPaymentField) != c.getActualMaximum(Calendar.DAY_OF_MONTH)) {
            int days = CoreDateUtils.getDateDifferenceInDays(dateBeginingPaymentField, dateEndOfPaymentField);
            days = Math.abs(days) + 1;
            double daysOfMonth = c.getActualMaximum(Calendar.DAY_OF_MONTH);
            k = days / daysOfMonth;
        }

        EducationYear year = getDao().getByDate(dateBeginingPaymentField);
        String month = MonthWrapper.getMonthsList(dateBeginingPaymentField, dateBeginingPaymentField, year).get(0);

        for (DataWrapper wrapper : studentSelectedList) {
            Student student = (Student) wrapper.getProperty("student");

            GrantEntity ge = new DQLSelectBuilder().fromEntity(GrantEntity.class, "e")
                    .where(DQLExpressions.eqValue(DQLExpressions.property(GrantEntity.orgUnit().fromAlias("e")), student.getEducationOrgUnit().getFormativeOrgUnit()))
                    .where(DQLExpressions.eqValue(DQLExpressions.property(GrantEntity.month().fromAlias("e")), month))
                    .where(DQLExpressions.eqValue(DQLExpressions.property(GrantEntity.view().fromAlias("e")), grantView))
                    .createStatement(this._uiSupport.getSession())
                    .uniqueResult();

            if (ge == null)
                continue;

            StudentGrantEntity sge = (StudentGrantEntity) wrapper.getProperty("grant");
            double sum = ge.getSum() * k;
            sum = Math.round(sum);
            sge.setSum(sum);
        }
    }

    private IListRepresentSocialGrantManagerModifyDAO getDao() {
        return (IListRepresentSocialGrantManagerModifyDAO) ListRepresentSocialGrantManager.instance().getListObjectModifyDAO();
    }

    @Override
    public void saveSettings() {
        _uiSettings.set("userClickApply", true);
        super.saveSettings();
    }

    public ListRepresentSocialGrant getListRepresentGrant() {
        return listRepresent;
    }

    public void setListRepresentGrant(ListRepresentSocialGrant listRepresent) {
        this.listRepresent = listRepresent;
    }

    public void onViewRepresentationFromList() {
    }


    public RepresentationType getTypeRepresentFilter() {
        return typeRepresentFilter;
    }

    public void setTypeRepresentFilter(RepresentationType typeRepresentFilter) {
        this.typeRepresentFilter = typeRepresentFilter;
    }

    public RepresentationReason getReasonRepresentFilter() {
        return reasonRepresentFilter;
    }

    public void setReasonRepresentFilter(RepresentationReason reasonRepresentFilter) {
        this.reasonRepresentFilter = reasonRepresentFilter;
    }

    public RepresentationBasement getBasementRepresentFilter() {
        return basementRepresentFilter;
    }

    public void setBasementRepresentFilter(RepresentationBasement basementRepresentFilter) {
        this.basementRepresentFilter = basementRepresentFilter;
    }

    public Long getListRepresentId() {
        return listRepresentId;
    }

    public void setListRepresentId(Long listRepresentId) {
        this.listRepresentId = listRepresentId;
    }

    public Date getRepresentationBasementDate()
    {
        return _representationBasementDate;
    }

    public void setRepresentationBasementDate(Date representationBasementDate)
    {
        _representationBasementDate = representationBasementDate;
    }

    public String getRepresentationBasementNumber()
    {
        return _representationBasementNumber;
    }

    public void setRepresentationBasementNumber(String representationBasementNumber)
    {
        _representationBasementNumber = representationBasementNumber;
    }


}
