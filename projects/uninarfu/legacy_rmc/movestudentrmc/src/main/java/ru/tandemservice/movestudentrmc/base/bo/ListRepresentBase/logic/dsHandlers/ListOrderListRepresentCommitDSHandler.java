package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.entity.ListOrdListRepresent;
import ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents;

public class ListOrderListRepresentCommitDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput> {


    public static final String STATE_COLUMN = "state";
    public static final String DATE_BEGINING_PAYMENT = "createDate";
    public static final String REPRESENT_TITLE_COLUMN = "listRepresentTitle";


    public ListOrderListRepresentCommitDSHandler(String ownerId) {
        super(ownerId);
    }


    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context) {

        Object studentId = context.get("studentId");

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(ListOrdListRepresent.class, "o")
                .joinEntity("o", DQLJoinType.left, RelListRepresentStudents.class, "srep", DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("srep")), DQLExpressions.property(ListOrdListRepresent.representation().fromAlias("o"))))
                .addColumn("o")
                .where(DQLExpressions.eqValue(DQLExpressions.property(RelListRepresentStudents.student().id().fromAlias("srep")), studentId))

                        //Представления не включенные в приказ
                .where(DQLExpressions.eqValue(DQLExpressions.property(ListOrdListRepresent.order().state().code().fromAlias("o")), "5"));

        return DQLSelectOutputBuilder.get(input, builder, getSession()).order(ListRepresentDSHandler.class, "o").build();

    }


}
