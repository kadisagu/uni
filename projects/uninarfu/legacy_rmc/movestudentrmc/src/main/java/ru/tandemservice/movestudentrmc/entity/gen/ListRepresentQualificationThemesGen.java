package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListRepresentQualificationThemes;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Списочное представление о назначении тем ВКР
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ListRepresentQualificationThemesGen extends ListRepresent
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.ListRepresentQualificationThemes";
    public static final String ENTITY_NAME = "listRepresentQualificationThemes";
    public static final int VERSION_HASH = -1056413084;
    private static IEntityMeta ENTITY_META;

    public static final String P_ACCEPT_ORG_UNIT_TYPE = "acceptOrgUnitType";
    public static final String L_ACCEPT_ORG_UNIT = "acceptOrgUnit";
    public static final String P_TYPE_PRINT_TITLE = "typePrintTitle";

    private String _acceptOrgUnitType;     // Тип утверждающего подразделения
    private OrgUnit _acceptOrgUnit;     // Кафедра

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип утверждающего подразделения.
     */
    @Length(max=255)
    public String getAcceptOrgUnitType()
    {
        return _acceptOrgUnitType;
    }

    /**
     * @param acceptOrgUnitType Тип утверждающего подразделения.
     */
    public void setAcceptOrgUnitType(String acceptOrgUnitType)
    {
        dirty(_acceptOrgUnitType, acceptOrgUnitType);
        _acceptOrgUnitType = acceptOrgUnitType;
    }

    /**
     * @return Кафедра.
     */
    public OrgUnit getAcceptOrgUnit()
    {
        return _acceptOrgUnit;
    }

    /**
     * @param acceptOrgUnit Кафедра.
     */
    public void setAcceptOrgUnit(OrgUnit acceptOrgUnit)
    {
        dirty(_acceptOrgUnit, acceptOrgUnit);
        _acceptOrgUnit = acceptOrgUnit;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ListRepresentQualificationThemesGen)
        {
            setAcceptOrgUnitType(((ListRepresentQualificationThemes)another).getAcceptOrgUnitType());
            setAcceptOrgUnit(((ListRepresentQualificationThemes)another).getAcceptOrgUnit());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ListRepresentQualificationThemesGen> extends ListRepresent.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ListRepresentQualificationThemes.class;
        }

        public T newInstance()
        {
            return (T) new ListRepresentQualificationThemes();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "acceptOrgUnitType":
                    return obj.getAcceptOrgUnitType();
                case "acceptOrgUnit":
                    return obj.getAcceptOrgUnit();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "acceptOrgUnitType":
                    obj.setAcceptOrgUnitType((String) value);
                    return;
                case "acceptOrgUnit":
                    obj.setAcceptOrgUnit((OrgUnit) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "acceptOrgUnitType":
                        return true;
                case "acceptOrgUnit":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "acceptOrgUnitType":
                    return true;
                case "acceptOrgUnit":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "acceptOrgUnitType":
                    return String.class;
                case "acceptOrgUnit":
                    return OrgUnit.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ListRepresentQualificationThemes> _dslPath = new Path<ListRepresentQualificationThemes>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ListRepresentQualificationThemes");
    }
            

    /**
     * @return Тип утверждающего подразделения.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentQualificationThemes#getAcceptOrgUnitType()
     */
    public static PropertyPath<String> acceptOrgUnitType()
    {
        return _dslPath.acceptOrgUnitType();
    }

    /**
     * @return Кафедра.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentQualificationThemes#getAcceptOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> acceptOrgUnit()
    {
        return _dslPath.acceptOrgUnit();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentQualificationThemes#getTypePrintTitle()
     */
    public static SupportedPropertyPath<String> typePrintTitle()
    {
        return _dslPath.typePrintTitle();
    }

    public static class Path<E extends ListRepresentQualificationThemes> extends ListRepresent.Path<E>
    {
        private PropertyPath<String> _acceptOrgUnitType;
        private OrgUnit.Path<OrgUnit> _acceptOrgUnit;
        private SupportedPropertyPath<String> _typePrintTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип утверждающего подразделения.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentQualificationThemes#getAcceptOrgUnitType()
     */
        public PropertyPath<String> acceptOrgUnitType()
        {
            if(_acceptOrgUnitType == null )
                _acceptOrgUnitType = new PropertyPath<String>(ListRepresentQualificationThemesGen.P_ACCEPT_ORG_UNIT_TYPE, this);
            return _acceptOrgUnitType;
        }

    /**
     * @return Кафедра.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentQualificationThemes#getAcceptOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> acceptOrgUnit()
        {
            if(_acceptOrgUnit == null )
                _acceptOrgUnit = new OrgUnit.Path<OrgUnit>(L_ACCEPT_ORG_UNIT, this);
            return _acceptOrgUnit;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentQualificationThemes#getTypePrintTitle()
     */
        public SupportedPropertyPath<String> typePrintTitle()
        {
            if(_typePrintTitle == null )
                _typePrintTitle = new SupportedPropertyPath<String>(ListRepresentQualificationThemesGen.P_TYPE_PRINT_TITLE, this);
            return _typePrintTitle;
        }

        public Class getEntityClass()
        {
            return ListRepresentQualificationThemes.class;
        }

        public String getEntityName()
        {
            return "listRepresentQualificationThemes";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTypePrintTitle();
}
