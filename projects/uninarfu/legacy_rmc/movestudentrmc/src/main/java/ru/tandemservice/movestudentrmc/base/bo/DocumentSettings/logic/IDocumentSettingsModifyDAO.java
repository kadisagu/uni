package ru.tandemservice.movestudentrmc.base.bo.DocumentSettings.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.movestudentrmc.entity.catalog.Document;
import ru.tandemservice.movestudentrmc.entity.catalog.DocumentKind;
import ru.tandemservice.movestudentrmc.entity.catalog.DocumentType;

import java.util.List;

public interface IDocumentSettingsModifyDAO extends INeedPersistenceSupport {

    public List<DocumentKind> getKinds(DocumentType documentType);

    public List<Document> getDocuments(DocumentType documentType, DocumentKind documentKind);

    public void saveRelations(DocumentType documentType, DocumentKind documentKind, List<Document> documents);
}
 