package ru.tandemservice.movestudentrmc.base.bo.ListRepresentQualificationThemes.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.AbstractListPrintDoc;
import ru.tandemservice.movestudentrmc.docord.IListPrintDoc;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate;
import ru.tandemservice.movestudentrmc.entity.DocListRepresentBasics;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListRepresentQualificationThemes;
import ru.tandemservice.movestudentrmc.entity.StudentQualificationThemes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.*;
//---------------------------------------------------------------------------
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import org.tandemframework.rtf.util.RtfString;
import ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents;
import ru.tandemservice.uni.entity.employee.Student;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
//---------------------------------------------------------------------------

public class ListPrintDoc extends AbstractListPrintDoc<ListRepresentQualificationThemes> implements IListPrintDoc
{

    private Map<Student, OrderParagraphInfo> hashMap = new HashMap<>();

    public final String TEMPLATE_HEADER = "list.rep.qualification.themes.header";
    public final String TEMPLATE_PARAG = "list.rep.qualification.themes.parag";

    public RtfDocument creationPrintDocOrder(List<? extends ListRepresent> listOfRepresents)
    {
    	//----------------------------------------------------------------------------------------------------------------
        //PrintTemplate printTemplate = UniDaoFacade.getCoreDao().getCatalogItem(PrintTemplate.class, getTemplateHeader());
    	TemplateDocument printTemplate = UniDaoFacade.getCoreDao().getCatalogItem(TemplateDocument.class, getTemplateHeader());
        //----------------------------------------------------------------------------------------------------------------
        RtfDocument templateHeader = new RtfReader().read(printTemplate.getContent());

        //------------------------------------------------------------------------------------------------------
        //printTemplate = UniDaoFacade.getCoreDao().getCatalogItem(PrintTemplate.class, getTemplateParag());
        printTemplate = UniDaoFacade.getCoreDao().getCatalogItem(TemplateDocument.class, getTemplateParag());
        //------------------------------------------------------------------------------------------------------
        RtfDocument templateParag = new RtfReader().read(printTemplate.getContent());

        Map<OrgUnit, Map<String, List<Object[]>>> dataMap = getStudentData(listOfRepresents);
        //сортировка формирующих по алфавиту
        List<OrgUnit> orgUnitList = new ArrayList<>(dataMap.keySet());
        Collections.sort(orgUnitList, new EntityComparator<>(new EntityOrder(OrgUnit.title())));

        RtfDocument result = templateHeader.getClone();
        result.setSettings(templateParag.getSettings());
        IRtfElement paragTemp = UniRtfUtil.findElement(result.getElementList(), "parag");
        new RtfInjectModifier().put("representTitle", listOfRepresents.get(0).getRepresentationType().getTitle()).modify(result);

        RtfDocument res = RtfBean.getElementFactory().createRtfDocument();
        res.setHeader(templateParag.getHeader());
        res.setSettings(templateParag.getSettings());
        //-------------------------------------------------------------------------------------
        //res.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.TAB));
        //-------------------------------------------------------------------------------------

        int formCounter = 0;
        int addonCounter = 0;

        for (OrgUnit formOU : orgUnitList)
        {
            Map<String, List<Object[]>> formMap = dataMap.get(formOU);

            formCounter++;
            res.getElementList().add(RtfBean.getElementFactory().createRtfText(String.valueOf(formCounter).trim() + "."));
            res.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.TAB));
            insertRow(res, "По " + formOU.getDativeCaseTitle());

            int counter = 0;
            for (List<Object[]> studentList : formMap.values())
            {
                counter++;
                addonCounter++;
                RtfDocument par = getParagraph(templateParag, formCounter, counter, addonCounter, studentList);
                res.getElementList().addAll(par.getElementList());
                
                //Приложение печатать не нужно
                //addonList.add(getAddon(templateAddon, addonCounter, studentList, order));
            }
        }

        //основания
        //------------------------------------------------------------------------------------------
        //res.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
        //insertRow(res, "Основание: " + UtilPrintSupport.getBasementTitles(listOfRepresents));
        //------------------------------------------------------------------------------------------
        result.getElementList().addAll(result.getElementList().indexOf(paragTemp), res.getElementList());

        //result.getElementList().add( RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE) );
        List<RtfDocument> addonList = new ArrayList<>();
        for (int i = 0; i < addonList.size(); i++)
        {
            result.getElementList().addAll(addonList.get(i).getElementList());
            if (i + 1 < addonList.size())
                result.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
        }
        SharedRtfUtil.removeParagraphsWithTagsRecursive(result, Collections.singletonList("parag"), false, false);
        return result;
    }

    @Override
    protected String getTemplateHeader()
    {
        return TEMPLATE_HEADER;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    protected String getTemplateParag()
    {
        return TEMPLATE_PARAG;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    protected String getTemplateAddon()
    {
        return null;
    }

    @Override
    protected void paragraphInjectModifier(RtfInjectModifier im, ListRepresentQualificationThemes listRepresent, Student student)
    {
        injectModifier(im, student);
//        im.put("highSchool", UtilPrintSupport.getHighLevelSchoolTypeString(student.getEducationOrgUnit().getEducationLevelHighSchool()));
//        im.put("reason", listRepresent.getRepresentationReason().getTitle());
//        im.put("practiceTitle", listRepresent.getPracticeTitle());
//        im.put("practicePaymentData", listRepresent.getPaymentData() ==null ?"":listRepresent.getPaymentData().getTitle());
    }

    @Override
    protected RtfDocument getParagraph(RtfDocument template, int formCounter, int counter, int addonCounter, List<Object[]> studentList)
    {
        RtfDocument result = template.getClone();
        ListRepresentQualificationThemes listRepresent = (ListRepresentQualificationThemes) studentList.get(0)[1];

        Set<Long> listRepresentIds = new HashSet<>();
        //----------------------------------------------------------------------------
        List<ListRepresentQualificationThemes> studentRepresentList = new ArrayList();
        //----------------------------------------------------------------------------

        Student student = (Student) studentList.get(0)[0];
        for (Object[] o : studentList)
        {
            Student s = (Student) o[0];
            listRepresentIds.add(((ListRepresentQualificationThemes) o[1]).getId());
            //----------------------------------------------------------------------------
            if(!studentRepresentList.contains((ListRepresentQualificationThemes)o[1])){
            	studentRepresentList.add((ListRepresentQualificationThemes)o[1]);
            }
            //----------------------------------------------------------------------------
            hashMap.put(s, new OrderParagraphInfo(formCounter, counter));
        }
        RtfInjectModifier im = new RtfInjectModifier();

        im.put("i", String.valueOf(formCounter).trim());
        im.put("j", String.valueOf(counter).trim());
        im.put("addonNumber", String.valueOf(addonCounter));

        paragraphInjectModifier(im, listRepresent, student);
        //-----------------------------------------------------
        //Вывод оснований для каждого пункта приказа
        im.put("basic", "Основание: " + UtilPrintSupport.getBasementTitles(studentRepresentList) + ".");
        //-----------------------------------------------------
        im.modify(result);
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(StudentQualificationThemes.class, "spd");
        dql.where(DQLExpressions.in(
                DQLExpressions.property(StudentQualificationThemes.listRepresent().id().fromAlias("spd")),
                listRepresentIds
        ));
        List<StudentQualificationThemes> qualificationThemesList = UniDaoFacade.getCoreDao().getList(dql);
        Map<Student, StudentQualificationThemes> dataMap = new HashMap<>();
        for (StudentQualificationThemes data : qualificationThemesList)
        {
            dataMap.put(data.getStudent(), data);
        }
        List<String[]> tableList = new ArrayList<>();
        int rowCounter = 1;
        for (Object[] arr : studentList)
        {
            Student stu = (Student) arr[0];
            StudentQualificationThemes qualificationThemes = dataMap.get(stu);
            String[] row = new String[4];
            //-----------------------------------------------------------------
            hashMap.get(stu).setStudentNumber(rowCounter);
            //-----------------------------------------------------------------
            row[0] = "" + rowCounter++;
            row[1] = stu.getPerson().getFullFio();
            if (qualificationThemes != null)
            {
                row[2] = StringUtils.trimToEmpty(qualificationThemes.getTheme());
                row[3] = StringUtils.trimToEmpty(qualificationThemes.getAdvisor());

            }
            tableList.add(row);
        }

        RtfTableModifier tm = new RtfTableModifier();
        tm.put("T", tableList.toArray(new String[][]{}));
        tm.modify(result);
        return result;
    }

    public static void injectModifier(RtfInjectModifier im, Student student)
    {
        String workType = "выпускных квалификационных работ";
        if (student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isMaster())
            workType = "магистерских диссертаций";

        String levelType = "специальность ";
        if (student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isMaster() ||
                student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isBachelor())
            levelType = "направление подготовки ";
        String code = UtilPrintSupport.getEduLevelCodeWithQualification(student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel());
        String levelTitle = UtilPrintSupport.getParentTitleIfProfile(student.getEducationOrgUnit().getEducationLevelHighSchool());


        EducationYear educationYear = UniDaoFacade.getCoreDao().get(EducationYear.class, EducationYear.current().s(), true);

        im.put("workType", workType);
        im.put("eduYear", educationYear == null ? "" : educationYear.getTitle().replace("/", "-"));
        //----------------------------------------------------------------------------------------------------------------------
        //im.put("eduLevelTitle", levelType + code + levelTitle);
        im.put("eduLevelTitle", new RtfString().append(levelType).append(code.trim()).append(IRtfData.SYMBOL_TILDE).append(levelTitle));
        //----------------------------------------------------------------------------------------------------------------------
        im.put("course", student.getCourse().getTitle());
        im.put("developForm", UtilPrintSupport.getDevelopFormGen(student.getEducationOrgUnit().getDevelopForm()));
    }

    @Override
    protected String getKey(Student student, ListRepresentQualificationThemes represent)
    {
        return String.valueOf(student.getEducationOrgUnit().getDevelopForm().getId()) +
                student.getCourse().getId() +
                student.getEducationOrgUnit().getEducationLevelHighSchool().getId();
    }

    @Override
    protected Class<? extends ListRepresent> getListRepresentClass()
    {
        return ListRepresentQualificationThemes.class;
    }

    @Override
    public Map<Student, OrderParagraphInfo> getParagInfomap()
    {
        return hashMap;
    }

}
