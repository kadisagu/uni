package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ListRepresentBaseManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.ListRepresentDSHandler;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.entity.employee.Student;


@Configuration
public class ListRepresentBaseList extends BusinessComponentManager
{

    public static final String LIST_REPRESENT_DS = "listRepresentDS";
    public static final String FORMATIVE_ORG_UNIT_DS = "formativeOrgUnitDS";
    public static final String TYPE_REPRESENT_DS = "typeRepresentHighDS";
    public static final String STATE_VIEW_DS = "stateViewDS";
    public static final String STUDENT_COMBO_DS = "studentComboDS";
    public static final String GRANT_VIEW_DS = "grantViewDS";

    @Bean
    public ColumnListExtPoint listRepresentDS()
    {
        return columnListExtPointBuilder(LIST_REPRESENT_DS)
                .addColumn(publisherColumn(ListRepresentDSHandler.REPRESENT_TITLE_COLUMN, ListRepresent.title()).permissionKey("rmc_view_list_represent_Button").order().create())
                .addColumn(textColumn(ListRepresentDSHandler.STATE_COLUMN, ListRepresent.state().title()).width("250px").create())
                .addColumn(publisherColumn(ListRepresentDSHandler.ORDER_TITLE_COLUMN, "order.fullTitle").publisherLinkResolver(
                        new IPublisherLinkResolver()
                        {

                            @Override
                            public Object getParameters(IEntity entity)
                            {
                                return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, ((ListRepresent) entity).getOrder().getId());
                            }

                            @Override
                            public String getComponentName(IEntity entity)
                            {
                                return null;
                            }
                        })
                        .permissionKey("rmc_view_list_represent_Button").create())
                .addColumn(textColumn(ListRepresentDSHandler.COUNT_STUDENT_COLUMN, ListRepresent.P_STUDENT_COUNT).create())
                .addColumn(booleanColumn(ListRepresentDSHandler.CHECK, ListRepresent.check()).create())
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("rmc_edit_list_represent_Button").disabled("ui:isFormative").create())
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER, alert("listRepresentDS.delete.alert")).permissionKey("rmc_delete_list_represent_Button").disabled("ui:isFormative").create())
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(LIST_REPRESENT_DS, listRepresentDS()).handler(ListRepresentBaseManager.instance().listRepresentDSHandler()))
                .addDataSource(selectDS(STATE_VIEW_DS, ListRepresentBaseManager.instance().stateViewDSHandler()))
                .addDataSource(selectDS(FORMATIVE_ORG_UNIT_DS, ListRepresentBaseManager.instance().formativeOrgUnitDSHandler()))
                .addDataSource(UniStudentManger.instance().courseDSConfig())
                .addDataSource(selectDS(TYPE_REPRESENT_DS, ListRepresentBaseManager.instance().typeRepresentDSHandler()))
                .addDataSource(selectDS(STUDENT_COMBO_DS, ListRepresentBaseManager.instance().studentComboDSHandler()).addColumn(Student.person().identityCard().fullFio().s()))
                .addDataSource(selectDS(GRANT_VIEW_DS, ListRepresentBaseManager.instance().grantViewDSHandler()).addColumn(GrantView.shortTitle().s()))
                .create();
    }


}
