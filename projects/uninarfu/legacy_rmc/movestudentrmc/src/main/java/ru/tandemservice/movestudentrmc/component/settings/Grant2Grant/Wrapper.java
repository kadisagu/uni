package ru.tandemservice.movestudentrmc.component.settings.Grant2Grant;

import ru.tandemservice.movestudentrmc.entity.Grant2Grant;
import ru.tandemservice.movestudentrmc.entity.catalog.Grant;

public class Wrapper {
    private Grant first;
    private Grant second;

    private boolean checked;
    private Grant2Grant relation;

    public Grant getFirst() {
        return first;
    }

    public void setFirst(Grant first) {
        this.first = first;
    }

    public Grant getSecond() {
        return second;
    }

    public void setSecond(Grant second) {
        this.second = second;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public Grant2Grant getRelation() {
        return relation;
    }

    public void setRelation(Grant2Grant relation) {
        this.relation = relation;
    }

    public boolean isDisabled() {
        return first.equals(second);
    }
}
