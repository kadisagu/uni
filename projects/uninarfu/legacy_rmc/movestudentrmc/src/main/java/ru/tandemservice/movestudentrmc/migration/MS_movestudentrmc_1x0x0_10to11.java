package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;

public class MS_movestudentrmc_1x0x0_10to11 extends IndependentMigrationScript {
    @Override
    public void run(DBTool tool) throws Exception {

		/* Миграция для обновления кодов статусов представлений
         *
		 */

        if (tool.tableExists("MOVESTUDENTEXTRACTSTATES_T")) {
            tool.executeUpdate("update MOVESTUDENTEXTRACTSTATES_T set TITLE_P='В приказах' where CODE_P='5'");
        }


    }
}
