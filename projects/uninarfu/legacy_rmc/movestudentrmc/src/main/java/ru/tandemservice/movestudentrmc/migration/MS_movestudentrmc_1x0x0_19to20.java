package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;

public class MS_movestudentrmc_1x0x0_19to20 extends IndependentMigrationScript {
    @Override
    public void run(DBTool tool) throws Exception {

		/* Миграция для удаления ненужных шаблонов 
         * RM #2136
		 */

        if (tool.tableExists("printtemplate_t"))
            tool.executeUpdate("delete from printtemplate_t where code_p in ('representCommonExtractHeader', 'orderCommonHeader2', 'orderCommonExtractHeader')");

    }
}
