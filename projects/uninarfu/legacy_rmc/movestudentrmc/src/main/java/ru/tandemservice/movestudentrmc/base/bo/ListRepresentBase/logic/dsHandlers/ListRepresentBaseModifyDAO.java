package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers;

import org.tandemframework.caf.logic.dao.BaseModifyAggregateDAO;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents;

import java.util.List;

public class ListRepresentBaseModifyDAO extends BaseModifyAggregateDAO<ListRepresent> implements IListRepresentBaseModifyDAO {

    @Override
    public void update(ListRepresent listRepresent,
                       List<RelListRepresentStudents> studentList)
    {


    }

    @Override
    public void updateState(ListRepresent listRepresent) {


    }

    @Override
    public void delete(ListRepresent listRepresent) {


    }


}
