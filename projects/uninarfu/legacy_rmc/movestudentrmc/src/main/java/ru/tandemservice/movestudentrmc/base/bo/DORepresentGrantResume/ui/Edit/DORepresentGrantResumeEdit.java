package ru.tandemservice.movestudentrmc.base.bo.DORepresentGrantResume.ui.Edit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.block.BlockListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.GrantViewDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.ReasonDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.EditBasic.DORepresentBaseEditBasic;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentExclude.ui.Edit.DORepresentExcludeEditUI;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

@Configuration
public class DORepresentGrantResumeEdit extends BusinessComponentManager {

    public static final String REASON_DS = "reasonDS";
    public static final String EDUCATION_YEAR_DS = "educationYearDS";
    public static final String GRANT_VIEW_DS = "grantViewDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(REASON_DS, reasonDSHandler()))
                .addDataSource(selectDS(EDUCATION_YEAR_DS, educationYearDSHandler()))
                .addDataSource(selectDS(GRANT_VIEW_DS, grantViewDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler reasonDSHandler()
    {
        return new ReasonDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler educationYearDSHandler()
    {
        return EducationYear.defaultSelectDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler grantViewDSHandler()
    {
        return new GrantViewDSHandler(getName());
    }

    @Bean
    public BlockListExtPoint editBasicBlockListExtPoint()
    {
        return blockListExtPointBuilder(DORepresentExcludeEditUI.EDIT_BASIC_BLOCK_LIST)
                .addBlock(componentBlock(DORepresentExcludeEditUI.EDIT_BASIC_BLOCK, DORepresentBaseEditBasic.class).parameters("ui:blockParam"))
                .create();
    }
}
