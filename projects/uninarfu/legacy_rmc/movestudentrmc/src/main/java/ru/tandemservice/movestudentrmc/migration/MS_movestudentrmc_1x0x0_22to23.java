package ru.tandemservice.movestudentrmc.migration;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.ReferenceDBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.hibsupport.EntityIDGenerator;
import ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab.MonthWrapper;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntity;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentationTypeCodes;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.io.FileWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

public class MS_movestudentrmc_1x0x0_22to23 extends IndependentMigrationScript {
    private DBTool tool;
    private IEntityMeta meta;
    private Long sgeStatus = null;

    @Override
    public void run(DBTool tool) throws Exception {
        this.tool = tool;
        this.meta = EntityRuntime.getMeta(StudentGrantEntity.class);
        if (!tool.tableExists(meta.getTableName()))
            return;

        processStructure();

        //если таблица пуста то нет смысла ничего чинить
        String sql = "select count(*) from " + meta.getTableName();
        ResultSet rs = tool.getStatement().executeQuery(sql);
        if (rs.next()) {
            Integer counter = (Integer) rs.getObject(1);
            if (counter == null || counter < 1)
                return;
        }
        else
            return;

        rs.close();

        //грузим статус Выплата
        sql = "select id from STUGRANTSTATUS_T where CODE_P=1";
        rs = tool.getStatement().executeQuery(sql);
        if (rs.next())
            this.sgeStatus = rs.getLong(1);
        else
            return;

        Map<String, Object> sgeData = new HashMap<String, Object>();
        int count = loadStudentGrants(sgeData, false); //not tmp
        p("total sge: " + count);

        //представления на стипендию(проведенные)
        List<OrderGrantData> orderData = new ArrayList<OrderGrantData>();
        count = loadOrderGrant_data(orderData);
        p("total grant order: " + count);
        processOrder_data(orderData, sgeData, false);

        //представления на соцвыплату(проведенные)
        orderData = new ArrayList<OrderGrantData>();
        count = loadOrderSocial_data(orderData);
        p("total social order: " + count);
        processOrder_data(orderData, sgeData, false);

        //представления на соцвыплату(в работе)
        orderData = new ArrayList<OrderGrantData>();
        count = loadOrderSocial_dataTmp(orderData);
        p("total social order (inwork): " + count);
        sgeData = new HashMap<String, Object>();
        count = loadStudentGrants(sgeData, true); //tmp
        p("total sge (tmp): " + count);
        processOrder_data(orderData, sgeData, true);

        //представления на стипендию(в работе)
        orderData = new ArrayList<OrderGrantData>();
        count = loadOrderGrant_dataTmp(orderData);
        p("total grant order (inwork): " + count);
        sgeData = new HashMap<String, Object>();
        processOrder_data(orderData, sgeData, true);

        count = processEmpty();
        p("delete empty: " + count);


        p("\n\n");
        //log.flush();
        //log.close();
        //throw new ApplicationException("drop it"); //TODO: drop it
    }

    private void processStructure() throws Exception {
        if (tool.table(this.meta.getTableName()).columnExists("TMP_P")) {
            tool.getConnection().createStatement().executeUpdate("update " + this.meta.getTableName() + " set TMP_P=0 where TMP_P is null");
            tool.table(this.meta.getTableName()).column("TMP_P").changeNullable(false);
        }

        IEntityMeta meta2 = EntityRuntime.getMeta(StudentGrantEntity.class);

        if (!tool.tableExists(meta2.getTableName()))
            return;

        if (tool.table(meta2.getTableName()).columnExists("LISTREPRESENT_ID"))
            return;

        ReferenceDBColumn col = new ReferenceDBColumn("LISTREPRESENT_ID", false, "fk_listrepresent_7a544ee4", this.meta.getTableName(), "ID");
        tool.table(meta2.getTableName()).createColumn(col);
    }

    private Set<Long> loadModularSGE() throws Exception {
        Set<Long> result = new HashSet<Long>();

        String sql = "select rep.GRANTSOLD_P from REPRESENTATION_T rep where rep.GRANTSOLD_P is not null";
        ResultSet rs = this.tool.getConnection().createStatement().executeQuery(sql);
        while (rs.next()) {
            String str = rs.getString(1);
            if (StringUtils.isEmpty(str))
                continue;

            String[] arr = str.split(",");
            for (String s : arr)
                result.add(Long.parseLong(s));
        }
        rs.close();

        return result;
    }

    private int processOrder_data(List<OrderGrantData> orderData, Map<String, Object> sgeData, boolean tmp) throws Exception {
        PreparedStatement pst = this.tool.getConnection().prepareStatement("update " + meta.getTableName() + " set LISTREPRESENT_ID=?, ORDERDATE_P=? where ID=?");
        PreparedStatement pst2 = this.tool.getConnection().prepareStatement("insert into " + meta.getTableName() + "(ID, DISCRIMINATOR, STUDENT_ID, VIEW_ID, EDUYEAR_ID, MONTH_P, SUM_P, STATUS_ID, ORDERNUMBER_P, ORDERDATE_P, TMP_P, LISTREPRESENT_ID) values(?,?,?,?,?,?,?,?,?,?,?,?)");
        short entityCode = meta.getEntityCode().shortValue();

        int counter = 0;
        int counter2 = 0;
        for (OrderGrantData ordData : orderData) {
            StudentGrantData found = find(ordData, sgeData);

            //нет назначенной стипендии
            if (found == null) {
                p("" + ++counter + " Not found. " + ordData.toSQLString());
                ordData.insertSGE(pst2, entityCode, this.sgeStatus, tmp);
            }
            else {
                found.represent_id = ordData.rep_id;
                found.doUpdate(pst);
                counter2++;
            }
        }

        p("process (inserted): " + counter);
        p("process (updated): " + counter2);

        return counter2;
    }

    private int processEmpty() throws Exception {
        Set<Long> modular = loadModularSGE();
        p("in modular: " + modular.size());

        int count = 0;
        PreparedStatement pst = tool.getConnection().prepareStatement("delete from " + this.meta.getTableName() + " where ID=?");

        String sql = "select ID from " + this.meta.getTableName() + " where LISTREPRESENT_ID is null";
        ResultSet rs = this.tool.getConnection().createStatement().executeQuery(sql);
        while (rs.next()) {
            Long id = rs.getLong(1);
            if (modular.contains(id)) {
                p("do NOT delete empty: " + id);
                continue;
            }

            p("delete empty : " + id);
            pst.setLong(1, id);
            pst.executeUpdate();

            count++;
        }
        rs.close();

        return count;
    }

    private StudentGrantData find(OrderGrantData ordData, Map<String, Object> sgeData) {
        String key = ordData.getKey();

        Object obj = sgeData.get(key);
        if (obj == null)
            return null;

        if (obj instanceof StudentGrantData) {
            sgeData.remove(key);
            return (StudentGrantData) obj;
        }

        List<StudentGrantData> sgeList = (List<StudentGrantData>) obj;
        StudentGrantData found = null;
        for (StudentGrantData sge : sgeList)
            if (sge.getKey().equals(key)) {
                found = sge;
                if (sge.status_id != this.sgeStatus)
                    break;

                sgeList.remove(sge);
                if (sgeList.isEmpty())
                    sgeData.remove(key);

                break;
            }

        if (found != null) {
            sgeList.remove(found);
            if (sgeList.isEmpty())
                sgeData.remove(key);
        }

        return found;
    }

    /*
    select
        null as ord_id,
        null as ord_number,
        null as ord_date,
        rep.ID as rep_id,
        repsup.DATEBEGININGPAYMENT_P as rep_datestart,
        repsup.DATEBEGININGPAYMENT_P as rep_dateend,
        repsup.GRANTVIEW_ID grantview_id,
        edu_year.id as year_id,
        edu_year.title_p as year_title,
        rep_stud.STUDENT_ID as stud_id,
        eou.formativeorgunit_id as fou_id,
        *
    from
        LISTREPRESENT_T rep
        inner join MOVESTUDENTEXTRACTSTATES_T rep_state on rep_state.ID=rep.STATE_ID
        inner join REPRESENTATIONTYPE_T rep_type on rep_type.ID = rep.REPRESENTATIONTYPE_ID
        inner join LISTREPRESENTSUPPORT_T repsup on repsup.ID=rep.id
        inner join educationyear_t edu_year on edu_year.id=repsup.EDUCATIONYEAR_ID
        inner join RELLISTREPRESENTSTUDENTS_T rep_stud on rep_stud.REPRESENTATION_ID=rep.ID
        inner join student_t st on st.id=rep_stud.STUDENT_ID
        inner join educationorgunit_t eou on eou.id=st.educationorgunit_id
    where
        rep_state.CODE_P<>'6'
        and rep_type.CODE_P='socialSupport'
     */
    private int loadOrderSocial_dataTmp(List<OrderGrantData> orderData) throws Exception {
        String sql = ""
                + " select"
                + " null as ord_id,"
                + " null as ord_number,"
                + " null as ord_date,"
                + " rep.ID as rep_id,"
                + " repsup.DATEBEGININGPAYMENT_P as rep_datestart,"
                + " repsup.DATEBEGININGPAYMENT_P as rep_dateend,"
                + " repsup.GRANTVIEW_ID grantview_id,"
                + " edu_year.id as year_id,"
                + " edu_year.title_p as year_title,"
                + " rep_stud.STUDENT_ID as stud_id,"
                + " eou.formativeorgunit_id as fou_id"
                + " from"
                + " LISTREPRESENT_T rep"
                + " inner join MOVESTUDENTEXTRACTSTATES_T rep_state on rep_state.ID=rep.STATE_ID"
                + " inner join REPRESENTATIONTYPE_T rep_type on rep_type.ID = rep.REPRESENTATIONTYPE_ID"
                + " inner join LISTREPRESENTSUPPORT_T repsup on repsup.ID=rep.id"
                + " inner join educationyear_t edu_year on edu_year.id=repsup.EDUCATIONYEAR_ID"
                + " inner join RELLISTREPRESENTSTUDENTS_T rep_stud on rep_stud.REPRESENTATION_ID=rep.ID"
                + " inner join student_t st on st.id=rep_stud.STUDENT_ID"
                + " inner join educationorgunit_t eou on eou.id=st.educationorgunit_id"
                + " where"
                + " rep_state.CODE_P<>?"
                + " and rep_type.CODE_P=?";

        PreparedStatement pst = this.tool.getConnection().prepareStatement(sql);
        pst.setString(1, "6"); //в приказах
        pst.setString(2, RepresentationTypeCodes.SOCIAL_SUPPORT); //rep_type.О назначении стипендии/выплаты

        ResultSet rs = pst.executeQuery();
        int counter = 0;
        while (rs.next()) {
            OrderGrantData od = OrderGrantData.getInstance(rs);
            if (od.error != null)
                p(od.error);

            List<OrderGrantData> data = od.generateByPeriod();
            orderData.addAll(data);
            counter += data.size();

        }
        rs.close();

        return counter;
    }

    /*
    select
        ord.ID as ord_id,
        ord.NUMBER_P as ord_number,
        ord.COMMITDATE_P as ord_date,
        rep.ID as rep_id,
        repsup.DATEBEGININGPAYMENT_P as rep_datestart,
        repsup.DATEBEGININGPAYMENT_P as rep_dateend,
        repsup.GRANTVIEW_ID grantview_id,
        edu_year.id as year_id,
        edu_year.title_p as year_title,
        rep_stud.STUDENT_ID as stud_id,
        eou.formativeorgunit_id as fou_id
    from
        LISTORDER_T ord
        inner join MOVESTUDENTORDERSTATES_T ord_st on ord_st.ID = ord.STATE_ID
        inner join LISTORDLISTREPRESENT_T ord_rel on ord_rel.ORDER_ID = ord.ID
        inner join LISTREPRESENT_T rep on rep.ID = ord_rel.REPRESENTATION_ID
        inner join REPRESENTATIONTYPE_T rep_type on rep_type.ID = rep.REPRESENTATIONTYPE_ID
        inner join LISTREPRESENTSUPPORT_T repsup on repsup.ID=rep.id
        inner join educationyear_t edu_year on edu_year.id=repsup.EDUCATIONYEAR_ID
        inner join RELLISTREPRESENTSTUDENTS_T rep_stud on rep_stud.REPRESENTATION_ID=rep.ID
        inner join student_t st on st.id=rep_stud.STUDENT_ID
        inner join educationorgunit_t eou on eou.id=st.educationorgunit_id
    where
        ord_st.CODE_P='5'
        and rep_type.CODE_P='socialSupport'

     */
    private int loadOrderSocial_data(List<OrderGrantData> orderData) throws Exception {
        String sql = ""
                + " select"
                + " ord.ID as ord_id,"
                + " ord.NUMBER_P as ord_number,"
                + " ord.COMMITDATE_P as ord_date,"
                + " rep.ID as rep_id,"
                + " repsup.DATEBEGININGPAYMENT_P as rep_datestart,"
                + " repsup.DATEBEGININGPAYMENT_P as rep_dateend,"
                + " repsup.GRANTVIEW_ID grantview_id,"
                + " edu_year.id as year_id,"
                + " edu_year.title_p as year_title,"
                + " rep_stud.STUDENT_ID as stud_id,"
                + " eou.formativeorgunit_id as fou_id"
                + " from"
                + " LISTORDER_T ord"
                + " inner join MOVESTUDENTORDERSTATES_T ord_st on ord_st.ID = ord.STATE_ID"
                + " inner join LISTORDLISTREPRESENT_T ord_rel on ord_rel.ORDER_ID = ord.ID"
                + " inner join LISTREPRESENT_T rep on rep.ID = ord_rel.REPRESENTATION_ID"
                + " inner join REPRESENTATIONTYPE_T rep_type on rep_type.ID = rep.REPRESENTATIONTYPE_ID"
                + " inner join LISTREPRESENTSUPPORT_T repsup on repsup.ID=rep.id"
                + " inner join educationyear_t edu_year on edu_year.id=repsup.EDUCATIONYEAR_ID"
                + " inner join RELLISTREPRESENTSTUDENTS_T rep_stud on rep_stud.REPRESENTATION_ID=rep.ID"
                + " inner join student_t st on st.id=rep_stud.STUDENT_ID"
                + " inner join educationorgunit_t eou on eou.id=st.educationorgunit_id"
                + " where"
                + " ord_st.CODE_P=?"
                + " and rep_type.CODE_P=?";

        PreparedStatement pst = this.tool.getConnection().prepareStatement(sql);
        pst.setString(1, "5"); //movestudentOrderStates.Проведено
        pst.setString(2, RepresentationTypeCodes.SOCIAL_SUPPORT); //rep_type.О назначении стипендии/выплаты

        ResultSet rs = pst.executeQuery();
        int counter = 0;
        while (rs.next()) {
            OrderGrantData od = OrderGrantData.getInstance(rs);
            if (od.error != null)
                p(od.error);

            List<OrderGrantData> data = od.generateByPeriod();
            orderData.addAll(data);
            counter += data.size();

        }
        rs.close();

        return counter;
    }

    /*
    select
        null as ord_id,
        null as ord_number,
        null as ord_date,
        rep.ID as rep_id,
        repgrant.DATEBEGININGPAYMENT_P as rep_datestart,
        repgrant.DATEENDOFPAYMENT_P as rep_dateend,
        repgrant.GRANTVIEW_ID as grantview_id,
        edu_year.id as year_id,
        edu_year.title_p as year_title,
        rep_stud.STUDENT_ID as stud_id,
        eou.formativeorgunit_id as fou_id
    from
        LISTREPRESENT_T rep
        inner join MOVESTUDENTEXTRACTSTATES_T rep_state on rep_state.ID=rep.STATE_ID
        inner join REPRESENTATIONTYPE_T rep_type on rep_type.ID = rep.REPRESENTATIONTYPE_ID
        inner join LISTREPRESENTGRANT_T repgrant on repgrant.ID=rep.ID
        inner join educationyear_t edu_year on edu_year.id=repgrant.EDUCATIONYEAR_ID
        inner join RELLISTREPRESENTSTUDENTS_T rep_stud on rep_stud.REPRESENTATION_ID=rep.ID
        inner join student_t st on st.id=rep_stud.STUDENT_ID
        inner join educationorgunit_t eou on eou.id=st.educationorgunit_id
    where
        rep_state.CODE_P<>'6'
        and rep_type.CODE_P='appointGrant'
     */
    private int loadOrderGrant_dataTmp(List<OrderGrantData> orderData) throws Exception {
        String sql = ""
                + " select"
                + " null as ord_id,"
                + " null as ord_number,"
                + " null as ord_date,"
                + " rep.ID as rep_id,"
                + " repgrant.DATEBEGININGPAYMENT_P as rep_datestart,"
                + " repgrant.DATEENDOFPAYMENT_P as rep_dateend,"
                + " repgrant.GRANTVIEW_ID as grantview_id,"
                + " edu_year.id as year_id,"
                + " edu_year.title_p as year_title,"
                + " rep_stud.STUDENT_ID as stud_id,"
                + " eou.formativeorgunit_id as fou_id"
                + " from"
                + " LISTREPRESENT_T rep"
                + " inner join MOVESTUDENTEXTRACTSTATES_T rep_state on rep_state.ID=rep.STATE_ID"
                + " inner join REPRESENTATIONTYPE_T rep_type on rep_type.ID = rep.REPRESENTATIONTYPE_ID"
                + " inner join LISTREPRESENTGRANT_T repgrant on repgrant.ID=rep.ID"
                + " inner join educationyear_t edu_year on edu_year.id=repgrant.EDUCATIONYEAR_ID"
                + " inner join RELLISTREPRESENTSTUDENTS_T rep_stud on rep_stud.REPRESENTATION_ID=rep.ID"
                + " inner join student_t st on st.id=rep_stud.STUDENT_ID"
                + " inner join educationorgunit_t eou on eou.id=st.educationorgunit_id"
                + " where"
                + " rep_state.CODE_P<>?"
                + " and rep_type.CODE_P=?";

        PreparedStatement pst = this.tool.getConnection().prepareStatement(sql);
        pst.setString(1, "6");
        pst.setString(2, RepresentationTypeCodes.APPOINT_GRANT); //rep_type.О назначении стипендии/выплаты

        ResultSet rs = pst.executeQuery();
        int counter = 0;
        while (rs.next()) {
            OrderGrantData od = OrderGrantData.getInstance(rs);
            if (od.error != null)
                p(od.error);

            List<OrderGrantData> data = od.generateByPeriod();
            orderData.addAll(data);
            counter += data.size();

        }
        rs.close();

        return counter;
    }

    /*
    select
        ord.ID as ord_id,
        ord.NUMBER_P as ord_number,
        ord.COMMITDATE_P as ord_date,
        rep.ID as rep_id,
        repgrant.DATEBEGININGPAYMENT_P as rep_datestart,
        repgrant.DATEENDOFPAYMENT_P as rep_dateend,
        repgrant.GRANTVIEW_ID as grantview_id,
        edu_year.id as year_id,
        edu_year.title_p as year_title,
        rep_stud.STUDENT_ID as stud_id,
        eou.formativeorgunit_id as fou_id
    from
        LISTORDER_T ord
        inner join MOVESTUDENTORDERSTATES_T ord_st on ord_st.ID = ord.STATE_ID
        inner join LISTORDLISTREPRESENT_T ord_rel on ord_rel.ORDER_ID = ord.ID
        inner join LISTREPRESENT_T rep on rep.ID = ord_rel.REPRESENTATION_ID
        inner join REPRESENTATIONTYPE_T rep_type on rep_type.ID = rep.REPRESENTATIONTYPE_ID
        inner join LISTREPRESENTGRANT_T repgrant on repgrant.ID=rep.ID
        inner join educationyear_t edu_year on edu_year.id=repgrant.EDUCATIONYEAR_ID
        inner join RELLISTREPRESENTSTUDENTS_T rep_stud on rep_stud.REPRESENTATION_ID=rep.ID
        inner join student_t st on st.id=rep_stud.STUDENT_ID
        inner join educationorgunit_t eou on eou.id=st.educationorgunit_id
    where
        ord_st.CODE_P='5'
        and rep_type.CODE_P='appointGrant'
     */
    private int loadOrderGrant_data(List<OrderGrantData> orderData) throws Exception {
        String sql =
                "select"
                        + " ord.ID as ord_id,"
                        + " ord.NUMBER_P as ord_number,"
                        + " ord.COMMITDATE_P as ord_date,"
                        + " rep.ID as rep_id,"
                        + " repgrant.DATEBEGININGPAYMENT_P as rep_datestart,"
                        + " repgrant.DATEENDOFPAYMENT_P as rep_dateend,"
                        + " repgrant.GRANTVIEW_ID as grantview_id,"
                        + " edu_year.id as year_id,"
                        + " edu_year.title_p as year_title,"
                        + " rep_stud.STUDENT_ID as stud_id,"
                        + " eou.formativeorgunit_id as fou_id"
                        + " from"
                        + " LISTORDER_T ord"
                        + " inner join MOVESTUDENTORDERSTATES_T ord_st on ord_st.ID = ord.STATE_ID"
                        + " inner join LISTORDLISTREPRESENT_T ord_rel on ord_rel.ORDER_ID = ord.ID"
                        + " inner join LISTREPRESENT_T rep on rep.ID = ord_rel.REPRESENTATION_ID"
                        + " inner join REPRESENTATIONTYPE_T rep_type on rep_type.ID = rep.REPRESENTATIONTYPE_ID"
                        + " inner join LISTREPRESENTGRANT_T repgrant on repgrant.ID=rep.ID"
                        + " inner join educationyear_t edu_year on edu_year.id=repgrant.EDUCATIONYEAR_ID"
                        + " inner join RELLISTREPRESENTSTUDENTS_T rep_stud on rep_stud.REPRESENTATION_ID=rep.ID"
                        + " inner join student_t st on st.id=rep_stud.STUDENT_ID"
                        + " inner join educationorgunit_t eou on eou.id=st.educationorgunit_id"
                        + " where"
                        + " ord_st.CODE_P=?"
                        + " and rep_type.CODE_P=?";

        PreparedStatement pst = this.tool.getConnection().prepareStatement(sql);
        pst.setString(1, "5"); //movestudentOrderStates.Проведено
        pst.setString(2, RepresentationTypeCodes.APPOINT_GRANT); //rep_type.О назначении стипендии/выплаты

        ResultSet rs = pst.executeQuery();
        int counter = 0;
        while (rs.next()) {
            OrderGrantData od = OrderGrantData.getInstance(rs);
            if (od.error != null)
                p(od.error);

            List<OrderGrantData> data = od.generateByPeriod();
            orderData.addAll(data);
            counter += data.size();

        }
        rs.close();

        return counter;
    }


    /*
    select
        sge.ID as id,
        sge.STUDENT_ID as stud_id,
        sge.VIEW_ID as view_id,
        sge.EDUYEAR_ID as year_id,
        sge.MONTH_P as month,
        sge.SUM_P as sum,
        sge.ORDERNUMBER_P as ord_number,
        sge.ORDERDATE_P as ord_date,
        sgs.ID as status_id,
        sgs.CODE_P as status_code
    from
        STUDENTGRANTENTITY_T sge
        inner join STUGRANTSTATUS_T sgs on sgs.ID=sge.STATUS_ID
    where
        sge.TMP_P=0
     */
    private int loadStudentGrants(Map<String, Object> sgeData, boolean tmp) throws Exception {
        String sql = ""
                + " select"
                + " sge.ID as id,"
                + " sge.STUDENT_ID as stud_id,"
                + " sge.VIEW_ID as view_id,"
                + " sge.EDUYEAR_ID as year_id,"
                + " sge.MONTH_P as month,"
                + " sge.SUM_P as sum,"
                + " sge.ORDERNUMBER_P as ord_number,"
                + " sge.ORDERDATE_P as ord_date,"
                + " sgs.ID as status_id,"
                + " sgs.CODE_P as status_code"
                + " from"
                + " STUDENTGRANTENTITY_T sge"
                + " inner join STUGRANTSTATUS_T sgs on sgs.ID=sge.STATUS_ID"
                + " where"
                + " sge.TMP_P=?	";

        PreparedStatement pst = this.tool.getConnection().prepareStatement(sql);
        pst.setBoolean(1, tmp);
        ResultSet rs = pst.executeQuery();

        int counter = 0;
        while (rs.next()) {
            StudentGrantData sgd = StudentGrantData.getInstance(rs);
            String key = sgd.getKey();

            if (sgeData.get(key) == null)
                sgeData.put(key, sgd);
            else if (sgeData.get(key) instanceof StudentGrantData) {
                StudentGrantData prev = (StudentGrantData) sgeData.get(key);

                List<StudentGrantData> list = new ArrayList<StudentGrantData>(Arrays.asList(prev, sgd));
                sgeData.put(key, list);
            }
            else {
                ((List) sgeData.get(key)).add(sgd);
            }
            counter++;
        }
        rs.close();

        return counter;
    }


    private FileWriter log = null;

    private void p(Object obj) throws Exception {
        /*
		if (log == null) {
			log = new FileWriter("d:/123.log");
		}

		String str = "";
		if (obj != null)
			str = obj.toString();
		
		log.write(str + "\n");
		*/
    }


    private static class OrderGrantData implements Cloneable {
        private Long ord_id;
        private String ord_number;
        private Date ord_date;
        private Long rep_id;
        private Date rep_datestart;
        private Date rep_dateend;
        private Long grantview_id;
        private Long year_id;
        private String year_title;
        private Long stud_id;
        private Long fou_id;

        private String month = null;
        private String error = null;

        public static OrderGrantData getInstance(ResultSet rs) throws Exception {
            OrderGrantData od = new OrderGrantData();

            od.ord_id = rs.getLong("ord_id");
            od.ord_number = rs.getString("ord_number");
            od.ord_date = rs.getDate("ord_date");
            od.rep_id = rs.getLong("rep_id");
            od.rep_datestart = rs.getDate("rep_datestart");
            od.rep_dateend = rs.getDate("rep_dateend");
            od.grantview_id = rs.getLong("grantview_id");
            od.year_id = rs.getLong("year_id");
            od.year_title = rs.getString("year_title");
            od.stud_id = rs.getLong("stud_id");
            od.fou_id = rs.getLong("fou_id");

            if (od.rep_id == null)
                od.error = "BEDA!!!\n   " + od.getKey() + " has null rep_id";

            return od;
        }

        public List<OrderGrantData> generateByPeriod() throws Exception {
            List<OrderGrantData> result = new ArrayList<OrderGrantData>();

            EducationYear eduYear = new EducationYear();
            eduYear.setTitle(this.year_title);
            List<String> monthList = MonthWrapper.getMonthsList(this.rep_datestart, this.rep_dateend, eduYear);

            for (String monthString : monthList) {
                OrderGrantData obj = (OrderGrantData) this.clone();
                obj.month = monthString;

                result.add(obj);
            }

            return result;
        }

        public void insertSGE(PreparedStatement pst, short entityCode, Long statusId, boolean tmp) throws Exception {
            //поиск суммы стипендии из настроек
            Float sum = 0.0f;
            String sql = ""
                    + "select ge.sum_p from GRANTENTITY_T ge"
                    + " where"
                    + "	ge.VIEW_ID=?"
                    + "	and ge.ORGUNIT_ID=?"
                    + "	and ge.EDUYEAR_ID=?"
                    + "	and ge.MONTH_P=?";
            PreparedStatement pst2 = pst.getConnection().prepareStatement(sql);
            pst2.setLong(1, this.grantview_id);
            pst2.setLong(2, this.fou_id);
            pst2.setLong(3, this.year_id);
            pst2.setString(4, this.month);
            ResultSet rs = pst2.executeQuery();
            if (rs.next())
                sum = rs.getFloat(1);
            rs.close();

            pst.setLong(1, EntityIDGenerator.generateNewId(entityCode).longValue()); //ID
            pst.setShort(2, entityCode);
            pst.setLong(3, this.stud_id); //STUDENT_ID
            pst.setLong(4, this.grantview_id);//VIEW_ID
            pst.setLong(5, this.year_id); //EDUYEAR_ID
            pst.setString(6, this.month); //MONTH_P
            pst.setFloat(7, sum); //SUM_P
            pst.setLong(8, statusId); //STATUS_ID
            pst.setString(9, this.ord_number); //ORDERNUMBER_P
            if (this.ord_date != null)
                pst.setDate(10, new java.sql.Date(this.ord_date.getTime())); //ORDERDATE_P
            else
                pst.setDate(10, null);
            pst.setBoolean(11, tmp); //TMP_P
            pst.setLong(12, this.rep_id); //LISTREPRESENT_ID

            pst.executeUpdate();
        }

        private String getKey() {
            StringBuilder sb = new StringBuilder()
                    .append(this.stud_id)
                    .append(this.grantview_id)
                    .append(this.month)
                    .append(this.ord_number);

            return sb.toString();
        }

        private String toSQLString() {
            StringBuilder sb = new StringBuilder()
                    .append("rep.ID=").append(this.rep_id)
                    .append(" and ")
                    .append("sge.STUDENT_ID=").append(this.stud_id)
                    .append(" and ")
                    .append("sge.MONTH_P='").append(this.month).append("'")
                    .append(" and ")
                    .append("sge.VIEW_ID=").append(this.grantview_id)
                    .append(" and ")
                    .append("sge.ORDERNUMBER_P='").append(this.ord_number).append("'")
                    .append(" and ")
                    .append("sge.ORDERDATE_P='").append(this.ord_date != null ? RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(this.ord_date) : "null").append("'");

            return sb.toString();
        }
    }


    private static class StudentGrantData {
        private Long id;
        private Long stud_id;
        private Long view_id;
        private Long year_id;
        private String month;
        private Double sum;
        private String ord_number;
        private Date ord_date;
        private Long status_id;

        private Long represent_id;

        public static StudentGrantData getInstance(ResultSet rs) throws Exception {
            StudentGrantData sd = new StudentGrantData();

            sd.id = rs.getLong("id");
            sd.stud_id = rs.getLong("stud_id");
            sd.view_id = rs.getLong("view_id");
            sd.year_id = rs.getLong("year_id");
            sd.month = rs.getString("month");
            sd.sum = rs.getDouble("sum");
            sd.ord_number = rs.getString("ord_number");
            sd.ord_date = rs.getDate("ord_date");
            sd.status_id = rs.getLong("status_id");

            return sd;
        }

        public String getKey() {
            StringBuilder sb = new StringBuilder()
                    .append(this.stud_id)
                    .append(this.view_id)
                    .append(this.month)
                    .append(this.ord_number);

            return sb.toString();
        }

        public int doUpdate(PreparedStatement pst) throws Exception {
            pst.setLong(1, this.represent_id);
            if (this.ord_date != null)
                pst.setDate(2, new java.sql.Date(this.ord_date.getTime()));
            else
                pst.setDate(2, null);
            pst.setLong(3, this.id);
            return pst.executeUpdate();
        }

        public String toSQLString() {
            StringBuilder sb = new StringBuilder()
                    .append("sge.STUDENT_ID").append(this.stud_id)
                    .append(" and ")
                    .append("sge.MONTH_P='").append(this.month).append("'")
                    .append(" and ")
                    .append("sge.VIEW_ID=").append(this.view_id)
                    .append(" and ")
                    .append("sge.ORDERNUMBER_P='").append(this.ord_number).append("'")
                    .append(" and ")
                    .append("sge.ORDERDATE_P='").append(RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(this.ord_date)).append("'");

            return sb.toString();
        }
    }
}
