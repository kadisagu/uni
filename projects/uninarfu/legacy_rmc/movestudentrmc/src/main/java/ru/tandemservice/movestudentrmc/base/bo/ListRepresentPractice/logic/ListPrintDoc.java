package ru.tandemservice.movestudentrmc.base.bo.ListRepresentPractice.logic;

import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudentrmc.docord.IListPrintDoc;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListRepresentPractice;
import ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents;
import ru.tandemservice.movestudentrmc.entity.StudentPracticeData;
import ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.*;

public class ListPrintDoc implements IListPrintDoc
{

    private Map<Student, OrderParagraphInfo> hashMap = new HashMap<>();

    public final String TEMPLATE_HEADER = "list.rep.practice.header";
    public final String TEMPLATE_PARAG = "list.rep.practice.parag";

    public RtfDocument creationPrintDocOrder(List<? extends ListRepresent> listOfRepresents)
    {
        PrintTemplate templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(PrintTemplate.class, TEMPLATE_HEADER);
        RtfDocument templateHeader = new RtfReader().read(templateDocument.getContent());

        templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(PrintTemplate.class, TEMPLATE_PARAG);
        RtfDocument templateParag = new RtfReader().read(templateDocument.getContent());

        Map<OrgUnit, Map<String, List<Object[]>>> dataMap = getStudentData(listOfRepresents);
        //сортировка формирующих по алфавиту
        List<OrgUnit> orgUnitList = new ArrayList<>(dataMap.keySet());
        Collections.sort(orgUnitList, new EntityComparator<>(new EntityOrder(OrgUnit.title())));

        RtfDocument result = templateHeader.getClone();
        result.setSettings(templateParag.getSettings());
        IRtfElement paragTemp = UniRtfUtil.findElement(result.getElementList(), "parag");
        new RtfInjectModifier().put("representTitle", listOfRepresents.get(0).getRepresentationType().getTitle()).modify(result);

        RtfDocument res = RtfBean.getElementFactory().createRtfDocument();
        res.setHeader(templateParag.getHeader());
        res.setSettings(templateParag.getSettings());
        res.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.TAB));

        int formCounter = 0;
        int addonCounter = 0;
        List<RtfDocument> addonList = new ArrayList<>();


        for (OrgUnit formOU : orgUnitList)
        {
            Map<String, List<Object[]>> formMap = dataMap.get(formOU);

            formCounter++;
            res.getElementList().add(RtfBean.getElementFactory().createRtfText(String.valueOf(formCounter).trim() + "."));
            res.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.TAB));
            insertRow(res, "По " + formOU.getDativeCaseTitle());

            int counter = 0;
            for (List<Object[]> studentList : formMap.values())
            {
                counter++;
                addonCounter++;
                RtfDocument par = getParagraph(templateParag, formCounter, counter, addonCounter, studentList);
                res.getElementList().addAll(par.getElementList());
                //Приложение печатать не нужно
                //addonList.add(getAddon(templateAddon, addonCounter, studentList, order));
            }
        }

        //основания
        res.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
        insertRow(res, "Основание: " + UtilPrintSupport.getBasementTitles(listOfRepresents));
        result.getElementList().addAll(result.getElementList().indexOf(paragTemp), res.getElementList());

        result.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));

        for (int i = 0; i < addonList.size(); i++)
        {
            result.getElementList().addAll(addonList.get(i).getElementList());
            if (i + 1 < addonList.size())
                result.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
        }
        SharedRtfUtil.removeParagraphsWithTagsRecursive(result, Collections.singletonList("parag"), false, false);
        return result;
    }

    private RtfDocument getParagraph(RtfDocument template, int formCounter, int counter, int addonCounter, List<Object[]> studentList)
    {
        RtfDocument result = template.getClone();
        ListRepresentPractice listRepresent = (ListRepresentPractice) studentList.get(0)[1];
        /*
         * ситуация, когда студентов отправляют на одну и ту же практику разными представлениями
         * для получения доп. данных по практике необходимо взять id представлений
         */
        Set<Long> listRepresentIds = new HashSet<>();

        Student student = (Student) studentList.get(0)[0];
        for (Object[] o : studentList)
        {
            Student s = (Student) o[0];
            listRepresentIds.add(((ListRepresentPractice) o[1]).getId());
            hashMap.put(s, new OrderParagraphInfo(formCounter, counter));
        }
        RtfInjectModifier im = new RtfInjectModifier();

        im.put("i", String.valueOf(formCounter).trim());
        im.put("j", String.valueOf(counter).trim());
        im.put("addonNumber", String.valueOf(addonCounter));

        injectModifier(im, listRepresent, student);
        im.modify(result);
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(StudentPracticeData.class, "spd");
        dql.where(DQLExpressions.in(
                DQLExpressions.property(StudentPracticeData.listRepresent().id().fromAlias("spd")),
                listRepresentIds
        ));
        List<StudentPracticeData> practiceDataList = UniDaoFacade.getCoreDao().getList(dql);
        Map<Student, StudentPracticeData> dataMap = new HashMap<>();
        for (StudentPracticeData data : practiceDataList)
        {
            dataMap.put(data.getStudent(), data);
        }
        List<String[]> tableList = new ArrayList<>();
        int rowCounter = 1;
        for (Object[] arr : studentList)
        {
            Student stu = (Student) arr[0];
            StudentPracticeData practiceData = dataMap.get(stu);
            String[] row = new String[6];
            row[0] = "" + rowCounter++;
            row[1] = stu.getPerson().getFullFio();
            row[2] = UtilPrintSupport.getCompensationTypeStr(stu.getCompensationType());
            row[3] = practiceData.getInnerAdvisor() == null ? "" : practiceData.getInnerAdvisor().getFullFio();
            row[4] = practiceData.getPracticeBase() == null ? "" : practiceData.getPracticeBase().getTitle();
            row[5] = practiceData.getOuterAdvisor();
            tableList.add(row);
        }

        RtfTableModifier tm = new RtfTableModifier();
        tm.put("T", tableList.toArray(new String[][]{}));
        tm.modify(result);
        return result;
    }

    public static void injectModifier(RtfInjectModifier im, ListRepresentPractice listRepresent, Student student)
    {
        im.put("dateBegin", UtilPrintSupport.getDateFormatterWithMonthString(listRepresent.getDateBeginningPractice()));
        im.put("dateEnd", UtilPrintSupport.getDateFormatterWithMonthString(listRepresent.getDateEndOfPractice()));
        im.put("course", student.getCourse().getTitle());
        im.put("developForm", UtilPrintSupport.getDevelopFormGen(student.getEducationOrgUnit().getDevelopForm()));
        im.put("highSchool", UtilPrintSupport.getHighLevelSchoolTypeString(student.getEducationOrgUnit().getEducationLevelHighSchool()));
        im.put("reason", listRepresent.getRepresentationReason().getTitle());
        im.put("practiceTitle", listRepresent.getPracticeTitle());
        //------------------------------------------------------------------------------------------------
        //im.put("practicePaymentData", listRepresent.getPaymentData() == null ? "" : listRepresent.getPaymentData().getTitle());
        im.put("practicePaymentData", listRepresent.getPaymentData() == null ? "" : ", "+listRepresent.getPaymentData().getTitle());
        //------------------------------------------------------------------------------------------------
    }


    private void insertRow(RtfDocument document, String text)
    {
        document.getElementList().add(RtfBean.getElementFactory().createRtfText(text));
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
    }

    private String getKey(Student student, ListRepresentPractice represent)
    {
        return new StringBuilder()
                .append(student.getEducationOrgUnit().getDevelopForm().getId())
                .append(student.getCourse().getId())
                .append(student.getEducationOrgUnit().getEducationLevelHighSchool().getId())
                .append(represent.getDateBeginningPractice().getTime())
                .append(represent.getDateEndOfPractice().getTime())
                .append(represent.getPracticeTitle())
                .append(represent.getPaymentData() != null ? represent.getPaymentData().getId() : "")
                .toString();
    }

    private Map<OrgUnit, Map<String, List<Object[]>>> getStudentData(List<? extends ListRepresent> listOfRepresents)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(RelListRepresentStudents.class, "rel")
                .where(DQLExpressions.in(
                        DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("rel")),
                        listOfRepresents
                ))
                .joinEntity(
                        "rel",
                        DQLJoinType.inner,
                        ListRepresentPractice.class,
                        "rep",
                        DQLExpressions.eq(
                                DQLExpressions.property(ListRepresentPractice.id().fromAlias("rep")),
                                DQLExpressions.property(RelListRepresentStudents.representation().id().fromAlias("rel"))
                        ))
                .column(DQLExpressions.property(RelListRepresentStudents.student().fromAlias("rel")))
                .column("rep")
                .order(DQLExpressions.property(RelListRepresentStudents.student().person().identityCard().fullFio().fromAlias("rel")));

        List<Object[]> studentDataList = UniDaoFacade.getCoreDao().getList(dql);

        Map<OrgUnit, Map<String, List<Object[]>>> dataMap = new HashMap<>();
        for (Object[] arr : studentDataList)
        {
            Student student = (Student) arr[0];
            ListRepresentPractice represent = (ListRepresentPractice) arr[1];

            OrgUnit formOU = student.getEducationOrgUnit().getFormativeOrgUnit();
            if (dataMap.get(formOU) == null)
                dataMap.put(formOU, new HashMap<>());

            String key = getKey(student, represent);
            if (dataMap.get(formOU).get(key) == null)
                dataMap.get(formOU).put(key, new ArrayList<>());

            dataMap.get(formOU).get(key).add(arr);
        }

        return dataMap;
    }


    @Override
    public Map<Student, OrderParagraphInfo> getParagInfomap()
    {
        return hashMap;
    }
}
