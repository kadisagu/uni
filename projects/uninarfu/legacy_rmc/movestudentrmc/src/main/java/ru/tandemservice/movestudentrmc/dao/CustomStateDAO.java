package ru.tandemservice.movestudentrmc.dao;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.docord.util.MoveStudentUtil;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Date;
import java.util.List;

public class CustomStateDAO extends UniBaseDao implements ICustomStateDAO {

    @Override
    public void commitStudentCustomState(StudentCustomStateCI state,
                                         Date beginDate, Date endDate, ListRepresent represent)
    {

        List<RelListRepresentStudents> representList = MoveStudentUtil.getStudentList(represent);

        ListOrder order = MoveStudentUtil.getOrder(represent);

        for (RelListRepresentStudents rel : representList) {
            Student student = rel.getStudent();

            StudentCustomState studentCustomState = new StudentCustomState();

            studentCustomState.setStudent(student);
            studentCustomState.setCustomState(state);
            studentCustomState.setBeginDate(beginDate);
            studentCustomState.setEndDate(endDate);
            studentCustomState.setDescription(order.getFullTitle());

            UniStudentManger.instance().studentCustomStateDAO().saveOrUpdate(studentCustomState);

            if (!UserContext.getInstance().getErrorCollector().hasErrors()) {
                RelStudentCustomStateRepresent r = new RelStudentCustomStateRepresent();
                r.setStudentCustomState(studentCustomState);
                r.setListRepresent(represent);
                getSession().saveOrUpdate(r);
            }
        }
    }

    @Override
    public void rollbackStudentCustomState(ListRepresent represent) {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentCustomState.class, "st")
                .joinEntity("st", DQLJoinType.inner, RelStudentCustomStateRepresent.class, "rel",
                            DQLExpressions.eq(
                                    DQLExpressions.property(StudentCustomState.id().fromAlias("st")),
                                    DQLExpressions.property(RelStudentCustomStateRepresent.studentCustomState().id().fromAlias("rel"))
                            ))
                .where(DQLExpressions.eqValue(DQLExpressions.property(RelStudentCustomStateRepresent.listRepresent().id().fromAlias("rel")), represent.getId()))
                .column("st.id");

        new DQLDeleteBuilder(StudentCustomState.class)
                .where(DQLExpressions.in(DQLExpressions.property(StudentCustomState.id()), builder.getQuery()))
                .createStatement(getSession()).execute();
    }

    @Override
    public void rollbackStudentCustomState(Representation represent) {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentCustomState.class, "st")
                .joinEntity("st", DQLJoinType.inner, RelStudentCustomStateRepresent.class, "rel",
                            DQLExpressions.eq(
                                    DQLExpressions.property(StudentCustomState.id().fromAlias("st")),
                                    DQLExpressions.property(RelStudentCustomStateRepresent.studentCustomState().id().fromAlias("rel"))
                            ))
                .where(DQLExpressions.eqValue(DQLExpressions.property(RelStudentCustomStateRepresent.representation().id().fromAlias("rel")), represent.getId()))
                .column("st.id");

        new DQLDeleteBuilder(StudentCustomState.class)
                .where(DQLExpressions.in(DQLExpressions.property(StudentCustomState.id()), builder.getQuery()))
                .createStatement(getSession()).execute();
    }

    @Override
    public void commitStudentCustomState(StudentCustomStateCI state,
                                         Date beginDate, Date endDate, Representation represent)
    {

        Student student = UtilPrintSupport.getStudent(represent);

        DocumentOrder order = MoveStudentUtil.getOrder(represent);

        StudentCustomState studentCustomState = new StudentCustomState();

        studentCustomState.setStudent(student);
        studentCustomState.setCustomState(state);
        studentCustomState.setBeginDate(beginDate);
        studentCustomState.setEndDate(endDate);
        studentCustomState.setDescription(order.getFullTitle());

        UniStudentManger.instance().studentCustomStateDAO().saveOrUpdate(studentCustomState);

        if (!UserContext.getInstance().getErrorCollector().hasErrors()) {
            RelStudentCustomStateRepresent r = new RelStudentCustomStateRepresent();
            r.setStudentCustomState(studentCustomState);
            r.setRepresentation(represent);
            getSession().saveOrUpdate(r);
        }

    }


}
