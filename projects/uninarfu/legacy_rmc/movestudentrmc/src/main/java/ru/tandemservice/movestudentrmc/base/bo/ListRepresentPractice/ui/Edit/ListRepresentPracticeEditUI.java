package ru.tandemservice.movestudentrmc.base.bo.ListRepresentPractice.ui.Edit;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections15.keyvalue.MultiKey;
import org.hibernate.Session;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Activator;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.ui.View.PersonView;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.StudentDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.StudentSelectedDSHandlerPractice;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ui.MassInnerAdvisorEdit.ListRepresentBaseMassInnerAdvisorEdit;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ui.MassInnerAdvisorEdit.ListRepresentBaseMassInnerAdvisorEditUI;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentPractice.ListRepresentPracticeManager;
import ru.tandemservice.movestudentrmc.dao.CheckStateUtil;
import ru.tandemservice.movestudentrmc.entity.DocListRepresentBasics;
import ru.tandemservice.movestudentrmc.entity.ListRepresentPractice;
import ru.tandemservice.movestudentrmc.entity.catalog.*;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentExtractStatesCodes;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.entity.registry.EppRegistryPractice;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

@State({
        @Bind(key = ListRepresentPracticeEditUI.LIST_REPRESENT_ID, binding = ListRepresentPracticeEditUI.LIST_REPRESENT_ID),
        @Bind(key = "typeRepresentFilter", binding = "typeRepresentFilter"),
        @Bind(key = "reasonRepresentFilter", binding = "reasonRepresentFilter"),
        @Bind(key = "basementRepresentFilter", binding = "basementRepresentFilter"),
        @Bind(key = "representationBasementDate", binding = "representationBasementDate"),
        @Bind(key = "representationBasementNumber", binding = "representationBasementNumber")
})
public class ListRepresentPracticeEditUI extends UIPresenter
{

    public static final String LIST_REPRESENT_ID = "listRepresentId";
    public static final String SUPERVISOR = "supervisor";
    public static final String USER_CLICK_APPLY = "userClickApply";
    public static final String PRACTICE_TITLE = "practiceTitle";
    public static final String DATE_BEGINNING_PRACTICE = "dateBeginningPractice";
    public static final String DATE_END_OF_PRACTICE = "dateEndOfPractice";
    public static final String PAYMENT_DATA = "paymentData";
    public static final String PRACTICE = "practice";
    public static final String IS_SUCCESSFULLY_HANDOVER_SESSION = "isSuccessfullyHandOverSession";
    public static final String YEAR_DIST_PART_FILTER = "yearDistPartFilter";
    public static final String EDUCATION_YEAR_FILTER = "educationYearFilter";
    public static final String IS_CONSIDER_SESSION_RESULTS = "isConsiderSessionResults";
    public static final String SESSION_MARKS_RESULT = "sessionMarksResult";
    public static final String FORMATIVE_ORGUNIT_FILTER = "formativeOrgUnitFilter";
    public static final String EDUCATION_LEVELS_HIGH_SCHOOL_DS_FILTER = "educationLevelsHighSchoolDSFilter";
    public static final String TERRITORIAL_ORGUNIT_FILTER = "territorialOrgUnitFilter";
    public static final String EDUCATION_ORGUNIT_FILTER = "educationOrgUnitFilter";
    public static final String GROUP_FILTER = "groupFilter";


    private Long listRepresentId;
    private RepresentationType typeRepresentFilter;
    private RepresentationReason reasonRepresentFilter;
    private RepresentationBasement basementRepresentFilter;

    private ISelectModel _supervisorModel;
    private Date _representationBasementDate;     // Дата основания
    private String _representationBasementNumber;     // № основания
    private List<DataWrapper> studentSelectedList;
    private List<Student> stuSelectedList;
    private ISelectModel practiceModel;
    private Map<EppWorkPlanRow, Set<MultiKey>> rowToDates = new HashMap<>();

    //    События компонента
    @Override
    public void onComponentActivate()
    {
        super.onComponentActivate();
        clearSettings();
    }

    @Override
    public void onComponentPrepareRender()
    {
        fillPracticeModel();
        if (!getSettings().getDefault(IS_SUCCESSFULLY_HANDOVER_SESSION, false))
        {
            getSettings().set(YEAR_DIST_PART_FILTER, null);
            getSettings().set(EDUCATION_YEAR_FILTER, null);
        }

        if (!getSettings().getDefault(IS_CONSIDER_SESSION_RESULTS, false))
            getSettings().set(SESSION_MARKS_RESULT, null);
    }


    @Override
    public void onComponentRefresh()
    {
        _supervisorModel = new DQLFullCheckSelectModel(Employee.P_FULL_FIO)
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(Employee.class, alias);
                FilterUtils.applySimpleLikeFilter(dql, alias, Employee.person().identityCard().fullFio().s(), filter);
                return dql;
            }
        };

        if (!getSettings().getDefault(USER_CLICK_APPLY, false))
        {
            if (listRepresentId != null)
            {
                ListRepresentPractice listRepresent = (ListRepresentPractice) getSupport().getSession().get(ListRepresentPractice.class, listRepresentId);
                getSettings().set(PRACTICE_TITLE, listRepresent.getPracticeTitle());
                getSettings().set(DATE_BEGINNING_PRACTICE, listRepresent.getDateBeginningPractice());
                getSettings().set(DATE_END_OF_PRACTICE, listRepresent.getDateEndOfPractice());
                getSettings().set(PAYMENT_DATA, listRepresent.getPaymentData());
                getSettings().set(PRACTICE, listRepresent.getRegistryElementRow());
                getSettings().set(SUPERVISOR, listRepresent.getSupervisor());

                stuSelectedList = ListRepresentPracticeManager.instance().getListObjectModifyDAO().selectRepresent(listRepresent);

                if (studentSelectedList == null)
                {
                    studentSelectedList = new ArrayList<>();
                    for (Student student : stuSelectedList)
                    {
                        DataWrapper dw = new DataWrapper(student.getId(), student.getTitle(), student);
                        dw.setProperty("student", student);
                        studentSelectedList.add(dw);
                    }
                }
            }
            else
            {
                if (studentSelectedList == null)
                    studentSelectedList = new ArrayList<>();
                if (stuSelectedList == null)
                    stuSelectedList = new ArrayList<>();
            }
        }
        
    }
    
    //---------------------------------------------------------------------------------------------------------------------------
    @Override
    public void onComponentBindReturnParameters(String childRegionName, Map<String, Object> returnedData) {

    	super.onComponentBindReturnParameters(childRegionName, returnedData);
    	
        if(returnedData!=null && returnedData.containsKey("clearCheckedStudents") && (boolean)returnedData.get("clearCheckedStudents")) {
        		
    	    CheckboxColumn column = (CheckboxColumn)((BaseSearchListDataSource)getConfig().getDataSource(ListRepresentPracticeEdit.STUDENT_SELECTED_DS)).getLegacyDataSource().getColumn(StudentSelectedDSHandlerPractice.CHECKBOX_COLUMN);
    	    column.getSelectedObjects().clear();
    	    	 
        }
    }
    //--------------------------------------------------------------------------------------------------------------------------

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        Map<String, Object> settingMap = getSettings().getAsMap(
                EDUCATION_YEAR_FILTER,
                YEAR_DIST_PART_FILTER,
                TERRITORIAL_ORGUNIT_FILTER,
                StudentDSHandler.FORMATIVE_ORG_UNIT_FILTER,
                EDUCATION_LEVELS_HIGH_SCHOOL_DS_FILTER,
                EDUCATION_ORGUNIT_FILTER,
                "educationLevelFilter",
                StudentDSHandler.QUALIFICATION_FILTER,
                "courseFilter",
                GROUP_FILTER,
                StudentDSHandler.DEVELOP_FORM_FILTER,
                "compensationTypeFilter",
                "benefitFilter",
                StudentDSHandler.STUDENT_FIO_FILTER,
                DATE_BEGINNING_PRACTICE,
                DATE_END_OF_PRACTICE,
                PRACTICE_TITLE,
                PAYMENT_DATA,
                "popupGrant",
                IS_SUCCESSFULLY_HANDOVER_SESSION,
                SESSION_MARKS_RESULT,
                IS_CONSIDER_SESSION_RESULTS,
                "isforeignCitizens",
                "studentFilter",
                USER_CLICK_APPLY,
                "studentStatusFilter",
                PRACTICE
        );
        dataSource.putAll(settingMap);
        dataSource.put(StudentSelectedDSHandlerPractice.STUDENT_SELECTED_LIST, studentSelectedList);
        dataSource.put(StudentSelectedDSHandlerPractice.LIST_REPRESENT_ID, getListRepresentId());
        dataSource.put(StudentDSHandler.STUDENT_SELECTED_LIST, stuSelectedList);
        dataSource.put(StudentDSHandler.ORDER_ID, getListRepresentId());
    }

    //    События формы
    public boolean getDisableSessionResultFilters()
    {
        List<OrgUnit> fou = getSettings().get(FORMATIVE_ORGUNIT_FILTER);
        List<OrgUnit> el = getSettings().get(EDUCATION_LEVELS_HIGH_SCHOOL_DS_FILTER);
        List<OrgUnit> tou = getSettings().get(TERRITORIAL_ORGUNIT_FILTER);
        List<OrgUnit> eou = getSettings().get(EDUCATION_ORGUNIT_FILTER);
        List<Group> group = getSettings().get(GROUP_FILTER);
        if ((fou != null && fou.size() > 0)
                || (tou != null && tou.size() > 0)
                || (el != null && el.size() > 0)
                || (eou != null && eou.size() > 0)
                || (group != null && group.size() > 0))
        {
            return false;
        }
        else
        {
            getSettings().set(IS_SUCCESSFULLY_HANDOVER_SESSION, false);
            getSettings().set(IS_CONSIDER_SESSION_RESULTS, false);
            return true;
        }

    }

    public void selectRepresent()
    {
        BaseSearchListDataSource representDS = getConfig().getDataSource(ListRepresentPracticeEdit.STUDENT_DS);

        Collection<IEntity> records = ((CheckboxColumn) representDS.getLegacyDataSource().getColumn(StudentDSHandler.CHECKBOX_COLUMN)).getSelectedObjects();
        List<Student> selectList = records.stream().map(record -> (Student) record).collect(Collectors.toList());

        if (getTypeRepresentFilter() != null)
        {
            ListRepresentPractice listRepresent;
            if (listRepresentId != null)
                listRepresent = (ListRepresentPractice) getSupport().getSession().get(ListRepresentPractice.class, listRepresentId);
            else
            {
                listRepresent = new ListRepresentPractice();
                listRepresent.setCreateDate(getSupport().getCurrentDate());
            }
            fillListRepresent(listRepresent);
            ListRepresentPracticeManager.instance().getListObjectModifyDAO().checkSelectStudent(listRepresent, selectList);
        }


        stuSelectedList.stream().filter(student -> !selectList.contains(student)).forEach(selectList::add);
        if (selectList.isEmpty())
            return;

        rowToDates.clear();

        final List<Long> practiceList = checkStudentWP(selectList);
        if (practiceList.isEmpty())
        {
            ContextLocal.getErrorCollector().add("Для выбранных студентов нет общих практик, закрепленных в РУП");
            selectList.clear();
        }

        for (IEntity record : selectList)
        {
            Student student = (Student) record;
            if (!stuSelectedList.contains(student))
            {
                stuSelectedList.add(student);
                DataWrapper dw = new DataWrapper(student.getId(), student.getTitle(), student);
                dw.setProperty("student", student);
                studentSelectedList.add(dw);
            }
        }
        fillPracticeModel();

    }

    private void fillPracticeModel()
    {
        if (!CollectionUtils.isEmpty(stuSelectedList))
        {
        	List<EppWorkPlanRow> rowList = IUniBaseDao.instance.get().getList(EppWorkPlanRow.class, checkStudentWP(stuSelectedList));
            EppWorkPlanRow currentRow = getSettings().get(PRACTICE);
            if (currentRow != null && !rowList.contains(currentRow))
                rowList.add(currentRow);
            setPracticeModel(new LazySimpleSelectModel<>(rowList, "displayableTitle"));

        }
    }

    public void deleteSelectedRepresent()
    {

        BaseSearchListDataSource representDS = getConfig().getDataSource(ListRepresentPracticeEdit.STUDENT_SELECTED_DS);

        Collection<IEntity> records = ((CheckboxColumn) representDS.getLegacyDataSource().getColumn(StudentSelectedDSHandlerPractice.CHECKBOX_COLUMN)).getSelectedObjects();

        for (IEntity record : records)
        {
            DataWrapper dataWrapper = (DataWrapper) record;
            studentSelectedList.remove(dataWrapper);
        }
        
        //----------------------------------------------------------
        stuSelectedList.removeAll(records);
        //----------------------------------------------------------
    }

    public void onClickSave()
    {
        Session session = getSupport().getSession();

        ListRepresentPractice listRepresent;
        if (listRepresentId != null)
        {
            listRepresent = (ListRepresentPractice) session.get(ListRepresentPractice.class, listRepresentId);
            CheckStateUtil.checkStateRepresent(listRepresent, Collections.singletonList(MovestudentExtractStatesCodes.CODE_1), session);
        }
        else
        {
            listRepresent = new ListRepresentPractice();
            listRepresent.setCreateDate(getSupport().getCurrentDate());
            listRepresent.setRepresentationType(getTypeRepresentFilter());
            listRepresent.setRepresentationReason(getReasonRepresentFilter());
            listRepresent.setRepresentationBasement(getBasementRepresentFilter());
            listRepresent.setState(DataAccessServices.dao().get(MovestudentExtractStates.class, MovestudentExtractStates.code().s(), "1"));
        }

        if (studentSelectedList.isEmpty())
            getConfig().getUserContext().getErrorCollector().add(getConfig().getProperty("ui.alertEmptySelectedRepresent"));

        fillListRepresent(listRepresent);

        if (getConfig().getUserContext().getErrorCollector().hasErrors())
            return;

        List<DocListRepresentBasics> representOldBasicsList = new DQLSelectBuilder()
                .fromEntity(DocListRepresentBasics.class, "b").column("b")
                .where(eq(property(DocListRepresentBasics.listRepresent().fromAlias("b")), value(listRepresent)))
                .createStatement(session).list();

        DocListRepresentBasics representBasics;

        if (representOldBasicsList.size() > 0)
            representBasics = representOldBasicsList.get(0);
        else
        {
            representBasics = new DocListRepresentBasics();
            representBasics.setListRepresent(listRepresent);
            representBasics.setBasic(listRepresent.getRepresentationBasement());
            representBasics.setReason(listRepresent.getRepresentationReason());

            representBasics.setRepresentationBasementDate(_representationBasementDate);
            representBasics.setRepresentationBasementNumber(_representationBasementNumber);
        }
        session.saveOrUpdate(listRepresent);
        ListRepresentPracticeManager.instance().getListObjectModifyDAO().saveSupport(listRepresent, studentSelectedList, representBasics);

        deactivate(2);
    }

    //    Вспомогательные функции

    private List<Long> checkStudentWP(List<Student> list)
    {
        List<EppStudentWorkPlanElement> wpList = new DQLSelectBuilder()
                .column("s")
                .fromEntity(EppStudentWorkPlanElement.class, "s")
                .where(in(property(EppStudentWorkPlanElement.student().fromAlias("s")), list))
                .joinPath(DQLJoinType.inner, EppStudentWorkPlanElement.registryElementPart().registryElement().fromAlias("s"), "re")
                .where(instanceOf("re", EppRegistryPractice.class))
                .where(isNotNull(property(EppStudentWorkPlanElement.sourceRow().fromAlias("s"))))
                //-----------------------------------------------------------------------------------
                .where(isNull(property(EppStudentWorkPlanElement.removalDate().fromAlias("s"))))
                //-----------------------------------------------------------------------------------
                .createStatement(getSupport().getSession()).list();

        //Поставим в соответствие практикам списки студентов
        Map<EppWorkPlanRow, HashSet<Student>> map = new HashMap<>();
        for (EppStudentWorkPlanElement slot : wpList)
        {
            Student stu = slot.getStudent();
            EppWorkPlanRow row = slot.getSourceRow();
            if (!map.containsKey(row))
                map.put(row, new HashSet<>());
            map.get(row).add(stu);
            if (row.getBeginDate() != null || row.getEndDate() != null)
            {
                if (!rowToDates.containsKey(row))
                    rowToDates.put(row, new HashSet<>());
                rowToDates.get(row).add(new MultiKey<>(row.getBeginDate(), row.getEndDate()));
            }
        }
        //если есть практики, общие для всех студентов - то true
        return map.entrySet().stream()
        		//--------------------------------------------------------
                //.filter(entry -> entry.getValue().size() == list.size())
                //--------------------------------------------------------
                .map(entry -> entry.getKey().getId())
                .collect(Collectors.toList());
    }

    public void onClickView()
    {
        try
        {
            Student st = DataAccessServices.dao().get(Student.class, getListenerParameter());
            getActivationBuilder().asRegionDialog(PersonView.class)
                    .parameter("publisherId", st.getPerson().getId())
                    .activate();
        }
        catch (Exception e)
        {
            getUserContext().getErrorCollector().add(e.getLocalizedMessage());
        }


    }

    public void onClickEdit()
    {
        saveSettings();
        if (studentSelectedList.isEmpty())
            ContextLocal.getInfoCollector().add("Список выбранных студентов пуст");
        else
        {
            ListRepresentPractice listRepresent;
            if (listRepresentId != null)
                listRepresent = (ListRepresentPractice) getSupport().getSession().get(ListRepresentPractice.class, listRepresentId);
            else
            {
                listRepresent = new ListRepresentPractice();
                listRepresent.setCreateDate(getSupport().getCurrentDate());
            }

            fillListRepresent(listRepresent);

            Activator activator = new ComponentActivator("ru.tandemservice.movestudentrmc.component.represent.StudentsPractice",
                                                         new ParametersMap().add("represent", listRepresent).add("list", studentSelectedList));
            UserContext.getInstance().activateDesktop("PersonShellDialog", activator);
        }
    }

    public void onClickMassEditInnerAdvisor()
    {
        saveSettings();
        BaseSearchListDataSource representDS = getConfig().getDataSource(ListRepresentPracticeEdit.STUDENT_SELECTED_DS);

        Collection<IEntity> records = ((CheckboxColumn) representDS.getLegacyDataSource()
                .getColumn(StudentSelectedDSHandlerPractice.CHECKBOX_COLUMN)).getSelectedObjects();

        if (records.isEmpty())
            ContextLocal.getInfoCollector().add("Нет выбранных студентов");
        else
            getActivationBuilder()
                    .asRegionDialog(ListRepresentBaseMassInnerAdvisorEdit.class)
                    .parameter(ListRepresentBaseMassInnerAdvisorEditUI.RECORDS, records)
                    .activate();
    }

    @Override
    public void saveSettings()
    {
        _uiSettings.set(USER_CLICK_APPLY, true);
        super.saveSettings();
    }

    public void onChangePractice()
    {
        if (getSettings().get(PRACTICE) != null)
        {
            getSettings().set(DATE_BEGINNING_PRACTICE, null);
            getSettings().set(DATE_END_OF_PRACTICE, null);
            EppWorkPlanRow row = getSettings().get(PRACTICE);
            getSettings().set(PRACTICE_TITLE, row.getDisplayableTitle());

            Set<MultiKey> multiKeySet = rowToDates.get(row);
            if (multiKeySet != null && multiKeySet.size() == 1)
            {
                MultiKey multiKey = multiKeySet.iterator().next();
                getSettings().set(DATE_BEGINNING_PRACTICE, multiKey.getKey(0));
                getSettings().set(DATE_END_OF_PRACTICE, multiKey.getKey(1));
            }
        }
    }

    public void onChangeDate()
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppStudentWorkPlanElement.class, "s");
        Date beginDate = _uiSettings.get(DATE_BEGINNING_PRACTICE, Date.class);
        Date endDate = _uiSettings.get(DATE_END_OF_PRACTICE, Date.class);
        if (stuSelectedList != null && stuSelectedList.size() > 0)
        {
            //Выберем мероприятия для студентов, даты проведения которых попадают в интервал
            dql.where(in(property(EppStudentWorkPlanElement.student().fromAlias("s")), stuSelectedList));
            if (beginDate != null)
                dql.where(ge(property(EppStudentWorkPlanElement.sourceRow().beginDate().fromAlias("s")), valueDate(beginDate)));
            if (endDate != null)
                dql.where(le(property(EppStudentWorkPlanElement.sourceRow().endDate().fromAlias("s")), valueDate(endDate)));

            List<EppStudentWorkPlanElement> list = dql.createStatement(getSupport().getSession()).list();
            //оставим только практики
            List<EppWorkPlanRow> practices = list.stream()
                    .filter(stu -> stu.getRegistryElementPart().getRegistryElement() instanceof EppRegistryPractice)
                    .map(EppStudentWorkPlanElement::getSourceRow)
                    .distinct()
                    .collect(Collectors.toList());
            //если практика одна - выводим ее название
            if (practices.size() > 0)
                _uiSettings.set(PRACTICE_TITLE, practices.get(0).getDisplayableTitle());
            else
                _uiSettings.set(PRACTICE_TITLE, null);
        }

    }

    private void fillListRepresent(ListRepresentPractice listRepresent)
    {
        listRepresent.setPracticeTitle(getSettings().get(PRACTICE_TITLE, String.class));
        listRepresent.setDateBeginningPractice(getSettings().get(DATE_BEGINNING_PRACTICE, Date.class));
        listRepresent.setDateEndOfPractice(getSettings().get(DATE_END_OF_PRACTICE, Date.class));
        listRepresent.setPaymentData(getSettings().get(PAYMENT_DATA, PracticePaymentData.class));
        listRepresent.setRegistryElementRow(getSettings().get(PRACTICE, EppWorkPlanRow.class));
        listRepresent.setSupervisor(getSettings().get(SUPERVISOR));
    }

    public RepresentationType getTypeRepresentFilter()
    {
        return typeRepresentFilter;
    }

    public void setTypeRepresentFilter(RepresentationType typeRepresentFilter)
    {
        this.typeRepresentFilter = typeRepresentFilter;
    }

    public RepresentationReason getReasonRepresentFilter()
    {
        return reasonRepresentFilter;
    }

    public void setReasonRepresentFilter(RepresentationReason reasonRepresentFilter)
    {
        this.reasonRepresentFilter = reasonRepresentFilter;
    }

    public RepresentationBasement getBasementRepresentFilter()
    {
        return basementRepresentFilter;
    }

    public void setBasementRepresentFilter(RepresentationBasement basementRepresentFilter)
    {
        this.basementRepresentFilter = basementRepresentFilter;
    }

    public Long getListRepresentId()
    {
        return listRepresentId;
    }

    public void setListRepresentId(Long listRepresentId)
    {
        this.listRepresentId = listRepresentId;
    }

    public Date getRepresentationBasementDate()
    {
        return _representationBasementDate;
    }

    public void setRepresentationBasementDate(Date representationBasementDate)
    {
        _representationBasementDate = representationBasementDate;
    }

    public String getRepresentationBasementNumber()
    {
        return _representationBasementNumber;
    }

    public void setRepresentationBasementNumber(String representationBasementNumber)
    {
        _representationBasementNumber = representationBasementNumber;
    }

    public ISelectModel getPracticeModel()
    {
        return practiceModel;
    }

    public void setPracticeModel(ISelectModel practiceModel)
    {
        this.practiceModel = practiceModel;
    }

    public ISelectModel getSupervisorModel()
    {
        return _supervisorModel;
    }

    public void setSupervisorModel(ISelectModel supervisorModel)
    {
        this._supervisorModel = supervisorModel;
    }
}
