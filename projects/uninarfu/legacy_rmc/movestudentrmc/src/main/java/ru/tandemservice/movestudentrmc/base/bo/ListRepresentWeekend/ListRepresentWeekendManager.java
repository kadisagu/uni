package ru.tandemservice.movestudentrmc.base.bo.ListRepresentWeekend;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentWeekend.logic.ListRepresentWeekendManagerModifyDAO;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentWeekend.ui.Edit.ListRepresentWeekendEdit;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentWeekend.ui.View.ListRepresentWeekendView;
import ru.tandemservice.movestudentrmc.docord.IListObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IListObjectModifyDAO;

@Configuration
public class ListRepresentWeekendManager extends BusinessObjectManager implements IListObjectByRepresentTypeManager {

    public static ListRepresentWeekendManager instance() {
        return instance(ListRepresentWeekendManager.class);
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return ListRepresentWeekendEdit.class;
    }

    @Override
    @Bean
    public IListObjectModifyDAO getListObjectModifyDAO() {
        return new ListRepresentWeekendManagerModifyDAO();
    }

    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return ListRepresentWeekendView.class;
    }

}
