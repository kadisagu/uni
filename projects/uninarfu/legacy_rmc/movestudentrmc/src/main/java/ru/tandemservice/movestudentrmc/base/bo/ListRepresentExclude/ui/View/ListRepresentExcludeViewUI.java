package ru.tandemservice.movestudentrmc.base.bo.ListRepresentExclude.ui.View;

import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.support.AbstractListRepresentViewUI;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentExclude.logic.ListPrintDoc;
import ru.tandemservice.movestudentrmc.entity.ListRepresentExclude;

import java.util.Arrays;

public class ListRepresentExcludeViewUI extends AbstractListRepresentViewUI<ListRepresentExclude>
{

    @Override
    public ListRepresentExclude getListRepresentObject() {
        return new ListRepresentExclude();
    }

    @Override
    protected RtfDocument printRepresent() {
        return new ListPrintDoc().creationPrintDocOrder(Arrays.asList(getListRepresent()));
    }

}
