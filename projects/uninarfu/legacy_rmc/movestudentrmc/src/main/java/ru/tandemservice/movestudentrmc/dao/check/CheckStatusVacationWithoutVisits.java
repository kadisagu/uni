package ru.tandemservice.movestudentrmc.dao.check;

public class CheckStatusVacationWithoutVisits extends AbstractCheckStudentStatus {

    /**
     * Статус студента - отпуск без посещения
     */
    private final static String STUDENT_STATUS_VACATION_WITHOUT_VISITS = "6";

    @Override
    protected String getStatusCode() {
        return STUDENT_STATUS_VACATION_WITHOUT_VISITS;
    }

}
