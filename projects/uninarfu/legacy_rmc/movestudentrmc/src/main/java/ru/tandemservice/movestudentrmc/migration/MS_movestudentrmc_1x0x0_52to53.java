package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_movestudentrmc_1x0x0_52to53 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.13"),
                        new ScriptDependency("org.tandemframework.shared", "1.3.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.3.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность grantType

        // создано обязательное свойство academ
        {
            // создать колонку
            tool.createColumn("granttype_t", new DBColumn("academ_p", DBType.BOOLEAN));

            // TODO: программист должен подтвердить это действие
            if (false)
                throw new UnsupportedOperationException("Confirm me: set default value for required property");

            // задать значение по умолчанию
            java.lang.Boolean defaultAcadem = false;        // TODO: правильно?
            tool.executeUpdate("update granttype_t set academ_p=? where academ_p is null", defaultAcadem);

            // сделать колонку NOT NULL
            tool.setColumnNullable("granttype_t", "academ_p", false);

        }

        // создано обязательное свойство social
        {
            // создать колонку
            tool.createColumn("granttype_t", new DBColumn("social_p", DBType.BOOLEAN));

            // TODO: программист должен подтвердить это действие
            if (false)
                throw new UnsupportedOperationException("Confirm me: set default value for required property");

            // задать значение по умолчанию
            java.lang.Boolean defaultSocial = false;        // TODO: правильно?
            tool.executeUpdate("update granttype_t set social_p=? where social_p is null", defaultSocial);

            // сделать колонку NOT NULL
            tool.setColumnNullable("granttype_t", "social_p", false);

        }


    }
}