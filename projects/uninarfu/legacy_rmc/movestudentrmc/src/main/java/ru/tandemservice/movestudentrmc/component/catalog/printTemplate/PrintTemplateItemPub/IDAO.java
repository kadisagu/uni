package ru.tandemservice.movestudentrmc.component.catalog.printTemplate.PrintTemplateItemPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultPrintCatalogItemPub.IDefaultPrintCatalogItemPubDAO;
import ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate;

/**
 * @author AutoGenerator
 *         Created on 07.12.2011
 */
public interface IDAO extends IDefaultPrintCatalogItemPubDAO<PrintTemplate, Model> {
}