package ru.tandemservice.movestudentrmc.base.bo.ListRepresentQualificationAdmission.ui.View;


import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.support.AbstractListRepresentViewUI;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentQualificationAdmission.logic.ListPrintDoc;
import ru.tandemservice.movestudentrmc.entity.ListRepresentQualificationAdmission;

import java.util.Collections;

public class ListRepresentQualificationAdmissionViewUI extends AbstractListRepresentViewUI<ListRepresentQualificationAdmission> {

    @Override
    public ListRepresentQualificationAdmission getListRepresentObject() {
        return new ListRepresentQualificationAdmission();
    }

    @Override
    protected RtfDocument printRepresent() {
        return new ListPrintDoc().creationPrintDocOrder(Collections.singletonList(getListRepresent()));
    }

}
