package ru.tandemservice.movestudentrmc.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Причины представлений
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RepresentationReasonGen extends EntityBase
 implements INaturalIdentifiable<RepresentationReasonGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason";
    public static final String ENTITY_NAME = "representationReason";
    public static final int VERSION_HASH = -1047474895;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_TITLE = "title";
    public static final String P_PRIORITY = "priority";
    public static final String P_PRINTABLE = "printable";
    public static final String P_USED = "used";

    private String _code;     // Системный код
    private String _title; 
    private int _priority; 
    private boolean _printable; 
    private boolean _used; 

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return  Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title  Свойство не может быть null и должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return  Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority  Свойство не может быть null и должно быть уникальным.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public boolean isPrintable()
    {
        return _printable;
    }

    /**
     * @param printable  Свойство не может быть null.
     */
    public void setPrintable(boolean printable)
    {
        dirty(_printable, printable);
        _printable = printable;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public boolean isUsed()
    {
        return _used;
    }

    /**
     * @param used  Свойство не может быть null.
     */
    public void setUsed(boolean used)
    {
        dirty(_used, used);
        _used = used;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RepresentationReasonGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((RepresentationReason)another).getCode());
            }
            setTitle(((RepresentationReason)another).getTitle());
            setPriority(((RepresentationReason)another).getPriority());
            setPrintable(((RepresentationReason)another).isPrintable());
            setUsed(((RepresentationReason)another).isUsed());
        }
    }

    public INaturalId<RepresentationReasonGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<RepresentationReasonGen>
    {
        private static final String PROXY_NAME = "RepresentationReasonNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof RepresentationReasonGen.NaturalId) ) return false;

            RepresentationReasonGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RepresentationReasonGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RepresentationReason.class;
        }

        public T newInstance()
        {
            return (T) new RepresentationReason();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "title":
                    return obj.getTitle();
                case "priority":
                    return obj.getPriority();
                case "printable":
                    return obj.isPrintable();
                case "used":
                    return obj.isUsed();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
                case "printable":
                    obj.setPrintable((Boolean) value);
                    return;
                case "used":
                    obj.setUsed((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "title":
                        return true;
                case "priority":
                        return true;
                case "printable":
                        return true;
                case "used":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "title":
                    return true;
                case "priority":
                    return true;
                case "printable":
                    return true;
                case "used":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "title":
                    return String.class;
                case "priority":
                    return Integer.class;
                case "printable":
                    return Boolean.class;
                case "used":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RepresentationReason> _dslPath = new Path<RepresentationReason>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RepresentationReason");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return  Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return  Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason#isPrintable()
     */
    public static PropertyPath<Boolean> printable()
    {
        return _dslPath.printable();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason#isUsed()
     */
    public static PropertyPath<Boolean> used()
    {
        return _dslPath.used();
    }

    public static class Path<E extends RepresentationReason> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _title;
        private PropertyPath<Integer> _priority;
        private PropertyPath<Boolean> _printable;
        private PropertyPath<Boolean> _used;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(RepresentationReasonGen.P_CODE, this);
            return _code;
        }

    /**
     * @return  Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(RepresentationReasonGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return  Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(RepresentationReasonGen.P_PRIORITY, this);
            return _priority;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason#isPrintable()
     */
        public PropertyPath<Boolean> printable()
        {
            if(_printable == null )
                _printable = new PropertyPath<Boolean>(RepresentationReasonGen.P_PRINTABLE, this);
            return _printable;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason#isUsed()
     */
        public PropertyPath<Boolean> used()
        {
            if(_used == null )
                _used = new PropertyPath<Boolean>(RepresentationReasonGen.P_USED, this);
            return _used;
        }

        public Class getEntityClass()
        {
            return RepresentationReason.class;
        }

        public String getEntityName()
        {
            return "representationReason";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
