package ru.tandemservice.movestudentrmc.base.bo.GrantsRelationSettings.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import ru.tandemservice.movestudentrmc.base.bo.GrantsRelationSettings.logic.GrantTypeDSHandler;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantType;

@Configuration
public class GrantsRelationSettingsList extends BusinessComponentManager {

    public static final String TYPE_DS = "grantTypeDS";

    @Bean
    public ColumnListExtPoint grantTypeDS()
    {
        return columnListExtPointBuilder(TYPE_DS)
                .addColumn(textColumn(GrantTypeDSHandler.TYPE_COLUMN, GrantType.title()).order().create())
                .addColumn(booleanColumn(GrantTypeDSHandler.ISCONFIG_COLUMN, GrantType.configRelation()).create())
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).create())
                .create();
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(TYPE_DS, grantTypeDS()).handler(grantTypeDSHandler()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> grantTypeDSHandler()
    {
        return new GrantTypeDSHandler(getName());
    }
}
