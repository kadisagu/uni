package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.IRepresentOrder;
import ru.tandemservice.movestudentrmc.entity.ListOrdListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListOrder;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность-связь 'Списочный Приказ'-'Списочное Представление'
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ListOrdListRepresentGen extends EntityBase
 implements IRepresentOrder{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.ListOrdListRepresent";
    public static final String ENTITY_NAME = "listOrdListRepresent";
    public static final int VERSION_HASH = 2115033581;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORDER = "order";
    public static final String L_REPRESENTATION = "representation";

    private ListOrder _order;     // Списочный Приказ
    private ListRepresent _representation;     // Списочное Представление

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Списочный Приказ. Свойство не может быть null.
     */
    @NotNull
    public ListOrder getOrder()
    {
        return _order;
    }

    /**
     * @param order Списочный Приказ. Свойство не может быть null.
     */
    public void setOrder(ListOrder order)
    {
        dirty(_order, order);
        _order = order;
    }

    /**
     * @return Списочное Представление. Свойство не может быть null.
     */
    @NotNull
    public ListRepresent getRepresentation()
    {
        return _representation;
    }

    /**
     * @param representation Списочное Представление. Свойство не может быть null.
     */
    public void setRepresentation(ListRepresent representation)
    {
        dirty(_representation, representation);
        _representation = representation;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ListOrdListRepresentGen)
        {
            setOrder(((ListOrdListRepresent)another).getOrder());
            setRepresentation(((ListOrdListRepresent)another).getRepresentation());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ListOrdListRepresentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ListOrdListRepresent.class;
        }

        public T newInstance()
        {
            return (T) new ListOrdListRepresent();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "order":
                    return obj.getOrder();
                case "representation":
                    return obj.getRepresentation();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "order":
                    obj.setOrder((ListOrder) value);
                    return;
                case "representation":
                    obj.setRepresentation((ListRepresent) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "order":
                        return true;
                case "representation":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "order":
                    return true;
                case "representation":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "order":
                    return ListOrder.class;
                case "representation":
                    return ListRepresent.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ListOrdListRepresent> _dslPath = new Path<ListOrdListRepresent>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ListOrdListRepresent");
    }
            

    /**
     * @return Списочный Приказ. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListOrdListRepresent#getOrder()
     */
    public static ListOrder.Path<ListOrder> order()
    {
        return _dslPath.order();
    }

    /**
     * @return Списочное Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListOrdListRepresent#getRepresentation()
     */
    public static ListRepresent.Path<ListRepresent> representation()
    {
        return _dslPath.representation();
    }

    public static class Path<E extends ListOrdListRepresent> extends EntityPath<E>
    {
        private ListOrder.Path<ListOrder> _order;
        private ListRepresent.Path<ListRepresent> _representation;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Списочный Приказ. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListOrdListRepresent#getOrder()
     */
        public ListOrder.Path<ListOrder> order()
        {
            if(_order == null )
                _order = new ListOrder.Path<ListOrder>(L_ORDER, this);
            return _order;
        }

    /**
     * @return Списочное Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListOrdListRepresent#getRepresentation()
     */
        public ListRepresent.Path<ListRepresent> representation()
        {
            if(_representation == null )
                _representation = new ListRepresent.Path<ListRepresent>(L_REPRESENTATION, this);
            return _representation;
        }

        public Class getEntityClass()
        {
            return ListOrdListRepresent.class;
        }

        public String getEntityName()
        {
            return "listOrdListRepresent";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
