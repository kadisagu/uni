package ru.tandemservice.movestudentrmc.base.bo.DORepresentTravel;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentTravel.logic.DORepresentTravelDAO;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentTravel.ui.Edit.DORepresentTravelEdit;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentTravel.ui.View.DORepresentTravelView;
import ru.tandemservice.movestudentrmc.docord.IDOObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IDOObjectModifyDAO;

@Configuration
public class DORepresentTravelManager extends BusinessObjectManager implements IDOObjectByRepresentTypeManager {

    public static DORepresentTravelManager instance()
    {
        return instance(DORepresentTravelManager.class);
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return DORepresentTravelEdit.class;
    }

    @Bean
    public IDOObjectModifyDAO getDOObjectModifyDAO() {
        return new DORepresentTravelDAO();
    }

    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return DORepresentTravelView.class;
    }
}
