package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import ru.tandemservice.movestudentrmc.entity.ListOrdListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListOrder;
import ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentationTypeCodes;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

public class QualificationThemeOrderDSHandler extends DefaultComboDataSourceHandler {
    public static final String STUDENT_ID = "studentId";

    public QualificationThemeOrderDSHandler(String ownerId) {

        super(ownerId, ListOrder.class, ListOrder.number());
    }

    /*
    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep) {
        String filter = ep.input.getComboFilterByValue();
        Long studentId = ep.context.get(STUDENT_ID);

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(RelListRepresentStudents.class, "r").joinEntity("r", DQLJoinType.inner, ListOrdListRepresent.class, "o", DQLExpressions.eq(DQLExpressions.property(ListOrdListRepresent.representation().fromAlias("o")), DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("r"))));
        dql.where(DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudents.student().id().fromAlias("r")), DQLExpressions.value(studentId)));
        dql.where(DQLExpressions.eq(DQLExpressions.property(ListOrdListRepresent.order().state().code().fromAlias("o")), DQLExpressions.value("5")));
        dql.where(DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudents.representation().type().code().fromAlias("r")), DQLExpressions.value(RepresentationTypeCodes.QUALIFICATION_THEMES_LIST)));
        dql.column(DQLExpressions.property(ListOrdListRepresent.order().id().fromAlias("o")));


        ep.dqlBuilder.where(in(DQLExpressions.property(ListOrder.id().fromAlias("e")), dql.buildQuery()));

//        if (StringUtils.isNotBlank(filter))
//        {
//            ep.dqlBuilder.where(like(DQLFunctions.upper(property(RepresentationReason.priority().fromAlias("e"))), value(CoreStringUtils.escapeLike(filter, true))));
//        }
    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep) {
        //ep.dqlBuilder.order("e." + RepresentationReason.priority());
    }
    */
    
    //------------------------------------------------------------------------------------------------------
    @Override
    protected ExecutionParameters<DSInput, DSOutput> prepareExecutionParameters(DSInput input, ExecutionContext context) {
		Class entityClass = this.getEntityClass(context);
		DSOutput output = new DSOutput(input);
		DQLSelectBuilder builder = new DQLSelectBuilder();
		builder.column(DQLExpressions.property("e"));
		builder.fromEntity(entityClass.getName(), "e");
		
		Long studentId = context.get(STUDENT_ID);
		
        DQLSelectBuilder dql = new DQLSelectBuilder()
        		.fromEntity(RelListRepresentStudents.class, "r")
        		.joinEntity("r", DQLJoinType.inner, ListOrdListRepresent.class, "o", eq(property(ListOrdListRepresent.representation().fromAlias("o")), property(RelListRepresentStudents.representation().fromAlias("r"))))
        		.where(eq(property(RelListRepresentStudents.student().id().fromAlias("r")), value(studentId)))
        		.where(eq(property(ListOrdListRepresent.order().state().code().fromAlias("o")), value("5")))
        		.where(eq(property(RelListRepresentStudents.representation().type().code().fromAlias("r")), value(RepresentationTypeCodes.QUALIFICATION_THEMES_LIST)))
        		.column(property(ListOrdListRepresent.order().id().fromAlias("o")))
        ;
        
        builder.where(in(DQLExpressions.property(ListOrder.id().fromAlias("e")), dql.buildQuery()));
		
		return new ExecutionParameters(input, output, context, builder);
    }
    //------------------------------------------------------------------------------------------------------
    
}
