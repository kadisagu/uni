package ru.tandemservice.movestudentrmc.component.catalog.osspGrantsView.OsspGrantsViewPub;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubModel;
import ru.tandemservice.movestudentrmc.entity.catalog.OsspGrantsView;

public class Model extends DefaultCatalogPubModel<OsspGrantsView> {
    public static class OsspGrantsWrapper extends
            IdentifiableWrapper<OsspGrantsView>
    {
        private static final long serialVersionUID = -4301478784488247008L;
        public static final String PRINTABLE = "printable";

        private OsspGrantsView osspGrants;
        private boolean printable;

        public OsspGrantsWrapper(
                OsspGrantsView osspGrants,
                boolean printable)
        {
            super(osspGrants.getId(), osspGrants
                    .getTitle());
            this.osspGrants = osspGrants;
            this.printable = printable;
        }

        public OsspGrantsView getOsspGrants() {
            return osspGrants;
        }

        public boolean isPrintable() {
            return printable;
        }

        public void setPrintable(boolean printable) {
            this.printable = printable;
        }
    }
}
