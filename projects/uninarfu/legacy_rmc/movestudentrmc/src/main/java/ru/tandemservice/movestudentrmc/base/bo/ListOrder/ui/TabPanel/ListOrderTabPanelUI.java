package ru.tandemservice.movestudentrmc.base.bo.ListOrder.ui.TabPanel;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = ListOrderTabPanelUI.STUDENT_ID)
})
public class ListOrderTabPanelUI extends UIPresenter {

    public static final String STUDENT_ID = "studentId";

    private String _selectedTab;
    private Map<String, Object> _paramsRepresentBaseTab;
    private Student _student;
    private Long _studentId;

    @Override
    public void onComponentRefresh()
    {

        if (ContextLocal.getUserContext().getPrincipalContext() instanceof EmployeePost) {
            EmployeePost post = (EmployeePost) ContextLocal.getUserContext().getPrincipalContext();
            post.getOrgUnit().getTitle();
        }

        List<Long> studentListId = new ArrayList<Long>();
        studentListId.add(_studentId);

        _paramsRepresentBaseTab = new HashMap<String, Object>();
        //_paramsRepresentBaseTab.put(ListRepresentBaseListUI.STUDENT_LIST_ID, studentListId);
    }

    //Getters

    public Map<String, Object> getParamsRepresentBaseTab() {
        return _paramsRepresentBaseTab;
    }

    public Long getStudentId() {
        return _studentId;
    }

    public void setStudentId(Long studentId) {
        _studentId = studentId;
    }

//Getters and Setters

    public String getSelectedTab() {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab) {
        _selectedTab = selectedTab;
    }

}
