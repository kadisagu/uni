package ru.tandemservice.movestudentrmc.base.bo.DOOrder.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.movestudentrmc.entity.DocumentOrder;

import java.util.List;

public interface IDOOrderManagerModifyDAO extends INeedPersistenceSupport {

    void save(DocumentOrder order, List<DataWrapper> representSelectedList);

    void update(DocumentOrder order, List<DataWrapper> representSelectedList);

    List<DataWrapper> selectRepresent(DocumentOrder order);

    void updateState(DocumentOrder order);

    void doCommit(DocumentOrder order, ErrorCollector error);

    void doRollback(DocumentOrder order, ErrorCollector error);

    void saveDocOrder(DocumentOrder order, RtfDocument docMain);

    public void saveExtractsText(DocumentOrder order);

    public void deleteExtractsText(DocumentOrder order);
}
