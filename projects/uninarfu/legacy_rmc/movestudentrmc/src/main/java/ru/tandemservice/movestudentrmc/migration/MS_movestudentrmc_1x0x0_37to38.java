package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;

public class MS_movestudentrmc_1x0x0_37to38 extends IndependentMigrationScript {

    @Override
    public void run(DBTool tool) throws Exception {
        /*
		 * Переименование названия типа представления RM#3234
		 */
        if (tool.tableExists("REPRESENTATIONTYPE_T")) {
            tool.executeUpdate("update REPRESENTATIONTYPE_T set TITLE_P='О выходе из академического отпуска' where CODE_p='weekendOut'");
            tool.executeUpdate("update REPRESENTATIONTYPE_T set TITLE_P='О продлении академического отпуска' where CODE_p='weekendProlong'");
        }
    }

}
