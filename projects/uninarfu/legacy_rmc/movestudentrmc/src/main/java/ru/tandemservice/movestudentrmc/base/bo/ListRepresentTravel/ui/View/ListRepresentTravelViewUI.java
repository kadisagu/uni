package ru.tandemservice.movestudentrmc.base.bo.ListRepresentTravel.ui.View;

import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.support.AbstractListRepresentViewUI;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentTravel.logic.ListPrintDoc;
import ru.tandemservice.movestudentrmc.entity.ListRepresentTravel;

import java.util.Arrays;

public class ListRepresentTravelViewUI extends AbstractListRepresentViewUI<ListRepresentTravel>
{

    @Override
    public ListRepresentTravel getListRepresentObject() {
        return new ListRepresentTravel();
    }

    @Override
    protected RtfDocument printRepresent() {
        return new ListPrintDoc().creationPrintDocOrder(Arrays.asList(getListRepresent()));
    }

}
