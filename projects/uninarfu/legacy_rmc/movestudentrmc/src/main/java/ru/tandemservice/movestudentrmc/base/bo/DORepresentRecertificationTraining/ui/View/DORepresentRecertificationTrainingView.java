package ru.tandemservice.movestudentrmc.base.bo.DORepresentRecertificationTraining.ui.View;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.entity.DocRecertificationTrainingDiscipline;

@Configuration
public class DORepresentRecertificationTrainingView extends BusinessComponentManager {

    public static final String LIST_DS1 = "listDS1";
    public static final String LIST_DS2 = "listDS2";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(LIST_DS1, columnListHandler1(), searchListDSHandler1()))
                .addDataSource(searchListDS(LIST_DS2, columnListHandler2(), searchListDSHandler2()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> searchListDSHandler1() {
        return _searchListDSHandler(false);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> searchListDSHandler2() {
        return _searchListDSHandler(true);
    }

    @Bean
    public ColumnListExtPoint columnListHandler1() {
        return _columnListHandler(LIST_DS1);
    }

    @Bean
    public ColumnListExtPoint columnListHandler2() {
        return _columnListHandler(LIST_DS2);
    }

    private ColumnListExtPoint _columnListHandler(String name) {
        return columnListExtPointBuilder(name)
                .addColumn(textColumn("discipline", DocRecertificationTrainingDiscipline.sessionTransferOutsideOperation().targetMark().slot().studentWpeCAction().studentWpe().sourceRow().displayableTitle().s()))
                .addColumn(textColumn("mark", DocRecertificationTrainingDiscipline.sessionTransferOutsideOperation().targetMark().valueTitle().s()))
                .create();
    }

    private IBusinessHandler<DSInput, DSOutput> _searchListDSHandler(final boolean flag) {
        return new DefaultSearchDataSourceHandler(getName()) {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context) {
                Long representId = context.get("representId");

                DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(DocRecertificationTrainingDiscipline.class, "e")
                        .where(DQLExpressions.eq(
                                DQLExpressions.property(DocRecertificationTrainingDiscipline.representation().id().fromAlias("e")),
                                DQLExpressions.value(representId)
                        ))
                        .where(DQLExpressions.eq(
                                DQLExpressions.property(DocRecertificationTrainingDiscipline.recertification().fromAlias("e")),
                                DQLExpressions.value(flag)
                        ));

                return DQLSelectOutputBuilder.get(input, dql, context.getSession()).build();
            }

            ;
        };
    }
}
