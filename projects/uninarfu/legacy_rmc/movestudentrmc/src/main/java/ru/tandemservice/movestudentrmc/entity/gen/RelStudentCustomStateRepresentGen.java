package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.RelStudentCustomStateRepresent;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность-связь 'Доп. статус студента'-'Представление'
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RelStudentCustomStateRepresentGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.RelStudentCustomStateRepresent";
    public static final String ENTITY_NAME = "relStudentCustomStateRepresent";
    public static final int VERSION_HASH = -773730008;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT_CUSTOM_STATE = "studentCustomState";
    public static final String L_LIST_REPRESENT = "listRepresent";
    public static final String L_REPRESENTATION = "representation";

    private StudentCustomState _studentCustomState;     // Доп. статус студента
    private ListRepresent _listRepresent;     // Списочное представление, которое назначает этот статус
    private Representation _representation;     // Индивидуальное представление, которое назначает этот статус

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Доп. статус студента. Свойство не может быть null.
     */
    @NotNull
    public StudentCustomState getStudentCustomState()
    {
        return _studentCustomState;
    }

    /**
     * @param studentCustomState Доп. статус студента. Свойство не может быть null.
     */
    public void setStudentCustomState(StudentCustomState studentCustomState)
    {
        dirty(_studentCustomState, studentCustomState);
        _studentCustomState = studentCustomState;
    }

    /**
     * @return Списочное представление, которое назначает этот статус.
     */
    public ListRepresent getListRepresent()
    {
        return _listRepresent;
    }

    /**
     * @param listRepresent Списочное представление, которое назначает этот статус.
     */
    public void setListRepresent(ListRepresent listRepresent)
    {
        dirty(_listRepresent, listRepresent);
        _listRepresent = listRepresent;
    }

    /**
     * @return Индивидуальное представление, которое назначает этот статус.
     */
    public Representation getRepresentation()
    {
        return _representation;
    }

    /**
     * @param representation Индивидуальное представление, которое назначает этот статус.
     */
    public void setRepresentation(Representation representation)
    {
        dirty(_representation, representation);
        _representation = representation;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RelStudentCustomStateRepresentGen)
        {
            setStudentCustomState(((RelStudentCustomStateRepresent)another).getStudentCustomState());
            setListRepresent(((RelStudentCustomStateRepresent)another).getListRepresent());
            setRepresentation(((RelStudentCustomStateRepresent)another).getRepresentation());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RelStudentCustomStateRepresentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RelStudentCustomStateRepresent.class;
        }

        public T newInstance()
        {
            return (T) new RelStudentCustomStateRepresent();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "studentCustomState":
                    return obj.getStudentCustomState();
                case "listRepresent":
                    return obj.getListRepresent();
                case "representation":
                    return obj.getRepresentation();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "studentCustomState":
                    obj.setStudentCustomState((StudentCustomState) value);
                    return;
                case "listRepresent":
                    obj.setListRepresent((ListRepresent) value);
                    return;
                case "representation":
                    obj.setRepresentation((Representation) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "studentCustomState":
                        return true;
                case "listRepresent":
                        return true;
                case "representation":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "studentCustomState":
                    return true;
                case "listRepresent":
                    return true;
                case "representation":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "studentCustomState":
                    return StudentCustomState.class;
                case "listRepresent":
                    return ListRepresent.class;
                case "representation":
                    return Representation.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RelStudentCustomStateRepresent> _dslPath = new Path<RelStudentCustomStateRepresent>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RelStudentCustomStateRepresent");
    }
            

    /**
     * @return Доп. статус студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelStudentCustomStateRepresent#getStudentCustomState()
     */
    public static StudentCustomState.Path<StudentCustomState> studentCustomState()
    {
        return _dslPath.studentCustomState();
    }

    /**
     * @return Списочное представление, которое назначает этот статус.
     * @see ru.tandemservice.movestudentrmc.entity.RelStudentCustomStateRepresent#getListRepresent()
     */
    public static ListRepresent.Path<ListRepresent> listRepresent()
    {
        return _dslPath.listRepresent();
    }

    /**
     * @return Индивидуальное представление, которое назначает этот статус.
     * @see ru.tandemservice.movestudentrmc.entity.RelStudentCustomStateRepresent#getRepresentation()
     */
    public static Representation.Path<Representation> representation()
    {
        return _dslPath.representation();
    }

    public static class Path<E extends RelStudentCustomStateRepresent> extends EntityPath<E>
    {
        private StudentCustomState.Path<StudentCustomState> _studentCustomState;
        private ListRepresent.Path<ListRepresent> _listRepresent;
        private Representation.Path<Representation> _representation;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Доп. статус студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelStudentCustomStateRepresent#getStudentCustomState()
     */
        public StudentCustomState.Path<StudentCustomState> studentCustomState()
        {
            if(_studentCustomState == null )
                _studentCustomState = new StudentCustomState.Path<StudentCustomState>(L_STUDENT_CUSTOM_STATE, this);
            return _studentCustomState;
        }

    /**
     * @return Списочное представление, которое назначает этот статус.
     * @see ru.tandemservice.movestudentrmc.entity.RelStudentCustomStateRepresent#getListRepresent()
     */
        public ListRepresent.Path<ListRepresent> listRepresent()
        {
            if(_listRepresent == null )
                _listRepresent = new ListRepresent.Path<ListRepresent>(L_LIST_REPRESENT, this);
            return _listRepresent;
        }

    /**
     * @return Индивидуальное представление, которое назначает этот статус.
     * @see ru.tandemservice.movestudentrmc.entity.RelStudentCustomStateRepresent#getRepresentation()
     */
        public Representation.Path<Representation> representation()
        {
            if(_representation == null )
                _representation = new Representation.Path<Representation>(L_REPRESENTATION, this);
            return _representation;
        }

        public Class getEntityClass()
        {
            return RelStudentCustomStateRepresent.class;
        }

        public String getEntityName()
        {
            return "relStudentCustomStateRepresent";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
