package ru.tandemservice.movestudentrmc.base.bo.RelationSettings;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.movestudentrmc.base.bo.RelationSettings.logic.IRelationSettingsModifyDAO;
import ru.tandemservice.movestudentrmc.base.bo.RelationSettings.logic.RelationSettingsModifyDAO;

@Configuration
public class DORelationSettingsManager extends BusinessObjectManager {
    public static DORelationSettingsManager instance()
    {
        return instance(DORelationSettingsManager.class);
    }

    @Bean
    public IRelationSettingsModifyDAO modifyDao()
    {
        return new RelationSettingsModifyDAO();
    }
}
