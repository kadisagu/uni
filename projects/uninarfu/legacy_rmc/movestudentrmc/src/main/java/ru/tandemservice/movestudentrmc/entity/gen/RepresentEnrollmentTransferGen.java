package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudentrmc.entity.IDocRepresentWithNewGroup;
import ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * О зачислении в порядке перевода
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RepresentEnrollmentTransferGen extends Representation
 implements IDocRepresentWithNewGroup{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer";
    public static final String ENTITY_NAME = "representEnrollmentTransfer";
    public static final int VERSION_HASH = 1885131747;
    private static IEntityMeta ENTITY_META;

    public static final String L_NEW_FORMATIVE_ORG_UNIT = "newFormativeOrgUnit";
    public static final String L_NEW_COURSE = "newCourse";
    public static final String L_NEW_GROUP = "newGroup";
    public static final String L_NEW_EDUCATION_LEVELS_HIGH_SCHOOL = "newEducationLevelsHighSchool";
    public static final String L_NEW_DEVELOP_FORM = "newDevelopForm";
    public static final String L_NEW_COMPENSATION_TYPE = "newCompensationType";
    public static final String L_EDUCATION_ORG_UNIT = "educationOrgUnit";
    public static final String P_LAST_NAME = "lastName";
    public static final String P_FIRST_NAME = "firstName";
    public static final String P_MIDDLE_NAME = "middleName";
    public static final String P_END_DATE = "endDate";
    public static final String P_FORMATIVE_ORG_UNIT_OLD_TEXT = "formativeOrgUnitOldText";
    public static final String L_FORMATIVE_ORG_UNIT_OLD = "formativeOrgUnitOld";
    public static final String L_COURSE_OLD = "courseOld";
    public static final String L_GROUP_OLD = "groupOld";
    public static final String L_EDUCATION_LEVELS_HIGH_SCHOOL_OLD = "educationLevelsHighSchoolOld";
    public static final String L_DEVELOP_FORM_OLD = "developFormOld";
    public static final String L_COMPENSATION_TYPE_OLD = "compensationTypeOld";
    public static final String L_EDUCATION_ORG_UNIT_OLD = "educationOrgUnitOld";
    public static final String P_LAST_NAME_OLD = "lastNameOld";
    public static final String P_FIRST_NAME_OLD = "firstNameOld";
    public static final String P_MIDDLE_NAME_OLD = "middleNameOld";
    public static final String P_ORDER_DATE_OLD = "orderDateOld";
    public static final String P_ORDER_NUMBER_OLD = "orderNumberOld";
    public static final String P_ORDER_DATE_ENR_OLD = "orderDateEnrOld";

    private OrgUnit _newFormativeOrgUnit;     // Новое формирующее подразделение
    private Course _newCourse;     // Новый курс
    private Group _newGroup;     // Новая группа
    private EducationLevelsHighSchool _newEducationLevelsHighSchool;     // Новое направление подготовки (специальность)
    private DevelopForm _newDevelopForm;     // Новая форма обучения
    private CompensationType _newCompensationType;     // Новый тип возмещения затрат
    private EducationOrgUnit _educationOrgUnit;     // Новое направление подготовки (специальность)
    private String _lastName;     // Фамилия
    private String _firstName;     // Имя
    private String _middleName;     // Отчество
    private Date _endDate;     // Дата окончания
    private String _formativeOrgUnitOldText;     // Перевод из
    private OrgUnit _formativeOrgUnitOld;     // Старое формирующее подразделение
    private Course _courseOld;     // Старый курс
    private Group _groupOld;     // Старая группа
    private EducationLevelsHighSchool _educationLevelsHighSchoolOld;     // Старое направление подготовки (специальность)
    private DevelopForm _developFormOld;     // Старая форма обучения
    private CompensationType _compensationTypeOld;     // Старый тип возмещения затрат
    private EducationOrgUnit _educationOrgUnitOld;     // Старое направление подготовки (специальность)
    private String _lastNameOld;     // Старая Фамилия
    private String _firstNameOld;     // Старое Имя
    private String _middleNameOld;     // Старое Отчество
    private Date _orderDateOld;     // Старая дата приказа
    private String _orderNumberOld;     // Старый номер приказа
    private Date _orderDateEnrOld;     // Старая дата зачисления

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Новое формирующее подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getNewFormativeOrgUnit()
    {
        return _newFormativeOrgUnit;
    }

    /**
     * @param newFormativeOrgUnit Новое формирующее подразделение. Свойство не может быть null.
     */
    public void setNewFormativeOrgUnit(OrgUnit newFormativeOrgUnit)
    {
        dirty(_newFormativeOrgUnit, newFormativeOrgUnit);
        _newFormativeOrgUnit = newFormativeOrgUnit;
    }

    /**
     * @return Новый курс.
     */
    public Course getNewCourse()
    {
        return _newCourse;
    }

    /**
     * @param newCourse Новый курс.
     */
    public void setNewCourse(Course newCourse)
    {
        dirty(_newCourse, newCourse);
        _newCourse = newCourse;
    }

    /**
     * @return Новая группа. Свойство не может быть null.
     */
    @NotNull
    public Group getNewGroup()
    {
        return _newGroup;
    }

    /**
     * @param newGroup Новая группа. Свойство не может быть null.
     */
    public void setNewGroup(Group newGroup)
    {
        dirty(_newGroup, newGroup);
        _newGroup = newGroup;
    }

    /**
     * @return Новое направление подготовки (специальность).
     */
    public EducationLevelsHighSchool getNewEducationLevelsHighSchool()
    {
        return _newEducationLevelsHighSchool;
    }

    /**
     * @param newEducationLevelsHighSchool Новое направление подготовки (специальность).
     */
    public void setNewEducationLevelsHighSchool(EducationLevelsHighSchool newEducationLevelsHighSchool)
    {
        dirty(_newEducationLevelsHighSchool, newEducationLevelsHighSchool);
        _newEducationLevelsHighSchool = newEducationLevelsHighSchool;
    }

    /**
     * @return Новая форма обучения.
     */
    public DevelopForm getNewDevelopForm()
    {
        return _newDevelopForm;
    }

    /**
     * @param newDevelopForm Новая форма обучения.
     */
    public void setNewDevelopForm(DevelopForm newDevelopForm)
    {
        dirty(_newDevelopForm, newDevelopForm);
        _newDevelopForm = newDevelopForm;
    }

    /**
     * @return Новый тип возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getNewCompensationType()
    {
        return _newCompensationType;
    }

    /**
     * @param newCompensationType Новый тип возмещения затрат. Свойство не может быть null.
     */
    public void setNewCompensationType(CompensationType newCompensationType)
    {
        dirty(_newCompensationType, newCompensationType);
        _newCompensationType = newCompensationType;
    }

    /**
     * @return Новое направление подготовки (специальность).
     */
    public EducationOrgUnit getEducationOrgUnit()
    {
        return _educationOrgUnit;
    }

    /**
     * @param educationOrgUnit Новое направление подготовки (специальность).
     */
    public void setEducationOrgUnit(EducationOrgUnit educationOrgUnit)
    {
        dirty(_educationOrgUnit, educationOrgUnit);
        _educationOrgUnit = educationOrgUnit;
    }

    /**
     * @return Фамилия.
     */
    @Length(max=255)
    public String getLastName()
    {
        return _lastName;
    }

    /**
     * @param lastName Фамилия.
     */
    public void setLastName(String lastName)
    {
        dirty(_lastName, lastName);
        _lastName = lastName;
    }

    /**
     * @return Имя.
     */
    @Length(max=255)
    public String getFirstName()
    {
        return _firstName;
    }

    /**
     * @param firstName Имя.
     */
    public void setFirstName(String firstName)
    {
        dirty(_firstName, firstName);
        _firstName = firstName;
    }

    /**
     * @return Отчество.
     */
    @Length(max=255)
    public String getMiddleName()
    {
        return _middleName;
    }

    /**
     * @param middleName Отчество.
     */
    public void setMiddleName(String middleName)
    {
        dirty(_middleName, middleName);
        _middleName = middleName;
    }

    /**
     * @return Дата окончания.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Перевод из.
     */
    @Length(max=255)
    public String getFormativeOrgUnitOldText()
    {
        return _formativeOrgUnitOldText;
    }

    /**
     * @param formativeOrgUnitOldText Перевод из.
     */
    public void setFormativeOrgUnitOldText(String formativeOrgUnitOldText)
    {
        dirty(_formativeOrgUnitOldText, formativeOrgUnitOldText);
        _formativeOrgUnitOldText = formativeOrgUnitOldText;
    }

    /**
     * @return Старое формирующее подразделение.
     */
    public OrgUnit getFormativeOrgUnitOld()
    {
        return _formativeOrgUnitOld;
    }

    /**
     * @param formativeOrgUnitOld Старое формирующее подразделение.
     */
    public void setFormativeOrgUnitOld(OrgUnit formativeOrgUnitOld)
    {
        dirty(_formativeOrgUnitOld, formativeOrgUnitOld);
        _formativeOrgUnitOld = formativeOrgUnitOld;
    }

    /**
     * @return Старый курс.
     */
    public Course getCourseOld()
    {
        return _courseOld;
    }

    /**
     * @param courseOld Старый курс.
     */
    public void setCourseOld(Course courseOld)
    {
        dirty(_courseOld, courseOld);
        _courseOld = courseOld;
    }

    /**
     * @return Старая группа.
     */
    public Group getGroupOld()
    {
        return _groupOld;
    }

    /**
     * @param groupOld Старая группа.
     */
    public void setGroupOld(Group groupOld)
    {
        dirty(_groupOld, groupOld);
        _groupOld = groupOld;
    }

    /**
     * @return Старое направление подготовки (специальность).
     */
    public EducationLevelsHighSchool getEducationLevelsHighSchoolOld()
    {
        return _educationLevelsHighSchoolOld;
    }

    /**
     * @param educationLevelsHighSchoolOld Старое направление подготовки (специальность).
     */
    public void setEducationLevelsHighSchoolOld(EducationLevelsHighSchool educationLevelsHighSchoolOld)
    {
        dirty(_educationLevelsHighSchoolOld, educationLevelsHighSchoolOld);
        _educationLevelsHighSchoolOld = educationLevelsHighSchoolOld;
    }

    /**
     * @return Старая форма обучения.
     */
    public DevelopForm getDevelopFormOld()
    {
        return _developFormOld;
    }

    /**
     * @param developFormOld Старая форма обучения.
     */
    public void setDevelopFormOld(DevelopForm developFormOld)
    {
        dirty(_developFormOld, developFormOld);
        _developFormOld = developFormOld;
    }

    /**
     * @return Старый тип возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationTypeOld()
    {
        return _compensationTypeOld;
    }

    /**
     * @param compensationTypeOld Старый тип возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationTypeOld(CompensationType compensationTypeOld)
    {
        dirty(_compensationTypeOld, compensationTypeOld);
        _compensationTypeOld = compensationTypeOld;
    }

    /**
     * @return Старое направление подготовки (специальность).
     */
    public EducationOrgUnit getEducationOrgUnitOld()
    {
        return _educationOrgUnitOld;
    }

    /**
     * @param educationOrgUnitOld Старое направление подготовки (специальность).
     */
    public void setEducationOrgUnitOld(EducationOrgUnit educationOrgUnitOld)
    {
        dirty(_educationOrgUnitOld, educationOrgUnitOld);
        _educationOrgUnitOld = educationOrgUnitOld;
    }

    /**
     * @return Старая Фамилия. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getLastNameOld()
    {
        return _lastNameOld;
    }

    /**
     * @param lastNameOld Старая Фамилия. Свойство не может быть null.
     */
    public void setLastNameOld(String lastNameOld)
    {
        dirty(_lastNameOld, lastNameOld);
        _lastNameOld = lastNameOld;
    }

    /**
     * @return Старое Имя. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFirstNameOld()
    {
        return _firstNameOld;
    }

    /**
     * @param firstNameOld Старое Имя. Свойство не может быть null.
     */
    public void setFirstNameOld(String firstNameOld)
    {
        dirty(_firstNameOld, firstNameOld);
        _firstNameOld = firstNameOld;
    }

    /**
     * @return Старое Отчество.
     */
    @Length(max=255)
    public String getMiddleNameOld()
    {
        return _middleNameOld;
    }

    /**
     * @param middleNameOld Старое Отчество.
     */
    public void setMiddleNameOld(String middleNameOld)
    {
        dirty(_middleNameOld, middleNameOld);
        _middleNameOld = middleNameOld;
    }

    /**
     * @return Старая дата приказа.
     */
    public Date getOrderDateOld()
    {
        return _orderDateOld;
    }

    /**
     * @param orderDateOld Старая дата приказа.
     */
    public void setOrderDateOld(Date orderDateOld)
    {
        dirty(_orderDateOld, orderDateOld);
        _orderDateOld = orderDateOld;
    }

    /**
     * @return Старый номер приказа.
     */
    @Length(max=255)
    public String getOrderNumberOld()
    {
        return _orderNumberOld;
    }

    /**
     * @param orderNumberOld Старый номер приказа.
     */
    public void setOrderNumberOld(String orderNumberOld)
    {
        dirty(_orderNumberOld, orderNumberOld);
        _orderNumberOld = orderNumberOld;
    }

    /**
     * @return Старая дата зачисления.
     */
    public Date getOrderDateEnrOld()
    {
        return _orderDateEnrOld;
    }

    /**
     * @param orderDateEnrOld Старая дата зачисления.
     */
    public void setOrderDateEnrOld(Date orderDateEnrOld)
    {
        dirty(_orderDateEnrOld, orderDateEnrOld);
        _orderDateEnrOld = orderDateEnrOld;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof RepresentEnrollmentTransferGen)
        {
            setNewFormativeOrgUnit(((RepresentEnrollmentTransfer)another).getNewFormativeOrgUnit());
            setNewCourse(((RepresentEnrollmentTransfer)another).getNewCourse());
            setNewGroup(((RepresentEnrollmentTransfer)another).getNewGroup());
            setNewEducationLevelsHighSchool(((RepresentEnrollmentTransfer)another).getNewEducationLevelsHighSchool());
            setNewDevelopForm(((RepresentEnrollmentTransfer)another).getNewDevelopForm());
            setNewCompensationType(((RepresentEnrollmentTransfer)another).getNewCompensationType());
            setEducationOrgUnit(((RepresentEnrollmentTransfer)another).getEducationOrgUnit());
            setLastName(((RepresentEnrollmentTransfer)another).getLastName());
            setFirstName(((RepresentEnrollmentTransfer)another).getFirstName());
            setMiddleName(((RepresentEnrollmentTransfer)another).getMiddleName());
            setEndDate(((RepresentEnrollmentTransfer)another).getEndDate());
            setFormativeOrgUnitOldText(((RepresentEnrollmentTransfer)another).getFormativeOrgUnitOldText());
            setFormativeOrgUnitOld(((RepresentEnrollmentTransfer)another).getFormativeOrgUnitOld());
            setCourseOld(((RepresentEnrollmentTransfer)another).getCourseOld());
            setGroupOld(((RepresentEnrollmentTransfer)another).getGroupOld());
            setEducationLevelsHighSchoolOld(((RepresentEnrollmentTransfer)another).getEducationLevelsHighSchoolOld());
            setDevelopFormOld(((RepresentEnrollmentTransfer)another).getDevelopFormOld());
            setCompensationTypeOld(((RepresentEnrollmentTransfer)another).getCompensationTypeOld());
            setEducationOrgUnitOld(((RepresentEnrollmentTransfer)another).getEducationOrgUnitOld());
            setLastNameOld(((RepresentEnrollmentTransfer)another).getLastNameOld());
            setFirstNameOld(((RepresentEnrollmentTransfer)another).getFirstNameOld());
            setMiddleNameOld(((RepresentEnrollmentTransfer)another).getMiddleNameOld());
            setOrderDateOld(((RepresentEnrollmentTransfer)another).getOrderDateOld());
            setOrderNumberOld(((RepresentEnrollmentTransfer)another).getOrderNumberOld());
            setOrderDateEnrOld(((RepresentEnrollmentTransfer)another).getOrderDateEnrOld());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RepresentEnrollmentTransferGen> extends Representation.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RepresentEnrollmentTransfer.class;
        }

        public T newInstance()
        {
            return (T) new RepresentEnrollmentTransfer();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "newFormativeOrgUnit":
                    return obj.getNewFormativeOrgUnit();
                case "newCourse":
                    return obj.getNewCourse();
                case "newGroup":
                    return obj.getNewGroup();
                case "newEducationLevelsHighSchool":
                    return obj.getNewEducationLevelsHighSchool();
                case "newDevelopForm":
                    return obj.getNewDevelopForm();
                case "newCompensationType":
                    return obj.getNewCompensationType();
                case "educationOrgUnit":
                    return obj.getEducationOrgUnit();
                case "lastName":
                    return obj.getLastName();
                case "firstName":
                    return obj.getFirstName();
                case "middleName":
                    return obj.getMiddleName();
                case "endDate":
                    return obj.getEndDate();
                case "formativeOrgUnitOldText":
                    return obj.getFormativeOrgUnitOldText();
                case "formativeOrgUnitOld":
                    return obj.getFormativeOrgUnitOld();
                case "courseOld":
                    return obj.getCourseOld();
                case "groupOld":
                    return obj.getGroupOld();
                case "educationLevelsHighSchoolOld":
                    return obj.getEducationLevelsHighSchoolOld();
                case "developFormOld":
                    return obj.getDevelopFormOld();
                case "compensationTypeOld":
                    return obj.getCompensationTypeOld();
                case "educationOrgUnitOld":
                    return obj.getEducationOrgUnitOld();
                case "lastNameOld":
                    return obj.getLastNameOld();
                case "firstNameOld":
                    return obj.getFirstNameOld();
                case "middleNameOld":
                    return obj.getMiddleNameOld();
                case "orderDateOld":
                    return obj.getOrderDateOld();
                case "orderNumberOld":
                    return obj.getOrderNumberOld();
                case "orderDateEnrOld":
                    return obj.getOrderDateEnrOld();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "newFormativeOrgUnit":
                    obj.setNewFormativeOrgUnit((OrgUnit) value);
                    return;
                case "newCourse":
                    obj.setNewCourse((Course) value);
                    return;
                case "newGroup":
                    obj.setNewGroup((Group) value);
                    return;
                case "newEducationLevelsHighSchool":
                    obj.setNewEducationLevelsHighSchool((EducationLevelsHighSchool) value);
                    return;
                case "newDevelopForm":
                    obj.setNewDevelopForm((DevelopForm) value);
                    return;
                case "newCompensationType":
                    obj.setNewCompensationType((CompensationType) value);
                    return;
                case "educationOrgUnit":
                    obj.setEducationOrgUnit((EducationOrgUnit) value);
                    return;
                case "lastName":
                    obj.setLastName((String) value);
                    return;
                case "firstName":
                    obj.setFirstName((String) value);
                    return;
                case "middleName":
                    obj.setMiddleName((String) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "formativeOrgUnitOldText":
                    obj.setFormativeOrgUnitOldText((String) value);
                    return;
                case "formativeOrgUnitOld":
                    obj.setFormativeOrgUnitOld((OrgUnit) value);
                    return;
                case "courseOld":
                    obj.setCourseOld((Course) value);
                    return;
                case "groupOld":
                    obj.setGroupOld((Group) value);
                    return;
                case "educationLevelsHighSchoolOld":
                    obj.setEducationLevelsHighSchoolOld((EducationLevelsHighSchool) value);
                    return;
                case "developFormOld":
                    obj.setDevelopFormOld((DevelopForm) value);
                    return;
                case "compensationTypeOld":
                    obj.setCompensationTypeOld((CompensationType) value);
                    return;
                case "educationOrgUnitOld":
                    obj.setEducationOrgUnitOld((EducationOrgUnit) value);
                    return;
                case "lastNameOld":
                    obj.setLastNameOld((String) value);
                    return;
                case "firstNameOld":
                    obj.setFirstNameOld((String) value);
                    return;
                case "middleNameOld":
                    obj.setMiddleNameOld((String) value);
                    return;
                case "orderDateOld":
                    obj.setOrderDateOld((Date) value);
                    return;
                case "orderNumberOld":
                    obj.setOrderNumberOld((String) value);
                    return;
                case "orderDateEnrOld":
                    obj.setOrderDateEnrOld((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "newFormativeOrgUnit":
                        return true;
                case "newCourse":
                        return true;
                case "newGroup":
                        return true;
                case "newEducationLevelsHighSchool":
                        return true;
                case "newDevelopForm":
                        return true;
                case "newCompensationType":
                        return true;
                case "educationOrgUnit":
                        return true;
                case "lastName":
                        return true;
                case "firstName":
                        return true;
                case "middleName":
                        return true;
                case "endDate":
                        return true;
                case "formativeOrgUnitOldText":
                        return true;
                case "formativeOrgUnitOld":
                        return true;
                case "courseOld":
                        return true;
                case "groupOld":
                        return true;
                case "educationLevelsHighSchoolOld":
                        return true;
                case "developFormOld":
                        return true;
                case "compensationTypeOld":
                        return true;
                case "educationOrgUnitOld":
                        return true;
                case "lastNameOld":
                        return true;
                case "firstNameOld":
                        return true;
                case "middleNameOld":
                        return true;
                case "orderDateOld":
                        return true;
                case "orderNumberOld":
                        return true;
                case "orderDateEnrOld":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "newFormativeOrgUnit":
                    return true;
                case "newCourse":
                    return true;
                case "newGroup":
                    return true;
                case "newEducationLevelsHighSchool":
                    return true;
                case "newDevelopForm":
                    return true;
                case "newCompensationType":
                    return true;
                case "educationOrgUnit":
                    return true;
                case "lastName":
                    return true;
                case "firstName":
                    return true;
                case "middleName":
                    return true;
                case "endDate":
                    return true;
                case "formativeOrgUnitOldText":
                    return true;
                case "formativeOrgUnitOld":
                    return true;
                case "courseOld":
                    return true;
                case "groupOld":
                    return true;
                case "educationLevelsHighSchoolOld":
                    return true;
                case "developFormOld":
                    return true;
                case "compensationTypeOld":
                    return true;
                case "educationOrgUnitOld":
                    return true;
                case "lastNameOld":
                    return true;
                case "firstNameOld":
                    return true;
                case "middleNameOld":
                    return true;
                case "orderDateOld":
                    return true;
                case "orderNumberOld":
                    return true;
                case "orderDateEnrOld":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "newFormativeOrgUnit":
                    return OrgUnit.class;
                case "newCourse":
                    return Course.class;
                case "newGroup":
                    return Group.class;
                case "newEducationLevelsHighSchool":
                    return EducationLevelsHighSchool.class;
                case "newDevelopForm":
                    return DevelopForm.class;
                case "newCompensationType":
                    return CompensationType.class;
                case "educationOrgUnit":
                    return EducationOrgUnit.class;
                case "lastName":
                    return String.class;
                case "firstName":
                    return String.class;
                case "middleName":
                    return String.class;
                case "endDate":
                    return Date.class;
                case "formativeOrgUnitOldText":
                    return String.class;
                case "formativeOrgUnitOld":
                    return OrgUnit.class;
                case "courseOld":
                    return Course.class;
                case "groupOld":
                    return Group.class;
                case "educationLevelsHighSchoolOld":
                    return EducationLevelsHighSchool.class;
                case "developFormOld":
                    return DevelopForm.class;
                case "compensationTypeOld":
                    return CompensationType.class;
                case "educationOrgUnitOld":
                    return EducationOrgUnit.class;
                case "lastNameOld":
                    return String.class;
                case "firstNameOld":
                    return String.class;
                case "middleNameOld":
                    return String.class;
                case "orderDateOld":
                    return Date.class;
                case "orderNumberOld":
                    return String.class;
                case "orderDateEnrOld":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RepresentEnrollmentTransfer> _dslPath = new Path<RepresentEnrollmentTransfer>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RepresentEnrollmentTransfer");
    }
            

    /**
     * @return Новое формирующее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getNewFormativeOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> newFormativeOrgUnit()
    {
        return _dslPath.newFormativeOrgUnit();
    }

    /**
     * @return Новый курс.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getNewCourse()
     */
    public static Course.Path<Course> newCourse()
    {
        return _dslPath.newCourse();
    }

    /**
     * @return Новая группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getNewGroup()
     */
    public static Group.Path<Group> newGroup()
    {
        return _dslPath.newGroup();
    }

    /**
     * @return Новое направление подготовки (специальность).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getNewEducationLevelsHighSchool()
     */
    public static EducationLevelsHighSchool.Path<EducationLevelsHighSchool> newEducationLevelsHighSchool()
    {
        return _dslPath.newEducationLevelsHighSchool();
    }

    /**
     * @return Новая форма обучения.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getNewDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> newDevelopForm()
    {
        return _dslPath.newDevelopForm();
    }

    /**
     * @return Новый тип возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getNewCompensationType()
     */
    public static CompensationType.Path<CompensationType> newCompensationType()
    {
        return _dslPath.newCompensationType();
    }

    /**
     * @return Новое направление подготовки (специальность).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getEducationOrgUnit()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
    {
        return _dslPath.educationOrgUnit();
    }

    /**
     * @return Фамилия.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getLastName()
     */
    public static PropertyPath<String> lastName()
    {
        return _dslPath.lastName();
    }

    /**
     * @return Имя.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getFirstName()
     */
    public static PropertyPath<String> firstName()
    {
        return _dslPath.firstName();
    }

    /**
     * @return Отчество.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getMiddleName()
     */
    public static PropertyPath<String> middleName()
    {
        return _dslPath.middleName();
    }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Перевод из.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getFormativeOrgUnitOldText()
     */
    public static PropertyPath<String> formativeOrgUnitOldText()
    {
        return _dslPath.formativeOrgUnitOldText();
    }

    /**
     * @return Старое формирующее подразделение.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getFormativeOrgUnitOld()
     */
    public static OrgUnit.Path<OrgUnit> formativeOrgUnitOld()
    {
        return _dslPath.formativeOrgUnitOld();
    }

    /**
     * @return Старый курс.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getCourseOld()
     */
    public static Course.Path<Course> courseOld()
    {
        return _dslPath.courseOld();
    }

    /**
     * @return Старая группа.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getGroupOld()
     */
    public static Group.Path<Group> groupOld()
    {
        return _dslPath.groupOld();
    }

    /**
     * @return Старое направление подготовки (специальность).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getEducationLevelsHighSchoolOld()
     */
    public static EducationLevelsHighSchool.Path<EducationLevelsHighSchool> educationLevelsHighSchoolOld()
    {
        return _dslPath.educationLevelsHighSchoolOld();
    }

    /**
     * @return Старая форма обучения.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getDevelopFormOld()
     */
    public static DevelopForm.Path<DevelopForm> developFormOld()
    {
        return _dslPath.developFormOld();
    }

    /**
     * @return Старый тип возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getCompensationTypeOld()
     */
    public static CompensationType.Path<CompensationType> compensationTypeOld()
    {
        return _dslPath.compensationTypeOld();
    }

    /**
     * @return Старое направление подготовки (специальность).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getEducationOrgUnitOld()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitOld()
    {
        return _dslPath.educationOrgUnitOld();
    }

    /**
     * @return Старая Фамилия. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getLastNameOld()
     */
    public static PropertyPath<String> lastNameOld()
    {
        return _dslPath.lastNameOld();
    }

    /**
     * @return Старое Имя. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getFirstNameOld()
     */
    public static PropertyPath<String> firstNameOld()
    {
        return _dslPath.firstNameOld();
    }

    /**
     * @return Старое Отчество.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getMiddleNameOld()
     */
    public static PropertyPath<String> middleNameOld()
    {
        return _dslPath.middleNameOld();
    }

    /**
     * @return Старая дата приказа.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getOrderDateOld()
     */
    public static PropertyPath<Date> orderDateOld()
    {
        return _dslPath.orderDateOld();
    }

    /**
     * @return Старый номер приказа.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getOrderNumberOld()
     */
    public static PropertyPath<String> orderNumberOld()
    {
        return _dslPath.orderNumberOld();
    }

    /**
     * @return Старая дата зачисления.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getOrderDateEnrOld()
     */
    public static PropertyPath<Date> orderDateEnrOld()
    {
        return _dslPath.orderDateEnrOld();
    }

    public static class Path<E extends RepresentEnrollmentTransfer> extends Representation.Path<E>
    {
        private OrgUnit.Path<OrgUnit> _newFormativeOrgUnit;
        private Course.Path<Course> _newCourse;
        private Group.Path<Group> _newGroup;
        private EducationLevelsHighSchool.Path<EducationLevelsHighSchool> _newEducationLevelsHighSchool;
        private DevelopForm.Path<DevelopForm> _newDevelopForm;
        private CompensationType.Path<CompensationType> _newCompensationType;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnit;
        private PropertyPath<String> _lastName;
        private PropertyPath<String> _firstName;
        private PropertyPath<String> _middleName;
        private PropertyPath<Date> _endDate;
        private PropertyPath<String> _formativeOrgUnitOldText;
        private OrgUnit.Path<OrgUnit> _formativeOrgUnitOld;
        private Course.Path<Course> _courseOld;
        private Group.Path<Group> _groupOld;
        private EducationLevelsHighSchool.Path<EducationLevelsHighSchool> _educationLevelsHighSchoolOld;
        private DevelopForm.Path<DevelopForm> _developFormOld;
        private CompensationType.Path<CompensationType> _compensationTypeOld;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnitOld;
        private PropertyPath<String> _lastNameOld;
        private PropertyPath<String> _firstNameOld;
        private PropertyPath<String> _middleNameOld;
        private PropertyPath<Date> _orderDateOld;
        private PropertyPath<String> _orderNumberOld;
        private PropertyPath<Date> _orderDateEnrOld;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Новое формирующее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getNewFormativeOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> newFormativeOrgUnit()
        {
            if(_newFormativeOrgUnit == null )
                _newFormativeOrgUnit = new OrgUnit.Path<OrgUnit>(L_NEW_FORMATIVE_ORG_UNIT, this);
            return _newFormativeOrgUnit;
        }

    /**
     * @return Новый курс.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getNewCourse()
     */
        public Course.Path<Course> newCourse()
        {
            if(_newCourse == null )
                _newCourse = new Course.Path<Course>(L_NEW_COURSE, this);
            return _newCourse;
        }

    /**
     * @return Новая группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getNewGroup()
     */
        public Group.Path<Group> newGroup()
        {
            if(_newGroup == null )
                _newGroup = new Group.Path<Group>(L_NEW_GROUP, this);
            return _newGroup;
        }

    /**
     * @return Новое направление подготовки (специальность).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getNewEducationLevelsHighSchool()
     */
        public EducationLevelsHighSchool.Path<EducationLevelsHighSchool> newEducationLevelsHighSchool()
        {
            if(_newEducationLevelsHighSchool == null )
                _newEducationLevelsHighSchool = new EducationLevelsHighSchool.Path<EducationLevelsHighSchool>(L_NEW_EDUCATION_LEVELS_HIGH_SCHOOL, this);
            return _newEducationLevelsHighSchool;
        }

    /**
     * @return Новая форма обучения.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getNewDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> newDevelopForm()
        {
            if(_newDevelopForm == null )
                _newDevelopForm = new DevelopForm.Path<DevelopForm>(L_NEW_DEVELOP_FORM, this);
            return _newDevelopForm;
        }

    /**
     * @return Новый тип возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getNewCompensationType()
     */
        public CompensationType.Path<CompensationType> newCompensationType()
        {
            if(_newCompensationType == null )
                _newCompensationType = new CompensationType.Path<CompensationType>(L_NEW_COMPENSATION_TYPE, this);
            return _newCompensationType;
        }

    /**
     * @return Новое направление подготовки (специальность).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getEducationOrgUnit()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
        {
            if(_educationOrgUnit == null )
                _educationOrgUnit = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT, this);
            return _educationOrgUnit;
        }

    /**
     * @return Фамилия.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getLastName()
     */
        public PropertyPath<String> lastName()
        {
            if(_lastName == null )
                _lastName = new PropertyPath<String>(RepresentEnrollmentTransferGen.P_LAST_NAME, this);
            return _lastName;
        }

    /**
     * @return Имя.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getFirstName()
     */
        public PropertyPath<String> firstName()
        {
            if(_firstName == null )
                _firstName = new PropertyPath<String>(RepresentEnrollmentTransferGen.P_FIRST_NAME, this);
            return _firstName;
        }

    /**
     * @return Отчество.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getMiddleName()
     */
        public PropertyPath<String> middleName()
        {
            if(_middleName == null )
                _middleName = new PropertyPath<String>(RepresentEnrollmentTransferGen.P_MIDDLE_NAME, this);
            return _middleName;
        }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(RepresentEnrollmentTransferGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Перевод из.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getFormativeOrgUnitOldText()
     */
        public PropertyPath<String> formativeOrgUnitOldText()
        {
            if(_formativeOrgUnitOldText == null )
                _formativeOrgUnitOldText = new PropertyPath<String>(RepresentEnrollmentTransferGen.P_FORMATIVE_ORG_UNIT_OLD_TEXT, this);
            return _formativeOrgUnitOldText;
        }

    /**
     * @return Старое формирующее подразделение.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getFormativeOrgUnitOld()
     */
        public OrgUnit.Path<OrgUnit> formativeOrgUnitOld()
        {
            if(_formativeOrgUnitOld == null )
                _formativeOrgUnitOld = new OrgUnit.Path<OrgUnit>(L_FORMATIVE_ORG_UNIT_OLD, this);
            return _formativeOrgUnitOld;
        }

    /**
     * @return Старый курс.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getCourseOld()
     */
        public Course.Path<Course> courseOld()
        {
            if(_courseOld == null )
                _courseOld = new Course.Path<Course>(L_COURSE_OLD, this);
            return _courseOld;
        }

    /**
     * @return Старая группа.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getGroupOld()
     */
        public Group.Path<Group> groupOld()
        {
            if(_groupOld == null )
                _groupOld = new Group.Path<Group>(L_GROUP_OLD, this);
            return _groupOld;
        }

    /**
     * @return Старое направление подготовки (специальность).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getEducationLevelsHighSchoolOld()
     */
        public EducationLevelsHighSchool.Path<EducationLevelsHighSchool> educationLevelsHighSchoolOld()
        {
            if(_educationLevelsHighSchoolOld == null )
                _educationLevelsHighSchoolOld = new EducationLevelsHighSchool.Path<EducationLevelsHighSchool>(L_EDUCATION_LEVELS_HIGH_SCHOOL_OLD, this);
            return _educationLevelsHighSchoolOld;
        }

    /**
     * @return Старая форма обучения.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getDevelopFormOld()
     */
        public DevelopForm.Path<DevelopForm> developFormOld()
        {
            if(_developFormOld == null )
                _developFormOld = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM_OLD, this);
            return _developFormOld;
        }

    /**
     * @return Старый тип возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getCompensationTypeOld()
     */
        public CompensationType.Path<CompensationType> compensationTypeOld()
        {
            if(_compensationTypeOld == null )
                _compensationTypeOld = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE_OLD, this);
            return _compensationTypeOld;
        }

    /**
     * @return Старое направление подготовки (специальность).
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getEducationOrgUnitOld()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitOld()
        {
            if(_educationOrgUnitOld == null )
                _educationOrgUnitOld = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT_OLD, this);
            return _educationOrgUnitOld;
        }

    /**
     * @return Старая Фамилия. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getLastNameOld()
     */
        public PropertyPath<String> lastNameOld()
        {
            if(_lastNameOld == null )
                _lastNameOld = new PropertyPath<String>(RepresentEnrollmentTransferGen.P_LAST_NAME_OLD, this);
            return _lastNameOld;
        }

    /**
     * @return Старое Имя. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getFirstNameOld()
     */
        public PropertyPath<String> firstNameOld()
        {
            if(_firstNameOld == null )
                _firstNameOld = new PropertyPath<String>(RepresentEnrollmentTransferGen.P_FIRST_NAME_OLD, this);
            return _firstNameOld;
        }

    /**
     * @return Старое Отчество.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getMiddleNameOld()
     */
        public PropertyPath<String> middleNameOld()
        {
            if(_middleNameOld == null )
                _middleNameOld = new PropertyPath<String>(RepresentEnrollmentTransferGen.P_MIDDLE_NAME_OLD, this);
            return _middleNameOld;
        }

    /**
     * @return Старая дата приказа.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getOrderDateOld()
     */
        public PropertyPath<Date> orderDateOld()
        {
            if(_orderDateOld == null )
                _orderDateOld = new PropertyPath<Date>(RepresentEnrollmentTransferGen.P_ORDER_DATE_OLD, this);
            return _orderDateOld;
        }

    /**
     * @return Старый номер приказа.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getOrderNumberOld()
     */
        public PropertyPath<String> orderNumberOld()
        {
            if(_orderNumberOld == null )
                _orderNumberOld = new PropertyPath<String>(RepresentEnrollmentTransferGen.P_ORDER_NUMBER_OLD, this);
            return _orderNumberOld;
        }

    /**
     * @return Старая дата зачисления.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer#getOrderDateEnrOld()
     */
        public PropertyPath<Date> orderDateEnrOld()
        {
            if(_orderDateEnrOld == null )
                _orderDateEnrOld = new PropertyPath<Date>(RepresentEnrollmentTransferGen.P_ORDER_DATE_ENR_OLD, this);
            return _orderDateEnrOld;
        }

        public Class getEntityClass()
        {
            return RepresentEnrollmentTransfer.class;
        }

        public String getEntityName()
        {
            return "representEnrollmentTransfer";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
