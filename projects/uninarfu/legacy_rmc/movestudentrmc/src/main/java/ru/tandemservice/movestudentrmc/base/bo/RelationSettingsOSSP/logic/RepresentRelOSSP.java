package ru.tandemservice.movestudentrmc.base.bo.RelationSettingsOSSP.logic;

import ru.tandemservice.movestudentrmc.entity.catalog.OsspGrants;
import ru.tandemservice.movestudentrmc.entity.catalog.OsspPgo;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;

import java.util.List;

public class RepresentRelOSSP {

    private RepresentationReason _reason;
    private boolean _budget;
    private boolean _allBasement;
    private boolean _allGrants;
    private boolean _allPgo;
    private List<OsspGrants> _grantsList;
    private List<OsspPgo> _pgoList;

    public RepresentationReason getReason() {
        return _reason;
    }

    public void setReason(RepresentationReason reason) {
        _reason = reason;
    }

    public boolean isBudget() {
        return _budget;
    }

    public void setBudget(boolean budget) {
        _budget = budget;
    }

    public boolean isAllBasement() {
        return _allBasement;
    }

    public void setAllBasement(boolean allBasement) {
        _allBasement = allBasement;
    }

    public boolean isAllGrants() {
        return _allGrants;
    }

    public void setAllGrants(boolean allGrants) {
        _allGrants = allGrants;
    }

    public boolean isAllPgo() {
        return _allPgo;
    }

    public void setAllPgo(boolean allPgo) {
        _allPgo = allPgo;
    }

    public List<OsspGrants> getGrantsList() {
        return _grantsList;
    }

    public void setGrantsList(List<OsspGrants> grantsList) {
        _grantsList = grantsList;
    }

    public List<OsspPgo> getPgoList() {
        return _pgoList;
    }

    public void setPgoList(List<OsspPgo> pgoList) {
        _pgoList = pgoList;
    }
}
