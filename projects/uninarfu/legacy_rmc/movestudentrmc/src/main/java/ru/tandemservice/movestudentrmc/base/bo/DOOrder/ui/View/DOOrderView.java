package ru.tandemservice.movestudentrmc.base.bo.DOOrder.ui.View;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.logic.RepresentOrderDSHandler;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.uni.entity.employee.Student;

@Configuration
public class DOOrderView extends BusinessComponentManager {

    public static final String REPRESENT_ORDER_DS = "representOrderDS";

    @Bean
    public ColumnListExtPoint representOrderDS()
    {
        return columnListExtPointBuilder(REPRESENT_ORDER_DS)
                .addColumn(textColumn(RepresentOrderDSHandler.DEVELOP_FORM_COLUMN, "student." + Student.educationOrgUnit().developForm().title()).order().create())
                .addColumn(textColumn(RepresentOrderDSHandler.FORMATIVE_ORG_UNIT_COLUMN, "student." + Student.educationOrgUnit().formativeOrgUnit().title()).order().create())
                .addColumn(textColumn(RepresentOrderDSHandler.TERRITORIAL_ORG_UNIT_COLUMN, "student." + Student.educationOrgUnit().territorialOrgUnit().fullTitle()).order().create())
                .addColumn(textColumn(RepresentOrderDSHandler.EDUCATION_LEVEL_HIGH_SCHOOL_COLUMN, "student." + Student.educationOrgUnit().educationLevelHighSchool().orgUnit().fullTitle()).order().create())
                .addColumn(textColumn(RepresentOrderDSHandler.EDUCATION_ORG_UNIT_COLUMN, "student." + Student.educationOrgUnit().title()).order().create())
                        //.addColumn(textColumn(RepresentOrderDSHandler.TYPE_REPRESENT_COLUMN, "represent." +  Representation.type().title()).order().create())
                .addColumn(publisherColumn(RepresentOrderDSHandler.TYPE_REPRESENT_COLUMN, "represent." + Representation.type().title()).order().create())
                .addColumn(dateColumn(RepresentOrderDSHandler.DATE_REPRESENT_COLUMN, "represent." + Representation.startDate()).order().create())
                .addColumn(textColumn(RepresentOrderDSHandler.COURSE_COLUMN, "student." + Student.course().title()).order().create())
                .addColumn(textColumn(RepresentOrderDSHandler.GROUP_COLUMN, "student." + Student.group().title()).order().create())
                .addColumn(textColumn(RepresentOrderDSHandler.BUDGET_COLUMN, "student." + Student.compensationType().shortTitle()).order().create())
                .addColumn(publisherColumn(RepresentOrderDSHandler.STUDENT_FIO_COLUMN, "student." + Student.person().identityCard().fullFio()).publisherLinkResolver(new IPublisherLinkResolver() {
                    @Override
                    public Object getParameters(IEntity entity) {
                        DataWrapper dataWrapper = (DataWrapper) entity;
                        Student student = (Student) dataWrapper.getProperty("student");
                        return new UniMap().add(PublisherActivator.PUBLISHER_ID_KEY, student != null ? student.getId() : null).add("selectedStudentTab", "studentTab");
                    }

                    @Override
                    public String getComponentName(IEntity entity) {
                        return null;
                    }
                }).order().order(OrderDirection.asc).create())
                .addColumn(booleanColumn(RepresentOrderDSHandler.CHECK, "represent." + Representation.check()).order().create())
                .create();
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(REPRESENT_ORDER_DS, representOrderDS()).handler(representOrderDSHandler()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> representOrderDSHandler()
    {
        return new RepresentOrderDSHandler(getName());
    }

}
