package ru.tandemservice.movestudentrmc.dao.check;

import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.List;

public interface ICheckDAO {

    public List<String> check(IEntity represent, List<Student> list, boolean value);

    public List<String> doCheck(List<Student> list, boolean value);
}
