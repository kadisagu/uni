package ru.tandemservice.movestudentrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Состояние приказа."
 * Имя сущности : movestudentOrderStates
 * Файл data.xml : movementstudent.data.xml
 */
public interface MovestudentOrderStatesCodes
{
    /** Константа кода (code) элемента : 1 (code). Название (title) : Формируется */
    String CODE_1 = "1";
    /** Константа кода (code) элемента : 2 (code). Название (title) : На согласовании */
    String CODE_2 = "2";
    /** Константа кода (code) элемента : 3 (code). Название (title) : Согласовано */
    String CODE_3 = "3";
    /** Константа кода (code) элемента : 4 (code). Название (title) : Отклонено */
    String CODE_4 = "4";
    /** Константа кода (code) элемента : 5 (code). Название (title) : Проведено */
    String CODE_5 = "5";

    Set<String> CODES = ImmutableSet.of(CODE_1, CODE_2, CODE_3, CODE_4, CODE_5);
}
