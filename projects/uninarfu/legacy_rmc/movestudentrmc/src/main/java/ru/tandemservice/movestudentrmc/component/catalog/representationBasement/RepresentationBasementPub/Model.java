package ru.tandemservice.movestudentrmc.component.catalog.representationBasement.RepresentationBasementPub;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubModel;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationBasement;

public class Model extends DefaultCatalogPubModel<RepresentationBasement> {
    public static class RepresentationBasementWrapper extends
            IdentifiableWrapper<RepresentationBasement>
    {
        private static final long serialVersionUID = -4301478784488247007L;
        public static final String PRINTABLE = "printable";
        public static final String NOTE_REQUIRED = "noteRequired";
        public static final String CONNECTED_WITH_DOCUMENT = "connectedWithDocument";
        public static final String MERGED = "merged";
        private RepresentationBasement representationBasement;
        private boolean printable;
        private boolean noteRequired;
        private boolean connectedWithDocument;
        private boolean merged;

        public RepresentationBasementWrapper(
                RepresentationBasement representationBasement,
                boolean printable, boolean noteRequired, boolean merged, boolean connectedWithDocument)
        {
            super(representationBasement.getId(), representationBasement
                    .getTitle());
            this.representationBasement = representationBasement;
            this.printable = printable;
            this.noteRequired = noteRequired;
            this.connectedWithDocument = connectedWithDocument;
            this.merged = merged;
        }

        public RepresentationBasement getRepresentationBasement() {
            return representationBasement;
        }

        public boolean isPrintable() {
            return printable;
        }

        public void setPrintable(boolean printable) {
            this.printable = printable;
        }

        public boolean isNoteRequired() {
            return noteRequired;
        }

        public void setNoteRequired(boolean noteRequired) {
            this.noteRequired = noteRequired;
        }

        public boolean isMerged() {
            return merged;
        }

        public void setMerged(boolean merged) {
            this.merged = merged;
        }

        public boolean isConnectedWithDocument() {
            return connectedWithDocument;
        }

        public void setConnectedWithDocument(boolean connectedWithDocument) {
            this.connectedWithDocument = connectedWithDocument;
        }
    }
}
