package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudentrmc.entity.DocListRepresentBasics;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationBasement;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность-связь 'Основание'-'Дополнительные поля для оснований спичочного представления'
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DocListRepresentBasicsGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.DocListRepresentBasics";
    public static final String ENTITY_NAME = "docListRepresentBasics";
    public static final int VERSION_HASH = 1045127432;
    private static IEntityMeta ENTITY_META;

    public static final String L_LIST_REPRESENT = "listRepresent";
    public static final String L_REASON = "reason";
    public static final String L_BASIC = "basic";
    public static final String P_REPRESENTATION_BASEMENT_DATE = "representationBasementDate";
    public static final String P_REPRESENTATION_BASEMENT_NUMBER = "representationBasementNumber";

    private ListRepresent _listRepresent;     // Представление
    private RepresentationReason _reason;     // Причина
    private RepresentationBasement _basic;     // Основание
    private Date _representationBasementDate;     // Дата основания
    private String _representationBasementNumber;     // № основания

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Представление. Свойство не может быть null.
     */
    @NotNull
    public ListRepresent getListRepresent()
    {
        return _listRepresent;
    }

    /**
     * @param listRepresent Представление. Свойство не может быть null.
     */
    public void setListRepresent(ListRepresent listRepresent)
    {
        dirty(_listRepresent, listRepresent);
        _listRepresent = listRepresent;
    }

    /**
     * @return Причина.
     */
    public RepresentationReason getReason()
    {
        return _reason;
    }

    /**
     * @param reason Причина.
     */
    public void setReason(RepresentationReason reason)
    {
        dirty(_reason, reason);
        _reason = reason;
    }

    /**
     * @return Основание. Свойство не может быть null.
     */
    @NotNull
    public RepresentationBasement getBasic()
    {
        return _basic;
    }

    /**
     * @param basic Основание. Свойство не может быть null.
     */
    public void setBasic(RepresentationBasement basic)
    {
        dirty(_basic, basic);
        _basic = basic;
    }

    /**
     * @return Дата основания.
     */
    public Date getRepresentationBasementDate()
    {
        return _representationBasementDate;
    }

    /**
     * @param representationBasementDate Дата основания.
     */
    public void setRepresentationBasementDate(Date representationBasementDate)
    {
        dirty(_representationBasementDate, representationBasementDate);
        _representationBasementDate = representationBasementDate;
    }

    /**
     * @return № основания.
     */
    @Length(max=255)
    public String getRepresentationBasementNumber()
    {
        return _representationBasementNumber;
    }

    /**
     * @param representationBasementNumber № основания.
     */
    public void setRepresentationBasementNumber(String representationBasementNumber)
    {
        dirty(_representationBasementNumber, representationBasementNumber);
        _representationBasementNumber = representationBasementNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DocListRepresentBasicsGen)
        {
            setListRepresent(((DocListRepresentBasics)another).getListRepresent());
            setReason(((DocListRepresentBasics)another).getReason());
            setBasic(((DocListRepresentBasics)another).getBasic());
            setRepresentationBasementDate(((DocListRepresentBasics)another).getRepresentationBasementDate());
            setRepresentationBasementNumber(((DocListRepresentBasics)another).getRepresentationBasementNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DocListRepresentBasicsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DocListRepresentBasics.class;
        }

        public T newInstance()
        {
            return (T) new DocListRepresentBasics();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "listRepresent":
                    return obj.getListRepresent();
                case "reason":
                    return obj.getReason();
                case "basic":
                    return obj.getBasic();
                case "representationBasementDate":
                    return obj.getRepresentationBasementDate();
                case "representationBasementNumber":
                    return obj.getRepresentationBasementNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "listRepresent":
                    obj.setListRepresent((ListRepresent) value);
                    return;
                case "reason":
                    obj.setReason((RepresentationReason) value);
                    return;
                case "basic":
                    obj.setBasic((RepresentationBasement) value);
                    return;
                case "representationBasementDate":
                    obj.setRepresentationBasementDate((Date) value);
                    return;
                case "representationBasementNumber":
                    obj.setRepresentationBasementNumber((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "listRepresent":
                        return true;
                case "reason":
                        return true;
                case "basic":
                        return true;
                case "representationBasementDate":
                        return true;
                case "representationBasementNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "listRepresent":
                    return true;
                case "reason":
                    return true;
                case "basic":
                    return true;
                case "representationBasementDate":
                    return true;
                case "representationBasementNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "listRepresent":
                    return ListRepresent.class;
                case "reason":
                    return RepresentationReason.class;
                case "basic":
                    return RepresentationBasement.class;
                case "representationBasementDate":
                    return Date.class;
                case "representationBasementNumber":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DocListRepresentBasics> _dslPath = new Path<DocListRepresentBasics>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DocListRepresentBasics");
    }
            

    /**
     * @return Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocListRepresentBasics#getListRepresent()
     */
    public static ListRepresent.Path<ListRepresent> listRepresent()
    {
        return _dslPath.listRepresent();
    }

    /**
     * @return Причина.
     * @see ru.tandemservice.movestudentrmc.entity.DocListRepresentBasics#getReason()
     */
    public static RepresentationReason.Path<RepresentationReason> reason()
    {
        return _dslPath.reason();
    }

    /**
     * @return Основание. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocListRepresentBasics#getBasic()
     */
    public static RepresentationBasement.Path<RepresentationBasement> basic()
    {
        return _dslPath.basic();
    }

    /**
     * @return Дата основания.
     * @see ru.tandemservice.movestudentrmc.entity.DocListRepresentBasics#getRepresentationBasementDate()
     */
    public static PropertyPath<Date> representationBasementDate()
    {
        return _dslPath.representationBasementDate();
    }

    /**
     * @return № основания.
     * @see ru.tandemservice.movestudentrmc.entity.DocListRepresentBasics#getRepresentationBasementNumber()
     */
    public static PropertyPath<String> representationBasementNumber()
    {
        return _dslPath.representationBasementNumber();
    }

    public static class Path<E extends DocListRepresentBasics> extends EntityPath<E>
    {
        private ListRepresent.Path<ListRepresent> _listRepresent;
        private RepresentationReason.Path<RepresentationReason> _reason;
        private RepresentationBasement.Path<RepresentationBasement> _basic;
        private PropertyPath<Date> _representationBasementDate;
        private PropertyPath<String> _representationBasementNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocListRepresentBasics#getListRepresent()
     */
        public ListRepresent.Path<ListRepresent> listRepresent()
        {
            if(_listRepresent == null )
                _listRepresent = new ListRepresent.Path<ListRepresent>(L_LIST_REPRESENT, this);
            return _listRepresent;
        }

    /**
     * @return Причина.
     * @see ru.tandemservice.movestudentrmc.entity.DocListRepresentBasics#getReason()
     */
        public RepresentationReason.Path<RepresentationReason> reason()
        {
            if(_reason == null )
                _reason = new RepresentationReason.Path<RepresentationReason>(L_REASON, this);
            return _reason;
        }

    /**
     * @return Основание. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocListRepresentBasics#getBasic()
     */
        public RepresentationBasement.Path<RepresentationBasement> basic()
        {
            if(_basic == null )
                _basic = new RepresentationBasement.Path<RepresentationBasement>(L_BASIC, this);
            return _basic;
        }

    /**
     * @return Дата основания.
     * @see ru.tandemservice.movestudentrmc.entity.DocListRepresentBasics#getRepresentationBasementDate()
     */
        public PropertyPath<Date> representationBasementDate()
        {
            if(_representationBasementDate == null )
                _representationBasementDate = new PropertyPath<Date>(DocListRepresentBasicsGen.P_REPRESENTATION_BASEMENT_DATE, this);
            return _representationBasementDate;
        }

    /**
     * @return № основания.
     * @see ru.tandemservice.movestudentrmc.entity.DocListRepresentBasics#getRepresentationBasementNumber()
     */
        public PropertyPath<String> representationBasementNumber()
        {
            if(_representationBasementNumber == null )
                _representationBasementNumber = new PropertyPath<String>(DocListRepresentBasicsGen.P_REPRESENTATION_BASEMENT_NUMBER, this);
            return _representationBasementNumber;
        }

        public Class getEntityClass()
        {
            return DocListRepresentBasics.class;
        }

        public String getEntityName()
        {
            return "docListRepresentBasics";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
