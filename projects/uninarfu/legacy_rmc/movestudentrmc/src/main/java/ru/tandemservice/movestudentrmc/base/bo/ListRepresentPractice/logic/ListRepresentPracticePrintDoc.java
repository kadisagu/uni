/* $Id$ */
package ru.tandemservice.movestudentrmc.base.bo.ListRepresentPractice.logic;

import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfString;

import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListRepresentPractice;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author Andrey Andreev
 * @since 26.10.2015
 */
public class ListRepresentPracticePrintDoc extends ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.ListRepresentPracticePrintDoc
{
    public ListRepresentPracticePrintDoc()
    {
        super("list.rep.practice.header",
                "list.rep.practice.parag");
    }

    @Override
    protected RtfInjectModifier paragraphModify(RtfInjectModifier im, ListRepresent represent, Student student)
    {
        ListRepresentPractice rp = (ListRepresentPractice) represent;
        im.put("dateBegin", UtilPrintSupport.getDateFormatterWithMonthString(rp.getDateBeginningPractice()))
                .put("dateEnd", UtilPrintSupport.getDateFormatterWithMonthString(rp.getDateEndOfPractice()))
                .put("course", student.getCourse().getTitle())
                .put("developForm", UtilPrintSupport.getDevelopFormGen(student.getEducationOrgUnit().getDevelopForm()))
                .put("highSchool", UtilPrintSupport.getHighLevelSchoolTypeString(student.getEducationOrgUnit().getEducationLevelHighSchool()))
                .put("reason", rp.getRepresentationReason().getTitle())
                .put("practiceTitle", rp.getPracticeTitle())
                //------------------------------------------------------------------------------------------------
                //.put("practicePaymentData", rp.getPaymentData() == null ? "" : rp.getPaymentData().getTitle())
                .put("practicePaymentData", rp.getPaymentData() == null ? "" : ", "+rp.getPaymentData().getTitle())
                //------------------------------------------------------------------------------------------------
                .put("basement", rp.getRepresentationBasement().getTitle())
                //----------------------------------------------------------------------------------------------------------------------------------------------------
                //.put("supervisor", rp.getSupervisor() == null ? "" : rp.getSupervisor().getFullFio());
        		.put("supervisor", rp.getSupervisor() == null ? new RtfString().append("") : new RtfString().par().append("Руководитель практики: ").append(rp.getSupervisor().getFullFio().toString()));
        		//----------------------------------------------------------------------------------------------------------------------------------------------------
        
        return im;
    }
}
