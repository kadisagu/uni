package ru.tandemservice.movestudentrmc.component.settings.OrderCategoryToRepresentationSettingsPub;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.entity.catalog.OrderCategory;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        OrderCategory category = get(OrderCategory.class, model.getFirstId());
        model.setOrderCategory(category);
    }

    @Override
    public void prepareListDataSource(Model model) {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RepresentationType.class, "e")
                .where(DQLExpressions.eqValue(DQLExpressions.property(RepresentationType.orderCategory().fromAlias("e")), model.getOrderCategory()));
        model.getDataSource().setCountRow(getCount(builder));
        (new DQLOrderDescriptionRegistry(RepresentationType.class, "e")).applyOrder(builder, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }

}
