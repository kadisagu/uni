package ru.tandemservice.movestudentrmc.base.bo.DORepresentPersonWorkPlan.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentDAO;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.ListRepresentTransfer;
import ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents;
import ru.tandemservice.movestudentrmc.entity.RepresentPersonWorkPlan;
import ru.tandemservice.movestudentrmc.entity.RepresentTransfer;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentExtractStatesCodes;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentationTypeCodes;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;

import static org.tandemframework.hibsupport.dql.DQLExpressions.and;
import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.not;
import static org.tandemframework.hibsupport.dql.DQLExpressions.or;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class DORepresentPersonWorkPlanDAO extends AbstractDORepresentDAO
{

    @Override
    public Representation createRepresentObject(RepresentationType type, List<Student> studentList) {

        RepresentPersonWorkPlan represent = new RepresentPersonWorkPlan();
        represent.setType(type);
        return represent;
    }

    @Override
    public void buildBodyExtract(Representation representationBase, String itemFac, RtfDocument docCommon) {
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        UtilPrintSupport.setParNum(docCommon, itemFac);

        RtfInjectModifier modifier = new RtfInjectModifier();

        UtilPrintSupport.injectModifierExtract(modifier, docRepresent);

        modifier.modify(docCommon);
    }

    @Override
    public void buildBodyRepresent(Representation representationBase, RtfDocument document, AtomicInteger parNumber, AtomicInteger extractNumber) {
        super.buildBodyRepresent(representationBase, document, parNumber, extractNumber);
        RepresentPersonWorkPlan represent = (RepresentPersonWorkPlan) representationBase;
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        Student student = UtilPrintSupport.getStudent(represent);

        RtfInjectModifier modifier = new RtfInjectModifier();

        UtilPrintSupport.injectModifierRepresent(modifier, docRepresent, student);
        //modifier.put("studentTitle", PersonManager.instance().declinationDao().getDeclinationFIO(student.getPerson().getIdentityCard(), GrammaCase.DATIVE));
        //-----------------------------------------------------------------------------------------------------------------------
        //modifier.put("studentTitle", UtilPrintSupport.printSeparatedFIO(student.getPerson().getIdentityCard(), GrammaCase.GENITIVE, student.getPerson().isMale()));
        modifier.put("studentTitle", UtilPrintSupport.printSeparatedFIO(student.getPerson().getIdentityCard(), GrammaCase.ACCUSATIVE, student.getPerson().isMale()));
        //-----------------------------------------------------------------------------------------------------------------------

        modifier.put("st_date", UtilPrintSupport.getDateFormatterWithMonthString(represent.getBeginPersonWorkPlanDate()));
        modifier.put("end_date", UtilPrintSupport.getDateFormatterWithMonthString(represent.getEndPersonWorkPlanDate()));

        //------------------------------------------------------------------------------------
        //modifier.put("studentSex", student.getPerson().isMale() ? "студенту" : "студентке");
        modifier.put("studentSex", student.getPerson().isMale() ? "студента" : "студентку");
        //------------------------------------------------------------------------------------

        if (StringUtils.isEmpty(modifier.getStringValue("basics")))
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Arrays.asList("basics"), false, false);

        modifier.modify(document);
    }

    @Override
    public void buildBodyDeclaration(Representation representationBase, RtfDocument document) {
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        RtfInjectModifier im = new RtfInjectModifier();

        RtfTableModifier tm = new RtfTableModifier();
        UtilPrintSupport.injectModifierDeclaration(im, tm, docRepresent);
        RepresentPersonWorkPlan representExtensionSession = (RepresentPersonWorkPlan) representationBase;
        im.put("st_date", UtilPrintSupport.getDateFormatterWithMonthString(representExtensionSession.getBeginPersonWorkPlanDate()));
        im.put("end_date", UtilPrintSupport.getDateFormatterWithMonthString(representExtensionSession.getEndPersonWorkPlanDate()));

        this.injectModifier(im, docRepresent);

        im.modify(document);
        tm.modify(document);
    }

    @Override
    public GrammaCase getGrammaCase() {
    	//---------------------------
        //return GrammaCase.DATIVE;
        return GrammaCase.ACCUSATIVE;
        //---------------------------
    }
    
    //----------------------------------------------------------------------------------------------
    @Override
    public boolean doCommit(Representation represent, ErrorCollector error) {
    	
        if(!super.doCommit(represent, error))
            return false;   
        
        DocRepresentStudentBase representStudentBase = IUniBaseDao.instance.get().get(DocRepresentStudentBase.class, DocRepresentStudentBase.representation().s(), represent);
        if (representStudentBase != null) {

            Student student = representStudentBase.getStudent();
            student.setPersonalEducation(true);		//Дополнительные данные студента "Индивидуальное образование"
        
        }
        
    	return true;
    } 
    
    @Override
    public boolean doRollback(Representation represent, ErrorCollector error) {
    	
        if (!super.doRollback(represent, error))
            return false;
        
        DocRepresentStudentBase representStudentBase = IUniBaseDao.instance.get().get(DocRepresentStudentBase.class, DocRepresentStudentBase.representation().s(), represent);
        if (representStudentBase != null) {

            Student student = representStudentBase.getStudent();
            
        	//Проверка списочных приказов "О переводе" с параметром "Утвердить ИУП"
        	DQLSelectBuilder checkBuilder = new DQLSelectBuilder()
        			.fromEntity(RelListRepresentStudents.class, "rel")
        			.joinEntity("rel", DQLJoinType.inner, ListRepresentTransfer.class, "rep", eq(RelListRepresentStudents.representation().id().fromAlias("rel"), ListRepresentTransfer.id().fromAlias("rep")) )
        			.where(eq(property(RelListRepresentStudents.student().id().fromAlias("rel")), value(student.getId())))
        			//.where(not(eq(property(RelListRepresentStudents.representation().id().fromAlias("rel")), value(represent.getId()))))            			
        			.where(eq(property(RelListRepresentStudents.representation().state().code().fromAlias("rel")), value(MovestudentExtractStatesCodes.CODE_6)))
        			.where(eq(property(ListRepresentTransfer.individualPlan().fromAlias("rep")), value(true)))
        		    .column(property(RelListRepresentStudents.representation().id().fromAlias("rel")))
        			;
        	List<Long> checkListRepresentIdList = checkBuilder.createStatement(getSession()).list();
        	
        	//Проверка индивидуальных приказов "О переводе" с параметром "Утвердить ИУП", а также индивидуального приказа "Об утверждении ИУП"
        	DQLSelectBuilder builder = new DQLSelectBuilder()
        			.fromEntity(DocRepresentStudentBase.class, "doc")
        			.joinEntity("doc", DQLJoinType.left, RepresentTransfer.class, "rep", eq(DocRepresentStudentBase.representation().id().fromAlias("doc"), RepresentTransfer.id().fromAlias("rep")) )
        			.where(eq(property(DocRepresentStudentBase.student().id().fromAlias("doc")), value(student.getId())))
        			.where(not(eq(property(DocRepresentStudentBase.representation().id().fromAlias("doc")), value(represent.getId()))))            			
        			.where(eq(property(DocRepresentStudentBase.representation().state().code().fromAlias("doc")), value(MovestudentExtractStatesCodes.CODE_6)))
        			.where(or(
        				eq(property(DocRepresentStudentBase.representation().type().code().fromAlias("doc")), value(RepresentationTypeCodes.PERSON_WORK_PLAN)), 
        				and(
        					eq(property(DocRepresentStudentBase.representation().type().code().fromAlias("doc")), value(RepresentationTypeCodes.TRANSFER)), 
        					eq(property(RepresentTransfer.individualPlan().fromAlias("rep")), value(true))
        				)
        				))
        		    .column(property(DocRepresentStudentBase.representation().id().fromAlias("doc")));
        	List<Long> checkRepresentIdList = builder.createStatement(getSession()).list();
        	
        	if(checkListRepresentIdList.size() == 0 && checkRepresentIdList.size() == 0) {
        		student.setPersonalEducation(false);		//Дополнительные данные студента "Индивидуальное образование"
        	}             
        
        }
    	
    	return true;
    }
    //----------------------------------------------------------------------------------------------

}
