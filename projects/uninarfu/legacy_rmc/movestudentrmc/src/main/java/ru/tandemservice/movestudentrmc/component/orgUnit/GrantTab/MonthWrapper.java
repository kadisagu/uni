package ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab;

import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/*
 * Отсчет месяцев ведется с начала учебного года, т.е. сентябрь=0, октябрь=1,...
 */
public class MonthWrapper extends EntityBase implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 9207452579112180166L;

    private Integer month;

    public MonthWrapper(int month) {
        this.month = month;
        setId((long) month);
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public String getTitle() {
        return RussianDateFormatUtils.getMonthName(getMonthNumber() + 1, true);
    }

    public int getMonthNumber() {
        return (Calendar.SEPTEMBER + this.month) % 12;
    }

    public String getStringValue(EducationYear year) {
        return new StringBuilder()
                .append(getMonthNumber() + 1)
                .append(".")
                .append(year.getTitle())
                .toString();
    }

    public Date getDate(EducationYear year) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.MILLISECOND, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.HOUR, 0);
        c.set(Calendar.DAY_OF_MONTH, 1);
        c.set(Calendar.MONTH, getMonthNumber());

        int y = year.getIntValue();
        if (getMonthNumber() < Calendar.SEPTEMBER)
            y++;
        c.set(Calendar.YEAR, y);

        return c.getTime();
    }

    public static List<String> getMonthsList(Date dateBeginingPayment, Date dateEndOfPayment, EducationYear educationYear) {
        //формируем список периодов
        List<String> mWrapperList = new ArrayList<String>();

        Calendar c1 = Calendar.getInstance();
        c1.setTime(dateBeginingPayment);
        c1.set(Calendar.DAY_OF_MONTH, 1);

        Calendar c2 = Calendar.getInstance();
        c2.setTime(dateEndOfPayment);
        //c2.set(Calendar.YEAR, c1.get(Calendar.YEAR));
        c2.set(Calendar.DAY_OF_MONTH, c2.getActualMaximum(Calendar.DAY_OF_MONTH));

        while (c1.before(c2)) {
            c1.add(Calendar.MONTH, 1);
            String stringValue = new MonthWrapper((c1.get(Calendar.MONTH) - Calendar.SEPTEMBER + 11) % 12).getStringValue(educationYear);
            if (!mWrapperList.contains(stringValue))
                mWrapperList.add(stringValue);
        }
        return mWrapperList;
    }
    
    //-----------------------------------------------------------------------------------------------------------------------------------
    public static List<String> getMonthsList(Date dateBeginingPayment, Date dateEndOfPayment) {
        //Список периодов без ограничения учебным годом
        List<String> mWrapperList = new ArrayList<String>();

        Calendar c1 = Calendar.getInstance();
        c1.setTime(dateBeginingPayment);
        c1.set(Calendar.DAY_OF_MONTH, 1);

        Calendar c2 = Calendar.getInstance();
        c2.setTime(dateEndOfPayment);
        c2.set(Calendar.DAY_OF_MONTH, c2.getActualMaximum(Calendar.DAY_OF_MONTH));

        while (c1.before(c2)) {
        	//------------------------
        	int yearNumber = c1.get(Calendar.YEAR) + (c1.get(Calendar.MONTH) < 8 ? -1 : 0);
        	EducationYear eduYear = UniDaoFacade.getCoreDao().getNotNull(EducationYear.class, EducationYear.intValue(), yearNumber);
        	//------------------------
            c1.add(Calendar.MONTH, 1);
            String stringValue = new MonthWrapper((c1.get(Calendar.MONTH) - Calendar.SEPTEMBER + 11) % 12).getStringValue(eduYear);
            if (!mWrapperList.contains(stringValue))
                mWrapperList.add(stringValue);
        }
        return mWrapperList;
    }    
    //-----------------------------------------------------------------------------------------------------------------------------------

    public static Data getInstance(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);

        int grantYear = c.get(Calendar.YEAR);
        if (c.get(Calendar.MONTH) < Calendar.SEPTEMBER)
            grantYear--;
        EducationYear eduYear = UniDaoFacade.getCoreDao().getNotNull(EducationYear.class, EducationYear.intValue(), grantYear);

        int grantMonth = c.get(Calendar.MONTH);
        MonthWrapper monthWrapper = new MonthWrapper((grantMonth + 12 - Calendar.SEPTEMBER) % 12);

        return new Data(monthWrapper, eduYear);
    }

    public static class Data {
        public MonthWrapper monthWrapper;
        public EducationYear educationYear;

        public Data(MonthWrapper monthWrapper, EducationYear educationYear) {
            this.monthWrapper = monthWrapper;
            this.educationYear = educationYear;
        }
    }
}
