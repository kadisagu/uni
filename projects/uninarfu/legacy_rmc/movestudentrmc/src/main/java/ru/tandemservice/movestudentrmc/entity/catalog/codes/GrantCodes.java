package ru.tandemservice.movestudentrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Наименование стипендий/выплат"
 * Имя сущности : grant
 * Файл data.xml : movementstudent.data.xml
 */
public interface GrantCodes
{
    /** Константа кода (code) элемента : grant1 (code). Название (title) : Академическая стипендия */
    String GRANT1 = "grant1";
    /** Константа кода (code) элемента : grant2 (code). Название (title) : Повышенная_1 стипендия */
    String GRANT2 = "grant2";
    /** Константа кода (code) элемента : grant3 (code). Название (title) : Повышенная_2 стипендия */
    String GRANT3 = "grant3";
    /** Константа кода (code) элемента : grant4 (code). Название (title) : Стипендия президента РФ */
    String GRANT4 = "grant4";
    /** Константа кода (code) элемента : grant5 (code). Название (title) : Правительства РФ */
    String GRANT5 = "grant5";
    /** Константа кода (code) элемента : grant6 (code). Название (title) : Министерства образования */
    String GRANT6 = "grant6";
    /** Константа кода (code) элемента : grant7 (code). Название (title) : Компании */
    String GRANT7 = "grant7";
    /** Константа кода (code) элемента : grant8 (code). Название (title) : ПГО */
    String GRANT8 = "grant8";
    /** Константа кода (code) элемента : grant9 (code). Название (title) : Опека */
    String GRANT9 = "grant9";
    /** Константа кода (code) элемента : grant10 (code). Название (title) : Социальная стипендия */
    String GRANT10 = "grant10";
    /** Константа кода (code) элемента : grant11 (code). Название (title) : Народы севера */
    String GRANT11 = "grant11";
    /** Константа кода (code) элемента : grant12 (code). Название (title) : Военные */
    String GRANT12 = "grant12";
    /** Константа кода (code) элемента : grant13 (code). Название (title) : Иностранные граждане */
    String GRANT13 = "grant13";
    /** Константа кода (code) элемента : grant14 (code). Название (title) : Академический отпуск */
    String GRANT14 = "grant14";
    /** Константа кода (code) элемента : grant15 (code). Название (title) : Беременность и уход за ребенком */
    String GRANT15 = "grant15";
    /** Константа кода (code) элемента : grant16 (code). Название (title) : Социальная выплата */
    String GRANT16 = "grant16";
    /** Константа кода (code) элемента : grant17 (code). Название (title) : Выплаты */
    String GRANT17 = "grant17";

    Set<String> CODES = ImmutableSet.of(GRANT1, GRANT2, GRANT3, GRANT4, GRANT5, GRANT6, GRANT7, GRANT8, GRANT9, GRANT10, GRANT11, GRANT12, GRANT13, GRANT14, GRANT15, GRANT16, GRANT17);
}
