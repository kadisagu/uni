package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudentrmc.entity.IStudentGrant;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListRepresentGrant;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Списочное представление о назначении стипендии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ListRepresentGrantGen extends ListRepresent
 implements IStudentGrant{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.ListRepresentGrant";
    public static final String ENTITY_NAME = "listRepresentGrant";
    public static final int VERSION_HASH = 1631497434;
    private static IEntityMeta ENTITY_META;

    public static final String P_BEGIN_GRANT_DATE = "beginGrantDate";
    public static final String P_END_GRANT_DATE = "endGrantDate";
    public static final String P_DATE_BEGINING_PAYMENT = "dateBeginingPayment";
    public static final String P_DATE_END_OF_PAYMENT = "dateEndOfPayment";
    public static final String P_FULL_EDUCATION_PERIOD = "fullEducationPeriod";
    public static final String L_GRANT_VIEW = "grantView";

    private Date _beginGrantDate;     // Дата назначения выплат
    private Date _endGrantDate;     // Дата окончания выплат
    private Date _dateBeginingPayment;     // Месяц начала выплаты
    private Date _dateEndOfPayment;     // Месяц окончания выплаты
    private Boolean _fullEducationPeriod;     // На период обучения
    private GrantView _grantView;     // Вид стипендии

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата назначения выплат. Свойство не может быть null.
     *
     * Это формула "dateBeginingPayment".
     */
    // @NotNull
    public Date getBeginGrantDate()
    {
        return _beginGrantDate;
    }

    /**
     * @param beginGrantDate Дата назначения выплат. Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setBeginGrantDate(Date beginGrantDate)
    {
        dirty(_beginGrantDate, beginGrantDate);
        _beginGrantDate = beginGrantDate;
    }

    /**
     * @return Дата окончания выплат.
     *
     * Это формула "dateEndOfPayment".
     */
    public Date getEndGrantDate()
    {
        return _endGrantDate;
    }

    /**
     * @param endGrantDate Дата окончания выплат.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setEndGrantDate(Date endGrantDate)
    {
        dirty(_endGrantDate, endGrantDate);
        _endGrantDate = endGrantDate;
    }

    /**
     * @return Месяц начала выплаты. Свойство не может быть null.
     */
    @NotNull
    public Date getDateBeginingPayment()
    {
        return _dateBeginingPayment;
    }

    /**
     * @param dateBeginingPayment Месяц начала выплаты. Свойство не может быть null.
     */
    public void setDateBeginingPayment(Date dateBeginingPayment)
    {
        dirty(_dateBeginingPayment, dateBeginingPayment);
        _dateBeginingPayment = dateBeginingPayment;
    }

    /**
     * @return Месяц окончания выплаты.
     */
    public Date getDateEndOfPayment()
    {
        return _dateEndOfPayment;
    }

    /**
     * @param dateEndOfPayment Месяц окончания выплаты.
     */
    public void setDateEndOfPayment(Date dateEndOfPayment)
    {
        dirty(_dateEndOfPayment, dateEndOfPayment);
        _dateEndOfPayment = dateEndOfPayment;
    }

    /**
     * @return На период обучения.
     */
    public Boolean getFullEducationPeriod()
    {
        return _fullEducationPeriod;
    }

    /**
     * @param fullEducationPeriod На период обучения.
     */
    public void setFullEducationPeriod(Boolean fullEducationPeriod)
    {
        dirty(_fullEducationPeriod, fullEducationPeriod);
        _fullEducationPeriod = fullEducationPeriod;
    }

    /**
     * @return Вид стипендии. Свойство не может быть null.
     */
    @NotNull
    public GrantView getGrantView()
    {
        return _grantView;
    }

    /**
     * @param grantView Вид стипендии. Свойство не может быть null.
     */
    public void setGrantView(GrantView grantView)
    {
        dirty(_grantView, grantView);
        _grantView = grantView;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ListRepresentGrantGen)
        {
            setBeginGrantDate(((ListRepresentGrant)another).getBeginGrantDate());
            setEndGrantDate(((ListRepresentGrant)another).getEndGrantDate());
            setDateBeginingPayment(((ListRepresentGrant)another).getDateBeginingPayment());
            setDateEndOfPayment(((ListRepresentGrant)another).getDateEndOfPayment());
            setFullEducationPeriod(((ListRepresentGrant)another).getFullEducationPeriod());
            setGrantView(((ListRepresentGrant)another).getGrantView());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ListRepresentGrantGen> extends ListRepresent.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ListRepresentGrant.class;
        }

        public T newInstance()
        {
            return (T) new ListRepresentGrant();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "beginGrantDate":
                    return obj.getBeginGrantDate();
                case "endGrantDate":
                    return obj.getEndGrantDate();
                case "dateBeginingPayment":
                    return obj.getDateBeginingPayment();
                case "dateEndOfPayment":
                    return obj.getDateEndOfPayment();
                case "fullEducationPeriod":
                    return obj.getFullEducationPeriod();
                case "grantView":
                    return obj.getGrantView();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "beginGrantDate":
                    obj.setBeginGrantDate((Date) value);
                    return;
                case "endGrantDate":
                    obj.setEndGrantDate((Date) value);
                    return;
                case "dateBeginingPayment":
                    obj.setDateBeginingPayment((Date) value);
                    return;
                case "dateEndOfPayment":
                    obj.setDateEndOfPayment((Date) value);
                    return;
                case "fullEducationPeriod":
                    obj.setFullEducationPeriod((Boolean) value);
                    return;
                case "grantView":
                    obj.setGrantView((GrantView) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "beginGrantDate":
                        return true;
                case "endGrantDate":
                        return true;
                case "dateBeginingPayment":
                        return true;
                case "dateEndOfPayment":
                        return true;
                case "fullEducationPeriod":
                        return true;
                case "grantView":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "beginGrantDate":
                    return true;
                case "endGrantDate":
                    return true;
                case "dateBeginingPayment":
                    return true;
                case "dateEndOfPayment":
                    return true;
                case "fullEducationPeriod":
                    return true;
                case "grantView":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "beginGrantDate":
                    return Date.class;
                case "endGrantDate":
                    return Date.class;
                case "dateBeginingPayment":
                    return Date.class;
                case "dateEndOfPayment":
                    return Date.class;
                case "fullEducationPeriod":
                    return Boolean.class;
                case "grantView":
                    return GrantView.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ListRepresentGrant> _dslPath = new Path<ListRepresentGrant>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ListRepresentGrant");
    }
            

    /**
     * @return Дата назначения выплат. Свойство не может быть null.
     *
     * Это формула "dateBeginingPayment".
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentGrant#getBeginGrantDate()
     */
    public static PropertyPath<Date> beginGrantDate()
    {
        return _dslPath.beginGrantDate();
    }

    /**
     * @return Дата окончания выплат.
     *
     * Это формула "dateEndOfPayment".
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentGrant#getEndGrantDate()
     */
    public static PropertyPath<Date> endGrantDate()
    {
        return _dslPath.endGrantDate();
    }

    /**
     * @return Месяц начала выплаты. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentGrant#getDateBeginingPayment()
     */
    public static PropertyPath<Date> dateBeginingPayment()
    {
        return _dslPath.dateBeginingPayment();
    }

    /**
     * @return Месяц окончания выплаты.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentGrant#getDateEndOfPayment()
     */
    public static PropertyPath<Date> dateEndOfPayment()
    {
        return _dslPath.dateEndOfPayment();
    }

    /**
     * @return На период обучения.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentGrant#getFullEducationPeriod()
     */
    public static PropertyPath<Boolean> fullEducationPeriod()
    {
        return _dslPath.fullEducationPeriod();
    }

    /**
     * @return Вид стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentGrant#getGrantView()
     */
    public static GrantView.Path<GrantView> grantView()
    {
        return _dslPath.grantView();
    }

    public static class Path<E extends ListRepresentGrant> extends ListRepresent.Path<E>
    {
        private PropertyPath<Date> _beginGrantDate;
        private PropertyPath<Date> _endGrantDate;
        private PropertyPath<Date> _dateBeginingPayment;
        private PropertyPath<Date> _dateEndOfPayment;
        private PropertyPath<Boolean> _fullEducationPeriod;
        private GrantView.Path<GrantView> _grantView;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата назначения выплат. Свойство не может быть null.
     *
     * Это формула "dateBeginingPayment".
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentGrant#getBeginGrantDate()
     */
        public PropertyPath<Date> beginGrantDate()
        {
            if(_beginGrantDate == null )
                _beginGrantDate = new PropertyPath<Date>(ListRepresentGrantGen.P_BEGIN_GRANT_DATE, this);
            return _beginGrantDate;
        }

    /**
     * @return Дата окончания выплат.
     *
     * Это формула "dateEndOfPayment".
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentGrant#getEndGrantDate()
     */
        public PropertyPath<Date> endGrantDate()
        {
            if(_endGrantDate == null )
                _endGrantDate = new PropertyPath<Date>(ListRepresentGrantGen.P_END_GRANT_DATE, this);
            return _endGrantDate;
        }

    /**
     * @return Месяц начала выплаты. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentGrant#getDateBeginingPayment()
     */
        public PropertyPath<Date> dateBeginingPayment()
        {
            if(_dateBeginingPayment == null )
                _dateBeginingPayment = new PropertyPath<Date>(ListRepresentGrantGen.P_DATE_BEGINING_PAYMENT, this);
            return _dateBeginingPayment;
        }

    /**
     * @return Месяц окончания выплаты.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentGrant#getDateEndOfPayment()
     */
        public PropertyPath<Date> dateEndOfPayment()
        {
            if(_dateEndOfPayment == null )
                _dateEndOfPayment = new PropertyPath<Date>(ListRepresentGrantGen.P_DATE_END_OF_PAYMENT, this);
            return _dateEndOfPayment;
        }

    /**
     * @return На период обучения.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentGrant#getFullEducationPeriod()
     */
        public PropertyPath<Boolean> fullEducationPeriod()
        {
            if(_fullEducationPeriod == null )
                _fullEducationPeriod = new PropertyPath<Boolean>(ListRepresentGrantGen.P_FULL_EDUCATION_PERIOD, this);
            return _fullEducationPeriod;
        }

    /**
     * @return Вид стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentGrant#getGrantView()
     */
        public GrantView.Path<GrantView> grantView()
        {
            if(_grantView == null )
                _grantView = new GrantView.Path<GrantView>(L_GRANT_VIEW, this);
            return _grantView;
        }

        public Class getEntityClass()
        {
            return ListRepresentGrant.class;
        }

        public String getEntityName()
        {
            return "listRepresentGrant";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
