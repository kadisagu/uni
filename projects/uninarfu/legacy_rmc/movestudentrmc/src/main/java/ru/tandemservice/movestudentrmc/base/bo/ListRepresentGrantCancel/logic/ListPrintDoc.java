package ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantCancel.logic;

import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudentrmc.docord.IListPrintDoc;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.*;

public class ListPrintDoc implements IListPrintDoc
{

    private Map<Student, OrderParagraphInfo> hashMap = new HashMap<>();

    public final String TEMPLATE_HEADER = "list.rep.grant.cancel.header";
    public final String TEMPLATE_PARAG = "list.rep.grant.cancel.parag";
    public final String TEMPLATE_ADDON = "list.rep.grant.cancel.addon";

    public RtfDocument creationPrintDocOrder(List<? extends ListRepresent> listOfRepresents)
    {
        PrintTemplate templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(PrintTemplate.class, TEMPLATE_HEADER);
        RtfDocument templateHeader = new RtfReader().read(templateDocument.getContent());

        templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(PrintTemplate.class, TEMPLATE_PARAG);
        RtfDocument templateParag = new RtfReader().read(templateDocument.getContent());

        templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(PrintTemplate.class, TEMPLATE_ADDON);
        RtfDocument templateAddon = new RtfReader().read(templateDocument.getContent());

        ListOrder order = getOrder(listOfRepresents.get(0));

        Map<OrgUnit, Map<String, List<Object[]>>> dataMap = getStudentData(listOfRepresents);
        //сортировка формирующих по алфавиту
        List<OrgUnit> orgUnitList = new ArrayList<>(dataMap.keySet());
        Collections.sort(orgUnitList, new EntityComparator<>(new EntityOrder(OrgUnit.title())));

        RtfDocument result = templateHeader.getClone();
        result.setSettings(templateParag.getSettings());
        IRtfElement paragTemp = UniRtfUtil.findElement(result.getElementList(), "parag");

        RtfDocument res = RtfBean.getElementFactory().createRtfDocument();
        res.setHeader(templateParag.getHeader());
        res.setSettings(templateParag.getSettings());
        res.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.TAB));

        int formCounter = 0;
        int addonCounter = 0;
        List<RtfDocument> addonList = new ArrayList<>();

        for (OrgUnit formOU : orgUnitList)
        {
            Map<String, List<Object[]>> formMap = dataMap.get(formOU);

            formCounter++;
            insertRow(res, "" + formCounter + ". По " + formOU.getDativeCaseTitle());

            int counter = 0;
            for (List<Object[]> studentList : formMap.values())
            {
                counter++;
                addonCounter++;
                RtfDocument par = getParagraph(templateParag, formCounter, counter, addonCounter, studentList);


                res.getElementList().addAll(par.getElementList());

                addonList.add(getAddon(templateAddon, addonCounter, studentList, order));
            }
        }

        //основания
        res.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
        insertRow(res, "Основание: " + UtilPrintSupport.getBasementTitles(listOfRepresents));
        result.getElementList().addAll(result.getElementList().indexOf(paragTemp), res.getElementList());

        result.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));

        for (int i = 0; i < addonList.size(); i++)
        {
            result.getElementList().addAll(addonList.get(i).getElementList());
            if (i + 1 < addonList.size())
                result.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
        }
        SharedRtfUtil.removeParagraphsWithTagsRecursive(result, Collections.singletonList("parag"), false, false);
        return result;
    }

    private RtfDocument getParagraph(RtfDocument template, int formCounter, int counter, int addonCounter, List<Object[]> studentList)
    {
        RtfDocument result = template.getClone();
        ListRepresentGrantCancel listRepresent = (ListRepresentGrantCancel) studentList.get(0)[1];
        Student student = (Student) studentList.get(0)[0];
        for (Object[] o : studentList)
        {
            Student s = (Student) o[0];
            hashMap.put(s, new OrderParagraphInfo(formCounter, counter));
        }
        RtfInjectModifier im = new RtfInjectModifier();

        im.put("i", "" + formCounter);
        im.put("j", "" + counter);
        im.put("addonNumber", "" + addonCounter);

        im.put("date", UtilPrintSupport.getDateFormatterWithMonthString(listRepresent.getDateCancelPayment()));
        im.put("grantView", listRepresent.getGrantView().getGenitive());
        im.put("developForm", UtilPrintSupport.getDevelopFormGen(student.getEducationOrgUnit().getDevelopForm()));
        im.put("compensationType", student.getCompensationType().getCode().equals(UniDefines.COMPENSATION_TYPE_BUDGET) ? "за счет средств федерального бюджета" : "по договору");

        im.modify(result);
        return result;
    }

    private RtfDocument getAddon(RtfDocument template, int addonCounter, List<Object[]> studentList, ListOrder order)
    {
        RtfDocument result = template.getClone();

        ListRepresentGrantCancel listRepresent = (ListRepresentGrantCancel) studentList.get(0)[1];
        Student firstStudent = (Student) studentList.get(0)[0];

        RtfInjectModifier im = new RtfInjectModifier();
        im.put("addonNumber", "" + addonCounter);
        im.put("orderDate", order != null ? UtilPrintSupport.getDateFormatterWithMonthStringAndYear(order.getCommitDate()) : new RtfString().append(""));
        im.put("orderNumber", order != null ? order.getNumber() : "");
        im.put("developForm", UtilPrintSupport.getDevelopFormGen(firstStudent.getEducationOrgUnit().getDevelopForm()));
        im.put("formOU", firstStudent.getEducationOrgUnit().getFormativeOrgUnit().getGenitiveCaseTitle());
        im.put("compensationType", firstStudent.getCompensationType().getCode().equals(UniDefines.COMPENSATION_TYPE_BUDGET) ? "за счет федерального бюджета" : "по договору");
        
        //-------------------------------------------------------------------------------------------------------
        //im.put("grantView", listRepresent.getGrantView().getGenitive());

        String grandView = listRepresent.getGrantView().getGenitive();
        if(grandView != null){
            boolean insertComma = false;
            if (grandView.contains(", обучающ")
                    || grandView.contains(", поступивш")
                    || grandView.contains(", находящ")
                    || grandView.contains(", осуществляющ")
                    || grandView.contains(", соответствующ")
                    || grandView.contains(", распределяем")) {
                insertComma = true;
            }
            im.put("grantView", grandView.concat(insertComma ? ", " : ""));
        }
        else{
        	im.put("grantView", "");
        }
        
        im.put("signPostGenitive", order != null ? order.getSignPostGenitive() : "");
        //-------------------------------------------------------------------------------------------------------        
        im.put("date", UtilPrintSupport.getDateFormatterWithMonthString(listRepresent.getDateCancelPayment()));
        im.modify(result);

        List<String[]> tableList = new ArrayList<>();
        int counter = 1;
        for (Object[] arr : studentList)
        {
            Student student = (Student) arr[0];

            String[] row = new String[4];
            row[0] = "" + counter++;
            row[1] = student.getBookNumber() != null ? student.getBookNumber() : "";
            row[2] = student.getPerson().getFullFio();
            row[3] = student.getCourse().getTitle();

            tableList.add(row);
        }

        RtfTableModifier tm = new RtfTableModifier();
        tm.put("T", tableList.toArray(new String[][]{}));
        tm.modify(result);

        return result;
    }

    private void insertRow(RtfDocument document, String text)
    {
        document.getElementList().add(RtfBean.getElementFactory().createRtfText(text));
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
    }

    private String getKey(Student student, ListRepresentGrantCancel represent)
    {
        return new StringBuilder()
                .append(student.getEducationOrgUnit().getDevelopForm().getId())
                .append(student.getCompensationType().getId())
                .append(represent.getGrantView().getId())
                .append(represent.getDateCancelPayment().getTime())
                .toString();
    }

    private Map<OrgUnit, Map<String, List<Object[]>>> getStudentData(List<? extends ListRepresent> listOfRepresents)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(RelListRepresentStudents.class, "rel")
                .where(DQLExpressions.in(
                        DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("rel")),
                        listOfRepresents
                ))
                .joinEntity(
                        "rel",
                        DQLJoinType.inner,
                        ListRepresentGrantCancel.class,
                        "rep",
                        DQLExpressions.eq(
                                DQLExpressions.property(ListRepresentGrantCancel.id().fromAlias("rep")),
                                DQLExpressions.property(RelListRepresentStudents.representation().id().fromAlias("rel"))
                        ))
                .column(DQLExpressions.property(RelListRepresentStudents.student().fromAlias("rel")))
                .column("rep")
                .order(DQLExpressions.property(RelListRepresentStudents.student().course().fromAlias("rel")))
                .order(DQLExpressions.property(RelListRepresentStudents.student().person().identityCard().fullFio().fromAlias("rel")));

        List<Object[]> studentDataList = UniDaoFacade.getCoreDao().getList(dql);

        Map<OrgUnit, Map<String, List<Object[]>>> dataMap = new HashMap<>();
        for (Object[] arr : studentDataList)
        {
            Student student = (Student) arr[0];
            ListRepresentGrantCancel represent = (ListRepresentGrantCancel) arr[1];

            OrgUnit formOU = student.getEducationOrgUnit().getFormativeOrgUnit();
            if (dataMap.get(formOU) == null)
                dataMap.put(formOU, new HashMap<>());

            String key = getKey(student, represent);
            if (dataMap.get(formOU).get(key) == null)
                dataMap.get(formOU).put(key, new ArrayList<>());

            dataMap.get(formOU).get(key).add(arr);
        }

        return dataMap;
    }

    private ListOrder getOrder(ListRepresent listRepresent)
    {
        List<ListOrdListRepresent> orderList = UniDaoFacade.getCoreDao().getList(ListOrdListRepresent.class, ListOrdListRepresent.representation(), listRepresent);
        return orderList.isEmpty() ? null : orderList.get(0).getOrder();
    }

    @Override
    public Map<Student, OrderParagraphInfo> getParagInfomap()
    {
        // TODO Auto-generated method stub
        return hashMap;
    }
}
