package ru.tandemservice.movestudentrmc.docord.util;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab.MonthWrapper;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.StuGrantStatusCodes;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MoveStudentUtil {

    public static boolean isChecked(List<Student> students, CompensationType newType, DevelopForm newForm) {

        for (Student student : students) {
            if (student.getCompensationType().isBudget() && student.getEducationOrgUnit().getDevelopForm().getCode().equals(DevelopFormCodes.FULL_TIME_FORM)) {
                if (newType == null && newForm == null)
                    return true;
                else if (newType != null && !newType.isBudget())
                    return true;
                else if (newForm != null && !newForm.getCode().equals(DevelopFormCodes.FULL_TIME_FORM))
                    return true;
            }
        }

        return false;
    }

    public static List<Student> checkStudentList(List<Student> students, CompensationType newType, DevelopForm newForm) {

        List<Student> list = new ArrayList<Student>();

        for (Student student : students) {
            if (student.getCompensationType().isBudget())
                if (newType != null && !newType.isBudget()) {
                    list.add(student);
                    continue;
                }
            if (student.getEducationOrgUnit().getDevelopForm().getCode().equals(DevelopFormCodes.FULL_TIME_FORM))
                if (newForm != null && !newForm.getCode().equals(DevelopFormCodes.FULL_TIME_FORM)) {
                    list.add(student);
                    continue;
                }
        }

        return list;
    }

    public static String checked(Date date, boolean exclude) {
        String str = exclude ? "отчисление" : "перевод";
        StringBuilder retVal = new StringBuilder();
        if (CommonBaseDateUtil.isBefore(date, new Date()))
            retVal.append("Не рекомендуется проводить " + str + " задним числом. ");

        if (CoreDateUtils.getDayOfMonth(date) == 1)
            retVal.append("Не рекомендуется производить " + str + " с 1-го числа месяца");

        return retVal.toString();
    }

    /**
     * Возвращает список стипендий назначенных студенту со статусом "Выплата"
     *
     * @param students  - список студентов
     * @param monthList - период назначения стипендии
     * @param academ    - академическая стипендия
     * @param social    - социальная стипендия
     *
     * @return
     */

    public static List<StudentGrantEntity> getSrudentGrantEntityList(List<Student> students, List<String> monthList, boolean academ, boolean social) {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentGrantEntity.class, "sge")
                .joinEntity("sge", DQLJoinType.left, RelTypeGrantView.class, "rel",
                            DQLExpressions.eq(
                                    DQLExpressions.property(StudentGrantEntity.view().id().fromAlias("sge")),
                                    DQLExpressions.property(RelTypeGrantView.view().id().fromAlias("rel")))
                )
                .where(DQLExpressions.eq(DQLExpressions.property(StudentGrantEntity.status().code().fromAlias("sge")),
                                         DQLExpressions.value(StuGrantStatusCodes.CODE_1)))
                .where(DQLExpressions.in(DQLExpressions.property(StudentGrantEntity.student().fromAlias("sge")),
                                         students))
                .where(DQLExpressions.in(DQLExpressions.property(StudentGrantEntity.month().fromAlias("sge")), monthList))
                .where(DQLExpressions.or(DQLExpressions.eq(DQLExpressions.property(StudentGrantEntity.tmp().fromAlias("sge")),
                                                           DQLExpressions.value(Boolean.FALSE)),
                                         DQLExpressions.isNull(DQLExpressions.property(StudentGrantEntity.tmp().fromAlias("sge")))
                       )
                )
                .column("sge");

        if (academ)
            builder.where(DQLExpressions.eqValue(DQLExpressions.property(RelTypeGrantView.type().academ().fromAlias("rel")), Boolean.TRUE));

        if (social)
            builder.where(DQLExpressions.eqValue(DQLExpressions.property(RelTypeGrantView.type().social().fromAlias("rel")), Boolean.TRUE));


        List<StudentGrantEntity> sge = IUniBaseDao.instance.get().getList(builder);
        return sge;
    }

    public static List<StudentGrantEntity> getSrudentGrantEntityList(List<Student> students, Date date) {

    	//----------------------------------------------------------------------------------------------------------
    	/*
        EducationYear educationYear = getEduYearByDate(date);

        if (CoreDateUtils.getDayOfMonth(date) == 1)
            return getSrudentGrantEntityList(students, getMonthList(date, educationYear), false, false);
        else
            return getSrudentGrantEntityList(students, getMonthList(date, educationYear), true, false);
        */
    	
    	//Изменения в связи с расширением периода выплат (снято ограничение учебным годом)
        if (CoreDateUtils.getDayOfMonth(date) == 1)
            return getSrudentGrantEntityList(students, getMonthList(date, students), false, false);
        else
            return getSrudentGrantEntityList(students, getMonthList(date, students), true, false);    	
    	//----------------------------------------------------------------------------------------------------------
        
    }

    public static EducationYear getEduYearByDate(Date date) {
        if (date == null) return null;

        int year = CoreDateUtils.getYear(date);
        if (CoreDateUtils.getMonth(date) <= Calendar.AUGUST)
            year -= 1;

        return IUniBaseDao.instance.get().get(EducationYear.class, EducationYear.intValue(), year);
    }

    public static List<String> getMonthList(Date date, EducationYear educationYear) {

        Calendar lastYearDate = Calendar.getInstance();
        lastYearDate.set(educationYear.getIntValue() + 1, Calendar.AUGUST, 31, 0, 0, 0);//31.08.year+1

        if (CoreDateUtils.getDayOfMonth(date) == 1)
            return MonthWrapper.getMonthsList(date, lastYearDate.getTime(), educationYear);
        else {
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            c.add(Calendar.MONTH, 1);
            return MonthWrapper.getMonthsList(c.getTime(), lastYearDate.getTime(), educationYear);
        }
    }
    
    //--------------------------------------------------------------------------------------------------------------------------------------
    public static List<String> getMonthList(Date date, List<Student> students) {

        //Последний учебный год назначения выплат
        DQLSelectBuilder lastYearBuilder = new DQLSelectBuilder()
        		.fromEntity(StudentGrantEntity.class, "sge")
        		.where(eq(property(StudentGrantEntity.status().code().fromAlias("sge")), value(StuGrantStatusCodes.CODE_1)))
        		.where(in(property(StudentGrantEntity.student().fromAlias("sge")), students))
        		.column(property(StudentGrantEntity.eduYear().intValue().fromAlias("sge")))
        		.order(property(StudentGrantEntity.eduYear().intValue().fromAlias("sge")), OrderDirection.desc)
        		.top(1)
        		;
        
        int lastYear = getEduYearByDate(date).getIntValue();	//значение по умолчанию (если не было выплат студентам)
        List<Integer> lastYearList = IUniBaseDao.instance.get().getList(lastYearBuilder);
        if(!lastYearList.isEmpty()) {
        	lastYear = lastYearList.get(0);
        }
        
        //Последний день последнего учебного года назначения выплат
        Calendar lastYearDate = Calendar.getInstance();
        lastYearDate.set(lastYear + 1, Calendar.AUGUST, 31, 0, 0, 0);	//31.08.year+1

        if (CoreDateUtils.getDayOfMonth(date) == 1)
            return MonthWrapper.getMonthsList(date, lastYearDate.getTime());
        else {
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            c.add(Calendar.MONTH, 1);
            return MonthWrapper.getMonthsList(c.getTime(), lastYearDate.getTime());
        }
    }    
    //--------------------------------------------------------------------------------------------------------------------------------------

    public static EducationOrgUnit getEducationOrgUnit(EducationOrgUnit educationOrgUnit) {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "eou")
                .where(DQLExpressions.eqValue(DQLExpressions.property(EducationOrgUnit.formativeOrgUnit().fromAlias("eou")), educationOrgUnit.getFormativeOrgUnit()))
                .where(DQLExpressions.eqValue(DQLExpressions.property(EducationOrgUnit.territorialOrgUnit().fromAlias("eou")), educationOrgUnit.getTerritorialOrgUnit()))
                .where(DQLExpressions.eqValue(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().fromAlias("eou")), educationOrgUnit.getEducationLevelHighSchool()))
                .where(DQLExpressions.eqValue(DQLExpressions.property(EducationOrgUnit.developForm().fromAlias("eou")), educationOrgUnit.getDevelopForm()))
                .where(DQLExpressions.eqValue(DQLExpressions.property(EducationOrgUnit.developCondition().fromAlias("eou")), educationOrgUnit.getDevelopCondition()))
                .where(DQLExpressions.eqValue(DQLExpressions.property(EducationOrgUnit.developPeriod().fromAlias("eou")), educationOrgUnit.getDevelopPeriod()))
                .where(DQLExpressions.eqValue(DQLExpressions.property(EducationOrgUnit.developTech().fromAlias("eou")), educationOrgUnit.getDevelopTech()));

        List<EducationOrgUnit> lst = UniDaoFacade.getCoreDao().getList(builder);

        if (!lst.isEmpty())
            return lst.get(0);
        else
            return null;
    }

    public static List<RelListRepresentStudents> getStudentList(ListRepresent listRepresent) {
        return DataAccessServices.dao().getList(RelListRepresentStudents.class, RelListRepresentStudents.representation(), listRepresent);
    }

    public static ListOrder getOrder(ListRepresent listRepresent) {
        List<ListOrdListRepresent> orderList = UniDaoFacade.getCoreDao().getList(ListOrdListRepresent.class, ListOrdListRepresent.representation(), listRepresent);

        if (!orderList.isEmpty())
            return orderList.get(0).getOrder();
        else
            return null;
    }

    public static DocumentOrder getOrder(Representation representation) {

        DocOrdRepresent docOrdRepresent = UniDaoFacade.getCoreDao().get(DocOrdRepresent.class, DocOrdRepresent.representation(), representation);

        if (docOrdRepresent != null) {
            return docOrdRepresent.getOrder();
        }
        else
            return null;
    }

    public static String getFIO_G(IdentityCard identityCard) {

        String lastName = PersonManager.instance().declinationDao().getDeclinationLastName(identityCard.getLastName(), GrammaCase.GENITIVE, identityCard.getPerson().isMale());
        String firstName = identityCard.getFirstName();
        String middleName = identityCard.getMiddleName();
        StringBuffer str = (new StringBuffer()).append(lastName).append(" ");
        if (StringUtils.isNotEmpty(firstName))
            str.append(firstName.substring(0, 1).toUpperCase()).append(".");
        if (StringUtils.isNotEmpty(middleName))
            str.append(middleName.substring(0, 1).toUpperCase()).append(".");

        return str.toString();
    }

    public static String getDeclinationIOF(IdentityCard identityCard, GrammaCase grammaCase) {

        String lastName = PersonManager.instance().declinationDao().getDeclinationLastName(identityCard.getLastName(), grammaCase, identityCard.getPerson().isMale());
        String firstName = identityCard.getFirstName();
        String middleName = identityCard.getMiddleName();
        StringBuffer str = new StringBuffer();
        if (StringUtils.isNotEmpty(firstName))
            str.append(firstName.substring(0, 1).toUpperCase()).append(".");
        if (StringUtils.isNotEmpty(middleName))
            str.append(middleName.substring(0, 1).toUpperCase()).append(".");
        str.append(lastName);
        return str.toString();
    }
    
    //--------------------------------------------------------------------------------------------------------------------------
    public static RtfString getDeclinationIO_F(IdentityCard identityCard, GrammaCase grammaCase) {
    	
    	RtfString rtfStr = new RtfString();

        String lastName = PersonManager.instance().declinationDao().getDeclinationLastName(identityCard.getLastName(), grammaCase, identityCard.getPerson().isMale());
        String firstName = identityCard.getFirstName();
        String middleName = identityCard.getMiddleName();

        if (StringUtils.isNotEmpty(firstName))
        	rtfStr.append(firstName.substring(0, 1).toUpperCase()).append(".");
        if (StringUtils.isNotEmpty(middleName))
        	rtfStr.append(middleName.substring(0, 1).toUpperCase()).append(".");
        rtfStr.append(IRtfData.SYMBOL_TILDE);
        rtfStr.append(lastName);
        
        return rtfStr;
    }    
    //--------------------------------------------------------------------------------------------------------------------------
}
