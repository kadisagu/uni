package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;

import java.util.List;

public class EppWorkPlanDSHandler extends DefaultComboDataSourceHandler {
    public static final String PLAN_VERSION = "planVersion";
    public static final String TERM = "term";
    public static final String EPP_YEAR = "eppYear";

    public EppWorkPlanDSHandler(String s) {
        super(s, EppWorkPlan.class, EppWorkPlan.number());
        setOrderByProperty(EppWorkPlan.number().s());
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep) {
        super.prepareConditions(ep);

        EppEduPlanVersion version = ep.context.get(PLAN_VERSION);
        DevelopGridTerm term = ep.context.get(TERM);
        EppYearEducationProcess year = ep.context.get(EPP_YEAR);

        MQBuilder builder = new MQBuilder(EppWorkPlan.ENTITY_CLASS, "wp")
                .add(MQExpression.eq("wp", EppWorkPlan.parent().eduPlanVersion(), version))
                .add(MQExpression.eq("wp", EppWorkPlan.term(), term != null ? term.getTerm() : null));
        if (year != null)
            builder
                    .addJoin("wp", EppWorkPlan.year(), "y")
                    .add(MQExpression.eq("y", EppYearEducationProcess.educationYear(), year.getEducationYear()));

        builder.getSelectAliasList().clear();
        builder.addSelect("wp.id");
        List<Long> ids = builder.getResultList(ep.context.getSession());

        ep.dqlBuilder.where(DQLExpressions.in(DQLExpressions.property(EppWorkPlan.id().fromAlias("e")), ids));
    }

}
