package ru.tandemservice.movestudentrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Дополнительный статус студента (справочник)"
 * Имя сущности : studentCustomStateCI
 * Файл data.xml : movementstudent.data.xml
 */
public interface StudentCustomStateCICodes
{
    /** Константа кода (code) элемента : Каникулы (title) */
    String HOLIDAYS = "movestudentrmc.holidays";
    /** Константа кода (code) элемента : Практика (title) */
    String PRACTICE = "movestudentrmc.practice";
    /** Константа кода (code) элемента : Поездка (title) */
    String TRIP = "movestudentrmc.trip";
    /** Константа кода (code) элемента : Академический отпуск по мед. показаниям (title) */
    String WEEKEND_HONEY_INDICATIONS = "movestudentrmc.weekend.honey.ind";

    Set<String> CODES = ImmutableSet.of(HOLIDAYS, PRACTICE, TRIP, WEEKEND_HONEY_INDICATIONS);
}
