package ru.tandemservice.movestudentrmc.base.bo.RelationSettingsOSSP.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonOSSP;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;

import java.util.List;

public interface IRelationSettingsOSSPManagerModifyDAO extends INeedPersistenceSupport {
    void save(RepresentationType typeRepresentation, List<RepresentRelOSSP> reasons);

    void update(RepresentationType typeRepresentation, List<RepresentRelOSSP> reasons, List<RelRepresentationReasonOSSP> osoList);
}
