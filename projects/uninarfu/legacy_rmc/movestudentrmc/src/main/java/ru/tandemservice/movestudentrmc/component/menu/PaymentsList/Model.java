package ru.tandemservice.movestudentrmc.component.menu.PaymentsList;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudentrmc.entity.IRepresentOrder;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;

import java.util.List;
import java.util.Map;

public class Model {

    private DynamicListDataSource<Wrapper> dataSource;
    private IDataSettings settings;

    private Map<Long, List<IRepresentOrder>> orderMap;

    private Map<Long, List<IRepresentOrder>> cancelOrderMap;

    private Map<Long, List<IRepresentOrder>> suspendOrderMap;

    private Map<Long, List<IRepresentOrder>> resumeOrderMap;

    private IMultiSelectModel grantViewModel;
    private IMultiSelectModel studentStatusModel;
    private IMultiSelectModel courseModel;
    private ISelectModel compensationTypeModel;
    private IMultiSelectModel groupModel;
    private IMultiSelectModel studentCategoryModel;
    private IMultiSelectModel formativeOrgUnitModel;
    private IMultiSelectModel eduLevelModel;
    private IMultiSelectModel qualificationsModel;
    private IMultiSelectModel developFormModel;
    private IMultiSelectModel developConditionModel;
    private IMultiSelectModel developPeriodModel;

    public DynamicListDataSource<Wrapper> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<Wrapper> dataSource) {
        this.dataSource = dataSource;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public IMultiSelectModel getGrantViewModel() {
        return grantViewModel;
    }

    public void setGrantViewModel(IMultiSelectModel grantViewModel) {
        this.grantViewModel = grantViewModel;
    }

    public IMultiSelectModel getStudentStatusModel() {
        return studentStatusModel;
    }

    public void setStudentStatusModel(IMultiSelectModel studentStatusModel) {
        this.studentStatusModel = studentStatusModel;
    }

    public IMultiSelectModel getCourseModel() {
        return courseModel;
    }

    public void setCourseModel(IMultiSelectModel courseModel) {
        this.courseModel = courseModel;
    }

    public ISelectModel getCompensationTypeModel() {
        return compensationTypeModel;
    }

    public void setCompensationTypeModel(ISelectModel compensationTypeModel) {
        this.compensationTypeModel = compensationTypeModel;
    }

    public IMultiSelectModel getGroupModel() {
        return groupModel;
    }

    public void setGroupModel(IMultiSelectModel groupModel) {
        this.groupModel = groupModel;
    }

    public IMultiSelectModel getStudentCategoryModel() {
        return studentCategoryModel;
    }

    public void setStudentCategoryModel(IMultiSelectModel studentCategoryModel) {
        this.studentCategoryModel = studentCategoryModel;
    }

    public IMultiSelectModel getFormativeOrgUnitModel() {
        return formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(IMultiSelectModel formativeOrgUnitModel) {
        this.formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public IMultiSelectModel getEduLevelModel() {
        return eduLevelModel;
    }

    public void setEduLevelModel(IMultiSelectModel eduLevelModel) {
        this.eduLevelModel = eduLevelModel;
    }

    public IMultiSelectModel getQualificationsModel() {
        return qualificationsModel;
    }

    public void setQualificationsModel(IMultiSelectModel qualificationsModel) {
        this.qualificationsModel = qualificationsModel;
    }

    public IMultiSelectModel getDevelopFormModel() {
        return developFormModel;
    }

    public void setDevelopFormModel(IMultiSelectModel developFormModel) {
        this.developFormModel = developFormModel;
    }

    public IMultiSelectModel getDevelopConditionModel() {
        return developConditionModel;
    }

    public void setDevelopConditionModel(IMultiSelectModel developConditionModel) {
        this.developConditionModel = developConditionModel;
    }

    public IMultiSelectModel getDevelopPeriodModel() {
        return developPeriodModel;
    }

    public void setDevelopPeriodModel(IMultiSelectModel developPeriodModel) {
        this.developPeriodModel = developPeriodModel;
    }

    public List<OrgUnit> getFormativeOrgUnitList() {
        return this.getSettings().get("formativeOrgUnitList");
    }

    public List<EducationLevelsHighSchool> getEducationLevelsHighSchools() {
        return this.getSettings().get("eduLevelList");
    }

    public List<DevelopForm> getDevelopForms() {
        return this.getSettings().get("developFormList");
    }

    public List<DevelopCondition> getDevelopConditions() {
        return this.getSettings().get("developConditionList");
    }


    public Map<Long, List<IRepresentOrder>> getOrderMap() {
        return orderMap;
    }

    public void setOrderMap(Map<Long, List<IRepresentOrder>> orderMap) {
        this.orderMap = orderMap;
    }

    public Map<Long, List<IRepresentOrder>> getCancelOrderMap() {
        return cancelOrderMap;
    }

    public void setCancelOrderMap(Map<Long, List<IRepresentOrder>> cancelOrderMap) {
        this.cancelOrderMap = cancelOrderMap;
    }

    public Map<Long, List<IRepresentOrder>> getSuspendOrderMap() {
        return suspendOrderMap;
    }

    public void setSuspendOrderMap(Map<Long, List<IRepresentOrder>> suspendOrderMap) {
        this.suspendOrderMap = suspendOrderMap;
    }

    public Map<Long, List<IRepresentOrder>> getResumeOrderMap() {
        return resumeOrderMap;
    }

    public void setResumeOrderMap(Map<Long, List<IRepresentOrder>> resumeOrderMap) {
        this.resumeOrderMap = resumeOrderMap;
    }


    public static class OrderInfo {
        private String title;
        private Long id;

        public OrderInfo(Long id, String title) {
            this.title = title;
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }
    }

}
