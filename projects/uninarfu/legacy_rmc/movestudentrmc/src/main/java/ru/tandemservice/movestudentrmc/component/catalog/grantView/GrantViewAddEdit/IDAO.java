package ru.tandemservice.movestudentrmc.component.catalog.grantView.GrantViewAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;

public interface IDAO extends IDefaultCatalogAddEditDAO<GrantView, Model> {

}
