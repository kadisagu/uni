package ru.tandemservice.movestudentrmc.base.bo.DORepresentGrantCancelAndDestination.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentDAO;
import ru.tandemservice.movestudentrmc.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.movestudentrmc.entity.catalog.StuGrantStatus;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentGrantTypeCodes;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.StuGrantStatusCodes;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class DORepresentGrantCancelAndDestinationDAO extends AbstractDORepresentDAO implements IDORepresentGrantCancelAndDestinationDAO {

    @Override
    public Representation createRepresentObject(RepresentationType type,
                                                List<Student> student)
    {
        RepresentGrantCancelAndDestination represent = new RepresentGrantCancelAndDestination();
        represent.setType(type);
        return represent;
    }

    @Override
    public void buildBodyExtract(Representation representationBase,
                                 String itemFac, RtfDocument docCommon)
    {
        RepresentGrantCancelAndDestination represent = (RepresentGrantCancelAndDestination) representationBase;
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        RtfInjectModifier modifier = new RtfInjectModifier();

        UtilPrintSupport.setParNum(docCommon, itemFac);

        UtilPrintSupport.injectModifierExtract(modifier, docRepresent);

        injectModifier(modifier, represent, docRepresent.getStudent());

        modifier.modify(docCommon);

    }

    @Override
    public void buildBodyRepresent(Representation representationBase,
                                   RtfDocument document, AtomicInteger parNumber, AtomicInteger extractNumber)
    {
        super.buildBodyRepresent(representationBase, document, parNumber, extractNumber);

        RepresentGrantCancelAndDestination represent = (RepresentGrantCancelAndDestination) representationBase;
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        Student student = UtilPrintSupport.getStudent(represent);
        RtfInjectModifier modifier = new RtfInjectModifier();

        if (parNumber != null && extractNumber != null) {
            StringBuilder str = new StringBuilder();
            str.append(parNumber.intValue())
                    .append(".")
                    .append(extractNumber.intValue())
                    .append(". ");
            modifier.put("parNumber2", str.toString());
            extractNumber.getAndAdd(1);
        }
        else {
            modifier.put("parNumber2", "");
        }

        UtilPrintSupport.injectModifierRepresent(modifier, docRepresent, student);

        injectModifier(modifier, represent, student);

        if (StringUtils.isEmpty(modifier.getStringValue("basics")))
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Arrays.asList("basics"), false, false);

        modifier.modify(document);

    }

    protected void injectModifier(RtfInjectModifier modifier, RepresentGrantCancelAndDestination represent, Student student) {

        modifier.put("studentTitle", UtilPrintSupport.printSeparatedFIO(student.getPerson().getIdentityCard(), GrammaCase.DATIVE, student.getPerson().isMale()));
        //--------------------------------------------------------------------------------------
        //modifier.put("studentSex_D", student.getPerson().isMale() ? "студенту" : "студентке");
        //--------------------------------------------------------------------------------------
        modifier.put("beginDate", UtilPrintSupport.getDateFormatterWithMonthString(represent.getBeginDate()));
        modifier.put("endDate", UtilPrintSupport.getDateFormatterWithMonthString(represent.getEndDate()));
        modifier.put("grantView", represent.getGrantView().getAccusative());

        //--------------------------------------------------------------------------------
        //modifier.put("student", student.getPerson().isMale() ? "студенту" : "студентки");
        //--------------------------------------------------------------------------------
        modifier.put("FIO", PersonManager.instance().declinationDao().getDeclinationFIO(student.getPerson().getIdentityCard(), GrammaCase.GENITIVE));

        List<DocRepresentOrderCancel> orders = IUniBaseDao.instance.get().getList(DocRepresentOrderCancel.class, DocRepresentOrderCancel.representation().id(), represent.getId());
        RtfString s = new RtfString();
        for (DocRepresentOrderCancel rel : orders) {
            s.append("приказ от").append(IRtfData.SYMBOL_TILDE);
            if (rel.getOrder() != null)
                UtilPrintSupport.getDateFormatterWithMonthStringAndYear(rel.getOrder().getCommitDate(), s);
            s.append(" №").append(IRtfData.SYMBOL_TILDE);
            if (rel.getOrder() != null)
                s.append(rel.getOrder().getNumber());
            s.append(" «")
                    .append(rel.getTitle())
                    .append("»")
                    .append(", ");
        }
        if (!s.toList().isEmpty())
            s.toList().remove(s.toList().size() - 1);
        modifier.put("orders", s);
    }

    @Override
    public boolean doCommit(Representation represent, ErrorCollector error) {
        if (!super.doCommit(represent, error))
            return false;

        DocumentOrder order = getOrder(represent);

        StuGrantStatus status = UniDaoFacade.getCoreDao().get(StuGrantStatus.class, StuGrantStatus.code(), StuGrantStatusCodes.CODE_3);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RelRepresentGrants.class, "sge")
                .where(DQLExpressions.eqValue(DQLExpressions.property(RelRepresentGrants.representation().fromAlias("sge")), represent))
                .column(DQLExpressions.property(RelRepresentGrants.studentGrantEntity().fromAlias("sge")));
        List<StudentGrantEntity> sgelist = IUniBaseDao.instance.get().getList(builder);

        for (StudentGrantEntity sge : sgelist) {
            //сохраним старые данные для отката
            RepresentGrantsOld grantsOld = new RepresentGrantsOld();
            grantsOld.setRepresentation(represent);
            grantsOld.setStudentGrantEntityId(sge.getId());
            grantsOld.setOrderDateOld(sge.getOrderDate());
            grantsOld.setOrderNumberOld(sge.getOrderNumber());
            getSession().saveOrUpdate(grantsOld);
            //сохраним данные приказа
            sge.setStatus(status);
            sge.setOrderDate(order.getCommitDate());
            sge.setOrderNumber(order.getNumber());
            getSession().saveOrUpdate(sge);
        }

        MoveStudentDaoFacade.getGrantDAO().saveGrantHistory(sgelist, represent, RepresentGrantTypeCodes.CANCEL);

        MoveStudentDaoFacade.getGrantDAO().commitStudentGrants(represent, order);

        return true;
    }

    @Override
    public boolean doRollback(Representation represent, ErrorCollector error) {
        if (!super.doRollback(represent, error))
            return false;

        DocumentOrder order = getOrder(represent);

        StuGrantStatus status = UniDaoFacade.getCoreDao().get(StuGrantStatus.class, StuGrantStatus.code(), StuGrantStatusCodes.CODE_1);

        List<RepresentGrantsOld> grantsOlds = UniDaoFacade.getCoreDao().getList(RepresentGrantsOld.class, RepresentGrantsOld.representation(), represent);

        for (RepresentGrantsOld grantOld : grantsOlds) {
            StudentGrantEntity sge = UniDaoFacade.getCoreDao().get(grantOld.getStudentGrantEntityId());

            sge.setStatus(status);
            sge.setOrderDate(grantOld.getOrderDateOld());
            sge.setOrderNumber(grantOld.getOrderNumberOld());

            getSession().saveOrUpdate(sge);

            getSession().delete(grantOld);
        }

        MoveStudentDaoFacade.getGrantDAO().deleteGrantHistory(represent);

        MoveStudentDaoFacade.getGrantDAO().rollbackStudentGrants(represent, order);

        return true;
    }

    @Override
    public GrammaCase getGrammaCase() {
        return GrammaCase.DATIVE;
    }

    @Override
    public void saveStudentGrantEntity(List<StudentGrantEntity> sgeList, RepresentGrantCancelAndDestination represent) {
        for (StudentGrantEntity sge : sgeList) {
            RelRepresentGrants rel = new RelRepresentGrants();
            rel.setRepresentation(represent);
            rel.setStudentGrantEntity(sge);

            getSession().saveOrUpdate(rel);
        }
    }

    @Override
    public void deleteStudentGrantEntity(Long representId) {
        new DQLDeleteBuilder(RelRepresentGrants.class)
                .where(DQLExpressions.eqValue(DQLExpressions.property(RelRepresentGrants.representation().id()), representId))
                .createStatement(getSession()).execute();
    }

    @Override
    public void saveOrderCancel(List<DocRepresentOrderCancel> ordersList) {
        for (DocRepresentOrderCancel rel : ordersList) {
            getSession().saveOrUpdate(rel);
        }
    }

    
    @Override
    public void deleteOrderCancel(Long representId) {
        new DQLDeleteBuilder(DocRepresentOrderCancel.class)
                .where(DQLExpressions.eqValue(DQLExpressions.property(DocRepresentOrderCancel.representation()), representId))
                .createStatement(getSession()).execute();
    }
    
}
