package ru.tandemservice.movestudentrmc.base.bo.DORepresentDiplomAndExclude.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentDAO;
import ru.tandemservice.movestudentrmc.docord.util.MoveStudentUtil;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.RepresentDiplomAndExclude;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntity;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class DORepresentDiplomAndExcludeDAO extends AbstractDORepresentDAO {

    @Override
    public Representation createRepresentObject(RepresentationType type, List<Student> studentList) {

        Student student = studentList.get(0);
        RepresentDiplomAndExclude represent = new RepresentDiplomAndExclude();
        represent.setType(type);

        represent.setFormativeOrgUnit(student.getEducationOrgUnit().getFormativeOrgUnit());
        represent.setEducationLevelsHighSchool(student.getEducationOrgUnit().getEducationLevelHighSchool());

        return represent;
    }

    @Override
    public boolean doCommit(Representation represent, ErrorCollector error) {
        if (!super.doCommit(represent, error))
            return false;

        List<DocRepresentStudentBase> studentList = getStudentList(represent);
        DocRepresentStudentBase docRepresent = studentList.get(0);
        Student student = docRepresent.getStudent();

        docRepresent.setFinishedYear(student.getFinishYear());
        student.setFinishYear(CoreDateUtils.getYear(represent.getOrder().getCommitDate()));

        student.setStatus(DataAccessServices.dao().get(StudentStatus.class, StudentStatus.code().s(), "3"));
        baseUpdate(docRepresent);
        baseUpdate(student);

        List<StudentGrantEntity> sgeList = MoveStudentUtil.getSrudentGrantEntityList(Arrays.asList(student), represent.getStartDate());

        commitStudentGrantsCancel(represent, sgeList);

        return true;
    }

    @Override
    public boolean doRollback(Representation represent, ErrorCollector error) {
        if (!super.doRollback(represent, error))
            return false;

        List<DocRepresentStudentBase> studentList = getStudentList(represent);
        Student student = studentList.get(0).getStudent();
        student.setStatus(represent.getStatusOld());
        student.setFinishYear(studentList.get(0).getFinishedYear());
        baseUpdate(student);

        rollbackStudentGrantsCancel(represent);
        return true;
    }

    @Override
    public void buildBodyExtract(Representation representationBase, String itemFac, RtfDocument docCommon) {
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        UtilPrintSupport.setParNum(docCommon, itemFac);

        RtfInjectModifier modifier = new RtfInjectModifier();

        UtilPrintSupport.injectModifierExtract(modifier, docRepresent);

        modifier.modify(docCommon);
    }

    @Override
    public void buildBodyRepresent(Representation representationBase, RtfDocument document, AtomicInteger parNumber, AtomicInteger extractNumber) {
        super.buildBodyRepresent(representationBase, document, parNumber, extractNumber);
        RepresentDiplomAndExclude represent = (RepresentDiplomAndExclude) representationBase;
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        Student student = UtilPrintSupport.getStudent(represent);

        RtfInjectModifier modifier = new RtfInjectModifier();

        UtilPrintSupport.injectModifierRepresent(modifier, docRepresent, student);

        //modifier.put("studentTitle", PersonManager.instance().declinationDao().getDeclinationFIO(student.getPerson().getIdentityCard(), GrammaCase.DATIVE));
        modifier.put("studentTitle", UtilPrintSupport.printSeparatedFIO(student.getPerson().getIdentityCard(), GrammaCase.DATIVE, student.getPerson().isMale()));

        modifier.put("studentSex", student.getPerson().isMale() ? "студенту" : "студентке");

        String genderBasedDevelop = "обучающемуся";
        if (!student.getPerson().isMale()) {
            genderBasedDevelop = "обучающейся";
        }

        modifier.put("developPayment", genderBasedDevelop + " " + docRepresent.getCompensationTypeStr());

        modifier.put("diplomaQualification", represent.getEducationLevelsHighSchool().getQualificationTitleNullSafe().toLowerCase());
        modifier.put("stHonor", represent.isHasDiplomaWithHonors() ? "с отличием" : "без отличия");

        if (StringUtils.isEmpty(modifier.getStringValue("basics")))
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Arrays.asList("basics"), false, false);

        modifier.modify(document);
    }
}
