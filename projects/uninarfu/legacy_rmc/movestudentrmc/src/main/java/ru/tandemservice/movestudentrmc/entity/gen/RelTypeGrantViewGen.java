package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.RelTypeGrantView;
import ru.tandemservice.movestudentrmc.entity.catalog.Grant;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantType;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность-связь 'Тип стипендии'-'Наименование стипендии'-'Вид стипедии'
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RelTypeGrantViewGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.RelTypeGrantView";
    public static final String ENTITY_NAME = "relTypeGrantView";
    public static final int VERSION_HASH = -2081240696;
    private static IEntityMeta ENTITY_META;

    public static final String L_TYPE = "type";
    public static final String L_GRANT = "grant";
    public static final String L_VIEW = "view";
    public static final String P_JOIN_GROUP = "joinGroup";
    public static final String P_GROUP_NAME = "groupName";

    private GrantType _type;     // Тип стипендии
    private Grant _grant;     // Наименование стипендии
    private GrantView _view;     // Вид стипендии
    private boolean _joinGroup = false;     // Входит в группу
    private String _groupName;     // Имя группы

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип стипендии. Свойство не может быть null.
     */
    @NotNull
    public GrantType getType()
    {
        return _type;
    }

    /**
     * @param type Тип стипендии. Свойство не может быть null.
     */
    public void setType(GrantType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Наименование стипендии. Свойство не может быть null.
     */
    @NotNull
    public Grant getGrant()
    {
        return _grant;
    }

    /**
     * @param grant Наименование стипендии. Свойство не может быть null.
     */
    public void setGrant(Grant grant)
    {
        dirty(_grant, grant);
        _grant = grant;
    }

    /**
     * @return Вид стипендии. Свойство не может быть null.
     */
    @NotNull
    public GrantView getView()
    {
        return _view;
    }

    /**
     * @param view Вид стипендии. Свойство не может быть null.
     */
    public void setView(GrantView view)
    {
        dirty(_view, view);
        _view = view;
    }

    /**
     * @return Входит в группу. Свойство не может быть null.
     */
    @NotNull
    public boolean isJoinGroup()
    {
        return _joinGroup;
    }

    /**
     * @param joinGroup Входит в группу. Свойство не может быть null.
     */
    public void setJoinGroup(boolean joinGroup)
    {
        dirty(_joinGroup, joinGroup);
        _joinGroup = joinGroup;
    }

    /**
     * @return Имя группы.
     */
    @Length(max=255)
    public String getGroupName()
    {
        return _groupName;
    }

    /**
     * @param groupName Имя группы.
     */
    public void setGroupName(String groupName)
    {
        dirty(_groupName, groupName);
        _groupName = groupName;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RelTypeGrantViewGen)
        {
            setType(((RelTypeGrantView)another).getType());
            setGrant(((RelTypeGrantView)another).getGrant());
            setView(((RelTypeGrantView)another).getView());
            setJoinGroup(((RelTypeGrantView)another).isJoinGroup());
            setGroupName(((RelTypeGrantView)another).getGroupName());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RelTypeGrantViewGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RelTypeGrantView.class;
        }

        public T newInstance()
        {
            return (T) new RelTypeGrantView();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "type":
                    return obj.getType();
                case "grant":
                    return obj.getGrant();
                case "view":
                    return obj.getView();
                case "joinGroup":
                    return obj.isJoinGroup();
                case "groupName":
                    return obj.getGroupName();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "type":
                    obj.setType((GrantType) value);
                    return;
                case "grant":
                    obj.setGrant((Grant) value);
                    return;
                case "view":
                    obj.setView((GrantView) value);
                    return;
                case "joinGroup":
                    obj.setJoinGroup((Boolean) value);
                    return;
                case "groupName":
                    obj.setGroupName((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "type":
                        return true;
                case "grant":
                        return true;
                case "view":
                        return true;
                case "joinGroup":
                        return true;
                case "groupName":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "type":
                    return true;
                case "grant":
                    return true;
                case "view":
                    return true;
                case "joinGroup":
                    return true;
                case "groupName":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "type":
                    return GrantType.class;
                case "grant":
                    return Grant.class;
                case "view":
                    return GrantView.class;
                case "joinGroup":
                    return Boolean.class;
                case "groupName":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RelTypeGrantView> _dslPath = new Path<RelTypeGrantView>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RelTypeGrantView");
    }
            

    /**
     * @return Тип стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelTypeGrantView#getType()
     */
    public static GrantType.Path<GrantType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Наименование стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelTypeGrantView#getGrant()
     */
    public static Grant.Path<Grant> grant()
    {
        return _dslPath.grant();
    }

    /**
     * @return Вид стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelTypeGrantView#getView()
     */
    public static GrantView.Path<GrantView> view()
    {
        return _dslPath.view();
    }

    /**
     * @return Входит в группу. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelTypeGrantView#isJoinGroup()
     */
    public static PropertyPath<Boolean> joinGroup()
    {
        return _dslPath.joinGroup();
    }

    /**
     * @return Имя группы.
     * @see ru.tandemservice.movestudentrmc.entity.RelTypeGrantView#getGroupName()
     */
    public static PropertyPath<String> groupName()
    {
        return _dslPath.groupName();
    }

    public static class Path<E extends RelTypeGrantView> extends EntityPath<E>
    {
        private GrantType.Path<GrantType> _type;
        private Grant.Path<Grant> _grant;
        private GrantView.Path<GrantView> _view;
        private PropertyPath<Boolean> _joinGroup;
        private PropertyPath<String> _groupName;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelTypeGrantView#getType()
     */
        public GrantType.Path<GrantType> type()
        {
            if(_type == null )
                _type = new GrantType.Path<GrantType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Наименование стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelTypeGrantView#getGrant()
     */
        public Grant.Path<Grant> grant()
        {
            if(_grant == null )
                _grant = new Grant.Path<Grant>(L_GRANT, this);
            return _grant;
        }

    /**
     * @return Вид стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelTypeGrantView#getView()
     */
        public GrantView.Path<GrantView> view()
        {
            if(_view == null )
                _view = new GrantView.Path<GrantView>(L_VIEW, this);
            return _view;
        }

    /**
     * @return Входит в группу. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelTypeGrantView#isJoinGroup()
     */
        public PropertyPath<Boolean> joinGroup()
        {
            if(_joinGroup == null )
                _joinGroup = new PropertyPath<Boolean>(RelTypeGrantViewGen.P_JOIN_GROUP, this);
            return _joinGroup;
        }

    /**
     * @return Имя группы.
     * @see ru.tandemservice.movestudentrmc.entity.RelTypeGrantView#getGroupName()
     */
        public PropertyPath<String> groupName()
        {
            if(_groupName == null )
                _groupName = new PropertyPath<String>(RelTypeGrantViewGen.P_GROUP_NAME, this);
            return _groupName;
        }

        public Class getEntityClass()
        {
            return RelTypeGrantView.class;
        }

        public String getEntityName()
        {
            return "relTypeGrantView";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
