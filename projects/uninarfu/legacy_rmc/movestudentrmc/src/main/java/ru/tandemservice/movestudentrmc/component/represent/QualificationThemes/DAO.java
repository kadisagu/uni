package ru.tandemservice.movestudentrmc.component.represent.QualificationThemes;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.entity.StudentQualificationThemes;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(final Model model) {


    }

    @Override
    public void update(Model model) {
    }

    @Override
    public void prepareListDataSource(Model model) {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(StudentQualificationThemes.class, "spd");
        dql.where(DQLExpressions.eq(
                DQLExpressions.property(StudentQualificationThemes.listRepresent().fromAlias("spd")),
                DQLExpressions.value(model.getRepresent())
        ));
        List<StudentQualificationThemes> dataList = dql.createStatement(getSession()).list();
        Map<Long, StudentQualificationThemes> dataMap = new HashMap<>();
        for (StudentQualificationThemes item : dataList) {
            dataMap.put(item.getStudent().getId(), item);
        }

        Map<Long, StudentQualificationThemes> valueMap = ((IValueMapHolder) model.getDataSource().getColumn("qualificationThemes")).getValueMap();
        valueMap.putAll(dataMap);

        List<DataWrapper> itemList = model.getList();

        for (DataWrapper dataW : itemList) {
            Student student = (Student) dataW.getProperty("student");
            StudentQualificationThemes qualificationThemes = (StudentQualificationThemes) dataW.getProperty("qualificationThemes");
            if (qualificationThemes != null && dataMap.containsKey(student.getId())) {
                //без этого ОСИ на редактировании приказа->редактировании данных по практике, приходится доставать внутреннего руководителя из базы в текущей сессии
                // practiceData.setInnerAdvisor(dataMap.get(student.getId()).getInnerAdvisor());
                valueMap.put(student.getId(), qualificationThemes);
            }
            //Если в базе нет
            else if (qualificationThemes != null && !dataMap.containsKey(student.getId())) {
                valueMap.put(student.getId(), qualificationThemes);
            }
            else {
                if (!valueMap.containsKey(student.getId()))
                    valueMap.put(student.getId(), new StudentQualificationThemes());


            }
        }
        ((BlockColumn) model.getDataSource().getColumn("qualificationThemes")).setValueMap(valueMap);

        UniUtils.createPage(model.getDataSource(), model.getList());
    }


}
