package ru.tandemservice.movestudentrmc.component.catalog.checkOnOrder.CheckOnOrderPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudentrmc.entity.catalog.CheckOnOrder;

public class Model extends DefaultCatalogPubModel<CheckOnOrder> {

    public static final Long SHOW_USE_CODE = 0L;
    public static final Long SHOW_NON_USE_CODE = 1L;

    private ISelectModel usedModel;

    public ISelectModel getUsedModel() {
        return usedModel;
    }

    public void setUsedModel(ISelectModel usedModel) {
        this.usedModel = usedModel;
    }

}
