package ru.tandemservice.movestudentrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип шаблона представления"
 * Имя сущности : typeTemplateRepresent
 * Файл data.xml : movementstudent.data.xml
 */
public interface TypeTemplateRepresentCodes
{
    /** Константа кода (code) элемента : common (code). Название (title) : Общий шаблон */
    String COMMON = "common";
    /** Константа кода (code) элемента : represent (code). Название (title) : Шаблон представления */
    String REPRESENT = "represent";
    /** Константа кода (code) элемента : extract (code). Название (title) : Шаблон выписки */
    String EXTRACT = "extract";
    /** Константа кода (code) элемента : declaration (code). Название (title) : Шаблон заявления */
    String DECLARATION = "declaration";
    /** Константа кода (code) элемента : attachment (code). Название (title) : Шаблон приложения */
    String ATTACHMENT = "attachment";

    Set<String> CODES = ImmutableSet.of(COMMON, REPRESENT, EXTRACT, DECLARATION, ATTACHMENT);
}
