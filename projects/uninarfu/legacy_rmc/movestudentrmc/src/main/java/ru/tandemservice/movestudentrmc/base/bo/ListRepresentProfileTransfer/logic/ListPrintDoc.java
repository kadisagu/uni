package ru.tandemservice.movestudentrmc.base.bo.ListRepresentProfileTransfer.logic;

import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudentrmc.docord.IListPrintDoc;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.*;

public class ListPrintDoc implements IListPrintDoc
{

    private Map<Student, OrderParagraphInfo> hashMap = new HashMap<>();
    private Map<Student, Integer> studentNumber = new HashMap<>();

    public final String TEMPLATE_HEADER = "list.rep.profile.transfer.header";
    public final String TEMPLATE_PARAG = "list.rep.profile.transfer.parag";

    public RtfDocument creationPrintDocOrder(List<? extends ListRepresent> listOfRepresents)
    {
        PrintTemplate templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(PrintTemplate.class, TEMPLATE_HEADER);
        RtfDocument templateHeader = new RtfReader().read(templateDocument.getContent());

        templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(PrintTemplate.class, TEMPLATE_PARAG);
        RtfDocument templateParag = new RtfReader().read(templateDocument.getContent());

        Map<OrgUnit, Map<String, Map<Group, List<Object[]>>>> dataMap = getStudentData(listOfRepresents);
        //сортировка формирующих по алфавиту
        List<OrgUnit> orgUnitList = new ArrayList<>(dataMap.keySet());
        Collections.sort(orgUnitList, new EntityComparator<>(new EntityOrder(OrgUnit.title())));

        RtfDocument result = templateHeader.getClone();
        result.setSettings(templateParag.getSettings());
        IRtfElement paragTemp = UniRtfUtil.findElement(result.getElementList(), "parag");
        new RtfInjectModifier().put("representTitle", listOfRepresents.get(0).getRepresentationType().getTitle()).modify(result);

        RtfDocument res = RtfBean.getElementFactory().createRtfDocument();
        res.setHeader(templateParag.getHeader());
        res.setSettings(templateParag.getSettings());
        res.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.TAB));

        int formCounter = 0;
        int addonCounter = 0;
        List<RtfDocument> addonList = new ArrayList<>();


        for (OrgUnit formOU : orgUnitList)
        {
            Map<String, Map<Group, List<Object[]>>> formMap = dataMap.get(formOU);

            formCounter++;
            res.getElementList().add(RtfBean.getElementFactory().createRtfText(String.valueOf(formCounter).trim() + "."));
            res.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.TAB));
            insertRow(res, "По " + formOU.getDativeCaseTitle());

            int counter = 0;
            //----------------------------------------------------------------
            ArrayList<String> paragraphKeyList = new ArrayList<>(formMap.keySet());
            Collections.sort(paragraphKeyList, String::compareTo);
            for (String paragraphKey : paragraphKeyList) {
            	Map<Group, List<Object[]>> studentList = formMap.get(paragraphKey);
            	
            //for (Map<Group, List<Object[]>> studentList : formMap.values())
            //{            	
            //----------------------------------------------------------------            
                counter++;
                addonCounter++;
                RtfDocument par = getParagraph(templateParag, formCounter, counter, addonCounter, studentList);
                res.getElementList().addAll(par.getElementList());
                //Приложение печатать не нужно
                //addonList.add(getAddon(templateAddon, addonCounter, studentList, order));
            }
        }

        //основания
        res.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
        insertRow(res, "Основание: " + UtilPrintSupport.getBasementTitles(listOfRepresents));
        result.getElementList().addAll(result.getElementList().indexOf(paragTemp), res.getElementList());
        //раскомментировать при необходимости печати приложения
        //result.getElementList().add( RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE) );

        for (int i = 0; i < addonList.size(); i++)
        {
            result.getElementList().addAll(addonList.get(i).getElementList());
            if (i + 1 < addonList.size())
                result.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
        }
        SharedRtfUtil.removeParagraphsWithTagsRecursive(result, Collections.singletonList("parag"), false, false);
        return result;
    }

    private RtfDocument getParagraph(RtfDocument template, int formCounter, int counter, int addonCounter, Map<Group, List<Object[]>> studentList)
    {
        RtfDocument result = template.getClone();

        ListRepresentProfileTransfer listRepresent = (ListRepresentProfileTransfer) studentList.values().iterator().next().get(0)[1];
        Student student = (Student) studentList.values().iterator().next().get(0)[0];
        for (Map.Entry<Group, List<Object[]>> entry : studentList.entrySet())
        {
            List<Object[]> list = entry.getValue();
            for (Object[] o : list)
            {
                Student s = (Student) o[0];
                hashMap.put(s, new OrderParagraphInfo(formCounter, counter));
            }
        }
        RtfInjectModifier im = new RtfInjectModifier();

        im.put("i", String.valueOf(formCounter).trim());
        im.put("j", String.valueOf(counter).trim());
        im.put("addonNumber", String.valueOf(addonCounter));

        injectModifier(im, listRepresent, student);
        im.modify(result);

        List<String[]> tableList = new ArrayList<>();

        for (Map.Entry<Group, List<Object[]>> entry : studentList.entrySet())
        {
            int rowCounter = 1;
            List<Object[]> list = entry.getValue();
            String[] groupRow = new String[2];
            groupRow[0] = "";
            groupRow[1] = entry.getKey().getTitle() + " группа";
            tableList.add(groupRow);
            for (Object[] arr : list)
            {
                Student stu = (Student) arr[0];
                String[] row = new String[2];
                studentNumber.put(stu, rowCounter);
                row[0] = "" + rowCounter++ + ".";
                row[1] = stu.getPerson().getFullFio();
                tableList.add(row);
            }
        }

        RtfTableModifier tm = new RtfTableModifier();
        tm.put("T", tableList.toArray(new String[][]{}));
        tm.modify(result);
        return result;
    }

    public static void injectModifier(RtfInjectModifier im, ListRepresentProfileTransfer listRepresent, Student student)
    {
        im.put("represent_StartDate", UtilPrintSupport.getDateFormatterWithMonthString(listRepresent.getDateBeginingTransfer()));
        im.put("representEduLevelFullTitle", UtilPrintSupport.getHighLevelSchoolTypeString(listRepresent.getNewEducationOrgUnit().getEducationLevelHighSchool()));
        im.put("course", listRepresent.getNewGroup().getCourse().getTitle());
        im.put("developForm", UtilPrintSupport.getDevelopFormGen(student.getEducationOrgUnit().getDevelopForm()));
        im.put("highSchool", UtilPrintSupport.getHighLevelSchoolTypeString(student.getEducationOrgUnit().getEducationLevelHighSchool()));
        im.put("reason", listRepresent.getRepresentationReason().getTitle());
    }

    private RtfDocument getAddon(RtfDocument template, int addonCounter, List<Object[]> studentList, ListOrder order)
    {
        RtfDocument result = template.getClone();

        ListRepresentProfileTransfer listRepresent = (ListRepresentProfileTransfer) studentList.get(0)[1];
        Student firstStudent = (Student) studentList.get(0)[0];

        RtfInjectModifier im = new RtfInjectModifier();
        im.put("addonNumber", "" + addonCounter);
        im.put("orderDate", order != null ? DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(order.getCommitDate()) + " года" : "");
        im.put("orderNumber", order != null ? order.getNumber() : "");

        injectModifier(im, listRepresent, firstStudent);

        im.modify(result);

        List<String[]> tableList = new ArrayList<>();
        int counter = 1;
        for (Object[] arr : studentList)
        {
            Student student = (Student) arr[0];

            String[] row = new String[4];
            row[0] = "" + counter++;
            row[1] = student.getBookNumber() != null ? student.getBookNumber() : "";
            row[2] = student.getPerson().getFullFio();
            row[3] = UtilPrintSupport.getCompensationTypeStr(student.getCompensationType());

            tableList.add(row);
        }

        RtfTableModifier tm = new RtfTableModifier();
        tm.put("T", tableList.toArray(new String[][]{}));
        tm.modify(result);

        return result;
    }

    private void insertRow(RtfDocument document, String text)
    {
        document.getElementList().add(RtfBean.getElementFactory().createRtfText(text));
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
    }

    private ListOrder getOrder(ListRepresent listRepresent)
    {
        List<ListOrdListRepresent> orderList = UniDaoFacade.getCoreDao().getList(ListOrdListRepresent.class, ListOrdListRepresent.representation(), listRepresent);
        return orderList.isEmpty() ? null : orderList.get(0).getOrder();
    }

    private String getKey(Student student, ListRepresentProfileTransfer represent)
    {
        return new StringBuilder()
                .append(student.getEducationOrgUnit().getFormativeOrgUnit().getId())
                        //   .append(represent.getNewGroup().getCreationDate())

                //-------------------------------------------------------------------------------
                //.append(represent.getNewGroup().getCourse().getId())
                //.append(represent.getNewEducationOrgUnit().getEducationLevelHighSchool().getId())
                //.append(represent.getNewEducationOrgUnit().getDevelopForm().getId())                
               
                .append(represent.getNewEducationOrgUnit().getDevelopForm().getCode())
                .append(represent.getNewGroup().getCourse().getCode())
                .append(represent.getNewEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject().getSubjectIndex().getProgramKind().getCode())
                .append(represent.getNewEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject().getSubjectCode())
                .append(represent.getDateBeginingTransfer())
                .append(represent.getNewEducationOrgUnit().getEducationLevelHighSchool().getId())
                //-------------------------------------------------------------------------------                
                .toString();
    }

    private Map<OrgUnit, Map<String, Map<Group, List<Object[]>>>> getStudentData(List<? extends ListRepresent> listOfRepresents)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(RelListRepresentStudents.class, "rel")
                .where(DQLExpressions.in(
                        DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("rel")),
                        listOfRepresents
                ))
                .joinEntity(
                        "rel",
                        DQLJoinType.inner,
                        ListRepresentProfileTransfer.class,
                        "rep",
                        DQLExpressions.eq(
                                DQLExpressions.property(ListRepresentProfileTransfer.id().fromAlias("rep")),
                                DQLExpressions.property(RelListRepresentStudents.representation().id().fromAlias("rel"))
                        ))
                .column(DQLExpressions.property(RelListRepresentStudents.student().fromAlias("rel")))
                .column("rep")
                .order(DQLExpressions.property(RelListRepresentStudents.student().person().identityCard().fullFio().fromAlias("rel")));

        List<Object[]> studentDataList = UniDaoFacade.getCoreDao().getList(dql);

        Map<OrgUnit, Map<String, Map<Group, List<Object[]>>>> dataMap = new HashMap<>();
        for (Object[] arr : studentDataList)
        {
            Student student = (Student) arr[0];
            ListRepresentProfileTransfer represent = (ListRepresentProfileTransfer) arr[1];

            OrgUnit formOU = student.getEducationOrgUnit().getFormativeOrgUnit();
            if (dataMap.get(formOU) == null)
                dataMap.put(formOU, new HashMap<>());

            String key = getKey(student, represent);
            if (dataMap.get(formOU).get(key) == null)
                dataMap.get(formOU).put(key, new HashMap<>());
            Group group = represent.getNewGroup();
            if (dataMap.get(formOU).get(key).get(group) == null)
                dataMap.get(formOU).get(key).put(group, new ArrayList<>());
            dataMap.get(formOU).get(key).get(group).add(arr);
        }

        return dataMap;
    }


    @Override
    public Map<Student, OrderParagraphInfo> getParagInfomap()
    {
        return hashMap;
    }

    public Map<Student, Integer> getStudentNumberMap()
    {
        return studentNumber;
    }
}
