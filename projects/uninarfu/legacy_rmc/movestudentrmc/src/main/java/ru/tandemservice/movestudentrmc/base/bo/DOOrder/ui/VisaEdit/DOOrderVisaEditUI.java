package ru.tandemservice.movestudentrmc.base.bo.DOOrder.ui.VisaEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.DataAccessServices;

@State({
        @Bind(key = "orderId", binding = "orderId")
})
public class DOOrderVisaEditUI extends UIPresenter {

    private Long orderId;

    private IEntity order;


    @Override
    public void onComponentRefresh() {

        IEntity order = DataAccessServices.dao().get(getOrderId());

        if (order != null) {
            setOrder(order);
        }
    }


    public void onClickSave() {
        DataAccessServices.dao().saveOrUpdate(getOrder());
        deactivate();
    }


    public Long getOrderId() {
        return orderId;
    }


    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }


    public IEntity getOrder() {
        return order;
    }


    public void setOrder(IEntity order) {
        this.order = order;
    }

}
