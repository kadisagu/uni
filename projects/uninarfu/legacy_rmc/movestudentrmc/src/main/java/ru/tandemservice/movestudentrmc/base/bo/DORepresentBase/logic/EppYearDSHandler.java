package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;

import java.util.List;

public class EppYearDSHandler extends DefaultComboDataSourceHandler {
    public static final String PLAN_VERSION = "planVersion";

    public EppYearDSHandler(String name) {
        super(name, EppYearEducationProcess.class);
        setOrderByProperty(EppYearEducationProcess.educationYear().intValue().s());
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep) {
        super.prepareConditions(ep);

        EppEduPlanVersion version = ep.context.get(PLAN_VERSION);

        MQBuilder builder = new MQBuilder(EppWorkPlan.ENTITY_CLASS, "wp")
                .addJoin("wp", EppWorkPlan.parent(), "p")
                .add(MQExpression.eq("p", EppEduPlanVersionBlock.eduPlanVersion(), version))
                .addJoin("wp", EppWorkPlan.year(), "y");
        builder.getSelectAliasList().clear();
        builder.addSelect("y.id");
        builder.setNeedDistinct(true);

        List<Long> ids = builder.getResultList(ep.context.getSession());
        ep.dqlBuilder.where(DQLExpressions.in(DQLExpressions.property(EppYearEducationProcess.id().fromAlias("e")), ids));
    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep) {
        ep.dqlBuilder.order(DQLExpressions.property(EppYearEducationProcess.educationYear().intValue().fromAlias("e")), OrderDirection.desc);
    }
}
