package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.IAbstractRepresentation;
import ru.tandemservice.movestudentrmc.entity.IRepresent2Student;
import ru.tandemservice.uni.entity.employee.Student;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import ru.tandemservice.movestudentrmc.entity.IRepresent2Student;

@SuppressWarnings({"all", "unchecked", "unused"})
public abstract class IRepresent2StudentGen extends InterfaceStubBase
 implements IRepresent2Student{
    public static final int VERSION_HASH = 954130819;

    public static final String L_STUDENT = "student";
    public static final String L_REPRESENTATION = "representation";

    private Student _student;
    private IAbstractRepresentation _representation;

    @NotNull

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        _student = student;
    }

    @NotNull

    public IAbstractRepresentation getRepresentation()
    {
        return _representation;
    }

    public void setRepresentation(IAbstractRepresentation representation)
    {
        _representation = representation;
    }

    private static final Path<IRepresent2Student> _dslPath = new Path<IRepresent2Student>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ru.tandemservice.movestudentrmc.entity.IRepresent2Student");
    }
            

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.IRepresent2Student#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.IRepresent2Student#getRepresentation()
     */
    public static IAbstractRepresentationGen.Path<IAbstractRepresentation> representation()
    {
        return _dslPath.representation();
    }

    public static class Path<E extends IRepresent2Student> extends EntityPath<E>
    {
        private Student.Path<Student> _student;
        private IAbstractRepresentationGen.Path<IAbstractRepresentation> _representation;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.IRepresent2Student#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.IRepresent2Student#getRepresentation()
     */
        public IAbstractRepresentationGen.Path<IAbstractRepresentation> representation()
        {
            if(_representation == null )
                _representation = new IAbstractRepresentationGen.Path<IAbstractRepresentation>(L_REPRESENTATION, this);
            return _representation;
        }

        public Class getEntityClass()
        {
            return IRepresent2Student.class;
        }

        public String getEntityName()
        {
            return "ru.tandemservice.movestudentrmc.entity.IRepresent2Student";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
