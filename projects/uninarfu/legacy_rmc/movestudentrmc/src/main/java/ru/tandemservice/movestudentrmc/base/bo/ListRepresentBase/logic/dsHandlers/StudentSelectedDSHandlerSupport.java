package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab.MonthWrapper;
import ru.tandemservice.movestudentrmc.entity.GrantEntity;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntity;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import ru.tandemservice.movestudentrmc.entity.catalog.StuGrantStatus;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.StuGrantStatusCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.*;

public class StudentSelectedDSHandlerSupport extends AbstractSearchDataSourceHandler<DSInput, DSOutput> {

    public static final String DEVELOP_FORM_COLUMN = "developForm";
    public static final String FORMATIVE_ORG_UNIT_COLUMN = "formativeOrgUnit";
    public static final String TERRITORIAL_ORG_UNIT_COLUMN = "territorialOrgUnit";
    public static final String EDUCATION_LEVEL_HIGH_SCHOOL_COLUMN = "educationLevelHighSchool";
    public static final String EDUCATION_ORG_UNIT_COLUMN = "educationOrgUnit";
    public static final String TYPE_REPRESENT_COLUMN = "typeRepresent";
    public static final String COURSE_COLUMN = "course";
    public static final String GROUP_COLUMN = "group";
    public static final String BUDGET_COLUMN = "budget";
    public static final String DATE_REPRESENT_COLUMN = "dateRepresent";
    public static final String STUDENT_FIO_COLUMN = "studentFio";
    public static final String CHECKBOX_COLUMN = "checkbox";
    public static final String STATUS_COLUMN = "status";
    public static final String COMPENSATION_TYPE_COLUMN = "compensationType";
    public static final String DYPLOM_COLUMN = "dyplom";
    public static final String GRANT_COLUMN = "grant";
    public static final String LIST_REPRESENT_ID = "listRepresentId";


    public static final String STUDENT_SELECTED_LIST = "studentSelectList";

    public StudentSelectedDSHandlerSupport(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        List<String> errorList = new ArrayList<String>();

        Collections.sort(context.<List>get(STUDENT_SELECTED_LIST), new EntityComparator(input.getEntityOrder()));
        List<DataWrapper> selectedList = context.<List>get(STUDENT_SELECTED_LIST);

        if (UserContext.getInstance().getAttribute("popupGrant") == null || (UserContext.getInstance().getAttribute("popupGrant") != null && !(Boolean) UserContext.getInstance().getAttribute("popupGrant"))) {
            for (DataWrapper student : selectedList) {

                if (context.get("grantViewField") != null && context.get("educationYearField") != null && context.get("dateBeginingPaymentField") != null) {
                    Student studentData = (Student) student.getProperty("student");
                    StudentGrantEntity stuGrantEntity = (StudentGrantEntity) student.getProperty("grant");

                    if (stuGrantEntity != null && stuGrantEntity.getSum() > 0) {
                        student.setProperty("grant", stuGrantEntity);
                        continue;
                    }
                    MonthWrapper.Data monthData = MonthWrapper.getInstance((Date) context.get("dateBeginingPaymentField"));

                    if (context.get(LIST_REPRESENT_ID) != null) {
                        DQLSelectBuilder tmpGrantsBuilder = new DQLSelectBuilder();
                        tmpGrantsBuilder.fromEntity(StudentGrantEntity.class, "sg")
                                .addColumn("sg")
                                .where(DQLExpressions.eq(DQLExpressions.property(StudentGrantEntity.listRepresent().id().fromAlias("sg")),
                                                         DQLExpressions.commonValue(context.get(LIST_REPRESENT_ID))))
                                .where(DQLExpressions.eq(DQLExpressions.property(StudentGrantEntity.student().fromAlias("sg")),
                                                         DQLExpressions.value(studentData)))
                        ;

                        StudentGrantEntity tmpStudentgrantEntity = tmpGrantsBuilder.createStatement(getSession()).uniqueResult();

                        if (tmpStudentgrantEntity != null) {
                            student.setProperty("grant", tmpStudentgrantEntity);
                            continue;
                        }
                    }
                    OrgUnit orgUnit = studentData.getEducationOrgUnit().getFormativeOrgUnit();

                    Calendar c1 = Calendar.getInstance();
                    c1.setTime((Date) context.get("dateBeginingPaymentField"));

                    DQLSelectBuilder orgUnitGrants = new DQLSelectBuilder()
                            .fromEntity(GrantEntity.class, "og")
                            .where(DQLExpressions.and(
                                    DQLExpressions.eq(DQLExpressions.property(GrantEntity.orgUnit().fromAlias("og")), DQLExpressions.value(orgUnit)),
                                    DQLExpressions.eq(DQLExpressions.property(GrantEntity.view().fromAlias("og")), DQLExpressions.value((GrantView) context.get("grantViewField"))),
                                    DQLExpressions.eq(DQLExpressions.property(GrantEntity.eduYear().fromAlias("og")), DQLExpressions.commonValue(context.get("educationYearField"))),
                                    DQLExpressions.eq(DQLExpressions.property(GrantEntity.month().fromAlias("og")), DQLExpressions.value(monthData == null ? null : monthData.monthWrapper.getStringValue((EducationYear) context.get("educationYearField"))))
                            ));

		            /*Create new StudentGrantEntity*/
                    StudentGrantEntity studentGrantEntity = new StudentGrantEntity();

                    studentGrantEntity.setTmp(true);
                    studentGrantEntity.setMonth(monthData == null ? null : monthData.monthWrapper.getStringValue((EducationYear) context.get("educationYearField")));
                    studentGrantEntity.setOrderDate(null);
                    studentGrantEntity.setOrderNumber(null);
                    studentGrantEntity.setView((GrantView) context.get("grantViewField"));

                    GrantEntity grantEntity = orgUnitGrants.createStatement(getSession()).uniqueResult();

                    if (grantEntity == null) {
                        String errStr = "Не настроена сумма выплаты " + ((GrantView) context.get("grantViewField")).getTitle() + " на период " + monthData.monthWrapper.getStringValue((EducationYear) context.get("educationYearField")) + " в университете \""
                                + studentData.getEducationOrgUnit().getFormativeOrgUnit().getTitle() + "\"";
                        if (!errorList.contains(errStr))
                            errorList.add(errStr);
                        ContextLocal.getInfoCollector().add(errStr);
                        studentGrantEntity.setSum(Double.parseDouble("0"));
                        //continue;
                    }
                    else {
                        studentGrantEntity.setSum(grantEntity.getSum());
                    }

                    studentGrantEntity.setEduYear((EducationYear) context.get("educationYearField"));
                    studentGrantEntity.setStudent(studentData);
                    studentGrantEntity.setStatus(DataAccessServices.dao().get(StuGrantStatus.class, StuGrantStatus.code(), StuGrantStatusCodes.CODE_1));

		            /**/

                    student.setProperty("grant", studentGrantEntity);
                }
            }

        }

        UserContext.getInstance().setAttribute("popupGrant", false);

        //return ListOutputBuilder.get(input, resultList).pageable(true).build();

        return ListOutputBuilder.get(input, selectedList).pageable(true).build();
    }
}
