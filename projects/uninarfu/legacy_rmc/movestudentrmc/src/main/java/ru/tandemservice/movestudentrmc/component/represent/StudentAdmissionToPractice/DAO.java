package ru.tandemservice.movestudentrmc.component.represent.StudentAdmissionToPractice;


import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import ru.tandemservice.movestudentrmc.entity.StudentPracticeData;
import ru.tandemservice.movestudentrmc.entity.catalog.PracticeBase;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        //---------------------------------------------------------------------------------------------------------------
    	//model.setPracticeBaseList(getCatalogItemList(PracticeBase.class));
    	model.setPracticeBaseModel(new DQLFullCheckSelectModel(PracticeBase.P_TITLE)
    	{
    		@Override
			protected DQLSelectBuilder query(String alias, String filter) {
				DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(PracticeBase.class, alias);
                FilterUtils.applySimpleLikeFilter(dql, alias, PracticeBase.title().s(), filter);
                return dql;
			}    		
    	});
    	//---------------------------------------------------------------------------------------------------------------
        model.setInnerAdvisorModel(new DQLFullCheckSelectModel(Employee.P_FULL_FIO)
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(Employee.class, alias);
                FilterUtils.applySimpleLikeFilter(dql, alias, Employee.person().identityCard().fullFio().s(), filter);
                return dql;
            }
        });
    }

    @Override
    public void update(Model model)
    {
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(StudentPracticeData.class, "spd");
        dql.where(DQLExpressions.eq(
                DQLExpressions.property(StudentPracticeData.listRepresent().fromAlias("spd")),
                DQLExpressions.value(model.getRepresent())
        ));
        List<StudentPracticeData> dataList = dql.createStatement(getSession()).list();
        Map<Long, StudentPracticeData> dataMap = new HashMap<>();
        for (StudentPracticeData item : dataList) {
            dataMap.put(item.getStudent().getId(), item);
        }

        Map<Long, StudentPracticeData> valueMap = ((IValueMapHolder) model.getDataSource().getColumn("practiceData")).getValueMap();
        valueMap.putAll(dataMap);

        List<DataWrapper> itemList = model.getList();

        for (DataWrapper dataW : itemList) {
            Student student = (Student) dataW.getProperty("student");
            StudentPracticeData practiceData = (StudentPracticeData) dataW.getProperty("practiceData");
            if (practiceData != null) {
                valueMap.put(student.getId(), practiceData);
            } else {
                valueMap.put(student.getId(), new StudentPracticeData());
            }
        }
        ((BlockColumn) model.getDataSource().getColumn("practiceData")).setValueMap(valueMap);

        UniUtils.createPage(model.getDataSource(), model.getList());
    }

    public void addNewPracticeBase(Model model, String filter, Long id)
    {
        Map<Long, StudentPracticeData> map = ((BlockColumn) model.getDataSource().getColumn("practiceData")).getValueMap();
        Integer max = getMaxCode();
        PracticeBase base = get(PracticeBase.class, PracticeBase.title(), filter);
        if (base == null) {
            base = new PracticeBase();
            base.setTitle(filter);
            base.setCode(String.valueOf(max + 1));
            saveOrUpdate(base);
        }
        map.get(id).setPracticeBase(base);
    }

    private Integer getMaxCode()
    {
        Integer result = 0;
        List<PracticeBase> list = getCatalogItemList(PracticeBase.class);
        for (PracticeBase item : list) {
            if (Integer.valueOf(item.getCode()) > result)
                result = Integer.valueOf(item.getCode());
        }
        return result;
    }
}
