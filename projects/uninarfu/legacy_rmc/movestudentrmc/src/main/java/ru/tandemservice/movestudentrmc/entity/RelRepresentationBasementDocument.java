package ru.tandemservice.movestudentrmc.entity;

import org.tandemframework.shared.commonbase.dao.IEntityRelation;
import ru.tandemservice.movestudentrmc.entity.catalog.Document;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationBasement;
import ru.tandemservice.movestudentrmc.entity.gen.RelRepresentationBasementDocumentGen;

/**
 * Сущность-связь Основание представления-Документ
 */
public class RelRepresentationBasementDocument extends RelRepresentationBasementDocumentGen implements IEntityRelation<RepresentationBasement, Document> {
}