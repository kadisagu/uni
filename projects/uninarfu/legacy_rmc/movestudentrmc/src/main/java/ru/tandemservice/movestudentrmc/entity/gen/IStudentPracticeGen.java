package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudentrmc.entity.IStudentPractice;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import ru.tandemservice.movestudentrmc.entity.IStudentPractice;

@SuppressWarnings({"all", "unchecked", "unused"})
public abstract class IStudentPracticeGen extends InterfaceStubBase
 implements IStudentPractice{
    public static final int VERSION_HASH = 12548325;

    public static final String P_DATE_BEGINNING_PRACTICE = "dateBeginningPractice";
    public static final String P_DATE_END_OF_PRACTICE = "dateEndOfPractice";
    public static final String L_REGISTRY_ELEMENT_ROW = "registryElementRow";

    private Date _dateBeginningPractice;
    private Date _dateEndOfPractice;
    private EppWorkPlanRow _registryElementRow;

    @NotNull

    public Date getDateBeginningPractice()
    {
        return _dateBeginningPractice;
    }

    public void setDateBeginningPractice(Date dateBeginningPractice)
    {
        _dateBeginningPractice = dateBeginningPractice;
    }

    @NotNull

    public Date getDateEndOfPractice()
    {
        return _dateEndOfPractice;
    }

    public void setDateEndOfPractice(Date dateEndOfPractice)
    {
        _dateEndOfPractice = dateEndOfPractice;
    }


    public EppWorkPlanRow getRegistryElementRow()
    {
        return _registryElementRow;
    }

    public void setRegistryElementRow(EppWorkPlanRow registryElementRow)
    {
        _registryElementRow = registryElementRow;
    }

    private static final Path<IStudentPractice> _dslPath = new Path<IStudentPractice>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ru.tandemservice.movestudentrmc.entity.IStudentPractice");
    }
            

    /**
     * @return Дата начала практики. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.IStudentPractice#getDateBeginningPractice()
     */
    public static PropertyPath<Date> dateBeginningPractice()
    {
        return _dslPath.dateBeginningPractice();
    }

    /**
     * @return Дата окончания практики. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.IStudentPractice#getDateEndOfPractice()
     */
    public static PropertyPath<Date> dateEndOfPractice()
    {
        return _dslPath.dateEndOfPractice();
    }

    /**
     * @return Мероприятие реестра.
     * @see ru.tandemservice.movestudentrmc.entity.IStudentPractice#getRegistryElementRow()
     */
    public static EppWorkPlanRow.Path<EppWorkPlanRow> registryElementRow()
    {
        return _dslPath.registryElementRow();
    }

    public static class Path<E extends IStudentPractice> extends EntityPath<E>
    {
        private PropertyPath<Date> _dateBeginningPractice;
        private PropertyPath<Date> _dateEndOfPractice;
        private EppWorkPlanRow.Path<EppWorkPlanRow> _registryElementRow;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата начала практики. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.IStudentPractice#getDateBeginningPractice()
     */
        public PropertyPath<Date> dateBeginningPractice()
        {
            if(_dateBeginningPractice == null )
                _dateBeginningPractice = new PropertyPath<Date>(IStudentPracticeGen.P_DATE_BEGINNING_PRACTICE, this);
            return _dateBeginningPractice;
        }

    /**
     * @return Дата окончания практики. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.IStudentPractice#getDateEndOfPractice()
     */
        public PropertyPath<Date> dateEndOfPractice()
        {
            if(_dateEndOfPractice == null )
                _dateEndOfPractice = new PropertyPath<Date>(IStudentPracticeGen.P_DATE_END_OF_PRACTICE, this);
            return _dateEndOfPractice;
        }

    /**
     * @return Мероприятие реестра.
     * @see ru.tandemservice.movestudentrmc.entity.IStudentPractice#getRegistryElementRow()
     */
        public EppWorkPlanRow.Path<EppWorkPlanRow> registryElementRow()
        {
            if(_registryElementRow == null )
                _registryElementRow = new EppWorkPlanRow.Path<EppWorkPlanRow>(L_REGISTRY_ELEMENT_ROW, this);
            return _registryElementRow;
        }

        public Class getEntityClass()
        {
            return IStudentPractice.class;
        }

        public String getEntityName()
        {
            return "ru.tandemservice.movestudentrmc.entity.IStudentPractice";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
