package ru.tandemservice.movestudentrmc.base.bo.DORepresentWeekendOut.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.DevelopForm;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class FormativeOrgUnitDSHandler extends DefaultComboDataSourceHandler {

    public FormativeOrgUnitDSHandler(String ownerId) {

        super(ownerId, OrgUnit.class, DevelopForm.P_TITLE);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        String filter = ep.input.getComboFilterByValue();

        ep.dqlBuilder.where(DQLExpressions.or(
                DQLExpressions.eq(property(OrgUnit.orgUnitType().code().fromAlias("e")), value("institute")),
                DQLExpressions.eq(property(OrgUnit.orgUnitType().code().fromAlias("e")), value("faculty")),
                DQLExpressions.eq(property(OrgUnit.orgUnitType().code().fromAlias("e")), value("college"))
        ));

        if (StringUtils.isNotBlank(filter)) {
            ep.dqlBuilder.where(like(DQLFunctions.upper(property(OrgUnit.title().fromAlias("e"))), value(CoreStringUtils.escapeLike(filter, true))));
        }
    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
    {
        ep.dqlBuilder.order("e." + OrgUnit.title());
    }
}
