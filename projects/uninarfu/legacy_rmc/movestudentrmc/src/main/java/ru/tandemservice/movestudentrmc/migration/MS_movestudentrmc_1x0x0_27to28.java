package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

public class MS_movestudentrmc_1x0x0_27to28
        extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.10"),
                        new ScriptDependency("org.tandemframework.shared", "1.0.16")
                };
    }

    public void run(DBTool tool) throws Exception
    {
        // таблица сущности переименована
        {
            // переименовать таблицу
            tool.renameTable("listrepresentdiplomandexclude_t", "lst_rep_diplom_and_excl_t");
            tool.renameTable("rellistrepresentstudentsolddata_t", "rel_lst_rep_stu_olddata_t");
        }
    }
}
