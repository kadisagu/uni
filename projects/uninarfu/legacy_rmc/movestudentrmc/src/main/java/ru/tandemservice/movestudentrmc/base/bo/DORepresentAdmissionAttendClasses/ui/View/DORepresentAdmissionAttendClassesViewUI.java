package ru.tandemservice.movestudentrmc.base.bo.DORepresentAdmissionAttendClasses.ui.View;

import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.DevelopGridTermDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentViewUI;
import ru.tandemservice.movestudentrmc.entity.RepresentAdmissionAttendClasses;

public class DORepresentAdmissionAttendClassesViewUI extends AbstractDORepresentViewUI
{

    public String getTerm() {
        DevelopGridTermDSHandler.Formatter fmt = new DevelopGridTermDSHandler.Formatter();
        RepresentAdmissionAttendClasses represent = (RepresentAdmissionAttendClasses) _represent;
        return fmt.format(represent.getTerm());
    }

}
