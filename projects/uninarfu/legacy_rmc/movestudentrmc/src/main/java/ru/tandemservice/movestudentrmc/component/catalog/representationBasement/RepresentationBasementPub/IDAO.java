package ru.tandemservice.movestudentrmc.component.catalog.representationBasement.RepresentationBasementPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationBasement;

public interface IDAO extends IDefaultCatalogPubDAO<RepresentationBasement, Model> {
    void updatePrintable(Model model, Long id);

    void updateNoteRequired(Model model, Long id);

    void updateMerged(Model model, Long id);

    void updateConnectedWithDocument(Model model, Long id);
}
