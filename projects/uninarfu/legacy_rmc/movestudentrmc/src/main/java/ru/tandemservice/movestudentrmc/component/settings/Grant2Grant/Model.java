package ru.tandemservice.movestudentrmc.component.settings.Grant2Grant;

import org.tandemframework.shared.commonbase.base.util.PairKey;
import ru.tandemservice.movestudentrmc.entity.catalog.Grant;

import java.util.List;
import java.util.Map;

public class Model {

    private List<Grant> firstList;
    private List<Grant> secondList;
    private Grant currentFirst;
    private Grant currentSecond;

    private Map<PairKey<Grant, Grant>, Wrapper> settings;

    public List<Grant> getFirstList() {
        return firstList;
    }

    public void setFirstList(List<Grant> firstList) {
        this.firstList = firstList;
    }

    public List<Grant> getSecondList() {
        return secondList;
    }

    public void setSecondList(List<Grant> secondList) {
        this.secondList = secondList;
    }

    public Wrapper getRelation(Grant first, Grant second) {
        PairKey<Grant, Grant> keyObj = new PairKey<Grant, Grant>(first, second);
        return settings.get(keyObj);
    }

    public void setSettings(Map<PairKey<Grant, Grant>, Wrapper> settings) {
        this.settings = settings;
    }

    public Map<PairKey<Grant, Grant>, Wrapper> getSettings() {
        return this.settings;
    }

    public Grant getCurrentFirst() {
        return currentFirst;
    }

    public void setCurrentFirst(Grant currentFirst) {
        this.currentFirst = currentFirst;
    }

    public Grant getCurrentSecond() {
        return currentSecond;
    }

    public void setCurrentSecond(Grant currentSecond) {
        this.currentSecond = currentSecond;
    }


}
