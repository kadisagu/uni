package ru.tandemservice.movestudentrmc.base.bo.ListRepresentExclude.ui.Edit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.support.AbstractListRepresentEditUI;
import ru.tandemservice.movestudentrmc.dao.CheckStateUtil;
import ru.tandemservice.movestudentrmc.docord.util.MoveStudentUtil;
import ru.tandemservice.movestudentrmc.entity.ListRepresentExclude;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentExtractStatesCodes;

import java.util.Arrays;

@State({
        @Bind(key = AbstractListRepresentEditUI.LIST_REPRESENT_ID, binding = AbstractListRepresentEditUI.LIST_REPRESENT_ID),
        @Bind(key = "typeRepresentFilter", binding = "typeRepresentFilter"),
        @Bind(key = "reasonRepresentFilter", binding = "reasonRepresentFilter"),
        @Bind(key = "basementRepresentFilter", binding = "basementRepresentFilter"),
        @Bind(key = "representationBasementDate", binding = "representationBasementDate"),
        @Bind(key = "representationBasementNumber", binding = "representationBasementNumber"),
})
public class ListRepresentExcludeEditUI extends AbstractListRepresentEditUI<ListRepresentExclude> {

    private boolean save = false;

    @Override
    public ListRepresentExclude getListRepresentObject() {
        return new ListRepresentExclude();
    }

    @Override
    public void onClickSave() {

        if (getListRepresentId() != null)
            CheckStateUtil.checkStateRepresent(getListRepresent(), Arrays.asList(MovestudentExtractStatesCodes.CODE_1), _uiSupport.getSession());

        boolean ok = MoveStudentUtil.isChecked(getStudentSelectedList(), null, null);

        if ((ok && !isSave()) || (ok && isSave() && isWarning())) {
            String mess = MoveStudentUtil.checked(getListRepresent().getDateExclude(), true);
            if (!StringUtils.isEmpty(mess)) {
                setSave(true);
                setWarning(true);
                setWarningMessage(mess);
                return;
            }
        }

        super.onClickSave();

    }

    @Override
    public void onIgnoreWarning() {

        if (isSave()) {
            setWarning(false);
            onClickSave();
        }
        else
            super.onIgnoreWarning();
    }

    @Override
    public void onCancelRepresent() {
        setWarning(false);
        setSave(false);
    }

    public boolean isSave() {
        return save;
    }

    public void setSave(boolean save) {
        this.save = save;
    }

}
