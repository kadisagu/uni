package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.ExtractTextRelation;
import ru.tandemservice.movestudentrmc.entity.IRepresentOrder;
import ru.tandemservice.uni.entity.employee.Student;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Текст выписки из приказа студента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ExtractTextRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.ExtractTextRelation";
    public static final String ENTITY_NAME = "extractTextRelation";
    public static final int VERSION_HASH = 159739874;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORDER = "order";
    public static final String L_STUDENT = "student";
    public static final String P_TEXT = "text";

    private IRepresentOrder _order;     // Приказ
    private Student _student;     // Студент
    private byte[] _text;     // Текст выписки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приказ. Свойство не может быть null.
     */
    @NotNull
    public IRepresentOrder getOrder()
    {
        return _order;
    }

    /**
     * @param order Приказ. Свойство не может быть null.
     */
    public void setOrder(IRepresentOrder order)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && order!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IRepresentOrder.class);
            IEntityMeta actual =  order instanceof IEntity ? EntityRuntime.getMeta((IEntity) order) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_order, order);
        _order = order;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Текст выписки.
     */
    public byte[] getText()
    {
        return _text;
    }

    /**
     * @param text Текст выписки.
     */
    public void setText(byte[] text)
    {
        dirty(_text, text);
        _text = text;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ExtractTextRelationGen)
        {
            setOrder(((ExtractTextRelation)another).getOrder());
            setStudent(((ExtractTextRelation)another).getStudent());
            setText(((ExtractTextRelation)another).getText());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ExtractTextRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ExtractTextRelation.class;
        }

        public T newInstance()
        {
            return (T) new ExtractTextRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "order":
                    return obj.getOrder();
                case "student":
                    return obj.getStudent();
                case "text":
                    return obj.getText();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "order":
                    obj.setOrder((IRepresentOrder) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "text":
                    obj.setText((byte[]) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "order":
                        return true;
                case "student":
                        return true;
                case "text":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "order":
                    return true;
                case "student":
                    return true;
                case "text":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "order":
                    return IRepresentOrder.class;
                case "student":
                    return Student.class;
                case "text":
                    return byte[].class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ExtractTextRelation> _dslPath = new Path<ExtractTextRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ExtractTextRelation");
    }
            

    /**
     * @return Приказ. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ExtractTextRelation#getOrder()
     */
    public static IRepresentOrderGen.Path<IRepresentOrder> order()
    {
        return _dslPath.order();
    }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ExtractTextRelation#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Текст выписки.
     * @see ru.tandemservice.movestudentrmc.entity.ExtractTextRelation#getText()
     */
    public static PropertyPath<byte[]> text()
    {
        return _dslPath.text();
    }

    public static class Path<E extends ExtractTextRelation> extends EntityPath<E>
    {
        private IRepresentOrderGen.Path<IRepresentOrder> _order;
        private Student.Path<Student> _student;
        private PropertyPath<byte[]> _text;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приказ. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ExtractTextRelation#getOrder()
     */
        public IRepresentOrderGen.Path<IRepresentOrder> order()
        {
            if(_order == null )
                _order = new IRepresentOrderGen.Path<IRepresentOrder>(L_ORDER, this);
            return _order;
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ExtractTextRelation#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Текст выписки.
     * @see ru.tandemservice.movestudentrmc.entity.ExtractTextRelation#getText()
     */
        public PropertyPath<byte[]> text()
        {
            if(_text == null )
                _text = new PropertyPath<byte[]>(ExtractTextRelationGen.P_TEXT, this);
            return _text;
        }

        public Class getEntityClass()
        {
            return ExtractTextRelation.class;
        }

        public String getEntityName()
        {
            return "extractTextRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
