package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import ru.tandemservice.uni.entity.catalog.StudentStatus;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class StudentStatusDSHandler extends DefaultComboDataSourceHandler {

    public StudentStatusDSHandler(String ownerId) {
        super(ownerId, StudentStatus.class);

    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep) {

        ep.dqlBuilder.where(DQLExpressions.in(DQLExpressions.property(StudentStatus.usedInSystem().fromAlias("e")), DQLExpressions.value(true)));


        String filter = ep.input.getComboFilterByValue();
        ;
        if (StringUtils.isNotBlank(filter)) {
            ep.dqlBuilder.where(like(DQLFunctions.upper(property(StudentStatus.title().fromAlias("e"))), value(CoreStringUtils.escapeLike(filter, true))));
        }
    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep) {
        ep.dqlBuilder.order("e." + StudentStatus.priority());
    }


}
