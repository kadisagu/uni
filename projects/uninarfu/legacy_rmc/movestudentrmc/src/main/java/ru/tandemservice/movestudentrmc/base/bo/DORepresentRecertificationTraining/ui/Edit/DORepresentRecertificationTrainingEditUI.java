package ru.tandemservice.movestudentrmc.base.bo.DORepresentRecertificationTraining.ui.Edit;

import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentEditUI;
import ru.tandemservice.movestudentrmc.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudentrmc.entity.DocRecertificationTrainingDiscipline;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.RepresentRecertificationTraining;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.unisession.entity.document.SessionTransferOutsideOperation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DORepresentRecertificationTrainingEditUI extends AbstractDORepresentEditUI
{

    private List<SessionTransferOutsideOperation> list1 = new ArrayList<SessionTransferOutsideOperation>();
    private List<SessionTransferOutsideOperation> list2 = new ArrayList<SessionTransferOutsideOperation>();

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        super.onBeforeDataSourceFetch(dataSource);

        RepresentRecertificationTraining rep = (RepresentRecertificationTraining) getRepresentObj();
        dataSource.put("eduYear", rep.getEducationYear());

        if (DORepresentRecertificationTrainingEdit.TRANSFER_DS1.equals(dataSource.getName())) {
            dataSource.put("exceptList", getList2());
        }
        else if (DORepresentRecertificationTrainingEdit.TRANSFER_DS2.equals(dataSource.getName())) {
            dataSource.put("exceptList", getList1());
        }
    }

    @Override
    protected void initDocument() {
        super.initDocument();
        _studentStatusNewStr = _student.getStatus().getTitle();

        List<DocRecertificationTrainingDiscipline> list = MoveStudentDaoFacade.getGrantDAO().getList(DocRecertificationTrainingDiscipline.class, DocRecertificationTrainingDiscipline.representation(), getRepresentObj());
        for (DocRecertificationTrainingDiscipline rel : list)
            if (rel.isRecertification())
                this.list2.add(rel.getSessionTransferOutsideOperation());
            else
                this.list1.add(rel.getSessionTransferOutsideOperation());
    }

    //TODO: зачем этот метод тягаем везде?
    @Override
    protected void FetchStudents(Representation doc) {
        _studentDocList.clear();
        _studentList.clear();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocRepresentStudentBase.class, "stud").column("stud")
                .where(DQLExpressions.eq(
                        DQLExpressions.property(DocRepresentStudentBase.representation().fromAlias("stud")),
                        DQLExpressions.value(doc.getId())));

        List<DocRepresentStudentBase> studentList = builder.createStatement(getSupport().getSession()).list();

        for (DocRepresentStudentBase student : studentList) {
            _studentDocList.add(student);
            _studentList.add(student.getStudent());
        }

        _student = _studentList.get(0);
    }

    @Override
    @Transactional
    public void onClickSave() throws IOException {
        if (getList1().isEmpty() && getList2().isEmpty())
            throw new ApplicationException("Оба списка дисциплин пусты");

        super.onClickSave();

        //перезаписываем мероприятия
        List<DocRecertificationTrainingDiscipline> list = MoveStudentDaoFacade.getGrantDAO().getList(DocRecertificationTrainingDiscipline.class, DocRecertificationTrainingDiscipline.representation(), getRepresentObj());
        for (DocRecertificationTrainingDiscipline rel : list)
            MoveStudentDaoFacade.getGrantDAO().delete(rel);

        saveList(getList1(), false);
        saveList(getList2(), true);
    }

    private void saveList(List<SessionTransferOutsideOperation> list, boolean flag) {
        if (list == null || list.isEmpty())
            return;

        RepresentRecertificationTraining rep = (RepresentRecertificationTraining) getRepresentObj();

        for (SessionTransferOutsideOperation op : list) {
            DocRecertificationTrainingDiscipline rel = new DocRecertificationTrainingDiscipline();
            rel.setRepresentation(rep);
            rel.setSessionTransferOutsideOperation(op);
            rel.setRecertification(flag);

            MoveStudentDaoFacade.getGrantDAO().saveOrUpdate(rel);
        }
    }

    public List<SessionTransferOutsideOperation> getList1() {
        return list1;
    }

    public void setList1(List<SessionTransferOutsideOperation> list1) {
        this.list1 = list1;
    }

    public List<SessionTransferOutsideOperation> getList2() {
        return list2;
    }

    public void setList2(List<SessionTransferOutsideOperation> list2) {
        this.list2 = list2;
    }

}
