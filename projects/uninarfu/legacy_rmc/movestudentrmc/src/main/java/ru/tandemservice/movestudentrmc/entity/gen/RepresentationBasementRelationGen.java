package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.entity.RepresentationBasementRelation;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationBasement;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Основания представления
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RepresentationBasementRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.RepresentationBasementRelation";
    public static final String ENTITY_NAME = "representationBasementRelation";
    public static final int VERSION_HASH = 2067568023;
    private static IEntityMeta ENTITY_META;

    public static final String L_REPRESENTATION = "representation";
    public static final String L_BASEMENT = "basement";
    public static final String P_BASEMENT_DATE = "basementDate";
    public static final String P_BASEMENT_NUMBER = "basementNumber";

    private Representation _representation;     // Базовое представление в приказ
    private RepresentationBasement _basement;     // Основания представлений
    private Date _basementDate;     // Дата основания
    private String _basementNumber;     // Номер основания

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Базовое представление в приказ. Свойство не может быть null.
     */
    @NotNull
    public Representation getRepresentation()
    {
        return _representation;
    }

    /**
     * @param representation Базовое представление в приказ. Свойство не может быть null.
     */
    public void setRepresentation(Representation representation)
    {
        dirty(_representation, representation);
        _representation = representation;
    }

    /**
     * @return Основания представлений. Свойство не может быть null.
     */
    @NotNull
    public RepresentationBasement getBasement()
    {
        return _basement;
    }

    /**
     * @param basement Основания представлений. Свойство не может быть null.
     */
    public void setBasement(RepresentationBasement basement)
    {
        dirty(_basement, basement);
        _basement = basement;
    }

    /**
     * @return Дата основания.
     */
    public Date getBasementDate()
    {
        return _basementDate;
    }

    /**
     * @param basementDate Дата основания.
     */
    public void setBasementDate(Date basementDate)
    {
        dirty(_basementDate, basementDate);
        _basementDate = basementDate;
    }

    /**
     * @return Номер основания.
     */
    @Length(max=255)
    public String getBasementNumber()
    {
        return _basementNumber;
    }

    /**
     * @param basementNumber Номер основания.
     */
    public void setBasementNumber(String basementNumber)
    {
        dirty(_basementNumber, basementNumber);
        _basementNumber = basementNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RepresentationBasementRelationGen)
        {
            setRepresentation(((RepresentationBasementRelation)another).getRepresentation());
            setBasement(((RepresentationBasementRelation)another).getBasement());
            setBasementDate(((RepresentationBasementRelation)another).getBasementDate());
            setBasementNumber(((RepresentationBasementRelation)another).getBasementNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RepresentationBasementRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RepresentationBasementRelation.class;
        }

        public T newInstance()
        {
            return (T) new RepresentationBasementRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "representation":
                    return obj.getRepresentation();
                case "basement":
                    return obj.getBasement();
                case "basementDate":
                    return obj.getBasementDate();
                case "basementNumber":
                    return obj.getBasementNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "representation":
                    obj.setRepresentation((Representation) value);
                    return;
                case "basement":
                    obj.setBasement((RepresentationBasement) value);
                    return;
                case "basementDate":
                    obj.setBasementDate((Date) value);
                    return;
                case "basementNumber":
                    obj.setBasementNumber((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "representation":
                        return true;
                case "basement":
                        return true;
                case "basementDate":
                        return true;
                case "basementNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "representation":
                    return true;
                case "basement":
                    return true;
                case "basementDate":
                    return true;
                case "basementNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "representation":
                    return Representation.class;
                case "basement":
                    return RepresentationBasement.class;
                case "basementDate":
                    return Date.class;
                case "basementNumber":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RepresentationBasementRelation> _dslPath = new Path<RepresentationBasementRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RepresentationBasementRelation");
    }
            

    /**
     * @return Базовое представление в приказ. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentationBasementRelation#getRepresentation()
     */
    public static Representation.Path<Representation> representation()
    {
        return _dslPath.representation();
    }

    /**
     * @return Основания представлений. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentationBasementRelation#getBasement()
     */
    public static RepresentationBasement.Path<RepresentationBasement> basement()
    {
        return _dslPath.basement();
    }

    /**
     * @return Дата основания.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentationBasementRelation#getBasementDate()
     */
    public static PropertyPath<Date> basementDate()
    {
        return _dslPath.basementDate();
    }

    /**
     * @return Номер основания.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentationBasementRelation#getBasementNumber()
     */
    public static PropertyPath<String> basementNumber()
    {
        return _dslPath.basementNumber();
    }

    public static class Path<E extends RepresentationBasementRelation> extends EntityPath<E>
    {
        private Representation.Path<Representation> _representation;
        private RepresentationBasement.Path<RepresentationBasement> _basement;
        private PropertyPath<Date> _basementDate;
        private PropertyPath<String> _basementNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Базовое представление в приказ. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentationBasementRelation#getRepresentation()
     */
        public Representation.Path<Representation> representation()
        {
            if(_representation == null )
                _representation = new Representation.Path<Representation>(L_REPRESENTATION, this);
            return _representation;
        }

    /**
     * @return Основания представлений. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentationBasementRelation#getBasement()
     */
        public RepresentationBasement.Path<RepresentationBasement> basement()
        {
            if(_basement == null )
                _basement = new RepresentationBasement.Path<RepresentationBasement>(L_BASEMENT, this);
            return _basement;
        }

    /**
     * @return Дата основания.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentationBasementRelation#getBasementDate()
     */
        public PropertyPath<Date> basementDate()
        {
            if(_basementDate == null )
                _basementDate = new PropertyPath<Date>(RepresentationBasementRelationGen.P_BASEMENT_DATE, this);
            return _basementDate;
        }

    /**
     * @return Номер основания.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentationBasementRelation#getBasementNumber()
     */
        public PropertyPath<String> basementNumber()
        {
            if(_basementNumber == null )
                _basementNumber = new PropertyPath<String>(RepresentationBasementRelationGen.P_BASEMENT_NUMBER, this);
            return _basementNumber;
        }

        public Class getEntityClass()
        {
            return RepresentationBasementRelation.class;
        }

        public String getEntityName()
        {
            return "representationBasementRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
