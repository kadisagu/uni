package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListRepresentWeekend;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Списочное представление о предоставлении канукул
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ListRepresentWeekendGen extends ListRepresent
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.ListRepresentWeekend";
    public static final String ENTITY_NAME = "listRepresentWeekend";
    public static final int VERSION_HASH = 68424087;
    private static IEntityMeta ENTITY_META;

    public static final String P_DATE_BEGINNING_WEEKEND = "dateBeginningWeekend";
    public static final String P_DATE_END_OF_WEEKEND = "dateEndOfWeekend";

    private Date _dateBeginningWeekend;     // Дата начала канукул
    private Date _dateEndOfWeekend;     // Дата окончания каникул

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата начала канукул. Свойство не может быть null.
     */
    @NotNull
    public Date getDateBeginningWeekend()
    {
        return _dateBeginningWeekend;
    }

    /**
     * @param dateBeginningWeekend Дата начала канукул. Свойство не может быть null.
     */
    public void setDateBeginningWeekend(Date dateBeginningWeekend)
    {
        dirty(_dateBeginningWeekend, dateBeginningWeekend);
        _dateBeginningWeekend = dateBeginningWeekend;
    }

    /**
     * @return Дата окончания каникул. Свойство не может быть null.
     */
    @NotNull
    public Date getDateEndOfWeekend()
    {
        return _dateEndOfWeekend;
    }

    /**
     * @param dateEndOfWeekend Дата окончания каникул. Свойство не может быть null.
     */
    public void setDateEndOfWeekend(Date dateEndOfWeekend)
    {
        dirty(_dateEndOfWeekend, dateEndOfWeekend);
        _dateEndOfWeekend = dateEndOfWeekend;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ListRepresentWeekendGen)
        {
            setDateBeginningWeekend(((ListRepresentWeekend)another).getDateBeginningWeekend());
            setDateEndOfWeekend(((ListRepresentWeekend)another).getDateEndOfWeekend());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ListRepresentWeekendGen> extends ListRepresent.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ListRepresentWeekend.class;
        }

        public T newInstance()
        {
            return (T) new ListRepresentWeekend();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "dateBeginningWeekend":
                    return obj.getDateBeginningWeekend();
                case "dateEndOfWeekend":
                    return obj.getDateEndOfWeekend();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "dateBeginningWeekend":
                    obj.setDateBeginningWeekend((Date) value);
                    return;
                case "dateEndOfWeekend":
                    obj.setDateEndOfWeekend((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "dateBeginningWeekend":
                        return true;
                case "dateEndOfWeekend":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "dateBeginningWeekend":
                    return true;
                case "dateEndOfWeekend":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "dateBeginningWeekend":
                    return Date.class;
                case "dateEndOfWeekend":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ListRepresentWeekend> _dslPath = new Path<ListRepresentWeekend>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ListRepresentWeekend");
    }
            

    /**
     * @return Дата начала канукул. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentWeekend#getDateBeginningWeekend()
     */
    public static PropertyPath<Date> dateBeginningWeekend()
    {
        return _dslPath.dateBeginningWeekend();
    }

    /**
     * @return Дата окончания каникул. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentWeekend#getDateEndOfWeekend()
     */
    public static PropertyPath<Date> dateEndOfWeekend()
    {
        return _dslPath.dateEndOfWeekend();
    }

    public static class Path<E extends ListRepresentWeekend> extends ListRepresent.Path<E>
    {
        private PropertyPath<Date> _dateBeginningWeekend;
        private PropertyPath<Date> _dateEndOfWeekend;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата начала канукул. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentWeekend#getDateBeginningWeekend()
     */
        public PropertyPath<Date> dateBeginningWeekend()
        {
            if(_dateBeginningWeekend == null )
                _dateBeginningWeekend = new PropertyPath<Date>(ListRepresentWeekendGen.P_DATE_BEGINNING_WEEKEND, this);
            return _dateBeginningWeekend;
        }

    /**
     * @return Дата окончания каникул. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresentWeekend#getDateEndOfWeekend()
     */
        public PropertyPath<Date> dateEndOfWeekend()
        {
            if(_dateEndOfWeekend == null )
                _dateEndOfWeekend = new PropertyPath<Date>(ListRepresentWeekendGen.P_DATE_END_OF_WEEKEND, this);
            return _dateEndOfWeekend;
        }

        public Class getEntityClass()
        {
            return ListRepresentWeekend.class;
        }

        public String getEntityName()
        {
            return "listRepresentWeekend";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
