package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.IDocRepresentWithNewGroup;
import ru.tandemservice.uni.entity.orgstruct.Group;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import ru.tandemservice.movestudentrmc.entity.IDocRepresentWithNewGroup;

@SuppressWarnings({"all", "unchecked", "unused"})
public abstract class IDocRepresentWithNewGroupGen extends InterfaceStubBase
 implements IDocRepresentWithNewGroup{
    public static final int VERSION_HASH = -322672646;

    public static final String L_NEW_GROUP = "newGroup";
    public static final String L_GROUP_OLD = "groupOld";

    private Group _newGroup;
    private Group _groupOld;

    @NotNull

    public Group getNewGroup()
    {
        return _newGroup;
    }

    public void setNewGroup(Group newGroup)
    {
        _newGroup = newGroup;
    }

    @NotNull

    public Group getGroupOld()
    {
        return _groupOld;
    }

    public void setGroupOld(Group groupOld)
    {
        _groupOld = groupOld;
    }

    private static final Path<IDocRepresentWithNewGroup> _dslPath = new Path<IDocRepresentWithNewGroup>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ru.tandemservice.movestudentrmc.entity.IDocRepresentWithNewGroup");
    }
            

    /**
     * @return Новая группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.IDocRepresentWithNewGroup#getNewGroup()
     */
    public static Group.Path<Group> newGroup()
    {
        return _dslPath.newGroup();
    }

    /**
     * @return Старая группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.IDocRepresentWithNewGroup#getGroupOld()
     */
    public static Group.Path<Group> groupOld()
    {
        return _dslPath.groupOld();
    }

    public static class Path<E extends IDocRepresentWithNewGroup> extends EntityPath<E>
    {
        private Group.Path<Group> _newGroup;
        private Group.Path<Group> _groupOld;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Новая группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.IDocRepresentWithNewGroup#getNewGroup()
     */
        public Group.Path<Group> newGroup()
        {
            if(_newGroup == null )
                _newGroup = new Group.Path<Group>(L_NEW_GROUP, this);
            return _newGroup;
        }

    /**
     * @return Старая группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.IDocRepresentWithNewGroup#getGroupOld()
     */
        public Group.Path<Group> groupOld()
        {
            if(_groupOld == null )
                _groupOld = new Group.Path<Group>(L_GROUP_OLD, this);
            return _groupOld;
        }

        public Class getEntityClass()
        {
            return IDocRepresentWithNewGroup.class;
        }

        public String getEntityName()
        {
            return "ru.tandemservice.movestudentrmc.entity.IDocRepresentWithNewGroup";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
