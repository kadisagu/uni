package ru.tandemservice.movestudentrmc.base.bo.DOOrder.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.sec.entity.Principal;
import ru.tandemservice.movestudentrmc.entity.Principal2Visa;

public interface IOrderVisaDAO extends INeedPersistenceSupport {


    public Principal2Visa getPrincipal2Visa(Principal principal);

    public Principal2Visa getPrincipal2Visa();

    public void updateOrderVisa(Principal2Visa principal2Visa);
}
