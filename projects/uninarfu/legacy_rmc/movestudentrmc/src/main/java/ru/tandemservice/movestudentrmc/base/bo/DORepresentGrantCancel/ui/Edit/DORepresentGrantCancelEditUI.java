package ru.tandemservice.movestudentrmc.base.bo.DORepresentGrantCancel.ui.Edit;

import org.apache.commons.lang.time.DateUtils;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.GrantViewDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentEditUI;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.RepresentGrantCancel;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.uniedu.base.bo.EducationYear.EducationYearManager;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DORepresentGrantCancelEditUI extends AbstractDORepresentEditUI
{

    @Override
    protected void FetchStudents(Representation doc) {
        _studentDocList.clear();
        _studentList.clear();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocRepresentStudentBase.class, "stud").column("stud");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(DocRepresentStudentBase.representation().fromAlias("stud")),
                DQLExpressions.value(doc.getId())));

        List<DocRepresentStudentBase> studentList = builder.createStatement(getSupport().getSession()).list();
        for (DocRepresentStudentBase student : studentList) {

            _studentDocList.add(student);
            _studentList.add(student.getStudent());
        }

        _student = _studentList.get(0);
    }

    @Override
    protected void initDocument() {
        super.initDocument();

        if (getRepresentId() == null) {
            RepresentGrantCancel represent = (RepresentGrantCancel) this.getRepresentObj();
            //------------------------------------------------------------------------------------
            //represent.setEducationYear(EducationYearManager.instance().dao().getCurrent());
            //------------------------------------------------------------------------------------
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(DateUtils.addMonths(new Date(), 1));
            represent.setDateCancelPayment(CoreDateUtils.getMonthFirstTimeMoment(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH)));
        }

    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        super.onBeforeDataSourceFetch(dataSource);

        if (GrantViewDSHandler.GRANT_VIEW_DS.equals(dataSource.getName())) {
            dataSource.put(GrantViewDSHandler.STUDENT_ID, _student.getId());
            //---------------------------------------------------------------------------------------------------------------
            //dataSource.put(GrantViewDSHandler.EDU_YEAR, ((RepresentGrantCancel) this.getRepresentObj()).getEducationYear());
            //---------------------------------------------------------------------------------------------------------------
            dataSource.put(GrantViewDSHandler.MONTH, ((RepresentGrantCancel) this.getRepresentObj()).getDateCancelPayment());
        }
    }
    
    //---------------------------------------------------------------------------------
    @Override
    public void onClickSave() throws IOException {
    	
    	RepresentGrantCancel represent = (RepresentGrantCancel) this.getRepresentObj();
    	represent.setStartDate(represent.getDateCancelPayment());
    	super.onClickSave();
    	
    }
    //---------------------------------------------------------------------------------
}


