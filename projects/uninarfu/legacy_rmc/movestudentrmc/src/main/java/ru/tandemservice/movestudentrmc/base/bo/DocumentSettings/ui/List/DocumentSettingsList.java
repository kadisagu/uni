package ru.tandemservice.movestudentrmc.base.bo.DocumentSettings.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import ru.tandemservice.movestudentrmc.base.bo.DocumentSettings.logic.DocumentTypeDSHandler;
import ru.tandemservice.movestudentrmc.entity.catalog.Document;

@Configuration
public class DocumentSettingsList extends BusinessComponentManager {

    public static final String TYPE_DS = "documentTypeDS";

    @Bean
    public ColumnListExtPoint typeDS() {
        return columnListExtPointBuilder(TYPE_DS)
                .addColumn(textColumn(DocumentTypeDSHandler.TYPE_COLUMN, Document.title()).order().create())
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).create())
                .create();
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(TYPE_DS, typeDS()).handler(documentTypeDSHandler()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> documentTypeDSHandler() {
        return new DocumentTypeDSHandler(getName());
    }
}
