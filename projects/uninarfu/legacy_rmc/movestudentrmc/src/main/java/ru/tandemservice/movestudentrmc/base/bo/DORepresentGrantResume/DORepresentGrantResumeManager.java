package ru.tandemservice.movestudentrmc.base.bo.DORepresentGrantResume;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentGrantResume.logic.DORepresentGrantResumeDAO;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentGrantResume.ui.Edit.DORepresentGrantResumeEdit;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentGrantResume.ui.View.DORepresentGrantResumeView;
import ru.tandemservice.movestudentrmc.docord.IDOObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IDOObjectModifyDAO;

@Configuration
public class DORepresentGrantResumeManager extends BusinessObjectManager implements IDOObjectByRepresentTypeManager
{

    public static DORepresentGrantResumeManager instance() {
        return instance(DORepresentGrantResumeManager.class);
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return DORepresentGrantResumeEdit.class;
    }

    @Bean
    public IDOObjectModifyDAO getDOObjectModifyDAO() {
        return new DORepresentGrantResumeDAO();
    }

    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return DORepresentGrantResumeView.class;
    }

}
