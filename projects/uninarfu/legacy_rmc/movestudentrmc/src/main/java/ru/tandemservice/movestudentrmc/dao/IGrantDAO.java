package ru.tandemservice.movestudentrmc.dao;

import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.Grant;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.List;
import java.util.Map;

public interface IGrantDAO extends IUniBaseDao {

    /*
     * Можно ли добавить новую стипендию newGrantView студенту student на месяц month
     */
    boolean canAddGrant(GrantView newGrantView, Student student, String month);

    boolean checkStudentGrants(Long representId, GrantView grantView, List<Student> studentList, List<String> monthList, List<String> errorList);

    void saveStudentGrants(ListRepresent represent, GrantView grantView, List<Student> studentList, List<String> monthList, EducationYear educationYear);
    
    //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    void saveStudentGrants(ListRepresent represent, GrantView grantView, List<Student> studentList, List<String> monthList, EducationYear educationYear, boolean deleteGrants);
    //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    void saveStudentGrants(ListRepresent represent, GrantView grantView, Map<Student, Double> studentMap, String month, EducationYear educationYear);
    
    void commitStudentGrants(ListRepresent represent, ListOrder order);

    void rollbackStudentGrants(ListRepresent represent, ListOrder order);

    void deleteStudentGrants(ListRepresent represent);

    void deleteStudentGrants(Representation represent);

    void saveStudentGrants(Representation represent, GrantView grantView, Student student, List<String> monthList, EducationYear educationYear);
    
    //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
    void saveStudentGrants(Representation represent, GrantView grantView, Student student, List<String> monthList, EducationYear educationYear, boolean deleteGrants);
    //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

    void saveStudentGrants(Representation represent, GrantView grantView, Student student, double sum, String month, EducationYear educationYear);

    void commitStudentGrants(Representation represent, DocumentOrder order);

     void rollbackStudentGrants(Representation represent, DocumentOrder order);

    void saveGrantHistory(List<StudentGrantEntity> list, IAbstractRepresentation represent, String typeCode);

    void deleteGrantHistory(IAbstractRepresentation represent);

    /*
     * Согут ли вместе быть назначены две стипендии grant1 && grant2
     */
    boolean canAddGrant(Grant grant1, Grant grant2);



	
}
