package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.ListApprove;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.RepresentDSHandler;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DORepresentBaseListApproveUI extends UIPresenter {

    @Override
    public void onComponentRefresh() {

        List<ExtractStates> stateList = new ArrayList<ExtractStates>();

        stateList.add(DataAccessServices.dao().get(ExtractStates.class, ExtractStates.code().s(), "2"));
        stateList.add(DataAccessServices.dao().get(ExtractStates.class, ExtractStates.code().s(), "3"));

        getData().put("stateList", stateList);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (DORepresentBaseListApprove.REPRESENT_DS.equals(dataSource.getName())) {

            Map<String, Object> settingMap = _uiSettings.getAsMap(
                    RepresentDSHandler.DATE_REPRESENT_FILTER,
                    RepresentDSHandler.DEVELOP_FORM_FILTER,
                    RepresentDSHandler.FORMATIVE_ORG_UNIT_FILTER,
                    RepresentDSHandler.STUDENT_FIO_FILTER,
                    RepresentDSHandler.TYPE_REPRESENT_FILTER,
                    RepresentDSHandler.STATE_FILTER
            );
            dataSource.putAll(settingMap);
        }
    }

    public void onClickView()
    {
        BaseSearchListDataSource representDS = (BaseSearchListDataSource) getConfig().getDataSource(DORepresentBaseListApprove.REPRESENT_DS);
        IEntity e = representDS.getRecordById(getListenerParameterAsLong());

        Representation representBase = (Representation) ((DataWrapper) e).getWrapped();
        String type = representBase.getType().getCode();
        /*
        IDOObjectByRepresentTypeManager representManager = DORepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(type);
        _uiActivation.asCurrent(representManager.getViewComponentManager())
                .parameter(AbstractDORepresentViewUI.REPRESENTATION_ID, representBase.getId())
                .activate();
        */
    }

    public void onEditEntityFromList()
    {
        BaseSearchListDataSource representDS = (BaseSearchListDataSource) getConfig().getDataSource(DORepresentBaseListApprove.REPRESENT_DS);
        IEntity e = representDS.getRecordById(getListenerParameterAsLong());

        Representation representBase = (Representation) ((DataWrapper) e).getWrapped();

        List<Long> studentListId = new ArrayList<Long>();
        for (Object represent : representDS.getRecords()) {

            Student student = (Student) ((DataWrapper) represent).getProperty("student");
            studentListId.add(student.getId());
        }

        String type = representBase.getType().getCode();
        /*
        IDOObjectByRepresentTypeManager representManager = DORepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(type);
        _uiActivation.asCurrent(representManager.getEditComponentManager())
                .parameter(AbstractDORepresentEditUI.TYPE_ID, representBase.getRepresentation().getId())
                .parameter(AbstractDORepresentEditUI.REPRESENTATION_ID, representBase.getId())
                .parameter(AbstractDORepresentEditUI.STUDENT_LIST_ID, studentListId)
                .activate();
		*/
    }

    public Boolean getIsApprove() {

        BaseSearchListDataSource representDS = (BaseSearchListDataSource) getConfig().getDataSource(DORepresentBaseListApprove.REPRESENT_DS);
        IEntity e = representDS.getCurrentEntity();

        Representation representBase = ((Representation) e);
        return !representBase.getState().getCode().equals("2");
    }
}
