package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import ru.tandemservice.uni.entity.catalog.CompensationType;

public class CompensationTypeDSHandler extends DefaultComboDataSourceHandler {

    public CompensationTypeDSHandler(String ownerId) {

        super(ownerId, CompensationType.class, CompensationType.P_SHORT_TITLE);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        super.prepareConditions(ep);

    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
    {
        super.prepareOrders(ep);
        //ep.dqlBuilder.order("e." + CompensationType.shortTitle());
    }
}
