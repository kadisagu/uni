package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;


public class MS_movestudentrmc_1x0x0_0to1 extends IndependentMigrationScript {
    @Override
    public void run(DBTool tool) throws Exception {


        // vch - добавил еще одну удалялку
        // связь один ко многим - значит можно уронить
        if (tool.tableExists("relrepresentationreasonbasic_t"))
            tool.executeUpdate("delete from relrepresentationreasonbasic_t");


        if (tool.tableExists("representationbasement_t"))
            tool.executeUpdate("delete from representationbasement_t");
    }
}