package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.entity.ListOrdListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListOrderListRepresentDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput> {


    public static final String STATE_COLUMN = "state";
    public static final String DATE_BEGINING_PAYMENT = "createDate";
    public static final String REPRESENT_TITLE_COLUMN = "listRepresentTitle";


    public ListOrderListRepresentDSHandler(String ownerId) {
        super(ownerId);
    }


    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context) {

        Object studentId = context.get("studentId");


        DQLSelectBuilder inOrderNotCommit = new DQLSelectBuilder()
                .fromEntity(ListOrdListRepresent.class, "o")
                .joinEntity("o", DQLJoinType.full, RelListRepresentStudents.class, "srep", DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("srep")), DQLExpressions.property(ListOrdListRepresent.representation().fromAlias("o"))))
                .column("o")
                .where(DQLExpressions.eqValue(DQLExpressions.property(RelListRepresentStudents.student().id().fromAlias("srep")), studentId))

                        //Представления  в  НЕ проведенном приказе
                .where(DQLExpressions.ne(DQLExpressions.property(ListOrdListRepresent.order().state().code().fromAlias("o")), DQLExpressions.value("5")));


        DQLSelectBuilder notInOrder = new DQLSelectBuilder()
                .fromEntity(RelListRepresentStudents.class, "rel")
                .joinEntity("rel", DQLJoinType.left, ListOrdListRepresent.class, "o",
                            DQLExpressions.eq(
                                    DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("rel")),
                                    DQLExpressions.property(ListOrdListRepresent.representation().fromAlias("o"))))
                .column("rel.representation")
                .where(DQLExpressions.eqValue(DQLExpressions.property(RelListRepresentStudents.student().id().fromAlias("rel")), studentId))

                        //Представления не включенные в приказ
                .where(DQLExpressions.isNull(DQLExpressions.property("o")))
//        .distinct()
                ;

        List<ListOrdListRepresent> inOrderNotCommitList = inOrderNotCommit.createStatement(getSession()).list();

        List<ListRepresent> notInOrderList = notInOrder.createStatement(getSession()).list();

        List<DataWrapper> resultList = new ArrayList<DataWrapper>();

        for (ListOrdListRepresent data : inOrderNotCommitList) {

            DataWrapper w = new DataWrapper(data.getRepresentation());
            w.setProperty("order", data.getOrder());
            w.setProperty("represent", data.getRepresentation());
            resultList.add(w);
        }

        for (ListRepresent data : notInOrderList) {

            DataWrapper w = new DataWrapper(data);
            w.setProperty("represent", data);
            resultList.add(w);
        }

        Collections.sort(resultList, new EntityComparator(input.getEntityOrder()));

        return ListOutputBuilder.get(input, resultList).pageable(true).build();

    }


}
