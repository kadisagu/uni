package ru.tandemservice.movestudentrmc.base.bo.ListRepresentAdmissionToPractice.logic;


import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.AbstractListRepresentDAO;
import ru.tandemservice.movestudentrmc.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.StudentCustomStateCICodes;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.employee.Student;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ListRepresentAdmissionToPracticeManagerModifyDAO extends AbstractListRepresentDAO
{
    public void save(ListRepresent listRepresent, List<Student> studentSelectedList, DocListRepresentBasics representBasics) {
    }

    @Override
    public boolean doCommit(ListRepresent represent, ErrorCollector error) {
        if (!super.doCommit(represent, UserContext.getInstance().getErrorCollector()))
            return false;

        ListRepresentAdmissionToPractice listRepresent = (ListRepresentAdmissionToPractice) represent;

        StudentCustomStateCI state = UniDaoFacade.getCoreDao().get(StudentCustomStateCI.class, StudentCustomStateCI.code(), StudentCustomStateCICodes.PRACTICE);

        MoveStudentDaoFacade.getCustomStateDAO().commitStudentCustomState(state, listRepresent.getDateBeginningPractice(), listRepresent.getDateEndOfPractice(), listRepresent);

        return true;
    }

    @Override
    public boolean doRollback(ListRepresent represent, ErrorCollector error) {
        if (!super.doRollback(represent, UserContext.getInstance().getErrorCollector()))
            return false;

        //удалим статус, который назначили этим представлением
        MoveStudentDaoFacade.getCustomStateDAO().rollbackStudentCustomState(represent);
        return true;
    }

    @Transactional
    public void saveSupport(ListRepresent listRepresent, List<DataWrapper> studentSelectedList, DocListRepresentBasics representBasics) {

        ListRepresentAdmissionToPractice listPractice = (ListRepresentAdmissionToPractice) listRepresent;

        List<Student> stuSelectedList = new ArrayList<Student>();
        List<StudentPracticeData> practiceDataList = new ArrayList<>();
        for (DataWrapper dataW : studentSelectedList) {
            Student student = (Student) dataW.getProperty("student");
            StudentPracticeData studentPracticeData = (StudentPracticeData) dataW.getProperty("practiceData");
            studentPracticeData.setListRepresent(listRepresent);
            practiceDataList.add(studentPracticeData);
            stuSelectedList.add(student);
        }


        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(RelListRepresentStudents.class)
                .fromEntity(RelListRepresentStudents.class, "rls")
                .where(DQLExpressions.eq
                        (DQLExpressions.property(RelListRepresentStudents.representation()), DQLExpressions.value(listRepresent)));


        //Почистим старых студентов
        deleteBuilder.createStatement(getSession()).execute();

        //Сохраним оператора, изменившего представление
        IPrincipalContext context = ContextLocal.getUserContext().getPrincipalContext();
        Person person = PersonManager.instance().dao().getPerson(context);
        if (person != null)
            listPractice.setOperator(person);

        listPractice.setCreator(context);


        baseCreateOrUpdate(listRepresent);
        baseCreateOrUpdate(representBasics);
        for (Student student : stuSelectedList) {
            RelListRepresentStudents relListRepresentGrantStudents = new RelListRepresentStudents();
            relListRepresentGrantStudents.setRepresentation(listRepresent);
            relListRepresentGrantStudents.setStudent(student);
            baseCreateOrUpdate(relListRepresentGrantStudents);
        }

        deleteStudentPracticeData(listPractice);
        saveStudentPracticeData(practiceDataList);
    }

    public void deleteStudentPracticeData(ListRepresent represent) {
        List<StudentPracticeData> dataList = DataAccessServices.dao().getList(StudentPracticeData.class, StudentPracticeData.listRepresent(), represent);
        for (StudentPracticeData data : dataList)
            DataAccessServices.dao().delete(data);
    }

    public void saveStudentPracticeData(List<StudentPracticeData> practiceDataList) {
        for (StudentPracticeData data : practiceDataList)
            DataAccessServices.dao().save(data);
    }


    @Override
    public void buildBodyExtract(ListOrdListRepresent listOrdListRepresent,
                                 Student student, RtfDocument docExtract, Map<Student, OrderParagraphInfo> map)
    {
        super.buildBodyExtract(listOrdListRepresent, student, docExtract, map);

        RtfInjectModifier im = new RtfInjectModifier();
        
        //--------------------------------------------------------------------------------------
        //Порядковый номер студента в параграфе
        
        ListRepresentAdmissionToPractice representation = (ListRepresentAdmissionToPractice) listOrdListRepresent.getRepresentation();
        
        //Данные о практике для студента
        String spd_alias = "spd";
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(StudentPracticeData.class, spd_alias)
                .where(eq(property(spd_alias, StudentPracticeData.student()), value(student)))
                .where(eq(property(spd_alias, StudentPracticeData.listRepresent()), value(representation)));	
        List<StudentPracticeData> listOfStudentPracticeData = IUniBaseDao.instance.get().getList(dql);
        StudentPracticeData data = new StudentPracticeData();
        if(listOfStudentPracticeData!=null && !listOfStudentPracticeData.isEmpty()) {
        	data = (StudentPracticeData) listOfStudentPracticeData.get(0);
        }
 
        //Номер студента в списке
        map.get(student).getStudentNumber();
        
        List<String[]> rowList = new ArrayList<>();
        String[] row = new String[6];
        row[0] = "" + map.get(student).getStudentNumber();
        row[1] = data.getStudent().getFullFio();
        row[2] = UtilPrintSupport.getCompensationTypeStr(student.getCompensationType());
        row[3] = data.getInnerAdvisor() == null ? "" : data.getInnerAdvisor().getFullFio();
        row[4] = data.getPracticeBase() == null ? "" : data.getPracticeBase().getTitle();
        //row[5] = data.getOuterAdvisor();
        rowList.add(row);
        
        new RtfTableModifier().put("T", rowList.toArray(new String[][]{})).modify(docExtract);
        
        //Руководитель практики
        im.put("supervisor", representation.getSupervisor() == null ? new RtfString().append("") : new RtfString().par().append("Руководитель практики: ").append(representation.getSupervisor().getFullFio()));
        //--------------------------------------------------------------------------------------

        

        ListRepresentAdmissionToPractice represent = (ListRepresentAdmissionToPractice) listOrdListRepresent.getRepresentation();

        ListPrintDoc.injectModifier(im, represent, student);

        im.modify(docExtract);
    }

    @Override
    public Map<Student, OrderParagraphInfo> buildBodyRepresent(List<ListRepresent> representationBase, RtfDocument document) {

        ListRepresentAdmissionToPracticePrintDoc printDoc = new ListRepresentAdmissionToPracticePrintDoc();
        RtfDocument rtfDocument = printDoc.createDocument(representationBase);
        document.getElementList().addAll(rtfDocument.getElementList());

        return printDoc.getParagInfomap();
    }


    @Override
    public List<Student> selectRepresent(ListRepresent listRepresent) {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(RelListRepresentStudents.class, "rls");

        builder.where(DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("rls")), DQLExpressions.value(listRepresent)));

        builder.column(DQLExpressions.property(RelListRepresentStudents.student().fromAlias("rls")));

        return builder.createStatement(getSession()).list();
    }

}
