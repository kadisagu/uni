package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import org.tandemframework.shared.person.catalog.entity.codes.IdentityCardTypeCodes;
import ru.tandemservice.movestudentrmc.entity.StudentPracticeData;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkGradeValueCatalogItemCodes;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionSlotRegularMark;

import java.util.*;

public class StudentDSHandlerPractice extends AbstractSearchDataSourceHandler<DSInput, DSOutput> {

    public static final String DEVELOP_FORM_FILTER = "developFormFilter";
    public static final String DECLARATION_FILTER = "declarationFilter";
    public static final String FORMATIVE_ORG_UNIT_FILTER = "formativeOrgUnitFilter";
    public static final String TYPE_REPRESENT_FILTER = "typeRepresentFilter";
    public static final String DATE_REPRESENT_FILTER = "dateRepresentFilter";
    public static final String STUDENT_FIO_FILTER = "studentFioFilter";
    public static final String EDUCATION_LEVELS_HIGH_SHOOL_FILTER = "educationLevelsHighSchoolDSFilter";
    public static final String EDUCATION_YEAR_FILTER = "educationYearFilter";


    public static final String DEVELOP_FORM_COLUMN = "developForm";
    public static final String FORMATIVE_ORG_UNIT_COLUMN = "formativeOrgUnit";
    public static final String TERRITORIAL_ORG_UNIT_COLUMN = "territorialOrgUnit";
    public static final String EDUCATION_LEVEL_HIGH_SCHOOL_COLUMN = "educationLevelHighSchool";
    public static final String EDUCATION_ORG_UNIT_COLUMN = "educationOrgUnit";
    public static final String TYPE_REPRESENT_COLUMN = "typeRepresent";
    public static final String COURSE_COLUMN = "course";
    public static final String GROUP_COLUMN = "group";
    public static final String BUDGET_COLUMN = "budget";
    public static final String DATE_REPRESENT_COLUMN = "dateRepresent";
    public static final String STUDENT_FIO_COLUMN = "studentFio";
    public static final String CHECKBOX_COLUMN = "checkbox";
    public static final String STUDENT_SELECTED_LIST = "studentSelectedList";
    public static final String ORDER_ID = "orderId";
    public static final String COMPENSATION_TYPE_COLUMN = "compensationType";
    public static final String DYPLOM_COLUMN = "dyplom";
    public static final String STATUS_COLUMN = "status";

    public static final List<String> USPESNO_SDAVSZIE_SESSIU =
            Arrays.asList(SessionMarkGradeValueCatalogItemCodes.UDOVLETVORITELNO,
                          SessionMarkGradeValueCatalogItemCodes.HOROSHO,
                          SessionMarkGradeValueCatalogItemCodes.OTLICHNO,
                          SessionMarkGradeValueCatalogItemCodes.ZACHTENO);


    public StudentDSHandlerPractice(String ownerId)
    {
        super(ownerId);
    }


    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        List<Object> territorialOrgUnitFilter = context.get("territorialOrgUnitFilter");
        List<Object> formativeOrgUnitFilter = context.get("formativeOrgUnitFilter");

        List<Object> educationLevelsHighSchoolDSFilter = context.get("educationLevelsHighSchoolDSFilter");
        Object educationLevelFilter = context.get("educationLevelFilter");
        List<Object> courseFilter = context.get("courseFilter");
        List<Object> groupFilter = context.get("groupFilter");
        List<Object> developFormFilter = context.get("developFormFilter");
        Object compensationTypeFilter = context.get("compensationTypeFilter");

        List<Object> educationOrgUnitFilter = context.get("educationOrgUnitFilter");

        List<Object> benefitFilter = context.get("benefitFilter");
        List<Object> studentFilter = context.get("studentFilter");

        Boolean isSuccessfullyHandOverSession = (Boolean) (context.get("isSuccessfullyHandOverSession") == null ? false : context.get("isSuccessfullyHandOverSession"));
        List<Object> educationYearFilter = context.get("educationYearFilter");
        List<Object> yearDistPartFilter = context.get("yearDistPartFilter");

        List<Object> studentStatusFilter = context.get("studentStatusFilter");


        Boolean isConsiderSessionResults = (Boolean) (context.get("isConsiderSessionResults") == null ? false : context.get("isConsiderSessionResults"));

        Boolean isforeignCitizens = (Boolean) (context.get("isforeignCitizens") == null ? false : context.get("isforeignCitizens"));

        Boolean isNoGroup = (Boolean) (context.get("isNoGroup") == null ? false : context.get("isNoGroup"));


        List<IdentifiableWrapper> sessionMarksResult = context.get("sessionMarksResult");


        DQLSelectBuilder builder = new DQLSelectBuilder();

        builder.fromEntity(Student.class, "s");

        if (isforeignCitizens) {
            FilterUtils.applySelectFilter(builder, "s", Student.compensationType().code(), CompensationTypeCodes.COMPENSATION_TYPE_BUDGET);
            builder.where(DQLExpressions.ne(DQLExpressions.property(Student.person().identityCard().cardType().code().fromAlias("s")), DQLExpressions.value(IdentityCardTypeCodes.PASPORT_GRAJDANINA_ROSSIYSKOY_FEDERATSII)));
        }

        if (benefitFilter != null && !benefitFilter.isEmpty()) {
            DQLSelectBuilder benefitBuilder = new DQLSelectBuilder();

            benefitBuilder.fromEntity(PersonBenefit.class, "b");
            FilterUtils.applySelectFilter(benefitBuilder, "b", PersonBenefit.benefit(), benefitFilter);
            benefitBuilder.addColumn(DQLExpressions.property(PersonBenefit.person().fromAlias("b")));

            builder.where(DQLExpressions.in(DQLExpressions.property(Student.person().fromAlias("s")), benefitBuilder.getQuery()));
        }

        if (isNoGroup)
            builder.where(DQLExpressions.isNull(DQLExpressions.property(Student.group().fromAlias("s"))));
        builder.where(DQLExpressions.eq(DQLExpressions.property(Student.archival().fromAlias("s")), DQLExpressions.value(false)));

        FilterUtils.applySelectFilter(builder, "s", Student.educationOrgUnit().formativeOrgUnit(), formativeOrgUnitFilter);

        FilterUtils.applySelectFilter(builder, "s", Student.educationOrgUnit().educationLevelHighSchool().orgUnit(), educationOrgUnitFilter);

        FilterUtils.applySelectFilter(builder, "s", Student.course(), courseFilter);

        FilterUtils.applySelectFilter(builder, "s", Student.group(), groupFilter);

        FilterUtils.applySelectFilter(builder, "s", Student.compensationType(), compensationTypeFilter);


        FilterUtils.applySelectFilter(builder, "s", Student.educationOrgUnit().territorialOrgUnit(), territorialOrgUnitFilter);

        FilterUtils.applySelectFilter(builder, "s", Student.educationOrgUnit().developForm(), developFormFilter);

        FilterUtils.applySelectFilter(builder, "s", Student.educationOrgUnit().educationLevelHighSchool(), educationLevelsHighSchoolDSFilter);

        FilterUtils.applySelectFilter(builder, "s", Student.status(), studentStatusFilter);


        if (educationLevelFilter != null)
            builder.where(DQLExpressions.in(
                    DQLExpressions.property(Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().fromAlias("s")),
                    getByRoot((StructureEducationLevels) educationLevelFilter)
            ));


        if (studentFilter != null && !studentFilter.isEmpty())
            FilterUtils.applySelectFilter(builder, "s", Student.id(), studentFilter);

        //Поиск среди успешно сдавших сессию
        if (isSuccessfullyHandOverSession) {
            List<Long> successfullyHandOverSession = successfullyHandOverSession(USPESNO_SDAVSZIE_SESSIU, yearDistPartFilter, educationYearFilter);
            if (!successfullyHandOverSession.isEmpty())
                builder.where(DQLExpressions.in(DQLExpressions.property(Student.id().fromAlias("s")), successfullyHandOverSession));

        }

        //Учитывать результаты сессии
        if (isConsiderSessionResults && sessionMarksResult != null) {
            List<Long> resultStudent = new ArrayList<Long>();
            for (IdentifiableWrapper w : sessionMarksResult) {

                if (w.getId().equals(Long.valueOf(1L))) {
                    resultStudent.addAll(considerSessionResults_4_5());
                }
                else {
                    if (w.getId().equals(Long.valueOf(0L)))
                        resultStudent.addAll(considerSessionResults(SessionMarkGradeValueCatalogItemCodes.HOROSHO));
                    if (w.getId().equals(Long.valueOf(2L))) {
                        resultStudent.addAll(considerSessionResults(SessionMarkGradeValueCatalogItemCodes.OTLICHNO));
                    }
                }
            }
            if (!resultStudent.isEmpty())
                builder.where(DQLExpressions.in(DQLExpressions.property(Student.id().fromAlias("s")), resultStudent));
        }

        //if (input.getEntityOrder() != null)
        //	builder.order(DQLExpressions.property("s", input.getEntityOrder().getKeyString()), input.getEntityOrder().getDirection());

        List<Student> studentList = builder.createStatement(getSession()).list();

        List<DataWrapper> resultList = new ArrayList<DataWrapper>();
        List<DataWrapper> selectedList = context.get(STUDENT_SELECTED_LIST);

        for (Student student : studentList) {

            Student studentData = (Student) student;

            StudentPracticeData practiceData = null;

            DataWrapper w = new DataWrapper(studentData.getId(), studentData.getTitle(), studentData);
            w.setProperty("student", studentData);
            w.setProperty("practiceData", practiceData);

            if (selectedList != null)
                if (selectedList.contains(w))
                    continue;

            resultList.add(w);
        }

        Collections.sort(resultList, new EntityComparator(input.getEntityOrder()));

        return ListOutputBuilder.get(input, resultList).pageable(true).build();
    }

    //Поиск среди успешно сдавших сессию либо зачет
    private List<Long> successfullyHandOverSession(final List<String> markCodes, List<Object> yearDistPartFilter, List<Object> educationYearFilter) {

        DQLSelectBuilder markBuilder = new DQLSelectBuilder();

        markBuilder.fromEntity(SessionMark.class, "m");
        markBuilder.where(DQLExpressions.and(
                DQLExpressions.in(DQLExpressions.property(SessionMark.cachedMarkValue().code().fromAlias("m")), markCodes),
                DQLExpressions.isNull(SessionMark.slot().studentWpeCAction().removalDate().fromAlias("m"))));

        markBuilder.addColumn(DQLExpressions.property(SessionMark.slot().studentWpeCAction().studentWpe().student().id().fromAlias("m")));
        markBuilder.group(DQLExpressions.property(SessionMark.slot().studentWpeCAction().studentWpe().student().id().fromAlias("m")));

        //Фильтруем по учебному году
        FilterUtils.applySelectFilter(markBuilder, "m", SessionMark.slot().studentWpeCAction().studentWpe().year().educationYear(), educationYearFilter);
        //Фильтруем по семестру
        FilterUtils.applySelectFilter(markBuilder, "m", SessionMark.slot().studentWpeCAction().studentWpe().part(), yearDistPartFilter);

        List<Long> allStudents = markBuilder.createStatement(getSession()).list();

        final List<Long> resultStudents = new ArrayList<Long>();

        BatchUtils.execute(allStudents, 256, new BatchUtils.Action<Long>() {
            @Override
            public void execute(Collection<Long> studs) {

                for (Long studentId : studs) {
                    boolean addStudent = true;

                    List<EppStudentWpeCAction> slots = DataAccessServices.dao().getList(EppStudentWpeCAction.class, EppStudentWpeCAction.studentWpe().student().id(), studentId);

                    List<SessionSlotRegularMark> marks = new DQLSelectBuilder().fromEntity(SessionSlotRegularMark.class, "s")
                            .where(DQLExpressions.and(
                                    DQLExpressions.in(DQLExpressions.property(SessionSlotRegularMark.slot().studentWpeCAction().fromAlias("s")), slots),
                                    DQLExpressions.eq(DQLExpressions.property(SessionSlotRegularMark.slot().studentWpeCAction().studentWpe().student().fromAlias("s")), DQLExpressions.value(studentId)),
                                    DQLExpressions.eq(DQLExpressions.property(SessionSlotRegularMark.slot().inSession().fromAlias("s")), DQLExpressions.value(true))))
                            .createStatement(DataAccessServices.dao().getComponentSession()).list();
                    if (slots.size() != marks.size())
                        continue;

                    for (SessionSlotRegularMark mark : marks) {
                        //Если находим оценку отличную от запрашиваемой - пропускаем студента
                        if (!markCodes.contains(mark.getCachedMarkValue().getCode())) {
                            addStudent = false;
                            break;
                        }
                    }
                    if (addStudent)
                        resultStudents.add(studentId);
                }
            }
        });

        return resultStudents;
    }

    //Поиск среди сдавших сессию на 4 и 5 либо зачет
    public List<Long> considerSessionResults_4_5() {

        DQLSelectBuilder markBuilder = new DQLSelectBuilder();

        markBuilder.fromEntity(SessionMark.class, "m");
        markBuilder.where(DQLExpressions.and(
                DQLExpressions.or(
                        DQLExpressions.eq(DQLExpressions.property(SessionMark.cachedMarkValue().code().fromAlias("m")), DQLExpressions.value(SessionMarkGradeValueCatalogItemCodes.HOROSHO)),
                        DQLExpressions.eq(DQLExpressions.property(SessionMark.cachedMarkValue().code().fromAlias("m")), DQLExpressions.value(SessionMarkGradeValueCatalogItemCodes.OTLICHNO))),
                DQLExpressions.isNull(SessionMark.slot().studentWpeCAction().removalDate().fromAlias("m"))));

        markBuilder.addColumn(DQLExpressions.property(SessionMark.slot().studentWpeCAction().studentWpe().student().id().fromAlias("m")));
        markBuilder.group(DQLExpressions.property(SessionMark.slot().studentWpeCAction().studentWpe().student().id().fromAlias("m")));

        List<Long> allStudents = markBuilder.createStatement(getSession()).list();

        final List<Long> resultStudents = new ArrayList<Long>();

        BatchUtils.execute(allStudents, 256, new BatchUtils.Action<Long>() {
            @Override
            public void execute(Collection<Long> studs) {

                for (Long studentId : studs) {
                    boolean is4 = false;
                    boolean is5 = false;
                    boolean isZacet = false;


                    List<EppStudentWpeCAction> slots = DataAccessServices.dao().getList(EppStudentWpeCAction.class, EppStudentWpeCAction.studentWpe().student().id(), studentId);

                    List<SessionSlotRegularMark> marks = new DQLSelectBuilder().fromEntity(SessionSlotRegularMark.class, "s")
                            .where(DQLExpressions.and(
                                    DQLExpressions.in(DQLExpressions.property(SessionSlotRegularMark.slot().studentWpeCAction().fromAlias("s")), slots),
                                    DQLExpressions.eq(DQLExpressions.property(SessionSlotRegularMark.slot().studentWpeCAction().studentWpe().student().fromAlias("s")), DQLExpressions.value(studentId)),
                                    DQLExpressions.eq(DQLExpressions.property(SessionSlotRegularMark.slot().inSession().fromAlias("s")), DQLExpressions.value(true))))
                            .createStatement(DataAccessServices.dao().getComponentSession()).list();
                    if (slots.size() != marks.size())
                        continue;


                    for (SessionSlotRegularMark mark : marks) {
                        //Если находим оценку отличную от запрашиваемой - пропускаем студента
                        if (mark.getCachedMarkValue().getCode().equals(SessionMarkGradeValueCatalogItemCodes.HOROSHO))
                            is4 = true;
                        if (mark.getCachedMarkValue().getCode().equals(SessionMarkGradeValueCatalogItemCodes.OTLICHNO))
                            is5 = true;
                        if (mark.getCachedMarkValue().getCode().equals(SessionMarkGradeValueCatalogItemCodes.OTLICHNO))
                            isZacet = true;
                    }
                    if (is4 && is5 && isZacet)
                        resultStudents.add(studentId);
                }
            }

        });
        return resultStudents;
    }

    //Поиск среди сдавших сессию на конкретную отметку либо зачет
    public List<Long> considerSessionResults(final String code) {

        //Для сокращения поиска находим студентов у которых вообще есть такая оценка
        DQLSelectBuilder markBuilder = new DQLSelectBuilder();

        markBuilder.fromEntity(SessionMark.class, "m");
        markBuilder.where(DQLExpressions.and(
                DQLExpressions.eq(DQLExpressions.property(SessionMark.cachedMarkValue().code().fromAlias("m")),
                                  DQLExpressions.value(code)),
                DQLExpressions.isNull(SessionMark.slot().studentWpeCAction().removalDate().fromAlias("m"))));

        markBuilder.addColumn(DQLExpressions.property(SessionMark.slot().studentWpeCAction().studentWpe().student().id().fromAlias("m")));
        markBuilder.group(DQLExpressions.property(SessionMark.slot().studentWpeCAction().studentWpe().student().id().fromAlias("m")));

        List<Long> allStudents = markBuilder.createStatement(getSession()).list();

        final List<Long> resultStudents = new ArrayList<Long>();


        BatchUtils.execute(allStudents, 256, new BatchUtils.Action<Long>() {
            @Override
            public void execute(Collection<Long> studs) {

                for (Long studentId : studs) {
                    boolean addStudent = true;

                    List<EppStudentWpeCAction> slots = DataAccessServices.dao().getList(EppStudentWpeCAction.class, EppStudentWpeCAction.studentWpe().student().id(), studentId);

                    List<SessionSlotRegularMark> marks = new DQLSelectBuilder().fromEntity(SessionSlotRegularMark.class, "s")
                            .where(DQLExpressions.and(
                                    DQLExpressions.in(DQLExpressions.property(SessionSlotRegularMark.slot().studentWpeCAction().fromAlias("s")), slots),
                                    DQLExpressions.eq(DQLExpressions.property(SessionSlotRegularMark.slot().studentWpeCAction().studentWpe().student().fromAlias("s")), DQLExpressions.value(studentId)),
                                    DQLExpressions.eq(DQLExpressions.property(SessionSlotRegularMark.slot().inSession().fromAlias("s")), DQLExpressions.value(true))))
                            .createStatement(DataAccessServices.dao().getComponentSession()).list();
                    if (slots.size() != marks.size())
                        continue;

                    for (SessionSlotRegularMark mark : marks) {
                        //Если находим оценку отличную от запрашиваемой - пропускаем студента
                        if (!mark.getCachedMarkValue().getCode().equals(code) && !mark.getCachedMarkValue().getCode().equals(SessionMarkGradeValueCatalogItemCodes.ZACHTENO)) {
                            addStudent = false;
                            break;
                        }
                    }
                    if (addStudent)
                        resultStudents.add(studentId);
                }
            }
        });
        return resultStudents;
    }


    private Set<StructureEducationLevels> getByRoot(StructureEducationLevels root) {
        if (root == null)
            return null;

        Set<StructureEducationLevels> resultSet = new HashSet<>();
        resultSet.add(root);

        int counter = 0;
        while (counter < resultSet.size()) {
            counter = resultSet.size();

            List<StructureEducationLevels> list = IUniBaseDao.instance.get().getList(StructureEducationLevels.class, StructureEducationLevels.parent(), resultSet);
            resultSet.addAll(list);
        }

        return resultSet;
    }

}
