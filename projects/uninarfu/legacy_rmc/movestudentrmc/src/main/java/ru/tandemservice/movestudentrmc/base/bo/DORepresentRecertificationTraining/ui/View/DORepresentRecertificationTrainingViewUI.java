package ru.tandemservice.movestudentrmc.base.bo.DORepresentRecertificationTraining.ui.View;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentViewUI;

public class DORepresentRecertificationTrainingViewUI extends AbstractDORepresentViewUI {

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        super.onBeforeDataSourceFetch(dataSource);
        dataSource.put(AbstractDORepresentViewUI.REPRESENTATION_ID, this.getRepresentId());
    }

}
