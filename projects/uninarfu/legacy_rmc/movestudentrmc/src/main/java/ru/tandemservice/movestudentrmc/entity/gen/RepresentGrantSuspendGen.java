package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudentrmc.entity.IStudentGrant;
import ru.tandemservice.movestudentrmc.entity.RepresentGrantSuspend;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * О приостановлении стипендии/выплаты
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RepresentGrantSuspendGen extends Representation
 implements IStudentGrant{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.RepresentGrantSuspend";
    public static final String ENTITY_NAME = "representGrantSuspend";
    public static final int VERSION_HASH = -325114552;
    private static IEntityMeta ENTITY_META;

    public static final String P_BEGIN_GRANT_DATE = "beginGrantDate";
    public static final String P_END_GRANT_DATE = "endGrantDate";
    public static final String P_DATE_SUSPEND_PAYMENT = "dateSuspendPayment";
    public static final String L_GRANT_VIEW = "grantView";

    private Date _beginGrantDate;     // Дата приостановления стипендии/выплаты
    private Date _endGrantDate;     // Дата окончания выплат
    private Date _dateSuspendPayment;     // Дата приостановления стипендии/выплаты
    private GrantView _grantView;     // Вид стипендии/выплаты

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата приостановления стипендии/выплаты. Свойство не может быть null.
     *
     * Это формула "dateSuspendPayment".
     */
    // @NotNull
    public Date getBeginGrantDate()
    {
        return _beginGrantDate;
    }

    /**
     * @param beginGrantDate Дата приостановления стипендии/выплаты. Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setBeginGrantDate(Date beginGrantDate)
    {
        dirty(_beginGrantDate, beginGrantDate);
        _beginGrantDate = beginGrantDate;
    }

    /**
     * @return Дата окончания выплат.
     *
     * Это формула "null".
     */
    public Date getEndGrantDate()
    {
        return _endGrantDate;
    }

    /**
     * @param endGrantDate Дата окончания выплат.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setEndGrantDate(Date endGrantDate)
    {
        dirty(_endGrantDate, endGrantDate);
        _endGrantDate = endGrantDate;
    }

    /**
     * @return Дата приостановления стипендии/выплаты. Свойство не может быть null.
     */
    @NotNull
    public Date getDateSuspendPayment()
    {
        return _dateSuspendPayment;
    }

    /**
     * @param dateSuspendPayment Дата приостановления стипендии/выплаты. Свойство не может быть null.
     */
    public void setDateSuspendPayment(Date dateSuspendPayment)
    {
        dirty(_dateSuspendPayment, dateSuspendPayment);
        _dateSuspendPayment = dateSuspendPayment;
    }

    /**
     * @return Вид стипендии/выплаты. Свойство не может быть null.
     */
    @NotNull
    public GrantView getGrantView()
    {
        return _grantView;
    }

    /**
     * @param grantView Вид стипендии/выплаты. Свойство не может быть null.
     */
    public void setGrantView(GrantView grantView)
    {
        dirty(_grantView, grantView);
        _grantView = grantView;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof RepresentGrantSuspendGen)
        {
            setBeginGrantDate(((RepresentGrantSuspend)another).getBeginGrantDate());
            setEndGrantDate(((RepresentGrantSuspend)another).getEndGrantDate());
            setDateSuspendPayment(((RepresentGrantSuspend)another).getDateSuspendPayment());
            setGrantView(((RepresentGrantSuspend)another).getGrantView());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RepresentGrantSuspendGen> extends Representation.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RepresentGrantSuspend.class;
        }

        public T newInstance()
        {
            return (T) new RepresentGrantSuspend();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "beginGrantDate":
                    return obj.getBeginGrantDate();
                case "endGrantDate":
                    return obj.getEndGrantDate();
                case "dateSuspendPayment":
                    return obj.getDateSuspendPayment();
                case "grantView":
                    return obj.getGrantView();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "beginGrantDate":
                    obj.setBeginGrantDate((Date) value);
                    return;
                case "endGrantDate":
                    obj.setEndGrantDate((Date) value);
                    return;
                case "dateSuspendPayment":
                    obj.setDateSuspendPayment((Date) value);
                    return;
                case "grantView":
                    obj.setGrantView((GrantView) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "beginGrantDate":
                        return true;
                case "endGrantDate":
                        return true;
                case "dateSuspendPayment":
                        return true;
                case "grantView":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "beginGrantDate":
                    return true;
                case "endGrantDate":
                    return true;
                case "dateSuspendPayment":
                    return true;
                case "grantView":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "beginGrantDate":
                    return Date.class;
                case "endGrantDate":
                    return Date.class;
                case "dateSuspendPayment":
                    return Date.class;
                case "grantView":
                    return GrantView.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RepresentGrantSuspend> _dslPath = new Path<RepresentGrantSuspend>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RepresentGrantSuspend");
    }
            

    /**
     * @return Дата приостановления стипендии/выплаты. Свойство не может быть null.
     *
     * Это формула "dateSuspendPayment".
     * @see ru.tandemservice.movestudentrmc.entity.RepresentGrantSuspend#getBeginGrantDate()
     */
    public static PropertyPath<Date> beginGrantDate()
    {
        return _dslPath.beginGrantDate();
    }

    /**
     * @return Дата окончания выплат.
     *
     * Это формула "null".
     * @see ru.tandemservice.movestudentrmc.entity.RepresentGrantSuspend#getEndGrantDate()
     */
    public static PropertyPath<Date> endGrantDate()
    {
        return _dslPath.endGrantDate();
    }

    /**
     * @return Дата приостановления стипендии/выплаты. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentGrantSuspend#getDateSuspendPayment()
     */
    public static PropertyPath<Date> dateSuspendPayment()
    {
        return _dslPath.dateSuspendPayment();
    }

    /**
     * @return Вид стипендии/выплаты. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentGrantSuspend#getGrantView()
     */
    public static GrantView.Path<GrantView> grantView()
    {
        return _dslPath.grantView();
    }

    public static class Path<E extends RepresentGrantSuspend> extends Representation.Path<E>
    {
        private PropertyPath<Date> _beginGrantDate;
        private PropertyPath<Date> _endGrantDate;
        private PropertyPath<Date> _dateSuspendPayment;
        private GrantView.Path<GrantView> _grantView;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата приостановления стипендии/выплаты. Свойство не может быть null.
     *
     * Это формула "dateSuspendPayment".
     * @see ru.tandemservice.movestudentrmc.entity.RepresentGrantSuspend#getBeginGrantDate()
     */
        public PropertyPath<Date> beginGrantDate()
        {
            if(_beginGrantDate == null )
                _beginGrantDate = new PropertyPath<Date>(RepresentGrantSuspendGen.P_BEGIN_GRANT_DATE, this);
            return _beginGrantDate;
        }

    /**
     * @return Дата окончания выплат.
     *
     * Это формула "null".
     * @see ru.tandemservice.movestudentrmc.entity.RepresentGrantSuspend#getEndGrantDate()
     */
        public PropertyPath<Date> endGrantDate()
        {
            if(_endGrantDate == null )
                _endGrantDate = new PropertyPath<Date>(RepresentGrantSuspendGen.P_END_GRANT_DATE, this);
            return _endGrantDate;
        }

    /**
     * @return Дата приостановления стипендии/выплаты. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentGrantSuspend#getDateSuspendPayment()
     */
        public PropertyPath<Date> dateSuspendPayment()
        {
            if(_dateSuspendPayment == null )
                _dateSuspendPayment = new PropertyPath<Date>(RepresentGrantSuspendGen.P_DATE_SUSPEND_PAYMENT, this);
            return _dateSuspendPayment;
        }

    /**
     * @return Вид стипендии/выплаты. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentGrantSuspend#getGrantView()
     */
        public GrantView.Path<GrantView> grantView()
        {
            if(_grantView == null )
                _grantView = new GrantView.Path<GrantView>(L_GRANT_VIEW, this);
            return _grantView;
        }

        public Class getEntityClass()
        {
            return RepresentGrantSuspend.class;
        }

        public String getEntityName()
        {
            return "representGrantSuspend";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
