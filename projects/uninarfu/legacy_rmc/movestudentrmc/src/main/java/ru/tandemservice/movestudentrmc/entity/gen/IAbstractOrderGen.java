package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudentrmc.entity.IAbstractOrder;
import ru.tandemservice.movestudentrmc.entity.catalog.MovestudentOrderStates;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import ru.tandemservice.movestudentrmc.entity.IAbstractOrder;

@SuppressWarnings({"all", "unchecked", "unused"})
public abstract class IAbstractOrderGen extends InterfaceStubBase
 implements IAbstractOrder{
    public static final int VERSION_HASH = 2008326219;

    public static final String P_ORDER_I_D = "orderID";
    public static final String P_COMMIT_DATE = "commitDate";
    public static final String P_NUMBER = "number";
    public static final String P_CREATE_DATE = "createDate";
    public static final String L_STATE = "state";

    private Long _orderID;
    private Date _commitDate;
    private String _number;
    private Date _createDate;
    private MovestudentOrderStates _state;


    public Long getOrderID()
    {
        return _orderID;
    }

    public void setOrderID(Long orderID)
    {
        _orderID = orderID;
    }


    public Date getCommitDate()
    {
        return _commitDate;
    }

    public void setCommitDate(Date commitDate)
    {
        _commitDate = commitDate;
    }

    @Length(max=255)

    public String getNumber()
    {
        return _number;
    }

    public void setNumber(String number)
    {
        _number = number;
    }


    public Date getCreateDate()
    {
        return _createDate;
    }

    public void setCreateDate(Date createDate)
    {
        _createDate = createDate;
    }


    public MovestudentOrderStates getState()
    {
        return _state;
    }

    public void setState(MovestudentOrderStates state)
    {
        _state = state;
    }

    private static final Path<IAbstractOrder> _dslPath = new Path<IAbstractOrder>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ru.tandemservice.movestudentrmc.entity.IAbstractOrder");
    }
            

    /**
     * @return ID приказа.
     * @see ru.tandemservice.movestudentrmc.entity.IAbstractOrder#getOrderID()
     */
    public static PropertyPath<Long> orderID()
    {
        return _dslPath.orderID();
    }

    /**
     * @return Дата приказа.
     * @see ru.tandemservice.movestudentrmc.entity.IAbstractOrder#getCommitDate()
     */
    public static PropertyPath<Date> commitDate()
    {
        return _dslPath.commitDate();
    }

    /**
     * @return Номер приказа.
     * @see ru.tandemservice.movestudentrmc.entity.IAbstractOrder#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Дата формирования.
     * @see ru.tandemservice.movestudentrmc.entity.IAbstractOrder#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    /**
     * @return Состояние приказа..
     * @see ru.tandemservice.movestudentrmc.entity.IAbstractOrder#getState()
     */
    public static MovestudentOrderStates.Path<MovestudentOrderStates> state()
    {
        return _dslPath.state();
    }

    public static class Path<E extends IAbstractOrder> extends EntityPath<E>
    {
        private PropertyPath<Long> _orderID;
        private PropertyPath<Date> _commitDate;
        private PropertyPath<String> _number;
        private PropertyPath<Date> _createDate;
        private MovestudentOrderStates.Path<MovestudentOrderStates> _state;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ID приказа.
     * @see ru.tandemservice.movestudentrmc.entity.IAbstractOrder#getOrderID()
     */
        public PropertyPath<Long> orderID()
        {
            if(_orderID == null )
                _orderID = new PropertyPath<Long>(IAbstractOrderGen.P_ORDER_I_D, this);
            return _orderID;
        }

    /**
     * @return Дата приказа.
     * @see ru.tandemservice.movestudentrmc.entity.IAbstractOrder#getCommitDate()
     */
        public PropertyPath<Date> commitDate()
        {
            if(_commitDate == null )
                _commitDate = new PropertyPath<Date>(IAbstractOrderGen.P_COMMIT_DATE, this);
            return _commitDate;
        }

    /**
     * @return Номер приказа.
     * @see ru.tandemservice.movestudentrmc.entity.IAbstractOrder#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(IAbstractOrderGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Дата формирования.
     * @see ru.tandemservice.movestudentrmc.entity.IAbstractOrder#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(IAbstractOrderGen.P_CREATE_DATE, this);
            return _createDate;
        }

    /**
     * @return Состояние приказа..
     * @see ru.tandemservice.movestudentrmc.entity.IAbstractOrder#getState()
     */
        public MovestudentOrderStates.Path<MovestudentOrderStates> state()
        {
            if(_state == null )
                _state = new MovestudentOrderStates.Path<MovestudentOrderStates>(L_STATE, this);
            return _state;
        }

        public Class getEntityClass()
        {
            return IAbstractOrder.class;
        }

        public String getEntityName()
        {
            return "ru.tandemservice.movestudentrmc.entity.IAbstractOrder";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
