package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.OrgUnitTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.List.DORepresentBaseListUI;
import ru.tandemservice.uni.dao.IOrgstructDAO;

import java.util.HashMap;
import java.util.Map;

@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = DORepresentBaseOrgUnitTabUI.ORG_UNIT_ID)
})
public class DORepresentBaseOrgUnitTabUI extends UIPresenter {

    public static final String ORG_UNIT_ID = "orgUnitId";

    private String _selectedTab;
    private Map<String, Object> _paramsRepresentBaseTab;
    private OrgUnit _orgUnit;
    private Long _orgUnitId;
    private CommonPostfixPermissionModel _secModel;

    @Override
    public void onComponentRefresh() {

        _orgUnit = DataAccessServices.dao().get(_orgUnitId);
        _secModel = new OrgUnitSecModel(_orgUnit);
        _paramsRepresentBaseTab = new HashMap<String, Object>();
        _paramsRepresentBaseTab.put(DORepresentBaseListUI.ORG_UNIT_ID, _orgUnitId);

        getData().put("viewRepresent", _secModel.getPermission("viewTabOrgUnitDocOrdRepresent"));
        getData().put("viewRepresentCommit", _secModel.getPermission("viewTabOrgUnitDocOrdRepresentCommit"));
    }

//Getters

    public Map<String, Object> getParamsRepresentBaseTab() {
        return _paramsRepresentBaseTab;
    }

    public boolean isOrgUnitDocOrdTabVisible(OrgUnit orgUnit)
    {
        return IOrgstructDAO.instance.get().isOrgUnitFormingOrTerritorial(orgUnit);
    }

//Getters and Setters

    public String getSelectedTab() {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab) {
        _selectedTab = selectedTab;
    }

    public Long getOrgUnitId() {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId) {
        _orgUnitId = orgUnitId;
    }
}
