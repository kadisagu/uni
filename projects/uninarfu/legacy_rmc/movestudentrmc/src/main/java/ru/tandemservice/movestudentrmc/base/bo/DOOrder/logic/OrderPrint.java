package ru.tandemservice.movestudentrmc.base.bo.DOOrder.logic;

import org.hibernate.Session;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.RtfParagraph;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.DORepresentBaseManager;
import ru.tandemservice.movestudentrmc.docord.IDOObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class OrderPrint {

    private static String ALIAS_BUILDER = "s";

    private static DocumentOrder _order;
    private static boolean _saveExtract;
    private static Session _session;
    private static int _itemFuc;

    public static Map<Long, OrderParagraphInfo> studentParagInfoMap = new HashMap<Long, OrderParagraphInfo>();

    public static RtfDocument createPrintDocOrder(DocumentOrder order, ErrorCollector error, boolean saveExtract) {
        studentParagInfoMap.clear();
        DQLSelectBuilder builder;                        //Конструктор для запросов
        RtfParagraph paragraph = new RtfParagraph(); //Параграф для представлений

        //Инициализация переменных класса
        _order = order;
        _saveExtract = saveExtract;
        _session = DataAccessServices.dao().getComponentSession();
        _itemFuc = 1;

        Map<OrgUnit, Map<RepresentationType, List<Representation>>> mapOrgUnitToMapRepresentTypes = new HashMap<OrgUnit, Map<RepresentationType, List<Representation>>>();
        Map<RepresentationType, List<Representation>> mapRepresentTypes = new HashMap<RepresentationType, List<Representation>>();

        //Запрос для получения количества представлений приказа
        Integer countRepresent = DataAccessServices.dao().getCount(getBaseBuilder());

        //Проверка приказ на содержание в нем представлений
        if (countRepresent.equals(0)) {
            error.add("Список представлений для вывода на печать пустой.");
            return null;
        }

        builder = getBaseBuilder();

        List<Object[]> listResult = builder.createStatement(_session).list();

        Map<OrgUnit, List<DocRepresentStudentBase>> keyMap = new HashMap<OrgUnit, List<DocRepresentStudentBase>>();
        List<DocRepresentStudentBase> travelRepresentStudents = new ArrayList<>();

        for (Object[] data : listResult) {
            DocOrdRepresent relOrderToRepresent = (DocOrdRepresent) data[0];
            DocRepresentStudentBase relRepresentToStudent = (DocRepresentStudentBase) data[1];

            Representation represent = relOrderToRepresent.getRepresentation();
            RepresentationType representType = represent.getType();
            Student student = relRepresentToStudent.getStudent();

            OrgUnit orgUnit = student.getEducationOrgUnit().getFormativeOrgUnit();
            /*
            if (representType.getCode().equals(RepresentationTypeCodes.TRAVEL_INDIVIDUAL))
                travelRepresentStudents.add(relRepresentToStudent);
			*/
            //MultiKey key = new MultiKey(new Object[]{orgUnit});

            if (keyMap.containsKey(orgUnit)) {
                ((List) keyMap.get(orgUnit)).add(relRepresentToStudent);
            }
            else {
                List<DocRepresentStudentBase> arr = new ArrayList<DocRepresentStudentBase>();
                arr.add(relRepresentToStudent);
                keyMap.put(orgUnit, arr);
            }
        }

        List<OrgUnit> orgUnitList = new ArrayList(keyMap.keySet());

        for (OrgUnit orgUnit : orgUnitList) {
            if (orgUnit.getRank() == null) {
                ContextLocal.getErrorCollector().add("Есть формирующие подразделения без номера.");
                return null;
            }
        }

        Collections.sort(orgUnitList, new Comparator<OrgUnit>() {
            public int compare(OrgUnit t1, OrgUnit t2)
            {
                Integer i1 = t1.getRank();
                Integer i2 = t2.getRank();
                return i1.compareTo(i2);
            }
        });

        if (travelRepresentStudents.size() > 0) {
            Map<String, List<DocRepresentStudentBase>> travelMap = checkStudentsForTravel(travelRepresentStudents);
            int counter = 0;
            for (Map.Entry<String, List<DocRepresentStudentBase>> entry : travelMap.entrySet()) {
                IRtfText representText = RtfBean.getElementFactory().createRtfText(
                        "\\fs28\\f0"
                                + " " + ++counter + ". "
                                + "");
                representText.setRaw(true);
                paragraph.addElement(representText);
                /*if (entry.getValue().size() == 1)
                    //printRepresent(entry.getValue().get(0).getRepresentation(), paragraph, null, null);
                else {*/
                printRepresent(entry.getValue().get(0).getRepresentation(), paragraph, true, false);
                for (int i = 1; i < entry.getValue().size(); i++) {
                    if (i == entry.getValue().size() - 1)
                        printRepresent(entry.getValue().get(i).getRepresentation(), paragraph, false, true);
                    else
                        printRepresent(entry.getValue().get(i).getRepresentation(), paragraph, false, false);

                }
//                }
            }
        }
        else {
            int acNumb = 1;
            int orgUnits = 0;
            int j = 1;
            OrgUnit lastOrgUnit = null;

            for (OrgUnit orgUnit : orgUnitList) {
                List<DocRepresentStudentBase> relRepresentToStudent = keyMap.get(orgUnit);

                OrgUnit currentUnit = orgUnit;

                if (!currentUnit.equals(lastOrgUnit)) {
                    //Пишем факультет
                    orgUnits++;
                    IRtfText instituteText = RtfBean.getElementFactory().createRtfText(
                            "\\qj\\fi709\\faauto\\adjustright\\fs28\\f0"
                                    + " " + orgUnits + ". "
                                    + UtilPrintSupport.getFacultyWithCodeStr(currentUnit)
                                    + "\\par ");
                    instituteText.setRaw(true);
                    paragraph.addElement(instituteText);
                    j = 1;
                }

                for (DocRepresentStudentBase representToStudent : relRepresentToStudent) {

                    studentParagInfoMap.put(representToStudent.getId(), new OrderParagraphInfo(orgUnits, j));
                /*
                IRtfText representText = RtfBean.getElementFactory().createRtfText(
                        "\\fs28\\f0"
                                + " " + orgUnits + "." + j++ + ". "
                                + "");
                representText.setRaw(true);
                paragraph.addElement(representText);
				*/
                    AtomicInteger i = new AtomicInteger(orgUnits);
                    AtomicInteger k = new AtomicInteger(j);

                    printRepresent(representToStudent.getRepresentation(), paragraph, i, k);

                    j = k.get();
                    orgUnits = i.get();
                }

                lastOrgUnit = orgUnit;
            }
        }

        final RtfDocument docMain = UtilPrintSupport.getTemplate("orderCommonHeader", "common");


        RtfInjectModifier rtf = new RtfInjectModifier()
                .put("date_or", UtilPrintSupport.getDateFormatterWithMonthString(order.getCommitDate()))
                .put("num_or", order.getNumber())
                .put("type", isSingleType() ? ((DocOrdRepresent) listResult.get(0)[0]).getRepresentation().getType().getTitle() : "По личному составу обучающихся");
        rtf.modify(docMain);

        UtilPrintSupport.injectVisaTable(docMain, order);

        UtilPrintSupport.injectBranchLabels(docMain, orgUnitList);

        return mergeDocs(docMain, paragraph);

    }

    private static void printRepresent(Representation represent, RtfParagraph paragraph, AtomicInteger parN, AtomicInteger extrN) {

        //Получаем менеджер представления
        IDOObjectByRepresentTypeManager representManager = DORepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(represent.getType().getCode());
        //Получаем шаблон представления
        RtfDocument docCommon = UtilPrintSupport.getTemplate(represent.getType(), "represent");


        SharedRtfUtil.removeParagraphsWithTagsRecursive(docCommon, Arrays.asList("formativeOrgUnit"), false, false);

        //Заполняем тело представления
        representManager.getDOObjectModifyDAO().buildBodyRepresent(represent, docCommon, parN, extrN);

        //Добовляем к основному телу, тело приказа.
        paragraph.getElementList().addAll(docCommon.getElementList());
    }

    private static void printRepresent(Representation represent, RtfParagraph paragraph, Boolean printHeader, Boolean printBasics) {

        //Получаем менеджер представления
        IDOObjectByRepresentTypeManager representManager = DORepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(represent.getType().getCode());
        //Получаем шаблон представления
        RtfDocument docCommon = UtilPrintSupport.getTemplate(represent.getType(), "represent");


        SharedRtfUtil.removeParagraphsWithTagsRecursive(docCommon, Arrays.asList("formativeOrgUnit"), false, false);
        if (!printHeader)
            SharedRtfUtil.removeParagraphsWithTagsRecursive(docCommon, Arrays.asList("settlement"), false, false);
        if (!printBasics)
            SharedRtfUtil.removeParagraphsWithTagsRecursive(docCommon, Arrays.asList("basics"), false, false);

        //Заполняем тело представления
        representManager.getDOObjectModifyDAO().buildBodyRepresent(represent, docCommon);

        //Добовляем к основному телу, тело приказа.
        paragraph.getElementList().addAll(docCommon.getElementList());
    }


    private static DQLSelectBuilder getBaseBuilder() {

        return new DQLSelectBuilder()
                .fromEntity(DocOrdRepresent.class, "r")
                .joinEntity("r", DQLJoinType.inner, DocRepresentStudentBase.class, ALIAS_BUILDER,
                            DQLExpressions.eq(DQLExpressions.property(DocOrdRepresent.representation().fromAlias("r")),
                                              DQLExpressions.property(DocRepresentStudentBase.representation().fromAlias(ALIAS_BUILDER))))
                .where(DQLExpressions.eq(
                        DQLExpressions.property(DocOrdRepresent.order().fromAlias("r")),
                        DQLExpressions.value(_order)))
                .order(DQLExpressions.property(DocOrdRepresent.representation().type().priority().fromAlias("r")), OrderDirection.asc)
                .order(DQLExpressions.property(DocOrdRepresent.representation().startDate().fromAlias("r")), OrderDirection.asc)
                .order(DQLExpressions.property(DocOrdRepresent.representation().reason().priority().fromAlias("r")), OrderDirection.asc)
                .order(DQLExpressions.property(DocRepresentStudentBase.groupStr().fromAlias(ALIAS_BUILDER)), OrderDirection.asc)
                .order(DQLExpressions.property(DocRepresentStudentBase.studentTitleStr().fromAlias(ALIAS_BUILDER)), OrderDirection.asc)
                ;
    }

    private static RtfDocument mergeDocs(RtfDocument docMain, RtfParagraph rtfParagraph) {

        RtfSearchResult searchResult = UniRtfUtil.findRtfMark(docMain, "body");
        if (searchResult.isFound()) {
            searchResult.getElementList().set(searchResult.getIndex(), rtfParagraph);
        }

        return docMain;
    }

    private static boolean isSingleType() {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(DocOrdRepresent.class, "r")
                .where(DQLExpressions.eq(
                        DQLExpressions.property(DocOrdRepresent.order().fromAlias("r")),
                        DQLExpressions.value(_order)));
        builder.column(DQLExpressions.property(DocOrdRepresent.representation().type().id().fromAlias("r")));
        builder.group(DQLExpressions.property(DocOrdRepresent.representation().type().id().fromAlias("r")));

        List<Long> list = builder.createStatement(_session).list();
        if (list.size() > 1)
            return false;
        else
            return true;
    }

    private static Map<String, List<DocRepresentStudentBase>> checkStudentsForTravel(List<DocRepresentStudentBase> list)
    {
        Map<String, List<DocRepresentStudentBase>> result = new HashMap<>();
        for (DocRepresentStudentBase item : list) {
            String key = getKey(item);
            if (!result.containsKey(key))
                result.put(key, new ArrayList<DocRepresentStudentBase>());
            result.get(key).add(item);
        }
        return result;
    }

    private static String getKey(DocRepresentStudentBase rel)
    {
        RepresentTravel represent = (RepresentTravel) rel.getRepresentation();
        String result = "";
        result += represent.getTarget() == null ? "" : represent.getTarget();
        result += represent.getEndDate() == null ? "" : RussianDateFormatUtils.getDateFormattedWithMonthName(represent.getEndDate());
        result += represent.getBeginDate() == null ? "" : RussianDateFormatUtils.getDateFormattedWithMonthName(represent.getBeginDate());
        result += represent.getOrganization() == null ? "" : represent.getOrganization();
        result += represent.getAddressCountry() == null ? "" : represent.getAddressCountry().getTitle();
        result += represent.getAddressItem() == null ? "" : represent.getAddressItem().getTitle();
        result += represent.getDays() == null ? "" : represent.getDays().toString();
        result += represent.getTravelPaymentData() == null ? "" : represent.getTravelPaymentData().getTitle();
        return result;
    }
}