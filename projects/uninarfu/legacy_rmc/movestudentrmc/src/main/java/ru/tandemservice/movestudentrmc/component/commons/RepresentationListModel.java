package ru.tandemservice.movestudentrmc.component.commons;

import org.tandemframework.common.util.PostfixPermissionModel;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.*;

import java.util.List;

public class RepresentationListModel {

    public static final String EDUCATION_YEAR = "educationYear";
    public static final String FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String TERRITORIAL_ORG_UNIT = "territorialOrgUnit";
    public static final String PRODUCING_ORG_UNIT = "producingOrgUnit";
    public static final String EDUCATION_LEVEL_HIGH_SCHOOL = "educationLevelHighSchool";
    public static final String COURSE = "course";
    public static final String DEVELOP_FORM = "developForm";
    public static final String COMPENSATION_TYPE = "compensationType";
    public static final String EXTRACT_TYPE = "extractType";
    public static final String EXTRACT_STATE = "extractState";
    public static final String EXTRACT_REASON = "extractReason";
    private DynamicListDataSource _dataSource;
    private IDataSettings _settings;
    private PostfixPermissionModel _secModel;
    private ISelectModel _educationYearList;
    private ISelectModel _formativeOrgUnitModel;
    private ISelectModel _territorialOrgUnitModel;
    private ISelectModel _producingOrgUnitModel;
    private ISelectModel _educationLevelHighSchool;
    private IMultiSelectModel _extractReasonModel;
    private List _courseList;
    private List _developFormList;
    private List _compensationTypeList;
    private IMultiSelectModel _extractTypesListModel;
    private List _extractStateList;


    public RepresentationListModel() {
    }

    public OrgUnit getFormativeOrgUnit() {
        return (OrgUnit) _settings.get("formativeOrgUnit");
    }

    public OrgUnit getTerritorialOrgUnit() {
        return (OrgUnit) _settings.get("territorialOrgUnit");
    }

    public EducationLevelsHighSchool getEducationLevelsHighSchool() {
        return (EducationLevelsHighSchool) _settings.get("educationLevelHighSchool");
    }

    public DevelopForm getDevelopForm() {
        throw new UnsupportedOperationException();
    }

    public DevelopCondition getDevelopCondition() {
        throw new UnsupportedOperationException();
    }

    public DevelopTech getDevelopTech() {
        throw new UnsupportedOperationException();
    }

    public DevelopPeriod getDevelopPeriod() {
        throw new UnsupportedOperationException();
    }

    public DynamicListDataSource getDataSource() {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource dataSource) {
        _dataSource = dataSource;
    }

    public IDataSettings getSettings() {
        return _settings;
    }

    public void setSettings(IDataSettings settings) {
        _settings = settings;
    }

    public PostfixPermissionModel getSecModel() {
        return _secModel;
    }

    public void setSecModel(PostfixPermissionModel secModel) {
        _secModel = secModel;
    }

    public ISelectModel getEducationYearList() {
        return _educationYearList;
    }

    public void setEducationYearList(ISelectModel educationYearList) {
        _educationYearList = educationYearList;
    }

    public ISelectModel getFormativeOrgUnitModel() {
        return _formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel) {
        _formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public ISelectModel getTerritorialOrgUnitModel() {
        return _territorialOrgUnitModel;
    }

    public void setTerritorialOrgUnitModel(ISelectModel territorialOrgUnitModel) {
        _territorialOrgUnitModel = territorialOrgUnitModel;
    }

    public ISelectModel getProducingOrgUnitModel() {
        return _producingOrgUnitModel;
    }

    public void setProducingOrgUnitModel(ISelectModel producingOrgUnitModel) {
        _producingOrgUnitModel = producingOrgUnitModel;
    }

    public ISelectModel getEducationLevelHighSchool() {
        return _educationLevelHighSchool;
    }

    public void setEducationLevelHighSchool(ISelectModel educationLevelHighSchool) {
        _educationLevelHighSchool = educationLevelHighSchool;
    }

    public IMultiSelectModel getExtractReasonModel() {
        return _extractReasonModel;
    }

    public void setExtractReasonModel(IMultiSelectModel extractReasonModel) {
        _extractReasonModel = extractReasonModel;
    }

    public List getCourseList() {
        return _courseList;
    }

    public void setCourseList(List courseList) {
        _courseList = courseList;
    }

    public List getDevelopFormList() {
        return _developFormList;
    }

    public void setDevelopFormList(List developFormList) {
        _developFormList = developFormList;
    }

    public List getCompensationTypeList() {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List compensationTypeList) {
        _compensationTypeList = compensationTypeList;
    }

    public IMultiSelectModel getExtractTypesListModel() {
        return _extractTypesListModel;
    }

    public void setExtractTypesListModel(IMultiSelectModel extractTypesListModel) {
        _extractTypesListModel = extractTypesListModel;
    }

    public List getExtractStateList() {
        return _extractStateList;
    }

    public void setExtractStateList(List extractStateList) {
        _extractStateList = extractStateList;
    }
}
