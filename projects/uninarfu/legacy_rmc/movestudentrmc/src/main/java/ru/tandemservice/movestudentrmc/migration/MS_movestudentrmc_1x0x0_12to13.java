package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.BooleanDBColumn;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;


public class MS_movestudentrmc_1x0x0_12to13 extends IndependentMigrationScript {
    @Override
    public void run(DBTool tool) throws Exception {

        IEntityMeta meta = EntityRuntime.getMeta(RepresentationReason.class);

        if (!tool.tableExists(meta.getTableName()) || tool.table(meta.getTableName()).columnExists("used_p"))
            return;

        DBColumn column = new BooleanDBColumn("used_p");
        column.setNullable(true);
        tool.table(meta.getTableName()).createColumn(column);
        tool.executeUpdate("update " + meta.getTableName() + " set used_p = ?", new Object[]{Boolean.TRUE});
        tool.table(meta.getTableName()).column("used_p").changeNullable(false);
    }
}