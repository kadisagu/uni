package ru.tandemservice.movestudentrmc.component.catalog.printTemplate.PrintTemplatePub;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultPrintCatalogPub.DefaultPrintCatalogPubDAO;
import ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate;
import ru.tandemservice.uni.UniUtils;

/**
 * @author AutoGenerator
 *         Created on 07.12.2011
 */
public class DAO extends DefaultPrintCatalogPubDAO<PrintTemplate, Model> implements IDAO {

    @Override
    protected void prepareListItemDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(PrintTemplate.ENTITY_CLASS, "ci");

        applyFilters(model, builder);

        /**
         * Выводим все имеющиеся шаблоны (как представлений так и сборных приказов; см. movementstudent.data.xml)
         * на UI справочника "Печатные шаблоны" RM#2136.
         * Раньше выводились только те шаблоны представлений, типы представлений которых помечены
         * в справочнике "Типы представлений" как "использовать" со значением TRUE. 
         */
        //builder.add(MQExpression.eq("ci", PrintTemplate.type().use(), Boolean.TRUE));
        getOrderDescriptionRegistry().applyOrder(builder, model.getDataSource().getEntityOrder());

        UniUtils.createPage(model.getDataSource(), builder, getSession());
    }
}