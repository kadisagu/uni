package ru.tandemservice.movestudentrmc.base.bo.RelationSettings.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationBasement;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class BasicDSHandler extends DefaultComboDataSourceHandler {

    public BasicDSHandler(String ownerId) {

        super(ownerId, RepresentationBasement.class, RepresentationBasement.P_TITLE);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        String filter = ep.input.getComboFilterByValue();
        if (StringUtils.isNotBlank(filter)) {
            ep.dqlBuilder.where(like(DQLFunctions.upper(DQLExpressions.property(RepresentationBasement.title().fromAlias("e"))), value(CoreStringUtils.escapeLike(filter, true))));
        }
    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
    {
        ep.dqlBuilder.order("e." + RepresentationBasement.title());
    }
}
