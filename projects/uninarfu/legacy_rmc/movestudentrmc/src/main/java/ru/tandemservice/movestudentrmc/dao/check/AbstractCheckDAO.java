package ru.tandemservice.movestudentrmc.dao.check;

import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractCheckDAO extends UniBaseDao implements ICheckDAO {

    protected IEntity represent;

    @Override
    public List<String> check(IEntity represent, List<Student> list, boolean value) {
        this.represent = represent;
        return doCheck(list, value);
    }

    @Override
    public List<String> doCheck(List<Student> list, boolean value) {
        List<String> errorList = new ArrayList<String>();

        for (Student student : list) {
            if (getCondition(student) != value) {
                errorList.add(getErrorMessage(student, value));
            }
        }

        return errorList;
    }

    protected abstract boolean getCondition(Student student);

    protected abstract String getErrorMessage(Student student, boolean value);

}
