package ru.tandemservice.movestudentrmc.entity;

import ru.tandemservice.movestudentrmc.entity.gen.PersonNARFUGen;

/**
 * Доп атрибуты студента
 */
public class PersonNARFU extends PersonNARFUGen
{
    public String getAddressTempRegistrationWithAdditionalInfo()
    {
        return this.getAddressTempRegistration() == null ? "" : this.getAddressTempRegistration().getTitleWithFlat() + (this.getAddressTempRegistrationInfo() == null ? "" : " (" + this.getAddressTempRegistrationInfo() + ")");
    }

    public String getAddressRegistrationWithAdditionalInfo()
    {
        return this.getPerson().getAddressRegistration() == null ? "" : this.getPerson().getAddressRegistration().getTitleWithFlat() + (this.getAddressRegistrationInfo() == null ? "" : " (" + this.getAddressRegistrationInfo() + ")");
    }

    public String getAddressWithAdditionalInfo()
    {
        return this.getPerson().getAddress() == null ? "" : this.getPerson().getAddress().getTitleWithFlat() + (this.getAddressInfo() == null ? "" : " (" + this.getAddressInfo() + ")");
    }
}