package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_movestudentrmc_1x0x0_68to69 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies() {
        return new ScriptDependency[] {
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.2"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.9.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность listRepresentAdmissionToPractice

		// создано свойство supervisor
		{
			// создать колонку
			tool.createColumn("listrepradmissionpractice_t", new DBColumn("supervisor_id", DBType.LONG));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность listRepresentPractice

		// создано свойство supervisor
		{
			// создать колонку
			tool.createColumn("listrepresentpractice_t", new DBColumn("supervisor_id", DBType.LONG));

		}


    }
}