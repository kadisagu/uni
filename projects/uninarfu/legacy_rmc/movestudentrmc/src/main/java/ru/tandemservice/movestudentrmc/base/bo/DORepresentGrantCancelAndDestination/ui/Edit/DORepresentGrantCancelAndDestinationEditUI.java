package ru.tandemservice.movestudentrmc.base.bo.DORepresentGrantCancelAndDestination.ui.Edit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentEditUI;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentGrantCancelAndDestination.DORepresentGrantCancelAndDestinationManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentGrantCancelAndDestination.logic.IDORepresentGrantCancelAndDestinationDAO;
import ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab.MonthWrapper;
import ru.tandemservice.movestudentrmc.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.Grant;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentationTypeCodes;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.StuGrantStatusCodes;
import ru.tandemservice.movestudentrmc.entity.gen.IRepresentOrderGen;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.util.ReportRenderer;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import static org.tandemframework.hibsupport.dql.DQLExpressions.and;
import static org.tandemframework.hibsupport.dql.DQLExpressions.between;
import static org.tandemframework.hibsupport.dql.DQLExpressions.gt;
import static org.tandemframework.hibsupport.dql.DQLExpressions.lt;
import static org.tandemframework.hibsupport.dql.DQLExpressions.or;
import static org.tandemframework.hibsupport.dql.DQLExpressions.plus;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;
import static org.tandemframework.hibsupport.dql.DQLExpressions.valueDate;
import static org.tandemframework.hibsupport.dql.DQLFunctions.createTimestamp;

import java.io.IOException;
import java.util.*;

public class DORepresentGrantCancelAndDestinationEditUI extends AbstractDORepresentEditUI
{

    private DynamicListDataSource<DocRepresentOrderCancel> orderCancelDataSource;
    private List<DocRepresentOrderCancel> ordersCancel = new ArrayList<>();

    @Override
    protected void FetchStudents(Representation doc) {
        _studentDocList.clear();
        _studentList.clear();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocRepresentStudentBase.class, "stud").column("stud");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(DocRepresentStudentBase.representation().fromAlias("stud")),
                DQLExpressions.value(doc.getId())));

        List<DocRepresentStudentBase> studentList = builder.createStatement(getSupport().getSession()).list();
        for (DocRepresentStudentBase student : studentList) {

            _studentDocList.add(student);
            _studentList.add(student.getStudent());
        }

        _student = _studentList.get(0);
    }

    @Override
    protected void initDocument() {
        super.initDocument();

        //-----------------------------------------------------------------------------------------------------------------------------
        /*
        if (getRepresentId() == null)
            getRepresent().setEducationYear(DataAccessServices.dao().get(EducationYear.class, EducationYear.current(), Boolean.TRUE));
        */
        //-----------------------------------------------------------------------------------------------------------------------------

        if (getOrderCancelDataSource() == null) {
            setOrderCancelDataSource(new DynamicListDataSource<>(this, new IListDataSourceDelegate()
            {
                private boolean loaded = false;

                @Override
                public void updateListDataSource(IBusinessComponent component)
                {
                    List<DocRepresentOrderCancel> list;
                    if (loaded)
                    {
                        list = getOrdersCancel();
                    }
                    else
                    {
                    	//--------------------------------------------------------------------------------------------
                        //list = IUniBaseDao.instance.get().getList(DocRepresentOrderCancel.class, DocRepresentOrderCancel.representation().id(), getRepresent().getId());
                    	//setOrdersCancel(list);
                    	
                    	getOrdersCancel().clear();
                        for (DocRepresentOrderCancel orderCancel : IUniBaseDao.instance.get().getList(DocRepresentOrderCancel.class, DocRepresentOrderCancel.representation().id(), getRepresent().getId())) {
                        	
                            DocRepresentOrderCancel rel = new DocRepresentOrderCancel();
                            rel.setRepresentation(orderCancel.getRepresentation());
                            rel.setGrantView(orderCancel.getGrantView());
                            rel.setOrder(orderCancel.getOrder());
                            rel.setTitle(orderCancel.getTitle());

                            getOrdersCancel().add(rel);
                        	
                        }
                        list = getOrdersCancel();
                        //--------------------------------------------------------------------------------------------
                    }

                    getOrderCancelDataSource().setCountRow(list.size());
                    UniBaseUtils.createPage(getOrderCancelDataSource(), list);

                    loaded = true;
                }
            }));

            getOrderCancelDataSource().addColumn(new SimpleColumn("Номер приказа", DocRepresentOrderCancel.order().number()).setWidth("10%").setClickable(false));
            getOrderCancelDataSource().addColumn(new SimpleColumn("Дата приказа", DocRepresentOrderCancel.order().commitDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setWidth("10%").setClickable(false));
            getOrderCancelDataSource().addColumn(new SimpleColumn("Вид стипендии", DocRepresentOrderCancel.grantView().title()).setWidth("80%").setClickable(false));
        }

    }

    public RepresentGrantCancelAndDestination getRepresent() {
        return (RepresentGrantCancelAndDestination) this.getRepresentObj();
    }

    public void onChangeDate() {
        if (getRepresent().getBeginDate() == null || getRepresent().getEndDate() == null)
            return;

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentGrantEntityHistory.class, "h")
                .where(DQLExpressions.in(DQLExpressions.property(StudentGrantEntityHistory.studentGrantEntity().fromAlias("h")), getStudentGrants()))
                .joinEntity("h", DQLJoinType.inner, IRepresentOrder.class, "rel",
                            DQLExpressions.eq(DQLExpressions.property(StudentGrantEntityHistory.representation().id().fromAlias("h")),
                                              DQLExpressions.property(IRepresentOrderGen.representation().id().fromAlias("rel"))))
                .column(DQLExpressions.property(StudentGrantEntityHistory.studentGrantEntity().id().fromAlias("h")))
                .column(DQLExpressions.property(StudentGrantEntityHistory.studentGrantEntity().view().id().fromAlias("h")))
                .column("rel")
                .order(DQLExpressions.property(StudentGrantEntityHistory.studentGrantEntity().id().fromAlias("h")))
                .order(DQLExpressions.property(StudentGrantEntityHistory.createDate().fromAlias("h")));

        List<Object[]> list = IUniBaseDao.instance.get().getList(builder);

        getOrdersCancel().clear();

        Map<IAbstractOrder, Long> grantViewMap = new HashMap<>();
        Map<Long, IAbstractOrder> orderMap = new HashMap<>();
        for (Object[] obj : list) {
            Long sgeId = (Long) obj[0];
            Long grantViewId = (Long) obj[1];
            IRepresentOrder order = (IRepresentOrder) obj[2];
            orderMap.put(sgeId, order.getOrder());
            grantViewMap.put(order.getOrder(), grantViewId);
        }

        Set<IAbstractOrder> set = new HashSet<>();

        for (Long key : orderMap.keySet()) {
            IAbstractOrder order = orderMap.get(key);
            if (set.contains(order))
                continue;
            Long grantViewId = grantViewMap.get(order);
            GrantView grantView = IUniBaseDao.instance.get().get(grantViewId);
            DocRepresentOrderCancel rel = new DocRepresentOrderCancel();
            rel.setRepresentation(getRepresent());
            rel.setGrantView(grantView);
            rel.setOrder(order);
            rel.setTitle(getTitleOrder(order, grantView));

            getOrdersCancel().add(rel);

            set.add(order);
        }
    }

    private String getTitleOrder(IAbstractOrder order, GrantView grantView) {
        if (order == null)
            return null;

        if (order instanceof ListOrder) {

            ListRepresent listRepresent = ((ListOrder) order).getRepresentationList().get(0);

            if (listRepresent.getRepresentationType().getCode().equals(RepresentationTypeCodes.APPOINT_GRANT)) {
                StringBuilder sb = new StringBuilder();

                List<RelTypeGrantView> relTypeGrantViews = DataAccessServices.dao().getList(RelTypeGrantView.class, RelTypeGrantView.view(),
                                                                                            grantView);

                if (relTypeGrantViews != null) {
                    Grant grant = relTypeGrantViews.get(0).getGrant();
                    sb.append("О назначении ")
                            .append(grant.getGenitive() == null ? grant.getTitle() : grant.getGenitive());
                }
                else
                    sb.append(listRepresent.getRepresentationType().getTitle());

                return sb.toString();
            }
            else
                return listRepresent.getRepresentationType().getTitle();
        }
        else {
            DocumentOrder dor = (DocumentOrder) order;
            return UtilPrintSupport.isSingleType(dor) ? dor.getRepresentationTypes() : "По личному составу обучающихся";
        }
    }

    public List<StudentGrantEntity> getStudentGrants() {

    	//-----------------------------------------------------------------------------------------------------------------------------------------------
        //List<String> period = MonthWrapper.getMonthsList(getRepresent().getBeginDate(), getRepresent().getEndDate(), getRepresent().getEducationYear());
        List<String> period = MonthWrapper.getMonthsList(getRepresent().getBeginDate(), getRepresent().getEndDate());	//Снято ограничение учебным годом
    	//-----------------------------------------------------------------------------------------------------------------------------------------------

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentGrantEntity.class, "sge")
                .joinEntity("sge", DQLJoinType.inner, RelTypeGrantView.class, "rel",
                            DQLExpressions.eq(
                                    DQLExpressions.property(StudentGrantEntity.view().id().fromAlias("sge")),
                                    DQLExpressions.property(RelTypeGrantView.view().id().fromAlias("rel"))
                            )
                )
                .where(DQLExpressions.or(
                        DQLExpressions.isNull(DQLExpressions.property(StudentGrantEntity.tmp().fromAlias("sge"))),
                        DQLExpressions.eqValue(DQLExpressions.property(StudentGrantEntity.tmp().fromAlias("sge")), Boolean.FALSE)
                ))
                .where(DQLExpressions.eqValue(DQLExpressions.property(StudentGrantEntity.student().id().fromAlias("sge")), this.getStudentList().get(0).getId()))
                .where(DQLExpressions.eqValue(DQLExpressions.property(StudentGrantEntity.status().code().fromAlias("sge")), StuGrantStatusCodes.CODE_1))//статус выплата
                .where(DQLExpressions.in(DQLExpressions.property(StudentGrantEntity.month().fromAlias("sge")), period))
                .where(DQLExpressions.eqValue(DQLExpressions.property(RelTypeGrantView.type().academ().fromAlias("rel")), Boolean.TRUE))
                .column("sge");

        List<StudentGrantEntity> sgeList = IUniBaseDao.instance.get().getList(builder);

        return sgeList;
    }

    @Override
    public void onClickSave() throws IOException {

        if (getOrdersCancel().isEmpty())
            throw new ApplicationException("Невозможно сохранить представление, т.к. для данного студента в указанный период не назначена ни одна академическая стипендия");

        if (CommonBaseDateUtil.isAfter(getRepresent().getBeginDate(), getRepresent().getEndDate())) {
            throw new ApplicationException("Дата начала выплаты не может быть позже даты окончания выплаты");
        }
        
        //---------------------------------------------------------------------------------------------------------------------
		DQLSelectBuilder eduYearBuilder = new DQLSelectBuilder()
                .fromEntity(EducationYear.class, "year")
                .where(or(between(valueDate(getRepresent().getBeginDate())
                			, createTimestamp(property(EducationYear.intValue().fromAlias("year")), value(9), value(1), value(0), value(0), value(0))
                			, createTimestamp(plus(property(EducationYear.intValue().fromAlias("year")), value(1)), value(8), value(31), value(23), value(59), value(59)))
                		, between(valueDate(getRepresent().getEndDate())
                    			, createTimestamp(property(EducationYear.intValue().fromAlias("year")), value(9), value(1), value(0), value(0), value(0))
                    			, createTimestamp(plus(property(EducationYear.intValue().fromAlias("year")), value(1)), value(8), value(31), value(23), value(59), value(59)))

                		, and(lt(valueDate(getRepresent().getBeginDate()), createTimestamp(property(EducationYear.intValue().fromAlias("year")), value(9), value(1), value(0), value(0), value(0)))
                			, gt(valueDate(getRepresent().getEndDate()), createTimestamp(plus(property(EducationYear.intValue().fromAlias("year")), value(1)), value(8), value(31), value(23), value(59), value(59)))) 
                		))
                ;
		
		List<EducationYear> eduYearList = IUniBaseDao.instance.get().getList(eduYearBuilder);
		
		List<String> errorList = new ArrayList<>();
		
		for(EducationYear eduYear : eduYearList) {  
			
			Date beginDate;
			Date endDate;
			
	        Calendar beginYear = Calendar.getInstance();
	        beginYear.set(eduYear.getIntValue(), Calendar.SEPTEMBER, 1, 0, 0, 0);
	        beginYear.set(Calendar.MILLISECOND, 0);
	        if(getRepresent().getBeginDate().compareTo(beginYear.getTime()) > 0) {
	        	beginDate = getRepresent().getBeginDate();
	        }
	        else {
	        	beginDate = beginYear.getTime();
	        }
	        
	        Calendar endYear = Calendar.getInstance();
	        endYear.set(eduYear.getIntValue() + 1, Calendar.AUGUST, 31, 23, 59, 59);
	        endYear.set(Calendar.MILLISECOND, 999);	
	        if(getRepresent().getEndDate().compareTo(endYear.getTime()) > 0) {
	        	endDate = endYear.getTime();
	        }
	        else {
	        	endDate = getRepresent().getEndDate();
	        }	
			
			List<String> periodList = MonthWrapper.getMonthsList(beginDate, endDate, eduYear);
			
			MoveStudentDaoFacade.getGrantDAO().checkStudentGrants(getRepresent().getId(), getRepresent().getGrantView(), this.getStudentList(), periodList, errorList);
		
		}
		
        if (!errorList.isEmpty()) {
            RtfDocument errorReport = RtfBean.getElementFactory().createRtfDocument();
            IRtfControl par = RtfBean.getElementFactory().createRtfControl(IRtfData.PAR);
            for (String text : errorList) {
                IRtfText studText = RtfBean.getElementFactory().createRtfText(text);
                errorReport.addElement(studText);
                errorReport.getElementList().addAll(Arrays.asList(par, par));
            }
            BusinessComponentUtils.downloadDocument(new ReportRenderer("Ошибки.rtf", errorReport), true);

            throw new ApplicationException("Не удалось сохранить представление");
        }
        
        // Вносим дату начала действия приказа для избежания nullPointException при печати приказа.
        _representObj.setStartDate(new Date());

        super.onClickSave();
        
        IDORepresentGrantCancelAndDestinationDAO dao = (IDORepresentGrantCancelAndDestinationDAO) DORepresentGrantCancelAndDestinationManager.instance().getDOObjectModifyDAO();

        dao.deleteStudentGrantEntity(getRepresent().getId());
        dao.saveStudentGrantEntity(getStudentGrants(), getRepresent());
        dao.deleteOrderCancel(getRepresent().getId());
        dao.saveOrderCancel(getOrdersCancel());

        MoveStudentDaoFacade.getGrantDAO().deleteStudentGrants(getRepresent());
        
        for(EducationYear eduYear : eduYearList) {
        	
			Date beginDate;
			Date endDate;
			
	        Calendar beginYear = Calendar.getInstance();
	        beginYear.set(eduYear.getIntValue(), Calendar.SEPTEMBER, 1, 0, 0, 0);
	        beginYear.set(Calendar.MILLISECOND, 0);
	        if(getRepresent().getBeginDate().compareTo(beginYear.getTime()) > 0) {
	        	beginDate = getRepresent().getBeginDate();
	        }
	        else {
	        	beginDate = beginYear.getTime();
	        }
	        
	        Calendar endYear = Calendar.getInstance();
	        endYear.set(eduYear.getIntValue() + 1, Calendar.AUGUST, 31, 23, 59, 59);
	        endYear.set(Calendar.MILLISECOND, 999);	
	        if(getRepresent().getEndDate().compareTo(endYear.getTime()) > 0) {
	        	endDate = endYear.getTime();
	        }
	        else {
	        	endDate = getRepresent().getEndDate();
	        }	        
	        
	        List<String> periodList = MonthWrapper.getMonthsList(beginDate, endDate, eduYear);
	        
	        MoveStudentDaoFacade.getGrantDAO().saveStudentGrants(getRepresent(), getRepresent().getGrantView(), _student, periodList, eduYear, false);
        	
        }

        /*
        //период попадает в учебный год
        EducationYear eduYear = getRepresent().getEducationYear();

        Calendar beginYear = Calendar.getInstance();
        beginYear.set(eduYear.getIntValue(), 8, 1, 0, 0, 0);
        beginYear.set(14, 0);
        Calendar endYear = Calendar.getInstance();
        endYear.set(eduYear.getIntValue() + 1, 7, 31, 23, 59, 59);
        endYear.set(14, 999);

        if (!CommonBaseDateUtil.isBetween(getRepresent().getBeginDate(), beginYear.getTime(), endYear.getTime())
                || !CommonBaseDateUtil.isBetween(getRepresent().getEndDate(), beginYear.getTime(), endYear.getTime()))
            throw new ApplicationException("Даты назначения стипендии/выплаты выходят за границы учебного года " + eduYear.getTitle());

        List<String> periodList = MonthWrapper.getMonthsList(getRepresent().getBeginDate(), getRepresent().getEndDate(), getRepresent().getEducationYear());
        List<String> errorList = new ArrayList<>();

        boolean good = MoveStudentDaoFacade.getGrantDAO().checkStudentGrants(getRepresent().getId(), getRepresent().getGrantView(), this.getStudentList(), periodList, errorList);

        if (!good) {
            RtfDocument errorReport = RtfBean.getElementFactory().createRtfDocument();
            IRtfControl par = RtfBean.getElementFactory().createRtfControl(IRtfData.PAR);
            for (String text : errorList) {
                IRtfText studText = RtfBean.getElementFactory().createRtfText(text);
                errorReport.addElement(studText);
                errorReport.getElementList().addAll(Arrays.asList(par, par));
            }
            BusinessComponentUtils.downloadDocument(new ReportRenderer("Ошибки.rtf", errorReport), true);

            throw new ApplicationException("Не удалось сохранить представление");
        }

        List<String> months = new ArrayList<>();

        for (String month : periodList) {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(GrantEntity.class, "ge")
                    .where(DQLExpressions.eqValue(DQLExpressions.property(GrantEntity.orgUnit().fromAlias("ge")), _student.getEducationOrgUnit().getFormativeOrgUnit()))
                    .where(DQLExpressions.eqValue(DQLExpressions.property(GrantEntity.month().fromAlias("ge")), month))
                    .where(DQLExpressions.eqValue(DQLExpressions.property(GrantEntity.eduYear().fromAlias("ge")), getRepresent().getEducationYear()))
                    .where(DQLExpressions.eqValue(DQLExpressions.property(GrantEntity.view().fromAlias("ge")), getRepresent().getGrantView()));
            List<GrantEntity> lst = UniDaoFacade.getCoreDao().getList(builder);

            GrantEntity ge = lst.get(0);

            if (ge.getSum() == 0) {
                months.add(getMonthName(month));
            }

        }
        if (!months.isEmpty())
            ContextLocal.getInfoCollector().add("У выбранного вида стипендии сумма за " + StringUtils.join(months, ", ") + " равна 0.");

        // Вносим дату начала действия приказа для избежания nullPointException при печати приказа.
        _representObj.setStartDate(new Date());

        super.onClickSave();

        IDORepresentGrantCancelAndDestinationDAO dao = (IDORepresentGrantCancelAndDestinationDAO) DORepresentGrantCancelAndDestinationManager.instance().getDOObjectModifyDAO();

        dao.deleteStudentGrantEntity(getRepresent().getId());
        dao.saveStudentGrantEntity(getStudentGrants(), getRepresent());
        dao.deleteOrderCancel(getRepresent().getId());
        dao.saveOrderCancel(getOrdersCancel());

        MoveStudentDaoFacade.getGrantDAO().saveStudentGrants(getRepresent(), getRepresent().getGrantView(), _student, periodList, getRepresent().getEducationYear());
        */
        //---------------------------------------------------------------------------------------------------------------------

    }

    private String getMonthName(String month) {
        return RussianDateFormatUtils.getMonthName(Integer.parseInt(month.substring(0, month.indexOf("."))), true);
    }

    public DynamicListDataSource<DocRepresentOrderCancel> getOrderCancelDataSource() {
        return orderCancelDataSource;
    }

    public void setOrderCancelDataSource(
            DynamicListDataSource<DocRepresentOrderCancel> orderCancelDataSource)
    {
        this.orderCancelDataSource = orderCancelDataSource;
    }

    public List<DocRepresentOrderCancel> getOrdersCancel() {
        return ordersCancel;
    }

    public void setOrdersCancel(List<DocRepresentOrderCancel> ordersCancel) {
        this.ordersCancel = ordersCancel;
    }

}
