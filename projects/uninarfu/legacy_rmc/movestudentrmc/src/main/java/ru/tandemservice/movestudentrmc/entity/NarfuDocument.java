package ru.tandemservice.movestudentrmc.entity;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.movestudentrmc.entity.gen.NarfuDocumentGen;

/**
 * Сущность документ
 */
public class NarfuDocument extends NarfuDocumentGen
{
    public String getTitle() {
        StringBuilder sb = new StringBuilder()
                .append(this.getSettings().getDocument().getTitle())
                .append(" ");

        if (this.getSettings().getDocument().isShowName() && !StringUtils.isEmpty(this.getName()))
            sb.append(this.getName()).append(" ");
        if (this.getSettings().getDocument().isSeriaExist() && !StringUtils.isEmpty(this.getSeria()))
            sb.append(this.getSeria()).append(" ");
        if (this.getSettings().getDocument().isNumberExist() && !StringUtils.isEmpty(this.getNumber()))
            sb.append("№ ").append(this.getNumber()).append(" ");
        if (this.getSettings().getDocument().isIssuanceDateExist() && this.getIssuanceDate() != null)
            sb.append("от ").append(DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(this.getIssuanceDate())).append(" ");

        return sb.toString();
    }

    public String getGenitiveCaseTitle() {
        StringBuilder sb = new StringBuilder();
        if (this.getSettings().getDocument().getGenericCaseTitle() != null)
            sb.append(this.getSettings().getDocument().getGenericCaseTitle())
                    .append(" ");

        if (this.getSettings().getDocument().isShowName() && !StringUtils.isEmpty(this.getName()))
            sb.append(this.getName()).append(" ");
        if (this.getSettings().getDocument().isSeriaExist() && !StringUtils.isEmpty(this.getSeria()))
            sb.append(this.getSeria()).append(" ");
        if (this.getSettings().getDocument().isNumberExist() && !StringUtils.isEmpty(this.getNumber()))
            sb.append("№ ").append(this.getNumber()).append(" ");
        if (this.getSettings().getDocument().isIssuanceDateExist() && this.getIssuanceDate() != null)
            sb.append("от ").append(DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(this.getIssuanceDate())).append(" ");

        return sb.toString();
    }

    public String getPrintTitle() {
        String result = new String();

        if (this.getSettings().getDocument().isShowName() && !StringUtils.isEmpty(this.getName()))
            result = this.getName();
        else if (this.getSettings().getDocument().isAdditionalNameExist() && !StringUtils.isEmpty(this.getAdditionalName()))
            result = this.getAdditionalName();
        else
            result = "";
        return result;
    }

    public boolean isTemporary() {
        return Boolean.TRUE.equals(this.getTmp());
    }

    public boolean isAttachmentExist() {
        return this.getFile() != null;
    }
}