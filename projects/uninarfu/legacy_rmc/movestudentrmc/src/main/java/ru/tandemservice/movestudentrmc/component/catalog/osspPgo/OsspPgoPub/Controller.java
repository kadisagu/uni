package ru.tandemservice.movestudentrmc.component.catalog.osspPgo.OsspPgoPub;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.movestudentrmc.entity.catalog.OsspPgo;

public class Controller extends DefaultCatalogPubController<OsspPgo, Model, IDAO> {

    @Override
    protected DynamicListDataSource<OsspPgo> createListDataSource(IBusinessComponent context) {
        Model model = getModel(context);
        DynamicListDataSource<OsspPgo> ds = new DynamicListDataSource<OsspPgo>(context, this);
        ds.addColumn(new SimpleColumn("Название", "title").setClickable(false).setOrderable(true), 2);
        //ds.addColumn(new SimpleColumn("Номер", "priority").setClickable(false).setOrderable(true), 3);
        ds.addColumn(new ToggleColumn("Печатать", Model.OsspPgoWrapper.PRINTABLE).setListener("onClickTogglePrintable"), 4);
        ds.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
        if (model.isUserCatalog())
            ds.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", ICatalogItem.CATALOG_ITEM_TITLE).setPermissionKey(model.getCatalogItemDelete()).setDisableHandler(model.getDisabledEntityHandler()));
        //ds.setOrder("priority", OrderDirection.asc);
        return ds;
    }

    public void onClickTogglePrintable(IBusinessComponent component)
    {
        getDao().updatePrintable(getModel(component), (Long) component.getListenerParameter());
    }
}
