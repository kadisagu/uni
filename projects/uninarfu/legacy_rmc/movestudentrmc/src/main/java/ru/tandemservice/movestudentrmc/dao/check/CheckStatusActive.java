package ru.tandemservice.movestudentrmc.dao.check;

import ru.tandemservice.uni.UniDefines;

public class CheckStatusActive extends AbstractCheckStudentStatus {

    protected String getStatusCode() {
        return UniDefines.CATALOG_STUDENT_STATUS_ACTIVE;
    }

}
