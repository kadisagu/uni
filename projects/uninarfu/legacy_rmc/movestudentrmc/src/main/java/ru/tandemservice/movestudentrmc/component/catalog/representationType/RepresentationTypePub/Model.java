package ru.tandemservice.movestudentrmc.component.catalog.representationType.RepresentationTypePub;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubModel;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;

public class Model extends DefaultCatalogPubModel<RepresentationType> {
    public static class RepresentationWrapper extends IdentifiableWrapper<RepresentationType> {
        /**
         *
         */
        private static final long serialVersionUID = -6584988081070948829L;
        public static final String USE = "use";
        public static final String GROUPING = "grouping";
        public static final String INSTRUCTION = "instruction";
        public static final String LIST_REPRESENTATION = "listRepresentation";
        private RepresentationType representation;
        private boolean use;
        private boolean grouping;
        private boolean instruction;
        private boolean listRepresentation;

        public RepresentationWrapper(RepresentationType representation, boolean use, boolean grouping, boolean instruction, boolean listRepresentation)
        {
            super(representation.getId(), representation.getTitle());
            this.representation = representation;
            this.use = use;
            this.grouping = grouping;
            this.instruction = instruction;
            this.listRepresentation = listRepresentation;
        }


        public RepresentationType getRepresentation()
        {
            return representation;
        }

        public boolean isUse()
        {
            return use;
        }

        public void setUse(boolean use)
        {
            this.use = use;
        }


        public boolean isGrouping() {
            return grouping;
        }


        public void setGrouping(boolean grouping) {
            this.grouping = grouping;
        }


        public boolean isInstruction() {
            return instruction;
        }


        public void setInstruction(boolean instruction) {
            this.instruction = instruction;
        }


        public boolean isListRepresentation() {
            return listRepresentation;
        }


        public void setListRepresentation(boolean listRepresentation) {
            this.listRepresentation = listRepresentation;
        }
    }
}
