package ru.tandemservice.movestudentrmc.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.OsspGrantsView;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Вид стипендии ОССП
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OsspGrantsViewGen extends EntityBase
 implements INaturalIdentifiable<OsspGrantsViewGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.catalog.OsspGrantsView";
    public static final String ENTITY_NAME = "osspGrantsView";
    public static final int VERSION_HASH = -1003858695;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_TITLE = "title";
    public static final String P_NUMBER = "number";
    public static final String P_PRINTABLE = "printable";

    private String _code;     // Системный код
    private String _title; 
    private String _number;     // Номер
    private boolean _printable; 

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return  Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title  Свойство не может быть null и должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Номер.
     */
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public boolean isPrintable()
    {
        return _printable;
    }

    /**
     * @param printable  Свойство не может быть null.
     */
    public void setPrintable(boolean printable)
    {
        dirty(_printable, printable);
        _printable = printable;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OsspGrantsViewGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((OsspGrantsView)another).getCode());
            }
            setTitle(((OsspGrantsView)another).getTitle());
            setNumber(((OsspGrantsView)another).getNumber());
            setPrintable(((OsspGrantsView)another).isPrintable());
        }
    }

    public INaturalId<OsspGrantsViewGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<OsspGrantsViewGen>
    {
        private static final String PROXY_NAME = "OsspGrantsViewNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof OsspGrantsViewGen.NaturalId) ) return false;

            OsspGrantsViewGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OsspGrantsViewGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OsspGrantsView.class;
        }

        public T newInstance()
        {
            return (T) new OsspGrantsView();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "title":
                    return obj.getTitle();
                case "number":
                    return obj.getNumber();
                case "printable":
                    return obj.isPrintable();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "printable":
                    obj.setPrintable((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "title":
                        return true;
                case "number":
                        return true;
                case "printable":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "title":
                    return true;
                case "number":
                    return true;
                case "printable":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "title":
                    return String.class;
                case "number":
                    return String.class;
                case "printable":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OsspGrantsView> _dslPath = new Path<OsspGrantsView>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OsspGrantsView");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.OsspGrantsView#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return  Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.OsspGrantsView#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Номер.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.OsspGrantsView#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.OsspGrantsView#isPrintable()
     */
    public static PropertyPath<Boolean> printable()
    {
        return _dslPath.printable();
    }

    public static class Path<E extends OsspGrantsView> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _title;
        private PropertyPath<String> _number;
        private PropertyPath<Boolean> _printable;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.OsspGrantsView#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(OsspGrantsViewGen.P_CODE, this);
            return _code;
        }

    /**
     * @return  Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.OsspGrantsView#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(OsspGrantsViewGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Номер.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.OsspGrantsView#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(OsspGrantsViewGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.OsspGrantsView#isPrintable()
     */
        public PropertyPath<Boolean> printable()
        {
            if(_printable == null )
                _printable = new PropertyPath<Boolean>(OsspGrantsViewGen.P_PRINTABLE, this);
            return _printable;
        }

        public Class getEntityClass()
        {
            return OsspGrantsView.class;
        }

        public String getEntityName()
        {
            return "osspGrantsView";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
