package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonOSSP;
import ru.tandemservice.movestudentrmc.entity.catalog.OsspGrants;
import ru.tandemservice.movestudentrmc.entity.catalog.OsspPgo;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность-связь 'Представление'-'Причина'-'ОССП'
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RelRepresentationReasonOSSPGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonOSSP";
    public static final String ENTITY_NAME = "relRepresentationReasonOSSP";
    public static final int VERSION_HASH = 1709888553;
    private static IEntityMeta ENTITY_META;

    public static final String L_TYPE = "type";
    public static final String L_REASON = "reason";
    public static final String P_BUDGET = "budget";
    public static final String L_GRANTS_O_S_S_P = "grantsOSSP";
    public static final String L_PGO_O_S_S_P = "pgoOSSP";

    private RepresentationType _type;     // Тип Представления
    private RepresentationReason _reason;     // Причина
    private boolean _budget = false;     // Бюджет
    private OsspGrants _grantsOSSP;     // Стипендия ОССП
    private OsspPgo _pgoOSSP;     // ПГО ОССП

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип Представления. Свойство не может быть null.
     */
    @NotNull
    public RepresentationType getType()
    {
        return _type;
    }

    /**
     * @param type Тип Представления. Свойство не может быть null.
     */
    public void setType(RepresentationType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Причина. Свойство не может быть null.
     */
    @NotNull
    public RepresentationReason getReason()
    {
        return _reason;
    }

    /**
     * @param reason Причина. Свойство не может быть null.
     */
    public void setReason(RepresentationReason reason)
    {
        dirty(_reason, reason);
        _reason = reason;
    }

    /**
     * @return Бюджет. Свойство не может быть null.
     */
    @NotNull
    public boolean isBudget()
    {
        return _budget;
    }

    /**
     * @param budget Бюджет. Свойство не может быть null.
     */
    public void setBudget(boolean budget)
    {
        dirty(_budget, budget);
        _budget = budget;
    }

    /**
     * @return Стипендия ОССП.
     */
    public OsspGrants getGrantsOSSP()
    {
        return _grantsOSSP;
    }

    /**
     * @param grantsOSSP Стипендия ОССП.
     */
    public void setGrantsOSSP(OsspGrants grantsOSSP)
    {
        dirty(_grantsOSSP, grantsOSSP);
        _grantsOSSP = grantsOSSP;
    }

    /**
     * @return ПГО ОССП.
     */
    public OsspPgo getPgoOSSP()
    {
        return _pgoOSSP;
    }

    /**
     * @param pgoOSSP ПГО ОССП.
     */
    public void setPgoOSSP(OsspPgo pgoOSSP)
    {
        dirty(_pgoOSSP, pgoOSSP);
        _pgoOSSP = pgoOSSP;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RelRepresentationReasonOSSPGen)
        {
            setType(((RelRepresentationReasonOSSP)another).getType());
            setReason(((RelRepresentationReasonOSSP)another).getReason());
            setBudget(((RelRepresentationReasonOSSP)another).isBudget());
            setGrantsOSSP(((RelRepresentationReasonOSSP)another).getGrantsOSSP());
            setPgoOSSP(((RelRepresentationReasonOSSP)another).getPgoOSSP());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RelRepresentationReasonOSSPGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RelRepresentationReasonOSSP.class;
        }

        public T newInstance()
        {
            return (T) new RelRepresentationReasonOSSP();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "type":
                    return obj.getType();
                case "reason":
                    return obj.getReason();
                case "budget":
                    return obj.isBudget();
                case "grantsOSSP":
                    return obj.getGrantsOSSP();
                case "pgoOSSP":
                    return obj.getPgoOSSP();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "type":
                    obj.setType((RepresentationType) value);
                    return;
                case "reason":
                    obj.setReason((RepresentationReason) value);
                    return;
                case "budget":
                    obj.setBudget((Boolean) value);
                    return;
                case "grantsOSSP":
                    obj.setGrantsOSSP((OsspGrants) value);
                    return;
                case "pgoOSSP":
                    obj.setPgoOSSP((OsspPgo) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "type":
                        return true;
                case "reason":
                        return true;
                case "budget":
                        return true;
                case "grantsOSSP":
                        return true;
                case "pgoOSSP":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "type":
                    return true;
                case "reason":
                    return true;
                case "budget":
                    return true;
                case "grantsOSSP":
                    return true;
                case "pgoOSSP":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "type":
                    return RepresentationType.class;
                case "reason":
                    return RepresentationReason.class;
                case "budget":
                    return Boolean.class;
                case "grantsOSSP":
                    return OsspGrants.class;
                case "pgoOSSP":
                    return OsspPgo.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RelRepresentationReasonOSSP> _dslPath = new Path<RelRepresentationReasonOSSP>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RelRepresentationReasonOSSP");
    }
            

    /**
     * @return Тип Представления. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonOSSP#getType()
     */
    public static RepresentationType.Path<RepresentationType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Причина. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonOSSP#getReason()
     */
    public static RepresentationReason.Path<RepresentationReason> reason()
    {
        return _dslPath.reason();
    }

    /**
     * @return Бюджет. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonOSSP#isBudget()
     */
    public static PropertyPath<Boolean> budget()
    {
        return _dslPath.budget();
    }

    /**
     * @return Стипендия ОССП.
     * @see ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonOSSP#getGrantsOSSP()
     */
    public static OsspGrants.Path<OsspGrants> grantsOSSP()
    {
        return _dslPath.grantsOSSP();
    }

    /**
     * @return ПГО ОССП.
     * @see ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonOSSP#getPgoOSSP()
     */
    public static OsspPgo.Path<OsspPgo> pgoOSSP()
    {
        return _dslPath.pgoOSSP();
    }

    public static class Path<E extends RelRepresentationReasonOSSP> extends EntityPath<E>
    {
        private RepresentationType.Path<RepresentationType> _type;
        private RepresentationReason.Path<RepresentationReason> _reason;
        private PropertyPath<Boolean> _budget;
        private OsspGrants.Path<OsspGrants> _grantsOSSP;
        private OsspPgo.Path<OsspPgo> _pgoOSSP;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип Представления. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonOSSP#getType()
     */
        public RepresentationType.Path<RepresentationType> type()
        {
            if(_type == null )
                _type = new RepresentationType.Path<RepresentationType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Причина. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonOSSP#getReason()
     */
        public RepresentationReason.Path<RepresentationReason> reason()
        {
            if(_reason == null )
                _reason = new RepresentationReason.Path<RepresentationReason>(L_REASON, this);
            return _reason;
        }

    /**
     * @return Бюджет. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonOSSP#isBudget()
     */
        public PropertyPath<Boolean> budget()
        {
            if(_budget == null )
                _budget = new PropertyPath<Boolean>(RelRepresentationReasonOSSPGen.P_BUDGET, this);
            return _budget;
        }

    /**
     * @return Стипендия ОССП.
     * @see ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonOSSP#getGrantsOSSP()
     */
        public OsspGrants.Path<OsspGrants> grantsOSSP()
        {
            if(_grantsOSSP == null )
                _grantsOSSP = new OsspGrants.Path<OsspGrants>(L_GRANTS_O_S_S_P, this);
            return _grantsOSSP;
        }

    /**
     * @return ПГО ОССП.
     * @see ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonOSSP#getPgoOSSP()
     */
        public OsspPgo.Path<OsspPgo> pgoOSSP()
        {
            if(_pgoOSSP == null )
                _pgoOSSP = new OsspPgo.Path<OsspPgo>(L_PGO_O_S_S_P, this);
            return _pgoOSSP;
        }

        public Class getEntityClass()
        {
            return RelRepresentationReasonOSSP.class;
        }

        public String getEntityName()
        {
            return "relRepresentationReasonOSSP";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
