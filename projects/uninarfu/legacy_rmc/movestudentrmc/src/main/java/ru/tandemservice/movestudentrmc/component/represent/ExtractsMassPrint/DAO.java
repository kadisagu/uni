package ru.tandemservice.movestudentrmc.component.represent.ExtractsMassPrint;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.logic.OrderPrint;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.DORepresentBaseManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ListRepresentBaseManager;
import ru.tandemservice.movestudentrmc.docord.IDOObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IListObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(final Model model) {


        if (model.isListOrder()) {
            List<ListOrder> lst = getList(ListOrder.class, model.getOrdersId());
            model.setListLst(lst);

            model.setOrdersModel(new DQLFullCheckSelectModel() {

                @Override
                protected DQLSelectBuilder query(String alias, String filter) {

                    DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(ListOrder.class, alias)
                            .where(DQLExpressions.in(DQLExpressions.property(ListOrder.id().fromAlias(alias)), model.getListLst()));

                    if (!StringUtils.isEmpty(filter))
                        builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(alias, ListOrder.P_NUMBER)), DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));

                    return builder;
                }
            });
        }
        else {
            List<DocumentOrder> lst = getList(DocumentOrder.class, model.getOrdersId());
            model.setIndivLst(lst);

            model.setOrdersModel(new DQLFullCheckSelectModel() {

                @Override
                protected DQLSelectBuilder query(String alias, String filter) {

                    DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocumentOrder.class, alias)
                            .where(DQLExpressions.in(DQLExpressions.property(DocumentOrder.id().fromAlias(alias)), model.getIndivLst()));

                    if (!StringUtils.isEmpty(filter))
                        builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(alias, DocumentOrder.P_NUMBER)), DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));

                    return builder;
                }
            });
        }

    }

    @Override
    public RtfDocument printListExtracts(Model model) {

        RtfDocument result = RtfBean.getElementFactory().createRtfDocument();

        List<Long> ids = model.getOrdersList() != null && !model.getOrdersList().isEmpty() ? UniUtils.getIdList(model.getOrdersList()) : model.getOrdersId();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(ListOrdListRepresent.class, "rel")
                .joinEntity("rel", DQLJoinType.inner, RelListRepresentStudents.class, "stud",
                            DQLExpressions.eq(
                                    DQLExpressions.property(ListOrdListRepresent.representation().id().fromAlias("rel")),
                                    DQLExpressions.property(RelListRepresentStudents.representation().id().fromAlias("stud"))
                            )
                )
                .where(DQLExpressions.in(DQLExpressions.property(ListOrdListRepresent.order().id().fromAlias("rel")), ids))
                .column("rel")
                .order(DQLExpressions.property(ListOrdListRepresent.order().number().fromAlias("rel")))
                .joinPath(DQLJoinType.inner, ListOrdListRepresent.order().fromAlias("rel"), "ord")
                .joinPath(DQLJoinType.inner, ListOrdListRepresent.representation().fromAlias("rel"), "repres")
                .column(DQLExpressions.property(RelListRepresentStudents.student().fromAlias("stud")));

        List<Object[]> lst = getList(builder);

        Map<Long, Map<Student, OrderParagraphInfo>> infoMap = new HashMap<>();
        Map<Long, RtfDocument> templMap = new HashMap<>();

        Map<MultiKey, RtfDocument> textMap = getExtractText(ids);

        int n = 1;
        for (Object[] obj : lst) {
            ListOrdListRepresent rel = (ListOrdListRepresent) obj[0];
            Student student = (Student) obj[1];
            RtfDocument templateExtract;
            MultiKey key = new MultiKey(rel, student);

            if (textMap.containsKey(key)) {
                templateExtract = textMap.get(key);
            }
            else {
                ListRepresent listRepresent = rel.getRepresentation();

                IListObjectByRepresentTypeManager manager = ListRepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(listRepresent.getRepresentationType().getCode());

                Map<Student, OrderParagraphInfo> infoParagMap;
                if (infoMap.get(rel.getOrder().getId()) == null) {
                    infoParagMap = manager.getListObjectModifyDAO().buildBodyRepresent(rel.getOrder().getRepresentationList(), RtfBean.getElementFactory().createRtfDocument());
                    infoMap.put(rel.getOrder().getId(), infoParagMap);
                }
                else
                    infoParagMap = infoMap.get(rel.getOrder().getId());

                //Шаблон выписки
                if (templMap.get(listRepresent.getRepresentationType().getId()) == null) {
                    RtfDocument template = manager.getListObjectModifyDAO().getTemplateExtract(listRepresent.getRepresentationType());
                    templMap.put(listRepresent.getRepresentationType().getId(), template);
                    templateExtract = template.getClone();
                }
                else
                    templateExtract = templMap.get(listRepresent.getRepresentationType().getId()).getClone();

                manager.getListObjectModifyDAO().buildBodyExtract(rel, student, templateExtract, infoParagMap);
            }
            
            //------------------------------------------------------------------------------------------------------------------
            //Исполнитель, печатающий выписку
            UtilPrintSupport.injectExtractsExecutorTable(templateExtract);
            //------------------------------------------------------------------------------------------------------------------

            result.setHeader(templateExtract.getHeader());
            result.setSettings(templateExtract.getSettings());
            result.getElementList().addAll(templateExtract.getElementList());
            if (lst.indexOf(obj) != lst.size() - 1)
                if (n % 2 != 0)
                    result.addElement(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
                else
                    result.addElement(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));

            n++;
        }

        return result;
    }

    private Map<MultiKey, RtfDocument> getExtractText(List<Long> ids) {
        //проверим наличие текста выписки в БД
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(ExtractTextRelation.class, "r")
                .where(DQLExpressions.in(DQLExpressions.property(ExtractTextRelation.order().order().id().fromAlias("r")), ids));

        List<ExtractTextRelation> list = DataAccessServices.dao().getList(builder);
        Map<MultiKey, RtfDocument> map = new HashMap<>();
        for (ExtractTextRelation text : list) {
            byte[] textBytes = text.getText();
            if (textBytes == null)
                throw new ApplicationException("Текст выписки пуст");
            map.put(new MultiKey(text.getOrder(), text.getStudent()), new RtfReader().read(textBytes));
        }

        return map;
    }

    @Override
    public RtfDocument printIndividualExtracts(Model model) {
        RtfDocument result = RtfBean.getElementFactory().createRtfDocument();

        List<Long> ids = model.getOrdersList() != null && !model.getOrdersList().isEmpty() ? UniUtils.getIdList(model.getOrdersList()) : model.getOrdersId();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocOrdRepresent.class, "rel")
                .joinEntity("rel", DQLJoinType.inner, DocRepresentStudentBase.class, "stud",
                            DQLExpressions.eq(
                                    DQLExpressions.property(DocOrdRepresent.representation().id().fromAlias("rel")),
                                    DQLExpressions.property(DocRepresentStudentBase.representation().id().fromAlias("stud"))
                            )
                )
                .where(DQLExpressions.in(DQLExpressions.property(DocOrdRepresent.order().id().fromAlias("rel")), ids))
                .column("rel")
                .order(DQLExpressions.property(DocOrdRepresent.order().number().fromAlias("rel")))
                .joinPath(DQLJoinType.inner, DocOrdRepresent.order().fromAlias("rel"), "ord")
                .joinPath(DQLJoinType.inner, DocOrdRepresent.representation().fromAlias("rel"), "repres")
                .column("stud");

        List<Object[]> lst = getList(builder);

        Map<Long, Map<Long, OrderParagraphInfo>> infoMap = new HashMap<>();
        Map<Long, RtfDocument> templMap = new HashMap<>();
        RtfDocument commonExtractTemplate = UtilPrintSupport.getTemplate("modularExtractCommonTemplate", "common");

        Map<MultiKey, RtfDocument> textMap = getExtractText(ids);

        int n = 1;
        for (Object[] obj : lst) {
            RtfDocument extract = commonExtractTemplate.getClone();
            DocOrdRepresent rel = (DocOrdRepresent) obj[0];
            DocRepresentStudentBase student = (DocRepresentStudentBase) obj[1];
            MultiKey key = new MultiKey(rel, student.getStudent());

            if (textMap.containsKey(key)) {
                extract = textMap.get(key);
            }
            else {
                Representation represent = rel.getRepresentation();

                IDOObjectByRepresentTypeManager representManager = DORepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(represent.getType().getCode());

                OrderParagraphInfo paragraphInfo;
                if (infoMap.get(rel.getOrder().getId()) == null) {
                    OrderPrint.createPrintDocOrder(rel.getOrder(), new ErrorCollector(), false);
                    infoMap.put(rel.getOrder().getId(), OrderPrint.studentParagInfoMap);
                    paragraphInfo = infoMap.get(rel.getOrder().getId()).get(student.getId());
                }
                else
                    paragraphInfo = infoMap.get(rel.getOrder().getId()).get(student.getId());

                //Шаблон выписки
                RtfDocument templateRepresent;
                if (templMap.get(represent.getType().getId()) == null) {
                    RtfDocument template = representManager.getDOObjectModifyDAO().getTemplateRepresent(represent.getType());
                    templMap.put(represent.getType().getId(), template);
                    templateRepresent = template.getClone();
                }
                else
                    templateRepresent = templMap.get(represent.getType().getId()).getClone();

                extract = UtilPrintSupport.extractRendererRtf(represent, rel.getOrder(), extract, templateRepresent, paragraphInfo);
            }

            //------------------------------------------------------------------------------------------------------------------
            //Исполнитель, печатающий выписку
            UtilPrintSupport.injectExtractsExecutorTable(extract);
            //------------------------------------------------------------------------------------------------------------------
            
            result.setHeader(extract.getHeader());
            result.setSettings(extract.getSettings());
            result.getElementList().addAll(extract.getElementList());
            if (n % 2 != 0) {
                result.addElement(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            }
            else {
                result.addElement(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
            }

            n++;
        }

        return result;
    }

}
