package ru.tandemservice.movestudentrmc.base.bo.ListRepresentDiplomAndExclude;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentDiplomAndExclude.logic.ListRepresentDiplomAndExcludeManagerModifyDAO;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentDiplomAndExclude.ui.Edit.ListRepresentDiplomAndExcludeEdit;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentDiplomAndExclude.ui.View.ListRepresentDiplomAndExcludeView;
import ru.tandemservice.movestudentrmc.docord.IListObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IListObjectModifyDAO;


@Configuration
public class ListRepresentDiplomAndExcludeManager extends BusinessObjectManager implements IListObjectByRepresentTypeManager
{

    public static ListRepresentDiplomAndExcludeManager instance()
    {
        return instance(ListRepresentDiplomAndExcludeManager.class);
    }


    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return ListRepresentDiplomAndExcludeEdit.class;
    }


    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return ListRepresentDiplomAndExcludeView.class;
    }


    @Bean
    @Override
    public IListObjectModifyDAO getListObjectModifyDAO() {
        return new ListRepresentDiplomAndExcludeManagerModifyDAO();
    }
}
