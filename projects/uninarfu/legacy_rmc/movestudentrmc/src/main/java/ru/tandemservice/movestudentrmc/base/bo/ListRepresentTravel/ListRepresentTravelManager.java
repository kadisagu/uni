package ru.tandemservice.movestudentrmc.base.bo.ListRepresentTravel;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentTravel.logic.ListRepresentTravelManagerModifyDAO;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentTravel.ui.Edit.ListRepresentTravelEdit;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentTravel.ui.View.ListRepresentTravelView;
import ru.tandemservice.movestudentrmc.docord.IListObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IListObjectModifyDAO;

@Configuration
public class ListRepresentTravelManager extends BusinessObjectManager implements IListObjectByRepresentTypeManager {

    public static ListRepresentTravelManager instance() {
        return instance(ListRepresentTravelManager.class);
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return ListRepresentTravelEdit.class;
    }

    @Override
    @Bean
    public IListObjectModifyDAO getListObjectModifyDAO() {
        return new ListRepresentTravelManagerModifyDAO();
    }

    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return ListRepresentTravelView.class;
    }

}
