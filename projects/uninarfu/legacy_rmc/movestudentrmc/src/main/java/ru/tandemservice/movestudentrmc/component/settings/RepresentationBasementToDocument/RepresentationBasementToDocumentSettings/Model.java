package ru.tandemservice.movestudentrmc.component.settings.RepresentationBasementToDocument.RepresentationBasementToDocumentSettings;

import ru.tandemservice.uni.component.settings.abstractRelation.AbstractRelationList.AbstractRelationListModel;

public class Model extends AbstractRelationListModel {

    private Wrapper wrapper;

    public Wrapper getWrapper() {
        return wrapper;
    }

    public void setWrapper(Wrapper wrapper) {
        this.wrapper = wrapper;
    }
}
