package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.movestudentrmc.entity.*;

import java.util.List;


public interface IDORepresentBaseModifyDAO extends INeedPersistenceSupport {

    void update(Representation document, List<DocRepresentStudentBase> studentList, List<DocRepresentBasics> representBasicsList, List<DocRepresentGrants> representGrantsList, List<DocRepresentStudentDocuments> representStudentDocumentsList, List<DocRepresentStudentIC> representIdentityCardList);

    void updateState(Representation document);

    void delete(Representation document);
}
