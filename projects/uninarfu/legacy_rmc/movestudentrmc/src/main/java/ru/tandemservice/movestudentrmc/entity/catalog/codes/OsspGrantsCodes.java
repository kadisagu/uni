package ru.tandemservice.movestudentrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Стипендии ОССП"
 * Имя сущности : osspGrants
 * Файл data.xml : movementstudent.data.xml
 */
public interface OsspGrantsCodes
{
    /** Константа кода (code) элемента : grant-1-1 (code). Название (title) : без назначения государственной академической стипендии */
    String GRANT_1_1 = "grant-1-1";
    /** Константа кода (code) элемента : grant-1-2 (code). Название (title) : выплачивать пособие в размере */
    String GRANT_1_2 = "grant-1-2";
    /** Константа кода (code) элемента : grant-1-3 (code). Название (title) : выплачивать пособие в размере минимальной стипендии */
    String GRANT_1_3 = "grant-1-3";
    /** Константа кода (code) элемента : grant-1-4 (code). Название (title) : выплачивать пособие в размере минимальной стипендии по счету приносящей доход деятельности */
    String GRANT_1_4 = "grant-1-4";
    /** Константа кода (code) элемента : grant-1-5 (code). Название (title) : с назначением */
    String GRANT_1_5 = "grant-1-5";
    /** Константа кода (code) элемента : grant-1-6 (code). Название (title) : с назначением компенсационной выплаты в размере 50 рублей */
    String GRANT_1_6 = "grant-1-6";
    /** Константа кода (code) элемента : grant-1-7 (code). Название (title) : с сохранением */
    String GRANT_1_7 = "grant-1-7";
    /** Константа кода (code) элемента : grant-1-8 (code). Название (title) : с сохранением компенсационной выплаты в размере 50 рублей */
    String GRANT_1_8 = "grant-1-8";
    /** Константа кода (code) элемента : grant-1-9 (code). Название (title) : с сохранением пособия в размере минимальной стипендии по счету приносящей доход деятельности */
    String GRANT_1_9 = "grant-1-9";
    /** Константа кода (code) элемента : grant-1-10 (code). Название (title) : с сохранением стипендии обучающемуся/йся на основании международного договора */
    String GRANT_1_10 = "grant-1-10";
    /** Константа кода (code) элемента : grant-1-11 (code). Название (title) : с сохранением пособия в размере */
    String GRANT_1_11 = "grant-1-11";
    /** Константа кода (code) элемента : grant-1-12 (code). Название (title) : с сохранением пособия в размере минимальной стипендии */
    String GRANT_1_12 = "grant-1-12";
    /** Константа кода (code) элемента : none (code). Название (title) : - */
    String NONE = "none";

    Set<String> CODES = ImmutableSet.of(GRANT_1_1, GRANT_1_2, GRANT_1_3, GRANT_1_4, GRANT_1_5, GRANT_1_6, GRANT_1_7, GRANT_1_8, GRANT_1_9, GRANT_1_10, GRANT_1_11, GRANT_1_12, NONE);
}
