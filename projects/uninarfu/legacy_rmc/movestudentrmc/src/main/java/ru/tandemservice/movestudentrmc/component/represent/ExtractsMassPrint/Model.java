package ru.tandemservice.movestudentrmc.component.represent.ExtractsMassPrint;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import ru.tandemservice.movestudentrmc.entity.DocumentOrder;
import ru.tandemservice.movestudentrmc.entity.ListOrder;

import java.util.List;

@Input({
        @org.tandemframework.core.component.Bind(key = "ordersId", binding = "ordersId"),
        @org.tandemframework.core.component.Bind(key = "listOrder", binding = "listOrder")
})
public class Model {

    private IMultiSelectModel ordersModel;
    private List<IEntity> ordersList;
    private boolean listOrder;
    private List<Long> ordersId;
    private List<ListOrder> listLst;
    private List<DocumentOrder> indivLst;


    public boolean isListOrder() {
        return listOrder;
    }

    public void setListOrder(boolean listOrder) {
        this.listOrder = listOrder;
    }

    public IMultiSelectModel getOrdersModel() {
        return ordersModel;
    }

    public void setOrdersModel(IMultiSelectModel ordersModel) {
        this.ordersModel = ordersModel;
    }

    public List<IEntity> getOrdersList() {
        return ordersList;
    }

    public void setOrdersList(List<IEntity> ordersList) {
        this.ordersList = ordersList;
    }

    public List<Long> getOrdersId() {
        return ordersId;
    }

    public void setOrdersId(List<Long> ordersId) {
        this.ordersId = ordersId;
    }

    public List<ListOrder> getListLst() {
        return listLst;
    }

    public void setListLst(List<ListOrder> listLst) {
        this.listLst = listLst;
    }

    public List<DocumentOrder> getIndivLst() {
        return indivLst;
    }

    public void setIndivLst(List<DocumentOrder> indivLst) {
        this.indivLst = indivLst;
    }
}
