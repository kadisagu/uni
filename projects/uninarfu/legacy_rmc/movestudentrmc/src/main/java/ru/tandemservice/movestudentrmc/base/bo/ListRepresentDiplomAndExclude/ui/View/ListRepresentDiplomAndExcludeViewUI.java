package ru.tandemservice.movestudentrmc.base.bo.ListRepresentDiplomAndExclude.ui.View;

import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.support.AbstractListRepresentViewUI;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentDiplomAndExclude.logic.ListPrintDoc;
import ru.tandemservice.movestudentrmc.entity.ListRepresentDiplomAndExclude;

import java.util.Collections;

public class ListRepresentDiplomAndExcludeViewUI extends AbstractListRepresentViewUI<ListRepresentDiplomAndExclude> {

    @Override
    public ListRepresentDiplomAndExclude getListRepresentObject() {
        return new ListRepresentDiplomAndExclude();
    }

    @Override
    protected RtfDocument printRepresent() {
        return new ListPrintDoc().creationPrintDocOrder(Collections.singletonList(getListRepresent()));
    }

}
