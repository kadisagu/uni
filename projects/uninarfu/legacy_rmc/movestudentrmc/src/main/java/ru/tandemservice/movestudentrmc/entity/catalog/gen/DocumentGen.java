package ru.tandemservice.movestudentrmc.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.Document;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Документ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DocumentGen extends EntityBase
 implements INaturalIdentifiable<DocumentGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.catalog.Document";
    public static final String ENTITY_NAME = "document";
    public static final int VERSION_HASH = 1872649158;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_SHOW_NAME = "showName";
    public static final String P_ADDITIONAL_NAME_EXIST = "additionalNameExist";
    public static final String P_SERIA_EXIST = "seriaExist";
    public static final String P_SERIA_REQUIRED = "seriaRequired";
    public static final String P_SERIA_MIN = "seriaMin";
    public static final String P_SERIA_MAX = "seriaMax";
    public static final String P_NUMBER_EXIST = "numberExist";
    public static final String P_NUMBER_REQUIRED = "numberRequired";
    public static final String P_NUMBER_MIN = "numberMin";
    public static final String P_NUMBER_MAX = "numberMax";
    public static final String P_ISSUANCE_DATE_EXIST = "issuanceDateExist";
    public static final String P_ISSUANCE_DATE_REQUIRED = "issuanceDateRequired";
    public static final String P_PERIOD_DATE_EXIST = "periodDateExist";
    public static final String P_PERIOD_DATE_REQUIRED = "periodDateRequired";
    public static final String P_GENERIC_CASE_TITLE = "genericCaseTitle";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private boolean _showName;     // Отображать наименование документа
    private boolean _additionalNameExist;     // Дополнительное поле наименования документа
    private boolean _seriaExist;     // Серия документа
    private boolean _seriaRequired;     // Обязательность указания серии документа
    private Integer _seriaMin;     // Минимальная длина серии
    private Integer _seriaMax;     // Максимальная длина серии
    private boolean _numberExist;     // Номер документа
    private boolean _numberRequired;     // Обязательность указания номера документа
    private Integer _numberMin;     // Минимальная длина номера
    private Integer _numberMax;     // Максимальная длина номера
    private boolean _issuanceDateExist;     // Дата выдачи документа
    private boolean _issuanceDateRequired;     // Обязательность указания даты выдачи документа
    private boolean _periodDateExist;     // Дата начала периода действия документа
    private boolean _periodDateRequired;     // Обязательность указания периода действия документа
    private String _genericCaseTitle;     // Название в родительном падеже
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Отображать наименование документа. Свойство не может быть null.
     */
    @NotNull
    public boolean isShowName()
    {
        return _showName;
    }

    /**
     * @param showName Отображать наименование документа. Свойство не может быть null.
     */
    public void setShowName(boolean showName)
    {
        dirty(_showName, showName);
        _showName = showName;
    }

    /**
     * @return Дополнительное поле наименования документа. Свойство не может быть null.
     */
    @NotNull
    public boolean isAdditionalNameExist()
    {
        return _additionalNameExist;
    }

    /**
     * @param additionalNameExist Дополнительное поле наименования документа. Свойство не может быть null.
     */
    public void setAdditionalNameExist(boolean additionalNameExist)
    {
        dirty(_additionalNameExist, additionalNameExist);
        _additionalNameExist = additionalNameExist;
    }

    /**
     * @return Серия документа. Свойство не может быть null.
     */
    @NotNull
    public boolean isSeriaExist()
    {
        return _seriaExist;
    }

    /**
     * @param seriaExist Серия документа. Свойство не может быть null.
     */
    public void setSeriaExist(boolean seriaExist)
    {
        dirty(_seriaExist, seriaExist);
        _seriaExist = seriaExist;
    }

    /**
     * @return Обязательность указания серии документа. Свойство не может быть null.
     */
    @NotNull
    public boolean isSeriaRequired()
    {
        return _seriaRequired;
    }

    /**
     * @param seriaRequired Обязательность указания серии документа. Свойство не может быть null.
     */
    public void setSeriaRequired(boolean seriaRequired)
    {
        dirty(_seriaRequired, seriaRequired);
        _seriaRequired = seriaRequired;
    }

    /**
     * @return Минимальная длина серии.
     */
    public Integer getSeriaMin()
    {
        return _seriaMin;
    }

    /**
     * @param seriaMin Минимальная длина серии.
     */
    public void setSeriaMin(Integer seriaMin)
    {
        dirty(_seriaMin, seriaMin);
        _seriaMin = seriaMin;
    }

    /**
     * @return Максимальная длина серии.
     */
    public Integer getSeriaMax()
    {
        return _seriaMax;
    }

    /**
     * @param seriaMax Максимальная длина серии.
     */
    public void setSeriaMax(Integer seriaMax)
    {
        dirty(_seriaMax, seriaMax);
        _seriaMax = seriaMax;
    }

    /**
     * @return Номер документа. Свойство не может быть null.
     */
    @NotNull
    public boolean isNumberExist()
    {
        return _numberExist;
    }

    /**
     * @param numberExist Номер документа. Свойство не может быть null.
     */
    public void setNumberExist(boolean numberExist)
    {
        dirty(_numberExist, numberExist);
        _numberExist = numberExist;
    }

    /**
     * @return Обязательность указания номера документа. Свойство не может быть null.
     */
    @NotNull
    public boolean isNumberRequired()
    {
        return _numberRequired;
    }

    /**
     * @param numberRequired Обязательность указания номера документа. Свойство не может быть null.
     */
    public void setNumberRequired(boolean numberRequired)
    {
        dirty(_numberRequired, numberRequired);
        _numberRequired = numberRequired;
    }

    /**
     * @return Минимальная длина номера.
     */
    public Integer getNumberMin()
    {
        return _numberMin;
    }

    /**
     * @param numberMin Минимальная длина номера.
     */
    public void setNumberMin(Integer numberMin)
    {
        dirty(_numberMin, numberMin);
        _numberMin = numberMin;
    }

    /**
     * @return Максимальная длина номера.
     */
    public Integer getNumberMax()
    {
        return _numberMax;
    }

    /**
     * @param numberMax Максимальная длина номера.
     */
    public void setNumberMax(Integer numberMax)
    {
        dirty(_numberMax, numberMax);
        _numberMax = numberMax;
    }

    /**
     * @return Дата выдачи документа. Свойство не может быть null.
     */
    @NotNull
    public boolean isIssuanceDateExist()
    {
        return _issuanceDateExist;
    }

    /**
     * @param issuanceDateExist Дата выдачи документа. Свойство не может быть null.
     */
    public void setIssuanceDateExist(boolean issuanceDateExist)
    {
        dirty(_issuanceDateExist, issuanceDateExist);
        _issuanceDateExist = issuanceDateExist;
    }

    /**
     * @return Обязательность указания даты выдачи документа. Свойство не может быть null.
     */
    @NotNull
    public boolean isIssuanceDateRequired()
    {
        return _issuanceDateRequired;
    }

    /**
     * @param issuanceDateRequired Обязательность указания даты выдачи документа. Свойство не может быть null.
     */
    public void setIssuanceDateRequired(boolean issuanceDateRequired)
    {
        dirty(_issuanceDateRequired, issuanceDateRequired);
        _issuanceDateRequired = issuanceDateRequired;
    }

    /**
     * @return Дата начала периода действия документа. Свойство не может быть null.
     */
    @NotNull
    public boolean isPeriodDateExist()
    {
        return _periodDateExist;
    }

    /**
     * @param periodDateExist Дата начала периода действия документа. Свойство не может быть null.
     */
    public void setPeriodDateExist(boolean periodDateExist)
    {
        dirty(_periodDateExist, periodDateExist);
        _periodDateExist = periodDateExist;
    }

    /**
     * @return Обязательность указания периода действия документа. Свойство не может быть null.
     */
    @NotNull
    public boolean isPeriodDateRequired()
    {
        return _periodDateRequired;
    }

    /**
     * @param periodDateRequired Обязательность указания периода действия документа. Свойство не может быть null.
     */
    public void setPeriodDateRequired(boolean periodDateRequired)
    {
        dirty(_periodDateRequired, periodDateRequired);
        _periodDateRequired = periodDateRequired;
    }

    /**
     * @return Название в родительном падеже.
     */
    @Length(max=255)
    public String getGenericCaseTitle()
    {
        return _genericCaseTitle;
    }

    /**
     * @param genericCaseTitle Название в родительном падеже.
     */
    public void setGenericCaseTitle(String genericCaseTitle)
    {
        dirty(_genericCaseTitle, genericCaseTitle);
        _genericCaseTitle = genericCaseTitle;
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DocumentGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((Document)another).getCode());
            }
            setShowName(((Document)another).isShowName());
            setAdditionalNameExist(((Document)another).isAdditionalNameExist());
            setSeriaExist(((Document)another).isSeriaExist());
            setSeriaRequired(((Document)another).isSeriaRequired());
            setSeriaMin(((Document)another).getSeriaMin());
            setSeriaMax(((Document)another).getSeriaMax());
            setNumberExist(((Document)another).isNumberExist());
            setNumberRequired(((Document)another).isNumberRequired());
            setNumberMin(((Document)another).getNumberMin());
            setNumberMax(((Document)another).getNumberMax());
            setIssuanceDateExist(((Document)another).isIssuanceDateExist());
            setIssuanceDateRequired(((Document)another).isIssuanceDateRequired());
            setPeriodDateExist(((Document)another).isPeriodDateExist());
            setPeriodDateRequired(((Document)another).isPeriodDateRequired());
            setGenericCaseTitle(((Document)another).getGenericCaseTitle());
            setTitle(((Document)another).getTitle());
        }
    }

    public INaturalId<DocumentGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<DocumentGen>
    {
        private static final String PROXY_NAME = "DocumentNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof DocumentGen.NaturalId) ) return false;

            DocumentGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DocumentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Document.class;
        }

        public T newInstance()
        {
            return (T) new Document();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "showName":
                    return obj.isShowName();
                case "additionalNameExist":
                    return obj.isAdditionalNameExist();
                case "seriaExist":
                    return obj.isSeriaExist();
                case "seriaRequired":
                    return obj.isSeriaRequired();
                case "seriaMin":
                    return obj.getSeriaMin();
                case "seriaMax":
                    return obj.getSeriaMax();
                case "numberExist":
                    return obj.isNumberExist();
                case "numberRequired":
                    return obj.isNumberRequired();
                case "numberMin":
                    return obj.getNumberMin();
                case "numberMax":
                    return obj.getNumberMax();
                case "issuanceDateExist":
                    return obj.isIssuanceDateExist();
                case "issuanceDateRequired":
                    return obj.isIssuanceDateRequired();
                case "periodDateExist":
                    return obj.isPeriodDateExist();
                case "periodDateRequired":
                    return obj.isPeriodDateRequired();
                case "genericCaseTitle":
                    return obj.getGenericCaseTitle();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "showName":
                    obj.setShowName((Boolean) value);
                    return;
                case "additionalNameExist":
                    obj.setAdditionalNameExist((Boolean) value);
                    return;
                case "seriaExist":
                    obj.setSeriaExist((Boolean) value);
                    return;
                case "seriaRequired":
                    obj.setSeriaRequired((Boolean) value);
                    return;
                case "seriaMin":
                    obj.setSeriaMin((Integer) value);
                    return;
                case "seriaMax":
                    obj.setSeriaMax((Integer) value);
                    return;
                case "numberExist":
                    obj.setNumberExist((Boolean) value);
                    return;
                case "numberRequired":
                    obj.setNumberRequired((Boolean) value);
                    return;
                case "numberMin":
                    obj.setNumberMin((Integer) value);
                    return;
                case "numberMax":
                    obj.setNumberMax((Integer) value);
                    return;
                case "issuanceDateExist":
                    obj.setIssuanceDateExist((Boolean) value);
                    return;
                case "issuanceDateRequired":
                    obj.setIssuanceDateRequired((Boolean) value);
                    return;
                case "periodDateExist":
                    obj.setPeriodDateExist((Boolean) value);
                    return;
                case "periodDateRequired":
                    obj.setPeriodDateRequired((Boolean) value);
                    return;
                case "genericCaseTitle":
                    obj.setGenericCaseTitle((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "showName":
                        return true;
                case "additionalNameExist":
                        return true;
                case "seriaExist":
                        return true;
                case "seriaRequired":
                        return true;
                case "seriaMin":
                        return true;
                case "seriaMax":
                        return true;
                case "numberExist":
                        return true;
                case "numberRequired":
                        return true;
                case "numberMin":
                        return true;
                case "numberMax":
                        return true;
                case "issuanceDateExist":
                        return true;
                case "issuanceDateRequired":
                        return true;
                case "periodDateExist":
                        return true;
                case "periodDateRequired":
                        return true;
                case "genericCaseTitle":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "showName":
                    return true;
                case "additionalNameExist":
                    return true;
                case "seriaExist":
                    return true;
                case "seriaRequired":
                    return true;
                case "seriaMin":
                    return true;
                case "seriaMax":
                    return true;
                case "numberExist":
                    return true;
                case "numberRequired":
                    return true;
                case "numberMin":
                    return true;
                case "numberMax":
                    return true;
                case "issuanceDateExist":
                    return true;
                case "issuanceDateRequired":
                    return true;
                case "periodDateExist":
                    return true;
                case "periodDateRequired":
                    return true;
                case "genericCaseTitle":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "showName":
                    return Boolean.class;
                case "additionalNameExist":
                    return Boolean.class;
                case "seriaExist":
                    return Boolean.class;
                case "seriaRequired":
                    return Boolean.class;
                case "seriaMin":
                    return Integer.class;
                case "seriaMax":
                    return Integer.class;
                case "numberExist":
                    return Boolean.class;
                case "numberRequired":
                    return Boolean.class;
                case "numberMin":
                    return Integer.class;
                case "numberMax":
                    return Integer.class;
                case "issuanceDateExist":
                    return Boolean.class;
                case "issuanceDateRequired":
                    return Boolean.class;
                case "periodDateExist":
                    return Boolean.class;
                case "periodDateRequired":
                    return Boolean.class;
                case "genericCaseTitle":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Document> _dslPath = new Path<Document>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Document");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Отображать наименование документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#isShowName()
     */
    public static PropertyPath<Boolean> showName()
    {
        return _dslPath.showName();
    }

    /**
     * @return Дополнительное поле наименования документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#isAdditionalNameExist()
     */
    public static PropertyPath<Boolean> additionalNameExist()
    {
        return _dslPath.additionalNameExist();
    }

    /**
     * @return Серия документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#isSeriaExist()
     */
    public static PropertyPath<Boolean> seriaExist()
    {
        return _dslPath.seriaExist();
    }

    /**
     * @return Обязательность указания серии документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#isSeriaRequired()
     */
    public static PropertyPath<Boolean> seriaRequired()
    {
        return _dslPath.seriaRequired();
    }

    /**
     * @return Минимальная длина серии.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#getSeriaMin()
     */
    public static PropertyPath<Integer> seriaMin()
    {
        return _dslPath.seriaMin();
    }

    /**
     * @return Максимальная длина серии.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#getSeriaMax()
     */
    public static PropertyPath<Integer> seriaMax()
    {
        return _dslPath.seriaMax();
    }

    /**
     * @return Номер документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#isNumberExist()
     */
    public static PropertyPath<Boolean> numberExist()
    {
        return _dslPath.numberExist();
    }

    /**
     * @return Обязательность указания номера документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#isNumberRequired()
     */
    public static PropertyPath<Boolean> numberRequired()
    {
        return _dslPath.numberRequired();
    }

    /**
     * @return Минимальная длина номера.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#getNumberMin()
     */
    public static PropertyPath<Integer> numberMin()
    {
        return _dslPath.numberMin();
    }

    /**
     * @return Максимальная длина номера.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#getNumberMax()
     */
    public static PropertyPath<Integer> numberMax()
    {
        return _dslPath.numberMax();
    }

    /**
     * @return Дата выдачи документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#isIssuanceDateExist()
     */
    public static PropertyPath<Boolean> issuanceDateExist()
    {
        return _dslPath.issuanceDateExist();
    }

    /**
     * @return Обязательность указания даты выдачи документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#isIssuanceDateRequired()
     */
    public static PropertyPath<Boolean> issuanceDateRequired()
    {
        return _dslPath.issuanceDateRequired();
    }

    /**
     * @return Дата начала периода действия документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#isPeriodDateExist()
     */
    public static PropertyPath<Boolean> periodDateExist()
    {
        return _dslPath.periodDateExist();
    }

    /**
     * @return Обязательность указания периода действия документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#isPeriodDateRequired()
     */
    public static PropertyPath<Boolean> periodDateRequired()
    {
        return _dslPath.periodDateRequired();
    }

    /**
     * @return Название в родительном падеже.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#getGenericCaseTitle()
     */
    public static PropertyPath<String> genericCaseTitle()
    {
        return _dslPath.genericCaseTitle();
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends Document> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<Boolean> _showName;
        private PropertyPath<Boolean> _additionalNameExist;
        private PropertyPath<Boolean> _seriaExist;
        private PropertyPath<Boolean> _seriaRequired;
        private PropertyPath<Integer> _seriaMin;
        private PropertyPath<Integer> _seriaMax;
        private PropertyPath<Boolean> _numberExist;
        private PropertyPath<Boolean> _numberRequired;
        private PropertyPath<Integer> _numberMin;
        private PropertyPath<Integer> _numberMax;
        private PropertyPath<Boolean> _issuanceDateExist;
        private PropertyPath<Boolean> _issuanceDateRequired;
        private PropertyPath<Boolean> _periodDateExist;
        private PropertyPath<Boolean> _periodDateRequired;
        private PropertyPath<String> _genericCaseTitle;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(DocumentGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Отображать наименование документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#isShowName()
     */
        public PropertyPath<Boolean> showName()
        {
            if(_showName == null )
                _showName = new PropertyPath<Boolean>(DocumentGen.P_SHOW_NAME, this);
            return _showName;
        }

    /**
     * @return Дополнительное поле наименования документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#isAdditionalNameExist()
     */
        public PropertyPath<Boolean> additionalNameExist()
        {
            if(_additionalNameExist == null )
                _additionalNameExist = new PropertyPath<Boolean>(DocumentGen.P_ADDITIONAL_NAME_EXIST, this);
            return _additionalNameExist;
        }

    /**
     * @return Серия документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#isSeriaExist()
     */
        public PropertyPath<Boolean> seriaExist()
        {
            if(_seriaExist == null )
                _seriaExist = new PropertyPath<Boolean>(DocumentGen.P_SERIA_EXIST, this);
            return _seriaExist;
        }

    /**
     * @return Обязательность указания серии документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#isSeriaRequired()
     */
        public PropertyPath<Boolean> seriaRequired()
        {
            if(_seriaRequired == null )
                _seriaRequired = new PropertyPath<Boolean>(DocumentGen.P_SERIA_REQUIRED, this);
            return _seriaRequired;
        }

    /**
     * @return Минимальная длина серии.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#getSeriaMin()
     */
        public PropertyPath<Integer> seriaMin()
        {
            if(_seriaMin == null )
                _seriaMin = new PropertyPath<Integer>(DocumentGen.P_SERIA_MIN, this);
            return _seriaMin;
        }

    /**
     * @return Максимальная длина серии.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#getSeriaMax()
     */
        public PropertyPath<Integer> seriaMax()
        {
            if(_seriaMax == null )
                _seriaMax = new PropertyPath<Integer>(DocumentGen.P_SERIA_MAX, this);
            return _seriaMax;
        }

    /**
     * @return Номер документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#isNumberExist()
     */
        public PropertyPath<Boolean> numberExist()
        {
            if(_numberExist == null )
                _numberExist = new PropertyPath<Boolean>(DocumentGen.P_NUMBER_EXIST, this);
            return _numberExist;
        }

    /**
     * @return Обязательность указания номера документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#isNumberRequired()
     */
        public PropertyPath<Boolean> numberRequired()
        {
            if(_numberRequired == null )
                _numberRequired = new PropertyPath<Boolean>(DocumentGen.P_NUMBER_REQUIRED, this);
            return _numberRequired;
        }

    /**
     * @return Минимальная длина номера.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#getNumberMin()
     */
        public PropertyPath<Integer> numberMin()
        {
            if(_numberMin == null )
                _numberMin = new PropertyPath<Integer>(DocumentGen.P_NUMBER_MIN, this);
            return _numberMin;
        }

    /**
     * @return Максимальная длина номера.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#getNumberMax()
     */
        public PropertyPath<Integer> numberMax()
        {
            if(_numberMax == null )
                _numberMax = new PropertyPath<Integer>(DocumentGen.P_NUMBER_MAX, this);
            return _numberMax;
        }

    /**
     * @return Дата выдачи документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#isIssuanceDateExist()
     */
        public PropertyPath<Boolean> issuanceDateExist()
        {
            if(_issuanceDateExist == null )
                _issuanceDateExist = new PropertyPath<Boolean>(DocumentGen.P_ISSUANCE_DATE_EXIST, this);
            return _issuanceDateExist;
        }

    /**
     * @return Обязательность указания даты выдачи документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#isIssuanceDateRequired()
     */
        public PropertyPath<Boolean> issuanceDateRequired()
        {
            if(_issuanceDateRequired == null )
                _issuanceDateRequired = new PropertyPath<Boolean>(DocumentGen.P_ISSUANCE_DATE_REQUIRED, this);
            return _issuanceDateRequired;
        }

    /**
     * @return Дата начала периода действия документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#isPeriodDateExist()
     */
        public PropertyPath<Boolean> periodDateExist()
        {
            if(_periodDateExist == null )
                _periodDateExist = new PropertyPath<Boolean>(DocumentGen.P_PERIOD_DATE_EXIST, this);
            return _periodDateExist;
        }

    /**
     * @return Обязательность указания периода действия документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#isPeriodDateRequired()
     */
        public PropertyPath<Boolean> periodDateRequired()
        {
            if(_periodDateRequired == null )
                _periodDateRequired = new PropertyPath<Boolean>(DocumentGen.P_PERIOD_DATE_REQUIRED, this);
            return _periodDateRequired;
        }

    /**
     * @return Название в родительном падеже.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#getGenericCaseTitle()
     */
        public PropertyPath<String> genericCaseTitle()
        {
            if(_genericCaseTitle == null )
                _genericCaseTitle = new PropertyPath<String>(DocumentGen.P_GENERIC_CASE_TITLE, this);
            return _genericCaseTitle;
        }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.Document#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(DocumentGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return Document.class;
        }

        public String getEntityName()
        {
            return "document";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
