package ru.tandemservice.movestudentrmc.component.settings.RepresentationBasementToDocument.RepresentationBasementToDocumentSettings;

import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.movestudentrmc.entity.RelRepresentationBasementDocument;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.component.settings.abstractRelation.AbstractRelationList.AbstractRelationListDAO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DAO extends AbstractRelationListDAO<Model> implements IDAO {

    @Override
    public void prepareListDataSource(Model model) {

        MQBuilder builder = new MQBuilder(getFirstObjectEntityName(), "o");
        builder.add(MQExpression.eq("o", "connectedWithDocument", true));
        (new OrderDescriptionRegistry("o")).applyOrder(builder, model.getDataSource().getEntityOrder());

        DynamicListDataSource<IEntity> dataSource = model.getDataSource();

        List<IEntity> entities = builder.getResultList(getSession());

        List<Wrapper> wrappers = new ArrayList<Wrapper>();

        for (IEntity basement : entities) {

            MQBuilder builder1 = new MQBuilder(RelRepresentationBasementDocument.ENTITY_NAME, "o")
                    .add(MQExpression.eq("o", "first", basement));

            builder1.applyOrder(model.getDataSource().getEntityOrder());

            boolean isExist = builder1.getResultCount(getSession()) > 0 ? true : false;

            wrappers.add(new Wrapper(basement, isExist));
        }
        Collections.sort(wrappers, new EntityComparator<Wrapper>(model.getDataSource().getEntityOrder()));
        UniUtils.createPage(model.getDataSource(), wrappers);

    }

    @Override
    protected String getFirstObjectEntityName() {
        return "ru.tandemservice.movestudentrmc.entity.catalog.RepresentationBasement";
    }

}
