package ru.tandemservice.movestudentrmc.component.catalog.grantView.GrantViewPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;

public interface IDAO extends IDefaultCatalogPubDAO<GrantView, Model> {
    void updateInUse(Model model, Long id);

    void updateInSuspend(Model model, Long id);
}
