package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.List;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.RepresentationDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentEditUI;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.Add.DORepresentBaseAdd;
import ru.tandemservice.movestudentrmc.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentIC;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentationTypeCodes;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.ReportRenderer;

import java.util.List;
import java.util.Map;

@State({
        @Bind(key = DORepresentBaseListUI.STUDENT_LIST_ID, binding = DORepresentBaseListUI.STUDENT_LIST_ID),
        @Bind(key = DORepresentBaseListUI.ORG_UNIT_ID, binding = DORepresentBaseListUI.ORG_UNIT_ID)
})
public class DORepresentBaseListUI extends UIPresenter {

    public static final String STUDENT_LIST_ID = "studentListId";
    public static final String ORG_UNIT_ID = "orgUnitId";

    private BaseSearchListDataSource _contentDS;
    private List<Long> _studentListId;
    private Long _orgUnitId;


    @Override
    public void onComponentRefresh()
    {
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource iuiDataSource) {

        if (DORepresentBaseList.REPRESENTATION_DS.equals(iuiDataSource.getName())) {
            if (_studentListId != null && !_studentListId.isEmpty())
                iuiDataSource.put(RepresentationDSHandler.STUDENT_ID_FILTER, _studentListId.get(0));

            if (_orgUnitId != null)
                iuiDataSource.put(RepresentationDSHandler.ORG_UNIT_ID_FILTER, _orgUnitId);

            Map<String, Object> settingMap = _uiSettings.getAsMap(
                    RepresentationDSHandler.TYPE_REPRESENT_FILTER,
                    RepresentationDSHandler.DATE_FORMATIVE_FROM_FILTER,
                    RepresentationDSHandler.DATE_FORMATIVE_TO_FILTER
            );
            iuiDataSource.putAll(settingMap);
        }
    }

    public BaseSearchListDataSource getContentDS()
    {
        if (_contentDS == null)
            _contentDS = getConfig().getDataSource(DORepresentBaseList.REPRESENTATION_DS);
        return _contentDS;
    }

    public void onClickAdd()
    {
        _uiActivation.asRegion(DORepresentBaseAdd.class)
                .parameter(AbstractDORepresentEditUI.STUDENT_LIST_ID, _studentListId)
                .activate();
    }

    public void onEditEntityFromList()
    {
        DataWrapper e = getContentDS().getRecordById(getListenerParameterAsLong());
        Representation representBase = e.getWrapped();
        String type = representBase.getType().getCode();
        /*
        IDOObjectByRepresentTypeManager representManager = DORepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(type);
        _uiActivation.asCurrent(representManager.getEditComponentManager())
                .parameter(AbstractDORepresentEditUI.TYPE_ID, representBase.getRepresentation().getId())
                .parameter(AbstractDORepresentEditUI.REPRESENTATION_ID, representBase.getId())
                .parameter(AbstractDORepresentEditUI.STUDENT_LIST_ID, _studentListId)
                .activate();
		*/
    }

    public void onPrintExtract()
    {
        DataWrapper e = getContentDS().getRecordById(getListenerParameterAsLong());
        Representation representBase = e.getWrapped();

        try {
            if (representBase.getDocExtract() == null)
                throw new ApplicationException("Файл выписки пуст");
            IDocumentRenderer doc = new ReportRenderer("Extract.rtf", representBase.getDocExtract());
            BusinessComponentUtils.downloadDocument(doc, true);
        }
        catch (Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public void onPrintRepresent()
    {
        DataWrapper e = getContentDS().getRecordById(getListenerParameterAsLong());
        Representation representBase = e.getWrapped();

        try {
            IDocumentRenderer doc = UtilPrintSupport.representRenderer(representBase);
            if (doc != null)
                BusinessComponentUtils.downloadDocument(doc, true);
        }
        catch (Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }

    }

    public void onDeleteEntityFromList()
    {

        DataWrapper e = getContentDS().getRecordById(getListenerParameterAsLong());
        Representation representBase = e.getWrapped();
        String type = representBase.getType().getCode();
        if (type.equals(RepresentationTypeCodes.CHANGE_NAME)) {
            DQLSelectBuilder builder = new DQLSelectBuilder();
            builder.fromEntity(DocRepresentStudentIC.class, "sic").column("sic");
            builder.where(DQLExpressions.eq(DQLExpressions.property(DocRepresentStudentIC.representation().fromAlias("sic")), DQLExpressions.value(representBase)));
            List<DocRepresentStudentIC> representList = builder.createStatement(getSupport().getSession()).list();

            for (DocRepresentStudentIC dr : representList) {
                IdentityCard ic = dr.getStudentIC();
                DataAccessServices.dao().delete(dr);
                DataAccessServices.dao().delete(ic);
            }

        }
        MoveStudentDaoFacade.getGrantDAO().deleteStudentGrants(representBase);
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public Boolean getIsFormative() {

        DataWrapper e = getContentDS().getCurrent();
        Representation representBase = e.getWrapped();
        return !representBase.getState().getCode().equals("1");
    }

    public Boolean getIsOrgUnit() {

        return _orgUnitId != null;
    }

    public Boolean getIsPrintExtract() {

        DataWrapper e = getContentDS().getCurrent();
        Representation representBase = e.getWrapped();
        return representBase.getDocExtract() == null;
    }

    public List<Long> getStudentListId() {
        return _studentListId;
    }

    public void setStudentListId(List<Long> studentListId) {
        _studentListId = studentListId;
    }

    public Long getOrgUnitId() {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId) {
        _orgUnitId = orgUnitId;
    }

    public boolean isArchived()
    {
        if (_studentListId.size() == 1)
            return IUniBaseDao.instance.get().get(Student.class, "id", _studentListId.get(0)).isArchival();
        return false;
    }
}
