package ru.tandemservice.movestudentrmc.base.bo.DOOrder.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.entity.DocOrdRepresent;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;

import java.util.List;

public class StudentComboCommitDSHandler extends DefaultComboDataSourceHandler {
    public StudentComboCommitDSHandler(String ownerId) {
        super(ownerId, Student.class, Student.person().identityCard().fullFio());
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep) {
        String userInput = ep.input.getComboFilterByValue();
        List<Object> formativeOrgUnitsFilter = ep.context.get("formativeOrgUnitsFilter");

        List<Object> coursesFilter = ep.context.get("coursesFilter");

        List<RepresentationType> typeRepresentFilter = ep.context.get("typesRepresentFilter");
        List<RepresentationReason> reasons = ep.context.get("reasonsRepresentFilter");

        FilterUtils.applySelectFilter(ep.dqlBuilder, "e", Student.educationOrgUnit().formativeOrgUnit(), formativeOrgUnitsFilter);

        FilterUtils.applySelectFilter(ep.dqlBuilder, "e", Student.course(), coursesFilter);

        FilterUtils.applySimpleLikeFilter(ep.dqlBuilder, "e", Student.person().identityCard().fullFio(), userInput);

        applyTypeRepresentFilter(ep.dqlBuilder, typeRepresentFilter, reasons);

    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep) {
        ep.dqlBuilder.order("e." + Student.person().identityCard().fullFio());
    }

    public void applyTypeRepresentFilter(DQLSelectBuilder builder, List<RepresentationType> listTypes, List<RepresentationReason> listReasons) {
        DQLSelectBuilder subBuilder = new DQLSelectBuilder()
                .fromEntity(DocRepresentStudentBase.class, "drs")
                .joinEntity("drs", DQLJoinType.inner, DocOrdRepresent.class, "dor",
                            DQLExpressions.eq(
                                    DQLExpressions.property(DocRepresentStudentBase.representation().id().fromAlias("drs")),
                                    DQLExpressions.property(DocOrdRepresent.representation().id().fromAlias("dor")))
                )
                .column(DQLExpressions.property(DocRepresentStudentBase.student().id().fromAlias("drs")))
                .where(DQLExpressions.eq(DQLExpressions.property(DocRepresentStudentBase.representation().committed().fromAlias("drs")), DQLExpressions.value(Boolean.TRUE)))
                .predicate(DQLPredicateType.distinct);
        if (listTypes != null) {
            FilterUtils.applySelectFilter(subBuilder, "drs", DocRepresentStudentBase.representation().type(), listTypes);
        }

        if (listReasons != null) {
            FilterUtils.applySelectFilter(subBuilder, "drs", DocRepresentStudentBase.representation().reason(), listReasons);
        }

        builder.where(DQLExpressions.in(DQLExpressions.property(Student.id().fromAlias("e")), subBuilder.buildQuery()));
    }
}
