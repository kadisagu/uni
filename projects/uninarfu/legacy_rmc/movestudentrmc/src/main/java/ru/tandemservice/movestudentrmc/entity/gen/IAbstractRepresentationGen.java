package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.IAbstractRepresentation;
import ru.tandemservice.movestudentrmc.entity.catalog.MovestudentExtractStates;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import ru.tandemservice.movestudentrmc.entity.IAbstractRepresentation;

@SuppressWarnings({"all", "unchecked", "unused"})
public abstract class IAbstractRepresentationGen extends InterfaceStubBase
 implements IAbstractRepresentation{
    public static final int VERSION_HASH = -910743932;

    public static final String L_TYPE = "type";
    public static final String P_REPRESENT_I_D = "representID";
    public static final String L_STATE = "state";

    private RepresentationType _type;
    private Long _representID;
    private MovestudentExtractStates _state;

    @NotNull

    public RepresentationType getType()
    {
        return _type;
    }

    public void setType(RepresentationType type)
    {
        _type = type;
    }


    public Long getRepresentID()
    {
        return _representID;
    }

    public void setRepresentID(Long representID)
    {
        _representID = representID;
    }


    public MovestudentExtractStates getState()
    {
        return _state;
    }

    public void setState(MovestudentExtractStates state)
    {
        _state = state;
    }

    private static final Path<IAbstractRepresentation> _dslPath = new Path<IAbstractRepresentation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ru.tandemservice.movestudentrmc.entity.IAbstractRepresentation");
    }
            

    /**
     * @return Тип представления. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.IAbstractRepresentation#getType()
     */
    public static RepresentationType.Path<RepresentationType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return ID представления.
     * @see ru.tandemservice.movestudentrmc.entity.IAbstractRepresentation#getRepresentID()
     */
    public static PropertyPath<Long> representID()
    {
        return _dslPath.representID();
    }

    /**
     * @return Состояние представления..
     * @see ru.tandemservice.movestudentrmc.entity.IAbstractRepresentation#getState()
     */
    public static MovestudentExtractStates.Path<MovestudentExtractStates> state()
    {
        return _dslPath.state();
    }

    public static class Path<E extends IAbstractRepresentation> extends EntityPath<E>
    {
        private RepresentationType.Path<RepresentationType> _type;
        private PropertyPath<Long> _representID;
        private MovestudentExtractStates.Path<MovestudentExtractStates> _state;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип представления. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.IAbstractRepresentation#getType()
     */
        public RepresentationType.Path<RepresentationType> type()
        {
            if(_type == null )
                _type = new RepresentationType.Path<RepresentationType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return ID представления.
     * @see ru.tandemservice.movestudentrmc.entity.IAbstractRepresentation#getRepresentID()
     */
        public PropertyPath<Long> representID()
        {
            if(_representID == null )
                _representID = new PropertyPath<Long>(IAbstractRepresentationGen.P_REPRESENT_I_D, this);
            return _representID;
        }

    /**
     * @return Состояние представления..
     * @see ru.tandemservice.movestudentrmc.entity.IAbstractRepresentation#getState()
     */
        public MovestudentExtractStates.Path<MovestudentExtractStates> state()
        {
            if(_state == null )
                _state = new MovestudentExtractStates.Path<MovestudentExtractStates>(L_STATE, this);
            return _state;
        }

        public Class getEntityClass()
        {
            return IAbstractRepresentation.class;
        }

        public String getEntityName()
        {
            return "ru.tandemservice.movestudentrmc.entity.IAbstractRepresentation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
