package ru.tandemservice.movestudentrmc.dao.check;

import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;

public class CheckStatusTravel extends AbstractCheckStudentAdditionalStatus {

    private final static String TRAVEL_CODE = "movestudentrmc.trip";

    @Override
    protected void prepareConditionData() {
        this.studentCustomStateCode = TRAVEL_CODE;
        this.studentCustomStateCI = getCatalogItem(StudentCustomStateCI.class, studentCustomStateCode);
    }

}
