package ru.tandemservice.movestudentrmc.dao.check;

import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisession.entity.mark.SessionMark;

public class CheckStudentFirstSessionMarkExcellent extends AbstractCheckSessionMark {

    private final static String MARK_EXCELLENT_CODE = "scale5.5";
    /**
     * Зачтено
     */
    private final static String MARK_APPROVED = "scale2.5";

    @Override
    protected boolean checkCondition(Student student) {

        if (!marksMap.containsKey(student.getId()))
            return false;

        for (SessionMark sessionMark : marksMap.get(student.getId()))
            if ((!sessionMark.getValueItem().getCode().equals(MARK_EXCELLENT_CODE) && !sessionMark.getValueItem().getCode().equals(MARK_APPROVED))
                    && (sessionMark.getSlot().getTryNumber() != null || !(Integer.valueOf(1).equals(sessionMark.getSlot().getTryNumber()))))
                return false;

        return true;
    }

    @Override
    protected String createError(Student student, boolean value) {
        StringBuilder sb = new StringBuilder()
                .append("Студент ")
                .append(student.getPerson().getFullFio())
                .append(" ")
                .append(value ? "не " : "")
                .append("имеет только оценки в сессию «Отлично» без пересдач за последнюю сессию");
        return sb.toString();
    }

    @Override
    protected void prepareParam() {
        this.inSession = true;
    }
}
