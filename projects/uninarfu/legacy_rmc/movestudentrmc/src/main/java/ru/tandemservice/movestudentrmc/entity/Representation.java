package ru.tandemservice.movestudentrmc.entity;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;

import ru.tandemservice.movestudentrmc.entity.catalog.MovestudentExtractStates;
import ru.tandemservice.movestudentrmc.entity.gen.RepresentationGen;

import java.util.List;

//-------------------------------------------------------------
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
//-------------------------------------------------------------

/**
 * Базовое представление в приказ
 */
public class Representation extends RepresentationGen
{
    @Override
    public String getTitle() {
        return getType().getTitle();
    }

    /**
     * Название типа представления без ФИО сотрудника и даты создания
     *
     * @return
     */
    public String getRepresentationTitle() {
        return getTitle();
    }
    
    //---------------------------------------------------------------------------------------------------------------
    @EntityDSLSupport
    public String getTitleWithCreator() {
        return getTitle() 
        		+ ((getCreator() != null && getCreator() instanceof EmployeePost) ? ", " + ((EmployeePost) getCreator()).getEmployee().getFullFio() + ", " + ((EmployeePost) getCreator()).getOrgUnit().getShortTitle() : "") 
        		;
        
    } 
    //---------------------------------------------------------------------------------------------------------------

    @Override
    public void setState(ICatalogItem state) {
        setState((MovestudentExtractStates) state);
    }

    @Override
    public String toString() {

        return getType().getTitle();
    }

    public DocumentOrder getOrder() {

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(DocOrdRepresent.class, "do")
                .where(DQLExpressions.eqValue(DQLExpressions.property("do", DocOrdRepresent.representation()), this))
                .column(DQLExpressions.property("do", DocOrdRepresent.order()))
                //.setPredicate(DQLPredicateType.distinct)
                ;

        builder.createStatement(DataAccessServices.dao().getComponentSession()).uniqueResult();
        List<DocumentOrder> list = builder.createStatement(DataAccessServices.dao().getComponentSession()).list();

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

}