package ru.tandemservice.movestudentrmc.docord;

import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationBasement;

import java.util.List;

public class EditBasicGroupList {

    private List<RepresentationBasement> _basicList;
    private RepresentationBasement _selected;
    private String _title;
    private String _groupName;
    private String _groupTitle;

    public String getGroupName() {
        return _groupName;
    }

    public void setGroupName(String groupName) {
        _groupName = groupName;
    }

    public List<RepresentationBasement> getBasicList() {
        return _basicList;
    }

    public void setBasicList(List<RepresentationBasement> basicList) {
        _basicList = basicList;
    }

    public String getTitle() {
        return _title;
    }

    public void setTitle(String title) {
        _title = title;
    }

    public RepresentationBasement getSelected() {
        return _selected;
    }

    public void setSelected(RepresentationBasement selected) {
        _selected = selected;
    }

    public String getGroupTitle() {
        return _groupTitle;
    }

    public void setGroupTitle(String groupTitle) {
        _groupTitle = groupTitle;
    }
}
