package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntity;

//StudentGrantEntity.tmp больше не nullable
public class MS_movestudentrmc_1x0x0_21to22 extends IndependentMigrationScript {
    @Override
    public void run(DBTool tool) throws Exception {
        IEntityMeta meta = EntityRuntime.getMeta(StudentGrantEntity.class);
        if (!tool.tableExists(meta.getTableName()))
            return;

        String sql = "update " + meta.getTableName() + " set tmp_p=0 where tmp_p is null";
        tool.getConnection().createStatement().executeUpdate(sql);
    }


}
