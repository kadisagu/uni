package ru.tandemservice.movestudentrmc.base.bo.DocumentSettings;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.movestudentrmc.base.bo.DocumentSettings.logic.DocumentSettingsModifyDAO;
import ru.tandemservice.movestudentrmc.base.bo.DocumentSettings.logic.IDocumentSettingsModifyDAO;


@Configuration
public class DocumentSettingsManager extends BusinessObjectManager {
    public static DocumentSettingsManager instance() {
        return instance(DocumentSettingsManager.class);
    }

    @Bean
    public IDocumentSettingsModifyDAO dao() {
        return new DocumentSettingsModifyDAO();
    }
}
