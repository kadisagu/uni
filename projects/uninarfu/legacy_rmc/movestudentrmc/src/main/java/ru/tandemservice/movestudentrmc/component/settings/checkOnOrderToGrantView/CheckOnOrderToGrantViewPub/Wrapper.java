package ru.tandemservice.movestudentrmc.component.settings.checkOnOrderToGrantView.CheckOnOrderToGrantViewPub;

import org.tandemframework.core.entity.EntityBase;
import ru.tandemservice.movestudentrmc.entity.catalog.Grant;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;

public class Wrapper extends EntityBase {

    public static final String USED = "used";
    public static final String VALUE = "value";
    public static final String GRANT_VIEW = "view.title";
    public static final String GRANT = "grant.title";
    public static final String GRANT_ID = "grant.id";

    private Grant grant;
    private GrantView view;
    private boolean used;
    private boolean value;

    public Wrapper(Grant grant, GrantView view, boolean used, boolean value) {
        this.grant = grant;
        this.view = view;
        this.used = used;
        this.value = value;
    }

    @Override
    public Long getId() {
        return this.getView().getId();
    }

    public Grant getGrant() {
        return grant;
    }

    public void setGrant(Grant grant) {
        this.grant = grant;
    }

    public GrantView getView() {
        return view;
    }

    public void setView(GrantView view) {
        this.view = view;
    }

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    public boolean isValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

}
