package ru.tandemservice.movestudentrmc.base.bo.DORepresentWeekendOut.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.orgstruct.Group;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class NewGroupDSHandler extends DefaultComboDataSourceHandler {

    public static final String NEW_FORMATIVE_ORG_UNIT = "newFormativeOrgUnit";
    public static final String NEW_COURSE = "newCourse";
    public static final String NEW_EDUCATION_LEVELS_HIGH_SCHOOL = "newEducationLevelsHighSchool";
    public static final String NEW_DEVELOP_FORM = "newDevelopForm";

    public NewGroupDSHandler(String ownerId) {

        super(ownerId, Group.class, Group.P_TITLE);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        OrgUnit newFormativeOrgUnit = ep.context.get(NEW_FORMATIVE_ORG_UNIT);
        Course newCourse = ep.context.get(NEW_COURSE);
        EducationLevelsHighSchool newEducationLevelsHighSchool = ep.context.get(NEW_EDUCATION_LEVELS_HIGH_SCHOOL);
        DevelopForm newDevelopForm = ep.context.get(NEW_DEVELOP_FORM);

        String filter = ep.input.getComboFilterByValue();
        if (StringUtils.isNotBlank(filter)) {
            ep.dqlBuilder.where(like(DQLFunctions.upper(property(Group.title().fromAlias("e"))), value("%" + filter.toUpperCase() + "%")));
        }

        if (newFormativeOrgUnit != null)
            ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property(Group.educationOrgUnit().formativeOrgUnit().fromAlias("e")), DQLExpressions.value(newFormativeOrgUnit)));

        if (newCourse != null)
            ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property(Group.course().fromAlias("e")), DQLExpressions.value(newCourse)));

        if (newEducationLevelsHighSchool != null)
            ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property(Group.educationOrgUnit().educationLevelHighSchool().fromAlias("e")), DQLExpressions.value(newEducationLevelsHighSchool)));

        if (newDevelopForm != null)
            ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property(Group.educationOrgUnit().developForm().fromAlias("e")), DQLExpressions.value(newDevelopForm)));


    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
    {
        ep.dqlBuilder.order("e." + Group.title());
    }
}
