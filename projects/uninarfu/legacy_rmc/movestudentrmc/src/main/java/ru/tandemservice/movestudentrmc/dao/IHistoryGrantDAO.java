package ru.tandemservice.movestudentrmc.dao;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uni.dao.IUniBaseDao;


public interface IHistoryGrantDAO extends IUniBaseDao {

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void fillHistory();
}
