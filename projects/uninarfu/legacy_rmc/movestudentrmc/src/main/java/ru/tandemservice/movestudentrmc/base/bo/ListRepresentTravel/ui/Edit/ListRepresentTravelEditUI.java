package ru.tandemservice.movestudentrmc.base.bo.ListRepresentTravel.ui.Edit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import org.tandemframework.tapsupport.component.selection.SingleSelectTextModel;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentTravel.logic.DORepresentTravelDAO;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.support.AbstractListRepresentEditUI;
import ru.tandemservice.movestudentrmc.dao.CheckStateUtil;
import ru.tandemservice.movestudentrmc.entity.ListRepresentTravel;
import ru.tandemservice.movestudentrmc.entity.RepresentTravel;
import ru.tandemservice.movestudentrmc.entity.catalog.TravelPaymentData;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentExtractStatesCodes;
import ru.tandemservice.uni.dao.IUniBaseDao;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@State({
        @Bind(key = AbstractListRepresentEditUI.LIST_REPRESENT_ID, binding = AbstractListRepresentEditUI.LIST_REPRESENT_ID),
        @Bind(key = "typeRepresentFilter", binding = "typeRepresentFilter"),
        @Bind(key = "reasonRepresentFilter", binding = "reasonRepresentFilter"),
        @Bind(key = "basementRepresentFilter", binding = "basementRepresentFilter"),
        @Bind(key = "representationBasementDate", binding = "representationBasementDate"),
        @Bind(key = "representationBasementNumber", binding = "representationBasementNumber"),
})
public class ListRepresentTravelEditUI extends AbstractListRepresentEditUI<ListRepresentTravel> {
    private ISelectModel organizationSelectModel;

    @Override
    public ListRepresentTravel getListRepresentObject() {
        return new ListRepresentTravel();
    }

    @Override
    public void onComponentRefresh() {

        setOrganizationSelectModel(new SingleSelectTextModel() {
            @Override
            public ListResult findValues(String filter) {

            	//--------------------------------------------------------------------------------------------------------------------------
            	/*
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(ListRepresentTravel.class, "e")
                        .column(DQLExpressions.property(ListRepresentTravel.organization().fromAlias("e")))
                        .setPredicate(DQLPredicateType.distinct)
                        .where(DQLExpressions.likeUpper(DQLExpressions.property(ListRepresentTravel.organization().fromAlias("e")), DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));
                */
            	
                DQLSelectBuilder fragment = new DQLSelectBuilder()
                        .fromEntity(ListRepresentTravel.class, "e")
                        .column(DQLExpressions.property(ListRepresentTravel.organization().fromAlias("e")))
                        .predicate(DQLPredicateType.distinct)
                        .where(DQLExpressions.likeUpper(DQLExpressions.property(ListRepresentTravel.organization().fromAlias("e")), DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));
                		

                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(RepresentTravel.class, "e")
                        .column(DQLExpressions.property(RepresentTravel.organization().fromAlias("e")), "org")
                        .predicate(DQLPredicateType.distinct)
                        .where(DQLExpressions.likeUpper(DQLExpressions.property(RepresentTravel.organization().fromAlias("e")), DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))))
                        .union(fragment.buildSelectRule())
                ;                
                //--------------------------------------------------------------------------------------------------------------------------

                List list = builder.createStatement(getSupport().getSession()).setMaxResults(50).list();
                Number number = (Number) builder.createCountStatement(new DQLExecutionContext(getSupport().getSession())).uniqueResult();

                return new ListResult(list, number == null ? 0L : number.intValue());
            }
        });
        super.onComponentRefresh();

        final ListRepresentTravel rep = (ListRepresentTravel) getListRepresent();
        if (rep.getAddressCountry() == null) {
            IUniBaseDao dao = IUniBaseDao.instance.get();
            AddressCountry result = dao.get(AddressCountry.class, AddressCountry.code(), DORepresentTravelDAO.RUSSIA_CODE);
            rep.setAddressCountry(result);
        }
        if (rep.getBeginDate() == null)
            rep.setBeginDate(new Date());
    }

    public void onAddNewTravelPaymentData() {
        String title = (String) getClientParameter();

        if (!StringUtils.isEmpty(title)) {
            TravelPaymentData entity = IUniBaseDao.instance.get().get(TravelPaymentData.class, TravelPaymentData.title(), title);
            if (entity == null) {
                entity = new TravelPaymentData();
                entity.setCode(getNewCatalogItemCode());
                entity.setTitle(title);
                DataAccessServices.dao().saveOrUpdate(entity);
            }

            ((ListRepresentTravel) getListRepresent()).setTravelPaymentData(entity);
        }
    }

    public String getNewCatalogItemCode()
    {
        int count;
        for (count = DataAccessServices.dao().getCount(TravelPaymentData.class); CatalogManager.instance().dao().getCatalogItem(TravelPaymentData.class, Integer.toString(count)) != null; count++) ;
        return String.valueOf(count);
    }


    public ISelectModel getOrganizationSelectModel() {
        return organizationSelectModel;
    }

    public void setOrganizationSelectModel(ISelectModel organizationSelectModel) {
        this.organizationSelectModel = organizationSelectModel;
    }

    @Override
    public void onClickSave()
    {
        if (getListRepresentId() != null)
            CheckStateUtil.checkStateRepresent(getListRepresent(), Arrays.asList(MovestudentExtractStatesCodes.CODE_1), _uiSupport.getSession());

        final ListRepresentTravel rep = (ListRepresentTravel) getListRepresent();
        if (rep.getBeginDate() != null && rep.getEndDate() != null && rep.getDays() == null) {
            long _diff = rep.getEndDate().getTime() - rep.getBeginDate().getTime();
            int days = (int) (_diff / (1000 * 60 * 60 * 24)) + 1;
            rep.setDays(days);
        }
        super.onClickSave();
    }
    
    //---------------------------------------------------------------------------------------------------------
    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
    	
    	super.onBeforeDataSourceFetch(dataSource);
    	
    	if(dataSource.getName().equals("settlementDS")) {
    		dataSource.put("country", ((ListRepresentTravel) getListRepresent()).getAddressCountry());
    	}
    }
    
    public void onClickChangeDate() {
    	
    	ListRepresentTravel rep = (ListRepresentTravel) getListRepresent();
        if (rep.getBeginDate() != null && rep.getEndDate() != null) {
            int days = (int) ((rep.getEndDate().getTime() - rep.getBeginDate().getTime()) / (1000 * 60 * 60 * 24)) + 1;
            rep.setDays(days);
        }
    }
    //---------------------------------------------------------------------------------------------------------
}


