package ru.tandemservice.movestudentrmc.base.bo.DORepresentWeekendOut.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.FilterUtils;

public class EducationLevelsHighSchoolDSHandler extends DefaultComboDataSourceHandler {

    public static final String NEW_FORMATIVE_ORG_UNIT = "newFormativeOrgUnit";

    public EducationLevelsHighSchoolDSHandler(String ownerId) {
        super(ownerId, EducationLevelsHighSchool.class);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep) {

        OrgUnit newFormativeOrgUnit = ep.context.get(NEW_FORMATIVE_ORG_UNIT);

        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(EducationOrgUnit.class, "eou");

        builder.addColumn("eou." + EducationOrgUnit.educationLevelHighSchool().s());

        FilterUtils.applySelectFilter(builder, "eou", EducationOrgUnit.formativeOrgUnit(), newFormativeOrgUnit);

        String filterByValue = ep.input.getComboFilterByValue();
        FilterUtils.applySimpleLikeFilter(ep.dqlBuilder, "e", EducationLevelsHighSchool.fullTitle(), filterByValue);

        ep.dqlBuilder.where(DQLExpressions.in(DQLExpressions.property(EducationLevelsHighSchool.id().fromAlias("e")), builder.getQuery()));

    }


    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep) {
        //ep.dqlBuilder.order("e." + EducationOrgUnit.educationLevelHighSchool().displayableTitle());
        ep.dqlBuilder.order(DQLExpressions.property(EducationLevelsHighSchool.educationLevel().inheritedOkso().fromAlias("e")));
        ep.dqlBuilder.order(DQLExpressions.property(EducationLevelsHighSchool.educationLevel().title().fromAlias("e")));

        //ep.dqlBuilder.order(DQLExpressions.property(EducationLevelsHighSchool.printTitle().fromAlias("e")));

    }
}
