package ru.tandemservice.movestudentrmc.base.bo.DORepresentWeekendOut.ui.Edit;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentEditUI;
import ru.tandemservice.movestudentrmc.docord.util.MoveStudentUtil;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentationTypeCodes;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.*;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DORepresentWeekendOutEditUI extends AbstractDORepresentEditUI
{

    private EducationOrgUnit eduOrgUnit = new EducationOrgUnit();
    private boolean showAllGroups = false;

    @Override
    protected void FetchStudents(Representation doc) {

        _studentDocList.clear();
        _studentList.clear();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocRepresentStudentBase.class, "stud").column("stud");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(DocRepresentStudentBase.representation().fromAlias("stud")),
                DQLExpressions.value(doc.getId())));

        List<DocRepresentStudentBase> studentList = builder.createStatement(getSupport().getSession()).list();

        for (DocRepresentStudentBase student : studentList) {

            _studentDocList.add(student);
            _studentList.add(student.getStudent());
        }

        _student = _studentList.get(0);
    }

    @Override
    protected void initDocument() {
        super.initDocument();
        _studentStatusNewStr = UniDaoFacade.getCoreDao().getCatalogItem(StudentStatus.class, "1").getTitle();
        //newFormativeOrgUnit = _student.getEducationOrgUnit().getFormativeOrgUnit();

        if (getRepresentWeekendOut().getId() != null && getRepresentWeekendOut().getEducationOrgUnit() != null) {
            getEduOrgUnit().update(getRepresentWeekendOut().getEducationOrgUnit());
            setShowAllGroups(!getEduOrgUnit().getEducationLevelHighSchool().equals(getRepresentWeekendOut().getNewGroup().getEducationOrgUnit().getEducationLevelHighSchool()));
        }

        //ищем у студента проведенный приказ о предоставлении АО и о продлении АО
        DQLSelectBuilder selectBuilder = new DQLSelectBuilder()
                .fromEntity(DocOrdRepresent.class, "dor")
                .column("dor")
                .joinEntity("dor", DQLJoinType.left, DocRepresentStudentBase.class, "drs", DQLExpressions.eq(DQLExpressions.property(DocRepresentStudentBase.representation().fromAlias("drs")), DQLExpressions.property(DocOrdRepresent.representation().fromAlias("dor"))))
                .where(DQLExpressions.eq(DQLExpressions.property(DocRepresentStudentBase.student().fromAlias("drs")), DQLExpressions.value(_student)))
                .where(DQLExpressions.eq(DQLExpressions.property(DocOrdRepresent.order().state().code().fromAlias("dor")), DQLExpressions.value("5")))
                .where(DQLExpressions.or(
                        DQLExpressions.eq(DQLExpressions.property(DocOrdRepresent.representation().type().code().fromAlias("dor")), DQLExpressions.value(RepresentationTypeCodes.WEEKEND)),
                        DQLExpressions.eq(DQLExpressions.property(DocOrdRepresent.representation().type().code().fromAlias("dor")), DQLExpressions.value(RepresentationTypeCodes.WEEKEND_PROLONG))
                ))
                .order(DQLExpressions.property(DocOrdRepresent.order().commitDateSystem().fromAlias("dor")), OrderDirection.desc);

        List<DocOrdRepresent> list = UniDaoFacade.getCoreDao().getList(selectBuilder);
        DocOrdRepresent orderRepresent;
        if (!list.isEmpty()) {
            orderRepresent = list.get(0); //берем последний по дате проведения приказ
            Date endDate;
            if (orderRepresent.getRepresentation() instanceof RepresentWeekend)
                endDate = ((RepresentWeekend) orderRepresent.getRepresentation()).getEndDate();
            else
                endDate = ((RepresentWeekendProlong) orderRepresent.getRepresentation()).getStartDate();

            Calendar c = Calendar.getInstance();
            c.setTime(endDate);
            c.add(Calendar.DAY_OF_YEAR, 1); //добавляем к дате окончания АО 1 день
            ((RepresentWeekendOut) _representObj).setStartDate(c.getTime());
        }

    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);
        if (DORepresentWeekendOutEdit.NEW_GROUP_DS.equals(dataSource.getName())) {
            dataSource.put(GroupDSHandler.SHOW_ALL_GROUPS, isShowAllGroups());
            dataSource.put(GroupDSHandler.NEW_FORMATIVE_ORG_UNIT, getEduOrgUnit().getFormativeOrgUnit());
            dataSource.put(GroupDSHandler.NEW_TERRITORIAL_ORG_UNIT, getEduOrgUnit().getTerritorialOrgUnit());
            dataSource.put(GroupDSHandler.EDUCATION_LEVELS_HIGH_SCHOOL, getEduOrgUnit().getEducationLevelHighSchool());
            dataSource.put(GroupDSHandler.DEVELOP_FORM, getEduOrgUnit().getDevelopForm());
            dataSource.put(GroupDSHandler.DEVELOP_CONDITION, getEduOrgUnit().getDevelopCondition());
            dataSource.put(GroupDSHandler.DEVELOP_PERIOD, getEduOrgUnit().getDevelopPeriod());
            dataSource.put(GroupDSHandler.DEVELOP_TECH, getEduOrgUnit().getDevelopTech());
            dataSource.put(GroupDSHandler.COURSE, getRepresentWeekendOut().getNewCourse());
        }

        if (DORepresentWeekendOutEdit.EDU_LEVEL_DS.equals(dataSource.getName())) {
            dataSource.put(EducationLevelsHighSchoolDSHandler.FILTER_BY_DEVELOP, Boolean.FALSE);
            dataSource.put(EducationLevelsHighSchoolDSHandler.NEW_TERRITORIAL_ORG_UNIT, getEduOrgUnit().getTerritorialOrgUnit());
            dataSource.put(EducationLevelsHighSchoolDSHandler.NEW_FORMATIVE_ORG_UNIT, getEduOrgUnit().getFormativeOrgUnit());
        }

        if (DORepresentWeekendOutEdit.DEVELOP_FORM_DS.equals(dataSource.getName())) {
            dataSource.put(DevelopFormDSHandler.NEW_FORMATIVE_ORG_UNIT, getEduOrgUnit().getFormativeOrgUnit());
            dataSource.put(DevelopFormDSHandler.NEW_TERRITORIAL_ORG_UNIT, getEduOrgUnit().getTerritorialOrgUnit());
            dataSource.put(DevelopFormDSHandler.EDUCATION_LEVELS_HIGH_SCHOOL, getEduOrgUnit().getEducationLevelHighSchool());
        }

        if (DORepresentWeekendOutEdit.DEVELOP_CONDITION_DS.equals(dataSource.getName())) {
            dataSource.put(DevelopConditionDSHandler.NEW_FORMATIVE_ORG_UNIT, getEduOrgUnit().getFormativeOrgUnit());
            dataSource.put(DevelopConditionDSHandler.NEW_TERRITORIAL_ORG_UNIT, getEduOrgUnit().getTerritorialOrgUnit());
            dataSource.put(DevelopConditionDSHandler.EDUCATION_LEVELS_HIGH_SCHOOL, getEduOrgUnit().getEducationLevelHighSchool());
            dataSource.put(DevelopConditionDSHandler.DEVELOP_FORM, getEduOrgUnit().getDevelopForm());
        }

        if (DORepresentWeekendOutEdit.DEVELOP_PERIOD_DS.equals(dataSource.getName())) {
            dataSource.put(DevelopPeriodDSHandler.NEW_FORMATIVE_ORG_UNIT, getEduOrgUnit().getFormativeOrgUnit());
            dataSource.put(DevelopPeriodDSHandler.NEW_TERRITORIAL_ORG_UNIT, getEduOrgUnit().getTerritorialOrgUnit());
            dataSource.put(DevelopPeriodDSHandler.EDUCATION_LEVELS_HIGH_SCHOOL, getEduOrgUnit().getEducationLevelHighSchool());
            dataSource.put(DevelopPeriodDSHandler.DEVELOP_FORM, getEduOrgUnit().getDevelopForm());
            dataSource.put(DevelopPeriodDSHandler.DEVELOP_CONDITION, getEduOrgUnit().getDevelopCondition());
        }

        if (DORepresentWeekendOutEdit.DEVELOP_TECH_DS.equals(dataSource.getName())) {
            dataSource.put(DevelopTechDSHandler.NEW_FORMATIVE_ORG_UNIT, getEduOrgUnit().getFormativeOrgUnit());
            dataSource.put(DevelopTechDSHandler.NEW_TERRITORIAL_ORG_UNIT, getEduOrgUnit().getTerritorialOrgUnit());
            dataSource.put(DevelopTechDSHandler.EDUCATION_LEVELS_HIGH_SCHOOL, getEduOrgUnit().getEducationLevelHighSchool());
            dataSource.put(DevelopTechDSHandler.DEVELOP_FORM, getEduOrgUnit().getDevelopForm());
            dataSource.put(DevelopTechDSHandler.DEVELOP_CONDITION, getEduOrgUnit().getDevelopCondition());
            dataSource.put(DevelopTechDSHandler.DEVELOP_PERIOD, getEduOrgUnit().getDevelopPeriod());
        }

    }

    @Override
    public void onClickSave() throws IOException {

        EducationOrgUnit educationOrgUnit = MoveStudentUtil.getEducationOrgUnit(getEduOrgUnit());
        if (educationOrgUnit == null)
            throw new ApplicationException("Подходящее направление подготовки (специальность) подразделения не найдено.");
        getRepresentWeekendOut().setEducationOrgUnit(educationOrgUnit);
        getRepresentWeekendOut().setNewFormativeOrgUnit(educationOrgUnit.getFormativeOrgUnit());
        getRepresentWeekendOut().setNewEducationLevelsHighSchool(educationOrgUnit.getEducationLevelHighSchool());
        getRepresentWeekendOut().setNewDevelopForm(educationOrgUnit.getDevelopForm());
        
        //--------------------------------------------------------------------------------------------------------------------
        if (!getRepresentWeekendOut().isDebtLiquidation())
        	getRepresentWeekendOut().setDebtLiquidationDate(null); 
        if (!getRepresentWeekendOut().isDifferenceLiquidation())
        	getRepresentWeekendOut().setDifferenceLiquidationDate(null); 
        //--------------------------------------------------------------------------------------------------------------------
        
        super.onClickSave();
    }

    public void onChangeGroup() {
        if (getRepresentWeekendOut().getNewCourse() == null)
            getRepresentWeekendOut().setNewCourse(getRepresentWeekendOut().getNewGroup().getCourse());
    }

    public RepresentWeekendOut getRepresentWeekendOut() {
        return (RepresentWeekendOut) this.getRepresentObj();
    }

    public EducationOrgUnit getEduOrgUnit() {
        return eduOrgUnit;
    }

    public void setEduOrgUnit(EducationOrgUnit eduOrgUnit) {
        this.eduOrgUnit = eduOrgUnit;
    }

    public boolean isShowAllGroups() {
        return showAllGroups;
    }

    public void setShowAllGroups(boolean showAllGroups) {
        this.showAllGroups = showAllGroups;
    }

}
