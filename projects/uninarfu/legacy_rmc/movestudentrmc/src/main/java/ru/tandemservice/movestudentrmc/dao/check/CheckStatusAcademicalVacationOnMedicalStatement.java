package ru.tandemservice.movestudentrmc.dao.check;

import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;

public class CheckStatusAcademicalVacationOnMedicalStatement extends AbstractCheckStudentAdditionalStatus {

    public final static String ACADEMICAL_VACATION_ON_MEDICAL_STATEMENT_CODE = "movestudentrmc.weekend.honey.ind";

    @Override
    protected void prepareConditionData() {
        this.studentCustomStateCode = ACADEMICAL_VACATION_ON_MEDICAL_STATEMENT_CODE;
        this.studentCustomStateCI = getCatalogItem(StudentCustomStateCI.class, studentCustomStateCode);
    }
}
