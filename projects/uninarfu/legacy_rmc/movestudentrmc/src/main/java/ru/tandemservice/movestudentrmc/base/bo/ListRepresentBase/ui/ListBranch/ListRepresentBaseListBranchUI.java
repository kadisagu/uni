package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ui.ListBranch;

import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.ListRepresentDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.support.AbstractListRepresentBaseListUI;

import java.util.HashMap;
import java.util.Map;

public class ListRepresentBaseListBranchUI extends AbstractListRepresentBaseListUI
{

    @Override
    protected Map<String, Object> getParams()
    {
        Map<String, Object> map = new HashMap<>();

        map.put(ListRepresentDSHandler.HIGH, Boolean.FALSE);
        map.put(ListRepresentDSHandler.MIDDLE, Boolean.FALSE);
        map.put(ListRepresentDSHandler.POSTGRADUATE, Boolean.FALSE);
        map.put(ListRepresentDSHandler.HIGH_BRANCH, Boolean.TRUE);
        map.put(ListRepresentDSHandler.MIDDLE_BRANCH, Boolean.FALSE);

        return map;
    }
}
