package ru.tandemservice.movestudentrmc.entity;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.entity.catalog.MovestudentOrderStates;
import ru.tandemservice.movestudentrmc.entity.gen.ListOrderGen;
import ru.tandemservice.uni.dao.IUniBaseDao;

import java.util.List;


/**
 * Списочный приказ
 */
public class ListOrder extends ListOrderGen
{
    public static final String P_REPRESENTATION_TYPES = "representationTypes";
    public static final String P_REPRESENTATION_COUNT = "representationCount";
    public static final String P_STUDENT_COUNT = "studentCount";

    public void setState(ICatalogItem state) {

        setState((MovestudentOrderStates) state);
    }

    public ICatalogItem getType() {

        return null;
    }

    public String getTitle() {

        return getFullTitle();
    }

    @EntityDSLSupport(parts = {P_TITLE})
    public String getFullTitle() {

        String str;

        str = "Приказ №" + getNumber() + ", от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getCommitDate());

        return str;
    }

    @EntityDSLSupport
    public String getRepresentationTypes() {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(ListOrdListRepresent.class, "lor")
                .where(DQLExpressions.eq(DQLExpressions.property(ListOrdListRepresent.order().fromAlias("lor")), DQLExpressions.value(this)))
                .column(DQLExpressions.property(ListOrdListRepresent.representation().representationType().title().fromAlias("lor")))
                .setPredicate(DQLPredicateType.distinct);

        List<String> result = IUniBaseDao.instance.get().getList(builder);
        return StringUtils.join(result, ", ");
    }

    @EntityDSLSupport
    public Integer getRepresentationCount() {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(ListOrdListRepresent.class, "lor")
                .where(DQLExpressions.eq(DQLExpressions.property(ListOrdListRepresent.order().fromAlias("lor")), DQLExpressions.value(this)));
        return IUniBaseDao.instance.get().getCount(builder);
    }

    public List<ListRepresent> getRepresentationList() {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(ListOrdListRepresent.class, "lor")
                .column(DQLExpressions.property("lor", ListOrdListRepresent.representation()))
                .where(DQLExpressions.eq(DQLExpressions.property(ListOrdListRepresent.order().fromAlias("lor")), DQLExpressions.value(this)));
        return IUniBaseDao.instance.get().getList(builder);
    }

    @EntityDSLSupport
    public Integer getStudentCount() {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(ListOrdListRepresent.class, "lor")
                .where(DQLExpressions.eq(DQLExpressions.property(ListOrdListRepresent.order().fromAlias("lor")), DQLExpressions.value(this)))
                .column(DQLExpressions.property(ListOrdListRepresent.representation().fromAlias("lor")));

        List<ListRepresent> list = IUniBaseDao.instance.get().getList(builder);

        int result = 0;
        for (ListRepresent listRepresent : list) {
            result += listRepresent.getStudentCount();
        }

        return result;

    }

}