package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudentrmc.entity.RepresentAdmissionAttendClasses;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * О допуске к посещению занятий
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RepresentAdmissionAttendClassesGen extends Representation
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.RepresentAdmissionAttendClasses";
    public static final String ENTITY_NAME = "representAdmissionAttendClasses";
    public static final int VERSION_HASH = 149151331;
    private static IEntityMeta ENTITY_META;

    public static final String L_COURSE = "course";
    public static final String L_TERM = "term";
    public static final String P_EXAMS_DATE = "examsDate";

    private Course _course;     // Номер курса, на котором разрешается продолжить студенту обучение
    private DevelopGridTerm _term;     // Семестр, в котором разрешается продолжить студенту обучение
    private Date _examsDate;     // Устранить академическую задолженность до

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Номер курса, на котором разрешается продолжить студенту обучение. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Номер курса, на котором разрешается продолжить студенту обучение. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Семестр, в котором разрешается продолжить студенту обучение. Свойство не может быть null.
     */
    @NotNull
    public DevelopGridTerm getTerm()
    {
        return _term;
    }

    /**
     * @param term Семестр, в котором разрешается продолжить студенту обучение. Свойство не может быть null.
     */
    public void setTerm(DevelopGridTerm term)
    {
        dirty(_term, term);
        _term = term;
    }

    /**
     * @return Устранить академическую задолженность до. Свойство не может быть null.
     */
    @NotNull
    public Date getExamsDate()
    {
        return _examsDate;
    }

    /**
     * @param examsDate Устранить академическую задолженность до. Свойство не может быть null.
     */
    public void setExamsDate(Date examsDate)
    {
        dirty(_examsDate, examsDate);
        _examsDate = examsDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof RepresentAdmissionAttendClassesGen)
        {
            setCourse(((RepresentAdmissionAttendClasses)another).getCourse());
            setTerm(((RepresentAdmissionAttendClasses)another).getTerm());
            setExamsDate(((RepresentAdmissionAttendClasses)another).getExamsDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RepresentAdmissionAttendClassesGen> extends Representation.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RepresentAdmissionAttendClasses.class;
        }

        public T newInstance()
        {
            return (T) new RepresentAdmissionAttendClasses();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return obj.getCourse();
                case "term":
                    return obj.getTerm();
                case "examsDate":
                    return obj.getExamsDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "term":
                    obj.setTerm((DevelopGridTerm) value);
                    return;
                case "examsDate":
                    obj.setExamsDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                        return true;
                case "term":
                        return true;
                case "examsDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return true;
                case "term":
                    return true;
                case "examsDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return Course.class;
                case "term":
                    return DevelopGridTerm.class;
                case "examsDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RepresentAdmissionAttendClasses> _dslPath = new Path<RepresentAdmissionAttendClasses>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RepresentAdmissionAttendClasses");
    }
            

    /**
     * @return Номер курса, на котором разрешается продолжить студенту обучение. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentAdmissionAttendClasses#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Семестр, в котором разрешается продолжить студенту обучение. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentAdmissionAttendClasses#getTerm()
     */
    public static DevelopGridTerm.Path<DevelopGridTerm> term()
    {
        return _dslPath.term();
    }

    /**
     * @return Устранить академическую задолженность до. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentAdmissionAttendClasses#getExamsDate()
     */
    public static PropertyPath<Date> examsDate()
    {
        return _dslPath.examsDate();
    }

    public static class Path<E extends RepresentAdmissionAttendClasses> extends Representation.Path<E>
    {
        private Course.Path<Course> _course;
        private DevelopGridTerm.Path<DevelopGridTerm> _term;
        private PropertyPath<Date> _examsDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Номер курса, на котором разрешается продолжить студенту обучение. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentAdmissionAttendClasses#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Семестр, в котором разрешается продолжить студенту обучение. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentAdmissionAttendClasses#getTerm()
     */
        public DevelopGridTerm.Path<DevelopGridTerm> term()
        {
            if(_term == null )
                _term = new DevelopGridTerm.Path<DevelopGridTerm>(L_TERM, this);
            return _term;
        }

    /**
     * @return Устранить академическую задолженность до. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentAdmissionAttendClasses#getExamsDate()
     */
        public PropertyPath<Date> examsDate()
        {
            if(_examsDate == null )
                _examsDate = new PropertyPath<Date>(RepresentAdmissionAttendClassesGen.P_EXAMS_DATE, this);
            return _examsDate;
        }

        public Class getEntityClass()
        {
            return RepresentAdmissionAttendClasses.class;
        }

        public String getEntityName()
        {
            return "representAdmissionAttendClasses";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
