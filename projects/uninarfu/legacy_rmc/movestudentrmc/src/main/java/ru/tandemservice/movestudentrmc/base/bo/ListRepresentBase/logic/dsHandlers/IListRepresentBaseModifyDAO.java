package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents;

import java.util.List;

public interface IListRepresentBaseModifyDAO extends INeedPersistenceSupport {

    void update(ListRepresent listRepresent, List<RelListRepresentStudents> studentList);

    void updateState(ListRepresent listRepresent);

    void delete(ListRepresent listRepresent);

}
