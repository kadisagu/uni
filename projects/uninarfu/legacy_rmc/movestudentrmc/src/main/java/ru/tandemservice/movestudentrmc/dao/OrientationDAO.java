package ru.tandemservice.movestudentrmc.dao;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

public class OrientationDAO extends UniBaseDao implements IOrientationDAO {

    @Override
    public DQLSelectBuilder getOrientationsBuilder() {
        return null;
    }

    @Override
    public DQLSelectBuilder getStudentBuilder(
            EducationOrgUnit educationOrgUnit, EducationYear educationYear)
    {
        return null;
    }

}
