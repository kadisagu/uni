package ru.tandemservice.movestudentrmc.base.bo.DOOrder.ui.ListCommit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.logic.OrderCommitDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.logic.OrderPrint;
import ru.tandemservice.movestudentrmc.entity.DocumentOrder;
import ru.tandemservice.movestudentrmc.component.represent.ExtractsMassPrint.Model;
import ru.tandemservice.uni.util.ReportRenderer;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.Map;

public class DOOrderListCommitUI extends UIPresenter {

    @Override
    public void onComponentRefresh()
    {
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        Map<String, Object> settingMap = _uiSettings.getAsMap(
                OrderCommitDSHandler.NUMBER_ORDER_FILTER,
                OrderCommitDSHandler.DATE_COMMIT_FROM_FILTER,
                OrderCommitDSHandler.DATE_FORMATIVE_FROM_FILTER,
                OrderCommitDSHandler.DATE_COMMIT_TO_FILTER,
                OrderCommitDSHandler.DATE_FORMATIVE_TO_FILTER,
                OrderCommitDSHandler.DATE_START_FROM_FILTER,
                OrderCommitDSHandler.DATE_START_TO_FILTER,
                "typesRepresentCommitFilter",
                "reasonsRepresentFilter",
                "formativeOrgUnitsFilter",
                "coursesFilter",
                "studentsFilter",
                "authorsFilter"
        );
        dataSource.putAll(settingMap);
    }

    @Override
    public void saveSettings() {

        UniBaseUtils.validateDatesPeriod(getSettings(), OrderCommitDSHandler.DATE_FORMATIVE_FROM_FILTER, OrderCommitDSHandler.DATE_FORMATIVE_TO_FILTER);
        UniBaseUtils.validateDatesPeriod(getSettings(), OrderCommitDSHandler.DATE_COMMIT_FROM_FILTER, OrderCommitDSHandler.DATE_COMMIT_TO_FILTER);
        UniBaseUtils.validateDatesPeriod(getSettings(), OrderCommitDSHandler.DATE_START_FROM_FILTER, OrderCommitDSHandler.DATE_START_TO_FILTER);

        super.saveSettings();
    }

    public void onClickPrintFromList() {

        try {
            IDocumentRenderer doc = documentRenderer();
            if (doc != null)
                BusinessComponentUtils.downloadDocument(doc, true);
        }
        catch (Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    private IDocumentRenderer documentRenderer() {

        ErrorCollector error = getConfig().getUserContext().getErrorCollector();

        DocumentOrder order = DataAccessServices.dao().get(getListenerParameterAsLong());

        if (order.getDocument() == null) {

            final RtfDocument docMain = OrderPrint.createPrintDocOrder(order, error, false);
            if (docMain != null) {
                return new ReportRenderer("Order(draft).rtf", docMain, false);
            }
            else

                return null;
        }
        else {

            return new ReportRenderer("Order.rtf", order.getDocument(), false);
        }
    }

    public void onClickMassPrint() {

        IUIDataSource dataSource = _uiConfig.getDataSource(DOOrderListCommit.ORDER_COMMIT_DS);

        ContextLocal.createDesktop("PersonShellDialog",
                                   new ComponentActivator(Model.class.getPackage().getName(),
                                                          new UniMap().add("ordersId", dataSource.getOutput().getRecordIds()).add("listOrder", false))
        );
    }
}
