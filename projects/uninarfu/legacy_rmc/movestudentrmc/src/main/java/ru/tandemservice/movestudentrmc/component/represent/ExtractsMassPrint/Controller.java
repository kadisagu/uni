package ru.tandemservice.movestudentrmc.component.represent.ExtractsMassPrint;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.rtf.RtfDocumentRenderer;
import org.tandemframework.rtf.document.RtfDocument;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {

        Model model = (Model) getModel(component);

        ((IDAO) getDao()).prepare(model);
    }

    public void onClickApply(IBusinessComponent component) {

        Model model = getModel(component);

        RtfDocument downloadPrintPersonalInfo = null;
        if (model.isListOrder())
            downloadPrintPersonalInfo = ((IDAO) getDao()).printListExtracts(model);
        else
            downloadPrintPersonalInfo = ((IDAO) getDao()).printIndividualExtracts(model);
        BusinessComponentUtils.downloadDocument(new RtfDocumentRenderer("Выписки.rtf", downloadPrintPersonalInfo), false);

        deactivate(component, 2);
    }
}
