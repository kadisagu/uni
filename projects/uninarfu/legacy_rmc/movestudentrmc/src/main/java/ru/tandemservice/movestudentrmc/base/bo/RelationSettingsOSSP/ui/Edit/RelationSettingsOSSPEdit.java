package ru.tandemservice.movestudentrmc.base.bo.RelationSettingsOSSP.ui.Edit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.movestudentrmc.base.bo.RelationSettingsOSSP.logic.GrantsOSSPDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.RelationSettingsOSSP.logic.PgoOSSPDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.RelationSettingsOSSP.logic.ReasonDSHandler;

@Configuration
public class RelationSettingsOSSPEdit extends BusinessComponentManager {

    public static final String REASON_DS = "reasonDS";
    public static final String GRANTS_OSSP_DS = "grantsOSSPDS";
    public static final String CPGO_OSSP_DS = "pgoOSSPDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(REASON_DS, reasonDSHandler()))
                .addDataSource(selectDS(GRANTS_OSSP_DS, grantsOSSPDSHandler()))
                .addDataSource(selectDS(CPGO_OSSP_DS, pgoOSSPDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler reasonDSHandler()
    {
        return (IDefaultComboDataSourceHandler) new ReasonDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler grantsOSSPDSHandler()
    {
        return (IDefaultComboDataSourceHandler) new GrantsOSSPDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler pgoOSSPDSHandler()
    {
        return (IDefaultComboDataSourceHandler) new PgoOSSPDSHandler(getName());
    }
}

