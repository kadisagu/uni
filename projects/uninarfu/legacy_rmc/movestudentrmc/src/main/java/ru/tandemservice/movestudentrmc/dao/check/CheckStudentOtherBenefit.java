package ru.tandemservice.movestudentrmc.dao.check;

import org.tandemframework.shared.person.base.entity.PersonBenefit;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CheckStudentOtherBenefit extends AbstractCheckStudentBenefit {

    private static final List<String> benefitCodes = Collections
            .unmodifiableList(Arrays.asList("4", "5", "6", "10", "11", "14"));

    @Override
    protected boolean checkCondition(Student student) {
        Long personId = student.getPerson().getId();

        if (!personBenefitMap.containsKey(personId))
            return false;

        List<PersonBenefit> benefits = personBenefitMap.get(personId);
        for (PersonBenefit personBenefit : benefits)
            if (benefitCodes.contains(personBenefit.getBenefit().getCode()))
                return true;

        return false;
    }

    @Override
    protected String createError(Student student, boolean value) {
        StringBuilder sb = new StringBuilder()
                .append("Студент ")
                .append(student.getPerson().getFullFio())
                .append(" ")
                .append(value ? "не " : "")
                .append("имеет одну из льгот:")
                .append("«лица из числа детей-сирот и детей, оставшихся без попечения родителей», ")
                .append("«лица, пострадавшие в результате аварии на Чернобыльской АЭС и других радиационных катастроф», ")
                .append("«лица, являющиеся инвалидами и ветеранами боевых действий», ")
                .append("«лица, признанные в установленном порядке инвалидами I и II групп», ")
                .append("«студенты, являющиеся членами малоимущих семей».");
        return sb.toString();
    }
}
