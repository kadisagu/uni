package ru.tandemservice.movestudentrmc.dao.check;

import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.employee.Student;

public class CheckBachelorAndSpecialty extends AbstractCheckDAO {

    @Override
    protected boolean getCondition(Student student) {
        return student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getQualification().getCode().equals(QualificationsCodes.SPETSIALIST) ||
                student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getQualification().getCode().equals(QualificationsCodes.BAKALAVR);
    }

    @Override
    protected String getErrorMessage(Student student, boolean value) {
        return new StringBuilder()
                .append("Студент ")
                .append(student.getPerson().getFullFio())
                .append(" ")
                .append(value ? "не " : "")
                .append("обучается по программам бакалавриата или специалитета.").toString();
    }

}
