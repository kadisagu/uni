package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudentrmc.entity.DocumentOrder;
import ru.tandemservice.movestudentrmc.entity.ListOrder;
import ru.tandemservice.movestudentrmc.entity.RepresentChangeQualificationTheme;
import ru.tandemservice.movestudentrmc.entity.Representation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * О внесении изменений в приказ об утверждении тем ВКР
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RepresentChangeQualificationThemeGen extends Representation
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.RepresentChangeQualificationTheme";
    public static final String ENTITY_NAME = "representChangeQualificationTheme";
    public static final int VERSION_HASH = -394107659;
    private static IEntityMeta ENTITY_META;

    public static final String L_LIST_ORDER = "listOrder";
    public static final String L_DOCUMENT_ORDER = "documentOrder";
    public static final String P_NEW_THEME = "newTheme";
    public static final String P_NEW_ADVISOR = "newAdvisor";
    public static final String P_ACCEPT_ORG_UNIT_TYPE = "acceptOrgUnitType";
    public static final String L_ACCEPT_ORG_UNIT = "acceptOrgUnit";
    public static final String P_TYPE_PRINT_TITLE = "typePrintTitle";

    private ListOrder _listOrder;     // Приказ о назначении темы ВКР
    private DocumentOrder _documentOrder;     // Приказ о внесении изменений в приказ о назначении темы ВКР
    private String _newTheme;     // Новая тема ВКР
    private String _newAdvisor;     // Новый научный руководитель
    private String _acceptOrgUnitType;     // Тип утверждающего подразделения
    private OrgUnit _acceptOrgUnit;     // Кафедра

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приказ о назначении темы ВКР.
     */
    public ListOrder getListOrder()
    {
        return _listOrder;
    }

    /**
     * @param listOrder Приказ о назначении темы ВКР.
     */
    public void setListOrder(ListOrder listOrder)
    {
        dirty(_listOrder, listOrder);
        _listOrder = listOrder;
    }

    /**
     * @return Приказ о внесении изменений в приказ о назначении темы ВКР.
     */
    public DocumentOrder getDocumentOrder()
    {
        return _documentOrder;
    }

    /**
     * @param documentOrder Приказ о внесении изменений в приказ о назначении темы ВКР.
     */
    public void setDocumentOrder(DocumentOrder documentOrder)
    {
        dirty(_documentOrder, documentOrder);
        _documentOrder = documentOrder;
    }

    /**
     * @return Новая тема ВКР.
     */
    public String getNewTheme()
    {
        return _newTheme;
    }

    /**
     * @param newTheme Новая тема ВКР.
     */
    public void setNewTheme(String newTheme)
    {
        dirty(_newTheme, newTheme);
        _newTheme = newTheme;
    }

    /**
     * @return Новый научный руководитель.
     */
    public String getNewAdvisor()
    {
        return _newAdvisor;
    }

    /**
     * @param newAdvisor Новый научный руководитель.
     */
    public void setNewAdvisor(String newAdvisor)
    {
        dirty(_newAdvisor, newAdvisor);
        _newAdvisor = newAdvisor;
    }

    /**
     * @return Тип утверждающего подразделения.
     */
    @Length(max=255)
    public String getAcceptOrgUnitType()
    {
        return _acceptOrgUnitType;
    }

    /**
     * @param acceptOrgUnitType Тип утверждающего подразделения.
     */
    public void setAcceptOrgUnitType(String acceptOrgUnitType)
    {
        dirty(_acceptOrgUnitType, acceptOrgUnitType);
        _acceptOrgUnitType = acceptOrgUnitType;
    }

    /**
     * @return Кафедра.
     */
    public OrgUnit getAcceptOrgUnit()
    {
        return _acceptOrgUnit;
    }

    /**
     * @param acceptOrgUnit Кафедра.
     */
    public void setAcceptOrgUnit(OrgUnit acceptOrgUnit)
    {
        dirty(_acceptOrgUnit, acceptOrgUnit);
        _acceptOrgUnit = acceptOrgUnit;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof RepresentChangeQualificationThemeGen)
        {
            setListOrder(((RepresentChangeQualificationTheme)another).getListOrder());
            setDocumentOrder(((RepresentChangeQualificationTheme)another).getDocumentOrder());
            setNewTheme(((RepresentChangeQualificationTheme)another).getNewTheme());
            setNewAdvisor(((RepresentChangeQualificationTheme)another).getNewAdvisor());
            setAcceptOrgUnitType(((RepresentChangeQualificationTheme)another).getAcceptOrgUnitType());
            setAcceptOrgUnit(((RepresentChangeQualificationTheme)another).getAcceptOrgUnit());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RepresentChangeQualificationThemeGen> extends Representation.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RepresentChangeQualificationTheme.class;
        }

        public T newInstance()
        {
            return (T) new RepresentChangeQualificationTheme();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "listOrder":
                    return obj.getListOrder();
                case "documentOrder":
                    return obj.getDocumentOrder();
                case "newTheme":
                    return obj.getNewTheme();
                case "newAdvisor":
                    return obj.getNewAdvisor();
                case "acceptOrgUnitType":
                    return obj.getAcceptOrgUnitType();
                case "acceptOrgUnit":
                    return obj.getAcceptOrgUnit();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "listOrder":
                    obj.setListOrder((ListOrder) value);
                    return;
                case "documentOrder":
                    obj.setDocumentOrder((DocumentOrder) value);
                    return;
                case "newTheme":
                    obj.setNewTheme((String) value);
                    return;
                case "newAdvisor":
                    obj.setNewAdvisor((String) value);
                    return;
                case "acceptOrgUnitType":
                    obj.setAcceptOrgUnitType((String) value);
                    return;
                case "acceptOrgUnit":
                    obj.setAcceptOrgUnit((OrgUnit) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "listOrder":
                        return true;
                case "documentOrder":
                        return true;
                case "newTheme":
                        return true;
                case "newAdvisor":
                        return true;
                case "acceptOrgUnitType":
                        return true;
                case "acceptOrgUnit":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "listOrder":
                    return true;
                case "documentOrder":
                    return true;
                case "newTheme":
                    return true;
                case "newAdvisor":
                    return true;
                case "acceptOrgUnitType":
                    return true;
                case "acceptOrgUnit":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "listOrder":
                    return ListOrder.class;
                case "documentOrder":
                    return DocumentOrder.class;
                case "newTheme":
                    return String.class;
                case "newAdvisor":
                    return String.class;
                case "acceptOrgUnitType":
                    return String.class;
                case "acceptOrgUnit":
                    return OrgUnit.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RepresentChangeQualificationTheme> _dslPath = new Path<RepresentChangeQualificationTheme>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RepresentChangeQualificationTheme");
    }
            

    /**
     * @return Приказ о назначении темы ВКР.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentChangeQualificationTheme#getListOrder()
     */
    public static ListOrder.Path<ListOrder> listOrder()
    {
        return _dslPath.listOrder();
    }

    /**
     * @return Приказ о внесении изменений в приказ о назначении темы ВКР.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentChangeQualificationTheme#getDocumentOrder()
     */
    public static DocumentOrder.Path<DocumentOrder> documentOrder()
    {
        return _dslPath.documentOrder();
    }

    /**
     * @return Новая тема ВКР.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentChangeQualificationTheme#getNewTheme()
     */
    public static PropertyPath<String> newTheme()
    {
        return _dslPath.newTheme();
    }

    /**
     * @return Новый научный руководитель.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentChangeQualificationTheme#getNewAdvisor()
     */
    public static PropertyPath<String> newAdvisor()
    {
        return _dslPath.newAdvisor();
    }

    /**
     * @return Тип утверждающего подразделения.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentChangeQualificationTheme#getAcceptOrgUnitType()
     */
    public static PropertyPath<String> acceptOrgUnitType()
    {
        return _dslPath.acceptOrgUnitType();
    }

    /**
     * @return Кафедра.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentChangeQualificationTheme#getAcceptOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> acceptOrgUnit()
    {
        return _dslPath.acceptOrgUnit();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.RepresentChangeQualificationTheme#getTypePrintTitle()
     */
    public static SupportedPropertyPath<String> typePrintTitle()
    {
        return _dslPath.typePrintTitle();
    }

    public static class Path<E extends RepresentChangeQualificationTheme> extends Representation.Path<E>
    {
        private ListOrder.Path<ListOrder> _listOrder;
        private DocumentOrder.Path<DocumentOrder> _documentOrder;
        private PropertyPath<String> _newTheme;
        private PropertyPath<String> _newAdvisor;
        private PropertyPath<String> _acceptOrgUnitType;
        private OrgUnit.Path<OrgUnit> _acceptOrgUnit;
        private SupportedPropertyPath<String> _typePrintTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приказ о назначении темы ВКР.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentChangeQualificationTheme#getListOrder()
     */
        public ListOrder.Path<ListOrder> listOrder()
        {
            if(_listOrder == null )
                _listOrder = new ListOrder.Path<ListOrder>(L_LIST_ORDER, this);
            return _listOrder;
        }

    /**
     * @return Приказ о внесении изменений в приказ о назначении темы ВКР.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentChangeQualificationTheme#getDocumentOrder()
     */
        public DocumentOrder.Path<DocumentOrder> documentOrder()
        {
            if(_documentOrder == null )
                _documentOrder = new DocumentOrder.Path<DocumentOrder>(L_DOCUMENT_ORDER, this);
            return _documentOrder;
        }

    /**
     * @return Новая тема ВКР.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentChangeQualificationTheme#getNewTheme()
     */
        public PropertyPath<String> newTheme()
        {
            if(_newTheme == null )
                _newTheme = new PropertyPath<String>(RepresentChangeQualificationThemeGen.P_NEW_THEME, this);
            return _newTheme;
        }

    /**
     * @return Новый научный руководитель.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentChangeQualificationTheme#getNewAdvisor()
     */
        public PropertyPath<String> newAdvisor()
        {
            if(_newAdvisor == null )
                _newAdvisor = new PropertyPath<String>(RepresentChangeQualificationThemeGen.P_NEW_ADVISOR, this);
            return _newAdvisor;
        }

    /**
     * @return Тип утверждающего подразделения.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentChangeQualificationTheme#getAcceptOrgUnitType()
     */
        public PropertyPath<String> acceptOrgUnitType()
        {
            if(_acceptOrgUnitType == null )
                _acceptOrgUnitType = new PropertyPath<String>(RepresentChangeQualificationThemeGen.P_ACCEPT_ORG_UNIT_TYPE, this);
            return _acceptOrgUnitType;
        }

    /**
     * @return Кафедра.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentChangeQualificationTheme#getAcceptOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> acceptOrgUnit()
        {
            if(_acceptOrgUnit == null )
                _acceptOrgUnit = new OrgUnit.Path<OrgUnit>(L_ACCEPT_ORG_UNIT, this);
            return _acceptOrgUnit;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.RepresentChangeQualificationTheme#getTypePrintTitle()
     */
        public SupportedPropertyPath<String> typePrintTitle()
        {
            if(_typePrintTitle == null )
                _typePrintTitle = new SupportedPropertyPath<String>(RepresentChangeQualificationThemeGen.P_TYPE_PRINT_TITLE, this);
            return _typePrintTitle;
        }

        public Class getEntityClass()
        {
            return RepresentChangeQualificationTheme.class;
        }

        public String getEntityName()
        {
            return "representChangeQualificationTheme";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTypePrintTitle();
}
