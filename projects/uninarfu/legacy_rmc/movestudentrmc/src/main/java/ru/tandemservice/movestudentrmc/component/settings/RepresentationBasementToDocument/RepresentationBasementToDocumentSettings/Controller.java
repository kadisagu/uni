package ru.tandemservice.movestudentrmc.component.settings.RepresentationBasementToDocument.RepresentationBasementToDocumentSettings;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.BooleanColumn;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.component.settings.abstractRelation.AbstractRelationList.AbstractRelationListController;
import ru.tandemservice.uni.component.settings.abstractRelation.AbstractRelationList.AbstractRelationListModel;
import ru.tandemservice.uni.component.settings.abstractRelation.AbstractRelationList.IAbstractRelationListDAO;
import ru.tandemservice.uni.dao.IListDataSourceDao;

import java.util.HashMap;
import java.util.Map;

public class Controller extends AbstractRelationListController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        //super.onRefreshComponent(component);
        AbstractRelationListModel model = (AbstractRelationListModel) getModel(component);

        ((IAbstractRelationListDAO) getDao()).prepare(model);

        prepareListDataSource(component);

    }

    private void prepareListDataSource(IBusinessComponent component) {
        AbstractRelationListModel model = (AbstractRelationListModel) getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource dataSource = UniUtils.createDataSource(component, (IListDataSourceDao) getDao());
        PublisherLinkColumn column = new PublisherLinkColumn("Название", "first.title");
        column.setOrderable(true);
        IPublisherLinkResolver resolver = new IPublisherLinkResolver() {
            public Object getParameters(IEntity entity)
            {
                Map params = new HashMap();
                params.put("firstId", entity.getId());
                return params;
            }

            public String getComponentName(IEntity entity)
            {
                return getRelationPubComponent();
            }
        };
        column.setResolver(resolver);
        dataSource.addColumn(column);

        dataSource.addColumn(new BooleanColumn("Настроено", "configured"));
        model.setDataSource(dataSource);
    }

    @Override
    protected String getRelationPubComponent() {
        return "ru.tandemservice.movestudentrmc.component.settings.RepresentationBasementToDocument.RepresentationBasementToDocumentSettingsPub";
    }

}
