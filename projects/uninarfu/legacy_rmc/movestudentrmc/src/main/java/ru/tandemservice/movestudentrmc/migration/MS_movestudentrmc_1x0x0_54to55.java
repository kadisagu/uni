package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;
import ru.tandemservice.movestudentrmc.entity.PersonNARFU;

import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_movestudentrmc_1x0x0_54to55 extends IndependentMigrationScript {
    //RM#4960
    private static final Long LONG_ZERO = new Long(0L);

    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.13"),
                        new ScriptDependency("org.tandemframework.shared", "1.3.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.3.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность personNARFU
        IEntityMeta meta = EntityRuntime.getMeta(PersonNARFU.class);

        if (tool.tableExists(meta.getTableName())) {
            // создано свойство resident
            if (!tool.columnExists(meta.getTableName(), "resident_p"))
                // создать колонку
                tool.createColumn(meta.getTableName(), new DBColumn("resident_p", DBType.BOOLEAN));

            short code = EntityRuntime.getMeta(PersonNARFU.class).getEntityCode();

            Statement stmt = tool.getConnection().createStatement();

            stmt.execute("select p.id, c.code_p, p_ext.id from person_t p " +
                                 "left join personnarfu_t p_ext on p.id = p_ext.person_id " +
                                 "inner join identitycard_t ic on p.identitycard_id = ic.id " +
                                 "left join addresscountry_t c on ic.citizenship_id = c.id");

            ResultSet rs = stmt.getResultSet();

            while (rs.next()) {
                Long personID = rs.getLong(1);
                int citizenshipCode = rs.getInt(2);
                Long personExtID = rs.getLong(3);

                if (citizenshipCode == 0) {
                    //если у персоны нет расширения - создаем
                    if (personExtID.equals(LONG_ZERO)) {
                        tool.executeUpdate("insert into " + meta.getTableName() + " (id, discriminator, person_id, needSocialPayment_p, needDormitoryOnReceiptPeriod_p, needDormitoryOnStudingPeriod_p, RESIDENT_P) " +
                                                   "values (?,?,?,?,?,?,?)",
                                           EntityIDGenerator.generateNewId(code), code, personID, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE);
                    }
                    else {
                        tool.executeUpdate("update " + meta.getTableName() + " set resident_p = ? where id = ?", Boolean.TRUE, personExtID);
                    }
                }
                else {
                    //если у персоны нет расширения - создаем
                    if (personExtID.equals(LONG_ZERO)) {
                        tool.executeUpdate("insert into " + meta.getTableName() + " (id, discriminator, person_id, needSocialPayment_p, needDormitoryOnReceiptPeriod_p, needDormitoryOnStudingPeriod_p, RESIDENT_P) " +
                                                   "values (?,?,?,?,?,?,?)",
                                           EntityIDGenerator.generateNewId(code), code, personID, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE);
                    }
                    else {
                        tool.executeUpdate("update " + meta.getTableName() + " set resident_p = ? where id = ?", Boolean.FALSE, personExtID);
                    }
                }
            }
        }


    }

}