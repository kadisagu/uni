package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import ru.tandemservice.movestudentrmc.entity.DocOrdRepresent;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.DocumentOrder;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class CancelOrderDSHandler extends DefaultComboDataSourceHandler {

    public static final String CANCEL_ORDER_DS = "cancelOrderDS";
    public static final String TYPE_REPRESENT = "typeRepresent";
    public static final String STUDENT_ID = "studentId";

    public CancelOrderDSHandler(String ownerId) {

        super(ownerId, DocumentOrder.class, DocumentOrder.number());
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        String filter = ep.input.getComboFilterByValue();
        RepresentationType type = ep.context.get(TYPE_REPRESENT);
        Long studentId = ep.context.get(STUDENT_ID);

        ep.dqlBuilder.joinEntity("e", DQLJoinType.inner, DocOrdRepresent.class, "r",
                                 eq(property(DocOrdRepresent.order().id().fromAlias("r")),
                                    property(DocumentOrder.id().fromAlias("e"))));

        ep.dqlBuilder.joinEntity("r", DQLJoinType.inner, DocRepresentStudentBase.class, "s",
                                 eq(property(DocRepresentStudentBase.representation().fromAlias("s")),
                                    property(DocOrdRepresent.representation().fromAlias("r"))));

        ep.dqlBuilder.where(eq(property(DocOrdRepresent.representation().type().fromAlias("r")), value(type)));
        ep.dqlBuilder.where(eq(DQLExpressions.property(DocRepresentStudentBase.student().id().fromAlias("s")), value(studentId)));

        if (StringUtils.isNotBlank(filter)) {
            ep.dqlBuilder.where(like(DQLFunctions.upper(DQLExpressions.property(RepresentationReason.priority().fromAlias("e"))), value(CoreStringUtils.escapeLike(filter, true))));
        }
    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
    {
        ep.dqlBuilder.order("e." + RepresentationReason.priority());
    }
}
