package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonBasic;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationBasement;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность-связь 'Тип представления'-'Причина'-'Основание'
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RelRepresentationReasonBasicGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonBasic";
    public static final String ENTITY_NAME = "relRepresentationReasonBasic";
    public static final int VERSION_HASH = -1433272493;
    private static IEntityMeta ENTITY_META;

    public static final String L_TYPE = "type";
    public static final String L_REASON = "reason";
    public static final String L_BASIC = "basic";
    public static final String P_REQUIRED = "required";
    public static final String P_JOIN_GROUP = "joinGroup";
    public static final String P_GROUP_NAME = "groupName";

    private RepresentationType _type;     // Тип Представления
    private RepresentationReason _reason;     // Причина
    private RepresentationBasement _basic;     // Основание
    private boolean _required = false;     // Обязательное основание
    private boolean _joinGroup = false;     // Входит в группу
    private String _groupName;     // Имя группы

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип Представления. Свойство не может быть null.
     */
    @NotNull
    public RepresentationType getType()
    {
        return _type;
    }

    /**
     * @param type Тип Представления. Свойство не может быть null.
     */
    public void setType(RepresentationType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Причина. Свойство не может быть null.
     */
    @NotNull
    public RepresentationReason getReason()
    {
        return _reason;
    }

    /**
     * @param reason Причина. Свойство не может быть null.
     */
    public void setReason(RepresentationReason reason)
    {
        dirty(_reason, reason);
        _reason = reason;
    }

    /**
     * @return Основание. Свойство не может быть null.
     */
    @NotNull
    public RepresentationBasement getBasic()
    {
        return _basic;
    }

    /**
     * @param basic Основание. Свойство не может быть null.
     */
    public void setBasic(RepresentationBasement basic)
    {
        dirty(_basic, basic);
        _basic = basic;
    }

    /**
     * @return Обязательное основание. Свойство не может быть null.
     */
    @NotNull
    public boolean isRequired()
    {
        return _required;
    }

    /**
     * @param required Обязательное основание. Свойство не может быть null.
     */
    public void setRequired(boolean required)
    {
        dirty(_required, required);
        _required = required;
    }

    /**
     * @return Входит в группу. Свойство не может быть null.
     */
    @NotNull
    public boolean isJoinGroup()
    {
        return _joinGroup;
    }

    /**
     * @param joinGroup Входит в группу. Свойство не может быть null.
     */
    public void setJoinGroup(boolean joinGroup)
    {
        dirty(_joinGroup, joinGroup);
        _joinGroup = joinGroup;
    }

    /**
     * @return Имя группы.
     */
    @Length(max=255)
    public String getGroupName()
    {
        return _groupName;
    }

    /**
     * @param groupName Имя группы.
     */
    public void setGroupName(String groupName)
    {
        dirty(_groupName, groupName);
        _groupName = groupName;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RelRepresentationReasonBasicGen)
        {
            setType(((RelRepresentationReasonBasic)another).getType());
            setReason(((RelRepresentationReasonBasic)another).getReason());
            setBasic(((RelRepresentationReasonBasic)another).getBasic());
            setRequired(((RelRepresentationReasonBasic)another).isRequired());
            setJoinGroup(((RelRepresentationReasonBasic)another).isJoinGroup());
            setGroupName(((RelRepresentationReasonBasic)another).getGroupName());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RelRepresentationReasonBasicGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RelRepresentationReasonBasic.class;
        }

        public T newInstance()
        {
            return (T) new RelRepresentationReasonBasic();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "type":
                    return obj.getType();
                case "reason":
                    return obj.getReason();
                case "basic":
                    return obj.getBasic();
                case "required":
                    return obj.isRequired();
                case "joinGroup":
                    return obj.isJoinGroup();
                case "groupName":
                    return obj.getGroupName();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "type":
                    obj.setType((RepresentationType) value);
                    return;
                case "reason":
                    obj.setReason((RepresentationReason) value);
                    return;
                case "basic":
                    obj.setBasic((RepresentationBasement) value);
                    return;
                case "required":
                    obj.setRequired((Boolean) value);
                    return;
                case "joinGroup":
                    obj.setJoinGroup((Boolean) value);
                    return;
                case "groupName":
                    obj.setGroupName((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "type":
                        return true;
                case "reason":
                        return true;
                case "basic":
                        return true;
                case "required":
                        return true;
                case "joinGroup":
                        return true;
                case "groupName":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "type":
                    return true;
                case "reason":
                    return true;
                case "basic":
                    return true;
                case "required":
                    return true;
                case "joinGroup":
                    return true;
                case "groupName":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "type":
                    return RepresentationType.class;
                case "reason":
                    return RepresentationReason.class;
                case "basic":
                    return RepresentationBasement.class;
                case "required":
                    return Boolean.class;
                case "joinGroup":
                    return Boolean.class;
                case "groupName":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RelRepresentationReasonBasic> _dslPath = new Path<RelRepresentationReasonBasic>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RelRepresentationReasonBasic");
    }
            

    /**
     * @return Тип Представления. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonBasic#getType()
     */
    public static RepresentationType.Path<RepresentationType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Причина. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonBasic#getReason()
     */
    public static RepresentationReason.Path<RepresentationReason> reason()
    {
        return _dslPath.reason();
    }

    /**
     * @return Основание. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonBasic#getBasic()
     */
    public static RepresentationBasement.Path<RepresentationBasement> basic()
    {
        return _dslPath.basic();
    }

    /**
     * @return Обязательное основание. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonBasic#isRequired()
     */
    public static PropertyPath<Boolean> required()
    {
        return _dslPath.required();
    }

    /**
     * @return Входит в группу. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonBasic#isJoinGroup()
     */
    public static PropertyPath<Boolean> joinGroup()
    {
        return _dslPath.joinGroup();
    }

    /**
     * @return Имя группы.
     * @see ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonBasic#getGroupName()
     */
    public static PropertyPath<String> groupName()
    {
        return _dslPath.groupName();
    }

    public static class Path<E extends RelRepresentationReasonBasic> extends EntityPath<E>
    {
        private RepresentationType.Path<RepresentationType> _type;
        private RepresentationReason.Path<RepresentationReason> _reason;
        private RepresentationBasement.Path<RepresentationBasement> _basic;
        private PropertyPath<Boolean> _required;
        private PropertyPath<Boolean> _joinGroup;
        private PropertyPath<String> _groupName;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип Представления. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonBasic#getType()
     */
        public RepresentationType.Path<RepresentationType> type()
        {
            if(_type == null )
                _type = new RepresentationType.Path<RepresentationType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Причина. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonBasic#getReason()
     */
        public RepresentationReason.Path<RepresentationReason> reason()
        {
            if(_reason == null )
                _reason = new RepresentationReason.Path<RepresentationReason>(L_REASON, this);
            return _reason;
        }

    /**
     * @return Основание. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonBasic#getBasic()
     */
        public RepresentationBasement.Path<RepresentationBasement> basic()
        {
            if(_basic == null )
                _basic = new RepresentationBasement.Path<RepresentationBasement>(L_BASIC, this);
            return _basic;
        }

    /**
     * @return Обязательное основание. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonBasic#isRequired()
     */
        public PropertyPath<Boolean> required()
        {
            if(_required == null )
                _required = new PropertyPath<Boolean>(RelRepresentationReasonBasicGen.P_REQUIRED, this);
            return _required;
        }

    /**
     * @return Входит в группу. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonBasic#isJoinGroup()
     */
        public PropertyPath<Boolean> joinGroup()
        {
            if(_joinGroup == null )
                _joinGroup = new PropertyPath<Boolean>(RelRepresentationReasonBasicGen.P_JOIN_GROUP, this);
            return _joinGroup;
        }

    /**
     * @return Имя группы.
     * @see ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonBasic#getGroupName()
     */
        public PropertyPath<String> groupName()
        {
            if(_groupName == null )
                _groupName = new PropertyPath<String>(RelRepresentationReasonBasicGen.P_GROUP_NAME, this);
            return _groupName;
        }

        public Class getEntityClass()
        {
            return RelRepresentationReasonBasic.class;
        }

        public String getEntityName()
        {
            return "relRepresentationReasonBasic";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
