package ru.tandemservice.movestudentrmc.base.bo.GrantsRelationSettings.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreStringUtils;
import ru.tandemservice.movestudentrmc.entity.catalog.Grant;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class GrantDSHandler extends DefaultComboDataSourceHandler {

    public GrantDSHandler(String ownerId) {

        super(ownerId, Grant.class, Grant.P_TITLE);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        String filter = ep.input.getComboFilterByValue();
        if (StringUtils.isNotBlank(filter)) {
            ep.dqlBuilder.where(likeUpper(property(Grant.title().fromAlias("e")), value(CoreStringUtils.escapeLike(filter, true))));
        }
    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
    {
        ep.dqlBuilder.order("e." + Grant.title());
    }
}
