package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;

public class MS_movestudentrmc_1x0x0_4to5 extends IndependentMigrationScript {
    @Override
    public void run(DBTool tool) throws Exception {

        if (tool.tableExists("relrepresentationreasonbasic_t"))
            tool.executeUpdate("delete from relrepresentationreasonbasic_t");


        if (tool.tableExists("representationreason_t"))
            tool.executeUpdate("delete from representationreason_t");
    }
}
