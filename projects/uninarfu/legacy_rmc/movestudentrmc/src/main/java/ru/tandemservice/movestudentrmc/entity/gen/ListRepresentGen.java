package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.sec.entity.gen.IPrincipalContextGen;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudentrmc.entity.IAbstractRepresentation;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.catalog.MovestudentExtractStates;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationBasement;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Списочное представление
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ListRepresentGen extends EntityBase
 implements IAbstractRepresentation{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.ListRepresent";
    public static final String ENTITY_NAME = "listRepresent";
    public static final int VERSION_HASH = 1866639687;
    private static IEntityMeta ENTITY_META;

    public static final String P_REPRESENT_I_D = "representID";
    public static final String L_TYPE = "type";
    public static final String P_TITLE = "title";
    public static final String L_STATE = "state";
    public static final String P_COMMITTED = "committed";
    public static final String P_CREATE_DATE = "createDate";
    public static final String P_START_DATE = "startDate";
    public static final String L_REPRESENTATION_TYPE = "representationType";
    public static final String L_REPRESENTATION_REASON = "representationReason";
    public static final String L_REPRESENTATION_BASEMENT = "representationBasement";
    public static final String P_DOCUMENT = "document";
    public static final String L_OPERATOR = "operator";
    public static final String L_CREATOR = "creator";
    public static final String P_CHECK = "check";

    private Long _representID;     // ID представления
    private RepresentationType _type;     // Тип представления
    private String _title;     // Представление
    private MovestudentExtractStates _state;     // Cостояние представления
    private boolean _committed;     // Проведено
    private Date _createDate;     // Дата формирования
    private Date _startDate;     // Дата вступления в силу
    private RepresentationType _representationType;     // Типы представлений
    private RepresentationReason _representationReason;     // Причины представлений
    private RepresentationBasement _representationBasement;     // Основания представлений
    private byte[] _document;     // Текст представления
    private Person _operator;     // Персона
    private IPrincipalContext _creator;     // Создатель представления
    private Boolean _check;     // Проверка

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ID представления.
     *
     * Это формула "id".
     */
    public Long getRepresentID()
    {
        return _representID;
    }

    /**
     * @param representID ID представления.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setRepresentID(Long representID)
    {
        dirty(_representID, representID);
        _representID = representID;
    }

    /**
     * @return Тип представления.
     *
     * Это формула "representationType".
     */
    public RepresentationType getType()
    {
        return _type;
    }

    /**
     * @param type Тип представления.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setType(RepresentationType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Представление.
     */
    @Length(max=1024)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Представление.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Cостояние представления.
     */
    public MovestudentExtractStates getState()
    {
        return _state;
    }

    /**
     * @param state Cостояние представления.
     */
    public void setState(MovestudentExtractStates state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * @return Проведено. Свойство не может быть null.
     */
    @NotNull
    public boolean isCommitted()
    {
        return _committed;
    }

    /**
     * @param committed Проведено. Свойство не может быть null.
     */
    public void setCommitted(boolean committed)
    {
        dirty(_committed, committed);
        _committed = committed;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getCreateDate()
    {
        return _createDate;
    }

    /**
     * @param createDate Дата формирования. Свойство не может быть null.
     */
    public void setCreateDate(Date createDate)
    {
        dirty(_createDate, createDate);
        _createDate = createDate;
    }

    /**
     * @return Дата вступления в силу.
     */
    public Date getStartDate()
    {
        return _startDate;
    }

    /**
     * @param startDate Дата вступления в силу.
     */
    public void setStartDate(Date startDate)
    {
        dirty(_startDate, startDate);
        _startDate = startDate;
    }

    /**
     * @return Типы представлений. Свойство не может быть null.
     */
    @NotNull
    public RepresentationType getRepresentationType()
    {
        return _representationType;
    }

    /**
     * @param representationType Типы представлений. Свойство не может быть null.
     */
    public void setRepresentationType(RepresentationType representationType)
    {
        dirty(_representationType, representationType);
        _representationType = representationType;
    }

    /**
     * @return Причины представлений. Свойство не может быть null.
     */
    @NotNull
    public RepresentationReason getRepresentationReason()
    {
        return _representationReason;
    }

    /**
     * @param representationReason Причины представлений. Свойство не может быть null.
     */
    public void setRepresentationReason(RepresentationReason representationReason)
    {
        dirty(_representationReason, representationReason);
        _representationReason = representationReason;
    }

    /**
     * @return Основания представлений. Свойство не может быть null.
     */
    @NotNull
    public RepresentationBasement getRepresentationBasement()
    {
        return _representationBasement;
    }

    /**
     * @param representationBasement Основания представлений. Свойство не может быть null.
     */
    public void setRepresentationBasement(RepresentationBasement representationBasement)
    {
        dirty(_representationBasement, representationBasement);
        _representationBasement = representationBasement;
    }

    /**
     * @return Текст представления.
     */
    public byte[] getDocument()
    {
        return _document;
    }

    /**
     * @param document Текст представления.
     */
    public void setDocument(byte[] document)
    {
        dirty(_document, document);
        _document = document;
    }

    /**
     * @return Персона.
     */
    public Person getOperator()
    {
        return _operator;
    }

    /**
     * @param operator Персона.
     */
    public void setOperator(Person operator)
    {
        dirty(_operator, operator);
        _operator = operator;
    }

    /**
     * @return Создатель представления.
     */
    public IPrincipalContext getCreator()
    {
        return _creator;
    }

    /**
     * @param creator Создатель представления.
     */
    public void setCreator(IPrincipalContext creator)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && creator!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IPrincipalContext.class);
            IEntityMeta actual =  creator instanceof IEntity ? EntityRuntime.getMeta((IEntity) creator) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_creator, creator);
        _creator = creator;
    }

    /**
     * @return Проверка.
     */
    public Boolean getCheck()
    {
        return _check;
    }

    /**
     * @param check Проверка.
     */
    public void setCheck(Boolean check)
    {
        dirty(_check, check);
        _check = check;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ListRepresentGen)
        {
            setRepresentID(((ListRepresent)another).getRepresentID());
            setType(((ListRepresent)another).getType());
            setTitle(((ListRepresent)another).getTitle());
            setState(((ListRepresent)another).getState());
            setCommitted(((ListRepresent)another).isCommitted());
            setCreateDate(((ListRepresent)another).getCreateDate());
            setStartDate(((ListRepresent)another).getStartDate());
            setRepresentationType(((ListRepresent)another).getRepresentationType());
            setRepresentationReason(((ListRepresent)another).getRepresentationReason());
            setRepresentationBasement(((ListRepresent)another).getRepresentationBasement());
            setDocument(((ListRepresent)another).getDocument());
            setOperator(((ListRepresent)another).getOperator());
            setCreator(((ListRepresent)another).getCreator());
            setCheck(((ListRepresent)another).getCheck());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ListRepresentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ListRepresent.class;
        }

        public T newInstance()
        {
            return (T) new ListRepresent();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "representID":
                    return obj.getRepresentID();
                case "type":
                    return obj.getType();
                case "title":
                    return obj.getTitle();
                case "state":
                    return obj.getState();
                case "committed":
                    return obj.isCommitted();
                case "createDate":
                    return obj.getCreateDate();
                case "startDate":
                    return obj.getStartDate();
                case "representationType":
                    return obj.getRepresentationType();
                case "representationReason":
                    return obj.getRepresentationReason();
                case "representationBasement":
                    return obj.getRepresentationBasement();
                case "document":
                    return obj.getDocument();
                case "operator":
                    return obj.getOperator();
                case "creator":
                    return obj.getCreator();
                case "check":
                    return obj.getCheck();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "representID":
                    obj.setRepresentID((Long) value);
                    return;
                case "type":
                    obj.setType((RepresentationType) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "state":
                    obj.setState((MovestudentExtractStates) value);
                    return;
                case "committed":
                    obj.setCommitted((Boolean) value);
                    return;
                case "createDate":
                    obj.setCreateDate((Date) value);
                    return;
                case "startDate":
                    obj.setStartDate((Date) value);
                    return;
                case "representationType":
                    obj.setRepresentationType((RepresentationType) value);
                    return;
                case "representationReason":
                    obj.setRepresentationReason((RepresentationReason) value);
                    return;
                case "representationBasement":
                    obj.setRepresentationBasement((RepresentationBasement) value);
                    return;
                case "document":
                    obj.setDocument((byte[]) value);
                    return;
                case "operator":
                    obj.setOperator((Person) value);
                    return;
                case "creator":
                    obj.setCreator((IPrincipalContext) value);
                    return;
                case "check":
                    obj.setCheck((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "representID":
                        return true;
                case "type":
                        return true;
                case "title":
                        return true;
                case "state":
                        return true;
                case "committed":
                        return true;
                case "createDate":
                        return true;
                case "startDate":
                        return true;
                case "representationType":
                        return true;
                case "representationReason":
                        return true;
                case "representationBasement":
                        return true;
                case "document":
                        return true;
                case "operator":
                        return true;
                case "creator":
                        return true;
                case "check":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "representID":
                    return true;
                case "type":
                    return true;
                case "title":
                    return true;
                case "state":
                    return true;
                case "committed":
                    return true;
                case "createDate":
                    return true;
                case "startDate":
                    return true;
                case "representationType":
                    return true;
                case "representationReason":
                    return true;
                case "representationBasement":
                    return true;
                case "document":
                    return true;
                case "operator":
                    return true;
                case "creator":
                    return true;
                case "check":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "representID":
                    return Long.class;
                case "type":
                    return RepresentationType.class;
                case "title":
                    return String.class;
                case "state":
                    return MovestudentExtractStates.class;
                case "committed":
                    return Boolean.class;
                case "createDate":
                    return Date.class;
                case "startDate":
                    return Date.class;
                case "representationType":
                    return RepresentationType.class;
                case "representationReason":
                    return RepresentationReason.class;
                case "representationBasement":
                    return RepresentationBasement.class;
                case "document":
                    return byte[].class;
                case "operator":
                    return Person.class;
                case "creator":
                    return IPrincipalContext.class;
                case "check":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ListRepresent> _dslPath = new Path<ListRepresent>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ListRepresent");
    }
            

    /**
     * @return ID представления.
     *
     * Это формула "id".
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresent#getRepresentID()
     */
    public static PropertyPath<Long> representID()
    {
        return _dslPath.representID();
    }

    /**
     * @return Тип представления.
     *
     * Это формула "representationType".
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresent#getType()
     */
    public static RepresentationType.Path<RepresentationType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Представление.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresent#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Cостояние представления.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresent#getState()
     */
    public static MovestudentExtractStates.Path<MovestudentExtractStates> state()
    {
        return _dslPath.state();
    }

    /**
     * @return Проведено. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresent#isCommitted()
     */
    public static PropertyPath<Boolean> committed()
    {
        return _dslPath.committed();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresent#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    /**
     * @return Дата вступления в силу.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresent#getStartDate()
     */
    public static PropertyPath<Date> startDate()
    {
        return _dslPath.startDate();
    }

    /**
     * @return Типы представлений. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresent#getRepresentationType()
     */
    public static RepresentationType.Path<RepresentationType> representationType()
    {
        return _dslPath.representationType();
    }

    /**
     * @return Причины представлений. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresent#getRepresentationReason()
     */
    public static RepresentationReason.Path<RepresentationReason> representationReason()
    {
        return _dslPath.representationReason();
    }

    /**
     * @return Основания представлений. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresent#getRepresentationBasement()
     */
    public static RepresentationBasement.Path<RepresentationBasement> representationBasement()
    {
        return _dslPath.representationBasement();
    }

    /**
     * @return Текст представления.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresent#getDocument()
     */
    public static PropertyPath<byte[]> document()
    {
        return _dslPath.document();
    }

    /**
     * @return Персона.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresent#getOperator()
     */
    public static Person.Path<Person> operator()
    {
        return _dslPath.operator();
    }

    /**
     * @return Создатель представления.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresent#getCreator()
     */
    public static IPrincipalContextGen.Path<IPrincipalContext> creator()
    {
        return _dslPath.creator();
    }

    /**
     * @return Проверка.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresent#getCheck()
     */
    public static PropertyPath<Boolean> check()
    {
        return _dslPath.check();
    }

    public static class Path<E extends ListRepresent> extends EntityPath<E>
    {
        private PropertyPath<Long> _representID;
        private RepresentationType.Path<RepresentationType> _type;
        private PropertyPath<String> _title;
        private MovestudentExtractStates.Path<MovestudentExtractStates> _state;
        private PropertyPath<Boolean> _committed;
        private PropertyPath<Date> _createDate;
        private PropertyPath<Date> _startDate;
        private RepresentationType.Path<RepresentationType> _representationType;
        private RepresentationReason.Path<RepresentationReason> _representationReason;
        private RepresentationBasement.Path<RepresentationBasement> _representationBasement;
        private PropertyPath<byte[]> _document;
        private Person.Path<Person> _operator;
        private IPrincipalContextGen.Path<IPrincipalContext> _creator;
        private PropertyPath<Boolean> _check;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ID представления.
     *
     * Это формула "id".
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresent#getRepresentID()
     */
        public PropertyPath<Long> representID()
        {
            if(_representID == null )
                _representID = new PropertyPath<Long>(ListRepresentGen.P_REPRESENT_I_D, this);
            return _representID;
        }

    /**
     * @return Тип представления.
     *
     * Это формула "representationType".
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresent#getType()
     */
        public RepresentationType.Path<RepresentationType> type()
        {
            if(_type == null )
                _type = new RepresentationType.Path<RepresentationType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Представление.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresent#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(ListRepresentGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Cостояние представления.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresent#getState()
     */
        public MovestudentExtractStates.Path<MovestudentExtractStates> state()
        {
            if(_state == null )
                _state = new MovestudentExtractStates.Path<MovestudentExtractStates>(L_STATE, this);
            return _state;
        }

    /**
     * @return Проведено. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresent#isCommitted()
     */
        public PropertyPath<Boolean> committed()
        {
            if(_committed == null )
                _committed = new PropertyPath<Boolean>(ListRepresentGen.P_COMMITTED, this);
            return _committed;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresent#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(ListRepresentGen.P_CREATE_DATE, this);
            return _createDate;
        }

    /**
     * @return Дата вступления в силу.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresent#getStartDate()
     */
        public PropertyPath<Date> startDate()
        {
            if(_startDate == null )
                _startDate = new PropertyPath<Date>(ListRepresentGen.P_START_DATE, this);
            return _startDate;
        }

    /**
     * @return Типы представлений. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresent#getRepresentationType()
     */
        public RepresentationType.Path<RepresentationType> representationType()
        {
            if(_representationType == null )
                _representationType = new RepresentationType.Path<RepresentationType>(L_REPRESENTATION_TYPE, this);
            return _representationType;
        }

    /**
     * @return Причины представлений. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresent#getRepresentationReason()
     */
        public RepresentationReason.Path<RepresentationReason> representationReason()
        {
            if(_representationReason == null )
                _representationReason = new RepresentationReason.Path<RepresentationReason>(L_REPRESENTATION_REASON, this);
            return _representationReason;
        }

    /**
     * @return Основания представлений. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresent#getRepresentationBasement()
     */
        public RepresentationBasement.Path<RepresentationBasement> representationBasement()
        {
            if(_representationBasement == null )
                _representationBasement = new RepresentationBasement.Path<RepresentationBasement>(L_REPRESENTATION_BASEMENT, this);
            return _representationBasement;
        }

    /**
     * @return Текст представления.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresent#getDocument()
     */
        public PropertyPath<byte[]> document()
        {
            if(_document == null )
                _document = new PropertyPath<byte[]>(ListRepresentGen.P_DOCUMENT, this);
            return _document;
        }

    /**
     * @return Персона.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresent#getOperator()
     */
        public Person.Path<Person> operator()
        {
            if(_operator == null )
                _operator = new Person.Path<Person>(L_OPERATOR, this);
            return _operator;
        }

    /**
     * @return Создатель представления.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresent#getCreator()
     */
        public IPrincipalContextGen.Path<IPrincipalContext> creator()
        {
            if(_creator == null )
                _creator = new IPrincipalContextGen.Path<IPrincipalContext>(L_CREATOR, this);
            return _creator;
        }

    /**
     * @return Проверка.
     * @see ru.tandemservice.movestudentrmc.entity.ListRepresent#getCheck()
     */
        public PropertyPath<Boolean> check()
        {
            if(_check == null )
                _check = new PropertyPath<Boolean>(ListRepresentGen.P_CHECK, this);
            return _check;
        }

        public Class getEntityClass()
        {
            return ListRepresent.class;
        }

        public String getEntityName()
        {
            return "listRepresent";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
