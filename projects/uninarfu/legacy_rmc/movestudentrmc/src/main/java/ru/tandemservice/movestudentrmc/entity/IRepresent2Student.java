package ru.tandemservice.movestudentrmc.entity;

import ru.tandemservice.uni.entity.employee.Student;

public interface IRepresent2Student {

    public Student getStudent();

    public IAbstractRepresentation getRepresentation();
}
