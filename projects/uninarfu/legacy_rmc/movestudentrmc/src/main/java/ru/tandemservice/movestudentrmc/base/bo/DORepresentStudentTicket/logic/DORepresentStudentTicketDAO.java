package ru.tandemservice.movestudentrmc.base.bo.DORepresentStudentTicket.logic;


import org.apache.commons.lang.StringUtils;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentDAO;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.RepresentStudentTicket;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class DORepresentStudentTicketDAO extends AbstractDORepresentDAO implements IDORepresentStudentTicketDAO {

    @Override
    public Representation createRepresentObject(RepresentationType type, List<Student> student) {
        RepresentStudentTicket represent = new RepresentStudentTicket();
        represent.setType(type);
        return represent;
    }

    @Override
    public void buildBodyExtract(Representation representationBase, String itemFac, RtfDocument docCommon) {
        RepresentStudentTicket represent = (RepresentStudentTicket) representationBase;

        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        RtfInjectModifier modifier = new RtfInjectModifier();

        UtilPrintSupport.setParNum(docCommon, itemFac);

        UtilPrintSupport.injectModifierExtract(modifier, docRepresent);

        injectModifier(modifier, represent, docRepresent.getStudent());

        modifier.modify(docCommon);

    }

    @Override
    public void buildBodyRepresent(Representation representationBase, RtfDocument document, AtomicInteger parNumber, AtomicInteger extractNumber) {
        super.buildBodyRepresent(representationBase, document, parNumber, extractNumber);
        RepresentStudentTicket represent = (RepresentStudentTicket) representationBase;

        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        Student student = UtilPrintSupport.getStudent(represent);
        RtfInjectModifier modifier = new RtfInjectModifier();

        UtilPrintSupport.injectModifierRepresent(modifier, docRepresent, student);

        injectModifier(modifier, represent, student);

        if (StringUtils.isEmpty(modifier.getStringValue("basics")))
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Arrays.asList("basics"), false, false);

        modifier.modify(document);
    }

    protected void injectModifier(RtfInjectModifier modifier, RepresentStudentTicket represent, Student student)
    {
        modifier.put("studentTitle", UtilPrintSupport.printSeparatedFIO(student.getPerson().getIdentityCard(), GrammaCase.DATIVE, student.getPerson().isMale()));
        modifier.put("studentSex_D", student.getPerson().isMale() ? "студенту" : "студентке");
        modifier.put("giveCopy", represent.isReprimand() ? "Объявить выговор и выдать дубликат" : "Выдать дубликат");
        modifier.put("documentType", represent.getDocumentType().getText());
    }

    @Override
    public GrammaCase getGrammaCase() {
        return GrammaCase.DATIVE;
    }
}
