package ru.tandemservice.movestudentrmc.base.bo.DORepresentGrantResume.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentDAO;
import ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab.MonthWrapper;
import ru.tandemservice.movestudentrmc.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.movestudentrmc.entity.catalog.StuGrantStatus;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentGrantTypeCodes;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.StuGrantStatusCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class DORepresentGrantResumeDAO extends AbstractDORepresentDAO implements IDORepresentGrantResumeDAO {

    @Override
    public Representation createRepresentObject(RepresentationType type,
                                                List<Student> student)
    {
        RepresentGrantResume represent = new RepresentGrantResume();
        represent.setType(type);
        return represent;
    }

    @Override
    public GrammaCase getGrammaCase() {
        return GrammaCase.DATIVE;
    }

    @Override
    public void buildBodyExtract(Representation representationBase,
                                 String itemFac, RtfDocument docCommon)
    {
        RepresentGrantResume represent = (RepresentGrantResume) representationBase;
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        RtfInjectModifier modifier = new RtfInjectModifier();

        UtilPrintSupport.setParNum(docCommon, itemFac);

        UtilPrintSupport.injectModifierExtract(modifier, docRepresent);

        injectModifier(modifier, represent, docRepresent.getStudent());

        modifier.modify(docCommon);

    }

    @Override
    public void buildBodyRepresent(Representation representationBase,
                                   RtfDocument document, AtomicInteger parNumber, AtomicInteger extractNumber)
    {
        super.buildBodyRepresent(representationBase, document, parNumber, extractNumber);
        RepresentGrantResume represent = (RepresentGrantResume) representationBase;
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        Student student = UtilPrintSupport.getStudent(represent);
        RtfInjectModifier modifier = new RtfInjectModifier();

        UtilPrintSupport.injectModifierRepresent(modifier, docRepresent, student);

        injectModifier(modifier, represent, student);

        if (StringUtils.isEmpty(modifier.getStringValue("basics")))
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Arrays.asList("basics"), false, false);

        modifier.modify(document);

    }

    protected void injectModifier(RtfInjectModifier modifier, RepresentGrantResume represent, Student student) {

        modifier.put("studentTitle", UtilPrintSupport.printSeparatedFIO(student.getPerson().getIdentityCard(), GrammaCase.DATIVE, student.getPerson().isMale()));
        modifier.put("reason", represent.getReason().getTitle());
        modifier.put("studentSex_D", student.getPerson().isMale() ? "студенту" : "студентке");
        modifier.put("dateResumePayment", UtilPrintSupport.getDateFormatterWithMonthString(represent.getDateResumePayment()));
        modifier.put("grantView", represent.getGrantView().getGenitive());
    }

    @Override
    public boolean doCommit(Representation represent, ErrorCollector error) {
        if (!super.doCommit(represent, error))
            return false;

        RepresentGrantResume representGrantResume = (RepresentGrantResume) represent;

        StuGrantStatus status = UniDaoFacade.getCoreDao().get(StuGrantStatus.class, StuGrantStatus.code(), StuGrantStatusCodes.CODE_1);

        DocumentOrder order = getOrder(represent);
        List<DocRepresentStudentBase> studentList = getStudentList(represent);
        Student student = studentList.get(0).getStudent();

        //-------------------------------------------------------------------------------------------------------------------------------
        /*
        Calendar lastYearDate = Calendar.getInstance();
        lastYearDate.set(representGrantResume.getEducationYear().getIntValue() + 1, Calendar.AUGUST, 31, 0, 0, 0);//31.08.year+1
        List<String> monthList = MonthWrapper.getMonthsList(representGrantResume.getDateResumePayment(), lastYearDate.getTime(), representGrantResume.getEducationYear());
        */
        
        //Последний учебный год назначения выплат
        DQLSelectBuilder lastYearBuilder = new DQLSelectBuilder()
        		.fromEntity(StudentGrantEntity.class, "sge")
        		.where(eq(property(StudentGrantEntity.status().code().fromAlias("sge")), value(StuGrantStatusCodes.CODE_2)))
        		.where(eq(property(StudentGrantEntity.student().fromAlias("sge")), value(student)))
        		.where(eq(property(StudentGrantEntity.view().fromAlias("sge")), value(representGrantResume.getGrantView())))
        		.column(property(StudentGrantEntity.eduYear().intValue().fromAlias("sge")))
        		.order(property(StudentGrantEntity.eduYear().intValue().fromAlias("sge")), OrderDirection.desc)
        		.top(1);

        int lastYear = lastYearBuilder.createStatement(getSession()).uniqueResult();
        
        //Последний день последнего учебного года назначения выплат
        Calendar lastYearDate = Calendar.getInstance();
        lastYearDate.set(lastYear + 1, Calendar.AUGUST, 31, 0, 0, 0);	//31.08.year+1
        List<String> monthList = MonthWrapper.getMonthsList(representGrantResume.getDateResumePayment(), lastYearDate.getTime());         
        //-------------------------------------------------------------------------------------------------------------------------------

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentGrantEntity.class, "sge")
                .where(DQLExpressions.eq(DQLExpressions.property(StudentGrantEntity.view().fromAlias("sge")),
                                         DQLExpressions.value(representGrantResume.getGrantView())))
                .where(DQLExpressions.eq(DQLExpressions.property(StudentGrantEntity.status().code().fromAlias("sge")),
                                         DQLExpressions.value(StuGrantStatusCodes.CODE_2)))
                .where(DQLExpressions.eq(DQLExpressions.property(StudentGrantEntity.student().id().fromAlias("sge")),
                                         DQLExpressions.value(student.getId())))
                .where(DQLExpressions.in(DQLExpressions.property(StudentGrantEntity.month().fromAlias("sge")), monthList))
                .where(DQLExpressions.or(DQLExpressions.eq(DQLExpressions.property(StudentGrantEntity.tmp().fromAlias("sge")),
                                                           DQLExpressions.value(Boolean.FALSE)),
                                         DQLExpressions.isNull(DQLExpressions.property(StudentGrantEntity.tmp().fromAlias("sge")))));

        List<StudentGrantEntity> sgelist = UniDaoFacade.getCoreDao().getList(builder);

        for (StudentGrantEntity sge : sgelist) {
            //сохраним старые данные для отката
            RepresentGrantsOld grantsOld = new RepresentGrantsOld();
            grantsOld.setRepresentation(represent);
            grantsOld.setStudentGrantEntityId(sge.getId());
            grantsOld.setOrderDateOld(sge.getOrderDate());
            grantsOld.setOrderNumberOld(sge.getOrderNumber());
            getSession().saveOrUpdate(grantsOld);
            //сохраним данные приказа
            sge.setStatus(status);
            sge.setOrderDate(order.getCommitDate());
            sge.setOrderNumber(order.getNumber());
            getSession().saveOrUpdate(sge);
        }

        MoveStudentDaoFacade.getGrantDAO().saveGrantHistory(sgelist, represent, RepresentGrantTypeCodes.RESUME);

        return true;
    }

    @Override
    public boolean doRollback(Representation represent, ErrorCollector error) {
        if (!super.doRollback(represent, error))
            return false;

        StuGrantStatus status = UniDaoFacade.getCoreDao().get(StuGrantStatus.class, StuGrantStatus.code(), StuGrantStatusCodes.CODE_2);

        List<RepresentGrantsOld> grantsOlds = UniDaoFacade.getCoreDao().getList(RepresentGrantsOld.class, RepresentGrantsOld.representation(), represent);

        for (RepresentGrantsOld grantOld : grantsOlds) {
            StudentGrantEntity sge = UniDaoFacade.getCoreDao().get(grantOld.getStudentGrantEntityId());

            sge.setStatus(status);
            sge.setOrderDate(grantOld.getOrderDateOld());
            sge.setOrderNumber(grantOld.getOrderNumberOld());

            getSession().saveOrUpdate(sge);

            getSession().delete(grantOld);
        }

        MoveStudentDaoFacade.getGrantDAO().deleteGrantHistory(represent);

        return true;
    }

}
