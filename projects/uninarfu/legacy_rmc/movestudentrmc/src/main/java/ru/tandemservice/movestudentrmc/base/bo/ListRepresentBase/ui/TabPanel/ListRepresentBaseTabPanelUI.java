package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ui.TabPanel;

import org.tandemframework.caf.ui.UIPresenter;

public class ListRepresentBaseTabPanelUI extends UIPresenter {

    private String selectedTab;

    public String getSelectedTab() {
        return selectedTab;
    }

    public void setSelectedTab(String selectedTab) {
        this.selectedTab = selectedTab;
    }

}
