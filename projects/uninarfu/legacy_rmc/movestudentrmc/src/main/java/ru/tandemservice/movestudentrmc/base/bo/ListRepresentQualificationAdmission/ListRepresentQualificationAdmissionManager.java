package ru.tandemservice.movestudentrmc.base.bo.ListRepresentQualificationAdmission;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentQualificationAdmission.logic.ListRepresentQualificationAdmissionManagerModifyDAO;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentQualificationAdmission.ui.Edit.ListRepresentQualificationAdmissionEdit;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentQualificationAdmission.ui.View.ListRepresentQualificationAdmissionView;
import ru.tandemservice.movestudentrmc.docord.IListObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IListObjectModifyDAO;


@Configuration
public class ListRepresentQualificationAdmissionManager extends BusinessObjectManager implements IListObjectByRepresentTypeManager
{

    public static ListRepresentQualificationAdmissionManager instance()
    {
        return instance(ListRepresentQualificationAdmissionManager.class);
    }


    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return ListRepresentQualificationAdmissionEdit.class;
    }


    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return ListRepresentQualificationAdmissionView.class;
    }


    @Bean
    @Override
    public IListObjectModifyDAO getListObjectModifyDAO() {
        return new ListRepresentQualificationAdmissionManagerModifyDAO();
    }
}
