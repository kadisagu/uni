package ru.tandemservice.movestudentrmc.entity;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.movestudentrmc.entity.gen.ListRepresentGrantCancelGen;

/**
 * Списочное представление о назначении стиендии
 */
public class ListRepresentGrantCancel extends ListRepresentGrantCancelGen {

    @Override
    public String getTitle() {
        String shortTitle = getOrgUnitShortTitle();
        return new StringBuilder()
                .append(getRepresentationType().getTitle())
                .append(" (")
                .append(getGrantView().getShortTitle())
                .append(") с ")
                .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateCancelPayment()))
                .append((getOperator() != null ? ", " + getOperator().getFullFio() : ""))
                .append((shortTitle.isEmpty() ? "" : ", " + shortTitle))
                .append(", от ")
                .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getCreateDate()))
                .toString();
    }

    @Override
    public String getRepresentationTitle() {
        String shortTitle = getOrgUnitShortTitle();
        return new StringBuilder()
                .append(getRepresentationType().getTitle())
                .append(" (")
                .append(getGrantView().getShortTitle())
                .append(") с ")
                .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateCancelPayment()))
                .append((shortTitle.isEmpty() ? "" : ", " + shortTitle))
                .toString();
    }
}