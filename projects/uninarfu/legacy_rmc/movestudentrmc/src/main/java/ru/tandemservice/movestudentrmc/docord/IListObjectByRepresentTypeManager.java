package ru.tandemservice.movestudentrmc.docord;

import org.tandemframework.caf.ui.config.BusinessComponentManager;

public interface IListObjectByRepresentTypeManager {

    Class<? extends BusinessComponentManager> getEditComponentManager();

    IListObjectModifyDAO getListObjectModifyDAO();

    Class<? extends BusinessComponentManager> getViewComponentManager();
}
