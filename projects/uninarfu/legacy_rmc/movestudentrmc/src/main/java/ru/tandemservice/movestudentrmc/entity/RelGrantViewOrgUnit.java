package ru.tandemservice.movestudentrmc.entity;

import org.tandemframework.shared.commonbase.dao.IEntityRelation;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import ru.tandemservice.movestudentrmc.entity.gen.RelGrantViewOrgUnitGen;

/**
 * Сущность-связь Вид стипендии/выплаты – институт
 */
public class RelGrantViewOrgUnit extends RelGrantViewOrgUnitGen implements IEntityRelation<GrantView, OrgUnit> {
}