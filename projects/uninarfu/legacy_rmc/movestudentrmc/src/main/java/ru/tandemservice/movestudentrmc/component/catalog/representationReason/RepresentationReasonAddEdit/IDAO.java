package ru.tandemservice.movestudentrmc.component.catalog.representationReason.RepresentationReasonAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;

public interface IDAO extends IDefaultCatalogAddEditDAO<RepresentationReason, Model> {

}
