package ru.tandemservice.movestudentrmc.base.bo.RelationSettings.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.movestudentrmc.base.bo.RelationSettings.logic.BasicDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.RelationSettings.logic.ReasonDSHandler;

@Configuration
public class RelationSettingsAddEdit extends BusinessComponentManager {

    public static final String REASON_DS = "reasonDS";
    public static final String BASIC_DS = "basicDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(REASON_DS, reasonDSHandler()))
                .addDataSource(selectDS(BASIC_DS, basicDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler reasonDSHandler()
    {
        return (IDefaultComboDataSourceHandler) new ReasonDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler basicDSHandler()
    {
        return (IDefaultComboDataSourceHandler) new BasicDSHandler(getName());
    }
}
