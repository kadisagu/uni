package ru.tandemservice.movestudentrmc.entity;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.movestudentrmc.entity.gen.RepresentGrantSuspendGen;

/**
 * О приостановлении стипендии/выплаты
 */
public class RepresentGrantSuspend extends RepresentGrantSuspendGen
{
    @Override
    public String getTitle() {
        return new StringBuilder()
                .append(getType().getTitle())
                .append(" (")
                .append(getGrantView().getShortTitle())
                .append(") с ")
                .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateSuspendPayment()))
                //---------------------------------------------------------------------
                //.append(", от ")
                //.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getCreateDate()))
                //---------------------------------------------------------------------
                .toString();
    }

    @Override
    public String getRepresentationTitle() {
        return new StringBuilder()
                .append(getType().getTitle())
                .append(" (")
                .append(getGrantView().getShortTitle())
                .append(") с ")
                .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateSuspendPayment()))
                .toString();
    }
}