package ru.tandemservice.movestudentrmc.component.catalog.osspGrantsView.OsspGrantsViewPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;
import ru.tandemservice.movestudentrmc.entity.catalog.OsspGrantsView;

public interface IDAO extends IDefaultCatalogPubDAO<OsspGrantsView, Model> {
    void updatePrintable(Model model, Long id);

}
