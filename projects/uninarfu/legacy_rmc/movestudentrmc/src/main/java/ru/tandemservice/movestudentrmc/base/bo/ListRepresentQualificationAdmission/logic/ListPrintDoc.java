package ru.tandemservice.movestudentrmc.base.bo.ListRepresentQualificationAdmission.logic;

import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.AbstractListPrintDoc;
import ru.tandemservice.movestudentrmc.docord.IListPrintDoc;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListRepresentQualificationAdmission;
import ru.tandemservice.movestudentrmc.entity.ListRepresentQualificationThemes;
import ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.*;

public class ListPrintDoc extends AbstractListPrintDoc<ListRepresentQualificationAdmission> implements IListPrintDoc
{

    private Map<Student, OrderParagraphInfo> hashMap = new HashMap<>();

    public final String TEMPLATE_HEADER = "list.rep.qualification.admission.header";
    public final String TEMPLATE_PARAG = "list.rep.qualification.admission.parag";

    public RtfDocument creationPrintDocOrder(List<? extends ListRepresent> listOfRepresents)
    {
        //HERE
        PrintTemplate printTemplate = UniDaoFacade.getCoreDao().getCatalogItem(PrintTemplate.class, getTemplateHeader());
        RtfDocument templateHeader = new RtfReader().read(printTemplate.getContent());

        printTemplate = UniDaoFacade.getCoreDao().getCatalogItem(PrintTemplate.class, getTemplateParag());
        RtfDocument templateParag = new RtfReader().read(printTemplate.getContent());

        Map<OrgUnit, Map<String, List<Object[]>>> dataMap = getStudentData(listOfRepresents);
        //сортировка формирующих по алфавиту
        List<OrgUnit> orgUnitList = new ArrayList<>(dataMap.keySet());
        Collections.sort(orgUnitList, new EntityComparator<>(new EntityOrder(OrgUnit.title())));

        RtfDocument result = templateHeader.getClone();
        result.setSettings(templateParag.getSettings());
        IRtfElement paragTemp = UniRtfUtil.findElement(result.getElementList(), "parag");
        new RtfInjectModifier().put("representTitle", listOfRepresents.get(0).getRepresentationType().getTitle()).modify(result);

        RtfDocument res = RtfBean.getElementFactory().createRtfDocument();
        res.setHeader(templateParag.getHeader());
        res.setSettings(templateParag.getSettings());
        res.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.TAB));

        int formCounter = 0;
        int addonCounter = 0;

        for (OrgUnit formOU : orgUnitList)
        {
            Map<String, List<Object[]>> formMap = dataMap.get(formOU);

            formCounter++;
            res.getElementList().add(RtfBean.getElementFactory().createRtfText(String.valueOf(formCounter).trim() + "."));
            res.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.TAB));
            insertRow(res, "По " + formOU.getDativeCaseTitle());

            int counter = 0;
            for (List<Object[]> studentList : formMap.values())
            {
                counter++;
                addonCounter++;
                RtfDocument par = getParagraph(templateParag, formCounter, counter, addonCounter, studentList);
                res.getElementList().addAll(par.getElementList());
            }
        }

        //основания
        //------------------------------------------------------------------------------------
        //res.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
        //insertRow(res, "Основание: " + UtilPrintSupport.getBasementTitles(listOfRepresents));
        //------------------------------------------------------------------------------------
        result.getElementList().addAll(result.getElementList().indexOf(paragTemp), res.getElementList());

        // result.getElementList().add( RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE) );

        List<RtfDocument> addonList = new ArrayList<>();
        for (int i = 0; i < addonList.size(); i++)
        {
            result.getElementList().addAll(addonList.get(i).getElementList());
            if (i + 1 < addonList.size())
                result.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
        }
        SharedRtfUtil.removeParagraphsWithTagsRecursive(result, Collections.singletonList("parag"), false, false);
        return result;
    }

    @Override
    protected String getTemplateHeader()
    {
        return TEMPLATE_HEADER;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    protected String getTemplateParag()
    {
        return TEMPLATE_PARAG;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    protected String getTemplateAddon()
    {
        return null;
    }

    @Override
    protected void paragraphInjectModifier(RtfInjectModifier im, ListRepresentQualificationAdmission listRepresent, Student student)
    {
        injectModifier(im, listRepresent, student);
    }

    @Override
    protected RtfDocument getParagraph(RtfDocument template, int formCounter, int counter, int addonCounter, List<Object[]> studentList)
    {
        RtfDocument result = template.getClone();
        ListRepresentQualificationAdmission listRepresent = (ListRepresentQualificationAdmission) studentList.get(0)[1];

        Set<Long> listRepresentIds = new HashSet<>();
        
        //----------------------------------------------------------------------------
        List<ListRepresentQualificationAdmission> studentRepresentList = new ArrayList();
        //----------------------------------------------------------------------------
        
        List<Student> students = new ArrayList<>();
        Student student = (Student) studentList.get(0)[0];
        for (Object[] o : studentList)
        {
            Student s = (Student) o[0];
            students.add(s);
            listRepresentIds.add(((ListRepresentQualificationAdmission) o[1]).getId());
            //----------------------------------------------------------------------------
            if(!studentRepresentList.contains((ListRepresentQualificationAdmission)o[1])){
            	studentRepresentList.add((ListRepresentQualificationAdmission)o[1]);
            }
            //----------------------------------------------------------------------------
            hashMap.put(s, new OrderParagraphInfo(formCounter, counter));
        }
        RtfInjectModifier im = new RtfInjectModifier();

        im.put("i", String.valueOf(formCounter).trim());
        im.put("j", String.valueOf(counter).trim());
        im.put("addonNumber", String.valueOf(addonCounter));

        paragraphInjectModifier(im, listRepresent, student);
        Collections.sort(students, new EntityComparator<>(new EntityOrder(Student.fio().s())));
        RtfString rtfString = new RtfString();
        for (Student stu : students)
        {
        	//---------------------------------------------------------------
        	hashMap.get(stu).setStudentNumber(students.indexOf(stu) + 1);
        	//---------------------------------------------------------------
            rtfString
                    .append(String.valueOf(students.indexOf(stu) + 1))
                    .append(") ")
                    .append(PersonManager.instance().declinationDao().getDeclinationFIO(stu.getPerson().getIdentityCard(), GrammaCase.ACCUSATIVE));
            if (!stu.getCompensationType().isBudget())
                rtfString.append(" (по договору)");
            if (students.indexOf(stu) != students.size() - 1)
                rtfString.append(IRtfData.PAR);
        }
        im.put("STUDENT_LIST", rtfString);
        //------------------------------------------------------------------------------------------------------
        //Вывод оснований для каждого пункта приказа
        im.put("basic", "Основание: " + UtilPrintSupport.getBasementTitles(studentRepresentList) + ".");
        //------------------------------------------------------------------------------------------------------
        
        im.modify(result);
        
        return result;
    }

    public static void injectModifier(RtfInjectModifier im, ListRepresentQualificationAdmission listRepresent, Student student)
    {
        String levelType = "специальность ";
        if (student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isMaster() ||
                student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isBachelor())
            levelType = "направление подготовки ";
        String code = UtilPrintSupport.getEduLevelCodeWithQualification(student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel());
        String levelTitle = UtilPrintSupport.getParentTitleIfProfile(student.getEducationOrgUnit().getEducationLevelHighSchool());
        im.put("qualificationExamType", listRepresent.getQualificationExamType() == null ? "" : listRepresent.getQualificationExamType().getTitle());
        //---------------------------------------------------------------------------------------------------------------
        if(listRepresent.getQualificationExamType() != null && !(listRepresent.getQualificationExamType().getCode().equals("4") || listRepresent.getQualificationExamType().getCode().equals("5"))) {
        	im.put("completedPlan", "как полностью выполнивших учебный план ");
        }
        else {
        	im.put("completedPlan", ""); 
        }
        //---------------------------------------------------------------------------------------------------------------
        im.put("eduLevelTitle", levelType + code + levelTitle);
        im.put("course", student.getCourse().getTitle());
        im.put("developForm", UtilPrintSupport.getDevelopFormGen(student.getEducationOrgUnit().getDevelopForm()));
    }

    @Override
    protected String getKey(Student student, ListRepresentQualificationAdmission represent)
    {
        return String.valueOf(student.getEducationOrgUnit().getDevelopForm().getId()) +
                student.getCourse().getId() +
                student.getEducationOrgUnit().getEducationLevelHighSchool().getId()
                //-------------------------------------------------------------------
                //Добавлен тип мероприятия ГИА
                + represent.getQualificationExamType().getId()
                //-------------------------------------------------------------------
                ;
    }


    @Override
    protected Class<? extends ListRepresent> getListRepresentClass()
    {
        return ListRepresentQualificationAdmission.class;
    }

    @Override
    public Map<Student, OrderParagraphInfo> getParagInfomap()
    {
        return hashMap;
    }
}
