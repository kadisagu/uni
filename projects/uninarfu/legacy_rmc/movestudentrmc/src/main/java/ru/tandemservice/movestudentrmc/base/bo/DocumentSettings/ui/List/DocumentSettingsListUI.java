package ru.tandemservice.movestudentrmc.base.bo.DocumentSettings.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import ru.tandemservice.movestudentrmc.base.bo.DocumentSettings.ui.AddEdit.DocumentSettingsAddEdit;
import ru.tandemservice.movestudentrmc.base.bo.DocumentSettings.ui.AddEdit.DocumentSettingsAddEditUI;


public class DocumentSettingsListUI extends UIPresenter {

    @Override
    public void onComponentRefresh() {
    }

    public void onEditEntityFromList() {
        _uiActivation.asRegion(DocumentSettingsAddEdit.class)
                .parameter(DocumentSettingsAddEditUI.TYPE_ID, getListenerParameterAsLong())
                .activate();

    }
}
