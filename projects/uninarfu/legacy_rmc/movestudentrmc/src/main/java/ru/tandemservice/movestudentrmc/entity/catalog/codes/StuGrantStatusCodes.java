package ru.tandemservice.movestudentrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Статус стипендии/выплаты"
 * Имя сущности : stuGrantStatus
 * Файл data.xml : movementstudent.data.xml
 */
public interface StuGrantStatusCodes
{
    /** Константа кода (code) элемента : 1 (code). Название (title) : Выплата */
    String CODE_1 = "1";
    /** Константа кода (code) элемента : 2 (code). Название (title) : Приостановлена */
    String CODE_2 = "2";
    /** Константа кода (code) элемента : 3 (code). Название (title) : Отменена */
    String CODE_3 = "3";

    Set<String> CODES = ImmutableSet.of(CODE_1, CODE_2, CODE_3);
}
