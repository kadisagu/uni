package ru.tandemservice.movestudentrmc.base.bo.DORepresentWeekend;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentWeekend.logic.DORepresentWeekendDAO;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentWeekend.ui.Edit.DORepresentWeekendEdit;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentWeekend.ui.View.DORepresentWeekendView;
import ru.tandemservice.movestudentrmc.docord.IDOObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IDOObjectModifyDAO;

@Configuration
public class DORepresentWeekendManager extends BusinessObjectManager implements IDOObjectByRepresentTypeManager {

    public static DORepresentWeekendManager instance()
    {
        return instance(DORepresentWeekendManager.class);
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return DORepresentWeekendEdit.class;
    }

    @Override
    @Bean
    public IDOObjectModifyDAO getDOObjectModifyDAO() {
        return new DORepresentWeekendDAO();
    }

    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return DORepresentWeekendView.class;
    }
}
