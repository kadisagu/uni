package ru.tandemservice.movestudentrmc.base.bo.DORepresentExclude.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfString;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentDAO;
import ru.tandemservice.movestudentrmc.docord.util.MoveStudentUtil;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class DORepresentExcludeDAO extends AbstractDORepresentDAO {

    @Override
    public Representation createRepresentObject(RepresentationType type, List<Student> studentList) {

        RepresentExclude represent = new RepresentExclude();
        represent.setType(type);
        return represent;
    }

    @Override
    public boolean doCommit(Representation represent, ErrorCollector error) {
        if (!super.doCommit(represent, error))
            return false;

        List<DocRepresentStudentBase> studentList = getStudentList(represent);

        Student student = studentList.get(0).getStudent();
        student.setStatus(DataAccessServices.dao().get(StudentStatus.class, StudentStatus.code().s(), UniDefines.CATALOG_STUDENT_STATUS_EXCLUDED));
        baseUpdate(student);

        List<StudentGrantEntity> sgeList = MoveStudentUtil.getSrudentGrantEntityList(Arrays.asList(student), represent.getStartDate());

        commitStudentGrantsCancel(represent, sgeList);
        return true;
    }

    @Override
    public boolean doRollback(Representation represent, ErrorCollector error) {
        if (!super.doRollback(represent, error))
            return false;

        List<DocRepresentStudentBase> studentList = getStudentList(represent);

        Student student = studentList.get(0).getStudent();
        student.setStatus(represent.getStatusOld());
        baseUpdate(student);

        rollbackStudentGrantsCancel(represent);

        return true;
    }

    @Override
    public void buildBodyExtract(Representation representationBase, String itemFac, RtfDocument docCommon) {
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        UtilPrintSupport.setParNum(docCommon, itemFac);

        RtfInjectModifier modifier = new RtfInjectModifier();

        UtilPrintSupport.injectModifierExtract(modifier, docRepresent);

        modifier.modify(docCommon);
    }

    @Override
    public void buildBodyRepresent(Representation representationBase, RtfDocument document, AtomicInteger parNumber, AtomicInteger extractNumber) {
        super.buildBodyRepresent(representationBase, document, parNumber, extractNumber);
        RepresentExclude represent = (RepresentExclude) representationBase;
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        Student student = UtilPrintSupport.getStudent(represent);

        RtfInjectModifier modifier = new RtfInjectModifier();

        UtilPrintSupport.injectModifierRepresent(modifier, docRepresent, student);

        //modifier.put("studentTitle", PersonManager.instance().declinationDao().getDeclinationFIO(student.getPerson().getIdentityCard(),getGrammaCase()));
        modifier.put("studentTitle", UtilPrintSupport.printSeparatedFIO(student.getPerson().getIdentityCard(), getGrammaCase(), student.getPerson().isMale()));

        modifier.put("studentSex", student.getPerson().isMale() ? "студента" : "студентку");

        String genderBasedDevelop = student.getPerson().isMale() ? "обучающегося" : "обучающуюся";
        modifier.put("developPayment", genderBasedDevelop + " " + docRepresent.getCompensationTypeStr());

        if (represent.getReason().getCode().equals("narfu-1.3") && represent.getEduInstitution() != null) {
            StringBuilder str = new StringBuilder("в связи с переводом в ")
                    .append(represent.getEduInstitution().getTitle());
            modifier.put("reason", str.toString());
            modifier.put("basics", getBasics(represent));
        }

        if (StringUtils.isEmpty(modifier.getStringValue("basics")))
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Arrays.asList("basics"), false, false);

        modifier.modify(document);
    }

    public RtfString getBasics(RepresentExclude represent) {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocRepresentBasics.class, "basic").column("basic");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(DocRepresentBasics.representation().fromAlias("basic")),
                DQLExpressions.value(represent.getId())));

        List<DocRepresentBasics> basicsList = DataAccessServices.dao().getList(builder);

        RtfString str = new RtfString();

        List<DocRepresentBasics> basicList = new ArrayList<DocRepresentBasics>();
        List<DocRepresentBasics> basicNotUnionList = new ArrayList<DocRepresentBasics>();

        for (DocRepresentBasics basics : basicsList) {
            if (basics.getBasic().isMerged()) {
                if (!UtilPrintSupport.findBasicList(basicList, basics))
                    basicList.add(basics);
            }
            if (!basics.getBasic().isMerged())
                if (!UtilPrintSupport.findBasicList(basicNotUnionList, basics))
                    basicNotUnionList.add(basics);
        }

        boolean isFirst = true;
        for (DocRepresentBasics basic : basicList) {
            if (isFirst)
                isFirst = false;
            else
                str.append(", ");

            if (basic.getBasic().isPrintable()) {
                UtilPrintSupport.getBasicTitle(str, basic);

                if (basic.getBasic().getCode().equals("24")) {
                    str.append(" ").append(represent.getEduInstitution().getTitle());
                }
                if (basic.getRepresentationBasementDate() != null || basic.getRepresentationBasementNumber() != null) {
                    str.append(" (");
                    if (basic.getRepresentationBasementNumber() != null) {
                        str.append("№").append(IRtfData.SYMBOL_TILDE);
                        str.append(basic.getRepresentationBasementNumber());
                        if (basic.getRepresentationBasementDate() != null) {
                            str.append(" ");
                        }
                    }
                    if (basic.getRepresentationBasementDate() != null) {
                        str.append("от ");
                        UtilPrintSupport.getDateFormatterWithMonthStringAndYear(basic.getRepresentationBasementDate(), str);
                    }
                    str.append(")");
                }
            }
        }
        for (DocRepresentBasics basic : basicNotUnionList) {
            if (isFirst)
                isFirst = false;
            else
                str.append(", ");

            if (basic.getBasic().isPrintable()) {
                UtilPrintSupport.getBasicTitle(str, basic);
                if (basic.getBasic().getCode().equals("24")) {
                    str.append(" ").append(represent.getEduInstitution().getTitle());
                }
                if (basic.getRepresentationBasementDate() != null || basic.getRepresentationBasementNumber() != null) {
                    str.append(" (");
                    if (basic.getRepresentationBasementNumber() != null) {
                        str.append("№").append(IRtfData.SYMBOL_TILDE);
                        str.append(basic.getRepresentationBasementNumber());
                        if (basic.getRepresentationBasementDate() != null) {
                            str.append(" ");
                        }
                    }
                    if (basic.getRepresentationBasementDate() != null) {
                        str.append("от ");
                        UtilPrintSupport.getDateFormatterWithMonthStringAndYear(basic.getRepresentationBasementDate(), str);
                    }
                    str.append(")");
                }
            }
        }

        return str;
    }

    @Override
    public void buildBodyDeclaration(Representation representationBase, RtfDocument document) {
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        RtfInjectModifier im = new RtfInjectModifier();
        RtfTableModifier tm = new RtfTableModifier();
        UtilPrintSupport.injectModifierDeclaration(im, tm, docRepresent);

        this.injectModifier(im, docRepresent);

        im.modify(document);
        tm.modify(document);
    }

}
