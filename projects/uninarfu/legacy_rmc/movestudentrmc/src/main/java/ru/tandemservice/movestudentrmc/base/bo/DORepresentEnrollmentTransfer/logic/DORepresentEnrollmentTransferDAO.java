package ru.tandemservice.movestudentrmc.base.bo.DORepresentEnrollmentTransfer.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.PersonForeignLanguage;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentDAO;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.DocOrdRepresent;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class DORepresentEnrollmentTransferDAO extends AbstractDORepresentDAO
{

    @Override
    public Representation createRepresentObject(RepresentationType type, List<Student> studentList) {

        Student student = studentList.get(0);
        IdentityCard ic = student.getPerson().getIdentityCard();
        RepresentEnrollmentTransfer represent = new RepresentEnrollmentTransfer();
        represent.setType(type);

        represent.setFirstName(ic.getFirstName());
        represent.setLastName(ic.getLastName());
        represent.setMiddleName(ic.getMiddleName());

        represent.setFirstNameOld(ic.getFirstName());
        represent.setLastNameOld(ic.getLastName());
        represent.setMiddleNameOld(ic.getMiddleName());

        represent.setFormativeOrgUnitOld(student.getEducationOrgUnit().getFormativeOrgUnit());
        represent.setEducationOrgUnitOld(student.getEducationOrgUnit());
        represent.setGroupOld(student.getGroup());
        represent.setCourseOld(student.getCourse());
        represent.setCompensationTypeOld(student.getCompensationType());

        return represent;
    }

    @Override
    public boolean doCommit(Representation represent, ErrorCollector error) {
        if (!super.doCommit(represent, error))
            return false;

        List<DocRepresentStudentBase> studentList = getStudentList(represent);
        Student student = studentList.get(0).getStudent();

        RepresentEnrollmentTransfer representET = (RepresentEnrollmentTransfer) represent;

        StudentStatus status = UniDaoFacade.getCoreDao().getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_ACTIVE);
        student.setStatus(status);

        student.setGroup(representET.getNewGroup());
        student.setCourse(representET.getNewCourse());

        student.setCompensationType(representET.getNewCompensationType());

        if (representET.getEducationOrgUnit() != null)
            student.setEducationOrgUnit(representET.getEducationOrgUnit());
        else
            student.setEducationOrgUnit(representET.getNewGroup().getEducationOrgUnit());

        IdentityCard ic = student.getPerson().getIdentityCard();

        ic.setFirstName(representET.getFirstName());
        ic.setLastName(representET.getLastName());
        ic.setMiddleName(representET.getMiddleName());

        baseUpdate(ic);

        baseUpdate(student);

        DocOrdRepresent ordRepresent = UniDaoFacade.getCoreDao().get(DocOrdRepresent.class, DocOrdRepresent.representation(), representET);
        OrderData data = UniDaoFacade.getCoreDao().get(OrderData.class, OrderData.student(), student);
        if (data == null) {
            //сохраняем данные в "Данные последних приказов" приказ о зачислении
            data = new OrderData();
            data.setStudent(student);
            data.setEduEnrollmentOrderDate(ordRepresent.getOrder().getCommitDate());
            data.setEduEnrollmentOrderNumber(ordRepresent.getOrder().getNumber());
            data.setEduEnrollmentOrderEnrDate(ordRepresent.getOrder().getCommitDate());
        }
        else {
            //если там есть данные, то сохраняем их для восстановления при отмене проведения приказа
            representET.setOrderDateOld(data.getEduEnrollmentOrderDate());
            representET.setOrderNumberOld(data.getEduEnrollmentOrderNumber());
            representET.setOrderDateEnrOld(data.getEduEnrollmentOrderEnrDate());
            baseUpdate(representET);
            //заполняем новыми данными
            data.setEduEnrollmentOrderDate(ordRepresent.getOrder().getCommitDate());
            data.setEduEnrollmentOrderNumber(ordRepresent.getOrder().getNumber());
            data.setEduEnrollmentOrderEnrDate(ordRepresent.getOrder().getCommitDate());
        }
        baseCreateOrUpdate(data);

        return true;
    }

    @Override
    public boolean doRollback(Representation represent, ErrorCollector error) {
        if (!super.doRollback(represent, error))
            return false;

        RepresentEnrollmentTransfer rep = (RepresentEnrollmentTransfer) represent;

        List<DocRepresentStudentBase> studentList = getStudentList(represent);
        Student student = studentList.get(0).getStudent();

        if (represent.getStatusOld() == null)
            error.add("Не найден старый статус студента");
        student.setStatus(rep.getStatusOld());

        Group group = null;
        if (rep.getGroupOld() != null) {
            group = ((RepresentEnrollmentTransfer) represent).getGroupOld();
            if (group == null) {
                error.add("Не найдена старая группа");
                return false;
            }
        }
        if (rep.getCourseOld() == null)
            error.add("Не найден старый курс");

        EducationOrgUnit edu = null;
        if (rep.getEducationOrgUnitOld() != null)
            edu = rep.getEducationOrgUnitOld();
        else
            error.add("Не найдены старые данные");

        if (!error.isHasFieldErrors()) {
            student.setStatus(represent.getStatusOld());
            student.setGroup(((RepresentEnrollmentTransfer) represent).getGroupOld());
            student.setCourse(((RepresentEnrollmentTransfer) represent).getCourseOld());
            student.setCompensationType(((RepresentEnrollmentTransfer) represent).getCompensationTypeOld());

            student.setEducationOrgUnit(edu);

            IdentityCard ic = student.getPerson().getIdentityCard();

            ic.setFirstName(((RepresentEnrollmentTransfer) represent).getFirstNameOld());
            ic.setLastName(((RepresentEnrollmentTransfer) represent).getLastNameOld());
            ic.setMiddleName(((RepresentEnrollmentTransfer) represent).getMiddleNameOld());

            baseUpdate(ic);

            baseUpdate(student);
            //возвращаем старые данные в "Даты послених приказов"
            OrderData data = UniDaoFacade.getCoreDao().get(OrderData.class, OrderData.student(), student);
            RepresentEnrollmentTransfer enrollmentTransfer = (RepresentEnrollmentTransfer) represent;
            data.setEduEnrollmentOrderDate(enrollmentTransfer.getOrderDateOld());
            data.setEduEnrollmentOrderEnrDate(enrollmentTransfer.getOrderDateEnrOld());
            data.setEduEnrollmentOrderNumber(enrollmentTransfer.getOrderNumberOld());
            baseUpdate(data);

        }
        else
            return false;


        return true;
    }

    @Override
    public void buildBodyExtract(Representation representationBase, String itemFac, RtfDocument docCommon) {

        RepresentEnrollmentTransfer represent = (RepresentEnrollmentTransfer) representationBase;


        RtfInjectModifier modifier = new RtfInjectModifier();

        UtilPrintSupport.injectParNumModifier(modifier, itemFac);

        UtilPrintSupport.injectModifierExtract(modifier, represent);

        modifier.put("NewGroupNumber", represent.getNewGroup().getTitle());

        RtfSearchResult searchResult = UniRtfUtil.findRtfMark(docCommon, "Ending");
        if (searchResult.isFound()) {
            IRtfText text = RtfBean.getElementFactory().createRtfText("\\fs28\\f0 им");
            text.setRaw(true);
            searchResult.getElementList().set(searchResult.getIndex(), text);
        }

        modifier.modify(docCommon);
    }

    @Override
    public void buildBodyRepresent(Representation representationBase, RtfDocument document, AtomicInteger parNumber, AtomicInteger extractNumber) {
        super.buildBodyRepresent(representationBase, document, parNumber, extractNumber);
        RepresentEnrollmentTransfer represent = (RepresentEnrollmentTransfer) representationBase;

        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        Student student = UtilPrintSupport.getStudent(represent);
        RtfInjectModifier modifier = new RtfInjectModifier();

        IdentityCard identityCard = student.getPerson().getIdentityCard();
        boolean isMaleSex = UniDefines.CATALOG_SEX_MALE.equals(identityCard.getSex().getCode());

        UtilPrintSupport.injectModifierRepresent(modifier, docRepresent, student);

        //modifier.put("studentTitle", PersonManager.instance().declinationDao().getDeclinationFIO(student.getPerson().getIdentityCard(), GrammaCase.ACCUSATIVE));
        modifier.put("studentTitle", UtilPrintSupport.printSeparatedFIO(student.getPerson().getIdentityCard(), GrammaCase.ACCUSATIVE, student.getPerson().isMale()));

        modifier.put("formativeOrgUnit", "По " + represent.getNewFormativeOrgUnit().getDativeCaseTitle());

        modifier.put("formativeOrgUnitOld", represent.getFormativeOrgUnitOldText());
        modifier.put("formativeOrgUnitNew", represent.getNewFormativeOrgUnit().getDativeCaseTitle());
        modifier.put("courseNew", represent.getNewCourse().getTitle());

        modifier.put("groupNew", represent.getNewGroup().getTitle());

        modifier.put("highSchoolNew", UtilPrintSupport.getHighLevelSchoolTypeString(represent.getNewEducationLevelsHighSchool()));

        if (represent.getEndDate() != null) {
            RtfString str = new RtfString().append(", с условием ликвидации разницы в учебных планах до ");
            UtilPrintSupport.getDateFormatterWithMonthStringAndYear(represent.getEndDate(), str);
            modifier.put("DateEnd", str);
        }
        else
            modifier.put("DateEnd", "");

        String maleSex = "";

        if (isMaleSex)
            maleSex = "\\fs28\\f0 им";
        else
            maleSex = "\\fs28\\f0 ей";

        RtfSearchResult searchResult = UniRtfUtil.findRtfMark(document, "Ending");
        if (searchResult.isFound()) {
            IRtfText text = RtfBean.getElementFactory().createRtfText(maleSex);
            text.setRaw(true);
            searchResult.getElementList().set(searchResult.getIndex(), text);
        }

        if (StringUtils.isEmpty(modifier.getStringValue("basics")))
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Arrays.asList("basics"), false, false);

        modifier.modify(document);
    }

    @Override
    public void buildBodyDeclaration(Representation representationBase, RtfDocument document) {
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        RepresentEnrollmentTransfer represent = (RepresentEnrollmentTransfer) representationBase;

        Student student = UtilPrintSupport.getStudent(represent);

        RtfInjectModifier im = new RtfInjectModifier();
        RtfTableModifier tm = new RtfTableModifier();
        UtilPrintSupport.injectModifierDeclaration(im, tm, docRepresent);

        this.injectModifier(im, docRepresent);

        im.put("formativeOrgUnitOld", represent.getFormativeOrgUnitOld().getGenitiveCaseTitle());
        im.put("formativeOrgUnitNew", represent.getNewFormativeOrgUnit().getGenitiveCaseTitle());
        im.put("courseNew", represent.getNewCourse().getTitle());

        String groupStr = "";

        if (represent.getNewGroup() != null)
            groupStr = "в " + represent.getNewGroup().getTitle() + " группу ";

        im.put("groupNew", groupStr);

        im.put("developForm", UtilPrintSupport.getDevelopFormGen(represent.getNewDevelopForm()));
        im.put("highSchoolNew", UtilPrintSupport.getDisplayableShortTitle(represent.getNewEducationLevelsHighSchool()));
        im.put("compensationTypeNew", represent.getNewCompensationType().getTitle());

        im.put("b_date", student.getPerson().getBirthDateStr());
        im.put("b_place", student.getPerson().getIdentityCard().getBirthPlace());
        im.put("mob_tel", student.getPerson().getContactData() != null ? student.getPerson().getContactData().getPhoneMobile() : null);
        im.put("tel", student.getPerson().getContactData() != null ? student.getPerson().getContactData().getPhoneFact() : null);

        String strLang = "";
        List<PersonForeignLanguage> langList = UniDaoFacade.getCoreDao()
                .getList(PersonForeignLanguage.class, "person",
                         student.getPerson());
        for (Iterator<PersonForeignLanguage> lang = langList.iterator(); lang
                .hasNext(); ) {
            strLang += lang.next().getLanguage().getTitle();
            if (lang.hasNext())
                strLang += ", ";
        }
        im.put("f_lang", strLang);

        im.modify(document);
        tm.modify(document);
    }

}
