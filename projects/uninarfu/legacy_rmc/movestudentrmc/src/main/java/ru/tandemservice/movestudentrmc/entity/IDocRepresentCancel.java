package ru.tandemservice.movestudentrmc.entity;

import org.tandemframework.core.entity.IEntity;

public interface IDocRepresentCancel extends IEntity {

    String L_CANCEL_ORDER = "cancelOrder";
    String L_CANCEL_REPRESENT = "cancelRepresent";

    DocumentOrder getCancelOrder();

    void setCancelOrder(DocumentOrder order);

    Representation getCancelRepresent();

    void setCancelRepresent(Representation represent);
}
