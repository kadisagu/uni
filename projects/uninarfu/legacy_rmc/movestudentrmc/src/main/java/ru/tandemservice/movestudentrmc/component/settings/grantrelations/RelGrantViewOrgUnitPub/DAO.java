package ru.tandemservice.movestudentrmc.component.settings.grantrelations.RelGrantViewOrgUnitPub;

import ru.tandemservice.uni.component.settings.abstractRelation.AbstractRelationPub.AbstractRelationPubDAO;

public class DAO extends AbstractRelationPubDAO<Model> implements IDAO {

    @Override
    protected String getRelationEntityName() {
        return "ru.tandemservice.movestudentrmc.entity.RelGrantViewOrgUnit";
    }

}
