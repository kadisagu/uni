package ru.tandemservice.movestudentrmc.component.student.DocumentList;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.movestudentrmc.entity.NarfuDocument;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        Student student = getNotNull(Student.class, model.getStudent().getId());
        model.setStudent(student);
    }

    @Override
    public void prepareListDataSource(Model model) {
        MQBuilder builder = new MQBuilder(NarfuDocument.ENTITY_CLASS, "d")
                .add(MQExpression.eq("d", NarfuDocument.student(), model.getStudent()))
                .add(MQExpression.or(
                        MQExpression.isNull("d", NarfuDocument.tmp()),
                        MQExpression.eq("d", NarfuDocument.tmp(), Boolean.FALSE)
                ));
        new OrderDescriptionRegistry("d").applyOrder(builder, model.getDataSource().getEntityOrder());

        List<NarfuDocument> documents = builder.getResultList(getSession());
        UniUtils.createPage(model.getDataSource(), documents);
    }
}
