package ru.tandemservice.movestudentrmc.dao.check;

public class CheckSeverodvinsk extends CheckArkhangelsk {

    @Override
    protected String getCityCode() {
        return "2900000400000";
    }

    @Override
    protected String getCityStr() {
        return "Северодвинск";
    }
}
