package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonBasic;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


public class ReasonRepresentCommitDSHandler extends DefaultComboDataSourceHandler {

    public ReasonRepresentCommitDSHandler(String ownerId) {
        super(ownerId, RepresentationReason.class);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep) {
        String filter = ep.input.getComboFilterByValue();
        if (StringUtils.isNotBlank(filter)) {
            ep.dqlBuilder.where(like(DQLFunctions.upper(DQLExpressions.property(RepresentationReason.title().fromAlias("e"))), value(CoreStringUtils.escapeLike(filter, true))));
        }

        List<Object> typeRepresentFilter = ep.context.get("typesRepresentFilter");

        ep.dqlBuilder.joinEntity("e", DQLJoinType.inner, RelRepresentationReasonBasic.class, "b",
                                 eq(DQLExpressions.property(RelRepresentationReasonBasic.reason().id().fromAlias("b")),
                                    DQLExpressions.property(RepresentationReason.id().fromAlias("e"))));

        if (typeRepresentFilter != null && !typeRepresentFilter.isEmpty())
            ep.dqlBuilder.where(DQLExpressions.in(
                    DQLExpressions.property(RelRepresentationReasonBasic.type().fromAlias("b")),
                    typeRepresentFilter));
    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
    {
        ep.dqlBuilder.order("e." + RepresentationReason.title());
    }
}
