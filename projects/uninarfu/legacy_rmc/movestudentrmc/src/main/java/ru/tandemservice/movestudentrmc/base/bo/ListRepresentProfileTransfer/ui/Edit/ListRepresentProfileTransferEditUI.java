package ru.tandemservice.movestudentrmc.base.bo.ListRepresentProfileTransfer.ui.Edit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.bo.Person.ui.View.PersonView;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.StudentDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.StudentSelectedDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentProfileTransfer.ListRepresentProfileTransferManager;
import ru.tandemservice.movestudentrmc.dao.CheckStateUtil;
import ru.tandemservice.movestudentrmc.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudentrmc.docord.util.MoveStudentUtil;
import ru.tandemservice.movestudentrmc.entity.DocListRepresentBasics;
import ru.tandemservice.movestudentrmc.entity.ListRepresentProfileTransfer;
import ru.tandemservice.movestudentrmc.entity.catalog.MovestudentExtractStates;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationBasement;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentExtractStatesCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.*;


@State({
        @Bind(key = ListRepresentProfileTransferEditUI.LIST_REPRESENT_ID, binding = ListRepresentProfileTransferEditUI.LIST_REPRESENT_ID),
        @Bind(key = "typeRepresentFilter", binding = "typeRepresentFilter"),
        @Bind(key = "reasonRepresentFilter", binding = "reasonRepresentFilter"),
        @Bind(key = "basementRepresentFilter", binding = "basementRepresentFilter"),
        @Bind(key = "representationBasementDate", binding = "representationBasementDate"),
        @Bind(key = "representationBasementNumber", binding = "representationBasementNumber")
})
public class ListRepresentProfileTransferEditUI extends UIPresenter {

    public static final String LIST_REPRESENT_ID = "listRepresentId";

    private Long listRepresentId;
    private RepresentationType typeRepresentFilter;
    private RepresentationReason reasonRepresentFilter;
    private RepresentationBasement basementRepresentFilter;

    private Date _representationBasementDate;     // Дата основания

    private String _representationBasementNumber;     // № основания

    private ListRepresentProfileTransfer listRepresent;

    private List<Student> studentSelectedList;
    //private ISelectModel sessionResults;
    private boolean warning = false;
    private boolean save = false;
    private String warningMessage;

    private ISelectModel orientationModel;
    private ISelectModel eduYearModel;

    //    События компонента

    @Override
    public void onComponentPrepareRender() {

    }


    @Override
    public void onComponentRefresh()
    {
        clearSettings();

        if (getListRepresentId() != null) {
            listRepresent = DataAccessServices.dao().get(ListRepresentProfileTransfer.class, ListRepresentProfileTransfer.id().s(), getListRepresentId());
/*			_uiSettings.set("educationYearField", listRepresent.getEducationYear());
            _uiSettings.set("grantViewField", listRepresent.getGrantView());
			_uiSettings.set("dateBeginingPaymentField", listRepresent.getDateBeginingPayment());
			_uiSettings.set("dateEndOfPaymentField", listRepresent.getDateEndOfPayment());			
			_uiSettings.set("fullEducationPeriod", listRepresent.getFullEducationPeriod());
*/
            _uiSettings.set("groupField", listRepresent.getNewGroup());
            _uiSettings.set("educationLevelsHighSchoolProfileDSField", listRepresent.getNewEducationOrgUnit());
            _uiSettings.set("dateBeginningTransferField", listRepresent.getDateBeginingTransfer());


            studentSelectedList = ListRepresentProfileTransferManager.instance().getListObjectModifyDAO().selectRepresent(listRepresent);
        }
        else {
            listRepresent = new ListRepresentProfileTransfer();
            _uiSettings.set("dateBeginningTransferField", new Date());
            listRepresent.setRepresentationType(getTypeRepresentFilter());
            listRepresent.setRepresentationReason(getReasonRepresentFilter());
            listRepresent.setRepresentationBasement(getBasementRepresentFilter());
            listRepresent.setState(DataAccessServices.dao().get(MovestudentExtractStates.class, MovestudentExtractStates.code().s(), "1"));
            if (studentSelectedList == null)
                studentSelectedList = new ArrayList<Student>();
        }

        setEduYearModel(new LazySimpleSelectModel<EducationYear>(EducationYear.class, EducationYear.title().s()));
        setOrientationModel(new DQLFullCheckSelectModel(EducationOrgUnit.educationLevelHighSchool().fullTitle().s()) {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, alias)
                        .where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.id().fromAlias(alias)),
                                                 MoveStudentDaoFacade.getOrientationDAO().getOrientationsBuilder().buildQuery()));
                FilterUtils.applySimpleLikeFilter(builder, alias, EducationOrgUnit.educationLevelHighSchool().fullTitle(), filter);
                return builder;
            }
        });

    }


    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        Map<String, Object> settingMap = _uiSettings.getAsMap(
                "educationYearFilter",
                "yearDistPartFilter",
                "territorialOrgUnitFilter",
                StudentDSHandler.FORMATIVE_ORG_UNIT_FILTER,
                "educationLevelsHighSchoolDSFilter",
                "educationOrgUnitFilter",
                "educationLevelFilter",
                StudentDSHandler.QUALIFICATION_FILTER,
                "courseFilter",
                "groupFilter",
                StudentDSHandler.DEVELOP_FORM_FILTER,
                "compensationTypeFilter",
                "benefitFilter",
                StudentDSHandler.STUDENT_FIO_FILTER,
                "educationYearField",
                "isSuccessfullyHandOverSession",
                "sessionMarksResult",
                "isConsiderSessionResults",
                "isforeignCitizens",
                "studentFilter",
                "userClickApply",
                "groupField",
                "educationLevelsHighSchoolDSField",
                "studentStatusFilter",
                "isNoGroup",
                "dateBeginningTransferField"
        );
        dataSource.putAll(settingMap);
        dataSource.put(StudentDSHandler.STUDENT_SELECTED_LIST, studentSelectedList);
        dataSource.put(StudentDSHandler.ORDER_ID, getListRepresentId());

        if (dataSource.getName().equals(ListRepresentProfileTransferEdit.GROUP_DS)) {
            dataSource.put("orientationFilter", getOrientation());
        }

        if (dataSource.getName().equals(ListRepresentProfileTransferEdit.STUDENT_DS)) {
            dataSource.put("eduYearFilter", _uiSettings.get("eduYearFilter"));
            dataSource.put("orientationFilter", _uiSettings.get("orientationFilter"));
        }

    }

    //    События формы

    public void selectRepresent() {

        BaseSearchListDataSource representDS = getConfig().getDataSource(ListRepresentProfileTransferEdit.STUDENT_DS);

        Collection<IEntity> records = ((CheckboxColumn) representDS.getLegacyDataSource().getColumn(StudentDSHandler.CHECKBOX_COLUMN)).getSelectedObjects();

        records.stream()
                .map(record -> (Student) record)
                .filter(record -> !studentSelectedList.contains(record))
                .forEach(record -> studentSelectedList.add((Student) record));

        ListRepresentProfileTransferManager.instance().getListObjectModifyDAO().checkSelectStudent(listRepresent, studentSelectedList);
    }

    public void deleteSelectedRepresent() {

        BaseSearchListDataSource representDS = (BaseSearchListDataSource) getConfig().getDataSource(ListRepresentProfileTransferEdit.STUDENT_SELECTED_DS);

        Collection<IEntity> records = ((CheckboxColumn) representDS.getLegacyDataSource().getColumn(StudentSelectedDSHandler.CHECKBOX_COLUMN)).getSelectedObjects();

        for (IEntity record : records) {
            studentSelectedList.remove((Student) record);
        }
    }

    public void onClickSave() {
        if (getListRepresentId() != null)
            CheckStateUtil.checkStateRepresent(getListRepresentProfileTransfer(), Arrays.asList(MovestudentExtractStatesCodes.CODE_1), _uiSupport.getSession());

        if (studentSelectedList.isEmpty())
            getConfig().getUserContext().getErrorCollector().add(getConfig().getProperty("ui.alertEmptySelectedRepresent"));

        Group selectedGroup = _uiSettings.get("groupField", Group.class);
        EducationOrgUnit educationOrgUnit = _uiSettings.get("educationLevelsHighSchoolProfileDSField", EducationOrgUnit.class);
        Date dateBeginningTransfer = _uiSettings.get("dateBeginningTransferField", Date.class);

        boolean ok = MoveStudentUtil.isChecked(studentSelectedList, null, educationOrgUnit.getDevelopForm());

        if ((ok && !isSave()) || (ok && isSave() && isWarning())) {
            String mess = MoveStudentUtil.checked(dateBeginningTransfer, false);
            if (!StringUtils.isEmpty(mess)) {
                setSave(true);
                setWarning(true);
                setWarningMessage(mess);
                return;
            }
        }

        listRepresent.setNewGroup(selectedGroup);
        listRepresent.setNewEducationOrgUnit(educationOrgUnit);
        listRepresent.setCreateDate(getSupport().getCurrentDate());
        listRepresent.setDateBeginingTransfer(dateBeginningTransfer);

        //------------------------------------------------------------------------------------------------------------------
        for(Student student : studentSelectedList){
        	if(!student.getEducationOrgUnit().getDevelopForm().equals(educationOrgUnit.getDevelopForm())) {
        		getConfig().getUserContext().getErrorCollector().add("Форма обучения студента "+ student.getFullFio() + " (" + student.getEducationOrgUnit().getDevelopForm().getShortTitle() + ")" +  " не соответствует форме обучения выбранной группы " + (selectedGroup != null ? selectedGroup.getTitle() : "") + " (" + educationOrgUnit.getDevelopForm().getShortTitle() + ")");
        	}
        	if( !student.getEducationOrgUnit().getDevelopCondition().equals(educationOrgUnit.getDevelopCondition())) {
        		getConfig().getUserContext().getErrorCollector().add("Условие обучения студента "+ student.getFullFio() + " (" + student.getEducationOrgUnit().getDevelopCondition().getShortTitle() + ")" +  " не соответствует условию обучения выбранной группы " + (selectedGroup != null ? selectedGroup.getTitle() : "") + " (" + educationOrgUnit.getDevelopCondition().getShortTitle() + ")");
        	}
        	if(!student.getEducationOrgUnit().getDevelopPeriod().equals(educationOrgUnit.getDevelopPeriod())) {
        		getConfig().getUserContext().getErrorCollector().add("Период обучения студента "+ student.getFullFio() + " (" + student.getEducationOrgUnit().getDevelopPeriod().getTitle() + ")" +  " не соответствует периоду обучения выбранной группы " + (selectedGroup != null ? selectedGroup.getTitle() : "") + " (" + educationOrgUnit.getDevelopPeriod().getTitle() + ")");
        	}
        	if(!student.getEducationOrgUnit().getDevelopTech().equals(educationOrgUnit.getDevelopTech())) {
        		getConfig().getUserContext().getErrorCollector().add("Технология обучения студента "+ student.getFullFio() + " (" + student.getEducationOrgUnit().getDevelopTech().getShortTitle() + ")" +  " не соответствует технологии обучения выбранной группы " + (selectedGroup != null ? selectedGroup.getTitle() : "") + " (" + educationOrgUnit.getDevelopTech().getShortTitle() + ")");
        	}
        	if(!student.getEducationOrgUnit().getFormativeOrgUnit().equals(educationOrgUnit.getFormativeOrgUnit())) {
        		getConfig().getUserContext().getErrorCollector().add("Формирующее подразделение студента "+ student.getFullFio() + " (" + student.getEducationOrgUnit().getFormativeOrgUnit().getShortTitle() + ")" +  " не соответствует формирующему подразделению выбранной группы " + (selectedGroup != null ? selectedGroup.getTitle() : "") + " (" + educationOrgUnit.getFormativeOrgUnit().getShortTitle() + ")");
        	}
        	if(!student.getEducationOrgUnit().getTerritorialOrgUnit().equals(educationOrgUnit.getTerritorialOrgUnit())) {
        		getConfig().getUserContext().getErrorCollector().add("Территориальное подразделение студента "+ student.getFullFio() + " (" + student.getEducationOrgUnit().getTerritorialOrgUnit().getShortTitle() + ")" +  " не соответствует территориальному подразделению выбранной группы " + (selectedGroup != null ? selectedGroup.getTitle() : "") + " (" + educationOrgUnit.getTerritorialOrgUnit().getShortTitle() + ")");
        	}
        	if(!student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject().equals(educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject())) {
        		getConfig().getUserContext().getErrorCollector().add("Направление подготовки (специальность) студента "+ student.getFullFio() + " (" + student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject().getTitleWithCode() + ")" +  " не соответствует направлению подготовки (специальности) выбранной группы " + (selectedGroup != null ? selectedGroup.getTitle() : "") + " (" + educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject().getTitleWithCode() + ")");
        	}
        }
        //------------------------------------------------------------------------------------------------------------------
        
        if (getConfig().getUserContext().getErrorCollector().hasErrors())
            return;

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocListRepresentBasics.class, "b").addColumn("b");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(DocListRepresentBasics.listRepresent().fromAlias("b")),
                DQLExpressions.value(listRepresent)));

        List<DocListRepresentBasics> representOldBasicsList = builder.createStatement(getSupport().getSession()).list();

        DocListRepresentBasics representBasics = null;

        if (representOldBasicsList.size() > 0) {
            representBasics = representOldBasicsList.get(0);
        }
        else {
            representBasics = new DocListRepresentBasics();
            representBasics.setListRepresent(listRepresent);
            representBasics.setBasic(listRepresent.getRepresentationBasement());
            representBasics.setReason(listRepresent.getRepresentationReason());

            representBasics.setRepresentationBasementDate(_representationBasementDate);
            representBasics.setRepresentationBasementNumber(_representationBasementNumber);
        }
        IPrincipalContext context = ContextLocal.getUserContext().getPrincipalContext();
        Person person = PersonManager.instance().dao().getPerson(context);
        listRepresent.setOperator(person);
        listRepresent.setCreator(context);
        if (getListRepresentId() == null)
            ListRepresentProfileTransferManager.instance().getListObjectModifyDAO().save(listRepresent, studentSelectedList, representBasics);
        else
            ListRepresentProfileTransferManager.instance().getListObjectModifyDAO().update(listRepresent, studentSelectedList);

        deactivate(2);
    }

    //    Вспомогательные функции


    //    Getters and Setters

    public void onClickView()
    {
        try {
            Student st = DataAccessServices.dao().get(Student.class, (Long) getListenerParameter());
            _uiActivation.asRegionDialog(PersonView.class)
                    .parameter("publisherId", st.getPerson().getId())
                    .activate();
        }
        catch (Exception e) {
            getUserContext().getErrorCollector().add(e.getLocalizedMessage());
        }


    }

    public void onIgnoreWarning() {
        if (isSave()) {
            setWarning(false);
            onClickSave();
        }
    }

    public void onCancelRepresent() {
        setWarning(false);
        setSave(false);
    }

    public ListRepresentProfileTransfer getListRepresentProfileTransfer() {
        return listRepresent;
    }

    public void setListRepresentProfileTransfer(ListRepresentProfileTransfer listRepresent) {
        this.listRepresent = listRepresent;
    }

    public void onViewRepresentationFromList() {
		/*
    	if (_orderId != null) {
        	List<DataWrapper> representSelect = ListRepresentProfileTransferManager.instance().modifyDao().selectRepresent(_order);

        	String type = "";
        	for (DataWrapper representW : representSelect) {
        		Representation repr = (Representation) representW.getProperty("represent");
        		if (repr.getId() == getListenerParameterAsLong()) {
        			type = repr.getType().getCode();
        			break;
        		}        		
        	}
        	String componentName = "";
        	this.getUserContext().getCurrentComponent().getParentRegion().activate(new ComponentActivator(componentName, (new UniMap()).add("representationId", getListenerParameter())));
        } 
		 */
    }


    public RepresentationType getTypeRepresentFilter() {
        return typeRepresentFilter;
    }

    public void setTypeRepresentFilter(RepresentationType typeRepresentFilter) {
        this.typeRepresentFilter = typeRepresentFilter;
    }

    public RepresentationReason getReasonRepresentFilter() {
        return reasonRepresentFilter;
    }

    public void setReasonRepresentFilter(RepresentationReason reasonRepresentFilter) {
        this.reasonRepresentFilter = reasonRepresentFilter;
    }

    public RepresentationBasement getBasementRepresentFilter() {
        return basementRepresentFilter;
    }

    public void setBasementRepresentFilter(RepresentationBasement basementRepresentFilter) {
        this.basementRepresentFilter = basementRepresentFilter;
    }

    public Long getListRepresentId() {
        return listRepresentId;
    }

    public void setListRepresentId(Long listRepresentId) {
        this.listRepresentId = listRepresentId;
    }

    public Date getRepresentationBasementDate()
    {
        return _representationBasementDate;
    }

    public void setRepresentationBasementDate(Date representationBasementDate)
    {
        _representationBasementDate = representationBasementDate;
    }

    public String getRepresentationBasementNumber()
    {
        return _representationBasementNumber;
    }

    public void setRepresentationBasementNumber(String representationBasementNumber)
    {
        _representationBasementNumber = representationBasementNumber;
    }

    public boolean isWarning() {
        return warning;
    }


    public void setWarning(boolean warning) {
        this.warning = warning;
    }


    public boolean isSave() {
        return save;
    }


    public void setSave(boolean save) {
        this.save = save;
    }

    public EducationOrgUnit getOrientation() {
        return _uiSettings.get("orientationFilter");
    }

    public String getWarningMessage() {
        return warningMessage;
    }


    public void setWarningMessage(String warningMessage) {
        this.warningMessage = warningMessage;
    }


    public ISelectModel getOrientationModel() {
        return orientationModel;
    }


    public void setOrientationModel(ISelectModel orientationModel) {
        this.orientationModel = orientationModel;
    }


    public ISelectModel getEduYearModel() {
        return eduYearModel;
    }


    public void setEduYearModel(ISelectModel eduYearModel) {
        this.eduYearModel = eduYearModel;
    }

}
