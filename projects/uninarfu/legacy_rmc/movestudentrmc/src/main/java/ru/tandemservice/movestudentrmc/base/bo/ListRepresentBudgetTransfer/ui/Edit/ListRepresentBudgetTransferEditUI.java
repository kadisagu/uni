package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBudgetTransfer.ui.Edit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.bo.Person.ui.View.PersonView;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ListRepresentBaseManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.StudentDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.StudentSelectedDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBudgetTransfer.ListRepresentBudgetTransferManager;
import ru.tandemservice.movestudentrmc.dao.CheckStateUtil;
import ru.tandemservice.movestudentrmc.entity.DocListRepresentBasics;
import ru.tandemservice.movestudentrmc.entity.ListRepresentBudgetTransfer;
import ru.tandemservice.movestudentrmc.entity.catalog.*;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentExtractStatesCodes;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.*;


@State({
        @Bind(key = ListRepresentBudgetTransferEditUI.LIST_REPRESENT_ID, binding = ListRepresentBudgetTransferEditUI.LIST_REPRESENT_ID),
        @Bind(key = "typeRepresentFilter", binding = "typeRepresentFilter"),
        @Bind(key = "reasonRepresentFilter", binding = "reasonRepresentFilter"),
        @Bind(key = "basementRepresentFilter", binding = "basementRepresentFilter"),
        @Bind(key = "representationBasementDate", binding = "representationBasementDate"),
        @Bind(key = "representationBasementNumber", binding = "representationBasementNumber")
})
public class ListRepresentBudgetTransferEditUI extends UIPresenter {

    public static final String LIST_REPRESENT_ID = "listRepresentId";

    private Long listRepresentId;
    private RepresentationType typeRepresentFilter;
    private RepresentationReason reasonRepresentFilter;
    private RepresentationBasement basementRepresentFilter;

    private Date _representationBasementDate;     // Дата основания

    private String _representationBasementNumber;     // № основания

    private ListRepresentBudgetTransfer listRepresent;

    private List<Student> studentSelectedList;
    //private ISelectModel sessionResults;


    //    События компонента

    @Override
    public void onComponentPrepareRender() {

    }


    @Override
    public void onComponentRefresh()
    {
        clearSettings();
        _uiSettings.set("compensationTypeFilter", DataAccessServices.dao().get(CompensationType.class, CompensationType.code(), CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT));
        if (getListRepresentId() != null) {
            listRepresent = DataAccessServices.dao().get(ListRepresentBudgetTransfer.class, ListRepresentBudgetTransfer.id().s(), getListRepresentId());
            _uiSettings.set("educationYearField", listRepresent.getEducationYear());
            _uiSettings.set("grantViewField", listRepresent.getGrantView());
            _uiSettings.set("dateEndOfPaymentField", listRepresent.getDateEndOfPayment());
            _uiSettings.set("dateBeginingPaymentField", listRepresent.getDateBeginingPayment());
            _uiSettings.set("dateBeginingTransfer", listRepresent.getDateBeginingTransfer());

            studentSelectedList = ListRepresentBudgetTransferManager.instance().getListObjectModifyDAO().selectRepresent(listRepresent);
        }
        else {
            listRepresent = new ListRepresentBudgetTransfer();

            listRepresent.setRepresentationType(getTypeRepresentFilter());
            listRepresent.setRepresentationReason(getReasonRepresentFilter());
            listRepresent.setRepresentationBasement(getBasementRepresentFilter());
            listRepresent.setState(DataAccessServices.dao().get(MovestudentExtractStates.class, MovestudentExtractStates.code().s(), "1"));
            if (studentSelectedList == null)
                studentSelectedList = new ArrayList<Student>();
        }
    }


    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        Map<String, Object> settingMap = _uiSettings.getAsMap(
                "educationYearFilter",
                "yearDistPartFilter",
                "territorialOrgUnitFilter",
                StudentDSHandler.FORMATIVE_ORG_UNIT_FILTER,
                "educationLevelsHighSchoolDSFilter",
                "educationOrgUnitFilter",
                "educationLevelFilter",
                StudentDSHandler.QUALIFICATION_FILTER,
                "courseFilter",
                "groupFilter",
                StudentDSHandler.DEVELOP_FORM_FILTER,
                "compensationTypeFilter",
                "benefitFilter",
                StudentDSHandler.STUDENT_FIO_FILTER,
                "educationYearField",
                "isSuccessfullyHandOverSession",
                "sessionMarksResult",
                "isConsiderSessionResults",
                "isforeignCitizens",
                "studentFilter",
                "userClickApply",
                "groupField",
                "educationLevelsHighSchoolDSField",
                "studentStatusFilter"
        );
        dataSource.putAll(settingMap);
        dataSource.put(StudentDSHandler.STUDENT_SELECTED_LIST, studentSelectedList);
        dataSource.put(StudentDSHandler.ORDER_ID, getListRepresentId());

    }

    //    События формы

    public void selectRepresent() {

        BaseSearchListDataSource representDS = (BaseSearchListDataSource) getConfig().getDataSource(ListRepresentBudgetTransferEdit.STUDENT_DS);

        Collection<IEntity> records = ((CheckboxColumn) representDS.getLegacyDataSource().getColumn(StudentDSHandler.CHECKBOX_COLUMN)).getSelectedObjects();

        for (IEntity record : records) {
            if (!studentSelectedList.contains((Student) record))
                studentSelectedList.add((Student) record);
        }

        ListRepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(getTypeRepresentFilter().getCode()).getListObjectModifyDAO().checkSelectStudent(this.listRepresent, studentSelectedList);

    }

    public void deleteSelectedRepresent() {

        BaseSearchListDataSource representDS = (BaseSearchListDataSource) getConfig().getDataSource(ListRepresentBudgetTransferEdit.STUDENT_SELECTED_DS);

        Collection<IEntity> records = ((CheckboxColumn) representDS.getLegacyDataSource().getColumn(StudentSelectedDSHandler.CHECKBOX_COLUMN)).getSelectedObjects();

        for (IEntity record : records) {
            studentSelectedList.remove((Student) record);
        }
    }

    public void onClickSave() {

        if (listRepresentId != null)
            CheckStateUtil.checkStateRepresent(listRepresent, Arrays.asList(MovestudentExtractStatesCodes.CODE_1), _uiSupport.getSession());

        if (studentSelectedList.isEmpty())
            getConfig().getUserContext().getErrorCollector().add(getConfig().getProperty("ui.alertEmptySelectedRepresent"));

        //Дата перевода студентов на бюджет
        Date dateBeginingTransfer = _uiSettings.get("dateBeginingTransfer", Date.class);
        listRepresent.setDateBeginingTransfer(dateBeginingTransfer);

        //Параметры для выплаты стипендии
        listRepresent.setGrantView(_uiSettings.get("grantViewField", GrantView.class));
        listRepresent.setDateEndOfPayment(_uiSettings.get("dateEndOfPaymentField", Date.class));
        listRepresent.setDateBeginingPayment(_uiSettings.get("dateBeginingPaymentField", Date.class));
        listRepresent.setEducationYear(_uiSettings.get("educationYearField", EducationYear.class));

        listRepresent.setCreateDate(getSupport().getCurrentDate());

        if (getConfig().getUserContext().getErrorCollector().hasErrors())
            return;

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocListRepresentBasics.class, "b").addColumn("b");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(DocListRepresentBasics.listRepresent().fromAlias("b")),
                DQLExpressions.value(listRepresent)));

        List<DocListRepresentBasics> representOldBasicsList = builder.createStatement(getSupport().getSession()).list();

        DocListRepresentBasics representBasics = null;

        if (representOldBasicsList.size() > 0) {
            representBasics = representOldBasicsList.get(0);
        }
        else {
            representBasics = new DocListRepresentBasics();
            representBasics.setListRepresent(listRepresent);
            representBasics.setBasic(listRepresent.getRepresentationBasement());
            representBasics.setReason(listRepresent.getRepresentationReason());

            representBasics.setRepresentationBasementDate(_representationBasementDate);
            representBasics.setRepresentationBasementNumber(String.valueOf(_representationBasementNumber));
        }
        IPrincipalContext context = ContextLocal.getUserContext().getPrincipalContext();
        Person person = PersonManager.instance().dao().getPerson(context);
        listRepresent.setOperator(person);
        listRepresent.setCreator(context);
    /*	if (getListRepresentId() == null)*/
        ListRepresentBudgetTransferManager.instance().getListObjectModifyDAO().save(listRepresent, studentSelectedList, representBasics);
/*		else
			ListRepresentBudgetTransferManager.instance().getListObjectModifyDAO().update(listRepresent, studentSelectedList);
*/

        deactivate(2);
    }

    //    Вспомогательные функции


    //    Getters and Setters

    public void onClickView()
    {
        try {
            Student st = DataAccessServices.dao().get(Student.class, (Long) getListenerParameter());
            _uiActivation.asRegionDialog(PersonView.class)
                    .parameter("publisherId", st.getPerson().getId())
                    .activate();
        }
        catch (Exception e) {
            getUserContext().getErrorCollector().add(e.getLocalizedMessage());
        }


    }

    public ListRepresentBudgetTransfer getListRepresentBudgetTransfer() {
        return listRepresent;
    }

    public void setListRepresentBudgetTransfer(ListRepresentBudgetTransfer listRepresent) {
        this.listRepresent = listRepresent;
    }

    public void onViewRepresentationFromList() {
		/*
    	if (_orderId != null) {
        	List<DataWrapper> representSelect = ListRepresentBudgetTransferManager.instance().modifyDao().selectRepresent(_order);

        	String type = "";
        	for (DataWrapper representW : representSelect) {
        		Representation repr = (Representation) representW.getProperty("represent");
        		if (repr.getId() == getListenerParameterAsLong()) {
        			type = repr.getType().getCode();
        			break;
        		}        		
        	}
        	String componentName = "";
        	this.getUserContext().getCurrentComponent().getParentRegion().activate(new ComponentActivator(componentName, (new UniMap()).add("representationId", getListenerParameter())));
        } 
		 */
    }

    public void onDateBeginingTransferChange() {
        Date date = _uiSettings.get("dateBeginingTransfer", Date.class);

        Calendar c1 = Calendar.getInstance();
        c1.setTime(date);
        c1.set(Calendar.DAY_OF_MONTH, 1);
        c1.add(Calendar.MONTH, 1);

        _uiSettings.set("dateBeginingPaymentField", c1.getTime());
    }


    public RepresentationType getTypeRepresentFilter() {
        return typeRepresentFilter;
    }

    public void setTypeRepresentFilter(RepresentationType typeRepresentFilter) {
        this.typeRepresentFilter = typeRepresentFilter;
    }

    public RepresentationReason getReasonRepresentFilter() {
        return reasonRepresentFilter;
    }

    public void setReasonRepresentFilter(RepresentationReason reasonRepresentFilter) {
        this.reasonRepresentFilter = reasonRepresentFilter;
    }

    public RepresentationBasement getBasementRepresentFilter() {
        return basementRepresentFilter;
    }

    public void setBasementRepresentFilter(RepresentationBasement basementRepresentFilter) {
        this.basementRepresentFilter = basementRepresentFilter;
    }

    public Long getListRepresentId() {
        return listRepresentId;
    }

    public void setListRepresentId(Long listRepresentId) {
        this.listRepresentId = listRepresentId;
    }

    public Date getRepresentationBasementDate()
    {
        return _representationBasementDate;
    }

    public void setRepresentationBasementDate(Date representationBasementDate)
    {
        _representationBasementDate = representationBasementDate;
    }

    public String getRepresentationBasementNumber()
    {
        return _representationBasementNumber;
    }

    public void setRepresentationBasementNumber(String representationBasementNumber)
    {
        _representationBasementNumber = representationBasementNumber;
    }

}
