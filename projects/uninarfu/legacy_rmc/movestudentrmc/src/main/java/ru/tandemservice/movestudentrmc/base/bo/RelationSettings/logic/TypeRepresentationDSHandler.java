package ru.tandemservice.movestudentrmc.base.bo.RelationSettings.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;

public class TypeRepresentationDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput> {

    public static final String TYPE_COLUMN = "type";
    public static final String ISCONFIG_COLUMN = "isConfig";

    public TypeRepresentationDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RepresentationType.class, "type").addColumn("type");
        builder.where(DQLExpressions.eq(DQLExpressions.property(RepresentationType.use().fromAlias("type")), DQLExpressions.value(true)));

        return DQLSelectOutputBuilder.get(input, builder, getSession()).order(RepresentationType.class, "type").build();
    }
}
