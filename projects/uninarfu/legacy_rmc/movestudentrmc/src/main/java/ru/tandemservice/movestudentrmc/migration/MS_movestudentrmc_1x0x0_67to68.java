/*$Id$*/
package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author DMITRY KNYAZEV
 * @since 19.02.2015
 */
public class MS_movestudentrmc_1x0x0_67to68 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.5.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.5.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        //Здесь могла быть ваша реклама.
        throw new IllegalAccessError("Здесь должна была быть миграция. Но в предоставленных САФУ исходных кодах ее нет см. DEV-6870. Если вы все же это видите, значит с вами что-то не так");
    }
}
