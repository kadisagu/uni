package ru.tandemservice.movestudentrmc.base.bo.DOOrder.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.TypeRepresentationDSHandler;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.uni.util.FilterUtils;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class TypeRepresentCommitDSHandler extends TypeRepresentationDSHandler
{

    public TypeRepresentCommitDSHandler(String ownerId) {

        super(ownerId);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        String filter = ep.input.getComboFilterByValue();
        ep.dqlBuilder.where(eq(DQLExpressions.property(RepresentationType.listRepresentation().fromAlias("e")), value(Boolean.FALSE)));
        if (StringUtils.isNotBlank(filter)) {
            FilterUtils.applySimpleLikeFilter(ep.dqlBuilder, "e", RepresentationType.title().s(), filter);
        }
    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
    {
        ep.dqlBuilder.order("e." + RepresentationType.title());
    }
}
