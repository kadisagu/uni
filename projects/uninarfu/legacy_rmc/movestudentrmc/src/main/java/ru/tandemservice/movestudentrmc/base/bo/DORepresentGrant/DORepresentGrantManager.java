package ru.tandemservice.movestudentrmc.base.bo.DORepresentGrant;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentGrant.logic.DORepresentGrantDAO;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentGrant.ui.Edit.DORepresentGrantEdit;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentGrant.ui.View.DORepresentGrantView;
import ru.tandemservice.movestudentrmc.docord.IDOObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IDOObjectModifyDAO;

@Configuration
public class DORepresentGrantManager extends BusinessObjectManager implements IDOObjectByRepresentTypeManager {

    public static DORepresentGrantManager instance() {
        return instance(DORepresentGrantManager.class);
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return DORepresentGrantEdit.class;
    }

    @Bean
    public IDOObjectModifyDAO getDOObjectModifyDAO() {
        return new DORepresentGrantDAO();
    }

    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return DORepresentGrantView.class;
    }

}
