package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import ru.tandemservice.movestudentrmc.entity.NarfuDocument;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class StudentDocumentsDSHandler extends DefaultComboDataSourceHandler {

    public static final String STUDENT_DOCUMENTS_DS = "studentDocumentsDS";
    public static final String STUDENT_ID = "studentId";

    public StudentDocumentsDSHandler(String ownerId) {

        super(ownerId, NarfuDocument.class, NarfuDocument.number());
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        String filter = ep.input.getComboFilterByValue();
        Long studentId = ep.context.get(STUDENT_ID);

        ep.dqlBuilder.where(eq(property(NarfuDocument.student().id().fromAlias("e")), value(studentId)));


        if (StringUtils.isNotBlank(filter)) {
            ep.dqlBuilder.where(like(DQLFunctions.upper(property(NarfuDocument.additionalName().fromAlias("e"))), value(CoreStringUtils.escapeLike(filter, true))));
        }
    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
    {
        ep.dqlBuilder.order("e." + NarfuDocument.additionalName());
    }
}
