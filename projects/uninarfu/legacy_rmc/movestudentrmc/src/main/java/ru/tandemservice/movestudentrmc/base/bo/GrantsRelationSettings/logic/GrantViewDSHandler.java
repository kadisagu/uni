package ru.tandemservice.movestudentrmc.base.bo.GrantsRelationSettings.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class GrantViewDSHandler extends DefaultComboDataSourceHandler {

    public GrantViewDSHandler(String ownerId) {

        super(ownerId, GrantView.class, GrantView.P_TITLE);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        String filter = ep.input.getComboFilterByValue();
        ep.dqlBuilder.where(eq(property(GrantView.use().fromAlias("e")), value(true)));
        if (StringUtils.isNotBlank(filter)) {
            ep.dqlBuilder.where(like(DQLFunctions.upper(property(GrantView.title().fromAlias("e"))), value(CoreStringUtils.escapeLike(filter, true))));
        }
    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
    {
        ep.dqlBuilder.order("e." + GrantView.title());
    }
}
