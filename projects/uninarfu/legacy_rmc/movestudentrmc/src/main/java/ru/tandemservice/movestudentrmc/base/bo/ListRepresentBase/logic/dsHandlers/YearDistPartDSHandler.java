package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;

public class YearDistPartDSHandler extends DefaultComboDataSourceHandler {

    public YearDistPartDSHandler(String ownerId) {
        super(ownerId, YearDistributionPart.class);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {


    }


    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
    {
        //ep.dqlBuilder.order("e." + PersonBenefit.benefit().title());
    }

}
