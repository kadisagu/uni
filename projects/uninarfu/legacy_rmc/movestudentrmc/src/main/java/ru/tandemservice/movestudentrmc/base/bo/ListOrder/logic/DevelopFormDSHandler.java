package ru.tandemservice.movestudentrmc.base.bo.ListOrder.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import ru.tandemservice.uni.entity.catalog.DevelopForm;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DevelopFormDSHandler extends DefaultComboDataSourceHandler {

    public DevelopFormDSHandler(String ownerId) {

        super(ownerId, DevelopForm.class, DevelopForm.P_TITLE);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        String filter = ep.input.getComboFilterByValue();
        if (StringUtils.isNotBlank(filter)) {
            ep.dqlBuilder.where(like(DQLFunctions.upper(property(DevelopForm.title().fromAlias("e"))), value(CoreStringUtils.escapeLike(filter, true))));
        }
    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
    {
        ep.dqlBuilder.order("e." + DevelopForm.title());
    }
}
