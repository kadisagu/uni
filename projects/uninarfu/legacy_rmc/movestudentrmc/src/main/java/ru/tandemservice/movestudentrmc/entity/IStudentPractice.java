package ru.tandemservice.movestudentrmc.entity;

import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;

import java.util.Date;

public interface IStudentPractice {

    public Date getDateBeginningPractice();

    public Date getDateEndOfPractice();

    public EppWorkPlanRow getRegistryElementRow();
}
