package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.sec.entity.gen.IPrincipalContextGen;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudentrmc.entity.IAbstractRepresentation;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.entity.catalog.MovestudentExtractStates;
import ru.tandemservice.movestudentrmc.entity.catalog.OsspGrants;
import ru.tandemservice.movestudentrmc.entity.catalog.OsspGrantsView;
import ru.tandemservice.movestudentrmc.entity.catalog.OsspPgo;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.unimv.IAbstractDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Базовое представление в приказ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RepresentationGen extends EntityBase
 implements IAbstractDocument, IAbstractRepresentation{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.Representation";
    public static final String ENTITY_NAME = "representation";
    public static final int VERSION_HASH = -1313147090;
    private static IEntityMeta ENTITY_META;

    public static final String P_TITLE = "title";
    public static final String L_TYPE = "type";
    public static final String P_CREATE_DATE = "createDate";
    public static final String P_NUMBER = "number";
    public static final String P_COMMITTED = "committed";
    public static final String L_STATE = "state";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_REASON = "reason";
    public static final String L_OSSP_GRANTS = "osspGrants";
    public static final String L_OSSP_GRANTS_VIEW = "osspGrantsView";
    public static final String L_OSSP_PGO = "osspPgo";
    public static final String L_DEVELOP_CONDITION = "developCondition";
    public static final String P_START_DATE = "startDate";
    public static final String P_DOC_REPRESENT = "docRepresent";
    public static final String P_DOC_EXTRACT = "docExtract";
    public static final String P_SAVE_GRANTS = "saveGrants";
    public static final String P_GRANTS_OLD = "grantsOld";
    public static final String L_STATUS_OLD = "statusOld";
    public static final String P_CHECK = "check";
    public static final String P_REPRESENT_I_D = "representID";
    public static final String L_CREATOR = "creator";
    public static final String P_TITLE_WITH_CREATOR = "titleWithCreator";

    private String _title;     // Представление
    private RepresentationType _type;     // Тип представления
    private Date _createDate;     // Дата формирования
    private String _number;     // Номер представления
    private boolean _committed;     // Проведено
    private MovestudentExtractStates _state;     // Cостояние представления
    private OrgUnit _orgUnit;     // Формирующее подразделение
    private RepresentationReason _reason;     // Причина представления
    private OsspGrants _osspGrants;     // Стипендии ОССП
    private OsspGrantsView _osspGrantsView;     // Тип стипендии
    private OsspPgo _osspPgo;     // ПГО ОССП
    private DevelopCondition _developCondition;     // Условие освоения
    private Date _startDate;     // Дата вступления в силу
    private byte[] _docRepresent;     // Представление в zip
    private byte[] _docExtract;     // Выписка в zip
    private boolean _saveGrants;     // С сохранением стипендий/выплат
    private String _grantsOld;     // Отмененные стипендии
    private StudentStatus _statusOld;     // Старый статус студента
    private Boolean _check;     // Проверка
    private Long _representID;     // ID представления
    private IPrincipalContext _creator;     // Создатель представления

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Представление.
     */
    @Length(max=1024)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Представление.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Тип представления. Свойство не может быть null.
     */
    @NotNull
    public RepresentationType getType()
    {
        return _type;
    }

    /**
     * @param type Тип представления. Свойство не может быть null.
     */
    public void setType(RepresentationType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getCreateDate()
    {
        return _createDate;
    }

    /**
     * @param createDate Дата формирования. Свойство не может быть null.
     */
    public void setCreateDate(Date createDate)
    {
        dirty(_createDate, createDate);
        _createDate = createDate;
    }

    /**
     * @return Номер представления.
     */
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер представления.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Проведено. Свойство не может быть null.
     */
    @NotNull
    public boolean isCommitted()
    {
        return _committed;
    }

    /**
     * @param committed Проведено. Свойство не может быть null.
     */
    public void setCommitted(boolean committed)
    {
        dirty(_committed, committed);
        _committed = committed;
    }

    /**
     * @return Cостояние представления.
     */
    public MovestudentExtractStates getState()
    {
        return _state;
    }

    /**
     * @param state Cостояние представления.
     */
    public void setState(MovestudentExtractStates state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * @return Формирующее подразделение.
     */
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Формирующее подразделение.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Причина представления.
     */
    public RepresentationReason getReason()
    {
        return _reason;
    }

    /**
     * @param reason Причина представления.
     */
    public void setReason(RepresentationReason reason)
    {
        dirty(_reason, reason);
        _reason = reason;
    }

    /**
     * @return Стипендии ОССП.
     */
    public OsspGrants getOsspGrants()
    {
        return _osspGrants;
    }

    /**
     * @param osspGrants Стипендии ОССП.
     */
    public void setOsspGrants(OsspGrants osspGrants)
    {
        dirty(_osspGrants, osspGrants);
        _osspGrants = osspGrants;
    }

    /**
     * @return Тип стипендии.
     */
    public OsspGrantsView getOsspGrantsView()
    {
        return _osspGrantsView;
    }

    /**
     * @param osspGrantsView Тип стипендии.
     */
    public void setOsspGrantsView(OsspGrantsView osspGrantsView)
    {
        dirty(_osspGrantsView, osspGrantsView);
        _osspGrantsView = osspGrantsView;
    }

    /**
     * @return ПГО ОССП.
     */
    public OsspPgo getOsspPgo()
    {
        return _osspPgo;
    }

    /**
     * @param osspPgo ПГО ОССП.
     */
    public void setOsspPgo(OsspPgo osspPgo)
    {
        dirty(_osspPgo, osspPgo);
        _osspPgo = osspPgo;
    }

    /**
     * @return Условие освоения.
     */
    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    /**
     * @param developCondition Условие освоения.
     */
    public void setDevelopCondition(DevelopCondition developCondition)
    {
        dirty(_developCondition, developCondition);
        _developCondition = developCondition;
    }

    /**
     * @return Дата вступления в силу.
     */
    public Date getStartDate()
    {
        return _startDate;
    }

    /**
     * @param startDate Дата вступления в силу.
     */
    public void setStartDate(Date startDate)
    {
        dirty(_startDate, startDate);
        _startDate = startDate;
    }

    /**
     * @return Представление в zip.
     */
    public byte[] getDocRepresent()
    {
        return _docRepresent;
    }

    /**
     * @param docRepresent Представление в zip.
     */
    public void setDocRepresent(byte[] docRepresent)
    {
        dirty(_docRepresent, docRepresent);
        _docRepresent = docRepresent;
    }

    /**
     * @return Выписка в zip.
     */
    public byte[] getDocExtract()
    {
        return _docExtract;
    }

    /**
     * @param docExtract Выписка в zip.
     */
    public void setDocExtract(byte[] docExtract)
    {
        dirty(_docExtract, docExtract);
        _docExtract = docExtract;
    }

    /**
     * @return С сохранением стипендий/выплат. Свойство не может быть null.
     */
    @NotNull
    public boolean isSaveGrants()
    {
        return _saveGrants;
    }

    /**
     * @param saveGrants С сохранением стипендий/выплат. Свойство не может быть null.
     */
    public void setSaveGrants(boolean saveGrants)
    {
        dirty(_saveGrants, saveGrants);
        _saveGrants = saveGrants;
    }

    /**
     * @return Отмененные стипендии.
     */
    public String getGrantsOld()
    {
        return _grantsOld;
    }

    /**
     * @param grantsOld Отмененные стипендии.
     */
    public void setGrantsOld(String grantsOld)
    {
        dirty(_grantsOld, grantsOld);
        _grantsOld = grantsOld;
    }

    /**
     * @return Старый статус студента.
     */
    public StudentStatus getStatusOld()
    {
        return _statusOld;
    }

    /**
     * @param statusOld Старый статус студента.
     */
    public void setStatusOld(StudentStatus statusOld)
    {
        dirty(_statusOld, statusOld);
        _statusOld = statusOld;
    }

    /**
     * @return Проверка.
     */
    public Boolean getCheck()
    {
        return _check;
    }

    /**
     * @param check Проверка.
     */
    public void setCheck(Boolean check)
    {
        dirty(_check, check);
        _check = check;
    }

    /**
     * @return ID представления.
     *
     * Это формула "id".
     */
    public Long getRepresentID()
    {
        return _representID;
    }

    /**
     * @param representID ID представления.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setRepresentID(Long representID)
    {
        dirty(_representID, representID);
        _representID = representID;
    }

    /**
     * @return Создатель представления.
     */
    public IPrincipalContext getCreator()
    {
        return _creator;
    }

    /**
     * @param creator Создатель представления.
     */
    public void setCreator(IPrincipalContext creator)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && creator!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IPrincipalContext.class);
            IEntityMeta actual =  creator instanceof IEntity ? EntityRuntime.getMeta((IEntity) creator) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_creator, creator);
        _creator = creator;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RepresentationGen)
        {
            setTitle(((Representation)another).getTitle());
            setType(((Representation)another).getType());
            setCreateDate(((Representation)another).getCreateDate());
            setNumber(((Representation)another).getNumber());
            setCommitted(((Representation)another).isCommitted());
            setState(((Representation)another).getState());
            setOrgUnit(((Representation)another).getOrgUnit());
            setReason(((Representation)another).getReason());
            setOsspGrants(((Representation)another).getOsspGrants());
            setOsspGrantsView(((Representation)another).getOsspGrantsView());
            setOsspPgo(((Representation)another).getOsspPgo());
            setDevelopCondition(((Representation)another).getDevelopCondition());
            setStartDate(((Representation)another).getStartDate());
            setDocRepresent(((Representation)another).getDocRepresent());
            setDocExtract(((Representation)another).getDocExtract());
            setSaveGrants(((Representation)another).isSaveGrants());
            setGrantsOld(((Representation)another).getGrantsOld());
            setStatusOld(((Representation)another).getStatusOld());
            setCheck(((Representation)another).getCheck());
            setRepresentID(((Representation)another).getRepresentID());
            setCreator(((Representation)another).getCreator());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RepresentationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Representation.class;
        }

        public T newInstance()
        {
            return (T) new Representation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "title":
                    return obj.getTitle();
                case "type":
                    return obj.getType();
                case "createDate":
                    return obj.getCreateDate();
                case "number":
                    return obj.getNumber();
                case "committed":
                    return obj.isCommitted();
                case "state":
                    return obj.getState();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "reason":
                    return obj.getReason();
                case "osspGrants":
                    return obj.getOsspGrants();
                case "osspGrantsView":
                    return obj.getOsspGrantsView();
                case "osspPgo":
                    return obj.getOsspPgo();
                case "developCondition":
                    return obj.getDevelopCondition();
                case "startDate":
                    return obj.getStartDate();
                case "docRepresent":
                    return obj.getDocRepresent();
                case "docExtract":
                    return obj.getDocExtract();
                case "saveGrants":
                    return obj.isSaveGrants();
                case "grantsOld":
                    return obj.getGrantsOld();
                case "statusOld":
                    return obj.getStatusOld();
                case "check":
                    return obj.getCheck();
                case "representID":
                    return obj.getRepresentID();
                case "creator":
                    return obj.getCreator();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "type":
                    obj.setType((RepresentationType) value);
                    return;
                case "createDate":
                    obj.setCreateDate((Date) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "committed":
                    obj.setCommitted((Boolean) value);
                    return;
                case "state":
                    obj.setState((MovestudentExtractStates) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "reason":
                    obj.setReason((RepresentationReason) value);
                    return;
                case "osspGrants":
                    obj.setOsspGrants((OsspGrants) value);
                    return;
                case "osspGrantsView":
                    obj.setOsspGrantsView((OsspGrantsView) value);
                    return;
                case "osspPgo":
                    obj.setOsspPgo((OsspPgo) value);
                    return;
                case "developCondition":
                    obj.setDevelopCondition((DevelopCondition) value);
                    return;
                case "startDate":
                    obj.setStartDate((Date) value);
                    return;
                case "docRepresent":
                    obj.setDocRepresent((byte[]) value);
                    return;
                case "docExtract":
                    obj.setDocExtract((byte[]) value);
                    return;
                case "saveGrants":
                    obj.setSaveGrants((Boolean) value);
                    return;
                case "grantsOld":
                    obj.setGrantsOld((String) value);
                    return;
                case "statusOld":
                    obj.setStatusOld((StudentStatus) value);
                    return;
                case "check":
                    obj.setCheck((Boolean) value);
                    return;
                case "representID":
                    obj.setRepresentID((Long) value);
                    return;
                case "creator":
                    obj.setCreator((IPrincipalContext) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "title":
                        return true;
                case "type":
                        return true;
                case "createDate":
                        return true;
                case "number":
                        return true;
                case "committed":
                        return true;
                case "state":
                        return true;
                case "orgUnit":
                        return true;
                case "reason":
                        return true;
                case "osspGrants":
                        return true;
                case "osspGrantsView":
                        return true;
                case "osspPgo":
                        return true;
                case "developCondition":
                        return true;
                case "startDate":
                        return true;
                case "docRepresent":
                        return true;
                case "docExtract":
                        return true;
                case "saveGrants":
                        return true;
                case "grantsOld":
                        return true;
                case "statusOld":
                        return true;
                case "check":
                        return true;
                case "representID":
                        return true;
                case "creator":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "title":
                    return true;
                case "type":
                    return true;
                case "createDate":
                    return true;
                case "number":
                    return true;
                case "committed":
                    return true;
                case "state":
                    return true;
                case "orgUnit":
                    return true;
                case "reason":
                    return true;
                case "osspGrants":
                    return true;
                case "osspGrantsView":
                    return true;
                case "osspPgo":
                    return true;
                case "developCondition":
                    return true;
                case "startDate":
                    return true;
                case "docRepresent":
                    return true;
                case "docExtract":
                    return true;
                case "saveGrants":
                    return true;
                case "grantsOld":
                    return true;
                case "statusOld":
                    return true;
                case "check":
                    return true;
                case "representID":
                    return true;
                case "creator":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "title":
                    return String.class;
                case "type":
                    return RepresentationType.class;
                case "createDate":
                    return Date.class;
                case "number":
                    return String.class;
                case "committed":
                    return Boolean.class;
                case "state":
                    return MovestudentExtractStates.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "reason":
                    return RepresentationReason.class;
                case "osspGrants":
                    return OsspGrants.class;
                case "osspGrantsView":
                    return OsspGrantsView.class;
                case "osspPgo":
                    return OsspPgo.class;
                case "developCondition":
                    return DevelopCondition.class;
                case "startDate":
                    return Date.class;
                case "docRepresent":
                    return byte[].class;
                case "docExtract":
                    return byte[].class;
                case "saveGrants":
                    return Boolean.class;
                case "grantsOld":
                    return String.class;
                case "statusOld":
                    return StudentStatus.class;
                case "check":
                    return Boolean.class;
                case "representID":
                    return Long.class;
                case "creator":
                    return IPrincipalContext.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Representation> _dslPath = new Path<Representation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Representation");
    }
            

    /**
     * @return Представление.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Тип представления. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getType()
     */
    public static RepresentationType.Path<RepresentationType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    /**
     * @return Номер представления.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Проведено. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#isCommitted()
     */
    public static PropertyPath<Boolean> committed()
    {
        return _dslPath.committed();
    }

    /**
     * @return Cостояние представления.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getState()
     */
    public static MovestudentExtractStates.Path<MovestudentExtractStates> state()
    {
        return _dslPath.state();
    }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Причина представления.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getReason()
     */
    public static RepresentationReason.Path<RepresentationReason> reason()
    {
        return _dslPath.reason();
    }

    /**
     * @return Стипендии ОССП.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getOsspGrants()
     */
    public static OsspGrants.Path<OsspGrants> osspGrants()
    {
        return _dslPath.osspGrants();
    }

    /**
     * @return Тип стипендии.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getOsspGrantsView()
     */
    public static OsspGrantsView.Path<OsspGrantsView> osspGrantsView()
    {
        return _dslPath.osspGrantsView();
    }

    /**
     * @return ПГО ОССП.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getOsspPgo()
     */
    public static OsspPgo.Path<OsspPgo> osspPgo()
    {
        return _dslPath.osspPgo();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getDevelopCondition()
     */
    public static DevelopCondition.Path<DevelopCondition> developCondition()
    {
        return _dslPath.developCondition();
    }

    /**
     * @return Дата вступления в силу.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getStartDate()
     */
    public static PropertyPath<Date> startDate()
    {
        return _dslPath.startDate();
    }

    /**
     * @return Представление в zip.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getDocRepresent()
     */
    public static PropertyPath<byte[]> docRepresent()
    {
        return _dslPath.docRepresent();
    }

    /**
     * @return Выписка в zip.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getDocExtract()
     */
    public static PropertyPath<byte[]> docExtract()
    {
        return _dslPath.docExtract();
    }

    /**
     * @return С сохранением стипендий/выплат. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#isSaveGrants()
     */
    public static PropertyPath<Boolean> saveGrants()
    {
        return _dslPath.saveGrants();
    }

    /**
     * @return Отмененные стипендии.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getGrantsOld()
     */
    public static PropertyPath<String> grantsOld()
    {
        return _dslPath.grantsOld();
    }

    /**
     * @return Старый статус студента.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getStatusOld()
     */
    public static StudentStatus.Path<StudentStatus> statusOld()
    {
        return _dslPath.statusOld();
    }

    /**
     * @return Проверка.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getCheck()
     */
    public static PropertyPath<Boolean> check()
    {
        return _dslPath.check();
    }

    /**
     * @return ID представления.
     *
     * Это формула "id".
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getRepresentID()
     */
    public static PropertyPath<Long> representID()
    {
        return _dslPath.representID();
    }

    /**
     * @return Создатель представления.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getCreator()
     */
    public static IPrincipalContextGen.Path<IPrincipalContext> creator()
    {
        return _dslPath.creator();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getTitleWithCreator()
     */
    public static SupportedPropertyPath<String> titleWithCreator()
    {
        return _dslPath.titleWithCreator();
    }

    public static class Path<E extends Representation> extends EntityPath<E>
    {
        private PropertyPath<String> _title;
        private RepresentationType.Path<RepresentationType> _type;
        private PropertyPath<Date> _createDate;
        private PropertyPath<String> _number;
        private PropertyPath<Boolean> _committed;
        private MovestudentExtractStates.Path<MovestudentExtractStates> _state;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private RepresentationReason.Path<RepresentationReason> _reason;
        private OsspGrants.Path<OsspGrants> _osspGrants;
        private OsspGrantsView.Path<OsspGrantsView> _osspGrantsView;
        private OsspPgo.Path<OsspPgo> _osspPgo;
        private DevelopCondition.Path<DevelopCondition> _developCondition;
        private PropertyPath<Date> _startDate;
        private PropertyPath<byte[]> _docRepresent;
        private PropertyPath<byte[]> _docExtract;
        private PropertyPath<Boolean> _saveGrants;
        private PropertyPath<String> _grantsOld;
        private StudentStatus.Path<StudentStatus> _statusOld;
        private PropertyPath<Boolean> _check;
        private PropertyPath<Long> _representID;
        private IPrincipalContextGen.Path<IPrincipalContext> _creator;
        private SupportedPropertyPath<String> _titleWithCreator;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Представление.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(RepresentationGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Тип представления. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getType()
     */
        public RepresentationType.Path<RepresentationType> type()
        {
            if(_type == null )
                _type = new RepresentationType.Path<RepresentationType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(RepresentationGen.P_CREATE_DATE, this);
            return _createDate;
        }

    /**
     * @return Номер представления.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(RepresentationGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Проведено. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#isCommitted()
     */
        public PropertyPath<Boolean> committed()
        {
            if(_committed == null )
                _committed = new PropertyPath<Boolean>(RepresentationGen.P_COMMITTED, this);
            return _committed;
        }

    /**
     * @return Cостояние представления.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getState()
     */
        public MovestudentExtractStates.Path<MovestudentExtractStates> state()
        {
            if(_state == null )
                _state = new MovestudentExtractStates.Path<MovestudentExtractStates>(L_STATE, this);
            return _state;
        }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Причина представления.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getReason()
     */
        public RepresentationReason.Path<RepresentationReason> reason()
        {
            if(_reason == null )
                _reason = new RepresentationReason.Path<RepresentationReason>(L_REASON, this);
            return _reason;
        }

    /**
     * @return Стипендии ОССП.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getOsspGrants()
     */
        public OsspGrants.Path<OsspGrants> osspGrants()
        {
            if(_osspGrants == null )
                _osspGrants = new OsspGrants.Path<OsspGrants>(L_OSSP_GRANTS, this);
            return _osspGrants;
        }

    /**
     * @return Тип стипендии.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getOsspGrantsView()
     */
        public OsspGrantsView.Path<OsspGrantsView> osspGrantsView()
        {
            if(_osspGrantsView == null )
                _osspGrantsView = new OsspGrantsView.Path<OsspGrantsView>(L_OSSP_GRANTS_VIEW, this);
            return _osspGrantsView;
        }

    /**
     * @return ПГО ОССП.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getOsspPgo()
     */
        public OsspPgo.Path<OsspPgo> osspPgo()
        {
            if(_osspPgo == null )
                _osspPgo = new OsspPgo.Path<OsspPgo>(L_OSSP_PGO, this);
            return _osspPgo;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getDevelopCondition()
     */
        public DevelopCondition.Path<DevelopCondition> developCondition()
        {
            if(_developCondition == null )
                _developCondition = new DevelopCondition.Path<DevelopCondition>(L_DEVELOP_CONDITION, this);
            return _developCondition;
        }

    /**
     * @return Дата вступления в силу.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getStartDate()
     */
        public PropertyPath<Date> startDate()
        {
            if(_startDate == null )
                _startDate = new PropertyPath<Date>(RepresentationGen.P_START_DATE, this);
            return _startDate;
        }

    /**
     * @return Представление в zip.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getDocRepresent()
     */
        public PropertyPath<byte[]> docRepresent()
        {
            if(_docRepresent == null )
                _docRepresent = new PropertyPath<byte[]>(RepresentationGen.P_DOC_REPRESENT, this);
            return _docRepresent;
        }

    /**
     * @return Выписка в zip.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getDocExtract()
     */
        public PropertyPath<byte[]> docExtract()
        {
            if(_docExtract == null )
                _docExtract = new PropertyPath<byte[]>(RepresentationGen.P_DOC_EXTRACT, this);
            return _docExtract;
        }

    /**
     * @return С сохранением стипендий/выплат. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#isSaveGrants()
     */
        public PropertyPath<Boolean> saveGrants()
        {
            if(_saveGrants == null )
                _saveGrants = new PropertyPath<Boolean>(RepresentationGen.P_SAVE_GRANTS, this);
            return _saveGrants;
        }

    /**
     * @return Отмененные стипендии.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getGrantsOld()
     */
        public PropertyPath<String> grantsOld()
        {
            if(_grantsOld == null )
                _grantsOld = new PropertyPath<String>(RepresentationGen.P_GRANTS_OLD, this);
            return _grantsOld;
        }

    /**
     * @return Старый статус студента.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getStatusOld()
     */
        public StudentStatus.Path<StudentStatus> statusOld()
        {
            if(_statusOld == null )
                _statusOld = new StudentStatus.Path<StudentStatus>(L_STATUS_OLD, this);
            return _statusOld;
        }

    /**
     * @return Проверка.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getCheck()
     */
        public PropertyPath<Boolean> check()
        {
            if(_check == null )
                _check = new PropertyPath<Boolean>(RepresentationGen.P_CHECK, this);
            return _check;
        }

    /**
     * @return ID представления.
     *
     * Это формула "id".
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getRepresentID()
     */
        public PropertyPath<Long> representID()
        {
            if(_representID == null )
                _representID = new PropertyPath<Long>(RepresentationGen.P_REPRESENT_I_D, this);
            return _representID;
        }

    /**
     * @return Создатель представления.
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getCreator()
     */
        public IPrincipalContextGen.Path<IPrincipalContext> creator()
        {
            if(_creator == null )
                _creator = new IPrincipalContextGen.Path<IPrincipalContext>(L_CREATOR, this);
            return _creator;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.Representation#getTitleWithCreator()
     */
        public SupportedPropertyPath<String> titleWithCreator()
        {
            if(_titleWithCreator == null )
                _titleWithCreator = new SupportedPropertyPath<String>(RepresentationGen.P_TITLE_WITH_CREATOR, this);
            return _titleWithCreator;
        }

        public Class getEntityClass()
        {
            return Representation.class;
        }

        public String getEntityName()
        {
            return "representation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTitleWithCreator();
}
