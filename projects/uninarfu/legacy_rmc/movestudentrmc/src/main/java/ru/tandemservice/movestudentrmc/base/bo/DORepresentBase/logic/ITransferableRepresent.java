package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic;

import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;

public interface ITransferableRepresent extends IEntity {

    public Course getCourse();

    public void setCourse(Course course);

    public Group getGroup();

    public void setGroup(Group group);

    public Long getOldCourseId();

    public void setOldCourseId(Long id);

    public Long getOldGroupId();

    public void setOldGroupId(Long id);

    public EppWorkPlan getWorkPlan();

    public void setWorkPlan(EppWorkPlan workPlan);

    public EppStudent2WorkPlan getWorkPlanRel();

    public void setWorkPlanRel(EppStudent2WorkPlan workPlan);

    public Long getOldEduPlanRelId();

    public void setOldEduPlanRelId(Long id);

    //public Long getOldWorkPlanRelId();
    //public void setOldWorkPlanRelId(Long id);

    public String getOldWorkPlanRelIds();

    public void setOldWorkPlanRelIds(String oldIds);
}
