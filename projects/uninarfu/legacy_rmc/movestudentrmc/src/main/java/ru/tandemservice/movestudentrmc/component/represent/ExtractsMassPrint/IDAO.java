package ru.tandemservice.movestudentrmc.component.represent.ExtractsMassPrint;

import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {

    public RtfDocument printListExtracts(Model model);

    public RtfDocument printIndividualExtracts(Model model);

}
