package ru.tandemservice.movestudentrmc.base.bo.DORepresentChangeName.ui.Edit;

import org.tandemframework.core.component.Activator;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.bo.Person.util.IssuancePlaceSelectModel;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentEditUI;
import ru.tandemservice.movestudentrmc.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentIC;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.uni.dao.UniDaoFacade;

import java.io.IOException;
import java.util.List;

public class DORepresentChangeNameEditUI extends AbstractDORepresentEditUI
{

    private String newFIOStr;
    private IdentityCard ic;

    @Override
    public void onComponentRefresh() {
        super.onComponentRefresh();
        if (_representObj != null) {
            List<DocRepresentStudentIC> list = MoveStudentDaoFacade.getDocumentDAO().getIdentityCards(_representObj.getId());
            if (!list.isEmpty()) {
                ic = list.get(0).getStudentIC();
                setNewFIOStr(ic.getFullFio());
                getRepresentIdentityCardList().addAll(list);
            }
            else if (!getRepresentIdentityCardList().isEmpty()) {
                ic = UniDaoFacade.getCoreDao().get(IdentityCard.class, getRepresentIdentityCardList().get(0).getStudentIC().getId());
                if (ic != null) {
                    getRepresentIdentityCardList().get(0).setStudentIC(ic);
                    setNewFIOStr(ic.getFullFio());
                }
            }
        }
    }

    @Override
    public void onComponentDeactivate() {
        //если "передумали" формировать представление и добавили уже УЛ, то при деактивации компонента удаляем УЛ
        if (!getRepresentIdentityCardList().isEmpty()) {
            DocRepresentStudentIC rel = getRepresentIdentityCardList().get(0);
            if (rel.getId() <= 0)
                PersonManager.instance().dao().deleteIdentityCard(rel.getStudentIC().getId());
        }

    }

    @Override
    public void onClickSave() throws IOException {
        if (getRepresentIdentityCardList().size() == 0) {
            ContextLocal.getInfoCollector().add("Удостоверение Личности обязательно для заполнения.");
            return;
        }
        else {
            super.onClickSave();
        }
    }

    @Override
    public void deactivate() {
        //ситуация, кодга при редактировании представления удалили УЛ студента и нажимаем кнопку "Отмена"
        if (_representId != null) {
            if (getRepresentIdentityCardList().size() == 0) {
                ContextLocal.getInfoCollector().add("Удостоверение Личности обязательно для заполнения.");
                return;
            }
        }
        super.deactivate();
    }

    @Override
    protected void FetchStudents(Representation doc) {

        _studentDocList.clear();
        _studentList.clear();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocRepresentStudentBase.class, "stud").column("stud");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(DocRepresentStudentBase.representation().fromAlias("stud")),
                DQLExpressions.value(doc.getId())));

        List<DocRepresentStudentBase> studentList = builder.createStatement(getSupport().getSession()).list();

        for (DocRepresentStudentBase student : studentList) {

            _studentDocList.add(student);
            _studentList.add(student.getStudent());
        }

        _student = _studentList.get(0);
    }

    @Override
    public void onClickDeleteIC() {
        List<DocRepresentStudentIC> list = MoveStudentDaoFacade.getDocumentDAO().getIdentityCards(_representObj.getId());
        if (!list.isEmpty()) {
            DocRepresentStudentIC sic = list.get(0);
            DataAccessServices.dao().delete(sic);
        }
        PersonManager.instance().dao().deleteIdentityCard(ic.getId());
        setIc(null);
        getRepresentIdentityCardList().clear();
        setNewFIOStr("");
    }

    public void onClickEditIC() {
        Activator activator = new ComponentActivator(
                "org.tandemframework.shared.person.base.bo.Person.ui.IdentityCardAddEdit",
                new ParametersMap().add("identityCardId", ic.getId())
                        .add("publisherId", _student.getPerson().getId())
                        .add("birthDateCheck", Boolean.TRUE)
                        .add("represent", true).add("list", getRepresentIdentityCardList())
        );
        UserContext.getInstance().activateDesktop("PersonShellDialog", activator);
    }

    public boolean isButtonVisible() {
        return ic == null;
    }

    public ISelectModel getIssuancePlaceHandler() {

        return new IssuancePlaceSelectModel();
    }

    public String getNewFIOStr() {
        return newFIOStr;
    }

    public void setNewFIOStr(String newFIOStr) {
        this.newFIOStr = newFIOStr;
    }

    public IdentityCard getIc() {
        return ic;
    }

    public void setIc(IdentityCard ic) {
        this.ic = ic;
    }


}
