package ru.tandemservice.movestudentrmc.component.catalog.representationReason.RepresentationReasonPub;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonBasic;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;

public class DAO extends DefaultCatalogPubDAO<RepresentationReason, Model> implements IDAO {

    public void updatePrintable(Model model, Long id) {
        RepresentationReason representationReason = getNotNull(RepresentationReason.class, id);
        if (representationReason.isPrintable()) {
            representationReason.setPrintable(false);
        }
        else {
            representationReason.setPrintable(true);
        }
        save(representationReason);
    }

    public void updateUsed(Model model, Long id) {
        RepresentationReason representationReason = getNotNull(RepresentationReason.class, id);
        representationReason.setUsed(!representationReason.isUsed());
        save(representationReason);
    }

    public boolean checkRelationReason(Long reasonId)
    {
        boolean result = false;

        MQBuilder builder = new MQBuilder(RelRepresentationReasonBasic.ENTITY_CLASS, "rrrb");
        builder.addSelect("rrrb", new String[]{RelRepresentationReasonBasic.reason().id().s()});
        builder.add(MQExpression.eq("rrrb", "reason.id", (Object) reasonId));
        result = builder.getResultCount(getSession()) > 0;

        return result;
    }
}
