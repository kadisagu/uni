package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_movestudentrmc_1x0x0_50to51 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.12"),
                        new ScriptDependency("org.tandemframework.shared", "1.2.5"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.2.5")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность personNARFU

        // создано свойство addressTempRegistrationInfo
        {
            // создать колонку
            tool.createColumn("personnarfu_t", new DBColumn("addresstempregistrationinfo_p", DBType.createVarchar(255)));

        }

        // создано свойство addressRegistrationInfo
        {
            // создать колонку
            tool.createColumn("personnarfu_t", new DBColumn("addressregistrationinfo_p", DBType.createVarchar(255)));

        }

        // создано свойство addressInfo
        {
            // создать колонку
            tool.createColumn("personnarfu_t", new DBColumn("addressinfo_p", DBType.createVarchar(255)));

        }


    }
}