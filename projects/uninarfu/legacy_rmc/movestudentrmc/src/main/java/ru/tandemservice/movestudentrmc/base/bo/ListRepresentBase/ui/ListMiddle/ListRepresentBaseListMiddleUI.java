package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ui.ListMiddle;

import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.ListRepresentDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.support.AbstractListRepresentBaseListUI;

import java.util.HashMap;
import java.util.Map;

public class ListRepresentBaseListMiddleUI extends AbstractListRepresentBaseListUI
{

    @Override
    protected Map<String, Object> getParams()
    {
        Map<String, Object> map = new HashMap<>();

        map.put(ListRepresentDSHandler.HIGH, Boolean.FALSE);
        map.put(ListRepresentDSHandler.MIDDLE, Boolean.TRUE);
        map.put(ListRepresentDSHandler.POSTGRADUATE, Boolean.FALSE);
        map.put(ListRepresentDSHandler.HIGH_BRANCH, Boolean.FALSE);
        map.put(ListRepresentDSHandler.MIDDLE_BRANCH, Boolean.FALSE);

        return map;
    }

}
