package ru.tandemservice.movestudentrmc.base.bo.DORepresentDiplomAndExclude.ui.Edit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentEditUI;
import ru.tandemservice.movestudentrmc.docord.util.MoveStudentUtil;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.NarfuDocument;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.util.ListenerWrapper;
import ru.tandemservice.uni.dao.UniDaoFacade;

import java.io.IOException;
import java.util.List;

public class DORepresentDiplomAndExcludeEditUI extends AbstractDORepresentEditUI implements ListenerWrapper.IListener<NarfuDocument>
{

    @Override
    protected void initDocument() {
        super.initDocument();
        _studentStatusNewStr = UniDaoFacade.getCoreDao().getCatalogItem(ru.tandemservice.uni.entity.catalog.StudentStatus.class, "3").getTitle();
    }

    @Override
    protected void FetchStudents(Representation doc) {

        _studentDocList.clear();
        _studentList.clear();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocRepresentStudentBase.class, "stud").column("stud");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(DocRepresentStudentBase.representation().fromAlias("stud")),
                DQLExpressions.value(doc.getId())));

        List<DocRepresentStudentBase> studentList = builder.createStatement(getSupport().getSession()).list();

        for (DocRepresentStudentBase student : studentList) {

            _studentDocList.add(student);
            _studentList.add(student.getStudent());
        }

        _student = _studentList.get(0);
    }

    @Override
    public void onClickSave() throws IOException {

        boolean ok = MoveStudentUtil.isChecked(getStudentList(), null, null);

        if ((ok && !isSave()) || (ok && isSave() && isWarning())) {
            String mess = MoveStudentUtil.checked(getRepresentObj().getStartDate(), true);
            if (!StringUtils.isEmpty(mess)) {
                setSave(true);
                setWarning(true);
                setWarningMessage(mess);
                return;
            }
        }

        super.onClickSave();
    }

}
