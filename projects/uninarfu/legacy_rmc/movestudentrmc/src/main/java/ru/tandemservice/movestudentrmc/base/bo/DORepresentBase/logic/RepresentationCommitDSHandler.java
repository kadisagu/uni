package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudentrmc.entity.DocOrdRepresent;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.DocumentOrder;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.uni.util.FilterUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RepresentationCommitDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput> {

    public static final String STUDENT_ID_FILTER = "studentId";
    public static final String ORG_UNIT_ID_FILTER = "orgUnitId";
    public static final String TYPE_REPRESENT_FILTER = "typeRepresentFilter";
    public static final String DATE_FORMATIVE_FROM_FILTER = "dateFormativeFromFilter";
    public static final String DATE_COMMIT_FROM_FILTER = "dateCommitFromFilter";
    public static final String DATE_FORMATIVE_TO_FILTER = "dateFormativeToFilter";
    public static final String DATE_COMMIT_TO_FILTER = "dateCommitToFilter";
    public static final String DATE_START_FROM_FILTER = "dateStartFromFilter";
    public static final String DATE_START_TO_FILTER = "dateStartToFilter";

    public static final String REPRESENT_COLUMN = "docOrdRepresent.representation.type.title";
    public static final String CREATE_DATE_COLUMN = "docOrdRepresent.representation.createDate";
    public static final String NUMBER_COLUMN = "docOrdRepresent.representation.number";
    public static final String COMMIT_DATE_COLUMN = "documentOrder.commitDateSystem";
    public static final String ORDER_COLUMN = "documentOrder.number";
    public static final String GROUP_COLUMN = "stud.student.group.title";
    public static final String STUDENT_COLUMN = "stud.student.person.identityCard.fullFio";
    public static final String CHECK_COLUMN = "docOrdRepresent.representation.check";

    public RepresentationCommitDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {

        Long studentId = (Long) context.get(STUDENT_ID_FILTER);
        Long orgUnitId = (Long) context.get(ORG_UNIT_ID_FILTER);
        RepresentationType typeRepresentFilter = (RepresentationType) context.get(TYPE_REPRESENT_FILTER);
        Date dateCommitFromFilter = (Date) context.get(DATE_COMMIT_FROM_FILTER);
        Date dateFormativeFromFilter = (Date) context.get(DATE_FORMATIVE_FROM_FILTER);
        Date dateCommitToFilter = (Date) context.get(DATE_COMMIT_TO_FILTER);
        Date dateFormativeToFilter = (Date) context.get(DATE_FORMATIVE_TO_FILTER);
        Date dateStartFromFilter = (Date) context.get(DATE_START_FROM_FILTER);
        Date dateStartToFilter = (Date) context.get(DATE_START_TO_FILTER);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocOrdRepresent.class, DocOrdRepresent.ENTITY_NAME).addColumn(DocOrdRepresent.ENTITY_NAME);

        builder.joinEntity(DocOrdRepresent.ENTITY_NAME, DQLJoinType.left, DocRepresentStudentBase.class, "stud",
                           DQLExpressions.eq(DQLExpressions.property(DocOrdRepresent.representation().id().fromAlias(DocOrdRepresent.ENTITY_NAME)),
                                             DQLExpressions.property(DocRepresentStudentBase.representation().fromAlias("stud"))));
        builder.addColumn("stud");


        builder.joinEntity(DocOrdRepresent.ENTITY_NAME, DQLJoinType.inner, DocumentOrder.class, "documentOrder",
                           DQLExpressions.eq(DQLExpressions.property(DocOrdRepresent.order().id().fromAlias(DocOrdRepresent.ENTITY_NAME)),
                                             DQLExpressions.property(DocumentOrder.id().fromAlias("documentOrder"))));
        builder.addColumn("documentOrder");

        if (studentId != null) {
            builder.where(DQLExpressions.eq(DQLExpressions.property(DocRepresentStudentBase.student().id().fromAlias("stud")), DQLExpressions.value(studentId)));
        }

        if (orgUnitId != null) {
            OrgUnit orgUnit = DataAccessServices.dao().get(OrgUnit.class, orgUnitId);

            IDQLExpression expression = FilterUtils.getEducationOrgUnitFilter(DocRepresentStudentBase.student().educationOrgUnit().fromAlias("stud").s(), orgUnit);
            builder.where(null == expression ? DQLExpressions.eq(DQLExpressions.property("stud.id"), DQLExpressions.value(-1L)) : expression);
        }

        builder.where(DQLExpressions.eq(
                DQLExpressions.property(DocOrdRepresent.representation().state().code().fromAlias(DocOrdRepresent.ENTITY_NAME)),
                DQLExpressions.value("6")
        ));

        if (typeRepresentFilter != null)
            builder.where(DQLExpressions.eq(
                    DQLExpressions.property(DocOrdRepresent.representation().type().fromAlias(DocOrdRepresent.ENTITY_NAME)),
                    DQLExpressions.value(typeRepresentFilter)
            ));

        //Период проведения
        if (dateCommitFromFilter != null && dateCommitToFilter == null)
            builder.where(DQLExpressions.ge(
                    DQLExpressions.property(DocOrdRepresent.order().commitDateSystem().fromAlias(DocOrdRepresent.ENTITY_NAME)),
                    DQLExpressions.valueDate(dateCommitFromFilter)
            ));

        if (dateCommitFromFilter == null && dateCommitToFilter != null)
            builder.where(DQLExpressions.le(
                    DQLExpressions.property(DocOrdRepresent.order().commitDateSystem().fromAlias(DocOrdRepresent.ENTITY_NAME)),
                    DQLExpressions.valueDate(dateCommitToFilter)
            ));

        if (dateCommitFromFilter != null && dateCommitToFilter != null)
            builder.where(DQLExpressions.between(
                    DQLExpressions.property(DocOrdRepresent.order().commitDateSystem().fromAlias(DocOrdRepresent.ENTITY_NAME)),
                    DQLExpressions.valueDate(dateCommitFromFilter),
                    DQLExpressions.valueDate(dateCommitToFilter)
            ));

        //Период создания
        if (dateFormativeFromFilter != null && dateFormativeToFilter == null)
            builder.where(DQLExpressions.ge(
                    DQLExpressions.property(DocOrdRepresent.representation().createDate().fromAlias(DocOrdRepresent.ENTITY_NAME)),
                    DQLExpressions.valueDate(dateFormativeFromFilter)
            ));

        if (dateFormativeFromFilter == null && dateFormativeToFilter != null)
            builder.where(DQLExpressions.le(
                    DQLExpressions.property(DocOrdRepresent.representation().createDate().fromAlias(DocOrdRepresent.ENTITY_NAME)),
                    DQLExpressions.valueDate(dateFormativeToFilter)
            ));

        if (dateFormativeFromFilter != null && dateFormativeToFilter != null)
            builder.where(DQLExpressions.between(
                    DQLExpressions.property(DocOrdRepresent.representation().createDate().fromAlias(DocOrdRepresent.ENTITY_NAME)),
                    DQLExpressions.valueDate(dateFormativeFromFilter),
                    DQLExpressions.valueDate(dateFormativeToFilter)
            ));

        //Период вступления в силу
        if (dateStartFromFilter != null && dateStartToFilter == null)
            builder.where(DQLExpressions.ge(
                    DQLExpressions.property(DocOrdRepresent.order().commitDate().fromAlias(DocOrdRepresent.ENTITY_NAME)),
                    DQLExpressions.valueDate(dateStartFromFilter)
            ));

        if (dateStartFromFilter == null && dateStartToFilter != null)
            builder.where(DQLExpressions.le(
                    DQLExpressions.property(DocOrdRepresent.order().commitDate().fromAlias(DocOrdRepresent.ENTITY_NAME)),
                    DQLExpressions.valueDate(dateStartToFilter)
            ));

        if (dateStartFromFilter != null && dateStartToFilter != null)
            builder.where(DQLExpressions.between(
                    DQLExpressions.property(DocOrdRepresent.order().commitDate().fromAlias(DocOrdRepresent.ENTITY_NAME)),
                    DQLExpressions.valueDate(dateStartFromFilter),
                    DQLExpressions.valueDate(dateStartToFilter)
            ));

        if (input.getEntityOrder() != null)
            builder.order(DQLExpressions.property(input.getEntityOrder().getColumnName()), input.getEntityOrder().getDirection());

        List<Object[]> rowList = builder.createStatement(context.getSession()).list();
        List<DataWrapper> resultList = new ArrayList<DataWrapper>();

        for (Object[] row : rowList) {
            DocOrdRepresent orderData = (DocOrdRepresent) row[0];
            DocRepresentStudentBase studentData = (DocRepresentStudentBase) row[1];

            DataWrapper w = new DataWrapper(studentData.getRepresentation().getId(), studentData.getRepresentation().getTitle(), studentData.getRepresentation());

            w.setProperty(DocOrdRepresent.ENTITY_NAME, orderData);
            w.setProperty(DocRepresentStudentBase.ENTITY_NAME, studentData);

            resultList.add(w);
        }

        return ListOutputBuilder.get(input, resultList).pageable(true).build();
    }
}
