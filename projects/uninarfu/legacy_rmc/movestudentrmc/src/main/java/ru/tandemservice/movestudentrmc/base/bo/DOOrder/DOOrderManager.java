package ru.tandemservice.movestudentrmc.base.bo.DOOrder;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.logic.DOOrderManagerModifyDAO;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.logic.IDOOrderManagerModifyDAO;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.logic.IOrderVisaDAO;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.logic.OrderVisaDAO;

@Configuration
public class DOOrderManager extends BusinessObjectManager {

    public static DOOrderManager instance()
    {
        return instance(DOOrderManager.class);
    }

    @Bean
    public IDOOrderManagerModifyDAO modifyDao()
    {
        return new DOOrderManagerModifyDAO();
    }

    @Bean
    public IOrderVisaDAO visaDao()
    {
        return new OrderVisaDAO();
    }
}
