package ru.tandemservice.movestudentrmc.component.catalog.grantType.GrantTypePub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantType;

public class DAO extends DefaultCatalogPubDAO<GrantType, Model> implements IDAO {

    @Override
    public void updateAcadem(Model model, Long id) {
        GrantType type = getNotNull(GrantType.class, id);
        if (type.isAcadem()) {
            type.setAcadem(false);
        }
        else {
            type.setAcadem(true);
        }
        save(type);
    }

    @Override
    public void updateSocial(Model model, Long id) {
        GrantType type = getNotNull(GrantType.class, id);
        if (type.isSocial()) {
            type.setSocial(false);
        }
        else {
            type.setSocial(true);
        }
        save(type);
    }

}
