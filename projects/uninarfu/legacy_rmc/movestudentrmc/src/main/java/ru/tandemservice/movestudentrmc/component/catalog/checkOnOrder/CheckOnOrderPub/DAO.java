package ru.tandemservice.movestudentrmc.component.catalog.checkOnOrder.CheckOnOrderPub;

import com.google.common.collect.ImmutableList;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.movestudentrmc.entity.catalog.CheckOnOrder;

public class DAO extends DefaultCatalogPubDAO<CheckOnOrder, Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        super.prepare(model);
        model.setUsedModel(new LazySimpleSelectModel<>(ImmutableList.of(
                new IdentifiableWrapper(Model.SHOW_USE_CODE, "Да"),
                new IdentifiableWrapper(Model.SHOW_NON_USE_CODE, "Нет"))));
    }

    public void updateInUse(Model model, Long id) {
        CheckOnOrder checkOnOrder = getNotNull(CheckOnOrder.class, id);
        if (checkOnOrder.isUsed()) {
            checkOnOrder.setUsed(false);
        }
        else {
            checkOnOrder.setUsed(true);
        }
        save(checkOnOrder);
    }

    @Override
    protected void applyFilters(Model model, MQBuilder builder) {
        super.applyFilters(model, builder);

        IdentifiableWrapper used = (IdentifiableWrapper) model.getSettings().get("used");

        if (used != null) {
            builder.add(MQExpression.eq("ci", "used", used.getId().equals(Model.SHOW_USE_CODE)));
        }
    }
}
