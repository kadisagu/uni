package ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab;

import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudentrmc.entity.GrantEntity;
import ru.tandemservice.movestudentrmc.entity.RelGrantViewOrgUnit;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(final Model model) {
        model.setOrgUnit(getNotNull(OrgUnit.class, model.getOrgUnit().getId()));
        model.setEduYearList(getCatalogItemList(EducationYear.class));
        if (model.getEduYear() == null)
            model.setEduYear(get(EducationYear.class, EducationYear.current().s(), Boolean.valueOf(true)));

        Calendar c = Calendar.getInstance();
        c.get(Calendar.MONTH);
        List<MonthWrapper> months = new ArrayList<MonthWrapper>();
        for (int i = 0; i < 12; i++) {
            MonthWrapper wrapper = new MonthWrapper(i);
            if (model.getMonth() == null && wrapper.getMonthNumber() == c.get(Calendar.MONTH))
                model.setMonth(wrapper);

            months.add(wrapper);
        }
        model.setMonthModel(months);
    }

    @Override
    public void prepareListDataSource(Model model) {
        List<RelGrantViewOrgUnit> relList = getList(RelGrantViewOrgUnit.class, RelGrantViewOrgUnit.second(), model.getOrgUnit());

        List<GrantWrapper> result = new ArrayList<GrantWrapper>();
        long fakeId = 1L;
        for (RelGrantViewOrgUnit rel : relList) {
            MQBuilder builder = new MQBuilder(GrantEntity.ENTITY_CLASS, "ge")
                    .add(MQExpression.eq("ge", GrantEntity.orgUnit(), model.getOrgUnit()))
                    .add(MQExpression.eq("ge", GrantEntity.view(), rel.getFirst()))
                    .add(MQExpression.eq("ge", GrantEntity.eduYear(), model.getEduYear()))
                    .add(MQExpression.eq("ge", GrantEntity.month(), model.getMonth().getStringValue(model.getEduYear())));

            //TODO: #2028, в базе както получилось что лежит две записи.
            //пока берем первую попавшуюся. потом надо разобраться почему так происходит
            //GrantEntity grantEntity = (GrantEntity)builder.uniqueResult(getSession());
            List<GrantEntity> grantEntityList = builder.getResultList(getSession());
            GrantEntity grantEntity = grantEntityList.isEmpty() ? null : grantEntityList.get(0);

            if (grantEntity == null) {
                grantEntity = new GrantEntity();
                grantEntity.setOrgUnit(model.getOrgUnit());
                grantEntity.setView(rel.getFirst());
                grantEntity.setEduYear(model.getEduYear());
                grantEntity.setMonth(model.getMonth().getStringValue(model.getEduYear()));
            }

            GrantWrapper wrapper = new GrantWrapper(grantEntity);
            wrapper.setId(fakeId++);
            result.add(wrapper);
        }

        Collections.sort(result, new EntityComparator<GrantWrapper>(model.getDataSource().getEntityOrder()));
        UniUtils.createPage(model.getDataSource(), result);
    }

    @Override
    public void deleteRow(GrantEntity grant) {
        delete(grant);
    }
}
