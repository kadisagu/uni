package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.RelOrderCategoryRepresentationType;
import ru.tandemservice.movestudentrmc.entity.catalog.OrderCategory;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность-связь Категория приказа-Типы представлений
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RelOrderCategoryRepresentationTypeGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.RelOrderCategoryRepresentationType";
    public static final String ENTITY_NAME = "relOrderCategoryRepresentationType";
    public static final int VERSION_HASH = -1924731275;
    private static IEntityMeta ENTITY_META;

    public static final String L_FIRST = "first";
    public static final String L_SECOND = "second";

    private OrderCategory _first;     // Категория приказа
    private RepresentationType _second;     // Типы представлений

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Категория приказа. Свойство не может быть null.
     */
    @NotNull
    public OrderCategory getFirst()
    {
        return _first;
    }

    /**
     * @param first Категория приказа. Свойство не может быть null.
     */
    public void setFirst(OrderCategory first)
    {
        dirty(_first, first);
        _first = first;
    }

    /**
     * @return Типы представлений. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public RepresentationType getSecond()
    {
        return _second;
    }

    /**
     * @param second Типы представлений. Свойство не может быть null и должно быть уникальным.
     */
    public void setSecond(RepresentationType second)
    {
        dirty(_second, second);
        _second = second;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RelOrderCategoryRepresentationTypeGen)
        {
            setFirst(((RelOrderCategoryRepresentationType)another).getFirst());
            setSecond(((RelOrderCategoryRepresentationType)another).getSecond());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RelOrderCategoryRepresentationTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RelOrderCategoryRepresentationType.class;
        }

        public T newInstance()
        {
            return (T) new RelOrderCategoryRepresentationType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "first":
                    return obj.getFirst();
                case "second":
                    return obj.getSecond();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "first":
                    obj.setFirst((OrderCategory) value);
                    return;
                case "second":
                    obj.setSecond((RepresentationType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "first":
                        return true;
                case "second":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "first":
                    return true;
                case "second":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "first":
                    return OrderCategory.class;
                case "second":
                    return RepresentationType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RelOrderCategoryRepresentationType> _dslPath = new Path<RelOrderCategoryRepresentationType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RelOrderCategoryRepresentationType");
    }
            

    /**
     * @return Категория приказа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelOrderCategoryRepresentationType#getFirst()
     */
    public static OrderCategory.Path<OrderCategory> first()
    {
        return _dslPath.first();
    }

    /**
     * @return Типы представлений. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.RelOrderCategoryRepresentationType#getSecond()
     */
    public static RepresentationType.Path<RepresentationType> second()
    {
        return _dslPath.second();
    }

    public static class Path<E extends RelOrderCategoryRepresentationType> extends EntityPath<E>
    {
        private OrderCategory.Path<OrderCategory> _first;
        private RepresentationType.Path<RepresentationType> _second;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Категория приказа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelOrderCategoryRepresentationType#getFirst()
     */
        public OrderCategory.Path<OrderCategory> first()
        {
            if(_first == null )
                _first = new OrderCategory.Path<OrderCategory>(L_FIRST, this);
            return _first;
        }

    /**
     * @return Типы представлений. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.RelOrderCategoryRepresentationType#getSecond()
     */
        public RepresentationType.Path<RepresentationType> second()
        {
            if(_second == null )
                _second = new RepresentationType.Path<RepresentationType>(L_SECOND, this);
            return _second;
        }

        public Class getEntityClass()
        {
            return RelOrderCategoryRepresentationType.class;
        }

        public String getEntityName()
        {
            return "relOrderCategoryRepresentationType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
