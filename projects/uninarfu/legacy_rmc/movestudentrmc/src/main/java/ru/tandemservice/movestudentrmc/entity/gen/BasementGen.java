package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudentrmc.entity.Basement;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationBasement;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Основания для представления
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class BasementGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.Basement";
    public static final String ENTITY_NAME = "basement";
    public static final int VERSION_HASH = 1694513467;
    private static IEntityMeta ENTITY_META;

    public static final String L_BASEMENT = "basement";
    public static final String P_BASEMENT_DATE = "basementDate";
    public static final String P_BASEMENT_NUMBER = "basementNumber";

    private RepresentationBasement _basement;     // Основания представлений
    private Date _basementDate;     // Дата основания
    private String _basementNumber;     // Номер основания

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Основания представлений. Свойство не может быть null.
     */
    @NotNull
    public RepresentationBasement getBasement()
    {
        return _basement;
    }

    /**
     * @param basement Основания представлений. Свойство не может быть null.
     */
    public void setBasement(RepresentationBasement basement)
    {
        dirty(_basement, basement);
        _basement = basement;
    }

    /**
     * @return Дата основания.
     */
    public Date getBasementDate()
    {
        return _basementDate;
    }

    /**
     * @param basementDate Дата основания.
     */
    public void setBasementDate(Date basementDate)
    {
        dirty(_basementDate, basementDate);
        _basementDate = basementDate;
    }

    /**
     * @return Номер основания.
     */
    @Length(max=255)
    public String getBasementNumber()
    {
        return _basementNumber;
    }

    /**
     * @param basementNumber Номер основания.
     */
    public void setBasementNumber(String basementNumber)
    {
        dirty(_basementNumber, basementNumber);
        _basementNumber = basementNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof BasementGen)
        {
            setBasement(((Basement)another).getBasement());
            setBasementDate(((Basement)another).getBasementDate());
            setBasementNumber(((Basement)another).getBasementNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends BasementGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Basement.class;
        }

        public T newInstance()
        {
            return (T) new Basement();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "basement":
                    return obj.getBasement();
                case "basementDate":
                    return obj.getBasementDate();
                case "basementNumber":
                    return obj.getBasementNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "basement":
                    obj.setBasement((RepresentationBasement) value);
                    return;
                case "basementDate":
                    obj.setBasementDate((Date) value);
                    return;
                case "basementNumber":
                    obj.setBasementNumber((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "basement":
                        return true;
                case "basementDate":
                        return true;
                case "basementNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "basement":
                    return true;
                case "basementDate":
                    return true;
                case "basementNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "basement":
                    return RepresentationBasement.class;
                case "basementDate":
                    return Date.class;
                case "basementNumber":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Basement> _dslPath = new Path<Basement>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Basement");
    }
            

    /**
     * @return Основания представлений. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.Basement#getBasement()
     */
    public static RepresentationBasement.Path<RepresentationBasement> basement()
    {
        return _dslPath.basement();
    }

    /**
     * @return Дата основания.
     * @see ru.tandemservice.movestudentrmc.entity.Basement#getBasementDate()
     */
    public static PropertyPath<Date> basementDate()
    {
        return _dslPath.basementDate();
    }

    /**
     * @return Номер основания.
     * @see ru.tandemservice.movestudentrmc.entity.Basement#getBasementNumber()
     */
    public static PropertyPath<String> basementNumber()
    {
        return _dslPath.basementNumber();
    }

    public static class Path<E extends Basement> extends EntityPath<E>
    {
        private RepresentationBasement.Path<RepresentationBasement> _basement;
        private PropertyPath<Date> _basementDate;
        private PropertyPath<String> _basementNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Основания представлений. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.Basement#getBasement()
     */
        public RepresentationBasement.Path<RepresentationBasement> basement()
        {
            if(_basement == null )
                _basement = new RepresentationBasement.Path<RepresentationBasement>(L_BASEMENT, this);
            return _basement;
        }

    /**
     * @return Дата основания.
     * @see ru.tandemservice.movestudentrmc.entity.Basement#getBasementDate()
     */
        public PropertyPath<Date> basementDate()
        {
            if(_basementDate == null )
                _basementDate = new PropertyPath<Date>(BasementGen.P_BASEMENT_DATE, this);
            return _basementDate;
        }

    /**
     * @return Номер основания.
     * @see ru.tandemservice.movestudentrmc.entity.Basement#getBasementNumber()
     */
        public PropertyPath<String> basementNumber()
        {
            if(_basementNumber == null )
                _basementNumber = new PropertyPath<String>(BasementGen.P_BASEMENT_NUMBER, this);
            return _basementNumber;
        }

        public Class getEntityClass()
        {
            return Basement.class;
        }

        public String getEntityName()
        {
            return "basement";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
