package ru.tandemservice.movestudentrmc.entity;

import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.movestudentrmc.entity.gen.GrantEntityGen;

/**
 * Сущность стипендия/выплата
 */
public class GrantEntity extends GrantEntityGen
{
    public String getTitle() {
        return this.getView().getTitle() + " " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(this.getSum()) + " руб.";
    }
}