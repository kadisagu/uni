package ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrant.ui.View;

import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.support.AbstractListRepresentViewUI;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrant.logic.ListPrintDoc;
import ru.tandemservice.movestudentrmc.entity.ListRepresentGrant;

import java.util.Collections;

public class ListRepresentGrantViewUI extends AbstractListRepresentViewUI<ListRepresentGrant> {

    @Override
    public ListRepresentGrant getListRepresentObject() {
        return new ListRepresentGrant();
    }

    @Override
    protected RtfDocument printRepresent() {
        return new ListPrintDoc().creationPrintDocOrder(Collections.singletonList(getListRepresent()));
    }

}
