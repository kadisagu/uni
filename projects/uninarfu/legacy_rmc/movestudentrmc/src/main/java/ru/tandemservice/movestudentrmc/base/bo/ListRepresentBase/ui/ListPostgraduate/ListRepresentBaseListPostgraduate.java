package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ui.ListPostgraduate;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import ru.tandemservice.movestudentrmc.base.bo.ListOrder.logic.TypeRepresentDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.*;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.entity.employee.Student;

@Configuration
public class ListRepresentBaseListPostgraduate extends BusinessComponentManager
{

    public static final String LIST_REPRESENT_DS = "listRepresentDS";
    public static final String FORMATIVE_ORG_UNIT_DS = "formativeOrgUnitDS";
    public static final String TYPE_REPRESENT_DS = "typeRepresentPostgraduateDS";
    public static final String STATE_VIEW_DS = "stateViewDS";
    public static final String STUDENT_COMBO_DS = "studentComboDS";
    public static final String GRANT_VIEW_DS = "grantViewDS";

    @Bean
    public ColumnListExtPoint listRepresentDS()
    {
        return columnListExtPointBuilder(LIST_REPRESENT_DS)
                .addColumn(publisherColumn(ListRepresentDSHandler.REPRESENT_TITLE_COLUMN, ListRepresent.title()).permissionKey("rmc_view_list_represent_Button").order().create())
                .addColumn(textColumn(ListRepresentDSHandler.STATE_COLUMN, ListRepresent.state().title()).width("250px").create())
                .addColumn(publisherColumn(ListRepresentDSHandler.ORDER_TITLE_COLUMN, "order.fullTitle").publisherLinkResolver(
                        new IPublisherLinkResolver()
                        {

                            @Override
                            public Object getParameters(IEntity entity)
                            {
                                return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, ((ListRepresent) entity).getOrder().getId());
                            }

                            @Override
                            public String getComponentName(IEntity entity)
                            {
                                return null;
                            }
                        })
                        .permissionKey("rmc_view_list_represent_Button").create())
                .addColumn(textColumn(ListRepresentDSHandler.COUNT_STUDENT_COLUMN, ListRepresent.P_STUDENT_COUNT).create())
                .addColumn(booleanColumn(ListRepresentDSHandler.CHECK, ListRepresent.check()).create())
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("rmc_edit_list_represent_Button").disabled("ui:isFormative").create())
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER, alert("listRepresentDS.delete.alert")).permissionKey("rmc_delete_list_represent_Button").disabled("ui:isFormative").create())
                .create();
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(LIST_REPRESENT_DS, listRepresentDS()).handler(listRepresentDSHandler()))
                .addDataSource(selectDS(STATE_VIEW_DS, stateViewDSHandler()))
                .addDataSource(selectDS(FORMATIVE_ORG_UNIT_DS, formativeOrgUnitDSHandler()))
                .addDataSource(selectDS(TYPE_REPRESENT_DS, typeRepresentDSHandler()))
                .addDataSource(UniStudentManger.instance().courseDSConfig())
                .addDataSource(selectDS(STUDENT_COMBO_DS, studentComboDSHandler()).addColumn(Student.person().identityCard().fullFio().s()))
                .addDataSource(selectDS(GRANT_VIEW_DS, grantViewDSHandler()).addColumn(GrantView.shortTitle().s()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> listRepresentDSHandler()
    {
        return new ListRepresentDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> formativeOrgUnitDSHandler()
    {
        return new FormativeOrgUnitDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> grantViewDSHandler()
    {
        return new GrantViewDSHandler(getName(), "shortTitle");
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> studentComboDSHandler()
    {
        return new StudentComboDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> typeRepresentDSHandler()
    {
        return new TypeRepresentDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> stateViewDSHandler()
    {
        return new StateViewDSHandler(getName());
    }
}
