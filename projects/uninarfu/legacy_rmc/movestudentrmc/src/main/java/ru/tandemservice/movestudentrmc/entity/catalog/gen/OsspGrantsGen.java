package ru.tandemservice.movestudentrmc.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.OsspGrants;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Стипендии ОССП
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OsspGrantsGen extends EntityBase
 implements INaturalIdentifiable<OsspGrantsGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.catalog.OsspGrants";
    public static final String ENTITY_NAME = "osspGrants";
    public static final int VERSION_HASH = -960174146;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_TITLE = "title";
    public static final String P_NUMBER = "number";
    public static final String P_PRINTABLE = "printable";
    public static final String P_REQ_CLARIFICATION = "reqClarification";
    public static final String P_REQ_PERIOD = "reqPeriod";
    public static final String P_REQ_SIZE = "reqSize";

    private String _code;     // Системный код
    private String _title; 
    private Integer _number;     // Номер
    private boolean _printable; 
    private boolean _reqClarification; 
    private boolean _reqPeriod; 
    private boolean _reqSize; 

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return  Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title  Свойство не может быть null и должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Номер.
     */
    public Integer getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер.
     */
    public void setNumber(Integer number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public boolean isPrintable()
    {
        return _printable;
    }

    /**
     * @param printable  Свойство не может быть null.
     */
    public void setPrintable(boolean printable)
    {
        dirty(_printable, printable);
        _printable = printable;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public boolean isReqClarification()
    {
        return _reqClarification;
    }

    /**
     * @param reqClarification  Свойство не может быть null.
     */
    public void setReqClarification(boolean reqClarification)
    {
        dirty(_reqClarification, reqClarification);
        _reqClarification = reqClarification;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public boolean isReqPeriod()
    {
        return _reqPeriod;
    }

    /**
     * @param reqPeriod  Свойство не может быть null.
     */
    public void setReqPeriod(boolean reqPeriod)
    {
        dirty(_reqPeriod, reqPeriod);
        _reqPeriod = reqPeriod;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public boolean isReqSize()
    {
        return _reqSize;
    }

    /**
     * @param reqSize  Свойство не может быть null.
     */
    public void setReqSize(boolean reqSize)
    {
        dirty(_reqSize, reqSize);
        _reqSize = reqSize;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OsspGrantsGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((OsspGrants)another).getCode());
            }
            setTitle(((OsspGrants)another).getTitle());
            setNumber(((OsspGrants)another).getNumber());
            setPrintable(((OsspGrants)another).isPrintable());
            setReqClarification(((OsspGrants)another).isReqClarification());
            setReqPeriod(((OsspGrants)another).isReqPeriod());
            setReqSize(((OsspGrants)another).isReqSize());
        }
    }

    public INaturalId<OsspGrantsGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<OsspGrantsGen>
    {
        private static final String PROXY_NAME = "OsspGrantsNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof OsspGrantsGen.NaturalId) ) return false;

            OsspGrantsGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OsspGrantsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OsspGrants.class;
        }

        public T newInstance()
        {
            return (T) new OsspGrants();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "title":
                    return obj.getTitle();
                case "number":
                    return obj.getNumber();
                case "printable":
                    return obj.isPrintable();
                case "reqClarification":
                    return obj.isReqClarification();
                case "reqPeriod":
                    return obj.isReqPeriod();
                case "reqSize":
                    return obj.isReqSize();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "printable":
                    obj.setPrintable((Boolean) value);
                    return;
                case "reqClarification":
                    obj.setReqClarification((Boolean) value);
                    return;
                case "reqPeriod":
                    obj.setReqPeriod((Boolean) value);
                    return;
                case "reqSize":
                    obj.setReqSize((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "title":
                        return true;
                case "number":
                        return true;
                case "printable":
                        return true;
                case "reqClarification":
                        return true;
                case "reqPeriod":
                        return true;
                case "reqSize":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "title":
                    return true;
                case "number":
                    return true;
                case "printable":
                    return true;
                case "reqClarification":
                    return true;
                case "reqPeriod":
                    return true;
                case "reqSize":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "title":
                    return String.class;
                case "number":
                    return Integer.class;
                case "printable":
                    return Boolean.class;
                case "reqClarification":
                    return Boolean.class;
                case "reqPeriod":
                    return Boolean.class;
                case "reqSize":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OsspGrants> _dslPath = new Path<OsspGrants>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OsspGrants");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.OsspGrants#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return  Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.OsspGrants#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Номер.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.OsspGrants#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.OsspGrants#isPrintable()
     */
    public static PropertyPath<Boolean> printable()
    {
        return _dslPath.printable();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.OsspGrants#isReqClarification()
     */
    public static PropertyPath<Boolean> reqClarification()
    {
        return _dslPath.reqClarification();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.OsspGrants#isReqPeriod()
     */
    public static PropertyPath<Boolean> reqPeriod()
    {
        return _dslPath.reqPeriod();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.OsspGrants#isReqSize()
     */
    public static PropertyPath<Boolean> reqSize()
    {
        return _dslPath.reqSize();
    }

    public static class Path<E extends OsspGrants> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _title;
        private PropertyPath<Integer> _number;
        private PropertyPath<Boolean> _printable;
        private PropertyPath<Boolean> _reqClarification;
        private PropertyPath<Boolean> _reqPeriod;
        private PropertyPath<Boolean> _reqSize;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.OsspGrants#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(OsspGrantsGen.P_CODE, this);
            return _code;
        }

    /**
     * @return  Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.OsspGrants#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(OsspGrantsGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Номер.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.OsspGrants#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(OsspGrantsGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.OsspGrants#isPrintable()
     */
        public PropertyPath<Boolean> printable()
        {
            if(_printable == null )
                _printable = new PropertyPath<Boolean>(OsspGrantsGen.P_PRINTABLE, this);
            return _printable;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.OsspGrants#isReqClarification()
     */
        public PropertyPath<Boolean> reqClarification()
        {
            if(_reqClarification == null )
                _reqClarification = new PropertyPath<Boolean>(OsspGrantsGen.P_REQ_CLARIFICATION, this);
            return _reqClarification;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.OsspGrants#isReqPeriod()
     */
        public PropertyPath<Boolean> reqPeriod()
        {
            if(_reqPeriod == null )
                _reqPeriod = new PropertyPath<Boolean>(OsspGrantsGen.P_REQ_PERIOD, this);
            return _reqPeriod;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.OsspGrants#isReqSize()
     */
        public PropertyPath<Boolean> reqSize()
        {
            if(_reqSize == null )
                _reqSize = new PropertyPath<Boolean>(OsspGrantsGen.P_REQ_SIZE, this);
            return _reqSize;
        }

        public Class getEntityClass()
        {
            return OsspGrants.class;
        }

        public String getEntityName()
        {
            return "osspGrants";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
