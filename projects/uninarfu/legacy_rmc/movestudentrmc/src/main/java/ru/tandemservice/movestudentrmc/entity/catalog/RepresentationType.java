package ru.tandemservice.movestudentrmc.entity.catalog;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonBasic;
import ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonOSSP;
import ru.tandemservice.movestudentrmc.entity.catalog.gen.RepresentationTypeGen;
import ru.tandemservice.uni.dao.UniDaoFacade;

/**
 * Типы представлений
 */
public class RepresentationType extends RepresentationTypeGen {
    public static final String P_TITLE_WITH_TYPE = "titleWithType";

    @EntityDSLSupport(parts = {P_TITLE})
    public Boolean isConfigRelation() {

        Boolean result;

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RelRepresentationReasonBasic.class, "rel").addColumn("rel");
        builder.where(DQLExpressions.eq(DQLExpressions.property(RelRepresentationReasonBasic.type().id().fromAlias("rel")), DQLExpressions.value(getId())));

        result = UniDaoFacade.getCoreDao().getCount(builder) > 0;

        return result;
    }

    @EntityDSLSupport(parts = {P_TITLE})
    public Boolean isConfigOSSP() {

        Boolean result;

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RelRepresentationReasonOSSP.class, "rel").addColumn("rel");
        builder.where(DQLExpressions.eq(DQLExpressions.property(RelRepresentationReasonOSSP.type().id().fromAlias("rel")), DQLExpressions.value(getId())));

        result = UniDaoFacade.getCoreDao().getCount(builder) > 0;

        return result;
    }

    @Override
    public Boolean getConfigRelation() {
        return isConfigRelation();
    }

    @Override
    public Boolean getConfigOSSP() {
        return isConfigOSSP();
    }

    @EntityDSLSupport(parts = {P_TITLE})
    public String getTitleWithType() {
        StringBuilder sb = new StringBuilder(this.getTitle());
        if (this.isListRepresentation())
            return sb.append(" (списочное)").toString();
        else
            return sb.append(" (индивидуальное)").toString();
    }

}