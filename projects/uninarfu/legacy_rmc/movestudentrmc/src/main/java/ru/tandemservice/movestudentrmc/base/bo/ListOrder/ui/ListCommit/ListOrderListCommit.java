package ru.tandemservice.movestudentrmc.base.bo.ListOrder.ui.ListCommit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import ru.tandemservice.movestudentrmc.base.bo.ListOrder.logic.*;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.ReasonRepresentCommitDSHandler;
import ru.tandemservice.movestudentrmc.entity.ListOrder;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.entity.employee.Student;

@Configuration
public class ListOrderListCommit extends BusinessComponentManager {

    public static final String ORDER_COMMIT_DS = "orderCommitDS";
    public static final String FORMATIVE_ORG_UNIT_DS = "formativeOrgUnitCommitDS";
    public static final String TYPE_REPRESENT_DS = "typeRepresentDS";
    public static final String REASON_REPRESENT_COMMIT_DS = "reasonRepresentCommitDS";
    public static final String STUDENT_COMBO_COMMIT_DS = "studentComboCommitDS";

    @Bean
    public ColumnListExtPoint orderCommitDS()
    {
        return columnListExtPointBuilder(ORDER_COMMIT_DS)
                .addColumn(publisherColumn(OrderCommitDSHandler.ORDER_TITLE_COLUMN, ListOrder.P_FULL_TITLE).order().create())
                .addColumn(dateColumn(OrderCommitDSHandler.CREATE_DATE_COLUMN, ListOrder.createDate()).order().create())
                .addColumn(dateColumn(OrderCommitDSHandler.COMMIT_DATE_COLUMN, ListOrder.commitDateSystem()).order().create())
                .addColumn(textColumn(OrderDSHandler.REPRESENTATION_TYPES, ListOrder.P_REPRESENTATION_TYPES).create())
                .addColumn(textColumn(OrderDSHandler.OPERATOR, ListOrder.operator().fullFio()).order().create())
                .addColumn(textColumn(OrderDSHandler.REPRESENTATION_COUNT, ListOrder.P_REPRESENTATION_COUNT).create())
                .addColumn(textColumn(OrderDSHandler.STUDENT_COUNT, ListOrder.P_STUDENT_COUNT).create())
                .addColumn(actionColumn(OrderCommitDSHandler.PRINT_COLUMN_NAME, CommonDefines.ICON_PRINT, "onClickPrintFromList").permissionKey("rmc_print_commited_list_order").create())
                .create();
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(ORDER_COMMIT_DS, orderCommitDS()).handler(orderCommitDSHandler()))
                .addDataSource(selectDS(TYPE_REPRESENT_DS, typeRepresentDSHandler()))
                .addDataSource(selectDS(REASON_REPRESENT_COMMIT_DS, reasonRepresentCommitDSHandler()))
                .addDataSource(selectDS(FORMATIVE_ORG_UNIT_DS, formativeOrgUnitDSHandler()))
                .addDataSource(UniStudentManger.instance().courseDSConfig())
                .addDataSource(selectDS(STUDENT_COMBO_COMMIT_DS, studentComboCommitDSHandler()).addColumn(Student.person().identityCard().fullFio().s()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> orderCommitDSHandler()
    {
        return new OrderCommitDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> studentComboCommitDSHandler()
    {
        return new StudentComboCommitDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> typeRepresentDSHandler()
    {
        return new TypeRepresentDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> formativeOrgUnitDSHandler()
    {
        return new FormativeOrgUnitCommitDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> reasonRepresentCommitDSHandler() {
        return new ReasonRepresentCommitDSHandler(getName());
    }

}
