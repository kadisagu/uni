package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.sec.entity.Principal;
import ru.tandemservice.movestudentrmc.entity.Principal2Visa;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность связь между пользователем и  приказом для проставления полей виз
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class Principal2VisaGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.Principal2Visa";
    public static final String ENTITY_NAME = "principal2Visa";
    public static final int VERSION_HASH = 723354620;
    private static IEntityMeta ENTITY_META;

    public static final String L_PRINCIPAL = "principal";
    public static final String P_EDIT_DATE = "editDate";
    public static final String P_SIGN_POST = "signPost";
    public static final String P_SIGN_FIO = "signFio";
    public static final String P_SIGN_POST_GENITIVE = "signPostGenitive";
    public static final String P_POST_TITLE1 = "postTitle1";
    public static final String P_POST_FIO1 = "postFio1";
    public static final String P_POST_TITLE2 = "postTitle2";
    public static final String P_POST_FIO2 = "postFio2";
    public static final String P_POST_TITLE3 = "postTitle3";
    public static final String P_POST_FIO3 = "postFio3";
    public static final String P_POST_TITLE4 = "postTitle4";
    public static final String P_POST_FIO4 = "postFio4";
    public static final String P_POST_TITLE5 = "postTitle5";
    public static final String P_POST_FIO5 = "postFio5";
    public static final String P_POST_TITLE6 = "postTitle6";
    public static final String P_POST_FIO6 = "postFio6";

    private Principal _principal;     // Пользователь
    private Date _editDate;     // Дата редактирования
    private String _signPost;     // Должность подписывающее лицо
    private String _signFio;     // ФИО подписывающее лицо
    private String _signPostGenitive;     // Должность подписывающего лица (в родительном падеже)
    private String _postTitle1;     // Должность 1
    private String _postFio1;     // ФИО 1
    private String _postTitle2;     // Должность 2
    private String _postFio2;     // ФИО 2
    private String _postTitle3;     // Должность 3
    private String _postFio3;     // ФИО 3
    private String _postTitle4;     // Должность 4
    private String _postFio4;     // ФИО 4
    private String _postTitle5;     // Должность 5
    private String _postFio5;     // ФИО 5
    private String _postTitle6;     // Должность 6
    private String _postFio6;     // ФИО 6

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Пользователь. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public Principal getPrincipal()
    {
        return _principal;
    }

    /**
     * @param principal Пользователь. Свойство не может быть null и должно быть уникальным.
     */
    public void setPrincipal(Principal principal)
    {
        dirty(_principal, principal);
        _principal = principal;
    }

    /**
     * @return Дата редактирования. Свойство не может быть null.
     */
    @NotNull
    public Date getEditDate()
    {
        return _editDate;
    }

    /**
     * @param editDate Дата редактирования. Свойство не может быть null.
     */
    public void setEditDate(Date editDate)
    {
        dirty(_editDate, editDate);
        _editDate = editDate;
    }

    /**
     * @return Должность подписывающее лицо.
     */
    @Length(max=255)
    public String getSignPost()
    {
        return _signPost;
    }

    /**
     * @param signPost Должность подписывающее лицо.
     */
    public void setSignPost(String signPost)
    {
        dirty(_signPost, signPost);
        _signPost = signPost;
    }

    /**
     * @return ФИО подписывающее лицо.
     */
    @Length(max=255)
    public String getSignFio()
    {
        return _signFio;
    }

    /**
     * @param signFio ФИО подписывающее лицо.
     */
    public void setSignFio(String signFio)
    {
        dirty(_signFio, signFio);
        _signFio = signFio;
    }

    /**
     * @return Должность подписывающего лица (в родительном падеже).
     */
    @Length(max=255)
    public String getSignPostGenitive()
    {
        return _signPostGenitive;
    }

    /**
     * @param signPostGenitive Должность подписывающего лица (в родительном падеже).
     */
    public void setSignPostGenitive(String signPostGenitive)
    {
        dirty(_signPostGenitive, signPostGenitive);
        _signPostGenitive = signPostGenitive;
    }

    /**
     * @return Должность 1.
     */
    @Length(max=255)
    public String getPostTitle1()
    {
        return _postTitle1;
    }

    /**
     * @param postTitle1 Должность 1.
     */
    public void setPostTitle1(String postTitle1)
    {
        dirty(_postTitle1, postTitle1);
        _postTitle1 = postTitle1;
    }

    /**
     * @return ФИО 1.
     */
    @Length(max=255)
    public String getPostFio1()
    {
        return _postFio1;
    }

    /**
     * @param postFio1 ФИО 1.
     */
    public void setPostFio1(String postFio1)
    {
        dirty(_postFio1, postFio1);
        _postFio1 = postFio1;
    }

    /**
     * @return Должность 2.
     */
    @Length(max=255)
    public String getPostTitle2()
    {
        return _postTitle2;
    }

    /**
     * @param postTitle2 Должность 2.
     */
    public void setPostTitle2(String postTitle2)
    {
        dirty(_postTitle2, postTitle2);
        _postTitle2 = postTitle2;
    }

    /**
     * @return ФИО 2.
     */
    @Length(max=255)
    public String getPostFio2()
    {
        return _postFio2;
    }

    /**
     * @param postFio2 ФИО 2.
     */
    public void setPostFio2(String postFio2)
    {
        dirty(_postFio2, postFio2);
        _postFio2 = postFio2;
    }

    /**
     * @return Должность 3.
     */
    @Length(max=255)
    public String getPostTitle3()
    {
        return _postTitle3;
    }

    /**
     * @param postTitle3 Должность 3.
     */
    public void setPostTitle3(String postTitle3)
    {
        dirty(_postTitle3, postTitle3);
        _postTitle3 = postTitle3;
    }

    /**
     * @return ФИО 3.
     */
    @Length(max=255)
    public String getPostFio3()
    {
        return _postFio3;
    }

    /**
     * @param postFio3 ФИО 3.
     */
    public void setPostFio3(String postFio3)
    {
        dirty(_postFio3, postFio3);
        _postFio3 = postFio3;
    }

    /**
     * @return Должность 4.
     */
    @Length(max=255)
    public String getPostTitle4()
    {
        return _postTitle4;
    }

    /**
     * @param postTitle4 Должность 4.
     */
    public void setPostTitle4(String postTitle4)
    {
        dirty(_postTitle4, postTitle4);
        _postTitle4 = postTitle4;
    }

    /**
     * @return ФИО 4.
     */
    @Length(max=255)
    public String getPostFio4()
    {
        return _postFio4;
    }

    /**
     * @param postFio4 ФИО 4.
     */
    public void setPostFio4(String postFio4)
    {
        dirty(_postFio4, postFio4);
        _postFio4 = postFio4;
    }

    /**
     * @return Должность 5.
     */
    @Length(max=255)
    public String getPostTitle5()
    {
        return _postTitle5;
    }

    /**
     * @param postTitle5 Должность 5.
     */
    public void setPostTitle5(String postTitle5)
    {
        dirty(_postTitle5, postTitle5);
        _postTitle5 = postTitle5;
    }

    /**
     * @return ФИО 5.
     */
    @Length(max=255)
    public String getPostFio5()
    {
        return _postFio5;
    }

    /**
     * @param postFio5 ФИО 5.
     */
    public void setPostFio5(String postFio5)
    {
        dirty(_postFio5, postFio5);
        _postFio5 = postFio5;
    }

    /**
     * @return Должность 6.
     */
    @Length(max=255)
    public String getPostTitle6()
    {
        return _postTitle6;
    }

    /**
     * @param postTitle6 Должность 6.
     */
    public void setPostTitle6(String postTitle6)
    {
        dirty(_postTitle6, postTitle6);
        _postTitle6 = postTitle6;
    }

    /**
     * @return ФИО 6.
     */
    @Length(max=255)
    public String getPostFio6()
    {
        return _postFio6;
    }

    /**
     * @param postFio6 ФИО 6.
     */
    public void setPostFio6(String postFio6)
    {
        dirty(_postFio6, postFio6);
        _postFio6 = postFio6;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof Principal2VisaGen)
        {
            setPrincipal(((Principal2Visa)another).getPrincipal());
            setEditDate(((Principal2Visa)another).getEditDate());
            setSignPost(((Principal2Visa)another).getSignPost());
            setSignFio(((Principal2Visa)another).getSignFio());
            setSignPostGenitive(((Principal2Visa)another).getSignPostGenitive());
            setPostTitle1(((Principal2Visa)another).getPostTitle1());
            setPostFio1(((Principal2Visa)another).getPostFio1());
            setPostTitle2(((Principal2Visa)another).getPostTitle2());
            setPostFio2(((Principal2Visa)another).getPostFio2());
            setPostTitle3(((Principal2Visa)another).getPostTitle3());
            setPostFio3(((Principal2Visa)another).getPostFio3());
            setPostTitle4(((Principal2Visa)another).getPostTitle4());
            setPostFio4(((Principal2Visa)another).getPostFio4());
            setPostTitle5(((Principal2Visa)another).getPostTitle5());
            setPostFio5(((Principal2Visa)another).getPostFio5());
            setPostTitle6(((Principal2Visa)another).getPostTitle6());
            setPostFio6(((Principal2Visa)another).getPostFio6());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends Principal2VisaGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Principal2Visa.class;
        }

        public T newInstance()
        {
            return (T) new Principal2Visa();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "principal":
                    return obj.getPrincipal();
                case "editDate":
                    return obj.getEditDate();
                case "signPost":
                    return obj.getSignPost();
                case "signFio":
                    return obj.getSignFio();
                case "signPostGenitive":
                    return obj.getSignPostGenitive();
                case "postTitle1":
                    return obj.getPostTitle1();
                case "postFio1":
                    return obj.getPostFio1();
                case "postTitle2":
                    return obj.getPostTitle2();
                case "postFio2":
                    return obj.getPostFio2();
                case "postTitle3":
                    return obj.getPostTitle3();
                case "postFio3":
                    return obj.getPostFio3();
                case "postTitle4":
                    return obj.getPostTitle4();
                case "postFio4":
                    return obj.getPostFio4();
                case "postTitle5":
                    return obj.getPostTitle5();
                case "postFio5":
                    return obj.getPostFio5();
                case "postTitle6":
                    return obj.getPostTitle6();
                case "postFio6":
                    return obj.getPostFio6();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "principal":
                    obj.setPrincipal((Principal) value);
                    return;
                case "editDate":
                    obj.setEditDate((Date) value);
                    return;
                case "signPost":
                    obj.setSignPost((String) value);
                    return;
                case "signFio":
                    obj.setSignFio((String) value);
                    return;
                case "signPostGenitive":
                    obj.setSignPostGenitive((String) value);
                    return;
                case "postTitle1":
                    obj.setPostTitle1((String) value);
                    return;
                case "postFio1":
                    obj.setPostFio1((String) value);
                    return;
                case "postTitle2":
                    obj.setPostTitle2((String) value);
                    return;
                case "postFio2":
                    obj.setPostFio2((String) value);
                    return;
                case "postTitle3":
                    obj.setPostTitle3((String) value);
                    return;
                case "postFio3":
                    obj.setPostFio3((String) value);
                    return;
                case "postTitle4":
                    obj.setPostTitle4((String) value);
                    return;
                case "postFio4":
                    obj.setPostFio4((String) value);
                    return;
                case "postTitle5":
                    obj.setPostTitle5((String) value);
                    return;
                case "postFio5":
                    obj.setPostFio5((String) value);
                    return;
                case "postTitle6":
                    obj.setPostTitle6((String) value);
                    return;
                case "postFio6":
                    obj.setPostFio6((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "principal":
                        return true;
                case "editDate":
                        return true;
                case "signPost":
                        return true;
                case "signFio":
                        return true;
                case "signPostGenitive":
                        return true;
                case "postTitle1":
                        return true;
                case "postFio1":
                        return true;
                case "postTitle2":
                        return true;
                case "postFio2":
                        return true;
                case "postTitle3":
                        return true;
                case "postFio3":
                        return true;
                case "postTitle4":
                        return true;
                case "postFio4":
                        return true;
                case "postTitle5":
                        return true;
                case "postFio5":
                        return true;
                case "postTitle6":
                        return true;
                case "postFio6":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "principal":
                    return true;
                case "editDate":
                    return true;
                case "signPost":
                    return true;
                case "signFio":
                    return true;
                case "signPostGenitive":
                    return true;
                case "postTitle1":
                    return true;
                case "postFio1":
                    return true;
                case "postTitle2":
                    return true;
                case "postFio2":
                    return true;
                case "postTitle3":
                    return true;
                case "postFio3":
                    return true;
                case "postTitle4":
                    return true;
                case "postFio4":
                    return true;
                case "postTitle5":
                    return true;
                case "postFio5":
                    return true;
                case "postTitle6":
                    return true;
                case "postFio6":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "principal":
                    return Principal.class;
                case "editDate":
                    return Date.class;
                case "signPost":
                    return String.class;
                case "signFio":
                    return String.class;
                case "signPostGenitive":
                    return String.class;
                case "postTitle1":
                    return String.class;
                case "postFio1":
                    return String.class;
                case "postTitle2":
                    return String.class;
                case "postFio2":
                    return String.class;
                case "postTitle3":
                    return String.class;
                case "postFio3":
                    return String.class;
                case "postTitle4":
                    return String.class;
                case "postFio4":
                    return String.class;
                case "postTitle5":
                    return String.class;
                case "postFio5":
                    return String.class;
                case "postTitle6":
                    return String.class;
                case "postFio6":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Principal2Visa> _dslPath = new Path<Principal2Visa>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Principal2Visa");
    }
            

    /**
     * @return Пользователь. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getPrincipal()
     */
    public static Principal.Path<Principal> principal()
    {
        return _dslPath.principal();
    }

    /**
     * @return Дата редактирования. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getEditDate()
     */
    public static PropertyPath<Date> editDate()
    {
        return _dslPath.editDate();
    }

    /**
     * @return Должность подписывающее лицо.
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getSignPost()
     */
    public static PropertyPath<String> signPost()
    {
        return _dslPath.signPost();
    }

    /**
     * @return ФИО подписывающее лицо.
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getSignFio()
     */
    public static PropertyPath<String> signFio()
    {
        return _dslPath.signFio();
    }

    /**
     * @return Должность подписывающего лица (в родительном падеже).
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getSignPostGenitive()
     */
    public static PropertyPath<String> signPostGenitive()
    {
        return _dslPath.signPostGenitive();
    }

    /**
     * @return Должность 1.
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getPostTitle1()
     */
    public static PropertyPath<String> postTitle1()
    {
        return _dslPath.postTitle1();
    }

    /**
     * @return ФИО 1.
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getPostFio1()
     */
    public static PropertyPath<String> postFio1()
    {
        return _dslPath.postFio1();
    }

    /**
     * @return Должность 2.
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getPostTitle2()
     */
    public static PropertyPath<String> postTitle2()
    {
        return _dslPath.postTitle2();
    }

    /**
     * @return ФИО 2.
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getPostFio2()
     */
    public static PropertyPath<String> postFio2()
    {
        return _dslPath.postFio2();
    }

    /**
     * @return Должность 3.
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getPostTitle3()
     */
    public static PropertyPath<String> postTitle3()
    {
        return _dslPath.postTitle3();
    }

    /**
     * @return ФИО 3.
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getPostFio3()
     */
    public static PropertyPath<String> postFio3()
    {
        return _dslPath.postFio3();
    }

    /**
     * @return Должность 4.
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getPostTitle4()
     */
    public static PropertyPath<String> postTitle4()
    {
        return _dslPath.postTitle4();
    }

    /**
     * @return ФИО 4.
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getPostFio4()
     */
    public static PropertyPath<String> postFio4()
    {
        return _dslPath.postFio4();
    }

    /**
     * @return Должность 5.
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getPostTitle5()
     */
    public static PropertyPath<String> postTitle5()
    {
        return _dslPath.postTitle5();
    }

    /**
     * @return ФИО 5.
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getPostFio5()
     */
    public static PropertyPath<String> postFio5()
    {
        return _dslPath.postFio5();
    }

    /**
     * @return Должность 6.
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getPostTitle6()
     */
    public static PropertyPath<String> postTitle6()
    {
        return _dslPath.postTitle6();
    }

    /**
     * @return ФИО 6.
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getPostFio6()
     */
    public static PropertyPath<String> postFio6()
    {
        return _dslPath.postFio6();
    }

    public static class Path<E extends Principal2Visa> extends EntityPath<E>
    {
        private Principal.Path<Principal> _principal;
        private PropertyPath<Date> _editDate;
        private PropertyPath<String> _signPost;
        private PropertyPath<String> _signFio;
        private PropertyPath<String> _signPostGenitive;
        private PropertyPath<String> _postTitle1;
        private PropertyPath<String> _postFio1;
        private PropertyPath<String> _postTitle2;
        private PropertyPath<String> _postFio2;
        private PropertyPath<String> _postTitle3;
        private PropertyPath<String> _postFio3;
        private PropertyPath<String> _postTitle4;
        private PropertyPath<String> _postFio4;
        private PropertyPath<String> _postTitle5;
        private PropertyPath<String> _postFio5;
        private PropertyPath<String> _postTitle6;
        private PropertyPath<String> _postFio6;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Пользователь. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getPrincipal()
     */
        public Principal.Path<Principal> principal()
        {
            if(_principal == null )
                _principal = new Principal.Path<Principal>(L_PRINCIPAL, this);
            return _principal;
        }

    /**
     * @return Дата редактирования. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getEditDate()
     */
        public PropertyPath<Date> editDate()
        {
            if(_editDate == null )
                _editDate = new PropertyPath<Date>(Principal2VisaGen.P_EDIT_DATE, this);
            return _editDate;
        }

    /**
     * @return Должность подписывающее лицо.
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getSignPost()
     */
        public PropertyPath<String> signPost()
        {
            if(_signPost == null )
                _signPost = new PropertyPath<String>(Principal2VisaGen.P_SIGN_POST, this);
            return _signPost;
        }

    /**
     * @return ФИО подписывающее лицо.
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getSignFio()
     */
        public PropertyPath<String> signFio()
        {
            if(_signFio == null )
                _signFio = new PropertyPath<String>(Principal2VisaGen.P_SIGN_FIO, this);
            return _signFio;
        }

    /**
     * @return Должность подписывающего лица (в родительном падеже).
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getSignPostGenitive()
     */
        public PropertyPath<String> signPostGenitive()
        {
            if(_signPostGenitive == null )
                _signPostGenitive = new PropertyPath<String>(Principal2VisaGen.P_SIGN_POST_GENITIVE, this);
            return _signPostGenitive;
        }

    /**
     * @return Должность 1.
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getPostTitle1()
     */
        public PropertyPath<String> postTitle1()
        {
            if(_postTitle1 == null )
                _postTitle1 = new PropertyPath<String>(Principal2VisaGen.P_POST_TITLE1, this);
            return _postTitle1;
        }

    /**
     * @return ФИО 1.
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getPostFio1()
     */
        public PropertyPath<String> postFio1()
        {
            if(_postFio1 == null )
                _postFio1 = new PropertyPath<String>(Principal2VisaGen.P_POST_FIO1, this);
            return _postFio1;
        }

    /**
     * @return Должность 2.
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getPostTitle2()
     */
        public PropertyPath<String> postTitle2()
        {
            if(_postTitle2 == null )
                _postTitle2 = new PropertyPath<String>(Principal2VisaGen.P_POST_TITLE2, this);
            return _postTitle2;
        }

    /**
     * @return ФИО 2.
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getPostFio2()
     */
        public PropertyPath<String> postFio2()
        {
            if(_postFio2 == null )
                _postFio2 = new PropertyPath<String>(Principal2VisaGen.P_POST_FIO2, this);
            return _postFio2;
        }

    /**
     * @return Должность 3.
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getPostTitle3()
     */
        public PropertyPath<String> postTitle3()
        {
            if(_postTitle3 == null )
                _postTitle3 = new PropertyPath<String>(Principal2VisaGen.P_POST_TITLE3, this);
            return _postTitle3;
        }

    /**
     * @return ФИО 3.
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getPostFio3()
     */
        public PropertyPath<String> postFio3()
        {
            if(_postFio3 == null )
                _postFio3 = new PropertyPath<String>(Principal2VisaGen.P_POST_FIO3, this);
            return _postFio3;
        }

    /**
     * @return Должность 4.
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getPostTitle4()
     */
        public PropertyPath<String> postTitle4()
        {
            if(_postTitle4 == null )
                _postTitle4 = new PropertyPath<String>(Principal2VisaGen.P_POST_TITLE4, this);
            return _postTitle4;
        }

    /**
     * @return ФИО 4.
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getPostFio4()
     */
        public PropertyPath<String> postFio4()
        {
            if(_postFio4 == null )
                _postFio4 = new PropertyPath<String>(Principal2VisaGen.P_POST_FIO4, this);
            return _postFio4;
        }

    /**
     * @return Должность 5.
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getPostTitle5()
     */
        public PropertyPath<String> postTitle5()
        {
            if(_postTitle5 == null )
                _postTitle5 = new PropertyPath<String>(Principal2VisaGen.P_POST_TITLE5, this);
            return _postTitle5;
        }

    /**
     * @return ФИО 5.
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getPostFio5()
     */
        public PropertyPath<String> postFio5()
        {
            if(_postFio5 == null )
                _postFio5 = new PropertyPath<String>(Principal2VisaGen.P_POST_FIO5, this);
            return _postFio5;
        }

    /**
     * @return Должность 6.
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getPostTitle6()
     */
        public PropertyPath<String> postTitle6()
        {
            if(_postTitle6 == null )
                _postTitle6 = new PropertyPath<String>(Principal2VisaGen.P_POST_TITLE6, this);
            return _postTitle6;
        }

    /**
     * @return ФИО 6.
     * @see ru.tandemservice.movestudentrmc.entity.Principal2Visa#getPostFio6()
     */
        public PropertyPath<String> postFio6()
        {
            if(_postFio6 == null )
                _postFio6 = new PropertyPath<String>(Principal2VisaGen.P_POST_FIO6, this);
            return _postFio6;
        }

        public Class getEntityClass()
        {
            return Principal2Visa.class;
        }

        public String getEntityName()
        {
            return "principal2Visa";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
