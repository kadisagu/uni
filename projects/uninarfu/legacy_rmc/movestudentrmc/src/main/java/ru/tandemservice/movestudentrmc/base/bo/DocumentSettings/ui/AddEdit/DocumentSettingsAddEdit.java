package ru.tandemservice.movestudentrmc.base.bo.DocumentSettings.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.movestudentrmc.base.bo.DocumentSettings.logic.BasicDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DocumentSettings.logic.DocumentKindDSHandler;

@Configuration
public class DocumentSettingsAddEdit extends BusinessComponentManager {

    public static final String KIND_DS = "kindDS";
    public static final String DOC_DS = "basicDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(KIND_DS, kindDSHandler()))
                .addDataSource(selectDS(DOC_DS, basicDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler kindDSHandler() {
        return (IDefaultComboDataSourceHandler) new DocumentKindDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler basicDSHandler() {
        return (IDefaultComboDataSourceHandler) new BasicDSHandler(getName());
    }
}
