package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.hibsupport.EntityIDGenerator;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentIC;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentExtractStatesCodes;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

//RT:2541
public class MS_movestudentrmc_1x0x0_26to27 extends IndependentMigrationScript {
    private IEntityMeta meta;
    private DBTool tool;

    @Override
    public void run(DBTool tool) throws Exception {
        this.meta = EntityRuntime.getMeta(DocRepresentStudentIC.class);

        if (!tool.tableExists(meta.getTableName()))
            return;

        this.tool = tool;

        process();
    }

    /*
    select
      rep.ID as rep_id,
      p.identitycard_id as ic_id,
      (select count(dic.REPRESENTATION_ID) from DOCREPRESENTSTUDENTIC_T dic where dic.REPRESENTATION_ID=rep.id) as aaa
    from
      REPRESENTCHANGENAME_T rep
      inner join REPRESENTATION_T r on r.ID=rep.ID
      inner join MOVESTUDENTEXTRACTSTATES_T stat on stat.ID=r.STATE_ID
      inner join DOCREPRESENTSTUDENTBASE_T doc on doc.REPRESENTATION_ID=rep.id
      inner join student_t st on st.id=doc.STUDENT_ID
      inner join personrole_t pr on pr.id=st.id
      inner join person_t p on p.id=pr.person_id
    where
      stat.CODE_P='6'
     */
    private void process() throws Exception {
        String sql = ""
                + " select"
                + " rep.ID as rep_id,"
                + " p.identitycard_id as ic_id,"
                + " (select count(dic.REPRESENTATION_ID) from DOCREPRESENTSTUDENTIC_T dic where dic.REPRESENTATION_ID=rep.id) as ok"
                + " from"
                + " REPRESENTCHANGENAME_T rep"
                + " inner join REPRESENTATION_T r on r.ID=rep.ID"
                + " inner join MOVESTUDENTEXTRACTSTATES_T stat on stat.ID=r.STATE_ID"
                + " inner join DOCREPRESENTSTUDENTBASE_T doc on doc.REPRESENTATION_ID=rep.id"
                + " inner join student_t st on st.id=doc.STUDENT_ID"
                + " inner join personrole_t pr on pr.id=st.id"
                + " inner join person_t p on p.id=pr.person_id"
                + " where"
                + " stat.CODE_P=?";

        PreparedStatement pst = this.tool.getConnection().prepareStatement(sql);
        pst.setString(1, MovestudentExtractStatesCodes.CODE_6);
        ResultSet rs = pst.executeQuery();

        short entityCode = meta.getEntityCode();
        pst = this.tool.getConnection().prepareStatement("insert into " + this.meta.getTableName() + " (ID, DISCRIMINATOR, REPRESENTATION_ID, STUDENTIC_ID) VALUES (?,?,?,?)");
        while (rs.next()) {
            Long repId = rs.getLong("rep_id");
            Long icId = rs.getLong("ic_id");
            int ok = rs.getInt("ok");

            if (ok > 0)
                continue;

            pst.setLong(1, EntityIDGenerator.generateNewId(entityCode).longValue());
            pst.setShort(2, entityCode);
            pst.setLong(3, repId);
            pst.setLong(4, icId);

            pst.executeUpdate();
        }
        rs.close();
    }
}
