package ru.tandemservice.movestudentrmc.base.ext.SystemAction.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.ui.Pub.SystemActionPubUI;
import ru.tandemservice.movestudentrmc.dao.HistoryGrantDAO;

public class SystemActionPubAddon extends UIAddon {

    public SystemActionPubAddon(IUIPresenter presenter, String name,
                                String componentId)
    {
        super(presenter, name, componentId);

    }

    SystemActionPubUI parent = getPresenter();

    public void onClickFillGrantHistory() {

        ErrorCollector errors = parent.getUserContext().getErrorCollector();
        if (HistoryGrantDAO.IS_BEAZY) {
            errors.add("Служба уже исполняется " + HistoryGrantDAO.LOG_STRING);
            return;
        }
        else {
            // стартуем
            HistoryGrantDAO.FORSE_RUN = true;
            HistoryGrantDAO.DAEMON.wakeUpDaemon();
        }


    }
}
