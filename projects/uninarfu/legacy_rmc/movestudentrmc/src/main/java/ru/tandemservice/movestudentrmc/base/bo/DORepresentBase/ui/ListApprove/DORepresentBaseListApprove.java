package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.ListApprove;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.logic.TypeRepresentDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.DevelopFormDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.FormativeOrgUnitDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.RepresentDSHandler;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.uni.entity.employee.Student;

@Configuration
public class DORepresentBaseListApprove extends BusinessComponentManager {

    public static final String REPRESENT_DS = "representDS";
    public static final String DEVELOP_FORM_DS = "developFormDS";
    public static final String FORMATIVE_ORG_UNIT_DS = "formativeOrgUnitDS";
    public static final String TYPE_REPRESENT_DS = "typeRepresentDS";

    @Bean
    public ColumnListExtPoint representDS()
    {
        return columnListExtPointBuilder(REPRESENT_DS)
                .addColumn(textColumn(RepresentDSHandler.DEVELOP_FORM_COLUMN, "student." + Student.educationOrgUnit().developForm().title()).order().create())
                .addColumn(textColumn(RepresentDSHandler.FORMATIVE_ORG_UNIT_COLUMN, "student." + Student.educationOrgUnit().formativeOrgUnit().title()).order().create())
                .addColumn(textColumn(RepresentDSHandler.TERRITORIAL_ORG_UNIT_COLUMN, "student." + Student.educationOrgUnit().territorialOrgUnit().fullTitle()).order().create())
                .addColumn(textColumn(RepresentDSHandler.EDUCATION_LEVEL_HIGH_SCHOOL_COLUMN, "student." + Student.educationOrgUnit().educationLevelHighSchool().orgUnit().fullTitle()).order().create())
                .addColumn(textColumn(RepresentDSHandler.EDUCATION_ORG_UNIT_COLUMN, "student." + Student.educationOrgUnit().title()).order().create())
                .addColumn(actionColumn(RepresentDSHandler.TYPE_REPRESENT_COLUMN, "represent." + Representation.type().title(), "onClickView").order().create())
                .addColumn(dateColumn(RepresentDSHandler.DATE_REPRESENT_COLUMN, "represent." + Representation.startDate()).order().create())
                .addColumn(textColumn(RepresentDSHandler.COURSE_COLUMN, "student." + Student.course().title()).order().create())
                .addColumn(textColumn(RepresentDSHandler.GROUP_COLUMN, "student." + Student.group().title()).order().create())
                .addColumn(textColumn(RepresentDSHandler.BUDGET_COLUMN, "student." + Student.compensationType().shortTitle()).order().create())
                .addColumn(textColumn(RepresentDSHandler.STUDENT_FIO_COLUMN, "student." + Student.person().identityCard().fullFio()).order().create())
                .addColumn(textColumn(RepresentDSHandler.STATE_COLUMN, "represent." + Representation.state().title()).order().create())
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("docOrdRepresentListEdit").disabled("isApprove").create())
                .create();
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(REPRESENT_DS, representDS()).handler(representDSHandler()))
                .addDataSource(selectDS(DEVELOP_FORM_DS, developFormDSHandler()))
                .addDataSource(selectDS(FORMATIVE_ORG_UNIT_DS, formativeOrgUnitDSHandler()))
                .addDataSource(selectDS(TYPE_REPRESENT_DS, typeRepresentDSHandler()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> representDSHandler()
    {
        return new RepresentDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> developFormDSHandler()
    {
        return new DevelopFormDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> formativeOrgUnitDSHandler()
    {
        return new FormativeOrgUnitDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> typeRepresentDSHandler()
    {
        return new TypeRepresentDSHandler(getName());
    }
}
