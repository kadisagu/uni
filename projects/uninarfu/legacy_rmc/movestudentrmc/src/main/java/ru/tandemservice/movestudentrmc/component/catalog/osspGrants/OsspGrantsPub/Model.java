package ru.tandemservice.movestudentrmc.component.catalog.osspGrants.OsspGrantsPub;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubModel;
import ru.tandemservice.movestudentrmc.entity.catalog.OsspGrants;

public class Model extends DefaultCatalogPubModel<OsspGrants> {
    public static class OsspGrantsWrapper extends
            IdentifiableWrapper<OsspGrants>
    {
        private static final long serialVersionUID = -4301478784488247008L;
        public static final String PRINTABLE = "printable";
        public static final String REQ_CLARIFICATION = "reqClarification";
        public static final String REQ_PERIOD = "reqPeriod";
        public static final String REQ_SIZE = "reqSize";

        private OsspGrants osspGrants;
        private boolean printable;
        private boolean reqClarification;
        private boolean reqPeriod;
        private boolean reqSize;

        public OsspGrantsWrapper(
                OsspGrants osspGrants,
                boolean printable, boolean reqClarification, boolean reqPeriod, boolean reqSize)
        {
            super(osspGrants.getId(), osspGrants
                    .getTitle());
            this.osspGrants = osspGrants;
            this.printable = printable;
            this.reqClarification = reqClarification;
            this.reqPeriod = reqPeriod;
            this.reqSize = reqSize;
        }

        public OsspGrants getOsspGrants() {
            return osspGrants;
        }

        public boolean isPrintable() {
            return printable;
        }

        public void setPrintable(boolean printable) {
            this.printable = printable;
        }

        public boolean isReqClarification() {
            return reqClarification;
        }

        public void setReqClarification(boolean reqClarification) {
            this.reqClarification = reqClarification;
        }

        public boolean isReqPeriod() {
            return reqPeriod;
        }

        public void setReqPeriod(boolean reqPeriod) {
            this.reqPeriod = reqPeriod;
        }

        public boolean isReqSize() {
            return reqSize;
        }

        public void setReqSize(boolean reqSize) {
            this.reqSize = reqSize;
        }
    }
}
