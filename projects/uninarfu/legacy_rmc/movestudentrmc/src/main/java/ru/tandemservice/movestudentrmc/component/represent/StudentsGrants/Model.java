package ru.tandemservice.movestudentrmc.component.represent.StudentsGrants;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import java.util.List;

@Input({
        @Bind(key = "list", binding = "list"),
        @Bind(key = "represent", binding = "represent")
})


public class Model {

    private List<DataWrapper> list;
    private boolean _represent;

    private DynamicListDataSource<DataWrapper> dataSource;

    public DynamicListDataSource<DataWrapper> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<DataWrapper> dataSource) {
        this.dataSource = dataSource;
    }

    public void setRepresent(boolean represent)
    {
        this._represent = represent;
    }

    public boolean getRepresent()
    {
        return this._represent;
    }

    public List<DataWrapper> getList() {
        return list;
    }

    public void setList(List<DataWrapper> list) {
        this.list = list;
    }

}
