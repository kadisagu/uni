package ru.tandemservice.movestudentrmc.base.bo.ListRepresentWeekend.logic;

import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfString;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.AbstractListPrintDoc;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListRepresentWeekend;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.*;

public class ListPrintDoc extends AbstractListPrintDoc<ListRepresentWeekend>
{

    public final String TEMPLATE_HEADER = "list.rep.weekend.header";
    public final String TEMPLATE_PARAG = "list.rep.weekend.parag";

    @Override
    protected String getTemplateHeader()
    {
        return TEMPLATE_HEADER;
    }

    @Override
    protected String getTemplateParag()
    {
        return TEMPLATE_PARAG;
    }

    @Override
    protected String getTemplateAddon()
    {
        return null;
    }

    @Override
    protected void paragraphInjectModifier(RtfInjectModifier im,
                                           ListRepresentWeekend listRepresent, Student student)
    {
        injectModifier(im, listRepresent, student);
    }

    @Override
    protected Class<? extends ListRepresent> getListRepresentClass()
    {
        return ListRepresentWeekend.class;
    }

    @Override
    protected String getKey(Student student, ListRepresentWeekend represent)
    {
        return new StringBuilder()
                .append(student.getEducationOrgUnit().getDevelopForm().getId())
                .append(student.getCourse().getId())
                .append(represent.getDateBeginningWeekend().getTime())
                .append(represent.getDateEndOfWeekend().getTime())
                .toString();
    }

    public static void injectModifier(RtfInjectModifier im, ListRepresentWeekend listRepresent, Student student)
    {
        im.put("dateBegin", UtilPrintSupport.getDateFormatterWithMonthString(listRepresent.getDateBeginningWeekend()));
        im.put("dateEnd", UtilPrintSupport.getDateFormatterWithMonthString(listRepresent.getDateEndOfWeekend()));
        im.put("course", student.getCourse().getTitle());
        im.put("developForm", UtilPrintSupport.getDevelopFormGen(student.getEducationOrgUnit().getDevelopForm()));
        im.put("highSchool", UtilPrintSupport.getHighLevelSchoolTypeString(student.getEducationOrgUnit().getEducationLevelHighSchool()));
        im.put("reason", listRepresent.getRepresentationReason().getTitle());
    }

    @Override
    protected RtfDocument getParagraph(RtfDocument template, int formCounter,
                                       int counter, int addonCounter, List<Object[]> studentList)
    {
        RtfDocument result = template.getClone();
        ListRepresentWeekend listRepresent = (ListRepresentWeekend) studentList.get(0)[1];
        Student student = (Student) studentList.get(0)[0];

        Map<EducationLevelsHighSchool, List<Student>> levelMap = new HashMap<>();

        for (Object[] o : studentList)
        {
            Student s = (Student) o[0];
            EducationLevelsHighSchool key = s.getEducationOrgUnit().getEducationLevelHighSchool();
            getParagInfomap().put(s, new OrderParagraphInfo(formCounter, counter));

            List<Student> lst = levelMap.get(key);
            if (lst == null)
                lst = new ArrayList<>();
            lst.add(s);

            levelMap.put(key, lst);
        }
        RtfInjectModifier im = new RtfInjectModifier();
        injectModifier(im, listRepresent, student);
        im.put("i", String.valueOf(formCounter).trim());
        im.put("j", String.valueOf(counter).trim());

        List<EducationLevelsHighSchool> keyList = new ArrayList<>(levelMap.keySet());

        Collections.sort(keyList,
                new EntityComparator<>(new EntityOrder(EducationLevelsHighSchool.displayableTitle())));

        for (EducationLevelsHighSchool key : keyList)
        {
            insertRow(result, UtilPrintSupport.getHighLevelSchoolTypeString(key));
            List<Student> lst = levelMap.get(key);
            int n = 1;
            for (Student std : lst)
                result.getElementList().addAll(new RtfString().append(n++ + ")").append(IRtfData.TAB)
                        .append(std.getPerson().getFullFio())
                        .append(std.getCompensationType().isBudget() ? "" : " (по договору)")
                        .par().toList());

        }
        result.addElement(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
        im.modify(result);
        return result;
    }

}
