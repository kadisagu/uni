package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ui.StudentCommit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.ListOrderListRepresentCommitDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.ListRepresentDSHandler;
import ru.tandemservice.movestudentrmc.entity.ListOrdListRepresent;


@Configuration
public class ListRepresentBaseStudentCommit extends BusinessComponentManager {

    public static final String LIST_REPRESENT_DS = "listRepresentDS";


    @Bean
    public ColumnListExtPoint listRepresentDS()
    {
        return columnListExtPointBuilder(LIST_REPRESENT_DS)
                //.addColumn(textColumn(ListRepresentDSHandler.REPRESENT_TITLE_COLUMN,ListOrdListRepresent.representation().title()).width("400px").order().create())
//                .addColumn(actionColumn(ListRepresentDSHandler.REPRESENT_TITLE_COLUMN,ListOrdListRepresent.representation().title(),"onClickView").width("400px").order().create())
                .addColumn(publisherColumn(ListRepresentDSHandler.REPRESENT_TITLE_COLUMN, ListOrdListRepresent.representation().title()).publisherLinkResolver(
                        new IPublisherLinkResolver() {
                            @Override
                            public Object getParameters(IEntity entity) {
                                return new UniMap().add(PublisherActivator.PUBLISHER_ID_KEY, ((ListOrdListRepresent) entity).getRepresentation().getId());
                            }

                            @Override
                            public String getComponentName(IEntity entity) {
                                return null;
                            }
                        })
                                   .width("400px").order().create())
                .addColumn(textColumn("createDate", ListOrdListRepresent.representation().createDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order().create())

                .addColumn(textColumn(ListRepresentDSHandler.STATE_COLUMN, ListOrdListRepresent.representation().state().title()).order().create())

                        //.addColumn(textColumn("orderNumber", ListOrdListRepresent.order().number()).order().create())
//        		.addColumn(actionColumn("orderNumber", ListOrdListRepresent.order().number(),"onClickViewOrder").order().create())
                .addColumn(publisherColumn("orderNumber", ListOrdListRepresent.order().number()).publisherLinkResolver(
                        new IPublisherLinkResolver() {
                            @Override
                            public Object getParameters(IEntity entity) {
                                return new UniMap().add(PublisherActivator.PUBLISHER_ID_KEY, ((ListOrdListRepresent) entity).getOrder().getId());
                            }

                            @Override
                            public String getComponentName(IEntity entity) {
                                return null;
                            }
                        }
                )
                                   .order().create())

                .addColumn(textColumn("orderState", ListOrdListRepresent.order().state().title()).order().create())
                .addColumn(textColumn("orderСreateDate", ListOrdListRepresent.order().createDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order().create())
                .addColumn(textColumn("orderСommitDate", ListOrdListRepresent.order().commitDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order().create())
                .addColumn(booleanColumn(ListRepresentDSHandler.CHECK, ListOrdListRepresent.representation().check()).order().create())
                .addColumn(actionColumn("printExtractColumn", CommonDefines.ICON_PRINT, "onClickPrintExtract").displayHeader(true).disabled(false).permissionKey("rmc_print_extracts_list_represent_Button").create())
                .addColumn(actionColumn("printRepresent", CommonDefines.ICON_PRINT, "onClickPrintFromList").displayHeader(true).permissionKey("rmc_print_list_represent_Button").create())
                .create();
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(LIST_REPRESENT_DS, listRepresentDS()).handler(listRepresentDSHandler()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> listRepresentDSHandler()
    {
        return new ListOrderListRepresentCommitDSHandler(getName());
    }


}
