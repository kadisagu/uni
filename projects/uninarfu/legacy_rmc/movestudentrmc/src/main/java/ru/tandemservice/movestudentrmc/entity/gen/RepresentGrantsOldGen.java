package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudentrmc.entity.RepresentGrantsOld;
import ru.tandemservice.movestudentrmc.entity.Representation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность для хранения старых данных о стипендии/выплаты
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RepresentGrantsOldGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.RepresentGrantsOld";
    public static final String ENTITY_NAME = "representGrantsOld";
    public static final int VERSION_HASH = 1406314902;
    private static IEntityMeta ENTITY_META;

    public static final String L_REPRESENTATION = "representation";
    public static final String P_STUDENT_GRANT_ENTITY_ID = "studentGrantEntityId";
    public static final String P_ORDER_NUMBER_OLD = "orderNumberOld";
    public static final String P_ORDER_DATE_OLD = "orderDateOld";

    private Representation _representation;     // Представление
    private long _studentGrantEntityId;     // Стипендия/выплата студенту
    private String _orderNumberOld;     // Старый номер приказа о назначении стипендии/выплаты
    private Date _orderDateOld;     // Старая дата приказа о назначении стипендии/выплаты

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Представление. Свойство не может быть null.
     */
    @NotNull
    public Representation getRepresentation()
    {
        return _representation;
    }

    /**
     * @param representation Представление. Свойство не может быть null.
     */
    public void setRepresentation(Representation representation)
    {
        dirty(_representation, representation);
        _representation = representation;
    }

    /**
     * @return Стипендия/выплата студенту. Свойство не может быть null.
     */
    @NotNull
    public long getStudentGrantEntityId()
    {
        return _studentGrantEntityId;
    }

    /**
     * @param studentGrantEntityId Стипендия/выплата студенту. Свойство не может быть null.
     */
    public void setStudentGrantEntityId(long studentGrantEntityId)
    {
        dirty(_studentGrantEntityId, studentGrantEntityId);
        _studentGrantEntityId = studentGrantEntityId;
    }

    /**
     * @return Старый номер приказа о назначении стипендии/выплаты.
     */
    @Length(max=255)
    public String getOrderNumberOld()
    {
        return _orderNumberOld;
    }

    /**
     * @param orderNumberOld Старый номер приказа о назначении стипендии/выплаты.
     */
    public void setOrderNumberOld(String orderNumberOld)
    {
        dirty(_orderNumberOld, orderNumberOld);
        _orderNumberOld = orderNumberOld;
    }

    /**
     * @return Старая дата приказа о назначении стипендии/выплаты.
     */
    public Date getOrderDateOld()
    {
        return _orderDateOld;
    }

    /**
     * @param orderDateOld Старая дата приказа о назначении стипендии/выплаты.
     */
    public void setOrderDateOld(Date orderDateOld)
    {
        dirty(_orderDateOld, orderDateOld);
        _orderDateOld = orderDateOld;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RepresentGrantsOldGen)
        {
            setRepresentation(((RepresentGrantsOld)another).getRepresentation());
            setStudentGrantEntityId(((RepresentGrantsOld)another).getStudentGrantEntityId());
            setOrderNumberOld(((RepresentGrantsOld)another).getOrderNumberOld());
            setOrderDateOld(((RepresentGrantsOld)another).getOrderDateOld());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RepresentGrantsOldGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RepresentGrantsOld.class;
        }

        public T newInstance()
        {
            return (T) new RepresentGrantsOld();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "representation":
                    return obj.getRepresentation();
                case "studentGrantEntityId":
                    return obj.getStudentGrantEntityId();
                case "orderNumberOld":
                    return obj.getOrderNumberOld();
                case "orderDateOld":
                    return obj.getOrderDateOld();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "representation":
                    obj.setRepresentation((Representation) value);
                    return;
                case "studentGrantEntityId":
                    obj.setStudentGrantEntityId((Long) value);
                    return;
                case "orderNumberOld":
                    obj.setOrderNumberOld((String) value);
                    return;
                case "orderDateOld":
                    obj.setOrderDateOld((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "representation":
                        return true;
                case "studentGrantEntityId":
                        return true;
                case "orderNumberOld":
                        return true;
                case "orderDateOld":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "representation":
                    return true;
                case "studentGrantEntityId":
                    return true;
                case "orderNumberOld":
                    return true;
                case "orderDateOld":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "representation":
                    return Representation.class;
                case "studentGrantEntityId":
                    return Long.class;
                case "orderNumberOld":
                    return String.class;
                case "orderDateOld":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RepresentGrantsOld> _dslPath = new Path<RepresentGrantsOld>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RepresentGrantsOld");
    }
            

    /**
     * @return Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentGrantsOld#getRepresentation()
     */
    public static Representation.Path<Representation> representation()
    {
        return _dslPath.representation();
    }

    /**
     * @return Стипендия/выплата студенту. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentGrantsOld#getStudentGrantEntityId()
     */
    public static PropertyPath<Long> studentGrantEntityId()
    {
        return _dslPath.studentGrantEntityId();
    }

    /**
     * @return Старый номер приказа о назначении стипендии/выплаты.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentGrantsOld#getOrderNumberOld()
     */
    public static PropertyPath<String> orderNumberOld()
    {
        return _dslPath.orderNumberOld();
    }

    /**
     * @return Старая дата приказа о назначении стипендии/выплаты.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentGrantsOld#getOrderDateOld()
     */
    public static PropertyPath<Date> orderDateOld()
    {
        return _dslPath.orderDateOld();
    }

    public static class Path<E extends RepresentGrantsOld> extends EntityPath<E>
    {
        private Representation.Path<Representation> _representation;
        private PropertyPath<Long> _studentGrantEntityId;
        private PropertyPath<String> _orderNumberOld;
        private PropertyPath<Date> _orderDateOld;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentGrantsOld#getRepresentation()
     */
        public Representation.Path<Representation> representation()
        {
            if(_representation == null )
                _representation = new Representation.Path<Representation>(L_REPRESENTATION, this);
            return _representation;
        }

    /**
     * @return Стипендия/выплата студенту. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentGrantsOld#getStudentGrantEntityId()
     */
        public PropertyPath<Long> studentGrantEntityId()
        {
            if(_studentGrantEntityId == null )
                _studentGrantEntityId = new PropertyPath<Long>(RepresentGrantsOldGen.P_STUDENT_GRANT_ENTITY_ID, this);
            return _studentGrantEntityId;
        }

    /**
     * @return Старый номер приказа о назначении стипендии/выплаты.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentGrantsOld#getOrderNumberOld()
     */
        public PropertyPath<String> orderNumberOld()
        {
            if(_orderNumberOld == null )
                _orderNumberOld = new PropertyPath<String>(RepresentGrantsOldGen.P_ORDER_NUMBER_OLD, this);
            return _orderNumberOld;
        }

    /**
     * @return Старая дата приказа о назначении стипендии/выплаты.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentGrantsOld#getOrderDateOld()
     */
        public PropertyPath<Date> orderDateOld()
        {
            if(_orderDateOld == null )
                _orderDateOld = new PropertyPath<Date>(RepresentGrantsOldGen.P_ORDER_DATE_OLD, this);
            return _orderDateOld;
        }

        public Class getEntityClass()
        {
            return RepresentGrantsOld.class;
        }

        public String getEntityName()
        {
            return "representGrantsOld";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
