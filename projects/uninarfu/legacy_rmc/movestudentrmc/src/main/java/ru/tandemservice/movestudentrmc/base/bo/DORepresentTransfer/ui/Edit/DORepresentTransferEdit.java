package ru.tandemservice.movestudentrmc.base.bo.DORepresentTransfer.ui.Edit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.block.BlockListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.EditBasic.DORepresentBaseEditBasic;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.EditWorkPlan.DORepresentBaseEditWorkPlan;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentExclude.ui.Edit.DORepresentExcludeEditUI;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.CompensationTypeDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.*;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;

@Configuration
public class DORepresentTransferEdit extends BusinessComponentManager {
    public static final String REASON_DS = "reasonDS";
    public static final String GROUP_DS = "groupDS";
    public static final String FORM_ORGUNIT_DS = "formOrgUnitDS";
    public static final String TERR_ORGUNIT_DS = "terrOrgUnitDS";
    public static final String COMPENSATION_TYPE_DS = "compensationTypeDS";
    public static final String DEVELOP_FORM_DS = "developFormDS";
    public static final String DEVELOP_CONDITION_DS = "developConditionDS";
    public static final String DEVELOP_PERIOD_DS = "developPeriodDS";
    public static final String DEVELOP_TECH_DS = "developTechDS";
    public static final String EDU_LEVEL_DS = "eduLevelDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(REASON_DS, reasonDSHandler()))
                .addDataSource(UniStudentManger.instance().courseDSConfig())
                .addDataSource(selectDS(GROUP_DS, groupDSHandler()).addColumn("fullTitle"))
                .addDataSource(selectDS(FORM_ORGUNIT_DS, formOrgUnitDSHandler()))
                .addDataSource(selectDS(TERR_ORGUNIT_DS, terrOrgUnitDSHandler()))
                .addDataSource(selectDS(COMPENSATION_TYPE_DS, compensationTypeDSHandler()))
                .addDataSource(selectDS(DEVELOP_FORM_DS, developFormDSHandler()))
                .addDataSource(selectDS(DEVELOP_CONDITION_DS, developCondition()))
                .addDataSource(selectDS(DEVELOP_PERIOD_DS, developPeriodDSHandler()))
                .addDataSource(selectDS(DEVELOP_TECH_DS, developTechDSHandler()))
                .addDataSource(selectDS(EDU_LEVEL_DS, eduLevelDSHandler()).addColumn(EducationLevelsHighSchool.fullTitle().s()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler reasonDSHandler() {
        return new ReasonDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler groupDSHandler() {
        return new GroupDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler formOrgUnitDSHandler() {
        return new FormativeOrgUnitDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler terrOrgUnitDSHandler() {
        return new TerritorialOrgUnitDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler developFormDSHandler() {
        return new DevelopFormDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler developCondition() {
        return new DevelopConditionDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler compensationTypeDSHandler() {
        return new CompensationTypeDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler developPeriodDSHandler() {
        return new DevelopPeriodDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler developTechDSHandler() {
        return new DevelopTechDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler eduLevelDSHandler() {
        return new EducationLevelsHighSchoolDSHandler(getName());
    }

    @Bean
    public BlockListExtPoint editBasicBlockListExtPoint() {
        return blockListExtPointBuilder(DORepresentExcludeEditUI.EDIT_BASIC_BLOCK_LIST)
                .addBlock(componentBlock(DORepresentExcludeEditUI.EDIT_BASIC_BLOCK, DORepresentBaseEditBasic.class).parameters("ui:blockParam"))
                .create();
    }

    @Bean
    public BlockListExtPoint editWorkPlanBlockListExtPoint() {
        return blockListExtPointBuilder("editWorkPlanBlockListExtPoint")
                .addBlock(componentBlock("editWorkPlanBlockListExtPoint", DORepresentBaseEditWorkPlan.class).parameters("ui:blockParam"))
                .create();
    }
}
