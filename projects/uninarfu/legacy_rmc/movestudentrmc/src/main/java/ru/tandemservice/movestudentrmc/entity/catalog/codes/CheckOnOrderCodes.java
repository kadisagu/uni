package ru.tandemservice.movestudentrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Проверки по приказам"
 * Имя сущности : checkOnOrder
 * Файл data.xml : movementstudent.data.xml
 */
public interface CheckOnOrderCodes
{
    /** Константа кода (code) элемента : Состояние «Архивный» (title) */
    String STATUS_ARCHIVAL = "movestudentrmc.1";
    /** Константа кода (code) элемента : Состояние «Активный» (title) */
    String STATUS_ACTIVE = "movestudentrmc.2";
    /** Константа кода (code) элемента : Состояние «Возможный» (title) */
    String STATUS_POSSIBLE = "movestudentrmc.3";
    /** Константа кода (code) элемента : Состояние «Зак. с дипломом» (title) */
    String STATUS_GRADUATED_WITH_DPLOMA = "movestudentrmc.4";
    /** Константа кода (code) элемента : Состояние «Отложить защиту» (title) */
    String STATUS_DEFER_PROTECTION = "movestudentrmc.5";
    /** Константа кода (code) элемента : Граждане РФ (title) */
    String CITIZENS_R_F = "movestudentrmc.6";
    /** Константа кода (code) элемента : Студенты, обучающиеся по очной форме (title) */
    String DEVELOP_FORM_INTERNAL = "movestudentrmc.7";
    /** Константа кода (code) элемента : Студенты, обучающиеся за счет средств федерального бюджета (title) */
    String COMPENSATION_TYPE_BUDGET = "movestudentrmc.8";
    /** Константа кода (code) элемента : Студенты, обучающиеся по программам ВПО (title) */
    String HIGH = "movestudentrmc.9";
    /** Константа кода (code) элемента : Студенты, обучающиеся по программам бакалавриата или специалитета (title) */
    String BACHELOR_AND_SPECIALTY = "movestudentrmc.10";
    /** Константа кода (code) элемента : Студенты, обучающиеся по программам магистратуры (title) */
    String MASTER = "movestudentrmc.11";
    /** Константа кода (code) элемента : Студенты, обучающиеся по программам СПО (title) */
    String MIDDLE = "movestudentrmc.12";
    /** Константа кода (code) элемента : Студенты, обучающиеся на 1 курсе бакалавриата/специалитета (title) */
    String COURSE1_BACHELOR_AND_SPECIALTY = "movestudentrmc.13";
    /** Константа кода (code) элемента : Студенты, обучающиеся на 2 курсе бакалавриата/специалитета (title) */
    String COURSE2_BACHELOR_AND_SPECIALTY = "movestudentrmc.14";
    /** Константа кода (code) элемента : Студенты, обучающиеся на 3 курсе бакалавриата/специалитета (title) */
    String COURSE3_BACHELOR_AND_SPECIALTY = "movestudentrmc.15";
    /** Константа кода (code) элемента : Студенты, обучающиеся на 4 курсе бакалавриата/специалитета (title) */
    String COURSE4_BACHELOR_AND_SPECIALTY = "movestudentrmc.16";
    /** Константа кода (code) элемента : Студенты, обучающиеся на 5 курсе бакалавриата/специалитета (title) */
    String COURSE5_BACHELOR_AND_SPECIALTY = "movestudentrmc.17";
    /** Константа кода (code) элемента : Студенты, обучающиеся на 6 курсе бакалавриата/специалитета (title) */
    String COURSE6_BACHELOR_AND_SPECIALTY = "movestudentrmc.18";
    /** Константа кода (code) элемента : Студенты, обучающиеся на 1 курсе магистратуры (title) */
    String COURSE1_MASTER = "movestudentrmc.19";
    /** Константа кода (code) элемента : Студенты, обучающиеся на 2 курсе магистартуры (title) */
    String COURSE2_MASTER = "movestudentrmc.20";
    /** Константа кода (code) элемента : Студенты, обучающиеся в г.Архангельск (title) */
    String ARKHANGELSK = "movestudentrmc.21";
    /** Константа кода (code) элемента : Студенты, обучающиеся в г.Северодвинск (title) */
    String SEVERODVINSK = "movestudentrmc.22";
    /** Константа кода (code) элемента : Студенты, обучающиеся в г.Коряжма (title) */
    String KORJAZHMA = "movestudentrmc.23";
    /** Константа кода (code) элемента : Состояние «Отпуск академический без посещения» (title) */
    String STATUS_ACADEMICAL_VACATION_WITHOUT_VISITS = "movestudentrmc.24";
    /** Константа кода (code) элемента : Состояние «Отпуск без посещения» (title) */
    String STATUS_VACATION_WITHOUT_VISITS = "movestudentrmc.25";
    /** Константа кода (code) элемента : Состояние «Отпуск по беременности и родам» (title) */
    String STATUS_MATERNITY_LEAVE = "movestudentrmc.26";
    /** Константа кода (code) элемента : Состояние «Отпуск по уходу за ребенком» (title) */
    String STATUS_CHILD_REARING_LEAVE = "movestudentrmc.27";
    /** Константа кода (code) элемента : Состояние «Отпуск по уходу за ребенком без посещения» (title) */
    String STATUS_CHILD_REARING_LEAVE_WITHOUT_VISITS = "movestudentrmc.28";
    /** Константа кода (code) элемента : Состояние «Отпуск по уходу за ребенком с посещением» (title) */
    String STATUS_CHILD_REARING_LEAVE_WITH_VISITS = "movestudentrmc.29";
    /** Константа кода (code) элемента : Состояние «Отпуск с посещением» (title) */
    String STATUS_VACATION_WITH_VISITS = "movestudentrmc.30";
    /** Константа кода (code) элемента : Состояние «Академический отпуск по мед. показаниям» (title) */
    String STATUS_ACADEMICAL_VACATION_ON_MEDICAL_STATEMENT = "movestudentrmc.31";
    /** Константа кода (code) элемента : Состояние «Отпуск академический без посещения или академический отпуск по мед. показаниям» (title) */
    String STATUS_ACADEMICAL_VACATION_ON_MEDICAL_STATEMENT_OR_WITHOUT_VISITS = "movestudentrmc.32";
    /** Константа кода (code) элемента : Состояние «Отчислен» (title) */
    String STATUS_DEDUCTED = "movestudentrmc.33";
    /** Константа кода (code) элемента : Состояние «Каникулы» (title) */
    String STATUS_HOLIDAYS = "movestudentrmc.34";
    /** Константа кода (code) элемента : Состояние «Практика» (title) */
    String STATUS_PRACTICE = "movestudentrmc.35";
    /** Константа кода (code) элемента : Состояние «Поездка» (title) */
    String STATUS_TRAVEL = "movestudentrmc.36";
    /** Константа кода (code) элемента : Атрибут студента «Выпускной курс» (title) */
    String ATTRIBUTE_GRADUATION_COURSE = "movestudentrmc.37";
    /** Константа кода (code) элемента : «Отсутствие академической задолженности» за последнюю заполненную сессию (title) */
    String ABSENCE_ACADEMICAL_DEBTS = "movestudentrmc.38";
    /** Константа кода (code) элемента : Студенты, обучающиеся на «хорошо» за последнюю заполненную сессию (title) */
    String STUDENT_SESSION_MARK_GOOD = "movestudentrmc.39";
    /** Константа кода (code) элемента : Студенты, обучающиеся на «хорошо» и «отлично» за последнюю заполненную сессию (title) */
    String STUDENT_SESSION_MARK_GOOD_AND_EXCELLENT = "movestudentrmc.40";
    /** Константа кода (code) элемента : Студенты, обучающиеся на «отлично» за последнюю заполненную сессию (title) */
    String STUDENT_SESSION_MARK_EXCELLENT = "movestudentrmc.41";
    /** Константа кода (code) элемента : Студенты, обучающиеся на «хорошо» за последнюю заполненную сессию (учет первоначально сданных оценок – без пересдач) (title) */
    String STUDENT_FIRST_SESSION_MARK_GOOD = "movestudentrmc.42";
    /** Константа кода (code) элемента : Студенты, обучающиеся на «хорошо» и «отлично» за последнюю заполненную сессию (учет первоначально сданных оценок – без пересдач) (title) */
    String STUDENT_FIRST_SESSION_MARK_GOOD_AND_EXCELLENT = "movestudentrmc.43";
    /** Константа кода (code) элемента : Студенты, обучающиеся на «отлично» за последнюю заполненную сессию (учет первоначально сданных оценок – без пересдач) (title) */
    String STUDENT_FIRST_SESSION_MARK_EXCELLENT = "movestudentrmc.44";
    /** Константа кода (code) элемента : Студенты, имеющие льготу «дети-сироты, дети, оставшиеся без попечения родителей и лица из их числа» (title) */
    String STUDENT_BENEFIT_CHILDREN_ORPHAN = "movestudentrmc.45";
    /** Константа кода (code) элемента : Студенты, имеющие льготу «представитель коренных малочисленных народов Крайнего Севера» (title) */
    String STUDENT_NEED_SOCIAL_PAYMENT = "movestudentrmc.46";
    /** Константа кода (code) элемента : Студенты, имеющие льготу «граждане, проходившие не менее 3х лет военную службу по контракту в Вооруженных силах РФ и уволенные с военной службы» (title) */
    String STUDENT_MILITARY_BENEFIT = "movestudentrmc.47";
    /** Константа кода (code) элемента : Студенты, имеющие хотя бы одну из льгот: A. «лицам из числа детей-сирот и детей, оставшихся без попечения родителей» B. «лицам, пострадавшим в результате аварии на Чернобыльской АЭС и других радиационных катастроф» C. «лицам, являющимся инвалидами и ветеранами боевых действий» D. «лицам, признанным в установленном порядке инвалидами I и II групп» E. «студентам, являющимся членами малоимущих семей»  (title) */
    String STUDENT_OTHER_BENEFIT = "movestudentrmc.48";
    /** Константа кода (code) элемента : Условие «Начало периода назначения стипендии студенту, являющемуся членом малоимущей семьи, должно быть не больше даты окончания срока действия справки о статусе малоимущей семьи» (title) */
    String STUDENT_SOCIAL_GRANT = "movestudentrmc.49";

    Set<String> CODES = ImmutableSet.of(STATUS_ARCHIVAL, STATUS_ACTIVE, STATUS_POSSIBLE, STATUS_GRADUATED_WITH_DPLOMA, STATUS_DEFER_PROTECTION, CITIZENS_R_F, DEVELOP_FORM_INTERNAL, COMPENSATION_TYPE_BUDGET, HIGH, BACHELOR_AND_SPECIALTY, MASTER, MIDDLE, COURSE1_BACHELOR_AND_SPECIALTY, COURSE2_BACHELOR_AND_SPECIALTY, COURSE3_BACHELOR_AND_SPECIALTY, COURSE4_BACHELOR_AND_SPECIALTY, COURSE5_BACHELOR_AND_SPECIALTY, COURSE6_BACHELOR_AND_SPECIALTY, COURSE1_MASTER, COURSE2_MASTER, ARKHANGELSK, SEVERODVINSK, KORJAZHMA, STATUS_ACADEMICAL_VACATION_WITHOUT_VISITS, STATUS_VACATION_WITHOUT_VISITS, STATUS_MATERNITY_LEAVE, STATUS_CHILD_REARING_LEAVE, STATUS_CHILD_REARING_LEAVE_WITHOUT_VISITS, STATUS_CHILD_REARING_LEAVE_WITH_VISITS, STATUS_VACATION_WITH_VISITS, STATUS_ACADEMICAL_VACATION_ON_MEDICAL_STATEMENT, STATUS_ACADEMICAL_VACATION_ON_MEDICAL_STATEMENT_OR_WITHOUT_VISITS, STATUS_DEDUCTED, STATUS_HOLIDAYS, STATUS_PRACTICE, STATUS_TRAVEL, ATTRIBUTE_GRADUATION_COURSE, ABSENCE_ACADEMICAL_DEBTS, STUDENT_SESSION_MARK_GOOD, STUDENT_SESSION_MARK_GOOD_AND_EXCELLENT, STUDENT_SESSION_MARK_EXCELLENT, STUDENT_FIRST_SESSION_MARK_GOOD, STUDENT_FIRST_SESSION_MARK_GOOD_AND_EXCELLENT, STUDENT_FIRST_SESSION_MARK_EXCELLENT, STUDENT_BENEFIT_CHILDREN_ORPHAN, STUDENT_NEED_SOCIAL_PAYMENT, STUDENT_MILITARY_BENEFIT, STUDENT_OTHER_BENEFIT, STUDENT_SOCIAL_GRANT);
}
