package ru.tandemservice.movestudentrmc.component.settings.checkOnOrderToRepresentType.CheckOnOrderToRepresentTypePub;

import com.google.common.collect.ImmutableList;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.movestudentrmc.entity.RelCheckOnorderRepresentType;
import ru.tandemservice.movestudentrmc.entity.catalog.CheckOnOrder;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.OrderCategoryCodes;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        CheckOnOrder check = get(CheckOnOrder.class, model.getCheck().getId());

        if (check != null)
            model.setCheck(check);

        model.setUsedModel(new LazySimpleSelectModel<>(ImmutableList.of(
                new IdentifiableWrapper(Model.SHOW_USE_CODE, "Да"),
                new IdentifiableWrapper(Model.SHOW_NON_USE_CODE, "Нет"))));
    }

    @Override
    public void prepareListDataSource(Model model) {

        DQLSelectBuilder builder = createBuilder(model);

        List<RepresentationType> types = getList(builder);
        List<Wrapper> lst = new ArrayList<>();

        IdentifiableWrapper used = model.getSettings().get("used");

        for (RepresentationType type : types) {
            RelCheckOnorderRepresentType rel = getCheckOnorderRepresentType(model.getCheck(), type);

            boolean use = false;
            boolean value = false;

            if (rel != null) {
                use = true;
                value = rel.isValue();
            }

            if (used != null) {
                if (!(use == used.getId().equals(Model.SHOW_USE_CODE)))
                    continue;
            }

            lst.add(new Wrapper(type, use, value));

        }

        model.getDataSource().setCountRow(lst.size());

        Collections.sort(lst, new EntityComparator<>(model.getDataSource().getEntityOrder()));
        UniBaseUtils.createPage(model.getDataSource(), lst);
    }

    private RelCheckOnorderRepresentType getCheckOnorderRepresentType(CheckOnOrder check, RepresentationType type) {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RelCheckOnorderRepresentType.class, "rel")
                .where(DQLExpressions.eqValue(DQLExpressions.property(RelCheckOnorderRepresentType.check().fromAlias("rel")), check))
                .where(DQLExpressions.eqValue(DQLExpressions.property(RelCheckOnorderRepresentType.type().fromAlias("rel")), type));

        return builder.createStatement(getSession()).uniqueResult();
    }

    private DQLSelectBuilder createBuilder(Model model) {

        String title = model.getSettings().get("title");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RepresentationType.class, "r")
                .column("r")
                .where(DQLExpressions.eqValue(DQLExpressions.property(RepresentationType.orderCategory().code().fromAlias("r")), OrderCategoryCodes.MOVEMENTSTUDENT));

        if (title != null)
            builder.where(DQLExpressions.like(DQLExpressions.property(RepresentationType.title().fromAlias("r")), DQLExpressions.value(CoreStringUtils.escapeLike(title))));

        return builder;
    }

    public void updateInUse(Model model, Long id) {
        RepresentationType type = getNotNull(RepresentationType.class, id);
        RelCheckOnorderRepresentType rel = getCheckOnorderRepresentType(model.getCheck(), type);

        if (rel == null) {
            RelCheckOnorderRepresentType entity = new RelCheckOnorderRepresentType();
            entity.setCheck(model.getCheck());
            entity.setType(type);
            getSession().saveOrUpdate(entity);
        }
        else {
            new DQLDeleteBuilder(RelCheckOnorderRepresentType.class)
                    .where(DQLExpressions.eqValue(DQLExpressions.property(RelCheckOnorderRepresentType.check()), model.getCheck()))
                    .where(DQLExpressions.eqValue(DQLExpressions.property(RelCheckOnorderRepresentType.type()), type))
                    .createStatement(getSession()).execute();
        }

    }

    public void updateValue(Model model, Long id) {
        RepresentationType type = getNotNull(RepresentationType.class, id);
        RelCheckOnorderRepresentType rel = getCheckOnorderRepresentType(model.getCheck(), type);

        if (rel != null) {
            if (rel.isValue())
                rel.setValue(false);
            else
                rel.setValue(true);

            getSession().saveOrUpdate(rel);
        }
    }

}
