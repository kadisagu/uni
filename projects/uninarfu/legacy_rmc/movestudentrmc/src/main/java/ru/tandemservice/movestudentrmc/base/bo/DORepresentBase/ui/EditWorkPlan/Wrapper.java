package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.EditWorkPlan;

import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;

public class Wrapper {

    private DevelopGridTerm gridTerm;
    private EppStudent2WorkPlan workPlan;

    public String getTermTitle() {
        StringBuilder sb = new StringBuilder()
                .append(gridTerm.getTerm().getTitle())
                .append(", ")
                .append(gridTerm.getPart().getTitle())
                .append(", ")
                .append(gridTerm.getCourse().getTitle())
                .append(" курс");
        return sb.toString();
    }

    public String getYearTitle() {
        if (workPlan != null)
            return workPlan.getEppYear().getEducationYear().getTitle();
        else return "";
    }

    public String getPlanTitle() {
        if (workPlan != null)
            return workPlan.getWorkPlan().getTitle();
        else return "";
    }

    public DevelopGridTerm getGridTerm() {
        return gridTerm;
    }

    public void setGridTerm(DevelopGridTerm gridTerm) {
        this.gridTerm = gridTerm;
    }

    public EppStudent2WorkPlan getWorkPlan() {
        return workPlan;
    }

    public void setWorkPlan(EppStudent2WorkPlan workPlan) {
        this.workPlan = workPlan;
    }


}
