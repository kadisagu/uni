package ru.tandemservice.movestudentrmc.base.bo.DocumentSettings.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreStringUtils;
import ru.tandemservice.movestudentrmc.entity.catalog.DocumentKind;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DocumentKindDSHandler extends DefaultComboDataSourceHandler {

    public DocumentKindDSHandler(String ownerId) {

        super(ownerId, DocumentKind.class, DocumentKind.P_TITLE);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep) {
        String filter = ep.input.getComboFilterByValue();
        if (StringUtils.isNotBlank(filter)) {
            ep.dqlBuilder.where(likeUpper(property(DocumentKind.title().fromAlias("e")), value(CoreStringUtils.escapeLike(filter, true))));
        }
    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep) {
        ep.dqlBuilder.order("e." + DocumentKind.title());
    }
}
