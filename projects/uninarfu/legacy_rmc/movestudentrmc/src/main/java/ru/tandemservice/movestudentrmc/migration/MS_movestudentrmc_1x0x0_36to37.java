package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_movestudentrmc_1x0x0_36to37 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.10"),
                        new ScriptDependency("org.tandemframework.shared", "1.1.3"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.1.3")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность principal2ListOrderVisa

        // сущность была удалена
        if (tool.tableExists("principal2listordervisa_t")) {
            // TODO: программист должен подтвердить это действие

            // удалить таблицу
            tool.dropTable("principal2listordervisa_t", true /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("principal2ListOrderVisa");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность principal2DocumentOrderVisa

        // сущность была удалена
        if (tool.tableExists("principal2documentordervisa_t")) {
            // TODO: программист должен подтвердить это действие
/*			if( true )
                throw new UnsupportedOperationException("Confirm me: delete table");
*/
            // удалить таблицу
            tool.dropTable("principal2documentordervisa_t", true /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("principal2DocumentOrderVisa");

        }

        if (tool.tableExists("principal2visa_t"))
            tool.dropTable("principal2visa_t", true);


    }
}