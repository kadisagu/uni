package ru.tandemservice.movestudentrmc.base.bo.DORepresentCourseTransfer.ui.View;

import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentViewUI;
import ru.tandemservice.movestudentrmc.entity.RepresentCourseTransfer;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

public class DORepresentCourseTransferViewUI extends AbstractDORepresentViewUI
{

    public EducationOrgUnit getEducationOrgUnit() {
        RepresentCourseTransfer rep = (RepresentCourseTransfer) this._represent;
        if (rep.getEducationOrgUnit() != null)
            return rep.getEducationOrgUnit();
        else
            return rep.getGroup().getEducationOrgUnit();
    }
}
