package ru.tandemservice.movestudentrmc.component.catalog.representationReason.RepresentationReasonPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;

public interface IDAO extends IDefaultCatalogPubDAO<RepresentationReason, Model> {
    void updatePrintable(Model model, Long id);

    public void updateUsed(Model model, Long id);

    public boolean checkRelationReason(Long reasonId);

}
