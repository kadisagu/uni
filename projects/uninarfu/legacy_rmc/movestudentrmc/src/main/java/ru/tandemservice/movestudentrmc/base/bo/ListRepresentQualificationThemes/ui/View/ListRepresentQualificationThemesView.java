package ru.tandemservice.movestudentrmc.base.bo.ListRepresentQualificationThemes.ui.View;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.RelListRepresentStudentsDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.StudentDSHandler;
import ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents;


@Configuration
public class ListRepresentQualificationThemesView extends BusinessComponentManager {

    public static final String STUDENT_DS = "studentDS";

    @Bean
    public ColumnListExtPoint studentDS()
    {
        return columnListExtPointBuilder(STUDENT_DS)
                .addColumn(publisherColumn(StudentDSHandler.STUDENT_FIO_COLUMN, RelListRepresentStudents.student().person().identityCard().fullFio()).publisherLinkResolver(new IPublisherLinkResolver() {
                    @Override
                    public Object getParameters(IEntity entity)
                    {
                        RelListRepresentStudents representStudents = (RelListRepresentStudents) entity;
                        return new UniMap().add(PublisherActivator.PUBLISHER_ID_KEY, representStudents.getStudent() != null ? representStudents.getStudent().getId() : null).add("selectedStudentTab", "studentTab");
                    }

                    @Override
                    public String getComponentName(IEntity entity)
                    {
                        return null;
                    }
                }).order().order(OrderDirection.asc).create())
                .addColumn(textColumn(StudentDSHandler.TERRITORIAL_ORG_UNIT_COLUMN, RelListRepresentStudents.student().educationOrgUnit().territorialOrgUnit().fullTitle()).order().create())
                .addColumn(textColumn(StudentDSHandler.FORMATIVE_ORG_UNIT_COLUMN, RelListRepresentStudents.student().educationOrgUnit().formativeOrgUnit().title()).order().create())
                .addColumn(textColumn(StudentDSHandler.EDUCATION_LEVEL_HIGH_SCHOOL_COLUMN, RelListRepresentStudents.student().educationOrgUnit().educationLevelHighSchool().orgUnit().fullTitle()).order().create())
                .addColumn(textColumn(StudentDSHandler.EDUCATION_ORG_UNIT_COLUMN, RelListRepresentStudents.student().educationOrgUnit().title()).order().create())
                .addColumn(textColumn(StudentDSHandler.STATUS_COLUMN, RelListRepresentStudents.student().status().title()).order().create())

                .addColumn(textColumn(StudentDSHandler.COURSE_COLUMN, RelListRepresentStudents.student().course().title()).order().create())
                .addColumn(textColumn(StudentDSHandler.GROUP_COLUMN, RelListRepresentStudents.student().group().title()).order().create())
                .addColumn(textColumn(StudentDSHandler.DEVELOP_FORM_COLUMN, RelListRepresentStudents.student().educationOrgUnit().developForm().title()).order().create())

                .addColumn(textColumn(StudentDSHandler.COMPENSATION_TYPE_COLUMN, RelListRepresentStudents.student().compensationType().shortTitle()).order().create())
                .addColumn(textColumn(StudentDSHandler.DYPLOM_COLUMN, RelListRepresentStudents.student().educationOrgUnit().educationLevelHighSchool().assignedQualification().title()).order().create())
                .create();
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(STUDENT_DS, studentDS()).handler(relListRepresentStudentsDSHandler()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> relListRepresentStudentsDSHandler()
    {
        return new RelListRepresentStudentsDSHandler(getName());
    }

}
