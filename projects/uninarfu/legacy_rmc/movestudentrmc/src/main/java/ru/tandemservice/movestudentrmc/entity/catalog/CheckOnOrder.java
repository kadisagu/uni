package ru.tandemservice.movestudentrmc.entity.catalog;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.entity.RelCheckOnorderGrantView;
import ru.tandemservice.movestudentrmc.entity.RelCheckOnorderRepresentType;
import ru.tandemservice.movestudentrmc.entity.catalog.gen.CheckOnOrderGen;
import ru.tandemservice.uni.dao.UniDaoFacade;

/**
 * Проверки по приказам
 */
public class CheckOnOrder extends CheckOnOrderGen {
    @EntityDSLSupport(parts = {P_TITLE})
    public Boolean isConfigRelation() {

        Boolean result;

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RelCheckOnorderRepresentType.class, "rel").column("rel");
        builder.where(DQLExpressions.eq(DQLExpressions.property(RelCheckOnorderRepresentType.check().id().fromAlias("rel")), DQLExpressions.value(this.getId())));

        result = UniDaoFacade.getCoreDao().getCount(builder) > 0;

        return result;
    }

    @EntityDSLSupport(parts = {P_TITLE})
    public Boolean isGrantConfigRelation() {

        Boolean result;

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RelCheckOnorderGrantView.class, "rel").column("rel");
        builder.where(DQLExpressions.eq(DQLExpressions.property(RelCheckOnorderGrantView.check().id().fromAlias("rel")), DQLExpressions.value(this.getId())));

        result = UniDaoFacade.getCoreDao().getCount(builder) > 0;

        return result;
    }

    @Override
    public Boolean getGrantConfigRelation() {
        return isGrantConfigRelation();
    }

    @Override
    public Boolean getConfigRelation() {
        return isConfigRelation();
    }
}