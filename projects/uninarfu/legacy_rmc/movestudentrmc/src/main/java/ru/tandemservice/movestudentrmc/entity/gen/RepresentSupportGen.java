package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudentrmc.entity.IStudentGrant;
import ru.tandemservice.movestudentrmc.entity.RepresentSupport;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * О назначении социальной поддержки в связи с тяжелым материальным положением
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RepresentSupportGen extends Representation
 implements IStudentGrant{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.RepresentSupport";
    public static final String ENTITY_NAME = "representSupport";
    public static final int VERSION_HASH = 2018615445;
    private static IEntityMeta ENTITY_META;

    public static final String P_BEGIN_GRANT_DATE = "beginGrantDate";
    public static final String P_END_GRANT_DATE = "endGrantDate";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String L_EDUCATION_YEAR = "educationYear";
    public static final String L_GRANT_VIEW = "grantView";
    public static final String P_SUM = "sum";

    private Date _beginGrantDate;     // Дата назначения выплат
    private Date _endGrantDate;     // Дата окончания выплат
    private Date _beginDate;     // Месяц выплаты
    private EducationYear _educationYear;     // Учебный год
    private GrantView _grantView;     // Вид выплаты
    private double _sum;     // Размер выплаты

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата назначения выплат. Свойство не может быть null.
     *
     * Это формула "beginDate".
     */
    // @NotNull
    public Date getBeginGrantDate()
    {
        return _beginGrantDate;
    }

    /**
     * @param beginGrantDate Дата назначения выплат. Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setBeginGrantDate(Date beginGrantDate)
    {
        dirty(_beginGrantDate, beginGrantDate);
        _beginGrantDate = beginGrantDate;
    }

    /**
     * @return Дата окончания выплат.
     *
     * Это формула "null".
     */
    public Date getEndGrantDate()
    {
        return _endGrantDate;
    }

    /**
     * @param endGrantDate Дата окончания выплат.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setEndGrantDate(Date endGrantDate)
    {
        dirty(_endGrantDate, endGrantDate);
        _endGrantDate = endGrantDate;
    }

    /**
     * @return Месяц выплаты. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Месяц выплаты. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    /**
     * @param educationYear Учебный год. Свойство не может быть null.
     */
    public void setEducationYear(EducationYear educationYear)
    {
        dirty(_educationYear, educationYear);
        _educationYear = educationYear;
    }

    /**
     * @return Вид выплаты. Свойство не может быть null.
     */
    @NotNull
    public GrantView getGrantView()
    {
        return _grantView;
    }

    /**
     * @param grantView Вид выплаты. Свойство не может быть null.
     */
    public void setGrantView(GrantView grantView)
    {
        dirty(_grantView, grantView);
        _grantView = grantView;
    }

    /**
     * @return Размер выплаты. Свойство не может быть null.
     */
    @NotNull
    public double getSum()
    {
        return _sum;
    }

    /**
     * @param sum Размер выплаты. Свойство не может быть null.
     */
    public void setSum(double sum)
    {
        dirty(_sum, sum);
        _sum = sum;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof RepresentSupportGen)
        {
            setBeginGrantDate(((RepresentSupport)another).getBeginGrantDate());
            setEndGrantDate(((RepresentSupport)another).getEndGrantDate());
            setBeginDate(((RepresentSupport)another).getBeginDate());
            setEducationYear(((RepresentSupport)another).getEducationYear());
            setGrantView(((RepresentSupport)another).getGrantView());
            setSum(((RepresentSupport)another).getSum());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RepresentSupportGen> extends Representation.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RepresentSupport.class;
        }

        public T newInstance()
        {
            return (T) new RepresentSupport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "beginGrantDate":
                    return obj.getBeginGrantDate();
                case "endGrantDate":
                    return obj.getEndGrantDate();
                case "beginDate":
                    return obj.getBeginDate();
                case "educationYear":
                    return obj.getEducationYear();
                case "grantView":
                    return obj.getGrantView();
                case "sum":
                    return obj.getSum();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "beginGrantDate":
                    obj.setBeginGrantDate((Date) value);
                    return;
                case "endGrantDate":
                    obj.setEndGrantDate((Date) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "educationYear":
                    obj.setEducationYear((EducationYear) value);
                    return;
                case "grantView":
                    obj.setGrantView((GrantView) value);
                    return;
                case "sum":
                    obj.setSum((Double) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "beginGrantDate":
                        return true;
                case "endGrantDate":
                        return true;
                case "beginDate":
                        return true;
                case "educationYear":
                        return true;
                case "grantView":
                        return true;
                case "sum":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "beginGrantDate":
                    return true;
                case "endGrantDate":
                    return true;
                case "beginDate":
                    return true;
                case "educationYear":
                    return true;
                case "grantView":
                    return true;
                case "sum":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "beginGrantDate":
                    return Date.class;
                case "endGrantDate":
                    return Date.class;
                case "beginDate":
                    return Date.class;
                case "educationYear":
                    return EducationYear.class;
                case "grantView":
                    return GrantView.class;
                case "sum":
                    return Double.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RepresentSupport> _dslPath = new Path<RepresentSupport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RepresentSupport");
    }
            

    /**
     * @return Дата назначения выплат. Свойство не может быть null.
     *
     * Это формула "beginDate".
     * @see ru.tandemservice.movestudentrmc.entity.RepresentSupport#getBeginGrantDate()
     */
    public static PropertyPath<Date> beginGrantDate()
    {
        return _dslPath.beginGrantDate();
    }

    /**
     * @return Дата окончания выплат.
     *
     * Это формула "null".
     * @see ru.tandemservice.movestudentrmc.entity.RepresentSupport#getEndGrantDate()
     */
    public static PropertyPath<Date> endGrantDate()
    {
        return _dslPath.endGrantDate();
    }

    /**
     * @return Месяц выплаты. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentSupport#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentSupport#getEducationYear()
     */
    public static EducationYear.Path<EducationYear> educationYear()
    {
        return _dslPath.educationYear();
    }

    /**
     * @return Вид выплаты. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentSupport#getGrantView()
     */
    public static GrantView.Path<GrantView> grantView()
    {
        return _dslPath.grantView();
    }

    /**
     * @return Размер выплаты. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentSupport#getSum()
     */
    public static PropertyPath<Double> sum()
    {
        return _dslPath.sum();
    }

    public static class Path<E extends RepresentSupport> extends Representation.Path<E>
    {
        private PropertyPath<Date> _beginGrantDate;
        private PropertyPath<Date> _endGrantDate;
        private PropertyPath<Date> _beginDate;
        private EducationYear.Path<EducationYear> _educationYear;
        private GrantView.Path<GrantView> _grantView;
        private PropertyPath<Double> _sum;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата назначения выплат. Свойство не может быть null.
     *
     * Это формула "beginDate".
     * @see ru.tandemservice.movestudentrmc.entity.RepresentSupport#getBeginGrantDate()
     */
        public PropertyPath<Date> beginGrantDate()
        {
            if(_beginGrantDate == null )
                _beginGrantDate = new PropertyPath<Date>(RepresentSupportGen.P_BEGIN_GRANT_DATE, this);
            return _beginGrantDate;
        }

    /**
     * @return Дата окончания выплат.
     *
     * Это формула "null".
     * @see ru.tandemservice.movestudentrmc.entity.RepresentSupport#getEndGrantDate()
     */
        public PropertyPath<Date> endGrantDate()
        {
            if(_endGrantDate == null )
                _endGrantDate = new PropertyPath<Date>(RepresentSupportGen.P_END_GRANT_DATE, this);
            return _endGrantDate;
        }

    /**
     * @return Месяц выплаты. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentSupport#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(RepresentSupportGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentSupport#getEducationYear()
     */
        public EducationYear.Path<EducationYear> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new EducationYear.Path<EducationYear>(L_EDUCATION_YEAR, this);
            return _educationYear;
        }

    /**
     * @return Вид выплаты. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentSupport#getGrantView()
     */
        public GrantView.Path<GrantView> grantView()
        {
            if(_grantView == null )
                _grantView = new GrantView.Path<GrantView>(L_GRANT_VIEW, this);
            return _grantView;
        }

    /**
     * @return Размер выплаты. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentSupport#getSum()
     */
        public PropertyPath<Double> sum()
        {
            if(_sum == null )
                _sum = new PropertyPath<Double>(RepresentSupportGen.P_SUM, this);
            return _sum;
        }

        public Class getEntityClass()
        {
            return RepresentSupport.class;
        }

        public String getEntityName()
        {
            return "representSupport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
