package ru.tandemservice.movestudentrmc.base.bo.DOOrder.ui.TabPanel;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.ui.List.DOOrderList;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.ui.ListCommit.DOOrderListCommit;

@Configuration
public class DOOrderTabPanel extends BusinessComponentManager {

    //Панель вкладок
    public static final String TAB_PANEL = "tabPanel";

    //Вкладки
    public static final String ORDER_WORK_TAB = "orderWorkTab";
    public static final String ORDER_COMMIT_TAB = "orderCommitTab";

    @Bean
    public TabPanelExtPoint tabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(TAB_PANEL)
                .addTab(componentTab(ORDER_WORK_TAB, DOOrderList.class).permissionKey("rmc_view_order_tab"))
                .addTab(componentTab(ORDER_COMMIT_TAB, DOOrderListCommit.class).permissionKey("rmc_view_commited_order_tab"))
                .create();
    }

}
