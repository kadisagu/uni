package ru.tandemservice.movestudentrmc.base.bo.GrantsRelationSettings.logic;

import org.tandemframework.hibsupport.dao.CommonDAO;
import ru.tandemservice.movestudentrmc.entity.RelTypeGrantView;
import ru.tandemservice.movestudentrmc.entity.catalog.Grant;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantType;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;

import java.util.List;

public class RelationSettingsModifyDAO extends CommonDAO implements IRelationSettingsModifyDAO {


    public void saveGrantType(GrantType grantType, List<FakeGrantsHandler> fakeGrantsList) {

        for (FakeGrantsHandler fakeGrant : fakeGrantsList) {

            for (GrantView view : fakeGrant.getViewList()) {

                RelTypeGrantView grantView = new RelTypeGrantView();

                grantView.setType(grantType);
                grantView.setGrant(fakeGrant.getGrant());
                grantView.setView(view);

                save(grantView);
            }
        }
    }

    public void updateGrantType(GrantType grantType, List<FakeGrantsHandler> fakeGrantsList, List<RelTypeGrantView> grantViewList) {

        //Step of list DB        
        for (RelTypeGrantView grantView : grantViewList) {
            Boolean isFindGrant = false;
            for (FakeGrantsHandler fakeGrant : fakeGrantsList) {
                if (fakeGrant.getGrant().equals(grantView.getGrant())) {
                    Boolean isFindView = false;
                    for (GrantView view : fakeGrant.getViewList()) {
                        if (view.equals(grantView.getView())) {
                            isFindView = true;
                            break;
                        }
                    }
                    if (!isFindView) {
                        delete(grantView);
                    }
                    isFindGrant = true;
                    break;
                }
            }

            if (!isFindGrant) {
                delete(grantView);
            }
        }

        for (FakeGrantsHandler fakeGrant : fakeGrantsList) {
            for (GrantView view : fakeGrant.getViewList()) {
                Boolean isFind = false;
                for (RelTypeGrantView grantView : grantViewList) {
                    if (grantView.getView().equals(view) && grantView.getGrant().equals(fakeGrant.getGrant())) {
                        isFind = true;
                        break;
                    }
                }
                if (!isFind) {
                    RelTypeGrantView grantView = new RelTypeGrantView();

                    grantView.setType(grantType);
                    grantView.setGrant(fakeGrant.getGrant());
                    grantView.setView(view);

                    save(grantView);
                }
            }
        }
    }

    public void updateRelations(GrantType grantType, Grant grant, List<RelTypeGrantView> relList) {

        for (RelTypeGrantView rel : relList) {

            if (!rel.isJoinGroup())
                rel.setGroupName(null);
            update(rel);
        }
    }
}
