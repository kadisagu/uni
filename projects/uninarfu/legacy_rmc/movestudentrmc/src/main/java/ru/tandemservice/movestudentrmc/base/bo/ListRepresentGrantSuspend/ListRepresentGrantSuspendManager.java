package ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantSuspend;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantSuspend.logic.ListRepresentGrantSuspendManagerModifyDAO;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantSuspend.ui.Edit.ListRepresentGrantSuspendEdit;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantSuspend.ui.View.ListRepresentGrantSuspendView;
import ru.tandemservice.movestudentrmc.docord.IListObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IListObjectModifyDAO;

@Configuration
public class ListRepresentGrantSuspendManager extends BusinessObjectManager implements IListObjectByRepresentTypeManager {

    public static ListRepresentGrantSuspendManager instance() {
        return instance(ListRepresentGrantSuspendManager.class);
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return ListRepresentGrantSuspendEdit.class;
    }

    @Bean
    @Override
    public IListObjectModifyDAO getListObjectModifyDAO() {
        return new ListRepresentGrantSuspendManagerModifyDAO();
    }

    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return ListRepresentGrantSuspendView.class;
    }
}
