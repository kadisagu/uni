package ru.tandemservice.movestudentrmc.base.bo.ListRepresentTransfer.ui.Edit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.catalog.entity.Benefit;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.DevelopConditionDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.DevelopPeriodDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.DevelopTechDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentTransfer.logic.EducationLevelsHighSchoolTransferDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentTransfer.logic.GroupTransferDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.*;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.DevelopFormDSHandler;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

@Configuration
public class ListRepresentTransferEdit extends BusinessComponentManager {

    public static final String STUDENT_DS = "studentDS";
    public static final String STUDENT_SELECTED_DS = "studentSelectedDS";
    public static final String DEVELOP_FORM_DS = "developFormDS";
    public static final String FORMATIVE_ORG_UNIT_DS = "formativeOrgUnitDS";
    public static final String TERRITOLRIAL_ORG_UNIT = "territorialOrgUnitDS";

    public static final String TYPE_REPRESENT_DS = "typeRepresentDS";
    public static final String REASON_REPRESENT_DS = "reasonRepresentDS";
    public static final String BASEMENT_REPRESENT_DS = "basementRepresentDS";


    public static final String EDUCATION_LEVELS_HIGH_SHOOL_DS = "educationLevelsHighSchoolDS";
    public static final String EDUCATION_YEAR_DS = "educationYearDS";
    public static final String ENROLLMENT_DIRECTION_DS = "enrollmentDirectionDS";
    public static final String EDUCATION_LEVEL_DS = "educationLevelDS";
    public static final String QUALIFICATION_DS = "qualificationDS";
    public static final String GROUP_DS = "groupDS";

    public static final String BENEFIT_DS = "benefitDS";
    public static final String DOCUMENT_DS = "documentDS";

    public static final String EDUCATION_ORG_UNIT = "educationOrgUnitDS";
    public static final String STUDENT_COMBO_DS = "studentComboDS";

    public static final String COMPENSATION_TYPE_DS = "compensationTypeDS";

    public static final String STUDENT_STATUS_DS = "studentStatusDS";

    public static final String FORM_ORG_UNIT_DS = "formOrgUnitDS";
    public static final String DEVELOP_FORM_DS2 = "developFormDS2";
    public static final String COMPENSATION_TYPE_DS2 = "compensationTypeDS2";
    public static final String EDU_LEVEL_DS = "eduLevelDS";
    public static final String COURSE_DS2 = "courseDS2";
    public static final String GROUP_DS2 = "groupDS2";

    public static final String DEVELOP_CONDITION_DS = "developConditionDS2";
    public static final String DEVELOP_PERIOD_DS = "developPeriodDS2";
    public static final String DEVELOP_TECH_DS = "developTechDS2";
    public static final String TERR_ORGUNIT_DS = "terrOrgUnitDS";

    @Bean
    public ColumnListExtPoint studentDS()
    {
        return columnListExtPointBuilder(STUDENT_DS)
                .addColumn(checkboxColumn(StudentDSHandler.CHECKBOX_COLUMN).create())
                .addColumn(publisherColumn(StudentDSHandler.STUDENT_FIO_COLUMN, Student.person().identityCard().fullFio()).publisherLinkResolver(new IPublisherLinkResolver() {
                    @Override
                    public Object getParameters(IEntity entity)
                    {
                        Student student = (Student) entity;
                        return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, student != null ? student.getId() : null).add("selectedStudentTab", "studentTab");
                    }

                    @Override
                    public String getComponentName(IEntity entity)
                    {
                        return null;
                    }
                }).order().create())
                .addColumn(textColumn(StudentDSHandler.TERRITORIAL_ORG_UNIT_COLUMN, Student.educationOrgUnit().territorialOrgUnit().shortTitle()).order().create())
                .addColumn(textColumn(StudentDSHandler.FORMATIVE_ORG_UNIT_COLUMN, Student.educationOrgUnit().formativeOrgUnit().title()).order().create())
                .addColumn(textColumn(StudentDSHandler.EDUCATION_LEVEL_HIGH_SCHOOL_COLUMN, Student.educationOrgUnit().educationLevelHighSchool().displayableTitle()).order().create())
                .addColumn(textColumn(StudentDSHandler.EDUCATION_ORG_UNIT_COLUMN, Student.educationOrgUnit().educationLevelHighSchool().orgUnit().title()).order().create())
                .addColumn(textColumn(StudentDSHandler.STATUS_COLUMN, Student.status().title()).order().create())
                .addColumn(textColumn(StudentDSHandler.COURSE_COLUMN, Student.course().title()).order().create())
                .addColumn(textColumn(StudentDSHandler.GROUP_COLUMN, Student.group().title()).order().create())
                .addColumn(textColumn(StudentDSHandler.DEVELOP_FORM_COLUMN, Student.educationOrgUnit().developForm().title()).order().create())

                .addColumn(textColumn(StudentDSHandler.COMPENSATION_TYPE_COLUMN, Student.compensationType().shortTitle()).order().create())
                .addColumn(textColumn(StudentDSHandler.DYPLOM_COLUMN, Student.educationOrgUnit().educationLevelHighSchool().assignedQualification().title()).order().create())

                .create();

    }

    @Bean
    public ColumnListExtPoint studentSelectedDS()
    {
        return columnListExtPointBuilder(STUDENT_DS)
                .addColumn(checkboxColumn(StudentDSHandler.CHECKBOX_COLUMN).create())
                .addColumn(textColumn(StudentDSHandler.STUDENT_FIO_COLUMN, Student.person().identityCard().fullFio()).order().create())
                .addColumn(textColumn(StudentDSHandler.TERRITORIAL_ORG_UNIT_COLUMN, Student.educationOrgUnit().territorialOrgUnit().shortTitle()).order().create())
                .addColumn(textColumn(StudentDSHandler.FORMATIVE_ORG_UNIT_COLUMN, Student.educationOrgUnit().formativeOrgUnit().title()).order().create())
                .addColumn(textColumn(StudentDSHandler.EDUCATION_LEVEL_HIGH_SCHOOL_COLUMN, Student.educationOrgUnit().educationLevelHighSchool().displayableTitle()).order().create())
                .addColumn(textColumn(StudentDSHandler.EDUCATION_ORG_UNIT_COLUMN, Student.educationOrgUnit().educationLevelHighSchool().orgUnit().title()).order().create())
                .addColumn(textColumn(StudentDSHandler.STATUS_COLUMN, Student.status().title()).order().create())
                .addColumn(textColumn(StudentDSHandler.COURSE_COLUMN, Student.course().title()).order().create())
                .addColumn(textColumn(StudentDSHandler.GROUP_COLUMN, Student.group().title()).order().create())
                .addColumn(textColumn(StudentDSHandler.DEVELOP_FORM_COLUMN, Student.educationOrgUnit().developForm().title()).order().create())

                .addColumn(textColumn(StudentDSHandler.COMPENSATION_TYPE_COLUMN, Student.compensationType().shortTitle()).order().create())
                .addColumn(textColumn(StudentDSHandler.DYPLOM_COLUMN, Student.educationOrgUnit().educationLevelHighSchool().assignedQualification().title()).order().create())

                .create();
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(STUDENT_DS, studentDS()).handler(studentDSHandler()))
                .addDataSource(searchListDS(STUDENT_SELECTED_DS, studentSelectedDS()).handler(studentSelectedDSHandler()))
                .addDataSource(selectDS(DEVELOP_FORM_DS, developFormDSHandler()))
                .addDataSource(selectDS(FORMATIVE_ORG_UNIT_DS, formativeOrgUnitDSHandler()))


                .addDataSource(selectDS(EDUCATION_LEVELS_HIGH_SHOOL_DS, educationLevelsHighSchoolDSHandler()).addColumn(EducationLevelsHighSchool.displayableTitle().s()))
                .addDataSource(selectDS(EDUCATION_YEAR_DS, educationYearDSHandler()))
                .addDataSource(selectDS(EDUCATION_LEVEL_DS, educationLevelDSHandler()).addColumn(StructureEducationLevels.shortTitle().s()))
                .addDataSource(selectDS(QUALIFICATION_DS, qualificationDSHandler()).addColumn(Qualifications.title().s()))

                .addDataSource(UniStudentManger.instance().courseDSConfig())
                .addDataSource(selectDS(GROUP_DS, groupDSHandler()))
                .addDataSource(selectDS(COMPENSATION_TYPE_DS, compensationTypeDSHandler()).addColumn(CompensationType.shortTitle().s()))
                .addDataSource(selectDS(STUDENT_COMBO_DS, studentComboDSHandler()).addColumn(Student.person().identityCard().fullFio().s()))

                .addDataSource(selectDS(BENEFIT_DS, benefitDSHandler()).addColumn(Benefit.shortTitle().s()))
                .addDataSource(selectDS(DOCUMENT_DS, documentDSHandler()))
                .addDataSource(selectDS(EDUCATION_ORG_UNIT, educationOrgUnitDSHandler()))
                .addDataSource(selectDS(TERRITOLRIAL_ORG_UNIT, territorialOrgUnitDSHandler()).addColumn(OrgUnit.shortTitle().s()))
                .addDataSource(selectDS(STUDENT_STATUS_DS, studentStatusDSHandler()))

                .addDataSource(selectDS(FORM_ORG_UNIT_DS, formativeOrgUnitDSHandler()))
                .addDataSource(selectDS(DEVELOP_FORM_DS2, developFormDS2Handler()))
                .addDataSource(selectDS(COMPENSATION_TYPE_DS2, compensationTypeDSHandler()).addColumn(CompensationType.shortTitle().s()))
                .addDataSource(selectDS(EDU_LEVEL_DS, educationLevelsHighSchoolTransferDSHandler()).addColumn(EducationLevelsHighSchool.displayableTitle().s()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(COURSE_DS2, getName(), Course.defaultSelectDSHandler(getName())))
                .addDataSource(selectDS(GROUP_DS2, groupTransferDSHandler()).addColumn("fullTitle"))
                .addDataSource(selectDS(DEVELOP_CONDITION_DS, developCondition()))
                .addDataSource(selectDS(DEVELOP_PERIOD_DS, developPeriodDSHandler()))
                .addDataSource(selectDS(DEVELOP_TECH_DS, developTechDSHandler()))
                .addDataSource(selectDS(TERR_ORGUNIT_DS, terrOrgUnitDSHandler()))

                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler terrOrgUnitDSHandler() {
        return new TerritorialOrgUnitDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> studentDSHandler()
    {
        return new StudentDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> studentSelectedDSHandler()
    {
        return new StudentSelectedDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> developFormDSHandler()
    {
        return new DevelopFormDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> developFormDS2Handler()
    {
        return new ru.tandemservice.movestudentrmc.base.bo.ListRepresentTransfer.logic.DevelopFormDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> formativeOrgUnitDSHandler()
    {
        return new FormativeOrgUnitDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> educationLevelsHighSchoolDSHandler()
    {
        return new EducationLevelsHighSchoolDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> educationLevelsHighSchoolTransferDSHandler()
    {
        return new EducationLevelsHighSchoolTransferDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> educationYearDSHandler()
    {
        return EducationYear.defaultSelectDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> educationLevelDSHandler()
    {
        return new EducationLevelDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> groupDSHandler()
    {
        return new GroupDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> groupTransferDSHandler()
    {
        return new GroupTransferDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> compensationTypeDSHandler()
    {
        return new CompensationTypeDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> studentComboDSHandler()
    {
        return new StudentComboDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> benefitDSHandler()
    {
        return new BenefitDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> documentDSHandler()
    {
        return new DocumentDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> educationOrgUnitDSHandler()
    {
        return new EducationOrgUnitDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> territorialOrgUnitDSHandler()
    {
        return new TerritorialOrgUnitDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> studentStatusDSHandler()
    {
        return new StudentStatusDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler developCondition() {
        return new DevelopConditionDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler developPeriodDSHandler() {
        return new DevelopPeriodDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler developTechDSHandler() {
        return new DevelopTechDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> qualificationDSHandler() {
        return new QualificationDSHandler(getName());
    }
}
