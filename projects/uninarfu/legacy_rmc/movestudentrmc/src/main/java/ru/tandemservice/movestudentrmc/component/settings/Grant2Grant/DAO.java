package ru.tandemservice.movestudentrmc.component.settings.Grant2Grant;

import org.tandemframework.shared.commonbase.base.util.PairKey;
import ru.tandemservice.movestudentrmc.entity.Grant2Grant;
import ru.tandemservice.movestudentrmc.entity.catalog.Grant;
import ru.tandemservice.uni.dao.UniDao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DAO extends UniDao<Model> implements IDAO {

    public void prepare(Model model) {
        List<Grant> firstList = getCatalogItemList(Grant.class);
        model.setFirstList(firstList);

        List<Grant> secondList = new ArrayList<Grant>();
        secondList.addAll(firstList);
        model.setSecondList(secondList);

        List<Grant2Grant> relationList = getList(Grant2Grant.class);
        Map<PairKey<Grant, Grant>, Grant2Grant> map = new HashMap<PairKey<Grant, Grant>, Grant2Grant>();
        for (Grant2Grant rel : relationList)
            map.put(new PairKey<Grant, Grant>(rel.getFirst(), rel.getSecond()), rel);

        Map<PairKey<Grant, Grant>, Wrapper> settings = new HashMap<PairKey<Grant, Grant>, Wrapper>();
        for (Grant first : firstList)
            for (Grant second : secondList) {
                Wrapper wrapper = new Wrapper();
                wrapper.setFirst(first);
                wrapper.setSecond(second);

                PairKey<Grant, Grant> keyObj = new PairKey<Grant, Grant>(first, second);
                Grant2Grant rel = map.get(keyObj);

                wrapper.setRelation(rel);
                wrapper.setChecked(rel != null);

                settings.put(keyObj, wrapper);
            }
        model.setSettings(settings);
    }

    public void update(Model model) {
        for (Wrapper wrapper : model.getSettings().values()) {
            Grant2Grant rel = wrapper.getRelation();

            if (wrapper.isChecked() && rel == null) {
                rel = new Grant2Grant();
                rel.setFirst(wrapper.getFirst());
                rel.setSecond(wrapper.getSecond());
                saveOrUpdate(rel);
                wrapper.setRelation(rel);
            }
            else if (!wrapper.isChecked() && rel != null) {
                delete(rel);
                wrapper.setRelation(null);
            }
        }
    }
}
