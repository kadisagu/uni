package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.IRepresent2Student;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents;
import ru.tandemservice.uni.entity.employee.Student;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность-связь 'Списоч Представление'-'Студенты'
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RelListRepresentStudentsGen extends EntityBase
 implements IRepresent2Student{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents";
    public static final String ENTITY_NAME = "relListRepresentStudents";
    public static final int VERSION_HASH = 787002876;
    private static IEntityMeta ENTITY_META;

    public static final String L_REPRESENTATION = "representation";
    public static final String L_STUDENT = "student";
    public static final String P_FIVE = "five";
    public static final String P_FINISHED_YEAR = "finishedYear";

    private ListRepresent _representation;     // Приказ
    private Student _student;     // Студенты
    private Boolean _five;     // Диплом с отличием
    private Integer _finishedYear;     // Год окончания на момент проведения приказа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приказ. Свойство не может быть null.
     */
    @NotNull
    public ListRepresent getRepresentation()
    {
        return _representation;
    }

    /**
     * @param representation Приказ. Свойство не может быть null.
     */
    public void setRepresentation(ListRepresent representation)
    {
        dirty(_representation, representation);
        _representation = representation;
    }

    /**
     * @return Студенты. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студенты. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Диплом с отличием.
     */
    public Boolean getFive()
    {
        return _five;
    }

    /**
     * @param five Диплом с отличием.
     */
    public void setFive(Boolean five)
    {
        dirty(_five, five);
        _five = five;
    }

    /**
     * @return Год окончания на момент проведения приказа.
     */
    public Integer getFinishedYear()
    {
        return _finishedYear;
    }

    /**
     * @param finishedYear Год окончания на момент проведения приказа.
     */
    public void setFinishedYear(Integer finishedYear)
    {
        dirty(_finishedYear, finishedYear);
        _finishedYear = finishedYear;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RelListRepresentStudentsGen)
        {
            setRepresentation(((RelListRepresentStudents)another).getRepresentation());
            setStudent(((RelListRepresentStudents)another).getStudent());
            setFive(((RelListRepresentStudents)another).getFive());
            setFinishedYear(((RelListRepresentStudents)another).getFinishedYear());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RelListRepresentStudentsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RelListRepresentStudents.class;
        }

        public T newInstance()
        {
            return (T) new RelListRepresentStudents();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "representation":
                    return obj.getRepresentation();
                case "student":
                    return obj.getStudent();
                case "five":
                    return obj.getFive();
                case "finishedYear":
                    return obj.getFinishedYear();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "representation":
                    obj.setRepresentation((ListRepresent) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "five":
                    obj.setFive((Boolean) value);
                    return;
                case "finishedYear":
                    obj.setFinishedYear((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "representation":
                        return true;
                case "student":
                        return true;
                case "five":
                        return true;
                case "finishedYear":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "representation":
                    return true;
                case "student":
                    return true;
                case "five":
                    return true;
                case "finishedYear":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "representation":
                    return ListRepresent.class;
                case "student":
                    return Student.class;
                case "five":
                    return Boolean.class;
                case "finishedYear":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RelListRepresentStudents> _dslPath = new Path<RelListRepresentStudents>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RelListRepresentStudents");
    }
            

    /**
     * @return Приказ. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents#getRepresentation()
     */
    public static ListRepresent.Path<ListRepresent> representation()
    {
        return _dslPath.representation();
    }

    /**
     * @return Студенты. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Диплом с отличием.
     * @see ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents#getFive()
     */
    public static PropertyPath<Boolean> five()
    {
        return _dslPath.five();
    }

    /**
     * @return Год окончания на момент проведения приказа.
     * @see ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents#getFinishedYear()
     */
    public static PropertyPath<Integer> finishedYear()
    {
        return _dslPath.finishedYear();
    }

    public static class Path<E extends RelListRepresentStudents> extends EntityPath<E>
    {
        private ListRepresent.Path<ListRepresent> _representation;
        private Student.Path<Student> _student;
        private PropertyPath<Boolean> _five;
        private PropertyPath<Integer> _finishedYear;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приказ. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents#getRepresentation()
     */
        public ListRepresent.Path<ListRepresent> representation()
        {
            if(_representation == null )
                _representation = new ListRepresent.Path<ListRepresent>(L_REPRESENTATION, this);
            return _representation;
        }

    /**
     * @return Студенты. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Диплом с отличием.
     * @see ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents#getFive()
     */
        public PropertyPath<Boolean> five()
        {
            if(_five == null )
                _five = new PropertyPath<Boolean>(RelListRepresentStudentsGen.P_FIVE, this);
            return _five;
        }

    /**
     * @return Год окончания на момент проведения приказа.
     * @see ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents#getFinishedYear()
     */
        public PropertyPath<Integer> finishedYear()
        {
            if(_finishedYear == null )
                _finishedYear = new PropertyPath<Integer>(RelListRepresentStudentsGen.P_FINISHED_YEAR, this);
            return _finishedYear;
        }

        public Class getEntityClass()
        {
            return RelListRepresentStudents.class;
        }

        public String getEntityName()
        {
            return "relListRepresentStudents";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
