package ru.tandemservice.movestudentrmc.util;

import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Date;

public class RepresentationUtil {

    /**
     * "Дата с" для индивидуального представления
     *
     * @param representation
     *
     * @return
     */
    public static Date getStartDateRepresentation(Representation representation) {
        Date startDate = null;
        // Индивидуальное представление "О допуске к посещению занятий"
        if (representation instanceof RepresentAdmissionAttendClasses) {
        }
        // Индивидуальное представление "О смене имени"
        if (representation instanceof RepresentChangeName) {
        }
        // Индивидуальное представление "О переводе на след курс"
        if (representation instanceof RepresentCourseTransfer) {
            startDate = representation.getStartDate();
        }
        // Индивидуальное представление "О выдаче диплома и отчислении"
        if (representation instanceof RepresentDiplomAndExclude) {
            startDate = representation.getStartDate();
        }
        // Индивидуальное представление "О зачислении в порядке перевода"
        if (representation instanceof RepresentEnrollmentTransfer) {
            startDate = representation.getStartDate();
        }
        // Индивидуальное представление "Об отчислении"
        if (representation instanceof RepresentExclude) {
            startDate = representation.getStartDate();
        }
        // Индивидуальное представление "O восстановлении"
        if (representation instanceof RepresentExcludeOut) {
            startDate = representation.getStartDate();
        }
        // Индивидуальное представление "О продлении сессии"
        if (representation instanceof RepresentExtensionSession) {
        }
        // Индивидуальное представление "Oб отмене и назначении стипендии/выплаты"
        if (representation instanceof RepresentGrantCancelAndDestination) {
            startDate = ((RepresentGrantCancelAndDestination) representation).getBeginDate();
        }
        // Индивидуальное представление "Oб отмене стипендии/выплаты"
        if (representation instanceof RepresentGrantCancel) {
            startDate = ((RepresentGrantCancel) representation).getDateCancelPayment();
        }
        // Индивидуальное представление "O назначении стипендии/выплаты"
        if (representation instanceof RepresentGrant) {
            startDate = ((RepresentGrant) representation).getBeginDate();
        }
        // Индивидуальное представление "O возобновлении стипендии/выплаты"
        if (representation instanceof RepresentGrantResume) {
            startDate = ((RepresentGrantResume) representation).getDateResumePayment();
        }
        // Индивидуальное представление "O приостановлении стипендии/выплаты"
        if (representation instanceof RepresentGrantSuspend) {
            startDate = ((RepresentGrantSuspend) representation).getDateSuspendPayment();
        }
        // Об объявлении выговора
        if (representation instanceof RepresentMakeReprimand) {
        }
        // О предоставлении индивидуального УП
        if (representation instanceof RepresentPersonWorkPlan) {
            startDate = ((RepresentPersonWorkPlan) representation).getBeginPersonWorkPlanDate();
        }
        // О перезачете/ переаттестации учебных дисциплин
        if (representation instanceof RepresentRecertificationTraining) {
        }
        // Индивидуальное представление о выдаче дубликата зачетной книжки/студенческого билета
        if (representation instanceof RepresentStudentTicket) {
        }
        // Индивидуальное представление О назначении социальной поддержки в связи с тяжелым материальным положением
        if (representation instanceof RepresentSupport) {
            startDate = ((RepresentSupport) representation).getBeginDate();
        }
        // Представление о переводе
        if (representation instanceof RepresentTransfer) {
            startDate = representation.getStartDate();
        }
        // О направлении в поездку
        if (representation instanceof RepresentTravel) {
            startDate = ((RepresentTravel) representation).getBeginDate();
        }
        // Представление "О предоставлении академического отпуска"
        if (representation instanceof RepresentWeekend) {
            startDate = representation.getStartDate();
        }
        // Представление О выходе из академического отпуска
        if (representation instanceof RepresentWeekendOut) {
            startDate = representation.getStartDate();
        }
        // Представление "О продлении академического отпуска"
        if (representation instanceof RepresentWeekendProlong) {
        }
        return startDate;
    }

    /**
     * "Дата по" для индивидуального представления
     *
     * @param representation
     *
     * @return
     */
    public static Date getEndDateRepresentation(Representation representation) {
        Date endDate = null;
        // О допуске к посещению занятий
        if (representation instanceof RepresentAdmissionAttendClasses) {
            endDate = ((RepresentAdmissionAttendClasses) representation).getExamsDate();
        }
        // Представление О смене имени
        if (representation instanceof RepresentChangeName) {
        }
        // Представление о переводе на след курс
        if (representation instanceof RepresentCourseTransfer) {
        }
        // Представление О выдаче диплома и отчислении
        if (representation instanceof RepresentDiplomAndExclude) {
        }
        // Представление О зачислении в порядке перевода
        if (representation instanceof RepresentEnrollmentTransfer) {
            endDate = ((RepresentEnrollmentTransfer) representation).getEndDate();
        }
        // Представление "Об отчислении"
        if (representation instanceof RepresentExclude) {
        }
        // Представление о восстановлении
        if (representation instanceof RepresentExcludeOut) {
        }
        // О продлении сессии
        if (representation instanceof RepresentExtensionSession) {
            endDate = ((RepresentExtensionSession) representation).getExtensionDate();
        }
        // Индивидуальное представление об отмене и назначении стипендии/выплаты
        if (representation instanceof RepresentGrantCancelAndDestination) {
            endDate = ((RepresentGrantCancelAndDestination) representation).getEndDate();
        }
        // Индивидуальное представление об отмене стипендии/выплаты
        if (representation instanceof RepresentGrantCancel) {
        }
        // Индивидуальное представление о назначении стипендии/выплаты
        if (representation instanceof RepresentGrant) {
            endDate = ((RepresentGrant) representation).getEndDate();
        }
        // Индивидуальное представление о возобновлении стипендии/выплаты
        if (representation instanceof RepresentGrantResume) {
        }
        // Индивидуальное представление о приостановлении стипендии/выплаты
        if (representation instanceof RepresentGrantSuspend) {
        }
        // Об объявлении выговора
        if (representation instanceof RepresentMakeReprimand) {
        }
        // О предоставлении ИУП
        if (representation instanceof RepresentPersonWorkPlan) {
            endDate = ((RepresentPersonWorkPlan) representation).getEndPersonWorkPlanDate();
        }
        // О перезачете/ переаттестации учебных дисциплин
        if (representation instanceof RepresentRecertificationTraining) {
        }
        // Индивидуальное представление о выдаче дубликата зачетной книжки/студенческого билета
        if (representation instanceof RepresentStudentTicket) {
        }
        // Индивидуальное представление О назначении социальной поддержки в связи с тяжелым материальным положением
        if (representation instanceof RepresentSupport) {
        }
        // Представление о переводе
        if (representation instanceof RepresentTransfer) {
        }
        // О направлении в поездку
        if (representation instanceof RepresentTravel) {
            endDate = ((RepresentTravel) representation).getEndDate();
        }
        // Представление "О предоставлении академического отпуска"
        if (representation instanceof RepresentWeekend) {
            endDate = ((RepresentWeekend) representation).getEndDate();
        }
        // Представление О выходе из академического отпуска
        if (representation instanceof RepresentWeekendOut) {
        }
        // Представление "О продлении академического отпуска"
        if (representation instanceof RepresentWeekendProlong) {
            endDate = representation.getStartDate();
        }
        return endDate;
    }

    /**
     * "Дата с" для списочного представления
     *
     * @param listRepresent
     *
     * @return
     */
    public static Date getStartDateListRepresentation(ListRepresent listRepresent) {
        Date startDate = null;
        // Списочное представление о допуске к практике
        if (listRepresent instanceof ListRepresentAdmissionToPractice) {
            startDate = ((ListRepresentAdmissionToPractice) listRepresent).getDateBeginningPractice();
        }
        // Списочное представление О переводе на места финансируемые из средств федерального бюджета
        if (listRepresent instanceof ListRepresentBudgetTransfer) {
            startDate = ((ListRepresentBudgetTransfer) listRepresent).getDateBeginingTransfer();
        }
        // Списочное представление О переводе на следующий курс
        if (listRepresent instanceof ListRepresentCourseTransfer) {
            startDate = ((ListRepresentCourseTransfer) listRepresent).getDateBeginingTransfer();
        }
        // Списочное представление О выдаче диплома и отчислении
        if (listRepresent instanceof ListRepresentDiplomAndExclude) {
            startDate = ((ListRepresentDiplomAndExclude) listRepresent).getDateExclude();
        }
        // Списочное представление об отчислении
        if (listRepresent instanceof ListRepresentExclude) {
            startDate = ((ListRepresentExclude) listRepresent).getDateExclude();
        }
        // Списочное представление об отмене и назначении стипендии/выплаты
        if (listRepresent instanceof ListRepresentGrantCancelAndDestination) {
            startDate = ((ListRepresentGrantCancelAndDestination) listRepresent).getBeginGrantDate();
        }
        // Списочное представление об отмене стипендии/выплаты
        if (listRepresent instanceof ListRepresentGrantCancel) {
            startDate = ((ListRepresentGrantCancel) listRepresent).getDateCancelPayment();
        }
        // Списочное представление о назначении стиендии
        if (listRepresent instanceof ListRepresentGrant) {
            startDate = ((ListRepresentGrant) listRepresent).getDateBeginingPayment();
        }
        // Списочное представление о возобновлении стипендии/выплаты
        if (listRepresent instanceof ListRepresentGrantResume) {
            startDate = ((ListRepresentGrantResume) listRepresent).getDateResumePayment();
        }
        // Списочное представление о приостановке стипендии/выплаты
        if (listRepresent instanceof ListRepresentGrantSuspend) {
            startDate = ((ListRepresentGrantSuspend) listRepresent).getDateSuspendPayment();
        }
        // Списочное представление о направлении на практику
        if (listRepresent instanceof ListRepresentPractice) {
            startDate = ((ListRepresentPractice) listRepresent).getDateBeginningPractice();
        }
        // Списочное представление О распределении обучаемых по профилям и группам
        if (listRepresent instanceof ListRepresentProfileTransfer) {
            startDate = ((ListRepresentProfileTransfer) listRepresent).getDateBeginingTransfer();
        }
        // Представление О назначении государственной социальной стипендии
        if (listRepresent instanceof ListRepresentSocialGrant) {
            startDate = ((ListRepresentSocialGrant) listRepresent).getDateBeginingPayment();
        }
        // Представление О назначении социальной  поддержки в связи с тяжелым материальным положением
        if (listRepresent instanceof ListRepresentSupport) {
        }
        // Списочное представление о переводе
        if (listRepresent instanceof ListRepresentTransfer) {
            startDate = ((ListRepresentTransfer) listRepresent).getDateTransfer();
        }
        // Списочное представление о направлении в поездку
        if (listRepresent instanceof ListRepresentTravel) {
            startDate = ((ListRepresentTravel) listRepresent).getBeginDate();
        }
        // Списочное представление о предоставлении каникул
        if (listRepresent instanceof ListRepresentWeekend) {
            startDate = ((ListRepresentWeekend) listRepresent).getDateBeginningWeekend();
        }

        return startDate;
    }

    /**
     * "Дата по" для списочного представления
     *
     * @param representation
     *
     * @return
     */
    public static Date getEndDateListRepresentation(ListRepresent listRepresent) {
        Date endDate = null;
        // Списочное представление о допуске к практике
        if (listRepresent instanceof ListRepresentAdmissionToPractice) {
            endDate = ((ListRepresentAdmissionToPractice) listRepresent).getDateEndOfPractice();
        }
        // Списочное представление О переводе на места финансируемые из средств федерального бюджета
        if (listRepresent instanceof ListRepresentBudgetTransfer) {
        }
        // Списочное представление О переводе на следующий курс
        if (listRepresent instanceof ListRepresentCourseTransfer) {
        }
        // Списочное представление О выдаче диплома и отчислении
        if (listRepresent instanceof ListRepresentDiplomAndExclude) {
        }
        // Списочное представление об отчислении
        if (listRepresent instanceof ListRepresentExclude) {
        }
        // Списочное представление об отмене и назначении стипендии/выплаты
        if (listRepresent instanceof ListRepresentGrantCancelAndDestination) {
            endDate = ((ListRepresentGrantCancelAndDestination) listRepresent).getEndGrantDate();
        }
        // Списочное представление об отмене стипендии/выплаты
        if (listRepresent instanceof ListRepresentGrantCancel) {
        }
        // Списочное представление о назначении стиендии
        if (listRepresent instanceof ListRepresentGrant) {
            endDate = ((ListRepresentGrant) listRepresent).getDateEndOfPayment();
        }
        // Списочное представление о возобновлении стипендии/выплаты
        if (listRepresent instanceof ListRepresentGrantResume) {
        }
        // Списочное представление о приостановке стипендии/выплаты
        if (listRepresent instanceof ListRepresentGrantSuspend) {
        }
        // Списочное представление о направлении на практику
        if (listRepresent instanceof ListRepresentPractice) {
            endDate = ((ListRepresentPractice) listRepresent).getDateEndOfPractice();
        }
        // Списочное представление О распределении обучаемых по профилям и группам
        if (listRepresent instanceof ListRepresentProfileTransfer) {
        }
        // Представление О назначении государственной социальной стипендии
        if (listRepresent instanceof ListRepresentSocialGrant) {
            endDate = ((ListRepresentSocialGrant) listRepresent).getDateEndOfPayment();
        }
        // Представление О назначении социальной  поддержки в связи с тяжелым материальным положением
        if (listRepresent instanceof ListRepresentSupport) {
        }
        // Списочное представление о переводе
        if (listRepresent instanceof ListRepresentTransfer) {
        }
        // Списочное представление о направлении в поездку
        if (listRepresent instanceof ListRepresentTravel) {
            endDate = ((ListRepresentTravel) listRepresent).getEndDate();
        }
        // Списочное представление о предоставлении каникул
        if (listRepresent instanceof ListRepresentWeekend) {
            endDate = ((ListRepresentWeekend) listRepresent).getDateEndOfWeekend();
        }

        return endDate;
    }


    public static StudentVKRTheme getStudentVkrThemeLong(Student student) {
        StudentVKRTheme theme = IUniBaseDao.instance.get().get(StudentVKRTheme.class, StudentVKRTheme.student(), student);
        if (theme == null) {
            theme = new StudentVKRTheme();
            theme.setStudent(student);
            IUniBaseDao.instance.get().save(theme);
        }

        return theme;
    }
}
