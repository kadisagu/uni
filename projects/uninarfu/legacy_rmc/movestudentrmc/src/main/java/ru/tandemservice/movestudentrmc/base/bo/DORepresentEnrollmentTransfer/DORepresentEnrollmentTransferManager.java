package ru.tandemservice.movestudentrmc.base.bo.DORepresentEnrollmentTransfer;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentEnrollmentTransfer.logic.DORepresentEnrollmentTransferDAO;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentEnrollmentTransfer.ui.Edit.DORepresentEnrollmentTransferEdit;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentEnrollmentTransfer.ui.View.DORepresentEnrollmentTransferView;
import ru.tandemservice.movestudentrmc.docord.IDOObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IDOObjectModifyDAO;

@Configuration
public class DORepresentEnrollmentTransferManager extends BusinessObjectManager implements IDOObjectByRepresentTypeManager {

    public static DORepresentEnrollmentTransferManager instance()
    {
        return instance(DORepresentEnrollmentTransferManager.class);
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return DORepresentEnrollmentTransferEdit.class;
    }

    @Override
    @Bean
    public IDOObjectModifyDAO getDOObjectModifyDAO() {
        return new DORepresentEnrollmentTransferDAO();
    }

    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return DORepresentEnrollmentTransferView.class;
    }
}
