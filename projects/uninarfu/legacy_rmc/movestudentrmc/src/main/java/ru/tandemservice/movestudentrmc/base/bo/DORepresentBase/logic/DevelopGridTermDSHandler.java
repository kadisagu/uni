package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;

/**
 * По developGrid и course отдает список DevelopGridTerm
 */
public class DevelopGridTermDSHandler extends DefaultComboDataSourceHandler {
    public static final String DEVELOP_GRID = "developGrid";
    public static final String COURSE = "course";

    public DevelopGridTermDSHandler(String ownerId) {
        super(ownerId, DevelopGridTerm.class, DevelopGridTerm.term().intValue());
        setOrderByProperty(DevelopGridTerm.term().intValue().s());
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep) {
        super.prepareConditions(ep);

        DevelopGrid developGrid = ep.context.get(DEVELOP_GRID);
        Course course = ep.context.get(COURSE);

        ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property(DevelopGridTerm.developGrid().fromAlias("e")), DQLExpressions.value(developGrid)));
        ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property(DevelopGridTerm.course().fromAlias("e")), DQLExpressions.value(course)));
    }


    public static class Formatter implements IFormatter<DevelopGridTerm> {
        @Override
        public String format(DevelopGridTerm term) {
            return "№" + term.getTerm().getTitle() + ", " + term.getPart().getTitle() + ", " + term.getCourse().getTitle() + " курс";
        }

    }
}
