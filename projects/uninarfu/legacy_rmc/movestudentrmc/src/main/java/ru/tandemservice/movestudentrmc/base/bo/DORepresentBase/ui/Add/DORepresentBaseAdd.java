package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.TypeRepresentationDSHandler;

@Configuration
public class DORepresentBaseAdd extends BusinessComponentManager {

    public static final String TYPE_REPRESENTATION_DS = "typeRepresentationDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(TYPE_REPRESENTATION_DS, typeRepresentationDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler typeRepresentationDSHandler()
    {
        return (IDefaultComboDataSourceHandler) new TypeRepresentationDSHandler(getName());
    }
}
