package ru.tandemservice.movestudentrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Категория приказа"
 * Имя сущности : orderCategory
 * Файл data.xml : movementstudent.data.xml
 */
public interface OrderCategoryCodes
{
    /** Константа кода (code) элемента : movementstudent (code). Название (title) : Движение контингента студентов */
    String MOVEMENTSTUDENT = "movementstudent";
    /** Константа кода (code) элемента : grants (code). Название (title) : Стипендии/Выплаты */
    String GRANTS = "grants";

    Set<String> CODES = ImmutableSet.of(MOVEMENTSTUDENT, GRANTS);
}
