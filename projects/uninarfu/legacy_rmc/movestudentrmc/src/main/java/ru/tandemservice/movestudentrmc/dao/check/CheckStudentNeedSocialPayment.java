package ru.tandemservice.movestudentrmc.dao.check;

import ru.tandemservice.movestudentrmc.entity.PersonNARFU;
import ru.tandemservice.uni.entity.employee.Student;

public class CheckStudentNeedSocialPayment extends AbstractCheckStudentBenefit {

    @Override
    protected boolean checkCondition(Student student) {
        Long personId = student.getPerson().getId();

        if (!personNarfuMap.containsKey(personId))
            return false;

        PersonNARFU personNARFU = personNarfuMap.get(personId);

        return personNARFU.isNeedSocialPayment();
    }

    @Override
    protected String createError(Student student, boolean value) {
        StringBuilder sb = new StringBuilder()
                .append("Студент ")
                .append(student.getPerson().getFullFio())
                .append(" ")
                .append(value ? "не " : "")
                .append("имеет льготу «представитель коренных малочисленных народов Крайнего Севера»");
        return sb.toString();
    }

}
