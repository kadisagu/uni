package ru.tandemservice.movestudentrmc.entity;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.movestudentrmc.entity.gen.ListRepresentGrantCancelAndDestinationGen;

/**
 * Об отмене и назначении стипендии/выплаты
 */
public class ListRepresentGrantCancelAndDestination extends ListRepresentGrantCancelAndDestinationGen
{
    @Override
    public String getTitle() {
        return new StringBuilder()
                .append(getRepresentationType().getTitle())
                .append(" (")
                .append(getGrantView().getShortTitle())
                .append(") с ")
                .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getBeginGrantDate()))
                .append(getEndGrantDate() != null ? " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getEndGrantDate()) : "")
                .append(", от ")
                .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getCreateDate()))
                .toString();
    }

    @Override
    public String getRepresentationTitle() {
        return new StringBuilder()
                .append(getRepresentationType().getTitle())
                .append(" (")
                .append(getGrantView().getShortTitle())
                .append(") с ")
                .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getBeginGrantDate()))
                .append(getEndGrantDate() != null ? " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getEndGrantDate()) : "")
                .toString()
                ;
    }
}