package ru.tandemservice.movestudentrmc.base.bo.ListRepresentSocialGrant;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentSocialGrant.logic.ListRepresentSocialGrantManagerModifyDAO;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentSocialGrant.ui.Edit.ListRepresentSocialGrantEdit;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentSocialGrant.ui.View.ListRepresentSocialGrantView;
import ru.tandemservice.movestudentrmc.docord.IListObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IListObjectModifyDAO;


@Configuration
public class ListRepresentSocialGrantManager extends BusinessObjectManager implements IListObjectByRepresentTypeManager {

    public static ListRepresentSocialGrantManager instance() {
        return instance(ListRepresentSocialGrantManager.class);
    }


    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return ListRepresentSocialGrantEdit.class;
    }


    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return ListRepresentSocialGrantView.class;
    }


    @Bean
    @Override
    public IListObjectModifyDAO getListObjectModifyDAO() {
        return new ListRepresentSocialGrantManagerModifyDAO();
    }
}
