package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.IAbstractRepresentation;
import ru.tandemservice.movestudentrmc.entity.RelRepresentGrants;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntity;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность-связь 'Представление'-'Стипендия назначенная студенту'
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RelRepresentGrantsGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.RelRepresentGrants";
    public static final String ENTITY_NAME = "relRepresentGrants";
    public static final int VERSION_HASH = -1327028558;
    private static IEntityMeta ENTITY_META;

    public static final String L_REPRESENTATION = "representation";
    public static final String L_STUDENT_GRANT_ENTITY = "studentGrantEntity";

    private IAbstractRepresentation _representation;     // Представление
    private StudentGrantEntity _studentGrantEntity;     // Стипендия назначенная студенту

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Представление. Свойство не может быть null.
     */
    @NotNull
    public IAbstractRepresentation getRepresentation()
    {
        return _representation;
    }

    /**
     * @param representation Представление. Свойство не может быть null.
     */
    public void setRepresentation(IAbstractRepresentation representation)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && representation!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IAbstractRepresentation.class);
            IEntityMeta actual =  representation instanceof IEntity ? EntityRuntime.getMeta((IEntity) representation) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_representation, representation);
        _representation = representation;
    }

    /**
     * @return Стипендия назначенная студенту.
     */
    public StudentGrantEntity getStudentGrantEntity()
    {
        return _studentGrantEntity;
    }

    /**
     * @param studentGrantEntity Стипендия назначенная студенту.
     */
    public void setStudentGrantEntity(StudentGrantEntity studentGrantEntity)
    {
        dirty(_studentGrantEntity, studentGrantEntity);
        _studentGrantEntity = studentGrantEntity;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RelRepresentGrantsGen)
        {
            setRepresentation(((RelRepresentGrants)another).getRepresentation());
            setStudentGrantEntity(((RelRepresentGrants)another).getStudentGrantEntity());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RelRepresentGrantsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RelRepresentGrants.class;
        }

        public T newInstance()
        {
            return (T) new RelRepresentGrants();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "representation":
                    return obj.getRepresentation();
                case "studentGrantEntity":
                    return obj.getStudentGrantEntity();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "representation":
                    obj.setRepresentation((IAbstractRepresentation) value);
                    return;
                case "studentGrantEntity":
                    obj.setStudentGrantEntity((StudentGrantEntity) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "representation":
                        return true;
                case "studentGrantEntity":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "representation":
                    return true;
                case "studentGrantEntity":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "representation":
                    return IAbstractRepresentation.class;
                case "studentGrantEntity":
                    return StudentGrantEntity.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RelRepresentGrants> _dslPath = new Path<RelRepresentGrants>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RelRepresentGrants");
    }
            

    /**
     * @return Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelRepresentGrants#getRepresentation()
     */
    public static IAbstractRepresentationGen.Path<IAbstractRepresentation> representation()
    {
        return _dslPath.representation();
    }

    /**
     * @return Стипендия назначенная студенту.
     * @see ru.tandemservice.movestudentrmc.entity.RelRepresentGrants#getStudentGrantEntity()
     */
    public static StudentGrantEntity.Path<StudentGrantEntity> studentGrantEntity()
    {
        return _dslPath.studentGrantEntity();
    }

    public static class Path<E extends RelRepresentGrants> extends EntityPath<E>
    {
        private IAbstractRepresentationGen.Path<IAbstractRepresentation> _representation;
        private StudentGrantEntity.Path<StudentGrantEntity> _studentGrantEntity;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RelRepresentGrants#getRepresentation()
     */
        public IAbstractRepresentationGen.Path<IAbstractRepresentation> representation()
        {
            if(_representation == null )
                _representation = new IAbstractRepresentationGen.Path<IAbstractRepresentation>(L_REPRESENTATION, this);
            return _representation;
        }

    /**
     * @return Стипендия назначенная студенту.
     * @see ru.tandemservice.movestudentrmc.entity.RelRepresentGrants#getStudentGrantEntity()
     */
        public StudentGrantEntity.Path<StudentGrantEntity> studentGrantEntity()
        {
            if(_studentGrantEntity == null )
                _studentGrantEntity = new StudentGrantEntity.Path<StudentGrantEntity>(L_STUDENT_GRANT_ENTITY, this);
            return _studentGrantEntity;
        }

        public Class getEntityClass()
        {
            return RelRepresentGrants.class;
        }

        public String getEntityName()
        {
            return "relRepresentGrants";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
