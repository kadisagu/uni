package ru.tandemservice.movestudentrmc.dao.check;

import ru.tandemservice.uni.entity.employee.Student;

public class CheckCompensationTypeBudget extends AbstractCheckDAO {

    @Override
    protected boolean getCondition(Student student) {
        return student.getCompensationType().isBudget();
    }

    @Override
    protected String getErrorMessage(Student student, boolean value) {
        return new StringBuilder()
                .append("Студент ")
                .append(student.getPerson().getFullFio())
                .append(" ")
                .append(value ? "не " : "")
                .append("обучается за счет средств федерального бюджета").toString();
    }
}
