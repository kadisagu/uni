package ru.tandemservice.movestudentrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип стипендии/выплаты"
 * Имя сущности : grantType
 * Файл data.xml : movementstudent.data.xml
 */
public interface GrantTypeCodes
{
    /** Константа кода (code) элемента : 1 (code). Название (title) : Академические и приравненные */
    String CODE_1 = "1";
    /** Константа кода (code) элемента : 2 (code). Название (title) : Повышенная 1 */
    String CODE_2 = "2";
    /** Константа кода (code) элемента : 3 (code). Название (title) : Индивидуальные академические */
    String CODE_3 = "3";
    /** Константа кода (code) элемента : 4 (code). Название (title) : Социальные и приравненные */
    String CODE_4 = "4";
    /** Константа кода (code) элемента : 5 (code). Название (title) : а/о и беременность */
    String CODE_5 = "5";
    /** Константа кода (code) элемента : 6 (code). Название (title) : Социальная выплата */
    String CODE_6 = "6";

    Set<String> CODES = ImmutableSet.of(CODE_1, CODE_2, CODE_3, CODE_4, CODE_5, CODE_6);
}
