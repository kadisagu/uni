package ru.tandemservice.movestudentrmc.base.bo.DOOrder.ui.Edit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.logic.*;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;

//import ru.tandemservice.movestudentrmc.entity.DocumentRepresentationBase;

@Configuration
public class DOOrderEdit extends BusinessComponentManager {

    public static final String REPRESENT_DS = "representDS";
    public static final String REPRESENT_SELECTED_DS = "representSelectedDS";
    public static final String DEVELOP_FORM_DS = "developFormDS";
    public static final String FORMATIVE_ORG_UNIT_DS = "formativeOrgUnitDS";
    public static final String TYPE_REPRESENT_DS = "typeRepresentDS";

    @Bean
    public ColumnListExtPoint representDS()
    {
        return columnListExtPointBuilder(REPRESENT_DS)
                .addColumn(checkboxColumn(RepresentDSHandler.CHECKBOX_COLUMN).create())
                .addColumn(textColumn(RepresentDSHandler.DEVELOP_FORM_COLUMN, "student." + Student.educationOrgUnit().developForm().title()).order().create())
                .addColumn(textColumn(RepresentDSHandler.FORMATIVE_ORG_UNIT_COLUMN, "student." + Student.educationOrgUnit().formativeOrgUnit().title()).order().create())
                .addColumn(textColumn(RepresentDSHandler.TERRITORIAL_ORG_UNIT_COLUMN, "student." + Student.educationOrgUnit().territorialOrgUnit().fullTitle()).order().create())
                .addColumn(textColumn(RepresentDSHandler.EDUCATION_LEVEL_HIGH_SCHOOL_COLUMN, "student." + Student.educationOrgUnit().educationLevelHighSchool().orgUnit().fullTitle()).order().create())
                .addColumn(textColumn(RepresentDSHandler.EDUCATION_ORG_UNIT_COLUMN, "student." + Student.educationOrgUnit().title()).order().create())
                .addColumn(textColumn(RepresentDSHandler.TYPE_REPRESENT_COLUMN, "represent." + Representation.type().title()).order().create())
                .addColumn(dateColumn(RepresentDSHandler.DATE_REPRESENT_COLUMN, "represent." + Representation.startDate()).order().create())
                .addColumn(textColumn(RepresentDSHandler.COURSE_COLUMN, "student." + Student.course().title()).order().create())
                .addColumn(textColumn(RepresentDSHandler.GROUP_COLUMN, "student." + Student.group().title()).order().create())
                .addColumn(textColumn(RepresentDSHandler.BUDGET_COLUMN, "student." + Student.compensationType().shortTitle()).order().create())
                .addColumn(publisherColumn(RepresentDSHandler.STUDENT_FIO_COLUMN, "student." + Student.person().fullFio()).publisherLinkResolver(new IPublisherLinkResolver() {
                    @Override
                    public Object getParameters(IEntity entity) {
                        Representation represent = ((DataWrapper) entity).getWrapped();
                        DocRepresentStudentBase docRepresentStudentBase = IUniBaseDao.instance.get().get(DocRepresentStudentBase.class, DocRepresentStudentBase.representation(), represent);
                        Student student = docRepresentStudentBase.getStudent();
                        return new UniMap().add(PublisherActivator.PUBLISHER_ID_KEY, student != null ? student.getId() : null).add("selectedStudentTab", "studentTab");
                    }

                    @Override
                    public String getComponentName(IEntity entity) {
                        return null;
                    }
                }).order().create())
                .create();
    }

    @Bean
    public ColumnListExtPoint representSelectedDS()
    {
        return columnListExtPointBuilder(REPRESENT_DS)
                .addColumn(checkboxColumn(RepresentSelectedDSHandler.CHECKBOX_COLUMN).create())
                .addColumn(textColumn(RepresentSelectedDSHandler.DEVELOP_FORM_COLUMN, "student." + Student.educationOrgUnit().developForm().title()).order().create())
                .addColumn(textColumn(RepresentSelectedDSHandler.FORMATIVE_ORG_UNIT_COLUMN, "student." + Student.educationOrgUnit().formativeOrgUnit().title()).order().create())
                .addColumn(textColumn(RepresentSelectedDSHandler.TERRITORIAL_ORG_UNIT_COLUMN, "student." + Student.educationOrgUnit().territorialOrgUnit().fullTitle()).order().create())
                .addColumn(textColumn(RepresentSelectedDSHandler.EDUCATION_LEVEL_HIGH_SCHOOL_COLUMN, "student." + Student.educationOrgUnit().educationLevelHighSchool().orgUnit().fullTitle()).order().create())
                .addColumn(textColumn(RepresentSelectedDSHandler.EDUCATION_ORG_UNIT_COLUMN, "student." + Student.educationOrgUnit().title()).order().create())
                .addColumn(textColumn(RepresentSelectedDSHandler.TYPE_REPRESENT_COLUMN, "represent." + Representation.type().title()).order().create())
                .addColumn(dateColumn(RepresentSelectedDSHandler.DATE_REPRESENT_COLUMN, "represent." + Representation.startDate()).order().create())
                .addColumn(textColumn(RepresentSelectedDSHandler.COURSE_COLUMN, "student." + Student.course().title()).order().create())
                .addColumn(textColumn(RepresentSelectedDSHandler.GROUP_COLUMN, "student." + Student.group().title()).order().create())
                .addColumn(textColumn(RepresentSelectedDSHandler.BUDGET_COLUMN, "student." + Student.compensationType().shortTitle()).order().create())
                .addColumn(textColumn(RepresentSelectedDSHandler.STUDENT_FIO_COLUMN, "student." + Student.person().identityCard().fullFio()).order().create())
                .create();
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(REPRESENT_DS, representDS()).handler(representDSHandler()))
                .addDataSource(searchListDS(REPRESENT_SELECTED_DS, representSelectedDS()).handler(representSelectedDSHandler()))
                .addDataSource(selectDS(DEVELOP_FORM_DS, developFormDSHandler()))
                .addDataSource(selectDS(FORMATIVE_ORG_UNIT_DS, formativeOrgUnitDSHandler()))
                .addDataSource(selectDS(TYPE_REPRESENT_DS, typeRepresentDSHandler()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> representDSHandler()
    {
        return new RepresentDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> representSelectedDSHandler()
    {
        return new RepresentSelectedDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> developFormDSHandler()
    {
        return new DevelopFormDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> formativeOrgUnitDSHandler()
    {
        return new FormativeOrgUnitDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> typeRepresentDSHandler()
    {
        return new TypeRepresentDSHandler(getName());
    }
}
