package ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantCancelAndDestination;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantCancelAndDestination.logic.ListRepresentGrantCancelAndDestinationManagerModifyDAO;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantCancelAndDestination.ui.Edit.ListRepresentGrantCancelAndDestinationEdit;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantCancelAndDestination.ui.View.ListRepresentGrantCancelAndDestinationView;
import ru.tandemservice.movestudentrmc.docord.IListObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IListObjectModifyDAO;

@Configuration
public class ListRepresentGrantCancelAndDestinationManager extends BusinessObjectManager implements IListObjectByRepresentTypeManager {

    public static ListRepresentGrantCancelAndDestinationManager instance() {
        return instance(ListRepresentGrantCancelAndDestinationManager.class);
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return ListRepresentGrantCancelAndDestinationEdit.class;
    }

    @Bean
    @Override
    public IListObjectModifyDAO getListObjectModifyDAO() {
        return new ListRepresentGrantCancelAndDestinationManagerModifyDAO();
    }

    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return ListRepresentGrantCancelAndDestinationView.class;
    }

}
