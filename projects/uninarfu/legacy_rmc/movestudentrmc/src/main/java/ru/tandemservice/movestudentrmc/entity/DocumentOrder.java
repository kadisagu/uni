package ru.tandemservice.movestudentrmc.entity;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.entity.catalog.MovestudentOrderStates;
import ru.tandemservice.movestudentrmc.entity.gen.DocumentOrderGen;
import ru.tandemservice.uni.dao.IUniBaseDao;

import java.util.List;


/**
 * САФУ Приказы
 */
public class DocumentOrder extends DocumentOrderGen
{
    public static final String P_REPRESENTATION_TYPES = "representationTypes";
    public static final String P_REPRESENTATION_COUNT = "representationCount";

    public void setState(ICatalogItem state) {

        setState((MovestudentOrderStates) state);
    }

    public ICatalogItem getType() {

        return null;
    }

    public String getTitle() {

        return getFullTitle();
    }

    @EntityDSLSupport(parts = {P_TITLE})
    public String getFullTitle() {

        String str;

        str = "Приказ №" + getNumber() + ", от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getCommitDate());

        return str;
    }

    public String getRepresentationTypes() {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(DocOrdRepresent.class, "doc")
                .where(DQLExpressions.eq(DQLExpressions.property(DocOrdRepresent.order().fromAlias("doc")), DQLExpressions.value(this)))
                .column(DQLExpressions.property(DocOrdRepresent.representation().type().title().fromAlias("doc")))
                .setPredicate(DQLPredicateType.distinct);

        List<String> result = IUniBaseDao.instance.get().getList(builder);
        return StringUtils.join(result, ", ");
    }

    public Integer getRepresentationCount() {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(DocOrdRepresent.class, "doc")
                .where(DQLExpressions.eq(DQLExpressions.property(DocOrdRepresent.order().fromAlias("doc")), DQLExpressions.value(this)));
        return IUniBaseDao.instance.get().getCount(builder);
    }
}