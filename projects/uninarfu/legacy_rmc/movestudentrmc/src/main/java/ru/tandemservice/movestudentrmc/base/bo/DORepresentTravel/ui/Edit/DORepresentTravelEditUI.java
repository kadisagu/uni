package ru.tandemservice.movestudentrmc.base.bo.DORepresentTravel.ui.Edit;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import org.tandemframework.tapsupport.component.selection.SingleSelectTextModel;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentEditUI;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentTravel.logic.DORepresentTravelDAO;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.ListRepresentTravel;
import ru.tandemservice.movestudentrmc.entity.NarfuDocument;
import ru.tandemservice.movestudentrmc.entity.RepresentTravel;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.util.ListenerWrapper;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.StudentStatus;

import java.util.Date;
import java.util.List;

public class DORepresentTravelEditUI extends AbstractDORepresentEditUI implements ListenerWrapper.IListener<NarfuDocument>
{
    private ISelectModel organizationSelectModel;

    @Override
    protected void initDocument() {
        super.initDocument();
        _studentStatusNewStr = ((StudentStatus) UniDaoFacade.getCoreDao().getCatalogItem(StudentStatus.class, "5")).getTitle();
    }

    @Override
    public void onComponentRefresh() {
        setOrganizationSelectModel(new SingleSelectTextModel() {
            @Override
            public ListResult findValues(String filter) {
            	
            	//-------------------------------------------------------------------
            	/*
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(RepresentTravel.class, "e")
                        .column(DQLExpressions.property(RepresentTravel.organization().fromAlias("e")))
                        .setPredicate(DQLPredicateType.distinct)
                        .where(DQLExpressions.likeUpper(DQLExpressions.property(RepresentTravel.organization().fromAlias("e")), DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));
                */

                DQLSelectBuilder fragment = new DQLSelectBuilder()
                        .fromEntity(ListRepresentTravel.class, "e")
                        .column(DQLExpressions.property(ListRepresentTravel.organization().fromAlias("e")))
                        .predicate(DQLPredicateType.distinct)
                        .where(DQLExpressions.likeUpper(DQLExpressions.property(ListRepresentTravel.organization().fromAlias("e")), DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));
                		

                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(RepresentTravel.class, "e")
                        .column(DQLExpressions.property(RepresentTravel.organization().fromAlias("e")), "org")
                        .predicate(DQLPredicateType.distinct)
                        .where(DQLExpressions.likeUpper(DQLExpressions.property(RepresentTravel.organization().fromAlias("e")), DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))))
                        .union(fragment.buildSelectRule())
                ;
           	//-------------------------------------------------------------------
                

                List list = builder.createStatement(getSupport().getSession()).setMaxResults(50).list();
                Number number = (Number) builder.createCountStatement(new DQLExecutionContext(getSupport().getSession())).uniqueResult();

                return new ListResult(list, number == null ? 0L : number.intValue());
            }
        });

        super.onComponentRefresh();
        RepresentTravel represent = (RepresentTravel) _representObj;
        if (represent.getAddressCountry() == null) {
            IUniBaseDao dao = IUniBaseDao.instance.get();
            AddressCountry result = dao.get(AddressCountry.class, AddressCountry.code(), DORepresentTravelDAO.RUSSIA_CODE);
            represent.setAddressCountry(result);
        }
        if (represent.getBeginDate() == null)
            represent.setBeginDate(new Date());
    }

    @Override
    protected void FetchStudents(Representation doc) {

        _studentDocList.clear();
        _studentList.clear();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocRepresentStudentBase.class, "stud").addColumn("stud");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(DocRepresentStudentBase.representation().fromAlias("stud")),
                DQLExpressions.value(doc.getId())));

        List<DocRepresentStudentBase> studentList = builder.createStatement(getSupport().getSession()).list();

        for (DocRepresentStudentBase student : studentList) {

            _studentDocList.add(student);
            _studentList.add(student.getStudent());
        }

        _student = _studentList.get(0);
    }

    public ISelectModel getOrganizationSelectModel() {
        return organizationSelectModel;
    }

    public void setOrganizationSelectModel(ISelectModel organizationSelectModel) {
        this.organizationSelectModel = organizationSelectModel;
    }
    
    //-----------------------------------------------------------------------------------------------------------------
    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
    	
    	super.onBeforeDataSourceFetch(dataSource);
    	
    	if(dataSource.getName().equals("settlementDS")) {
    		dataSource.put("country", ((RepresentTravel) getRepresentObj()).getAddressCountry());
    	}
    }
    
    public void onClickChangeDate() {
    	
    	RepresentTravel rep = (RepresentTravel) getRepresentObj();
        if (rep.getBeginDate() != null && rep.getEndDate() != null) {
            int days = (int) ((rep.getEndDate().getTime() - rep.getBeginDate().getTime()) / (1000 * 60 * 60 * 24)) + 1;
            rep.setDays(days);
        }
    }
    //-----------------------------------------------------------------------------------------------------------------
}
