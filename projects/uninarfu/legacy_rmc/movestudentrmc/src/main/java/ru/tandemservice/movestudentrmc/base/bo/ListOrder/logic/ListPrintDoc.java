package ru.tandemservice.movestudentrmc.base.bo.ListOrder.logic;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ListRepresentBaseManager;
import ru.tandemservice.movestudentrmc.docord.IListObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.ListOrdListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListOrder;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.*;
import java.util.Map.Entry;

public class ListPrintDoc {

    //Сборка печатной формы приказа
    public static RtfDocument creationListPrintDocOrder(ListOrder listOrder) {

        List<ListOrdListRepresent> listOrdListRepresents = DataAccessServices.dao().getList(ListOrdListRepresent.class, ListOrdListRepresent.order(), listOrder);

        HashMap<RepresentationType, List<ListRepresent>> representMap = new HashMap<>();

        //Сгруппируем представления по типам
        for (ListOrdListRepresent item : listOrdListRepresents) {
            RepresentationType key = item.getRepresentation().getRepresentationType();

            if (representMap.containsKey(key)) {
                representMap.get(key).add(item.getRepresentation());
            } else {
                List<ListRepresent> arr = new ArrayList<>();
                arr.add(item.getRepresentation());
                representMap.put(key, arr);
            }
        }

        RtfDocument template = UtilPrintSupport.getTemplate("listRepresent", "common");

        IRtfElement bodyTemp = UniRtfUtil.findElement(template.getElementList(), "body");

        Set<RepresentationType> types = new HashSet<>(representMap.keySet());

        for (Entry<RepresentationType, List<ListRepresent>> keyVal : representMap.entrySet()) {

            RtfDocument page = RtfBean.getElementFactory().createRtfDocument();

            //ListRepresent listRepresentBase = item.getRepresentation();
            List<ListRepresent> listOfRepresents = keyVal.getValue();
            IListObjectByRepresentTypeManager representManager = ListRepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(keyVal.getKey().getCode());

            representManager.getListObjectModifyDAO().buildBodyRepresent(listOfRepresents, page);

            if (types.size() > 1) {
                SharedRtfUtil.removeParagraphsWithTagsRecursive(page, Collections.singletonList("footer"), false, false);
                types.remove(keyVal.getKey());
                page.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            }

            template.getElementList().addAll(template.getElementList().indexOf(bodyTemp), page.getElementList());
        }

        //Добавим фуутер с подписями
        RtfDocument footerTemplate = UtilPrintSupport.getTemplate("listRepresentFooter", "common");

        if (footerTemplate != null) {
            IRtfElement footerTemp = UniRtfUtil.findElement(template.getElementList(), "footer");

            if (footerTemp != null)
                template.getElementList().addAll(template.getElementList().indexOf(footerTemp), footerTemplate.getElementList());
            else
                template.getElementList().addAll(footerTemplate.getElementList());
        }


        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("date_or", listOrder.getCommitDate() != null ? UtilPrintSupport.getDateFormatterWithMonthString(listOrder.getCommitDate()) : UtilPrintSupport.getDateFormatterWithMonthString(listOrder.getCreateDate()));
        injectModifier.put("num_or", listOrder.getNumber());
        injectModifier.modify(template);

        UtilPrintSupport.injectVisaTable(footerTemplate, listOrder);

        SharedRtfUtil.removeParagraphsWithTagsRecursive(template, Arrays.asList("body", "footer"), false, false);

        List<OrgUnit> orgUnitList = DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(RelListRepresentStudents.class, "rep")
                        .column(DQLExpressions.property("rep", RelListRepresentStudents.student().educationOrgUnit().formativeOrgUnit()))
                        .joinEntity("rep", DQLJoinType.inner, ListOrdListRepresent.class, "lor", DQLExpressions.eq(DQLExpressions.property("rep", RelListRepresentStudents.representation()), DQLExpressions.property("lor", ListOrdListRepresent.representation())))
                        .where(DQLExpressions.eq(DQLExpressions.property("lor", ListOrdListRepresent.order()), DQLExpressions.value(listOrder)))
        );

        UtilPrintSupport.injectBranchLabels(template, orgUnitList);

        return template;
    }
}
