package ru.tandemservice.movestudentrmc.base.bo.RelationSettings.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonBasic;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;

import java.util.List;

public interface IRelationSettingsModifyDAO extends INeedPersistenceSupport {

    void saveTypeRepresentation(RepresentationType typeRepresentation, List<FakeReasonsHandler> fakeReasonsList);

    void updateTypeRepresentation(RepresentationType typeRepresentation, List<FakeReasonsHandler> fakeReasonsList, List<RelRepresentationReasonBasic> reasonBasicList);

    void updateRelations(RepresentationType typeRepresentation, RepresentationReason reason, List<RelRepresentationReasonBasic> relList);
}
