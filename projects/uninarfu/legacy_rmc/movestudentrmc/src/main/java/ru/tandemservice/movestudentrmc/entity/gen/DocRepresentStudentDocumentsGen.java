package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentDocuments;
import ru.tandemservice.movestudentrmc.entity.NarfuDocument;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность-связь 'Представление'-'Документы Студента'
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DocRepresentStudentDocumentsGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.DocRepresentStudentDocuments";
    public static final String ENTITY_NAME = "docRepresentStudentDocuments";
    public static final int VERSION_HASH = -2025297853;
    private static IEntityMeta ENTITY_META;

    public static final String L_REPRESENTATION = "representation";
    public static final String L_REASON = "reason";
    public static final String L_STUDENT_DOCUMENTS = "studentDocuments";

    private Representation _representation;     // Представление
    private RepresentationReason _reason;     // Причина
    private NarfuDocument _studentDocuments;     // Сущность документ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Представление. Свойство не может быть null.
     */
    @NotNull
    public Representation getRepresentation()
    {
        return _representation;
    }

    /**
     * @param representation Представление. Свойство не может быть null.
     */
    public void setRepresentation(Representation representation)
    {
        dirty(_representation, representation);
        _representation = representation;
    }

    /**
     * @return Причина.
     */
    public RepresentationReason getReason()
    {
        return _reason;
    }

    /**
     * @param reason Причина.
     */
    public void setReason(RepresentationReason reason)
    {
        dirty(_reason, reason);
        _reason = reason;
    }

    /**
     * @return Сущность документ.
     */
    public NarfuDocument getStudentDocuments()
    {
        return _studentDocuments;
    }

    /**
     * @param studentDocuments Сущность документ.
     */
    public void setStudentDocuments(NarfuDocument studentDocuments)
    {
        dirty(_studentDocuments, studentDocuments);
        _studentDocuments = studentDocuments;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DocRepresentStudentDocumentsGen)
        {
            setRepresentation(((DocRepresentStudentDocuments)another).getRepresentation());
            setReason(((DocRepresentStudentDocuments)another).getReason());
            setStudentDocuments(((DocRepresentStudentDocuments)another).getStudentDocuments());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DocRepresentStudentDocumentsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DocRepresentStudentDocuments.class;
        }

        public T newInstance()
        {
            return (T) new DocRepresentStudentDocuments();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "representation":
                    return obj.getRepresentation();
                case "reason":
                    return obj.getReason();
                case "studentDocuments":
                    return obj.getStudentDocuments();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "representation":
                    obj.setRepresentation((Representation) value);
                    return;
                case "reason":
                    obj.setReason((RepresentationReason) value);
                    return;
                case "studentDocuments":
                    obj.setStudentDocuments((NarfuDocument) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "representation":
                        return true;
                case "reason":
                        return true;
                case "studentDocuments":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "representation":
                    return true;
                case "reason":
                    return true;
                case "studentDocuments":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "representation":
                    return Representation.class;
                case "reason":
                    return RepresentationReason.class;
                case "studentDocuments":
                    return NarfuDocument.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DocRepresentStudentDocuments> _dslPath = new Path<DocRepresentStudentDocuments>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DocRepresentStudentDocuments");
    }
            

    /**
     * @return Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentDocuments#getRepresentation()
     */
    public static Representation.Path<Representation> representation()
    {
        return _dslPath.representation();
    }

    /**
     * @return Причина.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentDocuments#getReason()
     */
    public static RepresentationReason.Path<RepresentationReason> reason()
    {
        return _dslPath.reason();
    }

    /**
     * @return Сущность документ.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentDocuments#getStudentDocuments()
     */
    public static NarfuDocument.Path<NarfuDocument> studentDocuments()
    {
        return _dslPath.studentDocuments();
    }

    public static class Path<E extends DocRepresentStudentDocuments> extends EntityPath<E>
    {
        private Representation.Path<Representation> _representation;
        private RepresentationReason.Path<RepresentationReason> _reason;
        private NarfuDocument.Path<NarfuDocument> _studentDocuments;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentDocuments#getRepresentation()
     */
        public Representation.Path<Representation> representation()
        {
            if(_representation == null )
                _representation = new Representation.Path<Representation>(L_REPRESENTATION, this);
            return _representation;
        }

    /**
     * @return Причина.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentDocuments#getReason()
     */
        public RepresentationReason.Path<RepresentationReason> reason()
        {
            if(_reason == null )
                _reason = new RepresentationReason.Path<RepresentationReason>(L_REASON, this);
            return _reason;
        }

    /**
     * @return Сущность документ.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentDocuments#getStudentDocuments()
     */
        public NarfuDocument.Path<NarfuDocument> studentDocuments()
        {
            if(_studentDocuments == null )
                _studentDocuments = new NarfuDocument.Path<NarfuDocument>(L_STUDENT_DOCUMENTS, this);
            return _studentDocuments;
        }

        public Class getEntityClass()
        {
            return DocRepresentStudentDocuments.class;
        }

        public String getEntityName()
        {
            return "docRepresentStudentDocuments";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
