package ru.tandemservice.movestudentrmc.entity.catalog;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import ru.tandemservice.movestudentrmc.entity.catalog.gen.PrintTemplateGen;

/**
 * Печатные шаблоны
 */
public class PrintTemplate extends PrintTemplateGen implements ICatalogItem, ITemplateDocument {

    public byte[] getContent() {
        return CommonBaseUtil.getTemplateContent(this);
    }
}