package ru.tandemservice.movestudentrmc.component.student.DocumentAddEdit;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.movestudentrmc.entity.NarfuDocument;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        getDao().prepare(getModel(component));
    }

    public void onClickApply(IBusinessComponent component) {
        Model model = component.getModel();
        getDao().update(model);
        deactivate(component);

        if (model.getListener() != null)
            model.getListener().process(true, model.getDocument());
    }

    public void onChangeDocument(IBusinessComponent component) {
        Model model = component.getModel();
        if (model.getDocumentRel().getDocument().getTitle() != null && model.getDocumentRel().getDocument().isShowName()) {
            model.getDocument().setName(model.getDocumentRel().getDocument().getTitle());
        }
        else {
            model.getDocument().setName("");
        }


    }

    public void onClickPrint(IBusinessComponent component) {
        Model model = component.getModel();

        NarfuDocument doc = model.getDocument();

        byte[] content = doc.getFile().getContent();
        if (content == null)
            throw new ApplicationException("Файл документа пуст.");
        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(content, doc.getFile().getFilename());
        activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new UniMap().add("id", id)));
    }

    public void onClickDelFile(IBusinessComponent component) {
        Model model = component.getModel();
        getDao().deleteFile(model);
    }
}
