package ru.tandemservice.movestudentrmc.entity;

public interface IRepresentOrder {

    public IAbstractOrder getOrder();

    public IAbstractRepresentation getRepresentation();
}
