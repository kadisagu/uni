package ru.tandemservice.movestudentrmc.dao;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

public interface IOrientationDAO {

    /**
     * Возвращает id всех выбранных студентами направленностей
     *
     * @return
     */
    public DQLSelectBuilder getOrientationsBuilder();

    /**
     * Возвращает id студентов, которые подписаны на данную направленность и данный учебный год
     *
     * @param educationOrgUnit - направленность
     * @param educationYear    - учебный год
     *
     * @return
     */
    public DQLSelectBuilder getStudentBuilder(EducationOrgUnit educationOrgUnit, EducationYear educationYear);
}
