package ru.tandemservice.movestudentrmc.component.menu.PaymentsList;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudentrmc.entity.IRepresentOrder;
import ru.tandemservice.movestudentrmc.entity.IStudentGrant;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntity;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntityHistory;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentGrantTypeCodes;
import ru.tandemservice.movestudentrmc.entity.gen.IRepresentOrderGen;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(final Model model) {

        model.setGrantViewModel(new LazySimpleSelectModel<>(getCatalogItemList(GrantView.class, GrantView.P_USE, true), GrantView.shortTitle().s()));
        model.setStudentStatusModel(new LazySimpleSelectModel<>(getCatalogItemList(StudentStatus.class, StudentStatus.P_USED_IN_SYSTEM, true)));
        model.setCourseModel(new LazySimpleSelectModel<>(DevelopGridDAO.getCourseList()));
        model.setCompensationTypeModel(new LazySimpleSelectModel<>(CompensationType.class));
        model.setGroupModel(new LazySimpleSelectModel<>(Group.class));
        model.setStudentCategoryModel(new LazySimpleSelectModel<>(StudentCategory.class));
        model.setFormativeOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING, null));
        model.setEduLevelModel(new FullCheckSelectModel(EducationLevelsHighSchool.displayableTitle().s()) {

            @Override
            public ListResult<EducationLevelsHighSchool> findValues(String filter) {

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "e")
                        .column(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().fromAlias("e")))
                        .where(DQLExpressions.eqValue(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().educationLevel().levelType().profile().fromAlias("e")), Boolean.FALSE))
                        .where(DQLExpressions.eqValue(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().educationLevel().levelType().specialization().fromAlias("e")), Boolean.FALSE));

                FilterUtils.applySelectFilter(builder, "e", EducationOrgUnit.formativeOrgUnit(), model.getFormativeOrgUnitList());

                if (filter != null)
                    builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().displayableTitle().fromAlias("e"))), DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));

                List<EducationLevelsHighSchool> lst = getList(builder);

                return new ListResult<>(lst);
            }
        });

        model.setQualificationsModel(new QualificationModel(getSession()));
        model.setDevelopFormModel(new FullCheckSelectModel(DevelopForm.title().s()) {

            @Override
            public ListResult<DevelopForm> findValues(String filter) {

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "e")
                        .column(DQLExpressions.property(EducationOrgUnit.developForm().fromAlias("e")));

                FilterUtils.applySelectFilter(builder, "e", EducationOrgUnit.formativeOrgUnit(), model.getFormativeOrgUnitList());
                FilterUtils.applySelectFilter(builder, "e", EducationOrgUnit.educationLevelHighSchool(), model.getEducationLevelsHighSchools());

                if (filter != null)
                    builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(EducationOrgUnit.developForm().title().fromAlias("e"))), DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));

                List<DevelopForm> lst = getList(builder);

                return new ListResult<>(lst);
            }
        });
        model.setDevelopConditionModel(new FullCheckSelectModel(DevelopCondition.title().s()) {

            @Override
            public ListResult<DevelopCondition> findValues(String filter) {

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "e")
                        .column(DQLExpressions.property(EducationOrgUnit.developCondition().fromAlias("e")));

                FilterUtils.applySelectFilter(builder, "e", EducationOrgUnit.formativeOrgUnit(), model.getFormativeOrgUnitList());
                FilterUtils.applySelectFilter(builder, "e", EducationOrgUnit.educationLevelHighSchool(), model.getEducationLevelsHighSchools());
                FilterUtils.applySelectFilter(builder, "e", EducationOrgUnit.developForm(), model.getDevelopForms());

                if (filter != null)
                    builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(EducationOrgUnit.developCondition().title().fromAlias("e"))), DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));

                List<DevelopCondition> lst = getList(builder);

                return new ListResult<>(lst);
            }
        });
        model.setDevelopPeriodModel(new FullCheckSelectModel(DevelopPeriod.title().s()) {

            @Override
            public ListResult<DevelopPeriod> findValues(String filter) {

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "e")
                        .column(DQLExpressions.property(EducationOrgUnit.developPeriod().fromAlias("e")));

                FilterUtils.applySelectFilter(builder, "e", EducationOrgUnit.formativeOrgUnit(), model.getFormativeOrgUnitList());
                FilterUtils.applySelectFilter(builder, "e", EducationOrgUnit.educationLevelHighSchool(), model.getEducationLevelsHighSchools());
                FilterUtils.applySelectFilter(builder, "e", EducationOrgUnit.developForm(), model.getDevelopForms());
                FilterUtils.applySelectFilter(builder, "e", EducationOrgUnit.developCondition(), model.getDevelopConditions());

                if (filter != null)
                    builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(EducationOrgUnit.developCondition().title().fromAlias("e"))), DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));

                List<DevelopPeriod> lst = getList(builder);

                return new ListResult<>(lst);
            }
        });
    }

    private Date getLastTime(Date date) {
        if (date != null) {
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            c.add(Calendar.DAY_OF_YEAR, 1);
            c.add(Calendar.MILLISECOND, -1);
            date = c.getTime();
        }
        return date;
    }

    @Override
    public void prepareListDataSource(Model model) {

        IDataSettings settings = model.getSettings();

        String fio = settings.get("title");
        List<GrantView> grantViewList = settings.get("grantViewlist");

        String orderNumber = (String) settings.get("orderNumber");
        Date orderFromDate = (Date) settings.get("orderFromDate");
        Date orderToDate = getLastTime((Date) settings.get("orderToDate"));

        String orderCancelNumber = (String) settings.get("orderCancelNumber");
        Date orderCancelFromDate = (Date) settings.get("orderCancelFromDate");
        Date orderCancelToDate = getLastTime((Date) settings.get("orderCancelToDate"));

        String orderSuspendNumber = (String) settings.get("orderSuspendNumber");
        Date orderSuspendFromDate = (Date) settings.get("orderSuspendFromDate");
        Date orderSuspendToDate = getLastTime((Date) settings.get("orderSuspendToDate"));

        String orderResumeNumber = (String) settings.get("orderResumeNumber");
        Date orderResumeFromDate = (Date) settings.get("orderResumeFromDate");
        Date orderResumeToDate = getLastTime((Date) settings.get("orderResumeToDate"));

        String bookNumber = (String) settings.get("bookNumber");
        Integer entranceYear = (Integer) settings.get("entranceYear");
        List<StudentStatus> studentStatusList = settings.get("studentStatusList");
        List<Course> courseList = settings.get("courseList");
        CompensationType compensationType = (CompensationType) settings.get("compensationType");
        List<Group> groupList = settings.get("groupList");
        List<StudentCategory> studentCategoryList = settings.get("studentCategory");
        List<OrgUnit> formativeOrgUnitList = settings.get("formativeOrgUnitList");
        List<EducationLevelsHighSchool> eduLevelList = settings.get("eduLevelList");
        List<Qualifications> qualificationsList = settings.get("qualificationsList");
        List<DevelopForm> developFormList = settings.get("developFormList");
        List<DevelopCondition> developConditionList = settings.get("developConditionList");
        List<DevelopPeriod> developPeriodList = settings.get("developPeriodList");

        DynamicListDataSource<Wrapper> dataSource = model.getDataSource();

        //из истории изменения стипендии заберем все стипендии, по которым были изменения кроме начисления
        DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(StudentGrantEntityHistory.class, "h")
                .where(DQLExpressions.ne(DQLExpressions.property(StudentGrantEntityHistory.representGrantType().code().fromAlias("h")), RepresentGrantTypeCodes.DESTINATION))
                .column(DQLExpressions.property(StudentGrantEntityHistory.studentGrantEntity().id().fromAlias("h")));
        //сгруппируем стипендии, по которым были изменения, по студенту и представлению, которое назначало стипедиию
        DQLSelectBuilder historyBuilder = new DQLSelectBuilder().fromEntity(StudentGrantEntityHistory.class, "h")
                .where(DQLExpressions.in(DQLExpressions.property(StudentGrantEntityHistory.studentGrantEntity().id().fromAlias("h")), subBuilder.getQuery()))
                .where(DQLExpressions.eqValue(DQLExpressions.property(StudentGrantEntityHistory.representGrantType().code().fromAlias("h")), RepresentGrantTypeCodes.DESTINATION))
                .column(DQLFunctions.max(DQLExpressions.property(StudentGrantEntityHistory.studentGrantEntity().id().fromAlias("h"))))
                .group(DQLExpressions.property(StudentGrantEntityHistory.studentGrantEntity().student().id().fromAlias("h")))
                .group(DQLExpressions.property(StudentGrantEntityHistory.studentGrantEntity().view().id().fromAlias("h")))
                .group(DQLExpressions.property(StudentGrantEntityHistory.representation().representID().fromAlias("h")));

        //применяем фильтры
        DQLSelectBuilder grantBuilder = new DQLSelectBuilder().fromEntity(StudentGrantEntity.class, "sge")
                .where(DQLExpressions.eqValue(DQLExpressions.property(StudentGrantEntity.tmp().fromAlias("sge")), Boolean.FALSE))
                .where(DQLExpressions.in(DQLExpressions.property(StudentGrantEntity.id().fromAlias("sge")), historyBuilder.getQuery()));

        if (fio != null)
            grantBuilder.where(like(DQLFunctions.upper(property(StudentGrantEntity.student().person().identityCard().fullFio().fromAlias("sge"))), value(CoreStringUtils.escapeLike(fio, true))));

        FilterUtils.applySelectFilter(grantBuilder, "sge", StudentGrantEntity.view(), grantViewList);
        FilterUtils.applySelectFilter(grantBuilder, "sge", StudentGrantEntity.student().bookNumber(), bookNumber);
        FilterUtils.applySelectFilter(grantBuilder, "sge", StudentGrantEntity.student().entranceYear(), entranceYear);
        FilterUtils.applySelectFilter(grantBuilder, "sge", StudentGrantEntity.student().status(), studentStatusList);
        FilterUtils.applySelectFilter(grantBuilder, "sge", StudentGrantEntity.student().course(), courseList);
        FilterUtils.applySelectFilter(grantBuilder, "sge", StudentGrantEntity.student().compensationType(), compensationType);
        FilterUtils.applySelectFilter(grantBuilder, "sge", StudentGrantEntity.student().group(), groupList);
        FilterUtils.applySelectFilter(grantBuilder, "sge", StudentGrantEntity.student().studentCategory(), studentCategoryList);
        FilterUtils.applySelectFilter(grantBuilder, "sge", StudentGrantEntity.student().educationOrgUnit().formativeOrgUnit(), formativeOrgUnitList);
        FilterUtils.applySelectFilter(grantBuilder, "sge", StudentGrantEntity.student().educationOrgUnit().educationLevelHighSchool(), eduLevelList);
        FilterUtils.applySelectFilter(grantBuilder, "sge", StudentGrantEntity.student().educationOrgUnit().educationLevelHighSchool().educationLevel().qualification(), qualificationsList);
        FilterUtils.applySelectFilter(grantBuilder, "sge", StudentGrantEntity.student().educationOrgUnit().developForm(), developFormList);
        FilterUtils.applySelectFilter(grantBuilder, "sge", StudentGrantEntity.student().educationOrgUnit().developCondition(), developConditionList);
        FilterUtils.applySelectFilter(grantBuilder, "sge", StudentGrantEntity.student().educationOrgUnit().developPeriod(), developPeriodList);

        //заполним инофрмацией о приказах, которые изменяли
        model.setOrderMap(getOrderInfoMap(grantBuilder, orderNumber, orderFromDate, orderToDate, RepresentGrantTypeCodes.DESTINATION));
        model.setCancelOrderMap(getOrderInfoMap(grantBuilder, orderCancelNumber, orderCancelFromDate, orderCancelToDate, RepresentGrantTypeCodes.CANCEL));
        model.setSuspendOrderMap(getOrderInfoMap(grantBuilder, orderSuspendNumber, orderSuspendFromDate, orderSuspendToDate, RepresentGrantTypeCodes.SUSPEND));
        model.setResumeOrderMap(getOrderInfoMap(grantBuilder, orderResumeNumber, orderResumeFromDate, orderResumeToDate, RepresentGrantTypeCodes.RESUME));

        List<Wrapper> result = new ArrayList<>();
        grantBuilder.order(DQLExpressions.property(StudentGrantEntity.student().person().identityCard().fullFio().fromAlias("sge")));
        List<StudentGrantEntity> lst = getList(grantBuilder);
        //формируем список
        for (StudentGrantEntity sge : lst) {

            IRepresentOrder order = model.getOrderMap().containsKey(sge.getId()) ? model.getOrderMap().get(sge.getId()).get(0) : null;
            IRepresentOrder cancelOrder = model.getCancelOrderMap().containsKey(sge.getId()) ? model.getCancelOrderMap().get(sge.getId()).get(0) : null;
            List<IRepresentOrder> suspendOrderLst = model.getSuspendOrderMap().get(sge.getId());
            List<IRepresentOrder> resumeOrderLst = model.getResumeOrderMap().get(sge.getId());

            //отбрысываем СГЕ, по которым нет приказов (не попали в фильтр)
            if (order == null)
                continue;
            if (isOrderFilterActive(orderCancelNumber, orderCancelFromDate, orderCancelToDate) && cancelOrder == null)
                continue;
            if (isOrderFilterActive(orderSuspendNumber, orderSuspendFromDate, orderSuspendToDate) && suspendOrderLst == null)
                continue;
            if (isOrderFilterActive(orderResumeNumber, orderResumeFromDate, orderResumeToDate) && resumeOrderLst == null)
                continue;

            //пданные приказа о назначениии
            String orderTitle = "";
            Long orderId = null;
            if (order != null) {

                Date startDate = ((IStudentGrant) order.getRepresentation()).getBeginGrantDate();
                Date enddate = ((IStudentGrant) order.getRepresentation()).getEndGrantDate();

                orderTitle = getOrderTitle(order.getOrder().getNumber(), order.getOrder().getCommitDate(), startDate, enddate);
                orderId = order.getOrder().getOrderID();
            }
            //данные приказа об отмене
            String orderCancelTitle = "";
            Long orderCancelId = null;
            if (cancelOrder != null) {

                Date date = cancelOrder.getRepresentation() instanceof IStudentGrant ? ((IStudentGrant) order.getRepresentation()).getBeginGrantDate() : null;

                orderCancelTitle = getOrderTitle(cancelOrder.getOrder().getNumber(), cancelOrder.getOrder().getCommitDate(), date, null);
                orderCancelId = cancelOrder.getOrder().getOrderID();
            }
            /*
			 * приказов о приостановлении и возобновлении может быть несколько
			 * для этого дублируем строки, для послеующего объединения по составному ключу
			 * 
			 *  Если было несколько приказов на приостановление и возобновление одной и той же выплаты, то строка в таблице должна быть одна. 
			 *  Соответствующие приказы выводить в ячейке друг под другом.
			 */
            if (suspendOrderLst != null && !suspendOrderLst.isEmpty()) {
                Iterator<IRepresentOrder> iterator = suspendOrderLst.iterator();
                while (iterator.hasNext()) {
                    IRepresentOrder suspendOrderInfo = (IRepresentOrder) iterator.next();

                    String suspendOrderTitle = getOrderTitle(suspendOrderInfo.getOrder().getNumber(), suspendOrderInfo.getOrder().getCommitDate(), ((IStudentGrant) suspendOrderInfo.getRepresentation()).getBeginGrantDate(), null);

                    IRepresentOrder resumeOrderInfo = (resumeOrderLst != null && resumeOrderLst.isEmpty()) ? null : (IRepresentOrder) resumeOrderLst.get(0);

                    String resumeOrderTitle = "";
                    Long resumeOrderId = null;
                    if (resumeOrderInfo != null) {
                        resumeOrderTitle = getOrderTitle(resumeOrderInfo.getOrder().getNumber(), resumeOrderInfo.getOrder().getCommitDate(), ((IStudentGrant) resumeOrderInfo.getRepresentation()).getBeginGrantDate(), null);
                        resumeOrderId = resumeOrderInfo.getOrder().getOrderID();
                    }

                    result.add(new Wrapper(sge, orderTitle, orderId, orderCancelTitle, orderCancelId, resumeOrderTitle, resumeOrderId, suspendOrderTitle, suspendOrderInfo.getOrder().getOrderID()));
                    if (resumeOrderInfo != null)
                        resumeOrderLst.remove(resumeOrderInfo);
                }
            }
            else
                result.add(new Wrapper(sge, orderTitle, orderId, orderCancelTitle, orderCancelId, "", null, "", null));

        }
        //сортировки
        Collections.sort(result, new EntityComparator<>(model.getDataSource().getEntityOrder()));

        UniBaseUtils.createPage(dataSource, result);
    }


    private Map<Long, List<IRepresentOrder>> getOrderInfoMap(DQLSelectBuilder sgeBuilder, String orderNumber, Date fromDate, Date toDate, String typeCode) {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentGrantEntityHistory.class, "h")
                .where(DQLExpressions.in(DQLExpressions.property(StudentGrantEntityHistory.studentGrantEntity().id().fromAlias("h")), sgeBuilder.getQuery()))
                .where(DQLExpressions.eqValue(DQLExpressions.property(StudentGrantEntityHistory.representGrantType().code().fromAlias("h")), typeCode))
                .joinEntity("h", DQLJoinType.inner, IRepresentOrder.class, "rel",
                            DQLExpressions.eq(DQLExpressions.property(StudentGrantEntityHistory.representation().id().fromAlias("h")),
                                              DQLExpressions.property(IRepresentOrderGen.representation().id().fromAlias("rel"))))
                .column(DQLExpressions.property(StudentGrantEntityHistory.studentGrantEntity().id().fromAlias("h")))
                .column("rel");

        FilterUtils.applySelectFilter(builder, "rel", IRepresentOrderGen.order().number(), orderNumber);
        FilterUtils.applyBetweenFilter(builder, "rel", IRepresentOrderGen.order().createDate().s(), fromDate, toDate);

        List<Object[]> lst = getList(builder);

        Map<Long, List<IRepresentOrder>> map = new HashMap<>();

        for (Object[] obj : lst) {
            Long id = (Long) obj[0];
            IRepresentOrder order = (IRepresentOrder) obj[1];

            List<IRepresentOrder> list = map.get(id);
            if (list == null)
                list = new ArrayList<>();
            list.add(order);

            map.put(id, list);
        }

        return map;
    }

    /**
     * есть ли фильтр по приказу
     */
    private boolean isOrderFilterActive(String orderNumber, Date fromDate, Date toDate) {
        return orderNumber != null || fromDate != null || toDate != null;
    }

    private String getOrderTitle(String number, Date commitDate, Date fromDate, Date toDate) {
        StringBuilder title = new StringBuilder();

        title.append("Приказ от ")
                .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(commitDate))
                .append(" №")
                .append(number);
        if (fromDate != null)
            title.append(" c ")
                    .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(fromDate));
        if (toDate != null)
            title.append(" по ")
                    .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(toDate));
        return title.toString();
    }

}

