package ru.tandemservice.movestudentrmc.base.bo.DocumentSettings.ui.AddEdit;

import ru.tandemservice.movestudentrmc.entity.catalog.Document;
import ru.tandemservice.movestudentrmc.entity.catalog.DocumentKind;

import java.util.ArrayList;
import java.util.List;

public class Wrapper {

    private DocumentKind kind;
    private List<Document> documents = new ArrayList<Document>();

    public DocumentKind getKind() {
        return kind;
    }

    public void setKind(DocumentKind kind) {
        this.kind = kind;
    }

    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

}
