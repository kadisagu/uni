package ru.tandemservice.movestudentrmc.dao.check;

import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.ICitizenship;
import ru.tandemservice.uni.entity.employee.Student;

public class CheckCitizensRF extends AbstractCheckDAO {

    @Override
    protected boolean getCondition(Student student)
    {
        ICitizenship citizenship = student.getPerson().getIdentityCard().getCitizenship();
        return citizenship instanceof AddressCountry && ((AddressCountry) citizenship).isDisabled();
    }

    @Override
    protected String getErrorMessage(Student student, boolean value) {

        return new StringBuilder()
                .append("Студент ")
                .append(student.getPerson().getFullFio())
                .append(" ")
                .append(value ? "не " : "")
                .append("является гражданином РФ").toString();
    }

}
