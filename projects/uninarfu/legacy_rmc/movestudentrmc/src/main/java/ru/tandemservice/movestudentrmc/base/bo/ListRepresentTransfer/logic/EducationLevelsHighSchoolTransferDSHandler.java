package ru.tandemservice.movestudentrmc.base.bo.ListRepresentTransfer.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.FilterUtils;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class EducationLevelsHighSchoolTransferDSHandler extends DefaultComboDataSourceHandler {

    public EducationLevelsHighSchoolTransferDSHandler(String ownerId) {
        super(ownerId, EducationLevelsHighSchool.class);

    }

    public static final String NEW_FORMATIVE_ORG_UNIT = "newFormativeOrgUnit";
    public static final String NEW_TERRITORIAL_ORG_UNIT = "newTerritorialOrgUnit";
    public static final String FILTER_BY_DEVELOP = "filterByDevelop";
    public static final String DEVELOP_FORM = "developForm";

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep) {


        OrgUnit newFormativeOrgUnit = ep.context.get(NEW_FORMATIVE_ORG_UNIT);
        OrgUnit newTerritorialOrgUnit = ep.context.get(NEW_TERRITORIAL_ORG_UNIT);
        Boolean filterByDevelop = (Boolean) ep.context.get(FILTER_BY_DEVELOP);
        DevelopForm devForm = ep.context.get(DEVELOP_FORM);

        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(EducationOrgUnit.class, "eou");

        builder.column(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().fromAlias("eou")));

        FilterUtils.applySelectFilter(builder, "eou", EducationOrgUnit.formativeOrgUnit(), newFormativeOrgUnit);

        if (newTerritorialOrgUnit != null)
            builder.where(DQLExpressions.eq(DQLExpressions.property(EducationOrgUnit.territorialOrgUnit().fromAlias("eou")), DQLExpressions.value(newTerritorialOrgUnit)));

        if (Boolean.TRUE.equals(filterByDevelop)) {
            builder.where(DQLExpressions.eq(DQLExpressions.property(EducationOrgUnit.developForm().fromAlias("eou")), DQLExpressions.value(devForm)));
        }


        ep.dqlBuilder.where(DQLExpressions.in(DQLExpressions.property(EducationLevelsHighSchool.id().fromAlias("e")), builder.getQuery()));

        String filter = ep.input.getComboFilterByValue();
        ;
        if (StringUtils.isNotBlank(filter)) {
            ep.dqlBuilder.where(like(DQLFunctions.upper(property(EducationLevelsHighSchool.fullTitle().fromAlias("e"))), value(CoreStringUtils.escapeLike(filter, true))));
        }
    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep) {
        ep.dqlBuilder.order("e." + EducationLevelsHighSchool.educationLevel().title());
    }


}
