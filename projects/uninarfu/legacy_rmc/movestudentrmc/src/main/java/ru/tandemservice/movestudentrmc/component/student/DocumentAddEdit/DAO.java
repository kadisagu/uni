package ru.tandemservice.movestudentrmc.component.student.DocumentAddEdit;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudentrmc.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudentrmc.entity.NarfuDocument;
import ru.tandemservice.movestudentrmc.entity.RelDocumentTypeKind;
import ru.tandemservice.movestudentrmc.entity.catalog.DocumentKind;
import ru.tandemservice.movestudentrmc.entity.catalog.DocumentType;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;

import java.io.InputStream;
import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(final Model model) {
        model.setStudent(getNotNull(Student.class, model.getStudent().getId()));

        if (model.getDocument().getId() == null) {
            model.setAddForm(true);
        }
        else {
            model.setAddForm(false);

            NarfuDocument document = get(NarfuDocument.class, model.getDocument().getId());
            model.setDocument(document);
            model.setDocumentType(document.getSettings().getType());
            model.setDocumentKind(document.getSettings().getKind());
            model.setDocumentRel(document.getSettings());
        }

        model.setTypeList(getCatalogItemList(DocumentType.class));

        model.setKindModel(new BaseSingleSelectModel() {
            @Override
            public ListResult<DocumentKind> findValues(String arg0) {
                List<DocumentKind> list = MoveStudentDaoFacade.getDocumentDAO().getKinds(model.getDocumentType());
                return new ListResult<>(list);
            }

            @Override
            public Object getValue(Object arg0) {
                return get(DocumentKind.class, (Long) arg0);
            }
        });

        model.setRelModel(new BaseSingleSelectModel("id", "document.title") {
            @Override
            public ListResult<RelDocumentTypeKind> findValues(String filter) {
                MQBuilder builder = new MQBuilder(RelDocumentTypeKind.ENTITY_CLASS, "r")
                        .add(MQExpression.eq("r", RelDocumentTypeKind.type(), model.getDocumentType()))
                        .add(MQExpression.eq("r", RelDocumentTypeKind.kind(), model.getDocumentKind()));

                if (filter != null && !StringUtils.isEmpty(filter))
                    builder.add(MQExpression.like("r", RelDocumentTypeKind.document().title(), filter));

                List<RelDocumentTypeKind> list = builder.getResultList(getSession());
                return new ListResult<>(list);
            }

            @Override
            public Object getValue(Object arg0) {
                return get(RelDocumentTypeKind.class, (Long) arg0);
            }

            @Override
            public String getFullLabel(Object obj) {
                return ((RelDocumentTypeKind) obj).getDocument().getTitle();
            }
        });
    }

    @Override
    public void update(Model model) {
        NarfuDocument document = model.getDocument();

        if (model.getDocumentRel().getDocument().isSeriaExist() && model.getDocumentRel().getDocument().isSeriaRequired()) {
            if (StringUtils.isEmpty(document.getSeria()))
                throw new ApplicationException("Поле серия не может быть пустым");

            if (model.getDocumentRel().getDocument().getSeriaMin() != null)
                if (document.getSeria().length() < model.getDocumentRel().getDocument().getSeriaMin())
                    throw new ApplicationException("Поле серия должно содержать минимум " + model.getDocumentRel().getDocument().getSeriaMin() + " знаков");

            if (model.getDocumentRel().getDocument().getSeriaMax() != null)
                if (document.getSeria().length() > model.getDocumentRel().getDocument().getSeriaMax())
                    throw new ApplicationException("Поле серия должно содержать максимум " + model.getDocumentRel().getDocument().getSeriaMax() + " знаков");

        }

        if (model.getDocumentRel().getDocument().isNumberExist() && model.getDocumentRel().getDocument().isNumberRequired()) {
            if (StringUtils.isEmpty(document.getNumber()))
                throw new ApplicationException("Поле номер не может быть пустым");

            if (model.getDocumentRel().getDocument().getNumberMin() != null)
                if (document.getNumber().length() < model.getDocumentRel().getDocument().getNumberMin())
                    throw new ApplicationException("Поле номер должно содержать минимум " + model.getDocumentRel().getDocument().getNumberMin() + " знаков");

            if (model.getDocumentRel().getDocument().getNumberMax() != null)
                if (document.getNumber().length() > model.getDocumentRel().getDocument().getNumberMax())
                    throw new ApplicationException("Поле номер должно содержать максимум " + model.getDocumentRel().getDocument().getNumberMax() + " знаков");

        }

        if (model.getDocumentRel().getDocument().isIssuanceDateExist() && model.getDocumentRel().getDocument().isIssuanceDateRequired()) {
            if (document.getIssuanceDate() == null)
                throw new ApplicationException("Поле дата выдачи не может быть пустым");
        }

        if (model.getDocumentRel().getDocument().isPeriodDateExist() && model.getDocumentRel().getDocument().isPeriodDateRequired()) {
            if (document.getPeriodDateStart() == null)
                throw new ApplicationException("Поле дата начала действия не может быть пустым");
            if (document.getPeriodDateEnd() == null)
                throw new ApplicationException("Поле дата окончания действия не может быть пустым");
        }

        document.setSettings(model.getDocumentRel());
        document.setStudent(model.getStudent());

        if (model.getFile() != null) {
            try {
                InputStream input = model.getFile().getStream();
                byte[] content = IOUtils.toByteArray(input);

                DatabaseFile file = new DatabaseFile();
                file.setContent(content);
                file.setContentType(CommonBaseUtil.getContentType(model.getFile()));
                file.setFilename(model.getFile().getFileName());

                if (document.getFile() != null)
                    getSession().delete(document.getFile());

                getSession().save(file);
                document.setFile(file);

            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        saveOrUpdate(document);
    }

    @Override
    public void deleteFile(Model model) {
        NarfuDocument doc = model.getDocument();
        DatabaseFile file = doc.getFile();
        if (file == null)
            return;

        doc.setFile(null);
        saveOrUpdate(doc);
        delete(file);

    }
}
