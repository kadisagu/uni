package ru.tandemservice.movestudentrmc.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.OrderCategory;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Типы представлений
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RepresentationTypeGen extends EntityBase
 implements INaturalIdentifiable<RepresentationTypeGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType";
    public static final String ENTITY_NAME = "representationType";
    public static final int VERSION_HASH = -889552985;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_INSTRUCTION = "instruction";
    public static final String P_PRIORITY = "priority";
    public static final String P_USE = "use";
    public static final String P_GROUPING = "grouping";
    public static final String P_LIST_REPRESENTATION = "listRepresentation";
    public static final String P_PRINT_ATTACHMENT = "printAttachment";
    public static final String P_PRINT_DECLARATION = "printDeclaration";
    public static final String P_CHECK = "check";
    public static final String L_ORDER_CATEGORY = "orderCategory";
    public static final String P_TITLE = "title";
    public static final String P_CONFIG_O_S_S_P = "configOSSP";
    public static final String P_CONFIG_RELATION = "configRelation";
    public static final String P_TITLE_WITH_TYPE = "titleWithType";

    private String _code;     // Системный код
    private boolean _instruction; 
    private int _priority; 
    private boolean _use; 
    private boolean _grouping; 
    private boolean _listRepresentation; 
    private boolean _printAttachment = false; 
    private boolean _printDeclaration; 
    private boolean _check = false;     // Выполнять проверку
    private OrderCategory _orderCategory;     // Категория приказа
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public boolean isInstruction()
    {
        return _instruction;
    }

    /**
     * @param instruction  Свойство не может быть null.
     */
    public void setInstruction(boolean instruction)
    {
        dirty(_instruction, instruction);
        _instruction = instruction;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority  Свойство не может быть null.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public boolean isUse()
    {
        return _use;
    }

    /**
     * @param use  Свойство не может быть null.
     */
    public void setUse(boolean use)
    {
        dirty(_use, use);
        _use = use;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public boolean isGrouping()
    {
        return _grouping;
    }

    /**
     * @param grouping  Свойство не может быть null.
     */
    public void setGrouping(boolean grouping)
    {
        dirty(_grouping, grouping);
        _grouping = grouping;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public boolean isListRepresentation()
    {
        return _listRepresentation;
    }

    /**
     * @param listRepresentation  Свойство не может быть null.
     */
    public void setListRepresentation(boolean listRepresentation)
    {
        dirty(_listRepresentation, listRepresentation);
        _listRepresentation = listRepresentation;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public boolean isPrintAttachment()
    {
        return _printAttachment;
    }

    /**
     * @param printAttachment  Свойство не может быть null.
     */
    public void setPrintAttachment(boolean printAttachment)
    {
        dirty(_printAttachment, printAttachment);
        _printAttachment = printAttachment;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public boolean isPrintDeclaration()
    {
        return _printDeclaration;
    }

    /**
     * @param printDeclaration  Свойство не может быть null.
     */
    public void setPrintDeclaration(boolean printDeclaration)
    {
        dirty(_printDeclaration, printDeclaration);
        _printDeclaration = printDeclaration;
    }

    /**
     * @return Выполнять проверку. Свойство не может быть null.
     */
    @NotNull
    public boolean isCheck()
    {
        return _check;
    }

    /**
     * @param check Выполнять проверку. Свойство не может быть null.
     */
    public void setCheck(boolean check)
    {
        dirty(_check, check);
        _check = check;
    }

    /**
     * @return Категория приказа. Свойство не может быть null.
     */
    @NotNull
    public OrderCategory getOrderCategory()
    {
        return _orderCategory;
    }

    /**
     * @param orderCategory Категория приказа. Свойство не может быть null.
     */
    public void setOrderCategory(OrderCategory orderCategory)
    {
        dirty(_orderCategory, orderCategory);
        _orderCategory = orderCategory;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RepresentationTypeGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((RepresentationType)another).getCode());
            }
            setInstruction(((RepresentationType)another).isInstruction());
            setPriority(((RepresentationType)another).getPriority());
            setUse(((RepresentationType)another).isUse());
            setGrouping(((RepresentationType)another).isGrouping());
            setListRepresentation(((RepresentationType)another).isListRepresentation());
            setPrintAttachment(((RepresentationType)another).isPrintAttachment());
            setPrintDeclaration(((RepresentationType)another).isPrintDeclaration());
            setCheck(((RepresentationType)another).isCheck());
            setOrderCategory(((RepresentationType)another).getOrderCategory());
            setTitle(((RepresentationType)another).getTitle());
        }
    }

    public INaturalId<RepresentationTypeGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<RepresentationTypeGen>
    {
        private static final String PROXY_NAME = "RepresentationTypeNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof RepresentationTypeGen.NaturalId) ) return false;

            RepresentationTypeGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RepresentationTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RepresentationType.class;
        }

        public T newInstance()
        {
            return (T) new RepresentationType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "instruction":
                    return obj.isInstruction();
                case "priority":
                    return obj.getPriority();
                case "use":
                    return obj.isUse();
                case "grouping":
                    return obj.isGrouping();
                case "listRepresentation":
                    return obj.isListRepresentation();
                case "printAttachment":
                    return obj.isPrintAttachment();
                case "printDeclaration":
                    return obj.isPrintDeclaration();
                case "check":
                    return obj.isCheck();
                case "orderCategory":
                    return obj.getOrderCategory();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "instruction":
                    obj.setInstruction((Boolean) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
                case "use":
                    obj.setUse((Boolean) value);
                    return;
                case "grouping":
                    obj.setGrouping((Boolean) value);
                    return;
                case "listRepresentation":
                    obj.setListRepresentation((Boolean) value);
                    return;
                case "printAttachment":
                    obj.setPrintAttachment((Boolean) value);
                    return;
                case "printDeclaration":
                    obj.setPrintDeclaration((Boolean) value);
                    return;
                case "check":
                    obj.setCheck((Boolean) value);
                    return;
                case "orderCategory":
                    obj.setOrderCategory((OrderCategory) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "instruction":
                        return true;
                case "priority":
                        return true;
                case "use":
                        return true;
                case "grouping":
                        return true;
                case "listRepresentation":
                        return true;
                case "printAttachment":
                        return true;
                case "printDeclaration":
                        return true;
                case "check":
                        return true;
                case "orderCategory":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "instruction":
                    return true;
                case "priority":
                    return true;
                case "use":
                    return true;
                case "grouping":
                    return true;
                case "listRepresentation":
                    return true;
                case "printAttachment":
                    return true;
                case "printDeclaration":
                    return true;
                case "check":
                    return true;
                case "orderCategory":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "instruction":
                    return Boolean.class;
                case "priority":
                    return Integer.class;
                case "use":
                    return Boolean.class;
                case "grouping":
                    return Boolean.class;
                case "listRepresentation":
                    return Boolean.class;
                case "printAttachment":
                    return Boolean.class;
                case "printDeclaration":
                    return Boolean.class;
                case "check":
                    return Boolean.class;
                case "orderCategory":
                    return OrderCategory.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RepresentationType> _dslPath = new Path<RepresentationType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RepresentationType");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType#isInstruction()
     */
    public static PropertyPath<Boolean> instruction()
    {
        return _dslPath.instruction();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType#isUse()
     */
    public static PropertyPath<Boolean> use()
    {
        return _dslPath.use();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType#isGrouping()
     */
    public static PropertyPath<Boolean> grouping()
    {
        return _dslPath.grouping();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType#isListRepresentation()
     */
    public static PropertyPath<Boolean> listRepresentation()
    {
        return _dslPath.listRepresentation();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType#isPrintAttachment()
     */
    public static PropertyPath<Boolean> printAttachment()
    {
        return _dslPath.printAttachment();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType#isPrintDeclaration()
     */
    public static PropertyPath<Boolean> printDeclaration()
    {
        return _dslPath.printDeclaration();
    }

    /**
     * @return Выполнять проверку. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType#isCheck()
     */
    public static PropertyPath<Boolean> check()
    {
        return _dslPath.check();
    }

    /**
     * @return Категория приказа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType#getOrderCategory()
     */
    public static OrderCategory.Path<OrderCategory> orderCategory()
    {
        return _dslPath.orderCategory();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType#getConfigOSSP()
     */
    public static SupportedPropertyPath<Boolean> configOSSP()
    {
        return _dslPath.configOSSP();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType#getConfigRelation()
     */
    public static SupportedPropertyPath<Boolean> configRelation()
    {
        return _dslPath.configRelation();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType#getTitleWithType()
     */
    public static SupportedPropertyPath<String> titleWithType()
    {
        return _dslPath.titleWithType();
    }

    public static class Path<E extends RepresentationType> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<Boolean> _instruction;
        private PropertyPath<Integer> _priority;
        private PropertyPath<Boolean> _use;
        private PropertyPath<Boolean> _grouping;
        private PropertyPath<Boolean> _listRepresentation;
        private PropertyPath<Boolean> _printAttachment;
        private PropertyPath<Boolean> _printDeclaration;
        private PropertyPath<Boolean> _check;
        private OrderCategory.Path<OrderCategory> _orderCategory;
        private PropertyPath<String> _title;
        private SupportedPropertyPath<Boolean> _configOSSP;
        private SupportedPropertyPath<Boolean> _configRelation;
        private SupportedPropertyPath<String> _titleWithType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(RepresentationTypeGen.P_CODE, this);
            return _code;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType#isInstruction()
     */
        public PropertyPath<Boolean> instruction()
        {
            if(_instruction == null )
                _instruction = new PropertyPath<Boolean>(RepresentationTypeGen.P_INSTRUCTION, this);
            return _instruction;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(RepresentationTypeGen.P_PRIORITY, this);
            return _priority;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType#isUse()
     */
        public PropertyPath<Boolean> use()
        {
            if(_use == null )
                _use = new PropertyPath<Boolean>(RepresentationTypeGen.P_USE, this);
            return _use;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType#isGrouping()
     */
        public PropertyPath<Boolean> grouping()
        {
            if(_grouping == null )
                _grouping = new PropertyPath<Boolean>(RepresentationTypeGen.P_GROUPING, this);
            return _grouping;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType#isListRepresentation()
     */
        public PropertyPath<Boolean> listRepresentation()
        {
            if(_listRepresentation == null )
                _listRepresentation = new PropertyPath<Boolean>(RepresentationTypeGen.P_LIST_REPRESENTATION, this);
            return _listRepresentation;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType#isPrintAttachment()
     */
        public PropertyPath<Boolean> printAttachment()
        {
            if(_printAttachment == null )
                _printAttachment = new PropertyPath<Boolean>(RepresentationTypeGen.P_PRINT_ATTACHMENT, this);
            return _printAttachment;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType#isPrintDeclaration()
     */
        public PropertyPath<Boolean> printDeclaration()
        {
            if(_printDeclaration == null )
                _printDeclaration = new PropertyPath<Boolean>(RepresentationTypeGen.P_PRINT_DECLARATION, this);
            return _printDeclaration;
        }

    /**
     * @return Выполнять проверку. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType#isCheck()
     */
        public PropertyPath<Boolean> check()
        {
            if(_check == null )
                _check = new PropertyPath<Boolean>(RepresentationTypeGen.P_CHECK, this);
            return _check;
        }

    /**
     * @return Категория приказа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType#getOrderCategory()
     */
        public OrderCategory.Path<OrderCategory> orderCategory()
        {
            if(_orderCategory == null )
                _orderCategory = new OrderCategory.Path<OrderCategory>(L_ORDER_CATEGORY, this);
            return _orderCategory;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(RepresentationTypeGen.P_TITLE, this);
            return _title;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType#getConfigOSSP()
     */
        public SupportedPropertyPath<Boolean> configOSSP()
        {
            if(_configOSSP == null )
                _configOSSP = new SupportedPropertyPath<Boolean>(RepresentationTypeGen.P_CONFIG_O_S_S_P, this);
            return _configOSSP;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType#getConfigRelation()
     */
        public SupportedPropertyPath<Boolean> configRelation()
        {
            if(_configRelation == null )
                _configRelation = new SupportedPropertyPath<Boolean>(RepresentationTypeGen.P_CONFIG_RELATION, this);
            return _configRelation;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType#getTitleWithType()
     */
        public SupportedPropertyPath<String> titleWithType()
        {
            if(_titleWithType == null )
                _titleWithType = new SupportedPropertyPath<String>(RepresentationTypeGen.P_TITLE_WITH_TYPE, this);
            return _titleWithType;
        }

        public Class getEntityClass()
        {
            return RepresentationType.class;
        }

        public String getEntityName()
        {
            return "representationType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract Boolean getConfigOSSP();

    public abstract Boolean getConfigRelation();

    public abstract String getTitleWithType();
}
