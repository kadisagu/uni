package ru.tandemservice.movestudentrmc.component.catalog.grant.GrantPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;
import ru.tandemservice.movestudentrmc.entity.catalog.Grant;

public interface IDAO extends IDefaultCatalogPubDAO<Grant, Model> {
}
