package ru.tandemservice.movestudentrmc.base.bo.DOOrder.ui.View;


import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.util.RtfUtil;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.DOOrderManager;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.logic.OrderPrint;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.logic.RepresentOrderDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.ui.Edit.DOOrderEdit;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.ui.Edit.DOOrderEditUI;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.ui.VisaEdit.DOOrderVisaEdit;
import ru.tandemservice.movestudentrmc.dao.CheckStateUtil;
import ru.tandemservice.movestudentrmc.entity.DocumentOrder;
import ru.tandemservice.movestudentrmc.entity.Principal2Visa;
import ru.tandemservice.movestudentrmc.entity.catalog.MovestudentOrderStates;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentOrderStatesCodes;
import ru.tandemservice.uni.util.ReportRenderer;

import java.util.Arrays;

@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = DOOrderViewUI.ORDER_ID)
})
public class DOOrderViewUI extends UIPresenter {


    public static final String ORDER_ID = "orderId";

    private Long _orderId;
    private DocumentOrder _order;

//    События компонента

    @Override
    public void onComponentRefresh()
    {
        _order = DataAccessServices.dao().get(DocumentOrder.class, DocumentOrder.id().s(), _orderId);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (DOOrderView.REPRESENT_ORDER_DS.equals(dataSource.getName())) {

            dataSource.put(RepresentOrderDSHandler.ORDER_ID, _orderId);
        }
    }

//    События формы

    public void onClickApprove() {
        if (_order.getCommitDate() == null)
            getConfig().getUserContext().getErrorCollector().add(getConfig().getProperty("ui.alertEmptyCommitedDate"));

        if (_order.getNumber() == null)
            getConfig().getUserContext().getErrorCollector().add(getConfig().getProperty("ui.alertEmptyOrderNumber"));

        if (getConfig().getUserContext().getErrorCollector().hasErrors())
            return;

        CheckStateUtil.checkStateOrder(_order, Arrays.asList(MovestudentOrderStatesCodes.CODE_1), _uiSupport.getSession());

        _order.setState(DataAccessServices.dao().get(MovestudentOrderStates.class, MovestudentOrderStates.code().s(), "2"));

        DOOrderManager.instance().modifyDao().updateState(_order);
    }

    public void onClickFormative() {

        CheckStateUtil.checkStateOrder(_order, Arrays.asList(MovestudentOrderStatesCodes.CODE_2), _uiSupport.getSession());

        _order.setState(DataAccessServices.dao().get(MovestudentOrderStates.class, MovestudentOrderStates.code().s(), "1"));

        DOOrderManager.instance().modifyDao().updateState(_order);
    }

    public void onClickDoApprove() {

        CheckStateUtil.checkStateOrder(_order, Arrays.asList(MovestudentOrderStatesCodes.CODE_2), _uiSupport.getSession());

        _order.setState(DataAccessServices.dao().get(MovestudentOrderStates.class, MovestudentOrderStates.code().s(), "3"));
        ErrorCollector error = getConfig().getUserContext().getErrorCollector();
        final RtfDocument docMain = OrderPrint.createPrintDocOrder(_order, error, false);
        if (docMain != null) {
            _order.setDocument(RtfUtil.toByteArray(docMain));
        }

        DOOrderManager.instance().modifyDao().updateState(_order);
        DOOrderManager.instance().modifyDao().saveExtractsText(_order);
    }

    public void onClickNotApprove() {

        CheckStateUtil.checkStateOrder(_order, Arrays.asList(MovestudentOrderStatesCodes.CODE_3), _uiSupport.getSession());

        _order.setState(DataAccessServices.dao().get(MovestudentOrderStates.class, MovestudentOrderStates.code().s(), "2"));
        _order.setDocument(null);
        DOOrderManager.instance().modifyDao().updateState(_order);
        DOOrderManager.instance().modifyDao().deleteExtractsText(_order);
    }

    public void onClickCommit() {
        CheckStateUtil.checkStateOrder(_order, Arrays.asList(MovestudentOrderStatesCodes.CODE_3), _uiSupport.getSession());

        ErrorCollector error = getConfig().getUserContext().getErrorCollector();
        //ErrorCollector error = new ErrorCollector();
        _order.setCommitDateSystem(getSupport().getCurrentDate());

        DOOrderManager.instance().modifyDao().doCommit(_order, error);
    }

    public void onClickRollback() {

        CheckStateUtil.checkStateOrder(_order, Arrays.asList(MovestudentOrderStatesCodes.CODE_5), _uiSupport.getSession());

        ErrorCollector error = getConfig().getUserContext().getErrorCollector();

        DOOrderManager.instance().modifyDao().doRollback(_order, error);
    }

    public void onClickPrint() {

        try {
            IDocumentRenderer doc = documentRenderer();
            if (doc != null)
                BusinessComponentUtils.downloadDocument(doc, true);
        }
        catch (Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    /**
     * Вызываем компонент редактирования данных полей визирующих лиц
     */
    public void onEditVisaFields() {
        _uiActivation.asRegionDialog(DOOrderVisaEdit.class)
                .parameter("orderId", _orderId)
                .activate();
    }

    public void onClickEdit() {
        _uiActivation.asCurrent(DOOrderEdit.class)
                .parameter(DOOrderEditUI.ORDER_ID, _orderId)
                .activate();
    }

//    Вспомогательные функции

    private IDocumentRenderer documentRenderer() {

        ErrorCollector error = getConfig().getUserContext().getErrorCollector();

        if (_order.getDocument() == null) {

            final RtfDocument docMain = OrderPrint.createPrintDocOrder(_order, error, false);
            if (docMain != null) {
                return new ReportRenderer("Order(draft).rtf", docMain, false);
            }
            else
                return null;
        }
        else {

            return new ReportRenderer("Order.rtf", _order.getDocument(), false);
        }
    }

//    Getters and Setters


    public Long getOrderId() {
        return _orderId;
    }

    public void setOrderId(Long orderId) {
        _orderId = orderId;
    }

    public DocumentOrder getOrder() {
        return _order;
    }

    public void setOrder(DocumentOrder order) {
        _order = order;
    }

    public Principal2Visa getPrincipal2Visa() {
        return DOOrderManager.instance().visaDao().getPrincipal2Visa();
    }
}
