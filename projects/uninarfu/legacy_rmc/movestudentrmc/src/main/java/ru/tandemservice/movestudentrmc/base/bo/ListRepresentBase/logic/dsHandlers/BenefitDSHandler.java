package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.shared.person.catalog.entity.Benefit;

public class BenefitDSHandler extends DefaultComboDataSourceHandler {

    public BenefitDSHandler(String ownerId) {
        super(ownerId, Benefit.class);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context) {
        return super.execute(input, context);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {

    }


    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
    {
        ep.dqlBuilder.order("e." + Benefit.shortTitle());
    }

}
