package ru.tandemservice.movestudentrmc.base.ext.UniStudent.ui.CustomStateList;

import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.movestudentrmc.entity.RelStudentCustomStateRepresent;
import ru.tandemservice.uni.dao.IUniBaseDao;


public class UniStudentCustomStateListUI extends ru.tandemservice.uni.base.bo.UniStudent.ui.CustomStateList.UniStudentCustomStateListUI {

    public boolean isVisible() {
        BaseSearchListDataSource stateDS = (BaseSearchListDataSource) getConfig().getDataSource("customStateListDS");

        IEntity entity = stateDS.getCurrentEntity();

        RelStudentCustomStateRepresent rel = IUniBaseDao.instance.get().get(RelStudentCustomStateRepresent.class, RelStudentCustomStateRepresent.studentCustomState().id(), entity.getId());

        return rel != null;
    }
}
