package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.FilterUtils;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DevelopFormDSHandler extends DefaultComboDataSourceHandler {

    public static final String NEW_FORMATIVE_ORG_UNIT = "newFormativeOrgUnit";
    public static final String NEW_TERRITORIAL_ORG_UNIT = "newTerritorialOrgUnit";
    public static final String EDUCATION_LEVELS_HIGH_SCHOOL = "educationLevelsHighSchool";

    public DevelopFormDSHandler(String ownerId) {

        super(ownerId, DevelopForm.class, DevelopForm.P_TITLE);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        String filter = ep.input.getComboFilterByValue();
        if (StringUtils.isNotBlank(filter)) {
            ep.dqlBuilder.where(like(DQLFunctions.upper(property(DevelopForm.title().fromAlias("e"))), value(CoreStringUtils.escapeLike(filter, true))));
        }

        OrgUnit newFormativeOrgUnit = ep.context.get(NEW_FORMATIVE_ORG_UNIT);
        OrgUnit newTerritorialOrgUnit = ep.context.get(NEW_TERRITORIAL_ORG_UNIT);
        EducationLevelsHighSchool educationLevelsHighSchool = ep.context.get(EDUCATION_LEVELS_HIGH_SCHOOL);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "eou")
                .column(DQLExpressions.property(EducationOrgUnit.developForm().id().fromAlias("eou")));

        FilterUtils.applySelectFilter(builder, "eou", EducationOrgUnit.formativeOrgUnit(), newFormativeOrgUnit);
        FilterUtils.applySelectFilter(builder, "eou", EducationOrgUnit.territorialOrgUnit(), newTerritorialOrgUnit);
        FilterUtils.applySelectFilter(builder, "eou", EducationOrgUnit.educationLevelHighSchool(), educationLevelsHighSchool);

        ep.dqlBuilder.where(DQLExpressions.in(DQLExpressions.property(DevelopForm.id().fromAlias("e")), builder.getQuery()));

    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
    {
        ep.dqlBuilder.order("e." + DevelopForm.title());
    }
}
