package ru.tandemservice.movestudentrmc.base.bo.RelationSettingsOSSP.logic;

import org.tandemframework.hibsupport.dao.CommonDAO;
import ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonOSSP;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;

import java.util.List;

public class RelationSettingsOSSPManagerModifyDAO extends CommonDAO implements IRelationSettingsOSSPManagerModifyDAO {

    public void save(RepresentationType typeRepresentation, List<RepresentRelOSSP> reasons) {

        for (RepresentRelOSSP relOSSP : reasons) {

            RelRepresentationReasonOSSP ossp = new RelRepresentationReasonOSSP();
            ossp.setBudget(relOSSP.isBudget());
            ossp.setReason(relOSSP.getReason());
            ossp.setType(typeRepresentation);
            saveOSSP(relOSSP, ossp);
        }
    }

    private void saveOSSP(RepresentRelOSSP reasons, RelRepresentationReasonOSSP ossp) {

        boolean isAllSave = false;
        int c = 0;
        while (!isAllSave) {
            isAllSave = true;
            RelRepresentationReasonOSSP relOSSP = new RelRepresentationReasonOSSP();
            relOSSP.setBudget(ossp.isBudget());
            relOSSP.setReason(ossp.getReason());
            relOSSP.setType(ossp.getType());
            /*
            if (reasons.getGrantsList().size() >= c+1) {
                isAllSave = false;
                relOSSP.setGrantsOSSP(reasons.getGrantsList().get(c));
            }
            if (reasons.getPgoList().size() >= c+1) {
                isAllSave = false;
                relOSSP.setPgoOSSP(reasons.getPgoList().get(c));
            }
            */
            if (!isAllSave) {
                save(relOSSP);
            }
            c = c + 1;
        }
    }

    public void update(RepresentationType typeRepresentation, List<RepresentRelOSSP> reasons, List<RelRepresentationReasonOSSP> osspList) {

        getSession().createQuery("delete from " + RelRepresentationReasonOSSP.ENTITY_CLASS + " r" +
                                         " where r.type.id=:typeId").setLong("typeId", typeRepresentation.getId()).executeUpdate();

        save(typeRepresentation, reasons);
    }
}
