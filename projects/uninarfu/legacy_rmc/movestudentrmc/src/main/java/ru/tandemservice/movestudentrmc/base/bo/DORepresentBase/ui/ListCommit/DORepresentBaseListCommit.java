package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.ListCommit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.logic.TypeRepresentDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.RepresentationCommitDSHandler;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.DocOrdRepresent;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;

@Configuration
public class DORepresentBaseListCommit extends BusinessComponentManager {

    public static final String REPRESENTATION_COMMIT_DS = "representationCommitDS";
    private static final String TYPE_REPRESENT_DS = "typeRepresentDS";

    @Bean
    public ColumnListExtPoint representationCommitDS()
    {
        return columnListExtPointBuilder(REPRESENTATION_COMMIT_DS)
                .addColumn(publisherColumn(RepresentationCommitDSHandler.REPRESENT_COLUMN, DocOrdRepresent.representation().title().fromAlias(DocOrdRepresent.ENTITY_NAME)).order().create())
                .addColumn(textColumn(RepresentationCommitDSHandler.STUDENT_COLUMN, DocRepresentStudentBase.student().person().identityCard().fullFio().fromAlias(DocRepresentStudentBase.ENTITY_NAME)).visible("ui:isOrgUnit").order().create())
                .addColumn(textColumn(RepresentationCommitDSHandler.GROUP_COLUMN, DocRepresentStudentBase.student().group().title().fromAlias(DocRepresentStudentBase.ENTITY_NAME)).visible("ui:isOrgUnit").order().create())
                .addColumn(dateColumn(RepresentationCommitDSHandler.CREATE_DATE_COLUMN, DocOrdRepresent.representation().createDate().fromAlias(DocOrdRepresent.ENTITY_NAME)).order().create())
                .addColumn(dateColumn(RepresentationCommitDSHandler.COMMIT_DATE_COLUMN, DocOrdRepresent.order().commitDateSystem().fromAlias(DocOrdRepresent.ENTITY_NAME)).order().create())
                        //Скрыто RM#2587
                        //.addColumn(textColumn(RepresentationCommitDSHandler.NUMBER_COLUMN, DocOrdRepresent.representation().number().fromAlias(DocOrdRepresent.ENTITY_NAME)).create())
                .addColumn(publisherColumn(RepresentationCommitDSHandler.ORDER_COLUMN, DocOrdRepresent.order().fullTitle().fromAlias(DocOrdRepresent.ENTITY_NAME)).publisherLinkResolver(
                        new IPublisherLinkResolver() {
                            @Override
                            public Object getParameters(IEntity entity) {
                                DataWrapper e = (DataWrapper) entity;
                                DocOrdRepresent docOrdRepresent = ((DocOrdRepresent) e.getProperty(DocOrdRepresent.ENTITY_NAME));
                                return new UniMap().add(PublisherActivator.PUBLISHER_ID_KEY, docOrdRepresent.getOrder().getId());
                            }

                            @Override
                            public String getComponentName(IEntity entity) {
                                return null;
                            }
                        }
                ).order().create())
                .addColumn(booleanColumn(RepresentationCommitDSHandler.CHECK_COLUMN, DocOrdRepresent.representation().check()).order().create())
                .addColumn(actionColumn(UtilPrintSupport.PRINT_EXTRACT_COLUMN_NAME, CommonDefines.ICON_PRINT, UtilPrintSupport.PRINT_EXTRACT_LISTENER).displayHeader(true).permissionKey("rmc_print_extracts_commited_represent").create())
                .addColumn(actionColumn(UtilPrintSupport.PRINT_REPRESENT_COLUMN_NAME, CommonDefines.ICON_PRINT, UtilPrintSupport.PRINT_REPRESENT_LISTENER).permissionKey("rmc_print_commited_represent").displayHeader(true).create())
                .create();
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(REPRESENTATION_COMMIT_DS, representationCommitDS()).handler(representationCommitDSHandler()))
                .addDataSource(selectDS(TYPE_REPRESENT_DS, typeRepresentDSHandler()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> representationCommitDSHandler()
    {
        return new RepresentationCommitDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> typeRepresentDSHandler()
    {
        return new TypeRepresentDSHandler(getName());
    }
}