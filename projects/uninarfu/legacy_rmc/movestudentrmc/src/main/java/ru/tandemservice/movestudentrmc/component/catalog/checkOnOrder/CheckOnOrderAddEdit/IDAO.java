package ru.tandemservice.movestudentrmc.component.catalog.checkOnOrder.CheckOnOrderAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.movestudentrmc.entity.catalog.CheckOnOrder;

public interface IDAO extends IDefaultCatalogAddEditDAO<CheckOnOrder, Model> {

}
