package ru.tandemservice.movestudentrmc.base.bo.DORepresentEnrollmentTransfer.ui.Edit;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentEditUI;
import ru.tandemservice.movestudentrmc.docord.util.MoveStudentUtil;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.RepresentEnrollmentTransfer;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.*;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

import java.io.IOException;
import java.util.List;

public class DORepresentEnrollmentTransferEditUI extends AbstractDORepresentEditUI
{

    private EducationOrgUnit eduOrgUnit = new EducationOrgUnit();
    private boolean showAllGroups = false;

    @Override
    protected void FetchStudents(Representation doc) {

        _studentDocList.clear();
        _studentList.clear();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocRepresentStudentBase.class, "stud").column("stud");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(DocRepresentStudentBase.representation().fromAlias("stud")),
                DQLExpressions.value(doc.getId())));

        List<DocRepresentStudentBase> studentList = builder.createStatement(getSupport().getSession()).list();

        for (DocRepresentStudentBase student : studentList) {

            _studentDocList.add(student);
            _studentList.add(student.getStudent());
        }

        _student = _studentList.get(0);
    }

    @Override
    protected void initDocument() {
        super.initDocument();
        _studentStatusNewStr = ((StudentStatus) UniDaoFacade.getCoreDao().getCatalogItem(ru.tandemservice.uni.entity.catalog.StudentStatus.class, "1")).getTitle();

        if (getRepresentEnrollmentTransfer().getId() != null && getRepresentEnrollmentTransfer().getEducationOrgUnit() != null) {
            getEduOrgUnit().update(getRepresentEnrollmentTransfer().getEducationOrgUnit());
            setShowAllGroups(!getEduOrgUnit().getEducationLevelHighSchool().equals(getRepresentEnrollmentTransfer().getNewGroup().getEducationOrgUnit().getEducationLevelHighSchool()));
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);
        if (DORepresentEnrollmentTransferEdit.NEW_GROUP_DS.equals(dataSource.getName())) {
            dataSource.put(GroupDSHandler.SHOW_ALL_GROUPS, isShowAllGroups());
            dataSource.put(GroupDSHandler.NEW_FORMATIVE_ORG_UNIT, getEduOrgUnit().getFormativeOrgUnit());
            dataSource.put(GroupDSHandler.NEW_TERRITORIAL_ORG_UNIT, getEduOrgUnit().getTerritorialOrgUnit());
            dataSource.put(GroupDSHandler.EDUCATION_LEVELS_HIGH_SCHOOL, getEduOrgUnit().getEducationLevelHighSchool());
            dataSource.put(GroupDSHandler.DEVELOP_FORM, getEduOrgUnit().getDevelopForm());
            dataSource.put(GroupDSHandler.DEVELOP_CONDITION, getEduOrgUnit().getDevelopCondition());
            dataSource.put(GroupDSHandler.DEVELOP_PERIOD, getEduOrgUnit().getDevelopPeriod());
            dataSource.put(GroupDSHandler.DEVELOP_TECH, getEduOrgUnit().getDevelopTech());
            dataSource.put(GroupDSHandler.COURSE, getRepresentEnrollmentTransfer().getNewCourse());
        }

        if (DORepresentEnrollmentTransferEdit.EDU_LEVEL_DS.equals(dataSource.getName())) {
            dataSource.put(EducationLevelsHighSchoolDSHandler.FILTER_BY_DEVELOP, Boolean.FALSE);
            dataSource.put(EducationLevelsHighSchoolDSHandler.NEW_TERRITORIAL_ORG_UNIT, getEduOrgUnit().getTerritorialOrgUnit());
            dataSource.put(EducationLevelsHighSchoolDSHandler.NEW_FORMATIVE_ORG_UNIT, getEduOrgUnit().getFormativeOrgUnit());
        }

        if (DORepresentEnrollmentTransferEdit.DEVELOP_FORM_DS.equals(dataSource.getName())) {
            dataSource.put(DevelopFormDSHandler.NEW_FORMATIVE_ORG_UNIT, getEduOrgUnit().getFormativeOrgUnit());
            dataSource.put(DevelopFormDSHandler.NEW_TERRITORIAL_ORG_UNIT, getEduOrgUnit().getTerritorialOrgUnit());
            dataSource.put(DevelopFormDSHandler.EDUCATION_LEVELS_HIGH_SCHOOL, getEduOrgUnit().getEducationLevelHighSchool());
        }

        if (DORepresentEnrollmentTransferEdit.DEVELOP_CONDITION_DS.equals(dataSource.getName())) {
            dataSource.put(DevelopConditionDSHandler.NEW_FORMATIVE_ORG_UNIT, getEduOrgUnit().getFormativeOrgUnit());
            dataSource.put(DevelopConditionDSHandler.NEW_TERRITORIAL_ORG_UNIT, getEduOrgUnit().getTerritorialOrgUnit());
            dataSource.put(DevelopConditionDSHandler.EDUCATION_LEVELS_HIGH_SCHOOL, getEduOrgUnit().getEducationLevelHighSchool());
            dataSource.put(DevelopConditionDSHandler.DEVELOP_FORM, getEduOrgUnit().getDevelopForm());
        }

        if (DORepresentEnrollmentTransferEdit.DEVELOP_PERIOD_DS.equals(dataSource.getName())) {
            dataSource.put(DevelopPeriodDSHandler.NEW_FORMATIVE_ORG_UNIT, getEduOrgUnit().getFormativeOrgUnit());
            dataSource.put(DevelopPeriodDSHandler.NEW_TERRITORIAL_ORG_UNIT, getEduOrgUnit().getTerritorialOrgUnit());
            dataSource.put(DevelopPeriodDSHandler.EDUCATION_LEVELS_HIGH_SCHOOL, getEduOrgUnit().getEducationLevelHighSchool());
            dataSource.put(DevelopPeriodDSHandler.DEVELOP_FORM, getEduOrgUnit().getDevelopForm());
            dataSource.put(DevelopPeriodDSHandler.DEVELOP_CONDITION, getEduOrgUnit().getDevelopCondition());
        }

        if (DORepresentEnrollmentTransferEdit.DEVELOP_TECH_DS.equals(dataSource.getName())) {
            dataSource.put(DevelopTechDSHandler.NEW_FORMATIVE_ORG_UNIT, getEduOrgUnit().getFormativeOrgUnit());
            dataSource.put(DevelopTechDSHandler.NEW_TERRITORIAL_ORG_UNIT, getEduOrgUnit().getTerritorialOrgUnit());
            dataSource.put(DevelopTechDSHandler.EDUCATION_LEVELS_HIGH_SCHOOL, getEduOrgUnit().getEducationLevelHighSchool());
            dataSource.put(DevelopTechDSHandler.DEVELOP_FORM, getEduOrgUnit().getDevelopForm());
            dataSource.put(DevelopTechDSHandler.DEVELOP_CONDITION, getEduOrgUnit().getDevelopCondition());
            dataSource.put(DevelopTechDSHandler.DEVELOP_PERIOD, getEduOrgUnit().getDevelopPeriod());
        }
    }

    public void onChangeGroup() {
        if (getRepresentEnrollmentTransfer().getNewCourse() == null)
            getRepresentEnrollmentTransfer().setNewCourse(getRepresentEnrollmentTransfer().getNewGroup().getCourse());
    }

    @Override
    public void onClickSave() throws IOException {

        EducationOrgUnit educationOrgUnit = MoveStudentUtil.getEducationOrgUnit(getEduOrgUnit());
        if (educationOrgUnit == null)
            throw new ApplicationException("Подходящее направление подготовки (специальность) подразделения не найдено.");
        getRepresentEnrollmentTransfer().setEducationOrgUnit(educationOrgUnit);
        getRepresentEnrollmentTransfer().setNewFormativeOrgUnit(educationOrgUnit.getFormativeOrgUnit());
        getRepresentEnrollmentTransfer().setNewEducationLevelsHighSchool(educationOrgUnit.getEducationLevelHighSchool());
        getRepresentEnrollmentTransfer().setNewDevelopForm(educationOrgUnit.getDevelopForm());

        super.onClickSave();
    }

    public RepresentEnrollmentTransfer getRepresentEnrollmentTransfer() {
        return (RepresentEnrollmentTransfer) this.getRepresentObj();
    }

    public EducationOrgUnit getEduOrgUnit() {
        return eduOrgUnit;
    }

    public void setEduOrgUnit(EducationOrgUnit eduOrgUnit) {
        this.eduOrgUnit = eduOrgUnit;
    }

    public boolean isShowAllGroups() {
        return showAllGroups;
    }

    public void setShowAllGroups(boolean showAllGroups) {
        this.showAllGroups = showAllGroups;
    }

}
