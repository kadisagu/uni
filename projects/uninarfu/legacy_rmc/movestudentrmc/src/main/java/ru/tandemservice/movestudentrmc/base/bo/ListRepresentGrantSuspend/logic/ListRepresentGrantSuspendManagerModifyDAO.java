package ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantSuspend.logic;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.AbstractListRepresentDAO;
import ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab.MonthWrapper;
import ru.tandemservice.movestudentrmc.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudentrmc.docord.IListObjectModifyDAO;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.StuGrantStatus;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentGrantTypeCodes;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.StuGrantStatusCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class ListRepresentGrantSuspendManagerModifyDAO extends AbstractListRepresentDAO implements IListObjectModifyDAO
{

    @Override
    public boolean doCommit(ListRepresent represent, ErrorCollector error) {
        boolean ok = super.doCommit(represent, UserContext.getInstance().getErrorCollector());
        if (!ok)
            return false;

        ListRepresentGrantSuspend listRepresent = (ListRepresentGrantSuspend) represent;
        ListOrder order = getOrder(listRepresent);

        List<RelListRepresentStudents> list = getStudentList(listRepresent);
        List<Student> studentList = CommonBaseEntityUtil.getPropertiesList(list, RelListRepresentStudents.student());
        
        //-------------------------------------------------------------------------------------------------------------------------------
        /*
        Calendar lastYearDate = Calendar.getInstance();
        lastYearDate.set(listRepresent.getEducationYear().getIntValue() + 1, Calendar.AUGUST, 31, 0, 0, 0);//31.08.year+1
        List<String> monthList = MonthWrapper.getMonthsList(listRepresent.getDateSuspendPayment(), lastYearDate.getTime(), listRepresent.getEducationYear());
        */
        
        //Последний учебный год назначения выплат
        DQLSelectBuilder lastYearBuilder = new DQLSelectBuilder()
        		.fromEntity(StudentGrantEntity.class, "sge")
        		.where(eq(property(StudentGrantEntity.status().code().fromAlias("sge")), value(StuGrantStatusCodes.CODE_1)))
        		.where(in(property(StudentGrantEntity.student().fromAlias("sge")), studentList))
        		.where(eq(property(StudentGrantEntity.view().fromAlias("sge")), value(listRepresent.getGrantView())))
        		.column(property(StudentGrantEntity.eduYear().intValue().fromAlias("sge")))
        		.order(property(StudentGrantEntity.eduYear().intValue().fromAlias("sge")), OrderDirection.desc)
        		.top(1);

        int lastYear = lastYearBuilder.createStatement(getSession()).uniqueResult();
        
        //Последний день последнего учебного года назначения выплат
        Calendar lastYearDate = Calendar.getInstance();
        lastYearDate.set(lastYear + 1, Calendar.AUGUST, 31, 0, 0, 0);	//31.08.year+1
        List<String> monthList = MonthWrapper.getMonthsList(listRepresent.getDateSuspendPayment(), lastYearDate.getTime());        
        //-------------------------------------------------------------------------------------------------------------------------------

        MQBuilder builder = new MQBuilder(StudentGrantEntity.ENTITY_CLASS, "sge")
                .add(MQExpression.or(
                        MQExpression.isNull("sge", StudentGrantEntity.tmp()),
                        MQExpression.eq("sge", StudentGrantEntity.tmp(), Boolean.FALSE)
                ))
                .add(MQExpression.eq("sge", StudentGrantEntity.status().code(), StuGrantStatusCodes.CODE_1))
                .add(MQExpression.in("sge", StudentGrantEntity.student(), studentList))
                .add(MQExpression.eq("sge", StudentGrantEntity.view(), listRepresent.getGrantView()))
                .add(MQExpression.in("sge", StudentGrantEntity.month(), monthList));

        List<StudentGrantEntity> sgeList = builder.getResultList(getSession());
        StuGrantStatus status = UniDaoFacade.getCoreDao().getCatalogItem(StuGrantStatus.class, StuGrantStatusCodes.CODE_2);

        for (StudentGrantEntity sge : sgeList) {
            //сохраняем данные этой стипендии
            HistoryItem item = new HistoryItem(listRepresent, sge.getId());
            item.addProperty("orderDate", sge.getOrderDate() != null ? "" + sge.getOrderDate().getTime() : null);
            item.addProperty("orderNumber", sge.getOrderNumber());

            List<ListRepresentHistoryItem> histryItemList = item.create();
            for (ListRepresentHistoryItem historyItem : histryItemList)
                getSession().saveOrUpdate(historyItem);

            sge.setOrderDate(order.getCommitDate());
            sge.setOrderNumber(order.getNumber());
            sge.setStatus(status);
            getSession().saveOrUpdate(sge);
        }

        MoveStudentDaoFacade.getGrantDAO().saveGrantHistory(sgeList, represent, RepresentGrantTypeCodes.SUSPEND);

        return true;
    }

    @Override
    public boolean doRollback(ListRepresent represent, ErrorCollector error) {
        boolean ok = super.doRollback(represent, UserContext.getInstance().getErrorCollector());
        if (!ok)
            return false;

        StuGrantStatus status = UniDaoFacade.getCoreDao().getCatalogItem(StuGrantStatus.class, StuGrantStatusCodes.CODE_1);

        ListRepresentGrantSuspend listRepresent = (ListRepresentGrantSuspend) represent;
        Map<Long, HistoryItem> historyItemMap = getHistoryMap(listRepresent);

        List<StudentGrantEntity> sgeList = UniDaoFacade.getCoreDao().getList(StudentGrantEntity.class, CommonBaseEntityUtil.getIdList(historyItemMap.values()));
        for (StudentGrantEntity sge : sgeList) {
            HistoryItem historyItem = historyItemMap.get(sge.getId());

            Date orderDate = historyItem.getPropertyValue("orderDate") != null ? new Date(Long.parseLong(historyItem.getPropertyValue("orderDate"))) : null;
            String orderNumber = historyItem.getPropertyValue("orderNumber");

            sge.setOrderDate(orderDate);
            sge.setOrderNumber(orderNumber);
            sge.setStatus(status);

            getSession().saveOrUpdate(sge);
        }

        deleteHistory(listRepresent);

        MoveStudentDaoFacade.getGrantDAO().deleteGrantHistory(listRepresent);

        return true;
    }


    @Override
    public void buildBodyExtract(ListOrdListRepresent listOrdListRepresent,
                                 Student student, RtfDocument docExtract, Map<Student, OrderParagraphInfo> map)
    {
        super.buildBodyExtract(listOrdListRepresent, student, docExtract, map);

        ListRepresentGrantSuspend listRepresent = (ListRepresentGrantSuspend) listOrdListRepresent.getRepresentation();
        RtfInjectModifier im = new RtfInjectModifier();

        im.put("date", UtilPrintSupport.getDateFormatterWithMonthString(listRepresent.getDateSuspendPayment()));
        im.put("grantView", listRepresent.getGrantView().getGenitive());

        im.modify(docExtract);
    }

    @Override
    public Map<Student, OrderParagraphInfo> buildBodyRepresent(List<ListRepresent> representationBase,
                                                               RtfDocument document)
    {
        ListPrintDoc listPrintDoc = new ListPrintDoc();
        RtfDocument templateRepresent = listPrintDoc.creationPrintDocOrder(representationBase);
        document.setHeader(templateRepresent.getHeader());
        document.setSettings(templateRepresent.getSettings());
        document.getElementList().addAll(templateRepresent.getElementList());
        return listPrintDoc.getParagInfomap();
    }

    @Override
    public List<Student> selectRepresent(ListRepresent listRepresent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void saveSupport(ListRepresent listRepresent,
                            List<DataWrapper> studentSelectedList,
                            DocListRepresentBasics representBasics)
    {
        // TODO Auto-generated method stub

    }

}
