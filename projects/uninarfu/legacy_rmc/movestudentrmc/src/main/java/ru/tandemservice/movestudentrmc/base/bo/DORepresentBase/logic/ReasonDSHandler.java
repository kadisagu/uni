package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonBasic;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class ReasonDSHandler extends DefaultComboDataSourceHandler {

    public static final String REASON_DS = "reasonDS";
    public static final String TYPE_REASON = "typeReason";

    public ReasonDSHandler(String ownerId) {
        super(ownerId, RepresentationReason.class, RepresentationReason.P_TITLE);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        String filter = ep.input.getComboFilterByValue();
        RepresentationType type = ep.context.get(TYPE_REASON);

        ep.dqlBuilder.joinEntity("e", DQLJoinType.inner, RelRepresentationReasonBasic.class, "r",
                                 eq(property(RelRepresentationReasonBasic.reason().id().fromAlias("r")),
                                    property(RepresentationReason.id().fromAlias("e"))));

        ep.dqlBuilder.where(eq(property(RelRepresentationReasonBasic.type().fromAlias("r")), value(type)));

        if (StringUtils.isNotBlank(filter)) {
            ep.dqlBuilder.where(like(DQLFunctions.upper(property(RepresentationReason.title().fromAlias("e"))), value(CoreStringUtils.escapeLike(filter, true))));
        }


    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
    {
        ep.dqlBuilder.order("e." + RepresentationReason.title());
    }
}
