package ru.tandemservice.movestudentrmc.base.bo.RelationSettings.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.base.bo.RelationSettings.DORelationSettingsManager;
import ru.tandemservice.movestudentrmc.base.bo.RelationSettings.logic.FakeReasonsHandler;
import ru.tandemservice.movestudentrmc.base.bo.RelationSettings.ui.EditRel.RelationSettingsEditRel;
import ru.tandemservice.movestudentrmc.base.bo.RelationSettings.ui.EditRel.RelationSettingsEditRelUI;
import ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonBasic;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationBasement;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;

import java.util.ArrayList;
import java.util.List;

@Input(
        @Bind(key = RelationSettingsAddEditUI.TYPE_REPRESENTATION_ID, binding = RelationSettingsAddEditUI.TYPE_REPRESENTATION_ID)
)
public class RelationSettingsAddEditUI extends UIPresenter {

    public static final String TYPE_REPRESENTATION_ID = "typeRepresentationId";

    private Long typeRepresentationId;
    private RepresentationType typeRepresentation;
    private List<FakeReasonsHandler> reasons;
    private List<RepresentationReason> reasonsList;
    private FakeReasonsHandler reasonRow;
    private String pageView;
    private int pageNum = 0;
    private int pageEnd = 1;
    private int countReasons = 0;
    private boolean firstRun = true;
    private boolean isAddForm = true;
    private boolean hasConfiguration = false;
    private List<RelRepresentationReasonBasic> listReasonBasic;

    @Override
    public void onComponentRefresh()
    {

        setPageView(getClass().getPackage().getName() + ".page" + (getPageNum()));

        if (!firstRun) {

            return;
        }

        if (null != typeRepresentationId)
            typeRepresentation = DataAccessServices.dao().getNotNull(RepresentationType.class, typeRepresentationId);
        else {
            typeRepresentation = new RepresentationType();
        }

        if (typeRepresentation.isConfigRelation()) {

            setPageNum(getPageNum() + 1);
            setPageView(getClass().getPackage().getName() + ".page" + (getPageNum()));
            isAddForm = false;
            hasConfiguration = true;
            fillArrayReasons();
        }
        else {
            setPageView(getClass().getPackage().getName() + ".page" + (getPageNum()));
            isAddForm = false;
            hasConfiguration = false;
            fillArrayReasons();

        }

        if (firstRun)
            firstRun = !firstRun;
    }

    private void fillArrayReasons() {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RelRepresentationReasonBasic.class, "rel").addColumn("rel");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(RelRepresentationReasonBasic.type().id().fromAlias("rel")),
                DQLExpressions.value(typeRepresentationId)));

        listReasonBasic = builder.createStatement(getSupport().getSession()).list();

        if (getReasonsList() == null)
            setReasonsList(new ArrayList<RepresentationReason>());
        else
            getReasonsList().clear();

        if (getReasons() == null)
            setReasons(new ArrayList<FakeReasonsHandler>());
        else
            getReasons().clear();

        for (RelRepresentationReasonBasic reasonBasic : listReasonBasic) {

            Boolean isFind = false;
            for (FakeReasonsHandler fakeReason : getReasons()) {

                if (fakeReason.getReason().equals(reasonBasic.getReason())) {
                    isFind = true;
                    fakeReason.getBasicsList().add(reasonBasic.getBasic());
                    break;
                }
            }

            if (!isFind) {

                FakeReasonsHandler fakeReason = new FakeReasonsHandler();
                fakeReason.setReason(reasonBasic.getReason());
                fakeReason.setEdit(true);
                fakeReason.setBasicsList(new ArrayList<RepresentationBasement>());
                fakeReason.getBasicsList().add(reasonBasic.getBasic());
                getReasons().add(fakeReason);
                if (!getReasonsList().contains(reasonBasic.getReason()))
                    getReasonsList().add(reasonBasic.getReason());
            }
        }
    }

    public void onClickEditRel() {

        _uiActivation.asRegion(RelationSettingsEditRel.class)
                .parameter(RelationSettingsEditRelUI.REASON_ID, getListenerParameterAsLong())
                .parameter(RelationSettingsEditRelUI.TYPE_REPRESENTATION_ID, getTypeRepresentationId())
                .activate();
    }

    public void onClickApply()
    {
        if (isAddForm) {
            DORelationSettingsManager.instance().modifyDao().saveTypeRepresentation(typeRepresentation, getReasons());
            isAddForm = false;
        }
        else
            DORelationSettingsManager.instance().modifyDao().updateTypeRepresentation(typeRepresentation, getReasons(), listReasonBasic);

        fillArrayReasons();
        //deactivate();
    }

    public void onClickPrev() {

        setPageNum(getPageNum() - 1);
        setPageView(getClass().getPackage().getName() + ".page" + (getPageNum()));
    }

    public void onClickNext() {

        if (pageNum == 0) {
            mergeArraysReasons();
        }

        setPageNum(getPageNum() + 1);
        setPageView(getClass().getPackage().getName() + ".page" + (getPageNum()));
    }

    @Override
    public void onComponentPrepareRender() {

        super.onComponentPrepareRender();
    }

    public void mergeArraysReasons() {

        List<FakeReasonsHandler> isFound = new ArrayList<FakeReasonsHandler>();

        if (getReasons() == null)
            setReasons(new ArrayList<FakeReasonsHandler>());
        for (RepresentationReason reason : getReasonsList()) {
            Boolean isFind = false;
            for (FakeReasonsHandler fakeReason : getReasons()) {
                if (fakeReason.getReason().equals(reason)) {
                    isFound.add(fakeReason);
                    isFind = true;
                    break;
                }
            }

            if (!isFind) {
                FakeReasonsHandler fakeReason = new FakeReasonsHandler();
                fakeReason.setReason(reason);
                isFound.add(fakeReason);
            }
        }

        getReasons().clear();
        getReasons().addAll(isFound);
    }

//    Getters and Setters

    public List<RepresentationReason> getReasonsList() {
        return reasonsList;
    }

    public void setReasonsList(List<RepresentationReason> reasonsList) {
        this.reasonsList = reasonsList;
    }

    public List<FakeReasonsHandler> getReasons() {
        return reasons;
    }

    public int getCountReasons() {
        this.countReasons = reasons.size() + 1;
        return countReasons;
    }

    public void setReasons(List<FakeReasonsHandler> reasons) {
        this.reasons = reasons;
    }

    public FakeReasonsHandler getReasonRow() {
        return reasonRow;
    }

    public void setReasonRow(FakeReasonsHandler reasonRow) {
        this.reasonRow = reasonRow;
    }

    public String getPageView() {
        return pageView;
    }

    public void setPageView(String pageView) {
        this.pageView = pageView;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageEnd() {
        return pageEnd;
    }

    public void setPageEnd(int pageEnd) {
        this.pageEnd = pageEnd;
    }

    public Long getTypeRepresentationId() {
        return typeRepresentationId;
    }

    public void setTypeRepresentationId(Long typeRepresentationId) {
        this.typeRepresentationId = typeRepresentationId;
    }

    public RepresentationType getTypeRepresentation() {
        return typeRepresentation;
    }

    public void setTypeRepresentation(RepresentationType typeRepresentation) {
        this.typeRepresentation = typeRepresentation;
    }
}
