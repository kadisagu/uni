package ru.tandemservice.movestudentrmc.base.bo.DORepresentExclude;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentExclude.logic.DORepresentExcludeDAO;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentExclude.ui.Edit.DORepresentExcludeEdit;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentExclude.ui.View.DORepresentExcludeView;
import ru.tandemservice.movestudentrmc.docord.IDOObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IDOObjectModifyDAO;

@Configuration
public class DORepresentExcludeManager extends BusinessObjectManager implements IDOObjectByRepresentTypeManager
{

    public static DORepresentExcludeManager instance()
    {
        return instance(DORepresentExcludeManager.class);
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return DORepresentExcludeEdit.class;
    }

    @Bean
    public IDOObjectModifyDAO getDOObjectModifyDAO() {
        return new DORepresentExcludeDAO();
    }

    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return DORepresentExcludeView.class;
    }
}
