/* $Id$ */
package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ui.MassInnerAdvisorEdit;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.movestudentrmc.entity.StudentPracticeData;
import ru.tandemservice.uni.util.FilterUtils;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Andrey Andreev
 * @since 19.10.2015
 */

@State({@Bind(key = ListRepresentBaseMassInnerAdvisorEditUI.RECORDS, binding = ListRepresentBaseMassInnerAdvisorEditUI.RECORDS)})
public class ListRepresentBaseMassInnerAdvisorEditUI extends UIPresenter
{
    public final static String RECORDS = "records";

    private Collection<DataWrapper> _records;

    private Employee _innerAdvisor;
    private ISelectModel _innerAdvisorModel;

    @Override
    public void onComponentPrepareRender()
    {
        _innerAdvisorModel = new DQLFullCheckSelectModel(Employee.P_FULL_FIO)
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(Employee.class, alias);
                FilterUtils.applySimpleLikeFilter(dql, alias, Employee.person().identityCard().fullFio().s(), filter);
                return dql;
            }
        };
    }

    public void onClickSave()
    {
        for (DataWrapper dataWrapper : _records) {
            StudentPracticeData practiceData = (StudentPracticeData) dataWrapper.getProperty("practiceData");
            practiceData.setInnerAdvisor(_innerAdvisor);
            dataWrapper.setProperty("practiceData", practiceData);
        }
        
        //------------------------------------------------------------------------------------------------------
        //deactivate();
        
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("clearCheckedStudents", true);
        deactivate(params);
        //------------------------------------------------------------------------------------------------------
    }


    public Collection<DataWrapper> getRecords()
    {
        return _records;
    }

    public void setRecords(Collection<DataWrapper> records)
    {
        this._records = records;
    }

    public Employee getInnerAdvisor()
    {
        return _innerAdvisor;
    }

    public void setInnerAdvisor(Employee innerAdvisor)
    {
        this._innerAdvisor = innerAdvisor;
    }

    public ISelectModel getInnerAdvisorModel()
    {
        return _innerAdvisorModel;
    }

    public void setInnerAdvisorModel(ISelectModel innerAdvisorModel)
    {
        this._innerAdvisorModel = innerAdvisorModel;
    }
}