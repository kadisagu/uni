package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.IRepresent2Student;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.uni.entity.employee.Student;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Базовая Сущность-связь 'Представление'-'Студент'
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DocRepresentStudentBaseGen extends EntityBase
 implements IRepresent2Student{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase";
    public static final String ENTITY_NAME = "docRepresentStudentBase";
    public static final int VERSION_HASH = -656004339;
    private static IEntityMeta ENTITY_META;

    public static final String L_REPRESENTATION = "representation";
    public static final String L_STUDENT = "student";
    public static final String P_STUDENT_TITLE_STR = "studentTitleStr";
    public static final String P_STUDENT_TITLE_GENETIVE_STR = "studentTitleGenetiveStr";
    public static final String P_STUDENT_STATUS_STR = "studentStatusStr";
    public static final String P_PERSONAL_NUMBER_STR = "personalNumberStr";
    public static final String P_COURSE_STR = "courseStr";
    public static final String P_GROUP_STR = "groupStr";
    public static final String P_COMPENSATION_TYPE_STR = "compensationTypeStr";
    public static final String P_FORMATIVE_ORG_UNIT_STR = "formativeOrgUnitStr";
    public static final String P_HEAD_TITLE_STR = "headTitleStr";
    public static final String P_FORMATIVE_ORG_UNIT_GENETIVE_STR = "formativeOrgUnitGenetiveStr";
    public static final String P_TERRITORIAL_ORG_UNIT_STR = "territorialOrgUnitStr";
    public static final String P_EDUCATION_LEVEL_HIGH_SCHOOL_STR = "educationLevelHighSchoolStr";
    public static final String P_EDUCATION_LEVEL_HIGH_SCHOOL_TYPE_STR = "educationLevelHighSchoolTypeStr";
    public static final String P_DEVELOP_FORM_STR = "developFormStr";
    public static final String P_DEVELOP_CONDITION_STR = "developConditionStr";
    public static final String P_DEVELOP_TECH_STR = "developTechStr";
    public static final String P_DEVELOP_PERIOD_STR = "developPeriodStr";
    public static final String P_FINISHED_YEAR = "finishedYear";

    private Representation _representation;     // Представление
    private Student _student;     // Студент
    private String _studentTitleStr;     // ФИО студента в именительном падеже
    private String _studentTitleGenetiveStr;     // ФИО студента в родительном падеже
    private String _studentStatusStr;     // Состояние
    private String _personalNumberStr;     // Личный номер
    private String _courseStr;     // Курс
    private String _groupStr;     // Группа
    private String _compensationTypeStr;     // Вид возмещения затрат
    private String _formativeOrgUnitStr;     // Формирующее подр.
    private String _headTitleStr;     // ФИО директора формирующего подр.
    private String _formativeOrgUnitGenetiveStr;     // Формирующее подр. в родительном падеже
    private String _territorialOrgUnitStr;     // Территориальное подр.
    private String _educationLevelHighSchoolStr;     // Направление подготовки (специальность)
    private String _educationLevelHighSchoolTypeStr;     // Тип: Направление подготовки или специальность
    private String _developFormStr;     // Форма освоения
    private String _developConditionStr;     // Условие освоения
    private String _developTechStr;     // Технология освоения
    private String _developPeriodStr;     // Срок освоения
    private Integer _finishedYear;     // Год выпуска

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Представление. Свойство не может быть null.
     */
    @NotNull
    public Representation getRepresentation()
    {
        return _representation;
    }

    /**
     * @param representation Представление. Свойство не может быть null.
     */
    public void setRepresentation(Representation representation)
    {
        dirty(_representation, representation);
        _representation = representation;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return ФИО студента в именительном падеже. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getStudentTitleStr()
    {
        return _studentTitleStr;
    }

    /**
     * @param studentTitleStr ФИО студента в именительном падеже. Свойство не может быть null.
     */
    public void setStudentTitleStr(String studentTitleStr)
    {
        dirty(_studentTitleStr, studentTitleStr);
        _studentTitleStr = studentTitleStr;
    }

    /**
     * @return ФИО студента в родительном падеже. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getStudentTitleGenetiveStr()
    {
        return _studentTitleGenetiveStr;
    }

    /**
     * @param studentTitleGenetiveStr ФИО студента в родительном падеже. Свойство не может быть null.
     */
    public void setStudentTitleGenetiveStr(String studentTitleGenetiveStr)
    {
        dirty(_studentTitleGenetiveStr, studentTitleGenetiveStr);
        _studentTitleGenetiveStr = studentTitleGenetiveStr;
    }

    /**
     * @return Состояние. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getStudentStatusStr()
    {
        return _studentStatusStr;
    }

    /**
     * @param studentStatusStr Состояние. Свойство не может быть null.
     */
    public void setStudentStatusStr(String studentStatusStr)
    {
        dirty(_studentStatusStr, studentStatusStr);
        _studentStatusStr = studentStatusStr;
    }

    /**
     * @return Личный номер. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPersonalNumberStr()
    {
        return _personalNumberStr;
    }

    /**
     * @param personalNumberStr Личный номер. Свойство не может быть null.
     */
    public void setPersonalNumberStr(String personalNumberStr)
    {
        dirty(_personalNumberStr, personalNumberStr);
        _personalNumberStr = personalNumberStr;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getCourseStr()
    {
        return _courseStr;
    }

    /**
     * @param courseStr Курс. Свойство не может быть null.
     */
    public void setCourseStr(String courseStr)
    {
        dirty(_courseStr, courseStr);
        _courseStr = courseStr;
    }

    /**
     * @return Группа.
     */
    @Length(max=255)
    public String getGroupStr()
    {
        return _groupStr;
    }

    /**
     * @param groupStr Группа.
     */
    public void setGroupStr(String groupStr)
    {
        dirty(_groupStr, groupStr);
        _groupStr = groupStr;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getCompensationTypeStr()
    {
        return _compensationTypeStr;
    }

    /**
     * @param compensationTypeStr Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationTypeStr(String compensationTypeStr)
    {
        dirty(_compensationTypeStr, compensationTypeStr);
        _compensationTypeStr = compensationTypeStr;
    }

    /**
     * @return Формирующее подр.. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFormativeOrgUnitStr()
    {
        return _formativeOrgUnitStr;
    }

    /**
     * @param formativeOrgUnitStr Формирующее подр.. Свойство не может быть null.
     */
    public void setFormativeOrgUnitStr(String formativeOrgUnitStr)
    {
        dirty(_formativeOrgUnitStr, formativeOrgUnitStr);
        _formativeOrgUnitStr = formativeOrgUnitStr;
    }

    /**
     * @return ФИО директора формирующего подр.. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getHeadTitleStr()
    {
        return _headTitleStr;
    }

    /**
     * @param headTitleStr ФИО директора формирующего подр.. Свойство не может быть null.
     */
    public void setHeadTitleStr(String headTitleStr)
    {
        dirty(_headTitleStr, headTitleStr);
        _headTitleStr = headTitleStr;
    }

    /**
     * @return Формирующее подр. в родительном падеже. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFormativeOrgUnitGenetiveStr()
    {
        return _formativeOrgUnitGenetiveStr;
    }

    /**
     * @param formativeOrgUnitGenetiveStr Формирующее подр. в родительном падеже. Свойство не может быть null.
     */
    public void setFormativeOrgUnitGenetiveStr(String formativeOrgUnitGenetiveStr)
    {
        dirty(_formativeOrgUnitGenetiveStr, formativeOrgUnitGenetiveStr);
        _formativeOrgUnitGenetiveStr = formativeOrgUnitGenetiveStr;
    }

    /**
     * @return Территориальное подр.. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTerritorialOrgUnitStr()
    {
        return _territorialOrgUnitStr;
    }

    /**
     * @param territorialOrgUnitStr Территориальное подр.. Свойство не может быть null.
     */
    public void setTerritorialOrgUnitStr(String territorialOrgUnitStr)
    {
        dirty(_territorialOrgUnitStr, territorialOrgUnitStr);
        _territorialOrgUnitStr = territorialOrgUnitStr;
    }

    /**
     * @return Направление подготовки (специальность). Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEducationLevelHighSchoolStr()
    {
        return _educationLevelHighSchoolStr;
    }

    /**
     * @param educationLevelHighSchoolStr Направление подготовки (специальность). Свойство не может быть null.
     */
    public void setEducationLevelHighSchoolStr(String educationLevelHighSchoolStr)
    {
        dirty(_educationLevelHighSchoolStr, educationLevelHighSchoolStr);
        _educationLevelHighSchoolStr = educationLevelHighSchoolStr;
    }

    /**
     * @return Тип: Направление подготовки или специальность. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEducationLevelHighSchoolTypeStr()
    {
        return _educationLevelHighSchoolTypeStr;
    }

    /**
     * @param educationLevelHighSchoolTypeStr Тип: Направление подготовки или специальность. Свойство не может быть null.
     */
    public void setEducationLevelHighSchoolTypeStr(String educationLevelHighSchoolTypeStr)
    {
        dirty(_educationLevelHighSchoolTypeStr, educationLevelHighSchoolTypeStr);
        _educationLevelHighSchoolTypeStr = educationLevelHighSchoolTypeStr;
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getDevelopFormStr()
    {
        return _developFormStr;
    }

    /**
     * @param developFormStr Форма освоения. Свойство не может быть null.
     */
    public void setDevelopFormStr(String developFormStr)
    {
        dirty(_developFormStr, developFormStr);
        _developFormStr = developFormStr;
    }

    /**
     * @return Условие освоения. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getDevelopConditionStr()
    {
        return _developConditionStr;
    }

    /**
     * @param developConditionStr Условие освоения. Свойство не может быть null.
     */
    public void setDevelopConditionStr(String developConditionStr)
    {
        dirty(_developConditionStr, developConditionStr);
        _developConditionStr = developConditionStr;
    }

    /**
     * @return Технология освоения. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getDevelopTechStr()
    {
        return _developTechStr;
    }

    /**
     * @param developTechStr Технология освоения. Свойство не может быть null.
     */
    public void setDevelopTechStr(String developTechStr)
    {
        dirty(_developTechStr, developTechStr);
        _developTechStr = developTechStr;
    }

    /**
     * @return Срок освоения. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getDevelopPeriodStr()
    {
        return _developPeriodStr;
    }

    /**
     * @param developPeriodStr Срок освоения. Свойство не может быть null.
     */
    public void setDevelopPeriodStr(String developPeriodStr)
    {
        dirty(_developPeriodStr, developPeriodStr);
        _developPeriodStr = developPeriodStr;
    }

    /**
     * @return Год выпуска.
     */
    public Integer getFinishedYear()
    {
        return _finishedYear;
    }

    /**
     * @param finishedYear Год выпуска.
     */
    public void setFinishedYear(Integer finishedYear)
    {
        dirty(_finishedYear, finishedYear);
        _finishedYear = finishedYear;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DocRepresentStudentBaseGen)
        {
            setRepresentation(((DocRepresentStudentBase)another).getRepresentation());
            setStudent(((DocRepresentStudentBase)another).getStudent());
            setStudentTitleStr(((DocRepresentStudentBase)another).getStudentTitleStr());
            setStudentTitleGenetiveStr(((DocRepresentStudentBase)another).getStudentTitleGenetiveStr());
            setStudentStatusStr(((DocRepresentStudentBase)another).getStudentStatusStr());
            setPersonalNumberStr(((DocRepresentStudentBase)another).getPersonalNumberStr());
            setCourseStr(((DocRepresentStudentBase)another).getCourseStr());
            setGroupStr(((DocRepresentStudentBase)another).getGroupStr());
            setCompensationTypeStr(((DocRepresentStudentBase)another).getCompensationTypeStr());
            setFormativeOrgUnitStr(((DocRepresentStudentBase)another).getFormativeOrgUnitStr());
            setHeadTitleStr(((DocRepresentStudentBase)another).getHeadTitleStr());
            setFormativeOrgUnitGenetiveStr(((DocRepresentStudentBase)another).getFormativeOrgUnitGenetiveStr());
            setTerritorialOrgUnitStr(((DocRepresentStudentBase)another).getTerritorialOrgUnitStr());
            setEducationLevelHighSchoolStr(((DocRepresentStudentBase)another).getEducationLevelHighSchoolStr());
            setEducationLevelHighSchoolTypeStr(((DocRepresentStudentBase)another).getEducationLevelHighSchoolTypeStr());
            setDevelopFormStr(((DocRepresentStudentBase)another).getDevelopFormStr());
            setDevelopConditionStr(((DocRepresentStudentBase)another).getDevelopConditionStr());
            setDevelopTechStr(((DocRepresentStudentBase)another).getDevelopTechStr());
            setDevelopPeriodStr(((DocRepresentStudentBase)another).getDevelopPeriodStr());
            setFinishedYear(((DocRepresentStudentBase)another).getFinishedYear());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DocRepresentStudentBaseGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DocRepresentStudentBase.class;
        }

        public T newInstance()
        {
            return (T) new DocRepresentStudentBase();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "representation":
                    return obj.getRepresentation();
                case "student":
                    return obj.getStudent();
                case "studentTitleStr":
                    return obj.getStudentTitleStr();
                case "studentTitleGenetiveStr":
                    return obj.getStudentTitleGenetiveStr();
                case "studentStatusStr":
                    return obj.getStudentStatusStr();
                case "personalNumberStr":
                    return obj.getPersonalNumberStr();
                case "courseStr":
                    return obj.getCourseStr();
                case "groupStr":
                    return obj.getGroupStr();
                case "compensationTypeStr":
                    return obj.getCompensationTypeStr();
                case "formativeOrgUnitStr":
                    return obj.getFormativeOrgUnitStr();
                case "headTitleStr":
                    return obj.getHeadTitleStr();
                case "formativeOrgUnitGenetiveStr":
                    return obj.getFormativeOrgUnitGenetiveStr();
                case "territorialOrgUnitStr":
                    return obj.getTerritorialOrgUnitStr();
                case "educationLevelHighSchoolStr":
                    return obj.getEducationLevelHighSchoolStr();
                case "educationLevelHighSchoolTypeStr":
                    return obj.getEducationLevelHighSchoolTypeStr();
                case "developFormStr":
                    return obj.getDevelopFormStr();
                case "developConditionStr":
                    return obj.getDevelopConditionStr();
                case "developTechStr":
                    return obj.getDevelopTechStr();
                case "developPeriodStr":
                    return obj.getDevelopPeriodStr();
                case "finishedYear":
                    return obj.getFinishedYear();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "representation":
                    obj.setRepresentation((Representation) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "studentTitleStr":
                    obj.setStudentTitleStr((String) value);
                    return;
                case "studentTitleGenetiveStr":
                    obj.setStudentTitleGenetiveStr((String) value);
                    return;
                case "studentStatusStr":
                    obj.setStudentStatusStr((String) value);
                    return;
                case "personalNumberStr":
                    obj.setPersonalNumberStr((String) value);
                    return;
                case "courseStr":
                    obj.setCourseStr((String) value);
                    return;
                case "groupStr":
                    obj.setGroupStr((String) value);
                    return;
                case "compensationTypeStr":
                    obj.setCompensationTypeStr((String) value);
                    return;
                case "formativeOrgUnitStr":
                    obj.setFormativeOrgUnitStr((String) value);
                    return;
                case "headTitleStr":
                    obj.setHeadTitleStr((String) value);
                    return;
                case "formativeOrgUnitGenetiveStr":
                    obj.setFormativeOrgUnitGenetiveStr((String) value);
                    return;
                case "territorialOrgUnitStr":
                    obj.setTerritorialOrgUnitStr((String) value);
                    return;
                case "educationLevelHighSchoolStr":
                    obj.setEducationLevelHighSchoolStr((String) value);
                    return;
                case "educationLevelHighSchoolTypeStr":
                    obj.setEducationLevelHighSchoolTypeStr((String) value);
                    return;
                case "developFormStr":
                    obj.setDevelopFormStr((String) value);
                    return;
                case "developConditionStr":
                    obj.setDevelopConditionStr((String) value);
                    return;
                case "developTechStr":
                    obj.setDevelopTechStr((String) value);
                    return;
                case "developPeriodStr":
                    obj.setDevelopPeriodStr((String) value);
                    return;
                case "finishedYear":
                    obj.setFinishedYear((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "representation":
                        return true;
                case "student":
                        return true;
                case "studentTitleStr":
                        return true;
                case "studentTitleGenetiveStr":
                        return true;
                case "studentStatusStr":
                        return true;
                case "personalNumberStr":
                        return true;
                case "courseStr":
                        return true;
                case "groupStr":
                        return true;
                case "compensationTypeStr":
                        return true;
                case "formativeOrgUnitStr":
                        return true;
                case "headTitleStr":
                        return true;
                case "formativeOrgUnitGenetiveStr":
                        return true;
                case "territorialOrgUnitStr":
                        return true;
                case "educationLevelHighSchoolStr":
                        return true;
                case "educationLevelHighSchoolTypeStr":
                        return true;
                case "developFormStr":
                        return true;
                case "developConditionStr":
                        return true;
                case "developTechStr":
                        return true;
                case "developPeriodStr":
                        return true;
                case "finishedYear":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "representation":
                    return true;
                case "student":
                    return true;
                case "studentTitleStr":
                    return true;
                case "studentTitleGenetiveStr":
                    return true;
                case "studentStatusStr":
                    return true;
                case "personalNumberStr":
                    return true;
                case "courseStr":
                    return true;
                case "groupStr":
                    return true;
                case "compensationTypeStr":
                    return true;
                case "formativeOrgUnitStr":
                    return true;
                case "headTitleStr":
                    return true;
                case "formativeOrgUnitGenetiveStr":
                    return true;
                case "territorialOrgUnitStr":
                    return true;
                case "educationLevelHighSchoolStr":
                    return true;
                case "educationLevelHighSchoolTypeStr":
                    return true;
                case "developFormStr":
                    return true;
                case "developConditionStr":
                    return true;
                case "developTechStr":
                    return true;
                case "developPeriodStr":
                    return true;
                case "finishedYear":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "representation":
                    return Representation.class;
                case "student":
                    return Student.class;
                case "studentTitleStr":
                    return String.class;
                case "studentTitleGenetiveStr":
                    return String.class;
                case "studentStatusStr":
                    return String.class;
                case "personalNumberStr":
                    return String.class;
                case "courseStr":
                    return String.class;
                case "groupStr":
                    return String.class;
                case "compensationTypeStr":
                    return String.class;
                case "formativeOrgUnitStr":
                    return String.class;
                case "headTitleStr":
                    return String.class;
                case "formativeOrgUnitGenetiveStr":
                    return String.class;
                case "territorialOrgUnitStr":
                    return String.class;
                case "educationLevelHighSchoolStr":
                    return String.class;
                case "educationLevelHighSchoolTypeStr":
                    return String.class;
                case "developFormStr":
                    return String.class;
                case "developConditionStr":
                    return String.class;
                case "developTechStr":
                    return String.class;
                case "developPeriodStr":
                    return String.class;
                case "finishedYear":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DocRepresentStudentBase> _dslPath = new Path<DocRepresentStudentBase>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DocRepresentStudentBase");
    }
            

    /**
     * @return Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getRepresentation()
     */
    public static Representation.Path<Representation> representation()
    {
        return _dslPath.representation();
    }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return ФИО студента в именительном падеже. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getStudentTitleStr()
     */
    public static PropertyPath<String> studentTitleStr()
    {
        return _dslPath.studentTitleStr();
    }

    /**
     * @return ФИО студента в родительном падеже. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getStudentTitleGenetiveStr()
     */
    public static PropertyPath<String> studentTitleGenetiveStr()
    {
        return _dslPath.studentTitleGenetiveStr();
    }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getStudentStatusStr()
     */
    public static PropertyPath<String> studentStatusStr()
    {
        return _dslPath.studentStatusStr();
    }

    /**
     * @return Личный номер. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getPersonalNumberStr()
     */
    public static PropertyPath<String> personalNumberStr()
    {
        return _dslPath.personalNumberStr();
    }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getCourseStr()
     */
    public static PropertyPath<String> courseStr()
    {
        return _dslPath.courseStr();
    }

    /**
     * @return Группа.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getGroupStr()
     */
    public static PropertyPath<String> groupStr()
    {
        return _dslPath.groupStr();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getCompensationTypeStr()
     */
    public static PropertyPath<String> compensationTypeStr()
    {
        return _dslPath.compensationTypeStr();
    }

    /**
     * @return Формирующее подр.. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getFormativeOrgUnitStr()
     */
    public static PropertyPath<String> formativeOrgUnitStr()
    {
        return _dslPath.formativeOrgUnitStr();
    }

    /**
     * @return ФИО директора формирующего подр.. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getHeadTitleStr()
     */
    public static PropertyPath<String> headTitleStr()
    {
        return _dslPath.headTitleStr();
    }

    /**
     * @return Формирующее подр. в родительном падеже. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getFormativeOrgUnitGenetiveStr()
     */
    public static PropertyPath<String> formativeOrgUnitGenetiveStr()
    {
        return _dslPath.formativeOrgUnitGenetiveStr();
    }

    /**
     * @return Территориальное подр.. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getTerritorialOrgUnitStr()
     */
    public static PropertyPath<String> territorialOrgUnitStr()
    {
        return _dslPath.territorialOrgUnitStr();
    }

    /**
     * @return Направление подготовки (специальность). Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getEducationLevelHighSchoolStr()
     */
    public static PropertyPath<String> educationLevelHighSchoolStr()
    {
        return _dslPath.educationLevelHighSchoolStr();
    }

    /**
     * @return Тип: Направление подготовки или специальность. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getEducationLevelHighSchoolTypeStr()
     */
    public static PropertyPath<String> educationLevelHighSchoolTypeStr()
    {
        return _dslPath.educationLevelHighSchoolTypeStr();
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getDevelopFormStr()
     */
    public static PropertyPath<String> developFormStr()
    {
        return _dslPath.developFormStr();
    }

    /**
     * @return Условие освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getDevelopConditionStr()
     */
    public static PropertyPath<String> developConditionStr()
    {
        return _dslPath.developConditionStr();
    }

    /**
     * @return Технология освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getDevelopTechStr()
     */
    public static PropertyPath<String> developTechStr()
    {
        return _dslPath.developTechStr();
    }

    /**
     * @return Срок освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getDevelopPeriodStr()
     */
    public static PropertyPath<String> developPeriodStr()
    {
        return _dslPath.developPeriodStr();
    }

    /**
     * @return Год выпуска.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getFinishedYear()
     */
    public static PropertyPath<Integer> finishedYear()
    {
        return _dslPath.finishedYear();
    }

    public static class Path<E extends DocRepresentStudentBase> extends EntityPath<E>
    {
        private Representation.Path<Representation> _representation;
        private Student.Path<Student> _student;
        private PropertyPath<String> _studentTitleStr;
        private PropertyPath<String> _studentTitleGenetiveStr;
        private PropertyPath<String> _studentStatusStr;
        private PropertyPath<String> _personalNumberStr;
        private PropertyPath<String> _courseStr;
        private PropertyPath<String> _groupStr;
        private PropertyPath<String> _compensationTypeStr;
        private PropertyPath<String> _formativeOrgUnitStr;
        private PropertyPath<String> _headTitleStr;
        private PropertyPath<String> _formativeOrgUnitGenetiveStr;
        private PropertyPath<String> _territorialOrgUnitStr;
        private PropertyPath<String> _educationLevelHighSchoolStr;
        private PropertyPath<String> _educationLevelHighSchoolTypeStr;
        private PropertyPath<String> _developFormStr;
        private PropertyPath<String> _developConditionStr;
        private PropertyPath<String> _developTechStr;
        private PropertyPath<String> _developPeriodStr;
        private PropertyPath<Integer> _finishedYear;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getRepresentation()
     */
        public Representation.Path<Representation> representation()
        {
            if(_representation == null )
                _representation = new Representation.Path<Representation>(L_REPRESENTATION, this);
            return _representation;
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return ФИО студента в именительном падеже. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getStudentTitleStr()
     */
        public PropertyPath<String> studentTitleStr()
        {
            if(_studentTitleStr == null )
                _studentTitleStr = new PropertyPath<String>(DocRepresentStudentBaseGen.P_STUDENT_TITLE_STR, this);
            return _studentTitleStr;
        }

    /**
     * @return ФИО студента в родительном падеже. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getStudentTitleGenetiveStr()
     */
        public PropertyPath<String> studentTitleGenetiveStr()
        {
            if(_studentTitleGenetiveStr == null )
                _studentTitleGenetiveStr = new PropertyPath<String>(DocRepresentStudentBaseGen.P_STUDENT_TITLE_GENETIVE_STR, this);
            return _studentTitleGenetiveStr;
        }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getStudentStatusStr()
     */
        public PropertyPath<String> studentStatusStr()
        {
            if(_studentStatusStr == null )
                _studentStatusStr = new PropertyPath<String>(DocRepresentStudentBaseGen.P_STUDENT_STATUS_STR, this);
            return _studentStatusStr;
        }

    /**
     * @return Личный номер. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getPersonalNumberStr()
     */
        public PropertyPath<String> personalNumberStr()
        {
            if(_personalNumberStr == null )
                _personalNumberStr = new PropertyPath<String>(DocRepresentStudentBaseGen.P_PERSONAL_NUMBER_STR, this);
            return _personalNumberStr;
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getCourseStr()
     */
        public PropertyPath<String> courseStr()
        {
            if(_courseStr == null )
                _courseStr = new PropertyPath<String>(DocRepresentStudentBaseGen.P_COURSE_STR, this);
            return _courseStr;
        }

    /**
     * @return Группа.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getGroupStr()
     */
        public PropertyPath<String> groupStr()
        {
            if(_groupStr == null )
                _groupStr = new PropertyPath<String>(DocRepresentStudentBaseGen.P_GROUP_STR, this);
            return _groupStr;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getCompensationTypeStr()
     */
        public PropertyPath<String> compensationTypeStr()
        {
            if(_compensationTypeStr == null )
                _compensationTypeStr = new PropertyPath<String>(DocRepresentStudentBaseGen.P_COMPENSATION_TYPE_STR, this);
            return _compensationTypeStr;
        }

    /**
     * @return Формирующее подр.. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getFormativeOrgUnitStr()
     */
        public PropertyPath<String> formativeOrgUnitStr()
        {
            if(_formativeOrgUnitStr == null )
                _formativeOrgUnitStr = new PropertyPath<String>(DocRepresentStudentBaseGen.P_FORMATIVE_ORG_UNIT_STR, this);
            return _formativeOrgUnitStr;
        }

    /**
     * @return ФИО директора формирующего подр.. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getHeadTitleStr()
     */
        public PropertyPath<String> headTitleStr()
        {
            if(_headTitleStr == null )
                _headTitleStr = new PropertyPath<String>(DocRepresentStudentBaseGen.P_HEAD_TITLE_STR, this);
            return _headTitleStr;
        }

    /**
     * @return Формирующее подр. в родительном падеже. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getFormativeOrgUnitGenetiveStr()
     */
        public PropertyPath<String> formativeOrgUnitGenetiveStr()
        {
            if(_formativeOrgUnitGenetiveStr == null )
                _formativeOrgUnitGenetiveStr = new PropertyPath<String>(DocRepresentStudentBaseGen.P_FORMATIVE_ORG_UNIT_GENETIVE_STR, this);
            return _formativeOrgUnitGenetiveStr;
        }

    /**
     * @return Территориальное подр.. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getTerritorialOrgUnitStr()
     */
        public PropertyPath<String> territorialOrgUnitStr()
        {
            if(_territorialOrgUnitStr == null )
                _territorialOrgUnitStr = new PropertyPath<String>(DocRepresentStudentBaseGen.P_TERRITORIAL_ORG_UNIT_STR, this);
            return _territorialOrgUnitStr;
        }

    /**
     * @return Направление подготовки (специальность). Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getEducationLevelHighSchoolStr()
     */
        public PropertyPath<String> educationLevelHighSchoolStr()
        {
            if(_educationLevelHighSchoolStr == null )
                _educationLevelHighSchoolStr = new PropertyPath<String>(DocRepresentStudentBaseGen.P_EDUCATION_LEVEL_HIGH_SCHOOL_STR, this);
            return _educationLevelHighSchoolStr;
        }

    /**
     * @return Тип: Направление подготовки или специальность. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getEducationLevelHighSchoolTypeStr()
     */
        public PropertyPath<String> educationLevelHighSchoolTypeStr()
        {
            if(_educationLevelHighSchoolTypeStr == null )
                _educationLevelHighSchoolTypeStr = new PropertyPath<String>(DocRepresentStudentBaseGen.P_EDUCATION_LEVEL_HIGH_SCHOOL_TYPE_STR, this);
            return _educationLevelHighSchoolTypeStr;
        }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getDevelopFormStr()
     */
        public PropertyPath<String> developFormStr()
        {
            if(_developFormStr == null )
                _developFormStr = new PropertyPath<String>(DocRepresentStudentBaseGen.P_DEVELOP_FORM_STR, this);
            return _developFormStr;
        }

    /**
     * @return Условие освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getDevelopConditionStr()
     */
        public PropertyPath<String> developConditionStr()
        {
            if(_developConditionStr == null )
                _developConditionStr = new PropertyPath<String>(DocRepresentStudentBaseGen.P_DEVELOP_CONDITION_STR, this);
            return _developConditionStr;
        }

    /**
     * @return Технология освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getDevelopTechStr()
     */
        public PropertyPath<String> developTechStr()
        {
            if(_developTechStr == null )
                _developTechStr = new PropertyPath<String>(DocRepresentStudentBaseGen.P_DEVELOP_TECH_STR, this);
            return _developTechStr;
        }

    /**
     * @return Срок освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getDevelopPeriodStr()
     */
        public PropertyPath<String> developPeriodStr()
        {
            if(_developPeriodStr == null )
                _developPeriodStr = new PropertyPath<String>(DocRepresentStudentBaseGen.P_DEVELOP_PERIOD_STR, this);
            return _developPeriodStr;
        }

    /**
     * @return Год выпуска.
     * @see ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase#getFinishedYear()
     */
        public PropertyPath<Integer> finishedYear()
        {
            if(_finishedYear == null )
                _finishedYear = new PropertyPath<Integer>(DocRepresentStudentBaseGen.P_FINISHED_YEAR, this);
            return _finishedYear;
        }

        public Class getEntityClass()
        {
            return DocRepresentStudentBase.class;
        }

        public String getEntityName()
        {
            return "docRepresentStudentBase";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
