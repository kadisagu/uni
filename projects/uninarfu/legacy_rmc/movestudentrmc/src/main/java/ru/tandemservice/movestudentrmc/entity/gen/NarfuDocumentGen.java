package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.movestudentrmc.entity.NarfuDocument;
import ru.tandemservice.movestudentrmc.entity.RelDocumentTypeKind;
import ru.tandemservice.uni.entity.employee.Student;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность документ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class NarfuDocumentGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.NarfuDocument";
    public static final String ENTITY_NAME = "narfuDocument";
    public static final int VERSION_HASH = -777673100;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT = "student";
    public static final String L_SETTINGS = "settings";
    public static final String P_NAME = "name";
    public static final String P_ADDITIONAL_NAME = "additionalName";
    public static final String P_SERIA = "seria";
    public static final String P_NUMBER = "number";
    public static final String P_ISSUANCE_DATE = "issuanceDate";
    public static final String P_PERIOD_DATE_START = "periodDateStart";
    public static final String P_PERIOD_DATE_END = "periodDateEnd";
    public static final String L_FILE = "file";
    public static final String P_TMP = "tmp";

    private Student _student;     // Студент
    private RelDocumentTypeKind _settings;     // Настройка документа
    private String _name;     // Название документа
    private String _additionalName;     // Дополнительное поле наименования
    private String _seria;     // Серия
    private String _number;     // Номер
    private Date _issuanceDate;     // Дата выдачи
    private Date _periodDateStart;     // Действует с
    private Date _periodDateEnd;     // Действует по
    private DatabaseFile _file;     // Файл документа
    private Boolean _tmp;     // Временный(скрытый)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Настройка документа. Свойство не может быть null.
     */
    @NotNull
    public RelDocumentTypeKind getSettings()
    {
        return _settings;
    }

    /**
     * @param settings Настройка документа. Свойство не может быть null.
     */
    public void setSettings(RelDocumentTypeKind settings)
    {
        dirty(_settings, settings);
        _settings = settings;
    }

    /**
     * @return Название документа.
     */
    @Length(max=255)
    public String getName()
    {
        return _name;
    }

    /**
     * @param name Название документа.
     */
    public void setName(String name)
    {
        dirty(_name, name);
        _name = name;
    }

    /**
     * @return Дополнительное поле наименования.
     */
    @Length(max=255)
    public String getAdditionalName()
    {
        return _additionalName;
    }

    /**
     * @param additionalName Дополнительное поле наименования.
     */
    public void setAdditionalName(String additionalName)
    {
        dirty(_additionalName, additionalName);
        _additionalName = additionalName;
    }

    /**
     * @return Серия.
     */
    @Length(max=255)
    public String getSeria()
    {
        return _seria;
    }

    /**
     * @param seria Серия.
     */
    public void setSeria(String seria)
    {
        dirty(_seria, seria);
        _seria = seria;
    }

    /**
     * @return Номер.
     */
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Дата выдачи.
     */
    public Date getIssuanceDate()
    {
        return _issuanceDate;
    }

    /**
     * @param issuanceDate Дата выдачи.
     */
    public void setIssuanceDate(Date issuanceDate)
    {
        dirty(_issuanceDate, issuanceDate);
        _issuanceDate = issuanceDate;
    }

    /**
     * @return Действует с.
     */
    public Date getPeriodDateStart()
    {
        return _periodDateStart;
    }

    /**
     * @param periodDateStart Действует с.
     */
    public void setPeriodDateStart(Date periodDateStart)
    {
        dirty(_periodDateStart, periodDateStart);
        _periodDateStart = periodDateStart;
    }

    /**
     * @return Действует по.
     */
    public Date getPeriodDateEnd()
    {
        return _periodDateEnd;
    }

    /**
     * @param periodDateEnd Действует по.
     */
    public void setPeriodDateEnd(Date periodDateEnd)
    {
        dirty(_periodDateEnd, periodDateEnd);
        _periodDateEnd = periodDateEnd;
    }

    /**
     * @return Файл документа.
     */
    public DatabaseFile getFile()
    {
        return _file;
    }

    /**
     * @param file Файл документа.
     */
    public void setFile(DatabaseFile file)
    {
        dirty(_file, file);
        _file = file;
    }

    /**
     * @return Временный(скрытый).
     */
    public Boolean getTmp()
    {
        return _tmp;
    }

    /**
     * @param tmp Временный(скрытый).
     */
    public void setTmp(Boolean tmp)
    {
        dirty(_tmp, tmp);
        _tmp = tmp;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof NarfuDocumentGen)
        {
            setStudent(((NarfuDocument)another).getStudent());
            setSettings(((NarfuDocument)another).getSettings());
            setName(((NarfuDocument)another).getName());
            setAdditionalName(((NarfuDocument)another).getAdditionalName());
            setSeria(((NarfuDocument)another).getSeria());
            setNumber(((NarfuDocument)another).getNumber());
            setIssuanceDate(((NarfuDocument)another).getIssuanceDate());
            setPeriodDateStart(((NarfuDocument)another).getPeriodDateStart());
            setPeriodDateEnd(((NarfuDocument)another).getPeriodDateEnd());
            setFile(((NarfuDocument)another).getFile());
            setTmp(((NarfuDocument)another).getTmp());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends NarfuDocumentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) NarfuDocument.class;
        }

        public T newInstance()
        {
            return (T) new NarfuDocument();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "student":
                    return obj.getStudent();
                case "settings":
                    return obj.getSettings();
                case "name":
                    return obj.getName();
                case "additionalName":
                    return obj.getAdditionalName();
                case "seria":
                    return obj.getSeria();
                case "number":
                    return obj.getNumber();
                case "issuanceDate":
                    return obj.getIssuanceDate();
                case "periodDateStart":
                    return obj.getPeriodDateStart();
                case "periodDateEnd":
                    return obj.getPeriodDateEnd();
                case "file":
                    return obj.getFile();
                case "tmp":
                    return obj.getTmp();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "settings":
                    obj.setSettings((RelDocumentTypeKind) value);
                    return;
                case "name":
                    obj.setName((String) value);
                    return;
                case "additionalName":
                    obj.setAdditionalName((String) value);
                    return;
                case "seria":
                    obj.setSeria((String) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "issuanceDate":
                    obj.setIssuanceDate((Date) value);
                    return;
                case "periodDateStart":
                    obj.setPeriodDateStart((Date) value);
                    return;
                case "periodDateEnd":
                    obj.setPeriodDateEnd((Date) value);
                    return;
                case "file":
                    obj.setFile((DatabaseFile) value);
                    return;
                case "tmp":
                    obj.setTmp((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "student":
                        return true;
                case "settings":
                        return true;
                case "name":
                        return true;
                case "additionalName":
                        return true;
                case "seria":
                        return true;
                case "number":
                        return true;
                case "issuanceDate":
                        return true;
                case "periodDateStart":
                        return true;
                case "periodDateEnd":
                        return true;
                case "file":
                        return true;
                case "tmp":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "student":
                    return true;
                case "settings":
                    return true;
                case "name":
                    return true;
                case "additionalName":
                    return true;
                case "seria":
                    return true;
                case "number":
                    return true;
                case "issuanceDate":
                    return true;
                case "periodDateStart":
                    return true;
                case "periodDateEnd":
                    return true;
                case "file":
                    return true;
                case "tmp":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "student":
                    return Student.class;
                case "settings":
                    return RelDocumentTypeKind.class;
                case "name":
                    return String.class;
                case "additionalName":
                    return String.class;
                case "seria":
                    return String.class;
                case "number":
                    return String.class;
                case "issuanceDate":
                    return Date.class;
                case "periodDateStart":
                    return Date.class;
                case "periodDateEnd":
                    return Date.class;
                case "file":
                    return DatabaseFile.class;
                case "tmp":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<NarfuDocument> _dslPath = new Path<NarfuDocument>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "NarfuDocument");
    }
            

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.NarfuDocument#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Настройка документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.NarfuDocument#getSettings()
     */
    public static RelDocumentTypeKind.Path<RelDocumentTypeKind> settings()
    {
        return _dslPath.settings();
    }

    /**
     * @return Название документа.
     * @see ru.tandemservice.movestudentrmc.entity.NarfuDocument#getName()
     */
    public static PropertyPath<String> name()
    {
        return _dslPath.name();
    }

    /**
     * @return Дополнительное поле наименования.
     * @see ru.tandemservice.movestudentrmc.entity.NarfuDocument#getAdditionalName()
     */
    public static PropertyPath<String> additionalName()
    {
        return _dslPath.additionalName();
    }

    /**
     * @return Серия.
     * @see ru.tandemservice.movestudentrmc.entity.NarfuDocument#getSeria()
     */
    public static PropertyPath<String> seria()
    {
        return _dslPath.seria();
    }

    /**
     * @return Номер.
     * @see ru.tandemservice.movestudentrmc.entity.NarfuDocument#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Дата выдачи.
     * @see ru.tandemservice.movestudentrmc.entity.NarfuDocument#getIssuanceDate()
     */
    public static PropertyPath<Date> issuanceDate()
    {
        return _dslPath.issuanceDate();
    }

    /**
     * @return Действует с.
     * @see ru.tandemservice.movestudentrmc.entity.NarfuDocument#getPeriodDateStart()
     */
    public static PropertyPath<Date> periodDateStart()
    {
        return _dslPath.periodDateStart();
    }

    /**
     * @return Действует по.
     * @see ru.tandemservice.movestudentrmc.entity.NarfuDocument#getPeriodDateEnd()
     */
    public static PropertyPath<Date> periodDateEnd()
    {
        return _dslPath.periodDateEnd();
    }

    /**
     * @return Файл документа.
     * @see ru.tandemservice.movestudentrmc.entity.NarfuDocument#getFile()
     */
    public static DatabaseFile.Path<DatabaseFile> file()
    {
        return _dslPath.file();
    }

    /**
     * @return Временный(скрытый).
     * @see ru.tandemservice.movestudentrmc.entity.NarfuDocument#getTmp()
     */
    public static PropertyPath<Boolean> tmp()
    {
        return _dslPath.tmp();
    }

    public static class Path<E extends NarfuDocument> extends EntityPath<E>
    {
        private Student.Path<Student> _student;
        private RelDocumentTypeKind.Path<RelDocumentTypeKind> _settings;
        private PropertyPath<String> _name;
        private PropertyPath<String> _additionalName;
        private PropertyPath<String> _seria;
        private PropertyPath<String> _number;
        private PropertyPath<Date> _issuanceDate;
        private PropertyPath<Date> _periodDateStart;
        private PropertyPath<Date> _periodDateEnd;
        private DatabaseFile.Path<DatabaseFile> _file;
        private PropertyPath<Boolean> _tmp;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.NarfuDocument#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Настройка документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.NarfuDocument#getSettings()
     */
        public RelDocumentTypeKind.Path<RelDocumentTypeKind> settings()
        {
            if(_settings == null )
                _settings = new RelDocumentTypeKind.Path<RelDocumentTypeKind>(L_SETTINGS, this);
            return _settings;
        }

    /**
     * @return Название документа.
     * @see ru.tandemservice.movestudentrmc.entity.NarfuDocument#getName()
     */
        public PropertyPath<String> name()
        {
            if(_name == null )
                _name = new PropertyPath<String>(NarfuDocumentGen.P_NAME, this);
            return _name;
        }

    /**
     * @return Дополнительное поле наименования.
     * @see ru.tandemservice.movestudentrmc.entity.NarfuDocument#getAdditionalName()
     */
        public PropertyPath<String> additionalName()
        {
            if(_additionalName == null )
                _additionalName = new PropertyPath<String>(NarfuDocumentGen.P_ADDITIONAL_NAME, this);
            return _additionalName;
        }

    /**
     * @return Серия.
     * @see ru.tandemservice.movestudentrmc.entity.NarfuDocument#getSeria()
     */
        public PropertyPath<String> seria()
        {
            if(_seria == null )
                _seria = new PropertyPath<String>(NarfuDocumentGen.P_SERIA, this);
            return _seria;
        }

    /**
     * @return Номер.
     * @see ru.tandemservice.movestudentrmc.entity.NarfuDocument#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(NarfuDocumentGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Дата выдачи.
     * @see ru.tandemservice.movestudentrmc.entity.NarfuDocument#getIssuanceDate()
     */
        public PropertyPath<Date> issuanceDate()
        {
            if(_issuanceDate == null )
                _issuanceDate = new PropertyPath<Date>(NarfuDocumentGen.P_ISSUANCE_DATE, this);
            return _issuanceDate;
        }

    /**
     * @return Действует с.
     * @see ru.tandemservice.movestudentrmc.entity.NarfuDocument#getPeriodDateStart()
     */
        public PropertyPath<Date> periodDateStart()
        {
            if(_periodDateStart == null )
                _periodDateStart = new PropertyPath<Date>(NarfuDocumentGen.P_PERIOD_DATE_START, this);
            return _periodDateStart;
        }

    /**
     * @return Действует по.
     * @see ru.tandemservice.movestudentrmc.entity.NarfuDocument#getPeriodDateEnd()
     */
        public PropertyPath<Date> periodDateEnd()
        {
            if(_periodDateEnd == null )
                _periodDateEnd = new PropertyPath<Date>(NarfuDocumentGen.P_PERIOD_DATE_END, this);
            return _periodDateEnd;
        }

    /**
     * @return Файл документа.
     * @see ru.tandemservice.movestudentrmc.entity.NarfuDocument#getFile()
     */
        public DatabaseFile.Path<DatabaseFile> file()
        {
            if(_file == null )
                _file = new DatabaseFile.Path<DatabaseFile>(L_FILE, this);
            return _file;
        }

    /**
     * @return Временный(скрытый).
     * @see ru.tandemservice.movestudentrmc.entity.NarfuDocument#getTmp()
     */
        public PropertyPath<Boolean> tmp()
        {
            if(_tmp == null )
                _tmp = new PropertyPath<Boolean>(NarfuDocumentGen.P_TMP, this);
            return _tmp;
        }

        public Class getEntityClass()
        {
            return NarfuDocument.class;
        }

        public String getEntityName()
        {
            return "narfuDocument";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
