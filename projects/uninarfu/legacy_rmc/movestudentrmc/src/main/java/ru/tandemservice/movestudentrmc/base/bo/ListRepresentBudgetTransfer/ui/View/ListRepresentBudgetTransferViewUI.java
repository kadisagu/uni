package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBudgetTransfer.ui.View;

import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.support.AbstractListRepresentViewUI;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBudgetTransfer.logic.ListPrintDoc;
import ru.tandemservice.movestudentrmc.entity.ListRepresentBudgetTransfer;

import java.util.Collections;

public class ListRepresentBudgetTransferViewUI extends AbstractListRepresentViewUI<ListRepresentBudgetTransfer> {

    @Override
    public ListRepresentBudgetTransfer getListRepresentObject() {
        return new ListRepresentBudgetTransfer();
    }

    @Override
    protected RtfDocument printRepresent() {
        return new ListPrintDoc().creationPrintDocOrder(Collections.singletonList(getListRepresent()));
    }

}
