package ru.tandemservice.movestudentrmc.dao.check;

import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisession.entity.mark.SessionMark;

public class CheckAbsenceAcademicalDebts extends AbstractCheckSessionMark {

    @Override
    protected boolean checkCondition(Student student) {

        if (!marksMap.containsKey(student.getId()))
            return false;

        Long totalDisciplines = totalDisciplineMap.get(student.getId()) != null ? totalDisciplineMap.get(student.getId()) : 0L;
        Long totalDisciplinesWithMarks = new Long(marksMap.get(student.getId()).size());

        for (SessionMark sessionMark : marksMap.get(student.getId())) {
            if (!sessionMark.getValueItem().isCachedPositiveStatus() || !totalDisciplines.equals(totalDisciplinesWithMarks))
                return false;
        }

        return true;
    }

    @Override
    protected String createError(Student student, boolean value) {
        StringBuilder sb = new StringBuilder()
                .append("Студент ")
                .append(student.getPerson().getFullFio())
                .append(" ")
                .append(value ? "не " : "")
                .append("имеет только положительные итоговые оценки за последнюю сессию");
        return sb.toString();
    }

    @Override
    protected void prepareParam() {
        this.inSession = false;
    }

}
