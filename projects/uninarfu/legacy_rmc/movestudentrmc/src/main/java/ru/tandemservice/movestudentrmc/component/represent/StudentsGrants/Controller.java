package ru.tandemservice.movestudentrmc.component.represent.StudentsGrants;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntity;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.*;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    public Controller()
    {
    }

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        getDao().prepare(getModel(component));
        prepareListDateSource(component);
    }

    public void onClickApply(IBusinessComponent component) {
        ((IDAO) getDao()).update((Model) getModel(component));
        deactivate(component);
    }

    @SuppressWarnings("unchecked")
    private void prepareListDateSource(IBusinessComponent component) {
        final Model model = component.getModel();
        if (model.getDataSource() != null)
            return;


        DynamicListDataSource<DataWrapper> dataSource = new DynamicListDataSource<>(component, arg0 -> {
            ((IDAO) getDao()).prepareListDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("ФИО студ.", "student." + Student.person().identityCard().fullFio().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", "student." + Student.educationOrgUnit().territorialOrgUnit().shortTitle().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", "student." + Student.educationOrgUnit().formativeOrgUnit().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Напр. подг. (спец.)", "student." + Student.educationOrgUnit().educationLevelHighSchool().displayableTitle().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Выпускающее подр.", "student." + Student.educationOrgUnit().educationLevelHighSchool().orgUnit().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Статус", "student." + Student.status().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Курс", "student." + Student.course().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Группа", "student." + Student.group().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Форма обучения", "student." + Student.educationOrgUnit().developForm().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Форма возм. затрат", "student." + Student.compensationType().shortTitle().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Вид диплома", "student." + Student.educationOrgUnit().educationLevelHighSchool().assignedQualification().title().s()).setClickable(false));

        for (Iterator i = prepareMarkColumns(model).iterator(); i.hasNext(); ) {
            AbstractColumn c = (AbstractColumn) i.next();
            dataSource.addColumn(c);
        }
        //dataSource.addColumn(new BlockColumn("Стипендия", "grant." + GrantEntity.sum().s()).setClickable(false));

        Map<Long, String> grantMap = new HashMap<>();

        for (DataWrapper dataW : model.getList()) {
            Student student = (Student) dataW.getProperty("student");
            StudentGrantEntity grantEntity = (StudentGrantEntity) dataW.getProperty("grant");
            if (grantEntity != null) {
                grantMap.put(student.getId(), String.valueOf(grantEntity.getSum()));
            }
            else {
                grantMap.put(student.getId(), "");
            }
        }

        ((BlockColumn) dataSource.getColumn("grant")).setValueMap(grantMap);

        model.setDataSource(dataSource);
    }

    protected Collection<AbstractColumn> prepareMarkColumns(Model model) {
        return Collections.singletonList(new BlockColumn("grant", "Сумма выплаты").setOrderable(false).setWidth("150"));
    }
}
