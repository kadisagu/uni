package ru.tandemservice.movestudentrmc.base.bo.DORepresentWeekendProlong;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentWeekendProlong.logic.DORepresentWeekendProlongDAO;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentWeekendProlong.ui.Edit.DORepresentWeekendProlongEdit;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentWeekendProlong.ui.View.DORepresentWeekendProlongView;
import ru.tandemservice.movestudentrmc.docord.IDOObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IDOObjectModifyDAO;

@Configuration
public class DORepresentWeekendProlongManager extends BusinessObjectManager implements IDOObjectByRepresentTypeManager {

    public static DORepresentWeekendProlongManager instance()
    {
        return instance(DORepresentWeekendProlongManager.class);
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return DORepresentWeekendProlongEdit.class;
    }

    @Bean
    public IDOObjectModifyDAO getDOObjectModifyDAO() {
        return new DORepresentWeekendProlongDAO();
    }

    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return DORepresentWeekendProlongView.class;
    }
}
