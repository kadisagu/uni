package ru.tandemservice.movestudentrmc.base.bo.DORepresentStudentTicket;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentStudentTicket.logic.DORepresentStudentTicketDAO;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentStudentTicket.ui.Edit.DORepresentStudentTicketEdit;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentStudentTicket.ui.View.DORepresentStudentTicketView;
import ru.tandemservice.movestudentrmc.docord.IDOObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IDOObjectModifyDAO;

@Configuration
public class DORepresentStudentTicketManager extends BusinessObjectManager implements IDOObjectByRepresentTypeManager
{

    public static DORepresentStudentTicketManager instance()
    {
        return instance(DORepresentStudentTicketManager.class);
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return DORepresentStudentTicketEdit.class;
    }

    @Bean
    public IDOObjectModifyDAO getDOObjectModifyDAO() {
        return new DORepresentStudentTicketDAO();
    }

    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return DORepresentStudentTicketView.class;
    }
}
