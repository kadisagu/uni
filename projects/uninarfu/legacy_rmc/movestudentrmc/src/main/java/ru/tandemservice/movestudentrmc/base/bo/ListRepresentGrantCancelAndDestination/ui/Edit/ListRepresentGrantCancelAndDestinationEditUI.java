package ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantCancelAndDestination.ui.Edit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.GrantViewDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.StudentDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.support.AbstractListRepresentEditUI;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantCancelAndDestination.ListRepresentGrantCancelAndDestinationManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantCancelAndDestination.logic.IListRepresentGrantCancelAndDestinationManagerModifyDAO;
import ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab.MonthWrapper;
import ru.tandemservice.movestudentrmc.dao.CheckStateUtil;
import ru.tandemservice.movestudentrmc.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.Grant;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentExtractStatesCodes;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentationTypeCodes;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.StuGrantStatusCodes;
import ru.tandemservice.movestudentrmc.entity.gen.IRepresentOrderGen;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.ReportRenderer;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import static org.tandemframework.hibsupport.dql.DQLExpressions.and;
import static org.tandemframework.hibsupport.dql.DQLExpressions.between;
import static org.tandemframework.hibsupport.dql.DQLExpressions.gt;
import static org.tandemframework.hibsupport.dql.DQLExpressions.lt;
import static org.tandemframework.hibsupport.dql.DQLExpressions.or;
import static org.tandemframework.hibsupport.dql.DQLExpressions.plus;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;
import static org.tandemframework.hibsupport.dql.DQLExpressions.valueDate;
import static org.tandemframework.hibsupport.dql.DQLFunctions.createTimestamp;

import java.util.*;

public class ListRepresentGrantCancelAndDestinationEditUI extends AbstractListRepresentEditUI<ListRepresentGrantCancelAndDestination> {

    private List<DocRepresentOrderCancel> ordersCancel = new ArrayList<DocRepresentOrderCancel>();

    @Override
    public ListRepresentGrantCancelAndDestination getListRepresentObject() {
        return new ListRepresentGrantCancelAndDestination();
    }

    @Override
    public void checkSelectStudent(List<Student> selectList) {

    }

    @Override
    public void onComponentPrepareRender() {
        Boolean isSuccessfullyHandOverSession = (Boolean) (_uiSettings.get("isSuccessfullyHandOverSession") == null ? false : _uiSettings.get("isSuccessfullyHandOverSession"));

        Boolean isConsiderSessionResults = (Boolean) (_uiSettings.get("isConsiderSessionResults") == null ? false : _uiSettings.get("isConsiderSessionResults"));

        if (!isSuccessfullyHandOverSession) {
            _uiSettings.set("yearDistPartFilter", null);
            _uiSettings.set("educationYearFilter", null);
        }

        if (!isConsiderSessionResults) {
            _uiSettings.set("sessionMarksResult", null);

            _uiSettings.set(StudentDSHandler.YEAR_DIST_SESSION_RESULT, null);
            _uiSettings.set(StudentDSHandler.YEAR_EDU_SESSION_RESULT, null);
        }
    }

    @Override
    public void onComponentRefresh() {
        super.onComponentRefresh();

        if (getListRepresentId() == null) {
            getListRepresent().setBeginGrantDate(new Date());
            //-------------------------------------------------------
            //EducationYear educationYear = IUniBaseDao.instance.get().get(EducationYear.class, EducationYear.current(), Boolean.TRUE);
            //getListRepresent().setEducationYear(educationYear);
            //-------------------------------------------------------
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        super.onBeforeDataSourceFetch(dataSource);

        if (ListRepresentGrantCancelAndDestinationEdit.GRANT_VIEW_DS.equals(dataSource.getName())) {
            dataSource.put(GrantViewDSHandler.GRANT, getListRepresent().getGrant());
        }
    }

    public void fillOrdersCancel() {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentGrantEntityHistory.class, "h")
                .where(DQLExpressions.in(DQLExpressions.property(StudentGrantEntityHistory.studentGrantEntity().fromAlias("h")), getStudentGrants()))
                .joinEntity("h", DQLJoinType.inner, IRepresentOrder.class, "rel",
                            DQLExpressions.eq(DQLExpressions.property(StudentGrantEntityHistory.representation().id().fromAlias("h")),
                                              DQLExpressions.property(IRepresentOrderGen.representation().id().fromAlias("rel"))))
                .column(DQLExpressions.property(StudentGrantEntityHistory.studentGrantEntity().id().fromAlias("h")))
                .column(DQLExpressions.property(StudentGrantEntityHistory.studentGrantEntity().view().id().fromAlias("h")))
                .column("rel")
                .order(DQLExpressions.property(StudentGrantEntityHistory.studentGrantEntity().id().fromAlias("h")))
                .order(DQLExpressions.property(StudentGrantEntityHistory.createDate().fromAlias("h")));

        List<Object[]> list = IUniBaseDao.instance.get().getList(builder);

        getOrdersCancel().clear();

        Map<IAbstractOrder, Long> grantViewMap = new HashMap<IAbstractOrder, Long>();
        Map<Long, IAbstractOrder> orderMap = new HashMap<Long, IAbstractOrder>();
        for (Object[] obj : list) {
            Long sgeId = (Long) obj[0];
            Long grantViewId = (Long) obj[1];
            IRepresentOrder order = (IRepresentOrder) obj[2];
            orderMap.put(sgeId, order.getOrder());
            grantViewMap.put(order.getOrder(), grantViewId);
        }

        Set<IAbstractOrder> set = new HashSet<IAbstractOrder>();

        for (Long key : orderMap.keySet()) {
            IAbstractOrder order = orderMap.get(key);
            if (set.contains(order))
                continue;
            Long grantViewId = grantViewMap.get(order);
            GrantView grantView = (GrantView) IUniBaseDao.instance.get().get(grantViewId);
            DocRepresentOrderCancel rel = new DocRepresentOrderCancel();
            rel.setRepresentation(getListRepresent());
            rel.setGrantView(grantView);
            rel.setOrder(order);
            rel.setTitle(getTitleOrder(order, grantView));

            getOrdersCancel().add(rel);

            set.add(order);
        }
    }

    private String getTitleOrder(IAbstractOrder order, GrantView grantView) {
        if (order == null)
            return null;

        if (order instanceof ListOrder) {

            ListRepresent listRepresent = ((ListOrder) order).getRepresentationList().get(0);

            if (listRepresent.getRepresentationType().getCode().equals(RepresentationTypeCodes.APPOINT_GRANT)) {
                StringBuilder sb = new StringBuilder();

                List<RelTypeGrantView> relTypeGrantViews = DataAccessServices.dao().getList(RelTypeGrantView.class, RelTypeGrantView.view(),
                                                                                            grantView);

                if (relTypeGrantViews != null) {
                    Grant grant = relTypeGrantViews.get(0).getGrant();
                    sb.append("О назначении ")
                            .append(grant.getGenitive() == null ? grant.getTitle() : grant.getGenitive());
                }
                else
                    sb.append(listRepresent.getRepresentationType().getTitle());

                return sb.toString();
            }
            else
                return listRepresent.getRepresentationType().getTitle();
        }
        else {
            DocumentOrder dor = (DocumentOrder) order;
            return UtilPrintSupport.isSingleType(dor) ? dor.getRepresentationTypes() : "По личному составу обучающихся";
        }
    }

    public List<StudentGrantEntity> getStudentGrants() {

    	//-----------------------------------------------------------------------------------------------------------------------
        //List<String> period = MonthWrapper.getMonthsList(getListRepresent().getBeginGrantDate(), getListRepresent().getEndGrantDate(), getListRepresent().getEducationYear());
        List<String> period = MonthWrapper.getMonthsList(getListRepresent().getBeginGrantDate(), getListRepresent().getEndGrantDate());		//Снято ограничение учебным годом
        //-----------------------------------------------------------------------------------------------------------------------

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentGrantEntity.class, "sge")
                .joinEntity("sge", DQLJoinType.inner, RelTypeGrantView.class, "rel",
                            DQLExpressions.eq(
                                    DQLExpressions.property(StudentGrantEntity.view().id().fromAlias("sge")),
                                    DQLExpressions.property(RelTypeGrantView.view().id().fromAlias("rel"))
                            )
                )
                .where(DQLExpressions.or(
                        DQLExpressions.isNull(DQLExpressions.property(StudentGrantEntity.tmp().fromAlias("sge"))),
                        DQLExpressions.eqValue(DQLExpressions.property(StudentGrantEntity.tmp().fromAlias("sge")), Boolean.FALSE)
                ))
                .where(DQLExpressions.in(DQLExpressions.property(StudentGrantEntity.student().fromAlias("sge")), this.getStudentSelectedList()))
                .where(DQLExpressions.eqValue(DQLExpressions.property(StudentGrantEntity.status().code().fromAlias("sge")), StuGrantStatusCodes.CODE_1))//статус выплата
                .where(DQLExpressions.in(DQLExpressions.property(StudentGrantEntity.month().fromAlias("sge")), period))
                .where(DQLExpressions.eqValue(DQLExpressions.property(RelTypeGrantView.type().academ().fromAlias("rel")), Boolean.TRUE))
                .column("sge");

        List<StudentGrantEntity> sgeList = IUniBaseDao.instance.get().getList(builder);

        return sgeList;
    }

    private void checkStudentGrants() {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentGrantEntity.class, "sge")
                .where(DQLExpressions.in(DQLExpressions.property("sge"), getStudentGrants()))
                .column(DQLExpressions.property(StudentGrantEntity.student().id().fromAlias("sge")))
//				.distinct()
                .group(DQLExpressions.property(StudentGrantEntity.student().fromAlias("sge")));

        List<Long> studList = IUniBaseDao.instance.get().getList(builder);

        if (studList.size() != getStudentSelectedList().size()) {

            List<String> badList = new ArrayList<String>();

            for (Student student : getStudentSelectedList()) {
                if (!studList.contains(student.getId())) {
                    badList.add(student.getPerson().getFullFio());
                }
            }

            if (!badList.isEmpty())
                throw new ApplicationException("Невозможно сохранить представление, т.к. для " + StringUtils.join(badList, ", ") + " в указанный период не назначена ни одна академическая стипендия");
        }

    }

    @Override
    public void onClickSave() {

        if (getStudentSelectedList().isEmpty())
            getConfig().getUserContext().getErrorCollector().add(getConfig().getProperty("ui.alertEmptySelectedRepresent"));

        checkStudentGrants();

        if (getListRepresentId() != null)
            CheckStateUtil.checkStateRepresent(getListRepresent(), Arrays.asList(MovestudentExtractStatesCodes.CODE_1), _uiSupport.getSession());

        if (CommonBaseDateUtil.isAfter(getListRepresent().getBeginGrantDate(), getListRepresent().getEndGrantDate())) {
            throw new ApplicationException("Дата начала выплаты не может быть позже даты окончания выплаты");
        }

        //-------------------------------------------------------------------------------------------------------------------
        /*
        //период попадает в учебный год
        EducationYear eduYear = getListRepresent().getEducationYear();

        Calendar beginYear = Calendar.getInstance();
        beginYear.set(eduYear.getIntValue(), 8, 1, 0, 0, 0);
        beginYear.set(14, 0);
        Calendar endYear = Calendar.getInstance();
        endYear.set(eduYear.getIntValue() + 1, 7, 31, 23, 59, 59);
        endYear.set(14, 999);

        if (!CommonBaseDateUtil.isBetween(getListRepresent().getBeginGrantDate(), beginYear.getTime(), endYear.getTime())
                || !CommonBaseDateUtil.isBetween(getListRepresent().getEndGrantDate(), beginYear.getTime(), endYear.getTime()))
            throw new ApplicationException("Даты назначения стипендии/выплаты выходят за границы учебного года " + eduYear.getTitle());

        List<String> periodList = MonthWrapper.getMonthsList(getListRepresent().getBeginGrantDate(), getListRepresent().getEndGrantDate(), getListRepresent().getEducationYear());
        List<String> errorList = new ArrayList<String>();
        boolean good = MoveStudentDaoFacade.getGrantDAO().checkStudentGrants(getListRepresent().getId(), getListRepresent().getGrantView(), getStudentSelectedList(), periodList, errorList);

        if (!good) {
            RtfDocument errorReport = RtfBean.getElementFactory().createRtfDocument();
            IRtfControl par = RtfBean.getElementFactory().createRtfControl(IRtfData.PAR);
            for (String text : errorList) {
                IRtfText studText = RtfBean.getElementFactory().createRtfText(text);
                errorReport.addElement(studText);
                errorReport.getElementList().addAll(Arrays.asList(par, par));
            }
            BusinessComponentUtils.downloadDocument(new ReportRenderer("Ошибки.rtf", errorReport), true);

            throw new ApplicationException("Не удалось сохранить представление");
        }            
        */
        
		DQLSelectBuilder eduYearBuilder = new DQLSelectBuilder()
                .fromEntity(EducationYear.class, "year")
                .where(or(between(valueDate(getListRepresent().getBeginGrantDate())
                			, createTimestamp(property(EducationYear.intValue().fromAlias("year")), value(9), value(1), value(0), value(0), value(0))
                			, createTimestamp(plus(property(EducationYear.intValue().fromAlias("year")), value(1)), value(8), value(31), value(23), value(59), value(59)))
                		, between(valueDate(getListRepresent().getEndGrantDate())
                    			, createTimestamp(property(EducationYear.intValue().fromAlias("year")), value(9), value(1), value(0), value(0), value(0))
                    			, createTimestamp(plus(property(EducationYear.intValue().fromAlias("year")), value(1)), value(8), value(31), value(23), value(59), value(59)))

                		, and(lt(valueDate(getListRepresent().getBeginGrantDate()), createTimestamp(property(EducationYear.intValue().fromAlias("year")), value(9), value(1), value(0), value(0), value(0)))
                			, gt(valueDate(getListRepresent().getEndGrantDate()), createTimestamp(plus(property(EducationYear.intValue().fromAlias("year")), value(1)), value(8), value(31), value(23), value(59), value(59)))) 
                		))
                ;
		
		List<EducationYear> eduYearList = IUniBaseDao.instance.get().getList(eduYearBuilder);
		
		List<String> errorList = new ArrayList<>();
		
		for(EducationYear eduYear : eduYearList) {
			
			Date beginDate;
			Date endDate;
			
	        Calendar beginYear = Calendar.getInstance();
	        beginYear.set(eduYear.getIntValue(), Calendar.SEPTEMBER, 1, 0, 0, 0);
	        beginYear.set(Calendar.MILLISECOND, 0);
	        if(getListRepresent().getBeginGrantDate().compareTo(beginYear.getTime()) > 0) {
	        	beginDate = getListRepresent().getBeginGrantDate();
	        }
	        else {
	        	beginDate = beginYear.getTime();
	        }
	        
	        Calendar endYear = Calendar.getInstance();
	        endYear.set(eduYear.getIntValue() + 1, Calendar.AUGUST, 31, 23, 59, 59);
	        endYear.set(Calendar.MILLISECOND, 999);	
	        if(getListRepresent().getEndGrantDate().compareTo(endYear.getTime()) > 0) {
	        	endDate = endYear.getTime();
	        }
	        else {
	        	endDate = getListRepresent().getEndGrantDate();
	        }	        
	        
	        List<String> periodList = MonthWrapper.getMonthsList(beginDate, endDate, eduYear);
	        MoveStudentDaoFacade.getGrantDAO().checkStudentGrants(getListRepresent().getId(), getListRepresent().getGrantView(), getStudentSelectedList(), periodList, errorList);
		}	
		
        if (!errorList.isEmpty()) {
        	
            RtfDocument errorReport = RtfBean.getElementFactory().createRtfDocument();
            IRtfControl par = RtfBean.getElementFactory().createRtfControl(IRtfData.PAR);
            for (String text : errorList) {
                IRtfText studText = RtfBean.getElementFactory().createRtfText(text);
                errorReport.addElement(studText);
                errorReport.getElementList().addAll(Arrays.asList(par, par));
            }
            BusinessComponentUtils.downloadDocument(new ReportRenderer("Ошибки.rtf", errorReport), true);

            throw new ApplicationException("Не удалось сохранить представление");  
            
        }
        //-------------------------------------------------------------------------------------------------------------------

        super.onClickSave();

        fillOrdersCancel();

        IListRepresentGrantCancelAndDestinationManagerModifyDAO dao = (IListRepresentGrantCancelAndDestinationManagerModifyDAO) ListRepresentGrantCancelAndDestinationManager.instance().getListObjectModifyDAO();

        dao.deleteStudentGrantEntity(getListRepresent().getId());
        dao.saveStudentGrantEntity(getStudentGrants(), getListRepresent());
        dao.deleteOrderCancel(getListRepresent().getId());
        dao.saveOrderCancel(getOrdersCancel());

        //----------------------------------------------------------------------------------------------------------------------
        //MoveStudentDaoFacade.getGrantDAO().saveStudentGrants(getListRepresent(), getListRepresent().getGrantView(), getStudentSelectedList(), periodList, getListRepresent().getEducationYear());
        
        MoveStudentDaoFacade.getGrantDAO().deleteStudentGrants(getListRepresent());
        
		for(EducationYear eduYear : eduYearList) {
			
			Date beginDate;
			Date endDate;
			
	        Calendar beginYear = Calendar.getInstance();
	        beginYear.set(eduYear.getIntValue(), Calendar.SEPTEMBER, 1, 0, 0, 0);
	        beginYear.set(Calendar.MILLISECOND, 0);
	        if(getListRepresent().getBeginGrantDate().compareTo(beginYear.getTime()) > 0) {
	        	beginDate = getListRepresent().getBeginGrantDate();
	        }
	        else {
	        	beginDate = beginYear.getTime();
	        }
	        
	        Calendar endYear = Calendar.getInstance();
	        endYear.set(eduYear.getIntValue() + 1, Calendar.AUGUST, 31, 23, 59, 59);
	        endYear.set(Calendar.MILLISECOND, 999);	
	        if(getListRepresent().getEndGrantDate().compareTo(endYear.getTime()) > 0) {
	        	endDate = endYear.getTime();
	        }
	        else {
	        	endDate = getListRepresent().getEndGrantDate();
	        }	        
	        
	        List<String> periodList = MonthWrapper.getMonthsList(beginDate, endDate, eduYear);
	        MoveStudentDaoFacade.getGrantDAO().saveStudentGrants(getListRepresent(), getListRepresent().getGrantView(), getStudentSelectedList(), periodList, eduYear, false);

		}         
        
        //----------------------------------------------------------------------------------------------------------------------
    }

    public boolean getDisableSessionResultFilters() {
        List<OrgUnit> fou = _uiSettings.get("formativeOrgUnitFilter");
        List<OrgUnit> el = _uiSettings.get("educationLevelsHighSchoolDSFilter");
        List<OrgUnit> tou = _uiSettings.get("territorialOrgUnitFilter");
        List<OrgUnit> eou = _uiSettings.get("educationOrgUnitFilter");
        List<Group> group = _uiSettings.get("groupFilter");
        if (
                (
                        fou != null && fou.size() > 0) ||
                        (tou != null && tou.size() > 0) ||
                        (el != null && el.size() > 0) ||
                        (eou != null && eou.size() > 0) ||
                        (group != null && group.size() > 0)
                )
        {
            return false;
        }
        else {
            _uiSettings.set("isSuccessfullyHandOverSession", false);
            _uiSettings.set("isConsiderSessionResults", false);
            return true;
        }

    }

    public List<DocRepresentOrderCancel> getOrdersCancel() {
        return ordersCancel;
    }

    public void setOrdersCancel(List<DocRepresentOrderCancel> ordersCancel) {
        this.ordersCancel = ordersCancel;
    }


}
