package ru.tandemservice.movestudentrmc.base.bo.RelationSettings.logic;

import org.tandemframework.hibsupport.dao.CommonDAO;
import ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonBasic;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationBasement;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;

import java.util.List;

public class RelationSettingsModifyDAO extends CommonDAO implements IRelationSettingsModifyDAO {


    public void saveTypeRepresentation(RepresentationType typeRepresentation, List<FakeReasonsHandler> fakeReasonsList) {

        for (FakeReasonsHandler fakeReason : fakeReasonsList) {

            for (RepresentationBasement basic : fakeReason.getBasicsList()) {

                RelRepresentationReasonBasic reasonBasic = new RelRepresentationReasonBasic();

                reasonBasic.setType(typeRepresentation);
                reasonBasic.setReason(fakeReason.getReason());
                reasonBasic.setBasic(basic);

                save(reasonBasic);
            }
        }
    }

    public void updateTypeRepresentation(RepresentationType typeRepresentation, List<FakeReasonsHandler> fakeReasonsList, List<RelRepresentationReasonBasic> reasonBasicList) {

        //Step of list DB        
        for (RelRepresentationReasonBasic reasonBasic : reasonBasicList) {
            Boolean isFindReason = false;
            for (FakeReasonsHandler fakeReason : fakeReasonsList) {
                if (fakeReason.getReason().equals(reasonBasic.getReason())) {
                    Boolean isFindBasic = false;
                    for (RepresentationBasement basic : fakeReason.getBasicsList()) {
                        if (basic.equals(reasonBasic.getBasic())) {
                            isFindBasic = true;
                            break;
                        }
                    }
                    if (!isFindBasic) {
                        delete(reasonBasic);
                    }
                    isFindReason = true;
                    break;
                }
            }

            if (!isFindReason) {
                delete(reasonBasic);
            }
        }

        for (FakeReasonsHandler fakeReason : fakeReasonsList) {
            for (RepresentationBasement basic : fakeReason.getBasicsList()) {
                Boolean isFind = false;
                for (RelRepresentationReasonBasic reasonBasic : reasonBasicList) {
                    if (reasonBasic.getBasic().equals(basic) && reasonBasic.getReason().equals(fakeReason.getReason())) {
                        isFind = true;
                        break;
                    }
                }
                if (!isFind) {
                    RelRepresentationReasonBasic reasonBasic = new RelRepresentationReasonBasic();

                    reasonBasic.setType(typeRepresentation);
                    reasonBasic.setReason(fakeReason.getReason());
                    reasonBasic.setBasic(basic);

                    save(reasonBasic);
                }
            }
        }
    }

    public void updateRelations(RepresentationType typeRepresentation, RepresentationReason reason, List<RelRepresentationReasonBasic> relList) {

        for (RelRepresentationReasonBasic rel : relList) {

            if (!rel.isJoinGroup())
                rel.setGroupName(null);
            update(rel);
        }
    }
}
