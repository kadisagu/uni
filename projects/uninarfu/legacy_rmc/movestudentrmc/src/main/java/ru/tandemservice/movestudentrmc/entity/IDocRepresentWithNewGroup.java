package ru.tandemservice.movestudentrmc.entity;

import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.uni.entity.orgstruct.Group;

public interface IDocRepresentWithNewGroup extends IEntity {

    String L_NEW_GROUP = "newGroup";
    String L_GROUP_OLD = "groupOld";

    Group getNewGroup();

    void setNewGroup(Group newGroup);

    Group getGroupOld();

    void setGroupOld(Group groupOld);
}
