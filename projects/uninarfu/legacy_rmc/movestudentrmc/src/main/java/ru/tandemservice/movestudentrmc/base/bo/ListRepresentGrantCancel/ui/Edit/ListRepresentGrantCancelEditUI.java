package ru.tandemservice.movestudentrmc.base.bo.ListRepresentGrantCancel.ui.Edit;

import com.ibm.icu.util.Calendar;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.support.AbstractListRepresentEditUI;
import ru.tandemservice.movestudentrmc.dao.CheckStateUtil;
import ru.tandemservice.movestudentrmc.entity.ListRepresentGrantCancel;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentExtractStatesCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.base.bo.EducationYear.EducationYearManager;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.Collections;
import java.util.Date;
import java.util.List;

@State({
        @Bind(key = AbstractListRepresentEditUI.LIST_REPRESENT_ID, binding = AbstractListRepresentEditUI.LIST_REPRESENT_ID),
        @Bind(key = "typeRepresentFilter", binding = "typeRepresentFilter"),
        @Bind(key = "reasonRepresentFilter", binding = "reasonRepresentFilter"),
        @Bind(key = "basementRepresentFilter", binding = "basementRepresentFilter"),
        @Bind(key = "representationBasementDate", binding = "representationBasementDate"),
        @Bind(key = "representationBasementNumber", binding = "representationBasementNumber"),
        @Bind(key = "grantViewFilter", binding = "grantViewFilter")
})
public class ListRepresentGrantCancelEditUI extends AbstractListRepresentEditUI<ListRepresentGrantCancel> {

    private GrantView grantViewFilter;

    @Override
    public ListRepresentGrantCancel getListRepresentObject() {
        return new ListRepresentGrantCancel();
    }

    @Override
    public void checkSelectStudent(List<Student> selectList) {

    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void onClickSave() {
        if (getListRepresentId() != null)
            CheckStateUtil.checkStateRepresent(getListRepresent(), Collections.singletonList(MovestudentExtractStatesCodes.CODE_1), _uiSupport.getSession());

        //период попадает в учебный год
        ListRepresentGrantCancel represent = getListRepresent();
        //-----------------------------------------------------------------------------------------------------------------------
        /*
        EducationYear eduYear = represent.getEducationYear();

        Calendar beginYear = Calendar.getInstance();
        beginYear.set(eduYear.getIntValue(), 8, 1, 0, 0, 0);
        beginYear.set(14, 0);
        Calendar endYear = Calendar.getInstance();
        endYear.set(eduYear.getIntValue() + 1, 7, 31, 23, 59, 59);
        endYear.set(14, 999);

        if (!CommonBaseDateUtil.isBetween(represent.getDateCancelPayment(), beginYear.getTime(), endYear.getTime()))
            throw new ApplicationException("Дата отмены стипендии/выплаты выходит за границы учебного года " + eduYear.getTitle());
        */
        //-----------------------------------------------------------------------------------------------------------------------

        super.onClickSave();

        IPrincipalContext context = ContextLocal.getUserContext().getPrincipalContext();
        Person person = PersonManager.instance().dao().getPerson(context);
        getListRepresent().setOperator(person);

        getListRepresent().setCreator(context);

        _uiSupport.getSession().saveOrUpdate(getListRepresent());
    }

    @Override
    public void onComponentRefresh() {
        super.onComponentRefresh();

        //инициация доп полей
        if (this.getListRepresentId() == null) {
        	//---------------------------------------------------------------------------------------
            //getListRepresent().setEducationYear(EducationYearManager.instance().dao().getCurrent());
            //---------------------------------------------------------------------------------------
            getListRepresent().setDateCancelPayment(new Date());

            getListRepresent().setGrantView(getGrantViewFilter());
        }
        _uiSettings.set("grantViewFilter", getListRepresent().getGrantView());

    }

    ///////////////////////////////////////////////////////////////////////////////////////

    public GrantView getGrantViewFilter() {
        return grantViewFilter;
    }

    public void setGrantViewFilter(GrantView grantViewFilter) {
        this.grantViewFilter = grantViewFilter;
    }
}
