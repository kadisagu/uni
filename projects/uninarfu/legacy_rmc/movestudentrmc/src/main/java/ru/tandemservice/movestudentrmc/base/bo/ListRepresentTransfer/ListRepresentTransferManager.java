package ru.tandemservice.movestudentrmc.base.bo.ListRepresentTransfer;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentTransfer.logic.ListRepresentTransferManagerModifyDAO;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentTransfer.ui.Edit.ListRepresentTransferEdit;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentTransfer.ui.View.ListRepresentTransferView;
import ru.tandemservice.movestudentrmc.docord.IListObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.IListObjectModifyDAO;

@Configuration
public class ListRepresentTransferManager extends BusinessObjectManager implements IListObjectByRepresentTypeManager {

    public static ListRepresentTransferManager instance() {
        return instance(ListRepresentTransferManager.class);
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditComponentManager() {
        return ListRepresentTransferEdit.class;
    }

    @Override
    @Bean
    public IListObjectModifyDAO getListObjectModifyDAO() {
        return new ListRepresentTransferManagerModifyDAO();
    }

    @Override
    public Class<? extends BusinessComponentManager> getViewComponentManager() {
        return ListRepresentTransferView.class;
    }

}
