package ru.tandemservice.movestudentrmc.base.bo.ListRepresentQualificationAdmission.logic;

import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.AbstractListRepresentDAO;
import ru.tandemservice.movestudentrmc.docord.IListPrintDoc;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ListRepresentQualificationAdmissionManagerModifyDAO extends AbstractListRepresentDAO {

//	public void save(ListRepresent listRepresent,List<Student> studentSelectedList, DocListRepresentBasics representBasics) {
//        super.save();
//	}

    @Override
    public boolean doCommit(ListRepresent represent, ErrorCollector error) {
        if (!super.doCommit(represent, UserContext.getInstance().getErrorCollector()))
            return false;

        ListRepresentQualificationAdmission listRepresent = (ListRepresentQualificationAdmission) represent;

        return true;
    }

    @Override
    public boolean doRollback(ListRepresent represent, ErrorCollector error) {
        if (!super.doRollback(represent, UserContext.getInstance().getErrorCollector()))
            return false;
        //удалим статус, который назначили этим представлением
        return true;
    }

    @Transactional
    public void saveSupport(ListRepresent listRepresent, List<DataWrapper> studentSelectedList, DocListRepresentBasics representBasics) {

        ListRepresentQualificationAdmission listQualificationAdmission = (ListRepresentQualificationAdmission) listRepresent;

        List<Student> stuSelectedList = new ArrayList<Student>();


        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(RelListRepresentStudents.class)
                .fromEntity(RelListRepresentStudents.class, "rls")
                .where(DQLExpressions.eq
                        (DQLExpressions.property(RelListRepresentStudents.representation()), DQLExpressions.value(listRepresent)));


        //Почистим старых студентов
        deleteBuilder.createStatement(getSession()).execute();

        //Сохраним оператора, изменившего представление
        Person person = PersonManager.instance().dao().getPerson(ContextLocal.getUserContext().getPrincipalContext());
        if (person != null)
            listQualificationAdmission.setOperator(person);

        IPrincipalContext context = ContextLocal.getUserContext().getPrincipalContext();
        listQualificationAdmission.setCreator(context);

        baseCreateOrUpdate(listRepresent);
        baseCreateOrUpdate(representBasics);
        for (Student student : stuSelectedList) {
            RelListRepresentStudents relListRepresentGrantStudents = new RelListRepresentStudents();
            relListRepresentGrantStudents.setRepresentation(listRepresent);
            relListRepresentGrantStudents.setStudent(student);
            baseCreateOrUpdate(relListRepresentGrantStudents);
        }
    }


    @Override
    public void buildBodyExtract(ListOrdListRepresent listOrdListRepresent,
                                 Student student, RtfDocument docExtract, Map<Student, OrderParagraphInfo> map)
    {
        super.buildBodyExtract(listOrdListRepresent, student, docExtract, map);

        ListRepresentQualificationAdmission representation = (ListRepresentQualificationAdmission) listOrdListRepresent.getRepresentation();
        RtfInjectModifier im = new RtfInjectModifier();
        //----------------------------------------------------------------------------
        im.put("studentNumber", String.valueOf(map.get(student).getStudentNumber()));
        im.put("studentTitle_acc", PersonManager.instance().declinationDao().getDeclinationFIO(student.getPerson().getIdentityCard(), GrammaCase.ACCUSATIVE));
        //----------------------------------------------------------------------------
        ListPrintDoc.injectModifier(im, representation, student);

        im.modify(docExtract);
    }

    @Override
    public Map<Student, OrderParagraphInfo> buildBodyRepresent(List<ListRepresent> representationBase, RtfDocument document) {

        ListPrintDoc listPrintDoc = new ListPrintDoc();
        RtfDocument rtfDocument = listPrintDoc.creationPrintDocOrder(representationBase);

        document.getElementList().addAll(rtfDocument.getElementList());

        return ((IListPrintDoc) listPrintDoc).getParagInfomap();
    }


    @Override
    public List<Student> selectRepresent(ListRepresent listRepresent) {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(RelListRepresentStudents.class, "rls");

        builder.where(DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("rls")), DQLExpressions.value(listRepresent)));

        builder.addColumn(DQLExpressions.property(RelListRepresentStudents.student().fromAlias("rls")));

        return builder.createStatement(getSession()).list();
    }
}
