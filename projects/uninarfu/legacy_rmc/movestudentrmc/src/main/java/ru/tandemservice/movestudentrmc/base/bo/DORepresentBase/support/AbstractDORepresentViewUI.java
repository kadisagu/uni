package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support;

import org.apache.commons.collections15.map.HashedMap;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.util.RtfUtil;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.DORepresentBaseManager;
import ru.tandemservice.movestudentrmc.dao.CheckStateUtil;
import ru.tandemservice.movestudentrmc.docord.IDOObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.DocRepresentBasics;
import ru.tandemservice.movestudentrmc.entity.DocRepresentGrants;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.entity.catalog.MovestudentExtractStates;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentExtractStatesCodes;
import ru.tandemservice.movestudentrmc.util.CheckUtil;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.ReportRenderer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = AbstractDORepresentViewUI.REPRESENTATION_ID),
})
public abstract class AbstractDORepresentViewUI extends UIPresenter {

    public static final String REPRESENTATION_ID = "representId";

    protected Long _representId;
    protected Representation _represent;
    protected Student _student;
    protected List<DocRepresentBasics> _representBasicsList;
    protected DocRepresentBasics _representBasicsRow;
    protected List<DocRepresentGrants> _representGrantsList;
    protected DocRepresentGrants _representGrantsRow;
    protected boolean displayWarning = false;
    protected Map<String, Object> componentParams;

    @Override
    public void onComponentRefresh() {

        _represent = DataAccessServices.dao().get(Representation.class, Representation.id().s(), _representId);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocRepresentStudentBase.class, "stud").column("stud");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(DocRepresentStudentBase.representation().fromAlias("stud")),
                DQLExpressions.value(_represent.getId())));

        List<DocRepresentStudentBase> studentList = builder.createStatement(getSupport().getSession()).list();

        _student = studentList.get(0).getStudent();

        builder = new DQLSelectBuilder().fromEntity(DocRepresentBasics.class, "b").column("b");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(DocRepresentBasics.representation().fromAlias("b")),
                DQLExpressions.value(_represent)));

        _representBasicsList = builder.createStatement(getSupport().getSession()).list();
        
        /*Grants*/
        builder = new DQLSelectBuilder().fromEntity(DocRepresentGrants.class, "g").column("g");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(DocRepresentGrants.representation().fromAlias("g")),
                DQLExpressions.value(_represent)));

        _representGrantsList = builder.createStatement(getSupport().getSession()).list();
        /**/

    }

    public void onClickApprove() {

        CheckStateUtil.checkStateRepresent(_represent, Arrays.asList(MovestudentExtractStatesCodes.CODE_1), _uiSupport.getSession());

        List<String> errorList = new ArrayList<String>();

        boolean ok = CheckUtil.checkIndividualRepresent(_represent, errorList);

        if (ok) {
            approve(true);
        }
        else {
            setDisplayWarning(true);

            RtfDocument errorReport = RtfBean.getElementFactory().createRtfDocument();
            IRtfControl par = RtfBean.getElementFactory().createRtfControl(IRtfData.PAR);
            for (String text : errorList) {
                IRtfText studText = RtfBean.getElementFactory().createRtfText(text);
                errorReport.addElement(studText);
                errorReport.getElementList().addAll(Arrays.asList(par, par));
            }
            BusinessComponentUtils.downloadDocument(new ReportRenderer("Ошибки.rtf", errorReport, false), true);
        }

    }

    public void onCancel() {
        _represent.setCheck(false);
        DORepresentBaseManager.instance().modifyDao().updateState(_represent);
        setDisplayWarning(false);
    }

    public void onIgnoreWarning() {
        approve(false);
        setDisplayWarning(false);
    }

    public void approve(boolean check) {
        _represent.setState(DataAccessServices.dao().get(MovestudentExtractStates.class, MovestudentExtractStates.code().s(), "2"));
        _represent.setCheck(check);
        DORepresentBaseManager.instance().modifyDao().updateState(_represent);
    }

    public void onClickFormative() {

        CheckStateUtil.checkStateRepresent(_represent, Arrays.asList(MovestudentExtractStatesCodes.CODE_2), _uiSupport.getSession());

        _represent.setState(DataAccessServices.dao().get(MovestudentExtractStates.class, MovestudentExtractStates.code().s(), "1"));

        DORepresentBaseManager.instance().modifyDao().updateState(_represent);

        //deactivate();
    }

    public void onClickDoApprove() {

        CheckStateUtil.checkStateRepresent(_represent, Arrays.asList(MovestudentExtractStatesCodes.CODE_2), _uiSupport.getSession());

        if (checkManyRepresent())
            getConfig().getUserContext().getErrorCollector().add(getConfig().getProperty("ui.alertManyRepresent"));

        if (getConfig().getUserContext().getErrorCollector().hasErrors())
            return;

        _represent.setDocRepresent(RtfUtil.toByteArray(UtilPrintSupport.representRendererRtf(_represent)));

        _represent.setState(DataAccessServices.dao().get(MovestudentExtractStates.class, MovestudentExtractStates.code().s(), "3"));

        DORepresentBaseManager.instance().modifyDao().updateState(_represent);

        //deactivate();
    }

    public void onClickNotApprove() {

        CheckStateUtil.checkStateRepresent(_represent, Arrays.asList(MovestudentExtractStatesCodes.CODE_3), _uiSupport.getSession());

        _represent.setState(DataAccessServices.dao().get(MovestudentExtractStates.class, MovestudentExtractStates.code().s(), "2"));
        _represent.setDocRepresent(null);
        DORepresentBaseManager.instance().modifyDao().updateState(_represent);

        //deactivate();
    }

    public void onClickPrintRepresent() {

        try {
            IDocumentRenderer doc = UtilPrintSupport.representRenderer(_represent);
            if (doc != null)
                BusinessComponentUtils.downloadDocument(doc, true);
        }
        catch (Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public void onClickPrintDeclaration() {
        try {
            IDocumentRenderer doc = UtilPrintSupport.declarationRenderer(_represent);
            if (doc != null)
                BusinessComponentUtils.downloadDocument(doc, true);
        }
        catch (Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public void onClickEdit() {

        List<Long> studentListId = new ArrayList<Long>();
        studentListId.add(_student.getId());

        Representation representBase = ((Representation) _represent);
        String type = representBase.getType().getCode();
        IDOObjectByRepresentTypeManager representManager = DORepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(type);
        _uiActivation.asCurrent(representManager.getEditComponentManager())
                .parameter(AbstractDORepresentEditUI.TYPE_ID, representBase.getType().getId())
                .parameter(AbstractDORepresentEditUI.REPRESENTATION_ID, representBase.getId())
                .parameter(AbstractDORepresentEditUI.STUDENT_LIST_ID, studentListId)
                .activate();
    }

    public Representation getRepresent() {
        return _represent;
    }

    public void setRepresent(Representation represent) {
        _represent = represent;
    }

    private boolean checkManyRepresent() {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Representation.class, "type").column("type");
        builder.joinEntity("type", DQLJoinType.inner, DocRepresentStudentBase.class, "stud",
                           DQLExpressions.eq(DQLExpressions.property(Representation.id().fromAlias("type")),
                                             DQLExpressions.property(DocRepresentStudentBase.representation().fromAlias("stud"))));
        builder.where(DQLExpressions.eq(DQLExpressions.property(DocRepresentStudentBase.student().fromAlias("stud")), DQLExpressions.value(_student)));
        builder.where(DQLExpressions.and(
                DQLExpressions.and(
                        DQLExpressions.ne(DQLExpressions.property(Representation.state().code().fromAlias("type")), DQLExpressions.value("1")),
                        DQLExpressions.ne(DQLExpressions.property(Representation.state().code().fromAlias("type")), DQLExpressions.value("2"))),
                DQLExpressions.ne(DQLExpressions.property(Representation.state().code().fromAlias("type")), DQLExpressions.value("6"))
        ));

        return !builder.createStatement(getSupport().getSession()).list().isEmpty();
    }

    public Student getStudent() {

        if (_student == null)
            return null;

//        IDOObjectByRepresentTypeManager representManager = DORepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(_represent.getType().getCode());
//        IdentityCard identityCard = _student.getPerson().getIdentityCard();
//        GrammaCase rusCase = representManager.getDOObjectModifyDAO().getGrammaCase();
//        boolean isMaleSex = UniDefines.CATALOG_SEX_MALE.equals(identityCard.getSex().getCode());
//
//        StringBuilder str = new StringBuilder(IDeclinationDao.instance.get().getDeclinationLastName(identityCard.getLastName(), rusCase, isMaleSex).toUpperCase());
//        str.append(" ").append(IDeclinationDao.instance.get().getDeclinationFirstName(identityCard.getFirstName(), rusCase, isMaleSex));
//        if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
//            str.append(" ").append(IDeclinationDao.instance.get().getDeclinationMiddleName(identityCard.getMiddleName(), rusCase, isMaleSex));
//
//        return str.toString();

        return _student;
    }

    public Boolean getEditScholarVal() {

        return null;
    }

    public Long getRepresentId() {
        return _representId;
    }

    public void setRepresentId(Long representId) {
        _representId = representId;
    }

    public List<DocRepresentBasics> getRepresentBasicsList() {
        return _representBasicsList;
    }

    public void setRepresentBasicsList(List<DocRepresentBasics> representBasicsList) {
        _representBasicsList = representBasicsList;
    }

    public DocRepresentBasics getRepresentBasicsRow() {
        return _representBasicsRow;
    }

    public void setRepresentBasicsRow(DocRepresentBasics representBasicsRow) {
        _representBasicsRow = representBasicsRow;
    }

    public List<DocRepresentGrants> getRepresentGrantsList() {
        return _representGrantsList;
    }

    public void setRepresentGrantsList(List<DocRepresentGrants> representGrantsList) {
        _representGrantsList = representGrantsList;
    }

    public DocRepresentGrants getRepresentGrantsRow() {
        return _representGrantsRow;
    }

    public void setRepresentGrantsRow(DocRepresentGrants representGrantsRow) {
        _representGrantsRow = representGrantsRow;
    }

    public boolean isDisplayWarning() {
        return displayWarning;
    }

    public void setDisplayWarning(boolean displayWarning) {
        this.displayWarning = displayWarning;
    }

    public Map<String, Object> getComponentParams() {
        componentParams = new HashedMap<>();
        componentParams.put("publisherId", _student.getId());
        componentParams.put("selectedStudentTab", "studentTab");
        return componentParams;
    }

    public void setComponentParams(Map<String, Object> componentParams) {
        this.componentParams = componentParams;
    }
}
