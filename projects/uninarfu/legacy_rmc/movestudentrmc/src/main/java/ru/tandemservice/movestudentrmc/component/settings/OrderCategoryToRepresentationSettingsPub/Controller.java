package ru.tandemservice.movestudentrmc.component.settings.OrderCategoryToRepresentationSettingsPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.unibase.UniBaseUtils;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        ((IDAO) getDao()).prepare(model);
        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component) {

        Model model = (Model) getModel(component);

        DynamicListDataSource<RepresentationType> dataSource = model.getDataSource();

        if (dataSource != null)
            return;

        dataSource = UniBaseUtils.createDataSource(component, (IListDataSourceDao<Model>) getDao());

        dataSource.addColumn(new SimpleColumn("Наименование", RepresentationType.titleWithType()));

        model.setDataSource(dataSource);

    }

}
