package ru.tandemservice.movestudentrmc.component.student.GrantList;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab.MonthWrapper;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntity;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

@State({
        @Bind(key = "studentId", binding = "student.id"),
        @Bind(key = "eduYearId", binding = "eduYear.id"),
        @Bind(key = "month", binding = "month")
})
public class Model {

    private Student student = new Student();
    private EducationYear eduYear = new EducationYear();
    private int month;
    private MonthWrapper monthWrapper;
    private String listTitle;

    private DynamicListDataSource<StudentGrantEntity> dataSource;

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public EducationYear getEduYear() {
        return eduYear;
    }

    public void setEduYear(EducationYear eduYear) {
        this.eduYear = eduYear;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getMonth() {
        return month;
    }

    public void setDataSource(DynamicListDataSource<StudentGrantEntity> dataSource) {
        this.dataSource = dataSource;
    }

    public DynamicListDataSource<StudentGrantEntity> getDataSource() {
        return dataSource;
    }

    public String getListTitle() {
        return this.listTitle;
    }

    public void setListTitle(String listTitle) {
        this.listTitle = listTitle;
    }

    public void setMonthWrapper(MonthWrapper monthWrapper) {
        this.monthWrapper = monthWrapper;
    }

    public MonthWrapper getMonthWrapper() {
        return monthWrapper;
    }


}
