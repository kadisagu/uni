package ru.tandemservice.movestudentrmc.docord;

import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.entity.DocListRepresentBasics;
import ru.tandemservice.movestudentrmc.entity.ListOrdListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.List;
import java.util.Map;


public interface IListObjectModifyDAO {

    //ListRepresent createRepresentObject(RepresentationType type, List<Student> student);

    boolean doCommit(ListRepresent represent, ErrorCollector error);

    boolean doRollback(ListRepresent represent, ErrorCollector error);

    void buildBodyExtract(ListOrdListRepresent listOrdListRepresent, Student student, RtfDocument docCommon);

    void buildBodyExtract(ListOrdListRepresent listOrdListRepresent, Student student, RtfDocument docCommon, Map<Student, OrderParagraphInfo> map);

    /**
     * Возвращает инфу о номере параграфа и номере студента в параграфе
     */
    Map<Student, OrderParagraphInfo> buildBodyRepresent(List<ListRepresent> representationBase, RtfDocument document);

    RtfDocument getTemplateExtract(ICatalogItem type);

    RtfDocument getTemplateRepresent(ICatalogItem type);

    GrammaCase getGrammaCase();

    List<Student> selectRepresent(ListRepresent listRepresent);

    @Transactional
    void save(ListRepresent listRepresent, List<Student> studentSelectedList, DocListRepresentBasics representBasics);

    @Transactional
    void saveSupport(ListRepresent listRepresent,
                     List<DataWrapper> studentSelectedList, DocListRepresentBasics representBasics);


    @Deprecated
    void update(ListRepresent listRepresent, List<Student> studentSelectedList);

    public void checkSelectStudent(ListRepresent listRepresent, List<Student> selectList);
}
