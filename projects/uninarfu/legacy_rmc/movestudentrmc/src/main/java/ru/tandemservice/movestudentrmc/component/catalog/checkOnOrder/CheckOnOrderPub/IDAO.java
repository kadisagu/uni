package ru.tandemservice.movestudentrmc.component.catalog.checkOnOrder.CheckOnOrderPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;
import ru.tandemservice.movestudentrmc.entity.catalog.CheckOnOrder;

public interface IDAO extends IDefaultCatalogPubDAO<CheckOnOrder, Model> {

    void updateInUse(Model model, Long id);
}
