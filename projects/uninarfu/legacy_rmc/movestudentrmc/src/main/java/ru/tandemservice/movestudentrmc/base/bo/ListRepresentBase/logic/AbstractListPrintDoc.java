package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic;

import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudentrmc.docord.IListPrintDoc;
import ru.tandemservice.movestudentrmc.docord.util.OrderParagraphInfo;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate;
import ru.tandemservice.movestudentrmc.entity.ListOrdListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListOrder;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.*;

public abstract class AbstractListPrintDoc<T extends ListRepresent> implements IListPrintDoc
{

    private Map<Student, OrderParagraphInfo> hashMap = new HashMap<>();

    public RtfDocument creationPrintDocOrder(List<? extends ListRepresent> listOfRepresents)
    {
        PrintTemplate printTemplate = UniDaoFacade.getCoreDao().getCatalogItem(PrintTemplate.class, getTemplateHeader());
        RtfDocument templateHeader = new RtfReader().read(printTemplate.getContent());

        printTemplate = UniDaoFacade.getCoreDao().getCatalogItem(PrintTemplate.class, getTemplateParag());
        RtfDocument templateParag = new RtfReader().read(printTemplate.getContent());

        RtfDocument templateAddon = null;
        if (getTemplateAddon() != null) {
            printTemplate = UniDaoFacade.getCoreDao().getCatalogItem(PrintTemplate.class, getTemplateAddon());
            templateAddon = new RtfReader().read(printTemplate.getContent());
        }
        ListOrder order = getOrder(listOfRepresents.get(0));

        Map<OrgUnit, Map<String, List<Object[]>>> dataMap = getStudentData(listOfRepresents);
        //сортировка формирующих по алфавиту
        List<OrgUnit> orgUnitList = new ArrayList<>(dataMap.keySet());
        Collections.sort(orgUnitList, new EntityComparator<OrgUnit>(new EntityOrder(OrgUnit.title())));

        RtfDocument result = templateHeader.getClone();
        result.setSettings(templateParag.getSettings());
        IRtfElement paragTemp = UniRtfUtil.findElement(result.getElementList(), "parag");

        RtfInjectModifier im = new RtfInjectModifier();
        im.put("representTitle", listOfRepresents.get(0).getRepresentationType().getTitle());
        T listRepresent = (T) listOfRepresents.get(0);
        headerInjectModifier(im, listRepresent);
        im.modify(result);
        RtfDocument res = RtfBean.getElementFactory().createRtfDocument();
        res.setHeader(templateParag.getHeader());
        res.setSettings(templateParag.getSettings());
        res.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.TAB));

        int formCounter = 0;
        int addonCounter = 0;
        List<RtfDocument> addonList = new ArrayList<>();

        for (OrgUnit formOU : orgUnitList) {
            Map<String, List<Object[]>> formMap = dataMap.get(formOU);

            formCounter++;
            res.getElementList().add(RtfBean.getElementFactory().createRtfText(String.valueOf(formCounter).trim() + "."));
            res.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.TAB));
            insertRow(res, "По " + formOU.getDativeCaseTitle());

            int counter = 0;
            //----------------------------------------------------------------
            //Добавлена сортировка параграфов по ключу getKey() 
            List<String> paragraphKeyList = new ArrayList<>(formMap.keySet());
            Collections.sort(paragraphKeyList, String::compareTo);
            for (String paragraphKey : paragraphKeyList) {
            	List<Object[]> studentList = formMap.get(paragraphKey);
            	
            //for (List<Object[]> studentList : formMap.values()) {
            //----------------------------------------------------------------            
                counter++;
                addonCounter++;
                RtfDocument par = getParagraph(templateParag, formCounter, counter, addonCounter, studentList);
                res.getElementList().addAll(par.getElementList());
                if (getTemplateAddon() != null)
                    addonList.add(getAddon(templateAddon, addonCounter, studentList, order));
            }
        }

        //основания
        if (getTemplateAddon() != null)
            res.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
        insertRow(res, "Основание: " + UtilPrintSupport.getBasementTitles(listOfRepresents));
        result.getElementList().addAll(result.getElementList().indexOf(paragTemp), res.getElementList());

        if (getTemplateAddon() != null)
            result.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));

        for (int i = 0; i < addonList.size(); i++) {
            result.getElementList().addAll(addonList.get(i).getElementList());
            if (i + 1 < addonList.size())
                result.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
        }
        SharedRtfUtil.removeParagraphsWithTagsRecursive(result, Collections.singletonList("parag"), false, false);
        return result;
    }

    /**
     * Метод заполняет метки в шапке
     */
    protected void headerInjectModifier(RtfInjectModifier im, T listRepresent)
    {

    }

    /**
     * Метод возвращает id основного шаблона
     */
    protected abstract String getTemplateHeader();

    /**
     * Метод возвращает id параграфа
     */
    protected abstract String getTemplateParag();

    /**
     * Метод возвращает id приложения со списком студентов
     */
    protected abstract String getTemplateAddon();

    /**
     * Метод заполняет метки в параграфе
     */
    protected abstract void paragraphInjectModifier(RtfInjectModifier im, T listRepresent, Student student);

    /**
     * Метод заполняет метки в приложении
     */
    protected void addonInjectModifier(RtfInjectModifier im, T listRepresent, Student student)
    {

    }

    protected void insertRow(RtfDocument document, String text)
    {
        document.getElementList().add(RtfBean.getElementFactory().createRtfText(text));
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
    }

    protected void insertRow(RtfDocument document, RtfString text)
    {
        document.getElementList().addAll(text.toList());
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
    }

    protected RtfDocument getParagraph(RtfDocument template, int formCounter, int counter, int addonCounter, List<Object[]> studentList)
    {
        RtfDocument result = template.getClone();
        T listRepresent = (T) studentList.get(0)[1];
        Student student = (Student) studentList.get(0)[0];
        for (Object[] o : studentList) {
            Student s = (Student) o[0];
            hashMap.put(s, new OrderParagraphInfo(formCounter, counter));
        }
        RtfInjectModifier im = new RtfInjectModifier();

        im.put("i", String.valueOf(formCounter).trim());
        im.put("j", String.valueOf(counter).trim());
        im.put("addonNumber", String.valueOf(addonCounter));

        paragraphInjectModifier(im, listRepresent, student);

        im.modify(result);
        return result;
    }

    protected RtfDocument getAddon(RtfDocument template, int addonCounter, List<Object[]> studentList, ListOrder order)
    {
        RtfDocument result = template.getClone();

        T listRepresent = (T) studentList.get(0)[1];
        Student firstStudent = (Student) studentList.get(0)[0];

        RtfInjectModifier im = new RtfInjectModifier();
        im.put("addonNumber", "" + addonCounter);
        im.put("orderDate", order != null ? UtilPrintSupport.getDateFormatterWithMonthStringAndYear(order.getCommitDate()) : new RtfString());
        im.put("orderNumber", order != null ? order.getNumber() : "");
        //--------------------------------------------------------------------------------------------------------------------------------------
        im.put("signPostGenitive", order != null ? order.getSignPostGenitive() : "");
        //--------------------------------------------------------------------------------------------------------------------------------------

        addonInjectModifier(im, listRepresent, firstStudent);

        im.modify(result);

        List<Student> lst = new ArrayList<>();
        for (Object[] arr : studentList)
            lst.add((Student) arr[0]);

        RtfTableModifier tm = new RtfTableModifier();
        tm.put("T", getTableData(lst, studentList).toArray(new String[][]{}));
        tm.modify(result);

        return result;
    }

    /**
     * Готовим данные для таблицы в приложении
     */
    protected List<String[]> getTableData(List<Student> studentList, List<Object[]> additionalList)
    {
        return Collections.emptyList();
    }

    protected Map<OrgUnit, Map<String, List<Object[]>>> getStudentData(List<? extends ListRepresent> listOfRepresents)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(RelListRepresentStudents.class, "rel")
                .where(DQLExpressions.in(
                        DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("rel")),
                        listOfRepresents
                ))
                .joinEntity(
                        "rel",
                        DQLJoinType.inner,
                        getListRepresentClass(),
                        "rep",
                        DQLExpressions.eq(
                                DQLExpressions.property("rep.id"),
                                DQLExpressions.property(RelListRepresentStudents.representation().id().fromAlias("rel"))
                        ))
                .column(DQLExpressions.property(RelListRepresentStudents.student().fromAlias("rel")))
                .column("rep");

        applyOrders(dql);

        List<Object[]> studentDataList = UniDaoFacade.getCoreDao().getList(dql);

        Map<OrgUnit, Map<String, List<Object[]>>> dataMap = new HashMap<>();
        for (Object[] arr : studentDataList) {
            Student student = (Student) arr[0];
            T represent = (T) arr[1];

            OrgUnit formOU = student.getEducationOrgUnit().getFormativeOrgUnit();
            if (dataMap.get(formOU) == null)
                dataMap.put(formOU, new HashMap<String, List<Object[]>>());

            String key = getKey(student, represent);
            if (dataMap.get(formOU).get(key) == null)
                dataMap.get(formOU).put(key, new ArrayList<Object[]>());

            dataMap.get(formOU).get(key).add(arr);
        }

        return dataMap;
    }

    /**
     * Тут можно указывать сортировки
     */
    protected void applyOrders(DQLSelectBuilder builder)
    {
        builder.order(DQLExpressions.property(RelListRepresentStudents.student().person().identityCard().fullFio().fromAlias("rel")));
    }

    /**
     * Метод возвращает класс списочного представления
     */
    protected abstract Class<? extends ListRepresent> getListRepresentClass();

    /**
     * Метод возвращает ключ, по которому необходимо группировать параграфы в приказе
     */
    protected abstract String getKey(Student student, T represent);

    protected ListOrder getOrder(ListRepresent listRepresent)
    {
        List<ListOrdListRepresent> orderList = UniDaoFacade.getCoreDao().getList(ListOrdListRepresent.class, ListOrdListRepresent.representation(), listRepresent);
        return orderList.isEmpty() ? null : orderList.get(0).getOrder();
    }

    @Override
    public Map<Student, OrderParagraphInfo> getParagInfomap()
    {
        return hashMap;
    }

}
