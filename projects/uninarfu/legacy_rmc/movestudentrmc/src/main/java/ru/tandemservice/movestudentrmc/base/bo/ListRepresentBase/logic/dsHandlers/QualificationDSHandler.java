package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLSelectableQuery;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class QualificationDSHandler extends DefaultComboDataSourceHandler {

    public QualificationDSHandler(String ownerId) {
        super(ownerId, Qualifications.class);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep) {

        IDQLSelectableQuery ex = new DQLSelectBuilder().fromEntity(EducationLevelsHighSchool.class, "ex").column(property(EducationLevelsHighSchool.id().fromAlias("ex")))
                .where(eq(property(Qualifications.id().fromAlias("e")), property(EducationLevelsHighSchool.educationLevel().qualification().id().fromAlias("ex"))))
                .buildQuery();

        ep.dqlBuilder.where(exists(ex));
    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep) {
        ep.dqlBuilder.order("e." + Qualifications.order());
    }

}
