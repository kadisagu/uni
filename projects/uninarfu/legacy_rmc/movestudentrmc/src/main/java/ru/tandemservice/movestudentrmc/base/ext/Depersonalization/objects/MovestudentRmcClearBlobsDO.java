package ru.tandemservice.movestudentrmc.base.ext.Depersonalization.objects;

import com.google.common.collect.ImmutableList;
import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.application.IModuleMeta;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.shared.commonbase.base.bo.Depersonalization.logic.DepersonalizationUtils;
import org.tandemframework.shared.commonbase.base.bo.Depersonalization.logic.IDepersonalizationObject;
import ru.tandemservice.movestudentrmc.entity.*;

import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.nul;

/**
 * @author avedernikov
 * @since 09.11.2015
 */

public class MovestudentRmcClearBlobsDO implements IDepersonalizationObject
{
	@Override
	public String getTitle()
	{
		return "Очистка содержимого документов (Движение контингента студентов)";
	}

	@Override
	public Collection<Class<? extends IEntity>> getEntityClasses()
	{
		return ImmutableList.of(
				Representation.class,
				DocumentOrder.class,
				AdditionalDocument.class,
				ListOrder.class,
				ListRepresent.class,
				ExtractTextRelation.class
		);
	}

	@Override
	public String getSettingsName()
	{
		return "movestudentRmcClearBlobs";
	}

	@Override
	public Collection<String> getDescription()
	{
		return ImmutableList.of(
				"Очищаются «Представление в zip» и «Выписка в zip» из «Базовое представление в приказ», «Приказ в zip» из «САФУ Приказы», «Документ» из «Дополнтельные документы», " +
				"«Приказ в zip» из «Списочный приказ», «Текст представления» из «Списочное представление», «Текст выписки» из «Текст выписки из приказа студента»."
		);
	}

	@Override
	public IModuleMeta getModule()
	{
		return DepersonalizationUtils.getModuleMeta(Representation.class);
	}

	@Override
	public void apply(Session session)
	{
		ImmutableList<String> docBlobs = ImmutableList.of(Representation.docRepresent().s(), Representation.docExtract().s());
		clearBlobs(Representation.class, docBlobs, session);
		clearBlobs(DocumentOrder.class, DocumentOrder.document().s(), session);
		clearBlobs(AdditionalDocument.class, AdditionalDocument.additionalDocFile().s(), session);
		clearBlobs(ListOrder.class, ListOrder.document().s(), session);
		clearBlobs(ListRepresent.class, ListRepresent.document().s(), session);
		clearBlobs(ExtractTextRelation.class, ExtractTextRelation.text().s(), session);
	}

	private void clearBlobs(Class<? extends IEntity> target, String blobProp, Session session)
	{
		CommonDAO.executeAndClear(new DQLUpdateBuilder(target).set(blobProp, nul(PropertyType.BLOB)), session);
	}

	private void clearBlobs(Class<? extends IEntity> target, Collection<String> blobProps, Session session)
	{
		DQLUpdateBuilder dql = new DQLUpdateBuilder(target);
		for (String prop: blobProps)
			dql.set(prop, nul(PropertyType.BLOB));
		CommonDAO.executeAndClear(dql, session);
	}
}

