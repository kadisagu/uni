/* $Id$ */
package ru.tandemservice.movestudentrmc.base.bo.ListRepresentAdmissionToPractice.logic;

import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.ListRepresentPracticePrintDoc;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.ListRepresent;
import ru.tandemservice.movestudentrmc.entity.ListRepresentAdmissionToPractice;
import ru.tandemservice.uni.entity.employee.Student;
//----------------------------------------------
import org.tandemframework.rtf.util.RtfString;
//----------------------------------------------

/**
 * @author Andrey Andreev
 * @since 26.10.2015
 */
public class ListRepresentAdmissionToPracticePrintDoc extends ListRepresentPracticePrintDoc
{
    public ListRepresentAdmissionToPracticePrintDoc()
    {
        super("list.rep.admission.practice.header",
                "list.rep.admission.practice.parag");
    }

    @Override
    protected RtfInjectModifier paragraphModify(RtfInjectModifier im, ListRepresent represent, Student student)
    {
        ListRepresentAdmissionToPractice rp = (ListRepresentAdmissionToPractice) represent;
        im.put("dateBegin", UtilPrintSupport.getDateFormatterWithMonthString(rp.getDateBeginningPractice()))
                .put("dateEnd", UtilPrintSupport.getDateFormatterWithMonthString(rp.getDateEndOfPractice()))
                .put("course", student.getCourse().getTitle())
                .put("developForm", UtilPrintSupport.getDevelopFormGen(student.getEducationOrgUnit().getDevelopForm()))
                .put("highSchool", UtilPrintSupport.getHighLevelSchoolTypeString(student.getEducationOrgUnit().getEducationLevelHighSchool()))
                .put("reason", rp.getRepresentationReason().getTitle())
                .put("practiceTitle", rp.getTitlePractice())
                .put("basement", rp.getRepresentationBasement().getTitle())
                //---------------------------------------------------------------------------------------------------------------------------
                //.put("supervisor", rp.getSupervisor() == null ? "" : rp.getSupervisor().getFullFio());
        		.put("supervisor", rp.getSupervisor() == null ? new RtfString().append("") : new RtfString().par().append("Руководитель практики: ").append(rp.getSupervisor().getFullFio()));
        		//---------------------------------------------------------------------------------------------------------------------------
        return im;
    }
}
