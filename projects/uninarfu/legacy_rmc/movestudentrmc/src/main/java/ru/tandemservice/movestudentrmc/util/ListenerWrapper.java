package ru.tandemservice.movestudentrmc.util;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.exception.ApplicationException;

public class ListenerWrapper<RESULT> {

    private Object listener;
    private RESULT result;
    private boolean ok = false;
    private Object param;

    public ListenerWrapper(Object listener) {
        this.listener = listener;
    }

    @SuppressWarnings("unchecked")
    public void process(boolean ok, RESULT result) {
        this.ok = ok;
        this.result = result;

        if (listener instanceof IBusinessComponent) {
            IBusinessComponent component = (IBusinessComponent) listener;
            ((IListener<RESULT>) component.getController()).listen(this);
        }
        else if (listener instanceof UIPresenter) {
            ((IListener<RESULT>) listener).listen(this);
        }
        else
            throw new ApplicationException("Неизвестный слушатель");
    }

    public RESULT getResult() {
        return result;
    }

    public void setResult(RESULT result) {
        this.result = result;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public Object getListener() {
        return listener;
    }

    public void setListener(Object listener) {
        this.listener = listener;
    }

    public Object getParam() {
        return param;
    }

    public void setParam(Object param) {
        this.param = param;
    }


    public static interface IListener<RESULT> {
        public void listen(ListenerWrapper<RESULT> wrapper);
    }
}
