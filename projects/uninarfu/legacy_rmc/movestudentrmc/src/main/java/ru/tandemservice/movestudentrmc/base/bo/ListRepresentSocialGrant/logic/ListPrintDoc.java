package ru.tandemservice.movestudentrmc.base.bo.ListRepresentSocialGrant.logic;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.AbstractListPrintDoc;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.Grant;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.ArrayList;
import java.util.List;

public class ListPrintDoc extends AbstractListPrintDoc<ListRepresentSocialGrant>
{

    public final String TEMPLATE_HEADER = "list.rep.socialGrant.header";
    public final String TEMPLATE_PARAG = "list.rep.socialGrant.parag";
    public final String TEMPLATE_ADDON = "list.rep.socialGrant.addon";

    @Override
    protected String getTemplateHeader()
    {
        return TEMPLATE_HEADER;
    }

    @Override
    protected String getTemplateParag()
    {
        return TEMPLATE_PARAG;
    }

    @Override
    protected String getTemplateAddon()
    {
        return TEMPLATE_ADDON;
    }

    @Override
    protected Class<? extends ListRepresent> getListRepresentClass()
    {
        return ListRepresentSocialGrant.class;
    }

    @Override
    protected void addonInjectModifier(RtfInjectModifier im, ListRepresentSocialGrant listRepresent, Student student)
    {
        injectModifier(im, listRepresent, student);
    }

    @Override
    protected void paragraphInjectModifier(RtfInjectModifier im, ListRepresentSocialGrant listRepresent, Student student)
    {
        injectModifier(im, listRepresent, student);
    }

    private void injectModifier(RtfInjectModifier im, ListRepresentSocialGrant listRepresent, Student student)
    {
        boolean isHigh = student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isHigh();

        im.put("ed_form_Genetive", student.getEducationOrgUnit().getDevelopForm().getGenCaseTitle().toLowerCase())
                .put("f_unit_Genetive", student.getEducationOrgUnit().getFormativeOrgUnit().getGenitiveCaseTitle().toLowerCase())
//			.put("vpo_spo", isHigh ? "высшего" : "среднего")
                .put("pay", student.getCompensationType().getCode().equals(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET) ? "за счет средств федерального бюджета" : student.getCompensationType().getShortTitle())
                .put("Spec_gr_Generive", listRepresent.getGrantView().getGenitive());
    }

    @Override
    protected List<String[]> getTableData(List<Student> studentList, List<Object[]> additionalList)
    {
        List<String[]> tableList = new ArrayList<>();
        int counter = 1;
        for (Student student : studentList)
        {
            Object[] data = additionalList.get(counter - 1);
            ListRepresentSocialGrant rep = (ListRepresentSocialGrant) data[1];

            String[] row = new String[6];
            row[0] = "" + counter++;
            row[1] = student.getBookNumber() != null ? student.getBookNumber() : "";
            row[2] = student.getPerson().getFullFio();
            row[3] = student.getCourse().getTitle();

            row[4] = RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(rep.getDateBeginingPayment()) + " - " + RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(rep.getDateEndOfPayment());
            row[5] = "" + Math.round((double) data[2]);

            tableList.add(row);
        }

        return tableList;
    }

    @Override
    protected String getKey(Student student, ListRepresentSocialGrant represent)
    {
        boolean isHigh = student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isHigh();

        return new StringBuilder()
                .append(student.getEducationOrgUnit().getDevelopForm().getId())
                .append(student.getCompensationType().getId())
                .append(represent.getGrantView().getId())
                .append(isHigh)
                .toString();
    }

    @Override
    protected void headerInjectModifier(RtfInjectModifier im, ListRepresentSocialGrant listRepresent)
    {
        //Наименование стипендии
        GrantView grantView = listRepresent.getGrantView();
        List<RelTypeGrantView> relTypeGrantViews = DataAccessServices.dao().getList(RelTypeGrantView.class, RelTypeGrantView.view(), grantView);
        Grant grant;
        try
        {
            grant = relTypeGrantViews.get(0).getGrant();
        } catch (Exception e)
        {
            throw new ApplicationException("Распечатать приказ невозможно, т.к. не найдена настройка стипендия для вида : \"" +
                    (grantView.getShortTitle() != null ? grantView.getShortTitle() : grantView.getTitle()) + "\"");
        }

        String grantTitle = relTypeGrantViews != null ? (grant.getGenitive() == null ? grant.getTitle() : grant.getGenitive()) : "-";

        im.put("Grant_name_Generive", grantTitle);
    }

    @Override
    protected void applyOrders(DQLSelectBuilder builder)
    {
        builder.joinEntity(
                "rel",
                DQLJoinType.inner,
                StudentGrantEntity.class,
                "sge",
                DQLExpressions.and(
                        DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudents.student().fromAlias("rel")), DQLExpressions.property(StudentGrantEntity.student().fromAlias("sge"))),
                        DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("rel")), DQLExpressions.property(StudentGrantEntity.listRepresent().fromAlias("sge")))
                ))
                .column(StudentGrantEntity.sum().fromAlias("sge").s());

        builder.order(DQLExpressions.property(ListRepresentSocialGrant.grantView().title().fromAlias("rep")))
                .order(DQLExpressions.property(RelListRepresentStudents.student().educationOrgUnit().developForm().shortTitle().fromAlias("rel")))
                .order(DQLExpressions.property(RelListRepresentStudents.student().compensationType().shortTitle().fromAlias("rel")));
        super.applyOrders(builder);
    }
}
