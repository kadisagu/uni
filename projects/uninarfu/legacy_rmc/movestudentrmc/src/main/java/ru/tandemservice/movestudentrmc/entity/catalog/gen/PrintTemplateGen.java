package ru.tandemservice.movestudentrmc.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.movestudentrmc.entity.catalog.TypeTemplateRepresent;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Печатные шаблоны
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PrintTemplateGen extends EntityBase
 implements INaturalIdentifiable<PrintTemplateGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate";
    public static final String ENTITY_NAME = "printTemplate";
    public static final int VERSION_HASH = 2132898705;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String L_TYPE = "type";
    public static final String P_PATH = "path";
    public static final String P_DOCUMENT = "document";
    public static final String P_EDIT_DATE = "editDate";
    public static final String P_COMMENT = "comment";
    public static final String L_TYPE_TEMPLATE = "typeTemplate";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private RepresentationType _type;     // Типы представлений
    private String _path;     // Путь в classpath до шаблона по умолчанию
    private byte[] _document;     // Шаблон в zip текущий
    private Date _editDate;     // Дата редактирования
    private String _comment;     // комментарий
    private TypeTemplateRepresent _typeTemplate;     // Тип шаблона представления
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Типы представлений.
     */
    public RepresentationType getType()
    {
        return _type;
    }

    /**
     * @param type Типы представлений.
     */
    public void setType(RepresentationType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Путь в classpath до шаблона по умолчанию.
     */
    @Length(max=255)
    public String getPath()
    {
        return _path;
    }

    /**
     * @param path Путь в classpath до шаблона по умолчанию.
     */
    public void setPath(String path)
    {
        dirty(_path, path);
        _path = path;
    }

    /**
     * @return Шаблон в zip текущий.
     */
    public byte[] getDocument()
    {
        return _document;
    }

    /**
     * @param document Шаблон в zip текущий.
     */
    public void setDocument(byte[] document)
    {
        dirty(_document, document);
        _document = document;
    }

    /**
     * @return Дата редактирования.
     */
    public Date getEditDate()
    {
        return _editDate;
    }

    /**
     * @param editDate Дата редактирования.
     */
    public void setEditDate(Date editDate)
    {
        dirty(_editDate, editDate);
        _editDate = editDate;
    }

    /**
     * @return комментарий.
     */
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * @return Тип шаблона представления.
     */
    public TypeTemplateRepresent getTypeTemplate()
    {
        return _typeTemplate;
    }

    /**
     * @param typeTemplate Тип шаблона представления.
     */
    public void setTypeTemplate(TypeTemplateRepresent typeTemplate)
    {
        dirty(_typeTemplate, typeTemplate);
        _typeTemplate = typeTemplate;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PrintTemplateGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((PrintTemplate)another).getCode());
            }
            setType(((PrintTemplate)another).getType());
            setPath(((PrintTemplate)another).getPath());
            setDocument(((PrintTemplate)another).getDocument());
            setEditDate(((PrintTemplate)another).getEditDate());
            setComment(((PrintTemplate)another).getComment());
            setTypeTemplate(((PrintTemplate)another).getTypeTemplate());
            setTitle(((PrintTemplate)another).getTitle());
        }
    }

    public INaturalId<PrintTemplateGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<PrintTemplateGen>
    {
        private static final String PROXY_NAME = "PrintTemplateNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof PrintTemplateGen.NaturalId) ) return false;

            PrintTemplateGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PrintTemplateGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PrintTemplate.class;
        }

        public T newInstance()
        {
            return (T) new PrintTemplate();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "type":
                    return obj.getType();
                case "path":
                    return obj.getPath();
                case "document":
                    return obj.getDocument();
                case "editDate":
                    return obj.getEditDate();
                case "comment":
                    return obj.getComment();
                case "typeTemplate":
                    return obj.getTypeTemplate();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "type":
                    obj.setType((RepresentationType) value);
                    return;
                case "path":
                    obj.setPath((String) value);
                    return;
                case "document":
                    obj.setDocument((byte[]) value);
                    return;
                case "editDate":
                    obj.setEditDate((Date) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "typeTemplate":
                    obj.setTypeTemplate((TypeTemplateRepresent) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "type":
                        return true;
                case "path":
                        return true;
                case "document":
                        return true;
                case "editDate":
                        return true;
                case "comment":
                        return true;
                case "typeTemplate":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "type":
                    return true;
                case "path":
                    return true;
                case "document":
                    return true;
                case "editDate":
                    return true;
                case "comment":
                    return true;
                case "typeTemplate":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "type":
                    return RepresentationType.class;
                case "path":
                    return String.class;
                case "document":
                    return byte[].class;
                case "editDate":
                    return Date.class;
                case "comment":
                    return String.class;
                case "typeTemplate":
                    return TypeTemplateRepresent.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PrintTemplate> _dslPath = new Path<PrintTemplate>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PrintTemplate");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Типы представлений.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate#getType()
     */
    public static RepresentationType.Path<RepresentationType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Путь в classpath до шаблона по умолчанию.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate#getPath()
     */
    public static PropertyPath<String> path()
    {
        return _dslPath.path();
    }

    /**
     * @return Шаблон в zip текущий.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate#getDocument()
     */
    public static PropertyPath<byte[]> document()
    {
        return _dslPath.document();
    }

    /**
     * @return Дата редактирования.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate#getEditDate()
     */
    public static PropertyPath<Date> editDate()
    {
        return _dslPath.editDate();
    }

    /**
     * @return комментарий.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @return Тип шаблона представления.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate#getTypeTemplate()
     */
    public static TypeTemplateRepresent.Path<TypeTemplateRepresent> typeTemplate()
    {
        return _dslPath.typeTemplate();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends PrintTemplate> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private RepresentationType.Path<RepresentationType> _type;
        private PropertyPath<String> _path;
        private PropertyPath<byte[]> _document;
        private PropertyPath<Date> _editDate;
        private PropertyPath<String> _comment;
        private TypeTemplateRepresent.Path<TypeTemplateRepresent> _typeTemplate;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(PrintTemplateGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Типы представлений.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate#getType()
     */
        public RepresentationType.Path<RepresentationType> type()
        {
            if(_type == null )
                _type = new RepresentationType.Path<RepresentationType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Путь в classpath до шаблона по умолчанию.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate#getPath()
     */
        public PropertyPath<String> path()
        {
            if(_path == null )
                _path = new PropertyPath<String>(PrintTemplateGen.P_PATH, this);
            return _path;
        }

    /**
     * @return Шаблон в zip текущий.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate#getDocument()
     */
        public PropertyPath<byte[]> document()
        {
            if(_document == null )
                _document = new PropertyPath<byte[]>(PrintTemplateGen.P_DOCUMENT, this);
            return _document;
        }

    /**
     * @return Дата редактирования.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate#getEditDate()
     */
        public PropertyPath<Date> editDate()
        {
            if(_editDate == null )
                _editDate = new PropertyPath<Date>(PrintTemplateGen.P_EDIT_DATE, this);
            return _editDate;
        }

    /**
     * @return комментарий.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(PrintTemplateGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @return Тип шаблона представления.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate#getTypeTemplate()
     */
        public TypeTemplateRepresent.Path<TypeTemplateRepresent> typeTemplate()
        {
            if(_typeTemplate == null )
                _typeTemplate = new TypeTemplateRepresent.Path<TypeTemplateRepresent>(L_TYPE_TEMPLATE, this);
            return _typeTemplate;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(PrintTemplateGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return PrintTemplate.class;
        }

        public String getEntityName()
        {
            return "printTemplate";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
