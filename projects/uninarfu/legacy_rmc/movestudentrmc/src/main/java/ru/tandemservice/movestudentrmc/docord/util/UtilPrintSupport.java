package ru.tandemservice.movestudentrmc.docord.util;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.RtfParagraph;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.movestudentrmc.base.bo.DOOrder.logic.OrderPrint;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.DORepresentBaseManager;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentQualificationThemes.logic.OrgUnitSelectedDSHandler;
import ru.tandemservice.movestudentrmc.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudentrmc.docord.IDOObjectByRepresentTypeManager;
import ru.tandemservice.movestudentrmc.entity.catalog.PrintTemplate;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentationTypeCodes;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.ReportRenderer;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;


public class UtilPrintSupport
{

    public static final String PRINT_EXTRACT_COLUMN_NAME = "printExtract";
    public static final String PRINT_EXTRACT_LISTENER = "onPrintExtract";
    public static final String PRINT_REPRESENT_COLUMN_NAME = "printRepresent";
    public static final String PRINT_REPRESENT_LISTENER = "onPrintRepresent";

    public static RtfDocument getTemplate(ICatalogItem type, String s)
    {

        DQLSelectBuilder builder = new DQLSelectBuilder();

        builder.fromEntity(PrintTemplate.class, "t").column("t");
        builder.where(DQLExpressions.eq(DQLExpressions.property(PrintTemplate.type().fromAlias("t")), DQLExpressions.value(type)));
        builder.where(DQLExpressions.eq(DQLExpressions.property(PrintTemplate.typeTemplate().code().fromAlias("t")), DQLExpressions.value(s)));

        List<PrintTemplate> templateList = DataAccessServices.dao().getList(builder);

        if (templateList.isEmpty()) {
            return RtfBean.getElementFactory().createRtfDocument();
        }

        PrintTemplate template = templateList.get(0);

        return new RtfReader().read(template.getContent());
    }

    public static RtfDocument getTemplate(String id, String s)
    {

        DQLSelectBuilder builder = new DQLSelectBuilder();

        builder.fromEntity(PrintTemplate.class, "t").column("t");
        builder.where(DQLExpressions.eq(DQLExpressions.property(PrintTemplate.code().fromAlias("t")), DQLExpressions.value(id)));
        builder.where(DQLExpressions.eq(DQLExpressions.property(PrintTemplate.typeTemplate().code().fromAlias("t")), DQLExpressions.value(s)));

        List<PrintTemplate> templateList = DataAccessServices.dao().getList(builder);

        if (templateList.isEmpty()) {
            return RtfBean.getElementFactory().createRtfDocument();
        }

        PrintTemplate template = templateList.get(0);

        return new RtfReader().read(template.getContent());
    }

    public static Student getStudent(Representation represent)
    {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocRepresentStudentBase.class, "stud").column("stud");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(DocRepresentStudentBase.representation().fromAlias("stud")),
                DQLExpressions.value(represent.getId())));

        List<DocRepresentStudentBase> studentList = DataAccessServices.dao().getList(builder);

        return studentList.get(0).getStudent();
    }

    public static String getStudentStr(Representation represent, GrammaCase accusative, Student student)
    {

        StringBuilder str = new StringBuilder();

        str.append(getStudentStrWithoutOSSP(accusative, student));


        if (represent.getOsspGrants() != null)
            if (!represent.getOsspGrants().getCode().equals("none") && !represent.getOsspGrants().getCode().equals("-")) {
                str.append(", ").append(represent.getOsspGrants().getTitle());
            }

        if (represent.getOsspGrants() != null)
            if (!represent.getOsspGrants().getCode().equals("none") && !represent.getOsspGrants().getCode().equals("-"))
                str.append(", ").append(represent.getOsspGrants().getTitle());

        return str.toString();
    }

    public static String getStudentStrWithoutGroup(Representation represent, GrammaCase accusative, Student student)
    {

        StringBuilder str = new StringBuilder();

        str.append(getStudentStrWithoutGroupOSO(accusative, student));

        if (represent.getOsspGrants() != null)
            if (!represent.getOsspGrants().getCode().equals("none") && !represent.getOsspGrants().getCode().equals("-")) {
                str.append(", ").append(represent.getOsspGrants().getTitle());
            }

        if (represent.getOsspGrants() != null)
            if (!represent.getOsspGrants().getCode().equals("none") && !represent.getOsspGrants().getCode().equals("-"))
                str.append(", ").append(represent.getOsspGrants().getTitle());

        return str.toString();
    }

    public static String getStudentStrWithoutOSSP(GrammaCase accusative, Student student)
    {

        IdentityCard identityCard = student.getPerson().getIdentityCard();

        StringBuilder str = new StringBuilder(getStudentStrWithoutGroupOSO(accusative, student));

        str.append(", ").append(DateFormatter.DATE_FORMATTER_JUST_YEAR.format(identityCard.getBirthDate())).append("г.р.");
        if (student.getGroup() != null)
            str.append(", ").append("гр.").append(student.getGroup().getTitle());
        str.append(" (").append(student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getInheritedOksoPrefix()).append(")");

        return str.toString();
    }

    public static String getStudentStrWithoutGroupOSO(GrammaCase accusative, Student student)
    {

        IdentityCard identityCard = student.getPerson().getIdentityCard();
        boolean isMaleSex = student.getPerson().isMale();

        StringBuilder str = new StringBuilder(PersonManager.instance().declinationDao().getDeclinationLastName(identityCard.getLastName(), accusative, isMaleSex).toUpperCase());
        str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationFirstName(identityCard.getFirstName(), accusative, isMaleSex));
        if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
            str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationMiddleName(identityCard.getMiddleName(), accusative, isMaleSex));

        return str.toString();
    }

    public static RtfString getBasics(Representation represent)
    {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocRepresentBasics.class, "basic").column("basic");
        builder.where(DQLExpressions.eq(
                DQLExpressions.property(DocRepresentBasics.representation().fromAlias("basic")),
                DQLExpressions.value(represent.getId())));

        List<DocRepresentBasics> basicsList = DataAccessServices.dao().getList(builder);

        RtfString str = new RtfString();

        List<DocRepresentBasics> basicList = new ArrayList<>();
        List<DocRepresentBasics> basicNotUnionList = new ArrayList<>();

        for (DocRepresentBasics basics : basicsList) {
            if (basics.getBasic().isMerged()) {
                if (!findBasicList(basicList, basics))
                    basicList.add(basics);
            }
            if (!basics.getBasic().isMerged())
                if (!findBasicList(basicNotUnionList, basics))
                    basicNotUnionList.add(basics);
        }

        boolean isFirst = true;
        for (DocRepresentBasics basic : basicList) {
            if (isFirst)
                isFirst = false;
            else
                str.append(", ");

            if (basic.getBasic().isPrintable()) {
                getBasicTitle(str, basic);
//				str.append(basic.getTitle());
                if (basic.getRepresentationBasementDate() != null || basic.getRepresentationBasementNumber() != null) {
                    if (represent instanceof IDiplomaRepresent)
                        str.append(" ").append(OrgUnitSelectedDSHandler.getBasicTitlePostfix(((IDiplomaRepresent) represent).getAcceptOrgUnitType(), ((IDiplomaRepresent) represent).getAcceptOrgUnit()));
                    //---------------------------------------------------------------------------------------------------------------------
                    /*
                    if (basic.getRepresentation() != null && basic.getRepresentation() instanceof RepresentDiplomAndExclude) {
                        str.append(" (протокол от ");
                    } else {
                        str.append(" (");
                    }
                    if (basic.getRepresentationBasementNumber() != null) {
                        str.append("№").append(IRtfData.SYMBOL_TILDE);
                        str.append(basic.getRepresentationBasementNumber());
                        if (basic.getRepresentationBasementDate() != null) {
                            str.append(" ");
                        }
                    }
                    if (basic.getRepresentationBasementDate() != null) {
                        str.append("от ");
                        getDateFormatterWithMonthStringAndYear(basic.getRepresentationBasementDate(), str);
                    }
                    */
                    
                    if (basic.getTitle().contains("решение")) {
                        str.append(" (протокол ");
                    } 
                    else {
                    	str.append(" ");
                    }                    
                    if (basic.getRepresentationBasementDate() != null) {
                        str.append("от ");
                        getDateFormatterWithMonthStringAndYear(basic.getRepresentationBasementDate(), str);
                        if(basic.getRepresentationBasementNumber() != null){
                        	str.append(" ");
                        }
                    }  
                    if (basic.getRepresentationBasementNumber() != null) {
                        str.append("№").append(IRtfData.SYMBOL_TILDE);
                        str.append(basic.getRepresentationBasementNumber());
                    }                    
                    //str.append(")");
                    if (basic.getTitle().contains("решение")) {
                        str.append(")");
                    }                    
                    //---------------------------------------------------------------------------------------------------------------------
                }
            }
        }
        for (DocRepresentBasics basic : basicNotUnionList) {
            if (isFirst)
                isFirst = false;
            else
                str.append(", ");

            if (basic.getBasic().isPrintable()) {
                getBasicTitle(str, basic);
//				str.append(basic.getTitle());
                if (basic.getRepresentationBasementDate() != null || basic.getRepresentationBasementNumber() != null) {
                	if (represent instanceof IDiplomaRepresent)
                        str.append(" ").append(OrgUnitSelectedDSHandler.getBasicTitlePostfix(((IDiplomaRepresent) represent).getAcceptOrgUnitType(), ((IDiplomaRepresent) represent).getAcceptOrgUnit()));
                	//--------------------------------------------------------------------------------------------------------------------
                    /*                	
                    str.append(" (");
                    if (basic.getRepresentationBasementNumber() != null) {
                        str.append("№").append(IRtfData.SYMBOL_TILDE);
                        str.append(basic.getRepresentationBasementNumber());
                        if (basic.getRepresentationBasementDate() != null) {
                            str.append(" ");
                        }
                    }
                    if (basic.getRepresentationBasementDate() != null) {
                        str.append("от ");
                        getDateFormatterWithMonthStringAndYear(basic.getRepresentationBasementDate(), str);
                    }
                    */
                    
                    if (basic.getTitle().contains("решение")) {
                        str.append(" (протокол ");
                    }
                    else {
                    	str.append(" ");
                    }
                    if (basic.getRepresentationBasementDate() != null) {
                        str.append("от ");
                        getDateFormatterWithMonthStringAndYear(basic.getRepresentationBasementDate(), str);
                        if(basic.getRepresentationBasementNumber() != null){
                        	str.append(" ");
                        }
                    }  
                    if (basic.getRepresentationBasementNumber() != null) {
                        str.append("№").append(IRtfData.SYMBOL_TILDE);
                        str.append(basic.getRepresentationBasementNumber());
                    }
                    //str.append(")");
                    if (basic.getTitle().contains("решение")) {
                        str.append(")");
                    }                    
                    //--------------------------------------------------------------------------------------------------------------------
                }
            }
        }

        return str;
    }

    public static void getBasicTitle(RtfString str, DocRepresentBasics basic)
    {
        if (basic.getBasic().isConnectedWithDocument() && basic.getDocument() != null) {
            NarfuDocument document = basic.getDocument();

            str.append(document.getSettings().getDocument().getTitle());
            if (document.getSettings().getDocument().isShowName() && !StringUtils.isEmpty(document.getName()))
                str.append(" ").append(document.getName());
            if (document.getSettings().getDocument().isIssuanceDateExist() && document.getIssuanceDate() != null) {
                str.append(" от ");
                getDateFormatterWithMonthStringAndYear(document.getIssuanceDate(), str);
            }
            if (document.getSettings().getDocument().isSeriaExist() && !StringUtils.isEmpty(document.getSeria()))
                str.append(" ").append(document.getSeria());
            if (document.getSettings().getDocument().isNumberExist() && !StringUtils.isEmpty(document.getNumber())) {
                str.append(" ").append("№").append(IRtfData.SYMBOL_TILDE).append(document.getNumber());
            }
        } else {
            str.append(basic.getTitle());
        }
    }

    public static boolean findBasicList(List<DocRepresentBasics> basicList, DocRepresentBasics basics)
    {
        boolean isFind = false;
        for (DocRepresentBasics basic : basicList) {
            if (basic.getBasic().equals(basics.getBasic())) {
                isFind = true;
                break;
            }
        }
        return isFind;
    }


    public static String getHeader(OrgUnit formativeOrgUnit)
    {

        if (null != formativeOrgUnit.getHead())
            return ((EmployeePost) formativeOrgUnit.getHead()).getEmployee().getPerson().getFio();
        else
            return "___________/_____________________/";

		/*
        StringBuilder result = new StringBuilder();
        for (EmployeePost employeePost : UniDaoFacade.getHeaderEmployeePostList(formativeOrgUnit)) {
            result.append(employeePost.getEmployee().getPerson().getFio()).append("\n");
        }
        return result.toString();
		 */
    }

    public static String getFacultyStr(OrgUnit orgUnit)
    {
        return "\\qc " + orgUnit.getTitleWithType().toUpperCase();
    }

    public static String getFacultyWithCodeStr(OrgUnit orgUnit)
    {

        return "По " + getStrOrgUnit(orgUnit, GrammaCase.DATIVE);
    }

    public static void setParNum(RtfDocument docBody, String itemFac)
    {

        new RtfInjectModifier()
                .put("ParagraphNumber", itemFac + ".")
                .modify(docBody);
    }

    /*
    public static String getParNum(int itemFac, Student student) {
        return student.getEducationOrgUnit().getFormativeOrgUnit().getRank().toString() + "." + ((Integer)itemFac).toString();
    }
     */
    public static IDocumentRenderer declarationRenderer(Representation represent)
    {
        //Получаем менеджер представления
        IDOObjectByRepresentTypeManager representManager = DORepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(represent.getType().getCode());
        //Получаем шаблон заявления
        RtfDocument docCommon = representManager.getDOObjectModifyDAO().getTemplateDeclaration(represent.getType());

        //Заполняем тело представления
        representManager.getDOObjectModifyDAO().buildBodyDeclaration(represent, docCommon);

        //representManager.getDOObjectModifyDAO().b
        return new ReportRenderer("Заявление.rtf", docCommon, false);
    }

    public static IDocumentRenderer representRenderer(Representation represent)
    {
        if (represent.getDocRepresent() == null) {
            return new ReportRenderer("Represent(draft).rtf", representRendererRtf(represent), false);
        } else
            return new ReportRenderer("Represent.rtf", represent.getDocRepresent(), false);
    }

    public static IDocumentRenderer extractRenderer(Representation representBase, Long studentId)
    {
        //проверим наличие текста выписки в БД
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(ExtractTextRelation.class, "r")
                .where(DQLExpressions.eqValue(DQLExpressions.property(ExtractTextRelation.order().representation().id().fromAlias("r")), representBase.getId()))
                .where(DQLExpressions.eqValue(DQLExpressions.property(ExtractTextRelation.student().id().fromAlias("r")), studentId));

        List<ExtractTextRelation> list = DataAccessServices.dao().getList(builder);

        if (!list.isEmpty()) {
            ExtractTextRelation relation = list.get(0);
            //--------------------------------------------------------------------
            //return new ReportRenderer("Extract.rtf", relation.getText(), false);
            
            //Исполнитель, печатающий выписку
            RtfDocument rtfDoc = RtfDocument.fromByteArray(relation.getText());
            UtilPrintSupport.injectExtractsExecutorTable(rtfDoc);
            return new ReportRenderer("Extract.rtf", rtfDoc, false);
            //--------------------------------------------------------------------
        } else {
        	//-----------------------------------------------------------------------------------------------------
            //return new ReportRenderer("Extract.rtf", extractRendererRtf(representBase, studentId), false);
        	
        	//Исполнитель, печатающий выписку
        	RtfDocument rtfDoc = extractRendererRtf(representBase, studentId);
            UtilPrintSupport.injectExtractsExecutorTable(rtfDoc);
            return new ReportRenderer("Extract.rtf", rtfDoc, false);
            //-----------------------------------------------------------------------------------------------------
        }
    }


    public static RtfDocument extractRendererRtf(Representation represent, DocumentOrder order, RtfDocument commonExtractTemplate,
                                                 RtfDocument representTemplate, OrderParagraphInfo paragraphInfo
    )
    {

        //Получаем менеджер представления
        IDOObjectByRepresentTypeManager representManager = DORepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(represent.getType().getCode());

        if (commonExtractTemplate == null)
            throw new ApplicationException("Не найден шаблон выписки для индивидуального приказа");

        if (order == null)
            throw new ApplicationException("Представлениие не включено в приказ.");

        //Заполняем тело представления
        representManager.getDOObjectModifyDAO().buildBodyRepresent(represent, representTemplate, new AtomicInteger(paragraphInfo.getParagraphNumber()), new AtomicInteger(paragraphInfo.getExtractNumber()));

        RtfSearchResult searchResult = UniRtfUtil.findRtfMark(commonExtractTemplate, "paragraph");
        if (searchResult.isFound()) {
            List<IRtfElement> elementList = representTemplate.getElementList();

            commonExtractTemplate.getElementList().addAll(searchResult.getIndex() + 2, elementList);

            new RtfInjectModifier()
                    .put("date_or", getDateFormatterWithMonthStringAndYear(order.getCommitDate()))
                    .put("num_or", order.getNumber())
                    .put("type", isSingleType(order) ? order.getRepresentationTypes() : "По личному составу обучающихся")

                    .modify(commonExtractTemplate);

        } else {
            throw new ApplicationException("В шаблоне выписки для индивидуального приказа не найден элемент paragraph, необходимый для создания текста выписки");
        }

        //Заполняем тело выписки
        representManager.getDOObjectModifyDAO().buildBodyExtract(represent, "", representTemplate);

        SharedRtfUtil.removeParagraphsWithTagsRecursive(commonExtractTemplate, Collections.singletonList("paragraph"), false, false);

        injectVisaTable(commonExtractTemplate, order);

        //----------------------------------------------------------
        //injectExtractsExecutorTable(commonExtractTemplate);
        //----------------------------------------------------------

        return commonExtractTemplate;
    }


    public static RtfDocument extractRendererRtf(Representation represent, Long studentId)
    {

        DQLSelectBuilder docbaseSelect = new DQLSelectBuilder()
                .fromEntity(DocRepresentStudentBase.class, "sb")
                .where(DQLExpressions.eqValue(DQLExpressions.property("sb", DocRepresentStudentBase.representation()), represent))
                .where(DQLExpressions.eqValue(DQLExpressions.property("sb", DocRepresentStudentBase.student().id()), studentId));

        DocRepresentStudentBase docBase = docbaseSelect.createStatement(DataAccessServices.dao().getComponentSession()).uniqueResult();

        //Получаем менеджер представления
        IDOObjectByRepresentTypeManager representManager = DORepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(represent.getType().getCode());
        RtfDocument commonExtractTemplate = getTemplate("modularExtractCommonTemplate", "common");

        DocumentOrder order = represent.getOrder();

        //Получаем шаблон представления
        RtfDocument representTemplate = representManager.getDOObjectModifyDAO().getTemplateRepresent(represent.getType());

        //Вызовем логику формирования приказа, для заполнения номеров параграфов
        OrderPrint.createPrintDocOrder(order, new ErrorCollector(), false);

        OrderParagraphInfo paragraphInfo = OrderPrint.studentParagInfoMap.get(docBase.getId());

        return extractRendererRtf(represent, order, commonExtractTemplate, representTemplate, paragraphInfo);
    }

    public static boolean isSingleType(DocumentOrder order)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(DocOrdRepresent.class, "doc")
                .where(DQLExpressions.eq(DQLExpressions.property(DocOrdRepresent.order().fromAlias("doc")), DQLExpressions.value(order)))
                .column(DQLExpressions.property(DocOrdRepresent.representation().type().title().fromAlias("doc")))
                .predicate(DQLPredicateType.distinct);

        List<String> result = IUniBaseDao.instance.get().getList(builder);
        return result.size() <= 1;
    }

    public static RtfDocument representRendererRtf(Representation represent)
    {

        //Получаем менеджер представления
        IDOObjectByRepresentTypeManager representManager = DORepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(represent.getType().getCode());
        //Получаем шаблон представления
        RtfDocument docCommon = representManager.getDOObjectModifyDAO().getTemplateRepresent(represent.getType());
        //Заполняем тело представления
        representManager.getDOObjectModifyDAO().buildBodyRepresent(represent, docCommon);

        //Получаем основу представления
        final RtfDocument docMain = UtilPrintSupport.getTemplate("representCommonHeader", "common");

        //Вставляем в основу тело
        RtfSearchResult searchResult = UniRtfUtil.findRtfMark(docMain, "Represent");
        if (searchResult.isFound()) {
            RtfParagraph rtfParagraph = new RtfParagraph();
            rtfParagraph.setElementList(docCommon.getElementList());
            searchResult.getElementList().set(searchResult.getIndex(), rtfParagraph);
        }

        return docMain;
    }

    public static String getStrOrgUnit(OrgUnit orgUnit, GrammaCase accusative)
    {
        if (orgUnit == null)
            return "";

        String title = orgUnit.getFullTitle();

        if (accusative.equals(GrammaCase.NOMINATIVE))
            return orgUnit.getNominativeCaseTitle() == null ? title : orgUnit.getNominativeCaseTitle();
        else if (accusative.equals(GrammaCase.GENITIVE))
            return orgUnit.getGenitiveCaseTitle() == null ? orgUnit.getFullTitle() : orgUnit.getGenitiveCaseTitle();
        else if (accusative.equals(GrammaCase.DATIVE))
            return orgUnit.getDativeCaseTitle() == null ? orgUnit.getFullTitle() : orgUnit.getDativeCaseTitle();
        else if (accusative.equals(GrammaCase.PREPOSITIONAL))
            return orgUnit.getPrepositionalCaseTitle() == null ? orgUnit.getFullTitle() : orgUnit.getPrepositionalCaseTitle();
        else
            return title;
    }

    public static void injectParNumModifier(RtfInjectModifier modifier, String itemFac)
    {

        modifier.put("ParagraphNumber", itemFac + ".");
    }

    public static void injectModifierDeclaration(RtfInjectModifier im, RtfTableModifier tm, DocRepresentStudentBase docRepresent)
    {
        Student student = docRepresent.getStudent();

        Representation representation = docRepresent.getRepresentation();

        im.put("studentSex2", student.getPerson().isMale() ? "обучающегося" : "обучающейся");
        im.put("course", docRepresent.getCourseStr());
        im.put("group", docRepresent.getGroupStr());
        im.put("inst", docRepresent.getFormativeOrgUnitGenetiveStr());
        im.put("specType", docRepresent.getEducationLevelHighSchoolTypeStr());
        im.put("numb_spec", "");//пока нет смысла заводить отдельное поле для номера
        im.put("spec", docRepresent.getEducationLevelHighSchoolStr());
        im.put("form", getDevelopFormGen(student.getEducationOrgUnit().getDevelopForm()));
        im.put("base", docRepresent.getCompensationTypeStr());
        im.put("F", PersonManager.instance().declinationDao().getDeclinationLastName(student.getPerson().getIdentityCard().getLastName(), GrammaCase.GENITIVE, student.getPerson().isMale()));
        im.put("I", PersonManager.instance().declinationDao().getDeclinationFirstName(student.getPerson().getIdentityCard().getFirstName(), GrammaCase.GENITIVE, student.getPerson().isMale()));
        im.put("O", PersonManager.instance().declinationDao().getDeclinationMiddleName(student.getPerson().getIdentityCard().getMiddleName(), GrammaCase.GENITIVE, student.getPerson().isMale()));

        Date startDate = docRepresent.getRepresentation().getStartDate();
        im.put("startDate", startDate != null ? getDateFormatterWithMonthStringAndYear(startDate) : new RtfString().append(""));
        im.put("date", getDateFormatterWithMonthStringAndYear(docRepresent.getRepresentation().getCreateDate()));

        im.put("inst2", docRepresent.getFormativeOrgUnitGenetiveStr());
        im.put("fioDirInst", docRepresent.getHeadTitleStr());

        for (int i = 1; i < 10; i++)
            im.put("female" + i, student.getPerson().isMale() ? "" : "а");//былА, отчисленА, ...
        for (int i = 1; i < 10; i++)
            im.put("femaleX" + i, student.getPerson().isMale() ? "ен" : "на");//согласен(на),

		/* Print Additional Documents in Representation */
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(DocRepresentStudentDocuments.class, "docs")
                .where(DQLExpressions.eq(DQLExpressions.property(DocRepresentStudentDocuments.representation().fromAlias("docs")), DQLExpressions.value(representation)));
        builder.column(DQLExpressions.property(DocRepresentStudentDocuments.studentDocuments().fromAlias("docs")));

        List<NarfuDocument> listAdditionalDocuments = builder.createStatement(DataAccessServices.dao().getComponentSession()).list();

        List<String[]> tableData = new ArrayList<>(listAdditionalDocuments.size());

        int counter = 1;

        if (listAdditionalDocuments.size() > 0) {
            im.put("additionalDocumentsText", "Прилагаю следующие документы:");
        } else {
            im.put("additionalDocumentsText", "");
        }
        for (NarfuDocument additionalDocument : listAdditionalDocuments) {
            String[] row = new String[4];
            row[0] = String.valueOf(counter++);
            row[1] = additionalDocument.getPrintTitle();
            row[2] = "от " + DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(additionalDocument.getIssuanceDate()) + " года";
            row[3] = "№ " + additionalDocument.getNumber();
            tableData.add(row);
        }

        tm.put("T", tableData.toArray(new String[tableData.size()][]));

		/**/
        String genderBasedDevelop = "обучающегося";
        if (!student.getPerson().isMale()) {
            genderBasedDevelop = "обучающейся";
        }
        EducationLevelsHighSchool highSchool = student.getEducationOrgUnit().getEducationLevelHighSchool();
        boolean isSpeciality = highSchool.isSpeciality() || highSchool.isSpecialization();
        im.put("genderBasedDevelop", genderBasedDevelop);
        im.put("specHighSchool", (isSpeciality ? "специальность" : "направление подготовки"));
        im.put("highSchool", docRepresent.getEducationLevelHighSchoolStr());

        /** Вывод отдельно ОКСО и специальности **/
        im.put("okso", docRepresent.getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getInheritedOksoPrefix());
        im.put("highSchool_without_okso", docRepresent.getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getTitleWithProfile());

        im.put("developForm", docRepresent.getDevelopFormStr());
        String developTermCode = student.getEducationOrgUnit().getDevelopCondition().getCode();
        if ("1".equals(developTermCode)) {
            developTermCode = "";
        } else if ("2".equals(developTermCode)) {
            developTermCode = " в сокращенные сроки " + student.getEducationOrgUnit().getDevelopPeriod().getTitle();
        } else if ("3".equals(developTermCode)) {
            developTermCode = " в ускоренные сроки " + student.getEducationOrgUnit().getDevelopPeriod().getTitle();
        } else if ("4".equals(developTermCode)) {
            developTermCode = " в сокращенные ускоренные сроки " + student.getEducationOrgUnit().getDevelopPeriod().getTitle();
        } else {
            developTermCode = "";
        }
        im.put("compensationType", docRepresent.getCompensationTypeStr() + " " + developTermCode);

        if (docRepresent.getRepresentation().getReason().isPrintable())
            im.put("reason", docRepresent.getRepresentation().getReason().getTitle());
        else
            im.put("reason", "");
    }

    public static void injectModifierRepresent(RtfInjectModifier modifier, Representation represent, Student student)
    {

        injectModifierExtract(modifier, represent);
        modifier.put("Student", getStudentStr(represent, getGrammaCase(represent), student));
        modifier.put("Dean", getHeader(student.getGroup().getEducationOrgUnit().getFormativeOrgUnit()));
        modifier.put("FacultyName", student.getGroup().getEducationOrgUnit().getFormativeOrgUnit().getShortTitle());
        modifier.put("Basics", getBasics(represent));
    }

    public static void injectModifierRepresent(RtfInjectModifier modifier, DocRepresentStudentBase docRepresent, Student student)
    {
        injectModifierRepresent(modifier, docRepresent, student, student.getEducationOrgUnit(), student.getCourse(), student.getGroup());
    }

    public static void injectModifierRepresent(RtfInjectModifier modifier, DocRepresentStudentBase docRepresent, Student student,
                                               EducationOrgUnit eou, Course course, Group group
    )
    {
        injectModifierExtract(modifier, docRepresent);
        Representation represent = docRepresent.getRepresentation();

        // modifier.put("studentTitle", getStudentStr(represent, getGrammaCase(represent), student));

        modifier.put("studentTitle", printSeparatedFIO(student.getPerson().getIdentityCard(), GrammaCase.GENITIVE, student.getPerson().isMale()));
        modifier.put("studentSex", student.getPerson().isMale() ? "студента" : "студентки");
        //------------------------------------------------------------------------------------
        modifier.put("studentSex_D", student.getPerson().isMale() ? "студенту" : "студентке");
        //------------------------------------------------------------------------------------
        modifier.put("course", course.getTitle());

        String groupStr = "";

        if (group != null) {
            if (represent.getType().getCode().equals(RepresentationTypeCodes.EXCLUDE_OUT))
                groupStr = "в " + group.getTitle() + " группе ";
            else
                groupStr = group.getTitle() + " группы ";
        }

        modifier.put("group", groupStr);
        modifier.put("formativeOrgUnit", "По " + eou.getFormativeOrgUnit().getDativeCaseTitle());
        modifier.put("developForm", getDevelopFormGen(eou.getDevelopForm()));
        modifier.put("highSchool", UtilPrintSupport.getHighLevelSchoolTypeString(eou.getEducationLevelHighSchool()));
        String developTermCode = eou.getDevelopCondition().getCode();
        if ("1".equals(developTermCode)) {
            developTermCode = "";
        } else if ("2".equals(developTermCode)) {
            developTermCode = " в сокращенные сроки " + eou.getDevelopPeriod().getTitle();
        } else if ("3".equals(developTermCode)) {
            developTermCode = " в ускоренные сроки";
        } else if ("4".equals(developTermCode)) {
            developTermCode = " в сокращенные ускоренные сроки " + eou.getDevelopPeriod().getTitle();
        } else {
            developTermCode = "";
        }

        IDOObjectByRepresentTypeManager representManager = DORepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(represent.getType().getCode());
        representManager.getDOObjectModifyDAO().getGrammaCase();

        String genderBasedDevelop = getLearningStr(representManager.getDOObjectModifyDAO().getGrammaCase(), student.getPerson().isMale());
        /*String genderBasedDevelop = "обучающегося";
        if(!student.getPerson().isMale()) {
			genderBasedDevelop = "обучающуюся";
		}
	*/
        /*
        String developPayment = "за счет средств федерального бюджета";
		if(1==1) {
			developPayment = "за собственный счет";
		}
		 */

        modifier.put("developPayment", genderBasedDevelop + " " + docRepresent.getCompensationTypeStr());
        modifier.put("developTermCode", developTermCode);


        modifier.put("Dean", getHeader(eou.getFormativeOrgUnit()));
        modifier.put("FacultyName", eou.getFormativeOrgUnit().getShortTitle());
        modifier.put("basics", getBasics(represent));
    }

    public static String getLearningStr(GrammaCase grammaCase, boolean male)
    {

        if (grammaCase.equals(GrammaCase.ACCUSATIVE)) {
            if (male)
                return "обучающегося";
            else
                return "обучающуюся";
        }
        if (grammaCase.equals(GrammaCase.DATIVE)) {
            if (male)
                return "обучающемуся";
            else
                return "обучающейся";
        }
        return "";
    }

    public static String getCompensationTypeStr(CompensationType type)
    {

        if (type.isBudget())
            return "за счет средств федерального бюджета";
        else
            return "с полным возмещением затрат";

    }

    public static void injectModifierExtract(RtfInjectModifier modifier, DocRepresentStudentBase docRepresent)
    {
        if (docRepresent.getRepresentation().getStartDate() != null) {
            modifier.put("startDate", getDateFormatterWithMonthString(docRepresent.getRepresentation().getStartDate()).append(IRtfData.SYMBOL_TILDE).append("года"));
        }
        //modifier.put("studentSex", startDate!=null ? (DateFormattingUtil.MONTH_STRING_DATE_FORMAT.format(startDate)+" года"):"");
        modifier.put("studentSex", docRepresent.getStudent().getPerson().isMale() ? "студента" : "студентки");
        //-----------------------------------------------------------------------------------------------------
        modifier.put("studentSex_D", docRepresent.getStudent().getPerson().isMale() ? "студенту" : "студентке");
        //-----------------------------------------------------------------------------------------------------

        if (docRepresent.getRepresentation().getReason().isPrintable())
            modifier.put("reason", docRepresent.getRepresentation().getReason().getTitle());
        else
            modifier.put("reason", "");
    }

    public static void injectModifierExtract(RtfInjectModifier modifier, Representation represent)
    {
        modifier.put("DateStart", getDateFormatterWithMonthString(represent.getStartDate()));
        if (represent.getReason().isPrintable())
            modifier.put("Reason", represent.getReason().getTitle().trim());
        else
            modifier.put("Reason", "");
    }

    public static GrammaCase getGrammaCase(Representation represent)
    {

        //Получаем менеджер представления
        IDOObjectByRepresentTypeManager representManager = DORepresentBaseManager.instance().doObjectByRepresentTypeExtPoint().getItems().get(represent.getType().getCode());
        return representManager.getDOObjectModifyDAO().getGrammaCase();
        //return null;
    }

    public static List<DocListRepresentBasics> getListRepresentBasic(ListRepresent listRepresent)
    {
        DQLSelectBuilder builderBasement = new DQLSelectBuilder().fromEntity(DocListRepresentBasics.class, "b").column("b");
        builderBasement.where(DQLExpressions.eq(
                DQLExpressions.property(DocListRepresentBasics.listRepresent().fromAlias("b")),
                DQLExpressions.value(listRepresent)));

        return builderBasement.createStatement(DataAccessServices.dao().getComponentSession()).list();
    }

    public static String getListRepresentBasicTitle(ListRepresent listRepresent)
    {

        StringBuilder base = new StringBuilder();
        base.append(listRepresent.getRepresentationBasement().getTitle());

        List<DocListRepresentBasics> representBasicsList = getListRepresentBasic(listRepresent);

        DocListRepresentBasics basic = null;

        if (representBasicsList.size() > 0) {
            basic = representBasicsList.get(0);
        }

        if (basic != null) {
            if (basic.getRepresentationBasementDate() != null || basic.getRepresentationBasementNumber() != null) {
                base.append(" (");
                if (basic.getRepresentationBasementDate() != null) {
                    base.append("дата основания ");
                    base.append(DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(basic.getRepresentationBasementDate())).append(" года");
                    if (basic.getRepresentationBasementNumber() != null) {
                        base.append(", ");
                    }
                }
                if (basic.getRepresentationBasementNumber() != null) {
                    base.append("№ основания ");
                    base.append(basic.getRepresentationBasementNumber());
                }
                base.append(")");
            }
        }
        return base.toString();
    }

    public static Collection<String> getBasement(ListRepresent listRepresent)
    {
        TreeSet<String> basics = new TreeSet<>();
        for (DocListRepresentBasics representBasics : UtilPrintSupport.getListRepresentBasic(listRepresent)) {
            StringBuilder title = new StringBuilder();
            //----------------------------------------------------------------------
            /*
            title.append(representBasics.getBasic().getTitle());
            if (listRepresent instanceof IDiplomaRepresent)
                title.append(OrgUnitSelectedDSHandler.getBasicTitlePostfix(
                        ((IDiplomaRepresent) listRepresent).getAcceptOrgUnitType(),
                        ((IDiplomaRepresent) listRepresent).getAcceptOrgUnit()));
            */
            
            title.append(representBasics.getBasic().getTitle().trim());
            if (listRepresent instanceof IDiplomaRepresent){
            	title.append(" ");
                title.append(OrgUnitSelectedDSHandler.getBasicTitlePostfix(
                        ((IDiplomaRepresent) listRepresent).getAcceptOrgUnitType(),
                        ((IDiplomaRepresent) listRepresent).getAcceptOrgUnit()));
            }
            
            //-----------------------------------------------------------------------

            title.append(getRepresentBasicDateAndNumber(representBasics));
            basics.add(title.toString());
        }

        return basics;
    }

    public static String getBasementTitles(List<? extends ListRepresent> listOfRepresents)
    {
        TreeSet<String> basics = new TreeSet<>();
        for (ListRepresent listRepresent : listOfRepresents)
            basics.addAll(getBasement(listRepresent));

        return StringUtils.join(basics.toArray(), ", ");
    }

    private static String getRepresentBasicDateAndNumber(DocListRepresentBasics representBasics)
    {
    	//-----------------------------------------------------------------------------------------------------------------------------------------
        /*
    	String date = representBasics.getRepresentationBasementDate() != null ?
                "протокол от " +  DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(representBasics.getRepresentationBasementDate())+ " года" : "";
        String number = representBasics.getRepresentationBasementNumber() != null ?
                "№" + representBasics.getRepresentationBasementNumber() : "";
        if (representBasics.getRepresentationBasementDate() != null || representBasics.getRepresentationBasementNumber() != null)
            return " ("
                    + date
                    + (date.length() > 0 && number.length() > 0 ? " " : "")
                    + number
                    + ")";                
        */        
    	
    	StringBuilder dateStringBuilder= new StringBuilder();
        if(representBasics.getRepresentationBasementDate() != null){
        	dateStringBuilder.append(representBasics.getBasic().getTitle().contains("решение") ? "протокол от " : "от ");
        	dateStringBuilder.append(DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(representBasics.getRepresentationBasementDate()));
        	dateStringBuilder.append(" года");
        }
        else{
        	dateStringBuilder.append("");
        }

        String date = dateStringBuilder.toString();
        String number = representBasics.getRepresentationBasementNumber() != null ?
                "№ " + representBasics.getRepresentationBasementNumber() : "";
                
        if (representBasics.getRepresentationBasementDate() != null || representBasics.getRepresentationBasementNumber() != null) {
        	
        	if(representBasics.getBasic().getTitle().contains("решение")) {
                return " ("
                        + date
                        + (date.length() > 0 && number.length() > 0 ? " " : "")
                        + number
                        + ")";        		
        	}
        	else {
                return " "
                        + date
                        + (date.length() > 0 && number.length() > 0 ? " " : "")
                        + number;        		
        	}
        }
        //-----------------------------------------------------------------------------------------------------------------------------------------

        return "";
    }

    public static String getHighLevelSchoolTypeString(EducationLevels level)
    {
        while (level != null) {
            if (level.getLevelType().getTitle().contains("специальность"))
                return "специальность";

            level = level.getParentLevel();
        }
        return "направление подготовки";
    }


    public static RtfString getHighLevelSchoolTypeString(EducationLevelsHighSchool level)
    {
        return MoveStudentDaoFacade.getSpecificPrintTitle().getSpecificPrintTitle(level);
    }


    public static RtfString printSeparatedFIO(IdentityCard card, GrammaCase grammaCase, Boolean isMaleSex)
    {
        RtfString rtfString = new RtfString();
        rtfString
                .append(PersonManager.instance().declinationDao().getDeclinationLastName(card.getLastName(), grammaCase, isMaleSex))
                .append(IRtfData.PAR)
                .append(PersonManager.instance().declinationDao().getDeclinationFirstName(card.getFirstName(), grammaCase, isMaleSex));

        if (card.getMiddleName() != null)
            rtfString
                    .append(" ")
                    .append(PersonManager.instance().declinationDao().getDeclinationMiddleName(card.getMiddleName(), grammaCase, isMaleSex));

        return rtfString;
    }

    public static void injectVisaTable(RtfDocument document, DocumentOrder order)
    {
        RtfTableModifier tm = new RtfTableModifier();

        List<String[]> tableData = new ArrayList<>();

        if (hasForeignersStudents(order)) {
            String postTitle = getForeignerVisaPostTitle();
            String postFio = getForeignerVisaPostFio();
            if (!StringUtils.isEmpty(postTitle) || !StringUtils.isEmpty(postFio)) {
                tableData.add(new String[]{StringUtils.trimToEmpty(postTitle), null, StringUtils.trimToEmpty(postFio)});
            }
        }

        if (order.getPostTitle1() != null || order.getPostFio1() != null) {
            tableData.add(new String[]{StringUtils.trimToEmpty(order.getPostTitle1()), null, StringUtils.trimToEmpty(order.getPostFio1())});
        }

        if (order.getPostTitle2() != null || order.getPostFio2() != null) {
            tableData.add(new String[]{StringUtils.trimToEmpty(order.getPostTitle2()), null, StringUtils.trimToEmpty(order.getPostFio2())});
        }


        if (order.getPostTitle3() != null || order.getPostFio3() != null) {
            tableData.add(new String[]{StringUtils.trimToEmpty(order.getPostTitle3()), null, StringUtils.trimToEmpty(order.getPostFio3())});
        }

        if (order.getPostTitle4() != null || order.getPostFio4() != null) {
            tableData.add(new String[]{StringUtils.trimToEmpty(order.getPostTitle4()), null, StringUtils.trimToEmpty(order.getPostFio4())});
        }

        if (order.getPostTitle5() != null || order.getPostFio5() != null) {
            tableData.add(new String[]{StringUtils.trimToEmpty(order.getPostTitle5()), null, StringUtils.trimToEmpty(order.getPostFio5())});
        }

        if (order.getPostTitle6() != null || order.getPostFio6() != null) {
            tableData.add(new String[]{StringUtils.trimToEmpty(order.getPostTitle6()), null, StringUtils.trimToEmpty(order.getPostFio6())});
        }

        tm.put("postTitle1", tableData.toArray(new String[tableData.size()][]));

        tableData = new ArrayList<>();

        if (order.getSignPost() != null || order.getSignFio() != null) {
            tableData.add(new String[]{StringUtils.trimToEmpty(order.getSignPost()), StringUtils.trimToEmpty(order.getSignFio())});
        }

        tm.put("signPost", tableData.toArray(new String[tableData.size()][]));

        tm.modify(document);

    }

    public static void injectExtractsExecutorTable(RtfDocument document)
    {

        IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();

        String fio = "";
        String post = "";

        if (principalContext instanceof EmployeePost) {
            post = ((EmployeePost) principalContext).getPostRelation().getPostBoundedWithQGandQL().getTitle();
            fio = ((EmployeePost) principalContext).getPerson().getIdentityCard().getIof();
        }

        RtfInjectModifier im = new RtfInjectModifier();

        im.put("executorPost", post)
                .put("executorFIO", fio);

        im.modify(document);

    }

    public static void injectVisaTable(RtfDocument document, ListOrder order)
    {
        RtfTableModifier tm = new RtfTableModifier();

        List<String[]> tableData = new ArrayList<>();

        if (hasForeignersStudents(order)) {
            String postTitle = getForeignerVisaPostTitle();
            String postFio = getForeignerVisaPostFio();
            if (!StringUtils.isEmpty(postTitle) || !StringUtils.isEmpty(postFio)) {
                tableData.add(new String[]{StringUtils.trimToEmpty(postTitle), null, StringUtils.trimToEmpty(postFio)});
            }
        }

        if (order.getPostTitle1() != null || order.getPostFio1() != null) {
            tableData.add(new String[]{StringUtils.trimToEmpty(order.getPostTitle1()), null, StringUtils.trimToEmpty(order.getPostFio1())});
        }

        if (order.getPostTitle2() != null || order.getPostFio2() != null) {
            tableData.add(new String[]{StringUtils.trimToEmpty(order.getPostTitle2()), null, StringUtils.trimToEmpty(order.getPostFio2())});
        }


        if (order.getPostTitle3() != null || order.getPostFio3() != null) {
            tableData.add(new String[]{StringUtils.trimToEmpty(order.getPostTitle3()), null, StringUtils.trimToEmpty(order.getPostFio3())});
        }

        if (order.getPostTitle4() != null || order.getPostFio4() != null) {
            tableData.add(new String[]{StringUtils.trimToEmpty(order.getPostTitle4()), null, StringUtils.trimToEmpty(order.getPostFio4())});
        }

        if (order.getPostTitle5() != null || order.getPostFio5() != null) {
            tableData.add(new String[]{StringUtils.trimToEmpty(order.getPostTitle5()), null, StringUtils.trimToEmpty(order.getPostFio5())});
        }

        if (order.getPostTitle6() != null || order.getPostFio6() != null) {
            tableData.add(new String[]{StringUtils.trimToEmpty(order.getPostTitle6()), null, StringUtils.trimToEmpty(order.getPostFio6())});
        }

        tm.put("postTitle1", tableData.toArray(new String[tableData.size()][]));

        tableData = new ArrayList<>();

        if (order.getSignPost() != null || order.getSignFio() != null) {
            tableData.add(new String[]{StringUtils.trimToEmpty(order.getSignPost()), StringUtils.trimToEmpty(order.getSignFio())});
        }

        tm.put("signPost", tableData.toArray(new String[tableData.size()][]));

        tm.modify(document);

    }

    public static boolean hasForeignersStudents(ListOrder order)
    {

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(ListOrdListRepresent.class, "lor")
                .joinEntity("lor", DQLJoinType.inner, RelListRepresentStudents.class, "stud", DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudents.representation().id().fromAlias("stud")), DQLExpressions.property(ListOrdListRepresent.representation().id().fromAlias("lor"))))
                .where(DQLExpressions.eq(DQLExpressions.property(ListOrdListRepresent.order().fromAlias("lor")), DQLExpressions.value(order)))
                .where(DQLExpressions.ne(DQLExpressions.property(RelListRepresentStudents.student().person().identityCard().citizenship().code().fromAlias("stud")), "0"))// 0 - код РФ
                .column(DQLExpressions.property(RelListRepresentStudents.student().fromAlias("stud")));

        return IUniBaseDao.instance.get().getCount(builder) > 0;
    }

    public static boolean hasForeignersStudents(DocumentOrder order)
    {

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(DocOrdRepresent.class, "doc")
                .joinEntity("doc", DQLJoinType.inner, DocRepresentStudentBase.class, "stud", DQLExpressions.eq(DQLExpressions.property(DocRepresentStudentBase.representation().id().fromAlias("stud")), DQLExpressions.property(DocOrdRepresent.representation().id().fromAlias("doc"))))
                .where(DQLExpressions.eq(DQLExpressions.property(DocOrdRepresent.order().fromAlias("doc")), DQLExpressions.value(order)))
                .where(DQLExpressions.ne(DQLExpressions.property(DocRepresentStudentBase.student().person().identityCard().citizenship().code().fromAlias("stud")), "0"))// 0 - код РФ
                .column(DQLExpressions.property(DocRepresentStudentBase.student().fromAlias("stud")));

        return IUniBaseDao.instance.get().getCount(builder) > 0;
    }

    public static String getForeignerVisaPostTitle()
    {
        IDataSettings settings = DataSettingsFacade.getSettings("foreignerVisa");
        Object post = settings.get("postTitle");
        if (post instanceof String)
            return String.valueOf(post);
        else
            return null;

    }

    public static String getForeignerVisaPostFio()
    {
        IDataSettings settings = DataSettingsFacade.getSettings("foreignerVisa");
        Object fio = settings.get("postFio");
        if (fio instanceof String)
            return String.valueOf(fio);
        else
            return null;
    }

    public static void injectBranchLabels(RtfDocument template, List<OrgUnit> orgUnitList)
    {
        LinkedHashSet<OrgUnit> branchOrgUnits = new LinkedHashSet<>();

        RtfInjectModifier injectModifier = new RtfInjectModifier();

        for (OrgUnit orgUnit : orgUnitList) {

            OrgUnit brachOU = null;
            //Если филиал
            if (orgUnit.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.BRANCH)) {
                brachOU = orgUnit;
            } else if (orgUnit.getParent() != null && orgUnit.getParent().getOrgUnitType().getCode().equals(OrgUnitTypeCodes.BRANCH)) {
                brachOU = orgUnit.getParent();
            }

            branchOrgUnits.add(brachOU);
        }

        /**
         * Шапка для филиалов
         */
        String orgUnitCity = "г. Архангельск";
        //Только если все подразделения одного типа и  = "Филиал"
        OrgUnit brachOU = branchOrgUnits.size() == 1 ? branchOrgUnits.iterator().next() : null;


        if (brachOU != null) {
            injectModifier.put("parentOrgUnitTitle", brachOU.getTitle().toUpperCase());
            AddressDetailed address = brachOU.getAddress();
            orgUnitCity = address != null ? "г. " + address.getSettlement().getTitle() : "null";
        } else
            SharedRtfUtil.removeParagraphsWithTagsRecursive(template, Collections.singletonList("parentOrgUnitTitle"), false, false);

        injectModifier.put("orgUnitCity", orgUnitCity);
        injectModifier.modify(template);
    }

    public static String getDevelopFormGen(DevelopForm developForm)
    {
    	//------------------------------------------------
        //String code = developForm.getCode();

        //if (code.equals(DevelopFormCodes.PART_TIME_FORM))
            //return "очно-заочной (вечерней)";
        //else
            //return developForm.getGenCaseTitle();
        
    	return developForm.getGenCaseTitle();
        //------------------------------------------------

    }

    public static RtfString getDateFormatterWithMonthString(Date date, RtfString rtfString, GrammaCase grammaCase)
    {
        String day = RussianDateFormatUtils.getDayString(date, true);
        String month = CommonBaseDateUtil.getMonthNameDeclined(date, grammaCase);
        String year = RussianDateFormatUtils.getYearString(date, false);

        rtfString.append(day)
                .append(IRtfData.SYMBOL_TILDE)
                .append(month)
                .append(" ")
                .append(year);

        return rtfString;
    }

    public static RtfString getDateFormatterWithMonthString(Date date, RtfString rtfString)
    {
        return getDateFormatterWithMonthString(date, rtfString, GrammaCase.GENITIVE);
    }

    public static RtfString getDateFormatterWithMonthString(Date date, GrammaCase grammaCase)
    {
        RtfString retVal = new RtfString();
        return getDateFormatterWithMonthString(date, retVal, grammaCase);
    }

    public static RtfString getDateFormatterWithMonthString(Date date)
    {
        RtfString retVal = new RtfString();
        return getDateFormatterWithMonthString(date, retVal);
    }

    public static RtfString getDateFormatterWithMonthStringAndYear(Date date)
    {
        return getDateFormatterWithMonthString(date).append(IRtfData.SYMBOL_TILDE).append("года");
    }

    public static RtfString getDateFormatterWithMonthStringAndYear(Date date, GrammaCase grammaCase)
    {
        return getDateFormatterWithMonthString(date, grammaCase).append(IRtfData.SYMBOL_TILDE).append("года");
    }

    public static RtfString getDateFormatterWithMonthStringAndYear(Date date, RtfString rtfString)
    {
        return getDateFormatterWithMonthString(date, rtfString).append(IRtfData.SYMBOL_TILDE).append("года");
    }

    public static RtfString getDateFormatterWithMonthStringAndYear(Date date, RtfString rtfString, GrammaCase grammaCase)
    {
        return getDateFormatterWithMonthString(date, rtfString, grammaCase).append(IRtfData.SYMBOL_TILDE).append("года");
    }

    public static RtfString getDisplayableShortTitle(EducationLevelsHighSchool highSchool)
    {
        RtfString result = new RtfString().append(highSchool.getEducationLevel().getInheritedOksoPrefix());
        if (highSchool.getShortTitle() != null)
            result.append(IRtfData.SYMBOL_TILDE).append(highSchool.getShortTitle());
        return result;
    }

    public static String getDaysStr(int days)
    {

        String str = String.valueOf(days);
        char lastChar = str.charAt(str.length() - 1);
        int last = Integer.valueOf(String.valueOf(lastChar));

        if (days == 1 || (last == 1 && days > 20))
            return "календарный день";

        if ((days >= 2 && days <= 4) || (last >= 2 && last <= 4 && days > 20))
            return "календарных дня";

        if ((days >= 5 && days <= 20) || (last >= 5 && last <= 9) || (last == 0))
            return "календарных дней";

        return "";
    }

    public static String getEduLevelCodeWithQualification(EducationLevels levels)
    {
        String code = "";
        if (levels.getEduProgramSubject() != null) {
            EduProgramSubject eduProgramSubject = levels.getEduProgramSubject();
            code = StringUtils.trimToEmpty(eduProgramSubject.getSubjectCode());
            if (!StringUtils.isEmpty(code) && eduProgramSubject.getSubjectIndex().getTitle().contains("2009"))   //У перечней 2009 нет кодов квалификации - прибавляем
                code += "." + levels.getQualification().getCode();
            code += " ";
        }
        return code;
    }


    public static String getParentTitleIfProfile(EducationLevelsHighSchool educationLevelsHighSchool)
    {
        EducationLevels level = educationLevelsHighSchool.getEducationLevel();
        StructureEducationLevels levelType = level.getLevelType();
        return (levelType.isSpecialization()) || (levelType.isProfile()) ? level.getParentLevel().getTitle() : educationLevelsHighSchool.getTitle();
    }

}
