package ru.tandemservice.movestudentrmc.base.bo.GrantsRelationSettings.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.movestudentrmc.entity.RelTypeGrantView;
import ru.tandemservice.movestudentrmc.entity.catalog.Grant;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantType;

import java.util.List;

public interface IRelationSettingsModifyDAO extends INeedPersistenceSupport {

    void saveGrantType(GrantType grantType, List<FakeGrantsHandler> fakeGrantsList);

    void updateGrantType(GrantType grantType, List<FakeGrantsHandler> fakeGrantsList, List<RelTypeGrantView> typeGrantViewList);

    void updateRelations(GrantType grantType, Grant grant, List<RelTypeGrantView> relList);
}
