package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.BusinessCommand;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class TerritorialOrgUnitDSHandler extends DefaultComboDataSourceHandler {

    public TerritorialOrgUnitDSHandler(String ownerId) {

        super(ownerId, OrgUnit.class);
    }


    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        String filter = ep.input.getComboFilterByValue();
        ep.dqlBuilder.where(eq(property(OrgUnit.orgUnitType().code().fromAlias("e")), value("institute")));


        if (StringUtils.isNotBlank(filter)) {
            ep.dqlBuilder.where(like(DQLFunctions.upper(property(OrgUnit.title().fromAlias("e"))), value(CoreStringUtils.escapeLike(filter, true))));
        }


    }

    @Override
    protected void doAfterExecute(BusinessCommand<DSInput, DSOutput> arg0,
                                  DSOutput arg1, ExecutionContext arg2)
    {

        super.doAfterExecute(arg0, arg1, arg2);
        ListResult findValues = new OrgUnitKindAutocompleteModel("3", null).findValues("");
        arg1.setRecordList(findValues.getObjects());
        setPopupListSize(100);
    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
    {
        setOrderByProperty(OrgUnit.P_SHORT_TITLE);
        //ep.dqlBuilder.order("e." + OrgUnit.orgUnitType().title());
    }
}
