package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.Grant2Grant;
import ru.tandemservice.movestudentrmc.entity.catalog.Grant;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Свзяь стипендия - стипендия
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class Grant2GrantGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.Grant2Grant";
    public static final String ENTITY_NAME = "grant2Grant";
    public static final int VERSION_HASH = 204526944;
    private static IEntityMeta ENTITY_META;

    public static final String L_FIRST = "first";
    public static final String L_SECOND = "second";

    private Grant _first;     // Первичная стипендия
    private Grant _second;     // Вторичная стипендия

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Первичная стипендия. Свойство не может быть null.
     */
    @NotNull
    public Grant getFirst()
    {
        return _first;
    }

    /**
     * @param first Первичная стипендия. Свойство не может быть null.
     */
    public void setFirst(Grant first)
    {
        dirty(_first, first);
        _first = first;
    }

    /**
     * @return Вторичная стипендия. Свойство не может быть null.
     */
    @NotNull
    public Grant getSecond()
    {
        return _second;
    }

    /**
     * @param second Вторичная стипендия. Свойство не может быть null.
     */
    public void setSecond(Grant second)
    {
        dirty(_second, second);
        _second = second;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof Grant2GrantGen)
        {
            setFirst(((Grant2Grant)another).getFirst());
            setSecond(((Grant2Grant)another).getSecond());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends Grant2GrantGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Grant2Grant.class;
        }

        public T newInstance()
        {
            return (T) new Grant2Grant();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "first":
                    return obj.getFirst();
                case "second":
                    return obj.getSecond();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "first":
                    obj.setFirst((Grant) value);
                    return;
                case "second":
                    obj.setSecond((Grant) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "first":
                        return true;
                case "second":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "first":
                    return true;
                case "second":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "first":
                    return Grant.class;
                case "second":
                    return Grant.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Grant2Grant> _dslPath = new Path<Grant2Grant>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Grant2Grant");
    }
            

    /**
     * @return Первичная стипендия. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.Grant2Grant#getFirst()
     */
    public static Grant.Path<Grant> first()
    {
        return _dslPath.first();
    }

    /**
     * @return Вторичная стипендия. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.Grant2Grant#getSecond()
     */
    public static Grant.Path<Grant> second()
    {
        return _dslPath.second();
    }

    public static class Path<E extends Grant2Grant> extends EntityPath<E>
    {
        private Grant.Path<Grant> _first;
        private Grant.Path<Grant> _second;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Первичная стипендия. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.Grant2Grant#getFirst()
     */
        public Grant.Path<Grant> first()
        {
            if(_first == null )
                _first = new Grant.Path<Grant>(L_FIRST, this);
            return _first;
        }

    /**
     * @return Вторичная стипендия. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.Grant2Grant#getSecond()
     */
        public Grant.Path<Grant> second()
        {
            if(_second == null )
                _second = new Grant.Path<Grant>(L_SECOND, this);
            return _second;
        }

        public Class getEntityClass()
        {
            return Grant2Grant.class;
        }

        public String getEntityName()
        {
            return "grant2Grant";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
