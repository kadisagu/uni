package ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.BasementRepresentDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.GrantViewDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.ReasonRepresentDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.logic.dsHandlers.TypeRepresentDSHandler;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;


@Configuration
public class ListRepresentBaseAdd extends BusinessComponentManager {

    public static final String TYPE_REPRESENTATION_DS = "typeRepresentationDS";
    public static final String REASON_REPRESENT_DS = "reasonRepresentDS";
    public static final String BASEMENT_REPRESENT_DS = "basementRepresentDS";

    public static final String GRANT_VIEW_DS = "grantViewDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(TYPE_REPRESENTATION_DS, typeRepresentationDSHandler()))
                .addDataSource(selectDS(REASON_REPRESENT_DS, reasonRepresentDSHandler()))
                .addDataSource(selectDS(BASEMENT_REPRESENT_DS, basementRepresentDSHandler()))
                .addDataSource(selectDS(GRANT_VIEW_DS, grantViewDSHandler()).addColumn(GrantView.shortTitle().s()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler typeRepresentationDSHandler()
    {
        return (IDefaultComboDataSourceHandler) new TypeRepresentDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> reasonRepresentDSHandler()
    {
        return new ReasonRepresentDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> basementRepresentDSHandler()
    {
        return new BasementRepresentDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> grantViewDSHandler() {
        return new GrantViewDSHandler(getName(), "shortTitle");
    }
}
