package ru.tandemservice.movestudentrmc.base.bo.DORepresentDiplomAndExclude.ui.Edit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.block.BlockListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic.ReasonDSHandler;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.ui.EditBasic.DORepresentBaseEditBasic;

@Configuration
public class DORepresentDiplomAndExcludeEdit extends BusinessComponentManager {

    public static final String REASON_DS = "reasonDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(REASON_DS, reasonDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler reasonDSHandler()
    {
        return (IDefaultComboDataSourceHandler) new ReasonDSHandler(getName());
    }

    @Bean
    public BlockListExtPoint editBasicBlockListExtPoint()
    {
        return blockListExtPointBuilder(DORepresentDiplomAndExcludeEditUI.EDIT_BASIC_BLOCK_LIST)
                .addBlock(componentBlock(DORepresentDiplomAndExcludeEditUI.EDIT_BASIC_BLOCK, DORepresentBaseEditBasic.class).parameters("ui:blockParam"))
                .create();
    }
}
