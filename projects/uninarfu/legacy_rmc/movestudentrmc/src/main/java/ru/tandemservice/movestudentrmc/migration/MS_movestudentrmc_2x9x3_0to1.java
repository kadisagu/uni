package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings("unused")
public class MS_movestudentrmc_2x9x3_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies() {
        return new ScriptDependency[] {
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.3"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.9.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception {
		// сущность representDiplomAndExclude. удалено свойство diplomaQualifications
    	tool.dropColumn("representdiplomandexclude_t", "diplomaqualifications_id");
    }
}