package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.AdditionalDocument;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.entity.RepresentationAdditionalDocumentsRelation;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Дополнительные документы представления
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RepresentationAdditionalDocumentsRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.RepresentationAdditionalDocumentsRelation";
    public static final String ENTITY_NAME = "representationAdditionalDocumentsRelation";
    public static final int VERSION_HASH = 204293305;
    private static IEntityMeta ENTITY_META;

    public static final String L_REPRESENTATION = "representation";
    public static final String L_REASON = "reason";
    public static final String L_DOCUMENTS = "documents";

    private Representation _representation;     // Представление
    private RepresentationReason _reason;     // Причина
    private AdditionalDocument _documents;     // Дополнительные документы

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Представление. Свойство не может быть null.
     */
    @NotNull
    public Representation getRepresentation()
    {
        return _representation;
    }

    /**
     * @param representation Представление. Свойство не может быть null.
     */
    public void setRepresentation(Representation representation)
    {
        dirty(_representation, representation);
        _representation = representation;
    }

    /**
     * @return Причина.
     */
    public RepresentationReason getReason()
    {
        return _reason;
    }

    /**
     * @param reason Причина.
     */
    public void setReason(RepresentationReason reason)
    {
        dirty(_reason, reason);
        _reason = reason;
    }

    /**
     * @return Дополнительные документы.
     */
    public AdditionalDocument getDocuments()
    {
        return _documents;
    }

    /**
     * @param documents Дополнительные документы.
     */
    public void setDocuments(AdditionalDocument documents)
    {
        dirty(_documents, documents);
        _documents = documents;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RepresentationAdditionalDocumentsRelationGen)
        {
            setRepresentation(((RepresentationAdditionalDocumentsRelation)another).getRepresentation());
            setReason(((RepresentationAdditionalDocumentsRelation)another).getReason());
            setDocuments(((RepresentationAdditionalDocumentsRelation)another).getDocuments());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RepresentationAdditionalDocumentsRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RepresentationAdditionalDocumentsRelation.class;
        }

        public T newInstance()
        {
            return (T) new RepresentationAdditionalDocumentsRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "representation":
                    return obj.getRepresentation();
                case "reason":
                    return obj.getReason();
                case "documents":
                    return obj.getDocuments();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "representation":
                    obj.setRepresentation((Representation) value);
                    return;
                case "reason":
                    obj.setReason((RepresentationReason) value);
                    return;
                case "documents":
                    obj.setDocuments((AdditionalDocument) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "representation":
                        return true;
                case "reason":
                        return true;
                case "documents":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "representation":
                    return true;
                case "reason":
                    return true;
                case "documents":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "representation":
                    return Representation.class;
                case "reason":
                    return RepresentationReason.class;
                case "documents":
                    return AdditionalDocument.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RepresentationAdditionalDocumentsRelation> _dslPath = new Path<RepresentationAdditionalDocumentsRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RepresentationAdditionalDocumentsRelation");
    }
            

    /**
     * @return Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentationAdditionalDocumentsRelation#getRepresentation()
     */
    public static Representation.Path<Representation> representation()
    {
        return _dslPath.representation();
    }

    /**
     * @return Причина.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentationAdditionalDocumentsRelation#getReason()
     */
    public static RepresentationReason.Path<RepresentationReason> reason()
    {
        return _dslPath.reason();
    }

    /**
     * @return Дополнительные документы.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentationAdditionalDocumentsRelation#getDocuments()
     */
    public static AdditionalDocument.Path<AdditionalDocument> documents()
    {
        return _dslPath.documents();
    }

    public static class Path<E extends RepresentationAdditionalDocumentsRelation> extends EntityPath<E>
    {
        private Representation.Path<Representation> _representation;
        private RepresentationReason.Path<RepresentationReason> _reason;
        private AdditionalDocument.Path<AdditionalDocument> _documents;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Представление. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentationAdditionalDocumentsRelation#getRepresentation()
     */
        public Representation.Path<Representation> representation()
        {
            if(_representation == null )
                _representation = new Representation.Path<Representation>(L_REPRESENTATION, this);
            return _representation;
        }

    /**
     * @return Причина.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentationAdditionalDocumentsRelation#getReason()
     */
        public RepresentationReason.Path<RepresentationReason> reason()
        {
            if(_reason == null )
                _reason = new RepresentationReason.Path<RepresentationReason>(L_REASON, this);
            return _reason;
        }

    /**
     * @return Дополнительные документы.
     * @see ru.tandemservice.movestudentrmc.entity.RepresentationAdditionalDocumentsRelation#getDocuments()
     */
        public AdditionalDocument.Path<AdditionalDocument> documents()
        {
            if(_documents == null )
                _documents = new AdditionalDocument.Path<AdditionalDocument>(L_DOCUMENTS, this);
            return _documents;
        }

        public Class getEntityClass()
        {
            return RepresentationAdditionalDocumentsRelation.class;
        }

        public String getEntityName()
        {
            return "representationAdditionalDocumentsRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
