package ru.tandemservice.movestudentrmc.migration;

import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;


public class MS_movestudentrmc_1x0x0_14to15 extends IndependentMigrationScript {
    @Override
    public void run(DBTool tool) throws Exception {

        IEntityMeta meta = EntityRuntime.getMeta(RepresentationType.class);

        if (!tool.tableExists(meta.getTableName()))
            return;

        tool.executeUpdate("update " + meta.getTableName() + " set TITLE_P = 'О назначении стипендии/выплаты' where CODE_P = 'appointGrant' ");

    }
}