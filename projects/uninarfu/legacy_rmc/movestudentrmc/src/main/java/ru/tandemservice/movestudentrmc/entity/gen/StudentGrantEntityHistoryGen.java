package ru.tandemservice.movestudentrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudentrmc.entity.IAbstractRepresentation;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntity;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntityHistory;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentGrantType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * История изменения стипендии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentGrantEntityHistoryGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentrmc.entity.StudentGrantEntityHistory";
    public static final String ENTITY_NAME = "studentGrantEntityHistory";
    public static final int VERSION_HASH = -894851357;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT_GRANT_ENTITY = "studentGrantEntity";
    public static final String L_REPRESENTATION = "representation";
    public static final String L_REPRESENT_GRANT_TYPE = "representGrantType";
    public static final String P_CREATE_DATE = "createDate";

    private StudentGrantEntity _studentGrantEntity;     // Стипендия
    private IAbstractRepresentation _representation;     // Представление, которое изменило стипендию
    private RepresentGrantType _representGrantType;     // Тип изменения
    private Date _createDate;     // Дата изменения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Стипендия. Свойство не может быть null.
     */
    @NotNull
    public StudentGrantEntity getStudentGrantEntity()
    {
        return _studentGrantEntity;
    }

    /**
     * @param studentGrantEntity Стипендия. Свойство не может быть null.
     */
    public void setStudentGrantEntity(StudentGrantEntity studentGrantEntity)
    {
        dirty(_studentGrantEntity, studentGrantEntity);
        _studentGrantEntity = studentGrantEntity;
    }

    /**
     * @return Представление, которое изменило стипендию. Свойство не может быть null.
     */
    @NotNull
    public IAbstractRepresentation getRepresentation()
    {
        return _representation;
    }

    /**
     * @param representation Представление, которое изменило стипендию. Свойство не может быть null.
     */
    public void setRepresentation(IAbstractRepresentation representation)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && representation!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IAbstractRepresentation.class);
            IEntityMeta actual =  representation instanceof IEntity ? EntityRuntime.getMeta((IEntity) representation) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_representation, representation);
        _representation = representation;
    }

    /**
     * @return Тип изменения. Свойство не может быть null.
     */
    @NotNull
    public RepresentGrantType getRepresentGrantType()
    {
        return _representGrantType;
    }

    /**
     * @param representGrantType Тип изменения. Свойство не может быть null.
     */
    public void setRepresentGrantType(RepresentGrantType representGrantType)
    {
        dirty(_representGrantType, representGrantType);
        _representGrantType = representGrantType;
    }

    /**
     * @return Дата изменения. Свойство не может быть null.
     */
    @NotNull
    public Date getCreateDate()
    {
        return _createDate;
    }

    /**
     * @param createDate Дата изменения. Свойство не может быть null.
     */
    public void setCreateDate(Date createDate)
    {
        dirty(_createDate, createDate);
        _createDate = createDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentGrantEntityHistoryGen)
        {
            setStudentGrantEntity(((StudentGrantEntityHistory)another).getStudentGrantEntity());
            setRepresentation(((StudentGrantEntityHistory)another).getRepresentation());
            setRepresentGrantType(((StudentGrantEntityHistory)another).getRepresentGrantType());
            setCreateDate(((StudentGrantEntityHistory)another).getCreateDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentGrantEntityHistoryGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentGrantEntityHistory.class;
        }

        public T newInstance()
        {
            return (T) new StudentGrantEntityHistory();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "studentGrantEntity":
                    return obj.getStudentGrantEntity();
                case "representation":
                    return obj.getRepresentation();
                case "representGrantType":
                    return obj.getRepresentGrantType();
                case "createDate":
                    return obj.getCreateDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "studentGrantEntity":
                    obj.setStudentGrantEntity((StudentGrantEntity) value);
                    return;
                case "representation":
                    obj.setRepresentation((IAbstractRepresentation) value);
                    return;
                case "representGrantType":
                    obj.setRepresentGrantType((RepresentGrantType) value);
                    return;
                case "createDate":
                    obj.setCreateDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "studentGrantEntity":
                        return true;
                case "representation":
                        return true;
                case "representGrantType":
                        return true;
                case "createDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "studentGrantEntity":
                    return true;
                case "representation":
                    return true;
                case "representGrantType":
                    return true;
                case "createDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "studentGrantEntity":
                    return StudentGrantEntity.class;
                case "representation":
                    return IAbstractRepresentation.class;
                case "representGrantType":
                    return RepresentGrantType.class;
                case "createDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentGrantEntityHistory> _dslPath = new Path<StudentGrantEntityHistory>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentGrantEntityHistory");
    }
            

    /**
     * @return Стипендия. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.StudentGrantEntityHistory#getStudentGrantEntity()
     */
    public static StudentGrantEntity.Path<StudentGrantEntity> studentGrantEntity()
    {
        return _dslPath.studentGrantEntity();
    }

    /**
     * @return Представление, которое изменило стипендию. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.StudentGrantEntityHistory#getRepresentation()
     */
    public static IAbstractRepresentationGen.Path<IAbstractRepresentation> representation()
    {
        return _dslPath.representation();
    }

    /**
     * @return Тип изменения. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.StudentGrantEntityHistory#getRepresentGrantType()
     */
    public static RepresentGrantType.Path<RepresentGrantType> representGrantType()
    {
        return _dslPath.representGrantType();
    }

    /**
     * @return Дата изменения. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.StudentGrantEntityHistory#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    public static class Path<E extends StudentGrantEntityHistory> extends EntityPath<E>
    {
        private StudentGrantEntity.Path<StudentGrantEntity> _studentGrantEntity;
        private IAbstractRepresentationGen.Path<IAbstractRepresentation> _representation;
        private RepresentGrantType.Path<RepresentGrantType> _representGrantType;
        private PropertyPath<Date> _createDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Стипендия. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.StudentGrantEntityHistory#getStudentGrantEntity()
     */
        public StudentGrantEntity.Path<StudentGrantEntity> studentGrantEntity()
        {
            if(_studentGrantEntity == null )
                _studentGrantEntity = new StudentGrantEntity.Path<StudentGrantEntity>(L_STUDENT_GRANT_ENTITY, this);
            return _studentGrantEntity;
        }

    /**
     * @return Представление, которое изменило стипендию. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.StudentGrantEntityHistory#getRepresentation()
     */
        public IAbstractRepresentationGen.Path<IAbstractRepresentation> representation()
        {
            if(_representation == null )
                _representation = new IAbstractRepresentationGen.Path<IAbstractRepresentation>(L_REPRESENTATION, this);
            return _representation;
        }

    /**
     * @return Тип изменения. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.StudentGrantEntityHistory#getRepresentGrantType()
     */
        public RepresentGrantType.Path<RepresentGrantType> representGrantType()
        {
            if(_representGrantType == null )
                _representGrantType = new RepresentGrantType.Path<RepresentGrantType>(L_REPRESENT_GRANT_TYPE, this);
            return _representGrantType;
        }

    /**
     * @return Дата изменения. Свойство не может быть null.
     * @see ru.tandemservice.movestudentrmc.entity.StudentGrantEntityHistory#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(StudentGrantEntityHistoryGen.P_CREATE_DATE, this);
            return _createDate;
        }

        public Class getEntityClass()
        {
            return StudentGrantEntityHistory.class;
        }

        public String getEntityName()
        {
            return "studentGrantEntityHistory";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
