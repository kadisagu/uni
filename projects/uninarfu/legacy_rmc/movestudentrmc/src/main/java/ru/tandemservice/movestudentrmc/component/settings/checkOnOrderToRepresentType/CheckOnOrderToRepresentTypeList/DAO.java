package ru.tandemservice.movestudentrmc.component.settings.checkOnOrderToRepresentType.CheckOnOrderToRepresentTypeList;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.movestudentrmc.entity.catalog.CheckOnOrder;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.component.settings.abstractRelation.AbstractRelationList.AbstractRelationListDAO;

public class DAO extends AbstractRelationListDAO<Model> implements IDAO {

    @Override
    public void prepareListDataSource(Model model) {
        MQBuilder builder = new MQBuilder(getFirstObjectEntityName(), "o")
                .add(MQExpression.eq("o", CheckOnOrder.P_USED, Boolean.TRUE));
        (new OrderDescriptionRegistry("o")).applyOrder(builder, model.getDataSource().getEntityOrder());
        UniUtils.createPage(model.getDataSource(), builder, getSession());
    }

    @Override
    protected String getFirstObjectEntityName() {
        return CheckOnOrder.ENTITY_CLASS;
    }

}
