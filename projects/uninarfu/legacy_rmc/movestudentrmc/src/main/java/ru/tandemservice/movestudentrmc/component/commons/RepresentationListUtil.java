package ru.tandemservice.movestudentrmc.component.commons;

import org.tandemframework.common.util.PostfixPermissionModel;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.ui.EducationYearModel;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;

public class RepresentationListUtil {
    public RepresentationListUtil() {
    }

    public static void initExtractListModel(RepresentationListModel model, String secPostfix, String parentExtractTypeCode) {
        IUniBaseDao dao = UniDaoFacade.getCoreDao();
        model.setSecModel(new PostfixPermissionModel(secPostfix));
        model.setEducationYearList(new EducationYearModel());
        model.setFormativeOrgUnitModel(new OrgUnitKindAutocompleteModel("2"));
        model.setTerritorialOrgUnitModel(new OrgUnitKindAutocompleteModel("3"));
        model.setProducingOrgUnitModel(new OrgUnitKindAutocompleteModel("1"));
    }

    public static MQBuilder prepareProjectListBuilder(IDataSettings settings, OrgUnit orgUnit) {
        MQBuilder builder = prepareModularExtractListBuilder(settings, orgUnit);
        builder.add(MQExpression.isNull("extract", "paragraph"));
        if (settings.get("extractState") == null) {
            Object extractStatesCodes[] = {"1", "2", "3", "4"};
            builder.add(MQExpression.in("state", "code", extractStatesCodes));
        }
        return builder;
    }

    public static MQBuilder prepareExtractListBuilder(IDataSettings settings, OrgUnit orgUnit) {
        MQBuilder builder = prepareModularExtractListBuilder(settings, orgUnit);
        builder.addJoin("extract", "paragraph.order", "order_");
        if (settings.get("extractState") == null) {
            Object extractStatesCodes[] = {"5", "6"};
            builder.add(MQExpression.in("state", "code", extractStatesCodes));
        }
        return builder;
    }

    private static MQBuilder prepareModularExtractListBuilder(IDataSettings settings, OrgUnit orgUnit) {
        MQBuilder builder = new MQBuilder("ru.tandemservice.movestudent.entity.ModularStudentExtract", "extract");
        builder.addJoin("extract", "state", "state");
        builder.addJoin("extract", "entity", "student");
        builder.addJoin("student", "person.identityCard", "idCard");
        builder.addJoin("student", "educationOrgUnit", "educationOrgUnit");
        builder.addJoin("educationOrgUnit", "educationLevelHighSchool", "educationLevelHighSchool");
        if (orgUnit != null) {
            AbstractExpression eqFormative = MQExpression.eq("educationOrgUnit", "formativeOrgUnit", orgUnit);
            AbstractExpression eqTerritorial = MQExpression.eq("educationOrgUnit", "territorialOrgUnit", orgUnit);
            builder.add(MQExpression.or(new AbstractExpression[]{eqFormative, eqTerritorial}));
        }
        return builder;
    }

}
