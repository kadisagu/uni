package ru.tandemservice.movestudentrmc.util;

import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.movestudentrmc.dao.check.ICheckDAO;
import ru.tandemservice.movestudentrmc.entity.catalog.CheckOnOrder;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentExtractStatesCodes;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.OrderCategoryCodes;
import ru.tandemservice.movestudentrmc.entity.gen.IRepresent2StudentGen;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CheckUtil {

    /**
     * возвращает список фамилий студентов у которых есть другие непроведенные списочные представления
     *
     * @param listRepresent текущее представление
     * @param studentList   студенты
     *
     * @return
     */
    public static List<String> checkExistingRepresents(IAbstractRepresentation represent, List<Student> studentList) {
        List<String> resultList = new ArrayList<String>();

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(IRepresent2Student.class, "rel")
                .where(DQLExpressions.in(DQLExpressions.property(IRepresent2StudentGen.student().fromAlias("rel")), studentList));

        if (represent != null)
            dql.where(DQLExpressions.ne(
                    DQLExpressions.property(IRepresent2StudentGen.representation().id().fromAlias("rel")),
                    DQLExpressions.value(represent.getRepresentID())
            ));

        dql.where(DQLExpressions.ne(
                DQLExpressions.property(IRepresent2StudentGen.representation().state().code().fromAlias("rel")),
                DQLExpressions.value(MovestudentExtractStatesCodes.CODE_6)
        ))
                .where(DQLExpressions.ne(DQLExpressions.property(IRepresent2StudentGen.representation().type().orderCategory().code().fromAlias("rel")), OrderCategoryCodes.GRANTS))
                .column(DQLExpressions.property(IRepresent2StudentGen.student().person().identityCard().fromAlias("rel")));
        List<IdentityCard> badList = IUniBaseDao.instance.get().getList(dql);

        for (IdentityCard badFio : badList) {
            String fio = PersonManager.instance().declinationDao().getDeclinationFIO(badFio, GrammaCase.GENITIVE);
            if (!resultList.contains(fio))
                resultList.add(fio);
        }

        return resultList;
    }

    public static boolean checkIndividualRepresent(Representation represent, List<String> errorList) {

        checkRepresent(represent, represent.getType(), getGrantView(represent), getStudents(represent), errorList);

        return errorList.isEmpty();
    }

    public static boolean checkListRepresent(ListRepresent represent, List<String> errorList) {

        checkRepresent(represent, represent.getRepresentationType(), getGrantView(represent), getStudents(represent), errorList);

        return errorList.isEmpty();
    }

    protected static void checkRepresent(IEntity represent, RepresentationType type, GrantView view, List<Student> studentList, List<String> errorList) {
        Set<String> set = new HashSet<String>();

        if (type.isCheck()) {

            if (type.getOrderCategory().getCode().equals(OrderCategoryCodes.MOVEMENTSTUDENT)) {
                List<RelCheckOnorderRepresentType> lst = getCheckList(type);

                for (RelCheckOnorderRepresentType rel : lst) {
                    set.addAll(getBean(rel.getCheck()).check(represent, studentList, rel.isValue()));
                }
            }

            if (type.getOrderCategory().getCode().equals(OrderCategoryCodes.GRANTS) && view != null) {

                List<RelCheckOnorderGrantView> lst = getCheckList(view);

                for (RelCheckOnorderGrantView rel : lst) {
                    set.addAll(getBean(rel.getCheck()).check(represent, studentList, rel.isValue()));
                }
            }
        }

        if (!set.isEmpty())
            errorList.addAll(set);
    }

    public static GrantView getGrantView(EntityBase entity) {
        /*
		 * Метод тянет вид стипендии. Если propertyPath отличный от "grantView" - добавить if
		*/
        GrantView view;
        try {
            view = (GrantView) entity.getProperty("grantView");
        }
        catch (Exception e) {
            view = null;
        }
        return view;
    }

    protected static ICheckDAO getBean(CheckOnOrder check) {
        return (ICheckDAO) ApplicationRuntime.getBean(check.getBean());
    }

    protected static List<Student> getStudents(ListRepresent represent) {
        List<Student> retVal = new ArrayList<Student>();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RelListRepresentStudents.class, "stud")
                .column(DQLExpressions.property(RelListRepresentStudents.student().fromAlias("stud")))
                .where(DQLExpressions.eq(
                        DQLExpressions.property(RelListRepresentStudents.representation().id().fromAlias("stud")),
                        DQLExpressions.value(represent.getId())));
        retVal = UniDaoFacade.getCoreDao().getList(builder);

        return retVal;
    }

    protected static List<Student> getStudents(Representation represent) {
        List<Student> retVal = new ArrayList<Student>();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocRepresentStudentBase.class, "stud")
                .column(DQLExpressions.property(DocRepresentStudentBase.student().fromAlias("stud")))
                .where(DQLExpressions.eq(
                        DQLExpressions.property(DocRepresentStudentBase.representation().id().fromAlias("stud")),
                        DQLExpressions.value(represent.getId())));
        retVal = UniDaoFacade.getCoreDao().getList(builder);

        return retVal;
    }

    private static List<RelCheckOnorderRepresentType> getCheckList(RepresentationType type) {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RelCheckOnorderRepresentType.class, "rel")
                .where(DQLExpressions.eqValue(DQLExpressions.property(RelCheckOnorderRepresentType.check().used().fromAlias("rel")), Boolean.TRUE))
                .where(DQLExpressions.eqValue(DQLExpressions.property(RelCheckOnorderRepresentType.type().id().fromAlias("rel")), type.getId()));

        return UniDaoFacade.getCoreDao().getList(builder);
    }

    private static List<RelCheckOnorderGrantView> getCheckList(GrantView view) {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RelCheckOnorderGrantView.class, "rel")
                .where(DQLExpressions.eqValue(DQLExpressions.property(RelCheckOnorderGrantView.check().used().fromAlias("rel")), Boolean.TRUE))
                .where(DQLExpressions.eqValue(DQLExpressions.property(RelCheckOnorderGrantView.view().id().fromAlias("rel")), view.getId()));

        return UniDaoFacade.getCoreDao().getList(builder);
    }
}
