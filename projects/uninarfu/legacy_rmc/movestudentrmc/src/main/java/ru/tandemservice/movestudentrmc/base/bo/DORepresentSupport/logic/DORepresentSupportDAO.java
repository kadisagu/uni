package ru.tandemservice.movestudentrmc.base.bo.DORepresentSupport.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.support.AbstractDORepresentDAO;
import ru.tandemservice.movestudentrmc.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.DocumentOrder;
import ru.tandemservice.movestudentrmc.entity.RepresentSupport;
import ru.tandemservice.movestudentrmc.entity.Representation;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class DORepresentSupportDAO extends AbstractDORepresentDAO implements IDORepresentSupportDAO {

    @Override
    public Representation createRepresentObject(RepresentationType type,
                                                List<Student> student)
    {
        RepresentSupport represent = new RepresentSupport();
        represent.setType(type);
        return represent;
    }

    @Override
    public void buildBodyExtract(Representation representationBase,
                                 String itemFac, RtfDocument docCommon)
    {
        RepresentSupport represent = (RepresentSupport) representationBase;
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        RtfInjectModifier modifier = new RtfInjectModifier();

        UtilPrintSupport.setParNum(docCommon, itemFac);

        UtilPrintSupport.injectModifierExtract(modifier, docRepresent);

        injectModifier(modifier, represent, docRepresent.getStudent());

        modifier.modify(docCommon);

    }

    @Override
    public void buildBodyRepresent(Representation representationBase,
                                   RtfDocument document, AtomicInteger parNumber, AtomicInteger extractNumber)
    {
        super.buildBodyRepresent(representationBase, document, parNumber, extractNumber);
        RepresentSupport represent = (RepresentSupport) representationBase;
        DocRepresentStudentBase docRepresent = getStudentList(representationBase).get(0);

        Student student = UtilPrintSupport.getStudent(represent);
        RtfInjectModifier modifier = new RtfInjectModifier();

        UtilPrintSupport.injectModifierRepresent(modifier, docRepresent, student);

        injectModifier(modifier, represent, student);

        if (StringUtils.isEmpty(modifier.getStringValue("basics")))
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Arrays.asList("basics"), false, false);

        modifier.modify(document);

    }

    protected void injectModifier(RtfInjectModifier modifier, RepresentSupport represent, Student student) {

        modifier.put("studentTitle", UtilPrintSupport.printSeparatedFIO(student.getPerson().getIdentityCard(), GrammaCase.DATIVE, student.getPerson().isMale()));
        modifier.put("studentSex_D", student.getPerson().isMale() ? "студенту" : "студентке");
        modifier.put("beginDate", UtilPrintSupport.getDateFormatterWithMonthString(represent.getBeginDate(), GrammaCase.PREPOSITIONAL));
        modifier.put("grantView", represent.getGrantView().getAccusative());
        modifier.put("sum", getSumStr(represent.getSum()));

    }

    private String getSumStr(double sum) {

        String[] s = String.valueOf(sum).split("\\.");

        return new StringBuilder().append(s[0])
                .append(" рублей ")
                .append(StringUtils.rightPad(s[1], 2, "0"))
                .append(" копеек").toString();
    }

    @Override
    public boolean doCommit(Representation represent, ErrorCollector error) {
        if (!super.doCommit(represent, error))
            return false;

        DocumentOrder order = getOrder(represent);

        MoveStudentDaoFacade.getGrantDAO().commitStudentGrants(represent, order);

        return true;
    }

    @Override
    public boolean doRollback(Representation represent, ErrorCollector error) {
        if (!super.doRollback(represent, error))
            return false;

        DocumentOrder order = getOrder(represent);

        MoveStudentDaoFacade.getGrantDAO().rollbackStudentGrants(represent, order);

        return true;
    }

    @Override
    public GrammaCase getGrammaCase() {
        return GrammaCase.DATIVE;
    }

}
