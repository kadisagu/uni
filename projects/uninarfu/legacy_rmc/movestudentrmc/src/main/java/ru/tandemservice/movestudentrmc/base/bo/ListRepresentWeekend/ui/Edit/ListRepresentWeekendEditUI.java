package ru.tandemservice.movestudentrmc.base.bo.ListRepresentWeekend.ui.Edit;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import ru.tandemservice.movestudentrmc.base.bo.ListRepresentBase.support.AbstractListRepresentEditUI;
import ru.tandemservice.movestudentrmc.dao.CheckStateUtil;
import ru.tandemservice.movestudentrmc.entity.ListRepresentWeekend;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentExtractStatesCodes;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

@State({
        @Bind(key = AbstractListRepresentEditUI.LIST_REPRESENT_ID, binding = AbstractListRepresentEditUI.LIST_REPRESENT_ID),
        @Bind(key = "typeRepresentFilter", binding = "typeRepresentFilter"),
        @Bind(key = "reasonRepresentFilter", binding = "reasonRepresentFilter"),
        @Bind(key = "basementRepresentFilter", binding = "basementRepresentFilter"),
        @Bind(key = "representationBasementDate", binding = "representationBasementDate"),
        @Bind(key = "representationBasementNumber", binding = "representationBasementNumber"),
})
public class ListRepresentWeekendEditUI extends AbstractListRepresentEditUI<ListRepresentWeekend> {

    @Override
    public ListRepresentWeekend getListRepresentObject() {
        return new ListRepresentWeekend();
    }

    @Override
    public void onComponentRefresh() {
        super.onComponentRefresh();
        if (this.getListRepresentId() == null) {
            //31 августа текущего года
            Calendar calendar = Calendar.getInstance();
            calendar.set(CoreDateUtils.getYear(new Date()), Calendar.AUGUST, 31);
            Date dateEndOfWeekend = calendar.getTime();

            this.getListRepresent().setDateEndOfWeekend(dateEndOfWeekend);
        }
    }

    @Override
    public void onClickSave() {
        if (getListRepresentId() != null)
            CheckStateUtil.checkStateRepresent(getListRepresent(), Arrays.asList(MovestudentExtractStatesCodes.CODE_1), _uiSupport.getSession());
        super.onClickSave();
    }

}
