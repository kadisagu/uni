package ru.tandemservice.movestudentrmc.base.bo.DORepresentBase.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.FilterUtils;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DevelopTechDSHandler extends DefaultComboDataSourceHandler {

    public static final String NEW_FORMATIVE_ORG_UNIT = "newFormativeOrgUnit";
    public static final String NEW_TERRITORIAL_ORG_UNIT = "newTerritorialOrgUnit";
    public static final String EDUCATION_LEVELS_HIGH_SCHOOL = "educationLevelsHighSchool";
    public static final String DEVELOP_FORM = "developForm";
    public static final String DEVELOP_CONDITION = "developCondition";
    public static final String DEVELOP_PERIOD = "developPeriod";


    public DevelopTechDSHandler(String name) {
        super(name, DevelopTech.class, DevelopTech.title());
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep) {
        String filter = ep.input.getComboFilterByValue();
        if (StringUtils.isNotBlank(filter)) {
            ep.dqlBuilder.where(like(DQLFunctions.upper(property(DevelopTech.title().fromAlias("e"))), value(CoreStringUtils.escapeLike(filter, true))));
        }

        OrgUnit newFormativeOrgUnit = ep.context.get(NEW_FORMATIVE_ORG_UNIT);
        OrgUnit newTerritorialOrgUnit = ep.context.get(NEW_TERRITORIAL_ORG_UNIT);
        EducationLevelsHighSchool educationLevelsHighSchool = ep.context.get(EDUCATION_LEVELS_HIGH_SCHOOL);
        DevelopForm developForm = (DevelopForm) ep.context.get(DEVELOP_FORM);
        DevelopCondition developCondition = (DevelopCondition) ep.context.get(DEVELOP_CONDITION);
        DevelopPeriod developPeriod = (DevelopPeriod) ep.context.get(DEVELOP_PERIOD);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "eou")
                .column(DQLExpressions.property(EducationOrgUnit.developTech().id().fromAlias("eou")));

        FilterUtils.applySelectFilter(builder, "eou", EducationOrgUnit.formativeOrgUnit(), newFormativeOrgUnit);
        FilterUtils.applySelectFilter(builder, "eou", EducationOrgUnit.territorialOrgUnit(), newTerritorialOrgUnit);
        builder.where(DQLExpressions.eq(DQLExpressions.property("eou", EducationOrgUnit.educationLevelHighSchool()), DQLExpressions.value(educationLevelsHighSchool)));
        builder.where(DQLExpressions.eq(DQLExpressions.property("eou", EducationOrgUnit.developForm()), DQLExpressions.value(developForm)));
        builder.where(DQLExpressions.eq(DQLExpressions.property("eou", EducationOrgUnit.developCondition()), DQLExpressions.value(developCondition)));
        builder.where(DQLExpressions.eq(DQLExpressions.property("eou", EducationOrgUnit.developPeriod()), DQLExpressions.value(developPeriod)));

        ep.dqlBuilder.where(DQLExpressions.in(DQLExpressions.property(DevelopTech.id().fromAlias("e")), builder.getQuery()));
    }
}
