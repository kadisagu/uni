package ru.tandemservice.uni.entity.vpo;

import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import ru.tandemservice.uni.entity.vpo.gen.UniRmcVPOTemplateDocumentGen;

/**
 * Печатные шаблоны отчета ВПО
 */
public class UniRmcVPOTemplateDocument extends UniRmcVPOTemplateDocumentGen
        implements ITemplateDocument
{

    @Override
    public byte[] getContent()
    {
        return CommonBaseUtil.getTemplateContent(this);
    }
}