package ru.tandemservice.vpormc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.Map;

public class MS_vpormc_1x0x0_0to1 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.5.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        Map<String, String> permissionKeyMap = new HashMap<>();
        {
            permissionKeyMap.put("uniRmcReportVPO", "vpoReport");
        }

        tool.table("accessmatrix_t").constraints().clear();

        PreparedStatement permissionKeyStatement = tool.prepareStatement("update accessmatrix_t set permissionkey_p = ? where permissionkey_p = ?");
        for (Map.Entry<String, String> permissionKeyEntry : permissionKeyMap.entrySet()) {
            permissionKeyStatement.setString(1, permissionKeyEntry.getValue());
            permissionKeyStatement.setString(2, permissionKeyEntry.getKey());

            permissionKeyStatement.execute();
        }


    }
}