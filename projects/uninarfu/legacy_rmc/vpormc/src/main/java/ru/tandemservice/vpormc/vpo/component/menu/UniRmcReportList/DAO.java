package ru.tandemservice.vpormc.vpo.component.menu.UniRmcReportList;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.sec.IPrincipalContext;
import ru.tandemservice.uni.dao.UniDao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.core.sec.SecurityManagerFacade;
import org.tandemframework.sec.runtime.SecurityRuntime;
*/

public class DAO extends UniDao<Model> implements IDAO {

    @SuppressWarnings("unchecked")
    public void prepareListDataSource(Model model, IPrincipalContext principalContext)
    {
        // ISecured securedObject = (ISecured) SecurityRuntime.getInstance().getCommonSecurityObject();

        List<Model.ReportDefinitionWrapper> wrappers = new ArrayList<Model.ReportDefinitionWrapper>();

        Long id = 0L;
        for (UniRmcReportDefinition repDef : (List<UniRmcReportDefinition>) ApplicationRuntime.getBean(IUniRmcReportDefinition.UNIRMC_REPORT_DEFINITION_LIST_BEAN_NAME)) {
            // if(!SecurityManagerFacade.check(securedObject, principalContext, repDef.getPermissionKey())) { continue; }
            wrappers.add(new Model.ReportDefinitionWrapper(id++, repDef));
        }

        Collections.sort(wrappers, ITitled.TITLED_COMPARATOR);

        model.getDataSource().setTotalSize(wrappers.size());
        model.getDataSource().setCountRow(wrappers.size());
        model.getDataSource().createPage(wrappers);
    }
}
