package ru.tandemservice.vpormc.component.catalog.uniRmcVPOTemplateDocument.UniRmcVPOTemplateDocumentPub;


import org.tandemframework.shared.commonbase.component.catalog.base.DefaultPrintCatalogPub.IDefaultPrintCatalogPubDAO;
import ru.tandemservice.uni.entity.vpo.UniRmcVPOTemplateDocument;


public interface IDAO extends IDefaultPrintCatalogPubDAO<UniRmcVPOTemplateDocument, Model> {
}
