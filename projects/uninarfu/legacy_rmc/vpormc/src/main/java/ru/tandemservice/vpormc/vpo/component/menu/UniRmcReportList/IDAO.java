package ru.tandemservice.vpormc.vpo.component.menu.UniRmcReportList;

import org.tandemframework.core.sec.IPrincipalContext;
import ru.tandemservice.uni.dao.IUniDao;


public interface IDAO extends IUniDao<Model> {
    void prepareListDataSource(Model model, IPrincipalContext principalContext);
}
