package ru.tandemservice.vpormc.vpo.component.menu.UniRmcReportList;

public interface IUniRmcReportDefinition {
    String UNIRMC_REPORT_DEFINITION_LIST_BEAN_NAME = "uniRmcReportDefinitionList";

    String getId();

    String getTitle();

    String getPermissionKey();

    String getComponentName();

}
