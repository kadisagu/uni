package ru.tandemservice.vpormc.vpo.component.menu.UniRmcReportList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

public class Controller extends AbstractBusinessController<IDAO, Model> {
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;
        final IDAO dao = getDao();

        final DynamicListDataSource<Model.ReportDefinitionWrapper> dataSource = new DynamicListDataSource<>(component, component1 -> {
            dao.prepareListDataSource(model, component1.getUserContext().getPrincipalContext());
        });

        PublisherLinkColumn plc = new PublisherLinkColumn("Название отчета", "reportDefinition.title");
        plc.setResolver(new IPublisherLinkResolver() {

            public Object getParameters(IEntity entity)
            {
                return null;
            }


            public String getComponentName(IEntity entity)
            {
                return ((Model.ReportDefinitionWrapper) entity).getReportDefinition().getComponentName();
            }
        });
        plc.setStyle("color:#002f86;");
        dataSource.addColumn(plc.setOrderable(false));

        model.setDataSource(dataSource);
    }
}

