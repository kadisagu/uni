package ru.tandemservice.vpormc.vpo.reports.VpoReport;

import java.util.ArrayList;
import java.util.List;

public class ReportGenerate {

    public static List<String> getEmptyPage3_5Row()
    {
        List<String> cells = new ArrayList<String>();
        for (int i = 0; i < 16; i++) {
            cells.add("");
        }
        return cells;
    }

    public static List<String> getEmptyPage14Row()
    {
        List<String> cells = new ArrayList<String>();
        for (int i = 0; i < 8; i++) {
            cells.add("");
        }
        return cells;
    }

    public static List<String> getEmptyPage13Row()
    {
        List<String> cells = new ArrayList<String>();
        for (int i = 0; i < 5; i++) {
            cells.add("");
        }
        return cells;
    }


    public static String Sql
            = " select " +
            " COUNT(student_id) as stu_count " +
            " , course_number " +
            " , compensation_code_p " +
            " , compensation_shorttitle_p " +
            " , studentcategory_code_p " +
            " , studentcategory_title_p " +
            " , sex_code_p " +
            " , sex_title_p " +
            " , developform_code_p " +
            " , developform_title_p " +
            " , developcondition_code_p " +
            " , developcondition_title_p " +
            " , educationlevels_id " +
            " , educationlevels_okso " +
            " , educationlevels_title_p " +
            " , parenteducationlevels_okso " +
            " , qualifications_code_p " +
            " , qualifications_title_p " +
            " , birthdate " +
            " , country_code_p " +
            " , country_digitalcode_p " +
            " , country_title_p " +
            " , addresscountrytype_code_p " +
            " , addresscountrytype_title_p " +
            " from " +
            " ( " +
            " select " +
            " student_t.id as student_id " +
            " , course_t.intvalue_p as course_number " +
            " , compensationtype_t.code_p as compensation_code_p " +
            " , compensationtype_t.shorttitle_p as compensation_shorttitle_p " +
            " , studentcategory_t.code_p as studentcategory_code_p " +
            " , studentcategory_t.title_p as studentcategory_title_p " +
            " , sex_t.code_p as sex_code_p " +
            " , sex_t.title_p as sex_title_p " +
            " , developform_t.code_p as developform_code_p " +
            " , developform_t.title_p as developform_title_p " +
            " , developtech_t.code_p as developtech_code_p " +
            " , developtech_t.title_p as developtech_title_p " +
            " , developcondition_t.code_p as developcondition_code_p " +
            " , developcondition_t.title_p as developcondition_title_p " +
            " , educationlevels_t.parentlevel_id as educationlevels_parentlevel_id " +
            " , educationlevels_t.id  as educationlevels_id " +
            " , educationlevels_t.okso_p as educationlevels_okso " +
            " , educationlevels_t.title_p as educationlevels_title_p " +
            " , parenteducationlevels_t.okso_p as parenteducationlevels_okso " +
            " , parenteducationlevels_t.title_p as parenteducationlevels_title_p " +
            " , qualifications_t.code_p as qualifications_code_p " +
            " , qualifications_t.title_p as qualifications_title_p " +
            " , structureeducationlevels_t.code_p as structureeducationlevels_code_p " +
            " , structureeducationlevels_t.title_p as structureeducationlevels_title_p " +
            " , identitycard_t.birthdate_p as birthdate " +
            " , addresscountry_t.code_p as country_code_p " +
            " , addresscountry_t.digitalcode_p as country_digitalcode_p " +
            " , addresscountry_t.title_p as country_title_p " +
            " , addresscountrytype_t.code_p as addresscountrytype_code_p " +
            " , addresscountrytype_t.title_p as addresscountrytype_title_p " +
            " from " +
            " student_t inner join  studentstatus_t on student_t.status_id = studentstatus_t.id " +
            "		  inner join personrole_t on student_t.id = personrole_t.id " +
            "		  inner join person_t on personrole_t.person_id = person_t.id " +
            "		  inner join identitycard_t on person_t.identitycard_id = identitycard_t.id " +
            "		  inner join sex_t on identitycard_t.sex_id = sex_t.id " +
            "		  inner join course_t on student_t.course_id =  course_t.id " +
            "		  inner join compensationtype_t on student_t.compensationtype_id = compensationtype_t.id " +
            "		  inner join studentcategory_t on student_t.studentcategory_id = studentcategory_t.id " +
            "		  inner join educationorgunit_t on student_t.educationorgunit_id = educationorgunit_t.id " +
            "		  inner join educationlevelshighschool_t on educationorgunit_t.educationlevelhighschool_id = educationlevelshighschool_t.id " +
            "		  inner join developform_t on educationorgunit_t.developform_id = developform_t.id " +
            "		  inner join developtech_t on educationorgunit_t.developtech_id = developtech_t.id " +
            "		  inner join developcondition_t on educationorgunit_t.developcondition_id = developcondition_t.id " +
            "		  inner join educationlevels_t on educationlevelshighschool_t.educationlevel_id = educationlevels_t.id " +
            "		  inner join qualifications_t on educationlevels_t.qualification_id = qualifications_t.id " +
            "		  inner join structureeducationlevels_t on educationlevels_t.leveltype_id = structureeducationlevels_t.id " +
            "		  left  join educationlevels_t as parenteducationlevels_t on educationlevels_t.parentlevel_id = parenteducationlevels_t.id " +
            "		  left join  addresscountry_t  on identitycard_t.citizenship_id = addresscountry_t.id " +
            "		  left join addresscountrytype_t on addresscountry_t.countrytype_id = addresscountrytype_t.id " +
            " where  " +
            "	studentstatus_t.code_p='1' " +
            "	) as tAll " +
            "	group by " +
            "	course_number " +
            "	, compensation_code_p " +
            "	, compensation_shorttitle_p " +
            "	, studentcategory_code_p " +
            "	, studentcategory_title_p " +
            "	, sex_code_p " +
            "	, sex_title_p " +
            "	, developform_code_p " +
            "	, developform_title_p " +
            "	, developcondition_code_p " +
            "	, developcondition_title_p " +
            "	, educationlevels_id " +
            "	, educationlevels_okso " +
            "	, educationlevels_title_p " +
            "	, parenteducationlevels_okso " +
            "	, qualifications_code_p " +
            "	, qualifications_title_p " +
            "   , birthdate " +
            "   , country_code_p " +
            "   , country_digitalcode_p " +
            "   , country_title_p " +
            "   , addresscountrytype_code_p " +
            "   , addresscountrytype_title_p ";


}
