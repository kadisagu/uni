package ru.tandemservice.vpormc.vpo.reports.VpoReport.List;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.entity.report.UniRmcStorableReport;

public class Controller extends AbstractBusinessController<IDAO, Model> {
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        createDataList(component);
        getDao().prepare(model);

    }


    private void createDataList(IBusinessComponent component)
    {

        Model model = getModel(component);

        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<UniRmcStorableReport> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareCustomDataSource(getModel(component1), getModel(component1).getDataSource());
        }, 10);

        dataSource.addColumn(IndicatorColumn.createIconColumn("report", "Отчет"));
        dataSource.addColumn(new SimpleColumn("Дата формирования", UniRmcStorableReport.P_CREATE_DATE, DateFormatter.DATE_FORMATTER_WITH_TIME));

        dataSource.addColumn(new SimpleColumn("Примечание", UniRmcStorableReport.P_PRIM)
                                     .setOrderable(false).setClickable(false));

        dataSource.addColumn(new IndicatorColumn("Печать", null, "onClickPrintReport")
                                     .defaultIndicator(new IndicatorColumn.Item("printer", "Печать"))
                                     .setImageHeader(false)
                                     .setDisableSecondSubmit(false)
                                     .setOrderable(false)
                                     .setPermissionKey("printGlobalStoredReport"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteReport",
                                              "Удалить отчет «Аттестация студентов» от «{0}»?", UniRmcStorableReport.P_CREATE_DATE)
                                     .setPermissionKey("deleteGlobalStoredReport"));
        dataSource.setOrder(UniRmcStorableReport.P_CREATE_DATE, OrderDirection.desc);

        model.setDataSource(dataSource);
    }


    public void onClickAddReport(IBusinessComponent context)
    {
        getDao().update(getModel(context));
    }

    public void onClickDeleteReport(IBusinessComponent component)
    {
        getDao().deleteRow(component);
    }

    public void onClickPrintReport(IBusinessComponent component) throws Exception
    {
        activateInRoot(component, new ComponentActivator(IUniComponents.DOWNLOAD_STORABLE_REPORT,
                                                         new ParametersMap().add("reportId", component.getListenerParameter()).add("extension", "rtf").add("zip", Boolean.FALSE)));
    }

}