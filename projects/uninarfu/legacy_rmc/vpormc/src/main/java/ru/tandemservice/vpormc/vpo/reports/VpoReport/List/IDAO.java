package ru.tandemservice.vpormc.vpo.reports.VpoReport.List;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.dao.IUniDao;


public interface IDAO extends IUniDao<Model> {
    <T extends IEntity> void prepareCustomDataSource(Model model, DynamicListDataSource<T> dataSource);
}


