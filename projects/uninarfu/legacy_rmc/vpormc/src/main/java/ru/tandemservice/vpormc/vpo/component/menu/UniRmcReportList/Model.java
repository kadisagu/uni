package ru.tandemservice.vpormc.vpo.component.menu.UniRmcReportList;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

public class Model {
    private DynamicListDataSource<ReportDefinitionWrapper> dataSource;

    public DynamicListDataSource<ReportDefinitionWrapper> getDataSource()
    {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<ReportDefinitionWrapper> dataSource)
    {
        this.dataSource = dataSource;
    }

    @SuppressWarnings("rawtypes")
    public static class ReportDefinitionWrapper extends IdentifiableWrapper {
        private static final long serialVersionUID = 5633480787283172831L;
        private UniRmcReportDefinition _reportDefinition;

        public ReportDefinitionWrapper(Long id, UniRmcReportDefinition reportDefinition)
        {
            super(id, reportDefinition.getTitle());
            _reportDefinition = reportDefinition;
        }

        public UniRmcReportDefinition getReportDefinition()
        {
            return _reportDefinition;
        }
    }
}
