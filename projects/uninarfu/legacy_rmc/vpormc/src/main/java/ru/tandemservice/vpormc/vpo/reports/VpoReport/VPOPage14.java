package ru.tandemservice.vpormc.vpo.reports.VpoReport;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import ru.tandemservice.tools.ObjToListLong;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Распределение сткдентов по возрасту
 *
 * @author Администратор
 */
@SuppressWarnings("unchecked")
public class VPOPage14 extends VPOBase {
    public VPOPage14(Session session)
    {
        super(session);
    }

    public RtfDocument makeReport()
    {
        // 1 - получим шаблон
        RtfDocument document = getTemplate("page14").getClone();

        RtfDocument resultDoc = document.getClone();

        resultDoc.getElementList().clear();

        for (int i = 1; i <= 5; i++) {
            List<String[]> lines14 = new ArrayList<String[]>();
            String _key = Integer.toString(i);

            lines14 = _makerow(_key, lines14);

            if (lines14.size() != 0) {
                // можно клеить итоговый документ
                RtfDocument doc = (RtfDocument) document.getClone();

                _fillToDocument(_key, lines14, doc);

                // копируем в итоговый документ
                resultDoc.getElementList().addAll(doc.getElementList());
                resultDoc.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
            }
        }

        return resultDoc;
    }

    private List<String[]> _makerow(String developForm_code, List<String[]> retVal)
    {

        // итоговая строка
        Long _totalAll = getTotal(developForm_code, "");
        Long _totalWomen = getTotal(developForm_code, " and v_vpo.sex_code_p = '2' ");


        List<String> cells = ReportGenerate.getEmptyPage14Row();
        cells.set(0, "Всего");
        cells.set(1, "01");
        cells.set(4, _totalAll.toString());
        cells.set(5, _totalWomen.toString());
        retVal.add(cells.toArray(new String[]{}));


        List<String> cells2 = ReportGenerate.getEmptyPage14Row();
        cells2.set(0, "в том числе в возрасте (число полных лет на 1 января )");
        cells2.set(1, "");
        retVal.add(cells2.toArray(new String[]{}));


        // сравнивалка
        AgeCompare compare = new AgeCompare();
        CalculateInfo(developForm_code, compare, "", 1);
        CalculateInfo(developForm_code, compare, " and v_vpo.sex_code_p = '2' ", 0);

        // генерируем строки
        List<AgeCompare.RowCompare> rows = compare.get_rows();

        // выводим обязательную часть
        int _rowKey = 1;
        for (AgeCompare.RowCompare rc : rows) {
            if (rc.isPrintAll()) {
                _rowKey++;

                List<String> cellsAdd = ReportGenerate.getEmptyPage14Row();
                cellsAdd.set(0, rc.getRowName());
                cellsAdd.set(1, Integer.toString(_rowKey));

                cellsAdd.set(4, Integer.toString(rc.getCountAll()));
                cellsAdd.set(5, Integer.toString(rc.getCountWoman()));
                retVal.add(cellsAdd.toArray(new String[]{}));
            }
        }

        // левые строки

        List<String> cells3 = ReportGenerate.getEmptyPage14Row();
        cells3.set(0, "в возрасте моложе 15 лет (указать каком):");
        cells3.set(1, "20");
        retVal.add(cells3.toArray(new String[]{}));

        for (AgeCompare.RowCompare rc : rows) {
            if (!rc.isPrintAll() && rc.getCountAll() != 0) {

                List<String> cellsAdd = ReportGenerate.getEmptyPage14Row();
                cellsAdd.set(0, rc.getRowName());
                cellsAdd.set(1, "");
                cellsAdd.set(4, Integer.toString(rc.getCountAll()));
                cellsAdd.set(5, Integer.toString(rc.getCountWoman()));
                retVal.add(cellsAdd.toArray(new String[]{}));
            }
        }

        // нужно проверить наличие данных
        if (compare.isEmpty())
            return new ArrayList<String[]>();
        else
            return retVal;
    }

    private Long getTotal(String developForm_code, String whereCond)
    {

        String _sql = "select " +
                "  SUM(stu_count) as totalSum " +
                " from ( " + ReportGenerate.Sql + " ) as v_vpo " +
                "  where " +
                "  developform_code_p = ? " + whereCond;


        SQLQuery qv = session.createSQLQuery(_sql);
        qv.setParameter(0, developForm_code);

        List<Long> ids = ObjToListLong.ToListLong(qv.list());

        Long _count = (long) 0;
        if (ids.size() != 0)
            _count = ids.get(0);

        return _count;
    }

    private void _fillToDocument
            (
                    String developForm_code, List<String[]> table,
                    RtfDocument document)
    {

        // Таблица в шаблоне - это T
        RtfTableModifier tablemod = createTableModifier();
        RtfInjectModifier varmod = createInjectModifier();

        // 2 - модифачим
        tablemod = processTableModifier(tablemod, table);
        varmod = processInjectModifier(varmod, developForm_code);

        // 3 - применяем к документу
        tablemod.modify(document);
        varmod.modify(document);

    }

    /**
     * Производим основной расчет страницы
     */
    public void CalculateInfo(String developForm_code, AgeCompare compare, String whereCond, int isAll)
    {

        // считаем всех
        String _sql = "select " +
                "  SUM(stu_count) as totalSum " +
                "  ,  birthdate " +
                " from ( " + ReportGenerate.Sql + " ) as v_vpo " +
                "  where " +
                "  developform_code_p = ? " + whereCond +
                "  group by " +
                "  birthdate ";

        SQLQuery qv = session.createSQLQuery(_sql);
        qv.setParameter(0, developForm_code);

        List<Object[]> itemList = qv.list();


        for (final Object[] item : itemList) {
            int totalSum = Integer.parseInt((item[0]).toString());
            Date dDate = (Date) item[1];
            if (dDate == null)
                dDate = new Date();
            compare.CompareAge(totalSum, isAll, dDate, true);
        }


    }
}
