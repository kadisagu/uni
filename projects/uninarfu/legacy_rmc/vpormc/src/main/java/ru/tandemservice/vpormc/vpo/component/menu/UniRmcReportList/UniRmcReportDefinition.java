package ru.tandemservice.vpormc.vpo.component.menu.UniRmcReportList;

public class UniRmcReportDefinition implements IUniRmcReportDefinition {
    private String _id;
    private String _title;
    private String _permissionKey;
    private String _componentName;


    public String getId()
    {
        return _id;
    }


    public String getTitle()
    {
        return _title;
    }


    public String getComponentName()
    {
        return _componentName;
    }


    public String getPermissionKey()
    {
        return _permissionKey;
    }

    public void setId(String id)
    {
        _id = id;
    }

    public void setTitle(String title)
    {
        _title = title;
    }

    public void setPermissionKey(String permissionKey)
    {
        _permissionKey = permissionKey;
    }

    public void setComponentName(String componentName)
    {
        _componentName = componentName;
    }
}
