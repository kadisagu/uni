package ru.tandemservice.uni.entity.report;

import ru.tandemservice.uni.IStorableReport;
import ru.tandemservice.uni.entity.report.gen.UniRmcStorableReportGen;

import java.util.Date;

/**
 * Хранимые отчеты
 */
public class UniRmcStorableReport extends UniRmcStorableReportGen implements IStorableReport {

    public Date getFormingDate() {

        return getCreateDate();
    }
}