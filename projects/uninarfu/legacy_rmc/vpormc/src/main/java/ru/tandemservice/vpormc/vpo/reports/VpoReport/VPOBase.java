package ru.tandemservice.vpormc.vpo.reports.VpoReport;


import org.hibernate.Session;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.vpo.UniRmcVPOTemplateDocument;

import java.util.List;

public class VPOBase {
    public Session session;

    public VPOBase(Session session)
    {
        this.session = session;
    }

    public DatabaseFile createFile(RtfDocument document)
    {
        DatabaseFile result = new DatabaseFile();
        result.setContent(RtfUtil.toByteArray(document));
        return result;
    }

    public RtfInjectModifier processInjectModifier(RtfInjectModifier varmod, String developForm_code)
    {
        // нужно найти форму освоения по коду
        // session.get(DevelopForm.class,)
        MQBuilder builder = new MQBuilder(DevelopForm.ENTITY_CLASS, "slot");
        builder.add(MQExpression.eq("slot", DevelopForm.P_CODE, developForm_code));
        List<DevelopForm> lst = builder.getResultList(session);

        if (lst.size() != 0) {
            String _printCode = lst.get(0).getTitle();
            varmod.put("developform_code", _printCode);
        }

        return varmod;
    }

    public RtfTableModifier processTableModifier(RtfTableModifier tablemod, List<String[]> table)
    {

        String[][] t = table.toArray(new String[][]{});
        tablemod.put("T", t);
        return tablemod;

    }

    public RtfTableModifier createTableModifier()
    {
        RtfTableModifier retVal = new RtfTableModifier();
        return retVal;
    }


    public RtfInjectModifier createInjectModifier()
    {
        RtfInjectModifier retVal = new RtfInjectModifier();
        return retVal;
    }


    public RtfDocument getTemplate(String templateName)
    {
        ITemplateDocument result = UniDaoFacade.getCoreDao().getCatalogItem(UniRmcVPOTemplateDocument.class, templateName);
        return new RtfReader().read(result.getContent());
    }
}
