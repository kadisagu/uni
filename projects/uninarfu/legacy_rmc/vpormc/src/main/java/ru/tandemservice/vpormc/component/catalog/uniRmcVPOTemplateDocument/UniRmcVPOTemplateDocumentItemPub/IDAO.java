package ru.tandemservice.vpormc.component.catalog.uniRmcVPOTemplateDocument.UniRmcVPOTemplateDocumentItemPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultPrintCatalogItemPub.IDefaultPrintCatalogItemPubDAO;
import ru.tandemservice.uni.entity.vpo.UniRmcVPOTemplateDocument;


public interface IDAO extends IDefaultPrintCatalogItemPubDAO<UniRmcVPOTemplateDocument, Model> {
}
