package ru.tandemservice.vpormc.vpo.reports.VpoReport.List;

import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.vpormc.vpo.reports.VpoReport.DoAction;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.report.UniRmcStorableReport;

import java.util.Date;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model)
    {
        model.setCreateDate(new Date());
    }

    public <T extends IEntity> void prepareCustomDataSource(Model model, DynamicListDataSource<T> dataSource)
    {

        MQBuilder builder = new MQBuilder(UniRmcStorableReport.ENTITY_CLASS, "report");
        builder.add(MQExpression.eq("report", UniRmcStorableReport.P_BIN_NAME, IReportName.REPORT_NAME));
        new OrderDescriptionRegistry("report").applyOrder(builder, model.getDataSource().getEntityOrder());

        UniUtils.createPage(model.getDataSource(), builder, getSession());

    }

    @Override
    public void update(Model model) {
        final Session session = getSession();

        // ReportGenerate _action = new ReportGenerate();
        DoAction _action = new DoAction(session);
        DatabaseFile content = _action.getContent();

        if (content != null) {
            save(content);

            // создаем отчет
            UniRmcStorableReport rep = new UniRmcStorableReport();
            rep.setCreateDate(model.getCreateDate());

            String prim = "Отчет ВПО ";
            rep.setPrim(prim);
            rep.setContent(content);
            rep.setBinName(IReportName.REPORT_NAME);

            save(rep);
        }
    }


}
