package ru.tandemservice.uniecrmc.component.report.EnrollmentResultByCategory.Pub;

import ru.tandemservice.uniecrmc.entity.entrant.EnrollmentResultByCategoryReport;
import ru.tandemservice.uni.dao.UniDao;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        model.setReport(get(EnrollmentResultByCategoryReport.class, model.getReport().getId()));
    }
}
