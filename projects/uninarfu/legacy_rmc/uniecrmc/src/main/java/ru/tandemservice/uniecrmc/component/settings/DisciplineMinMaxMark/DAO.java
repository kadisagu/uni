package ru.tandemservice.uniecrmc.component.settings.DisciplineMinMaxMark;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.BlockColumn;
import ru.tandemservice.uniecrmc.entity.entrant.Discipline2RealizationWayRelationExt;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DAO
        extends ru.tandemservice.uniec.component.settings.DisciplineMinMaxMark.DAO
{

    @Override
    @SuppressWarnings({"unchecked"})
    public void prepareListDataSource(ru.tandemservice.uniec.component.settings.DisciplineMinMaxMark.Model model)
    {
        super.prepareListDataSource(model);


        Map<Long, Discipline2RealizationWayRelationExt> valueMap = new HashMap<>();
        List<Discipline2RealizationWayRelation> disciplines = model.getDisciplines();
        for (Discipline2RealizationWayRelation d2rw : disciplines) {
            Discipline2RealizationWayRelationExt ext = getExtension(d2rw, model);
            valueMap.put(d2rw.getId(), ext);

            //System.out.println(d2rw.getPriority() + " " + d2rw.getTitle());

        }
        ((BlockColumn<Discipline2RealizationWayRelationExt>) model.getDataSource().getColumn("passMarkContract")).setValueMap(valueMap);
    }

    private Discipline2RealizationWayRelationExt getExtension(IEntity entity, ru.tandemservice.uniec.component.settings.DisciplineMinMaxMark.Model model)
    {
        Model modelExt = (Model) model;

        if (modelExt.getMapExt().containsKey(entity.getId()))
            return modelExt.getMapExt().get(entity.getId());

        Discipline2RealizationWayRelationExt retVal = get(Discipline2RealizationWayRelationExt.class, Discipline2RealizationWayRelationExt.L_DISCIPLINE2_REALIZATION_WAY_RELATION, entity);
        Discipline2RealizationWayRelation d2wr = (Discipline2RealizationWayRelation) entity;

        if (retVal == null) {
            retVal = new Discipline2RealizationWayRelationExt();
            retVal.setDiscipline2RealizationWayRelation(d2wr);
            retVal.setPassMarkContract(d2wr.getPassMark());
        }
        modelExt.getMapExt().put(entity.getId(), retVal);
        return retVal;
    }

    private void removeExtFromMap(IEntity entity, ru.tandemservice.uniec.component.settings.DisciplineMinMaxMark.Model model)
    {
        Model modelExt = (Model) model;

        if (modelExt.getMapExt().containsKey(entity.getId()))
            modelExt.getMapExt().remove(entity.getId());
    }


    @SuppressWarnings("unchecked")
    @Override
    public void update(
            ru.tandemservice.uniec.component.settings.DisciplineMinMaxMark.Model model)
    {
        super.update(model);
        Map<Long, Discipline2RealizationWayRelationExt> mapValue = ((BlockColumn<Discipline2RealizationWayRelationExt>) model.getDataSource().getColumn("passMarkContract")).getValueMap();
        for (Long id : mapValue.keySet()) {
            Discipline2RealizationWayRelationExt ext = mapValue.get(id);
            saveOrUpdate(ext);
            removeExtFromMap(ext.getDiscipline2RealizationWayRelation(), model);
        }
        getSession().flush();
    }

    @SuppressWarnings("unchecked")
    @Override
    public void validate(
            ru.tandemservice.uniec.component.settings.DisciplineMinMaxMark.Model model,
            ErrorCollector errors)
    {
        super.validate(model, errors);

        List<String> fieldsWithPassMarkErrorIds = new ArrayList<>();

        Map<Long, Discipline2RealizationWayRelationExt> mapValue = ((BlockColumn<Discipline2RealizationWayRelationExt>) model.getDataSource().getColumn("passMarkContract")).getValueMap();
        for (Discipline2RealizationWayRelation discipline : model.getDisciplines()) {
            Long id = discipline.getId();

            int maxMark = discipline.getMaxMark();
            int minMark = discipline.getMinMark();
            double passMark = mapValue.get(id).getPassMarkContract();

            if ((passMark < minMark) || (passMark > maxMark)) {
                fieldsWithPassMarkErrorIds.add("passMarkContract" + id);
            }
        }

        if (!fieldsWithPassMarkErrorIds.isEmpty()) {
            errors.add("Зачетный балл (договор) должен быть в диапазоне баллов от минимального до максимального.", (String[]) fieldsWithPassMarkErrorIds.toArray(new String[fieldsWithPassMarkErrorIds.size()]));
        }

    }

}
