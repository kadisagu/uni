package ru.tandemservice.uniecrmc.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommended;
import ru.tandemservice.uniecrmc.entity.entrant.EcgEntrantRecommendedExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение сущности Рекомендованный абитуриент основного распределения
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcgEntrantRecommendedExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniecrmc.entity.entrant.EcgEntrantRecommendedExt";
    public static final String ENTITY_NAME = "ecgEntrantRecommendedExt";
    public static final int VERSION_HASH = -1862405191;
    private static IEntityMeta ENTITY_META;

    public static final String L_ECG_ENTRANT_RECOMMENDED = "ecgEntrantRecommended";
    public static final String P_HAVE_SPECIAL_RIGHTS = "haveSpecialRights";

    private EcgEntrantRecommended _ecgEntrantRecommended;     // Рекомендованный абитуриент основного распределения
    private boolean _haveSpecialRights;     // Имеет особые права

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Рекомендованный абитуриент основного распределения. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EcgEntrantRecommended getEcgEntrantRecommended()
    {
        return _ecgEntrantRecommended;
    }

    /**
     * @param ecgEntrantRecommended Рекомендованный абитуриент основного распределения. Свойство не может быть null и должно быть уникальным.
     */
    public void setEcgEntrantRecommended(EcgEntrantRecommended ecgEntrantRecommended)
    {
        dirty(_ecgEntrantRecommended, ecgEntrantRecommended);
        _ecgEntrantRecommended = ecgEntrantRecommended;
    }

    /**
     * @return Имеет особые права. Свойство не может быть null.
     */
    @NotNull
    public boolean isHaveSpecialRights()
    {
        return _haveSpecialRights;
    }

    /**
     * @param haveSpecialRights Имеет особые права. Свойство не может быть null.
     */
    public void setHaveSpecialRights(boolean haveSpecialRights)
    {
        dirty(_haveSpecialRights, haveSpecialRights);
        _haveSpecialRights = haveSpecialRights;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EcgEntrantRecommendedExtGen)
        {
            setEcgEntrantRecommended(((EcgEntrantRecommendedExt)another).getEcgEntrantRecommended());
            setHaveSpecialRights(((EcgEntrantRecommendedExt)another).isHaveSpecialRights());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcgEntrantRecommendedExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcgEntrantRecommendedExt.class;
        }

        public T newInstance()
        {
            return (T) new EcgEntrantRecommendedExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "ecgEntrantRecommended":
                    return obj.getEcgEntrantRecommended();
                case "haveSpecialRights":
                    return obj.isHaveSpecialRights();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "ecgEntrantRecommended":
                    obj.setEcgEntrantRecommended((EcgEntrantRecommended) value);
                    return;
                case "haveSpecialRights":
                    obj.setHaveSpecialRights((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "ecgEntrantRecommended":
                        return true;
                case "haveSpecialRights":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "ecgEntrantRecommended":
                    return true;
                case "haveSpecialRights":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "ecgEntrantRecommended":
                    return EcgEntrantRecommended.class;
                case "haveSpecialRights":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EcgEntrantRecommendedExt> _dslPath = new Path<EcgEntrantRecommendedExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcgEntrantRecommendedExt");
    }
            

    /**
     * @return Рекомендованный абитуриент основного распределения. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EcgEntrantRecommendedExt#getEcgEntrantRecommended()
     */
    public static EcgEntrantRecommended.Path<EcgEntrantRecommended> ecgEntrantRecommended()
    {
        return _dslPath.ecgEntrantRecommended();
    }

    /**
     * @return Имеет особые права. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EcgEntrantRecommendedExt#isHaveSpecialRights()
     */
    public static PropertyPath<Boolean> haveSpecialRights()
    {
        return _dslPath.haveSpecialRights();
    }

    public static class Path<E extends EcgEntrantRecommendedExt> extends EntityPath<E>
    {
        private EcgEntrantRecommended.Path<EcgEntrantRecommended> _ecgEntrantRecommended;
        private PropertyPath<Boolean> _haveSpecialRights;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Рекомендованный абитуриент основного распределения. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EcgEntrantRecommendedExt#getEcgEntrantRecommended()
     */
        public EcgEntrantRecommended.Path<EcgEntrantRecommended> ecgEntrantRecommended()
        {
            if(_ecgEntrantRecommended == null )
                _ecgEntrantRecommended = new EcgEntrantRecommended.Path<EcgEntrantRecommended>(L_ECG_ENTRANT_RECOMMENDED, this);
            return _ecgEntrantRecommended;
        }

    /**
     * @return Имеет особые права. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EcgEntrantRecommendedExt#isHaveSpecialRights()
     */
        public PropertyPath<Boolean> haveSpecialRights()
        {
            if(_haveSpecialRights == null )
                _haveSpecialRights = new PropertyPath<Boolean>(EcgEntrantRecommendedExtGen.P_HAVE_SPECIAL_RIGHTS, this);
            return _haveSpecialRights;
        }

        public Class getEntityClass()
        {
            return EcgEntrantRecommendedExt.class;
        }

        public String getEntityName()
        {
            return "ecgEntrantRecommendedExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
