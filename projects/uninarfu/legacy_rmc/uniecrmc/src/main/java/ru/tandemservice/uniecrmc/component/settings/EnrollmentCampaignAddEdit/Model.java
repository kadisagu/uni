package ru.tandemservice.uniecrmc.component.settings.EnrollmentCampaignAddEdit;

import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionRestrictionRMC;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;

import java.util.ArrayList;
import java.util.List;

public class Model extends ru.tandemservice.uniec.component.settings.EnrollmentCampaignAddEdit.Model {

    private boolean disabled = false;
    private List<IdentifiableWrapper> optionGroupList;
    private IdentifiableWrapper priority;

    private List<IdentifiableWrapper> groupList;
    private IdentifiableWrapper useGroup;

    private boolean firstLoad = true;
    private boolean useLevelInRestrition;
    private List<EnrollmentDirectionRestrictionRMC> restrictionList = new ArrayList<>();
    private EnrollmentDirectionRestrictionRMC currentRestriction;
    private Integer currentRestrictionIndex;
    private List<StructureEducationLevels> strLevelList;

    public List<IdentifiableWrapper> getGroupList() {
        return groupList;
    }

    public void setGroupList(List<IdentifiableWrapper> groupList) {
        this.groupList = groupList;
    }

    public IdentifiableWrapper getUseGroup() {
        return useGroup;
    }

    public void setUseGroup(IdentifiableWrapper useGroup) {
        this.useGroup = useGroup;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public List<IdentifiableWrapper> getOptionGroupList() {
        return optionGroupList;
    }

    public void setOptionGroupList(List<IdentifiableWrapper> optionGroupList) {
        this.optionGroupList = optionGroupList;
    }

    public IdentifiableWrapper getPriority() {
        return priority;
    }

    public void setPriority(IdentifiableWrapper priority) {
        this.priority = priority;
    }

    public List<EnrollmentDirectionRestrictionRMC> getRestrictionList() {
        return restrictionList;
    }

    public void setRestrictionList(List<EnrollmentDirectionRestrictionRMC> restrictionList) {
        this.restrictionList = restrictionList;
    }

    public EnrollmentDirectionRestrictionRMC getCurrentRestriction() {
        return currentRestriction;
    }

    public void setCurrentRestriction(EnrollmentDirectionRestrictionRMC currentRestriction) {
        this.currentRestriction = currentRestriction;
    }

    public List<StructureEducationLevels> getStrLevelList() {
        return strLevelList;
    }

    public void setStrLevelList(List<StructureEducationLevels> strLevelList) {
        this.strLevelList = strLevelList;
    }

    public Integer getCurrentRestrictionIndex() {
        return currentRestrictionIndex;
    }

    public void setCurrentRestrictionIndex(Integer currentRestrictionIndex) {
        this.currentRestrictionIndex = currentRestrictionIndex;
    }

    public boolean isUseLevelInRestrition() {
        return useLevelInRestrition;
    }

    public void setUseLevelInRestrition(boolean useLevelInRestrition) {
        this.useLevelInRestrition = useLevelInRestrition;
    }

    public boolean isFirstLoad() {
        return firstLoad;
    }

    public void setFirstLoad(boolean firstLoad) {
        this.firstLoad = firstLoad;
    }

}
