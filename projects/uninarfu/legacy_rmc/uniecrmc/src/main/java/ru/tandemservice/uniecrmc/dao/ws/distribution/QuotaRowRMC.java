package ru.tandemservice.uniecrmc.dao.ws.distribution;

import ru.tandemservice.uniec.ws.distribution.EnrollmentDistributionEnvironmentNode;

public class QuotaRowRMC extends EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow {

    public SrQuotaRow srQuotaRow;

    public QuotaRowRMC(EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow quotaRow, SrQuotaRow srQuotaRow) {
        super(quotaRow.id, quotaRow.count, quotaRow.countBegin, quotaRow.defaultTaKind, quotaRow.taQuota);
        this.srQuotaRow = srQuotaRow;
    }

    public static class SrQuotaRow {
        public String id;
        public int count;
        public int countBegin;

        public SrQuotaRow() {

        }

        public SrQuotaRow(String id, int count, int countBegin) {
            this.id = id;
            this.count = count;
            this.countBegin = countBegin;
        }
    }
}
