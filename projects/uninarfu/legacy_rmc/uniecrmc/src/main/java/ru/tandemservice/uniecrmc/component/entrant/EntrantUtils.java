package ru.tandemservice.uniecrmc.component.entrant;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.uniecrmc.entity.entrant.Entrant2SpecialConditions;
import ru.tandemservice.uniecrmc.entity.entrant.EntrantExt;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniec.entity.entrant.Entrant;

import java.util.List;

public class EntrantUtils {
    public static EntrantExt getEntrantExt(Entrant entrant)
    {
        EntrantExt entrantExt = UniDaoFacade.getCoreDao().get(EntrantExt.class, EntrantExt.entrant().s(), entrant);
        if (entrantExt != null) {
            return entrantExt;
        }
        entrantExt = new EntrantExt();
        entrantExt.setEntrant(entrant);
        UniDaoFacade.getCoreDao().saveOrUpdate(entrantExt);
        return entrantExt;

    }

    public static String getEntrantSpecialConditions(Entrant entrant)
    {
        List<Entrant2SpecialConditions> entrant2SpecialConditionses = UniDaoFacade.getCoreDao().getList(Entrant2SpecialConditions.class, Entrant2SpecialConditions.entrant().s(), entrant);
        if (CollectionUtils.isEmpty(entrant2SpecialConditionses)) {
            return "";
        }
        List<String> conditions = CommonBaseUtil.getPropertiesList(entrant2SpecialConditionses, Entrant2SpecialConditions.specialConditionsForEntrant().title().s());

        return StringUtils.join(conditions, ", ");

    }

    public static Integer getEnrollmentPriority(Entrant entrant)
    {
        EntrantExt entrantExt = UniDaoFacade.getCoreDao().get(EntrantExt.class, EntrantExt.entrant().s(), entrant);

        return entrantExt == null ? null : entrantExt.getEnrollmentPriority();

    }
}
