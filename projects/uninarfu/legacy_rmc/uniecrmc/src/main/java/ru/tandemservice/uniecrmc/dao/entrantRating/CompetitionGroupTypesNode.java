package ru.tandemservice.uniecrmc.dao.entrantRating;

import ru.tandemservice.uniecrmc.dao.entrantRating.CompetitionGroup.CompetitionGroupType;

import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

public class CompetitionGroupTypesNode {

    @XmlElement(name = "CompetitionGroupType")
    public List<RowNode> list = new ArrayList<>();

    public CompetitionGroupTypesNode() {
    }

    public void add(CompetitionGroupType entity) {
        RowNode node = new RowNode("" + entity.getId(), entity.getTitle(), "");
        list.add(node);
    }
}
