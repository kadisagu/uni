package ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util;

import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgCortegeDTO;
import ru.tandemservice.uniec.entity.catalog.EnrollmentRecommendation;

import java.util.List;

public interface IEcgCortegeRMC extends IEcgCortegeDTO {

    public List<EnrollmentRecommendation> getRecommendations();

    public List<WrapChoseEntranseDiscipline> getDisciplines();

    public boolean isHaveSpecialRights();

    public String getDisciplinesHTMLDescription();

    public Integer getEnrollmentPriority();

    public String getEntrantRequestsInfoHtml();

}
