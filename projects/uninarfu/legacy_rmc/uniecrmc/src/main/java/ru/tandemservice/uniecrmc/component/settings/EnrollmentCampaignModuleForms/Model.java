package ru.tandemservice.uniecrmc.component.settings.EnrollmentCampaignModuleForms;

import java.util.ArrayList;
import java.util.List;

public class Model extends ru.tandemservice.uniec.component.settings.EnrollmentCampaignModuleForms.Model {

    private List<Block> blockList = new ArrayList<>();
    private Block currentBlock;
    private Block.Property currentProperty;

    public List<Block> getBlockList() {
        return blockList;
    }

    public void setBlockList(List<Block> blockList) {
        this.blockList = blockList;
    }

    public Block getCurrentBlock() {
        return currentBlock;
    }

    public void setCurrentBlock(Block currentBlock) {
        this.currentBlock = currentBlock;
    }

    public Block.Property getCurrentProperty() {
        return currentProperty;
    }

    public void setCurrentProperty(Block.Property currentProperty) {
        this.currentProperty = currentProperty;
    }

}
