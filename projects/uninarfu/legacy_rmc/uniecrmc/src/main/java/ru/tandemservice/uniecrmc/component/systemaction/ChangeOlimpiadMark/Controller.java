package ru.tandemservice.uniecrmc.component.systemaction.ChangeOlimpiadMark;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller
        extends AbstractBusinessController<IDAO, Model>
{

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setSettings(component.getSettings());
        getDao().prepare(model);
    }


    public void onClickApply(IBusinessComponent component)
    {
        IDAO dao = (IDAO) getDao();
        Model model = (Model) getModel(component);
        dao.update(model);

    }

    public void onRefresh(IBusinessComponent component)
    {
        Model model = getModel(component);

        component.saveSettings();
        getDao().prepare(model);
    }


}
