package ru.tandemservice.uniecrmc.dao.entrantRating;

import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement(name = "enrollmentCampaign")
public class EnrollmentCampaignNode {
    @XmlAttribute
    public Long id;

    @XmlAttribute
    public String title;

    @XmlAttribute
    public String educationYear;

    @XmlAttribute
    public Date currentTime;

    //справочники
    @XmlElement(name = "TargetAdmissionKinds")
    public TargetAdmissionKindNode targetAdmissionKindNode;

    @XmlElement(name = "CompetitionKinds")
    public CompetitionKindNode competitionKindNode;

    @XmlElement(name = "Benefits")
    public BenefitNode benefitNode;

    @XmlElement(name = "EnrollmentRecommendations")
    public EnrollmentRecommendationNode enrollmentRecommendationNode;

    @XmlElement(name = "CompensationTypes")
    public CompensationTypeNode compensationTypeNode;

    @XmlElement(name = "DevelopForms")
    public DevelopFormNode developFormNode;

    @XmlElement(name = "DevelopPeriods")
    public DevelopPeriodNode developPeriodNode;

    @XmlElement(name = "DevelopTechs")
    public DevelopTechNode developTechNode;

    @XmlElement(name = "DevelopConditions")
    public DevelopConditionNode developConditionNode;

    @XmlElement(name = "CompetitionGroupTypes")
    public CompetitionGroupTypesNode competitionGroupTypesNode;

    @XmlElement(name = "EntrantStates")
    public EntrantStateNode entrantStateNode;

    @XmlElement(name = "EduLevels")
    public EduLevelNode eduLevelNode;


    //направления приема
    @XmlElement(name = "EnrollmentDirections")
    public EnrollmentDirectionsNode enrollmentDirectionsNode;

    public EnrollmentCampaignNode() {
    }

    public EnrollmentCampaignNode(EnrollmentCampaign ec) {
        this.id = ec.getId();
        this.title = ec.getTitle();
        this.educationYear = ec.getEducationYear().getTitle();
        this.currentTime = new Date();
    }

    public void setEduLevelNode(EduLevelNode eduLevelNode) {
        this.eduLevelNode = eduLevelNode;
    }

    public void setTargetAdmissionKindNode(TargetAdmissionKindNode node) {
        this.targetAdmissionKindNode = node;
    }

    public void setCompetitionKindNode(CompetitionKindNode node) {
        this.competitionKindNode = node;
    }

    public void setBenefitNode(BenefitNode node) {
        this.benefitNode = node;
    }

    public void setEnrollmentRecommendationNode(EnrollmentRecommendationNode node) {
        this.enrollmentRecommendationNode = node;
    }

    public void setCompensationTypeNode(CompensationTypeNode node) {
        this.compensationTypeNode = node;
    }

    public void setEnrollmentDirectionsNode(EnrollmentDirectionsNode node) {
        this.enrollmentDirectionsNode = node;
    }

    public void setDevelopFormNode(DevelopFormNode node) {
        this.developFormNode = node;
    }

    public void setDevelopPeriodNode(DevelopPeriodNode node) {
        this.developPeriodNode = node;
    }

    public void setDevelopTechNode(DevelopTechNode node) {
        this.developTechNode = node;
    }

    public void setDevelopConditionNode(DevelopConditionNode node) {
        this.developConditionNode = node;
    }

    public void setCompetitionGroupTypesNode(CompetitionGroupTypesNode node) {
        this.competitionGroupTypesNode = node;
    }

    public void setEntrantStatesNode(EntrantStateNode node) {
        this.entrantStateNode = node;
    }
}
