package ru.tandemservice.uniecrmc.component.catalog.targetAdmissionKind.TargetAdmissionKindItemPub;

import ru.tandemservice.uniecrmc.entity.entrant.TargetAdmissionKindRMC;

import java.util.List;

public class Model extends ru.tandemservice.uniec.component.catalog.targetAdmissionKind.TargetAdmissionKindItemPub.Model {


    private TargetAdmissionKindRMC _targetAdmissionKindRMC;

    public TargetAdmissionKindRMC getTargetAdmissionKindRMC()
    {
        return _targetAdmissionKindRMC;
    }

    public void setTargetAdmissionKindRMC(TargetAdmissionKindRMC targetAdmissionKindRMC)
    {
        _targetAdmissionKindRMC = targetAdmissionKindRMC;
    }

    public List getParentList()
    {
        return _parentList;
    }

    public void setParentList(List parentList)
    {
        _parentList = parentList;
    }

    private List _parentList;

    boolean _hasContractNumber;

    public boolean isContractNumber()
    {
        return _hasContractNumber;
    }

    public void setHasContractNumber(boolean hasContractNumber)
    {
        _hasContractNumber = hasContractNumber;
    }


}
