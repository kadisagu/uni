package ru.tandemservice.uniecrmc.component.entrant.EntrantPub;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniecrmc.entity.entrant.Entrant2IndividualAchievements;
import ru.tandemservice.uniec.entity.entrant.Entrant;

public class Model {

    private Entrant entrant = new Entrant();

    private Entrant2IndividualAchievements achievements;
    private DynamicListDataSource<Entrant2IndividualAchievements> dataSource;

    public Entrant getEntrant() {
        return entrant;
    }

    public void setEntrant(Entrant entrant) {
        this.entrant = entrant;
    }

    public Entrant2IndividualAchievements getAchievements() {
        return achievements;
    }

    public void setAchievements(Entrant2IndividualAchievements achievements) {
        this.achievements = achievements;
    }

    public DynamicListDataSource<Entrant2IndividualAchievements> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<Entrant2IndividualAchievements> dataSource) {
        this.dataSource = dataSource;
    }

    public Boolean isNoProtocol() {
        return getDataSource().getEntityList().isEmpty();
    }

    public void setNoProtocol(Boolean b) {
    }

}
