package ru.tandemservice.uniecrmc.dao.entrantRating;

import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class EntrantNode {

    @XmlAttribute
    public Long id;

    @XmlAttribute
    public String fio;

    @XmlAttribute
    public int index;

    @XmlAttribute
    public boolean enrolled;

    @XmlAttribute
    public String requestNumber;

    @XmlAttribute
    public String competitionKindId;

    @XmlAttribute
    public Double averageEduInstitutionMark;

    @XmlAttribute
    public boolean hasSpecialRights;

    @XmlAttribute
    public boolean isTargetAdmission;

    @XmlAttribute(required = false)
    public String targetAdmission;

    @XmlAttribute
    public Double finalMark;

    @XmlAttribute
    public boolean takeAwayDocument;

    @XmlAttribute
    public boolean documentOriginal;

    @XmlAttribute
    public String entrantStateId;

    @XmlElement(name = "Marks")
    public MarksNode marksNode;

    @XmlElement(name = "EnrollmentRecommendations")
    public EnrollmentRecommendationNode enrollmentRecommendationNode;

    @XmlElement(name = "Benefits")
    public BenefitNode benefitNode;

    @XmlElement(name = "Order", required = false)
    public OrderNode orderNode;

    @XmlAttribute(required = false)
    public Boolean paid;

    public EntrantNode() {
    }

    public EntrantNode(EcgCortegeRMC container, int index) {
        RequestedEnrollmentDirection red = (RequestedEnrollmentDirection) container.getEntity();

        this.id = red.getId();
        this.fio = container.getFio();
        this.index = index;
        this.enrolled = container.isEnrolled();

        this.requestNumber = "" + red.getEntrantRequest().getRegNumber();
        this.competitionKindId = red.getCompetitionKind().getCode();

        this.averageEduInstitutionMark = container.getCertificateAverageMark() != null ? container.getCertificateAverageMark() : 0.0;
        this.hasSpecialRights = container.isHaveSpecialRights();

        this.isTargetAdmission = red.isTargetAdmission();
        if (container.getTargetAdmissionKind() != null)
            this.targetAdmission = container.getTargetAdmissionKind().getCode();

        this.finalMark = container.getFinalMark();

        this.takeAwayDocument = red.getEntrantRequest().isTakeAwayDocument();
        this.documentOriginal = red.isOriginalDocumentHandedIn();
        this.entrantStateId = red.getState().getCode();

        //дополнительные записи
        this.paid = container.getPaid();

        if (container.getExtract() != null) {
            this.orderNode = new OrderNode(container.getExtract());
        }
    }


}
