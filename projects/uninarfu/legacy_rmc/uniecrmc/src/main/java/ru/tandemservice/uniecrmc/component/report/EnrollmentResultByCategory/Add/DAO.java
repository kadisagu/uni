package ru.tandemservice.uniecrmc.component.report.EnrollmentResultByCategory.Add;

import org.hibernate.Session;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import ru.tandemservice.uniecrmc.entity.entrant.EnrollmentResultByCategoryReport;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.codes.EntrantStateCodes;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        Session session = getSession();

        model.setParameters(new MultiEnrollmentDirectionUtil.Parameters());
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());

        model.setQualificationListModel(new QualificationModel(getSession()));
        model.setDevelopFormListModel(MultiEnrollmentDirectionUtil.createDevelopFormModel(model));
        model.setFormativeOrgUnitListModel(MultiEnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setTerritorialOrgUnitListModel(MultiEnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setDevelopConditionListModel(MultiEnrollmentDirectionUtil.createDevelopConditionModel(model));

        model.setEntrantStateListModel(new DQLFullCheckSelectModel() {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EntrantState.class, alias)
                        .where(DQLExpressions.in(DQLExpressions.property(EntrantState.code().fromAlias(alias)),
                                                 Arrays.asList(EntrantStateCodes.ENROLED, EntrantStateCodes.IN_ORDER, EntrantStateCodes.PRELIMENARY_ENROLLED)));
                FilterUtils.applySimpleLikeFilter(builder, alias, EntrantState.title().s(), filter);
                return builder;
            }
        });
    }

    @Override
    public void update(Model model) {

        Session session = getSession();

        EnrollmentResultByCategoryReport report = model.getReport();
        report.setFormingDate(Calendar.getInstance().getTime());

        if (model.isQualificationActive()) {
            report.setQualification(UniStringUtils.join(model.getQualificationList(), "title", "; "));
        }
        else {
            report.setQualification(UniStringUtils.join(getQualifications(), "title", "; "));
        }

        if (model.isFormativeOrgUnitActive()) {
            report.setFormativeOrgUnit(UniStringUtils.join(model.getFormativeOrgUnitList(), "fullTitle", "; "));
        }

        if (model.isTerritorialOrgUnitActive()) {
            report.setTerritorialOrgUnit(UniStringUtils.join(model.getTerritorialOrgUnitList(), "territorialFullTitle", "; "));
        }

        if (model.isDevelopConditionActive()) {
            report.setDevelopCondition(UniStringUtils.join(model.getDevelopConditionList(), "title", "; "));
        }

        if (model.isEntrantStateActive()) {
            report.setEntrantState(UniStringUtils.join(model.getEntrantStateList(), "title", "; "));
        }

        DatabaseFile databaseFile = new EnrollmentResultByCategoryBuilder(model, session).getContent();
        session.save(databaseFile);

        report.setContent(databaseFile);
        session.save(report);
    }

    private List<Qualifications> getQualifications() {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(Qualifications.class, "q")
                .where(DQLExpressions.in(
                        DQLExpressions.property(Qualifications.code().fromAlias("q")),
                        Arrays.asList(QualificationsCodes.BAKALAVR, QualificationsCodes.SPETSIALIST, QualificationsCodes.MAGISTR, QualificationsCodes.BAZOVYY_UROVEN_S_P_O, QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O)))
                .column("q");

        return dql.createStatement(getSession()).list();
    }
}
