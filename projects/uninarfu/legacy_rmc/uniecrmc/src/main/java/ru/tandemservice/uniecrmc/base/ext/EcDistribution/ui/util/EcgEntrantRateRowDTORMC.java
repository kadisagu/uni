package ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.EcgEntrantRateRowDTO;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgEntrantRateDirectionDTO;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EnrollmentRecommendation;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;

import java.util.List;

public class EcgEntrantRateRowDTORMC extends EcgEntrantRateRowDTO implements IEcgCortegeRMC {

    private static final long serialVersionUID = 1L;

    private List<EnrollmentRecommendation> recommendations;
    private List<WrapChoseEntranseDiscipline> disciplines;
    private List<WrapRequestEDInfo> lstWrapRequestEDInfo;
    private boolean haveSpecialRights;
    private Integer enrollmentPriority;

    public EcgEntrantRateRowDTORMC(Long entrantId, String fio,
                                   double finalMark, Double profileMark, Double averageMark,
                                   boolean originalDocumentHandedIn,
                                   boolean graduatedProfileEduInstitution, boolean targetAdmission,
                                   TargetAdmissionKind targetAdmissionKind,
                                   CompetitionKind competitionKind,
                                   List<IEcgEntrantRateDirectionDTO> directionList,
                                   List<EnrollmentRecommendation> recommendations,
                                   List<WrapChoseEntranseDiscipline> disciplines,
                                   boolean haveSpecialRights,
                                   List<WrapRequestEDInfo> lstWrapRequestEDInfo,
                                   Integer enrollmentPriority
    )
            throws ClassCastException
    {
        super(entrantId, fio, finalMark, profileMark, averageMark,
              originalDocumentHandedIn, graduatedProfileEduInstitution,
              targetAdmission, targetAdmissionKind, competitionKind, directionList);
        this.recommendations = recommendations;
        this.disciplines = disciplines;
        this.haveSpecialRights = haveSpecialRights;
        this.lstWrapRequestEDInfo = lstWrapRequestEDInfo;
        this.enrollmentPriority = enrollmentPriority;
    }

    @Override
    public String getCompetitionKindTitle() {
        return haveSpecialRights ? new StringBuilder("Вне конкурса").append("5".equals(this.getCompetitionKind().getCode()) ? "" : new StringBuilder().append(", ").append(this.getCompetitionKind().getTitle()).toString()).toString() : super.getCompetitionKindTitle();
    }

    public String getRecommendationTitle() {
        if (this.getRecommendations() != null) {
            List<String> lst = CommonBaseUtil.getPropertiesList(this.getRecommendations(), EnrollmentRecommendation.shortTitle());
            return StringUtils.join(lst, ", ");
        }
        return "";
    }


    /**
     * Инфа по всем заявам абитуриента
     *
     * @return
     */
    public String getEntrantRequestsInfoHtml()
    {
        if (lstWrapRequestEDInfo != null && lstWrapRequestEDInfo.size() > 0) {
            String rnStart = "";

            StringBuilder str = new StringBuilder("<table cellspacing='0' cellpadding='0'>");
            for (WrapRequestEDInfo rd : lstWrapRequestEDInfo) {

                String rn = rd.getRequestNumber();
                String printRn = "";

                if (!rnStart.equals(rn)) {
                    rnStart = rn;
                    printRn = rn;
                }
                else
                    printRn = "";

                str.append("<tr>")

                        .append("<td>")
                        .append(printRn) // номер заявления
                        .append("</td>")

                        .append("<td>")
                        .append(rd.getStateTitle()) // Статус
                        .append("</td>")

                        .append("<td>")
                        .append(rd.getPriority()) // Приоритет
                        .append("</td>")

                        .append("<td>")
                        .append(rd.getEducationTitle()) // короткое наименование
                        .append("</td>")

                        .append("</tr>");
            }
            str.append("</table>");
            return str.toString();

        }
        else
            return "";
    }


    public String getDisciplinesHTMLDescription() {
        if (this.getDisciplines() != null) {
            StringBuilder str = new StringBuilder("<table cellspacing='0' cellpadding='0'>");
            for (WrapChoseEntranseDiscipline discipline : this.getDisciplines()) {
                str.append("<tr>")
                        .append("<td>")
                                //.append(discipline.getEnrollmentCampaignDiscipline().getEducationSubject().getTitle())
                        .append(discipline.getTitle())
                        .append(" ")
                        .append(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(discipline.getFinalMark()))
                        .append("</td>")
                        .append("</tr>");
            }
            str.append("</table>");
            return str.toString();
        }
        return "";
    }

    public List<EnrollmentRecommendation> getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(List<EnrollmentRecommendation> recommendations) {
        this.recommendations = recommendations;
    }

    public List<WrapChoseEntranseDiscipline> getDisciplines() {
        return disciplines;
    }

    /*
    public void setDisciplines(List<ChosenEntranceDiscipline> disciplines) {
        this.disciplines = disciplines;
    }
    */
    public boolean isHaveSpecialRights() {
        return haveSpecialRights;
    }

    public void setHaveSpecialRights(boolean haveSpecialRights) {
        this.haveSpecialRights = haveSpecialRights;
    }

    @Override
    public Integer getEnrollmentPriority() {
        return enrollmentPriority;
    }


}
