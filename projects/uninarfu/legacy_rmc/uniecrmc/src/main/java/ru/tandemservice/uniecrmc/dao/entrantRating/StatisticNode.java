package ru.tandemservice.uniecrmc.dao.entrantRating;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

public class StatisticNode {

    @XmlElement(name = "EntrantsBudget")
    public EntrantsBudgetNode entrantsBudget;

    @XmlElement(name = "EntrantsContract")
    public EntrantsContractNode entrantsContract;

    public StatisticNode() {
    }

    public static class EntrantsBudgetNode extends StatisticRow {

        public EntrantsBudgetNode() {
        }

        public EntrantsBudgetNode(String count, String originalCount) {
            super(count, originalCount);
        }

        public EntrantsBudgetNode(CompetitionKindsNode competitionKind, TargetAdmissionKindsNode targetAdmissionKinds) {
            if (competitionKind.rows.isEmpty() && targetAdmissionKinds.rows.isEmpty())
                return;

            this.count = String.valueOf(Integer.parseInt(competitionKind.count == null ? "0" : competitionKind.count) + Integer.parseInt(targetAdmissionKinds.count == null ? "0" : targetAdmissionKinds.count));
            this.originalCount = String.valueOf(Integer.parseInt(competitionKind.originalCount == null ? "0" : competitionKind.originalCount) + Integer.parseInt(targetAdmissionKinds.originalCount == null ? "0" : targetAdmissionKinds.originalCount));
            this.competitionKind = competitionKind;
            this.targetAdmissionKinds = targetAdmissionKinds;
        }

        @XmlElement(name = "CompetitionKinds")
        public CompetitionKindsNode competitionKind;

        @XmlElement(name = "TargetAdmissionKinds")
        public TargetAdmissionKindsNode targetAdmissionKinds;


        public static class CompetitionKindsNode extends StatisticRow {
            @XmlElement(name = "CompetitionKind")
            public List<StatisticRow> rows = new ArrayList<StatisticRow>();

            public void add(StatisticRow row) {
                int totalCount = this.count == null ? 0 : Integer.parseInt(this.count);
                int totalOriginalCount = this.originalCount == null ? 0 : Integer.parseInt(this.originalCount);
                totalCount += Integer.parseInt(row.count);
                totalOriginalCount += Integer.parseInt(row.originalCount);
                this.count = String.valueOf(totalCount);
                this.originalCount = String.valueOf(totalOriginalCount);

                this.rows.add(row);
            }

        }

        public static class TargetAdmissionKindsNode extends StatisticRow {
            @XmlElement(name = "TargetAdmissionKind")
            public List<StatisticRow> rows = new ArrayList<StatisticRow>();

            public void add(StatisticRow row) {
                int totalCount = this.count == null ? 0 : Integer.parseInt(this.count);
                int totalOriginalCount = this.originalCount == null ? 0 : Integer.parseInt(this.originalCount);
                totalCount += Integer.parseInt(row.count);
                totalOriginalCount += Integer.parseInt(row.originalCount);
                this.count = String.valueOf(totalCount);
                this.originalCount = String.valueOf(totalOriginalCount);
                this.rows.add(row);
            }
        }

    }

    public static class EntrantsContractNode extends StatisticRow {

        public EntrantsContractNode() {
        }

        public EntrantsContractNode(String count, String originalCount) {
            super(count, originalCount);
        }

        public void add(int count, int originalCount) {
            int totalCount = this.count == null ? 0 : Integer.parseInt(this.count);
            int totalOriginalCount = this.originalCount == null ? 0 : Integer.parseInt(this.originalCount);
            totalCount += count;
            totalOriginalCount += originalCount;
            this.count = String.valueOf(totalCount);
            this.originalCount = String.valueOf(totalOriginalCount);
        }
    }

    public static class StatisticRow {
        @XmlAttribute(required = false)
        public String id;
        @XmlAttribute
        public String count;
        @XmlAttribute
        public String originalCount;

        public StatisticRow() {
        }

        public StatisticRow(String count, String originalCount) {
            this(null, count, originalCount);
        }

        public StatisticRow(String id, String count, String originalCount) {
            this.id = id;
            this.count = count;
            this.originalCount = originalCount;
        }
    }
}
