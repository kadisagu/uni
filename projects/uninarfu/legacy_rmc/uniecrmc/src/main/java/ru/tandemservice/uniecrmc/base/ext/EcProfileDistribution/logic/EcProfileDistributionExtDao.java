package ru.tandemservice.uniecrmc.base.ext.EcProfileDistribution.logic;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.tools.EnumTools;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.logic.IEcDistributionExtDao;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.EcDistributionUtil;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.EcgEntrantRateRowDTORMC;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.WrapChoseEntranseDiscipline;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.WrapRequestEDInfo;
import ru.tandemservice.uniecrmc.base.ext.EcProfileDistribution.ui.Pub.EcProfileDistributionPubUI;
import ru.tandemservice.uniecrmc.base.ext.EcProfileDistribution.util.EcgpRecommendedDTORMC;
import ru.tandemservice.uniecrmc.entity.entrant.PreliminaryEnrollmentStudentExt;
import ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.base.bo.EcDistribution.EcDistributionManager;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.EcgEntrantRateDirectionDTO;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgEntrantRateDirectionDTO;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgEntrantRateRowDTO;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgEntrantRateRowDTO.Rule;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgQuotaManager;
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.logic.EcProfileDistributionDao;
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.util.EcgpQuotaManager;
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.util.IEcgpQuotaFreeDTO;
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.util.IEcgpRecommendedDTO;
import ru.tandemservice.uniec.base.entity.ecgp.EcgpDistribution;
import ru.tandemservice.uniec.base.entity.ecgp.EcgpEntrantRecommended;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EnrollmentRecommendation;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.catalog.gen.CompetitionKindGen;
import ru.tandemservice.uniec.entity.catalog.gen.TargetAdmissionKindGen;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.NotUsedTargetAdmissionKind;

import java.util.*;

public class EcProfileDistributionExtDao extends EcProfileDistributionDao {


    @Override
    public List<? extends IEcgEntrantRateRowDTO> getEntrantRateRowList(
            EcgpDistribution distribution,
            List<TargetAdmissionKind> usedTaKindList,
            Map<CompetitionKind, Integer> competitionKindPriorityMap,
            Rule rateRule)
    {

        if (!EcDistributionUtil.isUsePriority(distribution.getConfig().getEcgItem().getEnrollmentCampaign()))
            return super.getEntrantRateRowList(distribution, usedTaKindList,
                                               competitionKindPriorityMap, rateRule);

        Map<Long, TargetAdmissionKind> taKindMap = new HashMap<Long, TargetAdmissionKind>();
        for (TargetAdmissionKind item : usedTaKindList) {
            taKindMap.put(item.getId(), item);
        }
        Map<Long, CompetitionKind> coKindMap = new HashMap<Long, CompetitionKind>();
        for (CompetitionKind item : competitionKindPriorityMap.keySet()) {
            if (item != null) {
                coKindMap.put(item.getId(), item);
            }
        }
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PreliminaryEnrollmentStudent.class, "ps");
        builder.joinPath(DQLJoinType.inner, PreliminaryEnrollmentStudent.requestedEnrollmentDirection().fromAlias("ps"), "e");

        builder.where(DQLExpressions.eq(DQLExpressions.property(PreliminaryEnrollmentStudent.compensationType().fromAlias("ps")), DQLExpressions.value(distribution.getConfig().getCompensationType())));
        builder.where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().id().fromAlias("e")), DQLExpressions.value(distribution.getConfig().getEcgItem())));
        builder.where(DQLExpressions.in(DQLExpressions.property(PreliminaryEnrollmentStudent.studentCategory().code().fromAlias("ps")), distribution.getConfig().isSecondHighAdmission() ? Arrays.asList(new String[]{"3"}) : Arrays.asList(new String[]{"1", "2"})));

        builder.where(DQLExpressions.eq(DQLExpressions.property(PreliminaryEnrollmentStudent.educationOrgUnit().fromAlias("ps")), DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().fromAlias("e"))));

        builder.where(DQLExpressions.notIn(DQLExpressions.property(PreliminaryEnrollmentStudent.id().fromAlias("ps")),
                                           new DQLSelectBuilder().fromEntity(EcgpEntrantRecommended.class, "r")
                                                   .column(DQLExpressions.property(EcgpEntrantRecommended.preStudent().id().fromAlias("r")))
                                                   .where(DQLExpressions.eq(DQLExpressions.property(EcgpEntrantRecommended.distribution().fromAlias("r")), DQLExpressions.value(distribution)))
                                                   .buildQuery()));

        boolean targetAdmission = rateRule.equals(IEcgEntrantRateRowDTO.Rule.RULE_TARGET_ADMISSION);
        boolean haveSpecilaRights = rateRule.equals(IEcgEntrantRateRowDTO.Rule.RULE_NON_COMPETITIVE);

        switch (rateRule.name()) {
            case EcProfileDistributionPubUI.RULE_TARGET_ADMISSION:
                builder.where(DQLExpressions.eq(DQLExpressions.property(PreliminaryEnrollmentStudent.targetAdmission().fromAlias("ps")), DQLExpressions.value(Boolean.TRUE)));
                break;
            case EcProfileDistributionPubUI.RULE_WITHOUT_ENTRANCE_DISCIPLINES:
                builder.where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.competitionKind().fromAlias("e")), DQLExpressions.commonValue(getByNaturalId(new CompetitionKindGen.NaturalId(UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES)))));
                break;
            case EcProfileDistributionPubUI.RULE_NON_COMPETITIVE:
                builder.joinEntity("e", DQLJoinType.inner, RequestedEnrollmentDirectionExt.class, "ext",
                                   DQLExpressions.eq(
                                           DQLExpressions.property(RequestedEnrollmentDirection.id().fromAlias("e")),
                                           DQLExpressions.property(RequestedEnrollmentDirectionExt.requestedEnrollmentDirection().id().fromAlias("ext"))))
                        .where(DQLExpressions.eqValue(DQLExpressions.property(RequestedEnrollmentDirectionExt.haveSpecialRights().fromAlias("ext")), Boolean.TRUE));
                break;
            case EcProfileDistributionPubUI.RULE_COMPETITIVE:
                builder.where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.competitionKind().fromAlias("e")), DQLExpressions.commonValue(getByNaturalId(new CompetitionKindGen.NaturalId(UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION)))));
                break;
            default:
                throw new IllegalArgumentException("Unknown rateRule: " + rateRule);
        }

        builder.joinPath(DQLJoinType.inner, RequestedEnrollmentDirection.entrantRequest().fromAlias("e"), "req");
        builder.joinPath(DQLJoinType.inner, RequestedEnrollmentDirection.competitionKind().fromAlias("e"), "com");
        builder.joinPath(DQLJoinType.left, RequestedEnrollmentDirection.targetAdmissionKind().fromAlias("e"), "tar");
        builder.joinPath(DQLJoinType.inner, RequestedEnrollmentDirection.enrollmentDirection().fromAlias("e"), "dir");
        builder.joinPath(DQLJoinType.inner, EnrollmentDirection.educationOrgUnit().fromAlias("dir"), "ou");
        builder.joinPath(DQLJoinType.inner, EntrantRequest.entrant().fromAlias("req"), "ent");
        builder.joinPath(DQLJoinType.inner, Entrant.person().fromAlias("ent"), "per");
        builder.joinPath(DQLJoinType.inner, Person.identityCard().fromAlias("per"), "card");
        builder.joinPath(DQLJoinType.left, Person.personEduInstitution().fromAlias("per"), "edu");
        builder.joinPath(DQLJoinType.left, RequestedEnrollmentDirection.profileChosenEntranceDiscipline().fromAlias("e"), "pcd");

        builder.joinEntity("ps", DQLJoinType.left, ChosenEntranceDiscipline.class, "c", DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.id().fromAlias("e")), DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("c"))));

        builder.joinEntity("ps", DQLJoinType.inner, PriorityProfileEduOu.class, "pri", DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.id().fromAlias("e")), DQLExpressions.property(PriorityProfileEduOu.requestedEnrollmentDirection().id().fromAlias("pri"))));

        builder.joinPath(DQLJoinType.inner, PriorityProfileEduOu.profileEducationOrgUnit().educationOrgUnit().fromAlias("pri"), "priOu");

        int PRE_STUDENT_ID_IDX = 0;
        int GRADUATED_PROFILE_EDU_INSTITUTION_IDX = 1;
        int ORIGINAL_DOCUMENT_HANDED_IN_IDX = 2;
        int COMPETITION_KIND_IDX = 3;
        int TARGET_ADMISSION_KIND_IDX = 4;
        int FULL_FIO_IDX = 5;
        int MARK3_IDX = 6;
        int MARK4_IDX = 7;
        int MARK5_IDX = 8;
        int PROFILE_MARK_IDX = 9;
        int FINAL_MARK_IDX = 10;
        int PRIORITY_EDU_OU_ID_IDX = 11;
        int PRIORITY_EDU_OU_PROFILE_IDX = 12;
        final int PRIORITY_EDU_OU_PRI_IDX = 13;
        int SHORT_TITLE_IDX = 14;
        int DEVELOP_FORM_SHORT_TITLE_IDX = 15;
        int DEVELOP_CONDITION_SHORT_TITLE_IDX = 16;
        int REQUEST_ENROLLMENT_DIRECTION_ID_IDX = 17;
        int ENTRANT_ID_IDX = 18;

        builder.group(DQLExpressions.property(PreliminaryEnrollmentStudent.id().fromAlias("ps")));
        builder.group(DQLExpressions.property(RequestedEnrollmentDirection.graduatedProfileEduInstitution().fromAlias("e")));
        builder.group(DQLExpressions.property(RequestedEnrollmentDirection.originalDocumentHandedIn().fromAlias("e")));
        builder.group(DQLExpressions.property(CompetitionKind.id().fromAlias("com")));
        builder.group(DQLExpressions.property(TargetAdmissionKind.id().fromAlias("tar")));
        builder.group(DQLExpressions.property(IdentityCard.fullFio().fromAlias("card")));
        builder.group(DQLExpressions.property(PersonEduInstitution.mark3().fromAlias("edu")));
        builder.group(DQLExpressions.property(PersonEduInstitution.mark4().fromAlias("edu")));
        builder.group(DQLExpressions.property(PersonEduInstitution.mark5().fromAlias("edu")));
        builder.group(DQLExpressions.property(ChosenEntranceDiscipline.finalMark().fromAlias("pcd")));
        builder.group(DQLExpressions.property(PriorityProfileEduOu.id().fromAlias("pri")));
        builder.group(DQLExpressions.property(PriorityProfileEduOu.profileEducationOrgUnit().id().fromAlias("pri")));
        builder.group(DQLExpressions.property(PriorityProfileEduOu.priority().fromAlias("pri")));
        builder.group(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().shortTitle().fromAlias("priOu")));
        builder.group(DQLExpressions.property(EducationOrgUnit.developForm().shortTitle().fromAlias("priOu")));
        builder.group(DQLExpressions.property(EducationOrgUnit.developCondition().shortTitle().fromAlias("priOu")));
        builder.group(DQLExpressions.property(RequestedEnrollmentDirection.id().fromAlias("e")));
        builder.group(DQLExpressions.property(EntrantRequest.entrant().id().fromAlias("req")));

        builder.column(DQLExpressions.property(PreliminaryEnrollmentStudent.id().fromAlias("ps")));
        builder.column(DQLExpressions.property(RequestedEnrollmentDirection.graduatedProfileEduInstitution().fromAlias("e")));
        builder.column(DQLExpressions.property(RequestedEnrollmentDirection.originalDocumentHandedIn().fromAlias("e")));
        builder.column(DQLExpressions.property(CompetitionKind.id().fromAlias("com")));
        builder.column(DQLExpressions.property(TargetAdmissionKind.id().fromAlias("tar")));
        builder.column(DQLExpressions.property(IdentityCard.fullFio().fromAlias("card")));
        builder.column(DQLExpressions.property(PersonEduInstitution.mark3().fromAlias("edu")));
        builder.column(DQLExpressions.property(PersonEduInstitution.mark4().fromAlias("edu")));
        builder.column(DQLExpressions.property(PersonEduInstitution.mark5().fromAlias("edu")));
        builder.column(DQLExpressions.property(ChosenEntranceDiscipline.finalMark().fromAlias("pcd")));
        builder.column(DQLFunctions.sum(DQLExpressions.property(ChosenEntranceDiscipline.finalMark().fromAlias("c"))));
        builder.column(DQLExpressions.property(PriorityProfileEduOu.id().fromAlias("pri")));
        builder.column(DQLExpressions.property(PriorityProfileEduOu.profileEducationOrgUnit().id().fromAlias("pri")));
        builder.column(DQLExpressions.property(PriorityProfileEduOu.priority().fromAlias("pri")));
        builder.column(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().shortTitle().fromAlias("priOu")));
        builder.column(DQLExpressions.property(EducationOrgUnit.developForm().shortTitle().fromAlias("priOu")));
        builder.column(DQLExpressions.property(EducationOrgUnit.developCondition().shortTitle().fromAlias("priOu")));
        builder.column(DQLExpressions.property(RequestedEnrollmentDirection.id().fromAlias("e")));
        builder.column(DQLExpressions.property(EntrantRequest.entrant().id().fromAlias("req")));

        List<Object[]> dataRowList = builder.createStatement(getSession()).list();

        TargetAdmissionKind defaultTaKind = (TargetAdmissionKind) getByNaturalId(new TargetAdmissionKindGen.NaturalId("1"));

        Map<Long, List<Object[]>> preRateMap = new HashMap<Long, List<Object[]>>();

        List<Long> entrantIds = new ArrayList<Long>();
        List<Long> directionids = new ArrayList<Long>();

        for (Object[] dataRow : dataRowList) {

            entrantIds.add((Long) dataRow[ENTRANT_ID_IDX]);
            directionids.add((Long) dataRow[REQUEST_ENROLLMENT_DIRECTION_ID_IDX]);

            long preStudentId = ((Long) dataRow[PRE_STUDENT_ID_IDX]).longValue();
            List<Object[]> list = preRateMap.get(Long.valueOf(preStudentId));
            if (list == null)
                preRateMap.put(Long.valueOf(preStudentId), list = new ArrayList<Object[]>());
            list.add(dataRow);
        }

        Map<Long, List<EnrollmentRecommendation>> recommendMap = EcDistributionUtil.getRecommendationMap(entrantIds);
        Map<Long, Integer> enrPriorityMap = EcDistributionUtil.getEnrollmentPriorityMap(entrantIds);
        Map<Long, List<WrapChoseEntranseDiscipline>> edDiscMap = EcDistributionUtil.getChosenDisciplineMap(directionids);
        Map<Long, List<WrapRequestEDInfo>> mapEntrant2RD = EcDistributionUtil.getMapEntrantToRd(directionids);


        List<EcgEntrantRateRowDTORMC> resultList = new ArrayList<EcgEntrantRateRowDTORMC>();

        for (Map.Entry<Long, List<Object[]>> entry : preRateMap.entrySet()) {

            Object[] dataRow = entry.getValue().get(0);

            TargetAdmissionKind taKind = null;
            if (targetAdmission) {
                Long targetAdmissionKindId = (Long) dataRow[TARGET_ADMISSION_KIND_IDX];
                taKind = targetAdmissionKindId == null ? defaultTaKind : (TargetAdmissionKind) taKindMap.get(targetAdmissionKindId);

                if (taKind == null)
                    continue;
            }

            long competitionKindId = ((Long) dataRow[COMPETITION_KIND_IDX]).longValue();
            CompetitionKind competitionKind = coKindMap.get(Long.valueOf(competitionKindId));

            List<IEcgEntrantRateDirectionDTO> directionList = new ArrayList<IEcgEntrantRateDirectionDTO>();

            Collections.sort(entry.getValue(), new Comparator<Object[]>() {
                @Override
                public int compare(Object[] o1, Object[] o2) {
                    return ((Integer) o1[PRIORITY_EDU_OU_PRI_IDX]).compareTo((Integer) o2[PRIORITY_EDU_OU_PRI_IDX]);
                }
            });

            for (Object[] row : entry.getValue()) {
                long priorityEduOuId = ((Long) row[PRIORITY_EDU_OU_ID_IDX]).longValue();
                long profileDirectionId = ((Long) row[PRIORITY_EDU_OU_PROFILE_IDX]).longValue();
                String shortTitle = (String) row[SHORT_TITLE_IDX];
                String developFormShortTitle = (String) row[DEVELOP_FORM_SHORT_TITLE_IDX];
                String developConditionShortTitle = (String) row[DEVELOP_CONDITION_SHORT_TITLE_IDX];

                List<String> list = new ArrayList<String>();
                if (StringUtils.isNotEmpty(developFormShortTitle))
                    list.add(developFormShortTitle);
                if (StringUtils.isNotEmpty(developConditionShortTitle)) {
                    list.add(developConditionShortTitle);
                }
                directionList.add(new EcgEntrantRateDirectionDTO(Long.valueOf(priorityEduOuId), Long.valueOf(profileDirectionId), new StringBuilder().append(shortTitle).append(" (").append(StringUtils.join(list, ", ")).append(")").toString()));
            }

            String fio = (String) dataRow[FULL_FIO_IDX];
            boolean originalDocumentHandedIn = ((Boolean) dataRow[ORIGINAL_DOCUMENT_HANDED_IN_IDX]).booleanValue();
            Double profileMark = (Double) dataRow[PROFILE_MARK_IDX];
            Double finalMark = (Double) dataRow[FINAL_MARK_IDX];
            boolean graduatedProfileEduInstitution = ((Boolean) dataRow[GRADUATED_PROFILE_EDU_INSTITUTION_IDX]).booleanValue();

            Integer m3 = (Integer) dataRow[MARK3_IDX];
            Integer m4 = (Integer) dataRow[MARK4_IDX];
            Integer m5 = (Integer) dataRow[MARK5_IDX];
            m3 = Integer.valueOf(m3 != null ? m3.intValue() : 0);
            m4 = Integer.valueOf(m4 != null ? m4.intValue() : 0);
            m5 = Integer.valueOf(m5 != null ? m5.intValue() : 0);
            int sum = m3.intValue() + m4.intValue() + m5.intValue();
            double total = m3.intValue() * 3 + m4.intValue() * 4 + m5.intValue() * 5;
            Double averageMark = sum != 0 ? Double.valueOf(total / sum) : null;


            Long entrantId = (Long) entry.getKey();
            List<WrapRequestEDInfo> edEntrant = mapEntrant2RD.get(entrantId);


            resultList.add(new EcgEntrantRateRowDTORMC((Long) entry.getKey(), fio, finalMark == null ? 0.0D : finalMark.doubleValue(), profileMark, averageMark, originalDocumentHandedIn, graduatedProfileEduInstitution, targetAdmission, taKind, competitionKind, directionList, recommendMap.get(entry.getKey()), edDiscMap.get((Long) dataRow[REQUEST_ENROLLMENT_DIRECTION_ID_IDX]), haveSpecilaRights, edEntrant, enrPriorityMap.get(entry.getKey())));

        }
        Collections.sort(resultList, ((IEcDistributionExtDao) EcDistributionManager.instance().dao()).getComparator(distribution.getConfig().getEcgItem()));

        return resultList;
    }

    @Override
    public List<IEcgpRecommendedDTO> getEntrantRecommendedRowList(
            EcgpDistribution distribution)
    {

        EnrollmentCampaign enrollmentCampaign = distribution.getConfig().getEcgItem().getEnrollmentCampaign();

        if (!EcDistributionUtil.isUsePriority(enrollmentCampaign))
            return super.getEntrantRecommendedRowList(distribution);


        List<EcgpEntrantRecommended> entrantRecommendedList = getList(EcgpEntrantRecommended.class, EcgpEntrantRecommended.distribution(), distribution, new String[0]);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PreliminaryEnrollmentStudentExt.class, "ext")
                .column("ext")
                .column("er")
                .joinEntity("ext", DQLJoinType.inner, EcgpEntrantRecommended.class, "er",
                            DQLExpressions.eq(
                                    DQLExpressions.property(PreliminaryEnrollmentStudentExt.preliminaryEnrollmentStudent().id().fromAlias("ext")),
                                    DQLExpressions.property(EcgpEntrantRecommended.preStudent().id().fromAlias("er")))
                )
                .where(DQLExpressions.eqValue(DQLExpressions.property(EcgpEntrantRecommended.distribution().fromAlias("er")), distribution));

        List<Object[]> rows = builder.createStatement(getSession()).list();

        Map<EcgpEntrantRecommended, Boolean> haveSRMap = new HashMap<EcgpEntrantRecommended, Boolean>();

        for (Object[] dataRows : rows) {
            PreliminaryEnrollmentStudentExt studentExt = (PreliminaryEnrollmentStudentExt) dataRows[0];
            EcgpEntrantRecommended recommended = (EcgpEntrantRecommended) dataRows[1];

            haveSRMap.put(recommended, studentExt != null ? studentExt.isHaveSpecialRights() : null);
        }

        List<EcgpEntrantRecommended> activeList = new ArrayList<EcgpEntrantRecommended>();
        List<EcgpEntrantRecommended> nonActiveList = new ArrayList<EcgpEntrantRecommended>();
        for (EcgpEntrantRecommended recommended : entrantRecommendedList) {
            (isConfigAllowDirection(distribution.getConfig(), recommended.getPreStudent()) ? activeList : nonActiveList).add(recommended);
        }

        List<Long> directionIds = new ArrayList<Long>();
        List<Long> entrantIds = new ArrayList<Long>();
        for (EcgpEntrantRecommended recommended : activeList) {
            entrantIds.add(recommended.getPreStudent().getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getId());
            directionIds.add(recommended.getPreStudent().getRequestedEnrollmentDirection().getId());
        }

        int DIRECTION_ID = 0;
        int SHORT_TITLE_IX = 1;
        int DEVELOP_FORM_SHORT_TITLE_IX = 2;
        int DEVELOP_CONDITION_SHORT_TITLE_IX = 3;
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(PriorityProfileEduOu.class, "e")
                .column(DQLExpressions.property(PriorityProfileEduOu.requestedEnrollmentDirection().id().fromAlias("e")))
                .column(DQLExpressions.property(PriorityProfileEduOu.profileEducationOrgUnit().educationOrgUnit().educationLevelHighSchool().shortTitle().fromAlias("e")))
                .column(DQLExpressions.property(PriorityProfileEduOu.profileEducationOrgUnit().educationOrgUnit().developForm().shortTitle().fromAlias("e")))
                .column(DQLExpressions.property(PriorityProfileEduOu.profileEducationOrgUnit().educationOrgUnit().developCondition().shortTitle().fromAlias("e")))
                .where(DQLExpressions.in(DQLExpressions.property(PriorityProfileEduOu.requestedEnrollmentDirection().id().fromAlias("e")), directionIds))
                .order(DQLExpressions.property(PriorityProfileEduOu.priority().fromAlias("e")));

        Map<Long, List<String>> prioritiesMap = new Hashtable<Long, List<String>>();
        Map<Long, List<String>> prioritiesShortMap = new Hashtable<Long, List<String>>();

        rows = dql.createStatement(getSession()).list();

        for (Object[] dataRow : rows) {
            Long entrantId = (Long) dataRow[DIRECTION_ID];
            String shortTitle = (String) dataRow[SHORT_TITLE_IX];
            String developFormShortTitle = (String) dataRow[DEVELOP_FORM_SHORT_TITLE_IX];
            String developConditionShortTitle = (String) dataRow[DEVELOP_CONDITION_SHORT_TITLE_IX];

            List<String> list = new ArrayList<String>();
            if (StringUtils.isNotEmpty(developFormShortTitle))
                list.add(developFormShortTitle);
            if (StringUtils.isNotEmpty(developConditionShortTitle)) {
                list.add(developConditionShortTitle);
            }
            List<String> set = prioritiesMap.get(entrantId);
            if (set == null)
                prioritiesMap.put(entrantId, set = new ArrayList<String>());
            set.add(new StringBuilder().append(shortTitle).append(" (").append(StringUtils.join(list, ", ")).append(")").toString());

            List<String> shortSet = prioritiesShortMap.get(entrantId);
            if (shortSet == null)
                prioritiesShortMap.put(entrantId, shortSet = new ArrayList<String>());
            shortSet.add(shortTitle);
        }

        Map<Long, Double> finalMarkMap = new Hashtable<Long, Double>();

        int REQUESTED_ENROLLMENT_DIRECTION_ID_IX = 0;
        int FINAL_MARK_IX = 1;

        rows = new DQLSelectBuilder().fromEntity(ChosenEntranceDiscipline.class, "e")
                .column(DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("e")))
                .column(DQLFunctions.sum(DQLExpressions.property(ChosenEntranceDiscipline.finalMark().fromAlias("e"))))
                .where(DQLExpressions.in(DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("e")), directionIds))
                .where(DQLExpressions.isNotNull(ChosenEntranceDiscipline.finalMark().fromAlias("e")))
                .group(DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("e")))
                .createStatement(getSession()).list();

        for (Object[] dataRow : rows) {
            Long requestedEnrollmentDirectionId = (Long) dataRow[REQUESTED_ENROLLMENT_DIRECTION_ID_IX];
            Double finalMark = (Double) dataRow[FINAL_MARK_IX];
            finalMarkMap.put(requestedEnrollmentDirectionId, finalMark);
        }


        Map<Long, List<EnrollmentRecommendation>> recommendMap = EcDistributionUtil.getRecommendationMap(entrantIds);
        Map<Long, Integer> enrPriorityMap = EcDistributionUtil.getEnrollmentPriorityMap(entrantIds);
        Map<Long, List<WrapChoseEntranseDiscipline>> edDiscMap = EcDistributionUtil.getChosenDisciplineMap(directionIds);

        List<IEcgpRecommendedDTO> rateList = new ArrayList<IEcgpRecommendedDTO>();

        for (EcgpEntrantRecommended recommended : activeList) {
            List<String> priorityList = prioritiesMap.get(recommended.getPreStudent().getRequestedEnrollmentDirection().getId());
            String priorities = priorityList == null ? null : StringUtils.join(priorityList, ", ");
            List<String> shortPriorityList = prioritiesShortMap.get(recommended.getPreStudent().getRequestedEnrollmentDirection().getId());
            String shortPriorities = shortPriorityList == null ? null : StringUtils.join(shortPriorityList, ", ");
            Double finalMark = finalMarkMap.get(recommended.getPreStudent().getRequestedEnrollmentDirection().getId());
            rateList.add(new EcgpRecommendedDTORMC(recommended, finalMark, priorities, shortPriorities, recommendMap.get(recommended.getPreStudent().getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getId()), edDiscMap.get(recommended.getPreStudent().getRequestedEnrollmentDirection().getId()), haveSRMap.get(recommended), enrPriorityMap.get(recommended.getPreStudent().getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getId())));
        }

        Collections.sort(rateList, ((IEcDistributionExtDao) EcDistributionManager.instance().dao()).getComparator(distribution.getConfig().getEcgItem()));

        Collections.sort(nonActiveList, new Comparator<EcgpEntrantRecommended>() {
            @Override
            public int compare(EcgpEntrantRecommended o1, EcgpEntrantRecommended o2) {
                String fio1 = o1.getPreStudent().getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getPerson().getIdentityCard().getFullFio();
                String fio2 = o2.getPreStudent().getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getPerson().getIdentityCard().getFullFio();
                return fio1.compareTo(fio2);
            }
        });

        for (EcgpEntrantRecommended recommended : nonActiveList) {
            rateList.add(new EcgpRecommendedDTORMC(recommended, null, null, null, null, null, haveSRMap.get(recommended), null));
        }
        return rateList;
    }

    @Override
    public void doDistributionFill(EcgpDistribution distribution) {

        EnrollmentCampaign enrollmentCampaign = distribution.getConfig().getEcgItem().getEnrollmentCampaign();

        if (!EcDistributionUtil.isUsePriority(enrollmentCampaign)) {
            super.doDistributionFill(distribution);
            return;
        }

        Session session = getSession();

        session.refresh(distribution);

        IEcgpQuotaFreeDTO freeQuotaDTO = getFreeQuotaDTO(distribution);

        List<TargetAdmissionKind> targetAdmissionKindList = new DQLSelectBuilder().fromEntity(TargetAdmissionKind.class, "e")
                .where(DQLExpressions.notIn(DQLExpressions.property(TargetAdmissionKind.id().fromAlias("e")),
                                            new DQLSelectBuilder().fromEntity(NotUsedTargetAdmissionKind.class, "nu")
                                                    .column(DQLExpressions.property(NotUsedTargetAdmissionKind.targetAdmissionKind().id().fromAlias("nu")))
                                                    .where(DQLExpressions.eq(DQLExpressions.property(NotUsedTargetAdmissionKind.enrollmentCampaign().fromAlias("nu")), DQLExpressions.value(distribution.getConfig().getEcgItem().getEnrollmentCampaign())))
                                                    .buildQuery()))
                .createStatement(session).list();

        Map<CompetitionKind, Integer> competitionKindPriorityMap = UniecDAOFacade.getSettingsDAO().getCompetitionKindPriorities(distribution.getConfig().getEcgItem().getEnrollmentCampaign());

        IEcgQuotaManager quotaManager = new EcgpQuotaManager(freeQuotaDTO);

        List<Long> directionList = new ArrayList<Long>();

        Set<Long> usedEntrantIds = new HashSet<Long>();

        EnumTools.addEnum(IEcgEntrantRateRowDTO.Rule.class, EcProfileDistributionPubUI.RULE_WITHOUT_ENTRANCE_DISCIPLINES);

        EcDistributionUtil.invokePrivateMethod(this, EcProfileDistributionDao.class, "accumulateRateRow", new Object[]{distribution, targetAdmissionKindList, competitionKindPriorityMap, usedEntrantIds, quotaManager, IEcgEntrantRateRowDTO.Rule.RULE_TARGET_ADMISSION, directionList});

        EcDistributionUtil.invokePrivateMethod(this, EcProfileDistributionDao.class, "accumulateRateRow", new Object[]{distribution, targetAdmissionKindList, competitionKindPriorityMap, usedEntrantIds, quotaManager, IEcgEntrantRateRowDTO.Rule.valueOf(EcProfileDistributionPubUI.RULE_WITHOUT_ENTRANCE_DISCIPLINES), directionList});

        EcDistributionUtil.invokePrivateMethod(this, EcProfileDistributionDao.class, "accumulateRateRow", new Object[]{distribution, targetAdmissionKindList, competitionKindPriorityMap, usedEntrantIds, quotaManager, IEcgEntrantRateRowDTO.Rule.RULE_NON_COMPETITIVE, directionList});

        EcDistributionUtil.invokePrivateMethod(this, EcProfileDistributionDao.class, "accumulateRateRow", new Object[]{distribution, targetAdmissionKindList, competitionKindPriorityMap, usedEntrantIds, quotaManager, IEcgEntrantRateRowDTO.Rule.RULE_COMPETITIVE, directionList});

        saveEntrantRecommendedList(distribution, directionList);

    }
}
