package ru.tandemservice.uniecrmc.online.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;
import ru.tandemservice.uniecrmc.entity.catalog.IndividualAchievements;
import ru.tandemservice.uniecrmc.online.entity.OnlineEntrant2IndividualAchievement;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Индивидуальные достижения онлайн-абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OnlineEntrant2IndividualAchievementGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniecrmc.online.entity.OnlineEntrant2IndividualAchievement";
    public static final String ENTITY_NAME = "onlineEntrant2IndividualAchievement";
    public static final int VERSION_HASH = -1661343114;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT = "entrant";
    public static final String L_INDIVIDUAL_ACHIEVEMENTS = "individualAchievements";
    public static final String P_TEXT = "text";
    public static final String P_ORDER = "order";

    private OnlineEntrant _entrant;     // Онлайн-абитуриент
    private IndividualAchievements _individualAchievements;     // Вид индивидуального достижения
    private String _text;     // Описание
    private int _order;     // Приоритет

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Онлайн-абитуриент. Свойство не может быть null.
     */
    @NotNull
    public OnlineEntrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Онлайн-абитуриент. Свойство не может быть null.
     */
    public void setEntrant(OnlineEntrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Вид индивидуального достижения.
     */
    public IndividualAchievements getIndividualAchievements()
    {
        return _individualAchievements;
    }

    /**
     * @param individualAchievements Вид индивидуального достижения.
     */
    public void setIndividualAchievements(IndividualAchievements individualAchievements)
    {
        dirty(_individualAchievements, individualAchievements);
        _individualAchievements = individualAchievements;
    }

    /**
     * @return Описание.
     */
    public String getText()
    {
        return _text;
    }

    /**
     * @param text Описание.
     */
    public void setText(String text)
    {
        dirty(_text, text);
        _text = text;
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     */
    @NotNull
    public int getOrder()
    {
        return _order;
    }

    /**
     * @param order Приоритет. Свойство не может быть null.
     */
    public void setOrder(int order)
    {
        dirty(_order, order);
        _order = order;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OnlineEntrant2IndividualAchievementGen)
        {
            setEntrant(((OnlineEntrant2IndividualAchievement)another).getEntrant());
            setIndividualAchievements(((OnlineEntrant2IndividualAchievement)another).getIndividualAchievements());
            setText(((OnlineEntrant2IndividualAchievement)another).getText());
            setOrder(((OnlineEntrant2IndividualAchievement)another).getOrder());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OnlineEntrant2IndividualAchievementGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OnlineEntrant2IndividualAchievement.class;
        }

        public T newInstance()
        {
            return (T) new OnlineEntrant2IndividualAchievement();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrant":
                    return obj.getEntrant();
                case "individualAchievements":
                    return obj.getIndividualAchievements();
                case "text":
                    return obj.getText();
                case "order":
                    return obj.getOrder();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrant":
                    obj.setEntrant((OnlineEntrant) value);
                    return;
                case "individualAchievements":
                    obj.setIndividualAchievements((IndividualAchievements) value);
                    return;
                case "text":
                    obj.setText((String) value);
                    return;
                case "order":
                    obj.setOrder((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrant":
                        return true;
                case "individualAchievements":
                        return true;
                case "text":
                        return true;
                case "order":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrant":
                    return true;
                case "individualAchievements":
                    return true;
                case "text":
                    return true;
                case "order":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrant":
                    return OnlineEntrant.class;
                case "individualAchievements":
                    return IndividualAchievements.class;
                case "text":
                    return String.class;
                case "order":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OnlineEntrant2IndividualAchievement> _dslPath = new Path<OnlineEntrant2IndividualAchievement>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OnlineEntrant2IndividualAchievement");
    }
            

    /**
     * @return Онлайн-абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.online.entity.OnlineEntrant2IndividualAchievement#getEntrant()
     */
    public static OnlineEntrant.Path<OnlineEntrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Вид индивидуального достижения.
     * @see ru.tandemservice.uniecrmc.online.entity.OnlineEntrant2IndividualAchievement#getIndividualAchievements()
     */
    public static IndividualAchievements.Path<IndividualAchievements> individualAchievements()
    {
        return _dslPath.individualAchievements();
    }

    /**
     * @return Описание.
     * @see ru.tandemservice.uniecrmc.online.entity.OnlineEntrant2IndividualAchievement#getText()
     */
    public static PropertyPath<String> text()
    {
        return _dslPath.text();
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.online.entity.OnlineEntrant2IndividualAchievement#getOrder()
     */
    public static PropertyPath<Integer> order()
    {
        return _dslPath.order();
    }

    public static class Path<E extends OnlineEntrant2IndividualAchievement> extends EntityPath<E>
    {
        private OnlineEntrant.Path<OnlineEntrant> _entrant;
        private IndividualAchievements.Path<IndividualAchievements> _individualAchievements;
        private PropertyPath<String> _text;
        private PropertyPath<Integer> _order;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Онлайн-абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.online.entity.OnlineEntrant2IndividualAchievement#getEntrant()
     */
        public OnlineEntrant.Path<OnlineEntrant> entrant()
        {
            if(_entrant == null )
                _entrant = new OnlineEntrant.Path<OnlineEntrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Вид индивидуального достижения.
     * @see ru.tandemservice.uniecrmc.online.entity.OnlineEntrant2IndividualAchievement#getIndividualAchievements()
     */
        public IndividualAchievements.Path<IndividualAchievements> individualAchievements()
        {
            if(_individualAchievements == null )
                _individualAchievements = new IndividualAchievements.Path<IndividualAchievements>(L_INDIVIDUAL_ACHIEVEMENTS, this);
            return _individualAchievements;
        }

    /**
     * @return Описание.
     * @see ru.tandemservice.uniecrmc.online.entity.OnlineEntrant2IndividualAchievement#getText()
     */
        public PropertyPath<String> text()
        {
            if(_text == null )
                _text = new PropertyPath<String>(OnlineEntrant2IndividualAchievementGen.P_TEXT, this);
            return _text;
        }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.online.entity.OnlineEntrant2IndividualAchievement#getOrder()
     */
        public PropertyPath<Integer> order()
        {
            if(_order == null )
                _order = new PropertyPath<Integer>(OnlineEntrant2IndividualAchievementGen.P_ORDER, this);
            return _order;
        }

        public Class getEntityClass()
        {
            return OnlineEntrant2IndividualAchievement.class;
        }

        public String getEntityName()
        {
            return "onlineEntrant2IndividualAchievement";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
