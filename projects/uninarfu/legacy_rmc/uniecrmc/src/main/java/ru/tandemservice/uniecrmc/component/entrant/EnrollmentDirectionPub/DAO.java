package ru.tandemservice.uniecrmc.component.entrant.EnrollmentDirectionPub;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.EcDistributionUtil;
import ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionExt;
import ru.tandemservice.uniecrmc.entity.entrant.EntranceDisciplineExt;
import ru.tandemservice.uniecrmc.entity.entrant.EntrantRecommendedSettings;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistributionQuota;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;

import java.util.List;

public class DAO extends ru.tandemservice.uniec.component.entrant.EnrollmentDirectionPub.DAO {

    @Override
    public void prepare(ru.tandemservice.uniec.component.entrant.EnrollmentDirectionPub.Model model) {
        super.prepare(model);

        Model myModel = (Model) model;

        if (model.getEnrollmentDirection() != null) {
            EnrollmentDirectionExt enrollmentDirectionExt = get(EnrollmentDirectionExt.class, EnrollmentDirectionExt.enrollmentDirection().id(), model.getEnrollmentDirection().getId());

            if (enrollmentDirectionExt == null) {
                enrollmentDirectionExt = new EnrollmentDirectionExt();
                enrollmentDirectionExt.setEnrollmentDirection(model.getEnrollmentDirection());
            }

            myModel.setEnrollmentDirectionExt(enrollmentDirectionExt);
        }
        //наличие волны распределения по данному направлению подготовки
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EcgDistributionQuota.class, "edq")
                .where(DQLExpressions.eqValue(DQLExpressions.property(EcgDistributionQuota.direction().id().fromAlias("edq")), model.getEnrollmentDirection().getId()));
        EntrantRecommendedSettings settings = get(EntrantRecommendedSettings.class, EntrantRecommendedSettings.enrollmentCampaign(), model.getEnrollmentDirection().getEnrollmentCampaign());

        myModel.setVisible(settings != null && settings.isUsePriorityEntranceDiscipline() && getCount(builder) == 0);
    }

    @Override
    public void prepareListDataSource(
            ru.tandemservice.uniec.component.entrant.EnrollmentDirectionPub.Model model)
    {
        super.prepareListDataSource(model);

        if (!EcDistributionUtil.isUsePriority(model.getEnrollmentDirection().getEnrollmentCampaign()))
            return;

        DynamicListDataSource<EntranceDiscipline> dataSource;
        if (!model.getEnrollmentDirection().getEnrollmentCampaign().isExamSetDiff()) {
            model.setStudentCategory((StudentCategory) getCatalogItem(StudentCategory.class, "1"));
            dataSource = model.getDataSource();
        }
        else {
            dataSource = (DynamicListDataSource<EntranceDiscipline>) model.getDataSourceMap().get(model.getStudentCategory());
        }

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EntranceDisciplineExt.class, "ext")
                .where(DQLExpressions.in(DQLExpressions.property("ext", EntranceDisciplineExt.entranceDiscipline()), dataSource.getEntityList()))
                .order(DQLExpressions.property(EntranceDisciplineExt.priority().fromAlias("ext")))
                .column(DQLExpressions.property(EntranceDisciplineExt.entranceDiscipline().fromAlias("ext")));

        List<EntranceDiscipline> result = getList(builder);

        UniBaseUtils.createPage(dataSource, result);

    }
}
