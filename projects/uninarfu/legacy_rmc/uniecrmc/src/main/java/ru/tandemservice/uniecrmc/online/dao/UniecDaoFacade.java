package ru.tandemservice.uniecrmc.online.dao;

import org.tandemframework.core.runtime.ApplicationRuntime;

public class UniecDaoFacade extends ru.tandemservice.uniec.dao.UniecDAOFacade {

    private static IOnlineEntrantRegistrationDAO onlineEntrantRegistrationRmcDAO;

    public static IOnlineEntrantRegistrationDAO getOnlineEntrantRegistrationRmcDAO() {
        if (onlineEntrantRegistrationRmcDAO == null)
            onlineEntrantRegistrationRmcDAO = (IOnlineEntrantRegistrationDAO) ApplicationRuntime.getBean(IOnlineEntrantRegistrationDAO.class.getName());
        return onlineEntrantRegistrationRmcDAO;
    }

}
