package ru.tandemservice.uniecrmc.dao.entrantRating;

import ru.tandemservice.uni.entity.catalog.DevelopForm;

import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

public class DevelopFormNode {

    @XmlElement(name = "DevelopForm")
    public List<RowNode> list = new ArrayList<>();

    public DevelopFormNode() {
    }

    public void add(DevelopForm entity) {
        RowNode node = new RowNode(entity.getCode(), entity.getTitle(), entity.getShortTitle());
        list.add(node);
    }
}
