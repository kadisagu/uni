package ru.tandemservice.uniecrmc.dao.entrantRating;

import ru.tandemservice.uni.entity.catalog.EducationLevels;

import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

public class EduLevelNode {

    @XmlElement(name = "EduLevelId")
    public List<RowNode> list = new ArrayList<>();

    public EduLevelNode() {
    }

    public void add(EducationLevels entity) {
//        String code = ASPIRANT;
//        if(entity instanceof EducationLevelMiddleGos3)
//            code=MIDDLE;
//        if(entity instanceof EducationLevelHighGos3)
//            code=HIGH;


        RowNode node = new RowNode(null, null, null);
        list.add(node);
    }
}
