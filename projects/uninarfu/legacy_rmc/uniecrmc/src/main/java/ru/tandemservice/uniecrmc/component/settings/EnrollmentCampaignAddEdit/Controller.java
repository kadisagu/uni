package ru.tandemservice.uniecrmc.component.settings.EnrollmentCampaignAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionRestrictionRMC;

public class Controller extends ru.tandemservice.uniec.component.settings.EnrollmentCampaignAddEdit.Controller {

    public void onClickChangeRestrictionType(IBusinessComponent component) {
        ((IDAO) getDao()).prepare(getModel(component));
    }

    public void onClickAddRestriction(IBusinessComponent component) {
        ((Model) getModel(component)).getRestrictionList().add(new EnrollmentDirectionRestrictionRMC());
    }

    public void onClickDeleteRestriction(IBusinessComponent component)
    {
        ((Model) getModel(component)).getRestrictionList().remove(((Integer) component.getListenerParameter()).intValue() - 1);
    }
}
