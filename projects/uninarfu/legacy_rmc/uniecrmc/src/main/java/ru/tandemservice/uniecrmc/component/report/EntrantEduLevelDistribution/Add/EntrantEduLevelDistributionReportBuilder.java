package ru.tandemservice.uniecrmc.component.report.EntrantEduLevelDistribution.Add;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import jxl.write.Number;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Session;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class EntrantEduLevelDistributionReportBuilder {
    private Session _session;
    private Model _model;
    public final String TEMPLATE = "entrantEduLevelDistributionReport";
    private int rowIndex;

    private static WritableCellFormat rightBorderCellFormat;
    private static WritableCellFormat rightBorderCellFormatLeftAlign;
    private static WritableCellFormat rightBorderCellFormatRightAlign;
    private static WritableCellFormat noneBorderFormat;
    private static WritableCellFormat bottomRightBorderCellFormat;
    private static WritableCellFormat bottomRightBorderCellFormatLeftAlign;
    private static WritableCellFormat bottomNoneBorderFormat;
    private static WritableFont font;
    private static WritableFont boldFont;


    EntrantEduLevelDistributionReportBuilder(Model model, Session session) {
        this._model = model;
        this._session = session;
        this.rowIndex = 6;
    }


    DatabaseFile getContent() throws Exception {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private byte[] buildReport() throws Exception {
        UniecScriptItem template = IUniBaseDao.instance.get().getCatalogItem(UniecScriptItem.class, TEMPLATE);

        ByteArrayInputStream in = new ByteArrayInputStream(template.getTemplate());
        Workbook tempalteWorkbook = Workbook.getWorkbook(in);

        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        //  ws.setNamesDisabled(true);
        ws.setGCDisabled(true);
        initFormat();


        ByteArrayOutputStream workbookStream = new ByteArrayOutputStream();
        WritableWorkbook workbook = Workbook.createWorkbook(workbookStream, tempalteWorkbook, ws);

        List<RequestedEnrollmentDirection> requestedEnrollmentDirections = getRequestedEnrollmentDirections();
        List<PreliminaryEnrollmentStudent> preliminaryEnrollmentStudents = getPreliminaryEnrollmentStudents();
        ReportBlock bach = new ReportBlock(true, false, false, 1, _model.getDevelopConditionList());
        ReportBlock spec = new ReportBlock(false, true, false, bach.getLastNumber() + 1, _model.getDevelopConditionList());
        ReportBlock mast = new ReportBlock(false, false, true, spec.getLastNumber() + 1, _model.getDevelopConditionList());
        ReportDetailedBlock spoProf = new ReportDetailedBlock("Профессии СПО  - всего", "профессиям", mast.getLastNumber() + 1);
        ReportDetailedBlock spoSpec = new ReportDetailedBlock("Специальности  СПО  - всего", "специальностям", mast.getLastNumber() + 2);
        ReportBlock tempBlock = null;
        for (RequestedEnrollmentDirection direction : requestedEnrollmentDirections) {
            EducationLevelsHighSchool highSchool = direction.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool();
            DevelopCondition developCondition = direction.getEnrollmentDirection().getEducationOrgUnit().getDevelopCondition();
            DevelopTech tech = direction.getEnrollmentDirection().getEducationOrgUnit().getDevelopTech();
            CompensationType compensationType = direction.getCompensationType();
            StructureEducationLevels level = direction.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType();
            if (highSchool.getEducationLevel().getEduProgramSubject().getSubjectIndex().getProgramKind().isProgramSecondaryProf()) {
                if (highSchool.getEducationLevel().getEduProgramSubject().getSubjectIndex().getProgramKind().getCode().equals("2013.2.1.1"))
                    spoProf.increaseTotal(highSchool);
                if (highSchool.getEducationLevel().getEduProgramSubject().getSubjectIndex().getProgramKind().getCode().equals("2013.2.1.2"))
                    spoSpec.increaseTotal(highSchool);


            }
            else {
                if (level.isBachelor())
                    tempBlock = bach;
                if (level.isSpecialty())
                    tempBlock = spec;
                if (level.isMaster())
                    tempBlock = mast;
                if (tempBlock != null)
                    tempBlock.increase(highSchool, developCondition, tech, compensationType, false);
            }
        }

        for (PreliminaryEnrollmentStudent student : preliminaryEnrollmentStudents) {
            EducationLevelsHighSchool highSchool = student.getRequestedEnrollmentDirection().getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool();
            DevelopCondition developCondition = student.getRequestedEnrollmentDirection().getEnrollmentDirection().getEducationOrgUnit().getDevelopCondition();
            DevelopTech tech = student.getRequestedEnrollmentDirection().getEnrollmentDirection().getEducationOrgUnit().getDevelopTech();
            CompensationType compensationType = student.getCompensationType();
            StructureEducationLevels level = student.getRequestedEnrollmentDirection().getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType();
            if (highSchool.getEducationLevel().getEduProgramSubject().getSubjectIndex().getProgramKind().isProgramSecondaryProf()) {
                if (highSchool.getEducationLevel().getEduProgramSubject().getSubjectIndex().getProgramKind().getCode().equals("2013.2.1.1"))
                    spoProf.increaseEnrolled(highSchool, compensationType);
                if (highSchool.getEducationLevel().getEduProgramSubject().getSubjectIndex().getProgramKind().getCode().equals("2013.2.1.2"))
                    spoSpec.increaseEnrolled(highSchool, compensationType);


            }
            else {
                if (level.isBachelor())
                    tempBlock = bach;
                if (level.isSpecialty())
                    tempBlock = spec;
                if (level.isMaster())
                    tempBlock = mast;
                if (tempBlock != null)
                    tempBlock.increase(highSchool, developCondition, tech, compensationType, true);
            }
        }
        WritableSheet sheet = workbook.getSheet(0);
        //Рисуем впо
        drawBlock(sheet, bach, false);
        drawBlock(sheet, spec, false);
        drawBlock(sheet, mast, true);
        //Строчка с общей суммой
        ReportRow total = countTotalRow(bach, spec, mast);
        drawLastRow(sheet, total, true, Alignment.LEFT);
        //Заголовок для СПО
        copyHeader(sheet, 0, rowIndex + 3);
        //Рисуем данные СПО
        drawDetailedBlock(sheet, spoProf, false);
        drawDetailedBlock(sheet, spoSpec, true);
        //Строка с суммой по СПО
        total = countTotalRow(spoProf, spoSpec);
        drawLastRow(sheet, total, true, Alignment.LEFT);

        workbook.write();
        workbook.close();
        return workbookStream.toByteArray();
    }


    private List<RequestedEnrollmentDirection> getRequestedEnrollmentDirections() {
        DQLSelectBuilder dqlSelectBuilder = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "red");
        dqlSelectBuilder.where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().enrollmentCampaign().fromAlias("red")), DQLExpressions.value(_model.getReport().getEnrollmentCampaign())));
        if (!CollectionUtils.isEmpty(_model.getDevelopFormList()))
            dqlSelectBuilder.where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developForm().fromAlias("red")), _model.getDevelopFormList()));
        if (!CollectionUtils.isEmpty(_model.getEducationLevelHighSchoolList()))
            dqlSelectBuilder.where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().fromAlias("red")), _model.getEducationLevelHighSchoolList()));
        if (!CollectionUtils.isEmpty(_model.getFormativeOrgUnitList()))
            dqlSelectBuilder.where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().formativeOrgUnit().fromAlias("red")), _model.getFormativeOrgUnitList()));
        return dqlSelectBuilder.createStatement(_session).list();
    }

    private List<PreliminaryEnrollmentStudent> getPreliminaryEnrollmentStudents() {
        DQLSelectBuilder dqlSelectBuilder = new DQLSelectBuilder().fromEntity(PreliminaryEnrollmentStudent.class, "red");
        dqlSelectBuilder.where(DQLExpressions.eq(DQLExpressions.property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().enrollmentDirection().enrollmentCampaign().fromAlias("red")), DQLExpressions.value(_model.getReport().getEnrollmentCampaign())));
        if (!CollectionUtils.isEmpty(_model.getDevelopFormList()))
            dqlSelectBuilder.where(DQLExpressions.in(DQLExpressions.property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().enrollmentDirection().educationOrgUnit().developForm().fromAlias("red")), _model.getDevelopFormList()));
        if (!CollectionUtils.isEmpty(_model.getEducationLevelHighSchoolList()))
            dqlSelectBuilder.where(DQLExpressions.in(DQLExpressions.property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().enrollmentDirection().educationOrgUnit().educationLevelHighSchool().fromAlias("red")), _model.getEducationLevelHighSchoolList()));
        if (!CollectionUtils.isEmpty(_model.getFormativeOrgUnitList()))
            dqlSelectBuilder.where(DQLExpressions.in(DQLExpressions.property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().enrollmentDirection().educationOrgUnit().formativeOrgUnit().fromAlias("red")), _model.getFormativeOrgUnitList()));
        return dqlSelectBuilder.createStatement(_session).list();
    }


    private void drawBlock(WritableSheet sheet, ReportBlock block, boolean lastBlockInReport) throws Exception {

        drawDetailedBlock(sheet, block.getHeaderBlock(), false);
        drawDetailedBlock(sheet, block.getIndividualBlock(), false);
        drawDetailedBlock(sheet, block.getNetDevelopBlock(), false);
        drawDetailedBlock(sheet, block.getDistanceDevelopFormBlock(), lastBlockInReport);

    }

    private void drawDetailedBlock(WritableSheet sheet, ReportDetailedBlock detailedBlock, boolean lastBlock) throws Exception {
        List<EducationLevelsHighSchool> educationLevelsHighSchoolList = new ArrayList<>(detailedBlock.getRowMap().keySet());
        Collections.sort(educationLevelsHighSchoolList, new EntityComparator<EducationLevelsHighSchool>(new EntityOrder(EducationLevelsHighSchool.educationLevel().eduProgramSubject().subjectCode().s())));
        drawRow(sheet, detailedBlock.getHeader(), true, Alignment.LEFT);
        ReportRow subheader = new ReportRow();
        subheader.setTitle(detailedBlock.getSubHeader());
        subheader.setNeedNumber(true);
        if (educationLevelsHighSchoolList.isEmpty() && lastBlock)
            drawLastRow(sheet, subheader, false, Alignment.RIGHT);
        else
            drawRow(sheet, subheader, false, Alignment.RIGHT);
        for (EducationLevelsHighSchool educationLevelsHighSchool : educationLevelsHighSchoolList) {

            if (educationLevelsHighSchoolList.indexOf(educationLevelsHighSchool) == educationLevelsHighSchoolList.size() - 1 && lastBlock)    //Последняя запись в блоке - рисуем черту
                drawLastRow(sheet, detailedBlock.getRowMap().get(educationLevelsHighSchool), false, Alignment.LEFT);
            else
                drawRow(sheet, detailedBlock.getRowMap().get(educationLevelsHighSchool));
        }
    }

    private ReportRow countTotalRow(ReportBlock bach, ReportBlock spec, ReportBlock mast) {
        ReportRow retVal = new ReportRow();
        retVal.setTitle("Всего по программам высшего образования (сумма строк 01, 05, 09)");
        retVal.setNumber(String.valueOf(mast.getLastNumber() + 1));
        retVal.setTotalRequests(bach.getHeaderBlock().getHeader().getTotalRequests() + spec.getHeaderBlock().getHeader().getTotalRequests() + mast.getHeaderBlock().getHeader().getTotalRequests());
        retVal.setAcceptedRequests(bach.getHeaderBlock().getHeader().getAcceptedRequests() + spec.getHeaderBlock().getHeader().getAcceptedRequests() + mast.getHeaderBlock().getHeader().getAcceptedRequests());
        retVal.setAcceptedBudgetRequests(bach.getHeaderBlock().getHeader().getAcceptedBudgetRequests() + spec.getHeaderBlock().getHeader().getAcceptedBudgetRequests() + mast.getHeaderBlock().getHeader().getAcceptedBudgetRequests());
        retVal.setAcceptedContractRequests(bach.getHeaderBlock().getHeader().getAcceptedContractRequests() + spec.getHeaderBlock().getHeader().getAcceptedContractRequests() + mast.getHeaderBlock().getHeader().getAcceptedContractRequests());
        return retVal;
    }

    private ReportRow countTotalRow(ReportDetailedBlock prof, ReportDetailedBlock spec) {
        ReportRow retVal = new ReportRow();
        retVal.setTitle("Всего по программам среднего профессионального образования (сумма строк 14, 15)");
        retVal.setNumber(Integer.valueOf(spec.getHeader().getNumber() + 1).toString());
        retVal.setTotalRequests(prof.getHeader().getTotalRequests() + spec.getHeader().getTotalRequests());
        retVal.setAcceptedRequests(prof.getHeader().getAcceptedRequests() + spec.getHeader().getAcceptedRequests());
        retVal.setAcceptedBudgetRequests(prof.getHeader().getAcceptedBudgetRequests() + spec.getHeader().getAcceptedBudgetRequests());
        retVal.setAcceptedContractRequests(prof.getHeader().getAcceptedContractRequests() + spec.getHeader().getAcceptedContractRequests());
        return retVal;
    }


    /**
     * Рисует строку таблицы и увеличивает указатель
     *
     * @param sheet
     * @param row
     *
     * @throws Exception
     */
    private void drawRow(WritableSheet sheet, ReportRow row) throws Exception {
        setCellString(sheet, 0, row.getTitle(), rightBorderCellFormatLeftAlign);
        setCellString(sheet, 1, row.getNumber(), rightBorderCellFormat);
        setCellString(sheet, 2, row.getCode(), noneBorderFormat);
        drawRowData(sheet, row);
        rowIndex++;
    }

    private void drawRow(WritableSheet sheet, ReportRow row, boolean bold, Alignment alignment) throws Exception {

        WritableCellFormat format = new WritableCellFormat(rightBorderCellFormatRightAlign);
        if (bold)
            format.setFont(boldFont);
        if (alignment != null)
            format.setAlignment(alignment);
        setCellString(sheet, 0, row.getTitle(), format);
        setCellString(sheet, 1, row.getNumber(), rightBorderCellFormat);
        setCellString(sheet, 2, row.getCode(), noneBorderFormat);
        drawRowData(sheet, row);
        rowIndex++;
    }

    private void drawLastRow(WritableSheet sheet, ReportRow row, boolean bold, Alignment alignment) throws Exception {

        //     copyRow(sheet, rowIndex, rowIndex + 1);
        WritableCellFormat format = new WritableCellFormat(bottomRightBorderCellFormatLeftAlign);
        if (bold)
            format.setFont(boldFont);
        if (alignment != null)
            format.setAlignment(alignment);
        setCellString(sheet, 0, row.getTitle(), format);
        setCellString(sheet, 1, row.getNumber(), bottomRightBorderCellFormat);
        setCellString(sheet, 2, row.getCode(), bottomNoneBorderFormat);
        setCellNumber(sheet, 3, row.getTotalRequests(), bottomRightBorderCellFormat);
        setCellNumber(sheet, 4, row.getAcceptedRequests(), bottomRightBorderCellFormat);
        setCellNumber(sheet, 5, row.getAcceptedBudgetRequests(), bottomNoneBorderFormat);
        setCellNumber(sheet, 6, row.getAcceptedContractRequests(), bottomRightBorderCellFormat);
        rowIndex++;
    }

    /**
     * Рисует числовые данные строки
     *
     * @param sheet
     * @param row
     *
     * @throws Exception
     */
    private void drawRowData(WritableSheet sheet, ReportRow row) throws Exception {
        if (!row.isNeedNumber()) {
            setCellNumber(sheet, 3, row.getTotalRequests(), rightBorderCellFormat);
            setCellNumber(sheet, 4, row.getAcceptedRequests(), rightBorderCellFormat);
            setCellNumber(sheet, 5, row.getAcceptedBudgetRequests(), noneBorderFormat);
            setCellNumber(sheet, 6, row.getAcceptedContractRequests(), rightBorderCellFormat);
        }
        else {
            setCellString(sheet, 3, "", rightBorderCellFormat);
            setCellString(sheet, 4, "", rightBorderCellFormat);
            setCellString(sheet, 5, "", noneBorderFormat);
            setCellString(sheet, 6, "", rightBorderCellFormat);
        }
    }

    /**
     * Копирует строку вместе с форматом ячеек
     *
     * @param sheet
     * @param fromRow
     * @param toRow
     *
     * @throws Exception
     */
    protected void copyRow(WritableSheet sheet, int fromRow, int toRow) throws Exception {
        sheet.insertRow(toRow);
        sheet.setRowView(toRow, sheet.getRowView(fromRow));
        for (int col = 0; col < 100; col++)
            copyCell(sheet, col, fromRow, col, toRow);
    }

    protected WritableCell copyCell(WritableSheet sheet, int fromCol, int fromRow, int toCol, int toRow) throws Exception {
        WritableCell fromCell = (WritableCell) sheet.getCell(fromCol, fromRow);

        Label newCell;
        if (fromCell.getCellFormat() != null) {
            newCell = new Label(toCol, toRow, fromCell.getContents(), fromCell.getCellFormat());
        }
        else
            newCell = new Label(toCol, toRow, fromCell.getContents());

        sheet.addCell(newCell);
        return newCell;
    }

    private void setCellString(WritableSheet sheet, int colIdex, String string, WritableCellFormat format) throws WriteException {

        sheet.addCell(new Label(colIdex, rowIndex, string, format));
    }

    private void setCellNumber(WritableSheet sheet, int colIndex, Integer number, WritableCellFormat format) throws WriteException {
        sheet.addCell(new Number(colIndex, rowIndex, number, format));
    }

    protected static WritableCellFormat createFormat(boolean top, boolean bottom, boolean right, boolean left, Colour color, Alignment alignment) throws Exception {

        WritableCellFormat format = new WritableCellFormat(font);
        format.setVerticalAlignment(VerticalAlignment.TOP);
        format.setAlignment(alignment);
        if (color != null)
            format.setBackground(color);
        format.setWrap(true);


        format.setBorder(Border.ALL, jxl.format.BorderLineStyle.THIN);
        if (top)
            format.setBorder(Border.TOP, jxl.format.BorderLineStyle.MEDIUM);
        if (bottom)
            format.setBorder(Border.BOTTOM, jxl.format.BorderLineStyle.MEDIUM);
        if (right)
            format.setBorder(Border.RIGHT, jxl.format.BorderLineStyle.MEDIUM);
        if (left)
            format.setBorder(Border.LEFT, jxl.format.BorderLineStyle.MEDIUM);


        return format;
    }

    private void initFormat() throws Exception {
        font = new WritableFont(WritableFont.TIMES, 10);
        boldFont = new WritableFont(WritableFont.TIMES, 10);
        boldFont.setBoldStyle(WritableFont.BOLD);

        rightBorderCellFormat = createFormat(false, false, true, false, null, Alignment.CENTRE);
        rightBorderCellFormatLeftAlign = createFormat(false, false, true, false, null, Alignment.LEFT);
        rightBorderCellFormatRightAlign = createFormat(false, false, true, false, null, Alignment.RIGHT);
        noneBorderFormat = createFormat(false, false, false, false, null, Alignment.CENTRE);
        bottomRightBorderCellFormat = createFormat(false, true, true, false, null, Alignment.CENTRE);
        bottomRightBorderCellFormatLeftAlign = createFormat(false, true, true, false, null, Alignment.LEFT);
        bottomNoneBorderFormat = createFormat(false, true, false, false, null, Alignment.CENTRE);
    }

    private void copyHeader(WritableSheet sheet, int left, int top) throws Exception {
        for (int i = 0; i < 5; i++) {
            copyRow(sheet, i, top + i);
        }

        WritableCell cell = (WritableCell) sheet.getCell(0, top);
        String text = cell.getContents();
        String result = text.replaceAll("2.1.1", "2.1.2");
        result = result.replaceAll("ВО", "СПО");
        sheet.addCell(new Label(0, top, result, cell.getCellFormat()));

        sheet.mergeCells(0, top, 6, top);
        sheet.mergeCells(0, top + 2, 0, top + 4);
        sheet.mergeCells(1, top + 2, 1, top + 4);
        sheet.mergeCells(2, top + 2, 2, top + 4);
        sheet.mergeCells(3, top + 2, 3, top + 4);
        sheet.mergeCells(4, top + 2, 4, top + 4);
        sheet.mergeCells(5, top + 2, 6, top + 3);
        rowIndex = top + 5;
    }


}

