package ru.tandemservice.uniecrmc.base.ext.EcPreEnroll.logic;

import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniec.base.bo.EcPreEnroll.logic.EcPreEnrollParam;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.logic.IEcDistributionExtDao;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.EcDistributionUtil;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.WrapChoseEntranseDiscipline;
import ru.tandemservice.uniecrmc.base.ext.EcPreEnroll.util.PreEntrantDTORMC;
import ru.tandemservice.uniecrmc.entity.entrant.PreliminaryEnrollmentStudentExt;
import ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.base.bo.EcDistribution.EcDistributionManager;
import ru.tandemservice.uniec.base.bo.EcPreEnroll.logic.EcPreEnrollDao;
import ru.tandemservice.uniec.base.bo.EcPreEnroll.util.PreEntrantDTO;
import ru.tandemservice.uniec.entity.catalog.EnrollmentRecommendation;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.*;

public class EcPreEnrollExtDao extends EcPreEnrollDao {

    @Override
    public List<PreEntrantDTO> getPreEntrantDTOList(EcPreEnrollParam parameters)
    {
        if (parameters == null) return Collections.emptyList();

        EnrollmentDirection enrollmentDirection = parameters.getEnrollmentDirection();

        if ((enrollmentDirection == null) || (parameters.getCompensationType() == null))
            return Collections.emptyList();

        List<PreEntrantDTO> oldList = super.getPreEntrantDTOList(parameters);

        if (!EcDistributionUtil.isUsePriority(enrollmentDirection.getEnrollmentCampaign()))
            return oldList;


        List<Long> entrantIds = new ArrayList<Long>();
        List<Long> directionids = new ArrayList<Long>();

        for (PreEntrantDTO item : oldList) {
            directionids.add(item.getRequestedEnrollmentDirection().getId());
            entrantIds.add(item.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getId());
        }

        final Map<Long, RequestedEnrollmentDirectionExt> dirExtMap = new HashMap<Long, RequestedEnrollmentDirectionExt>();

        BatchUtils.execute(directionids, 1000, new BatchUtils.Action<Long>() {
            @Override
            public void execute(Collection<Long> elements) {
                DQLSelectBuilder directionExtBuilder = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirectionExt.class, "ext")
                        .where(DQLExpressions.in(RequestedEnrollmentDirectionExt.requestedEnrollmentDirection().id().fromAlias("ext"), elements));
                List<RequestedEnrollmentDirectionExt> list = getList(directionExtBuilder);

                for (RequestedEnrollmentDirectionExt ext : list) {
                    dirExtMap.put(ext.getRequestedEnrollmentDirection().getId(), ext);
                }
            }
        });

        Map<Long, List<EnrollmentRecommendation>> recommendMap = EcDistributionUtil.getRecommendationMap(entrantIds);
        Map<Long, Integer> enrPriorityMap = EcDistributionUtil.getEnrollmentPriorityMap(entrantIds);
        Map<Long, List<WrapChoseEntranseDiscipline>> edDiscMap = EcDistributionUtil.getChosenDisciplineMap(directionids);

        List<PreEntrantDTORMC> newList = new ArrayList<PreEntrantDTORMC>();

        for (PreEntrantDTO item : oldList) {
            boolean haveSpecialRights = dirExtMap.get(item.getRequestedEnrollmentDirection().getId()) != null ? dirExtMap.get(item.getRequestedEnrollmentDirection().getId()).isHaveSpecialRights() : false;
            newList.add(new PreEntrantDTORMC(item, recommendMap.get(item.getEntrant().getId()), edDiscMap.get(item.getRequestedEnrollmentDirection().getId()), haveSpecialRights, enrPriorityMap.get(item.getEntrant().getId())));
        }

        Collections.sort(newList, ((IEcDistributionExtDao) EcDistributionManager.instance().dao()).getComparator(enrollmentDirection));

        List<PreEntrantDTO> result = new ArrayList<PreEntrantDTO>(newList);

        return result;
    }

    @Override
    public PreliminaryEnrollmentStudent doChangeOrderType(
            Long requestedEnrollmentDirectionId,
            EducationOrgUnit educationOrgUnit,
            EntrantEnrollmentOrderType orderType)
    {
        RequestedEnrollmentDirection direction = getNotNull(requestedEnrollmentDirectionId);
        if (!EcDistributionUtil.isUsePriority(direction.getEnrollmentDirection().getEnrollmentCampaign())) {
            return super.doChangeOrderType(requestedEnrollmentDirectionId, educationOrgUnit, orderType);
        }

        PreliminaryEnrollmentStudent preliminaryEnrollmentStudent = super.doChangeOrderType(
                requestedEnrollmentDirectionId,
                educationOrgUnit,
                orderType);

        if (preliminaryEnrollmentStudent != null) {
            createOrUpdatePreStudentExt(preliminaryEnrollmentStudent, direction);
        }

        return preliminaryEnrollmentStudent;
    }

    protected void createOrUpdatePreStudentExt(PreliminaryEnrollmentStudent preliminaryEnrollmentStudent, RequestedEnrollmentDirection direction) {
        RequestedEnrollmentDirectionExt directionExt = get(RequestedEnrollmentDirectionExt.class, RequestedEnrollmentDirectionExt.requestedEnrollmentDirection(), direction);
        PreliminaryEnrollmentStudentExt studentExt = get(PreliminaryEnrollmentStudentExt.class, PreliminaryEnrollmentStudentExt.preliminaryEnrollmentStudent(), preliminaryEnrollmentStudent);
        boolean haveSpecialRights = false;
        if (directionExt != null)
            haveSpecialRights = directionExt.isHaveSpecialRights();

        if (studentExt == null) {
            studentExt = new PreliminaryEnrollmentStudentExt();
            studentExt.setPreliminaryEnrollmentStudent(preliminaryEnrollmentStudent);
        }

        studentExt.setHaveSpecialRights(haveSpecialRights);

        getSession().saveOrUpdate(studentExt);
    }
}
