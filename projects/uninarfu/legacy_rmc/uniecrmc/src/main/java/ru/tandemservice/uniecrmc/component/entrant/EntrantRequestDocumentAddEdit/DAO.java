package ru.tandemservice.uniecrmc.component.entrant.EntrantRequestDocumentAddEdit;

import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uniecrmc.entity.catalog.DocumentReturnMethod;
import ru.tandemservice.uniecrmc.entity.entrant.EntrantExt;
import ru.tandemservice.uni.dao.IUniBaseDao;


public class DAO extends ru.tandemservice.uniec.component.entrant.EntrantRequestDocumentAddEdit.DAO {

    @Override
    public void prepare(ru.tandemservice.uniec.component.entrant.EntrantRequestDocumentAddEdit.Model model) {
        super.prepare(model);
        Model modelExt = (Model) model;
        modelExt.setDocumentReturnMethodModel(new LazySimpleSelectModel<>(DocumentReturnMethod.class));
        modelExt.setDocumentReturnMethod(IUniBaseDao.instance.get().getCatalogItem(DocumentReturnMethod.class, "1"));
        if (modelExt.getEntrantRequest() != null && model.getEntrantRequest().getId() != null) {
            EntrantExt entrantExt = get(EntrantExt.class, EntrantExt.entrant().s(), model.getEntrantRequest().getEntrant());
            if (entrantExt != null)
                modelExt.setDocumentReturnMethod(entrantExt.getDocumentReturnMethod());
        }

    }

    @Override
    public void update(ru.tandemservice.uniec.component.entrant.EntrantRequestDocumentAddEdit.Model model)
    {
        super.update(model);
        Model modelExt = (Model) model;
        if (modelExt.getEntrantRequest() != null && modelExt.getEntrantRequest().getId() != null) {
            EntrantExt entrantExt = get(EntrantExt.class, EntrantExt.entrant().s(), model.getEntrantRequest().getEntrant());
            if (entrantExt == null) {
                entrantExt = new EntrantExt();
                entrantExt.setEntrant(modelExt.getEntrantRequest().getEntrant());
            }
            entrantExt.setDocumentReturnMethod(modelExt.getDocumentReturnMethod());
            saveOrUpdate(entrantExt);
        }

    }
}
