package ru.tandemservice.uniecrmc.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommendedDetail;
import ru.tandemservice.uniecrmc.entity.entrant.EcgEntrantRecommendedDetailExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение сущности Рекомендованный абитуриент уточняющего распределения
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcgEntrantRecommendedDetailExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniecrmc.entity.entrant.EcgEntrantRecommendedDetailExt";
    public static final String ENTITY_NAME = "ecgEntrantRecommendedDetailExt";
    public static final int VERSION_HASH = -928255481;
    private static IEntityMeta ENTITY_META;

    public static final String L_ECG_ENTRANT_RECOMMENDED_DETAIL = "ecgEntrantRecommendedDetail";
    public static final String P_HAVE_SPECIAL_RIGHTS = "haveSpecialRights";

    private EcgEntrantRecommendedDetail _ecgEntrantRecommendedDetail;     // Рекомендованный абитуриент уточняющего распределения
    private boolean _haveSpecialRights;     // Имеет особые права

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Рекомендованный абитуриент уточняющего распределения. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EcgEntrantRecommendedDetail getEcgEntrantRecommendedDetail()
    {
        return _ecgEntrantRecommendedDetail;
    }

    /**
     * @param ecgEntrantRecommendedDetail Рекомендованный абитуриент уточняющего распределения. Свойство не может быть null и должно быть уникальным.
     */
    public void setEcgEntrantRecommendedDetail(EcgEntrantRecommendedDetail ecgEntrantRecommendedDetail)
    {
        dirty(_ecgEntrantRecommendedDetail, ecgEntrantRecommendedDetail);
        _ecgEntrantRecommendedDetail = ecgEntrantRecommendedDetail;
    }

    /**
     * @return Имеет особые права. Свойство не может быть null.
     */
    @NotNull
    public boolean isHaveSpecialRights()
    {
        return _haveSpecialRights;
    }

    /**
     * @param haveSpecialRights Имеет особые права. Свойство не может быть null.
     */
    public void setHaveSpecialRights(boolean haveSpecialRights)
    {
        dirty(_haveSpecialRights, haveSpecialRights);
        _haveSpecialRights = haveSpecialRights;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EcgEntrantRecommendedDetailExtGen)
        {
            setEcgEntrantRecommendedDetail(((EcgEntrantRecommendedDetailExt)another).getEcgEntrantRecommendedDetail());
            setHaveSpecialRights(((EcgEntrantRecommendedDetailExt)another).isHaveSpecialRights());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcgEntrantRecommendedDetailExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcgEntrantRecommendedDetailExt.class;
        }

        public T newInstance()
        {
            return (T) new EcgEntrantRecommendedDetailExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "ecgEntrantRecommendedDetail":
                    return obj.getEcgEntrantRecommendedDetail();
                case "haveSpecialRights":
                    return obj.isHaveSpecialRights();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "ecgEntrantRecommendedDetail":
                    obj.setEcgEntrantRecommendedDetail((EcgEntrantRecommendedDetail) value);
                    return;
                case "haveSpecialRights":
                    obj.setHaveSpecialRights((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "ecgEntrantRecommendedDetail":
                        return true;
                case "haveSpecialRights":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "ecgEntrantRecommendedDetail":
                    return true;
                case "haveSpecialRights":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "ecgEntrantRecommendedDetail":
                    return EcgEntrantRecommendedDetail.class;
                case "haveSpecialRights":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EcgEntrantRecommendedDetailExt> _dslPath = new Path<EcgEntrantRecommendedDetailExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcgEntrantRecommendedDetailExt");
    }
            

    /**
     * @return Рекомендованный абитуриент уточняющего распределения. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EcgEntrantRecommendedDetailExt#getEcgEntrantRecommendedDetail()
     */
    public static EcgEntrantRecommendedDetail.Path<EcgEntrantRecommendedDetail> ecgEntrantRecommendedDetail()
    {
        return _dslPath.ecgEntrantRecommendedDetail();
    }

    /**
     * @return Имеет особые права. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EcgEntrantRecommendedDetailExt#isHaveSpecialRights()
     */
    public static PropertyPath<Boolean> haveSpecialRights()
    {
        return _dslPath.haveSpecialRights();
    }

    public static class Path<E extends EcgEntrantRecommendedDetailExt> extends EntityPath<E>
    {
        private EcgEntrantRecommendedDetail.Path<EcgEntrantRecommendedDetail> _ecgEntrantRecommendedDetail;
        private PropertyPath<Boolean> _haveSpecialRights;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Рекомендованный абитуриент уточняющего распределения. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EcgEntrantRecommendedDetailExt#getEcgEntrantRecommendedDetail()
     */
        public EcgEntrantRecommendedDetail.Path<EcgEntrantRecommendedDetail> ecgEntrantRecommendedDetail()
        {
            if(_ecgEntrantRecommendedDetail == null )
                _ecgEntrantRecommendedDetail = new EcgEntrantRecommendedDetail.Path<EcgEntrantRecommendedDetail>(L_ECG_ENTRANT_RECOMMENDED_DETAIL, this);
            return _ecgEntrantRecommendedDetail;
        }

    /**
     * @return Имеет особые права. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EcgEntrantRecommendedDetailExt#isHaveSpecialRights()
     */
        public PropertyPath<Boolean> haveSpecialRights()
        {
            if(_haveSpecialRights == null )
                _haveSpecialRights = new PropertyPath<Boolean>(EcgEntrantRecommendedDetailExtGen.P_HAVE_SPECIAL_RIGHTS, this);
            return _haveSpecialRights;
        }

        public Class getEntityClass()
        {
            return EcgEntrantRecommendedDetailExt.class;
        }

        public String getEntityName()
        {
            return "ecgEntrantRecommendedDetailExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
