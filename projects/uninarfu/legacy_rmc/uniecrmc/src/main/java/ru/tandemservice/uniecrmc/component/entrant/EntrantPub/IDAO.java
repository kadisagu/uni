package ru.tandemservice.uniecrmc.component.entrant.EntrantPub;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {
    public void prepareDataSource(Model paramModel);
}
