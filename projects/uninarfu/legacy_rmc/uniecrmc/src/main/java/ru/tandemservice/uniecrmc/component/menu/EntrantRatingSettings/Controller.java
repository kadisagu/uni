package ru.tandemservice.uniecrmc.component.menu.EntrantRatingSettings;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniecrmc.entity.entrant.EntrantRecommendedSettings;
import ru.tandemservice.uni.UniUtils;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        getDao().prepare(getModel(component));
        prepareListDataSource(component);
    }

    protected void prepareListDataSource(IBusinessComponent component) {
        Model model = getModel(component);
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<EntrantRecommendedSettings> dataSource = UniUtils.createDataSource(component, getDao());

        dataSource.addColumn(new SimpleColumn("Приемная кампания", EntrantRecommendedSettings.enrollmentCampaign().title()).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Учебный год", EntrantRecommendedSettings.enrollmentCampaign().educationYear().title()).setOrderable(true));
        dataSource.addColumn(new ToggleColumn("Запускать демон", EntrantRecommendedSettings.useDemon()).setListener("onChangeUse"));
        dataSource.addColumn(new BlockColumn<Integer>("interval", "Интервал"));
        dataSource.addColumn(new BlockColumn<String>("path", "Файл"));
        dataSource.addColumn(new ActionColumn("Сохранить", "save", "onClickSave").setPermissionKey("uniecrmcEntrantRatingSettingsEdit"));
        dataSource.addColumn(new BlockColumn<Long>("state", "Состояние"));

        model.setDataSource(dataSource);
    }

    public void onClickSave(IBusinessComponent component) {
        Model model = getModel(component);
        Long id = component.getListenerParameter();
        getDao().saveChanges(model, id);
    }

    public void onChangeUse(IBusinessComponent component) {
        Long id = component.getListenerParameter();
        getDao().changeUse(id);
    }

    public void onClickRun(IBusinessComponent component) {
        Long id = component.getListenerParameter();
        getDao().runNow(id);
    }
}
