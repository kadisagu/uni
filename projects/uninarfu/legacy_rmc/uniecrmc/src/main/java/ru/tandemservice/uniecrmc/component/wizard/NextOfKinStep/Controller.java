package ru.tandemservice.uniecrmc.component.wizard.NextOfKinStep;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInlineUI;
import ru.tandemservice.tools.AddressUtil;

public class Controller extends ru.tandemservice.uniec.component.wizard.NextOfKinStep.Controller {

    public void onChangeMatchEntrant(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        AddressBaseEditInlineUI addressBaseEditInlineUI = (AddressBaseEditInlineUI) component.getRegion("addressRegion").getActiveComponent().getPresenter();

        AddressUtil.copyToComponent(model.isMatchEntrant() ? model.getEntrant().getPerson().getAddress() : null, component, "addressRegion");
    }
}
