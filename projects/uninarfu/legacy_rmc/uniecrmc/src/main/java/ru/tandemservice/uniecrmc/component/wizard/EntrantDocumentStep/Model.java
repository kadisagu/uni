package ru.tandemservice.uniecrmc.component.wizard.EntrantDocumentStep;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniecrmc.entity.catalog.DocumentReturnMethod;

public class Model extends ru.tandemservice.uniec.component.wizard.EntrantDocumentStep.Model {
    private ISelectModel documentReturnMethodModel;
    private DocumentReturnMethod documentReturnMethod;

    public ISelectModel getDocumentReturnMethodModel() {
        return documentReturnMethodModel;
    }

    public void setDocumentReturnMethodModel(ISelectModel documentReturnMethodModel) {
        this.documentReturnMethodModel = documentReturnMethodModel;
    }

    public DocumentReturnMethod getDocumentReturnMethod() {
        return documentReturnMethod;
    }

    public void setDocumentReturnMethod(DocumentReturnMethod documentReturnMethod) {
        this.documentReturnMethod = documentReturnMethod;
    }
}
