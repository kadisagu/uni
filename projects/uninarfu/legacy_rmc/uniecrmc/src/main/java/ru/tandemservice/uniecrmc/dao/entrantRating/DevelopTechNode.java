package ru.tandemservice.uniecrmc.dao.entrantRating;

import ru.tandemservice.uni.entity.catalog.DevelopTech;

import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

public class DevelopTechNode {

    @XmlElement(name = "DevelopTech")
    public List<RowNode> list = new ArrayList<>();

    public DevelopTechNode() {
    }

    public void add(DevelopTech entity) {
        RowNode node = new RowNode(entity.getCode(), entity.getTitle(), entity.getShortTitle());
        list.add(node);
    }
}
