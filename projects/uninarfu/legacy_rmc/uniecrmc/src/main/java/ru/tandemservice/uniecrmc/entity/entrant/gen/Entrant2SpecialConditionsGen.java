package ru.tandemservice.uniecrmc.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniecrmc.entity.catalog.SpecialConditionsForEntrant;
import ru.tandemservice.uniecrmc.entity.entrant.Entrant2SpecialConditions;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение сущности Абитуриент
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class Entrant2SpecialConditionsGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniecrmc.entity.entrant.Entrant2SpecialConditions";
    public static final String ENTITY_NAME = "entrant2SpecialConditions";
    public static final int VERSION_HASH = -104524084;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT = "entrant";
    public static final String L_SPECIAL_CONDITIONS_FOR_ENTRANT = "specialConditionsForEntrant";

    private Entrant _entrant;     // Абитуриент
    private SpecialConditionsForEntrant _specialConditionsForEntrant;     // Особые условия для лиц с ограниченными возможностями

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public Entrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Абитуриент. Свойство не может быть null.
     */
    public void setEntrant(Entrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Особые условия для лиц с ограниченными возможностями.
     */
    public SpecialConditionsForEntrant getSpecialConditionsForEntrant()
    {
        return _specialConditionsForEntrant;
    }

    /**
     * @param specialConditionsForEntrant Особые условия для лиц с ограниченными возможностями.
     */
    public void setSpecialConditionsForEntrant(SpecialConditionsForEntrant specialConditionsForEntrant)
    {
        dirty(_specialConditionsForEntrant, specialConditionsForEntrant);
        _specialConditionsForEntrant = specialConditionsForEntrant;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof Entrant2SpecialConditionsGen)
        {
            setEntrant(((Entrant2SpecialConditions)another).getEntrant());
            setSpecialConditionsForEntrant(((Entrant2SpecialConditions)another).getSpecialConditionsForEntrant());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends Entrant2SpecialConditionsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Entrant2SpecialConditions.class;
        }

        public T newInstance()
        {
            return (T) new Entrant2SpecialConditions();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrant":
                    return obj.getEntrant();
                case "specialConditionsForEntrant":
                    return obj.getSpecialConditionsForEntrant();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrant":
                    obj.setEntrant((Entrant) value);
                    return;
                case "specialConditionsForEntrant":
                    obj.setSpecialConditionsForEntrant((SpecialConditionsForEntrant) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrant":
                        return true;
                case "specialConditionsForEntrant":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrant":
                    return true;
                case "specialConditionsForEntrant":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrant":
                    return Entrant.class;
                case "specialConditionsForEntrant":
                    return SpecialConditionsForEntrant.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Entrant2SpecialConditions> _dslPath = new Path<Entrant2SpecialConditions>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Entrant2SpecialConditions");
    }
            

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.Entrant2SpecialConditions#getEntrant()
     */
    public static Entrant.Path<Entrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Особые условия для лиц с ограниченными возможностями.
     * @see ru.tandemservice.uniecrmc.entity.entrant.Entrant2SpecialConditions#getSpecialConditionsForEntrant()
     */
    public static SpecialConditionsForEntrant.Path<SpecialConditionsForEntrant> specialConditionsForEntrant()
    {
        return _dslPath.specialConditionsForEntrant();
    }

    public static class Path<E extends Entrant2SpecialConditions> extends EntityPath<E>
    {
        private Entrant.Path<Entrant> _entrant;
        private SpecialConditionsForEntrant.Path<SpecialConditionsForEntrant> _specialConditionsForEntrant;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.Entrant2SpecialConditions#getEntrant()
     */
        public Entrant.Path<Entrant> entrant()
        {
            if(_entrant == null )
                _entrant = new Entrant.Path<Entrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Особые условия для лиц с ограниченными возможностями.
     * @see ru.tandemservice.uniecrmc.entity.entrant.Entrant2SpecialConditions#getSpecialConditionsForEntrant()
     */
        public SpecialConditionsForEntrant.Path<SpecialConditionsForEntrant> specialConditionsForEntrant()
        {
            if(_specialConditionsForEntrant == null )
                _specialConditionsForEntrant = new SpecialConditionsForEntrant.Path<SpecialConditionsForEntrant>(L_SPECIAL_CONDITIONS_FOR_ENTRANT, this);
            return _specialConditionsForEntrant;
        }

        public Class getEntityClass()
        {
            return Entrant2SpecialConditions.class;
        }

        public String getEntityName()
        {
            return "entrant2SpecialConditions";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
