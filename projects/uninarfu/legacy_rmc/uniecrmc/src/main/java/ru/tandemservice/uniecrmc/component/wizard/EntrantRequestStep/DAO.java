package ru.tandemservice.uniecrmc.component.wizard.EntrantRequestStep;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.matrix.CellCoordinates;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.EcDistributionUtil;
import ru.tandemservice.uniecrmc.base.ext.EcEntrant.logic.IEcEntrantDaoExt;
import ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniec.base.bo.EcEntrant.EcEntrantManager;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.ProfileExaminationMark;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.util.EntrantRequestAddEditUtil;

import java.util.*;

public class DAO extends ru.tandemservice.uniec.component.wizard.EntrantRequestStep.DAO {

    @Override
    public void prepare(final ru.tandemservice.uniec.component.entrant.EntrantRequestAddEdit.Model model) {
        super.prepare(model);
        final Model myModel = (Model) model;

        if (!myModel.isUseGroup())
            return;

        myModel.setCourseModel(new LazySimpleSelectModel<>(DevelopGridDAO.getCourseList()));
        myModel.setTermModel(new DQLFullCheckSelectModel(Term.title().s()) {

            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(DevelopGridTerm.class, "gr");

                FilterUtils.applySelectFilter(subBuilder, "gr", DevelopGridTerm.developGrid().developPeriod(), model.getDevelopPeriod());
                FilterUtils.applySelectFilter(subBuilder, "gr", DevelopGridTerm.course(), myModel.getCourse());

                subBuilder.column(DevelopGridTerm.term().id().fromAlias("gr").s());
                subBuilder.distinct();

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Term.class, alias)
                        .where(DQLExpressions.in(DQLExpressions.property(Term.id().fromAlias(alias)), subBuilder.buildQuery()))
                        .order(Term.code().fromAlias(alias).s());
                return builder;
            }
        });
        myModel.setGroupModel(new DQLFullCheckSelectModel(Group.title().s()) {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Group.class, alias);
                FilterUtils.applySelectFilter(builder, alias, Group.educationOrgUnit().formativeOrgUnit(), model.getFormativeOrgUnit());
                FilterUtils.applySelectFilter(builder, alias, Group.educationOrgUnit().territorialOrgUnit(), model.getTerritorialOrgUnit());
                FilterUtils.applySelectFilter(builder, alias, Group.educationOrgUnit().educationLevelHighSchool(), model.getEducationLevelsHighSchool());
                FilterUtils.applySelectFilter(builder, alias, Group.educationOrgUnit().developForm(), model.getDevelopForm());
                FilterUtils.applySelectFilter(builder, alias, Group.educationOrgUnit().developCondition(), model.getDevelopCondition());
                FilterUtils.applySelectFilter(builder, alias, Group.educationOrgUnit().developPeriod(), model.getDevelopPeriod());
                FilterUtils.applySelectFilter(builder, alias, Group.educationOrgUnit().developTech(), model.getDevelopTech());

                FilterUtils.applySimpleLikeFilter(builder, alias, Group.title(), filter);

                return builder;
            }
        });
    }

    @Override
    public void prepareSelectedEnrollmentDirectionList(
            ru.tandemservice.uniec.component.entrant.EntrantRequestAddEdit.Model model,
            ErrorCollector errorCollector)
    {
        Model myModel = (Model) model;

        if (!myModel.isUseGroup()) {
            super.prepareSelectedEnrollmentDirectionList(model, errorCollector);
            return;
        }

        EntrantRequest entrantRequest = model.getEntrantRequest();

        List<MultiKey> enrollmentDirectionList = (List<MultiKey>) EcDistributionUtil.invokePrivateMethod(this, ru.tandemservice.uniec.component.entrant.EntrantRequestAddEdit.DAO.class, "getAddingEnrollmentDirections", new Object[]{model, errorCollector});
//	    		getAddingEnrollmentDirections(model, errorCollector);
        if (errorCollector.hasErrors()) {
            return;
        }
        List<RequestedEnrollmentDirection> newDirections = new ArrayList<>();


        long id = 0L;
        List<RequestedEnrollmentDirection> alreadySelectedDirections = model.getSelectedRequestedEnrollmentDirectionList();
        for (RequestedEnrollmentDirection item : alreadySelectedDirections) {
            id = Math.min(id, item.getId().longValue());
        }
        for (MultiKey key : enrollmentDirectionList) {
            id -= 1L;

            RequestedEnrollmentDirection newDirection = new RequestedEnrollmentDirection();
            newDirection.update(model.getRequestedEnrollmentDirection());

            newDirection.setEnrollmentDirection((EnrollmentDirection) key.getKey(0));
            newDirection.setCompensationType((CompensationType) key.getKey(1));
            newDirection.setEntrantRequest(entrantRequest);
            newDirection.setRegDate(new Date());
            newDirection.setOriginalDocumentHandedIn(false);


            List<ProfileExaminationMark> profileMarkList = new ArrayList<>();
            for (Map.Entry<CellCoordinates, Integer> entry : model.getMatrixData().entrySet()) {
                if (entry.getValue() != null) {
                    Discipline2RealizationWayRelation discipline = (Discipline2RealizationWayRelation) getNotNull(Discipline2RealizationWayRelation.class, ((CellCoordinates) entry.getKey()).getRowId());
                    ProfileExaminationMark profileMark = new ProfileExaminationMark();
                    profileMark.setMark(((Integer) entry.getValue()).intValue());
                    profileMark.setDiscipline(discipline);
                    profileMark.setEntrant(entrantRequest.getEntrant());
                    profileMarkList.add(profileMark);
                }
            }
            newDirection.setId(Long.valueOf(id));

            model.getProfileMarkMap().put(Long.valueOf(id), profileMarkList);
            if (model.getProfileKnowledgeList() != null) {
                model.getProfileKnowledgeMap().put(Long.valueOf(id), model.getProfileKnowledgeList());
            }
            //сохраним расширение
            RequestedEnrollmentDirectionExt directionExt = new RequestedEnrollmentDirectionExt();
            directionExt.setRequestedEnrollmentDirection(newDirection);
            directionExt.setCourse(myModel.getCourse());
            directionExt.setTerm(myModel.getTerm());
            directionExt.setGroup(myModel.getGroup());

            myModel.getDirecrionExtMap().put(Long.valueOf(id), directionExt);

            newDirections.add(newDirection);
        }
        if (model.getRequestedEnrollmentDirection().isOriginalDocumentHandedIn()) {
            model.setOriginalHandedIn(Boolean.valueOf(true));
            for (RequestedEnrollmentDirection selectedDirection : alreadySelectedDirections) {
                selectedDirection.setOriginalDocumentHandedIn(false);
            }
            Map<String, List<RequestedEnrollmentDirection>> map = new Hashtable<>();
            for (RequestedEnrollmentDirection item : newDirections) {
                List<RequestedEnrollmentDirection> list = map.get(item.getCompensationType().getCode());
                if (list == null) {
                    map.put(item.getCompensationType().getCode(), list = new ArrayList<>());
                }
                list.add(item);
            }
            RequestedEnrollmentDirection direction;
            if ((model.getEnrollmentCampaign().isOriginalDocumentPerDirection()) && (map.size() > 1)) {
                List<RequestedEnrollmentDirection> list = map.get("1");
                for (RequestedEnrollmentDirection item : list) {
                    item.setOriginalDocumentHandedIn(true);
                }
                direction = list.get(list.size() - 1);
            }
            else {
                for (RequestedEnrollmentDirection item : newDirections) {
                    item.setOriginalDocumentHandedIn(true);
                }
                direction = newDirections.get(newDirections.size() - 1);
            }
            StructureEducationLevels levelType = direction.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType();
            model.setOriginalPlace(levelType.getDativeCaseShortTitle() + " " + direction.getEnrollmentDirection().getShortTitle());
        }
        List<RequestedEnrollmentDirection> mergedDirections = new ArrayList<>(alreadySelectedDirections);
        EntrantRequestAddEditUtil.mergeSelectedDirections(mergedDirections, newDirections);
        if (!EntrantRequestAddEditUtil.validateRequestedDirections(getSession(), mergedDirections, model.getForDelete(), model.getEntrantRequest())) {
            return;
        }
        model.setSelectedRequestedEnrollmentDirectionList(mergedDirections);
        if ((!model.isAddForm()) || (!model.isOneDirectionPerRequest())) {
            model.setRequestedEnrollmentDirection(new RequestedEnrollmentDirection());
        }
    }

    @Override
    public void reset(
            ru.tandemservice.uniec.component.entrant.EntrantRequestAddEdit.Model model)
    {
        super.reset(model);
        Model myModel = (Model) model;

        if (!myModel.isUseGroup())
            return;

        myModel.setCourse(null);
        myModel.setTerm(null);
        myModel.setGroup(null);

    }

    @Override
    public void update(
            ru.tandemservice.uniec.component.entrant.EntrantRequestAddEdit.Model model)
    {
        Model myModel = (Model) model;

        if (!myModel.isUseGroup())
            super.update(model);
        else
            ((IEcEntrantDaoExt) EcEntrantManager.instance().dao()).saveOrUpdateEntrantRequest(model.getSelectedRequestedEnrollmentDirectionList(), model.getForDelete(), model.isAddForm(), model.getEntrantRequest(), model.getRegNumber(), model.getDirectionNumber(), model.getProfileKnowledgeMap(), model.getProfileMarkMap(), myModel.getDirecrionExtMap());
    }
}	
