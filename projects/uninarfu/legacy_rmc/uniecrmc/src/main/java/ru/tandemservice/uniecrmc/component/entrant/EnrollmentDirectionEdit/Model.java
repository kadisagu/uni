package ru.tandemservice.uniecrmc.component.entrant.EnrollmentDirectionEdit;

import ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionExt;

public class Model extends ru.tandemservice.uniec.component.entrant.EnrollmentDirectionEdit.Model {

    private EnrollmentDirectionExt enrollmentDirectionExt;

    public EnrollmentDirectionExt getEnrollmentDirectionExt() {
        return enrollmentDirectionExt;
    }

    public void setEnrollmentDirectionExt(
            EnrollmentDirectionExt enrollmentDirectionExt)
    {
        this.enrollmentDirectionExt = enrollmentDirectionExt;
    }

}
