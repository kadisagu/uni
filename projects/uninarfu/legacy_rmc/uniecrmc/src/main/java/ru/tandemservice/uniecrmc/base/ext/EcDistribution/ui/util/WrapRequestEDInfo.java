package ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util;

public class WrapRequestEDInfo {
    private Long id;
    private String requestNumber;
    private String stateCode;
    private String stateTitle;
    private String educationTitle;
    private Integer priority;


    public WrapRequestEDInfo(
            Long id
            , String requestNumber
            , String stateCode
            , String stateTitle
            , String educationTitle
            , Integer priority
    )
    {

        this.id = id;
        this.requestNumber = requestNumber;
        this.stateTitle = stateTitle;
        this.educationTitle = educationTitle;
        this.priority = priority;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRequestNumber() {
        return requestNumber;
    }

    public void setRequestNumber(String requestNumber) {
        this.requestNumber = requestNumber;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getStateTitle() {
        return stateTitle;
    }

    public void setStateTitle(String stateTitle) {
        this.stateTitle = stateTitle;
    }

    public String getEducationTitle() {
        return educationTitle;
    }

    public void setEducationTitle(String educationTitle) {
        this.educationTitle = educationTitle;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    @Override
    public int hashCode()
    {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        // TODO Auto-generated method stub
        if (obj == null)
            return super.equals(obj);
        else {
            if (this.hashCode() == obj.hashCode())
                return true;
            else
                return false;
        }
    }
}
