package ru.tandemservice.uniecrmc.component.systemaction.CloneChosenEntranceDiscipline;

import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.dao.IEntrantDAO;
import ru.tandemservice.uniec.entity.catalog.codes.EntrantStateCodes;
import ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DAO
        extends UniDao<Model> implements IDAO
{

    @Override
    public void prepare(Model model)
    {
        // универсальная заполнячилка списка приемок
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        Object numberObj = model.getSettings().get("regNumber");
        if (numberObj == null)
            model.getSettings().set("regNumber", 0);
    }

    @Override
    public void update(Model model)
    {
    }


    private Map<Long, List<Long>> mapChosenEntranceDiscipline = new HashMap<>();


    @Override
    public List<Long> getRequestedEnrollmentDirectionList(
            EnrollmentCampaign enrollmentCampaign, int regNumber)
    {

        // исключая RD у которых есть ВИ
        DQLSelectBuilder dqlNotInCD = new DQLSelectBuilder().fromEntity(ChosenEntranceDiscipline.class, "cedNI")
                .where(DQLExpressions.eq(property(ChosenEntranceDiscipline.chosenEnrollmentDirection().enrollmentDirection().enrollmentCampaign().fromAlias("cedNI")), value(enrollmentCampaign)))
                .column(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("cedNI").s())
                .predicate(DQLPredicateType.distinct);

        // исключить зачисленных и пред. зачисленных
        DQLSelectBuilder dqlNotIn = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "redNI")
                .where(eq(property(RequestedEnrollmentDirection.enrollmentDirection().enrollmentCampaign().fromAlias("redNI")), value(enrollmentCampaign)))
                        // пред зачисленные и зачисленные
                .where(DQLExpressions.in(property(RequestedEnrollmentDirection.state().code().fromAlias("redNI")), getExcludeCodes()))
                .column(RequestedEnrollmentDirection.entrantRequest().id().fromAlias("redNI").s())
                .predicate(DQLPredicateType.distinct);

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "red")
                .where(eq(property(RequestedEnrollmentDirection.enrollmentDirection().enrollmentCampaign().fromAlias("red")), value(enrollmentCampaign)))

                        // правильные статусы
                .where(DQLExpressions.in(property(RequestedEnrollmentDirection.state().code().fromAlias("red")), getValidCodes()))

                        // исключая зачисленных и пред зачисленных
                .where(DQLExpressions.notIn(property(RequestedEnrollmentDirection.entrantRequest().id().fromAlias("red")), dqlNotIn.getQuery()))

                        // исключая тех, у кого есть ВИ
                .where(DQLExpressions.notIn(property(RequestedEnrollmentDirection.id().fromAlias("red")), dqlNotInCD.getQuery()))


                .column(RequestedEnrollmentDirection.id().fromAlias("red").s())
                .predicate(DQLPredicateType.distinct);

        if (regNumber > 0)
            dql.where(eq(property(RequestedEnrollmentDirection.entrantRequest().regNumber().fromAlias("red")), value(regNumber)));


        // строим мап с выбранными дисц
        DQLSelectBuilder dqlCD = new DQLSelectBuilder().fromEntity(ChosenEntranceDiscipline.class, "ced")
                .where(DQLExpressions.in(property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("ced")), dql.getQuery()))
                .column(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("ced").s())
                .column(ChosenEntranceDiscipline.id().fromAlias("ced").s());


        List<Object[]> lstCD = dqlCD.createStatement(getSession()).list();


        for (Object[] objs : lstCD) {
            Long requestedEnrollmentDirectionId = (Long) objs[0];
            Long chosenEntranceDisciplineId = (Long) objs[1];

            List<Long> lstSub = new ArrayList<>();
            if (mapChosenEntranceDiscipline.containsKey(requestedEnrollmentDirectionId))
                lstSub = mapChosenEntranceDiscipline.get(requestedEnrollmentDirectionId);
            else
                mapChosenEntranceDiscipline.put(requestedEnrollmentDirectionId, lstSub);

            lstSub.add(chosenEntranceDisciplineId);

        }


        List<Long> lst = dql.createStatement(getSession()).list();

        return lst;
    }

    @Override
    @Transactional
    public Long processRequestedEnrollmentDirection(
            Long requestedEnrollmentDirectionId)
    {
        //1 - смотрим, что нет всупительных испытаний
        if (_hasCD(requestedEnrollmentDirectionId))
            return null;

        // вступительных нет, ещем откуда копировать
        RequestedEnrollmentDirection copyFrom = findOtherDirection(requestedEnrollmentDirectionId);

        RequestedEnrollmentDirection copyTo = get(RequestedEnrollmentDirection.class, requestedEnrollmentDirectionId);

        if (copyFrom == null)
            return null;

        boolean hasCopy = cloneChosenEntranceDiscipline(copyFrom, copyTo);
        if (hasCopy) {
            Long entrantId = copyTo.getEntrantRequest().getEntrant().getId();
            return entrantId;
        }
        else
            return null;
    }

    private boolean cloneChosenEntranceDiscipline
            (
                    RequestedEnrollmentDirection fromRD,
                    RequestedEnrollmentDirection toRD)
    {

        boolean hasCopy = false;
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(ChosenEntranceDiscipline.class, "ced")
                .where(eq(property(ChosenEntranceDiscipline.chosenEnrollmentDirection().fromAlias("ced")), value(fromRD)))
                .column("ced");
        List<ChosenEntranceDiscipline> lst = dql.createStatement(getSession()).list();

        for (ChosenEntranceDiscipline ced : lst) {
            ChosenEntranceDiscipline cedNew = new ChosenEntranceDiscipline();

            cedNew.setChosenEnrollmentDirection(toRD);
            cedNew.setEnrollmentCampaignDiscipline(ced.getEnrollmentCampaignDiscipline());

            cedNew.setFinalMark(ced.getFinalMark());
            cedNew.setFinalMarkSource(ced.getFinalMarkSource());

            saveOrUpdate(cedNew);
            hasCopy = true;
        }

        return hasCopy;
    }

    private RequestedEnrollmentDirection findOtherDirection(
            Long requestedEnrollmentDirectionId)
    {

        // ищем в том-же заявлении
        RequestedEnrollmentDirection rd = get(RequestedEnrollmentDirection.class, requestedEnrollmentDirectionId);

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "red")

                // в том же заявлении
                .where(eq(property(RequestedEnrollmentDirection.entrantRequest().id().fromAlias("red")), value(rd.getEntrantRequest().getId())))
                        // по тому-же направлению
                .where(eq(property(RequestedEnrollmentDirection.enrollmentDirection().id().fromAlias("red")), value(rd.getEnrollmentDirection().getId())))
                        // исключая себя
                .where(DQLExpressions.ne(property(RequestedEnrollmentDirection.id().fromAlias("red")), value(requestedEnrollmentDirectionId)))
                .column("red");

        List<RequestedEnrollmentDirection> lst = dql.createStatement(getSession()).list();
        if (lst != null && lst.size() > 0)
            return lst.get(0);
        else
            return null;
    }

    private boolean _hasCD(Long requestedEnrollmentDirectionId)
    {
        if (mapChosenEntranceDiscipline.containsKey(requestedEnrollmentDirectionId)) {
            List<Long> lst = mapChosenEntranceDiscipline.get(requestedEnrollmentDirectionId);
            if (lst == null || lst.size() == 0)
                return false;
            else
                return true;
        }
        else
            return false;
    }

    @Override
    @Transactional
    public void recalculatePart(List<Long> part)
    {
        IEntrantDAO idao = (IEntrantDAO) ApplicationRuntime.getBean(IEntrantDAO.class.getName());
        idao.updateEntrantData(part);
    }

    private List<String> getExcludeCodes()
    {
        // только правильные статусы
        List<String> validCode = new ArrayList<>();
        validCode.add(EntrantStateCodes.ENROLED);
        validCode.add(EntrantStateCodes.PRELIMENARY_ENROLLED);
        return validCode;
    }

    private List<String> getValidCodes()
    {
        // только правильные статусы
        List<String> validCode = new ArrayList<>();
        validCode.add(EntrantStateCodes.ACTIVE);
        //validCode.add(EntrantStateCodes.TO_BE_ENROLED);
        return validCode;
    }

    @Override
    public void iniData() {
        mapChosenEntranceDiscipline = new HashMap<>();
    }


}
