package ru.tandemservice.uniecrmc.component.report.EnrollmentResultByCategory.Add;

import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniecrmc.entity.entrant.EnrollmentResultByCategoryReport;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil.Parameters;

import java.util.List;

public class Model implements MultiEnrollmentDirectionUtil.Model, IEnrollmentCampaignSelectModel {

    private EnrollmentResultByCategoryReport report = new EnrollmentResultByCategoryReport();
    private MultiEnrollmentDirectionUtil.Parameters parameters;
    private IPrincipalContext principalContext;

    private List<EnrollmentCampaign> enrollmentCampaignList;

    private List<DevelopForm> developFormList;
    private ISelectModel developFormListModel;

    private boolean qualificationActive;
    private ISelectModel qualificationListModel;
    private List<Qualifications> qualificationList;

    private boolean formativeOrgUnitActive;
    private ISelectModel formativeOrgUnitListModel;
    private List<OrgUnit> formativeOrgUnitList;

    private boolean territorialOrgUnitActive;
    private ISelectModel territorialOrgUnitListModel;
    private List<OrgUnit> territorialOrgUnitList;

    private boolean developConditionActive;
    private ISelectModel developConditionListModel;
    private List<DevelopCondition> developConditionList;

    private boolean entrantStateActive;
    private ISelectModel entrantStateListModel;
    private List<EntrantState> entrantStateList;

    public EnrollmentResultByCategoryReport getReport() {
        return report;
    }

    public void setReport(EnrollmentResultByCategoryReport report) {
        this.report = report;
    }

    public boolean isEntrantStateActive() {
        return entrantStateActive;
    }

    public void setEntrantStateActive(boolean entrantStateActive) {
        this.entrantStateActive = entrantStateActive;
    }

    public ISelectModel getEntrantStateListModel() {
        return entrantStateListModel;
    }

    public void setEntrantStateListModel(ISelectModel entrantStateListModel) {
        this.entrantStateListModel = entrantStateListModel;
    }

    public List<EntrantState> getEntrantStateList() {
        return entrantStateList;
    }

    public void setEntrantStateList(List<EntrantState> entrantStateList) {
        this.entrantStateList = entrantStateList;
    }

    public ISelectModel getDevelopFormListModel() {
        return developFormListModel;
    }

    public void setDevelopFormListModel(ISelectModel developFormListModel) {
        this.developFormListModel = developFormListModel;
    }

    public IDataSettings getSettings() {
        return null;
    }

    public EnrollmentCampaign getEnrollmentCampaign() {
        return this.report.getEnrollmentCampaign();
    }

    public DevelopForm getDevelopForm() {
        return this.report.getDevelopForm();
    }

    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign) {
        this.report.setEnrollmentCampaign(enrollmentCampaign);
    }

    public IPrincipalContext getPrincipalContext() {
        return principalContext;
    }

    public void setPrincipalContext(IPrincipalContext principalContext) {
        this.principalContext = principalContext;
    }

    public boolean isQualificationActive() {
        return qualificationActive;
    }

    public void setQualificationActive(boolean qualificationActive) {
        this.qualificationActive = qualificationActive;
    }

    public ISelectModel getQualificationListModel() {
        return qualificationListModel;
    }

    public void setQualificationListModel(ISelectModel qualificationListModel) {
        this.qualificationListModel = qualificationListModel;
    }

    public List<Qualifications> getQualificationList() {
        return qualificationList;
    }

    public void setQualificationList(List<Qualifications> qualificationList) {
        this.qualificationList = qualificationList;
    }

    public boolean isFormativeOrgUnitActive() {
        return formativeOrgUnitActive;
    }

    public void setFormativeOrgUnitActive(boolean formativeOrgUnitActive) {
        this.formativeOrgUnitActive = formativeOrgUnitActive;
    }

    public ISelectModel getFormativeOrgUnitListModel() {
        return formativeOrgUnitListModel;
    }

    public void setFormativeOrgUnitListModel(ISelectModel formativeOrgUnitListModel) {
        this.formativeOrgUnitListModel = formativeOrgUnitListModel;
    }

    public List<OrgUnit> getFormativeOrgUnitList() {
        return formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList) {
        this.formativeOrgUnitList = formativeOrgUnitList;
    }

    public boolean isTerritorialOrgUnitActive() {
        return territorialOrgUnitActive;
    }

    public void setTerritorialOrgUnitActive(boolean territorialOrgUnitActive) {
        this.territorialOrgUnitActive = territorialOrgUnitActive;
    }

    public ISelectModel getTerritorialOrgUnitListModel() {
        return territorialOrgUnitListModel;
    }

    public void setTerritorialOrgUnitListModel(ISelectModel territorialOrgUnitListModel) {
        this.territorialOrgUnitListModel = territorialOrgUnitListModel;
    }

    public List<OrgUnit> getTerritorialOrgUnitList() {
        return territorialOrgUnitList;
    }

    public void setTerritorialOrgUnitList(List<OrgUnit> territorialOrgUnitList) {
        this.territorialOrgUnitList = territorialOrgUnitList;
    }

    public boolean isDevelopConditionActive() {
        return developConditionActive;
    }

    public void setDevelopConditionActive(boolean developConditionActive) {
        this.developConditionActive = developConditionActive;
    }

    public ISelectModel getDevelopConditionListModel() {
        return developConditionListModel;
    }

    public void setDevelopConditionListModel(ISelectModel developConditionListModel) {
        this.developConditionListModel = developConditionListModel;
    }

    public List<DevelopCondition> getDevelopConditionList() {
        return developConditionList;
    }

    public void setDevelopConditionList(List<DevelopCondition> developConditionList) {
        this.developConditionList = developConditionList;
    }

    public List<DevelopForm> getDevelopFormList() {
        return developFormList;
    }

    public void setDevelopFormList(List<DevelopForm> developFormList) {
        this.developFormList = developFormList;
    }

    public void setParameters(MultiEnrollmentDirectionUtil.Parameters parameters) {
        this.parameters = parameters;
    }

    public List<EnrollmentCampaign> getSelectedEnrollmentCampaignList() {
        return MultiEnrollmentDirectionUtil.getSingletonList(getReport().getEnrollmentCampaign());
    }

    public List<OrgUnit> getSelectedFormativeOrgUnitList() {
        return isFormativeOrgUnitActive() ? getFormativeOrgUnitList() : null;
    }

    public List<OrgUnit> getSelectedTerritorialOrgUnitList() {
        return isTerritorialOrgUnitActive() ? getTerritorialOrgUnitList() : null;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList() {
        return this.enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> list) {
        this.enrollmentCampaignList = list;
    }

    @Override
    public Parameters getParameters() {
        return this.parameters;
    }

    @Override
    public List<DevelopCondition> getSelectedDevelopConditionList() {
        return isDevelopConditionActive() ? getDevelopConditionList() : null;
    }

    @Override
    public List<DevelopForm> getSelectedDevelopFormList() {
        return MultiEnrollmentDirectionUtil.getSingletonList(getReport().getDevelopForm());
    }

    @Override
    public List<DevelopPeriod> getSelectedDevelopPeriodList() {
        return null;
    }

    @Override
    public List<EducationLevelsHighSchool> getSelectedEducationLevelHighSchoolList() {
        return null;
    }

    @Override
    public List<DevelopTech> getSelectedDevelopTechList() {
        return null;
    }
}
