package ru.tandemservice.uniecrmc.component.entrant.EntrantRequestTab;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.BusinessComponent;
import org.tandemframework.core.component.impl.ComponentRegion;
import org.tandemframework.core.view.list.column.IEntityHandler;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniec.component.entrant.EntrantRequestTab.Model;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

public class Controller extends ru.tandemservice.uniec.component.entrant.EntrantRequestTab.Controller {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        super.onRefreshComponent(component);

        Model model = (Model) getModel(component);

        for (EntrantRequest entrantRequest : model.getEntrantRequestList())
            prepareEntrantRequestDataSource(model, entrantRequest);
    }

    private void prepareEntrantRequestDataSource(final Model model, final EntrantRequest entrantRequest) {

        DynamicListDataSource<RequestedEnrollmentDirection> dataSource = model.getRequestedEnrollmentDirectionDataSourceByRequest().get(entrantRequest);

        // если нет источника данных, или колонка создана (но нахрена такие условия)
        if (dataSource == null || dataSource.getColumn("haveSpecialRights") != null)
            return;

        IEntityHandler entityHandler = entity -> !model.isAccessible();
        ToggleColumn column = (ToggleColumn) new ToggleColumn("Имеет особые права", "haveSpecialRights").setListener("onClickToggle");
        dataSource.addColumn(column.setPermissionKey("haveSpecialRights").setDisableHandler(entityHandler), dataSource.getColumn("originalDocumentHandedIn").getNumber());
    }

    public void onClickToggle(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        ((IDAO) getDao()).updateSpecialRights(model, (Long) component.getListenerParameter());
        ((BusinessComponent) ((ComponentRegion) component.getParentRegion()).getOwner()).refresh();
    }

}
