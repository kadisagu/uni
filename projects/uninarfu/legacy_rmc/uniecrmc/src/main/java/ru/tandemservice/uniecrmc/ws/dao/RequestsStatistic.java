package ru.tandemservice.uniecrmc.ws.dao;

import ru.tandemservice.uniecrmc.ws.dao.WrapperEnrolmentDirectionStatistic.EnrolmentDirectionRow;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;

import java.util.List;

public class RequestsStatistic {
    private int competitionKindCode_1 = 0;
    private int competitionKindCode_2 = 0;
    private int competitionKindCode_3 = 0;
    private int competitionKindCode_5 = 0;

    private int targetMission = 0;
    private int contract = 0;

    private int ministerialplan = 0;
    private int contractplan = 0;


    public int getTotal()
    {
        return competitionKindCode_1 + competitionKindCode_2 + competitionKindCode_3 + competitionKindCode_5 + targetMission;
    }

    public void setCompetitionKindCode_1(int competitionKindCode_1) {
        this.competitionKindCode_1 = competitionKindCode_1;
    }

    public int getCompetitionKindCode_1() {
        return competitionKindCode_1;
    }

    public void setCompetitionKindCode_2(int competitionKindCode_2) {
        this.competitionKindCode_2 = competitionKindCode_2;
    }

    public int getCompetitionKindCode_2() {
        return competitionKindCode_2;
    }

    public void setCompetitionKindCode_3(int competitionKindCode_3) {
        this.competitionKindCode_3 = competitionKindCode_3;
    }

    public int getCompetitionKindCode_3() {
        return competitionKindCode_3;
    }

    public void setCompetitionKindCode_5(int competitionKindCode_5) {
        this.competitionKindCode_5 = competitionKindCode_5;
    }

    public int getCompetitionKindCode_5() {
        return competitionKindCode_5;
    }

    public void setTargetMission(int targetMission) {
        this.targetMission = targetMission;
    }

    public int getTargetMission() {
        return targetMission;
    }

    public void setContract(int contract) {
        this.contract = contract;
    }

    public int getContract() {
        return contract;
    }
    /*
	public void add(RequestedEnrollmentDirection direction) 
	{
		if (direction.isTargetAdmission())
		{
			targetMission++;
			return;
		}
		
		// контракт не делим по видам конкурса
		if (direction.getCompensationType().getCode().equals(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT))
		{
			// по контракту
			contract++;
			return;
		}
		
		// бюджетные виды конкурса (по видам возмещения затрат)
		if (direction.getCompetitionKind().getCode().equals("1"))
			competitionKindCode_1++;
		
		if (direction.getCompetitionKind().getCode().equals("2"))
			competitionKindCode_2++;
		
		if (direction.getCompetitionKind().getCode().equals("3"))
			competitionKindCode_3++;
		
		if (direction.getCompetitionKind().getCode().equals("5"))
			competitionKindCode_5++;
	}
*/

    /**
     * Итоговая статистика
     *
     * @param st
     */
    public void addRequestsStatistic(RequestsStatistic st)
    {
        competitionKindCode_1 += st.getCompetitionKindCode_1();
        competitionKindCode_2 += st.getCompetitionKindCode_2();
        competitionKindCode_3 += st.getCompetitionKindCode_3();
        competitionKindCode_5 += st.getCompetitionKindCode_5();

        targetMission += st.getTargetMission();
        contract += st.getContract();
        ministerialplan += st.getMinisterialplan();
        contractplan += st.getContractplan();
    }

    public void addEnrolmentDirectionStatistic(WrapperEnrolmentDirectionStatistic statisticED)
    {
		/* 
		statisticED.MinisterialPlan;
		statisticED.ContractPlan;
		statisticED.TargetAdmissionPlanBudget;
		statisticED.TargetAdmissionPlanContract;
		*/

        // переносим только итоги
        List<EnrolmentDirectionRow> lst = statisticED.rowStatistic;

        for (EnrolmentDirectionRow row : lst) {

            if (row.IsTarget) {
                targetMission += row.count;
                continue;
            }


            // контракт не делим по видам конкурса
            if (row.compensationType.getCode().equals(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT)) {
                // по контракту
                contract += row.count;
                continue;
            }

            // бюджетные виды конкурса (по видам возмещения затрат)
            if (row.competitionKind.getCode().equals("1"))
                competitionKindCode_1 += row.count;

            if (row.competitionKind.getCode().equals("2"))
                competitionKindCode_2 += row.count;

            if (row.competitionKind.getCode().equals("3"))
                competitionKindCode_3 += row.count;

            if (row.competitionKind.getCode().equals("5"))
                competitionKindCode_5 += row.count;
        }
    }


    public void setMinisterialplan(Integer ministerialplan)
    {
        if (ministerialplan != null)
            this.ministerialplan = ministerialplan.intValue();
    }

    public int getMinisterialplan() {
        return ministerialplan;
    }

    public void setContractplan(Integer contractplan) {
        if (contractplan != null)
            this.contractplan = contractplan.intValue();
    }

    public int getContractplan() {
        return contractplan;
    }


}
