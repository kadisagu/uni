package ru.tandemservice.uniecrmc.dao.entrantRating;

import ru.tandemservice.uni.entity.catalog.CompensationType;

import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

public class CompensationTypeNode {

    @XmlElement(name = "CompensationType")
    public List<RowNode> list = new ArrayList<>();

    public CompensationTypeNode() {
    }

    public void add(CompensationType entity) {
        RowNode node = new RowNode(entity.getCode(), entity.getTitle(), entity.getShortTitle(), null, null, null);
        list.add(node);
    }
}
