package ru.tandemservice.uniecrmc.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;
import ru.tandemservice.uniecrmc.entity.entrant.EntranceDisciplineExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение сущности Вступительное испытание для направления для приема
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntranceDisciplineExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniecrmc.entity.entrant.EntranceDisciplineExt";
    public static final String ENTITY_NAME = "entranceDisciplineExt";
    public static final int VERSION_HASH = 1655376066;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANCE_DISCIPLINE = "entranceDiscipline";
    public static final String P_PRIORITY = "priority";

    private EntranceDiscipline _entranceDiscipline;     // Вступительное испытание для направления для приема
    private int _priority;     // Приоритет

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Вступительное испытание для направления для приема. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EntranceDiscipline getEntranceDiscipline()
    {
        return _entranceDiscipline;
    }

    /**
     * @param entranceDiscipline Вступительное испытание для направления для приема. Свойство не может быть null и должно быть уникальным.
     */
    public void setEntranceDiscipline(EntranceDiscipline entranceDiscipline)
    {
        dirty(_entranceDiscipline, entranceDiscipline);
        _entranceDiscipline = entranceDiscipline;
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет. Свойство не может быть null.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntranceDisciplineExtGen)
        {
            setEntranceDiscipline(((EntranceDisciplineExt)another).getEntranceDiscipline());
            setPriority(((EntranceDisciplineExt)another).getPriority());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntranceDisciplineExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntranceDisciplineExt.class;
        }

        public T newInstance()
        {
            return (T) new EntranceDisciplineExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entranceDiscipline":
                    return obj.getEntranceDiscipline();
                case "priority":
                    return obj.getPriority();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entranceDiscipline":
                    obj.setEntranceDiscipline((EntranceDiscipline) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entranceDiscipline":
                        return true;
                case "priority":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entranceDiscipline":
                    return true;
                case "priority":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entranceDiscipline":
                    return EntranceDiscipline.class;
                case "priority":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntranceDisciplineExt> _dslPath = new Path<EntranceDisciplineExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntranceDisciplineExt");
    }
            

    /**
     * @return Вступительное испытание для направления для приема. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntranceDisciplineExt#getEntranceDiscipline()
     */
    public static EntranceDiscipline.Path<EntranceDiscipline> entranceDiscipline()
    {
        return _dslPath.entranceDiscipline();
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntranceDisciplineExt#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    public static class Path<E extends EntranceDisciplineExt> extends EntityPath<E>
    {
        private EntranceDiscipline.Path<EntranceDiscipline> _entranceDiscipline;
        private PropertyPath<Integer> _priority;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Вступительное испытание для направления для приема. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntranceDisciplineExt#getEntranceDiscipline()
     */
        public EntranceDiscipline.Path<EntranceDiscipline> entranceDiscipline()
        {
            if(_entranceDiscipline == null )
                _entranceDiscipline = new EntranceDiscipline.Path<EntranceDiscipline>(L_ENTRANCE_DISCIPLINE, this);
            return _entranceDiscipline;
        }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntranceDisciplineExt#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(EntranceDisciplineExtGen.P_PRIORITY, this);
            return _priority;
        }

        public Class getEntityClass()
        {
            return EntranceDisciplineExt.class;
        }

        public String getEntityName()
        {
            return "entranceDisciplineExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
