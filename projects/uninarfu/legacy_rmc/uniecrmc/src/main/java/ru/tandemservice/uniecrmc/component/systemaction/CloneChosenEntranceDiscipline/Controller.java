package ru.tandemservice.uniecrmc.component.systemaction.CloneChosenEntranceDiscipline;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.ArrayList;
import java.util.List;

public class Controller
        extends AbstractBusinessController<IDAO, Model>
{
    public static boolean INProcess = false;
    public static String ProcessInfo = "";


    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setSettings(component.getSettings());
        getDao().prepare(model);
    }


    public void onClickApply(IBusinessComponent component)
    {

        if (INProcess) {
            throw new ApplicationException("Процесс исполняет другой пользователь " + ProcessInfo);
        }
        try {
            INProcess = true;
            ProcessInfo = "";

            IDAO dao = (IDAO) getDao();
            Model model = (Model) getModel(component);


            EnrollmentCampaign enrollmentCampaign = model.getSettings().get("enrollmentCampaign");
            Number requestNumberFilter = (Number) model.getSettings().get("regNumber");

            if (requestNumberFilter == null)
                requestNumberFilter = 0;

            if (enrollmentCampaign != null) {
                List<Long> rdIds = dao.getRequestedEnrollmentDirectionList(enrollmentCampaign, requestNumberFilter.intValue());

                int i = 0;
                int size = rdIds.size();

                List<Long> entrantIds = new ArrayList<>();

                for (Long idRD : rdIds) {
                    i++;
                    Long entrantId = dao.processRequestedEnrollmentDirection(idRD);
                    ProcessInfo = "Обработано " + i + " из " + size;

                    if (entrantId != null) {
                        if (!entrantIds.contains(entrantId))
                            entrantIds.add(entrantId);
                    }
                }

                // пересчет пачками
                int iC = 0;
                int total = 0;
                int totalCount = entrantIds.size();

                List<Long> part = new ArrayList<>();

                for (Long idE : entrantIds) {
                    iC++;
                    total++;
                    part.add(idE);
                    if (iC > 20) {
                        iC = 0;
                        dao.recalculatePart(part);
                        part = new ArrayList<>();
                    }

                    ProcessInfo = "Обработка статусов " + total + " из " + totalCount;

                }

                if (part.size() > 0) {
                    dao.recalculatePart(part);
                }
            }
        }
        catch (Exception ex) {
            ProcessInfo += "" + ex.getMessage();
            throw new ApplicationException(ProcessInfo);
        }
        finally {
            INProcess = false;
            ProcessInfo = "";
        }


    }

    public void onRefresh(IBusinessComponent component)
    {
        Model model = getModel(component);
        component.saveSettings();
        getDao().prepare(model);
    }

}
