package ru.tandemservice.uniecrmc.component.settings.DisciplineMinMaxMark;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

public class Controller
        extends ru.tandemservice.uniec.component.settings.DisciplineMinMaxMark.Controller
{

    @SuppressWarnings("rawtypes")
    @Override
    protected void prepareDataSource(IBusinessComponent component)
    {
        Model model = (Model) getModel(component);
        if (model.getDataSource() != null)
            return;

        super.prepareDataSource(component);

        DynamicListDataSource<Discipline2RealizationWayRelation> dataSource = model.getDataSource();
        int columnWidth = 10;
        dataSource.addColumn(new BlockColumn("passMarkContract", "Зачетный балл (Договор)", "passMarkContract").setWidth(columnWidth), 5);

    }

}
