package ru.tandemservice.uniecrmc.component.report.EntrantEduLevelDistribution.Pub;

import org.tandemframework.core.component.State;
import ru.tandemservice.uniecrmc.entity.entrant.EntrantEduLevelDistributionReport;

@State(keys = {"publisherId"}, bindings = {"report.id"})
public class Model {
    private EntrantEduLevelDistributionReport _report = new EntrantEduLevelDistributionReport();

    public EntrantEduLevelDistributionReport getReport() {
        return this._report;
    }

    public void setReport(EntrantEduLevelDistributionReport report) {
        this._report = report;
    }
}

