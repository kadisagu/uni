package ru.tandemservice.uniecrmc.dao.entrantRating;

import ru.tandemservice.uniec.entity.catalog.EnrollmentRecommendation;

import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

public class EnrollmentRecommendationNode {

    @XmlElement(name = "EnrollmentRecommendation")
    public List<RowNode> list = new ArrayList<>();

    public EnrollmentRecommendationNode() {
    }

    public void add(EnrollmentRecommendation entity) {
        RowNode node = new RowNode(entity.getCode(), entity.getTitle(), entity.getShortTitle());
        list.add(node);
    }

    public void addShort(EnrollmentRecommendation entity) {
        RowNode node = new RowNode(entity.getCode(), null, null);
        list.add(node);
    }
}
