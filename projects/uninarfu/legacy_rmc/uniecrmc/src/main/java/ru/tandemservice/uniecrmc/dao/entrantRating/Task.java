package ru.tandemservice.uniecrmc.dao.entrantRating;

import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.uniecrmc.dao.entrantRating.IRatingDao.Params;

import java.io.File;
import java.util.LinkedHashSet;
import java.util.Set;


public class Task implements Runnable {
    private Boolean active;
    private long lastRun;

    private Long enrollmentCampaignId;
    private boolean canRun = false;
    private boolean canRunOnce = false;
    private long interval;//milli
    private String path;

    private static Set<Long> idSet = new LinkedHashSet<>();

    public Task(Long enrollmentCampaignId, int minutes, String path, boolean canRun) {
        synchronized (idSet) {
            if (idSet.contains(enrollmentCampaignId)) {
                throw new ApplicationException("123");
            }
            idSet.add(enrollmentCampaignId);
        }

        this.active = false;
        this.lastRun = System.currentTimeMillis();
        this.enrollmentCampaignId = enrollmentCampaignId;
        setParams(minutes, path, canRun);
    }

    public void run() {
        while (true) {
            try {
                if ((canRun || canRunOnce) && !active && lastRun + interval <= System.currentTimeMillis())
                    process();

                try {
                    Thread.sleep(5000);
                }
                catch (Throwable th) {
                }
            }
            catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    public void runNow() {
        synchronized (active) {
            if (active) return;
            lastRun = System.currentTimeMillis() - interval;
            canRunOnce = true;
        }
    }

    public void process() throws Exception {
        synchronized (active) {
            if (active) return;
            active = true;
            lastRun = System.currentTimeMillis();
        }

        try {
            File resultFile = RatingDao.instance().writeToFile(new Params(this.enrollmentCampaignId), this.path);
        }
        finally {
            canRunOnce = false;
            active = false;
        }
    }

    public boolean isActive() {
        return active;
    }

    public void setParams(int minutes, String path, boolean canRun) {
        this.interval = minutes * 60 * 1000;
        this.path = path;
        this.canRun = canRun;
    }

}
