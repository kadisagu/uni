package ru.tandemservice.uniecrmc.dao;

import org.hibernate.Query;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniecrmc.online.dao.UniecDaoFacade;
import ru.tandemservice.uni.dao.IEntityExportDAO.EntityExporter;
import ru.tandemservice.uniec.dao.UniecExportDao;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.settings.EnrollmentCompetitionKind;
import ru.tandemservice.uniec.entity.settings.Qualification2CompetitionKindRelation;

import java.util.Arrays;
import java.util.Iterator;


public class UniecExportDaoRMC
        extends UniecExportDao
{
    @Override
    public EntityExporter<EnrollmentCampaign> createEnrollmentCampaignExporter()
    {
        //return super.createEnrollmentCampaignExporter();
        return new EntityExporter<EnrollmentCampaign>() {
            @Override
            public Iterator<EnrollmentCampaign> iterate()
            {
                return Arrays.asList(getEnrollmentCampaignForOnline()).iterator();
            }
        };
    }


    private EnrollmentCampaign getEnrollmentCampaignForOnline()
    {
        return UniecDaoFacade.getOnlineEntrantRegistrationRmcDAO().getEnrollmentCampaignForOnline();
    }

    @Override
    public EntityExporter<Qualification2CompetitionKindRelation> createQualification2CompetitionKindRelationExporter()
    {
        return new EntityExporter<Qualification2CompetitionKindRelation>() {
            @SuppressWarnings("unchecked")
            @Override
            public Iterator<Qualification2CompetitionKindRelation> iterate()
            {
                Query query = getSession().createQuery("from " + Qualification2CompetitionKindRelation.ENTITY_CLASS + " where " + Qualification2CompetitionKindRelation.enrollmentCampaign().s() + " = :enrollmentCampaign");
                query.setParameter("enrollmentCampaign", getEnrollmentCampaignForOnline());
                return query.iterate();
            }
        };
    }

    @Override
    public EntityExporter<EnrollmentCompetitionKind> createEnrollmentCompetitionKindExporter()
    {
        return new EntityExporter<EnrollmentCompetitionKind>() {
            @SuppressWarnings("unchecked")
            @Override
            public Iterator<EnrollmentCompetitionKind> iterate()
            {
                Query query = getSession().createQuery("from " + EnrollmentCompetitionKind.ENTITY_CLASS + " where " + EnrollmentCompetitionKind.competitionKind().s() + " is not null and used = :used and " + EnrollmentCompetitionKind.enrollmentCampaign().s() + " = :enrollmentCampaign");
                query.setParameter("enrollmentCampaign", getEnrollmentCampaignForOnline());
                query.setParameter("used", true);
                return query.iterate();
            }
        };
    }


    @Override
    public EntityExporter<EnrollmentDirection> createEnrollmentDirectionExporter()
    {
        return new EntityExporter<EnrollmentDirection>() {
            @Override
            public Iterator<EnrollmentDirection> iterate()
            {
                return new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "e").column("e")
                        .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.enrollmentCampaign().fromAlias("e")), DQLExpressions.value(getEnrollmentCampaignForOnline())))
                        .where(DQLExpressions.or(
                                DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.budget().fromAlias("e")), DQLExpressions.value(Boolean.TRUE)),
                                DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.contract().fromAlias("e")), DQLExpressions.value(Boolean.TRUE))
                        )).createStatement(getSession()).iterate();
            }
        };
    }


}
