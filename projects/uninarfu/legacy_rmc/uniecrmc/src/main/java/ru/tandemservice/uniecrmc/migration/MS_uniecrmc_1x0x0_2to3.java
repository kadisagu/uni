package ru.tandemservice.uniecrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniecrmc_1x0x0_2to3 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception {
        if (!tool.tableExists("entrantrecommendedsettings_t"))
            return;
        if (tool.columnExists("entrantrecommendedsettings_t", "usedemon_p"))
            return;

        tool.createColumn("entrantrecommendedsettings_t", new DBColumn("usedemon_p", DBType.BOOLEAN));
        tool.createColumn("entrantrecommendedsettings_t", new DBColumn("interval_p", DBType.INTEGER));
        tool.createColumn("entrantrecommendedsettings_t", new DBColumn("path_p", DBType.createVarchar(255)));

        tool.executeUpdate("update entrantrecommendedsettings_t set usedemon_p=? where usedemon_p is null", false);
        tool.setColumnNullable("entrantrecommendedsettings_t", "usedemon_p", false);

        tool.executeUpdate("update entrantrecommendedsettings_t set interval_p=? where interval_p is null", 60);
        tool.setColumnNullable("entrantrecommendedsettings_t", "interval_p", false);
    }

}