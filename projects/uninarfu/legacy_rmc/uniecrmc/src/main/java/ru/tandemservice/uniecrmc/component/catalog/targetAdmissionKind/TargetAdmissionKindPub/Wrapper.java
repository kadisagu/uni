package ru.tandemservice.uniecrmc.component.catalog.targetAdmissionKind.TargetAdmissionKindPub;

import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;

public class Wrapper extends EntityBase implements IHierarchyItem {

    private TargetAdmissionKind base;
    private String contractNumber;
    private Wrapper parent;

    @Override
    public Long getId() {
        return base.getId();
    }

    @Override
    public IHierarchyItem getHierarhyParent() {
        return parent;
    }

    public void setParent(Wrapper parent) {
        this.parent = parent;
    }

    public void setBase(TargetAdmissionKind _base)
    {
        this.base = _base;
    }

    public TargetAdmissionKind getBase()
    {
        return this.base;
    }

    public String getContractNumber()
    {
        return this.contractNumber;
    }

    public void setContractNumber(String _contractNumber)
    {
        this.contractNumber = _contractNumber;
    }

}
