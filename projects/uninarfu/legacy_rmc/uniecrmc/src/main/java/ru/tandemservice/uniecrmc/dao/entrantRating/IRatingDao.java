package ru.tandemservice.uniecrmc.dao.entrantRating;

import org.tandemframework.core.exception.ApplicationException;

import java.io.File;


public interface IRatingDao {
    public String writeToString(IRatingDao.Params params) throws Exception;

    public File writeToFile(IRatingDao.Params params, String filename) throws Exception;

    public EnrollmentCampaignNode getRootNode(IRatingDao.Params params);


    public static class Params {
        private Long enrolmentCampaignId;
        private Long formativeOrgUnitId;
        private String developFormId;
        private Long enrollmentDirectionId;

        public Params(Long enrolmentCampaignId) {
            if (enrolmentCampaignId == null || enrolmentCampaignId < 1)
                throw new ApplicationException("enrolmentCampaignId не может быть пусто");

            this.enrolmentCampaignId = enrolmentCampaignId;
        }

        public Params(Long enrolmentCompaignId, Long formativeOrgUnitId, String developFormId, Long enrollmentDirectionId) {
            this(enrolmentCompaignId);

            this.formativeOrgUnitId = formativeOrgUnitId;
            this.developFormId = developFormId;
            this.enrollmentDirectionId = enrollmentDirectionId;
        }

        public Long getEnrolmentCampaignId() {
            return enrolmentCampaignId;
        }

        public void setEnrolmentCampaignId(Long enrolmentCampaignId) {
            this.enrolmentCampaignId = enrolmentCampaignId;
        }

        public Long getFormativeOrgUnitId() {
            return formativeOrgUnitId;
        }

        public void setFormativeOrgUnitId(Long formativeOrgUnitId) {
            this.formativeOrgUnitId = formativeOrgUnitId;
        }

        public String getDevelopFormId() {
            return developFormId;
        }

        public void setDevelopFormId(String developFormId) {
            this.developFormId = developFormId;
        }

        public Long getEnrollmentDirectionId() {
            return enrollmentDirectionId;
        }

        public void setEnrollmentDirectionId(Long enrollmentDirectionId) {
            this.enrollmentDirectionId = enrollmentDirectionId;
        }


    }
}
