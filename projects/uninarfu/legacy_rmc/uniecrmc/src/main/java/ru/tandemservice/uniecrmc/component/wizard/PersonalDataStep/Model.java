package ru.tandemservice.uniecrmc.component.wizard.PersonalDataStep;


public class Model extends ru.tandemservice.uniec.component.wizard.PersonalDataStep.Model {
    boolean hasBasicLanguage;

    public boolean isHasBasicLanguage() {
        return hasBasicLanguage;
    }

    public void setHasBasicLanguage(boolean hasBasicLanguage) {
        this.hasBasicLanguage = hasBasicLanguage;
    }
}
