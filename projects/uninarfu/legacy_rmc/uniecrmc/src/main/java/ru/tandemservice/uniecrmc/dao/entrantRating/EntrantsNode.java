package ru.tandemservice.uniecrmc.dao.entrantRating;

import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

public class EntrantsNode {

    @XmlElement(name = "Entrant")
    public List<EntrantNode> entrantList = new ArrayList<>();

    public EntrantsNode() {
    }
}
