package ru.tandemservice.uniecrmc.dao.entrantRating;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.person.catalog.entity.Benefit;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.IEcgCortegeRMC;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.WrapChoseEntranseDiscipline;
import ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EnrollmentRecommendation;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.orders.AbstractEntrantExtract;

import java.util.ArrayList;
import java.util.List;

public class EcgCortegeRMC implements IEcgCortegeRMC, Cloneable {

    protected RequestedEnrollmentDirection red;
    protected RequestedEnrollmentDirectionExt redExt;
    protected Double certAvgMark;
    protected boolean haveSpecialRights = false;
    protected boolean targetAdmission = false;
    protected CompetitionKind competitionKind;

    protected List<WrapChoseEntranseDiscipline> discList = new ArrayList<>();
    protected List<EnrollmentRecommendation> recommendList = new ArrayList<>();
    protected List<Benefit> benefitList = new ArrayList<>();

    protected boolean enrolled = false;
    protected Boolean paid;
    protected AbstractEntrantExtract extract;
    private Integer enrollmentPriority;

    public EcgCortegeRMC(RequestedEnrollmentDirection red, RequestedEnrollmentDirectionExt redExt, Double certAvgMark, AbstractEntrantExtract extract) {
        this.red = red;
        this.redExt = redExt;
        this.certAvgMark = certAvgMark;
        this.haveSpecialRights = redExt != null && redExt.isHaveSpecialRights();
        this.targetAdmission = red.isTargetAdmission();
        this.competitionKind = red.getCompetitionKind();
        this.extract = extract;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof EcgCortegeRMC))
            return false;

        EcgCortegeRMC another = (EcgCortegeRMC) obj;

        if (!this.red.equals(another.red))
            return false;
        if (this.targetAdmission != another.targetAdmission)
            return false;
        if (this.haveSpecialRights != another.haveSpecialRights)
            return false;
        if (!this.competitionKind.equals(another.competitionKind))
            return false;

        return true;
    }

    @Override
    public EcgCortegeRMC clone() {
        EcgCortegeRMC another = new EcgCortegeRMC(red, redExt, certAvgMark, this.extract);

        another.discList = new ArrayList<>(this.discList);
        another.recommendList = new ArrayList<>(this.recommendList);
        another.benefitList = new ArrayList<>(this.benefitList);
        another.haveSpecialRights = this.haveSpecialRights;
        another.targetAdmission = this.targetAdmission;
        another.competitionKind = this.competitionKind;
        another.paid = this.paid;

        return another;
    }

    public boolean isEnrolled() {
        return this.enrolled;
    }

    public void setEnrolled(boolean enrolled) {
        this.enrolled = enrolled;
    }

    public void setCommon() {
        this.targetAdmission = false;
        this.haveSpecialRights = false;
        this.enrolled = false;
        this.competitionKind = IUniBaseDao.instance.get().getCatalogItem(CompetitionKind.class, UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION);
    }

    public void addDisciplines(List<WrapChoseEntranseDiscipline> list) {
        this.discList.addAll(list);
    }

    public void addRecommendations(List<EnrollmentRecommendation> list) {
        this.recommendList.addAll(list);
    }

    public void addBenefit(Benefit benefit) {
        if (!this.benefitList.contains(benefit))
            this.benefitList.add(benefit);
    }

    public List<Benefit> getBenefits() {
        return this.benefitList;
    }

    public RequestedEnrollmentDirectionExt getExt() {
        return this.redExt;
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public IEntity getEntity() {
        return red;
    }

    @Override
    public boolean isTargetAdmission() {
        return this.targetAdmission;
    }

    @Override
    public TargetAdmissionKind getTargetAdmissionKind() {
        return isTargetAdmission() ? red.getTargetAdmissionKind() : null;
    }

    @Override
    public CompetitionKind getCompetitionKind() {
        return this.competitionKind;
    }

    private Double _finalMark = null;

    @Override
    public Double getFinalMark() {
        if (_finalMark == null) {
            _finalMark = 0.0;
            for (WrapChoseEntranseDiscipline discipline : this.discList)
                _finalMark += discipline.getFinalMark() != null ? discipline.getFinalMark() : 0.0;
        }

        return _finalMark;
    }

    @Override
    public Double getProfileMark() {
        return null;
    }

    @Override
    public Boolean isGraduatedProfileEduInstitution() {
        return null;
    }

    @Override
    public Double getCertificateAverageMark() {
        return this.certAvgMark;
    }

    @Override
    public String getFio() {
        return red.getEntrantRequest().getEntrant().getFullFio();
    }

    @Override
    public List<EnrollmentRecommendation> getRecommendations() {
        return this.recommendList;
    }

    @Override
    public List<WrapChoseEntranseDiscipline> getDisciplines() {
        return this.discList;
    }

    @Override
    public boolean isHaveSpecialRights() {
        return this.haveSpecialRights;
    }

    @Override
    public String getDisciplinesHTMLDescription() {
        return null;
    }

    public Boolean getPaid() {
        return paid;
    }

    public void setPaid(Boolean paid) {
        this.paid = paid;
    }

    public AbstractEntrantExtract getExtract() {
        return extract;
    }

    public void setExtract(AbstractEntrantExtract extract) {
        this.extract = extract;
    }

    @Override
    public String getEntrantRequestsInfoHtml()
    {
        return "";
    }

    @Override
    public Integer getEnrollmentPriority() {
        return enrollmentPriority;
    }

    public void setEnrollmentPriority(Integer enrollmentPriority) {
        this.enrollmentPriority = enrollmentPriority;
    }


}
