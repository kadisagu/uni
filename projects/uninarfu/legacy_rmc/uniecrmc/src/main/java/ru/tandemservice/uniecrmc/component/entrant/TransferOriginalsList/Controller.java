package ru.tandemservice.uniecrmc.component.entrant.TransferOriginalsList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniecrmc.dao.moveoriginal.DAOMoveOriginal;
import ru.tandemservice.uniecrmc.dao.moveoriginal.IDAOMoveOriginal;
import ru.tandemservice.uniecrmc.dao.moveoriginal.WrapEntrantRequest;
import ru.tandemservice.uniecrmc.dao.moveoriginal.WrapRequestedEnrollmentDirection;
import ru.tandemservice.uniecrmc.entity.entrant.ChangeOriginalDocumentRED;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    private static boolean ISRUN = false;

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        model.setSettings(component.getSettings());

        getDao().prepare(model);
        prepareDataSource(component);
    }

    protected void prepareDataSource(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<ChangeOriginalDocumentRED> dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(new CheckboxColumn("check"));

        dataSource.addColumn(new PublisherLinkColumn("Заявление абитуриента", ChangeOriginalDocumentRED.entrantRequest())
                                     .setResolver(new IPublisherLinkResolver() {
                                         public Object getParameters(IEntity ientity) {
                                             ChangeOriginalDocumentRED obj = (ChangeOriginalDocumentRED) ientity;
                                             return new ParametersMap()
                                                     .add(PublisherActivator.PUBLISHER_ID_KEY, obj.getEntrantRequest().getEntrant().getId())
                                                     .add("selectedTab", "entrantTab")
                                                     .add("selectedDataTab", "entrantRequestTab");
                                         }

                                         public String getComponentName(IEntity ientity) {
                                             return ru.tandemservice.uniec.component.entrant.EntrantPub.Model.class.getPackage().getName();
                                         }
                                     })
                                     .setFormatter(source -> {
                                         final EntrantRequest entrantRequest = (EntrantRequest) source;
                                         return new StringBuilder()
                                                 .append(entrantRequest.getStringNumber())
                                                 .append(" ")
                                                 .append(entrantRequest.getEntrant().getFullFio())
                                                 .toString();
                                     })
                                     .setOrderable(true)
        );
        //dataSource.addColumn(new SimpleColumn("Дата внесения измений", ChangeOriginalDocumentRED.dateChange()).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Дата внесения измений", ChangeOriginalDocumentRED.dateChange(), DateFormatter.DATE_FORMATTER_WITH_TIME).setOrderable(true));

        dataSource.addColumn(new SimpleColumn("Старое НП", ChangeOriginalDocumentRED.oldOrigin().enrollmentDirection().title()).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Новое НП", ChangeOriginalDocumentRED.newOrigin().enrollmentDirection().title()).setOrderable(true));

        dataSource.addColumn(new SimpleColumn("Примечание", ChangeOriginalDocumentRED.comment()).setOrderable(false));


        dataSource.addColumn(new ActionColumn("Отменить перенос", ActionColumn.DELETE, "onClickCancelTransfer", "Отменить перенос?", new Object[0]).setPermissionKey("uniecrmcTransferOriginalsCancel"));
        model.setDataSource(dataSource);
    }

    public void onClickTransferAll(IBusinessComponent component)
    {

        Model model = (Model) getModel(component);
        EnrollmentCampaign campaign = model.getEnrollmentCampaign();

		/*
		getDao().transferOriginal(ec);
		//DAOMoveOriginal.instance().moveOriginal(ec);
		*/

        IDAOMoveOriginal idao = DAOMoveOriginal.instance();

        // исполняем в транзакции
        if (ISRUN)
            throw new ApplicationException("Операция исполняется другим пользователем " + DAOMoveOriginal.LOG);

        try {
            ISRUN = true;

            List<WrapEntrantRequest> lstRequest = idao.doCalculate(campaign);

            int size = lstRequest.size();
            int pos = 0;

            List<Long> entrantIds = new ArrayList<>();

            // расчеты завершены, теперь само двиганье оригиналов
            for (WrapEntrantRequest wrap : lstRequest) {
                pos++;
                DAOMoveOriginal.LOG = "Всего " + size + " Обработано " + pos;

                List<WrapRequestedEnrollmentDirection> lstRED = idao.getWrapRequestedEnrollmentDirection(wrap.getEntrantRequestId());
                // если в заявлении есть НП с финальными статусами, то такое обрабатывать не надо
                if (idao.hasState(lstRED, DAOMoveOriginal.getEndEntrantState()))
                    continue;

                boolean needChange = false;
                boolean hasSet = false;
                for (WrapRequestedEnrollmentDirection red : lstRED) {
                    if (red.isOriginalEtStart() != red.isOriginal())
                        // начальное и конечное значение признака оригинала не совпало
                        // двигаем
                        needChange = true;

                    if (red.isOriginHasSet())
                        hasSet = true;
                }

                if (needChange && hasSet) {
                    Long entrantId = idao.moveOriginalForRequest(lstRED);
                    if (!entrantIds.contains(entrantId))
                        entrantIds.add(entrantId);
                }
            }


            // пересчет пачками
            int iC = 0;
            int total = 0;
            int totalCount = entrantIds.size();

            List<Long> part = new ArrayList<>();

            for (Long idE : entrantIds) {
                iC++;
                total++;
                part.add(idE);
                if (iC > 20) {
                    iC = 0;
                    idao.recalculatePart(part);
                    part = new ArrayList<>();
                }

                DAOMoveOriginal.LOG = "Обработка статусов " + total + " из " + totalCount;

            }

            if (part.size() > 0) {
                idao.recalculatePart(part);
            }


        }
        finally {
            ISRUN = false;
        }


    }

    public void onClickCancelTransfer(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        Long id = component.getListenerParameter();

        for (ChangeOriginalDocumentRED entity : model.getDataSource().getEntityList())
            if (entity.getId().equals(id)) {
                getDao().cancelTransfer(Arrays.asList(entity));
                break;
            }
    }

    public void onClickCancelTransferAll(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        CheckboxColumn column = (CheckboxColumn) model.getDataSource().getColumn("check");
        getDao().cancelTransfer((Collection) column.getSelectedObjects());
    }

    public void onClickSearch(IBusinessComponent component) {
        component.saveSettings();
        ((Model) getModel(component)).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        model.getSettings().clear();
        ((IDAO) getDao()).prepare(model);
        onClickSearch(component);
    }
}
