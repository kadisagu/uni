package ru.tandemservice.uniecrmc.component.wizard.NextOfKinStep;

import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import org.tandemframework.shared.person.catalog.entity.RelationDegree;
import ru.tandemservice.uniecrmc.online.entity.OnlineEntrantExt;
import ru.tandemservice.uniec.component.wizard.NextOfKinStep.Model;

public class DAO extends ru.tandemservice.uniec.component.wizard.NextOfKinStep.DAO {

    @Override
    public void prepare(Model model) {
        super.prepare(model);

        Long onlineEntrantId = model.getOnlineEntrant().getId();
        if (onlineEntrantId == null) {
            return;
        }

        OnlineEntrantExt onlineEntrantExt = get(OnlineEntrantExt.class, OnlineEntrantExt.entrant().id().s(), onlineEntrantId);
        if (onlineEntrantExt == null) {
            return;
        }

        prepareOnlineEntrantExtNextOfKin(model, onlineEntrantExt);
    }

    private void prepareOnlineEntrantExtNextOfKin(Model model, OnlineEntrantExt onlineEntrantExt) {
        PersonNextOfKin personNextOfKin = model.getKinModel().getNextOfKin();

        RelationDegree relationDegree = onlineEntrantExt.getNextOfKinRelationDegree();
        if (relationDegree == null) {
            return;
        }
        personNextOfKin.setRelationDegree(relationDegree);
        personNextOfKin.setLastname(onlineEntrantExt.getNextOfKinLastName());
        personNextOfKin.setFirstname(onlineEntrantExt.getNextOfKinFirstName());
        personNextOfKin.setPatronymic(onlineEntrantExt.getNextOfKinMiddleName());
        personNextOfKin.setEmploymentPlace(onlineEntrantExt.getNextOfKinWorkPlace());
        personNextOfKin.setPost(onlineEntrantExt.getNextOfKinWorkPost());
        personNextOfKin.setPhones(onlineEntrantExt.getNextOfKinPhone());

        prepareSelectNextOfKin(model);
    }
}
