package ru.tandemservice.uniecrmc.component.menu.EntrantRatingSettings;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {
    void changeUse(Long id);

    void saveChanges(Model model, Long id);

    void runNow(Long id);
}
