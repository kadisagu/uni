package ru.tandemservice.uniecrmc.base.ext.EcProfileDistribution.util;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.IEcgCortegeRMC;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.WrapChoseEntranseDiscipline;
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.util.EcgpRecommendedDTO;
import ru.tandemservice.uniec.base.entity.ecgp.EcgpEntrantRecommended;
import ru.tandemservice.uniec.entity.catalog.EnrollmentRecommendation;

import java.util.List;

public class EcgpRecommendedDTORMC extends EcgpRecommendedDTO implements IEcgCortegeRMC {

    private static final long serialVersionUID = 1L;

    private List<EnrollmentRecommendation> recommendations;
    private List<WrapChoseEntranseDiscipline> disciplines;
    private boolean haveSpecialRights;
    private Integer enrollmentPriority;

    public EcgpRecommendedDTORMC(EcgpEntrantRecommended entrantRecommended,
                                 Double finalMark, String priorities, String prioritiesShort,
                                 List<EnrollmentRecommendation> recommendations,
                                 List<WrapChoseEntranseDiscipline> disciplines,
                                 Boolean haveSpecialRights, Integer enrollmentPriority)
    {
        super(entrantRecommended, finalMark, priorities, prioritiesShort);
        this.recommendations = recommendations;
        this.disciplines = disciplines;
        this.haveSpecialRights = haveSpecialRights != null && haveSpecialRights.booleanValue();
        this.enrollmentPriority = enrollmentPriority;
    }

    public List<EnrollmentRecommendation> getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(List<EnrollmentRecommendation> recommendations) {
        this.recommendations = recommendations;
    }

    public List<WrapChoseEntranseDiscipline> getDisciplines() {
        return disciplines;
    }

    public void setDisciplines(List<WrapChoseEntranseDiscipline> disciplines) {
        this.disciplines = disciplines;
    }

    public boolean isHaveSpecialRights() {
        return haveSpecialRights;
    }

    public void setHaveSpecialRights(boolean haveSpecialRights) {
        this.haveSpecialRights = haveSpecialRights;
    }

    @Override
    public String getCompetitionKindTitle() {
        return haveSpecialRights ? new StringBuilder("Вне конкурса").append("5".equals(this.getCompetitionKind().getCode()) ? "" : new StringBuilder().append(", ").append(this.getCompetitionKind().getTitle()).toString()).toString() : super.getCompetitionKindTitle();
    }

    public String getRecommendationTitle() {
        if (this.getRecommendations() != null) {
            List<String> lst = CommonBaseUtil.getPropertiesList(this.getRecommendations(), EnrollmentRecommendation.shortTitle());
            return StringUtils.join(lst, ", ");
        }
        return "";
    }

    public String getDisciplinesHTMLDescription() {
        if (this.getDisciplines() != null) {
            StringBuilder str = new StringBuilder("<table cellspacing='0' cellpadding='0'>");
            for (WrapChoseEntranseDiscipline discipline : this.getDisciplines()) {
                str.append("<tr>")
                        .append("<td>")
                                //.append(discipline.getEnrollmentCampaignDiscipline().getEducationSubject().getTitle())
                        .append(discipline.getTitle())
                        .append(" ")
                        .append(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(discipline.getFinalMark()))
                        .append("</td>")
                        .append("</tr>");
            }
            str.append("</table>");
            return str.toString();
        }
        return "";
    }

    @Override
    public String getEntrantRequestsInfoHtml()
    {
        return "";
    }

    @Override
    public Integer getEnrollmentPriority() {
        return enrollmentPriority;
    }


}
