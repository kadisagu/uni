package ru.tandemservice.uniecrmc.component.systemaction.ChangeOlimpiadMark;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.List;

// олимпиады
public class Model
        implements IEnrollmentCampaignSelectModel
{
    public static final long OLYMPIAD_DIPLOMA_FOR_DIRECTION = 0;     // За дисциплины по одному выбранному направлению приема
    public static final long OLYMPIAD_DIPLOMA_FOR_ALL_DIRECTIONS = 1;// За дисциплины по всем выбранным направлениям приема

    private IDataSettings _settings;
    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private List<IdentifiableWrapper> olympiadDiplomaRuleList;

    private IdentifiableWrapper olympiadDiplomaRule;

    @Override
    public EnrollmentCampaign getEnrollmentCampaign() {
        return (EnrollmentCampaign) getSettings().get(ENROLLMENT_CAMPAIGN_FILTER_NAME);
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList() {
        return _enrollmentCampaignList;
    }

    @Override
    public IDataSettings getSettings() {

        return _settings;
    }

    public void setSettings(IDataSettings settings) {
        _settings = settings;
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign) {
        getSettings().set(ENROLLMENT_CAMPAIGN_FILTER_NAME, enrollmentCampaign);

    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList) {
        _enrollmentCampaignList = enrollmentCampaignList;

    }

    public List<IdentifiableWrapper> getOlympiadDiplomaRuleList() {
        return olympiadDiplomaRuleList;
    }

    public void setOlympiadDiplomaRuleList(List<IdentifiableWrapper> olympiadDiplomaRuleList) {
        this.olympiadDiplomaRuleList = olympiadDiplomaRuleList;
    }

    public IdentifiableWrapper getOlympiadDiplomaRule() {
        return olympiadDiplomaRule;
    }

    public void setOlympiadDiplomaRule(IdentifiableWrapper olympiadDiplomaRule) {
        this.olympiadDiplomaRule = olympiadDiplomaRule;
    }


}
