package ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util;

public class WrapChoseEntranseDiscipline {

    private Long id;
    private Long enrollmentCampaignDisciplineId;
    private Double finalMark;
    private String title;


    public WrapChoseEntranseDiscipline
            (
                    Long id
                    , Long enrollmentCampaignDisciplineId
                    , Double finalMark
                    , String title
            )
    {
        this.id = id;
        this.finalMark = finalMark;
        this.title = title;
        this.enrollmentCampaignDisciplineId = enrollmentCampaignDisciplineId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getFinalMark() {
        return finalMark;
    }

    public void setFinalMark(Double finalMark) {
        this.finalMark = finalMark;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getEnrollmentCampaignDisciplineId() {
        return enrollmentCampaignDisciplineId;
    }

    public void setEnrollmentCampaignDisciplineId(
            Long enrollmentCampaignDisciplineId)
    {
        this.enrollmentCampaignDisciplineId = enrollmentCampaignDisciplineId;
    }

}
