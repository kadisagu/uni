package ru.tandemservice.uniecrmc.component.catalog.targetAdmissionKind.TargetAdmissionKindPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.TitledFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;


public class Controller extends DefaultCatalogPubController {
    @Override
    protected DynamicListDataSource createListDataSource(IBusinessComponent context)
    {
        final Model model = (Model) getModel(context);

        String publisherItem = "ru.tandemservice.uniec.component.catalog.targetAdmissionKind.TargetAdmissionKindItemPub";

        final String componentName = publisherItem;

        DynamicListDataSource dataSource = new DynamicListDataSource<Wrapper>(context, component -> {
            ((IDAO) getDao()).prepareProjectsDataSource(model);

        });

        PublisherLinkColumn col = new PublisherLinkColumn("Название", "base");
        col.setClickable(true).setTreeColumn(true);
        col.setFormatter(TitledFormatter.INSTANCE);
        dataSource.addColumn(col);

        col.setResolver(new IPublisherLinkResolver() {

            @Override
            public Object getParameters(IEntity arg0) {
                return arg0.getId();
            }

            @Override
            public String getComponentName(IEntity arg0) {
                return componentName;
            }
        });
        dataSource.addColumn((new SimpleColumn("Сокращенное название", "base.shortTitle")).setClickable(false));
        dataSource.addColumn((new SimpleColumn("№ договора", "contractNumber")).setClickable(false));
        dataSource.addColumn((new ActionColumn("Редактировать", "edit", "onClickEditItem")).setPermissionKey(model.getCatalogItemEdit()));

        if (model.isUserCatalog())
            dataSource.addColumn(new ActionColumn("Удалить", "delete", "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", new Object[]{"base.title"}).setPermissionKey(model.getCatalogItemDelete()));

        model.setDataSource(dataSource);
        return dataSource;
    }

}