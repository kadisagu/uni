package ru.tandemservice.uniecrmc.component.systemaction.CloneChosenEntranceDiscipline;


import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.List;

public interface IDAO
        extends IUniDao<Model>
{


    public void iniData();

    public List<Long> getRequestedEnrollmentDirectionList
            (
                    EnrollmentCampaign enrollmentCampaign
                    , int regNumber
            );

    @Transactional
    public Long processRequestedEnrollmentDirection
            (
                    Long requestedEnrollmentDirectionId
            );

    @Transactional
    public void recalculatePart(List<Long> part);

}
