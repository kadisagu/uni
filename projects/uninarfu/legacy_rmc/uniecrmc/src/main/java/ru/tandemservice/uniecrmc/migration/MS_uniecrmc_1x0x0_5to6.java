package ru.tandemservice.uniecrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniecrmc_1x0x0_5to6 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.5.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.5.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность entrant2SpecialConditions

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("entrant2specialconditions_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("entrant_id", DBType.LONG).setNullable(false),
                                      new DBColumn("specialconditionsforentrant_id", DBType.LONG)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("entrant2SpecialConditions");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность entrantExt

        // удалено свойство specialConditionsForEntrant
        {
            // TODO: программист должен подтвердить это действие
            //		if( true )
            //			throw new UnsupportedOperationException("Confirm me: delete column");

            // удалить колонку
            tool.dropColumn("entrantext_t", "specialconditionsforentrant_id");

        }


    }
}