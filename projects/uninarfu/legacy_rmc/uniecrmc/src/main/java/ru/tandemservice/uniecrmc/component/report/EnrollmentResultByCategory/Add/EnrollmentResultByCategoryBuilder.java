package ru.tandemservice.uniecrmc.component.report.EnrollmentResultByCategory.Add;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import org.hibernate.Session;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.catalog.codes.EntrantStateCodes;
import ru.tandemservice.uniec.entity.entrant.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.lang.Boolean;
import java.util.*;

public class EnrollmentResultByCategoryBuilder {

    private Model model;
    private Session session;
    private IUniBaseDao dao;

    private final static Integer TABLE_COLUMN_COUNT = 17;
    private final static Integer TABLE_ROW_COUNT = 13;

    private WritableCellFormat cellFormat;

    public EnrollmentResultByCategoryBuilder(Model model, Session session) {
        this.model = model;
        this.session = session;
        this.dao = IUniBaseDao.instance.get();
    }

    public DatabaseFile getContent() {
        DatabaseFile content = new DatabaseFile();
        try {
            content.setContent(getPrintedContent());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return content;
    }

    private byte[] getPrintedContent() throws Exception {

        UniecScriptItem scriptItem = dao.getCatalogItem(UniecScriptItem.class, "enrollmentResultByCategoryReport");

        ByteArrayInputStream in = new ByteArrayInputStream(scriptItem.getCurrentTemplate());
        Workbook tempalteWorkbook = Workbook.getWorkbook(in);

        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);

        ByteArrayOutputStream workbookStream = new ByteArrayOutputStream();
        WritableWorkbook workbook = Workbook.createWorkbook(workbookStream, tempalteWorkbook, ws);
        WritableSheet sheet = workbook.getSheet(0);

        initFormats();

        List<Object[]> reportData = getPreliminaryData();

        List<RequestedEnrollmentDirection> directionData = getRequestedEnrollmentDirectionBuilderDQL().createStatement(session).list();
        Map<Long, Boolean> haveSpecialRightsMap = getHaveSpecialRightsMap();
        Map<Entrant, Boolean> haveEnrollmentRecommendationMap = getHaveEnrollmentRecommendationMap(directionData);
        Map<Entrant, Boolean> haveOlympiadDiplomaMap = getHaveOlympiadDiplomaMap(directionData);

        Map<Long, List<ChosenEntranceDiscipline>> chosenEntranceDisciplineMap = getChosenEntranceDisciplineMap();

        // данные квалификации для определения высшей формы получения образования
        List<String> highQualificationCodes = Arrays.asList(QualificationsCodes.BAKALAVR, QualificationsCodes.SPETSIALIST, QualificationsCodes.MAGISTR);

        // квалификации СПО
        List<String> middleQualificationCodes = Arrays.asList(QualificationsCodes.BAZOVYY_UROVEN_S_P_O, QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O);

        List<String> excludeQualificationCodes = Arrays.asList(QualificationsCodes.N_P_O_2, QualificationsCodes.N_P_O_3, QualificationsCodes.N_P_O_4,
                                                               QualificationsCodes.BAZOVYY_UROVEN_S_P_O, QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O);

        ReportRow[] dataTable = new ReportRow[TABLE_ROW_COUNT];
        for (int i = 0; i < TABLE_ROW_COUNT; i++) {
            dataTable[i] = new ReportRow(i);
        }

        for (RequestedEnrollmentDirection requestedEnrollmentDirection : directionData) {
            Qualifications qualification = requestedEnrollmentDirection.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getQualification();
            CompensationType compensationType = requestedEnrollmentDirection.getCompensationType();
            StudentCategory studentCategory = requestedEnrollmentDirection.getStudentCategory();
            CompetitionKind competitionKind = requestedEnrollmentDirection.getCompetitionKind();
            Entrant entrant = requestedEnrollmentDirection.getEntrantRequest().getEntrant();

            // соответсвующей квалификации
            fillReportRow(qualification, compensationType, dataTable[0], true);

            //  с категорией "Студент"
            if (StudentCategoryCodes.STUDENT_CATEGORY_STUDENT.equals(studentCategory.getCode())) {
                fillReportRow(qualification, compensationType, dataTable[1], true);
            }

            // только по результатам ЕГЭ
            if (isEge(chosenEntranceDisciplineMap.get(requestedEnrollmentDirection.getId()))) {
                fillReportRow(qualification, compensationType, dataTable[2], true);
            }

            // по результатам ЕГЭ и дополнительных вступительных испытаний
            if (isEntranceExamOrEge(chosenEntranceDisciplineMap.get(requestedEnrollmentDirection.getId()))) {
                fillReportRow(qualification, compensationType, dataTable[3], true);
            }

            // по результатам вступительных испытаний, проводимых вузом
            if (isEntranceExam(chosenEntranceDisciplineMap.get(requestedEnrollmentDirection.getId()))) {
                fillReportRow(qualification, compensationType, dataTable[4], true);
            }

            // Для квалификаций бакалавр, специалист, магистр - направления подготовки в заявлениях с видом конкурса "на общих сонованиях",
            // Целевой прием и особые права - false,
            // преимуществ при равенстве средних баллов аттестата нет
            if (UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION.equals(competitionKind.getCode()) &&
                    highQualificationCodes.contains(qualification.getCode()) &&
                    !haveSpecialRightsMap.containsKey(requestedEnrollmentDirection.getId()) &&
                    !requestedEnrollmentDirection.isTargetAdmission() &&
                    !haveEnrollmentRecommendationMap.containsKey(entrant))
            {
                fillReportRow(qualification, compensationType, dataTable[5], true);
            }

            // Направления подготовки в заявлениях с видом конкурса Без ВИ,
            // для всех квалификаций кроме Базового и повышенного уровней СПО и профессий и специальностей НПО
            if (UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES.equals(competitionKind.getCode())
                    && !excludeQualificationCodes.contains(qualification.getCode()))
            {
                fillReportRow(qualification, compensationType, dataTable[7], true);
            }

            // без вступительных испытаний и с дипломом олимпиады
            // для всех квалификаций кроме Базового и повышенного уровней СПО и профессий и специальностей НПО
            if (UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES.equals(competitionKind.getCode()) &&
                    !excludeQualificationCodes.contains(qualification.getCode()) &&
                    haveOlympiadDiplomaMap.containsKey(entrant))
            {
                fillReportRow(qualification, compensationType, dataTable[8], true);
            }

            // имеет особые права
            if (haveSpecialRightsMap.containsKey(requestedEnrollmentDirection.getId())) {
                fillReportRow(qualification, compensationType, dataTable[9], true);
            }

            // Направления подготовки в заявлениях с видом конкурса "на общих основаниях" или "Без ВИ"( в случае СПО),
            // где признаки "Целевой прием" и "имеет особые права" - false,
            // а в данных абитуриента есть хотя бы один из элементов справочника  "Преимущества при равенстве конкурсных баллов"
            if (((UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION.equals(competitionKind.getCode()) && highQualificationCodes.contains(qualification.getCode()))
                    || (UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES.equals(competitionKind.getCode()) && middleQualificationCodes.contains(qualification.getCode())))
                    && !haveSpecialRightsMap.containsKey(requestedEnrollmentDirection.getId())
                    && !requestedEnrollmentDirection.isTargetAdmission()
                    && haveEnrollmentRecommendationMap.containsKey(entrant))
            {
                fillReportRow(qualification, compensationType, dataTable[10], true);
            }

            // у которых есть признак "целевой прием"
            if (requestedEnrollmentDirection.isTargetAdmission()) {
                fillReportRow(qualification, compensationType, dataTable[11], true);
            }

            //  с категорией "Слушатель"
            if (StudentCategoryCodes.STUDENT_CATEGORY_LISTENER.equals(studentCategory.getCode())) {
                fillReportRow(qualification, compensationType, dataTable[12], true);
            }
        }

        for (Object[] item : reportData) {
            RequestedEnrollmentDirection requestedEnrollmentDirection = (RequestedEnrollmentDirection) item[0];
            Qualifications qualification = (Qualifications) item[1];
            CompensationType compensationType = (CompensationType) item[2];
            StudentCategory studentCategory = (StudentCategory) item[3];
            Entrant entrant = (Entrant) item[4];
            CompetitionKind competitionKind = requestedEnrollmentDirection.getCompetitionKind();

            // соответсвующей квалификации
            fillReportRow(qualification, compensationType, dataTable[0], false);

            //  с категорией "Студент"
            if (StudentCategoryCodes.STUDENT_CATEGORY_STUDENT.equals(studentCategory.getCode())) {
                fillReportRow(qualification, compensationType, dataTable[1], false);
            }

            // только по результатам ЕГЭ
            if (isEge(chosenEntranceDisciplineMap.get(requestedEnrollmentDirection.getId()))) {
                fillReportRow(qualification, compensationType, dataTable[2], false);
            }

            // по результатам ЕГЭ и дополнительных вступительных испытаний
            if (isEntranceExamOrEge(chosenEntranceDisciplineMap.get(requestedEnrollmentDirection.getId()))) {
                fillReportRow(qualification, compensationType, dataTable[3], false);
            }

            // по результатам вступительных испытаний, проводимых вузом
            if (isEntranceExam(chosenEntranceDisciplineMap.get(requestedEnrollmentDirection.getId()))) {
                fillReportRow(qualification, compensationType, dataTable[4], false);
            }

            // Для квалификаций бакалавр, специалист, магистр - направления подготовки в заявлениях с видом конкурса "на общих сонованиях",
            // Целевой прием и особые права - false,
            // преимуществ при равенстве средних баллов аттестата нет
            if (UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION.equals(competitionKind.getCode()) &&
                    highQualificationCodes.contains(qualification.getCode()) &&
                    !haveSpecialRightsMap.containsKey(requestedEnrollmentDirection.getId()) &&
                    !requestedEnrollmentDirection.isTargetAdmission() &&
                    !haveEnrollmentRecommendationMap.containsKey(entrant))
            {
                fillReportRow(qualification, compensationType, dataTable[5], false);
            }

            // Направления подготовки в заявлениях с видом конкурса Без ВИ,
            // для всех квалификаций кроме Базового и повышенного уровней СПО и профессий и специальностей НПО
            if (UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES.equals(competitionKind.getCode()) && !excludeQualificationCodes.contains(qualification.getCode())) {
                fillReportRow(qualification, compensationType, dataTable[7], false);
            }

            // без вступительных испытаний и с дипломом олимпиады
            // для всех квалификаций кроме Базового и повышенного уровней СПО и профессий и специальностей НПО
            if (UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES.equals(competitionKind.getCode()) && !excludeQualificationCodes.contains(qualification.getCode()) && haveOlympiadDiplomaMap.containsKey(entrant)) {
                fillReportRow(qualification, compensationType, dataTable[8], false);
            }

            // имеет особые права
            if (haveSpecialRightsMap.containsKey(requestedEnrollmentDirection.getId())) {
                fillReportRow(qualification, compensationType, dataTable[9], false);
            }

            // Направления подготовки в заявлениях с видом конкурса "на общих основаниях" или "Без ВИ"( в случае СПО),
            // где признаки "Целевой прием" и "имеет особые права" - false,
            // а в данных абитуриента есть хотя бы один из элементов справочника  "Преимущества при равенстве конкурсных баллов"
            if (((UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION.equals(competitionKind.getCode()) && highQualificationCodes.contains(qualification.getCode())) ||
                    (UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES.equals(competitionKind.getCode()) && middleQualificationCodes.contains(qualification.getCode())))
                    && !haveSpecialRightsMap.containsKey(requestedEnrollmentDirection.getId())
                    && !requestedEnrollmentDirection.isTargetAdmission()
                    && haveEnrollmentRecommendationMap.containsKey(entrant))
            {
                fillReportRow(qualification, compensationType, dataTable[10], false);
            }

            // у которых есть признак "целевой прием"
            if (requestedEnrollmentDirection.isTargetAdmission()) {
                fillReportRow(qualification, compensationType, dataTable[11], false);
            }

            //  с категорией "Слушатель"
            if (StudentCategoryCodes.STUDENT_CATEGORY_LISTENER.equals(studentCategory.getCode())) {
                fillReportRow(qualification, compensationType, dataTable[12], false);
            }
        }

        for (int i = 0; i < TABLE_ROW_COUNT; i++) {
            printRow(sheet, i, dataTable[i]);
        }

        workbook.write();
        workbook.close();
        return workbookStream.toByteArray();
    }

    private ReportRow fillReportRow(Qualifications qualification, CompensationType compensationType, ReportRow row, boolean isRequestData) {
        switch (qualification.getCode()) {

            case QualificationsCodes.BAKALAVR:
                if (isRequestData) {
                    row.addBakalavrRequest();
                }
                else {
                    row.addBakalavr();
                    if (compensationType.isBudget()) {
                        row.addBakalavrBudget();
                    }
                    else {
                        row.addBakalavrContract();
                    }
                }
                break;

            case QualificationsCodes.SPETSIALIST:
                if (isRequestData) {
                    row.addSpecialistRequest();
                }
                else {
                    row.addSpecialist();
                    if (compensationType.isBudget()) {
                        row.addSpecialistBudget();
                    }
                    else {
                        row.addSpecialistContract();
                    }
                }
                break;

            case QualificationsCodes.MAGISTR:
                if (isRequestData) {
                    row.addMagistrRequest();
                }
                else {
                    row.addMagistr();
                    if (compensationType.isBudget()) {
                        row.addMagistrBudget();
                    }
                    else {
                        row.addMagistrContract();
                    }
                }
                break;

            case QualificationsCodes.BAZOVYY_UROVEN_S_P_O:
                if (isRequestData) {
                    row.addSpoRequest();
                }
                else {
                    row.addSPO();
                    if (compensationType.isBudget()) {
                        row.addSpoBudget();
                    }
                    else {
                        row.addSpoContract();
                    }
                }
                break;

            case QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O:
                if (isRequestData) {
                    row.addSpoRequest();
                }
                else {
                    row.addSPO();
                    if (compensationType.isBudget()) {
                        row.addSpoBudget();
                    }
                    else {
                        row.addSpoContract();
                    }
                }
                break;
        }

        return row;
    }

    /**
     * По результатам ЕГЭ(ЕГЭ(вуз))
     * <p/>
     * (если источник оценки ЕГЭ или ЕГЭ вуз)
     *
     * @param chosenEntranceDisciplines
     *
     * @return
     */
    private boolean isEge(List<ChosenEntranceDiscipline> chosenEntranceDisciplines) {
        boolean isEge = true;

        if (chosenEntranceDisciplines == null || chosenEntranceDisciplines.isEmpty()) {
            return false;
        }

        List<Integer> stateExamSourceCodeList = Arrays.asList(new Integer[]{
                UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL, UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL_APPEAL, UniecDefines.FINAL_MARK_STATE_EXAM_WAVE1, UniecDefines.FINAL_MARK_STATE_EXAM_WAVE2});

        for (ChosenEntranceDiscipline discipline : chosenEntranceDisciplines) {
            if (discipline.getFinalMarkSource() != null && !(stateExamSourceCodeList.contains(discipline.getFinalMarkSource()))) {
                isEge = false;
                break;
            }
        }

        return isEge;
    }

    /**
     * по результатам ЕГЭ и дополнительных вступительных испытаний
     * <p/>
     * (Если есть источники оценки ЕГЭ и ЕГЭ вуз для некоторых вступительных испытаний, а для другие есть остальные источники оценки (Все они относятся к вступительным испытаниям по форме вуза))
     *
     * @param chosenEntranceDisciplines
     *
     * @return
     */
    private boolean isEntranceExamOrEge(List<ChosenEntranceDiscipline> chosenEntranceDisciplines) {
        boolean isEge = false;
        boolean isAdditionalEntranceExam = false;

        if (chosenEntranceDisciplines == null || chosenEntranceDisciplines.isEmpty()) {
            return false;
        }

        List<Integer> egeExamSourceCodeList = Arrays.asList(new Integer[]{
                UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL, UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL_APPEAL, UniecDefines.FINAL_MARK_STATE_EXAM_WAVE1, UniecDefines.FINAL_MARK_STATE_EXAM_WAVE2});

        List<Integer> otherExamSourceCodeList = Arrays.asList(new Integer[]{
                UniecDefines.FINAL_MARK_DISCIPLINE_EXAM, UniecDefines.FINAL_MARK_APPEAL_EXAM, UniecDefines.FINAL_MARK_DISCIPLINE_INTERVIEW, UniecDefines.FINAL_MARK_APPEAL_INTERVIEW,
                UniecDefines.FINAL_MARK_DISCIPLINE_TEST, UniecDefines.FINAL_MARK_APPEAL_TEST, UniecDefines.FINAL_MARK_OLYMPIAD});

        for (ChosenEntranceDiscipline discipline : chosenEntranceDisciplines) {
            if (discipline.getFinalMarkSource() == null)
                continue;

            if (egeExamSourceCodeList.contains(discipline.getFinalMarkSource())) {
                isEge = true;
            }

            if (otherExamSourceCodeList.contains(discipline.getFinalMarkSource())) {
                isAdditionalEntranceExam = true;
            }
        }

        return (isEge && isAdditionalEntranceExam);
    }

    /**
     * по результатам вступительных испытаний, проводимых вузом
     * <p/>
     * (Если источник получения оценки только экзамен, собеседование, тестирование или олимпиада (без ЕГЭ или ЕГЭ вуз))
     *
     * @param chosenEntranceDisciplines
     *
     * @return
     */
    private boolean isEntranceExam(List<ChosenEntranceDiscipline> chosenEntranceDisciplines) {
        boolean isEntranceExam = true;

        if (chosenEntranceDisciplines == null || chosenEntranceDisciplines.isEmpty()) {
            return false;
        }

        List<Integer> stateExamSourceCodeList = Arrays.asList(new Integer[]{
                UniecDefines.FINAL_MARK_DISCIPLINE_EXAM, UniecDefines.FINAL_MARK_APPEAL_EXAM, UniecDefines.FINAL_MARK_DISCIPLINE_INTERVIEW, UniecDefines.FINAL_MARK_APPEAL_INTERVIEW,
                UniecDefines.FINAL_MARK_DISCIPLINE_TEST, UniecDefines.FINAL_MARK_APPEAL_TEST, UniecDefines.FINAL_MARK_OLYMPIAD});

        for (ChosenEntranceDiscipline discipline : chosenEntranceDisciplines) {
            if (discipline.getFinalMarkSource() != null && !stateExamSourceCodeList.contains(discipline.getFinalMarkSource())) {
                isEntranceExam = false;
                break;
            }
        }

        return isEntranceExam;
    }

    private Map<Long, List<ChosenEntranceDiscipline>> getChosenEntranceDisciplineMap() {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(ChosenEntranceDiscipline.class, "ced")
                .where(DQLExpressions.in(DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().fromAlias("ced")), getRequestedEnrollmentDirectionBuilderDQL().buildQuery()))
                .predicate(DQLPredicateType.distinct)
                .column("ced");

        List<ChosenEntranceDiscipline> disciplines = builder.createStatement(session).list();

        Map<Long, List<ChosenEntranceDiscipline>> chosenMap = new HashMap<Long, List<ChosenEntranceDiscipline>>();

        for (ChosenEntranceDiscipline chosenEntranceDiscipline : disciplines) {
            SafeMap.safeGet(chosenMap, chosenEntranceDiscipline.getChosenEnrollmentDirection().getId(), ArrayList.class).add(chosenEntranceDiscipline);
        }

        return chosenMap;
    }

    private DQLSelectBuilder getRequestedEnrollmentDirectionBuilderDQL() {

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(RequestedEnrollmentDirection.class, "red")
                .column("red")
                .fetchPath(DQLJoinType.inner, RequestedEnrollmentDirection.enrollmentDirection().fromAlias("red"), "ed")
                .fetchPath(DQLJoinType.inner, RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().formativeOrgUnit().fromAlias("red"), "ou")
                .where(DQLExpressions.eqValue(DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().enrollmentCampaign().fromAlias("red")), model.getEnrollmentCampaign()))
                .where(DQLExpressions.eqValue(DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developForm().fromAlias("red")), model.getDevelopForm()))
                .where(DQLExpressions.eqValue(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().takeAwayDocument().fromAlias("red")), Boolean.FALSE));

        // условия освоения
        if (model.getDevelopConditionList() != null && !model.getDevelopConditionList().isEmpty()) {
            dql.where(DQLExpressions.in(
                    DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developCondition().fromAlias("red")), model.getDevelopConditionList()));
        }

        // квалификации
        if (model.getQualificationList() != null && !model.getQualificationList().isEmpty()) {
            dql.where(DQLExpressions.in(
                    DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().fromAlias("red")), model.getQualificationList()));
        }

        // квалификации
        if (model.getQualificationList() != null && !model.getQualificationList().isEmpty()) {
            dql.where(DQLExpressions.in(
                    DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().fromAlias("red")), model.getQualificationList()));
        }
        else {
            dql.where(DQLExpressions.in(
                    DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().fromAlias("red")),
                    getQualificationsDQL().buildQuery()));
        }

        // формирующие подразделения
        if (model.getFormativeOrgUnitList() != null && !model.getFormativeOrgUnitList().isEmpty()) {
            dql.where(DQLExpressions.in(
                    DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().formativeOrgUnit().fromAlias("red")), model.getFormativeOrgUnitList()));
        }

        // территориальные подразделения
        if (model.getTerritorialOrgUnitList() != null && !model.getTerritorialOrgUnitList().isEmpty()) {
            dql.where(DQLExpressions.in(
                    DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().territorialOrgUnit().fromAlias("red")), model.getTerritorialOrgUnitList()));
        }

        // состояния абитуриента
        if (model.getEntrantStateList() != null && !model.getEntrantStateList().isEmpty()) {
            dql.where(DQLExpressions.in(
                    DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().entrant().state().id().fromAlias("red")),
                    CommonDAO.ids(model.getEntrantStateList())));
        }
        else {
            DQLSelectBuilder subBuilder = new DQLSelectBuilder()
                    .fromEntity(EntrantState.class, "es")
                    .where(DQLExpressions.in(
                            DQLExpressions.property(EntrantState.code().fromAlias("es")), Arrays.asList(EntrantStateCodes.ENROLED, EntrantStateCodes.IN_ORDER, EntrantStateCodes.PRELIMENARY_ENROLLED)))
                    .column(EntrantState.id().fromAlias("es").s());

            dql.where(DQLExpressions.in(
                    DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().entrant().state().id().fromAlias("red")),
                    subBuilder.buildQuery()));
        }

        return dql;
    }

    private DQLSelectBuilder getPreliminaryEnrolmentStudentDQL() {

        DQLSelectBuilder dql = getRequestedEnrollmentDirectionBuilderDQL()
                .joinEntity("red", DQLJoinType.inner, PreliminaryEnrollmentStudent.class, "pes",
                            DQLExpressions.eq(
                                    DQLExpressions.property(RequestedEnrollmentDirection.id().fromAlias("red")),
                                    DQLExpressions.property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().id().fromAlias("pes"))));

        return dql;
    }

    private List<Object[]> getPreliminaryData() {

        DQLSelectBuilder builder = getPreliminaryEnrolmentStudentDQL();

        builder
                .column(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().enrollmentDirection().educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().fromAlias("pes").s())
                .column(PreliminaryEnrollmentStudent.compensationType().fromAlias("pes").s())
                .column(PreliminaryEnrollmentStudent.studentCategory().fromAlias("pes").s())
                .column(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().entrantRequest().entrant().fromAlias("pes").s());

        return builder.createStatement(session).list();
    }

    /**
     * выбранные НП, которые зачислены с признаком "имеет особые права"
     *
     * @return
     */
    private Map<Long, Boolean> getHaveSpecialRightsMap() {

        Map<Long, Boolean> haveSpecialRightsMap = new HashMap<Long, Boolean>();

        DQLSelectBuilder extREDBuilder = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirectionExt.class, "ext")
                .where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirectionExt.requestedEnrollmentDirection().fromAlias("ext")), getRequestedEnrollmentDirectionBuilderDQL().buildQuery()))
                .where(DQLExpressions.eqValue(DQLExpressions.property(RequestedEnrollmentDirectionExt.haveSpecialRights().fromAlias("ext")), Boolean.TRUE))
                .column(RequestedEnrollmentDirectionExt.requestedEnrollmentDirection().fromAlias("ext").s());

        List<RequestedEnrollmentDirection> requestedEnrollmentDirections = extREDBuilder.createStatement(session).list();

        for (RequestedEnrollmentDirection requestedEnrollmentDirection : requestedEnrollmentDirections) {
            haveSpecialRightsMap.put(requestedEnrollmentDirection.getId(), Boolean.TRUE);
        }

        return haveSpecialRightsMap;
    }

    /**
     * имеет право на преимущественное зачисление
     *
     * @return
     */
    private Map<Entrant, Boolean> getHaveEnrollmentRecommendationMap(List<RequestedEnrollmentDirection> requestedEnrollmentDirections) {
        Set<Long> entrantIds = new HashSet<Long>();

        for (RequestedEnrollmentDirection requestedEnrollmentDirection : requestedEnrollmentDirections) {
            entrantIds.add(requestedEnrollmentDirection.getEntrantRequest().getEntrantId());
        }

        final Map<Entrant, Boolean> haveEnrollmentRecommendationMap = new HashMap<Entrant, Boolean>();

        BatchUtils.execute(entrantIds, 1000, new BatchUtils.Action<Long>() {
            @Override
            public void execute(Collection<Long> elements) {

                DQLSelectBuilder dql = new DQLSelectBuilder()
                        .fromEntity(EntrantEnrolmentRecommendation.class, "eer")
                        .where(DQLExpressions.in(DQLExpressions.property(EntrantEnrolmentRecommendation.entrant().id().fromAlias("eer")), elements))
                        .column(EntrantEnrolmentRecommendation.entrant().fromAlias("eer").s());

                List<Entrant> entrants = dql.createStatement(session).list();

                for (Entrant entrant : entrants) {
                    haveEnrollmentRecommendationMap.put(entrant, Boolean.TRUE);
                }
            }
        });

        return haveEnrollmentRecommendationMap;
    }

    /**
     * имеет диплом олимпиады
     *
     * @return
     */
    private Map<Entrant, Boolean> getHaveOlympiadDiplomaMap(List<RequestedEnrollmentDirection> requestedEnrollmentDirections) {
        Set<Long> entrantIds = new HashSet<Long>();

        for (RequestedEnrollmentDirection requestedEnrollmentDirection : requestedEnrollmentDirections) {
            entrantIds.add(requestedEnrollmentDirection.getEntrantRequest().getEntrantId());
        }

        final Map<Entrant, Boolean> haveOlympiadDiplomaMap = new HashMap<Entrant, Boolean>();

        BatchUtils.execute(entrantIds, 1000, new BatchUtils.Action<Long>() {
            @Override
            public void execute(Collection<Long> elements) {

                DQLSelectBuilder dql = new DQLSelectBuilder()
                        .fromEntity(OlympiadDiploma.class, "od")
                        .where(DQLExpressions.in(DQLExpressions.property(OlympiadDiploma.entrant().id().fromAlias("od")), elements))
                        .column(OlympiadDiploma.entrant().fromAlias("od").s());

                List<Entrant> entrants = dql.createStatement(session).list();

                for (Entrant entrant : entrants) {
                    haveOlympiadDiplomaMap.put(entrant, Boolean.TRUE);
                }
            }
        });

        return haveOlympiadDiplomaMap;
    }

    private DQLSelectBuilder getQualificationsDQL() {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(Qualifications.class, "q")
                .where(DQLExpressions.in(
                        DQLExpressions.property(Qualifications.code().fromAlias("q")),
                        Arrays.asList(QualificationsCodes.BAKALAVR, QualificationsCodes.SPETSIALIST, QualificationsCodes.MAGISTR, QualificationsCodes.BAZOVYY_UROVEN_S_P_O, QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O)))
                .column("q");

        return dql;
    }

    private void initFormats() throws Exception {
        WritableFont arial11 = new WritableFont(WritableFont.ARIAL, 11);

        cellFormat = new WritableCellFormat(arial11);
        cellFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        cellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        cellFormat.setAlignment(Alignment.CENTRE);
        cellFormat.setWrap(true);
    }

    private void printRow(WritableSheet sheet, int row, ReportRow reportRow) throws Exception {
        String[] reportLine = reportRow.toArray();
        for (int i = 0; i < reportLine.length - 1; i++) {
            sheet.addCell(createCell(i + 2, 5 + row, reportLine[i + 1], cellFormat));
        }
    }

    protected WritableCell createCell(int col, int row, Object value, WritableCellFormat format) throws Exception {
        if (value instanceof Integer)
            return new jxl.write.Number(col, row, ((Integer) value) + 0.0, format);
        return new Label(col, row, value.toString(), format);
    }

    protected Label write(WritableSheet sheet, String loc, String content) {
        Label label = (Label) sheet.getCell(loc);
        label.setString(content);
        return label;
    }

    class ReportRow {

        private Integer rowNumber;
        private Integer bakalavrRequestCount;
        private Integer bakalavrCount;
        private Integer bakalavrBudgetCount;
        private Integer bakalavrContractCount;

        private Integer specialistRequestCount;
        private Integer specialistCount;
        private Integer specialistBudgetCount;
        private Integer specialistContractCount;

        private Integer magistrRequestCount;
        private Integer magistrCount;
        private Integer magistrBudgetCount;
        private Integer magistrContractCount;

        private Integer spoRequestCount;
        private Integer spoCount;
        private Integer spoBudgetCount;
        private Integer spoContractCount;

        public ReportRow(Integer rowNumber) {
            this.rowNumber = rowNumber;
            fillRow(rowNumber);
        }

        private void fillRow(int rowNumber) {
            // строку номер 6 ( из них выпускники подготовительных курсов) не заполняем
            if (rowNumber != 6) {
                this.bakalavrRequestCount = 0;
                this.bakalavrCount = 0;
                this.bakalavrBudgetCount = 0;
                this.bakalavrContractCount = 0;

                this.specialistRequestCount = 0;
                this.specialistCount = 0;
                this.specialistBudgetCount = 0;
                this.specialistContractCount = 0;

                this.magistrRequestCount = 0;
                this.magistrCount = 0;
                this.magistrBudgetCount = 0;
                this.magistrContractCount = 0;

                this.spoRequestCount = 0;
                this.spoCount = 0;
                this.spoBudgetCount = 0;
                this.spoContractCount = 0;
            }
        }

        public Integer getRowNumber() {
            return rowNumber;
        }

        public Integer getBakalavrRequestCount() {
            return bakalavrRequestCount;
        }

        public Integer getBakalavrCount() {
            return bakalavrCount;
        }

        public Integer getBakalavrBudgetCount() {
            return bakalavrBudgetCount;
        }

        public Integer getBakalavrContractCount() {
            return bakalavrContractCount;
        }

        public Integer getSpecialistRequestCount() {
            return specialistRequestCount;
        }

        public Integer getSpecialistCount() {
            return specialistCount;
        }

        public Integer getSpecialistBudgetCount() {
            return specialistBudgetCount;
        }

        public Integer getSpecialistContractCount() {
            return specialistContractCount;
        }

        public Integer getMagistrRequestCount() {
            return magistrRequestCount;
        }

        public Integer getMagistrCount() {
            return magistrCount;
        }

        public Integer getMagistrBudgetCount() {
            return magistrBudgetCount;
        }

        public Integer getMagistrContractCount() {
            return magistrContractCount;
        }

        public Integer getSpoRequestCount() {
            return spoRequestCount;
        }

        public Integer getSpoCount() {
            return spoCount;
        }

        public Integer getSpoBudgetCount() {
            return spoBudgetCount;
        }

        public Integer getSpoContractCount() {
            return spoContractCount;
        }

        public void addBakalavrRequest() {
            this.bakalavrRequestCount += 1;
        }

        public void addBakalavr() {
            this.bakalavrCount += 1;
        }

        public void addBakalavrBudget() {
            this.bakalavrBudgetCount += 1;
        }

        public void addBakalavrContract() {
            this.bakalavrContractCount += 1;
        }

        public void addSpecialistRequest() {
            this.specialistRequestCount += 1;
        }

        public void addSpecialist() {
            this.specialistCount += 1;
        }

        public void addSpecialistBudget() {
            this.specialistBudgetCount += 1;
        }

        public void addSpecialistContract() {
            this.specialistContractCount += 1;
        }

        public void addMagistrRequest() {
            this.magistrRequestCount += 1;
        }

        public void addMagistr() {
            this.magistrCount += 1;
        }

        public void addMagistrBudget() {
            this.magistrBudgetCount += 1;
        }

        public void addMagistrContract() {
            this.magistrContractCount += 1;
        }

        public void addSpoRequest() {
            this.spoRequestCount += 1;
        }

        public void addSPO() {
            this.spoCount += 1;
        }

        public void addSpoBudget() {
            this.spoBudgetCount += 1;
        }

        public void addSpoContract() {
            this.spoContractCount += 1;
        }

        public String[] toArray() {
            String[] arr = new String[TABLE_COLUMN_COUNT];

            arr[0] = String.valueOf(this.rowNumber);

            arr[1] = this.bakalavrRequestCount != null ? String.valueOf(this.bakalavrRequestCount) : "";
            arr[2] = this.bakalavrCount != null ? String.valueOf(this.bakalavrCount) : "";
            arr[3] = this.bakalavrBudgetCount != null ? String.valueOf(this.bakalavrBudgetCount) : "";
            arr[4] = this.bakalavrContractCount != null ? String.valueOf(this.bakalavrContractCount) : "";

            arr[5] = this.specialistRequestCount != null ? String.valueOf(this.specialistRequestCount) : "";
            arr[6] = this.specialistCount != null ? String.valueOf(this.specialistCount) : "";
            arr[7] = this.specialistBudgetCount != null ? String.valueOf(this.specialistBudgetCount) : "";
            arr[8] = this.specialistContractCount != null ? String.valueOf(this.specialistContractCount) : "";

            arr[9] = this.magistrRequestCount != null ? String.valueOf(this.magistrRequestCount) : "";
            arr[10] = this.magistrCount != null ? String.valueOf(this.magistrCount) : "";
            arr[11] = this.magistrBudgetCount != null ? String.valueOf(this.magistrBudgetCount) : "";
            arr[12] = this.magistrContractCount != null ? String.valueOf(this.magistrContractCount) : "";

            arr[13] = this.spoRequestCount != null ? String.valueOf(this.spoRequestCount) : "";
            arr[14] = this.spoCount != null ? String.valueOf(this.spoCount) : "";
            arr[15] = this.spoBudgetCount != null ? String.valueOf(this.spoBudgetCount) : "";
            arr[16] = this.spoContractCount != null ? String.valueOf(this.spoContractCount) : "";

            return arr;
        }
    }
}
