package ru.tandemservice.uniecrmc.entity.entrant;

import ru.tandemservice.uniecrmc.entity.entrant.gen.OnlinePriorityProfileEduOuGen;

import java.util.Comparator;

/**
 * Приоритет профиля направления подготовки по ВНП онлайн абитуриента
 */
public class OnlinePriorityProfileEduOu extends OnlinePriorityProfileEduOuGen
{
    public static final Comparator<OnlinePriorityProfileEduOu> COMPARATOR = new Comparator<OnlinePriorityProfileEduOu>() {
        @Override
        public int compare(OnlinePriorityProfileEduOu o1, OnlinePriorityProfileEduOu o2) {
            return o1.getPriority() - o2.getPriority();
        }
    };
}