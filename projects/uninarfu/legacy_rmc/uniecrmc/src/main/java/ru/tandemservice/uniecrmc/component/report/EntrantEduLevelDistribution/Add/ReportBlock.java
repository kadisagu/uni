package ru.tandemservice.uniecrmc.component.report.EntrantEduLevelDistribution.Add;

import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.codes.DevelopTechCodes;

import java.util.List;

public class ReportBlock {
    private final static String INDIVIDUAL_TITLE = " по индивидуальному учебному плану ускоренно (ускоренная подготовка  + сокращенная программа) по программам ";
    private final static String NET_TITLE = " с применением сетевой формы обучения по программам  ";
    private final static String DISTANCE_TITLE = " с использованием дистанционных образовательных технологий по программам ";

    private ReportDetailedBlock headerBlock;
    private ReportDetailedBlock individualBlock;
    private ReportDetailedBlock netDevelopBlock;
    private ReportDetailedBlock distanceDevelopFormBlock;
    private boolean bach;
    private boolean speciality;
    private boolean master;
    private String eduTitle1;
    private String eduTitle2;
    private int firstNumber;
    private List<DevelopCondition> conditionsForIndividual;

    public ReportBlock() {
    }

    public ReportBlock(boolean bach, boolean speciality, boolean master, int firstNumber, List<DevelopCondition> conditions) {
        this.bach = bach;
        this.speciality = speciality;
        this.master = master;
        this.conditionsForIndividual = conditions;
        if (bach) {
            eduTitle1 = "бакалавриата";
            eduTitle2 = "направлениям";
        }
        if (speciality) {
            eduTitle1 = "специалитета";
            eduTitle2 = "специальностям";
        }
        if (master) {
            eduTitle1 = "магистратуры";
            eduTitle2 = "направлениям";
        }
        headerBlock = new ReportDetailedBlock("Программы " + eduTitle1 + " - всего", eduTitle2);
        individualBlock = new ReportDetailedBlock("", eduTitle2);
        netDevelopBlock = new ReportDetailedBlock("", eduTitle2);
        distanceDevelopFormBlock = new ReportDetailedBlock("", eduTitle2);
        setFirstNumber(firstNumber);
    }

    public List<DevelopCondition> getConditionsForIndividual() {
        return conditionsForIndividual;
    }

    public void setConditionsForIndividual(List<DevelopCondition> conditionsForIndividual) {
        this.conditionsForIndividual = conditionsForIndividual;
    }

    public boolean isBach() {
        return bach;
    }

    public void setBach(boolean bach) {
        this.bach = bach;
    }

    public boolean isSpeciality() {
        return speciality;
    }

    public void setSpeciality(boolean speciality) {
        this.speciality = speciality;
    }

    public boolean isMaster() {
        return master;
    }

    public void setMaster(boolean master) {
        this.master = master;
    }

    public ReportDetailedBlock getHeaderBlock() {
        return headerBlock;
    }

    public void setHeaderBlock(ReportDetailedBlock headerBlock) {
        this.headerBlock = headerBlock;
    }

    public ReportDetailedBlock getIndividualBlock() {
        return individualBlock;
    }

    public void setIndividualBlock(ReportDetailedBlock individualBlock) {
        this.individualBlock = individualBlock;
    }

    public ReportDetailedBlock getNetDevelopBlock() {
        return netDevelopBlock;
    }

    public void setNetDevelopBlock(ReportDetailedBlock netDevelopBlock) {
        this.netDevelopBlock = netDevelopBlock;
    }

    public ReportDetailedBlock getDistanceDevelopFormBlock() {
        return distanceDevelopFormBlock;
    }

    public void setDistanceDevelopFormBlock(ReportDetailedBlock distanceDevelopFormBlock) {
        this.distanceDevelopFormBlock = distanceDevelopFormBlock;
    }

    private void setTitlesForDetailedBlock()
    {
        String prefix = "Из стр. 0" + String.valueOf(firstNumber);
        getHeaderBlock().getHeader().setNumber(String.valueOf(firstNumber));
        getIndividualBlock().getHeader().setTitle(prefix + INDIVIDUAL_TITLE + eduTitle1 + " - всего");
        getIndividualBlock().getHeader().setNumber(String.valueOf(firstNumber + 1));
        getNetDevelopBlock().getHeader().setTitle(prefix + NET_TITLE + eduTitle1 + " - всего");
        getNetDevelopBlock().getHeader().setNumber(String.valueOf(firstNumber + 2));
        getDistanceDevelopFormBlock().getHeader().setTitle(prefix + DISTANCE_TITLE + eduTitle1 + " - всего");
        getDistanceDevelopFormBlock().getHeader().setNumber(String.valueOf(firstNumber + 3));
    }

    private void setFirstNumber(int firstNumber)
    {
        this.firstNumber = firstNumber;
        headerBlock.getHeader().setNumber(String.valueOf(firstNumber));
        setTitlesForDetailedBlock();
    }

    public int getLastNumber()
    {
        return firstNumber + 3;
    }

    public void increase(EducationLevelsHighSchool educationLevelsHighSchool, DevelopCondition condition, DevelopTech tech, CompensationType compensationType, boolean isEnrolled)
    {
        if (isEnrolled) {
            headerBlock.increaseEnrolled(educationLevelsHighSchool, compensationType);
            if (tech.getCode().equals(DevelopTechCodes.DISTANCE))
                distanceDevelopFormBlock.increaseEnrolled(educationLevelsHighSchool, compensationType);
            if (conditionsForIndividual.contains(condition))
                individualBlock.increaseEnrolled(educationLevelsHighSchool, compensationType);
        }
        else {
            headerBlock.increaseTotal(educationLevelsHighSchool);
            if (tech.getCode().equals(DevelopTechCodes.DISTANCE))
                distanceDevelopFormBlock.increaseTotal(educationLevelsHighSchool);
            if (conditionsForIndividual.contains(condition))
                individualBlock.increaseTotal(educationLevelsHighSchool);
        }
    }
}
