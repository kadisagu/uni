package ru.tandemservice.uniecrmc.component.catalog.targetAdmissionKind.TargetAdmissionKindItemPub;

import ru.tandemservice.uniecrmc.entity.entrant.TargetAdmissionKindRMC;


public class DAO extends ru.tandemservice.uniec.component.catalog.targetAdmissionKind.TargetAdmissionKindItemPub.DAO {

    @Override
    public void prepare(ru.tandemservice.uniec.component.catalog.targetAdmissionKind.TargetAdmissionKindItemPub.Model model) {

        super.prepare(model);
        Model myModel = (Model) model;


        TargetAdmissionKindRMC targetAdmissionKindRMC = get(TargetAdmissionKindRMC.class, TargetAdmissionKindRMC.L_BASE, model.getCatalogItem());

        if (targetAdmissionKindRMC == null) {
            targetAdmissionKindRMC = new TargetAdmissionKindRMC();
            targetAdmissionKindRMC.setBase(model.getCatalogItem());
        }
        myModel.setTargetAdmissionKindRMC(targetAdmissionKindRMC);
    }
}
