package ru.tandemservice.uniecrmc.component.entrant.EntrantPub;

import ru.tandemservice.uniecrmc.entity.entrant.Entrant2IndividualAchievements;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.entrant.Entrant;

import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {


    @Override
    public void prepare(Model model) {
        Entrant entrant = getNotNull(Entrant.class, model.getEntrant().getId());
        model.setEntrant(entrant);
    }

    @Override
    public void prepareDataSource(Model model) {
        List<Entrant2IndividualAchievements> resultList = getList(Entrant2IndividualAchievements.class, Entrant2IndividualAchievements.entrant(), model.getEntrant());
//		for (Entrant2IndividualAchievements protocol : list) {
//			ViewWrapper<Entrant2IndividualAchievements> wrapper = new ViewWrapper<Entrant2IndividualAchievements>(protocol);
//			resultList.add(wrapper);
//		}
//
        model.getDataSource().setCountRow(resultList.isEmpty() ? 1 : resultList.size());
        UniUtils.createPage(model.getDataSource(), resultList);
    }


}
