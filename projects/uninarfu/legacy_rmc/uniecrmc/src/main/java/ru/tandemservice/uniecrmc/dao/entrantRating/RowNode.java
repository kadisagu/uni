package ru.tandemservice.uniecrmc.dao.entrantRating;

import javax.xml.bind.annotation.XmlAttribute;

public class RowNode {

    @XmlAttribute(name = "id")
    public String code;

    @XmlAttribute(required = false)
    public String title;

    @XmlAttribute(required = false)
    public String shortTitle;

    @XmlAttribute(required = false)
    public Boolean used;

    @XmlAttribute(required = false)
    public Integer priority;

    @XmlAttribute(name = "parent", required = false)
    public String parentCode;

    public RowNode() {
    }

    public RowNode(String code, String title, String shortTitle) {
        this.code = code;
        this.title = title;
        this.shortTitle = shortTitle;
    }

    public RowNode(String code, String title, String shortTitle, Boolean used, Integer priority, String parentCode) {
        this(code, title, shortTitle);
        this.used = used;
        this.priority = priority;
        this.parentCode = parentCode;
    }
}
