package ru.tandemservice.uniecrmc.component.entrant.EntrantRequestTab;

import org.hibernate.Session;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt;
import ru.tandemservice.uniec.component.entrant.EntrantRequestTab.Model;
import ru.tandemservice.uniec.dao.IEntrantDAO;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.codes.EntrantStateCodes;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DAO extends ru.tandemservice.uniec.component.entrant.EntrantRequestTab.DAO implements IDAO {

    @Override
    public void prepareRequestedEnrollmentDirectionDataSource(Model model,
                                                              EntrantRequest request)
    {
        super.prepareRequestedEnrollmentDirectionDataSource(model, request);

        DynamicListDataSource<RequestedEnrollmentDirection> dataSource = model.getRequestedEnrollmentDirectionDataSourceByRequest().get(request);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirectionExt.class, "ext")
                .where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirectionExt.requestedEnrollmentDirection().fromAlias("ext")), dataSource.getEntityList()));

        List<RequestedEnrollmentDirectionExt> directionsExt = getList(builder);

        Map<RequestedEnrollmentDirection, RequestedEnrollmentDirectionExt> mapExt = new HashMap<RequestedEnrollmentDirection, RequestedEnrollmentDirectionExt>();

        for (RequestedEnrollmentDirectionExt directionExt : directionsExt)
            mapExt.put(directionExt.getRequestedEnrollmentDirection(), directionExt);

        List<ViewWrapper<RequestedEnrollmentDirection>> patchedList = ViewWrapper.getPatchedList(dataSource);

        for (ViewWrapper<RequestedEnrollmentDirection> wrapper : patchedList) {
            RequestedEnrollmentDirectionExt directionExt = mapExt.get(wrapper.getEntity());
            boolean haveSpecialRights = false;
            if (directionExt != null)
                haveSpecialRights = directionExt.isHaveSpecialRights();
            wrapper.setViewProperty("haveSpecialRights", haveSpecialRights);
        }
    }

    @Override
    public void updateSpecialRights(Model model, Long id) {
        RequestedEnrollmentDirection direction = getNotNull(id);
        RequestedEnrollmentDirectionExt directionExt = get(RequestedEnrollmentDirectionExt.class, RequestedEnrollmentDirectionExt.requestedEnrollmentDirection(), direction);

        if (directionExt == null) {
            directionExt = new RequestedEnrollmentDirectionExt();
            directionExt.setRequestedEnrollmentDirection(direction);
            directionExt.setHaveSpecialRights(true);
        }
        else
            directionExt.setHaveSpecialRights(!directionExt.isHaveSpecialRights());

        getSession().saveOrUpdate(directionExt);
    }

    @Override
    public void updateDocumentsReturn(Long requestId)
    {
        Session session = getSession();
        EntrantRequest entrantRequest = (EntrantRequest) getNotNull(EntrantRequest.class, requestId);
        entrantRequest.setTakeAwayDocument(Boolean.FALSE.booleanValue());

        // во всех куйгуыеУткщддьутеВшкусешщт ставим статус активный
        // тогда все пересчитается правильно
        // + запускаем пересчет
        List<RequestedEnrollmentDirection> lst = getList(RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.entrantRequest().id(), requestId);
        session.update(entrantRequest);
        EntrantState state = getByCode(EntrantState.class, EntrantStateCodes.ACTIVE);

        for (RequestedEnrollmentDirection rd : lst) {
            rd.setState(state);
            session.update(rd);
        }

        List<Long> ids = new ArrayList<>();
        ids.add(entrantRequest.getEntrant().getId());

        // пересчитываем абитуриентов
        IEntrantDAO idao = (IEntrantDAO) ApplicationRuntime.getBean(IEntrantDAO.class.getName());
        idao.updateEntrantData(ids);

    }

}
