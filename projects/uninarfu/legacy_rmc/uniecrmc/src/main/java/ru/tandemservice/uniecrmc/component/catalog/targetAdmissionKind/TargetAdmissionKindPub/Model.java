package ru.tandemservice.uniecrmc.component.catalog.targetAdmissionKind.TargetAdmissionKindPub;

import ru.tandemservice.uniecrmc.entity.entrant.TargetAdmissionKindRMC;


public class Model extends ru.tandemservice.uniec.component.catalog.targetAdmissionKind.TargetAdmissionKindPub.Model {
    private TargetAdmissionKindRMC targetAdmissionKindRMC;

    public void setTargetAdmissionKindRMC(TargetAdmissionKindRMC targetAdmissionKindRMC)
    {
        this.targetAdmissionKindRMC = targetAdmissionKindRMC;
    }

    public TargetAdmissionKindRMC getTargetAdmissionKindRMC()
    {
        return targetAdmissionKindRMC;
    }

}
