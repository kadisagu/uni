package ru.tandemservice.uniecrmc.component.report.EntranceExaminationMeeting.EntranceExaminationMeetingReportAdd;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.component.report.EntranceExaminationMeeting.EntranceExaminationMeetingReportAdd.Model;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;

import java.io.ByteArrayOutputStream;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class DAO extends ru.tandemservice.uniec.component.report.EntranceExaminationMeeting.EntranceExaminationMeetingReportAdd.DAO {

    @Override
    public void update(Model model) {
        Session session = this.getSession();
        List enrollmentDirections = MultiEnrollmentDirectionUtil.getEnrollmentDirections(model, session);
        if (enrollmentDirections.isEmpty()) {
            throw new ApplicationException("Нет заявлений, подходящих под указанные параметры.");
        }
        else {
            EntranceExaminationMeetingReport report = model.getReport();
            report.setFormingDate(new Date());
            if (model.isTerritorialOrgUnitActive()) {
                report.setTerritorialOrgUnit(UniStringUtils.join(model.getTerritorialOrgUnitList(), "territorialTitle", ", "));
            }

            if (model.isDevelopConditionActive()) {
                report.setDevelopCondition(UniStringUtils.join(model.getDevelopConditionList(), "title", ", "));
            }

            if (model.isDevelopTechActive()) {
                report.setDevelopTech(UniStringUtils.join(model.getDevelopTechList(), "title", ", "));
            }

            if (model.isDevelopPeriodActive()) {
                report.setDevelopPeriod(UniStringUtils.join(model.getDevelopPeriodList(), "title", ", "));
            }

            if (model.isStudentCategoryActive()) {
                report.setStudentCategory(UniStringUtils.join(model.getStudentCategoryList(), "title", ", "));
            }

            if (model.isQualificationActive()) {
                enrollmentDirections = this.filterDirectionsByQualifications(enrollmentDirections, model.getQualificationList());
            }

            DatabaseFile content = new DatabaseFile();
            content.setContent(this.generateContent(model, enrollmentDirections));
            session.save(content);
            report.setContent(content);
            session.save(report);
        }
    }

    protected EntranceExaminationMeetingReportContentGenerator createReportGenerator(EntranceExaminationMeetingReport report, List<EnrollmentDirection> enrollmentDirections, List<StudentCategory> studentCategories) {
        return new EntranceExaminationMeetingReportContentGenerator(report, enrollmentDirections, studentCategories, this.getSession());
    }

    private List<EnrollmentDirection> filterDirectionsByQualifications(List<EnrollmentDirection> enrollmentDirections, List<Qualifications> qualifications) {
        ArrayList result = new ArrayList();
        Iterator i$ = enrollmentDirections.iterator();

        while (i$.hasNext()) {
            EnrollmentDirection enrollmentDirection = (EnrollmentDirection) i$.next();
            Qualifications qualification = enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getQualification();
            if (qualifications.contains(qualification)) {
                result.add(enrollmentDirection);
            }
        }

        return result;
    }

    private byte[] generateContent(Model model, List<EnrollmentDirection> enrollmentDirections) {
        EntranceExaminationMeetingReport report = model.getReport();
        List studentCategories = model.isStudentCategoryActive() ? model.getStudentCategoryList() : Collections.emptyList();

        try {
            byte[] e;
            if (!model.isByAllEducationLevels()) {
                e = this.createReportGenerator(report, enrollmentDirections, studentCategories).generateReportContent();
            }
            else {
                e = this.createZipFile(report, studentCategories, this.getGroupedEnrollmentDirections(enrollmentDirections, model.isPickOutUnlicensedEducationLevels()));
            }

            return e;
        }
        catch (ApplicationException var6) {
            throw var6;
        }
        catch (Exception var7) {
            throw new RuntimeException(var7);
        }
    }

    private List<List<EnrollmentDirection>> getGroupedEnrollmentDirections(List<EnrollmentDirection> enrollmentDirections, boolean pickOutUnlicensedEducationLevels) {
        ArrayList result = new ArrayList();
        if (!pickOutUnlicensedEducationLevels) {
            Iterator enrollmentDirectionGroups = enrollmentDirections.iterator();

            while (enrollmentDirectionGroups.hasNext()) {
                EnrollmentDirection i$ = (EnrollmentDirection) enrollmentDirectionGroups.next();
                result.add(Collections.singletonList(i$));
            }
        }
        else {
            Map enrollmentDirectionGroups1 = SafeMap.get(ArrayList.class);
            Iterator i$1 = enrollmentDirections.iterator();

            while (i$1.hasNext()) {
                EnrollmentDirection enrollmentDirection = (EnrollmentDirection) i$1.next();
                EducationOrgUnit educationOrgUnit = enrollmentDirection.getEducationOrgUnit();
                OrgUnit formativeOrgUnit = educationOrgUnit.getFormativeOrgUnit();
                EducationLevelsHighSchool educationLevelHighSchool = educationOrgUnit.getEducationLevelHighSchool();
                DevelopForm developForm = educationOrgUnit.getDevelopForm();
                DevelopCondition developCondition = educationOrgUnit.getDevelopCondition();
                DevelopTech developTech = educationOrgUnit.getDevelopTech();
                DevelopPeriod developPeriod = educationOrgUnit.getDevelopPeriod();
                MultiKey key = new MultiKey(new Object[]{formativeOrgUnit, educationLevelHighSchool, developForm, developCondition, developTech, developPeriod});
                ((List) enrollmentDirectionGroups1.get(key)).add(enrollmentDirection);
            }

            result = new ArrayList(enrollmentDirectionGroups1.values());
        }

        return result;
    }

    private byte[] createZipFile(EntranceExaminationMeetingReport report, List<StudentCategory> studentCategories, List<List<EnrollmentDirection>> enrollmentDirections) throws Exception {
        ByteArrayOutputStream out = null;

        try {
            out = new ByteArrayOutputStream();
            ZipOutputStream zipOut = new ZipOutputStream(out);

            for (int i = 0; i < enrollmentDirections.size(); ++i) {
                zipOut.putNextEntry(new ZipEntry("Otchet " + (i + 1) + ".xls"));
                zipOut.write(this.createReportGenerator(report, (List) enrollmentDirections.get(i), studentCategories).generateReportContent());
                zipOut.closeEntry();
            }

            zipOut.close();
            byte[] var10 = out.toByteArray();
            return var10;
        }
        finally {
            if (out != null) {
                out.close();
            }

        }
    }
}
