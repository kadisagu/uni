package ru.tandemservice.uniecrmc.dao.ws.distribution;

import org.apache.commons.lang.mutable.MutableDouble;
import org.hibernate.Session;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.NotUsedBenefit;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import org.tandemframework.shared.person.catalog.entity.Benefit;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.logic.IEcDistributionExtDao;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.EcDistributionUtil;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.EcgCortegeDTORMC;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.WrapChoseEntranseDiscipline;
import ru.tandemservice.uniecrmc.entity.entrant.EcgEntrantRecommendedDetailExt;
import ru.tandemservice.uniecrmc.entity.entrant.EcgEntrantRecommendedExt;
import ru.tandemservice.uniecrmc.entity.entrant.EntranceDisciplineExt;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.base.bo.EcDistribution.EcDistributionManager;
import ru.tandemservice.uniec.base.bo.EcDistribution.logic.IEcDistributionDao;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.*;
import ru.tandemservice.uniec.base.entity.ecg.*;
import ru.tandemservice.uniec.base.entity.ecg.gen.EcgDistributionGen;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EnrollmentRecommendation;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.catalog.gen.EntrantStateGen;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrderType;
import ru.tandemservice.uniec.entity.settings.*;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.ExamSetUtil;
import ru.tandemservice.uniec.ws.distribution.EnrollmentDistributionEnvironmentNode;
import ru.tandemservice.uniec.ws.distribution.EnrollmentDistributionServiceDao;

import java.util.*;

public class EnrollmentDistributionServiceExtDao extends EnrollmentDistributionServiceDao {

    @Override
    public EnrollmentDistributionEnvironmentNode getEnrollmentDistributionEnvironmentNode(
            Set<IEcgDistribution> distributionSet)
    {

        if ((distributionSet == null) || (distributionSet.isEmpty()))
            throw new RuntimeException("DistributionSet is empty.");

        EnrollmentCampaign enrollmentCampaign = distributionSet.iterator().next().getConfig().getEcgItem().getEnrollmentCampaign();

        if (!EcDistributionUtil.isUsePriority(enrollmentCampaign))
            return super.getEnrollmentDistributionEnvironmentNode(distributionSet);

        Session session = getSession();

        Set<Long> usedEnrollmentCampaignIds = new HashSet<Long>();
        for (IEcgDistribution distribution : distributionSet) {
            session.refresh(distribution);
            if ((distribution instanceof EcgDistributionDetail))
                session.refresh(distribution.getDistribution());
            usedEnrollmentCampaignIds.add(distribution.getConfig().getEcgItem().getEnrollmentCampaign().getId());
        }

        if (usedEnrollmentCampaignIds.size() > 1)
            throw new RuntimeException("DistributionSet contains two or more enrollmentCampaigns.");

        EnrollmentDistributionEnvironmentNode env = new EnrollmentDistributionEnvironmentNode();

        env.enrollmentCampaignTitle = enrollmentCampaign.getTitle();

        env.educationYearTitle = enrollmentCampaign.getEducationYear().getTitle();

        env.currentDate = new Date();


        List<Long> ids = new DQLSelectBuilder().fromEntity(NotUsedTargetAdmissionKind.class, "e")
                .column(DQLExpressions.property(NotUsedTargetAdmissionKind.targetAdmissionKind().id().fromAlias("e")))
                .where(DQLExpressions.eq(DQLExpressions.property(NotUsedTargetAdmissionKind.enrollmentCampaign().fromAlias("e")), DQLExpressions.value(enrollmentCampaign)))
                .createStatement(session).list();

        Set<Long> notUsedTargetAdmissionKindIds = new HashSet<Long>(ids);

        for (TargetAdmissionKind row : getList(TargetAdmissionKind.class, new String[]{"code"})) {
            env.targetAdmissionKind.row.add(new EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow(row.getShortTitle(), row.getTitle(), row.getCode(), row.getParent() == null ? null : row.getParent().getCode(), row.getPriority(), !notUsedTargetAdmissionKindIds.contains(row.getId())));
        }

        ids = new DQLSelectBuilder().fromEntity(EnrollmentCompetitionKind.class, "e")
                .column(DQLExpressions.property(EnrollmentCompetitionKind.competitionKind().id().fromAlias("e")))
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentCompetitionKind.enrollmentCampaign().fromAlias("e")), DQLExpressions.value(enrollmentCampaign)))
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentCompetitionKind.used().fromAlias("e")), DQLExpressions.value(Boolean.valueOf(false))))
                .createStatement(session).list();

        Set<Long> notUsedCompetitionKind = new HashSet<Long>(ids);

        for (CompetitionKind row : getList(CompetitionKind.class, new String[]{"code"})) {
            env.competitionKind.row.add(new EnrollmentDistributionEnvironmentNode.CompetitionKindNode.CompetitionKindRow(row.getShortTitle(), row.getTitle(), row.getCode(), !notUsedCompetitionKind.contains(row.getId())));
        }

        ids = new DQLSelectBuilder().fromEntity(NotUsedBenefit.class, "e")
                .column(DQLExpressions.property(NotUsedBenefit.benefit().id().fromAlias("e")))
                .where(DQLExpressions.eq(DQLExpressions.property(NotUsedBenefit.personRoleName().fromAlias("e")), DQLExpressions.value(Entrant.class.getSimpleName())))
                .createStatement(session).list();

        Set<Long> notUsedBenefitIds = new HashSet<Long>(ids);

        for (Benefit row : getList(Benefit.class, new String[]{"code"})) {
            env.benefit.row.add(new EnrollmentDistributionEnvironmentNode.BenefitNode.BenefitRow(row.getShortTitle(), row.getTitle(), row.getCode(), !notUsedBenefitIds.contains(row.getId())));
        }

        ids = new DQLSelectBuilder().fromEntity(EnrollmentOrderType.class, "e")
                .column(DQLExpressions.property(EnrollmentOrderType.entrantEnrollmentOrderType().id().fromAlias("e")))
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentOrderType.enrollmentCampaign().fromAlias("e")), DQLExpressions.value(enrollmentCampaign)))
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentOrderType.used().fromAlias("e")), DQLExpressions.value(Boolean.FALSE)))
                .createStatement(session).list();

        Set<Long> notUsedOrderTypeIds = new HashSet<Long>(ids);

        for (EntrantEnrollmentOrderType row : getList(EntrantEnrollmentOrderType.class, new String[]{"code"})) {
            env.orderType.row.add(new EnrollmentDistributionEnvironmentNode.EnrollmentOrderTypeNode.EnrollmentOrderTypeRow(row.getShortTitle(), row.getTitle(), row.getCode(), !notUsedOrderTypeIds.contains(row.getId())));
        }

        for (EcgDistributionState row : getList(EcgDistributionState.class, new String[]{"code"})) {
            env.distributionState.row.add(new EnrollmentDistributionEnvironmentNode.Row(row.getTitle(), row.getCode()));
        }

        env.documentState.row.add(new EnrollmentDistributionEnvironmentNode.Row("Оригиналы", Integer.toString(IEcgRecommendedDTO.DocumentStatus.BRING_ORIGINAL.ordinal() + 1)));
        env.documentState.row.add(new EnrollmentDistributionEnvironmentNode.Row("Копии", Integer.toString(IEcgRecommendedDTO.DocumentStatus.BRING_COPY.ordinal() + 1)));
        env.documentState.row.add(new EnrollmentDistributionEnvironmentNode.Row("Забрал документы", Integer.toString(IEcgRecommendedDTO.DocumentStatus.TOOK_AWAY.ordinal() + 1)));

        env.enrollmentState.row.add(new EnrollmentDistributionEnvironmentNode.Row("Рекомендован", "1"));
        env.enrollmentState.row.add(new EnrollmentDistributionEnvironmentNode.Row("Выбыл", "2"));
        env.enrollmentState.row.add(new EnrollmentDistributionEnvironmentNode.Row("Зачислен", "3"));

        for (IEcgDistribution distribution : distributionSet) {
            env.distribution.row.add(getDistributionRow(distribution));
        }
        return env;
    }

    private EnrollmentDistributionEnvironmentNode.DistributionNode.DistributionRow getDistributionRow(IEcgDistribution distribution) {
        if (((distribution instanceof EcgDistributionDetail)) && (!distribution.getState().isApproved())) {
            throw new RuntimeException("DistributionServiceDao dose not support not approved distributionDetail.");
        }

        Session session = getSession();
        IEcDistributionDao dao = EcDistributionManager.instance().dao();

        IEcDistributionExtDao daoRMC = (IEcDistributionExtDao) dao;

        EcgDistributionConfig config = distribution.getConfig();
        EnrollmentCampaign enrollmentCampaign = config.getEcgItem().getEnrollmentCampaign();
        List<StudentCategory> studentCategoryList = (List<StudentCategory>) EcDistributionUtil.invokePrivateMethod(this, EnrollmentDistributionServiceDao.class, "getStudentCategoryList", new Object[]{config});

        List<EnrollmentDirection> distributionDirectionList = dao.getDistributionDirectionList(distribution.getDistribution());
        IEcgQuotaDTO currentQuota = dao.getCurrentQuotaDTO(distribution.getDistribution());
        IEcgQuotaDTO beginQuota = distribution.getDistribution().getWave() == 1 ? currentQuota : dao.getCurrentQuotaDTO((EcgDistribution) getByNaturalId(new EcgDistributionGen.NaturalId(config, 1)));

        Map<Long, Integer> currentSRQuota = daoRMC.getCurrentSRQuota(distribution.getDistribution());
        Map<Long, Integer> beginSRQuota = distribution.getDistribution().getWave() == 1 ? currentSRQuota : daoRMC.getCurrentSRQuota((EcgDistribution) getByNaturalId(new EcgDistributionGen.NaturalId(config, 1)));

        List<TargetAdmissionKind> taKindList = currentQuota.getTaKindList();
        boolean defaultTaKind = (taKindList.size() == 1) && (((TargetAdmissionKind) taKindList.get(0)).getCode().equals("1"));

        Map<EnrollmentDirection, List<Discipline2RealizationWayRelation>> disciplineMap = ExamSetUtil.getDirection2DisciplineMap(session, enrollmentCampaign, studentCategoryList, config.getCompensationType());
        Set<Discipline2RealizationWayRelation> disciplineSet = new HashSet<Discipline2RealizationWayRelation>();
        for (EnrollmentDirection enrollmentDirection : distributionDirectionList)
            disciplineSet.addAll(disciplineMap.get(enrollmentDirection));
        List<Discipline2RealizationWayRelation> disciplineList = new ArrayList<Discipline2RealizationWayRelation>(disciplineSet);

		/*
		 * вместо дисциплины группа дисциплин по выбору
		 * делаем следующее:
		 * для всех дисциплин извлекаем группы, в которые они входят 
		 * эти группы добавляем в список, для выборки приоритетов
		 * делаем мап Дисциплина - Группа
		 */

        DQLSelectBuilder groupDiscBuilder = new DQLSelectBuilder().fromEntity(Group2DisciplineRelation.class, "gr")
                .where(DQLExpressions.in(DQLExpressions.property(Group2DisciplineRelation.discipline().fromAlias("gr")), disciplineList))
                .column(Group2DisciplineRelation.group().fromAlias("gr").s())
                .column(Group2DisciplineRelation.discipline().fromAlias("gr").s());

        List<Object[]> objects = getList(groupDiscBuilder);
        List<SetDiscipline> discGoupList = new ArrayList<SetDiscipline>(disciplineList);
        //map <Discipline id,Group id>
        final Map<Long, Long> disc2groupMap = new HashMap<Long, Long>();
        for (Object[] obj : objects) {
            DisciplinesGroup group = (DisciplinesGroup) obj[0];
            Discipline2RealizationWayRelation discipline = (Discipline2RealizationWayRelation) obj[1];
            if (disciplineList.contains(discipline)) {
                disc2groupMap.put(discipline.getId(), group.getId());
                discGoupList.add(group);
            }
        }

        DQLSelectBuilder edPriorityBuilder = new DQLSelectBuilder().fromEntity(EntranceDisciplineExt.class, "ext")
                .where(DQLExpressions.in(DQLExpressions.property(EntranceDisciplineExt.entranceDiscipline().enrollmentDirection().fromAlias("ext")), distributionDirectionList))
                .where(DQLExpressions.in(DQLExpressions.property(EntranceDisciplineExt.entranceDiscipline().discipline().fromAlias("ext")), discGoupList))
                .column(DQLExpressions.property(EntranceDisciplineExt.entranceDiscipline().discipline().id().fromAlias("ext")))
                .column(DQLExpressions.property(EntranceDisciplineExt.priority().fromAlias("ext")));

        List<Object[]> objs = getList(edPriorityBuilder);

        final Map<Long, Integer> entranceDisciplinePriorityMap = new HashMap<Long, Integer>();

        for (Object[] obj : objs) {
            entranceDisciplinePriorityMap.put((Long) obj[0], (Integer) obj[1]);
        }
//		Collections.sort(disciplineList, ITitled.TITLED_COMPARATOR);

        Collections.sort(disciplineList, new Comparator<Discipline2RealizationWayRelation>() {

            @Override
            public int compare(Discipline2RealizationWayRelation o1,
                               Discipline2RealizationWayRelation o2)
            {
				/*
				 * если в мапе приоритетов нет дисциплины - значит там группа
				 * по дисицплине берем группу и забираем приоритет
				 */
                int priority1 = entranceDisciplinePriorityMap.get(o1.getId()) == null ? entranceDisciplinePriorityMap.get(disc2groupMap.get(o1.getId())).intValue() : entranceDisciplinePriorityMap.get(o1.getId()).intValue();
                int priority2 = entranceDisciplinePriorityMap.get(o2.getId()) == null ? entranceDisciplinePriorityMap.get(disc2groupMap.get(o2.getId())).intValue() : entranceDisciplinePriorityMap.get(o2.getId()).intValue();

                return priority1 - priority2;
            }
        });


        List<IEcgCortegeDTO> cortegeDTOList = new ArrayList<IEcgCortegeDTO>();
        List<Entrant> nonActualEntrantList = new ArrayList<Entrant>();
        Comparator<IEcgCortegeDTO> comparator = daoRMC.getComparator(distributionDirectionList.get(0));

        MQBuilder builder = (MQBuilder) EcDistributionUtil.invokePrivateMethod(this, EnrollmentDistributionServiceDao.class, "getDistributionBuilder", new Object[]{distribution});
        EntrantDataUtil dataUtil = new EntrantDataUtil(session, distribution.getConfig().getEcgItem().getEnrollmentCampaign(), builder);
        Map<Person, Set<Benefit>> benefitMap = EntrantDataUtil.getBenefitMap(session, builder);
        Map<Entrant, RequestedEnrollmentDirection> entrantDirectionMap = getEntrantDirectionMap(distribution, comparator);
        Map<EcgEntrantRecommended, IEcgHasBringOriginal> recommendedStateMap = dao.getEntrantRecommendedWithPreStudentList(distribution.getDistribution());
        Map<Long, PreliminaryEnrollmentStudent> entrantPreStudentMap = dao.getEntrantPreStudentMap(distribution);

        Map<Long, EcgPriorityInfo> entrantPrioritInfoyMap = fillCortegeDTOList(distribution, dataUtil, cortegeDTOList, nonActualEntrantList, dao, entrantDirectionMap, comparator);

        Map<Long, EnrollmentDirection> ouId2directionMap = new HashMap<Long, EnrollmentDirection>();
        for (EnrollmentDirection direction : EcDistributionManager.instance().dao().getDistributionDirectionList(distribution.getDistribution())) {
            if (null != ouId2directionMap.put(direction.getEducationOrgUnit().getId(), direction)) {
                throw new RuntimeException("Dublicate educationOrgUnit '" + direction.getEducationOrgUnit().getId() + "' in distribution '" + distribution.getDistribution().getId() + "'");
            }
        }

        EnrollmentDistributionEnvironmentNode.DistributionNode.DistributionRow row = new EnrollmentDistributionEnvironmentNode.DistributionNode.DistributionRow();

        row.ecgItemId = config.getEcgItem().getId().toString();
        row.ecgItemTitle = config.getEcgItem().getTitle();
        row.compensationTypeId = config.getCompensationType().getCode();
        row.secondHighAdmission = config.isSecondHighAdmission();
        row.wave = distribution.getDistribution().getWave();
        row.distributionDetail = (distribution instanceof EcgDistributionDetail);
        row.state = distribution.getState().getCode();

        for (Discipline2RealizationWayRelation discipline : disciplineList) {
            row.discipline.row.add(new EnrollmentDistributionEnvironmentNode.DisciplineNode.DisciplineRow(discipline.getTitle(), discipline.getShortTitle(), discipline.getId().toString()));
        }

        for (EnrollmentDirection enrollmentDirection : distributionDirectionList) {
            Long enrollmentDirectionId = enrollmentDirection.getId();
            int current = ((Integer) currentQuota.getQuotaMap().get(enrollmentDirectionId)).intValue();
            int begin = ((Integer) beginQuota.getQuotaMap().get(enrollmentDirectionId)).intValue();
            Map<Long, Integer> currentTaMap = currentQuota.getTaQuotaMap().get(enrollmentDirectionId);
            Map<Long, Integer> beginTaMap = beginQuota.getTaQuotaMap().get(enrollmentDirectionId);
            int currentSR = currentSRQuota.get(enrollmentDirectionId).intValue();
            int beginSR = beginSRQuota.get(enrollmentDirectionId).intValue();

            EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow.TaQuotaNode taQuotaNode = new EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow.TaQuotaNode();
            for (TargetAdmissionKind taKind : taKindList)
                taQuotaNode.row.add(new EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow.TaQuotaNode.TaQuotaRow(taKind.getCode(), ((Integer) currentTaMap.get(taKind.getId())).intValue(), ((Integer) beginTaMap.get(taKind.getId())).intValue()));

            EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow quotaRow = new EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow(enrollmentDirectionId.toString(), current, begin, defaultTaKind, taQuotaNode);
            QuotaRowRMC.SrQuotaRow srQuotaRow = new QuotaRowRMC.SrQuotaRow(enrollmentDirectionId.toString(), currentSR, beginSR);

            row.quota.row.add(new QuotaRowRMC(quotaRow, srQuotaRow));
        }

        Map<Entrant, EnrollmentDistributionEnvironmentNode.EntrantNode.EntrantRow> entrantRowMap = new Hashtable<Entrant, EnrollmentDistributionEnvironmentNode.EntrantNode.EntrantRow>();

        for (IEcgCortegeDTO cortegeDTO : cortegeDTOList) {
            RequestedEnrollmentDirection direction = (RequestedEnrollmentDirection) EcDistributionUtil.invokePrivateMethod(this, EnrollmentDistributionServiceDao.class, "getDirectionFromCortegeDTO", new Object[]{cortegeDTO, entrantDirectionMap});
            EntrantRequest entrantRequest = direction.getEntrantRequest();
            Entrant entrant = entrantRequest.getEntrant();
            EcgPriorityInfo priorityInfo = (EcgPriorityInfo) entrantPrioritInfoyMap.get(entrant.getId());
            PreliminaryEnrollmentStudent preStudent = (PreliminaryEnrollmentStudent) entrantPreStudentMap.get(entrant.getId());
            EnrollmentDirection preEnrollDirection = preStudent == null ? null : (EnrollmentDirection) ouId2directionMap.get(preStudent.getEducationOrgUnit().getId());

            EntrantRowRMC entrantRow = new EntrantRowRMC();

            entrantRow.preEnrollId = (preEnrollDirection == null ? null : preEnrollDirection.getId().toString());
            entrantRow.priorityIds = (priorityInfo == null ? null : priorityInfo.getPriorityIdList());
            entrantRow.fio = cortegeDTO.getFio();
            entrantRow.competitionKind = cortegeDTO.getCompetitionKind().getCode().toString();

            Set<Benefit> benefitSet = benefitMap.get(entrant.getPerson());
            if (benefitSet != null) {
                entrantRow.benefitList = new ArrayList<String>();
                for (Benefit benefit : benefitSet)
                    entrantRow.benefitList.add(benefit.getCode());
                Collections.sort(entrantRow.benefitList);
            }

            entrantRow.averageEduInstitutionMark = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(cortegeDTO.getCertificateAverageMark());
            entrantRow.profileMark = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(cortegeDTO.getProfileMark());
            entrantRow.finalMark = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(cortegeDTO.getFinalMark());
            entrantRow.marks = new ArrayList<String>();

            Map<Discipline2RealizationWayRelation, Double> markMap = dataUtil.getMarkMap(direction);
            for (Discipline2RealizationWayRelation discipline : disciplineList) {
                Double mark = (Double) markMap.get(discipline);
                if (mark == null)
                    entrantRow.marks.add("x");
                else if (mark.doubleValue() == -1.0D)
                    entrantRow.marks.add("-");
                else {
                    entrantRow.marks.add(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(mark));
                }
            }
            entrantRow.targetAdmissionKind = (cortegeDTO.getTargetAdmissionKind() == null ? null : cortegeDTO.getTargetAdmissionKind().getCode());

            entrantRow.haveSpecialRights = ((EcgCortegeDTORMC) cortegeDTO).isHaveSpecialRights();
            entrantRow.recommendationTitle = ((EcgCortegeDTORMC) cortegeDTO).getRecommendationTitle();

            IEcgRecommendedDTO.DocumentStatus documentStatus = (IEcgRecommendedDTO.DocumentStatus) EcDistributionUtil.invokePrivateMethod(this, EnrollmentDistributionServiceDao.class, "getDocumentStatus", new Object[]{distribution, cortegeDTO, recommendedStateMap, entrantDirectionMap});
            entrantRow.documentState = Integer.toString(documentStatus.ordinal() + 1);
            entrantRow.enrollmentState = (String) EcDistributionUtil.invokePrivateMethod(this, EnrollmentDistributionServiceDao.class, "getEnrollmentStatus", new Object[]{distribution, cortegeDTO, recommendedStateMap, entrantPreStudentMap.keySet()});

            if ("3".equals(entrantRow.enrollmentState)) {
                entrantRowMap.put(entrant, entrantRow);
            }
            entrantRow.enrollmentDirection = direction.getEnrollmentDirection().getId().toString();
            entrantRow.requestNumber = entrantRequest.getStringNumber();
            entrantRow.regNumber = direction.getStringNumber();
            entrantRow.entrantId = entrant.getId().toString();

            row.entrant.row.add(entrantRow);
        }

        List<EnrollmentExtract> extracts = new DQLSelectBuilder().fromEntity(EnrollmentExtract.class, "e")
                .column("e")
                .where(DQLExpressions.in(DQLExpressions.property(EnrollmentExtract.entity().requestedEnrollmentDirection().entrantRequest().entrant().fromAlias("e")), entrantRowMap.keySet()))
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentExtract.entity().compensationType().fromAlias("e")), DQLExpressions.value(config.getCompensationType())))
                .where(DQLExpressions.in(DQLExpressions.property(EnrollmentExtract.entity().studentCategory().fromAlias("e")), (List<StudentCategory>) EcDistributionUtil.invokePrivateMethod(this, EnrollmentDistributionServiceDao.class, "getStudentCategoryList", new Object[]{config})))
                .where(DQLExpressions.in(DQLExpressions.property(EnrollmentExtract.entity().educationOrgUnit().id().fromAlias("e")),
                                         new DQLSelectBuilder().fromEntity(EcgDistributionQuota.class, "q")
                                                 .column(DQLExpressions.property(EcgDistributionQuota.direction().educationOrgUnit().id().fromAlias("q")))
                                                 .where(DQLExpressions.eq(DQLExpressions.property(EcgDistributionQuota.distribution().fromAlias("q")), DQLExpressions.value(distribution.getDistribution())))
                                                 .buildQuery()))
                .createStatement(session).list();

        for (EnrollmentExtract extract : extracts) {
            EnrollmentDistributionEnvironmentNode.EntrantNode.EntrantRow entrantRow = (EnrollmentDistributionEnvironmentNode.EntrantNode.EntrantRow) entrantRowMap.get(extract.getEntity().getRequestedEnrollmentDirection().getEntrantRequest().getEntrant());

            entrantRow.orderId = extract.getOrder().getId().toString();
            entrantRow.orderNumber = extract.getOrder().getNumber();
            entrantRow.orderType = extract.getOrder().getType().getCode();
        }

        for (Entrant entrant : nonActualEntrantList) {
            EnrollmentDistributionEnvironmentNode.EntrantNode.EntrantRow entrantRow = new EnrollmentDistributionEnvironmentNode.EntrantNode.EntrantRow();

            entrantRow.fio = entrant.getPerson().getIdentityCard().getFullFio();
            entrantRow.entrantId = entrant.getId().toString();

            row.entrant.row.add(entrantRow);
        }

        return row;
    }

    private Map<Long, EcgPriorityInfo> fillCortegeDTOList(IEcgDistribution distribution, EntrantDataUtil dataUtil, List<IEcgCortegeDTO> cortegeDTOList, List<Entrant> nonActualEntrantList, IEcDistributionDao dao, Map<Entrant, RequestedEnrollmentDirection> entrantDirectionMap, Comparator<IEcgCortegeDTO> comparator) {
        Session session = getSession();

        EcgDistributionConfig config = distribution.getConfig();

        Set<Long> usedEntrantIds = new HashSet<Long>();
        List<Long> directionIds = new ArrayList<Long>();
        List<Long> entrantIds = new ArrayList<Long>();
        Map<IEcgEntrantRecommended, Boolean> haveSRMap = new HashMap<IEcgEntrantRecommended, Boolean>();
        Map<Long, List<EnrollmentRecommendation>> recommendMap = new HashMap<Long, List<EnrollmentRecommendation>>();
        Map<Long, Integer> enrPriorityMap = new HashMap<Long, Integer>();
        Map<Long, List<WrapChoseEntranseDiscipline>> edDiscMap = new HashMap<>();

        if ((distribution instanceof EcgDistributionDetail)) {

            List<EcgEntrantRecommendedDetail> recommendedDetails = new DQLSelectBuilder().fromEntity(EcgEntrantRecommendedDetail.class, "e")
                    .column("e")
                    .where(DQLExpressions.eq(DQLExpressions.property(EcgEntrantRecommendedDetail.distribution().fromAlias("e")), DQLExpressions.value(distribution)))
                    .createStatement(session).list();

            for (EcgEntrantRecommendedDetail recommendedDetail : recommendedDetails) {
                directionIds.add(recommendedDetail.getDirection().getId());
                entrantIds.add(recommendedDetail.getDirection().getEntrantRequest().getEntrant().getId());
            }

            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EcgEntrantRecommendedDetailExt.class, "ext")
                    .where(DQLExpressions.in(DQLExpressions.property(EcgEntrantRecommendedDetailExt.ecgEntrantRecommendedDetail().fromAlias("ext")), recommendedDetails));
            List<EcgEntrantRecommendedDetailExt> lst = getList(builder);
            for (EcgEntrantRecommendedDetailExt ext : lst)
                haveSRMap.put(ext.getEcgEntrantRecommendedDetail(), ext.isHaveSpecialRights());

            recommendMap = EcDistributionUtil.getRecommendationMap(entrantIds);
            enrPriorityMap = EcDistributionUtil.getEnrollmentPriorityMap(entrantIds);
            edDiscMap = EcDistributionUtil.getChosenDisciplineMap(directionIds);

            for (EcgEntrantRecommendedDetail recommendedDetail : recommendedDetails) {
                if (usedEntrantIds.add(recommendedDetail.getDirection().getEntrantRequest().getEntrant().getId())) {
                    if (dao.isConfigAllowDirection(config, recommendedDetail.getDirection())) {
                        if (!dataUtil.getDirectionSet().contains(recommendedDetail.getDirection())) {
                            throw new RuntimeException("Invalid EntrantDataUtil, no direction '" + recommendedDetail.getDirection().getId() + "' for distribution '" + distribution.getId() + "'");
                        }
                        cortegeDTOList.add(new EcgCortegeDTORMC(recommendedDetail, recommendedDetail.getTargetAdmissionKind() != null, recommendedDetail.getTargetAdmissionKind(),
                                                                recommendedDetail.getDirection().getCompetitionKind(), Double.valueOf(dataUtil.getFinalMark(recommendedDetail.getDirection())),
                                                                recommendedDetail.getDirection().getProfileChosenEntranceDiscipline() == null ? null : recommendedDetail.getDirection().getProfileChosenEntranceDiscipline().getFinalMark(),
                                                                recommendedDetail.getDirection().isGraduatedProfileEduInstitution(), recommendedDetail.getDirection().getEntrantRequest().getEntrant().getPerson().getPersonEduInstitution() == null ? null : recommendedDetail.getDirection().getEntrantRequest().getEntrant().getPerson().getPersonEduInstitution().getAverageMark(),
                                                                recommendedDetail.getDirection().getEntrantRequest().getEntrant().getPerson().getIdentityCard().getFullFio(),
                                                                recommendMap.get(recommendedDetail.getDirection().getEntrantRequest().getEntrant().getId()),
                                                                edDiscMap.get(recommendedDetail.getDirection().getId()),
                                                                haveSRMap.get(recommendedDetail),
                                                                enrPriorityMap.get(recommendedDetail.getDirection().getEntrantRequest().getEntrant().getId())
                        ));
                    }
                    else
                        nonActualEntrantList.add(recommendedDetail.getDirection().getEntrantRequest().getEntrant());
                }
            }
        }

        List<EcgEntrantRecommended> recommendeds = new DQLSelectBuilder().fromEntity(EcgEntrantRecommended.class, "e")
                .column("e")
                .where(DQLExpressions.eq(DQLExpressions.property(EcgEntrantRecommended.distribution().config().fromAlias("e")), DQLExpressions.value(config)))
                .where(DQLExpressions.le(DQLExpressions.property(EcgEntrantRecommended.distribution().wave().fromAlias("e")), DQLExpressions.value(Integer.valueOf(distribution.getDistribution().getWave()))))
                .order(DQLExpressions.property(EcgEntrantRecommended.distribution().wave().fromAlias("e")), OrderDirection.desc)
                .createStatement(session).list();

        for (EcgEntrantRecommended recommended : recommendeds) {
            directionIds.add(recommended.getDirection().getId());
            entrantIds.add(recommended.getDirection().getEntrantRequest().getEntrant().getId());
        }

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EcgEntrantRecommendedExt.class, "ext")
                .where(DQLExpressions.in(DQLExpressions.property(EcgEntrantRecommendedExt.ecgEntrantRecommended().fromAlias("ext")), recommendeds));
        List<EcgEntrantRecommendedExt> lst = getList(builder);
        for (EcgEntrantRecommendedExt ext : lst)
            haveSRMap.put(ext.getEcgEntrantRecommended(), ext.isHaveSpecialRights());

        recommendMap = EcDistributionUtil.getRecommendationMap(entrantIds);
        enrPriorityMap = EcDistributionUtil.getEnrollmentPriorityMap(entrantIds);
        edDiscMap = EcDistributionUtil.getChosenDisciplineMap(directionIds);

        for (EcgEntrantRecommended recommended : recommendeds) {
            if (usedEntrantIds.add(recommended.getDirection().getEntrantRequest().getEntrant().getId())) {
                if (dao.isConfigAllowDirection(config, recommended.getDirection())) {
                    if (!dataUtil.getDirectionSet().contains(recommended.getDirection())) {
                        throw new RuntimeException("Invalid EntrantDataUtil, no direction '" + recommended.getDirection().getId() + "' for distribution '" + distribution.getId() + "'");
                    }
                    cortegeDTOList.add(new EcgCortegeDTORMC(recommended, recommended.getTargetAdmissionKind() != null, recommended.getTargetAdmissionKind(),
                                                            recommended.getDirection().getCompetitionKind(), Double.valueOf(dataUtil.getFinalMark(recommended.getDirection())),
                                                            recommended.getDirection().getProfileChosenEntranceDiscipline() == null ? null : recommended.getDirection().getProfileChosenEntranceDiscipline().getFinalMark(),
                                                            recommended.getDirection().isGraduatedProfileEduInstitution(), recommended.getDirection().getEntrantRequest().getEntrant().getPerson().getPersonEduInstitution() == null ? null : recommended.getDirection().getEntrantRequest().getEntrant().getPerson().getPersonEduInstitution().getAverageMark(),
                                                            recommended.getDirection().getEntrantRequest().getEntrant().getPerson().getIdentityCard().getFullFio(),
                                                            recommendMap.get(recommended.getDirection().getEntrantRequest().getEntrant().getId()),
                                                            edDiscMap.get(recommended.getDirection().getId()),
                                                            haveSRMap.get(recommended),
                                                            enrPriorityMap.get(recommended.getDirection().getEntrantRequest().getEntrant().getId())
                    ));
                }
                else
                    nonActualEntrantList.add(recommended.getDirection().getEntrantRequest().getEntrant());
            }
        }

        for (Map.Entry<Entrant, RequestedEnrollmentDirection> entry : entrantDirectionMap.entrySet()) {
            directionIds.add(entry.getValue().getId());
            entrantIds.add(entry.getKey().getId());
        }

        recommendMap = EcDistributionUtil.getRecommendationMap(entrantIds);
        enrPriorityMap = EcDistributionUtil.getEnrollmentPriorityMap(entrantIds);
        edDiscMap = EcDistributionUtil.getChosenDisciplineMap(directionIds);

        if (distribution.getDistribution().getState().isLockedOrApproved()) {

            List<EcgEntrantReserved> entrantReserveds = new DQLSelectBuilder().fromEntity(EcgEntrantReserved.class, "e")
                    .column("e")
                    .where(DQLExpressions.eq(DQLExpressions.property(EcgEntrantReserved.distribution().fromAlias("e")), DQLExpressions.value(distribution.getDistribution())))
                    .createStatement(session).list();

            for (EcgEntrantReserved reserved : entrantReserveds) {
                RequestedEnrollmentDirection direction = (RequestedEnrollmentDirection) entrantDirectionMap.get(reserved.getEntrant());

                if (usedEntrantIds.add(reserved.getEntrant().getId())) {
                    if ((direction != null) && (dao.isConfigAllowDirection(config, direction))) {
                        if (!dataUtil.getDirectionSet().contains(direction)) {
                            throw new RuntimeException("Invalid EntrantDataUtil, no direction '" + direction.getId() + "' for distribution '" + distribution.getId() + "'");
                        }
                        cortegeDTOList.add(new EcgCortegeDTORMC(reserved, false, null, direction.getCompetitionKind(), Double.valueOf(dataUtil.getFinalMark(direction)),
                                                                direction.getProfileChosenEntranceDiscipline() == null ? null : direction.getProfileChosenEntranceDiscipline().getFinalMark(),
                                                                direction.isGraduatedProfileEduInstitution(), reserved.getEntrant().getPerson().getPersonEduInstitution() == null ? null : reserved.getEntrant().getPerson().getPersonEduInstitution().getAverageMark(),
                                                                reserved.getEntrant().getPerson().getIdentityCard().getFullFio(),
                                                                recommendMap.get(reserved.getEntrant().getId()),
                                                                edDiscMap.get(direction.getId()),
                                                                Boolean.FALSE,
                                                                enrPriorityMap.get(reserved.getEntrant().getId())
                        ));
                    }
                    else
                        nonActualEntrantList.add(reserved.getEntrant());
                }
            }
        }
        else {
            for (RequestedEnrollmentDirection direction : entrantDirectionMap.values()) {
                if (usedEntrantIds.add(direction.getEntrantRequest().getEntrant().getId())) {
                    if (dao.isConfigAllowDirection(config, direction)) {
                        if (!dataUtil.getDirectionSet().contains(direction)) {
                            throw new RuntimeException("Invalid EntrantDataUtil, no direction '" + direction.getId() + "' for distribution '" + distribution.getId() + "'");
                        }
                        cortegeDTOList.add(new EcgCortegeDTORMC(direction, false, null, direction.getCompetitionKind(), Double.valueOf(dataUtil.getFinalMark(direction)),
                                                                direction.getProfileChosenEntranceDiscipline() == null ? null : direction.getProfileChosenEntranceDiscipline().getFinalMark(),
                                                                direction.isGraduatedProfileEduInstitution(), direction.getEntrantRequest().getEntrant().getPerson().getPersonEduInstitution() == null ? null : direction.getEntrantRequest().getEntrant().getPerson().getPersonEduInstitution().getAverageMark(),
                                                                direction.getEntrantRequest().getEntrant().getPerson().getIdentityCard().getFullFio(),
                                                                recommendMap.get(direction.getEntrantRequest().getEntrant().getId()),
                                                                edDiscMap.get(direction.getId()),
                                                                Boolean.FALSE,
                                                                enrPriorityMap.get(direction.getEntrantRequest().getEntrant().getId())
                        ));
                    }
                    else
                        nonActualEntrantList.add(direction.getEntrantRequest().getEntrant());
                }
            }

        }

        if (distribution.getDistribution().getWave() > 1) {

            List<EcgEntrantReserved> entrantReserveds = new DQLSelectBuilder().fromEntity(EcgEntrantReserved.class, "e")
                    .column("e")
                    .where(DQLExpressions.eq(DQLExpressions.property(EcgEntrantReserved.distribution().config().fromAlias("e")), DQLExpressions.value(config)))
                    .where(DQLExpressions.eq(DQLExpressions.property(EcgEntrantReserved.distribution().wave().fromAlias("e")), DQLExpressions.value(Integer.valueOf(1))))
                    .createStatement(session).list();

            for (EcgEntrantReserved reserved : entrantReserveds) {
                if (usedEntrantIds.add(reserved.getEntrant().getId()))
                    nonActualEntrantList.add(reserved.getEntrant());
            }

        }

        Collections.sort(cortegeDTOList, comparator);

        Collections.sort(nonActualEntrantList, new Comparator<Entrant>() {
            public int compare(Entrant o1, Entrant o2) {
                return o1.getPerson().getIdentityCard().getFullFio().compareTo(o2.getPerson().getIdentityCard().getFullFio());
            }
        });

        return EcDistributionManager.instance().dao().getEntrantPriorityMap(distribution, usedEntrantIds);
    }


    private Map<Entrant, RequestedEnrollmentDirection> getEntrantDirectionMap(IEcgDistribution distribution, Comparator<IEcgCortegeDTO> comparator) {
        Session session = getSession();

        List<StudentCategory> studentCategoryList = (List<StudentCategory>) EcDistributionUtil.invokePrivateMethod(this, EnrollmentDistributionServiceDao.class, "getStudentCategoryList", new Object[]{distribution.getConfig()});

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "e")
                .where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.compensationType().fromAlias("e")), DQLExpressions.value(distribution.getConfig().getCompensationType())))
                .where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirection.studentCategory().fromAlias("e")), studentCategoryList))
                .where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().id().fromAlias("e")),
                                         new DQLSelectBuilder().fromEntity(EcgDistributionQuota.class, "q")
                                                 .column(DQLExpressions.property(EcgDistributionQuota.direction().id().fromAlias("q")))
                                                 .where(DQLExpressions.eq(DQLExpressions.property(EcgDistributionQuota.distribution().fromAlias("q")), DQLExpressions.value(distribution.getDistribution())))
                                                 .buildQuery()));

        if (distribution.getDistribution().getState().isLockedOrApproved())
            builder.where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("e")),
                                            new DQLSelectBuilder().fromEntity(EcgEntrantReserved.class, "r")
                                                    .column(DQLExpressions.property(EcgEntrantReserved.entrant().id().fromAlias("r")))
                                                    .where(DQLExpressions.eq(DQLExpressions.property(EcgEntrantReserved.distribution().fromAlias("r")), DQLExpressions.value(distribution.getDistribution())))
                                                    .buildQuery()));
        else {
            builder.where(DQLExpressions.notIn(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("e")),
                                               new DQLSelectBuilder().fromEntity(EcgEntrantRecommended.class, "r2")
                                                       .column(DQLExpressions.property(EcgEntrantRecommended.direction().entrantRequest().entrant().id().fromAlias("r2")))
                                                       .where(DQLExpressions.eq(DQLExpressions.property(EcgEntrantRecommended.distribution().fromAlias("r2")), DQLExpressions.value(distribution.getDistribution())))
                                                       .buildQuery()));

            if (distribution.getDistribution().getWave() == 1) {
                builder.where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().entrant().archival().fromAlias("e")), DQLExpressions.value(Boolean.FALSE)));
                builder.where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.state().fromAlias("e")), DQLExpressions.commonValue(getByNaturalId(new EntrantStateGen.NaturalId("4")))));

                builder.where(DQLExpressions.notIn(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("e")),
                                                   new DQLSelectBuilder().fromEntity(PreliminaryEnrollmentStudent.class, "p")
                                                           .column(DQLExpressions.property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().entrantRequest().entrant().id().fromAlias("p")))
                                                           .where(DQLExpressions.eq(DQLExpressions.property(PreliminaryEnrollmentStudent.compensationType().fromAlias("p")), DQLExpressions.value(distribution.getConfig().getCompensationType())))
                                                           .where(DQLExpressions.in(DQLExpressions.property(PreliminaryEnrollmentStudent.studentCategory().fromAlias("p")), studentCategoryList))
                                                           .where(DQLExpressions.in(DQLExpressions.property(PreliminaryEnrollmentStudent.educationOrgUnit().id().fromAlias("p")),
                                                                                    new DQLSelectBuilder().fromEntity(EcgDistributionQuota.class, "q1")
                                                                                            .column(DQLExpressions.property(EcgDistributionQuota.direction().educationOrgUnit().id().fromAlias("q1")))
                                                                                            .where(DQLExpressions.eq(DQLExpressions.property(EcgDistributionQuota.distribution().fromAlias("q1")), DQLExpressions.value(distribution.getDistribution())))
                                                                                            .buildQuery()))
                                                           .buildQuery()));
            }
            else if (distribution.getConfig().getCompensationType().isBudget())
                builder.where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("e")), ((DQLSelectBuilder) ((DQLSelectBuilder) ((DQLSelectBuilder) ((DQLSelectBuilder) new DQLSelectBuilder().fromEntity(EcgEntrantReserved.class, "r")).column(DQLExpressions.property(EcgEntrantReserved.entrant().id().fromAlias("r")))).where(DQLExpressions.eq(DQLExpressions.property(EcgEntrantReserved.distribution().wave().fromAlias("r")), DQLExpressions.value(Integer.valueOf(distribution.getDistribution().getWave() - 1))))).where(DQLExpressions.eq(DQLExpressions.property(EcgEntrantReserved.distribution().config().fromAlias("r")), DQLExpressions.value(distribution.getConfig())))).getQuery()));

        }

        List<RequestedEnrollmentDirection> directionList = builder.createStatement(session).list();
        List<Long> directionIds = new ArrayList<Long>();
        List<Long> entrantIds = new ArrayList<Long>();
        for (RequestedEnrollmentDirection direction : directionList) {
            directionIds.add(direction.getId());
            entrantIds.add(direction.getEntrantRequest().getEntrant().getId());
        }

        int REQUESTED_ENROLLMENT_DIRECTION_ID_IX = 0;
        int FINAL_MARK_IX = 1;

        builder.joinEntity("e", DQLJoinType.inner, ChosenEntranceDiscipline.class, "c", DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.id().fromAlias("e")), DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("c"))));
        builder.where(DQLExpressions.isNotNull(ChosenEntranceDiscipline.finalMark().fromAlias("c")));
        builder.group(DQLExpressions.property(RequestedEnrollmentDirection.id().fromAlias("e")));
        builder.column(DQLExpressions.property(RequestedEnrollmentDirection.id().fromAlias("e")));
        builder.column(DQLFunctions.sum(DQLExpressions.property(ChosenEntranceDiscipline.finalMark().fromAlias("c"))));

        Map<Long, MutableDouble> finalMarkMap = new Hashtable<Long, MutableDouble>();
        List<Object[]> objs = builder.createStatement(session).list();
        for (Object[] dataRow : objs) {
            Long requestedEnrollmentDirectionId = (Long) dataRow[REQUESTED_ENROLLMENT_DIRECTION_ID_IX];
            Double finalMark = (Double) dataRow[FINAL_MARK_IX];
            if (null == finalMark)
                finalMark = Double.valueOf(0.0D);

            MutableDouble mutable = (MutableDouble) finalMarkMap.get(requestedEnrollmentDirectionId);
            if (mutable == null)
                finalMarkMap.put(requestedEnrollmentDirectionId, mutable = new MutableDouble());
            mutable.add(finalMark);
        }

        Map<Long, List<EnrollmentRecommendation>> recommendMap = EcDistributionUtil.getRecommendationMap(entrantIds);
        Map<Long, Integer> enrPriorityMap = EcDistributionUtil.getEnrollmentPriorityMap(entrantIds);
        Map<Long, List<WrapChoseEntranseDiscipline>> edDiscMap = EcDistributionUtil.getChosenDisciplineMap(directionIds);

        Map<Entrant, List<IEcgCortegeDTO>> directionMap = new Hashtable<Entrant, List<IEcgCortegeDTO>>();
        for (RequestedEnrollmentDirection direction : directionList) {
            List<IEcgCortegeDTO> list = directionMap.get(direction.getEntrantRequest().getEntrant());
            if (list == null)
                directionMap.put(direction.getEntrantRequest().getEntrant(), list = new ArrayList<IEcgCortegeDTO>());

            Person person = direction.getEntrantRequest().getEntrant().getPerson();
            PersonEduInstitution eduInstitution = person.getPersonEduInstitution();

            MutableDouble finalMark = (MutableDouble) finalMarkMap.get(direction.getId());

            list.add(new EcgCortegeDTORMC(direction, false, null, direction.getCompetitionKind(), Double.valueOf(finalMark == null ? 0.0D : finalMark.doubleValue()), direction.getProfileChosenEntranceDiscipline() == null ? null : direction.getProfileChosenEntranceDiscipline().getFinalMark(), direction.isGraduatedProfileEduInstitution(), eduInstitution == null ? null : eduInstitution.getAverageMark(), person.getIdentityCard().getFullFio(), recommendMap.get(direction.getEntrantRequest().getEntrant().getId()), edDiscMap.get(direction.getId()), Boolean.FALSE, enrPriorityMap.get(direction.getEntrantRequest().getEntrant().getId())));
        }

        Map<Entrant, RequestedEnrollmentDirection> resultMap = new Hashtable<Entrant, RequestedEnrollmentDirection>();

        for (Map.Entry<Entrant, List<IEcgCortegeDTO>> entry : directionMap.entrySet()) {
            Collections.sort(entry.getValue(), comparator);
            resultMap.put(entry.getKey(), (RequestedEnrollmentDirection) entry.getValue().get(0).getEntity());
        }

        return resultMap;
    }


}
