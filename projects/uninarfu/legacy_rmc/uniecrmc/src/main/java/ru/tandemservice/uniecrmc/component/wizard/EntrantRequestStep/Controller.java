package ru.tandemservice.uniecrmc.component.wizard.EntrantRequestStep;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.util.EntrantRequestAddEditUtil;

import java.util.List;
import java.util.Set;

public class Controller extends ru.tandemservice.uniec.component.entrant.EntrantRequestAddEdit.Controller {

    @Override
    public void onClickDelete(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        Long id = (Long) component.getListenerParameter();
        Set<Long> existedIds = model.getExistedRequestedEnrollmentDirectionIds();
        List<RequestedEnrollmentDirection> forDeleteList = model.getForDelete();
        RequestedEnrollmentDirection forDelete = EntrantRequestAddEditUtil.processClickDelete(model.getSelectedRequestedEnrollmentDirectionList(), id, existedIds, forDeleteList);
        if (forDelete != null) {
            model.getProfileMarkMap().remove(forDelete.getId());
            model.getDirecrionExtMap().remove(forDelete.getId());
        }
    }
}
