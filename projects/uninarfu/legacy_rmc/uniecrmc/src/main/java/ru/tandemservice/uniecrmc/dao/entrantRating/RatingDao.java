package ru.tandemservice.uniecrmc.dao.entrantRating;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import org.tandemframework.shared.person.catalog.entity.Benefit;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.logic.IEcDistributionExtDao;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.EcDistributionUtil;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.WrapChoseEntranseDiscipline;
import ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionExt;
import ru.tandemservice.uniecrmc.entity.entrant.EntranceDisciplineExt;
import ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uniec.base.bo.EcDistribution.EcDistributionManager;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EnrollmentRecommendation;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.orders.AbstractEntrantExtract;
import ru.tandemservice.uniec.entity.settings.EnrollmentCompetitionKind;
import ru.tandemservice.uniec.entity.settings.NotUsedTargetAdmissionKind;
import ru.tandemservice.uniec.entity.settings.TargetAdmissionPlanRelation;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.FileWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class RatingDao extends UniBaseDao implements IRatingDao {

    private static IRatingDao INSTANCE;

    public static IRatingDao instance() {
        if (INSTANCE == null)
            INSTANCE = (IRatingDao) ApplicationRuntime.getBean(IRatingDao.class.getName());
        return INSTANCE;
    }

    @Override
    public String writeToString(IRatingDao.Params params) throws Exception {
        StringWriter writer = new StringWriter();
        write(params, writer);

        return writer.toString();
    }

    @Override
    public File writeToFile(IRatingDao.Params params, String filename) throws Exception {
        File resultFile = null;
        File tmp = null;
        try {
            tmp = File.createTempFile("entrantRating_", "");
            FileWriter writer = new FileWriter(tmp);
            write(params, writer);
            writer.close();

            resultFile = new File(filename);
            FileUtils.copyFile(tmp, resultFile);
        }
        finally {
            try {
                if (tmp != null)
                    tmp.delete();
            }
            catch (Throwable th) {
            }
        }

        return resultFile;
    }

    protected void write(IRatingDao.Params params, Writer writer) throws Exception {
        EnrollmentCampaignNode rootNode = getRootNode(params);

        JAXBContext context = JAXBContext.newInstance(new Class[]{EnrollmentCampaignNode.class});
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty("jaxb.formatted.output", Boolean.valueOf(true));
        marshaller.marshal(rootNode, writer);
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public EnrollmentCampaignNode getRootNode(IRatingDao.Params params) {
        EnrollmentCampaign enrollmentCampaign = getNotNull(params.getEnrolmentCampaignId());
        EnrollmentCampaignNode rootNode = new EnrollmentCampaignNode(enrollmentCampaign);

        //справочники
        fillCompensationTypes(params, rootNode);
        fillTargetAdmissionKinds(params, rootNode);
        fillCompetitionKinds(params, rootNode);
        fillBenefits(params, rootNode);
        fillEnrollmentRecommendations(params, rootNode);
        fillDevelopForms(params, rootNode);
        fillDevelopPeriods(params, rootNode);
        fillDevelopTechs(params, rootNode);
        fillDevelopConditions(params, rootNode);
        fillEntrantStates(params, rootNode);
        fillEduLevels(params, rootNode);

        fillCompetitionBatches(params, rootNode);

        //направления
        fillEnrollmentDirections(params, rootNode);

        return rootNode;
    }

    protected void fillEnrollmentDirections(IRatingDao.Params params, EnrollmentCampaignNode rootNode) {
        EnrollmentDirectionsNode node = new EnrollmentDirectionsNode();
        rootNode.setEnrollmentDirectionsNode(node);

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "e")
                .joinEntity(
                        "e",
                        DQLJoinType.left,
                        EnrollmentDirectionExt.class,
                        "ext",
                        DQLExpressions.eq(
                                DQLExpressions.property(EnrollmentDirection.id().fromAlias("e")),
                                DQLExpressions.property(EnrollmentDirectionExt.enrollmentDirection().id().fromAlias("ext")))
                )
                .where(DQLExpressions.eqValue(DQLExpressions.property(EnrollmentDirection.enrollmentCampaign().id().fromAlias("e")), params.getEnrolmentCampaignId()))
                .column("e")
                .column("ext");

        if (!StringUtils.isEmpty(params.getDevelopFormId()))
            dql.where(DQLExpressions.eqValue(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().developForm().code().fromAlias("e")), params.getDevelopFormId()));
        if (params.getEnrollmentDirectionId() != null)
            dql.where(DQLExpressions.eqValue(DQLExpressions.property(EnrollmentDirection.id().fromAlias("e")), params.getEnrollmentDirectionId()));
        if (params.getFormativeOrgUnitId() != null)
            dql.where(DQLExpressions.eqValue(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().formativeOrgUnit().id().fromAlias("e")), params.getFormativeOrgUnitId()));

        List<Object[]> list = dql.createStatement(getSession()).list();

        for (Object[] arr : list) {
            EnrollmentDirection enrollmentDirection = (EnrollmentDirection) arr[0];
            EnrollmentDirectionExt enrollmentDirectionExt = (EnrollmentDirectionExt) arr[1];

            fillEnrollmentDirection(params, node, enrollmentDirection, enrollmentDirectionExt);
        }
    }

    ///////////////////////////// направление ////////////////////////////////////////////////////////////////
    protected void fillEnrollmentDirection(IRatingDao.Params params, EnrollmentDirectionsNode rootNode, EnrollmentDirection enrollmentDirection, EnrollmentDirectionExt enrollmentDirectionExt) {
        EnrollmentDirectionNode ecNode = rootNode.add(enrollmentDirection, enrollmentDirectionExt);

        fillEnrollmentDirectionDisciplines(params, ecNode, enrollmentDirection);

        ecNode.lists = new ArrayList<>();
        ecNode.lists.add(fillEnrollmentDirectionGroupList(params, ecNode, enrollmentDirection, enrollmentDirectionExt, true));//бюджет
        ecNode.lists.add(fillEnrollmentDirectionGroupList(params, ecNode, enrollmentDirection, enrollmentDirectionExt, false));//контракт
    }


    protected ListNode fillEnrollmentDirectionGroupList(IRatingDao.Params params, EnrollmentDirectionNode rootNode, EnrollmentDirection enrollmentDirection, EnrollmentDirectionExt enrollmentDirectionExt, boolean budget) {
        ListNode node = new ListNode(enrollmentDirection, budget);

        List<TargetAdmissionPlanRelation> list = new DQLSelectBuilder().fromEntity(TargetAdmissionPlanRelation.class, "e")
                .where(DQLExpressions.eqValue(DQLExpressions.property(TargetAdmissionPlanRelation.entranceEducationOrgUnit().fromAlias("e")), enrollmentDirection))
                .where(DQLExpressions.eqValue(DQLExpressions.property(TargetAdmissionPlanRelation.budget().fromAlias("e")), Boolean.valueOf(budget)))
                .order(TargetAdmissionPlanRelation.targetAdmissionKind().priority().fromAlias("e").s())
                .createStatement(getSession())
                .list();
        Integer planTA = budget ? enrollmentDirection.getTargetAdmissionPlanBudget() : enrollmentDirection.getTargetAdmissionPlanContract();
        CompetitionGroup group;
        List<CompetitionGroup> groupList = new ArrayList<>();
        AtomicInteger total = new AtomicInteger(node.plan != null ? node.plan : 0);

        // есть план целивиков
        if (!list.isEmpty()) {
            // целевики расписаны по типам
            for (TargetAdmissionPlanRelation rel : list) {
                group = new CompetitionGroup(rel, total);
                groupList.add(group);
            }
        }
        else {
            if (planTA != null && planTA > 0) {
                // целевики по типам не расписаны
                group = new CompetitionGroup(CompetitionGroup.CompetitionGroupType.TARGET_ADMISSION, total, planTA, CompetitionGroup.DEFAULT_TARGET_ADMISSION_KIND_CODE_BY_TU);
                groupList.add(group);
            }
            else {
                // не указан план целевиков, но зачем то создаем группу????
                // при этом у нас гарантированно не указаны виды целевого приема по данному направлению подготовки
                //group = new CompetitionGroup(CompetitionGroupType.TARGET_ADMISSION, total, 0);
                group = new CompetitionGroup(CompetitionGroup.CompetitionGroupType.TARGET_ADMISSION, total, 0, CompetitionGroup.DEFAULT_TARGET_ADMISSION_KIND_CODE_BY_TU);
                groupList.add(group);
            }
        }

        group = new CompetitionGroup(CompetitionGroup.CompetitionGroupType.BEZ_EKZ, total, null);
        groupList.add(group);

        Integer count = 0;
        if (enrollmentDirectionExt != null)
            count = budget ? enrollmentDirectionExt.getHaveSpecialRightsPlanBudget() : enrollmentDirectionExt.getHaveSpecialRightsPlanContract();
        group = new CompetitionGroup(CompetitionGroup.CompetitionGroupType.SPECIAL_RIGHT, total, count != null ? count : 0);
        groupList.add(group);

        group = new CompetitionGroup(CompetitionGroup.CompetitionGroupType.COMMON, total, null);
        groupList.add(group);

        List<EcgCortegeRMC> entrantList = getEntrantList(enrollmentDirection, budget);
        distributeEntrants(entrantList, groupList, enrollmentDirection);

        for (CompetitionGroup group2 : groupList) {
            fillGroupsNode(params, node.groupsNode, group2);
        }

        return node;
    }

    protected void distributeEntrants(List<EcgCortegeRMC> entrantList, List<CompetitionGroup> groupList, EnrollmentDirection direction) {
        Collections.sort(entrantList, ((IEcDistributionExtDao) EcDistributionManager.instance().dao()).getComparator(direction));

        CompetitionGroup commonGroup = null;
        for (CompetitionGroup group : groupList) {
            if (group.getType() == CompetitionGroup.CompetitionGroupType.COMMON) {
                commonGroup = group;
                continue;
            }
            group.accept(entrantList);
        }

        Collections.sort(entrantList, ((IEcDistributionExtDao) EcDistributionManager.instance().dao()).getComparator(direction));
        commonGroup.accept(entrantList);
    }

    protected void fillGroupsNode(IRatingDao.Params params, CompetitionGroupsNode rootNode, CompetitionGroup group) {
        //набор 0
//		if (group.getPlan() != null && group.getPlan() == 0)
//			return;

        CompetitionGroupNode node = new CompetitionGroupNode(group);
        rootNode.groupList.add(node);

        int index = 1;
        for (EcgCortegeRMC container : group.getAcceptList()) {
            fillEntrant(params, node.entrantsNode, container, index++);
        }
    }

    protected List<EcgCortegeRMC> getEntrantList(EnrollmentDirection enrollmentDirection, boolean budget) {
        String code = budget ? CompensationTypeCodes.COMPENSATION_TYPE_BUDGET : CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT;

        List<Object[]> list = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "red")
                .joinEntity(
                        "red",
                        DQLJoinType.left,
                        RequestedEnrollmentDirectionExt.class,
                        "ext",
                        DQLExpressions.eq(
                                DQLExpressions.property(RequestedEnrollmentDirection.id().fromAlias("red")),
                                DQLExpressions.property(RequestedEnrollmentDirectionExt.requestedEnrollmentDirection().id().fromAlias("ext")))
                )
                .where(DQLExpressions.eqValue(DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().fromAlias("red")), enrollmentDirection))
                .where(DQLExpressions.eqValue(DQLExpressions.property(RequestedEnrollmentDirection.compensationType().code().fromAlias("red")), code))
                        // .where(DQLExpressions.ne( DQLExpressions.property(RequestedEnrollmentDirection.state().code().fromAlias("red")), DQLExpressions.value(EntrantStateCodes.ACTIVE) ))
                .column("red")
                .column("ext")
                .createStatement(getSession())
                .list();
        //.where(DQLExpressions.ne( DQLExpressions.property(RequestedEnrollmentDirection.state().fromAlias("red")), EntrantStateCodes.TAKE_DOCUMENTS_AWAY ))

        List<AbstractEntrantExtract> extractList = getList(AbstractEntrantExtract.class, AbstractEntrantExtract.entity().requestedEnrollmentDirection().enrollmentDirection(), enrollmentDirection);
        Map<Long, AbstractEntrantExtract> extractMap = new HashMap<>();
        for (AbstractEntrantExtract extract : extractList)
            extractMap.put(extract.getEntity().getRequestedEnrollmentDirection().getId(), extract);

        Map<Long, EcgCortegeRMC> mapEntrant = new HashMap<>();
        Map<Long, EcgCortegeRMC> mapRED = new HashMap<>();
        Map<Long, EcgCortegeRMC> mapPerson = new HashMap<>();
        for (Object[] arr : list) {
            RequestedEnrollmentDirection red = (RequestedEnrollmentDirection) arr[0];
            RequestedEnrollmentDirectionExt redExt = (RequestedEnrollmentDirectionExt) arr[1];
            AbstractEntrantExtract extract = extractMap.get(red.getId());

            Double avgMark = red.getEntrantRequest().getEntrant().getPerson().getPersonEduInstitution() != null
                    ?
                    red.getEntrantRequest().getEntrant().getPerson().getPersonEduInstitution().getAverageMark()
                    : new Double(0.0);
            Long entrantId = red.getEntrantRequest().getEntrant().getId();

            EcgCortegeRMC container = new EcgCortegeRMC(red, redExt, avgMark, extract);
            mapEntrant.put(entrantId, container);
            mapRED.put(red.getId(), container);
            mapPerson.put(red.getEntrantRequest().getEntrant().getPerson().getId(), container);
        }

        Map<Long, List<WrapChoseEntranseDiscipline>> mapDisc = EcDistributionUtil.getChosenDisciplineMap(mapRED.keySet());
        for (Map.Entry<Long, List<WrapChoseEntranseDiscipline>> entry : mapDisc.entrySet()) {
            EcgCortegeRMC container = mapRED.get(entry.getKey());
            container.addDisciplines(entry.getValue());
        }

        Map<Long, List<EnrollmentRecommendation>> recommMap = EcDistributionUtil.getRecommendationMap(mapEntrant.keySet());
        for (Map.Entry<Long, List<EnrollmentRecommendation>> entry : recommMap.entrySet()) {
            EcgCortegeRMC container = mapEntrant.get(entry.getKey());
            container.addRecommendations(entry.getValue());
        }
        Map<Long, Integer> enrPriorMap = EcDistributionUtil.getEnrollmentPriorityMap(mapEntrant.keySet());
        for (Map.Entry<Long, Integer> entry : enrPriorMap.entrySet()) {
            EcgCortegeRMC container = mapEntrant.get(entry.getKey());
            container.setEnrollmentPriority(entry.getValue());
        }

        List<PersonBenefit> benefitList = getList(PersonBenefit.class, PersonBenefit.person().id(), mapPerson.keySet());
        for (PersonBenefit benefit : benefitList) {
            EcgCortegeRMC container = mapPerson.get(benefit.getPerson().getId());
            container.addBenefit(benefit.getBenefit());
        }

        List<EcgCortegeRMC> resultList = new ArrayList<>();
        resultList.addAll(mapEntrant.values());

        fillEntrantAdditional(enrollmentDirection, budget, mapEntrant, mapRED, mapPerson);

        return resultList;
    }

    /**
     * @param enrollmentDirection
     * @param budget              Мапы чтоб более удобно было обращаться к нужным записям
     * @param mapEntrant
     * @param mapRED
     * @param mapPerson
     */
    protected void fillEntrantAdditional(EnrollmentDirection enrollmentDirection, boolean budget, Map<Long, EcgCortegeRMC> mapEntrant, Map<Long, EcgCortegeRMC> mapRED, Map<Long, EcgCortegeRMC> mapPerson) {
        //перекрыть метод если надо дополнить дополнительной инфой
    }

    protected void fillEntrant(IRatingDao.Params params, EntrantsNode rootNode, EcgCortegeRMC container, int index) {
        EntrantNode node = new EntrantNode(container, index);
        rootNode.entrantList.add(node);

        node.marksNode = new MarksNode();
        for (WrapChoseEntranseDiscipline disc : container.getDisciplines()) {
            node.marksNode.addMark(disc);
        }

        node.enrollmentRecommendationNode = new EnrollmentRecommendationNode();
        for (EnrollmentRecommendation rec : container.getRecommendations())
            node.enrollmentRecommendationNode.addShort(rec);

        node.benefitNode = new BenefitNode();
        for (Benefit benefit : container.getBenefits())
            node.benefitNode.addShort(benefit);
    }

    protected void fillEnrollmentDirectionDisciplines(IRatingDao.Params params, EnrollmentDirectionNode rootNode, EnrollmentDirection enrollmentDirection) {
        DisciplinesNode node = new DisciplinesNode();
        rootNode.disciplinesNode = node;

        List<Object[]> discList = new DQLSelectBuilder().fromEntity(EntranceDiscipline.class, "e")
                .where(DQLExpressions.eqValue(DQLExpressions.property(EntranceDiscipline.enrollmentDirection().fromAlias("e")), enrollmentDirection))
                .joinEntity(
                        "e",
                        DQLJoinType.left,
                        EntranceDisciplineExt.class,
                        "ext",
                        DQLExpressions.eq(
                                DQLExpressions.property(EntranceDisciplineExt.entranceDiscipline().id().fromAlias("ext")),
                                DQLExpressions.property(EntranceDiscipline.id().fromAlias("e")))
                )
                .column("e")
                .column("ext")
                .createStatement(getSession())
                .list();

        for (Object[] arr : discList) {
            EntranceDiscipline disc = (EntranceDiscipline) arr[0];
            EntranceDisciplineExt discExt = (EntranceDisciplineExt) arr[1];

            node.addDiscipline(disc, discExt);
        }
    }

    ///////////////////////////// справочники ////////////////////////////////////////////////////////////////////

    protected void fillTargetAdmissionKinds(IRatingDao.Params params, EnrollmentCampaignNode rootNode) {
        TargetAdmissionKindNode node = new TargetAdmissionKindNode();
        rootNode.setTargetAdmissionKindNode(node);

        List<NotUsedTargetAdmissionKind> notUsedList = getList(NotUsedTargetAdmissionKind.class, NotUsedTargetAdmissionKind.enrollmentCampaign().id(), params.getEnrolmentCampaignId());
        List<Long> notUsedIdList = UniUtils.getPropertiesList(notUsedList, NotUsedTargetAdmissionKind.targetAdmissionKind().id());

        List<TargetAdmissionKind> list = getList(TargetAdmissionKind.class, new String[]{TargetAdmissionKind.code().s()});
        for (TargetAdmissionKind kind : list) {
            boolean used = !notUsedIdList.contains(kind.getId());
            node.add(kind, used);
        }
    }

    protected void fillCompetitionKinds(IRatingDao.Params params, EnrollmentCampaignNode rootNode) {
        CompetitionKindNode node = new CompetitionKindNode();
        rootNode.setCompetitionKindNode(node);

        List<Long> usedIdList = new DQLSelectBuilder().fromEntity(EnrollmentCompetitionKind.class, "e")
                .where(DQLExpressions.eqValue(DQLExpressions.property(EnrollmentCompetitionKind.enrollmentCampaign().fromAlias("e")), params.getEnrolmentCampaignId()))
                .where(DQLExpressions.eqValue(DQLExpressions.property(EnrollmentCompetitionKind.used().fromAlias("e")), Boolean.TRUE))
                .column(EnrollmentCompetitionKind.competitionKind().id().fromAlias("e").s())
                .createStatement(getSession())
                .list();

        List<CompetitionKind> list = getList(CompetitionKind.class, new String[]{CompetitionKind.code().s()});
        for (CompetitionKind kind : list) {
            boolean used = usedIdList.contains(kind.getId());
            node.add(kind, used);
        }
    }

    protected void fillBenefits(IRatingDao.Params params, EnrollmentCampaignNode rootNode) {
        BenefitNode node = new BenefitNode();
        rootNode.setBenefitNode(node);

        List<Benefit> list = getList(Benefit.class, new String[]{Benefit.code().s()});
        for (Benefit entity : list) {
            node.add(entity);
        }
    }

    protected void fillEnrollmentRecommendations(IRatingDao.Params params, EnrollmentCampaignNode rootNode) {
        EnrollmentRecommendationNode node = new EnrollmentRecommendationNode();
        rootNode.setEnrollmentRecommendationNode(node);

        List<EnrollmentRecommendation> list = getList(EnrollmentRecommendation.class, new String[]{EnrollmentRecommendation.code().s()});
        for (EnrollmentRecommendation entity : list) {
            node.add(entity);
        }
    }

    protected void fillCompensationTypes(IRatingDao.Params params, EnrollmentCampaignNode rootNode) {
        CompensationTypeNode node = new CompensationTypeNode();
        rootNode.setCompensationTypeNode(node);

        List<CompensationType> list = getList(CompensationType.class, new String[]{CompensationType.code().s()});
        for (CompensationType entity : list) {
            node.add(entity);
        }
    }

    protected void fillDevelopForms(IRatingDao.Params params, EnrollmentCampaignNode rootNode) {
        DevelopFormNode node = new DevelopFormNode();
        rootNode.setDevelopFormNode(node);

        List<DevelopForm> list = getList(DevelopForm.class, new String[]{DevelopForm.code().s()});
        for (DevelopForm entity : list) {
            node.add(entity);
        }
    }

    protected void fillDevelopPeriods(IRatingDao.Params params, EnrollmentCampaignNode rootNode) {
        DevelopPeriodNode node = new DevelopPeriodNode();
        rootNode.setDevelopPeriodNode(node);

        List<DevelopPeriod> list = getList(DevelopPeriod.class, new String[]{DevelopPeriod.code().s()});
        for (DevelopPeriod entity : list) {
            node.add(entity);
        }
    }

    protected void fillDevelopTechs(IRatingDao.Params params, EnrollmentCampaignNode rootNode) {
        DevelopTechNode node = new DevelopTechNode();
        rootNode.setDevelopTechNode(node);

        List<DevelopTech> list = getList(DevelopTech.class, new String[]{DevelopTech.code().s()});
        for (DevelopTech entity : list) {
            node.add(entity);
        }
    }

    protected void fillDevelopConditions(IRatingDao.Params params, EnrollmentCampaignNode rootNode) {
        DevelopConditionNode node = new DevelopConditionNode();
        rootNode.setDevelopConditionNode(node);

        List<DevelopCondition> list = getList(DevelopCondition.class, new String[]{DevelopCondition.code().s()});
        for (DevelopCondition entity : list) {
            node.add(entity);
        }
    }

    protected void fillEduLevels(IRatingDao.Params params, EnrollmentCampaignNode rootNode) {
        EduLevelNode node = new EduLevelNode();
        rootNode.setEduLevelNode(node);
//
//        List<StructureEducationLevels> list = getList(StructureEducationLevels.class, StructureEducationLevels.p});
//        for (DevelopCondition entity : list) {
        node.list.add(new RowNode("1", "ВПО", null));
        node.list.add(new RowNode("2", "СПО", null));
        node.list.add(new RowNode("3", "Аспирантура", null));
        //       }
    }

    protected void fillEntrantStates(IRatingDao.Params params, EnrollmentCampaignNode rootNode) {
        EntrantStateNode node = new EntrantStateNode();
        rootNode.setEntrantStatesNode(node);

        List<EntrantState> list = getList(EntrantState.class, new String[]{EntrantState.code().s()});
        for (EntrantState entity : list) {
            node.add(entity);
        }
    }

    protected void fillCompetitionBatches(IRatingDao.Params params, EnrollmentCampaignNode rootNode) {
        CompetitionGroupTypesNode node = new CompetitionGroupTypesNode();
        rootNode.setCompetitionGroupTypesNode(node);

        for (CompetitionGroup.CompetitionGroupType batch : CompetitionGroup.CompetitionGroupType.values()) {
            node.add(batch);
        }
    }

}