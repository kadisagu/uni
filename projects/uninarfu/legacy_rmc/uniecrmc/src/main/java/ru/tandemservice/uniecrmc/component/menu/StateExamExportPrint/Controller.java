package ru.tandemservice.uniecrmc.component.menu.StateExamExportPrint;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.tools.PrivateAccessor;
import ru.tandemservice.uniec.component.menu.StateExamExportPrint.Model;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

public class Controller extends ru.tandemservice.uniec.component.menu.StateExamExportPrint.Controller {
    @Override
    public IDocumentRenderer buildDocumentRenderer(IBusinessComponent component) throws UnsupportedEncodingException
    {
        Model model = getModel(component);
        if (model.getExportType() == 4) {
            deactivate(component);
            return new CommonBaseRenderer()
                    .xls()
                    .fileName((String) PrivateAccessor.invokePrivateMethod(this, ru.tandemservice.uniec.component.menu.StateExamExportPrint.Controller.class, "getReportFileName", new Object[]{model}))
                    .document(model.getExportData().toString().getBytes(Charset.forName("cp1251")));
        }
        else {
            return super.buildDocumentRenderer(component);
        }
    }

}
