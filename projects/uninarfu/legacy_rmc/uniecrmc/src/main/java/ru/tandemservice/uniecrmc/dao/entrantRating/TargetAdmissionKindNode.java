package ru.tandemservice.uniecrmc.dao.entrantRating;

import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;

import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

public class TargetAdmissionKindNode {

    @XmlElement(name = "TargetAdmissionKind")
    public List<RowNode> list = new ArrayList<>();

    public TargetAdmissionKindNode() {
    }

    public void add(TargetAdmissionKind entity, boolean used) {
        String parent = entity.getParent() != null ? entity.getParent().getCode() : null;
        RowNode node = new RowNode(entity.getCode(), entity.getTitle(), entity.getShortTitle(), used, entity.getPriority(), parent);
        list.add(node);
    }
}
