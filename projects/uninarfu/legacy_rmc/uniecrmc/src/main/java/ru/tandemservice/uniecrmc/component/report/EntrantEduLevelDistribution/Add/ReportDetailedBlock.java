package ru.tandemservice.uniecrmc.component.report.EntrantEduLevelDistribution.Add;

import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;

import java.util.HashMap;
import java.util.Map;

public class ReportDetailedBlock {
    private ReportRow header;
    private String subHeader;
    private Map<EducationLevelsHighSchool, ReportRow> rowMap;

    public ReportDetailedBlock() {
        this.header = new ReportRow();
        this.rowMap = new HashMap<>();
    }

    public ReportDetailedBlock(String header, String subHeader, int number) {
        this.header = new ReportRow();
        this.header.setTitle(header);
        this.header.setNumber(String.valueOf(number));
        this.subHeader = "в том числе по " + subHeader;
        this.rowMap = new HashMap<>();
    }

    public ReportDetailedBlock(String header, String subHeader) {
        this.header = new ReportRow();
        this.header.setTitle(header);
        this.subHeader = "в том числе по " + subHeader;
        this.rowMap = new HashMap<>();
    }

    public ReportRow getHeader() {
        return header;
    }

    public void setHeader(ReportRow header) {
        this.header = header;
    }

    public String getSubHeader() {
        return subHeader;
    }

    public void setSubHeader(String subHeader) {
        this.subHeader = subHeader;
    }

    public Map<EducationLevelsHighSchool, ReportRow> getRowMap() {
        return rowMap;
    }

    public void setRowMap(Map<EducationLevelsHighSchool, ReportRow> rowMap) {
        this.rowMap = rowMap;
    }

    public void increaseTotal(EducationLevelsHighSchool educationLevelsHighSchool)
    {
        if (!rowMap.containsKey(educationLevelsHighSchool)) {
            rowMap.put(educationLevelsHighSchool, new ReportRow(educationLevelsHighSchool.getPrintTitle(), educationLevelsHighSchool.getEducationLevel().getEduProgramSubject().getSubjectCode()));
        }
        rowMap.get(educationLevelsHighSchool).increaseTotal();
        header.increaseTotal();

    }

    public void increaseEnrolled(EducationLevelsHighSchool educationLevelsHighSchool, CompensationType compensationType)
    {
        if (!rowMap.containsKey(educationLevelsHighSchool)) {
            rowMap.put(educationLevelsHighSchool, new ReportRow(educationLevelsHighSchool.getPrintTitle(), educationLevelsHighSchool.getEducationLevel().getEduProgramSubject().getSubjectCode()));
        }
        rowMap.get(educationLevelsHighSchool).increaseEnrolled(compensationType);
        header.increaseEnrolled(compensationType);

    }
}
