package ru.tandemservice.uniecrmc.component.settings.DisciplineMinMaxMark;

import ru.tandemservice.uniecrmc.entity.entrant.Discipline2RealizationWayRelationExt;

import java.util.HashMap;
import java.util.Map;

public class Model extends
        ru.tandemservice.uniec.component.settings.DisciplineMinMaxMark.Model
{

    private Map<Long, Discipline2RealizationWayRelationExt> mapExt = new HashMap<>();

    public Map<Long, Discipline2RealizationWayRelationExt> getMapExt() {
        return mapExt;
    }

    public void setMapExt(Map<Long, Discipline2RealizationWayRelationExt> mapExt) {
        this.mapExt = mapExt;
    }


}
