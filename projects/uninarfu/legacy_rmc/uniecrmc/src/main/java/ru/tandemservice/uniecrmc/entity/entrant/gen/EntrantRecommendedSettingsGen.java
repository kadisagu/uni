package ru.tandemservice.uniecrmc.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniecrmc.entity.entrant.EntrantRecommendedSettings;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Алгоритм распределения (рейтинга абитуриентов) при равенстве суммы баллов
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrantRecommendedSettingsGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniecrmc.entity.entrant.EntrantRecommendedSettings";
    public static final String ENTITY_NAME = "entrantRecommendedSettings";
    public static final int VERSION_HASH = -52839154;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_USE_PRIORITY_ENTRANCE_DISCIPLINE = "usePriorityEntranceDiscipline";
    public static final String P_USE_STRUCTURE_LEVEL_IN_RESTRICTION = "useStructureLevelInRestriction";
    public static final String P_USE_GROUP = "useGroup";
    public static final String P_USE_DEMON = "useDemon";
    public static final String P_INTERVAL = "interval";
    public static final String P_PATH = "path";

    private EnrollmentCampaign _enrollmentCampaign;     // Приемная компания
    private boolean _usePriorityEntranceDiscipline = true;     // Учитывать приоритет ВИ при распределении
    private boolean _useStructureLevelInRestriction = false;     // Учитывать в ограничениях уровень образования
    private boolean _useGroup = false;     // Указывать академические группы для зачисления на второй и последующий курс)
    private boolean _useDemon = false;     // Запускать демон
    private int _interval = 60;     // Интервал между сапусками (в минутах)
    private String _path;     // Путь к xml файлу

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная компания. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная компания. Свойство не может быть null и должно быть уникальным.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Учитывать приоритет ВИ при распределении. Свойство не может быть null.
     */
    @NotNull
    public boolean isUsePriorityEntranceDiscipline()
    {
        return _usePriorityEntranceDiscipline;
    }

    /**
     * @param usePriorityEntranceDiscipline Учитывать приоритет ВИ при распределении. Свойство не может быть null.
     */
    public void setUsePriorityEntranceDiscipline(boolean usePriorityEntranceDiscipline)
    {
        dirty(_usePriorityEntranceDiscipline, usePriorityEntranceDiscipline);
        _usePriorityEntranceDiscipline = usePriorityEntranceDiscipline;
    }

    /**
     * @return Учитывать в ограничениях уровень образования. Свойство не может быть null.
     */
    @NotNull
    public boolean isUseStructureLevelInRestriction()
    {
        return _useStructureLevelInRestriction;
    }

    /**
     * @param useStructureLevelInRestriction Учитывать в ограничениях уровень образования. Свойство не может быть null.
     */
    public void setUseStructureLevelInRestriction(boolean useStructureLevelInRestriction)
    {
        dirty(_useStructureLevelInRestriction, useStructureLevelInRestriction);
        _useStructureLevelInRestriction = useStructureLevelInRestriction;
    }

    /**
     * @return Указывать академические группы для зачисления на второй и последующий курс). Свойство не может быть null.
     */
    @NotNull
    public boolean isUseGroup()
    {
        return _useGroup;
    }

    /**
     * @param useGroup Указывать академические группы для зачисления на второй и последующий курс). Свойство не может быть null.
     */
    public void setUseGroup(boolean useGroup)
    {
        dirty(_useGroup, useGroup);
        _useGroup = useGroup;
    }

    /**
     * @return Запускать демон. Свойство не может быть null.
     */
    @NotNull
    public boolean isUseDemon()
    {
        return _useDemon;
    }

    /**
     * @param useDemon Запускать демон. Свойство не может быть null.
     */
    public void setUseDemon(boolean useDemon)
    {
        dirty(_useDemon, useDemon);
        _useDemon = useDemon;
    }

    /**
     * @return Интервал между сапусками (в минутах). Свойство не может быть null.
     */
    @NotNull
    public int getInterval()
    {
        return _interval;
    }

    /**
     * @param interval Интервал между сапусками (в минутах). Свойство не может быть null.
     */
    public void setInterval(int interval)
    {
        dirty(_interval, interval);
        _interval = interval;
    }

    /**
     * @return Путь к xml файлу.
     */
    @Length(max=255)
    public String getPath()
    {
        return _path;
    }

    /**
     * @param path Путь к xml файлу.
     */
    public void setPath(String path)
    {
        dirty(_path, path);
        _path = path;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntrantRecommendedSettingsGen)
        {
            setEnrollmentCampaign(((EntrantRecommendedSettings)another).getEnrollmentCampaign());
            setUsePriorityEntranceDiscipline(((EntrantRecommendedSettings)another).isUsePriorityEntranceDiscipline());
            setUseStructureLevelInRestriction(((EntrantRecommendedSettings)another).isUseStructureLevelInRestriction());
            setUseGroup(((EntrantRecommendedSettings)another).isUseGroup());
            setUseDemon(((EntrantRecommendedSettings)another).isUseDemon());
            setInterval(((EntrantRecommendedSettings)another).getInterval());
            setPath(((EntrantRecommendedSettings)another).getPath());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrantRecommendedSettingsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrantRecommendedSettings.class;
        }

        public T newInstance()
        {
            return (T) new EntrantRecommendedSettings();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "usePriorityEntranceDiscipline":
                    return obj.isUsePriorityEntranceDiscipline();
                case "useStructureLevelInRestriction":
                    return obj.isUseStructureLevelInRestriction();
                case "useGroup":
                    return obj.isUseGroup();
                case "useDemon":
                    return obj.isUseDemon();
                case "interval":
                    return obj.getInterval();
                case "path":
                    return obj.getPath();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "usePriorityEntranceDiscipline":
                    obj.setUsePriorityEntranceDiscipline((Boolean) value);
                    return;
                case "useStructureLevelInRestriction":
                    obj.setUseStructureLevelInRestriction((Boolean) value);
                    return;
                case "useGroup":
                    obj.setUseGroup((Boolean) value);
                    return;
                case "useDemon":
                    obj.setUseDemon((Boolean) value);
                    return;
                case "interval":
                    obj.setInterval((Integer) value);
                    return;
                case "path":
                    obj.setPath((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "usePriorityEntranceDiscipline":
                        return true;
                case "useStructureLevelInRestriction":
                        return true;
                case "useGroup":
                        return true;
                case "useDemon":
                        return true;
                case "interval":
                        return true;
                case "path":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "usePriorityEntranceDiscipline":
                    return true;
                case "useStructureLevelInRestriction":
                    return true;
                case "useGroup":
                    return true;
                case "useDemon":
                    return true;
                case "interval":
                    return true;
                case "path":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "usePriorityEntranceDiscipline":
                    return Boolean.class;
                case "useStructureLevelInRestriction":
                    return Boolean.class;
                case "useGroup":
                    return Boolean.class;
                case "useDemon":
                    return Boolean.class;
                case "interval":
                    return Integer.class;
                case "path":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrantRecommendedSettings> _dslPath = new Path<EntrantRecommendedSettings>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrantRecommendedSettings");
    }
            

    /**
     * @return Приемная компания. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantRecommendedSettings#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Учитывать приоритет ВИ при распределении. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantRecommendedSettings#isUsePriorityEntranceDiscipline()
     */
    public static PropertyPath<Boolean> usePriorityEntranceDiscipline()
    {
        return _dslPath.usePriorityEntranceDiscipline();
    }

    /**
     * @return Учитывать в ограничениях уровень образования. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantRecommendedSettings#isUseStructureLevelInRestriction()
     */
    public static PropertyPath<Boolean> useStructureLevelInRestriction()
    {
        return _dslPath.useStructureLevelInRestriction();
    }

    /**
     * @return Указывать академические группы для зачисления на второй и последующий курс). Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantRecommendedSettings#isUseGroup()
     */
    public static PropertyPath<Boolean> useGroup()
    {
        return _dslPath.useGroup();
    }

    /**
     * @return Запускать демон. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantRecommendedSettings#isUseDemon()
     */
    public static PropertyPath<Boolean> useDemon()
    {
        return _dslPath.useDemon();
    }

    /**
     * @return Интервал между сапусками (в минутах). Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantRecommendedSettings#getInterval()
     */
    public static PropertyPath<Integer> interval()
    {
        return _dslPath.interval();
    }

    /**
     * @return Путь к xml файлу.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantRecommendedSettings#getPath()
     */
    public static PropertyPath<String> path()
    {
        return _dslPath.path();
    }

    public static class Path<E extends EntrantRecommendedSettings> extends EntityPath<E>
    {
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Boolean> _usePriorityEntranceDiscipline;
        private PropertyPath<Boolean> _useStructureLevelInRestriction;
        private PropertyPath<Boolean> _useGroup;
        private PropertyPath<Boolean> _useDemon;
        private PropertyPath<Integer> _interval;
        private PropertyPath<String> _path;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная компания. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantRecommendedSettings#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Учитывать приоритет ВИ при распределении. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantRecommendedSettings#isUsePriorityEntranceDiscipline()
     */
        public PropertyPath<Boolean> usePriorityEntranceDiscipline()
        {
            if(_usePriorityEntranceDiscipline == null )
                _usePriorityEntranceDiscipline = new PropertyPath<Boolean>(EntrantRecommendedSettingsGen.P_USE_PRIORITY_ENTRANCE_DISCIPLINE, this);
            return _usePriorityEntranceDiscipline;
        }

    /**
     * @return Учитывать в ограничениях уровень образования. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantRecommendedSettings#isUseStructureLevelInRestriction()
     */
        public PropertyPath<Boolean> useStructureLevelInRestriction()
        {
            if(_useStructureLevelInRestriction == null )
                _useStructureLevelInRestriction = new PropertyPath<Boolean>(EntrantRecommendedSettingsGen.P_USE_STRUCTURE_LEVEL_IN_RESTRICTION, this);
            return _useStructureLevelInRestriction;
        }

    /**
     * @return Указывать академические группы для зачисления на второй и последующий курс). Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantRecommendedSettings#isUseGroup()
     */
        public PropertyPath<Boolean> useGroup()
        {
            if(_useGroup == null )
                _useGroup = new PropertyPath<Boolean>(EntrantRecommendedSettingsGen.P_USE_GROUP, this);
            return _useGroup;
        }

    /**
     * @return Запускать демон. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantRecommendedSettings#isUseDemon()
     */
        public PropertyPath<Boolean> useDemon()
        {
            if(_useDemon == null )
                _useDemon = new PropertyPath<Boolean>(EntrantRecommendedSettingsGen.P_USE_DEMON, this);
            return _useDemon;
        }

    /**
     * @return Интервал между сапусками (в минутах). Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantRecommendedSettings#getInterval()
     */
        public PropertyPath<Integer> interval()
        {
            if(_interval == null )
                _interval = new PropertyPath<Integer>(EntrantRecommendedSettingsGen.P_INTERVAL, this);
            return _interval;
        }

    /**
     * @return Путь к xml файлу.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantRecommendedSettings#getPath()
     */
        public PropertyPath<String> path()
        {
            if(_path == null )
                _path = new PropertyPath<String>(EntrantRecommendedSettingsGen.P_PATH, this);
            return _path;
        }

        public Class getEntityClass()
        {
            return EntrantRecommendedSettings.class;
        }

        public String getEntityName()
        {
            return "entrantRecommendedSettings";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
