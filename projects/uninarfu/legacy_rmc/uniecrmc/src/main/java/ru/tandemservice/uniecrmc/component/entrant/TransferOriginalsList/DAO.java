package ru.tandemservice.uniecrmc.component.entrant.TransferOriginalsList;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uniecrmc.entity.entrant.ChangeOriginalDocumentRED;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.Collection;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(final Model model) {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.setEntrantStateModel(new LazySimpleSelectModel<EntrantState>(EntrantState.class));

        model.setEnrollmentDirectionModel(new DQLFullCheckSelectModel(EnrollmentDirection.title()) {
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, alias)
                        .where(DQLExpressions.eqValue(DQLExpressions.property(EnrollmentDirection.enrollmentCampaign().fromAlias(alias)), model.getEnrollmentCampaign()));
                if (!StringUtils.isEmpty(filter))
                    dql.where(DQLExpressions.likeUpper(DQLExpressions.property(EnrollmentDirection.title().fromAlias(alias)), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));

                return dql;
            }
        });

        model.setEntrantModel(new DQLFullCheckSelectModel(Entrant.fullFio()) {
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(Entrant.class, alias)
                        .where(DQLExpressions.eqValue(DQLExpressions.property(Entrant.enrollmentCampaign().fromAlias(alias)), model.getEnrollmentCampaign()));
                if (!StringUtils.isEmpty(filter))
                    dql.where(DQLExpressions.likeUpper(DQLExpressions.property(Entrant.person().identityCard().fullFio().fromAlias(alias)), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));
                return dql;
            }
        });
    }

    @Override
    public void prepareListDataSource(Model model) {
        Collection<EntrantState> enrollmentDirectionList = model.getSettings().get("enrollmentDirectionList");
        Collection<EntrantState> entrantList = model.getSettings().get("entrantList");
        Collection<EntrantState> stateList = model.getSettings().get("stateList");

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(ChangeOriginalDocumentRED.class, "e")
                .where(DQLExpressions.eqValue(DQLExpressions.property(ChangeOriginalDocumentRED.entrantRequest().entrant().enrollmentCampaign().fromAlias("e")), model.getEnrollmentCampaign()));

        if (enrollmentDirectionList != null && !enrollmentDirectionList.isEmpty())
            dql.where(DQLExpressions.or(
                    DQLExpressions.in(DQLExpressions.property(ChangeOriginalDocumentRED.oldOrigin().enrollmentDirection().fromAlias("e")), enrollmentDirectionList),
                    DQLExpressions.in(DQLExpressions.property(ChangeOriginalDocumentRED.newOrigin().enrollmentDirection().fromAlias("e")), enrollmentDirectionList)
            ));
        if (entrantList != null && !entrantList.isEmpty())
            dql.where(DQLExpressions.in(DQLExpressions.property(ChangeOriginalDocumentRED.entrantRequest().entrant().fromAlias("e")), entrantList));
        if (stateList != null && !stateList.isEmpty())
            dql.where(DQLExpressions.in(DQLExpressions.property(ChangeOriginalDocumentRED.entrantRequest().entrant().state().fromAlias("e")), stateList));

        if (model.getDataSource().getEntityOrder() != null)
            dql.order("e." + model.getDataSource().getEntityOrder().getKeyString(), model.getDataSource().getEntityOrder().getDirection());

        UniUtils.createPage(model.getDataSource(), dql, getSession());
    }

    @Override
    public void cancelTransfer(Collection<ChangeOriginalDocumentRED> cancelList) {
        // TODO Auto-generated method stub
    }


}
