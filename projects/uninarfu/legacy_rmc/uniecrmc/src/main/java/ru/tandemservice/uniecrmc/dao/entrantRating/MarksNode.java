package ru.tandemservice.uniecrmc.dao.entrantRating;

import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.WrapChoseEntranseDiscipline;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

public class MarksNode {

    @XmlElement(name = "Mark")
    public List<MarkRow> markList = new ArrayList<>();

    public void addMark(WrapChoseEntranseDiscipline discipline) {
        MarkRow node = new MarkRow(discipline);
        this.markList.add(node);
    }

    public static class MarkRow {
        @XmlAttribute
        public String id;

        @XmlAttribute(name = "disciplineId")
        public String disciplineId;

        @XmlAttribute(required = false)
        public Double mark;

        public MarkRow() {
        }

        public MarkRow(WrapChoseEntranseDiscipline discipline) {
            this.id = "" + discipline.getId();
            this.disciplineId = "" + discipline.getEnrollmentCampaignDisciplineId();
            this.mark = discipline.getFinalMark();
        }

    }
}
