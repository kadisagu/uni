package ru.tandemservice.uniecrmc.base.ext.EcDistribution.logic;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableDouble;
import org.apache.commons.lang.mutable.MutableInt;
import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.*;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.EcDistributionUtil.Rule;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.base.bo.EcDistribution.EcDistributionManager;
import ru.tandemservice.uniec.base.bo.EcDistribution.logic.EcDistributionDao;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.*;
import ru.tandemservice.uniec.base.entity.ecg.*;
import ru.tandemservice.uniec.base.entity.ecg.gen.EcgDistributionConfigGen;
import ru.tandemservice.uniec.base.entity.ecg.gen.EcgDistributionGen;
import ru.tandemservice.uniec.base.entity.ecg.gen.EcgDistributionStateGen;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EnrollmentRecommendation;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.catalog.gen.CompetitionKindGen;
import ru.tandemservice.uniec.entity.catalog.gen.EntrantStateGen;
import ru.tandemservice.uniec.entity.catalog.gen.TargetAdmissionKindGen;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.TargetAdmissionPlanRelation;
import ru.tandemservice.uniecrmc.entity.entrant.*;

import java.util.*;

public class EcDistributionExtDao extends EcDistributionDao implements IEcDistributionExtDao {

    @Override
    public Map<Long, Integer> getNextSRQuota(EcgConfigDTO configDTO) {

        List<EnrollmentDirection> directionList = EcDistributionManager.instance().dao().getDistributionDirectionList(configDTO);

        Set<EducationOrgUnit> educationOrgUnitSet = new HashSet<EducationOrgUnit>();
        for (EnrollmentDirection enrollmentDirection : directionList) {
            educationOrgUnitSet.add(enrollmentDirection.getEducationOrgUnit());
        }

        Map<Long, Integer> direction2quotaMap = new Hashtable<Long, Integer>();

        EcgDistributionConfig config = (EcgDistributionConfig) getByNaturalId(new EcgDistributionConfigGen.NaturalId(configDTO.getEcgItem(), configDTO.getCompensationType(), configDTO.isSecondHighAdmission()));

        if (config != null) {
            EcgDistribution distribution = (EcgDistribution) getByNaturalId(new EcgDistributionGen.NaturalId(config, 1));

            if (distribution != null) {
                List<EcgDistributionSRQuota> srQuotaList = getDistributionSRQuotaList(distribution.getId());
                for (EcgDistributionSRQuota srQuota : srQuotaList)
                    direction2quotaMap.put(srQuota.getDistributionQuota().getDirection().getId(), Integer.valueOf(srQuota.getQuota()));
            }
        }

        Map<Long, Integer> ou2countMap = new Hashtable<Long, Integer>();

        //предзачисленные абитуриенты по данным направлениям подготовки
        List<PreliminaryEnrollmentStudent> students = new DQLSelectBuilder().fromEntity(PreliminaryEnrollmentStudent.class, "e")
                .column("e")
                .where(DQLExpressions.eq(DQLExpressions.property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().enrollmentDirection().enrollmentCampaign().fromAlias("e")), DQLExpressions.value(configDTO.getEcgItem().getEnrollmentCampaign())))
                .where(DQLExpressions.in(DQLExpressions.property(PreliminaryEnrollmentStudent.educationOrgUnit().fromAlias("e")), educationOrgUnitSet))
                .where(DQLExpressions.eq(DQLExpressions.property(PreliminaryEnrollmentStudent.compensationType().fromAlias("e")), DQLExpressions.value(configDTO.getCompensationType())))
                .where(DQLExpressions.in(DQLExpressions.property(PreliminaryEnrollmentStudent.studentCategory().code().fromAlias("e")), configDTO.isSecondHighAdmission() ? Collections.singletonList("3") : Arrays.asList(new String[]{"1", "2"})))
                .createStatement(getSession()).list();

        List<PreliminaryEnrollmentStudentExt> studentsExt = new DQLSelectBuilder().fromEntity(PreliminaryEnrollmentStudentExt.class, "ext")
                .column("ext")
                .where(DQLExpressions.in(DQLExpressions.property(PreliminaryEnrollmentStudentExt.preliminaryEnrollmentStudent().fromAlias("ext")), students))
                .createStatement(getSession()).list();

        Map<Long, PreliminaryEnrollmentStudentExt> studentsMap = new HashMap<Long, PreliminaryEnrollmentStudentExt>();

        for (PreliminaryEnrollmentStudentExt studentExt : studentsExt)
            studentsMap.put(studentExt.getPreliminaryEnrollmentStudent().getId(), studentExt);

        for (PreliminaryEnrollmentStudent preStudent : students) {
            Long ouId = preStudent.getEducationOrgUnit().getId();
            PreliminaryEnrollmentStudentExt studentExt = studentsMap.get(preStudent.getId());
            if (studentExt != null && studentExt.isHaveSpecialRights()) {
                Integer plan = (Integer) ou2countMap.get(ouId);
                ou2countMap.put(ouId, Integer.valueOf(plan == null ? 1 : plan.intValue() + 1));
            }
        }

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrollmentDirectionExt.class, "ext")
                .where(DQLExpressions.in(DQLExpressions.property(EnrollmentDirectionExt.enrollmentDirection().fromAlias("ext")), directionList));

        List<EnrollmentDirectionExt> lst = getList(builder);

        Map<Long, EnrollmentDirectionExt> extMap = new HashMap<Long, EnrollmentDirectionExt>();

        for (EnrollmentDirectionExt ext : lst) {
            extMap.put(ext.getEnrollmentDirection().getId(), ext);
        }

        Map<Long, Integer> srQuotaMap = new HashMap<Long, Integer>();

        for (EnrollmentDirection direction : directionList) {
            Long directionId = direction.getId();
            Long ouId = direction.getEducationOrgUnit().getId();

            //план приема на направление
            Integer quotaA = (Integer) direction2quotaMap.get(directionId);
            if (quotaA == null) {
                EnrollmentDirectionExt directionExt = extMap.get(directionId);
                if (directionExt != null)
                    quotaA = configDTO.getCompensationType().isBudget() ? directionExt.getHaveSpecialRightsPlanBudget() : directionExt.getHaveSpecialRightsPlanContract();
            }
            if (quotaA == null) quotaA = Integer.valueOf(0);
            //уже занятые места
            Integer quotaB = (Integer) ou2countMap.get(ouId);
            if (quotaB == null) quotaB = Integer.valueOf(0);
            //свободные места
            int quota = quotaA.intValue() - quotaB.intValue();
            if (quota < 0) quota = 0;

            srQuotaMap.put(directionId, Integer.valueOf(quota));
        }

        return srQuotaMap;
    }

    @Override
    public IEcgQuotaDTO getNextQuotaDTO(EcgConfigDTO configDTO) {
        List<EnrollmentDirection> directionList = getDistributionDirectionList(configDTO);

        List<TargetAdmissionKind> taKindList = getDistributionTaKindList(configDTO);

        Set<EducationOrgUnit> educationOrgUnitSet = new HashSet<EducationOrgUnit>();
        for (EnrollmentDirection enrollmentDirection : directionList) {
            educationOrgUnitSet.add(enrollmentDirection.getEducationOrgUnit());
        }

        Map<Long, Integer> direction2quotaMap = new Hashtable<Long, Integer>();

        Map<Long, Map<Long, Integer>> direction2kind2quotaMap = new Hashtable<Long, Map<Long, Integer>>();

        EcgDistributionConfig config = (EcgDistributionConfig) getByNaturalId(new EcgDistributionConfigGen.NaturalId(configDTO.getEcgItem(), configDTO.getCompensationType(), configDTO.isSecondHighAdmission()));

        if (config != null) {
            EcgDistribution distribution = (EcgDistribution) getByNaturalId(new EcgDistributionGen.NaturalId(config, 1));

            if (distribution != null) {
                List<EcgDistributionQuota> quotaList = getList(EcgDistributionQuota.class, EcgDistributionQuota.distribution(), distribution, new String[0]);

                for (EcgDistributionQuota quota : quotaList) {
                    direction2quotaMap.put(quota.getDirection().getId(), Integer.valueOf(quota.getQuota()));
                }
                List<EcgDistributionTAQuota> taQuotaList = getList(EcgDistributionTAQuota.class, EcgDistributionTAQuota.distributionQuota(), quotaList, new String[0]);

                for (EcgDistributionTAQuota taQuota : taQuotaList) {
                    Long directionId = taQuota.getDistributionQuota().getDirection().getId();

                    Map<Long, Integer> kind2quotaMap = direction2kind2quotaMap.get(directionId);

                    if (kind2quotaMap == null) {
                        direction2kind2quotaMap.put(directionId, kind2quotaMap = new Hashtable<Long, Integer>());
                    }
                    kind2quotaMap.put(taQuota.getTargetAdmissionKind().getId(), Integer.valueOf(taQuota.getQuota()));
                }
            }

        }

        List<TargetAdmissionPlanRelation> planRelations = new DQLSelectBuilder().fromEntity(TargetAdmissionPlanRelation.class, "e")
                .column("e")
                .where(DQLExpressions.in(DQLExpressions.property(TargetAdmissionPlanRelation.entranceEducationOrgUnit().fromAlias("e")), directionList))
                .where(DQLExpressions.eq(DQLExpressions.property(TargetAdmissionPlanRelation.budget().fromAlias("e")), DQLExpressions.value(Boolean.valueOf(configDTO.getCompensationType().isBudget()))))
                .createStatement(getSession()).list();

        Map<Long, Map<Long, Integer>> direction2kind2planMap = new Hashtable<Long, Map<Long, Integer>>();

        for (TargetAdmissionPlanRelation planRelation : planRelations) {
            Long directionId = planRelation.getEntranceEducationOrgUnit().getId();
            Map<Long, Integer> map = direction2kind2planMap.get(directionId);
            if (map == null)
                direction2kind2planMap.put(directionId, map = new Hashtable<Long, Integer>());
            map.put(planRelation.getTargetAdmissionKind().getId(), Integer.valueOf(planRelation.getPlanValue()));
        }

        TargetAdmissionKind defaultTargetAdmissionKind = getByNaturalId(new TargetAdmissionKindGen.NaturalId("1"));

        Map<Long, Integer> ou2countMap = new Hashtable<Long, Integer>();

        Map<Long, Map<Long, Integer>> ou2kind2countMap = new Hashtable<Long, Map<Long, Integer>>();

        List<PreliminaryEnrollmentStudent> students = new DQLSelectBuilder().fromEntity(PreliminaryEnrollmentStudent.class, "e")
                .column("e")
                .where(DQLExpressions.eq(DQLExpressions.property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().enrollmentDirection().enrollmentCampaign().fromAlias("e")), DQLExpressions.value(configDTO.getEcgItem().getEnrollmentCampaign())))
                .where(DQLExpressions.in(DQLExpressions.property(PreliminaryEnrollmentStudent.educationOrgUnit().fromAlias("e")), educationOrgUnitSet))
                .where(DQLExpressions.eq(DQLExpressions.property(PreliminaryEnrollmentStudent.compensationType().fromAlias("e")), DQLExpressions.value(configDTO.getCompensationType())))
                .where(DQLExpressions.in(DQLExpressions.property(PreliminaryEnrollmentStudent.studentCategory().code().fromAlias("e")), configDTO.isSecondHighAdmission() ? Collections.singletonList("3") : Arrays.asList(new String[]{"1", "2"})))
                .createStatement(getSession()).list();

        for (PreliminaryEnrollmentStudent preStudent : students) {
            Long ouId = preStudent.getEducationOrgUnit().getId();

            Integer plan = ou2countMap.get(ouId);
            ou2countMap.put(ouId, Integer.valueOf(plan == null ? 1 : plan.intValue() + 1));

            if (preStudent.isTargetAdmission()) {
                Map<Long, Integer> kind2countMap = ou2kind2countMap.get(ouId);
                if (kind2countMap == null)
                    ou2kind2countMap.put(ouId, kind2countMap = new Hashtable<Long, Integer>());
                TargetAdmissionKind kind = preStudent.getRequestedEnrollmentDirection().getTargetAdmissionKind();
                if (kind == null)
                    kind = defaultTargetAdmissionKind;
                plan = kind2countMap.get(kind.getId());
                kind2countMap.put(kind.getId(), Integer.valueOf(plan == null ? 1 : plan.intValue() + 1));
            }

        }

        Map<Long, Integer> quotaMap = new Hashtable<Long, Integer>();

        Map<Long, Map<Long, Integer>> taQuotaMap = new Hashtable<Long, Map<Long, Integer>>();

        Map<Long, Integer> kind2quotaMap;
        Map<Long, Integer> kind2quotaFromDirectionMap;
        Map<Long, Integer> kind2quotaFromPreStudentMap;
        Map<Long, Integer> taQuotaResultMap;

        for (EnrollmentDirection direction : directionList) {
            Long directionId = direction.getId();
            Long ouId = direction.getEducationOrgUnit().getId();

            Integer quotaA = direction2quotaMap.get(directionId);

            if (quotaA == null) {
                quotaA = configDTO.getCompensationType().isBudget() ? direction.getMinisterialPlan() : direction.getContractPlan();
            }
            if (quotaA == null)
                quotaA = Integer.valueOf(0);

            Integer quotaB = ou2countMap.get(ouId);

            if (quotaB == null)
                quotaB = Integer.valueOf(0);

            int quota = quotaA.intValue() - quotaB.intValue();

            if (quota < 0)
                quota = 0;

            quotaMap.put(directionId, Integer.valueOf(quota));

            kind2quotaMap = direction2kind2quotaMap.get(directionId);
            if (kind2quotaMap == null)
                kind2quotaMap = Collections.emptyMap();

            kind2quotaFromDirectionMap = direction2kind2planMap.get(direction.getId());
            if (kind2quotaFromDirectionMap == null)
                kind2quotaFromDirectionMap = Collections.emptyMap();

            kind2quotaFromPreStudentMap = ou2kind2countMap.get(ouId);
            if (kind2quotaFromPreStudentMap == null)
                kind2quotaFromPreStudentMap = Collections.emptyMap();

            taQuotaResultMap = taQuotaMap.get(directionId);
            if (taQuotaResultMap == null) {
                taQuotaMap.put(directionId, taQuotaResultMap = new Hashtable<Long, Integer>());
            }
            for (TargetAdmissionKind taKind : taKindList) {
                Integer taQuotaA = kind2quotaMap.get(taKind.getId());

                if (taQuotaA == null) {
                    taQuotaA = kind2quotaFromDirectionMap.get(taKind.getId());
                }
                //если в приемке используется только 1 вид ЦП и для него не указан план приема (TargetAdmissionPlanRelation), тогда план приема целевиков берем из НП(С)
                if (taQuotaA == null && taKindList.size() == 1)
                    taQuotaA = configDTO.getCompensationType().isBudget() ? direction.getTargetAdmissionPlanBudget() : direction.getTargetAdmissionPlanContract();

                if (taQuotaA == null)
                    taQuotaA = Integer.valueOf(0);

                Integer taQuotaB = kind2quotaFromPreStudentMap.get(taKind.getId());

                if (taQuotaB == null)
                    taQuotaB = Integer.valueOf(0);

                int taQuota = taQuotaA.intValue() - taQuotaB.intValue();

                if (taQuota < 0)
                    taQuota = 0;

                taQuotaResultMap.put(taKind.getId(), Integer.valueOf(taQuota));
            }
        }

        return new EcgQuotaDTO(taKindList, quotaMap, taQuotaMap);
    }

    @Override
    public Map<Long, Integer> getCurrentSRQuota(EcgDistribution paramEcgDistribution) {

        List<EcgDistributionSRQuota> srQuotaList = getDistributionSRQuotaList(paramEcgDistribution.getId());

        Map<Long, Integer> srQuotaMap = new HashMap<Long, Integer>();
        Set<Long> usedQuotaIds = new HashSet<Long>();

        for (EcgDistributionSRQuota srQuota : srQuotaList) {
            Long directionId = srQuota.getDistributionQuota().getDirection().getId();
            if (usedQuotaIds.add(srQuota.getId())) {
                srQuotaMap.put(directionId, Integer.valueOf(srQuota.getQuota()));
            }
        }

        return srQuotaMap;
    }

    @Override
    public void saveSRQuota(Long distributionId, Map<Long, Integer> srQuotaMap) {
        List<EcgDistributionQuota> quaotaList = getList(EcgDistributionQuota.class, EcgDistributionQuota.distribution().id(), distributionId, new String[0]);

        for (EcgDistributionQuota qouta : quaotaList) {
            EcgDistributionSRQuota srQuota = new EcgDistributionSRQuota();
            srQuota.setDistributionQuota(qouta);
            Integer plan = (Integer) srQuotaMap.get(qouta.getDirection().getId());
            srQuota.setQuota((plan == null) || (plan.intValue() < 0) ? 0 : plan.intValue());
            getSession().save(srQuota);
        }

    }

    @Override
    public void updateSRQuota(Long distributionId, Map<Long, Integer> srQuotaMap) {
        List<EcgDistributionSRQuota> srQuotaList = getDistributionSRQuotaList(distributionId);

        for (EcgDistributionSRQuota srQuota : srQuotaList) {
            Integer plan = (Integer) srQuotaMap.get(srQuota.getDistributionQuota().getDirection().getId());
            srQuota.setQuota((plan == null) || (plan.intValue() < 0) ? 0 : plan.intValue());
            getSession().update(srQuota);
        }
    }

    public List<EcgDistributionSRQuota> getDistributionSRQuotaList(Long distributionId) {
        return new DQLSelectBuilder().fromEntity(EcgDistributionSRQuota.class, "e")
                .column("e")
                .where(DQLExpressions.eq(DQLExpressions.property(EcgDistributionSRQuota.distributionQuota().distribution().id().fromAlias("e")), DQLExpressions.value(distributionId)))
                .createStatement(getSession()).list();
    }

    @Override
    public void getQuotaHTMLDescription(IEcgDistribution distribution,
                                        Map<Long, String> srQuotaMap)
    {
        Map<Long, Integer> freeMap = getFreeSRQuota(distribution);
        Map<Long, Integer> quotaMap = getCurrentSRQuota(distribution.getDistribution());

        int quotaGlobalSum = 0;
        int freeGlobalSum = 0;

        String style = " style='white-space:nowrap;'";
        String styleRed = " style='white-space:nowrap;color:red;'";

        for (Map.Entry<Long, Integer> entry : quotaMap.entrySet()) {
            Long directionId = (Long) entry.getKey();
            int quota = ((Integer) entry.getValue()).intValue();
            int free = ((Integer) freeMap.get(directionId)).intValue();

            quotaGlobalSum += quota;
            freeGlobalSum += free;

            srQuotaMap.put(directionId, new StringBuilder().append("<div").append(free < 0 ? styleRed : style).append(">").append(free).append(" / ").append(quota).append("</div>").toString());
        }
        srQuotaMap.put(Long.valueOf(0L), new StringBuilder().append("<div").append(freeGlobalSum < 0 ? styleRed : style).append(">").append(freeGlobalSum).append(" / ").append(quotaGlobalSum).append("</div>").toString());
    }

    public Map<Long, Integer> getFreeSRQuota(IEcgDistribution distribution) {

        Map<Long, Integer> freeMap = new Hashtable<Long, Integer>();

        Map<Long, Integer> quotaMap = getCurrentSRQuota(distribution.getDistribution());
        Map<Long, Integer> usedMap = getUsedSRQuota(distribution);


        for (Map.Entry<Long, Integer> entry : quotaMap.entrySet()) {
            Long directionId = (Long) entry.getKey();
            int quota = ((Integer) entry.getValue()).intValue();
            Integer quotaUsed = usedMap.get(directionId);
            freeMap.put(directionId, Integer.valueOf(quota - (quotaUsed == null ? 0 : quotaUsed.intValue())));
        }

        return freeMap;
    }

    public Map<Long, Integer> getUsedSRQuota(IEcgDistribution distribution) {
        Map<Long, Integer> usedMap = new Hashtable<Long, Integer>();

        List<Object[]> list = new ArrayList<Object[]>();
        if ((distribution instanceof EcgDistribution)) {
            list = new DQLSelectBuilder().fromEntity(EcgEntrantRecommended.class, "e")
                    .column("e")
                    .joinEntity("e", DQLJoinType.left, EcgEntrantRecommendedExt.class, "ext",
                                DQLExpressions.eq(
                                        DQLExpressions.property(EcgEntrantRecommended.id().fromAlias("e")),
                                        DQLExpressions.property(EcgEntrantRecommendedExt.ecgEntrantRecommended().id().fromAlias("ext")))
                    )
                    .column(DQLExpressions.property(EcgEntrantRecommendedExt.haveSpecialRights().fromAlias("ext")))
                    .where(DQLExpressions.eq(DQLExpressions.property(EcgEntrantRecommended.distribution().fromAlias("e")), DQLExpressions.value(distribution)))
                    .createStatement(getSession()).list();
        }
        else {
            list = new DQLSelectBuilder().fromEntity(EcgEntrantRecommendedDetail.class, "e")
                    .column("e")
                    .joinEntity("e", DQLJoinType.left, EcgEntrantRecommendedDetailExt.class, "ext",
                                DQLExpressions.eq(
                                        DQLExpressions.property(EcgEntrantRecommended.id().fromAlias("e")),
                                        DQLExpressions.property(EcgEntrantRecommendedDetailExt.ecgEntrantRecommendedDetail().id().fromAlias("ext")))
                    )
                    .column(DQLExpressions.property(EcgEntrantRecommendedExt.haveSpecialRights().fromAlias("ext")))
                    .where(DQLExpressions.eq(DQLExpressions.property(EcgEntrantRecommendedDetail.distribution().fromAlias("e")), DQLExpressions.value(distribution)))
                    .createStatement(getSession()).list();
        }

        for (Object[] obj : list) {
            IEcgEntrantRecommended entrantRecommended = (IEcgEntrantRecommended) obj[0];
            Boolean haveSpecialRights = (Boolean) obj[1];
            Long directionId = entrantRecommended.getDirection().getEnrollmentDirection().getId();

            if (haveSpecialRights != null && haveSpecialRights.booleanValue()) {
                Integer used = (Integer) usedMap.get(directionId);
                usedMap.put(directionId, Integer.valueOf(used == null ? 1 : used.intValue() + 1));
            }
        }

        return usedMap;
    }

    @Override
    public List<? extends IEcgEntrantRateRowDTO> getEntrantRateRowList(
            IEcgDistribution distribution,
            List<TargetAdmissionKind> usedTaKindList, Rule rateRule)
    {

        Map<Long, TargetAdmissionKind> taKindMap = new Hashtable<Long, TargetAdmissionKind>();
        for (TargetAdmissionKind item : usedTaKindList) {
            taKindMap.put(item.getId(), item);
        }
        final Map<CompetitionKind, Integer> competitionKindPriorityMap = UniecDAOFacade.getSettingsDAO().getCompetitionKindPriorities(distribution.getConfig().getEcgItem().getEnrollmentCampaign());

        Map<Long, CompetitionKind> coKindMap = new Hashtable<Long, CompetitionKind>();
        for (CompetitionKind item : competitionKindPriorityMap.keySet()) {
            if (item != null)
                coKindMap.put(item.getId(), item);
        }

        DQLSelectBuilder builder = (distribution instanceof EcgDistribution) ?
                (DQLSelectBuilder) EcDistributionUtil.invokePrivateMethod(this, EcDistributionDao.class, "getDistributionRateBuilder", new Object[]{new Boolean(true), (EcgDistribution) distribution})
                :
                getDistributionDetailRateBuilder((EcgDistributionDetail) distribution, rateRule)
//					(DQLSelectBuilder) EcDistributionUtil.invokePrivateMethod(this, EcDistributionDao.class, "getDistributionDetailRateBuilder", new Object[]{(EcgDistributionDetail)distribution, tRule})
                ;

        EnrollmentCampaign ec = distribution.getConfig().getEcgItem().getEnrollmentCampaign();

        //<todo> исключить зачисленных
        DQLSelectBuilder dqlExclude = getDqlExcludeEntrantToEc(ec, distribution);

        if (dqlExclude != null) {
            builder.where(DQLExpressions.notIn(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("e")),
                                               dqlExclude.getQuery()
            ));
        }


        boolean targetAdmission = rateRule.equals(Rule.RULE_TARGET_ADMISSION);
        boolean haveSpecilaRights = rateRule.equals(Rule.RULE_NON_COMPETITIVE);

        switch (rateRule) {
            case RULE_TARGET_ADMISSION:
                builder.where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.targetAdmission().fromAlias("e")), DQLExpressions.value(Boolean.TRUE)));
                break;
            case RULE_WITHOUT_ENTRANCE_DISCIPLINES:
                builder.where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.competitionKind().fromAlias("e")), DQLExpressions.commonValue(getByNaturalId(new CompetitionKindGen.NaturalId(UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES)))));
                break;
            case RULE_NON_COMPETITIVE:
                builder.joinEntity("e", DQLJoinType.inner, RequestedEnrollmentDirectionExt.class, "ext",
                                   DQLExpressions.eq(
                                           DQLExpressions.property(RequestedEnrollmentDirection.id().fromAlias("e")),
                                           DQLExpressions.property(RequestedEnrollmentDirectionExt.requestedEnrollmentDirection().id().fromAlias("ext"))))
                        .where(DQLExpressions.eqValue(DQLExpressions.property(RequestedEnrollmentDirectionExt.haveSpecialRights().fromAlias("ext")), Boolean.TRUE));
                //	    		builder.where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.competitionKind().fromAlias("e")), DQLExpressions.value(getByNaturalId(new CompetitionKindGen.NaturalId(UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION)))));
                break;
            case RULE_COMPETITIVE:
                //	    		builder.where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirection.competitionKind().code().fromAlias("e")), Arrays.asList(UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION, UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION)));
                builder.where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.competitionKind().fromAlias("e")), DQLExpressions.commonValue(getByNaturalId(new CompetitionKindGen.NaturalId(UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION)))));
                break;
            case RULE_COMPETITIVE_WITH_ORIGINAL:
                builder.where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.competitionKind().fromAlias("e")), DQLExpressions.commonValue(getByNaturalId(new CompetitionKindGen.NaturalId(UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION)))));
                builder.where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.originalDocumentHandedIn().fromAlias("e")), DQLExpressions.value(Boolean.TRUE)));
                break;
            default:
                throw new IllegalArgumentException(new StringBuilder().append("Unknown rateRule: ").append(rateRule).toString());
        }

        builder.joinPath(DQLJoinType.inner, RequestedEnrollmentDirection.entrantRequest().fromAlias("e"), "req");
        builder.joinPath(DQLJoinType.inner, RequestedEnrollmentDirection.competitionKind().fromAlias("e"), "com");
        builder.joinPath(DQLJoinType.left, RequestedEnrollmentDirection.targetAdmissionKind().fromAlias("e"), "tar");
        builder.joinPath(DQLJoinType.inner, RequestedEnrollmentDirection.enrollmentDirection().fromAlias("e"), "dir");
        builder.joinPath(DQLJoinType.inner, EnrollmentDirection.educationOrgUnit().fromAlias("dir"), "ou");
        builder.joinPath(DQLJoinType.inner, EducationOrgUnit.educationLevelHighSchool().fromAlias("ou"), "hs");
        builder.joinPath(DQLJoinType.inner, EntrantRequest.entrant().fromAlias("req"), "ent");
        builder.joinPath(DQLJoinType.inner, Entrant.person().fromAlias("ent"), "per");
        builder.joinPath(DQLJoinType.inner, Person.identityCard().fromAlias("per"), "card");
        builder.joinPath(DQLJoinType.left, Person.personEduInstitution().fromAlias("per"), "edu");
        builder.joinPath(DQLJoinType.left, RequestedEnrollmentDirection.profileChosenEntranceDiscipline().fromAlias("e"), "pcd");

        builder.joinEntity("e", DQLJoinType.left, ChosenEntranceDiscipline.class, "c", DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.id().fromAlias("e")), DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("c"))));

        int REQUESTED_ENROLLMENT_DIRECTION_ID_IX = 0;
        int ENROLLMENT_DIRECTION_ID_IX = 1;
        final int REQUEST_NUMBER_IX = 2;
        final int PRIORITY_NUMBER_IX = 3;
        int GRADUATED_PROFILE_EDU_INSTITUTION_IX = 4;
        int ORIGINAL_DOCUMENT_HANDED_IN_IX = 5;
        int COMPETITION_KIND_IX = 6;
        int SHORT_TITLE_IX = 7;
        int TARGET_ADMISSION_KIND_IX = 8;
        int ENTRANT_ID_IX = 9;
        int FULL_FIO_IX = 10;
        int MARK3_IX = 11;
        int MARK4_IX = 12;
        int MARK5_IX = 13;
        int PROFILE_MARK_IX = 14;
        int FINAL_MARK_IX = 15;

        builder.group(DQLExpressions.property(RequestedEnrollmentDirection.id().fromAlias("e")));
        builder.group(DQLExpressions.property(EnrollmentDirection.id().fromAlias("dir")));
        builder.group(DQLExpressions.property(EntrantRequest.regNumber().fromAlias("req")));
        builder.group(DQLExpressions.property(RequestedEnrollmentDirection.priority().fromAlias("e")));
        builder.group(DQLExpressions.property(RequestedEnrollmentDirection.graduatedProfileEduInstitution().fromAlias("e")));
        builder.group(DQLExpressions.property(RequestedEnrollmentDirection.originalDocumentHandedIn().fromAlias("e")));
        builder.group(DQLExpressions.property(CompetitionKind.id().fromAlias("com")));
        builder.group(DQLExpressions.property(EducationLevelsHighSchool.shortTitle().fromAlias("hs")));
        builder.group(DQLExpressions.property(TargetAdmissionKind.id().fromAlias("tar")));
        builder.group(DQLExpressions.property(Entrant.id().fromAlias("ent")));
        builder.group(DQLExpressions.property(IdentityCard.fullFio().fromAlias("card")));
        builder.group(DQLExpressions.property(PersonEduInstitution.mark3().fromAlias("edu")));
        builder.group(DQLExpressions.property(PersonEduInstitution.mark4().fromAlias("edu")));
        builder.group(DQLExpressions.property(PersonEduInstitution.mark5().fromAlias("edu")));
        builder.group(DQLExpressions.property(ChosenEntranceDiscipline.finalMark().fromAlias("pcd")));

        builder.column(DQLExpressions.property(RequestedEnrollmentDirection.id().fromAlias("e")));
        builder.column(DQLExpressions.property(EnrollmentDirection.id().fromAlias("dir")));
        builder.column(DQLExpressions.property(EntrantRequest.regNumber().fromAlias("req")));
        builder.column(DQLExpressions.property(RequestedEnrollmentDirection.priority().fromAlias("e")));
        builder.column(DQLExpressions.property(RequestedEnrollmentDirection.graduatedProfileEduInstitution().fromAlias("e")));
        builder.column(DQLExpressions.property(RequestedEnrollmentDirection.originalDocumentHandedIn().fromAlias("e")));
        builder.column(DQLExpressions.property(CompetitionKind.id().fromAlias("com")));
        builder.column(DQLExpressions.property(EducationLevelsHighSchool.shortTitle().fromAlias("hs")));
        builder.column(DQLExpressions.property(TargetAdmissionKind.id().fromAlias("tar")));
        builder.column(DQLExpressions.property(Entrant.id().fromAlias("ent")));
        builder.column(DQLExpressions.property(IdentityCard.fullFio().fromAlias("card")));
        builder.column(DQLExpressions.property(PersonEduInstitution.mark3().fromAlias("edu")));
        builder.column(DQLExpressions.property(PersonEduInstitution.mark4().fromAlias("edu")));
        builder.column(DQLExpressions.property(PersonEduInstitution.mark5().fromAlias("edu")));
        builder.column(DQLExpressions.property(ChosenEntranceDiscipline.finalMark().fromAlias("pcd")));

        builder.column(DQLFunctions.sum(DQLExpressions.property(ChosenEntranceDiscipline.finalMark().fromAlias("c"))));

        List<Object[]> dataRowList = builder.createStatement(getSession()).list();

        TargetAdmissionKind defaultTaKind = (TargetAdmissionKind) getByNaturalId(new TargetAdmissionKindGen.NaturalId("1"));

        Map<Long, Map<TargetAdmissionKind, Map<CompetitionKind, List<Object[]>>>> preRateMap = new Hashtable<Long, Map<TargetAdmissionKind, Map<CompetitionKind, List<Object[]>>>>();
        List<Long> directionIds = new ArrayList<Long>();


        for (Object[] dataRow : dataRowList) {
            long entrantId = ((Long) dataRow[ENTRANT_ID_IX]).longValue();
            Long redId = ((Long) dataRow[REQUESTED_ENROLLMENT_DIRECTION_ID_IX]).longValue();
            if (!directionIds.contains(redId))
                directionIds.add(redId);


            TargetAdmissionKind taKind = null;
            if (targetAdmission) {
                Long targetAdmissionKindId = (Long) dataRow[TARGET_ADMISSION_KIND_IX];
                taKind = targetAdmissionKindId == null ? defaultTaKind : taKindMap.get(targetAdmissionKindId);
                if (taKind == null)
                    continue;
            }

            Map<TargetAdmissionKind, Map<CompetitionKind, List<Object[]>>> taMap = preRateMap.get(Long.valueOf(entrantId));
            if (taMap == null) {
                preRateMap.put(Long.valueOf(entrantId), taMap = new HashMap<TargetAdmissionKind, Map<CompetitionKind, List<Object[]>>>());
            }
            Map<CompetitionKind, List<Object[]>> coMap = (Map<CompetitionKind, List<Object[]>>) taMap.get(taKind);
            if (coMap == null) {
                taMap.put(taKind, coMap = new Hashtable<CompetitionKind, List<Object[]>>());
            }
            long competitionKindId = ((Long) dataRow[COMPETITION_KIND_IX]).longValue();
            CompetitionKind competitionKind = (CompetitionKind) coKindMap.get(Long.valueOf(competitionKindId));

            List<Object[]> list = coMap.get(competitionKind);
            if (list == null) {
                coMap.put(competitionKind, list = new ArrayList<Object[]>());
            }
            list.add(dataRow);

        }
        Comparator<CompetitionKind> competitionKindComparator = new Comparator<CompetitionKind>() {
            @Override
            public int compare(CompetitionKind o1, CompetitionKind o2) {
                return competitionKindPriorityMap.get(o1).intValue() - competitionKindPriorityMap.get(o2).intValue();
            }
        };
        Comparator<Object[]> directionComparator = new Comparator<Object[]>() {
            @Override
            public int compare(Object[] o1, Object[] o2) {
                int result = ((Integer) o1[REQUEST_NUMBER_IX]).compareTo((Integer) o2[REQUEST_NUMBER_IX]);
                if (result != 0) return result;
                return ((Integer) o1[PRIORITY_NUMBER_IX]).compareTo((Integer) o2[PRIORITY_NUMBER_IX]);
            }
        };

        Map<Long, List<EnrollmentRecommendation>> recommendMap = EcDistributionUtil.getRecommendationMap(preRateMap.keySet());
        Map<Long, Integer> enrPriorityMap = EcDistributionUtil.getEnrollmentPriorityMap(preRateMap.keySet());
        Map<Long, List<WrapChoseEntranseDiscipline>> edDiscMap = EcDistributionUtil.getChosenDisciplineMap(directionIds);
        Map<Long, List<WrapRequestEDInfo>> mapEntrant2RD = EcDistributionUtil.getMapEntrantToRd(directionIds);

        List<EcgEntrantRateRowDTORMC> resultList = new ArrayList<EcgEntrantRateRowDTORMC>();

        for (Map.Entry<Long, Map<TargetAdmissionKind, Map<CompetitionKind, List<Object[]>>>> entry : preRateMap.entrySet()) {
            Map<TargetAdmissionKind, Map<CompetitionKind, List<Object[]>>> taMap = entry.getValue();
            List<TargetAdmissionKind> taKindList = new ArrayList<TargetAdmissionKind>(taMap.keySet());
            if (taKindList.size() > 1)
                Collections.sort(taKindList, TargetAdmissionKind.ALL_LEVEL_PRIORITY_COMPARATOR);
            TargetAdmissionKind taKind = (TargetAdmissionKind) taKindList.get(0);

            Map<CompetitionKind, List<Object[]>> coMap = taMap.get(taKind);
            List<CompetitionKind> coList = new ArrayList<CompetitionKind>(coMap.keySet());
            if (coList.size() > 1)
                Collections.sort(coList, competitionKindComparator);
            CompetitionKind competitionKind = (CompetitionKind) coList.get(0);

            List<Object[]> list = coMap.get(competitionKind);
            Collections.sort(list, directionComparator);

            List<IEcgEntrantRateDirectionDTO> directionList = new ArrayList<IEcgEntrantRateDirectionDTO>();

            for (Object[] row : list) {
                Long requestedEnrollmentDirectionId = (Long) row[REQUESTED_ENROLLMENT_DIRECTION_ID_IX];
                Long enrollmentDirectionId = (Long) row[ENROLLMENT_DIRECTION_ID_IX];
                String shortTitle = (String) row[SHORT_TITLE_IX];
                directionList.add(new EcgEntrantRateDirectionDTO(requestedEnrollmentDirectionId, enrollmentDirectionId, shortTitle));
            }

            Object[] dataRow = (Object[]) list.get(0);
            directionIds.add((Long) dataRow[REQUESTED_ENROLLMENT_DIRECTION_ID_IX]);
            String fio = (String) dataRow[FULL_FIO_IX];
            boolean originalDocumentHandedIn = ((Boolean) dataRow[ORIGINAL_DOCUMENT_HANDED_IN_IX]).booleanValue();
            Double profileMark = (Double) dataRow[PROFILE_MARK_IX];
            Double finalMark = (Double) dataRow[FINAL_MARK_IX];
            boolean graduatedProfileEduInstitution = ((Boolean) dataRow[GRADUATED_PROFILE_EDU_INSTITUTION_IX]).booleanValue();

            Integer m3 = (Integer) dataRow[MARK3_IX];
            Integer m4 = (Integer) dataRow[MARK4_IX];
            Integer m5 = (Integer) dataRow[MARK5_IX];
            m3 = Integer.valueOf(m3 != null ? m3.intValue() : 0);
            m4 = Integer.valueOf(m4 != null ? m4.intValue() : 0);
            m5 = Integer.valueOf(m5 != null ? m5.intValue() : 0);
            int sum = m3.intValue() + m4.intValue() + m5.intValue();
            double total = m3.intValue() * 3 + m4.intValue() * 4 + m5.intValue() * 5;
            Double averageMark = sum != 0 ? Double.valueOf(total / sum) : null;

            Long entrantId = (Long) entry.getKey();
            List<WrapRequestEDInfo> edEntrant = mapEntrant2RD.get(entrantId);

            resultList.add(new EcgEntrantRateRowDTORMC(entrantId, fio, finalMark == null ? 0.0D : finalMark.doubleValue(), profileMark, averageMark, originalDocumentHandedIn, graduatedProfileEduInstitution, targetAdmission, taKind, competitionKind, directionList, recommendMap.get(entry.getKey()), edDiscMap.get((Long) dataRow[REQUESTED_ENROLLMENT_DIRECTION_ID_IX]), haveSpecilaRights, edEntrant, enrPriorityMap.get(entry.getKey())));
        }

        Collections.sort(resultList, getComparator(getDistributionDirectionList(distribution.getDistribution()).get(0)));

        return resultList;
    }

    private DQLSelectBuilder getDistributionDetailRateBuilder(EcgDistributionDetail distributionDetail, Rule rateRule)
    {
        IDQLSelectableQuery distributionDirectionQuery = new DQLSelectBuilder().fromEntity(EcgDistributionQuota.class, "q")
                .column(DQLExpressions.property(EcgDistributionQuota.direction().id().fromAlias("q")))
                .where(DQLExpressions.eq(DQLExpressions.property(EcgDistributionQuota.distribution().fromAlias("q")), DQLExpressions.value(distributionDetail.getDistribution()))).buildQuery();

        DQLSelectBuilder builder = (DQLSelectBuilder) new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "e");

        builder.where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.state().fromAlias("e")), DQLExpressions.commonValue(getByNaturalId(new EntrantStateGen.NaturalId("4")))));
        builder.where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().entrant().archival().fromAlias("e")), DQLExpressions.value(Boolean.FALSE)));

        builder.where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.compensationType().fromAlias("e")), DQLExpressions.value(distributionDetail.getConfig().getCompensationType())));
        builder.where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().id().fromAlias("e")), distributionDirectionQuery));
        builder.where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirection.studentCategory().id().fromAlias("e")),
                                        (Collection<Long>) EcDistributionUtil.invokePrivateMethod(this, EcDistributionDao.class, "getStudentCategoriesForDistribution", new Object[]{distributionDetail})
        ));

        builder.where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.originalDocumentHandedIn().fromAlias("e")), DQLExpressions.value(Boolean.TRUE)));

        DQLSelectBuilder recommendedBuilder = new DQLSelectBuilder().fromEntity(EcgEntrantRecommended.class, "r")
                .joinEntity("r", DQLJoinType.inner, EcgEntrantRecommendedState.class, "state",
                            DQLExpressions.and(new IDQLExpression[]{
                                    DQLExpressions.eq(DQLExpressions.property(EcgEntrantRecommended.id().fromAlias("r")), DQLExpressions.property(EcgEntrantRecommendedState.recommended().id().fromAlias("state"))),
                                    DQLExpressions.eq(DQLExpressions.property(EcgEntrantRecommendedState.bringOriginal().fromAlias("state")), DQLExpressions.value(Boolean.TRUE))}))
                .column(DQLExpressions.property(EcgEntrantRecommended.direction().entrantRequest().entrant().id().fromAlias("r")))
                .where(DQLExpressions.eq(DQLExpressions.property(EcgEntrantRecommended.distribution().fromAlias("r")), DQLExpressions.value(distributionDetail.getDistribution())))
                .where(DQLExpressions.eq(DQLExpressions.property(EcgEntrantRecommended.distribution().state().fromAlias("r")), DQLExpressions.commonValue(getByNaturalId(new EcgDistributionStateGen.NaturalId("3")))));

        switch (rateRule) {
            case RULE_TARGET_ADMISSION:
                recommendedBuilder.where(DQLExpressions.isNotNull(DQLExpressions.property(EcgEntrantRecommended.targetAdmissionKind().fromAlias("r"))));
                break;
            case RULE_NON_COMPETITIVE:
                recommendedBuilder
                        .joinEntity("r", DQLJoinType.inner, EcgEntrantRecommendedExt.class, "ext",
                                    DQLExpressions.eq(
                                            DQLExpressions.property(EcgEntrantRecommended.id().fromAlias("r")),
                                            DQLExpressions.property(EcgEntrantRecommendedExt.ecgEntrantRecommended().id().fromAlias("ext")))
                        )
                        .where(DQLExpressions.eqValue(DQLExpressions.property(EcgEntrantRecommendedExt.haveSpecialRights().fromAlias("ext")), Boolean.TRUE))
                ;
                break;
            default:
                recommendedBuilder
                        .where(DQLExpressions.isNull(DQLExpressions.property(EcgEntrantRecommended.targetAdmissionKind().fromAlias("r"))))
                        .where(DQLExpressions.notIn(DQLExpressions.property(EcgEntrantRecommended.id().fromAlias("r")), new DQLSelectBuilder().fromEntity(EcgEntrantRecommendedExt.class, "ext")
                                                            .column(DQLExpressions.property(EcgEntrantRecommendedExt.ecgEntrantRecommended().id().fromAlias("ext")))
                                                            .buildQuery()
                        ))
                ;
        }

        builder.where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("e")), recommendedBuilder.buildQuery()));

        builder.where(DQLExpressions.notIn(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("e")), new DQLSelectBuilder().fromEntity(EcgEntrantRecommendedDetail.class, "rd")
                .column(DQLExpressions.property(EcgEntrantRecommendedDetail.direction().id().fromAlias("rd")))
                .where(DQLExpressions.eq(DQLExpressions.property(EcgEntrantRecommendedDetail.distribution().fromAlias("rd")), DQLExpressions.value(distributionDetail))).buildQuery()));

        //выкинуть пред зачисленных
        builder.where(DQLExpressions.notIn(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("e")),
                                           (IDQLSelectableQuery) EcDistributionUtil.invokePrivateMethod(this, EcDistributionDao.class, "getPreStudentQuery", new Object[]{distributionDetail})
        ));
        return builder;
    }

    /**
     * Dql выражение, исключающее абитуриентов,
     * по дополнительной просьбе аналитиков
     *
     * @param ec
     *
     * @return
     */
    @Override
    public DQLSelectBuilder getDqlExcludeEntrantToEc(EnrollmentCampaign ec, IEcgDistribution distribution)
    {
        return null;
    }

    private Map<Long, List<EnrollmentRecommendation>> getRecommendationMap(Collection<Long> entrantIds) {
        DQLSelectBuilder recommendBuilder = new DQLSelectBuilder().fromEntity(EntrantEnrolmentRecommendation.class, "e")
                .where(DQLExpressions.in(DQLExpressions.property(EntrantEnrolmentRecommendation.entrant().id().fromAlias("e")), entrantIds));
        List<EntrantEnrolmentRecommendation> recommendations = getList(recommendBuilder);
        Map<Long, List<EnrollmentRecommendation>> recommendMap = new HashMap<Long, List<EnrollmentRecommendation>>();
        for (EntrantEnrolmentRecommendation recommendation : recommendations)
            SafeMap.safeGet(recommendMap, recommendation.getEntrant().getId(), ArrayList.class).add(recommendation.getRecommendation());

        return recommendMap;
    }

    /*
    private Map<Long, List<ChosenEntranceDiscipline>> getChosenDisciplineMap(List<Long> directionIds){
        DQLSelectBuilder edBuilder = new DQLSelectBuilder().fromEntity(ChosenEntranceDiscipline.class, "ced")
                .joinPath(DQLJoinType.inner, ChosenEntranceDiscipline.chosenEnrollmentDirection().enrollmentDirection().fromAlias("ced"), "dir")
                .joinEntity("ced", DQLJoinType.inner, EntranceDiscipline.class, "ed",
                        DQLExpressions.and(
                                DQLExpressions.eq(
                                        DQLExpressions.property("dir.id"),
                                        DQLExpressions.property(EntranceDiscipline.enrollmentDirection().id().fromAlias("ed"))
                                        ),
                                        DQLExpressions.eq(
                                                DQLExpressions.property(ChosenEntranceDiscipline.enrollmentCampaignDiscipline().id().fromAlias("ced")),
                                                DQLExpressions.property(EntranceDiscipline.discipline().id().fromAlias("ed"))
                                                )
                                )
                        )
                        .joinEntity("ed", DQLJoinType.inner, EntranceDisciplineExt.class, "ext",
                                DQLExpressions.eq(
                                        DQLExpressions.property(EntranceDiscipline.id().fromAlias("ed")),
                                        DQLExpressions.property(EntranceDisciplineExt.entranceDiscipline().id().fromAlias("ext"))))

                                        .where(DQLExpressions.in(DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("ced")), directionIds))
                                        .column(DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("ced")))
                                        .column("ced")
                                        .order(DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("ced")))
                                        .order(DQLExpressions.property(EntranceDisciplineExt.priority().fromAlias("ext")))
                                        ;

        List<Object[]> rows = getList(edBuilder);
        Map<Long, List<ChosenEntranceDiscipline>> edDiscMap = new HashMap<Long, List<ChosenEntranceDiscipline>>();
        for (Object[] objects : rows) {
            Long redId = (Long)objects[0];
            ChosenEntranceDiscipline disc = (ChosenEntranceDiscipline) objects[1];
            List<ChosenEntranceDiscipline> list = edDiscMap.get(redId);
            if (list == null)
                edDiscMap.put(redId, list = new ArrayList<ChosenEntranceDiscipline>());
            list.add(disc);
        }

        return edDiscMap;
    }
*/
    public void saveEntrantRecommendedList(IEcgDistribution distribution, boolean targetAdmission, boolean haveSpecialRights, Collection<Long> chosenDirectionIds) {
        Session session = getSession();
        session.refresh(distribution);

        if (distribution.getState().isLockedOrApproved())
            throw new ApplicationException(new StringBuilder().append("Распределение «").append(distribution.getTitle()).append("» уже нельзя изменять.").toString());

        IPersistentEcgItem ecgItem = distribution.getConfig().getEcgItem();
        EnrollmentCampaign enrollmentCampaign = ecgItem.getEnrollmentCampaign();
        Set<EnrollmentDirection> directionSet = new HashSet<EnrollmentDirection>(getDistributionDirectionList(distribution.getDistribution()));
        Set<Long> usedDirectionIds;
        if (distribution instanceof EcgDistribution) {
            List<Long> ids = new DQLSelectBuilder().fromEntity(EcgEntrantRecommended.class, "e")
                    .column(DQLExpressions.property(EcgEntrantRecommended.direction().id().fromAlias("e")))
                    .where(DQLExpressions.eq(DQLExpressions.property(EcgEntrantRecommended.distribution().fromAlias("e")), DQLExpressions.value(distribution)))
                    .createStatement(session).list();
            usedDirectionIds = new HashSet<Long>(ids);
        }
        else {
            List<Long> ids = new DQLSelectBuilder().fromEntity(EcgEntrantRecommendedDetail.class, "e")
                    .column(DQLExpressions.property(EcgEntrantRecommendedDetail.direction().id().fromAlias("e")))
                    .where(DQLExpressions.eq(DQLExpressions.property(EcgEntrantRecommendedDetail.distribution().fromAlias("e")), DQLExpressions.value(distribution)))
                    .createStatement(session).list();
            usedDirectionIds = new HashSet<Long>(ids);
        }

        Set<Long> usedEntrantIds = new HashSet<Long>();

        List<RequestedEnrollmentDirection> directionList = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "e")
                .column("e")
                .where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirection.id().fromAlias("e")), chosenDirectionIds))
                .createStatement(session).list();

        TargetAdmissionKind defaultTargetAdmissionKind = (targetAdmission) ? (TargetAdmissionKind) getByNaturalId(new TargetAdmissionKindGen.NaturalId("1")) : null;

        for (RequestedEnrollmentDirection direction : directionList) {
            EnrollmentDirection enrollmentDirection = direction.getEnrollmentDirection();
            TargetAdmissionKind targetAdmissionKind = direction.getTargetAdmissionKind();

            if (!(usedEntrantIds.add(direction.getEntrantRequest().getEntrant().getId())))
                throw new RuntimeException(new StringBuilder().append("Duplicate entrant '").append(direction.getEntrantRequest().getEntrant().getId()).append("' in distribution '").append(distribution.getId()).append("'.").toString());

            if (!(enrollmentDirection.getEnrollmentCampaign().equals(enrollmentCampaign)))
                throw new RuntimeException(new StringBuilder().append("EnrollmentCampaign '").append(enrollmentDirection.getEnrollmentCampaign()).append("' is not allowed for distribution '").append(distribution.getId()).append("'.").toString());

            if (!(directionSet.contains(direction.getEnrollmentDirection())))
                throw new RuntimeException(new StringBuilder().append("EnrollmentDirection '").append(direction.getEnrollmentDirection().getId()).append("' is not allowed for distribution '").append(distribution.getId()).append("'.").toString());

            if ((targetAdmission) && (targetAdmissionKind == null))
                targetAdmissionKind = defaultTargetAdmissionKind;

            if (usedDirectionIds.contains(direction.getId()))
                throw new ApplicationException(new StringBuilder().append("Абитуриента «").append(direction.getEntrantRequest().getEntrant().getPerson().getFullFio()).append("» по направлению «").append(direction.getTitle()).append("» уже добавили в это распределение.").toString());

            if (!(isConfigAllowDirection(distribution.getConfig(), direction)))
                throw new ApplicationException(new StringBuilder().append("У абитуриента «").append(direction.getEntrantRequest().getEntrant().getPerson().getFullFio()).append("» направление «").append(direction.getTitle()).append("» не может быть добавлено в распределение: не подходящие вид затрат или категория поступающего.").toString());

            if (distribution instanceof EcgDistribution) {
                EcgEntrantRecommended item = new EcgEntrantRecommended();
                item.setDistribution((EcgDistribution) distribution);
                item.setDirection(direction);
                if (targetAdmission)
                    item.setTargetAdmissionKind(targetAdmissionKind);
                session.save(item);
                if (haveSpecialRights) {
                    EcgEntrantRecommendedExt itemExt = new EcgEntrantRecommendedExt();
                    itemExt.setEcgEntrantRecommended(item);
                    itemExt.setHaveSpecialRights(true);
                    session.save(itemExt);
                }
            }
            else {
                EcgEntrantRecommendedDetail item = new EcgEntrantRecommendedDetail();
                item.setDistribution((EcgDistributionDetail) distribution);
                item.setDirection(direction);
                if (targetAdmission)
                    item.setTargetAdmissionKind(targetAdmissionKind);
                item.setDirection(direction);
                session.save(item);
                if (haveSpecialRights) {
                    EcgEntrantRecommendedDetailExt itemExt = new EcgEntrantRecommendedDetailExt();
                    itemExt.setEcgEntrantRecommendedDetail(item);
                    itemExt.setHaveSpecialRights(true);
                    session.save(itemExt);
                }
            }
        }
    }

    public IEcgPlanChoiceResult getPossibleDirectionList(IEcgEntrantRateRowDTO rateRowDTO, Long preferableDirectionId, Map<Long, MutableInt> freeMap, Map<Long, MutableInt> srFreeMap) {
        List<IEcgEntrantRateDirectionDTO> list = rateRowDTO.getDirectionList();

        IEcgEntrantRateDirectionDTO chosenDirection = null;
        List<IEcgEntrantRateDirectionDTO> possibleDirectionList = new ArrayList<IEcgEntrantRateDirectionDTO>();

        MutableInt preferableMutableQuota = null;
        MutableInt preferableMutableSrQuota = null;

        for (IEcgEntrantRateDirectionDTO direction : list) {
            MutableInt mutableQuota = freeMap.get(direction.getDirectionId());
            MutableInt mutableSrQuota = srFreeMap.get(direction.getDirectionId());

            if ((mutableQuota.intValue() > 0) && (null != mutableSrQuota) && (mutableSrQuota.intValue() > 0)) {
                possibleDirectionList.add(direction);

                if (direction.getId().equals(preferableDirectionId)) {
                    chosenDirection = direction;
                    preferableMutableQuota = mutableQuota;
                    preferableMutableSrQuota = mutableSrQuota;
                }
            }
        }

        if (chosenDirection == null) {
            if (!possibleDirectionList.isEmpty()) {
                chosenDirection = (IEcgEntrantRateDirectionDTO) possibleDirectionList.get(0);
                freeMap.get(chosenDirection.getDirectionId()).decrement();
                srFreeMap.get(chosenDirection.getDirectionId()).decrement();
            }
        }
        else {
            preferableMutableQuota.decrement();
            preferableMutableSrQuota.decrement();
        }

        return new EcgPlanChoiceResult(chosenDirection, possibleDirectionList);
    }

    @Override
    public void doBulkMakeDistribution(Set<EcgConfigDTO> configSet) {

        if (!isUsePriority(configSet.iterator().next().getEcgItem().getEnrollmentCampaign())) {
            super.doBulkMakeDistribution(configSet);
            return;
        }

        if (configSet.isEmpty()) return;

        EcgDistribution maxDistribution = getDistributionWithMaxWave((EcgConfigDTO) configSet.iterator().next());

        int wave = maxDistribution == null ? 1 : maxDistribution.getWave() + 1;

        for (EcgConfigDTO config : configSet) {
            IEcgQuotaDTO quotaDTO = getNextQuotaDTO(config);
            Map<Long, Integer> srQuotaMap = getNextSRQuota(config);

            if (!quotaDTO.isEmpty()) {
                EcgDistribution distribution = saveDistribution(config, wave, quotaDTO.getQuotaMap(), quotaDTO.getTaQuotaMap());
                saveSRQuota(distribution.getId(), srQuotaMap);
            }
        }
    }

    @Override
    public void doDistributionFill(EcgDistribution distribution) {

        if (!isUsePriority(distribution.getDistribution().getConfig().getEcgItem().getEnrollmentCampaign())) {
            super.doDistributionFill(distribution);
            return;
        }

        Session session = getSession();
        session.refresh(distribution);

        if (distribution.getState().isLockedOrApproved())
            throw new ApplicationException(new StringBuilder().append("Распределение «").append(distribution.getTitle()).append("» уже нельзя изменять.").toString());

        IEcgQuotaFreeDTO freeQuotaDTO = getFreeQuotaDTO(distribution);
        List<TargetAdmissionKind> targetAdmissionKindList = freeQuotaDTO.getQuotaDTO().getTaKindList();
        IEcgQuotaManager quotaManager = new EcgQuotaManager(freeQuotaDTO);

        List<Long> taDirectionList = new ArrayList<Long>();
        List<Long> directionList = new ArrayList<Long>();
        List<Long> srDirectionList = new ArrayList<Long>();
        Set<Long> usedEntrantIds = new HashSet<Long>();

        accumulateRateRow(distribution, targetAdmissionKindList, usedEntrantIds, quotaManager, EcDistributionUtil.Rule.RULE_TARGET_ADMISSION, taDirectionList);
        accumulateRateRow(distribution, targetAdmissionKindList, usedEntrantIds, quotaManager, EcDistributionUtil.Rule.RULE_WITHOUT_ENTRANCE_DISCIPLINES, directionList);
        accumulateRateRow(distribution, targetAdmissionKindList, usedEntrantIds, quotaManager, EcDistributionUtil.Rule.RULE_NON_COMPETITIVE, srDirectionList);
        accumulateRateRow(distribution, targetAdmissionKindList, usedEntrantIds, quotaManager, EcDistributionUtil.Rule.RULE_COMPETITIVE, directionList);

        saveEntrantRecommendedList(distribution, true, false, taDirectionList);
        saveEntrantRecommendedList(distribution, false, false, directionList);
        saveEntrantRecommendedList(distribution, false, true, srDirectionList);
    }

    private void accumulateRateRow(EcgDistribution distribution, List<TargetAdmissionKind> targetAdmissionKindList, Set<Long> usedEntrantIds, IEcgQuotaManager quotaManager, EcDistributionUtil.Rule rateRule, List<Long> directionList) {
        List<? extends IEcgEntrantRateRowDTO> rateList = getEntrantRateRowList(distribution, targetAdmissionKindList, rateRule);
        int len = rateList.size();
        int i = 0;
        int freeTotal = quotaManager.getFreeTotal();

        Map<Long, Integer> _srFreeMap = getFreeSRQuota(distribution);
        Map<Long, MutableInt> srFreeMap = new Hashtable<Long, MutableInt>();

        for (Map.Entry<Long, Integer> entry : _srFreeMap.entrySet()) {
            int free = ((Integer) entry.getValue()).intValue();
            srFreeMap.put(entry.getKey(), new MutableInt(free));
        }

        while ((i < len) && (freeTotal > 0)) {
            IEcgEntrantRateRowDTO rateRowDTO = (IEcgEntrantRateRowDTO) rateList.get(i);

            if (!usedEntrantIds.contains(rateRowDTO.getId())) {

                IEcgPlanChoiceResult choiceResult;
                if (rateRule.equals(EcDistributionUtil.Rule.RULE_NON_COMPETITIVE)) {
                    Map<Long, MutableInt> freeMap = (Map<Long, MutableInt>) EcDistributionUtil.getPrivateField(quotaManager, "_freeMap");
                    choiceResult = getPossibleDirectionList(rateRowDTO, null, freeMap, srFreeMap);
                    EcDistributionUtil.setPrivateField(quotaManager, "_freeMap", freeMap);
                }
                else
                    choiceResult = quotaManager.getPossibleDirectionList(rateRowDTO, null);

                IEcgEntrantRateDirectionDTO chosenDirection = choiceResult.getChosenDirection();

                if (chosenDirection != null) {
                    freeTotal--;
                    directionList.add(chosenDirection.getId());
                    usedEntrantIds.add(rateRowDTO.getId());
                }
            }

            i++;
        }
    }

    @Override
    public List<IEcgRecommendedPreStudentDTO> getEntrantRecommendedPreStudentRowList(
            IEcgDistribution distribution)
    {

        if (!isUsePriority(distribution.getDistribution().getConfig().getEcgItem().getEnrollmentCampaign()))
            return super.getEntrantRecommendedPreStudentRowList(distribution);

        List<IEcgRecommendedDTO> recommendedDTOList = getEntrantRecommendedRowList(distribution);

        Map<Long, EcgRecommendedDTORMC> map = new HashMap<Long, EcgRecommendedDTORMC>();
        for (IEcgRecommendedDTO recommended : recommendedDTOList) {
            EcgRecommendedDTORMC dtormc = (EcgRecommendedDTORMC) recommended;
            map.put(dtormc.getEntrantRecommended().getId(), dtormc);
        }

        List<IEcgRecommendedPreStudentDTO> list = super.getEntrantRecommendedPreStudentRowList(distribution);
        List<IEcgRecommendedPreStudentDTO> result = new ArrayList<IEcgRecommendedPreStudentDTO>();
        for (IEcgRecommendedPreStudentDTO recommended : list) {
            EcgRecommendedDTORMC dtormc = map.get(recommended.getEntrantRecommended().getId());
            result.add(new EcgRecommendedPreStudentDTORMC((EcgRecommendedPreStudentDTO) recommended, dtormc.getRecommendations(), dtormc.getDisciplines(), dtormc.isHaveSpecialRights()));
        }

        return result;
    }

    public boolean isUsePriority(EnrollmentCampaign campaign) {
        EntrantRecommendedSettings settings = UniDaoFacade.getCoreDao().get(EntrantRecommendedSettings.class, EntrantRecommendedSettings.enrollmentCampaign(), campaign);
        return settings != null && settings.isUsePriorityEntranceDiscipline();
    }

    @Override
    public List<IEcgRecommendedDTO> getEntrantRecommendedRowList(
            IEcgDistribution distribution)
    {

        if (!isUsePriority(distribution.getDistribution().getConfig().getEcgItem().getEnrollmentCampaign()))
            return super.getEntrantRecommendedRowList(distribution);

        List<IEcgEntrantRecommended> entrantRecommendedList;
        Map<IEcgEntrantRecommended, Boolean> haveSRMap = new HashMap<IEcgEntrantRecommended, Boolean>();
        if ((distribution instanceof EcgDistribution)) {
            entrantRecommendedList = new DQLSelectBuilder().fromEntity(EcgEntrantRecommended.class, "e")
                    .column("e")
                    .where(DQLExpressions.eq(DQLExpressions.property(EcgEntrantRecommended.distribution().fromAlias("e")), DQLExpressions.value(distribution)))
                    .createStatement(getSession()).list();
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EcgEntrantRecommendedExt.class, "ext")
                    .where(DQLExpressions.in(DQLExpressions.property(EcgEntrantRecommendedExt.ecgEntrantRecommended().fromAlias("ext")), entrantRecommendedList));
            List<EcgEntrantRecommendedExt> lst = getList(builder);
            for (EcgEntrantRecommendedExt ext : lst)
                haveSRMap.put(ext.getEcgEntrantRecommended(), ext.isHaveSpecialRights());
        }
        else {
            entrantRecommendedList = new DQLSelectBuilder().fromEntity(EcgEntrantRecommendedDetail.class, "e")
                    .column("e")
                    .where(DQLExpressions.eq(DQLExpressions.property(EcgEntrantRecommendedDetail.distribution().fromAlias("e")), DQLExpressions.value(distribution)))
                    .createStatement(getSession()).list();
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EcgEntrantRecommendedDetailExt.class, "ext")
                    .where(DQLExpressions.in(DQLExpressions.property(EcgEntrantRecommendedDetailExt.ecgEntrantRecommendedDetail().fromAlias("ext")), entrantRecommendedList));
            List<EcgEntrantRecommendedDetailExt> lst = getList(builder);
            for (EcgEntrantRecommendedDetailExt ext : lst)
                haveSRMap.put(ext.getEcgEntrantRecommendedDetail(), ext.isHaveSpecialRights());
        }

        List<IEcgEntrantRecommended> activeList = new ArrayList<IEcgEntrantRecommended>();
        List<IEcgEntrantRecommended> nonActiveList = new ArrayList<IEcgEntrantRecommended>();
        for (IEcgEntrantRecommended recommended : entrantRecommendedList) {
            (isConfigAllowDirection(distribution.getConfig(), recommended.getDirection()) ? activeList : nonActiveList).add(recommended);
        }

        List<Long> directionIds = new ArrayList<Long>();
        for (IEcgEntrantRecommended recommended : activeList) {
            directionIds.add(recommended.getDirection().getId());
        }

        List<Long> entrantIds = new ArrayList<Long>();
        for (IEcgEntrantRecommended recommended : activeList) {
            entrantIds.add(recommended.getDirection().getEntrantRequest().getEntrant().getId());
        }

        Map<Long, EcgPriorityInfo> entrantPriorityInfoMap = getEntrantPriorityMap(distribution, entrantIds);
        Map<Long, MutableDouble> finalMarkMap = new Hashtable<Long, MutableDouble>();

        int REQUESTED_ENROLLMENT_DIRECTION_ID_IX = 0;
        int FINAL_MARK_IX = 1;

        List<Object[]> objs = new DQLSelectBuilder().fromEntity(ChosenEntranceDiscipline.class, "e")
                .column(DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("e")))
                .column(DQLExpressions.property(ChosenEntranceDiscipline.finalMark().fromAlias("e")))
                .where(DQLExpressions.in(DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("e")), directionIds))
                .where(DQLExpressions.isNotNull(ChosenEntranceDiscipline.finalMark().fromAlias("e")))
                .createStatement(getSession()).list();

        for (Object[] dataRow : objs) {
            Long requestedEnrollmentDirectionId = (Long) dataRow[REQUESTED_ENROLLMENT_DIRECTION_ID_IX];
            Double finalMark = (Double) dataRow[FINAL_MARK_IX];
            MutableDouble mutable = (MutableDouble) finalMarkMap.get(requestedEnrollmentDirectionId);
            if (mutable == null)
                finalMarkMap.put(requestedEnrollmentDirectionId, mutable = new MutableDouble());
            mutable.add(finalMark);
        }

        Map<Long, List<EnrollmentRecommendation>> recommendMap = EcDistributionUtil.getRecommendationMap(entrantIds);
        Map<Long, Integer> enrPriorityMap = EcDistributionUtil.getEnrollmentPriorityMap(entrantIds);
        Map<Long, List<WrapChoseEntranseDiscipline>> edDiscMap = EcDistributionUtil.getChosenDisciplineMap(directionIds);

        List<IEcgRecommendedDTO> rateList = new ArrayList<IEcgRecommendedDTO>();

        Map<EcgEntrantRecommended, Boolean> recommendedBringOriginalMap = null;
        if (((distribution instanceof EcgDistribution)) && (distribution.getState().isApproved())) {
            recommendedBringOriginalMap = new HashMap<EcgEntrantRecommended, Boolean>();

            List<EcgEntrantRecommendedState> stateList = new DQLSelectBuilder().fromEntity(EcgEntrantRecommendedState.class, "e")
                    .where(DQLExpressions.eq(DQLExpressions.property(EcgEntrantRecommendedState.distribution().fromAlias("e")), DQLExpressions.value(distribution)))
                    .where(DQLExpressions.eq(DQLExpressions.property(EcgEntrantRecommendedState.recommended().distribution().fromAlias("e")), DQLExpressions.value(distribution)))
                    .createStatement(getSession()).list();

            for (EcgEntrantRecommendedState item : stateList) {
                recommendedBringOriginalMap.put(item.getRecommended(), item.getBringOriginal());
            }
        }
        for (IEcgEntrantRecommended recommended : activeList) {
            EcgPriorityInfo ecgPriorityInfo = (EcgPriorityInfo) entrantPriorityInfoMap.get(recommended.getDirection().getEntrantRequest().getEntrant().getId());
            String priorities = (ecgPriorityInfo == null) || (ecgPriorityInfo.getPriorityList() == null) ? null : StringUtils.join(ecgPriorityInfo.getPriorityList(), ", ");
            MutableDouble finalMark = (MutableDouble) finalMarkMap.get(recommended.getDirection().getId());
            IEcgRecommendedDTO.DocumentStatus documentStatus;
            if (distribution.getState().isApproved()) {
                Boolean bringOriginal;
                if ((recommended instanceof EcgEntrantRecommended)) {
                    EcgEntrantRecommended recommendedFromGeneral = (EcgEntrantRecommended) recommended;
                    bringOriginal = (Boolean) recommendedBringOriginalMap.get(recommendedFromGeneral);
                }
                else {
                    EcgEntrantRecommendedDetail recommendedFromDetail = (EcgEntrantRecommendedDetail) recommended;
                    bringOriginal = recommendedFromDetail.getBringOriginal();
                }

                documentStatus = bringOriginal.booleanValue() ? IEcgRecommendedDTO.DocumentStatus.BRING_ORIGINAL : bringOriginal == null ? IEcgRecommendedDTO.DocumentStatus.TOOK_AWAY : IEcgRecommendedDTO.DocumentStatus.BRING_COPY;
            }
            else {
                documentStatus = recommended.getDirection().isOriginalDocumentHandedIn() ? IEcgRecommendedDTO.DocumentStatus.BRING_ORIGINAL : recommended.getDirection().getState().getCode().equals("7") ? IEcgRecommendedDTO.DocumentStatus.TOOK_AWAY : IEcgRecommendedDTO.DocumentStatus.BRING_COPY;
            }

            rateList.add(new EcgRecommendedDTORMC(recommended, Double.valueOf(finalMark == null ? 0.0D : finalMark.doubleValue()), priorities, documentStatus, recommendMap.get(recommended.getDirection().getEntrantRequest().getEntrant().getId()), edDiscMap.get(recommended.getDirection().getId()), haveSRMap.get(recommended), enrPriorityMap.get(recommended.getDirection().getEntrantRequest().getEntrant().getId())));
        }

        Collections.sort(rateList, getComparator(getDistributionDirectionList(distribution.getDistribution()).get(0)));

        Collections.sort(nonActiveList, new Comparator<IEcgEntrantRecommended>() {
            public int compare(IEcgEntrantRecommended o1, IEcgEntrantRecommended o2)
            {
                String fio1 = o1.getDirection().getEntrantRequest().getEntrant().getPerson().getIdentityCard().getFullFio();
                String fio2 = o2.getDirection().getEntrantRequest().getEntrant().getPerson().getIdentityCard().getFullFio();
                return fio1.compareTo(fio2);
            }
        });
        for (IEcgEntrantRecommended recommended : nonActiveList) {
            rateList.add(new EcgRecommendedDTORMC(recommended, null, null, null, null, null, haveSRMap.get(recommended), null));
        }
        return rateList;

    }

    @Override
    public Comparator<IEcgCortegeDTO> getComparator(EnrollmentDirection direction) {
        return direction.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isMiddle() ? new EcgContegeComparatorSPO() : new EcgContegeComparatorRMC();
    }

}
