package ru.tandemservice.uniecrmc.component.catalog.documentReturnMethod.DocumentReturnMethodPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.uniecrmc.entity.catalog.DocumentReturnMethod;

public class Controller extends DefaultCatalogPubController<DocumentReturnMethod, Model, IDAO> {
    @Override
    protected void addColumnsBeforeEditColumn(IBusinessComponent context,
                                              DynamicListDataSource<DocumentReturnMethod> dataSource)
    {

        dataSource.addColumn(new SimpleColumn("Печатное название", DocumentReturnMethod.printTitle().s()).setClickable(false), 1);
    }
}
