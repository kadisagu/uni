package ru.tandemservice.uniecrmc.component.report.EnrollmentResultByCategory.Pub;

import org.tandemframework.core.component.State;
import ru.tandemservice.uniecrmc.entity.entrant.EnrollmentResultByCategoryReport;

@State(keys = {"publisherId"}, bindings = {"report.id"})
public class Model {

    private EnrollmentResultByCategoryReport report = new EnrollmentResultByCategoryReport();

    public EnrollmentResultByCategoryReport getReport() {
        return report;
    }

    public void setReport(EnrollmentResultByCategoryReport report) {
        this.report = report;
    }
}

