package ru.tandemservice.uniecrmc.base.ext.SystemAction.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.component.*;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.ui.Pub.SystemActionPubUI;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uniecrmc.dao.UniecrmcDao;

public class EcRMCSystemActionPubAddon extends UIAddon {


    SystemActionPubUI parent = getPresenter();

    public EcRMCSystemActionPubAddon(IUIPresenter presenter, String name, String componentId) {
        super(presenter, name, componentId);
    }

    public void onClickFillPriorityEntranceDisciple() {
        UniecrmcDao.Instanse().fillPriorityED();
    }

    public void onClickChangeCompetitionKindSpo() {
        String logFile = UniecrmcDao.Instanse().changeCompetitionKindSpo();
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().txt().fileName("Лог.txt").document(logFile.getBytes()), false);
    }

    public void onChangeOlimpiadPolitics()
    {
        // вызываем другой компонент
        activateInRoot(new ComponentActivator(
                "ChangeOlimpiadMark"));

    }


    public void onAddOtherDirectionToRequest()
    {
        activateInRoot(new ComponentActivator(
                "AddOtherDirectionToRequest"));

    }

    @SuppressWarnings("rawtypes")
    private void activateInRoot(Activator activator) {

        IBusinessComponent currentComponent = parent.getUserContext().getCurrentComponent();

        IBusinessController controller = currentComponent.getController();
        controller.activateInRoot(currentComponent, activator);
    }

    public void onCloneChosenEntranceDiscipline()
    {
        activateInRoot(new ComponentActivator(
                "CloneChosenEntranceDiscipline"));
    }

}
