package ru.tandemservice.uniecrmc.component.catalog.targetAdmissionKind.TargetAdmissionKindPub;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.uniecrmc.entity.entrant.TargetAdmissionKindRMC;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DAO extends DefaultCatalogPubDAO implements IDAO {

    @Override
    public void prepareProjectsDataSource(Model model) {
        Wrapper wrapper;
        Wrapper wrapperParent;
        List list = new ArrayList<Wrapper>();

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(TargetAdmissionKind.class, "base")
                .joinEntity("base", DQLJoinType.left, TargetAdmissionKindRMC.class, "rmc",
                            DQLExpressions.eq(DQLExpressions.property(TargetAdmissionKind.id().fromAlias("base")),
                                              DQLExpressions.property(TargetAdmissionKindRMC.base().fromAlias("rmc"))))
                .addColumn("base");

        List<TargetAdmissionKind> base = dql.createStatement(getSession()).list();
        for (TargetAdmissionKind targetAdmissionKind : base) {

            wrapper = new Wrapper();
            wrapper.setBase(targetAdmissionKind);


            try {
                DQLSelectBuilder dqlnumber = new DQLSelectBuilder()
                        .fromEntity(TargetAdmissionKindRMC.class, "s")
                        .addColumn("s")
                        .where(in(property(TargetAdmissionKindRMC.base().id().fromAlias("s")), value(targetAdmissionKind.getId())));
                List<TargetAdmissionKindRMC> numb = dqlnumber.createStatement(getSession()).list();

                wrapper.setContractNumber(numb.get(0).getContractNumber());
            }
            catch (Exception e) {

            }

            try {
                if (targetAdmissionKind.getParent() != null) {

                    wrapperParent = new Wrapper();
                    DQLSelectBuilder dqlparent = new DQLSelectBuilder()
                            .fromEntity(TargetAdmissionKind.class, "s")
                            .addColumn("s")
                            .where(in(property(TargetAdmissionKind.id()
                                                       .fromAlias("s")), value(targetAdmissionKind
                                                                                       .getParent().getId())));

                    List<TargetAdmissionKind> parent = dqlparent
                            .createStatement(getSession()).list();
                    wrapperParent.setBase(parent.get(0));

                    DQLSelectBuilder dqlnumber1 = new DQLSelectBuilder()
                            .fromEntity(TargetAdmissionKindRMC.class, "s")
                            .addColumn("s")
                            .where(in(property(TargetAdmissionKindRMC.base()
                                                       .id().fromAlias("s")),
                                      value(targetAdmissionKind.getParent()
                                                    .getId())));
                    List<TargetAdmissionKindRMC> numb1 = dqlnumber1
                            .createStatement(getSession()).list();
                    wrapper.setParent(wrapperParent);
                    if (numb1.size() > 0) {

                        wrapperParent.setContractNumber(numb1.get(0)
                                                                .getContractNumber());
                    }

                }
            }
            catch (Exception e) {

            }
            list.add(wrapper);
        }

        UniUtils.createPage(model.getDataSource(), list);
    }


    public void deleteRow(IBusinessComponent context)
    {
        TargetAdmissionKind targetAdmissionKind = (TargetAdmissionKind) get(TargetAdmissionKind.class, (Long) context.getListenerParameter());
        TargetAdmissionKind parent = targetAdmissionKind.getParent();
        Session session = getSession();
        Criteria criteria = session.createCriteria("ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind");
        if (parent == null)
            criteria.add(Restrictions.isNull("parent"));
        else
            criteria.add(Restrictions.eq("parent", parent));
        criteria.add(Restrictions.gt("priority", Integer.valueOf(targetAdmissionKind.getPriority())));
        TargetAdmissionKind lower;
        for (Iterator i$ = criteria.list().iterator(); i$.hasNext(); session.update(lower)) {
            lower = (TargetAdmissionKind) i$.next();
            lower.setPriority(lower.getPriority() - 1);
        }

        super.deleteRow(context);
    }

}

