package ru.tandemservice.uniecrmc.component.entrant.EntrantEnrollmentDocumentTab;

import ru.tandemservice.uniecrmc.entity.entrant.EntrantExt;

public class DAO extends ru.tandemservice.uniec.component.entrant.EntrantEnrollmentDocumentTab.DAO {
    @Override
    public void prepare(ru.tandemservice.uniec.component.entrant.EntrantEnrollmentDocumentTab.Model model)
    {
        super.prepare(model);
        Model modelExt = (Model) model;
        EntrantExt entrantExt = get(EntrantExt.class, EntrantExt.entrant().s(), model.getEntrant());
        if (entrantExt != null)
            modelExt.setEntrantExt(entrantExt);
    }
}
