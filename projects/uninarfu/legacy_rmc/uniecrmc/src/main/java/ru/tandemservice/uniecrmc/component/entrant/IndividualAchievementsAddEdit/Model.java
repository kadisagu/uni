package ru.tandemservice.uniecrmc.component.entrant.IndividualAchievementsAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniecrmc.entity.entrant.Entrant2IndividualAchievements;
import ru.tandemservice.uniec.entity.entrant.Entrant;

@State({
        @Bind(key = "entrantId", binding = "entrant.id"),
        @Bind(key = "achievementId", binding = "achievement.id")})

public class Model {

    private Entrant entrant = new Entrant();
    private Entrant2IndividualAchievements achievement = new Entrant2IndividualAchievements();

    private ISelectModel achievementModel;


    public boolean isAddForm() {
        return getAchievement().getId() == null;
    }

    public Entrant getEntrant() {
        return entrant;
    }

    public void setEntrant(Entrant entrant) {
        this.entrant = entrant;
    }

    public Entrant2IndividualAchievements getAchievement() {
        return achievement;
    }

    public void setAchievement(Entrant2IndividualAchievements achievement) {
        this.achievement = achievement;
    }

    public ISelectModel getAchievementModel() {
        return achievementModel;
    }

    public void setAchievementModel(ISelectModel achievementModel) {
        this.achievementModel = achievementModel;
    }
}
