package ru.tandemservice.uniecrmc.component.report.EntrantEduLevelDistribution.Add;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController<IDAO, Model> {
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = getModel(component);
        getDao().prepare(model);
    }

    public void onClickApply(IBusinessComponent component) {
        Model model = getModel(component);
        getDao().update(model);
        deactivate(component);
        activateInRoot(component, new PublisherActivator(model.getReport()));
    }
}

