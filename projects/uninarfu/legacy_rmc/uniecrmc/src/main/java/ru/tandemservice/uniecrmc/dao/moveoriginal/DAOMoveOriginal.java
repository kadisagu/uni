package ru.tandemservice.uniecrmc.dao.moveoriginal;

import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.uniecrmc.entity.entrant.ChangeOriginalDocumentRED;
import ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionExt;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uniec.dao.IEntrantDAO;
import ru.tandemservice.uniec.entity.catalog.codes.EntrantStateCodes;
import ru.tandemservice.uniec.entity.entrant.*;

import java.util.*;

public class DAOMoveOriginal
        extends UniBaseDao implements IDAOMoveOriginal
{

    public static String LOG = "";

    private static boolean ISRUN = false;

    protected static IDAOMoveOriginal INSTANCE;

    public static IDAOMoveOriginal instance() {
        if (INSTANCE == null)
            INSTANCE = (IDAOMoveOriginal) ApplicationRuntime.getBean(IDAOMoveOriginal.class.getName());
        return INSTANCE;
    }


    private Map<Long, Double> mapFinalMark = new HashMap<>();

    // в качестве ключа у нас competitionGroupId
    private Map<Long, List<WrapRequestedEnrollmentDirection>> mapWrap = new HashMap<>();

    // мап рассматриваемых заявлений (те заявы, которые оцениваем)
    private Map<Long, WrapEntrantRequest> mapRequest = new HashMap<>();

    // контрольные цифры приема (ключ - это competitionGroupId)
    private Map<Long, Integer> mapKSP = new HashMap<>();


    @Override
    public List<WrapEntrantRequest> doCalculate(EnrollmentCampaign campaign)
    {
        LOG = "";

        mapFinalMark = new HashMap<>();
        ;
        mapWrap = new HashMap<>();
        mapRequest = new HashMap<>();
        mapKSP = new HashMap<>();

        fillFinalMark(campaign);
        fillMapRequest(campaign);
        fillMapRating(campaign);

        // заполним контрольные цифры приема
        fillKSP(campaign);


        // получаем список заявлений, с максимальной
        // оценкой в обратном порядке
        Collection<WrapEntrantRequest> collRequest = mapRequest.values();
        List<WrapEntrantRequest> lstRequest = new ArrayList<>();
        lstRequest.addAll(collRequest);
        Collections.sort(lstRequest, new Comparator<WrapEntrantRequest>() {

            @Override
            public int compare(WrapEntrantRequest o1,
                               WrapEntrantRequest o2)
            {
                return Double.compare(o2.getFinalMark(), o1.getFinalMark());
            }
        });

        for (WrapEntrantRequest wrap : lstRequest) {
            System.out.println(wrap.getEntrantRequestId() + " " + wrap.getFinalMark());
            // установить в начальное значение признак подачи оригинала
            // ставим оригинал у минимального приоритета

            List<WrapRequestedEnrollmentDirection> lstRED = getWrapRequestedEnrollmentDirection(wrap.getEntrantRequestId());

            // признак оригинала ставим в 1-ую позицию
            // если нет финального статуса
            if (!hasState(lstRED, getEndEntrantState()))
                setOriginInFirst(lstRED);
        }

        // еще раз в обратном порядке по приоритету
        for (WrapEntrantRequest wrap : lstRequest) {
            List<WrapRequestedEnrollmentDirection> lstRED = getWrapRequestedEnrollmentDirection(wrap.getEntrantRequestId());
            // если в заявлении есть НП с финальными статусами, то такое обрабатывать не надо
            if (hasState(lstRED, getEndEntrantState()))
                continue;

            boolean originHasSet = false;

            for (WrapRequestedEnrollmentDirection red : lstRED) {
                if (originHasSet)
                    continue;

                Long competitionGroup = red.getCompetitionGroupId();
                // получаем КЦП
                if (!mapKSP.containsKey(competitionGroup))
                    continue;
                Integer ksp = mapKSP.get(competitionGroup);
                if (ksp <= 0)
                    continue;

                // смотрим на черту!!! (рисуем ее)
                // на верх черты пытаемся расположить зачисленных (типа занято уже)
                List<WrapRequestedEnrollmentDirection> lstRating = getRatingList(competitionGroup);

//				// для отладки
//				for (WrapRequestedEnrollmentDirection wr : lstRating)
//				{
//					System.out.println("     State=" + wr.getEntrantStateCode() + " CompetitionKindCode = " + wr.getCompetitionKindCode() + " rating " + wr.getFinalMark());
//				}

                int position = getRatingPosition(red, lstRating);

                if (position <= ksp) {
                    // все хорошо, оригинал ставим тут
                    // выходим из этого шага
                    setOrigin(red, lstRED);
                    originHasSet = true;

                    if (red.isOriginalEtStart() != red.isOriginal()) // изменения были
                        red.setOriginHasSet(true);

                    red.setComment("КЦП=" + ksp + " Приоритет=" + red.getPriority() + " позиция=" + position + " балл=" + red.getFinalMark() + " Оригинал ТУТ");
                }
                else {
                    // выпали за черту
                    // на сл шаге новая оценка
                    red.setComment("КЦП=" + ksp + " Приоритет=" + red.getPriority() + " позиция=" + position + " Выпали за черту");
                }
            }
        }


        return lstRequest;


    }

    private void fillMapRating(EnrollmentCampaign campaign) {

        // в рейтинге участвуют все заявления (Базжина 05/08/2014)
        // не оригинал тоже, но только бюджет
        DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(RequestedEnrollmentDirection.class, "redIn")
                .column(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().id().fromAlias("redIn")))
                .predicate(DQLPredicateType.distinct)

                        // по приемке
                .where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().entrant().enrollmentCampaign().fromAlias("redIn")),
                                         DQLExpressions.value(campaign)))

                        // правильные статусы (фильтр ниже)
                        //.where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirection.state().code().fromAlias("redIn")), getValidEntrantState()))

                        // по возмещению затрат - только бюджет
                .where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.compensationType().code().fromAlias("redIn")),
                                         DQLExpressions.value(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET)));


        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(RequestedEnrollmentDirection.class, "red")
                        // только правильные заявления (см выше)
                .where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().id().fromAlias("red")), dqlIn.getQuery()))


                        // двигаем только бюджет
                        // по возмещению затрат - только бюджет
                .where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.compensationType().code().fromAlias("red")),
                                         DQLExpressions.value(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET)))

                        // 	двигаем только между НП с конечными допустимыми статусами
                .where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirection.state().code().fromAlias("red")), getValidEntrantState()))

		
		
				/* id */.column(DQLExpressions.property(RequestedEnrollmentDirection.id().fromAlias("red")))
				/* entrantRequestId */.column(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().id().fromAlias("red")))
				/* enrollmentDirectionId */.column(DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().id().fromAlias("red")))
				/* compensationTypeCode */.column(DQLExpressions.property(RequestedEnrollmentDirection.compensationType().code().fromAlias("red")))
				/* competitionKindCode */.column(DQLExpressions.property(RequestedEnrollmentDirection.competitionKind().code().fromAlias("red")))
				/* entrantStateCode */.column(DQLExpressions.property(RequestedEnrollmentDirection.state().code().fromAlias("red")))
				/* priority */.column(DQLExpressions.property(RequestedEnrollmentDirection.priority().fromAlias("red")))
				/* targetAdmission */.column(DQLExpressions.property(RequestedEnrollmentDirection.targetAdmission().fromAlias("red")))
				/* original */.column(DQLExpressions.property(RequestedEnrollmentDirection.originalDocumentHandedIn().fromAlias("red")))
				/* приоритет статуса аюитуриента*/.column(DQLExpressions.property(RequestedEnrollmentDirection.state().priority().fromAlias("red")))
				/*конкурсная группа*/.column(DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().competitionGroup().id().fromAlias("red")));

        List<Object[]> lst = dql.createStatement(getSession()).list();

        for (Object[] objs : lst) {
            Long requestedEnrollmentDirectionId = (Long) objs[0];
            Long entrantRequestId = (Long) objs[1];
            Long enrollmentDirectionId = (Long) objs[2];

            String compensationTypeCode = (String) objs[3];
            String competitionKindCode = (String) objs[4];
            String entrantStateCode = (String) objs[5];

            Integer priority = (Integer) objs[6];
            Boolean targetAdmission = (Boolean) objs[7];
            Boolean original = (Boolean) objs[8];

            Double finalMark = 0.0;

            if (mapFinalMark.containsKey(requestedEnrollmentDirectionId))
                finalMark = mapFinalMark.get(requestedEnrollmentDirectionId);

            // без вступительных испытаний
            if (competitionKindCode.equals("1"))
                finalMark = 100000.00;

            Integer statePriority = (Integer) objs[9];

            Long competitionGroupId = requestedEnrollmentDirectionId;
            if (objs[10] != null)
                competitionGroupId = (Long) objs[10];


            WrapRequestedEnrollmentDirection wrap = new WrapRequestedEnrollmentDirection
                    (
                            requestedEnrollmentDirectionId
                            , entrantRequestId
                            , enrollmentDirectionId
                            , compensationTypeCode
                            , competitionKindCode
                            , entrantStateCode
                            , priority
                            , targetAdmission
                            , original
                            , finalMark
                            , statePriority
                            , competitionGroupId

                    );

            List<WrapRequestedEnrollmentDirection> lstMap = new ArrayList<>();
            if (mapWrap.containsKey(competitionGroupId))
                lstMap = mapWrap.get(competitionGroupId);
            else
                mapWrap.put(competitionGroupId, lstMap);

            lstMap.add(wrap);
        }

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Long moveOriginalForRequest(List<WrapRequestedEnrollmentDirection> lstRED)
    {
        // сначала false
        List<WrapRequestedEnrollmentDirection> lstSort = new ArrayList<>();
        lstSort.addAll(lstRED);

        // сначала без оригиналов
        Collections.sort(lstSort, new Comparator<WrapRequestedEnrollmentDirection>() {
            @Override
            public int compare(WrapRequestedEnrollmentDirection o1,
                               WrapRequestedEnrollmentDirection o2)
            {
                return Boolean.compare(o1.isOriginal(), o2.isOriginal());
            }
        });


        RequestedEnrollmentDirection oldOrigin = null;
        RequestedEnrollmentDirection newOrigin = null;
        String comment = "";

        Long entrantRequestId = null;
        EntrantRequest request = null;

        FlushMode defaultFlushMode = getSession().getFlushMode();

        getSession().setFlushMode(FlushMode.MANUAL);

        try {

            for (WrapRequestedEnrollmentDirection wrap : lstSort) {
                entrantRequestId = wrap.getEntrantRequestId();

                // начальное и конечное значени отличны
                if (wrap.isOriginalEtStart() != wrap.isOriginal()) {
                    RequestedEnrollmentDirection red = get(RequestedEnrollmentDirection.class, wrap.getId());
                    if (red.isOriginalDocumentHandedIn() && !wrap.isOriginal())
                        oldOrigin = red;

                    if (!red.isOriginalDocumentHandedIn() && wrap.isOriginal())
                        newOrigin = red;

                    red.setOriginalDocumentHandedIn(wrap.isOriginal());

                    // с записями коментов жесть какая-то


                    String commentRD = "{Оригинал перенесен автоматически}";
                    String eComment = red.getComment();
                    if (eComment == null)
                        eComment = "";
                    else
                        eComment = eComment.trim();

                    if (wrap.isOriginal()) {
                        if (!eComment.contains(commentRD))
                            eComment += " " + commentRD;

                        eComment = eComment.trim();
                        red.setComment(eComment);

                        // ставим признак оригинала
                        updateOriginalDocuments(red, true);
                    }
                    else {
                        // если в примечании было выставление оригинала - убрать

                        if (eComment.contains(commentRD))
                            eComment = eComment.replace(commentRD, "");

                        eComment = eComment.trim();
                        if (eComment.isEmpty())
                            eComment = null;
                        red.setComment(eComment);

                    }
                    update(red);
                    //getSession().flush();
                }

                if (wrap.getComment() != null && !wrap.getComment().isEmpty())
                    comment += " " + wrap.getComment();

            }


            request = get(EntrantRequest.class, entrantRequestId);


            ChangeOriginalDocumentRED log = new ChangeOriginalDocumentRED();
            log.setOldOrigin(oldOrigin); //oldOrigin может быть null, например оригиналы на направлении - забрал документы
            log.setNewOrigin(newOrigin);

            log.setComment(comment.trim());
            log.setEntrantRequest(request);
            log.setDateChange(new Date());

            save(log);
            getSession().flush();

        }
        finally {
            getSession().setFlushMode(defaultFlushMode);
        }

        return request.getEntrant().getId();

    }

    public void updateOriginalDocuments(RequestedEnrollmentDirection direction, boolean save)
    {
        Session session = getSession();
        //EntrantUtil.lockEntrant(session, direction.getEntrantRequest().getEntrant());
        EntrantOriginalDocumentRelation relation = (EntrantOriginalDocumentRelation) get(EntrantOriginalDocumentRelation.class, EntrantOriginalDocumentRelation.entrant(), direction.getEntrantRequest().getEntrant());
        if (save) {
            if (relation == null) {
                relation = new EntrantOriginalDocumentRelation();
                relation.setEntrant(direction.getEntrantRequest().getEntrant());
            }

            relation.setRequestedEnrollmentDirection(direction);

            if (relation.getId() == null)
                session.save(relation);
            else {
                session.update(relation);
            }
        }
        else if ((relation != null) &&
                (direction.equals(relation.getRequestedEnrollmentDirection())))
        {
            session.delete(relation);
        }
    }


    private void setOrigin(WrapRequestedEnrollmentDirection red,
                           List<WrapRequestedEnrollmentDirection> lstRED)
    {
        for (WrapRequestedEnrollmentDirection wrap : lstRED) {
            if (wrap.getId().equals(red.getId()))
                wrap.setOriginal(true);
            else
                wrap.setOriginal(false);
        }
    }

    private int getRatingPosition(WrapRequestedEnrollmentDirection red,
                                  List<WrapRequestedEnrollmentDirection> lstRating)
    {


        int pos = 0;
        // баллы нарастают
        for (WrapRequestedEnrollmentDirection wrap : lstRating) {
            pos++;
            // если быллы анализируемого направления больше баллов текущей
            // позиции рейтинга - считаем что мы в рейтинге
            if (red.getEntrantRequestId().equals(wrap.getEntrantRequestId()))
                return pos;
        }
        return pos;
    }

    // Рейтинг строим по конкурсным группам
    private List<WrapRequestedEnrollmentDirection> getRatingList(
            Long competitionGroupId)
    {
        List<WrapRequestedEnrollmentDirection> lst = mapWrap.get(competitionGroupId);
        Collections.sort(lst, new Comparator<WrapRequestedEnrollmentDirection>() {

            @Override
            public int compare(
                    WrapRequestedEnrollmentDirection o1,
                    WrapRequestedEnrollmentDirection o2)
            {
                // сначала приоритет по статусу
                int stateCompare = Integer.compare(o1.getEntrantStatePriority(), o2.getEntrantStatePriority());
                if (stateCompare != 0)
                    return stateCompare;
                // по оценке
                int markCompare = Double.compare(o2.getFinalMark(), o1.getFinalMark());
                //int markCompare = Double.compare(o1.getFinalMark(), o2.getFinalMark());
                return markCompare;
            }
        });

        // план приема отстроен на ГК
        // нужно оставить уникальное по заявлению с максимальным баллом
        // в конкурсной группе
        List<WrapRequestedEnrollmentDirection> retVal = new ArrayList<>();

        for (WrapRequestedEnrollmentDirection wr : lst) {

            WrapRequestedEnrollmentDirection find = null;
            for (WrapRequestedEnrollmentDirection wrd : retVal) {
                if (wrd.getEntrantRequestId().equals(wr.getEntrantRequestId())
                        && wrd.getCompetitionGroupId().equals(wr.getCompetitionGroupId())
                        )
                    find = wrd;
            }

            if (find != null) {
                // уже есть
                if (wr.getFinalMark() > find.getFinalMark()) {
                    retVal.remove(find);
                    retVal.add(wr);
                }
            }
            else
                // нет
                retVal.add(wr);
        }
        return retVal;
    }

    @Override
    public boolean hasState(List<WrapRequestedEnrollmentDirection> lstRED,
                            List<String> endEntrantState)
    {

        for (String state : endEntrantState) {
            for (WrapRequestedEnrollmentDirection wd : lstRED) {
                if (wd.getEntrantStateCode().equals(state))
                    return true;
            }
        }
        return false;
    }

    private void fillKSP(EnrollmentCampaign campaign)
    {

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EnrollmentDirection.class, "ed")
                .column(DQLExpressions.property(EnrollmentDirection.id().fromAlias("ed")))
                .column(DQLExpressions.property(EnrollmentDirection.competitionGroup().id().fromAlias("ed")))
                .column(DQLExpressions.property(EnrollmentDirection.ministerialPlan().fromAlias("ed")))
                        // по приемке
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.enrollmentCampaign().fromAlias("ed")),
                                         DQLExpressions.value(campaign)));

        List<Object[]> lst = dql.createStatement(getSession()).list();

        for (Object[] objs : lst) {
            Long enrollmentDirectionId = (Long) objs[0];
            Long competitionGroupId = null;
            if (objs[1] != null)
                competitionGroupId = (Long) objs[1];

            if (competitionGroupId == null)
                competitionGroupId = enrollmentDirectionId;

            Integer count = (Integer) objs[2];
            if (count == null)
                count = 0;

            if (mapKSP.containsKey(competitionGroupId)) {
                Integer ec = mapKSP.get(competitionGroupId);
                if (ec == null)
                    ec = 0;

                ec += count;
                mapKSP.put(competitionGroupId, ec);
            }
            else
                mapKSP.put(competitionGroupId, count);

        }


        dql = new DQLSelectBuilder()
                .fromEntity(EnrollmentDirectionExt.class, "ed")
                .column(DQLExpressions.property(EnrollmentDirectionExt.enrollmentDirection().id().fromAlias("ed")))
                .column(DQLExpressions.property(EnrollmentDirectionExt.enrollmentDirection().competitionGroup().id().fromAlias("ed")))
                .column(DQLExpressions.property(EnrollmentDirectionExt.advansedPlanBudget().fromAlias("ed")))
                        // по приемке
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirectionExt.enrollmentDirection().enrollmentCampaign().fromAlias("ed")),
                                         DQLExpressions.value(campaign)))
        ;

        lst = dql.createStatement(getSession()).list();
        for (Object[] objs : lst) {
            Long enrollmentDirectionId = (Long) objs[0];
            Long competitionGroupId = null;
            if (objs[1] != null)
                competitionGroupId = (Long) objs[1];
            if (competitionGroupId == null)
                competitionGroupId = enrollmentDirectionId;

            Integer count = (Integer) objs[2];
            if (count == null)
                count = 0;

            if (mapKSP.containsKey(competitionGroupId)) {
                Integer ec = mapKSP.get(competitionGroupId);
                if (ec == null)
                    ec = 0;

                ec += count;
                mapKSP.put(competitionGroupId, ec);
            }
            else
                mapKSP.put(competitionGroupId, count);
        }

    }

    // установить признак оригинала в начало
    private void setOriginInFirst(List<WrapRequestedEnrollmentDirection> lstRED)
    {
        int pos = 0;
        for (WrapRequestedEnrollmentDirection wr : lstRED) {
            pos++;
            if (pos == 1)
                wr.setOriginal(true);
            else
                wr.setOriginal(false);
        }
    }

    @Override
    public List<WrapRequestedEnrollmentDirection> getWrapRequestedEnrollmentDirection(
            Long entrantRequestId)
    {

        List<WrapRequestedEnrollmentDirection> retVal = new ArrayList<>();
        Set<Long> set = mapWrap.keySet();

        for (Long key : set) {
            List<WrapRequestedEnrollmentDirection> lstSub = mapWrap.get(key);
            for (WrapRequestedEnrollmentDirection wrap : lstSub) {
                if (wrap.getEntrantRequestId().equals(entrantRequestId))
                    retVal.add(wrap);
            }
        }

        // сортируем в порядке приоритета
        Collections.sort(retVal, new Comparator<WrapRequestedEnrollmentDirection>() {

            @Override
            public int compare(WrapRequestedEnrollmentDirection o1,
                               WrapRequestedEnrollmentDirection o2)
            {

                return Integer.compare(o1.getPriority(), o2.getPriority());
            }
        });
        return retVal;
    }

    private void fillMapRequest(EnrollmentCampaign campaign)
    {
        // оцениваем только заявления с оригиналами докуметов
        DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(RequestedEnrollmentDirection.class, "redIn")
                .column(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().id().fromAlias("redIn")))
                .predicate(DQLPredicateType.distinct)

                        // по приемке
                .where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().entrant().enrollmentCampaign().fromAlias("redIn")),
                                         DQLExpressions.value(campaign)))

                        // правильные статусы
                        // могли сместить на забрал документы
                        // скитаем, что есть такие заявы
                        //.where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirection.state().code().fromAlias("redIn")), getValidEntrantState()))

                        // по возмещению затрат - только бюджет
                .where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.compensationType().code().fromAlias("redIn")),
                                         DQLExpressions.value(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET)))

                        // только оригиналы
                .where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.originalDocumentHandedIn().fromAlias("redIn")),
                                         DQLExpressions.value(Boolean.TRUE)));


        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(RequestedEnrollmentDirection.class, "red")
                        // только правильные заявления (см выше)
                .where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().id().fromAlias("red")), dqlIn.getQuery()))

                        // двигаем только бюджет
                        // по возмещению затрат - только бюджет
                .where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.compensationType().code().fromAlias("red")),
                                         DQLExpressions.value(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET)))

                        // двигаем только между НП с конечными допустимыми статусами
                .where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirection.state().code().fromAlias("red")), getValidEntrantState()))
		
		
				
		/* id */.column(DQLExpressions.property(RequestedEnrollmentDirection.id().fromAlias("red")))
		/* entrantRequestId */.column(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().id().fromAlias("red")))
		/* enrollmentDirectionId */.column(DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().id().fromAlias("red")))
		/* compensationTypeCode */.column(DQLExpressions.property(RequestedEnrollmentDirection.compensationType().code().fromAlias("red")))
		/* competitionKindCode */.column(DQLExpressions.property(RequestedEnrollmentDirection.competitionKind().code().fromAlias("red")))
		/* entrantStateCode */.column(DQLExpressions.property(RequestedEnrollmentDirection.state().code().fromAlias("red")))
		/* priority */.column(DQLExpressions.property(RequestedEnrollmentDirection.priority().fromAlias("red")))
		/* targetAdmission */.column(DQLExpressions.property(RequestedEnrollmentDirection.targetAdmission().fromAlias("red")))
		/* original */.column(DQLExpressions.property(RequestedEnrollmentDirection.originalDocumentHandedIn().fromAlias("red")))
		/* приоритет статуса аюитуриента*/.column(DQLExpressions.property(RequestedEnrollmentDirection.state().priority().fromAlias("red")))
		/*конкурсная группа*/.column(DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().competitionGroup().id().fromAlias("red")));

        List<Object[]> lst = dql.createStatement(getSession()).list();

        for (Object[] objs : lst) {
            Long requestedEnrollmentDirectionId = (Long) objs[0];
            Long entrantRequestId = (Long) objs[1];
            String competitionKindCode = (String) objs[4];
            Double finalMark = 0.0;

            if (mapFinalMark.containsKey(requestedEnrollmentDirectionId))
                finalMark = mapFinalMark.get(requestedEnrollmentDirectionId);

            // без вступительных испытаний
            if (competitionKindCode.equals("1"))
                finalMark = 100000.00;

            WrapEntrantRequest wrapRequest = new WrapEntrantRequest(entrantRequestId, finalMark);
            if (mapRequest.containsKey(entrantRequestId))
                wrapRequest = mapRequest.get(entrantRequestId);
            else
                mapRequest.put(entrantRequestId, wrapRequest);

            // установим максимальную оченку
            if (wrapRequest.getFinalMark() < finalMark)
                wrapRequest.setFinalMark(finalMark);

        }
    }


    private void fillFinalMark(EnrollmentCampaign campaign)
    {


        Map<Long, Double> avgMark = new HashMap<>();


        DQLSelectBuilder builderPid = new DQLSelectBuilder()
                .fromEntity(PersonEduInstitution.class, "e")

                .column(DQLExpressions.property(PersonEduInstitution.person().id().fromAlias("e")))
                .column(DQLExpressions.property(PersonEduInstitution.mark3().fromAlias("e")))
                .column(DQLExpressions.property(PersonEduInstitution.mark4().fromAlias("e")))
                .column(DQLExpressions.property(PersonEduInstitution.mark5().fromAlias("e")));

        List<Object[]> listAvg = builderPid.createStatement(getSession()).list();
        for (Object[] dataRow : listAvg) {

            Long personId = (Long) dataRow[0];
            Integer mark3 = dataRow[1] == null ? 0 : (Integer) dataRow[1];
            Integer mark4 = dataRow[2] == null ? 0 : (Integer) dataRow[2];
            Integer mark5 = dataRow[3] == null ? 0 : (Integer) dataRow[3];

            int sum = mark3 + mark4 + mark5;
            double total = (mark3 * 3 + mark4 * 4 + mark5 * 5) / 10;
            Double avg = (sum != 0) ? total / sum : 0;

            avgMark.put(personId, avg);
        }


        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(ChosenEntranceDiscipline.class, "e")

                .column(DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("e")))
                .column(DQLFunctions.sum(DQLExpressions.property(ChosenEntranceDiscipline.finalMark().fromAlias("e"))))

                        // столбец персоны
                .column(DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().entrantRequest().entrant().person().id().fromAlias("e")))

                .group(DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("e")))
                .group(DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().entrantRequest().entrant().person().id().fromAlias("e")))


                        // по приемке
                .where(DQLExpressions.eq(DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().entrantRequest().entrant().enrollmentCampaign().fromAlias("e")),
                                         DQLExpressions.value(campaign)));

        List<Object[]> list = builder.createStatement(getSession()).list();

        for (Object[] dataRow : list) {
            Long requestedEnrollmentDirectionId = (Long) dataRow[0];
            Double finalMark = (Double) dataRow[1];
            if (finalMark == null)
                finalMark = 0D;

            Long personId = (Long) dataRow[2];

            if (avgMark.containsKey(personId)) {
                Double avg = avgMark.get(personId);
                finalMark += avg;
            }
            mapFinalMark.put(requestedEnrollmentDirectionId, finalMark);
        }

    }


    public static List<String> getValidEntrantState()
    {
        // только правильные статусы
        List<String> validCode = new ArrayList<>();
        validCode.add(EntrantStateCodes.TO_BE_ENROLED);
        validCode.add(EntrantStateCodes.PRELIMENARY_ENROLLED);
        validCode.add(EntrantStateCodes.IN_ORDER);
        validCode.add(EntrantStateCodes.ENROLED);


        return validCode;

    }

    public static List<String> getEndEntrantState()
    {
        // только правильные статусы
        List<String> validCode = new ArrayList<>();
        validCode.add(EntrantStateCodes.PRELIMENARY_ENROLLED);
        validCode.add(EntrantStateCodes.IN_ORDER);
        validCode.add(EntrantStateCodes.ENROLED);


        return validCode;

    }


    @Override
    @Transactional
    public void recalculatePart(List<Long> part)
    {
        IEntrantDAO idao = (IEntrantDAO) ApplicationRuntime.getBean(IEntrantDAO.class.getName());
        idao.updateEntrantData(part);
    }

}
