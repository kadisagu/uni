package ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtension;
import org.tandemframework.caf.ui.config.datasource.IColumnListExtensionBuilder;
import ru.tandemservice.uniec.base.bo.EcDistribution.ui.List.EcDistributionList;

@Configuration
public class EcDistributionListExt extends BusinessComponentExtensionManager {

    @Autowired
    private EcDistributionList parent;

    @Bean
    public ColumnListExtension extensionCG()
    {
        IColumnListExtensionBuilder c = columnListExtensionBuilder(parent.distributionCGDS());
        c.addAllAfter("taQuota");
        c.addColumn(blockColumn("srQuota", "srQuotaBlockColumn").visible("ui:usePriority").width("10").create());
        return c.create();
    }

    @Bean
    public ColumnListExtension extensionED()
    {
        IColumnListExtensionBuilder c = columnListExtensionBuilder(parent.distributionEDDS());
        c.addAllAfter("taQuota");
        c.addColumn(blockColumn("srQuota", "srQuotaBlockColumn").visible("ui:usePriority").width("10").create());
        return c.create();
    }
}
