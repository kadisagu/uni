package ru.tandemservice.uniecrmc.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionRestrictionRMC;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Ограничения по формам освоения, уровням образования и видам возмещения затрат для направлений приема/конкурсных групп
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrollmentDirectionRestrictionRMCGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionRestrictionRMC";
    public static final String ENTITY_NAME = "enrollmentDirectionRestrictionRMC";
    public static final int VERSION_HASH = 1467424093;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_DEVELOP_FORM = "developForm";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String L_STRUCTURE_EDUCATION_LEVELS = "structureEducationLevels";
    public static final String P_MAX_ENROLLMENT_DIRECTION = "maxEnrollmentDirection";
    public static final String P_MAX_MINISTERIAL_DIRECTION = "maxMinisterialDirection";
    public static final String P_MAX_FORMATIVE_ORG_UNIT = "maxFormativeOrgUnit";
    public static final String P_MAX_COMPETITION_GROUP = "maxCompetitionGroup";

    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private DevelopForm _developForm;     // Форма освоения
    private CompensationType _compensationType;     // Вид возмещения затрат
    private StructureEducationLevels _structureEducationLevels;     // Структура направлений подготовки (специальностей) Минобрнауки РФ
    private Integer _maxEnrollmentDirection;     // Максимальное число выбираемых направлений подготовки для приема
    private Integer _maxMinisterialDirection;     // Максимальное число выбираемых направлений подготовки по классификатору
    private Integer _maxFormativeOrgUnit;     // Максимальное число выбираемых формирующих подразделений
    private Integer _maxCompetitionGroup;     // Максимальное число выбираемых конкурсных групп

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения. Свойство не может быть null.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Структура направлений подготовки (специальностей) Минобрнауки РФ. Свойство не может быть null.
     */
    @NotNull
    public StructureEducationLevels getStructureEducationLevels()
    {
        return _structureEducationLevels;
    }

    /**
     * @param structureEducationLevels Структура направлений подготовки (специальностей) Минобрнауки РФ. Свойство не может быть null.
     */
    public void setStructureEducationLevels(StructureEducationLevels structureEducationLevels)
    {
        dirty(_structureEducationLevels, structureEducationLevels);
        _structureEducationLevels = structureEducationLevels;
    }

    /**
     * @return Максимальное число выбираемых направлений подготовки для приема.
     */
    public Integer getMaxEnrollmentDirection()
    {
        return _maxEnrollmentDirection;
    }

    /**
     * @param maxEnrollmentDirection Максимальное число выбираемых направлений подготовки для приема.
     */
    public void setMaxEnrollmentDirection(Integer maxEnrollmentDirection)
    {
        dirty(_maxEnrollmentDirection, maxEnrollmentDirection);
        _maxEnrollmentDirection = maxEnrollmentDirection;
    }

    /**
     * @return Максимальное число выбираемых направлений подготовки по классификатору.
     */
    public Integer getMaxMinisterialDirection()
    {
        return _maxMinisterialDirection;
    }

    /**
     * @param maxMinisterialDirection Максимальное число выбираемых направлений подготовки по классификатору.
     */
    public void setMaxMinisterialDirection(Integer maxMinisterialDirection)
    {
        dirty(_maxMinisterialDirection, maxMinisterialDirection);
        _maxMinisterialDirection = maxMinisterialDirection;
    }

    /**
     * @return Максимальное число выбираемых формирующих подразделений.
     */
    public Integer getMaxFormativeOrgUnit()
    {
        return _maxFormativeOrgUnit;
    }

    /**
     * @param maxFormativeOrgUnit Максимальное число выбираемых формирующих подразделений.
     */
    public void setMaxFormativeOrgUnit(Integer maxFormativeOrgUnit)
    {
        dirty(_maxFormativeOrgUnit, maxFormativeOrgUnit);
        _maxFormativeOrgUnit = maxFormativeOrgUnit;
    }

    /**
     * @return Максимальное число выбираемых конкурсных групп.
     */
    public Integer getMaxCompetitionGroup()
    {
        return _maxCompetitionGroup;
    }

    /**
     * @param maxCompetitionGroup Максимальное число выбираемых конкурсных групп.
     */
    public void setMaxCompetitionGroup(Integer maxCompetitionGroup)
    {
        dirty(_maxCompetitionGroup, maxCompetitionGroup);
        _maxCompetitionGroup = maxCompetitionGroup;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrollmentDirectionRestrictionRMCGen)
        {
            setEnrollmentCampaign(((EnrollmentDirectionRestrictionRMC)another).getEnrollmentCampaign());
            setDevelopForm(((EnrollmentDirectionRestrictionRMC)another).getDevelopForm());
            setCompensationType(((EnrollmentDirectionRestrictionRMC)another).getCompensationType());
            setStructureEducationLevels(((EnrollmentDirectionRestrictionRMC)another).getStructureEducationLevels());
            setMaxEnrollmentDirection(((EnrollmentDirectionRestrictionRMC)another).getMaxEnrollmentDirection());
            setMaxMinisterialDirection(((EnrollmentDirectionRestrictionRMC)another).getMaxMinisterialDirection());
            setMaxFormativeOrgUnit(((EnrollmentDirectionRestrictionRMC)another).getMaxFormativeOrgUnit());
            setMaxCompetitionGroup(((EnrollmentDirectionRestrictionRMC)another).getMaxCompetitionGroup());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrollmentDirectionRestrictionRMCGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrollmentDirectionRestrictionRMC.class;
        }

        public T newInstance()
        {
            return (T) new EnrollmentDirectionRestrictionRMC();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "developForm":
                    return obj.getDevelopForm();
                case "compensationType":
                    return obj.getCompensationType();
                case "structureEducationLevels":
                    return obj.getStructureEducationLevels();
                case "maxEnrollmentDirection":
                    return obj.getMaxEnrollmentDirection();
                case "maxMinisterialDirection":
                    return obj.getMaxMinisterialDirection();
                case "maxFormativeOrgUnit":
                    return obj.getMaxFormativeOrgUnit();
                case "maxCompetitionGroup":
                    return obj.getMaxCompetitionGroup();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "structureEducationLevels":
                    obj.setStructureEducationLevels((StructureEducationLevels) value);
                    return;
                case "maxEnrollmentDirection":
                    obj.setMaxEnrollmentDirection((Integer) value);
                    return;
                case "maxMinisterialDirection":
                    obj.setMaxMinisterialDirection((Integer) value);
                    return;
                case "maxFormativeOrgUnit":
                    obj.setMaxFormativeOrgUnit((Integer) value);
                    return;
                case "maxCompetitionGroup":
                    obj.setMaxCompetitionGroup((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "developForm":
                        return true;
                case "compensationType":
                        return true;
                case "structureEducationLevels":
                        return true;
                case "maxEnrollmentDirection":
                        return true;
                case "maxMinisterialDirection":
                        return true;
                case "maxFormativeOrgUnit":
                        return true;
                case "maxCompetitionGroup":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "developForm":
                    return true;
                case "compensationType":
                    return true;
                case "structureEducationLevels":
                    return true;
                case "maxEnrollmentDirection":
                    return true;
                case "maxMinisterialDirection":
                    return true;
                case "maxFormativeOrgUnit":
                    return true;
                case "maxCompetitionGroup":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "developForm":
                    return DevelopForm.class;
                case "compensationType":
                    return CompensationType.class;
                case "structureEducationLevels":
                    return StructureEducationLevels.class;
                case "maxEnrollmentDirection":
                    return Integer.class;
                case "maxMinisterialDirection":
                    return Integer.class;
                case "maxFormativeOrgUnit":
                    return Integer.class;
                case "maxCompetitionGroup":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrollmentDirectionRestrictionRMC> _dslPath = new Path<EnrollmentDirectionRestrictionRMC>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrollmentDirectionRestrictionRMC");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionRestrictionRMC#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionRestrictionRMC#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionRestrictionRMC#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Структура направлений подготовки (специальностей) Минобрнауки РФ. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionRestrictionRMC#getStructureEducationLevels()
     */
    public static StructureEducationLevels.Path<StructureEducationLevels> structureEducationLevels()
    {
        return _dslPath.structureEducationLevels();
    }

    /**
     * @return Максимальное число выбираемых направлений подготовки для приема.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionRestrictionRMC#getMaxEnrollmentDirection()
     */
    public static PropertyPath<Integer> maxEnrollmentDirection()
    {
        return _dslPath.maxEnrollmentDirection();
    }

    /**
     * @return Максимальное число выбираемых направлений подготовки по классификатору.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionRestrictionRMC#getMaxMinisterialDirection()
     */
    public static PropertyPath<Integer> maxMinisterialDirection()
    {
        return _dslPath.maxMinisterialDirection();
    }

    /**
     * @return Максимальное число выбираемых формирующих подразделений.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionRestrictionRMC#getMaxFormativeOrgUnit()
     */
    public static PropertyPath<Integer> maxFormativeOrgUnit()
    {
        return _dslPath.maxFormativeOrgUnit();
    }

    /**
     * @return Максимальное число выбираемых конкурсных групп.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionRestrictionRMC#getMaxCompetitionGroup()
     */
    public static PropertyPath<Integer> maxCompetitionGroup()
    {
        return _dslPath.maxCompetitionGroup();
    }

    public static class Path<E extends EnrollmentDirectionRestrictionRMC> extends EntityPath<E>
    {
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private DevelopForm.Path<DevelopForm> _developForm;
        private CompensationType.Path<CompensationType> _compensationType;
        private StructureEducationLevels.Path<StructureEducationLevels> _structureEducationLevels;
        private PropertyPath<Integer> _maxEnrollmentDirection;
        private PropertyPath<Integer> _maxMinisterialDirection;
        private PropertyPath<Integer> _maxFormativeOrgUnit;
        private PropertyPath<Integer> _maxCompetitionGroup;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionRestrictionRMC#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionRestrictionRMC#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionRestrictionRMC#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Структура направлений подготовки (специальностей) Минобрнауки РФ. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionRestrictionRMC#getStructureEducationLevels()
     */
        public StructureEducationLevels.Path<StructureEducationLevels> structureEducationLevels()
        {
            if(_structureEducationLevels == null )
                _structureEducationLevels = new StructureEducationLevels.Path<StructureEducationLevels>(L_STRUCTURE_EDUCATION_LEVELS, this);
            return _structureEducationLevels;
        }

    /**
     * @return Максимальное число выбираемых направлений подготовки для приема.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionRestrictionRMC#getMaxEnrollmentDirection()
     */
        public PropertyPath<Integer> maxEnrollmentDirection()
        {
            if(_maxEnrollmentDirection == null )
                _maxEnrollmentDirection = new PropertyPath<Integer>(EnrollmentDirectionRestrictionRMCGen.P_MAX_ENROLLMENT_DIRECTION, this);
            return _maxEnrollmentDirection;
        }

    /**
     * @return Максимальное число выбираемых направлений подготовки по классификатору.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionRestrictionRMC#getMaxMinisterialDirection()
     */
        public PropertyPath<Integer> maxMinisterialDirection()
        {
            if(_maxMinisterialDirection == null )
                _maxMinisterialDirection = new PropertyPath<Integer>(EnrollmentDirectionRestrictionRMCGen.P_MAX_MINISTERIAL_DIRECTION, this);
            return _maxMinisterialDirection;
        }

    /**
     * @return Максимальное число выбираемых формирующих подразделений.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionRestrictionRMC#getMaxFormativeOrgUnit()
     */
        public PropertyPath<Integer> maxFormativeOrgUnit()
        {
            if(_maxFormativeOrgUnit == null )
                _maxFormativeOrgUnit = new PropertyPath<Integer>(EnrollmentDirectionRestrictionRMCGen.P_MAX_FORMATIVE_ORG_UNIT, this);
            return _maxFormativeOrgUnit;
        }

    /**
     * @return Максимальное число выбираемых конкурсных групп.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionRestrictionRMC#getMaxCompetitionGroup()
     */
        public PropertyPath<Integer> maxCompetitionGroup()
        {
            if(_maxCompetitionGroup == null )
                _maxCompetitionGroup = new PropertyPath<Integer>(EnrollmentDirectionRestrictionRMCGen.P_MAX_COMPETITION_GROUP, this);
            return _maxCompetitionGroup;
        }

        public Class getEntityClass()
        {
            return EnrollmentDirectionRestrictionRMC.class;
        }

        public String getEntityName()
        {
            return "enrollmentDirectionRestrictionRMC";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
