package ru.tandemservice.uniecrmc.dao;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uniecrmc.entity.entrant.EntranceDisciplineExt;
import ru.tandemservice.uniecrmc.entity.entrant.EntrantRecommendedSettings;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.codes.EntrantStateCodes;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.EnrollmentCompetitionKind;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class UniecrmcDao extends UniBaseDao implements IUniecrmcDao {

    private static IUniecrmcDao uniecrmcDao = null;

    public static IUniecrmcDao Instanse() {
        if (uniecrmcDao == null)
            uniecrmcDao = (IUniecrmcDao) ApplicationRuntime.getBean("uniecrmcDao");
        return uniecrmcDao;
    }


    @Override
    public RequestedEnrollmentDirection getFirstRequestDirection(EntrantRequest request) {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "rd")
                .where(eq(property(RequestedEnrollmentDirection.entrantRequest().id().fromAlias("rd")), value(request.getId())))
                .column("rd")
                .order(property(RequestedEnrollmentDirection.priority().fromAlias("rd")));

        List<RequestedEnrollmentDirection> lst = dql.createStatement(getSession()).list();

        return !lst.isEmpty() ? lst.get(0) : null;
    }

    @Override
    public void fillPriorityED(EnrollmentCampaign campaign) {

        DQLSelectBuilder extBuilder = new DQLSelectBuilder().fromEntity(EntranceDisciplineExt.class, "ext")
                .column(DQLExpressions.property(EntranceDisciplineExt.entranceDiscipline().id().fromAlias("ext")));

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EntranceDiscipline.class, "ed")
                .where(DQLExpressions.eqValue(DQLExpressions.property(EntranceDiscipline.enrollmentDirection().enrollmentCampaign().fromAlias("ed")), campaign))
                .where(DQLExpressions.notIn(DQLExpressions.property(EntranceDiscipline.id().fromAlias("ed")), extBuilder.buildQuery()))
                .order(DQLExpressions.property(EntranceDiscipline.enrollmentDirection().id().fromAlias("ed")))
                .order(DQLExpressions.property(EntranceDiscipline.discipline().id().fromAlias("ed")));

        List<EntranceDiscipline> disciplines = getList(builder);

        Long lastID = 0L;
        int priority = 0;
        for (EntranceDiscipline discipline : disciplines) {
            if (!discipline.getEnrollmentDirection().getId().equals(lastID))
                priority = 0;
            priority++;
            EntranceDisciplineExt disciplineExt = new EntranceDisciplineExt();
            disciplineExt.setEntranceDiscipline(discipline);
            disciplineExt.setPriority(priority);

            getSession().saveOrUpdate(disciplineExt);

            lastID = discipline.getEnrollmentDirection().getId();
        }

    }

    @Override
    public void fillPriorityED() {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EntrantRecommendedSettings.class, "s")
                .where(DQLExpressions.eqValue(DQLExpressions.property(EntrantRecommendedSettings.usePriorityEntranceDiscipline().fromAlias("s")), Boolean.TRUE))
                .column(DQLExpressions.property(EntrantRecommendedSettings.enrollmentCampaign().fromAlias("s")));

        List<EnrollmentCampaign> campaigns = getList(builder);

        for (EnrollmentCampaign enrollmentCampaign : campaigns) {
            fillPriorityED(enrollmentCampaign);
        }
    }

    public String changeCompetitionKindSpo() {
        final String WITHOUT_EXAMS_KIND_CODE = "1";
        final String ORDINAR_KIND_CODE = "5";

        StringBuilder log = new StringBuilder();
        Set<Long> enrollmentDirections = new HashSet<>();

        //выбираем заявления на СПО ФГОС:
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(RequestedEnrollmentDirection.class, "e")
                .column(RequestedEnrollmentDirection.id().fromAlias("e").s())//[0]
                .column(RequestedEnrollmentDirection.state().id().fromAlias("e").s())//[1]
                .column(RequestedEnrollmentDirection.competitionKind().id().fromAlias("e").s())//[2]
                .column(RequestedEnrollmentDirection.enrollmentDirection().id().fromAlias("e").s())//[3]

                .column(IdentityCard.lastName().fromAlias("ic").s())//[4]
                .column(IdentityCard.firstName().fromAlias("ic").s())//[5]
                .column(IdentityCard.middleName().fromAlias("ic").s())//[6]
                .column(EducationLevelsHighSchool.printTitle().fromAlias("elhs").s())//[7]
                .column(EducationLevelsHighSchool.id().fromAlias("elhs").s())//[8]
                .column(Entrant.id().fromAlias("entr").s())//[9]
                .column(RequestedEnrollmentDirection.competitionKind().title().fromAlias("e").s())//[10]

                .column(RequestedEnrollmentDirection.enrollmentDirection().enrollmentCampaign().id().fromAlias("e").s())//[11]

                .joinPath(DQLJoinType.inner, RequestedEnrollmentDirection.entrantRequest().entrant().fromAlias("e"), "entr")
                .joinPath(DQLJoinType.inner, Entrant.person().identityCard().fromAlias("entr"), "ic")
                .joinPath(DQLJoinType.inner, RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().fromAlias("e"), "elhs")
                .joinPath(DQLJoinType.inner, EducationLevelsHighSchool.educationLevel().levelType().fromAlias("elhs"), "lt")

                .where(DQLExpressions.or(
                        DQLExpressions.eq(DQLExpressions.property(StructureEducationLevels.middle().fromAlias("lt")), DQLExpressions.value(Boolean.TRUE)),
                        DQLExpressions.eq(DQLExpressions.property(StructureEducationLevels.gos3().fromAlias("lt")), DQLExpressions.value(Boolean.TRUE))
                ));
        List<Object[]> spoREDList = getList(builder);

        log.append("выбрано " + spoREDList.size() + " заявлений на обработку\r\n");
        if (spoREDList.size() > 0) {
            //выберем те напр. подготовки для приема, где нет вступительных испытаний и которые есть в выбранных заявлениях
            for (Object[] line : spoREDList) {
                enrollmentDirections.add((Long) line[3]);
            }

            DQLSelectBuilder enrollDirDql = new DQLSelectBuilder().fromEntity(EntranceDiscipline.class, "disc")
                    .column(EntranceDiscipline.enrollmentDirection().id().fromAlias("disc").s())
                    .where(DQLExpressions.in(DQLExpressions.property(EntranceDiscipline.enrollmentDirection().id().fromAlias("disc")), enrollmentDirections))
                    .group(EntranceDiscipline.enrollmentDirection().id().fromAlias("disc").s());
            List<Long> enrollDirWithDiscList = getList(enrollDirDql);
            Set<Long> enrollDirWithDisc = new HashSet<>(enrollDirWithDiscList);

            //возьмем нужные справочники
            EntrantState activeState = getCatalogItem(EntrantState.class, EntrantStateCodes.ACTIVE);
            EntrantState toBeEnroledState = getCatalogItem(EntrantState.class, EntrantStateCodes.TO_BE_ENROLED);

            CompetitionKind withoutExamsKind = getCatalogItem(CompetitionKind.class, WITHOUT_EXAMS_KIND_CODE);
            CompetitionKind ordinarKind = getCatalogItem(CompetitionKind.class, ORDINAR_KIND_CODE);

            List<Long> withoutExamsReqList = new ArrayList<>();//id заявлений, где нужно поставить withoutExamsKind
            List<Long> ordinarReqList = new ArrayList<>();//id заявлений, где нужно поставить ordinarKind

            //приемные кампании, в которых будут изменены виды конкурса заявлений на "Без вступ. испытаний"
            Set<Long> enrollCampWithoutExams = new HashSet<>();
            //приемные кампании, в которых будут изменены виды конкурса заявлений на "На общих основаниях"
            Set<Long> enrollCampOrdinarKind = new HashSet<>();

            for (Object[] line : spoREDList) {
                CompetitionKind newCompetitionKind;
                Long reqId = (Long) line[0];
                Long oldCompetitionKind = (Long) line[2];
                Long enrDirId = (Long) line[3];
                Long stateId = (Long) line[1];
                Long enrollCampId = (Long) line[11];
                //у активных или к зачислению заявлений и у которых нет вступительных испытаний сменяем вид конкурса на "Без вступ. исптытаний"
                if (!enrollDirWithDisc.contains(enrDirId) &&
                        (stateId.equals(activeState.getId()) || stateId.equals(toBeEnroledState.getId())))
                {
                    newCompetitionKind = withoutExamsKind;
                }
                else {
                    newCompetitionKind = ordinarKind;
                }

                if (!newCompetitionKind.getId().equals(oldCompetitionKind)) {
                    if (newCompetitionKind.getId().equals(withoutExamsKind.getId())) {
                        withoutExamsReqList.add(reqId);
                        enrollCampWithoutExams.add(enrollCampId);
                    }
                    else {
                        ordinarReqList.add(reqId);
                        enrollCampOrdinarKind.add(enrollCampId);
                    }
                    log.append(getSpoReqEnrDirEntryForLog(line, newCompetitionKind.getTitle()));
                }
            }

            List<EnrollmentCompetitionKind> disabledCompKinds = new ArrayList<>();
            if (withoutExamsReqList.size() > 0) {
                disabledCompKinds.addAll(getDisabledCompKinds(enrollCampWithoutExams, WITHOUT_EXAMS_KIND_CODE));
            }
            if (ordinarReqList.size() > 0) {
                disabledCompKinds.addAll(getDisabledCompKinds(enrollCampOrdinarKind, ORDINAR_KIND_CODE));
            }
            if (disabledCompKinds.size() > 0) {
                StringBuilder errorMsg = new StringBuilder();
                for (EnrollmentCompetitionKind compKind : disabledCompKinds) {
                    errorMsg.append(String.format("На приемную кампанию %s нет вида конкурса %s.", compKind.getEnrollmentCampaign().getTitle(), compKind.getCompetitionKind().getTitle()));
                }
                errorMsg.append(" Пожалуйста, добавьте их и повторите попытку");
                throw new ApplicationException(errorMsg.toString());
            }

            //сделаем 2 апдейта:
            if (withoutExamsReqList.size() > 0) {
                DQLUpdateBuilder reqWithoutExams = new DQLUpdateBuilder(RequestedEnrollmentDirection.class)
                        .where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirection.id()), withoutExamsReqList))
                        .set(RequestedEnrollmentDirection.competitionKind().s(), DQLExpressions.value(withoutExamsKind));
                reqWithoutExams.createStatement(getSession()).execute();
            }

            if (ordinarReqList.size() > 0) {
                DQLUpdateBuilder reqOrdinarExams = new DQLUpdateBuilder(RequestedEnrollmentDirection.class)
                        .where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirection.id()), ordinarReqList))
                        .set(RequestedEnrollmentDirection.competitionKind().s(), DQLExpressions.value(ordinarKind));
                reqOrdinarExams.createStatement(getSession()).execute();
            }
        }

        return log.toString();
    }

    private List<EnrollmentCompetitionKind> getDisabledCompKinds(Set<Long> enrollmentCampaignIds, String competitionKindCode) {
        DQLSelectBuilder enrollmentCampCompKindDisabledDql = new DQLSelectBuilder()
                .fromEntity(EnrollmentCompetitionKind.class, "e2c")
                .column("e2c")
                .where(DQLExpressions.and(
                        DQLExpressions.in(DQLExpressions.property(EnrollmentCompetitionKind.enrollmentCampaign().id().fromAlias("e2c")), enrollmentCampaignIds),
                        DQLExpressions.eq(DQLExpressions.property(EnrollmentCompetitionKind.used().fromAlias("e2c")), DQLExpressions.value(Boolean.FALSE)),
                        DQLExpressions.eq(DQLExpressions.property(EnrollmentCompetitionKind.competitionKind().code().fromAlias("e2c")), DQLExpressions.value(competitionKindCode))
                ));
        return getList(enrollmentCampCompKindDisabledDql);
    }

    private String getSpoReqEnrDirEntryForLog(Object[] line, String newCompetitionKindTitle) {
        StringBuilder fio = new StringBuilder();
        fio.append((String) line[4]);
        fio.append(' ');
        fio.append((String) line[5]);
        if (line[6] != null && ((String) line[6]).length() > 0) {
            fio.append(' ');
            fio.append((String) line[6]);
        }
        String elhsPrintTitle = (String) line[7];
        Long elhsId = (Long) line[8];
        Long entrantId = (Long) line[9];
        String oldCompetitionKindTitle = (String) line[10];

        return String.format(
                "Абитуриент (%s, %d), выбранное направление %s, %d, вид конкурса было %s - вид конкурса стало %s\r\n",
                fio.toString(),
                entrantId,
                elhsPrintTitle,
                elhsId,
                oldCompetitionKindTitle,
                newCompetitionKindTitle
        );
    }


}
