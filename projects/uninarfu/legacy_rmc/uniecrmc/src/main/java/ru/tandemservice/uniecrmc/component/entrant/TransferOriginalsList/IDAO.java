package ru.tandemservice.uniecrmc.component.entrant.TransferOriginalsList;

import ru.tandemservice.uniecrmc.entity.entrant.ChangeOriginalDocumentRED;
import ru.tandemservice.uni.dao.IUniDao;

import java.util.Collection;

public interface IDAO extends IUniDao<Model> {

    void cancelTransfer(Collection<ChangeOriginalDocumentRED> cancelList);


}
