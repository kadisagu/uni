package ru.tandemservice.uniecrmc.component.wizard.NextOfKinStep;

import org.apache.tapestry.form.validator.BaseValidator;
import org.apache.tapestry.form.validator.Required;
import ru.tandemservice.uniecrmc.component.wizard.WizardUtil;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class Model extends ru.tandemservice.uniec.component.wizard.NextOfKinStep.Model {
    public static final String PARAM_PHONE = "entrant.wizard.NextOfKinStep.field.contactPhone";

    private boolean matchEntrant;

    public boolean isMatchEntrant() {
        return matchEntrant;
    }

    public void setMatchEntrant(boolean matchEntrant) {
        this.matchEntrant = matchEntrant;
    }

    public List<? extends BaseValidator> getPhoneValidators() {
        if (WizardUtil.isRequired(PARAM_PHONE))
            return Arrays.asList(new Required());
        else
            return Collections.emptyList();
    }
}
