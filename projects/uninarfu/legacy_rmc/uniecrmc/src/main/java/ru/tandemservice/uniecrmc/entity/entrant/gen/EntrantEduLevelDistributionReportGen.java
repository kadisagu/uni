package ru.tandemservice.uniecrmc.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniecrmc.entity.entrant.EntrantEduLevelDistributionReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Распределение приема по специальностям 2014
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrantEduLevelDistributionReportGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniecrmc.entity.entrant.EntrantEduLevelDistributionReport";
    public static final String ENTITY_NAME = "entrantEduLevelDistributionReport";
    public static final int VERSION_HASH = 3342754;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTENT = "content";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_DEVELOP_FORM_TITLE = "developFormTitle";
    public static final String P_FORMATIVE_ORG_UNIT_TITLE = "formativeOrgUnitTitle";
    public static final String P_EDUCATION_LEVELS_HIGH_SCHOOL_TITLE = "educationLevelsHighSchoolTitle";
    public static final String P_DEVELOP_CONDITION_TITLE = "developConditionTitle";

    private DatabaseFile _content;     // Печатная форма
    private Date _formingDate;     // Дата формирования
    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private String _developFormTitle; 
    private String _formativeOrgUnitTitle; 
    private String _educationLevelsHighSchoolTitle; 
    private String _developConditionTitle; 

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getDevelopFormTitle()
    {
        return _developFormTitle;
    }

    /**
     * @param developFormTitle 
     */
    public void setDevelopFormTitle(String developFormTitle)
    {
        dirty(_developFormTitle, developFormTitle);
        _developFormTitle = developFormTitle;
    }

    /**
     * @return 
     */
    public String getFormativeOrgUnitTitle()
    {
        return _formativeOrgUnitTitle;
    }

    /**
     * @param formativeOrgUnitTitle 
     */
    public void setFormativeOrgUnitTitle(String formativeOrgUnitTitle)
    {
        dirty(_formativeOrgUnitTitle, formativeOrgUnitTitle);
        _formativeOrgUnitTitle = formativeOrgUnitTitle;
    }

    /**
     * @return 
     */
    public String getEducationLevelsHighSchoolTitle()
    {
        return _educationLevelsHighSchoolTitle;
    }

    /**
     * @param educationLevelsHighSchoolTitle 
     */
    public void setEducationLevelsHighSchoolTitle(String educationLevelsHighSchoolTitle)
    {
        dirty(_educationLevelsHighSchoolTitle, educationLevelsHighSchoolTitle);
        _educationLevelsHighSchoolTitle = educationLevelsHighSchoolTitle;
    }

    /**
     * @return 
     */
    public String getDevelopConditionTitle()
    {
        return _developConditionTitle;
    }

    /**
     * @param developConditionTitle 
     */
    public void setDevelopConditionTitle(String developConditionTitle)
    {
        dirty(_developConditionTitle, developConditionTitle);
        _developConditionTitle = developConditionTitle;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntrantEduLevelDistributionReportGen)
        {
            setContent(((EntrantEduLevelDistributionReport)another).getContent());
            setFormingDate(((EntrantEduLevelDistributionReport)another).getFormingDate());
            setEnrollmentCampaign(((EntrantEduLevelDistributionReport)another).getEnrollmentCampaign());
            setDevelopFormTitle(((EntrantEduLevelDistributionReport)another).getDevelopFormTitle());
            setFormativeOrgUnitTitle(((EntrantEduLevelDistributionReport)another).getFormativeOrgUnitTitle());
            setEducationLevelsHighSchoolTitle(((EntrantEduLevelDistributionReport)another).getEducationLevelsHighSchoolTitle());
            setDevelopConditionTitle(((EntrantEduLevelDistributionReport)another).getDevelopConditionTitle());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrantEduLevelDistributionReportGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrantEduLevelDistributionReport.class;
        }

        public T newInstance()
        {
            return (T) new EntrantEduLevelDistributionReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "content":
                    return obj.getContent();
                case "formingDate":
                    return obj.getFormingDate();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "developFormTitle":
                    return obj.getDevelopFormTitle();
                case "formativeOrgUnitTitle":
                    return obj.getFormativeOrgUnitTitle();
                case "educationLevelsHighSchoolTitle":
                    return obj.getEducationLevelsHighSchoolTitle();
                case "developConditionTitle":
                    return obj.getDevelopConditionTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "developFormTitle":
                    obj.setDevelopFormTitle((String) value);
                    return;
                case "formativeOrgUnitTitle":
                    obj.setFormativeOrgUnitTitle((String) value);
                    return;
                case "educationLevelsHighSchoolTitle":
                    obj.setEducationLevelsHighSchoolTitle((String) value);
                    return;
                case "developConditionTitle":
                    obj.setDevelopConditionTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "content":
                        return true;
                case "formingDate":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "developFormTitle":
                        return true;
                case "formativeOrgUnitTitle":
                        return true;
                case "educationLevelsHighSchoolTitle":
                        return true;
                case "developConditionTitle":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "content":
                    return true;
                case "formingDate":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "developFormTitle":
                    return true;
                case "formativeOrgUnitTitle":
                    return true;
                case "educationLevelsHighSchoolTitle":
                    return true;
                case "developConditionTitle":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "content":
                    return DatabaseFile.class;
                case "formingDate":
                    return Date.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "developFormTitle":
                    return String.class;
                case "formativeOrgUnitTitle":
                    return String.class;
                case "educationLevelsHighSchoolTitle":
                    return String.class;
                case "developConditionTitle":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrantEduLevelDistributionReport> _dslPath = new Path<EntrantEduLevelDistributionReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrantEduLevelDistributionReport");
    }
            

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantEduLevelDistributionReport#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantEduLevelDistributionReport#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantEduLevelDistributionReport#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return 
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantEduLevelDistributionReport#getDevelopFormTitle()
     */
    public static PropertyPath<String> developFormTitle()
    {
        return _dslPath.developFormTitle();
    }

    /**
     * @return 
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantEduLevelDistributionReport#getFormativeOrgUnitTitle()
     */
    public static PropertyPath<String> formativeOrgUnitTitle()
    {
        return _dslPath.formativeOrgUnitTitle();
    }

    /**
     * @return 
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantEduLevelDistributionReport#getEducationLevelsHighSchoolTitle()
     */
    public static PropertyPath<String> educationLevelsHighSchoolTitle()
    {
        return _dslPath.educationLevelsHighSchoolTitle();
    }

    /**
     * @return 
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantEduLevelDistributionReport#getDevelopConditionTitle()
     */
    public static PropertyPath<String> developConditionTitle()
    {
        return _dslPath.developConditionTitle();
    }

    public static class Path<E extends EntrantEduLevelDistributionReport> extends EntityPath<E>
    {
        private DatabaseFile.Path<DatabaseFile> _content;
        private PropertyPath<Date> _formingDate;
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<String> _developFormTitle;
        private PropertyPath<String> _formativeOrgUnitTitle;
        private PropertyPath<String> _educationLevelsHighSchoolTitle;
        private PropertyPath<String> _developConditionTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantEduLevelDistributionReport#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantEduLevelDistributionReport#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(EntrantEduLevelDistributionReportGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantEduLevelDistributionReport#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return 
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantEduLevelDistributionReport#getDevelopFormTitle()
     */
        public PropertyPath<String> developFormTitle()
        {
            if(_developFormTitle == null )
                _developFormTitle = new PropertyPath<String>(EntrantEduLevelDistributionReportGen.P_DEVELOP_FORM_TITLE, this);
            return _developFormTitle;
        }

    /**
     * @return 
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantEduLevelDistributionReport#getFormativeOrgUnitTitle()
     */
        public PropertyPath<String> formativeOrgUnitTitle()
        {
            if(_formativeOrgUnitTitle == null )
                _formativeOrgUnitTitle = new PropertyPath<String>(EntrantEduLevelDistributionReportGen.P_FORMATIVE_ORG_UNIT_TITLE, this);
            return _formativeOrgUnitTitle;
        }

    /**
     * @return 
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantEduLevelDistributionReport#getEducationLevelsHighSchoolTitle()
     */
        public PropertyPath<String> educationLevelsHighSchoolTitle()
        {
            if(_educationLevelsHighSchoolTitle == null )
                _educationLevelsHighSchoolTitle = new PropertyPath<String>(EntrantEduLevelDistributionReportGen.P_EDUCATION_LEVELS_HIGH_SCHOOL_TITLE, this);
            return _educationLevelsHighSchoolTitle;
        }

    /**
     * @return 
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantEduLevelDistributionReport#getDevelopConditionTitle()
     */
        public PropertyPath<String> developConditionTitle()
        {
            if(_developConditionTitle == null )
                _developConditionTitle = new PropertyPath<String>(EntrantEduLevelDistributionReportGen.P_DEVELOP_CONDITION_TITLE, this);
            return _developConditionTitle;
        }

        public Class getEntityClass()
        {
            return EntrantEduLevelDistributionReport.class;
        }

        public String getEntityName()
        {
            return "entrantEduLevelDistributionReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
