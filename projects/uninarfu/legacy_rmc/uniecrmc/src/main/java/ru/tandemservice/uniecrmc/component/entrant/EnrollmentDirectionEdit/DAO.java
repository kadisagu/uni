package ru.tandemservice.uniecrmc.component.entrant.EnrollmentDirectionEdit;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionExt;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;

import java.util.Map;

public class DAO extends ru.tandemservice.uniec.component.entrant.EnrollmentDirectionEdit.DAO {

    @Override
    public void prepare(ru.tandemservice.uniec.component.entrant.EnrollmentDirectionEdit.Model model) {
        super.prepare(model);

        Model myModel = (Model) model;

        if (model.getEnrollmentDirection() != null) {
            EnrollmentDirectionExt enrollmentDirectionExt = get(EnrollmentDirectionExt.class, EnrollmentDirectionExt.enrollmentDirection().id(), model.getEnrollmentDirection().getId());

            if (enrollmentDirectionExt == null) {
                enrollmentDirectionExt = new EnrollmentDirectionExt();
                enrollmentDirectionExt.setEnrollmentDirection(model.getEnrollmentDirection());
            }

            myModel.setEnrollmentDirectionExt(enrollmentDirectionExt);
        }


    }

    @Override
    public void update(ru.tandemservice.uniec.component.entrant.EnrollmentDirectionEdit.Model model) {
        validate(model, UserContext.getInstance().getErrorCollector());
        super.update(model);
        Model myModel = (Model) model;
        getSession().saveOrUpdate(myModel.getEnrollmentDirectionExt());
    }

    @Override
    public void validate(
            ru.tandemservice.uniec.component.entrant.EnrollmentDirectionEdit.Model model,
            ErrorCollector errors)
    {

        Model myModel = (Model) model;

        Integer targetBudget = model.getEnrollmentDirection().getTargetAdmissionPlanBudget() == null ? 0 : model.getEnrollmentDirection().getTargetAdmissionPlanBudget();
        Integer targetContract = model.getEnrollmentDirection().getTargetAdmissionPlanContract() == null ? 0 : model.getEnrollmentDirection().getTargetAdmissionPlanContract();
        Integer totalBudget = model.getEnrollmentDirection().getMinisterialPlan() == null ? 0 : model.getEnrollmentDirection().getMinisterialPlan();
        Integer totalContract = model.getEnrollmentDirection().getContractPlan() == null ? 0 : model.getEnrollmentDirection().getContractPlan();
        Integer specialBudget = myModel.getEnrollmentDirectionExt().getHaveSpecialRightsPlanBudget() == null ? 0 : myModel.getEnrollmentDirectionExt().getHaveSpecialRightsPlanBudget();
        Integer specialContract = myModel.getEnrollmentDirectionExt().getHaveSpecialRightsPlanContract() == null ? 0 : myModel.getEnrollmentDirectionExt().getHaveSpecialRightsPlanContract();

        if (!model.isSimpleTargetAdmissionKind()) {
            targetBudget = getTotalPlanValue(model.getValueMapBudget());
            targetContract = getTotalPlanValue(model.getValueMapContract());
        }

        if (totalBudget.intValue() < (targetBudget.intValue() + specialBudget.intValue()))
            throw new ApplicationException("План приема на бюджет (всего) должен быть больше планам приема по целевому приему и имеющих особые права при поступлении");

        if (totalContract.intValue() < (targetContract.intValue() + specialContract.intValue()))
            throw new ApplicationException("План приема на договору (всего) должен быть больше планам приема по целевому приему и имеющих особые права при поступлении");

    }

    private Integer getTotalPlanValue(Map<TargetAdmissionKind, Integer> valueMap) {
        int totalPlanValue = 0;
        for (Map.Entry<TargetAdmissionKind, Integer> entry : valueMap.entrySet()) {
            Integer planValue = entry.getValue();
            if (planValue != null)
                totalPlanValue += planValue.intValue();
        }
        return totalPlanValue;
    }
}
