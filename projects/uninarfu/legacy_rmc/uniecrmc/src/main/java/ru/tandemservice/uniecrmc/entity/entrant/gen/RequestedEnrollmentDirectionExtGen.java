package ru.tandemservice.uniecrmc.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение сущности Выбранное направление приема
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RequestedEnrollmentDirectionExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt";
    public static final String ENTITY_NAME = "requestedEnrollmentDirectionExt";
    public static final int VERSION_HASH = 1729027340;
    private static IEntityMeta ENTITY_META;

    public static final String L_REQUESTED_ENROLLMENT_DIRECTION = "requestedEnrollmentDirection";
    public static final String P_HAVE_SPECIAL_RIGHTS = "haveSpecialRights";
    public static final String L_COURSE = "course";
    public static final String L_TERM = "term";
    public static final String L_GROUP = "group";

    private RequestedEnrollmentDirection _requestedEnrollmentDirection;     // Выбранное направление приема
    private boolean _haveSpecialRights;     // Имеет особые права
    private Course _course;     // Курс
    private Term _term;     // Семестр
    private Group _group;     // Группа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выбранное направление приема. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
    {
        return _requestedEnrollmentDirection;
    }

    /**
     * @param requestedEnrollmentDirection Выбранное направление приема. Свойство не может быть null и должно быть уникальным.
     */
    public void setRequestedEnrollmentDirection(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        dirty(_requestedEnrollmentDirection, requestedEnrollmentDirection);
        _requestedEnrollmentDirection = requestedEnrollmentDirection;
    }

    /**
     * @return Имеет особые права. Свойство не может быть null.
     */
    @NotNull
    public boolean isHaveSpecialRights()
    {
        return _haveSpecialRights;
    }

    /**
     * @param haveSpecialRights Имеет особые права. Свойство не может быть null.
     */
    public void setHaveSpecialRights(boolean haveSpecialRights)
    {
        dirty(_haveSpecialRights, haveSpecialRights);
        _haveSpecialRights = haveSpecialRights;
    }

    /**
     * @return Курс.
     */
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Семестр.
     */
    public Term getTerm()
    {
        return _term;
    }

    /**
     * @param term Семестр.
     */
    public void setTerm(Term term)
    {
        dirty(_term, term);
        _term = term;
    }

    /**
     * @return Группа.
     */
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RequestedEnrollmentDirectionExtGen)
        {
            setRequestedEnrollmentDirection(((RequestedEnrollmentDirectionExt)another).getRequestedEnrollmentDirection());
            setHaveSpecialRights(((RequestedEnrollmentDirectionExt)another).isHaveSpecialRights());
            setCourse(((RequestedEnrollmentDirectionExt)another).getCourse());
            setTerm(((RequestedEnrollmentDirectionExt)another).getTerm());
            setGroup(((RequestedEnrollmentDirectionExt)another).getGroup());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RequestedEnrollmentDirectionExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RequestedEnrollmentDirectionExt.class;
        }

        public T newInstance()
        {
            return (T) new RequestedEnrollmentDirectionExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "requestedEnrollmentDirection":
                    return obj.getRequestedEnrollmentDirection();
                case "haveSpecialRights":
                    return obj.isHaveSpecialRights();
                case "course":
                    return obj.getCourse();
                case "term":
                    return obj.getTerm();
                case "group":
                    return obj.getGroup();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "requestedEnrollmentDirection":
                    obj.setRequestedEnrollmentDirection((RequestedEnrollmentDirection) value);
                    return;
                case "haveSpecialRights":
                    obj.setHaveSpecialRights((Boolean) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "term":
                    obj.setTerm((Term) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "requestedEnrollmentDirection":
                        return true;
                case "haveSpecialRights":
                        return true;
                case "course":
                        return true;
                case "term":
                        return true;
                case "group":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "requestedEnrollmentDirection":
                    return true;
                case "haveSpecialRights":
                    return true;
                case "course":
                    return true;
                case "term":
                    return true;
                case "group":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "requestedEnrollmentDirection":
                    return RequestedEnrollmentDirection.class;
                case "haveSpecialRights":
                    return Boolean.class;
                case "course":
                    return Course.class;
                case "term":
                    return Term.class;
                case "group":
                    return Group.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RequestedEnrollmentDirectionExt> _dslPath = new Path<RequestedEnrollmentDirectionExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RequestedEnrollmentDirectionExt");
    }
            

    /**
     * @return Выбранное направление приема. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt#getRequestedEnrollmentDirection()
     */
    public static RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> requestedEnrollmentDirection()
    {
        return _dslPath.requestedEnrollmentDirection();
    }

    /**
     * @return Имеет особые права. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt#isHaveSpecialRights()
     */
    public static PropertyPath<Boolean> haveSpecialRights()
    {
        return _dslPath.haveSpecialRights();
    }

    /**
     * @return Курс.
     * @see ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Семестр.
     * @see ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt#getTerm()
     */
    public static Term.Path<Term> term()
    {
        return _dslPath.term();
    }

    /**
     * @return Группа.
     * @see ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    public static class Path<E extends RequestedEnrollmentDirectionExt> extends EntityPath<E>
    {
        private RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> _requestedEnrollmentDirection;
        private PropertyPath<Boolean> _haveSpecialRights;
        private Course.Path<Course> _course;
        private Term.Path<Term> _term;
        private Group.Path<Group> _group;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выбранное направление приема. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt#getRequestedEnrollmentDirection()
     */
        public RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> requestedEnrollmentDirection()
        {
            if(_requestedEnrollmentDirection == null )
                _requestedEnrollmentDirection = new RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection>(L_REQUESTED_ENROLLMENT_DIRECTION, this);
            return _requestedEnrollmentDirection;
        }

    /**
     * @return Имеет особые права. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt#isHaveSpecialRights()
     */
        public PropertyPath<Boolean> haveSpecialRights()
        {
            if(_haveSpecialRights == null )
                _haveSpecialRights = new PropertyPath<Boolean>(RequestedEnrollmentDirectionExtGen.P_HAVE_SPECIAL_RIGHTS, this);
            return _haveSpecialRights;
        }

    /**
     * @return Курс.
     * @see ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Семестр.
     * @see ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt#getTerm()
     */
        public Term.Path<Term> term()
        {
            if(_term == null )
                _term = new Term.Path<Term>(L_TERM, this);
            return _term;
        }

    /**
     * @return Группа.
     * @see ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

        public Class getEntityClass()
        {
            return RequestedEnrollmentDirectionExt.class;
        }

        public String getEntityName()
        {
            return "requestedEnrollmentDirectionExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
