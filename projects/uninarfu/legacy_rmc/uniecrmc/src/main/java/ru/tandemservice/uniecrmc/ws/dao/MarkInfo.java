package ru.tandemservice.uniecrmc.ws.dao;

import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineKind;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineType;

public class MarkInfo {
    public int Mark;
    public int MarkSource;
    public String Code;
    public String Title;
    public String ShortTitle;
    public EntranceDisciplineType EntranceDisciplineType;
    public EntranceDisciplineKind EntranceDisciplineKind;
}
