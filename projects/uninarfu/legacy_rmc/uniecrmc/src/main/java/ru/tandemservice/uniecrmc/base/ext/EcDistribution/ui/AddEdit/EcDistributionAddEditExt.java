package ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.AddEdit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtension;
import org.tandemframework.caf.ui.config.datasource.IColumnListExtensionBuilder;
import ru.tandemservice.uniec.base.bo.EcDistribution.ui.AddEdit.EcDistributionAddEdit;

@Configuration
public class EcDistributionAddEditExt extends BusinessComponentExtensionManager {

    @Autowired
    private EcDistributionAddEdit parent;

    @Bean
    public ColumnListExtension extension()
    {
        IColumnListExtensionBuilder c = columnListExtensionBuilder(parent.directionDS());
        c.addColumn(blockColumn("srQuota", "srQuotaBlockColumn").visible("ui:usePriority"));
        return c.create();
    }
}
