package ru.tandemservice.uniecrmc.dao.entrantRating;

import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

public class QuotasNode {

    @XmlElement(name = "Quota")
    public List<QuotaRow> quotaList = new ArrayList<>();

    public QuotasNode() {
    }

    public void addQuota(TargetAdmissionKind kind, int plan) {
        QuotaRow node = new QuotaRow(kind.getCode(), plan);
        quotaList.add(node);
    }


    public static class QuotaRow extends RowNode {
        @XmlAttribute
        public int plan;

        public QuotaRow() {
        }

        public QuotaRow(String code, int plan) {
            super(code, null, null);
            this.plan = plan;
        }
    }
}
