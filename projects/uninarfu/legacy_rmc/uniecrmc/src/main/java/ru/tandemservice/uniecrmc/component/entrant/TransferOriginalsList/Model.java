package ru.tandemservice.uniecrmc.component.entrant.TransferOriginalsList;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniecrmc.entity.entrant.ChangeOriginalDocumentRED;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.List;

public class Model implements IEnrollmentCampaignSelectModel {

    private List<EnrollmentCampaign> enrollmentCampaignList;
    private ISelectModel enrollmentDirectionModel;
    private ISelectModel entrantModel;
    private ISelectModel entrantStateModel;

    private DynamicListDataSource<ChangeOriginalDocumentRED> dataSource;

    private IDataSettings settings;

    public EnrollmentCampaign getEnrollmentCampaign() {
        return (EnrollmentCampaign) getSettings().get("enrollmentCampaign");
    }

    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign) {
        getSettings().set("enrollmentCampaign", enrollmentCampaign);
    }

    public List<EnrollmentCampaign> getEnrollmentCampaignList() {
        return enrollmentCampaignList;
    }

    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList) {
        this.enrollmentCampaignList = enrollmentCampaignList;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public ISelectModel getEnrollmentDirectionModel() {
        return enrollmentDirectionModel;
    }

    public void setEnrollmentDirectionModel(ISelectModel enrollmentDirectionModel) {
        this.enrollmentDirectionModel = enrollmentDirectionModel;
    }

    public ISelectModel getEntrantModel() {
        return entrantModel;
    }

    public void setEntrantModel(ISelectModel entrantModel) {
        this.entrantModel = entrantModel;
    }

    public ISelectModel getEntrantStateModel() {
        return entrantStateModel;
    }

    public void setEntrantStateModel(ISelectModel entrantStateModel) {
        this.entrantStateModel = entrantStateModel;
    }

    public DynamicListDataSource<ChangeOriginalDocumentRED> getDataSource() {
        return dataSource;
    }

    public void setDataSource(
            DynamicListDataSource<ChangeOriginalDocumentRED> dataSource)
    {
        this.dataSource = dataSource;
    }

}
