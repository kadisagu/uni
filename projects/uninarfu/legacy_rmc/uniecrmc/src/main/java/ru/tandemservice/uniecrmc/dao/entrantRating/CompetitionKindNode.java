package ru.tandemservice.uniecrmc.dao.entrantRating;

import ru.tandemservice.uniec.entity.catalog.CompetitionKind;

import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

public class CompetitionKindNode {

    @XmlElement(name = "CompetitionKind")
    public List<RowNode> list = new ArrayList<>();

    public CompetitionKindNode() {
    }

    public void add(CompetitionKind entity, boolean used) {
        RowNode node = new RowNode(entity.getCode(), entity.getTitle(), entity.getShortTitle(), used, entity.getPriority(), null);
        list.add(node);
    }
}
