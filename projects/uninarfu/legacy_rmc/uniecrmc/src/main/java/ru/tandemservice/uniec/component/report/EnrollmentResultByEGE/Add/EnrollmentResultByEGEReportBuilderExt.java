package ru.tandemservice.uniec.component.report.EnrollmentResultByEGE.Add;

import org.hibernate.Session;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.tools.PrivateAccessor;
import ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.util.EntrantDataUtil;

import java.util.*;

public class EnrollmentResultByEGEReportBuilderExt extends EnrollmentResultByEGEReportBuilder {

    private Session session;
    private Model model;

    public EnrollmentResultByEGEReportBuilderExt(Model model, Session session) {
        super(model, session);
        this.model = model;
        this.session = session;
    }

    @Override
    public DatabaseFile getContent() {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private byte[] buildReport() {

        MQBuilder builder = (MQBuilder) PrivateAccessor.invokePrivateMethod(this, EnrollmentResultByEGEReportBuilder.class, "getPreStudentBuilder", new Object[]{});

        List<PreliminaryEnrollmentStudent> preStudentSet = builder.getResultList(this.session);

        builder.getSelectAliasList().clear();
        builder.addSelect("p", new String[]{PreliminaryEnrollmentStudent.requestedEnrollmentDirection().s()});

        EntrantDataUtil dataUtil = new EntrantDataUtil(this.session, this.model.getEnrollmentCampaign(), builder, EntrantDataUtil.DetailLevel.EXAM_PASS);

        Set<EnrollmentDirection> directionSet = new HashSet<>();

        for (RequestedEnrollmentDirection direction : dataUtil.getDirectionSet()) {
            directionSet.add(direction.getEnrollmentDirection());
        }

        List<ISumLev> list = getDataRowList(dataUtil, preStudentSet, directionSet);
        // новые позиции строк для вывода
        final Map<Integer, ISumLev> reorderMap = new HashMap<>();

        // номера строк для выделения жирным шрифтом(это сводные строки по формирующим и итоговые строки)
        final Set<Integer> headerRows = new HashSet<>();

        // группируем по формирующему подразделению
        Map<OrgUnit, List<ISumLev>> resultMap = getGrouppedByFormativeOrgUnit(list);

        // новая размерность = количество элементов для вывода + количество итоговых строк по формирующим подразделениям + строка "ВСЕГО"
        String[][] data = new String[list.size() + resultMap.keySet().size() + 1][9];

        DoubleFormatter formatter = new DoubleFormatter(1, true);

        // для подсчета по суммарной информации по всем данным отчета
        double totalCommonSum = 0.0D, totalBenefitSum = 0.0D, totalOlympiadSum = 0.0D, totalTargetSum = 0.0D;
        int totalCommonSumCount = 0, totalBenefitSumCount = 0, totalOlympiadSumCount = 0, totalTargetSumCount = 0;
        int totalCount = 0, totalBenefitCount = 0, totalOlympiadCount = 0, totalTargetCount = 0;

        int i = 0; // зарезервируем счетчик для вывода строк с итоговыми данными по формирующему подразделению
        for (Map.Entry<OrgUnit, List<ISumLev>> entry : resultMap.entrySet()) {

            OrgUnit formativeOrgUnit = entry.getKey();

            // для подсчета по формирующим подразделениям
            double commonSum = 0.0D, benefitSum = 0.0D, olympiadSum = 0.0D, targetSum = 0.0D;
            int commonSumCount = 0, benefitSumCount = 0, olympiadSumCount = 0, targetSumCount = 0;
            int count = 0, benefitCount = 0, olympiadCount = 0, targetCount = 0;

            int j = i + 1; // счетчик для вывода записей, относящихся к формирующему подразделению

            for (ISumLev item : entry.getValue()) {
                SumLevExt sumLevExt = (SumLevExt) item;
                // формирующее подразделение
                commonSum += sumLevExt.getCommonSum().doubleValue();
                benefitSum += sumLevExt.getBenefitSum().doubleValue();
                olympiadSum += sumLevExt.getOlympiadSum().doubleValue();
                targetSum += sumLevExt.getTargetSum().doubleValue();

                commonSumCount += sumLevExt.getCommonSumCount().intValue();
                benefitSumCount += sumLevExt.getBenefitSumCount().intValue();
                olympiadSumCount += sumLevExt.getOlympiadSumCount().intValue();
                targetSumCount += sumLevExt.getTargetSumCount().intValue();

                count += sumLevExt.getCount();
                benefitCount += sumLevExt.getBenefitCount();
                olympiadCount += sumLevExt.getOlympiadCount();
                targetCount += sumLevExt.getTargetCount();

                // ВСЕГО
                totalCommonSum += sumLevExt.getCommonSum().doubleValue();
                totalBenefitSum += sumLevExt.getBenefitSum().doubleValue();
                totalOlympiadSum += sumLevExt.getOlympiadSum().doubleValue();
                totalTargetSum += sumLevExt.getTargetSum().doubleValue();

                totalCommonSumCount += sumLevExt.getCommonSumCount().intValue();
                totalBenefitSumCount += sumLevExt.getBenefitSumCount().intValue();
                totalOlympiadSumCount += sumLevExt.getOlympiadSumCount().intValue();
                totalTargetSumCount += sumLevExt.getTargetSumCount().intValue();

                totalCount += sumLevExt.getCount();
                totalBenefitCount += sumLevExt.getBenefitCount();
                totalOlympiadCount += sumLevExt.getOlympiadCount();
                totalTargetCount += sumLevExt.getTargetCount();

                // заполним строку таблицы
                fillRow(item, data[j]);

                // и запомним ее новую позицию
                reorderMap.put(j, sumLevExt);
                j++;
            }

            // заполним строку со сводными данными по формирующему подразделению
            String[] row = data[i];
            row[0] = formativeOrgUnit.getShortTitle();
            row[1] = getAverageValue(commonSum, commonSumCount, formatter);
            row[2] = getAverageValue(benefitSum, benefitSumCount, formatter);
            row[3] = getAverageValue(olympiadSum, olympiadSumCount, formatter);
            row[4] = getAverageValue(targetSum, targetSumCount, formatter);

            if (count > 0)
                row[5] = Integer.toString(count);
            if (benefitCount > 0)
                row[6] = Integer.toString(benefitCount);
            if (olympiadCount > 0)
                row[7] = Integer.toString(olympiadCount);
            if (targetCount > 0) {
                row[8] = Integer.toString(targetCount);
            }

            headerRows.add(i); // !!! помним, что сводную по формирующему строку выделяем

            i = j;
        }

        // строка ВСЕГО
        headerRows.add(i); // !!! помним, что строку ВСЕГО тоже выделяем
        String[] row = data[i];
        row[0] = "ВСЕГО";
        row[1] = getAverageValue(totalCommonSum, totalCommonSumCount, formatter);
        row[2] = getAverageValue(totalBenefitSum, totalBenefitSumCount, formatter);
        row[3] = getAverageValue(totalOlympiadSum, totalOlympiadSumCount, formatter);
        row[4] = getAverageValue(totalTargetSum, totalTargetSumCount, formatter);
        if (totalCount > 0)
            row[5] = Integer.toString(totalCount);
        if (totalBenefitCount > 0)
            row[6] = Integer.toString(totalBenefitCount);
        if (totalOlympiadCount > 0)
            row[7] = Integer.toString(totalOlympiadCount);
        if (totalTargetCount > 0)
            row[8] = Integer.toString(totalTargetCount);

        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, "enrollmentResultByEGE");
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());
        final int grayColor = document.getHeader().getColorTable().addColor(128, 128, 128);

        RtfInjectModifier im = new RtfInjectModifier();
        im.put("year", Integer.toString(this.model.getEnrollmentCampaign().getEducationYear().getIntValue()));
        im.put("params", (String) PrivateAccessor.invokePrivateMethod(this, EnrollmentResultByEGEReportBuilder.class, "getParamsTitle", new Object[]{}));
        im.modify(document);

        RtfTableModifier tm = new RtfTableModifier();
        tm.put("T", data);

        tm.put("T", new RtfRowIntercepterBase() {
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value) {
                ISumLev sumLevExt = reorderMap.get(rowIndex);

                if (headerRows.contains(rowIndex)) {
                    return new RtfString().boldBegin().append(value).boldEnd().toList();
                }

                if (sumLevExt == null)
                    return null;

                switch (sumLevExt.getLevel()) {
                    case 0:
                        return new RtfString().color(grayColor).boldBegin().append(value).boldEnd().toList();
                    case 1:
                        return new RtfString().color(grayColor).append(value).toList();
                    case 2:
                        return null;
                }
                throw new RuntimeException("Unknown level: " + sumLevExt.getLevel());
            }
        });
        tm.modify(document);

        return RtfUtil.toByteArray(document);
    }

    /**
     * Нахождение среднего арифметического
     */
    protected String getAverageValue(double targetValue, int targetCount, DoubleFormatter formatter) {
        double avgValue = ((targetCount == 0) ? 0.0D : targetValue / targetCount);
        if (avgValue > 0.0D) {
            return formatter.format(avgValue);
        }

        return null;
    }

    /**
     * Сгруппированные данные по формирующему подразделению
     */
    protected Map<OrgUnit, List<ISumLev>> getGrouppedByFormativeOrgUnit(List<ISumLev> sumLevList) {
        Map<OrgUnit, List<ISumLev>> grouppedMap = new HashMap<>();
        for (ISumLev sumLev : sumLevList) {
            OrgUnit formativeOrgUnit = ((SumLevExt) sumLev).getFormativeOrgUnit();
            List<ISumLev> lst = grouppedMap.get(formativeOrgUnit);
            if (lst == null) {
                lst = new ArrayList<>();
            }

            lst.add(sumLev);
            grouppedMap.put(formativeOrgUnit, lst);
        }

        return grouppedMap;
    }

    protected String[] fillRow(ISumLev sumLev, String[] row) {
        SumLevExt sumLevExt = (SumLevExt) sumLev;
        DoubleFormatter formatter = new DoubleFormatter(1, true);

        double commonAvgItem = sumLevExt.getCommonAvg();
        double benefitAvgItem = sumLevExt.getBenefitAvg();
        double olympiadAvgItem = sumLevExt.getOlympiadAvg();
        double targetAvgItem = sumLevExt.getTargetAvg();
        int countItem = sumLevExt.getCount();
        int benefitCountItem = sumLevExt.getBenefitCount();
        int olympiadCountItem = sumLevExt.getOlympiadCount();
        int targetCountItem = sumLevExt.getTargetCount();

        row[0] = sumLevExt.getTitle();

        if (commonAvgItem > 0.0D)
            row[1] = formatter.format(commonAvgItem);
        if (benefitAvgItem > 0.0D)
            row[2] = formatter.format(benefitAvgItem);
        if (olympiadAvgItem > 0.0D)
            row[3] = formatter.format(olympiadAvgItem);
        if (targetAvgItem > 0.0D)
            row[4] = formatter.format(targetAvgItem);
        if (countItem > 0)
            row[5] = Integer.toString(countItem);
        if (benefitCountItem > 0)
            row[6] = Integer.toString(benefitCountItem);
        if (olympiadCountItem > 0)
            row[7] = Integer.toString(olympiadCountItem);
        if (targetCountItem > 0) {
            row[8] = Integer.toString(targetCountItem);
        }

        return row;
    }

    private List<ISumLev> getDataRowList(EntrantDataUtil dataUtil, List<PreliminaryEnrollmentStudent> preStudentSet, Set<EnrollmentDirection> directionSet) {

        Map<EnrollmentDirection, EnrollmentDirectionDescription> directionDataMap = new Hashtable<>();

        // предварительно проинициализируем
        SumLevExt.requestedEnrolmentDirectionExtMap = getRequestedEnrolmentDirectionExt(preStudentSet);

        Map<EducationLevels, SumLevExt> mapLev1 = new Hashtable<>();
        Map<EducationLevels, SumLevExt> mapLev2 = new Hashtable<>();
        Map<EnrollmentDirection, SumLevExt> mapLev3 = new Hashtable<>();

        for (EnrollmentDirection enrollmentDirection : directionSet) {

            EducationLevels educationLevel = enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();
            OrgUnit formativeOrgUnit = enrollmentDirection.getEducationOrgUnit().getFormativeOrgUnit();

            while (!educationLevel.getLevelType().isGroupingLevel()) {
                educationLevel = educationLevel.getParentLevel();
            }

            EducationLevels educationLevelGroup = educationLevel;

            while (educationLevelGroup.getParentLevel() != null) {
                educationLevelGroup = educationLevelGroup.getParentLevel();
            }

            EnrollmentDirectionDescription data = new EnrollmentDirectionDescription();
            data.educationLevel = educationLevel;
            data.educationLevelGroup = educationLevelGroup;
            directionDataMap.put(enrollmentDirection, data);

            // Добавим формирующее подразделение в мапы для дальнейшей группировки
            if (!mapLev1.containsKey(educationLevelGroup)) {
                mapLev1.put(educationLevelGroup, new SumLevExt(dataUtil, educationLevelGroup.getDisplayableTitle(), educationLevelGroup.getId(), 0, formativeOrgUnit));
            }

            if (!mapLev2.containsKey(educationLevel)) {
                mapLev2.put(educationLevel, new SumLevExt(dataUtil, educationLevel.getDisplayableTitle(), educationLevel.getId(), 1, formativeOrgUnit));
            }

            if (!mapLev3.containsKey(enrollmentDirection)) {
                mapLev3.put(enrollmentDirection, new SumLevExt(dataUtil, enrollmentDirection.getPrintTitle(), enrollmentDirection.getId(), 2, formativeOrgUnit));
            }

        }

        for (PreliminaryEnrollmentStudent preStudent : preStudentSet) {
            mapLev3.get(preStudent.getRequestedEnrollmentDirection().getEnrollmentDirection()).add(preStudent);
        }

        Map<SumLevExt, Map<SumLevExt, Set<SumLevExt>>> resultMap = new TreeMap<>();

        for (Map.Entry<EnrollmentDirection, SumLevExt> entry : mapLev3.entrySet()) {
            EnrollmentDirection enrollmentDirection = entry.getKey();
            EnrollmentDirectionDescription data = directionDataMap.get(enrollmentDirection);

            SumLevExt sumLev1 = mapLev1.get(data.educationLevelGroup);
            SumLevExt sumLev2 = mapLev2.get(data.educationLevel);
            SumLevExt sumLev3 = entry.getValue();

            Map<SumLevExt, Set<SumLevExt>> map = resultMap.get(sumLev1);

            if ((map == null) && (null != resultMap.put(sumLev1, map = new TreeMap<>()))) {
                throw new RuntimeException("Fatal override");
            }
            Set<SumLevExt> list = map.get(sumLev2);

            if ((list == null) && (null != map.put(sumLev2, list = new TreeSet<>()))) {
                throw new RuntimeException("Fatal override");
            }

            if (!list.add(sumLev3)) {
                throw new RuntimeException("Fatal override");
            }
        }

        List<ISumLev> list = new ArrayList<>();
        SumLevExt sumLev1;

        for (Map.Entry<SumLevExt, Map<SumLevExt, Set<SumLevExt>>> entry : resultMap.entrySet()) {
            sumLev1 = entry.getKey();
            list.add(sumLev1);

            for (Map.Entry<SumLevExt, Set<SumLevExt>> subEntry : entry.getValue().entrySet()) {
                SumLevExt sumLev2 = subEntry.getKey();
                list.add(sumLev2);

                for (SumLevExt sumLev3 : subEntry.getValue()) {
                    list.add(sumLev3);
                }

                sumLev1.append(sumLev2);
            }

        }

        return list;
    }

    protected Map<Long, RequestedEnrollmentDirectionExt> getRequestedEnrolmentDirectionExt(List<PreliminaryEnrollmentStudent> preStudentSet) {

        Set<Long> directionIds = new HashSet<>();
        for (PreliminaryEnrollmentStudent preStudent : preStudentSet) {
            directionIds.add(preStudent.getRequestedEnrollmentDirection().getId());
        }

        final Map<Long, RequestedEnrollmentDirectionExt> dirExtMap = new HashMap<>();

        BatchUtils.execute(directionIds, 1000, elements -> {
            DQLSelectBuilder directionExtBuilder = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirectionExt.class, "ext")
                    .where(DQLExpressions.in(RequestedEnrollmentDirectionExt.requestedEnrollmentDirection().id().fromAlias("ext"), elements));
            List<RequestedEnrollmentDirectionExt> list = directionExtBuilder.createStatement(session).list();

            for (RequestedEnrollmentDirectionExt ext : list) {
                dirExtMap.put(ext.getRequestedEnrollmentDirection().getId(), ext);
            }
        });

        return dirExtMap;
    }

    private static class EnrollmentDirectionDescription {
        private EducationLevels educationLevel;
        private EducationLevels educationLevelGroup;
    }
}