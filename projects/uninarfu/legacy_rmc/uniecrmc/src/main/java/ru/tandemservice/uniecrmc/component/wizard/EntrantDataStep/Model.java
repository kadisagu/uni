package ru.tandemservice.uniecrmc.component.wizard.EntrantDataStep;

public class Model extends ru.tandemservice.uniec.component.wizard.EntrantDataStep.Model {
    private ru.tandemservice.uniecrmc.component.entrant.EntrantEdit.Model _entrantEditModel = new ru.tandemservice.uniecrmc.component.entrant.EntrantEdit.Model();

    @Override
    public ru.tandemservice.uniec.component.entrant.EntrantEdit.Model getEntrantEditModel()
    {
        return this._entrantEditModel;
    }


}
