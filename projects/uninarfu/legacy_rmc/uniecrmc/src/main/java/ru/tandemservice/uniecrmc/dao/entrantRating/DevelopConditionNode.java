package ru.tandemservice.uniecrmc.dao.entrantRating;

import ru.tandemservice.uni.entity.catalog.DevelopCondition;

import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

public class DevelopConditionNode {

    @XmlElement(name = "DevelopCondition")
    public List<RowNode> list = new ArrayList<>();

    public DevelopConditionNode() {
    }

    public void add(DevelopCondition entity) {
        RowNode node = new RowNode(entity.getCode(), entity.getTitle(), entity.getShortTitle());
        list.add(node);
    }
}
