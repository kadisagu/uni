package ru.tandemservice.uniecrmc.component.entrant.RequestedEnrollmentDirectionStateAddEdit;

import org.tandemframework.core.runtime.ApplicationRuntime;
import ru.tandemservice.uniec.component.entrant.RequestedEnrollmentDirectionStateAddEdit.Model;
import ru.tandemservice.uniec.dao.IEntrantDAO;

import java.util.ArrayList;
import java.util.List;

public class DAO
        extends ru.tandemservice.uniec.component.entrant.RequestedEnrollmentDirectionStateAddEdit.DAO
{

    @Override
    public void update(Model model)
    {
        super.update(model);
        // пересчитать абитуриента целиком
        Long entrantId = model.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getId();

        List<Long> ids = new ArrayList<>();
        ids.add(entrantId);

        // пересчитываем абитуриентов
        IEntrantDAO idao = (IEntrantDAO) ApplicationRuntime.getBean(IEntrantDAO.class.getName());
        idao.updateEntrantData(ids);

    }

}
