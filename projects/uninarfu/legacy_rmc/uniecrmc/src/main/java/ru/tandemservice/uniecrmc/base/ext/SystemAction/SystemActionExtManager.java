package ru.tandemservice.uniecrmc.base.ext.SystemAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.SystemActionManager;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.util.SystemActionDefinition;
import ru.tandemservice.uniecrmc.base.ext.SystemAction.ui.Pub.SystemActionPubExt;

@Configuration
public class SystemActionExtManager extends BusinessObjectExtensionManager {

    @Autowired
    private SystemActionManager systemActionManager;

    @Bean
    public ItemListExtension<SystemActionDefinition> buttonListExtension() {
        return itemListExtension(this.systemActionManager.buttonListExtPoint())
                .add("uniec_fillPriorityEntranceDisciple", new SystemActionDefinition("uniec", "fillPriorityEntranceDisciple", "onClickFillPriorityEntranceDisciple", SystemActionPubExt.ECRMC_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("uniec_changeCompetitionKindSpo", new SystemActionDefinition("uniec", "changeCompetitionKindSpo", "onClickChangeCompetitionKindSpo", SystemActionPubExt.ECRMC_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("uniec_changeOlimpiadPolitics", new SystemActionDefinition("uniec", "changeOlimpiadPolitics", "onChangeOlimpiadPolitics", SystemActionPubExt.ECRMC_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("uniec_addOtherDirectionToRequest", new SystemActionDefinition("uniec", "addOtherDirectionToRequest", "onAddOtherDirectionToRequest", SystemActionPubExt.ECRMC_SYSTEM_ACTION_PUB_ADDON_NAME))

                .add("uniec_cloneChosenEntranceDiscipline", new SystemActionDefinition("uniec", "cloneChosenEntranceDiscipline", "onCloneChosenEntranceDiscipline", SystemActionPubExt.ECRMC_SYSTEM_ACTION_PUB_ADDON_NAME))


                .create();
    }
}
