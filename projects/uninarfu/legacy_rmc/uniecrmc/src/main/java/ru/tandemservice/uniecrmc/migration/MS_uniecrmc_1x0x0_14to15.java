package ru.tandemservice.uniecrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniecrmc_1x0x0_14to15 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.5.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.5.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность changeOriginalDocumentRED

        // создана новая сущность
        if (!tool.tableExists("changeoriginaldocumentred_t")) {
            // создать таблицу
            DBTable dbt = new DBTable("changeoriginaldocumentred_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("oldorigin_id", DBType.LONG).setNullable(false),
                                      new DBColumn("neworigin_id", DBType.LONG).setNullable(false),
                                      new DBColumn("entrantrequest_id", DBType.LONG).setNullable(false),
                                      new DBColumn("datechange_p", DBType.TIMESTAMP).setNullable(false),
                                      new DBColumn("comment_p", DBType.createVarchar(2000))
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("changeOriginalDocumentRED");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrollmentDirectionExt

        // создано свойство advansedPlanBudget
        if (!tool.columnExists("enrollmentdirectionext_t", "advansedplanbudget_p")) {
            // создать колонку
            tool.createColumn("enrollmentdirectionext_t", new DBColumn("advansedplanbudget_p", DBType.INTEGER));
        }


    }
}