package ru.tandemservice.uniecrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniecrmc_1x0x0_10to11 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.5.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.5.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность onlineEntrantExt

        // создана новая сущность
        if (!tool.tableExists("onlineentrantext_t")) {
            // создать таблицу
            DBTable dbt = new DBTable("onlineentrantext_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("entrant_id", DBType.LONG).setNullable(false),
                                      new DBColumn("snilsnumber_p", DBType.createVarchar(255)),
                                      new DBColumn("nextofkinrelationdegree_id", DBType.LONG),
                                      new DBColumn("nextofkinlastname_p", DBType.createVarchar(255)),
                                      new DBColumn("nextofkinfirstname_p", DBType.createVarchar(255)),
                                      new DBColumn("nextofkinmiddlename_p", DBType.createVarchar(255)),
                                      new DBColumn("nextofkinworkplace_p", DBType.createVarchar(255)),
                                      new DBColumn("nextofkinworkpost_p", DBType.createVarchar(255)),
                                      new DBColumn("nextofkinphone_p", DBType.createVarchar(255))
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("onlineEntrantExt");

        }


    }
}