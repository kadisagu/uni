package ru.tandemservice.uniecrmc.component.wizard.IdentityCardStep;

import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import ru.tandemservice.uniecrmc.component.wizard.WizardUtil;
import ru.tandemservice.uniecrmc.online.entity.OnlineEntrantExt;
import ru.tandemservice.uniec.component.wizard.IdentityCardStep.Model;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;

import java.util.List;

public class DAO extends ru.tandemservice.uniec.component.wizard.IdentityCardStep.DAO {
    @Override
    public void prepare(Model model) {
        super.prepare(model);

		/*
        OnlineEntrant online = model.getEntrantModel().getOnlineEntrant();
		Address ra = online.getRegistrationAddress();
		AddressDetailed na = AddressBaseUtils.convertFromOldFormat(model.getEntrantModel().getOnlineEntrant().getRegistrationAddress(), false);
		String title = na.getTitleWithFlat();
		*/

        IdentityCard ic = model.getIdentityCard() != null && model.getIdentityCard().getCardType() != null ? model.getIdentityCard() : model.getEntrantModel().getIdentityCard();

        List<IdentityCardType> list = WizardUtil.getIdentityCardTypeList();
        if (ic.getCardType() != null && !list.contains(ic.getCardType()))
            list.add(model.getIdentityCard().getCardType());
        model.getEntrantModel().setIdentityCardTypeList(list);
    }

    @Override
    public void createEntrant(Model model) {
        //введем факт адрес на нужном шаге
        if (!model.getAddressModel().isAddressEquals()) {
            model.getEntrantModel().getPerson().setAddress(null);
        }

        super.createEntrant(model);

        OnlineEntrant onlineEntrant = model.getEntrantModel().getOnlineEntrant();
        if (onlineEntrant != null) {
            OnlineEntrantExt onlineEntrantExt = get(OnlineEntrantExt.class, OnlineEntrantExt.entrant().id().s(), onlineEntrant.getId());
            if (onlineEntrantExt != null) {
                onlineEntrant.getEntrant().getPerson().setSnilsNumber(onlineEntrantExt.getSnilsNumber());
                getSession().saveOrUpdate(onlineEntrant);
            }
        }
    }
}
