package ru.tandemservice.uniecrmc.base.ext.EcPreEnroll.util;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.IEcgCortegeRMC;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.WrapChoseEntranseDiscipline;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.base.bo.EcPreEnroll.util.PreEntrantDTO;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EnrollmentRecommendation;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.List;

public class PreEntrantDTORMC extends PreEntrantDTO implements IEcgCortegeRMC
{

    private static final long serialVersionUID = 1L;
    private List<EnrollmentRecommendation> recommendations;
    private List<WrapChoseEntranseDiscipline> disciplines;
    private boolean haveSpecialRights;
    private Integer enrollmentPriority;

    public PreEntrantDTORMC(
            RequestedEnrollmentDirection requestedEnrollmentDirection,
            PreliminaryEnrollmentStudent preStudent, String finalMarkStr,
            Double finalMark, Double averageMark, String orderNumber,
            int preStudentCount,
            List<EnrollmentRecommendation> recommendations,
            List<WrapChoseEntranseDiscipline> disciplines,
            boolean haveSpecialRights, Integer enrollmentPriority)
    {
        super(requestedEnrollmentDirection, preStudent, finalMarkStr, finalMark,
              averageMark, orderNumber, preStudentCount);
        this.recommendations = recommendations;
        this.disciplines = disciplines;
        this.haveSpecialRights = haveSpecialRights;
        this.enrollmentPriority = enrollmentPriority;

    }

    public PreEntrantDTORMC(PreEntrantDTO preEntrantDTO,
                            List<EnrollmentRecommendation> recommendations,
                            List<WrapChoseEntranseDiscipline> disciplines,
                            boolean haveSpecialRights, Integer enrollmentPriority)
    {
        super(preEntrantDTO.getRequestedEnrollmentDirection(), preEntrantDTO.getPreStudent(), preEntrantDTO.getFinalMarkStr(), preEntrantDTO.getFinalMark(),
              preEntrantDTO.getAverageMark(), preEntrantDTO.getOrderNumber(), preEntrantDTO.getPreStudentCount());
        this.recommendations = recommendations;
        this.disciplines = disciplines;
        this.haveSpecialRights = haveSpecialRights;
        this.enrollmentPriority = enrollmentPriority;
    }

    public List<EnrollmentRecommendation> getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(List<EnrollmentRecommendation> recommendations) {
        this.recommendations = recommendations;
    }

    public List<WrapChoseEntranseDiscipline> getDisciplines() {
        return disciplines;
    }

    public void setDisciplines(List<WrapChoseEntranseDiscipline> disciplines) {
        this.disciplines = disciplines;
    }

    public boolean isHaveSpecialRights() {
        return haveSpecialRights;
    }

    public void setHaveSpecialRights(boolean haveSpecialRights) {
        this.haveSpecialRights = haveSpecialRights;
    }

    @Override
    public CompetitionKind getCompetitionKind() {
        return getRequestedEnrollmentDirection().getCompetitionKind();
    }

    @Override
    public IEntity getEntity() {
        return null;
    }

    @Override
    public TargetAdmissionKind getTargetAdmissionKind() {
        return getRequestedEnrollmentDirection().getTargetAdmissionKind();
    }

    @Override
    public boolean isTargetAdmission() {
        return getRequestedEnrollmentDirection().isTargetAdmission();
    }

    @Override
    public String getDisciplinesHTMLDescription() {
        if (this.getDisciplines() != null) {
            StringBuilder str = new StringBuilder("<table cellspacing='0' cellpadding='0'>");
            for (WrapChoseEntranseDiscipline discipline : this.getDisciplines()) {
                str.append("<tr>")
                        .append("<td>")
                                //.append(discipline.getEnrollmentCampaignDiscipline().getEducationSubject().getTitle())
                        .append(discipline.getTitle())

                        .append(" ")
                        .append(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(discipline.getFinalMark()))
                        .append("</td>")
                        .append("</tr>");
            }
            str.append("</table>");
            return str.toString();
        }
        return "";
    }

    @Override
    public String getCompetitionKindTitle() {
        return haveSpecialRights ? new StringBuilder("Вне конкурса").append("5".equals(this.getCompetitionKind().getCode()) ? "" : new StringBuilder().append(", ").append(this.getCompetitionKind().getTitle()).toString()).toString() : super.getCompetitionKindTitle();
    }

    public String getRecommendationTitle() {
        if (this.getRecommendations() != null) {
            List<String> lst = UniBaseUtils.getPropertiesList(this.getRecommendations(), EnrollmentRecommendation.shortTitle());
            return StringUtils.join(lst, ", ");
        }
        return "";
    }

    @Override
    public String getEntrantRequestsInfoHtml()
    {
        return "";
    }

    @Override
    public Integer getEnrollmentPriority() {
        return enrollmentPriority;
    }


}
