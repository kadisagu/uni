package ru.tandemservice.uniecrmc.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniecrmc.entity.entrant.Discipline2RealizationWayRelationExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Дисциплина набора вступительных испытаний (расширение)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class Discipline2RealizationWayRelationExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniecrmc.entity.entrant.Discipline2RealizationWayRelationExt";
    public static final String ENTITY_NAME = "discipline2RealizationWayRelationExt";
    public static final int VERSION_HASH = 1154598495;
    private static IEntityMeta ENTITY_META;

    public static final String L_DISCIPLINE2_REALIZATION_WAY_RELATION = "discipline2RealizationWayRelation";
    public static final String P_PASS_MARK_CONTRACT = "passMarkContract";

    private Discipline2RealizationWayRelation _discipline2RealizationWayRelation;     // Дисциплина набора вступительных испытаний
    private double _passMarkContract;     // Зачетный балл (по договору)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дисциплина набора вступительных испытаний. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public Discipline2RealizationWayRelation getDiscipline2RealizationWayRelation()
    {
        return _discipline2RealizationWayRelation;
    }

    /**
     * @param discipline2RealizationWayRelation Дисциплина набора вступительных испытаний. Свойство не может быть null и должно быть уникальным.
     */
    public void setDiscipline2RealizationWayRelation(Discipline2RealizationWayRelation discipline2RealizationWayRelation)
    {
        dirty(_discipline2RealizationWayRelation, discipline2RealizationWayRelation);
        _discipline2RealizationWayRelation = discipline2RealizationWayRelation;
    }

    /**
     * @return Зачетный балл (по договору). Свойство не может быть null.
     */
    @NotNull
    public double getPassMarkContract()
    {
        return _passMarkContract;
    }

    /**
     * @param passMarkContract Зачетный балл (по договору). Свойство не может быть null.
     */
    public void setPassMarkContract(double passMarkContract)
    {
        dirty(_passMarkContract, passMarkContract);
        _passMarkContract = passMarkContract;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof Discipline2RealizationWayRelationExtGen)
        {
            setDiscipline2RealizationWayRelation(((Discipline2RealizationWayRelationExt)another).getDiscipline2RealizationWayRelation());
            setPassMarkContract(((Discipline2RealizationWayRelationExt)another).getPassMarkContract());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends Discipline2RealizationWayRelationExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Discipline2RealizationWayRelationExt.class;
        }

        public T newInstance()
        {
            return (T) new Discipline2RealizationWayRelationExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "discipline2RealizationWayRelation":
                    return obj.getDiscipline2RealizationWayRelation();
                case "passMarkContract":
                    return obj.getPassMarkContract();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "discipline2RealizationWayRelation":
                    obj.setDiscipline2RealizationWayRelation((Discipline2RealizationWayRelation) value);
                    return;
                case "passMarkContract":
                    obj.setPassMarkContract((Double) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "discipline2RealizationWayRelation":
                        return true;
                case "passMarkContract":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "discipline2RealizationWayRelation":
                    return true;
                case "passMarkContract":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "discipline2RealizationWayRelation":
                    return Discipline2RealizationWayRelation.class;
                case "passMarkContract":
                    return Double.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Discipline2RealizationWayRelationExt> _dslPath = new Path<Discipline2RealizationWayRelationExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Discipline2RealizationWayRelationExt");
    }
            

    /**
     * @return Дисциплина набора вступительных испытаний. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniecrmc.entity.entrant.Discipline2RealizationWayRelationExt#getDiscipline2RealizationWayRelation()
     */
    public static Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation> discipline2RealizationWayRelation()
    {
        return _dslPath.discipline2RealizationWayRelation();
    }

    /**
     * @return Зачетный балл (по договору). Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.Discipline2RealizationWayRelationExt#getPassMarkContract()
     */
    public static PropertyPath<Double> passMarkContract()
    {
        return _dslPath.passMarkContract();
    }

    public static class Path<E extends Discipline2RealizationWayRelationExt> extends EntityPath<E>
    {
        private Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation> _discipline2RealizationWayRelation;
        private PropertyPath<Double> _passMarkContract;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дисциплина набора вступительных испытаний. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniecrmc.entity.entrant.Discipline2RealizationWayRelationExt#getDiscipline2RealizationWayRelation()
     */
        public Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation> discipline2RealizationWayRelation()
        {
            if(_discipline2RealizationWayRelation == null )
                _discipline2RealizationWayRelation = new Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation>(L_DISCIPLINE2_REALIZATION_WAY_RELATION, this);
            return _discipline2RealizationWayRelation;
        }

    /**
     * @return Зачетный балл (по договору). Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.Discipline2RealizationWayRelationExt#getPassMarkContract()
     */
        public PropertyPath<Double> passMarkContract()
        {
            if(_passMarkContract == null )
                _passMarkContract = new PropertyPath<Double>(Discipline2RealizationWayRelationExtGen.P_PASS_MARK_CONTRACT, this);
            return _passMarkContract;
        }

        public Class getEntityClass()
        {
            return Discipline2RealizationWayRelationExt.class;
        }

        public String getEntityName()
        {
            return "discipline2RealizationWayRelationExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
