package ru.tandemservice.uniecrmc.base.ext.EcProfileDistribution.ui.Pub;

import ru.tandemservice.tools.EnumTools;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.EcDistributionUtil;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.IEcgCortegeRMC;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgEntrantRateRowDTO;
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.ui.EntrantAdd.EcProfileDistributionEntrantAdd;
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.ui.Pub.EcProfileDistributionPub;

public class EcProfileDistributionPubUI extends ru.tandemservice.uniec.base.bo.EcProfileDistribution.ui.Pub.EcProfileDistributionPubUI {

    public static final String RULE_WITHOUT_ENTRANCE_DISCIPLINES = "RULE_WITHOUT_ENTRANCE_DISCIPLINES";
    public static final String RULE_TARGET_ADMISSION = "RULE_TARGET_ADMISSION";
    public static final String RULE_NON_COMPETITIVE = "RULE_NON_COMPETITIVE";
    public static final String RULE_COMPETITIVE = "RULE_COMPETITIVE";

    private boolean usePriority;

    @Override
    public void onComponentRefresh() {
        super.onComponentRefresh();

        setUsePriority(EcDistributionUtil.isUsePriority(getDistribution().getConfig().getEcgItem().getEnrollmentCampaign()));

        if (!isUsePriority())
            return;

        EnumTools.addEnum(IEcgEntrantRateRowDTO.Rule.class, EcProfileDistributionPubUI.RULE_WITHOUT_ENTRANCE_DISCIPLINES);

    }

    public String getDisciplines() {
        return ((IEcgCortegeRMC) getConfig().getDataSource(EcProfileDistributionPub.RECOMMENDED_DS).getCurrent()).getDisciplinesHTMLDescription();
    }

    public boolean isUseProfileMark() {
        return !isUsePriority();
    }

    @Override
    public void onClickAddByOutOfCompetition() {

        if (!isUsePriority()) {
            super.onClickAddByOutOfCompetition();
            return;
        }

        this._uiSupport.doRefresh();

        this._uiActivation.asRegion(EcProfileDistributionEntrantAdd.class)
                .parameter("distribution", this.getDistribution())
                .parameter("rateRule", IEcgEntrantRateRowDTO.Rule.valueOf(RULE_WITHOUT_ENTRANCE_DISCIPLINES))
                .activate();
    }

    public void onClickAddBeyondCompetition() {
        this._uiSupport.doRefresh();
        this._uiActivation.asRegion(EcProfileDistributionEntrantAdd.class)
                .parameter("distribution", this.getDistribution())
                .parameter("rateRule", IEcgEntrantRateRowDTO.Rule.RULE_NON_COMPETITIVE)
                .activate();
    }

    public boolean isUsePriority() {
        return usePriority;
    }

    public void setUsePriority(boolean usePriority) {
        this.usePriority = usePriority;
    }

}
