package ru.tandemservice.uniecrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniecrmc_1x0x0_9to10 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.5.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.5.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        if (!tool.tableExists("entrantrecommendedsettings_t") || tool.columnExists("entrantrecommendedsettings_t", "sstrctrlvlinrstrctn_p"))
            return;

        // создать колонку
        tool.createColumn("entrantrecommendedsettings_t", new DBColumn("sstrctrlvlinrstrctn_p", DBType.BOOLEAN));
        // задать значение по умолчанию
        java.lang.Boolean defaultUseStructureLevelInRestriction = false;
        tool.executeUpdate("update entrantrecommendedsettings_t set sstrctrlvlinrstrctn_p=? where sstrctrlvlinrstrctn_p is null", defaultUseStructureLevelInRestriction);
        // сделать колонку NOT NULL
        tool.setColumnNullable("entrantrecommendedsettings_t", "sstrctrlvlinrstrctn_p", false);
    }
}