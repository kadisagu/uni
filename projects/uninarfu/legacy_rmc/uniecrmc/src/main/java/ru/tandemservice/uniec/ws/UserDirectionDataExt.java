package ru.tandemservice.uniec.ws;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "UserDirectionData")
public class UserDirectionDataExt
        extends ru.tandemservice.uniec.ws.UserDirectionData
{
    /**
     * Приоритет выбранного направления подготовки
     */
    public Integer order;

    public void fillByBase(UserDirectionData udBase)
    {
        enrollmentDirectionId = udBase.enrollmentDirectionId;
        compensationTypeId = udBase.compensationTypeId;
        studentCategoryId = udBase.studentCategoryId;
        competitionKindId = udBase.competitionKindId;
        targetAdmission = udBase.targetAdmission;
        profileWorkExperienceYears = udBase.profileWorkExperienceYears;
        profileWorkExperienceMonths = udBase.profileWorkExperienceMonths;
        profileWorkExperienceDays = udBase.profileWorkExperienceDays;
    }
}
