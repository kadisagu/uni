package ru.tandemservice.uniecrmc.component.entrant.RequestedEnrollmentDirectionAddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniecrmc.util.UniecrmcUtil;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.orgstruct.Group;

public class Model extends ru.tandemservice.uniec.component.entrant.RequestedEnrollmentDirectionAddEdit.Model {

    private ISelectModel courseModel;
    private ISelectModel termModel;
    private ISelectModel groupModel;
    private Course course;
    private Term term;
    private Group group;

    public boolean isUseGroup() {
        return UniecrmcUtil.isUseGroup(this.getEntrantRequest().getEntrant().getEnrollmentCampaign());
    }

    public ISelectModel getCourseModel() {
        return courseModel;
    }

    public void setCourseModel(ISelectModel courseModel) {
        this.courseModel = courseModel;
    }

    public ISelectModel getTermModel() {
        return termModel;
    }

    public void setTermModel(ISelectModel termModel) {
        this.termModel = termModel;
    }

    public ISelectModel getGroupModel() {
        return groupModel;
    }

    public void setGroupModel(ISelectModel groupModel) {
        this.groupModel = groupModel;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Term getTerm() {
        return term;
    }

    public void setTerm(Term term) {
        this.term = term;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

}
