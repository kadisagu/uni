package ru.tandemservice.uniecrmc.dao.entrantRating;

import ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionExt;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.Arrays;
import java.util.List;

public class EnrollmentDirectionNode {

    private final static List<String> HIGH_LEVEL = Arrays.asList(new String[]{"26", "1", "unieducationrmc.1"});
    private final static List<String> MIDDLE_LEVEL = Arrays.asList(new String[]{"34", "11", "unieducationrmc.9"});
    private final static List<String> ASPIRANT_LEVEL = Arrays.asList(new String[]{"38"});

    @XmlAttribute(name = "id")
    public Long id;

    @XmlAttribute
    public String eduLevelId;

    @XmlAttribute
    public String qualificationCode;

    @XmlAttribute(name = "fullTitle")
    public String fullTitle;

    @XmlAttribute(required = false)
    public String okso;

    @XmlAttribute(required = false)
    public String title;

    @XmlAttribute(required = false)
    public String shortTitle;

    @XmlAttribute
    public String developForm;

    @XmlAttribute
    public String developPeriod;

    @XmlAttribute
    public String developTech;

    @XmlAttribute
    public String developCondition;

    @XmlAttribute
    public Long orgUnitId;

    @XmlAttribute
    public String orgUnitShortTitle;

    @XmlAttribute
    public String orgUnitTitle;


    @XmlElement(name = "Disciplines", required = false)
    public DisciplinesNode disciplinesNode;

    @XmlElement(name = "List", required = false)
    public List<ListNode> lists;

    @XmlElement(name = "Plan", required = false)
    public PlanNode plan;

    @XmlElement(name = "Statistic", required = false)
    public StatisticNode statistic;

    public EnrollmentDirectionNode() {
    }

    public EnrollmentDirectionNode(EnrollmentDirection enrollmentDirection, EnrollmentDirectionExt enrollmentDirectionExt) {
        this.id = enrollmentDirection.getId();
        this.fullTitle = enrollmentDirection.getTitle();

        if (enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject() != null)
            this.okso = enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject().getSubjectCode();
        if (enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject() != null)
            this.title = enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject().getTitle();

        if (enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().getShortTitle() != null)
            this.shortTitle = enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().getShortTitle();

        this.developForm = enrollmentDirection.getEducationOrgUnit().getDevelopForm().getCode();
        this.developPeriod = enrollmentDirection.getEducationOrgUnit().getDevelopPeriod().getCode();
        this.developTech = enrollmentDirection.getEducationOrgUnit().getDevelopTech().getCode();
        this.developCondition = enrollmentDirection.getEducationOrgUnit().getDevelopCondition().getCode();

        this.orgUnitId = enrollmentDirection.getEducationOrgUnit().getFormativeOrgUnit().getId();
        this.orgUnitShortTitle = enrollmentDirection.getEducationOrgUnit().getFormativeOrgUnit().getShortTitle();
        this.orgUnitTitle = enrollmentDirection.getEducationOrgUnit().getFormativeOrgUnit().getTitle();
        StructureEducationLevels level = enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType();
        while (level.getParent() != null) {
            level = level.getParent();
        }
        if (ASPIRANT_LEVEL.contains(level.getCode()))
            eduLevelId = "3";
        if (HIGH_LEVEL.contains(level.getCode()))
            eduLevelId = "1";
        if (MIDDLE_LEVEL.contains(level.getCode()))
            eduLevelId = "2";

        this.qualificationCode = enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getQualification().getCode();
    }

}
