package ru.tandemservice.uniecrmc.online.dao;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;
import ru.tandemservice.uniec.ws.EntrantDataExt;
import ru.tandemservice.uniec.ws.ProfilePriorityData;
import ru.tandemservice.uniec.ws.UserDirectionDataExt;

import java.util.List;

public interface IOnlineEntrantRegistrationDAO extends ru.tandemservice.uniec.dao.IOnlineEntrantRegistrationDAO {

    public OnlineEntrant createOrUpdateOnlineEntrant(long userId, EntrantDataExt entrantDataExt);

    public EntrantDataExt getEntrantDataExt(long userId);

    /**
     * Определяем приемку, по которой работают онлайны
     *
     * @return
     */
    EnrollmentCampaign getEnrollmentCampaignForOnline();

    public List<UserDirectionDataExt> getUserDirectionsExt(long userId);

    @Transactional(propagation = Propagation.REQUIRED)
    public void synchronizeUserDirections(long userId, UserDirectionDataExt[] userDirections);

    public List<ProfilePriorityData> getProfilePriorities(long userId, List<UserDirectionDataExt> userDirections);

    public void synchronizeUserProfilePriorities(long userId, List<ProfilePriorityData> priorityList);
}
