package ru.tandemservice.uniecrmc.component.report.EnrollmentResultByCategory.Pub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.shared.commonbase.base.util.UniMap;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = getModel(component);
        getDao().prepare(model);
    }

    public void onClickPrint(IBusinessComponent component) {
        activateInRoot(component, new ComponentActivator("ru.tandemservice.uni.component.reports.DownloadStorableReport", new UniMap().add("reportId", getModel(component).getReport().getId()).add("extension", "xls").add("zip", false)));
    }
}
