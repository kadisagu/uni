package ru.tandemservice.uniecrmc.component.systemaction.AddOtherDirectionToRequest;

import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.ArrayList;
import java.util.List;

public class WrapAddDirection {
    private CompetitionGroup competitionGroup;
    private RequestedEnrollmentDirection requestedEnrollmentDirection;
    private List<EnrollmentDirection> lstToAdd = new ArrayList<>();

    public WrapAddDirection(CompetitionGroup competitionGroup)
    {
        this.competitionGroup = competitionGroup;
    }

    public CompetitionGroup getCompetitionGroup() {
        return competitionGroup;
    }

    public RequestedEnrollmentDirection getRequestedEnrollmentDirection() {
        return requestedEnrollmentDirection;
    }

    public void setRequestedEnrollmentDirection(
            RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        this.requestedEnrollmentDirection = requestedEnrollmentDirection;
    }

    public List<EnrollmentDirection> getLstToAdd() {
        return lstToAdd;
    }

    public void setLstToAdd(List<EnrollmentDirection> lstToAdd) {
        this.lstToAdd = lstToAdd;
    }

    public void AddEnrollmentDirection(EnrollmentDirection ed)
    {
        if (!this.lstToAdd.contains(ed))
            this.lstToAdd.add(ed);

    }

    @Override
    public int hashCode()
    {
        return this.getCompetitionGroup().hashCode();
    }

    ;

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof WrapAddDirection) {
            return this.getCompetitionGroup().equals(((WrapAddDirection) obj).getCompetitionGroup());
        }
        else
            return super.equals(obj);
    }

}
