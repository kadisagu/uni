package ru.tandemservice.uniec.ws;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "EntrantDataExt")
public class EntrantDataExt extends ru.tandemservice.uniec.ws.EntrantData {

    private String snilsNumber;
    private Long kinRelationDegreeId;
    private String kinLastName;
    private String kinFirstName;
    private String kinMiddleName;
    private String kinWorkPlace;
    private String kinWorkPost;
    private String kinPhone;
    private Long individualAchievementsId;
    private String individualAchievementsDesc;
    private Long individualAchievementsId2;
    private String individualAchievementsDesc2;
    private Long individualAchievementsId3;
    private String individualAchievementsDesc3;

    public Long getIndividualAchievementsId() {
        return individualAchievementsId;
    }

    public void setIndividualAchievementsId(Long individualAchievementsId) {
        this.individualAchievementsId = individualAchievementsId;
    }

    public String getIndividualAchievementsDesc() {
        return individualAchievementsDesc;
    }

    public void setIndividualAchievementsDesc(String individualAchievementsDesc) {
        this.individualAchievementsDesc = individualAchievementsDesc;
    }

    public Long getIndividualAchievementsId2() {
        return individualAchievementsId2;
    }

    public void setIndividualAchievementsId2(Long individualAchievementsId2) {
        this.individualAchievementsId2 = individualAchievementsId2;
    }

    public String getIndividualAchievementsDesc2() {
        return individualAchievementsDesc2;
    }

    public void setIndividualAchievementsDesc2(String individualAchievementsDesc2) {
        this.individualAchievementsDesc2 = individualAchievementsDesc2;
    }

    public Long getIndividualAchievementsId3() {
        return individualAchievementsId3;
    }

    public void setIndividualAchievementsId3(Long individualAchievementsId3) {
        this.individualAchievementsId3 = individualAchievementsId3;
    }

    public String getIndividualAchievementsDesc3() {
        return individualAchievementsDesc3;
    }

    public void setIndividualAchievementsDesc3(String individualAchievementsDesc3) {
        this.individualAchievementsDesc3 = individualAchievementsDesc3;
    }

    private String description;

    public String getSnilsNumber() {
        return snilsNumber;
    }

    public void setSnilsNumber(String snilsNumber) {
        this.snilsNumber = snilsNumber;
    }

    public Long getKinRelationDegreeId() {
        return kinRelationDegreeId;
    }

    public void setKinRelationDegreeId(Long kinRelationDegreeId) {
        this.kinRelationDegreeId = kinRelationDegreeId;
    }

    public String getKinLastName() {
        return kinLastName;
    }

    public void setKinLastName(String kinLastName) {
        this.kinLastName = kinLastName;
    }

    public String getKinFirstName() {
        return kinFirstName;
    }

    public void setKinFirstName(String kinFirstName) {
        this.kinFirstName = kinFirstName;
    }

    public String getKinMiddleName() {
        return kinMiddleName;
    }

    public void setKinMiddleName(String kinMiddleName) {
        this.kinMiddleName = kinMiddleName;
    }

    public String getKinWorkPlace() {
        return kinWorkPlace;
    }

    public void setKinWorkPlace(String kinWorkPlace) {
        this.kinWorkPlace = kinWorkPlace;
    }

    public String getKinWorkPost() {
        return kinWorkPost;
    }

    public void setKinWorkPost(String kinWorkPost) {
        this.kinWorkPost = kinWorkPost;
    }

    public String getKinPhone() {
        return kinPhone;
    }

    public void setKinPhone(String kinPhone) {
        this.kinPhone = kinPhone;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
