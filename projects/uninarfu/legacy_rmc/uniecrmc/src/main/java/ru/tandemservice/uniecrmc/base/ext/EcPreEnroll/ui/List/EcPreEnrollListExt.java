package ru.tandemservice.uniecrmc.base.ext.EcPreEnroll.ui.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtension;
import org.tandemframework.caf.ui.config.datasource.IColumnListExtensionBuilder;
import ru.tandemservice.uniec.base.bo.EcPreEnroll.ui.List.EcPreEnrollList;

@Configuration
public class EcPreEnrollListExt extends BusinessComponentExtensionManager {

    @Autowired
    private EcPreEnrollList parent;

    @Bean
    public ColumnListExtension extensionPreEntrantDS()
    {
        IColumnListExtensionBuilder c = columnListExtensionBuilder(parent.preEntrantDS());

        c.addColumn(checkboxColumn("selected").controlInHeader(true).before("regNumber"));
        // c.addColumn( blockColumn("checkbox", "checkboxBlockColumn").hasBlockHeader(true).create());

        //c.addAllAfter("finalMarkStr");
        c.addColumn(blockColumn("disciplines", "disciplinesBlockColumn").visible("ui:usePriority").after("finalMarkStr").create());
        c.addColumn(textColumn("recommendation", "recommendationTitle").visible("ui:usePriority").after("disciplines").create());
        return c.create();
    }
}
