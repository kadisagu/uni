package ru.tandemservice.uniecrmc.component.wizard.EduInstitutionStep;

import org.apache.tapestry.form.validator.BaseValidator;
import org.apache.tapestry.form.validator.Required;
import ru.tandemservice.uniecrmc.component.wizard.WizardUtil;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Model extends ru.tandemservice.uniec.component.wizard.EduInstitutionStep.Model {
    public static final String PARAM_SERIA = "entrant.wizard.EduInstitutionStep.field.seria";
    public static final String PARAM_NUMBER = "entrant.wizard.EduInstitutionStep.field.number";
    public static final String PARAM_DATE = "entrant.wizard.EduInstitutionStep.field.date";
    public static final String PARAM_MARK = "entrant.wizard.EduInstitutionStep.field.marks";

    public List<? extends BaseValidator> getMarkValidators() {
        if (WizardUtil.isRequired(PARAM_MARK))
            return Arrays.asList(new Required());
        else
            return Collections.emptyList();
    }

    @Override
    public List<BaseValidator> getSeriaValidators() {
        List<BaseValidator> list = super.getSeriaValidators();
        if (WizardUtil.isRequired(PARAM_SERIA))
            list.add(new Required());

        return list;
    }

    @Override
    public List<BaseValidator> getNumberValidators() {
        List<BaseValidator> list = super.getNumberValidators();
        if (WizardUtil.isRequired(PARAM_NUMBER))
            list.add(new Required());

        return list;
    }

    public List<? extends BaseValidator> getDateValidators() {
        if (WizardUtil.isRequired(PARAM_DATE))
            return Arrays.asList(new Required());
        else
            return Collections.emptyList();
    }
}
