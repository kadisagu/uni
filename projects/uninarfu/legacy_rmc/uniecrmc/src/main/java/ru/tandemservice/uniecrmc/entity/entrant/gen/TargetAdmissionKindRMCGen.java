package ru.tandemservice.uniecrmc.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniecrmc.entity.entrant.TargetAdmissionKindRMC;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение вид целевого приема
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TargetAdmissionKindRMCGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniecrmc.entity.entrant.TargetAdmissionKindRMC";
    public static final String ENTITY_NAME = "targetAdmissionKindRMC";
    public static final int VERSION_HASH = 1584256271;
    private static IEntityMeta ENTITY_META;

    public static final String L_BASE = "base";
    public static final String P_CONTRACT_NUMBER = "contractNumber";

    private TargetAdmissionKind _base;     // Вид целевого приема
    private String _contractNumber;     // № договора

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Вид целевого приема. Свойство не может быть null.
     */
    @NotNull
    public TargetAdmissionKind getBase()
    {
        return _base;
    }

    /**
     * @param base Вид целевого приема. Свойство не может быть null.
     */
    public void setBase(TargetAdmissionKind base)
    {
        dirty(_base, base);
        _base = base;
    }

    /**
     * @return № договора.
     */
    @Length(max=255)
    public String getContractNumber()
    {
        return _contractNumber;
    }

    /**
     * @param contractNumber № договора.
     */
    public void setContractNumber(String contractNumber)
    {
        dirty(_contractNumber, contractNumber);
        _contractNumber = contractNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof TargetAdmissionKindRMCGen)
        {
            setBase(((TargetAdmissionKindRMC)another).getBase());
            setContractNumber(((TargetAdmissionKindRMC)another).getContractNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TargetAdmissionKindRMCGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TargetAdmissionKindRMC.class;
        }

        public T newInstance()
        {
            return (T) new TargetAdmissionKindRMC();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "base":
                    return obj.getBase();
                case "contractNumber":
                    return obj.getContractNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "base":
                    obj.setBase((TargetAdmissionKind) value);
                    return;
                case "contractNumber":
                    obj.setContractNumber((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "base":
                        return true;
                case "contractNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "base":
                    return true;
                case "contractNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "base":
                    return TargetAdmissionKind.class;
                case "contractNumber":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TargetAdmissionKindRMC> _dslPath = new Path<TargetAdmissionKindRMC>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TargetAdmissionKindRMC");
    }
            

    /**
     * @return Вид целевого приема. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.TargetAdmissionKindRMC#getBase()
     */
    public static TargetAdmissionKind.Path<TargetAdmissionKind> base()
    {
        return _dslPath.base();
    }

    /**
     * @return № договора.
     * @see ru.tandemservice.uniecrmc.entity.entrant.TargetAdmissionKindRMC#getContractNumber()
     */
    public static PropertyPath<String> contractNumber()
    {
        return _dslPath.contractNumber();
    }

    public static class Path<E extends TargetAdmissionKindRMC> extends EntityPath<E>
    {
        private TargetAdmissionKind.Path<TargetAdmissionKind> _base;
        private PropertyPath<String> _contractNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Вид целевого приема. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.TargetAdmissionKindRMC#getBase()
     */
        public TargetAdmissionKind.Path<TargetAdmissionKind> base()
        {
            if(_base == null )
                _base = new TargetAdmissionKind.Path<TargetAdmissionKind>(L_BASE, this);
            return _base;
        }

    /**
     * @return № договора.
     * @see ru.tandemservice.uniecrmc.entity.entrant.TargetAdmissionKindRMC#getContractNumber()
     */
        public PropertyPath<String> contractNumber()
        {
            if(_contractNumber == null )
                _contractNumber = new PropertyPath<String>(TargetAdmissionKindRMCGen.P_CONTRACT_NUMBER, this);
            return _contractNumber;
        }

        public Class getEntityClass()
        {
            return TargetAdmissionKindRMC.class;
        }

        public String getEntityName()
        {
            return "targetAdmissionKindRMC";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
