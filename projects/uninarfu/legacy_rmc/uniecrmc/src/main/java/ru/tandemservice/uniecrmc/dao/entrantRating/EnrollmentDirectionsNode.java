package ru.tandemservice.uniecrmc.dao.entrantRating;

import ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionExt;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

public class EnrollmentDirectionsNode {

    @XmlElement(name = "EnrollmentDirection")
    List<EnrollmentDirectionNode> list = new ArrayList<>();

    public EnrollmentDirectionNode add(EnrollmentDirection enrollmentDirection, EnrollmentDirectionExt enrollmentDirectionExt) {
        EnrollmentDirectionNode node = new EnrollmentDirectionNode(enrollmentDirection, enrollmentDirectionExt);
        list.add(node);

        return node;
    }
}
