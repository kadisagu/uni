package ru.tandemservice.uniecrmc.ws.dao;

import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EntrantState;

import java.util.ArrayList;
import java.util.List;

public class WrapperEnrolmentDirectionStatistic {
    public int MinisterialPlan;
    public int ContractPlan;
    public int TargetAdmissionPlanBudget;
    public int TargetAdmissionPlanContract;

    public List<EnrolmentDirectionRow> rowStatistic = new ArrayList<EnrolmentDirectionRow>();

    public static class EnrolmentDirectionRow {
        public CompensationType compensationType;
        public CompetitionKind competitionKind;
        public EntrantState entrantState;
        public int count;
        public boolean IsTarget;

    }
}
