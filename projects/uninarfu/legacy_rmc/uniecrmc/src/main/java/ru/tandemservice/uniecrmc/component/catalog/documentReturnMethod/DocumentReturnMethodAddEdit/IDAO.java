package ru.tandemservice.uniecrmc.component.catalog.documentReturnMethod.DocumentReturnMethodAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.uniecrmc.entity.catalog.DocumentReturnMethod;

public interface IDAO extends IDefaultCatalogAddEditDAO<DocumentReturnMethod, Model> {
}
