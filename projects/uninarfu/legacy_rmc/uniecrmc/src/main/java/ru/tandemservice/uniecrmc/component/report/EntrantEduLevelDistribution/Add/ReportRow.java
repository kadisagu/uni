package ru.tandemservice.uniecrmc.component.report.EntrantEduLevelDistribution.Add;

import ru.tandemservice.uni.entity.catalog.CompensationType;

public class ReportRow {
    private String title;
    private boolean needNumber;
    private String number;
    private String code;
    private int totalRequests;
    private int acceptedRequests;
    private int acceptedBudgetRequests;
    private int acceptedContractRequests;

    public ReportRow() {
    }

    public ReportRow(String title, String code) {
        this.title = title;
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isNeedNumber() {
        return needNumber;
    }

    public void setNeedNumber(boolean needNumber) {
        this.needNumber = needNumber;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getTotalRequests() {
        return totalRequests;
    }

    public void setTotalRequests(int totalRequests) {
        this.totalRequests = totalRequests;
    }

    public int getAcceptedRequests() {
        return acceptedRequests;
    }

    public void setAcceptedRequests(int acceptedRequests) {
        this.acceptedRequests = acceptedRequests;
    }

    public int getAcceptedBudgetRequests() {
        return acceptedBudgetRequests;
    }

    public void setAcceptedBudgetRequests(int acceptedBudgetRequests) {
        this.acceptedBudgetRequests = acceptedBudgetRequests;
    }

    public int getAcceptedContractRequests() {
        return acceptedContractRequests;
    }

    public void setAcceptedContractRequests(int acceptedContractRequests) {
        this.acceptedContractRequests = acceptedContractRequests;
    }

    public void increaseTotal()
    {
        this.totalRequests++;

    }

    public void increaseEnrolled(CompensationType compensationType)
    {
        this.acceptedRequests++;
        if (compensationType.isBudget())
            this.acceptedBudgetRequests++;
        else
            this.acceptedContractRequests++;
    }
}
