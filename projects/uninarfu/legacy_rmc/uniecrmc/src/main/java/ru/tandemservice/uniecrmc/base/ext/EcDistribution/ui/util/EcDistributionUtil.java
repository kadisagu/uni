package ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util;

import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniecrmc.entity.entrant.EntranceDisciplineExt;
import ru.tandemservice.uniecrmc.entity.entrant.EntrantExt;
import ru.tandemservice.uniecrmc.entity.entrant.EntrantRecommendedSettings;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniec.entity.catalog.EnrollmentRecommendation;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.Group2DisciplineRelation;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

public class EcDistributionUtil {

    public static enum Rule {
        RULE_TARGET_ADMISSION,
        RULE_WITHOUT_ENTRANCE_DISCIPLINES,
        RULE_NON_COMPETITIVE,
        RULE_COMPETITIVE,
        RULE_COMPETITIVE_WITH_ORIGINAL;
    }

    /**
     * Вызывает private метод
     *
     * @param o
     * @param methodName
     * @param params
     *
     * @return
     */
    public static Object invokePrivateMethod(Object o, String methodName, Object[] params) {
        final Method methods[] = o.getClass().getDeclaredMethods();
        for (int i = 0; i < methods.length; ++i) {
            if (methodName.equals(methods[i].getName())) {
                try {
                    methods[i].setAccessible(true);
                    return methods[i].invoke(o, params);
                }
                catch (IllegalAccessException ex) {
                    // пока по простому
                    return null;
                }
                catch (InvocationTargetException ite) {
                    // пока по простому
                    return null;
                }
            }
        }
        return null;
    }

    @SuppressWarnings("rawtypes")
    public static Object invokePrivateMethod(Object o, Class cls, String methodName, Object[] params) {


        final Method methods[] = cls.getDeclaredMethods();

        for (int i = 0; i < methods.length; ++i) {
            if (methodName.equals(methods[i].getName())) {
                try {
                    methods[i].setAccessible(true);
                    return methods[i].invoke(o, params);
                }
                catch (IllegalAccessException ex) {
                    // пока по простому
                    return null;
                }
                catch (InvocationTargetException ite) {
                    // пока по простому
                    return null;
                }
            }
        }
        return null;
    }

    public static Object getPrivateField(Object o, String fieldName)
    {
        final Field fields[] = o.getClass().getDeclaredFields();
        for (int i = 0; i < fields.length; ++i) {
            if (fieldName.equals(fields[i].getName())) {
                try {
                    fields[i].setAccessible(true);
                    return fields[i].get(o);
                }
                catch (IllegalAccessException ex) {
                    // пока по простому
                    return null;
                }
            }
        }
        return null;
    }

    public static boolean setPrivateField(Object o, String fieldName, Object value)
    {
        final Field fields[] = o.getClass().getDeclaredFields();
        for (int i = 0; i < fields.length; ++i) {
            if (fieldName.equals(fields[i].getName())) {
                try {
                    fields[i].setAccessible(true);
                    fields[i].set(o, value);
                    return true;

                }
                catch (IllegalAccessException ex) {
                    ex.printStackTrace();
                    return false;
                }
            }
        }
        return false;
    }

    public static boolean isUsePriority(EnrollmentCampaign campaign) {
        EntrantRecommendedSettings settings = UniDaoFacade.getCoreDao().get(EntrantRecommendedSettings.class, EntrantRecommendedSettings.enrollmentCampaign(), campaign);
        return settings != null && settings.isUsePriorityEntranceDiscipline();
    }

    public static Map<Long, List<EnrollmentRecommendation>> getRecommendationMap(Collection<Long> entrantIds) {

        final Map<Long, List<EnrollmentRecommendation>> recommendMap = new HashMap<Long, List<EnrollmentRecommendation>>();

        BatchUtils.execute(entrantIds, 1000, new BatchUtils.Action<Long>() {
            @Override
            public void execute(Collection<Long> elements) {
                DQLSelectBuilder recommendBuilder = new DQLSelectBuilder().fromEntity(EntrantEnrolmentRecommendation.class, "e")
                        .where(DQLExpressions.in(DQLExpressions.property(EntrantEnrolmentRecommendation.entrant().id().fromAlias("e")), elements));
                List<EntrantEnrolmentRecommendation> recommendations = UniDaoFacade.getCoreDao().getList(recommendBuilder);

                for (EntrantEnrolmentRecommendation recommendation : recommendations)
                    SafeMap.safeGet(recommendMap, recommendation.getEntrant().getId(), ArrayList.class).add(recommendation.getRecommendation());
            }
        });

        return recommendMap;
    }

    public static Map<Long, Integer> getEnrollmentPriorityMap(Collection<Long> entrantIds) {
        final Map<Long, Integer> map = new HashMap<Long, Integer>();

        BatchUtils.execute(entrantIds, 1000, new BatchUtils.Action<Long>() {
            @Override
            public void execute(Collection<Long> elements) {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EntrantExt.class, "e")
                        .where(DQLExpressions.in(DQLExpressions.property(EntrantExt.entrant().id().fromAlias("e")), elements));
                List<EntrantExt> list = UniDaoFacade.getCoreDao().getList(builder);

                for (EntrantExt ext : list)
                    if (ext.getEnrollmentPriority() != null)
                        map.put(ext.getEntrant().getId(), ext.getEnrollmentPriority());
            }
        });

        return map;
    }

    public static String LOCK = "sss";

    public static Map<Long, List<ChosenEntranceDiscipline>> getChosenDisciplineMapNew(Collection<Long> directionIds) {

        synchronized (LOCK) {
            final Map<Long, List<ChosenEntranceDiscipline>> edDiscMap = new HashMap<Long, List<ChosenEntranceDiscipline>>();

            if (true)
                return edDiscMap;

            BatchUtils.execute(directionIds, 1000, new BatchUtils.Action<Long>() {
                @Override
                public void execute(Collection<Long> elements)
                {

                    DQLSelectBuilder edBuilder = new DQLSelectBuilder().fromEntity(ChosenEntranceDiscipline.class, "ced")
                            .joinPath(DQLJoinType.inner, ChosenEntranceDiscipline.enrollmentCampaignDiscipline().fromAlias("ced"), "ecd")
                            .joinPath(DQLJoinType.inner, ChosenEntranceDiscipline.chosenEnrollmentDirection().fromAlias("ced"), "red")

                            .joinEntity("red", DQLJoinType.inner, EnrollmentDirection.class, "edir",
                                        DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().id().fromAlias("red")),
                                                          DQLExpressions.property(EnrollmentDirection.id().fromAlias("edir"))
                                        )
                            )
                            .where(DQLExpressions.in(DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("ced")), elements))


                            .joinEntity("ecd", DQLJoinType.inner, EntranceDiscipline.class, "ed",
                                        DQLExpressions.and
                                                (
                                                        DQLExpressions.eq
                                                                (
                                                                        DQLExpressions.property(EnrollmentDirection.id().fromAlias("edir")),
                                                                        DQLExpressions.property(EntranceDiscipline.enrollmentDirection().id().fromAlias("ed"))
                                                                ),

                                                        DQLExpressions.eq
                                                                (
                                                                        DQLExpressions.property(ChosenEntranceDiscipline.enrollmentCampaignDiscipline().id().fromAlias("ced")),
                                                                        DQLExpressions.property(EntranceDiscipline.discipline().id().fromAlias("ed"))
                                                                )
                                                )
                            )
                            .joinEntity("ed", DQLJoinType.inner, EntranceDisciplineExt.class, "ext",
                                        DQLExpressions.eq
                                                (
                                                        DQLExpressions.property(EntranceDiscipline.id().fromAlias("ed")),
                                                        DQLExpressions.property(EntranceDisciplineExt.entranceDiscipline().id().fromAlias("ext"))
                                                )
                            )
                            .column(DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("ced")))
                            .column("ced")
                            .order(DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("ced")))
                            .order(DQLExpressions.property(EntranceDisciplineExt.priority().fromAlias("ext")));

                    List<Object[]> rows = UniDaoFacade.getCoreDao().getList(edBuilder);

                    for (Object[] objects : rows) {
                        Long redId = (Long) objects[0];
                        ChosenEntranceDiscipline disc = (ChosenEntranceDiscipline) objects[1];
                        List<ChosenEntranceDiscipline> list = edDiscMap.get(redId);
                        if (list == null)
                            edDiscMap.put(redId, list = new ArrayList<ChosenEntranceDiscipline>());

                        if (!list.contains(disc))
                            list.add(disc);
                    }
                }
            });

            return edDiscMap;
        }
    }

    public static Map<Long, List<WrapChoseEntranseDiscipline>> getChosenDisciplineMap(Collection<Long> directionIds) {

        final Map<Long, List<WrapChoseEntranseDiscipline>> edDiscMap = new HashMap<>();

        BatchUtils.execute(directionIds, 1000, new BatchUtils.Action<Long>() {
            @Override
            public void execute(Collection<Long> elements)
            {


                DQLSelectBuilder edBuilder = new DQLSelectBuilder().fromEntity(ChosenEntranceDiscipline.class, "ced")
                        .joinPath(DQLJoinType.inner, ChosenEntranceDiscipline.chosenEnrollmentDirection().enrollmentDirection().fromAlias("ced"), "dir")

                        .where(DQLExpressions.in(DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("ced")), elements))

                        .joinEntity("ced", DQLJoinType.left, Group2DisciplineRelation.class, "gr",
                                    DQLExpressions.eq(DQLExpressions.property(ChosenEntranceDiscipline.enrollmentCampaignDiscipline().id().fromAlias("ced")),
                                                      DQLExpressions.property(Group2DisciplineRelation.discipline().id().fromAlias("gr"))
                                    )
                        )

                        .joinEntity("ced", DQLJoinType.inner, EntranceDiscipline.class, "ed",
                                    DQLExpressions.and(
                                            DQLExpressions.eq(
                                                    DQLExpressions.property("dir.id"),
                                                    DQLExpressions.property(EntranceDiscipline.enrollmentDirection().id().fromAlias("ed"))
                                            ),
                                            DQLExpressions.or(
                                                    DQLExpressions.eq(
                                                            DQLExpressions.property(ChosenEntranceDiscipline.enrollmentCampaignDiscipline().id().fromAlias("ced")),
                                                            DQLExpressions.property(EntranceDiscipline.discipline().id().fromAlias("ed"))
                                                    )
                                                    ,
                                                    DQLExpressions.and(
                                                            DQLExpressions.isNotNull("gr"),
                                                            DQLExpressions.eq(
                                                                    DQLExpressions.property(Group2DisciplineRelation.group().id().fromAlias("gr")),
                                                                    DQLExpressions.property(EntranceDiscipline.discipline().id().fromAlias("ed"))
                                                            )
                                                    )
                                            )
                                    )
                        )
                        .joinEntity("ed", DQLJoinType.inner, EntranceDisciplineExt.class, "ext",
                                    DQLExpressions.eq(
                                            DQLExpressions.property(EntranceDiscipline.id().fromAlias("ed")),
                                            DQLExpressions.property(EntranceDisciplineExt.entranceDiscipline().id().fromAlias("ext"))))


                        .column(DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("ced")))
                        .column(DQLExpressions.property(ChosenEntranceDiscipline.id().fromAlias("ced")))

                        .column(DQLExpressions.property(ChosenEntranceDiscipline.enrollmentCampaignDiscipline().educationSubject().title().fromAlias("ced")))
                        .column(DQLExpressions.property(ChosenEntranceDiscipline.finalMark().fromAlias("ced")))
                        .column(DQLExpressions.property(ChosenEntranceDiscipline.enrollmentCampaignDiscipline().id().fromAlias("ced")))


                        .order(DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("ced")))
                        .order(DQLExpressions.property(EntranceDisciplineExt.priority().fromAlias("ext")));

                List<Object[]> rows = UniDaoFacade.getCoreDao().getList(edBuilder);

                for (Object[] objects : rows) {
                    Long redId = (Long) objects[0];
                    Long chId = (Long) objects[1];
                    String title = (String) objects[2];
                    Double fMark = (Double) objects[3];
                    Long enrollmentCampaignDiscipline = (Long) objects[4];

                    WrapChoseEntranseDiscipline disc = new WrapChoseEntranseDiscipline(chId, enrollmentCampaignDiscipline, fMark, title);

                    List<WrapChoseEntranseDiscipline> list = edDiscMap.get(redId);
                    if (list == null)
                        edDiscMap.put(redId, list = new ArrayList<>());

                    if (!list.contains(disc))
                        list.add(disc);
                }
            }
        });

        return edDiscMap;
    }


    // мап выбранных направлений подготовки для приема
    // по абитуриенту, сортирум
    // 1- номер заявления
    // 2 - приоритет в заявлении
    public static Map<Long, List<WrapRequestEDInfo>> getMapEntrantToRd
    (
            List<Long> directionIds
    )
    {

        // ВАЖНО - это может испониться несколько РАЗ!!! Следовательно класть в map с умом
        final Map<Long, List<WrapRequestEDInfo>> retVal = new HashMap<>();
        BatchUtils.execute(directionIds, 1000, new BatchUtils.Action<Long>() {
            @Override
            public void execute(Collection<Long> elements)
            {

                // только задействованные абитуриенты
                DQLSelectBuilder dqlIn = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "redId")
                        .where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirection.id().fromAlias("redId")), elements))
                        .predicate(DQLPredicateType.distinct)
                        .column(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("redId").s());


                // выбираем нужные RD в правильном порядке
                DQLSelectBuilder rdBuilder = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "red")

                        .where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("red")), dqlIn.getQuery()))

                                // id entrant
                        .column(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("red").s())

                                // id rd
                        .column(RequestedEnrollmentDirection.id().fromAlias("red").s())
                                // номер заявы
                        .column(RequestedEnrollmentDirection.entrantRequest().regNumber().fromAlias("red").s())

                        .column(RequestedEnrollmentDirection.state().code().fromAlias("red").s())
                        .column(RequestedEnrollmentDirection.state().shortTitle().fromAlias("red").s())
                        .column(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().shortTitle().fromAlias("red").s())
                        .column(RequestedEnrollmentDirection.priority().fromAlias("red").s())


                                // номер заявления
                        .order(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().regNumber().fromAlias("red")))
                                // приоритет
                        .order(DQLExpressions.property(RequestedEnrollmentDirection.priority().fromAlias("red")));

                List<Object[]> rows = UniDaoFacade.getCoreDao().getList(rdBuilder);

                for (Object[] objects : rows) {
                    Long entrantId = (Long) objects[0];
                    Long idRd = (Long) objects[1];
                    Integer reqNumber = (Integer) objects[2];


                    List<WrapRequestEDInfo> list = retVal.get(entrantId);
                    if (list == null)
                        retVal.put(entrantId, list = new ArrayList<WrapRequestEDInfo>());

                    String requestNumber = reqNumber.toString();
                    String stateCode = (String) objects[3];
                    String stateTitle = (String) objects[4];
                    String educationTitle = (String) objects[5];
                    Integer priority = (Integer) objects[6];

                    WrapRequestEDInfo wrap = new WrapRequestEDInfo(idRd, requestNumber, stateCode, stateTitle, educationTitle, priority);
                    // иначе сильно дублиться
                    if (!list.contains(wrap))
                        list.add(wrap);

                }
            }
        });

        return retVal;


    }
}
