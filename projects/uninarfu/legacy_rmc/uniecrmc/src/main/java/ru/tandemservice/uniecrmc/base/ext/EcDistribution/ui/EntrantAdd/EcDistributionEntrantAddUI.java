package ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.EntrantAdd;

import org.apache.commons.lang.mutable.MutableInt;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.EcDistributionExtManager;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.EcDistributionUtil;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.EcgEntrantRateRowDTORMC;
import ru.tandemservice.uniecrmc.entity.entrant.EntrantRecommendedSettings;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniec.base.bo.EcDistribution.EcDistributionManager;
import ru.tandemservice.uniec.base.bo.EcDistribution.ui.EntrantAdd.EcDistributionEntrantAdd;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.*;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.*;

@Input({@org.tandemframework.core.component.Bind(key = EcDistributionEntrantAddUI.RULE, binding = EcDistributionEntrantAddUI.RULE)})
public class EcDistributionEntrantAddUI extends ru.tandemservice.uniec.base.bo.EcDistribution.ui.EntrantAdd.EcDistributionEntrantAddUI {

    public static final String RULE = "rule";

    private EcDistributionUtil.Rule rule;
    private Long refreshAfterEntrantId;
    private Set<Long> chosenEntrantSet;
    private boolean haveSpecialRights;
    private boolean usePriority;

    @Override
    public void onComponentRefresh() {
        EntrantRecommendedSettings settings = UniDaoFacade.getCoreDao().get(EntrantRecommendedSettings.class, EntrantRecommendedSettings.enrollmentCampaign(), getDistribution().getConfig().getEcgItem().getEnrollmentCampaign());

        if (settings != null && settings.isUsePriorityEntranceDiscipline()) {
            this.setTargetAdmission(this.rule.equals(EcDistributionUtil.Rule.RULE_TARGET_ADMISSION));
            this.setDistributionPerDirection((getDistribution().getConfig().getEcgItem() instanceof EnrollmentDirection));
            haveSpecialRights = this.rule.equals(EcDistributionUtil.Rule.RULE_NON_COMPETITIVE);

            switch (this.rule) {
                case RULE_TARGET_ADMISSION:
                    this.setTitle("Добавление абитуриентов (поступающих по целевому приему) в распределение");
                    break;
                case RULE_WITHOUT_ENTRANCE_DISCIPLINES:
                    this.setTitle("Добавление абитуриентов (поступающих без ВИ) в распределение");
                    break;
                case RULE_NON_COMPETITIVE:
                    this.setTitle("Добавление абитуриентов (поступающих вне конкурса) в распределение");
                    break;
                case RULE_COMPETITIVE:
                    this.setTitle("Добавление абитуриентов (поступающих на общих основаниях) в распределение");
                    break;
                case RULE_COMPETITIVE_WITH_ORIGINAL:
                    this.setTitle("Добавление абитуриентов (поступающих на общих основаниях) в распределение с оригиналами документов");
                    break;
                default:
                    throw new IllegalArgumentException("Unknown rateRule: " + this.rule);
            }

            this.setMinCount(-1);

            this.setHasDefaultChecked(false);
            this.refreshAfterEntrantId = null;

            IEcgQuotaFreeDTO freeQuotaDTO = EcDistributionManager.instance().dao().getFreeQuotaDTO(this.getDistribution());

            this.setRateList(EcDistributionExtManager.daoRMC().getEntrantRateRowList(this.getDistribution(), freeQuotaDTO.getQuotaDTO().getTaKindList(), this.getRule()));

            this.chosenEntrantSet = new HashSet<Long>();


            setUsePriority(settings.isUsePriorityEntranceDiscipline());

            doRefreshEntrantRateList();
        }
        else
            super.onComponentRefresh();
    }

    public String getDisciplines() {
        return ((EcgEntrantRateRowDTORMC) getConfig().getDataSource("entrantDS").getCurrent()).getDisciplinesHTMLDescription();
    }

    public String getOtherDirectionInfo()
    {
        return ((EcgEntrantRateRowDTORMC) getConfig().getDataSource("entrantDS").getCurrent()).getEntrantRequestsInfoHtml();
    }

    public boolean isUseProfileMark() {
        return !isUsePriority();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        if (isUsePriority()) {
            if (EcDistributionEntrantAdd.ENTRANT_DS.equals(dataSource.getName())) {
                dataSource.put("list", this.getViewRateRowList());

                this.chosenEntrantSet = new HashSet<Long>();
                for (IEntity entity : this.getSelectedRateRowList())
                    this.chosenEntrantSet.add(entity.getId());
            }
        }
        else
            super.onBeforeDataSourceFetch(dataSource);

    }

    private void doRefreshEntrantRateList() {
        IEcgQuotaFreeDTO freeQuotaDTO = EcDistributionManager.instance().dao().getFreeQuotaDTO(getDistribution());
        IEcgQuotaManager quotaManager = new EcgQuotaManager(freeQuotaDTO);

        Map<Long, MutableInt> freeMap = new Hashtable<Long, MutableInt>();

        for (Map.Entry<Long, Integer> entry : freeQuotaDTO.getFreeMap().entrySet()) {
            int free = ((Integer) entry.getValue()).intValue();
            freeMap.put(entry.getKey(), new MutableInt(free));
        }

        Map<Long, Integer> _srFreeMap = EcDistributionExtManager.daoRMC().getFreeSRQuota(this.getDistribution());
        Map<Long, MutableInt> srFreeMap = new Hashtable<Long, MutableInt>();

        for (Map.Entry<Long, Integer> entry : _srFreeMap.entrySet()) {
            int free = ((Integer) entry.getValue()).intValue();
            srFreeMap.put(entry.getKey(), new MutableInt(free));
        }

        this.setChosenMap(new Hashtable<Long, IEcgEntrantRateDirectionDTO>());
        this.setChosen2listMap(new Hashtable<Long, List<IEcgEntrantRateDirectionDTO>>());
        int len = getRateList().size();
        int i = 0;
        getSelectedRateRowList().clear();
        boolean resetFlag = !isHasDefaultChecked();
        for (int freeTotal = quotaManager.getFreeTotal(); i < len && freeTotal > 0; i++) {
            IEcgEntrantRateRowDTO rateRowDTO = (IEcgEntrantRateRowDTO) getRateList().get(i);
            Long entrantId = rateRowDTO.getId();
            Long preferableDirectionId = null;
            boolean skip = false;
            if (isHasDefaultChecked()) {
                if (getChosenEntrantSet().contains(entrantId))
                    preferableDirectionId = resetFlag ? null : (Long) getChosenDirectionMap().get(entrantId);
                else
                    skip = true;
                resetFlag |= entrantId.equals(getRefreshAfterEntrantId());
            }
            if (skip)
                continue;

            IEcgPlanChoiceResult choiceResult;
            if (getRule().equals(EcDistributionUtil.Rule.RULE_NON_COMPETITIVE)) {
                choiceResult = EcDistributionExtManager.daoRMC().getPossibleDirectionList(rateRowDTO, preferableDirectionId, freeMap, srFreeMap);
            }
            else
                choiceResult = quotaManager.getPossibleDirectionList(rateRowDTO, preferableDirectionId);
            IEcgEntrantRateDirectionDTO chosenDirection = choiceResult.getChosenDirection();
            if (chosenDirection != null) {
                freeTotal--;
                getChosenMap().put(entrantId, chosenDirection);
                getChosen2listMap().put(entrantId, choiceResult.getPossibleDirectionList());
                getSelectedRateRowList().add(rateRowDTO);
            }
        }

        int count = i > getMinCount() ? i + 25 : getMinCount();
        this.setHasMore((count < this.getRateList().size()));
        this.setViewRateRowList((this.isHasMore() ? this.getRateList().subList(0, count) : this.getRateList()));
        this.setChosenDirectionMap(new Hashtable<Long, Long>());
    }

    @Override
    public boolean isRowChecked() {
        if (isUsePriority()) {
            Long entrantId = ((IEcgEntrantRateRowDTO) getConfig().getDataSource(EcDistributionEntrantAdd.ENTRANT_DS).getCurrent()).getId();
            return this.chosenEntrantSet.contains(entrantId);
        }
        else
            return super.isRowChecked();
    }

    @Override
    public void setRowChecked(boolean rowChecked) {
        if (isUsePriority()) {
            Long entrantId = ((IEcgEntrantRateRowDTO) getConfig().getDataSource(EcDistributionEntrantAdd.ENTRANT_DS).getCurrent()).getId();
            if (rowChecked)
                this.chosenEntrantSet.add(entrantId);
            else
                this.chosenEntrantSet.remove(entrantId);
        }
        else
            super.setRowChecked(rowChecked);
    }

    @Override
    public void onRateRefresh() {
        if (isUsePriority()) {
            this.setHasDefaultChecked(true);
            this.refreshAfterEntrantId = getListenerParameterAsLong();
            doRefreshEntrantRateList();
        }
        else
            super.onRateRefresh();
    }

    @Override
    public void onClickSave() {
        if (isUsePriority()) {
            if (this.isDistributionPerDirection()) {
                Set<Long> chosenDirectionIds = new HashSet<Long>();

                for (Long id : this.chosenEntrantSet) {
                    IEcgEntrantRateDirectionDTO rateDirectionDTO = (IEcgEntrantRateDirectionDTO) this.getChosenMap().get(id);
                    if (null != rateDirectionDTO) {
                        chosenDirectionIds.add(rateDirectionDTO.getId());
                    }
                }
                EcDistributionExtManager.daoRMC().saveEntrantRecommendedList(this.getDistribution(), this.isTargetAdmission(), this.isHaveSpecialRights(), chosenDirectionIds);
            }
            else {
                EcDistributionExtManager.daoRMC().saveEntrantRecommendedList(this.getDistribution(), this.isTargetAdmission(), this.isHaveSpecialRights(), this.getChosenDirectionMap().values());
            }

            deactivate();
        }
        else
            super.onClickSave();
    }

    public EcDistributionUtil.Rule getRule() {
        return rule;
    }

    public void setRule(EcDistributionUtil.Rule rule) {
        this.rule = rule;
    }

    public Long getRefreshAfterEntrantId() {
        return refreshAfterEntrantId;
    }

    public void setRefreshAfterEntrantId(Long refreshAfterEntrantId) {
        this.refreshAfterEntrantId = refreshAfterEntrantId;
    }

    public Set<Long> getChosenEntrantSet() {
        return chosenEntrantSet;
    }

    public void setChosenEntrantSet(Set<Long> chosenEntrantSet) {
        this.chosenEntrantSet = chosenEntrantSet;
    }

    public boolean isHaveSpecialRights() {
        return haveSpecialRights;
    }

    public void setHaveSpecialRights(boolean haveSpecialRights) {
        this.haveSpecialRights = haveSpecialRights;
    }

    public boolean isUsePriority() {
        return usePriority;
    }

    public void setUsePriority(boolean usePriority) {
        this.usePriority = usePriority;
    }
}
