package ru.tandemservice.uniecrmc.ws;

import org.apache.commons.lang.StringUtils;
import org.apache.cxf.interceptor.InInterceptors;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import ru.tandemservice.uniecrmc.dao.entrantRating.IRatingDao;
import ru.tandemservice.uniecrmc.ws.dao.IDAOWSReport;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;

/**
 * @author vch
 * @since 2012/06/15
 * <p/>
 * Сервис выдает данные для работы внешних отчетов
 */
@WebService(serviceName = "EntrantCompanyReports")
@InInterceptors(interceptors = {"ru.tandemservice.uniecrmc.ws.EntrantCompanyReportsInterceptor"})
public class EntrantCompanyReports {
    public static final String MUTEX = "mutex";

    @WebMethod
    public String getRating(
            @WebParam(name = "enrolmentCompaignId") Long enrolmentCompaignId,
            @WebParam(name = "formativeOrgUnitId") String formativeOrgUnitId,
            @WebParam(name = "developFormId") String developFormId,
            @WebParam(name = "enrollmentDirectionId") String enrollmentDirectionId)
    {

        synchronized (MUTEX) {
            System.out.println("WS Start getRating " + RussianDateFormatUtils.STANDARD_DATE_TIME_SEC_FORMAT.format(new Date()));
            System.out.println(" enrolmentCompaignId: " + enrolmentCompaignId);
            System.out.println(" formativeOrgUnitId: " + enrolmentCompaignId);
            System.out.println(" developFormId: " + enrolmentCompaignId);
            System.out.println(" enrollmentDirectionId: " + enrolmentCompaignId);


            IRatingDao dao = (IRatingDao) ApplicationRuntime.getBean(IRatingDao.class.getName());
            IRatingDao.Params params = new IRatingDao.Params(
                    enrolmentCompaignId,
                    StringUtils.isEmpty(formativeOrgUnitId) ? null : Long.parseLong(formativeOrgUnitId),
                    developFormId,
                    StringUtils.isEmpty(enrollmentDirectionId) ? null : Long.parseLong(enrollmentDirectionId));
            String retVal = null;
            try {
                retVal = dao.writeToString(params);
            }
            catch (Exception ex) {
                ex.printStackTrace();//чтоб в логах было видно
                StringWriter writer = new StringWriter();
                ex.printStackTrace(new PrintWriter(writer));
                retVal = writer.toString();
            }

            System.out.println(new StringBuilder()
                                       .append("WS End getRating")
                                       .append(" ").append(RussianDateFormatUtils.STANDARD_DATE_TIME_SEC_FORMAT.format(new Date()))
                                       .append(", size=").append(retVal.length())
                                       .toString());
            return retVal;
        }
    }

    @WebMethod
    public String getEntrantList(@WebParam(name = "enrolmentCompaignId") String enrolmentCompaignId) {
        synchronized (MUTEX) {
            System.out.println("WS Start getEntrantList ");
            IDAOWSReport report = (IDAOWSReport) ApplicationRuntime.getBean(IDAOWSReport.class.getName());

            String retVal = report.getEntrantList(enrolmentCompaignId);

            System.out.println("WS End getEntrantList size=" + retVal.length());

            return retVal;
        }
    }

    @WebMethod
    public String getEntrantRequestStatistic(@WebParam(name = "enrolmentCompaignId") String enrolmentCompaignId) {
        synchronized (MUTEX) {
            System.out.println("WS Start getEntrantRequestStatistic ");
            IDAOWSReport report = (IDAOWSReport) ApplicationRuntime.getBean(IDAOWSReport.class.getName());
            String retVal = report.getEntrantRequestStatistic(enrolmentCompaignId);
            System.out.println("WS End getEntrantRequestStatistic size=" + retVal.length());
            return retVal;
        }
    }

}
