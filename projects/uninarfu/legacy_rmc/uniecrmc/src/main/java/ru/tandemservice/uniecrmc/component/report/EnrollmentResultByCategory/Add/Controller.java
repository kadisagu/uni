package ru.tandemservice.uniecrmc.component.report.EnrollmentResultByCategory.Add;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        ((IDAO) getDao()).prepare(model);
    }

    public void onClickApply(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        ((IDAO) getDao()).update(model);
        deactivate(component);
        activateInRoot(component, new PublisherActivator(model.getReport()));
    }

}
