package ru.tandemservice.uniecrmc.component.entrant.IndividualAchievementsAddEdit;

import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uniecrmc.entity.catalog.IndividualAchievements;
import ru.tandemservice.uniecrmc.entity.entrant.Entrant2IndividualAchievements;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.entrant.Entrant;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(final Model model) {
        if (!model.isAddForm()) {
            model.setAchievement(get(Entrant2IndividualAchievements.class, model.getAchievement().getId()));
            model.setEntrant(model.getAchievement().getEntrant());
        }
        else {
            model.setEntrant(get(Entrant.class, model.getEntrant().getId()));
            model.getAchievement().setEntrant(model.getEntrant());
        }
        model.setAchievementModel(new LazySimpleSelectModel<>(IndividualAchievements.class));

    }


    @Override
    @Transactional
    public void update(Model model) {
        getSession().saveOrUpdate(model.getAchievement());
    }


}
