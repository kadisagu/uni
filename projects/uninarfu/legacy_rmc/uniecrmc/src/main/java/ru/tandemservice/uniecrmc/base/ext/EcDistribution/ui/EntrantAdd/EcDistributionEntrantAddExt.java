package ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.EntrantAdd;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtension;
import org.tandemframework.caf.ui.config.datasource.IColumnListExtensionBuilder;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.uniec.base.bo.EcDistribution.ui.EntrantAdd.EcDistributionEntrantAdd;

@Configuration
public class EcDistributionEntrantAddExt extends BusinessComponentExtensionManager {

    @Autowired
    private EcDistributionEntrantAdd parent;

    @Bean
    public ColumnListExtension extensionEntrantDS()
    {
        IColumnListExtensionBuilder c = columnListExtensionBuilder(parent.entrantDS());
        c.addAllAfter("finalMark");
        c.addColumn(blockColumn("disciplines", "disciplinesBlockColumn").visible("ui:usePriority").create());
        c.addColumn(textColumn("recommendation", "recommendationTitle").visible("ui:usePriority").create());
        c.addColumn(blockColumn("otherDirectionInfo", "otherDirectionInfoBlockColumn").visible("ui:usePriority").create());

        c.replaceColumn(textColumn("profileMark", "profileMark").formatter(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS).visible("ui:useProfileMark").create());
        return c.create();
    }
}
