package ru.tandemservice.uniecrmc.component.wizard.ContactDataStep;

import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.utils.AddressBaseUtils;
import ru.tandemservice.uniec.component.wizard.ContactDataStep.Model;

public class DAO extends ru.tandemservice.uniec.component.wizard.ContactDataStep.DAO {

    @Override
    public void prepare(Model model) {
        super.prepare(model);
        if (model.getAddress() != null && model.getAddress().getId() != null) {
            AddressBase copy = AddressBaseUtils.getSameAddress(model.getAddress());
            model.setAddress(copy);
        }
    }
}
