package ru.tandemservice.uniecrmc.component.entrant.EnrollmentDirectionList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import ru.tandemservice.uniec.component.entrant.EnrollmentDirectionList.Model;

public class Controller extends ru.tandemservice.uniec.component.entrant.EnrollmentDirectionList.Controller {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        super.onRefreshComponent(component);

        Model model = (Model) component.getModel();

        if (model.getDataSource() != null) {

            if (model.getDataSource().getColumn("haveSpecialRightsPlanBudget") == null && model.getDataSource().getColumn("targetAdmissionPlanBudget") != null) {
                AbstractColumn column = new SimpleColumn("КвБ", "haveSpecialRightsPlanBudget").setClickable(false).setOrderable(false);
                model.getDataSource().addColumn(column, model.getDataSource().getColumn("targetAdmissionPlanBudget").getNumber() + 1);
            }

            if (model.getDataSource().getColumn("haveSpecialRightsPlanContract") == null && model.getDataSource().getColumn("targetAdmissionPlanContract") != null) {
                AbstractColumn column = new SimpleColumn("КвК", "haveSpecialRightsPlanContract").setClickable(false).setOrderable(false);
                model.getDataSource().addColumn(column, model.getDataSource().getColumn("targetAdmissionPlanContract").getNumber() + 1);
            }

            if (model.getDataSource().getColumn("advansedPlanBudget") == null && model.getDataSource().getColumn("targetAdmissionPlanBudget") != null) {
                AbstractColumn column = new SimpleColumn("ДпБ", "advansedPlanBudget").setClickable(false).setOrderable(false);
                model.getDataSource().addColumn(column, model.getDataSource().getColumn("targetAdmissionPlanBudget").getNumber() + 1);
            }
        }
    }
}
