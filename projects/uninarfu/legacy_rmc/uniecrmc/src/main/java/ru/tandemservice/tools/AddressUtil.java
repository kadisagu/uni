package ru.tandemservice.tools;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInlineUI;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressInter;
import org.tandemframework.shared.fias.base.entity.AddressRu;
import org.tandemframework.shared.fias.base.entity.AddressString;

public class AddressUtil {

    public static AddressBase createInstance(AddressBase address) {
        if (address == null)
            return null;

        if (address instanceof AddressRu) {
            return new AddressRu();
        }
        else if (address instanceof AddressInter) {
            return new AddressInter();
        }
        else if (address instanceof AddressString) {
            return new AddressString();
        }
        else
            throw new ApplicationException("Неизвестный класс адреса" + address.getClass().getName());
    }

    public static AddressBase createCopy(AddressBase address) {
        AddressBase copy = createInstance(address);
        if (copy != null)
            copy.update(address);
        return copy;
    }

    public static void copyToComponent(AddressBase address, IBusinessComponent component, String regionName) {
        AddressBaseEditInlineUI addressBaseEditInlineUI = (AddressBaseEditInlineUI) component.getRegion("addressRegion").getActiveComponent().getPresenter();

        AddressBase newAddress = createCopy(address);
        addressBaseEditInlineUI.getAddressEditUIConfig().setAddressBase(newAddress);
        addressBaseEditInlineUI.refreshData();
    }
}
