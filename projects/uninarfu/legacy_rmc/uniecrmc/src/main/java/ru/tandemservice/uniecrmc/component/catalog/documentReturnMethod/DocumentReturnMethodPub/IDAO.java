package ru.tandemservice.uniecrmc.component.catalog.documentReturnMethod.DocumentReturnMethodPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;
import ru.tandemservice.uniecrmc.entity.catalog.DocumentReturnMethod;

public interface IDAO extends IDefaultCatalogPubDAO<DocumentReturnMethod, Model> {
}
