package ru.tandemservice.uniecrmc.component.report.EntrantEduLevelDistribution.Add;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uniecrmc.entity.entrant.EntrantEduLevelDistributionReport;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.Date;

public class DAO extends UniDao<Model>
        implements IDAO
{
    public void prepare(final Model model) {
        Session session = getSession();

        model.setEnrollmentCampaignModel(new DQLFullCheckSelectModel(new String[]{EnrollmentCampaign.title().s()}) {
                                             @SuppressWarnings("deprecation")
                                             @Override
                                             protected DQLSelectBuilder query(String alias, String filter) {
                                                 DQLSelectBuilder builder = new DQLSelectBuilder()
                                                         .fromEntity(EnrollmentCampaign.class, alias)
                                                         .order(DQLExpressions.property(EnrollmentCampaign.educationYear().intValue().fromAlias(alias)))
                                                         .order(DQLExpressions.property(EnrollmentCampaign.title().fromAlias(alias)));

                                                 if (filter != null && !filter.isEmpty()) {
                                                     builder.where(DQLExpressions.likeUpper(
                                                                           DQLExpressions.property(EnrollmentCampaign.title().fromAlias(alias)),
                                                                           DQLExpressions.value(CoreStringUtils.escapeLike((filter))))
                                                     );
                                                 }

                                                 return builder;
                                             }
                                         }
        );
        model.setFormativeOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
        model.setEducationLevelHighSchoolListModel(new DQLFullCheckSelectModel("printTitle") {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "ed").column(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().id().fromAlias("ed")));
                subBuilder.where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.formativeOrgUnit().fromAlias("ed")), model.getFormativeOrgUnitList()));

                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(EducationLevelsHighSchool.class, alias);
                builder.where(DQLExpressions.in(DQLExpressions.property(EducationLevelsHighSchool.id().fromAlias(alias)), subBuilder.buildQuery()));
                FilterUtils.applySimpleLikeFilter(builder, alias, EducationLevelsHighSchool.title(), filter);
                return builder;
            }
        });
        model.setDevelopFormListModel(new LazySimpleSelectModel<DevelopForm>(DevelopForm.class));
        model.setDevelopConditionListModel(new LazySimpleSelectModel<DevelopCondition>(DevelopCondition.class));

    }

    public void update(Model model) {
        Session session = getSession();

        EntrantEduLevelDistributionReport report = model.getReport();
        report.setFormingDate(new Date());
        report.setDevelopConditionTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(model.getDevelopConditionList(), "title"), "; "));


        if (model.isFormativeOrgUnitActive()) {
            report.setFormativeOrgUnitTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(model.getFormativeOrgUnitList(), "fullTitle"), "; "));
        }

        if (model.isEducationLevelHighSchoolActive()) {
            report.setEducationLevelsHighSchoolTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(model.getEducationLevelHighSchoolList(), "displayableTitle"), "; "));
        }
        if (model.isDevelopFormActive()) {
            report.setDevelopFormTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(model.getDevelopFormList(), "title"), "; "));
        }


        DatabaseFile databaseFile = null;
        try {
            databaseFile = new EntrantEduLevelDistributionReportBuilder(model, session).getContent();
        }
        catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        session.save(databaseFile);

        report.setContent(databaseFile);
        session.save(report);
    }
}

