package ru.tandemservice.uniecrmc.dao.moveoriginal;

public class WrapEntrantRequest {
    private Long entrantRequestId;
    private double finalMark;

    public WrapEntrantRequest(
            Long entrantRequestId
            , double finalMark
    )
    {
        this.entrantRequestId = entrantRequestId;
        this.finalMark = finalMark;
    }

    public Long getEntrantRequestId() {
        return entrantRequestId;
    }

    public void setEntrantRequestId(Long entrantRequestId) {
        this.entrantRequestId = entrantRequestId;
    }

    public double getFinalMark() {
        return finalMark;
    }

    public void setFinalMark(double finalMark) {
        this.finalMark = finalMark;
    }

    @Override
    public int hashCode()
    {
        return entrantRequestId.hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
            return false;
        else {
            if (obj.hashCode() == this.hashCode())
                return true;
            else
                return false;
        }
    }
}
