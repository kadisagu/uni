package ru.tandemservice.uniecrmc.online.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.catalog.entity.RelationDegree;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;
import ru.tandemservice.uniecrmc.online.entity.OnlineEntrantExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение сущности 'Онлайн-абитуриент'
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OnlineEntrantExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniecrmc.online.entity.OnlineEntrantExt";
    public static final String ENTITY_NAME = "onlineEntrantExt";
    public static final int VERSION_HASH = -1410690508;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT = "entrant";
    public static final String P_SNILS_NUMBER = "snilsNumber";
    public static final String L_NEXT_OF_KIN_RELATION_DEGREE = "nextOfKinRelationDegree";
    public static final String P_NEXT_OF_KIN_LAST_NAME = "nextOfKinLastName";
    public static final String P_NEXT_OF_KIN_FIRST_NAME = "nextOfKinFirstName";
    public static final String P_NEXT_OF_KIN_MIDDLE_NAME = "nextOfKinMiddleName";
    public static final String P_NEXT_OF_KIN_WORK_PLACE = "nextOfKinWorkPlace";
    public static final String P_NEXT_OF_KIN_WORK_POST = "nextOfKinWorkPost";
    public static final String P_NEXT_OF_KIN_PHONE = "nextOfKinPhone";
    public static final String P_DESCRIPTION = "description";

    private OnlineEntrant _entrant;     // Онлайн-абитуриент
    private String _snilsNumber;     // СНИЛС
    private RelationDegree _nextOfKinRelationDegree;     // Степень родства
    private String _nextOfKinLastName;     // Фамилия ближайшего родственника
    private String _nextOfKinFirstName;     // Имя ближайшего родственника
    private String _nextOfKinMiddleName;     // Отчество ближайшего родственника
    private String _nextOfKinWorkPlace;     // Место работы ближайшего родственника
    private String _nextOfKinWorkPost;     // Должность ближайшего родственника
    private String _nextOfKinPhone;     // Телефон ближайшего родственника
    private String _description;     // Дополнительная информация

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Онлайн-абитуриент. Свойство не может быть null.
     */
    @NotNull
    public OnlineEntrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Онлайн-абитуриент. Свойство не может быть null.
     */
    public void setEntrant(OnlineEntrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return СНИЛС.
     */
    @Length(max=255)
    public String getSnilsNumber()
    {
        return _snilsNumber;
    }

    /**
     * @param snilsNumber СНИЛС.
     */
    public void setSnilsNumber(String snilsNumber)
    {
        dirty(_snilsNumber, snilsNumber);
        _snilsNumber = snilsNumber;
    }

    /**
     * @return Степень родства.
     */
    public RelationDegree getNextOfKinRelationDegree()
    {
        return _nextOfKinRelationDegree;
    }

    /**
     * @param nextOfKinRelationDegree Степень родства.
     */
    public void setNextOfKinRelationDegree(RelationDegree nextOfKinRelationDegree)
    {
        dirty(_nextOfKinRelationDegree, nextOfKinRelationDegree);
        _nextOfKinRelationDegree = nextOfKinRelationDegree;
    }

    /**
     * @return Фамилия ближайшего родственника.
     */
    @Length(max=255)
    public String getNextOfKinLastName()
    {
        return _nextOfKinLastName;
    }

    /**
     * @param nextOfKinLastName Фамилия ближайшего родственника.
     */
    public void setNextOfKinLastName(String nextOfKinLastName)
    {
        dirty(_nextOfKinLastName, nextOfKinLastName);
        _nextOfKinLastName = nextOfKinLastName;
    }

    /**
     * @return Имя ближайшего родственника.
     */
    @Length(max=255)
    public String getNextOfKinFirstName()
    {
        return _nextOfKinFirstName;
    }

    /**
     * @param nextOfKinFirstName Имя ближайшего родственника.
     */
    public void setNextOfKinFirstName(String nextOfKinFirstName)
    {
        dirty(_nextOfKinFirstName, nextOfKinFirstName);
        _nextOfKinFirstName = nextOfKinFirstName;
    }

    /**
     * @return Отчество ближайшего родственника.
     */
    @Length(max=255)
    public String getNextOfKinMiddleName()
    {
        return _nextOfKinMiddleName;
    }

    /**
     * @param nextOfKinMiddleName Отчество ближайшего родственника.
     */
    public void setNextOfKinMiddleName(String nextOfKinMiddleName)
    {
        dirty(_nextOfKinMiddleName, nextOfKinMiddleName);
        _nextOfKinMiddleName = nextOfKinMiddleName;
    }

    /**
     * @return Место работы ближайшего родственника.
     */
    @Length(max=255)
    public String getNextOfKinWorkPlace()
    {
        return _nextOfKinWorkPlace;
    }

    /**
     * @param nextOfKinWorkPlace Место работы ближайшего родственника.
     */
    public void setNextOfKinWorkPlace(String nextOfKinWorkPlace)
    {
        dirty(_nextOfKinWorkPlace, nextOfKinWorkPlace);
        _nextOfKinWorkPlace = nextOfKinWorkPlace;
    }

    /**
     * @return Должность ближайшего родственника.
     */
    @Length(max=255)
    public String getNextOfKinWorkPost()
    {
        return _nextOfKinWorkPost;
    }

    /**
     * @param nextOfKinWorkPost Должность ближайшего родственника.
     */
    public void setNextOfKinWorkPost(String nextOfKinWorkPost)
    {
        dirty(_nextOfKinWorkPost, nextOfKinWorkPost);
        _nextOfKinWorkPost = nextOfKinWorkPost;
    }

    /**
     * @return Телефон ближайшего родственника.
     */
    @Length(max=255)
    public String getNextOfKinPhone()
    {
        return _nextOfKinPhone;
    }

    /**
     * @param nextOfKinPhone Телефон ближайшего родственника.
     */
    public void setNextOfKinPhone(String nextOfKinPhone)
    {
        dirty(_nextOfKinPhone, nextOfKinPhone);
        _nextOfKinPhone = nextOfKinPhone;
    }

    /**
     * @return Дополнительная информация.
     */
    @Length(max=255)
    public String getDescription()
    {
        return _description;
    }

    /**
     * @param description Дополнительная информация.
     */
    public void setDescription(String description)
    {
        dirty(_description, description);
        _description = description;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OnlineEntrantExtGen)
        {
            setEntrant(((OnlineEntrantExt)another).getEntrant());
            setSnilsNumber(((OnlineEntrantExt)another).getSnilsNumber());
            setNextOfKinRelationDegree(((OnlineEntrantExt)another).getNextOfKinRelationDegree());
            setNextOfKinLastName(((OnlineEntrantExt)another).getNextOfKinLastName());
            setNextOfKinFirstName(((OnlineEntrantExt)another).getNextOfKinFirstName());
            setNextOfKinMiddleName(((OnlineEntrantExt)another).getNextOfKinMiddleName());
            setNextOfKinWorkPlace(((OnlineEntrantExt)another).getNextOfKinWorkPlace());
            setNextOfKinWorkPost(((OnlineEntrantExt)another).getNextOfKinWorkPost());
            setNextOfKinPhone(((OnlineEntrantExt)another).getNextOfKinPhone());
            setDescription(((OnlineEntrantExt)another).getDescription());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OnlineEntrantExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OnlineEntrantExt.class;
        }

        public T newInstance()
        {
            return (T) new OnlineEntrantExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrant":
                    return obj.getEntrant();
                case "snilsNumber":
                    return obj.getSnilsNumber();
                case "nextOfKinRelationDegree":
                    return obj.getNextOfKinRelationDegree();
                case "nextOfKinLastName":
                    return obj.getNextOfKinLastName();
                case "nextOfKinFirstName":
                    return obj.getNextOfKinFirstName();
                case "nextOfKinMiddleName":
                    return obj.getNextOfKinMiddleName();
                case "nextOfKinWorkPlace":
                    return obj.getNextOfKinWorkPlace();
                case "nextOfKinWorkPost":
                    return obj.getNextOfKinWorkPost();
                case "nextOfKinPhone":
                    return obj.getNextOfKinPhone();
                case "description":
                    return obj.getDescription();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrant":
                    obj.setEntrant((OnlineEntrant) value);
                    return;
                case "snilsNumber":
                    obj.setSnilsNumber((String) value);
                    return;
                case "nextOfKinRelationDegree":
                    obj.setNextOfKinRelationDegree((RelationDegree) value);
                    return;
                case "nextOfKinLastName":
                    obj.setNextOfKinLastName((String) value);
                    return;
                case "nextOfKinFirstName":
                    obj.setNextOfKinFirstName((String) value);
                    return;
                case "nextOfKinMiddleName":
                    obj.setNextOfKinMiddleName((String) value);
                    return;
                case "nextOfKinWorkPlace":
                    obj.setNextOfKinWorkPlace((String) value);
                    return;
                case "nextOfKinWorkPost":
                    obj.setNextOfKinWorkPost((String) value);
                    return;
                case "nextOfKinPhone":
                    obj.setNextOfKinPhone((String) value);
                    return;
                case "description":
                    obj.setDescription((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrant":
                        return true;
                case "snilsNumber":
                        return true;
                case "nextOfKinRelationDegree":
                        return true;
                case "nextOfKinLastName":
                        return true;
                case "nextOfKinFirstName":
                        return true;
                case "nextOfKinMiddleName":
                        return true;
                case "nextOfKinWorkPlace":
                        return true;
                case "nextOfKinWorkPost":
                        return true;
                case "nextOfKinPhone":
                        return true;
                case "description":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrant":
                    return true;
                case "snilsNumber":
                    return true;
                case "nextOfKinRelationDegree":
                    return true;
                case "nextOfKinLastName":
                    return true;
                case "nextOfKinFirstName":
                    return true;
                case "nextOfKinMiddleName":
                    return true;
                case "nextOfKinWorkPlace":
                    return true;
                case "nextOfKinWorkPost":
                    return true;
                case "nextOfKinPhone":
                    return true;
                case "description":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrant":
                    return OnlineEntrant.class;
                case "snilsNumber":
                    return String.class;
                case "nextOfKinRelationDegree":
                    return RelationDegree.class;
                case "nextOfKinLastName":
                    return String.class;
                case "nextOfKinFirstName":
                    return String.class;
                case "nextOfKinMiddleName":
                    return String.class;
                case "nextOfKinWorkPlace":
                    return String.class;
                case "nextOfKinWorkPost":
                    return String.class;
                case "nextOfKinPhone":
                    return String.class;
                case "description":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OnlineEntrantExt> _dslPath = new Path<OnlineEntrantExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OnlineEntrantExt");
    }
            

    /**
     * @return Онлайн-абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.online.entity.OnlineEntrantExt#getEntrant()
     */
    public static OnlineEntrant.Path<OnlineEntrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return СНИЛС.
     * @see ru.tandemservice.uniecrmc.online.entity.OnlineEntrantExt#getSnilsNumber()
     */
    public static PropertyPath<String> snilsNumber()
    {
        return _dslPath.snilsNumber();
    }

    /**
     * @return Степень родства.
     * @see ru.tandemservice.uniecrmc.online.entity.OnlineEntrantExt#getNextOfKinRelationDegree()
     */
    public static RelationDegree.Path<RelationDegree> nextOfKinRelationDegree()
    {
        return _dslPath.nextOfKinRelationDegree();
    }

    /**
     * @return Фамилия ближайшего родственника.
     * @see ru.tandemservice.uniecrmc.online.entity.OnlineEntrantExt#getNextOfKinLastName()
     */
    public static PropertyPath<String> nextOfKinLastName()
    {
        return _dslPath.nextOfKinLastName();
    }

    /**
     * @return Имя ближайшего родственника.
     * @see ru.tandemservice.uniecrmc.online.entity.OnlineEntrantExt#getNextOfKinFirstName()
     */
    public static PropertyPath<String> nextOfKinFirstName()
    {
        return _dslPath.nextOfKinFirstName();
    }

    /**
     * @return Отчество ближайшего родственника.
     * @see ru.tandemservice.uniecrmc.online.entity.OnlineEntrantExt#getNextOfKinMiddleName()
     */
    public static PropertyPath<String> nextOfKinMiddleName()
    {
        return _dslPath.nextOfKinMiddleName();
    }

    /**
     * @return Место работы ближайшего родственника.
     * @see ru.tandemservice.uniecrmc.online.entity.OnlineEntrantExt#getNextOfKinWorkPlace()
     */
    public static PropertyPath<String> nextOfKinWorkPlace()
    {
        return _dslPath.nextOfKinWorkPlace();
    }

    /**
     * @return Должность ближайшего родственника.
     * @see ru.tandemservice.uniecrmc.online.entity.OnlineEntrantExt#getNextOfKinWorkPost()
     */
    public static PropertyPath<String> nextOfKinWorkPost()
    {
        return _dslPath.nextOfKinWorkPost();
    }

    /**
     * @return Телефон ближайшего родственника.
     * @see ru.tandemservice.uniecrmc.online.entity.OnlineEntrantExt#getNextOfKinPhone()
     */
    public static PropertyPath<String> nextOfKinPhone()
    {
        return _dslPath.nextOfKinPhone();
    }

    /**
     * @return Дополнительная информация.
     * @see ru.tandemservice.uniecrmc.online.entity.OnlineEntrantExt#getDescription()
     */
    public static PropertyPath<String> description()
    {
        return _dslPath.description();
    }

    public static class Path<E extends OnlineEntrantExt> extends EntityPath<E>
    {
        private OnlineEntrant.Path<OnlineEntrant> _entrant;
        private PropertyPath<String> _snilsNumber;
        private RelationDegree.Path<RelationDegree> _nextOfKinRelationDegree;
        private PropertyPath<String> _nextOfKinLastName;
        private PropertyPath<String> _nextOfKinFirstName;
        private PropertyPath<String> _nextOfKinMiddleName;
        private PropertyPath<String> _nextOfKinWorkPlace;
        private PropertyPath<String> _nextOfKinWorkPost;
        private PropertyPath<String> _nextOfKinPhone;
        private PropertyPath<String> _description;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Онлайн-абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.online.entity.OnlineEntrantExt#getEntrant()
     */
        public OnlineEntrant.Path<OnlineEntrant> entrant()
        {
            if(_entrant == null )
                _entrant = new OnlineEntrant.Path<OnlineEntrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return СНИЛС.
     * @see ru.tandemservice.uniecrmc.online.entity.OnlineEntrantExt#getSnilsNumber()
     */
        public PropertyPath<String> snilsNumber()
        {
            if(_snilsNumber == null )
                _snilsNumber = new PropertyPath<String>(OnlineEntrantExtGen.P_SNILS_NUMBER, this);
            return _snilsNumber;
        }

    /**
     * @return Степень родства.
     * @see ru.tandemservice.uniecrmc.online.entity.OnlineEntrantExt#getNextOfKinRelationDegree()
     */
        public RelationDegree.Path<RelationDegree> nextOfKinRelationDegree()
        {
            if(_nextOfKinRelationDegree == null )
                _nextOfKinRelationDegree = new RelationDegree.Path<RelationDegree>(L_NEXT_OF_KIN_RELATION_DEGREE, this);
            return _nextOfKinRelationDegree;
        }

    /**
     * @return Фамилия ближайшего родственника.
     * @see ru.tandemservice.uniecrmc.online.entity.OnlineEntrantExt#getNextOfKinLastName()
     */
        public PropertyPath<String> nextOfKinLastName()
        {
            if(_nextOfKinLastName == null )
                _nextOfKinLastName = new PropertyPath<String>(OnlineEntrantExtGen.P_NEXT_OF_KIN_LAST_NAME, this);
            return _nextOfKinLastName;
        }

    /**
     * @return Имя ближайшего родственника.
     * @see ru.tandemservice.uniecrmc.online.entity.OnlineEntrantExt#getNextOfKinFirstName()
     */
        public PropertyPath<String> nextOfKinFirstName()
        {
            if(_nextOfKinFirstName == null )
                _nextOfKinFirstName = new PropertyPath<String>(OnlineEntrantExtGen.P_NEXT_OF_KIN_FIRST_NAME, this);
            return _nextOfKinFirstName;
        }

    /**
     * @return Отчество ближайшего родственника.
     * @see ru.tandemservice.uniecrmc.online.entity.OnlineEntrantExt#getNextOfKinMiddleName()
     */
        public PropertyPath<String> nextOfKinMiddleName()
        {
            if(_nextOfKinMiddleName == null )
                _nextOfKinMiddleName = new PropertyPath<String>(OnlineEntrantExtGen.P_NEXT_OF_KIN_MIDDLE_NAME, this);
            return _nextOfKinMiddleName;
        }

    /**
     * @return Место работы ближайшего родственника.
     * @see ru.tandemservice.uniecrmc.online.entity.OnlineEntrantExt#getNextOfKinWorkPlace()
     */
        public PropertyPath<String> nextOfKinWorkPlace()
        {
            if(_nextOfKinWorkPlace == null )
                _nextOfKinWorkPlace = new PropertyPath<String>(OnlineEntrantExtGen.P_NEXT_OF_KIN_WORK_PLACE, this);
            return _nextOfKinWorkPlace;
        }

    /**
     * @return Должность ближайшего родственника.
     * @see ru.tandemservice.uniecrmc.online.entity.OnlineEntrantExt#getNextOfKinWorkPost()
     */
        public PropertyPath<String> nextOfKinWorkPost()
        {
            if(_nextOfKinWorkPost == null )
                _nextOfKinWorkPost = new PropertyPath<String>(OnlineEntrantExtGen.P_NEXT_OF_KIN_WORK_POST, this);
            return _nextOfKinWorkPost;
        }

    /**
     * @return Телефон ближайшего родственника.
     * @see ru.tandemservice.uniecrmc.online.entity.OnlineEntrantExt#getNextOfKinPhone()
     */
        public PropertyPath<String> nextOfKinPhone()
        {
            if(_nextOfKinPhone == null )
                _nextOfKinPhone = new PropertyPath<String>(OnlineEntrantExtGen.P_NEXT_OF_KIN_PHONE, this);
            return _nextOfKinPhone;
        }

    /**
     * @return Дополнительная информация.
     * @see ru.tandemservice.uniecrmc.online.entity.OnlineEntrantExt#getDescription()
     */
        public PropertyPath<String> description()
        {
            if(_description == null )
                _description = new PropertyPath<String>(OnlineEntrantExtGen.P_DESCRIPTION, this);
            return _description;
        }

        public Class getEntityClass()
        {
            return OnlineEntrantExt.class;
        }

        public String getEntityName()
        {
            return "onlineEntrantExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
