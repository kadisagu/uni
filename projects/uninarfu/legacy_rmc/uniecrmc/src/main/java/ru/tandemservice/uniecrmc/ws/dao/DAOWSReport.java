package ru.tandemservice.uniecrmc.ws.dao;

import com.ibm.icu.text.SimpleDateFormat;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.tandemservice.uniecrmc.dao.entrantRating.*;
import ru.tandemservice.uniecrmc.dao.entrantRating.PlanNode.PlanRowNode;
import ru.tandemservice.uniecrmc.dao.entrantRating.PlanNode.PlanRowNode.SRQuotaRow;
import ru.tandemservice.uniecrmc.dao.entrantRating.PlanNode.PlanRowNode.TAQuotasNode;
import ru.tandemservice.uniecrmc.dao.entrantRating.PlanNode.PlanRowNode.TAQuotasNode.TAQuotaRow;
import ru.tandemservice.uniecrmc.dao.entrantRating.StatisticNode.EntrantsBudgetNode;
import ru.tandemservice.uniecrmc.dao.entrantRating.StatisticNode.EntrantsBudgetNode.CompetitionKindsNode;
import ru.tandemservice.uniecrmc.dao.entrantRating.StatisticNode.EntrantsBudgetNode.TargetAdmissionKindsNode;
import ru.tandemservice.uniecrmc.dao.entrantRating.StatisticNode.EntrantsContractNode;
import ru.tandemservice.uniecrmc.dao.entrantRating.StatisticNode.StatisticRow;
import ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionExt;
import ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt;
import ru.tandemservice.uniecrmc.ws.dao.WrapperEnrolmentDirectionStatistic.EnrolmentDirectionRow;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.*;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.entity.settings.Group2DisciplineRelation;
import ru.tandemservice.uniec.entity.settings.TargetAdmissionPlanRelation;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DAOWSReport extends RatingDao implements IDAOWSReport {

    @Override
    public String getEntrantRequestStatistic(String enrolmentCompaignId) {

        String result = "";

        try {
            Params params = new Params(Long.parseLong(enrolmentCompaignId));
            EnrollmentCampaignNode rootNode = getRootNode(params);

            JAXBContext context = JAXBContext.newInstance(new Class[]{EnrollmentCampaignNode.class});
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty("jaxb.formatted.output", Boolean.valueOf(true));

            StringWriter wr = new StringWriter();
            marshaller.marshal(rootNode, wr);
            result = wr.toString();

        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void fillEnrollmentDirection(
            Params params,
            EnrollmentDirectionsNode rootNode,
            EnrollmentDirection enrollmentDirection,
            EnrollmentDirectionExt enrollmentDirectionExt)
    {

        EnrollmentDirectionNode ecNode = rootNode.add(enrollmentDirection, enrollmentDirectionExt);

        fillEnrollmentDirectionPlan(ecNode, enrollmentDirection, enrollmentDirectionExt);
        fillEnrollmentDirectionStatistic(ecNode, enrollmentDirection);
    }

    protected void fillEnrollmentDirectionStatistic(EnrollmentDirectionNode rootNode, EnrollmentDirection enrollmentDirection) {
        StatisticNode node = new StatisticNode();
        rootNode.statistic = node;

        EntrantsContractNode contractNode = new EntrantsContractNode();

        CompetitionKindsNode cNode = new CompetitionKindsNode();
        TargetAdmissionKindsNode taNode = new TargetAdmissionKindsNode();

        node.entrantsContract = contractNode;

        // вычислем итоговую статистику
        // по данному направлению

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(RequestedEnrollmentDirection.class, "r")
                .joinEntity("r", DQLJoinType.left, RequestedEnrollmentDirectionExt.class, "ext",
                            eq(property(RequestedEnrollmentDirection.id().fromAlias("r")),
                               property(RequestedEnrollmentDirectionExt.requestedEnrollmentDirection().id().fromAlias("ext"))))
                .joinEntity("r", DQLJoinType.left, TargetAdmissionKind.class, "ta",
                            eq(property(RequestedEnrollmentDirection.targetAdmissionKind().id().fromAlias("r")),
                               property(TargetAdmissionKind.id().fromAlias("ta"))))

                .column(RequestedEnrollmentDirection.compensationType().code().fromAlias("r").s())
                .column(RequestedEnrollmentDirection.competitionKind().code().fromAlias("r").s())
                        // признак целевого приема
                .column(TargetAdmissionKind.code().fromAlias("ta").s())
                .column(RequestedEnrollmentDirectionExt.haveSpecialRights().fromAlias("ext").s())

                .column(DQLFunctions.count("r"))
                .column(RequestedEnrollmentDirection.targetAdmission().fromAlias("r").s())
                .group(DQLExpressions.property(RequestedEnrollmentDirection.compensationType().code().fromAlias("r")))
                .group(DQLExpressions.property(RequestedEnrollmentDirection.competitionKind().code().fromAlias("r")))
                .group(TargetAdmissionKind.code().fromAlias("ta").s())
                .group(RequestedEnrollmentDirectionExt.haveSpecialRights().fromAlias("ext").s())
                .group(DQLExpressions.property(RequestedEnrollmentDirection.targetAdmission().fromAlias("r")))
                .distinct()
                .where(eq(property(RequestedEnrollmentDirection.enrollmentDirection().fromAlias("r")), value(enrollmentDirection)))
                        //состояния абитуриентов
                .where(in(property(RequestedEnrollmentDirection.state().code().fromAlias("r")), Arrays.asList("1", "2", "3", "4", "5", "6")));

        List<Object[]> lst = dql.createStatement(getSession()).list();

        for (Object[] obj : lst) {
            String compType = (String) obj[0];
            String compKind = (String) obj[1];

            String taKind = (String) obj[2];
            Boolean sr = (Boolean) obj[3];

            Integer count = ((Long) obj[4]).intValue();
            Boolean ta = (Boolean) obj[5];
            Integer originalCount = getOriginalCount(enrollmentDirection, compType, compKind, taKind, sr, ta);

            //договор
            if (compType.equals(UniDefines.COMPENSATION_TYPE_CONTRACT)) {
                contractNode.add(count.intValue(), originalCount.intValue());
                continue;
            }

            StatisticRow row = new StatisticRow();
            //целевик
            if (ta != null && ta.booleanValue()) {
                row.id = taKind != null ? taKind : "1";
                row.count = String.valueOf(count.intValue());
                row.originalCount = String.valueOf(originalCount.intValue());

                taNode.add(row);

                continue;
            }
            //имеет особые права
            if (sr != null && sr.booleanValue()) {
                row.id = UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION;
            }
            else
                row.id = compKind;

            row.count = String.valueOf(count.intValue());
            row.originalCount = String.valueOf(originalCount.intValue());

            cNode.add(row);
        }

        EntrantsBudgetNode budgetNode = new EntrantsBudgetNode(cNode, taNode);
        node.entrantsBudget = budgetNode;

    }

    private Integer getOriginalCount(EnrollmentDirection enrollmentDirection, String compType, String compKind, String taKind, Boolean sr, Boolean ta) {

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(RequestedEnrollmentDirection.class, "r")

                .joinEntity("r", DQLJoinType.left, RequestedEnrollmentDirectionExt.class, "ext",
                            eq(property(RequestedEnrollmentDirection.id().fromAlias("r")),
                               property(RequestedEnrollmentDirectionExt.requestedEnrollmentDirection().id().fromAlias("ext"))))
                .where(DQLExpressions.eqValue(property(RequestedEnrollmentDirection.originalDocumentHandedIn().fromAlias("r")), Boolean.TRUE))
                .where(DQLExpressions.eqValue(property(RequestedEnrollmentDirection.enrollmentDirection().fromAlias("r")), enrollmentDirection))
                .where(DQLExpressions.eqValue(property(RequestedEnrollmentDirection.compensationType().code().fromAlias("r")), compType))
                .where(DQLExpressions.eqValue(property(RequestedEnrollmentDirection.competitionKind().code().fromAlias("r")), compKind));
        if (taKind != null)
            dql.where(DQLExpressions.eqValue(property(RequestedEnrollmentDirection.targetAdmissionKind().code().fromAlias("r")), taKind));
        else
            dql.where(DQLExpressions.eqValue(property(RequestedEnrollmentDirection.targetAdmission().fromAlias("r")), ta));

        if (sr != null)
            dql.where(DQLExpressions.eqValue(property(RequestedEnrollmentDirectionExt.haveSpecialRights().fromAlias("ext")), sr));
        else
            dql.where(DQLExpressions.or(
                    DQLExpressions.isNull("ext"),
                    DQLExpressions.eqValue(property(RequestedEnrollmentDirectionExt.haveSpecialRights().fromAlias("ext")), Boolean.FALSE)
            ));

        return getCount(dql);
    }

    protected void fillEnrollmentDirectionPlan(EnrollmentDirectionNode rootNode, EnrollmentDirection enrollmentDirection, EnrollmentDirectionExt enrollmentDirectionExt) {
        PlanNode node = new PlanNode();
        rootNode.plan = node;

        if (enrollmentDirectionExt == null)
            enrollmentDirectionExt = new EnrollmentDirectionExt();

        SRQuotaRow srQuotas = new SRQuotaRow(enrollmentDirectionExt.getHaveSpecialRightsPlanBudget() == null ? "0" : enrollmentDirectionExt.getHaveSpecialRightsPlanBudget().intValue() + "", "Имеют особые права");

        Map<TargetAdmissionKind, Integer> valueMapBudget = new HashMap<TargetAdmissionKind, Integer>();
        Map<TargetAdmissionKind, Integer> valueMapContract = new HashMap<TargetAdmissionKind, Integer>();

        for (TargetAdmissionPlanRelation rel : getList(TargetAdmissionPlanRelation.class, TargetAdmissionPlanRelation.entranceEducationOrgUnit(), enrollmentDirection, new String[0])) {
            if (rel.isBudget())
                valueMapBudget.put(rel.getTargetAdmissionKind(), Integer.valueOf(rel.getPlanValue()));
            else
                valueMapContract.put(rel.getTargetAdmissionKind(), Integer.valueOf(rel.getPlanValue()));
        }

        TAQuotasNode taQuotas = new TAQuotasNode(enrollmentDirection.getTargetAdmissionPlanBudget() == null ? "0" : enrollmentDirection.getTargetAdmissionPlanBudget().intValue() + "", "Целевой прием");

        for (Map.Entry<TargetAdmissionKind, Integer> entry : valueMapBudget.entrySet()) {
            TAQuotaRow row = new TAQuotaRow(entry.getKey().getCode(), entry.getValue().intValue() + "");
            taQuotas.list.add(row);
        }

        node.ministerialPlan = new PlanRowNode(enrollmentDirection.getMinisterialPlan() == null ? "0" : enrollmentDirection.getMinisterialPlan().intValue() + "", "бюджет", "План приема на бюджет", srQuotas, taQuotas);

        srQuotas = new SRQuotaRow(enrollmentDirectionExt.getHaveSpecialRightsPlanContract() == null ? "0" : enrollmentDirectionExt.getHaveSpecialRightsPlanContract().intValue() + "", "Имеют особые права");

        taQuotas = new TAQuotasNode(enrollmentDirection.getTargetAdmissionPlanContract() == null ? "0" : enrollmentDirection.getTargetAdmissionPlanContract().intValue() + "", "Целевой прием");

        for (Map.Entry<TargetAdmissionKind, Integer> entry : valueMapContract.entrySet()) {
            TAQuotaRow row = new TAQuotaRow(entry.getKey().getCode(), entry.getValue().intValue() + "");
            taQuotas.list.add(row);
        }

        node.contractPlan = new PlanRowNode(enrollmentDirection.getContractPlan() == null ? "0" : enrollmentDirection.getContractPlan().intValue() + "", "договор", "План приема по договору", srQuotas, taQuotas);
    }

    Map<String, MultiKey> mapEntranceDisciplineType = new HashMap<String, MultiKey>();
    Map<Long, List<EduInstitytionInfo>> mapEduInstitytionInfo = new HashMap<Long, List<EduInstitytionInfo>>();
    Map<Long, List<RequestedEnrollmentDirection>> mapRequestedEnrollmentDirection = null;
    Map<Long, List<ChosenEntranceDiscipline>> mapChosenEntranceDiscipline = null;
    private Map<Long, IEntity> mapIEntity = new HashMap<Long, IEntity>();

    @Override
    public String getEntrantList(String enrolmentCompaignId) {
        clearCache();

        EnrollmentCampaign compaingn = null;
        try {
            compaingn = getEnrollmentCampaignById(enrolmentCompaignId);
        }
        catch (Exception ex) {
            String ec_msg = "";
            if (enrolmentCompaignId == null)
                ec_msg = "null";
            else
                ec_msg = enrolmentCompaignId;

            return "<error>" + "Не удалось найти по id EnrollmentCampaign, enrolmentCompaignId=" + ec_msg + " Сообщение об ошибке: " + ex.getMessage() + "</error>";
        }

        // Получим все заявы абитуриентов на тек момент
        Date curDate = new Date();

        List<String> stateList = new ArrayList<String>();
        stateList.add("1");
        stateList.add("2");
        stateList.add("3");
        stateList.add("4");
        stateList.add("5");
        stateList.add("6");
        stateList.add("7");

        List<EntrantState> entrantState = getEntrantStates(stateList);

        _fillMapPersonEduInstitution(compaingn, curDate, entrantState);
        _fillMapChosenEntranceDiscipline(compaingn, curDate, entrantState);

        makeCache(compaingn, curDate, entrantState);

        List<EntrantRequest> lst = _getEntrantRequest(compaingn, curDate, entrantState);


        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
        try {
            docBuilder = docFactory.newDocumentBuilder();

        }
        catch (ParserConfigurationException e) {
            return "<error>" + e.getMessage() + "</error>";
        }

        // root elements
        Document doc = docBuilder.newDocument();

        Element rootElement = doc.createElement("root");
        doc.appendChild(rootElement);
        _setEnrolmentCompaignAttribute(doc, rootElement, compaingn);


        // отказываемся от упаковки в виде структуры
        // vch 2013/07/08
        /*
		Map<OrgUnit, Map<EducationOrgUnit, List<EntrantRequest>>> map = _makeEntrantRequestMap(lst);
		_packEntrantRequestListByMap(rootElement, doc, map);
		*/


        // Для расчета планов приема, упомянутых в заявлениях
        Map<Long, EnrollmentDirection> mapForPlan = new HashMap<Long, EnrollmentDirection>();

        Element rootEntrantRequest = doc.createElement("entrantRequests");
        rootElement.appendChild(rootEntrantRequest);
        for (EntrantRequest er : lst) {
            _packEntrantRequest(rootEntrantRequest, doc, mapForPlan, er);
        }

        // статистика в отдельной ветке
        Element rootStatistic = doc.createElement("enrollmentDirectionsStatistic");
        rootElement.appendChild(rootStatistic);

        for (Long key : mapForPlan.keySet()) {
            EnrollmentDirection direction = mapForPlan.get(key);
            _packPlan(direction, doc, rootStatistic, entrantState);

        }

        clearCache();
        String output = xmlToString(doc);
        return output;
    }

    private String xmlToString(Document doc) {
        String output = "";

        try {

            // в строку
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer;

            transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

            StringWriter writer = new StringWriter();

            transformer.transform(new DOMSource(doc), new StreamResult(writer));
            //String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
            output = writer.getBuffer().toString();
        }
        catch (Exception ex) {
            output = "error is " + ex.getMessage();
        }

        return output;
    }

    /**
     * упаковка плана приема
     *
     * @param rd
     * @param doc
     * @param el_RequestedEnrollmentDirection
     */
    private Map<Long, WrapperEnrolmentDirectionStatistic> mapEnrolmentDirectionStatistic = new HashMap<Long, WrapperEnrolmentDirectionStatistic>();

    private void _packPlan(
            EnrollmentDirection ed
            , Document doc
            , Element element
            , List<EntrantState> entrantState
    )
    {
        Long key = ed.getId();

        WrapperEnrolmentDirectionStatistic statistic = null;
        if (mapEnrolmentDirectionStatistic.containsKey(key))
            statistic = mapEnrolmentDirectionStatistic.get(key);
        else {
            statistic = _calculateEnrollmentDirectionStatistic(ed, entrantState);
            mapEnrolmentDirectionStatistic.put(key, statistic);
        }

        // упаковка
        Element el_statistic = doc.createElement("Statistic");
        element.appendChild(el_statistic);

        // добавим ссылки
        _makeEducationOrgUnitAttr(doc, el_statistic, ed.getEducationOrgUnit());
        // ссылка на направление приема
        _makeAttribute(doc, el_statistic, "enrollmentDirectionId", Long.toString(ed.getId()));
        _makeAttribute(doc, el_statistic, "educationOrgUnitId", Long.toString(ed.getEducationOrgUnit().getId()));


        _makeAttribute(doc, el_statistic, "ministerialPlan", Integer.toString(statistic.MinisterialPlan));
        _makeAttribute(doc, el_statistic, "contractPlan", Integer.toString(statistic.ContractPlan));
        _makeAttribute(doc, el_statistic, "targetAdmissionPlanBudget", Integer.toString(statistic.TargetAdmissionPlanBudget));
        _makeAttribute(doc, el_statistic, "targetAdmissionPlanContract", Integer.toString(statistic.TargetAdmissionPlanContract));

        List<EnrolmentDirectionRow> lst = statistic.rowStatistic;
        for (EnrolmentDirectionRow row : lst) {
            Element el_row = doc.createElement("StatisticRow");
            el_statistic.appendChild(el_row);

            _packCodeTitle(el_row, doc, "CompensationType", row.compensationType.getCode(), row.compensationType.getShortTitle());
            _packCodeTitle(el_row, doc, "CompetitionKind", row.competitionKind.getCode(), row.competitionKind.getShortTitle());
            _packCodeTitle(el_row, doc, "EntrantState", row.entrantState.getCode(), row.entrantState.getShortTitle());
            _packCodeTitle(el_row, doc, "IsTarget", "value", Boolean.toString(row.IsTarget));

            Element el_row_count = doc.createElement("DirectionCount");
            el_row.appendChild(el_row_count);
            _makeAttribute(doc, el_row_count, "Count", Integer.toString(row.count));

        }
    }

    private void _packCodeTitle
            (
                    Element rootElement
                    , Document doc
                    , String attrName
                    , String code
                    , String title
            )
    {

        Element el = doc.createElement(attrName);
        rootElement.appendChild(el);


        Attr attr_code = doc.createAttribute("code");
        attr_code.setValue(code);
        el.setAttributeNode(attr_code);


        Attr attr_title = doc.createAttribute("title");
        attr_title.setValue(title);
        el.setAttributeNode(attr_title);


    }

    private void _makeAttribute(Document doc, Element element, String attrName, String attrValue)
    {
        Attr attr = doc.createAttribute(attrName);
        attr.setValue(attrValue);
        element.setAttributeNode(attr);
    }

    private void _makeEducationOrgUnitAttr(Document doc, Element el_EducationOrgUnit,
                                           EducationOrgUnit educationOrgUnit)
    {


        String okso = educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getOkso();


        String qual = "";
        if (educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getQualification() != null)
            qual = educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getQualification().getCode();

        String developPeriodCode = "";
        String developPeriodTitle = "";

        if (educationOrgUnit.getDevelopPeriod() != null) {
            developPeriodCode = educationOrgUnit.getDevelopPeriod().getCode();
            developPeriodTitle = educationOrgUnit.getDevelopPeriod().getTitle();
        }

        if (okso == null && educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getParentLevel() != null)
            okso = educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getParentLevel().getOkso();

        // задача 3333
        //String title = educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getTitle();
        String title = educationOrgUnit.getEducationLevelHighSchool().getTitle();
        if (title == null)
            title = educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getTitle();
        if (title == null && educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getParentLevel() != null)
            title = educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getParentLevel().getTitle();

        if (qual == null && educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getParentLevel() != null)
            qual = educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getParentLevel().getQualification().getCode();

        Attr attr_okso = doc.createAttribute("okso");
        attr_okso.setValue(okso);

        Attr attr_qual = doc.createAttribute("qualification");
        attr_qual.setValue(qual);

        Attr attr_title = doc.createAttribute("title");
        attr_title.setValue(title);


        Attr attr_developPeriodCode = doc.createAttribute("developPeriodCode");
        attr_developPeriodCode.setValue(developPeriodCode);

        Attr attr_developPeriodTitle = doc.createAttribute("developPeriodTitle");
        attr_developPeriodTitle.setValue(developPeriodTitle);

        // попросили вставить id
        _makeAttribute(doc, el_EducationOrgUnit, "educationOrgUnitId", Long.toString(educationOrgUnit.getId()));


        el_EducationOrgUnit.setAttributeNode(attr_okso);
        el_EducationOrgUnit.setAttributeNode(attr_qual);
        el_EducationOrgUnit.setAttributeNode(attr_title);

        el_EducationOrgUnit.setAttributeNode(attr_developPeriodCode);
        el_EducationOrgUnit.setAttributeNode(attr_developPeriodTitle);
    }

    private WrapperEnrolmentDirectionStatistic _calculateEnrollmentDirectionStatistic
            (
                    EnrollmentDirection ed
                    , List<EntrantState> stateList
            )
    {
        WrapperEnrolmentDirectionStatistic retVal = new WrapperEnrolmentDirectionStatistic();
        EnrollmentDirection direction = ed;

        retVal.MinisterialPlan = direction.getMinisterialPlan() == null ? 0 : direction.getMinisterialPlan().intValue();
        retVal.ContractPlan = direction.getContractPlan() == null ? 0 : direction.getContractPlan().intValue();
        retVal.TargetAdmissionPlanBudget = direction.getTargetAdmissionPlanBudget() == null ? 0 : direction.getTargetAdmissionPlanBudget().intValue();
        retVal.TargetAdmissionPlanContract = direction.getTargetAdmissionPlanContract() == null ? 0 : direction.getTargetAdmissionPlanContract().intValue();

        // вычислем итоговую статистику
        // по данному направлению

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(RequestedEnrollmentDirection.class, "r")

                .column(RequestedEnrollmentDirection.compensationType().id().fromAlias("r").s())
                .column(RequestedEnrollmentDirection.competitionKind().id().fromAlias("r").s())
                .column(RequestedEnrollmentDirection.state().id().fromAlias("r").s())
                        // признак целевого приема
                .column(RequestedEnrollmentDirection.targetAdmission().fromAlias("r").s())

                .column(DQLFunctions.count("r"))
                .group(DQLExpressions.property(RequestedEnrollmentDirection.compensationType().id().fromAlias("r")))
                .group(DQLExpressions.property(RequestedEnrollmentDirection.competitionKind().id().fromAlias("r")))
                .group(DQLExpressions.property(RequestedEnrollmentDirection.state().id().fromAlias("r")))
                .group(DQLExpressions.property(RequestedEnrollmentDirection.targetAdmission().fromAlias("r")))

                .distinct()
                .where(eq(property(RequestedEnrollmentDirection.enrollmentDirection().fromAlias("r")), value(direction)));

        if (!stateList.isEmpty())
            dql.where(in(property(RequestedEnrollmentDirection.state().fromAlias("r")), stateList));

        List<Object[]> lst = dql.createStatement(getSession()).list();
        for (Object[] objs : lst) {
            WrapperEnrolmentDirectionStatistic.EnrolmentDirectionRow row = new EnrolmentDirectionRow();
            Long compensationTypeId = (Long) objs[0];
            Long competitionKindId = (Long) objs[1];
            Long stateId = (Long) objs[2];
            boolean isTarget = (Boolean) objs[3];

            Integer count = ((Long) objs[4]).intValue();

            row.count = count;
            row.entrantState = (EntrantState) _getEntityById(stateId);
            row.compensationType = (CompensationType) _getEntityById(compensationTypeId);
            row.competitionKind = (CompetitionKind) _getEntityById(competitionKindId);
            row.IsTarget = isTarget;
            retVal.rowStatistic.add(row);
        }

        return retVal;
    }

    private IEntity _getEntityById(Long id)
    {
        if (mapIEntity.containsKey(id))
            return mapIEntity.get(id);
        else {
            IEntity ent = get(id);
            mapIEntity.put(id, ent);
            return ent;
        }

    }

    private Element _packEntrantRequest(Element rootElement, Document doc,
                                        Map<Long, EnrollmentDirection> mapForPlan, EntrantRequest er)
    {

        // пакуем каждую заяву
        Element entrantrequest = doc.createElement("entrantrequest");
        rootElement.appendChild(entrantrequest);

        // создаем атрибуты (номер заявления + фио абитуриента)

        Attr attr_RegNumber = doc.createAttribute("RegNumber");
        attr_RegNumber.setValue(Integer.toString(er.getRegNumber()));
        entrantrequest.setAttributeNode(attr_RegNumber);

        // дата регистрации
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        String regDate = sdf.format(er.getRegDate());

        //DateFormattingUtil.STANDARD_DATE_FORMAT.format(er.getRegDate());

        Attr attr_RegDate = doc.createAttribute("RegDate");
        attr_RegDate.setValue(regDate);
        entrantrequest.setAttributeNode(attr_RegDate);

        // фамилия имя отчество

        Attr attr_lastname = doc.createAttribute("lastname");
        attr_lastname.setValue(er.getEntrant().getPerson().getIdentityCard().getLastName());
        entrantrequest.setAttributeNode(attr_lastname);

        Attr attr_firstname = doc.createAttribute("firstname");
        attr_firstname.setValue(er.getEntrant().getPerson().getIdentityCard().getFirstName());
        entrantrequest.setAttributeNode(attr_firstname);


        Attr attr_middlename = doc.createAttribute("middlename");
        attr_middlename.setValue(er.getEntrant().getPerson().getIdentityCard().getMiddleName());
        entrantrequest.setAttributeNode(attr_middlename);

        // средний балл аттестата
        _packAttestatInfo(entrantrequest, doc, er);

        // все направления подготовки
        List<RequestedEnrollmentDirection> rds = _getRequestEnrolmentDirection(er, null);

        Element el_RequestedEnrollmentDirections = doc.createElement("requestedEnrollmentDirections");
        entrantrequest.appendChild(el_RequestedEnrollmentDirections);


        for (RequestedEnrollmentDirection rd : rds) {
            // каждое направление в отдельный элемент
            Element el_RequestedEnrollmentDirection = doc.createElement("requestedEnrollmentDirection");
            el_RequestedEnrollmentDirections.appendChild(el_RequestedEnrollmentDirection);

            Attr attr_priority = doc.createAttribute("priority");
            attr_priority.setValue(Integer.toString(rd.getPriority()));
            el_RequestedEnrollmentDirection.setAttributeNode(attr_priority);
            _makeEducationOrgUnitAttr(doc, el_RequestedEnrollmentDirection, rd.getEnrollmentDirection().getEducationOrgUnit());

            // ссылка на EnrollmentDirection (для правильного поиска статистики)
            _makeAttribute(doc, el_RequestedEnrollmentDirection, "enrollmentDirectionId", Long.toString(rd.getEnrollmentDirection().getId()));
            _makeAttribute(doc, el_RequestedEnrollmentDirection, "educationOrgUnitId", Long.toString(rd.getEnrollmentDirection().getEducationOrgUnit().getId()));

            // статус
            EntrantState state = rd.getState();
            _packCodeTitle(el_RequestedEnrollmentDirection, doc, "entrantState", state.getCode(), state.getTitle());

            // оригинал копия
            _packOriginalOrCopy(el_RequestedEnrollmentDirection, doc, rd);

            // виды конкурса
            CompetitionKind competitionKind = rd.getCompetitionKind();
            _packCodeTitle(el_RequestedEnrollmentDirection, doc, "competitionKind", competitionKind.getCode(), competitionKind.getTitle());


            OrgUnit ou = rd.getEnrollmentDirection().getEducationOrgUnit().getFormativeOrgUnit();
            _packOrgUnitToXml(el_RequestedEnrollmentDirection, doc, ou);


            // форма освоения
            CompensationType compensationType = rd.getCompensationType();
            DevelopForm developForm = rd.getEnrollmentDirection().getEducationOrgUnit().getDevelopForm();

            _makeICatalogItem("developform", doc, el_RequestedEnrollmentDirection, developForm);
            _makeICatalogItem("compensationType", doc, el_RequestedEnrollmentDirection, compensationType);

            // получаем все оценки, в цикле вкладываем в marks
            _makeMarkPart(rd, doc, el_RequestedEnrollmentDirection);

            _packTargetOrganisation(rd, doc, el_RequestedEnrollmentDirection);

            Long keyPlan = rd.getEnrollmentDirection().getId();
            if (!mapForPlan.containsKey(keyPlan))
                mapForPlan.put(keyPlan, rd.getEnrollmentDirection());
            // _packPlan(rd.getEnrollmentDirection(), doc, el_RequestedEnrollmentDirection);

        }
        return entrantrequest;
    }

    private void _packOriginalOrCopy
            (
                    Element rootElement
                    , Document doc
                    , RequestedEnrollmentDirection rd
            )
    {

        // оригинал документов
        boolean hasOriginalDocument = rd.isOriginalDocumentHandedIn();

        Element el_originalDocument = doc.createElement("originalDocument");
        rootElement.appendChild(el_originalDocument);


        Attr attr_original = doc.createAttribute("val");
        attr_original.setValue(Boolean.toString(hasOriginalDocument));
        el_originalDocument.setAttributeNode(attr_original);

        Attr attr_original_title = doc.createAttribute("title");
        if (hasOriginalDocument)
            attr_original_title.setValue("оригинал");
        else
            attr_original_title.setValue("копия");

        el_originalDocument.setAttributeNode(attr_original_title);

    }

    private void _packTargetOrganisation(
            RequestedEnrollmentDirection rd
            , Document doc
            , Element element)
    {
        Element elementTargets = doc.createElement("targetAdmissionKinds");
        element.appendChild(elementTargets);

        if (rd.isTargetAdmission()) {
            String targetAdmissionKindCode = "";
            String targetAdmissionKindTitle = "";
            String targetAdmissionKindShortTitle = "";

            if (rd.getTargetAdmissionKind() != null) {
                targetAdmissionKindCode = rd.getTargetAdmissionKind().getCode();
                targetAdmissionKindTitle = rd.getTargetAdmissionKind().getTitle();
                targetAdmissionKindShortTitle = rd.getTargetAdmissionKind().getShortTitle();
            }

            Element elementTarget = doc.createElement("targetAdmissionKind");
            elementTargets.appendChild(elementTarget);

            _makeAttribute(doc, elementTarget, "targetAdmissionKindCode", targetAdmissionKindCode);
            _makeAttribute(doc, elementTarget, "targetAdmissionKindTitle", targetAdmissionKindTitle);
            _makeAttribute(doc, elementTarget, "targetAdmissionKindShortTitle", targetAdmissionKindShortTitle);
            _makeAttribute(doc, elementTarget, "isTargetAdmission", Boolean.toString(rd.isTargetAdmission()));

        }
    }

    private MultiKey getEntranceDisciplineType
            (
                    EnrollmentDirection ed
                    , StudentCategory studentCategory
                    , Discipline2RealizationWayRelation wayRelation)
    {

        String key = Long.toString(wayRelation.getId()) + Long.toString(ed.getId());

        if (mapEntranceDisciplineType.containsKey(key))
            return mapEntranceDisciplineType.get(key);

        // предмет
        List<Group2DisciplineRelation> lstGroup = getList(Group2DisciplineRelation.class, Group2DisciplineRelation.discipline(), wayRelation);
        DQLSelectBuilder dql = new DQLSelectBuilder();
        // у нас группа дисциплин
        if (!lstGroup.isEmpty()) {
            Group2DisciplineRelation groupDiscipline = lstGroup.get(0);

            dql.fromEntity(EntranceDiscipline.class, "ed");

            dql.where(DQLExpressions.eq(
                    DQLExpressions.property(EntranceDiscipline.discipline().id().fromAlias("ed")), DQLExpressions.value(groupDiscipline.getGroup().getId())));

            // по направлению для приема
            dql.where(DQLExpressions.eq(
                    DQLExpressions.property(EntranceDiscipline.enrollmentDirection().id().fromAlias("ed")), DQLExpressions.value(ed.getId())));

            // категория
            if (studentCategory != null)
                dql.where(DQLExpressions.eq(
                        DQLExpressions.property(EntranceDiscipline.studentCategory().id().fromAlias("ed")), DQLExpressions.value(studentCategory.getId())));
        }
        else {
            // дисциплина не из группы дисциплин
            dql.fromEntity(EntranceDiscipline.class, "ed");

            dql.where(DQLExpressions.eq(
                    DQLExpressions.property(EntranceDiscipline.discipline().id().fromAlias("ed")), DQLExpressions.value(wayRelation.getId())));

            // по направлению для приема
            dql.where(DQLExpressions.eq(
                    DQLExpressions.property(EntranceDiscipline.enrollmentDirection().id().fromAlias("ed")), DQLExpressions.value(ed.getId())));

            // категория
            if (studentCategory != null)
                dql.where(DQLExpressions.eq(
                        DQLExpressions.property(EntranceDiscipline.studentCategory().id().fromAlias("ed")), DQLExpressions.value(studentCategory.getId())));

        }

        // работаем по 1-ой дисциплине
        List<EntranceDiscipline> entranceDisciplines = dql.createStatement(getSession()).list();

        // затычка (есть еще дисциплины по выбору)
        if (entranceDisciplines.isEmpty()) {
            dql = new DQLSelectBuilder();
            dql.fromEntity(EntranceDiscipline2SetDisciplineRelation.class, "ed");
            dql.column(DQLExpressions.property(EntranceDiscipline2SetDisciplineRelation.entranceDiscipline().fromAlias("ed")));
            // по setDiscipline
            dql.where(DQLExpressions.eq(
                    DQLExpressions.property(EntranceDiscipline2SetDisciplineRelation.setDiscipline().id().fromAlias("ed")), DQLExpressions.value(wayRelation.getId())));

            // по направлению для приема
            dql.where(DQLExpressions.eq(
                    DQLExpressions.property(EntranceDiscipline2SetDisciplineRelation.entranceDiscipline().enrollmentDirection().id().fromAlias("ed")), DQLExpressions.value(ed.getId())));

            // категория поступающего
            if (studentCategory != null)
                dql.where(DQLExpressions.eq(
                        DQLExpressions.property(EntranceDiscipline2SetDisciplineRelation.entranceDiscipline().studentCategory().id().fromAlias("ed")), DQLExpressions.value(studentCategory.getId())));

            entranceDisciplines = dql.createStatement(getSession()).list();
        }

        EntranceDisciplineType type = null;
        EntranceDisciplineKind kind = null;

        if (!entranceDisciplines.isEmpty()) {
            EntranceDiscipline entranceiscipline = entranceDisciplines.get(0);
            type = entranceiscipline.getType();
            kind = entranceiscipline.getKind();

        }
        MultiKey mk = new MultiKey(type, kind);
        mapEntranceDisciplineType.put(key, mk);
        return mk;
    }

    private void _makeMarkPart(RequestedEnrollmentDirection requestEnrollmentDirection, Document doc, Element parentElement)
    {
        // оценки, в том числе олимпиады (по вы)
        Element el_marks = doc.createElement("marks");
        parentElement.appendChild(el_marks);


        List<ChosenEntranceDiscipline> lst = new ArrayList<ChosenEntranceDiscipline>();
        Long key = requestEnrollmentDirection.getId();
        if (mapChosenEntranceDiscipline.containsKey(key))
            lst = mapChosenEntranceDiscipline.get(key);


        Map<Long, MarkInfo> _mapId = new HashMap<Long, MarkInfo>();

        for (ChosenEntranceDiscipline r : lst) {
            EnrollmentDirection ed = requestEnrollmentDirection.getEnrollmentDirection();

            StudentCategory studentCategory = requestEnrollmentDirection.getStudentCategory();
            Discipline2RealizationWayRelation wayRelation = r.getEnrollmentCampaignDiscipline();
            MultiKey mk = getEntranceDisciplineType(ed, studentCategory, wayRelation);

            EntranceDisciplineType type = mk.getKey(0) == null ? null : (EntranceDisciplineType) mk.getKey(0);
            EntranceDisciplineKind kind = mk.getKey(1) == null ? null : (EntranceDisciplineKind) mk.getKey(1);

            int ball_previos = 0;
            int ball_current = 0;

            MarkInfo mi = null;

            // дисциплины должны быть строго уникальны
            // + по дисциплине желательно брать максимум
            Long d_id = wayRelation.getId();
            if (_mapId.containsKey(d_id)) {
                mi = _mapId.get(d_id);
                ball_previos = mi.Mark;
            }
            else {

                mi = new MarkInfo();

                double d = 0;
                if (r.getFinalMark() != null)
                    d = r.getFinalMark();
                mi.Mark = (int) d;
                mi.Code = wayRelation.getEducationSubject().getCode();
                mi.Title = wayRelation.getEducationSubject().getTitle();
                mi.ShortTitle = wayRelation.getEducationSubject().getShortTitle();
                mi.EntranceDisciplineType = type;
                mi.EntranceDisciplineKind = kind;

                ball_current = mi.Mark;
                _mapId.put(d_id, mi);
            }

            // проверяем на максимум
            if (r.getFinalMark() != null) {
                double d = r.getFinalMark();
                ball_current = (int) d;
            }

            if (ball_current >= ball_previos) {
                mi.Mark = ball_current;
                mi.MarkSource = 0;
                if (r.getFinalMarkSource() != null)
                    mi.MarkSource = r.getFinalMarkSource();
            }
        }

        int finalBall = 0;

        // теперь сыпем подготовленную коллекцию в xml
        for (Long id : _mapId.keySet()) {
            MarkInfo mi = _mapId.get(id);


            Element el_mark = doc.createElement("mark");
            el_marks.appendChild(el_mark);

            // оценку в атрибутах
            _makeAttribute(doc, el_mark, "code", mi.Code);
            _makeAttribute(doc, el_mark, "title", mi.Title);
            _makeAttribute(doc, el_mark, "shorttitle", mi.ShortTitle);
            _makeAttribute(doc, el_mark, "mark", Integer.toString(mi.Mark));
            _makeAttribute(doc, el_mark, "markSourceCode", Integer.toString(mi.MarkSource));
            _makeAttribute(doc, el_mark, "markSourceTitle", getFinalMarkSrs(mi.MarkSource));

            // EntranceDisciplineType
            String entranceDisciplineTypeCode = "";
            if (mi.EntranceDisciplineType != null)
                entranceDisciplineTypeCode = mi.EntranceDisciplineType.getCode();
            _makeAttribute(doc, el_mark, "entranceDisciplineTypeCode", entranceDisciplineTypeCode);

            String entranceDisciplineTypeShortTitle = "";
            if (mi.EntranceDisciplineType != null)
                entranceDisciplineTypeShortTitle = mi.EntranceDisciplineType.getShortTitle();
            _makeAttribute(doc, el_mark, "entranceDisciplineTypeShortTitle", entranceDisciplineTypeShortTitle);


            String entranceDisciplineKindCode = mi.EntranceDisciplineKind == null ? "" : mi.EntranceDisciplineKind.getCode();
            String entranceDisciplineKindShortTitle = mi.EntranceDisciplineKind == null ? "" : mi.EntranceDisciplineKind.getShortTitle();

            _makeAttribute(doc, el_mark, "entranceDisciplineKindCode", entranceDisciplineKindCode);
            _makeAttribute(doc, el_mark, "entranceDisciplineKindShortTitle", entranceDisciplineKindShortTitle);


            finalBall += mi.Mark;
        }

        // упакуем финальную оценку
        _packCodeTitle(parentElement, doc, "summaryMark", "total", Integer.toString(finalBall));
    }

    private String getFinalMarkSrs(int code)
    {

		/*
	    int FINAL_MARK_OLYMPIAD = 1;   // по диплому олимпиады
	    int FINAL_MARK_DISCIPLINE_EXAM = 2;      // по дисциплине (экзамен)
	    int FINAL_MARK_APPEAL_EXAM = 3;          // по апелляции (экзамен)
	    int FINAL_MARK_DISCIPLINE_TEST = 4;      // по дисциплине (тестирование)
	    int FINAL_MARK_APPEAL_TEST = 5;          // по апелляции (тестирование)
	    int FINAL_MARK_DISCIPLINE_INTERVIEW = 6; // по дисциплине (собеседование)
	    int FINAL_MARK_APPEAL_INTERVIEW = 7;     // по апелляции (собеседование)
	    int FINAL_MARK_STATE_EXAM_INTERNAL = 8;  // по дисциплине (ЕГЭ (вуз))
	    int FINAL_MARK_STATE_EXAM_INTERNAL_APPEAL = 9;  // по апелляции (ЕГЭ (вуз))
	    int FINAL_MARK_STATE_EXAM_WAVE1 = 10; // финальная оценка вычислена по ЕГЭ (1-ая волна)
	    int FINAL_MARK_STATE_EXAM_WAVE2 = 11; // финальная оценка вычислена по ЕГЭ (2-ая волна)
	    */

        switch (code) {
            case 0:
                return "?";
            case 1:
                return "О";
            case 2:
                return "Э";
            case 3:
                return "АЭ";
            case 4:
                return "ТД";
            case 5:
                return "АТ";
            case 6:
                return "СД";
            case 7:
                return "АС";
            case 8:
                return "Е";
            case 9:
                return "АЕ";
            case 10:
                return "Е";
            case 11:
                return "Е";
        }

        return "НЕИЗВЕСТНО";

    }

    private void _makeICatalogItem(
            String elementName
            , Document doc
            , Element parentElement
            , org.tandemframework.common.catalog.entity.ICatalogItem item)
    {
        Element el_item = doc.createElement(elementName);
        parentElement.appendChild(el_item);

        Attr attr_code = doc.createAttribute("code");
        attr_code.setValue(item.getCode());
        el_item.setAttributeNode(attr_code);

        Attr attr_title = doc.createAttribute("title");
        attr_title.setValue(item.getTitle());
        el_item.setAttributeNode(attr_title);

    }

    private Element _packOrgUnitToXml
            (
                    Element element
                    , Document doc
                    , OrgUnit orgUnit
            )
    {
        Element el = doc.createElement("orgUnit");
        element.appendChild(el);

        Attr attr_code = doc.createAttribute("divisionCode");
        attr_code.setValue(orgUnit.getDivisionCode());
        el.setAttributeNode(attr_code);

        Attr attr_title = doc.createAttribute("title");
        attr_title.setValue(orgUnit.getTitle());
        el.setAttributeNode(attr_title);
        return el;
    }

    private List<RequestedEnrollmentDirection> _getRequestEnrolmentDirection(EntrantRequest request, CompensationType compensationType)
    {
        List<RequestedEnrollmentDirection> lst = mapRequestedEnrollmentDirection.get(request.getId());

        if (compensationType != null && !lst.isEmpty()) {
            List<RequestedEnrollmentDirection> rVal = new ArrayList<RequestedEnrollmentDirection>();
            for (RequestedEnrollmentDirection rd : lst) {
                if (rd.getCompensationType().equals(compensationType))
                    rVal.add(rd);
            }

            return rVal;
        }

        return lst;
    }

    private void _packAttestatInfo(
            Element rootElement
            , Document doc
            , EntrantRequest er)
    {
        Element element = doc.createElement("personEduInstitutions");
        rootElement.appendChild(element);
        Long key = er.getEntrant().getPerson().getId();

        if (mapEduInstitytionInfo.containsKey(key)) {
            List<EduInstitytionInfo> lst = mapEduInstitytionInfo.get(key);
            for (EduInstitytionInfo eii : lst) {
                Element elementEduInstitytionInfo = doc.createElement("personEduInstitution");
                element.appendChild(elementEduInstitytionInfo);

                _makeAttribute(doc, elementEduInstitytionInfo, "title", eii.AttestatInfo);
                _makeAttribute(doc, elementEduInstitytionInfo, "personId", Long.toString(eii.PersonId));
                _makeAttribute(doc, elementEduInstitytionInfo, "isMain", Boolean.toString(eii.IsMain));
                _makeAttribute(doc, elementEduInstitytionInfo, "Avg", eii.AvgMark() == null ? "" : Double.toString(eii.AvgMark()));
                _makeAttribute(doc, elementEduInstitytionInfo, "mark3", Integer.toString(eii.Mark3));
                _makeAttribute(doc, elementEduInstitytionInfo, "mark4", Integer.toString(eii.Mark4));
                _makeAttribute(doc, elementEduInstitytionInfo, "mark5", Integer.toString(eii.Mark5));
            }
        }
    }

    private void clearCache()
    {
        mapEntranceDisciplineType = new HashMap<String, MultiKey>();
        mapEduInstitytionInfo = new HashMap<Long, List<EduInstitytionInfo>>();
        mapRequestedEnrollmentDirection = null;
        mapChosenEntranceDiscipline = null;
        mapIEntity = new HashMap<Long, IEntity>();
    }

    /**
     * Получить приемную компанию по ее id
     *
     * @param enrollmentCampaign
     *
     * @return
     */
    private EnrollmentCampaign getEnrollmentCampaignById(String enrollmentCampaign)
    {
        Long id = Long.parseLong(enrollmentCampaign);
        EnrollmentCampaign ec = get(EnrollmentCampaign.class, id);
        return ec;
    }

    private List<EntrantState> getEntrantStates(List<String> lst)
    {
        List<EntrantState> lstState = new ArrayList<EntrantState>();
        for (String s : lst) {
            lstState.add(getCatalogItem(EntrantState.class, s));
        }
        return lstState;
    }

    private DQLSelectBuilder _getEntrantRequestIdDql(
            EnrollmentCampaign compaign
            , Date date
            , List<EntrantState> entrantState)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EntrantRequest.class, "rIn")
                .column(EntrantRequest.id().fromAlias("rIn").s())
                .where(lt(property(EntrantRequest.regDate().fromAlias("rIn")), valueDate(date)))
                .where(in(property(EntrantRequest.entrant().state().fromAlias("rIn")), entrantState))
                .where(eq(property(EntrantRequest.entrant().archival().fromAlias("rIn")), value(false)))
                .where(eq(property(EntrantRequest.entrant().enrollmentCampaign().fromAlias("rIn")), value(compaign)));

        return dql;
    }

    private void _fillMapPersonEduInstitution(
            EnrollmentCampaign compaingn
            , Date curDate
            , List<EntrantState> entrantState
    )
    {

        DQLSelectBuilder dqlInER = _getEntrantRequestIdDql(
                compaingn
                , curDate
                , entrantState);

        // основной диплом об образовании
        DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(EntrantRequest.class, "personIn")
                .column(EntrantRequest.entrant().person().personEduInstitution().id().fromAlias("personIn").s())
                .where(DQLExpressions.in(
                        DQLExpressions.property(EntrantRequest.id().fromAlias("personIn")),
                        dqlInER.buildQuery()));


        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(PersonEduInstitution.class, "pei")

                .column(PersonEduInstitution.person().id().fromAlias("pei").s())
                .column(PersonEduInstitution.seria().fromAlias("pei").s())
                .column(PersonEduInstitution.number().fromAlias("pei").s())
                        // .column(PersonEduInstitution.main().fromAlias("pei").s())
                .column(PersonEduInstitution.mark3().fromAlias("pei").s())
                .column(PersonEduInstitution.mark4().fromAlias("pei").s())
                .column(PersonEduInstitution.mark5().fromAlias("pei").s())

                .where(DQLExpressions.in(
                        DQLExpressions.property(PersonEduInstitution.id().fromAlias("pei")),
                        dqlIn.buildQuery()));

        List<Object[]> lst = dql.createStatement(getSession()).list();

        for (Object[] objs : lst) {
            Long id = (Long) objs[0];
            String title = "";
            if (objs[1] != null)
                title += "серия " + (String) objs[1];

            if (objs[2] != null)
                title += " номер " + (String) objs[2];


            boolean main = true; //(Boolean) objs[3];

            int mark3 = objs[3] == null ? 0 : (Integer) objs[3];
            int mark4 = objs[4] == null ? 0 : (Integer) objs[4];
            int mark5 = objs[5] == null ? 0 : (Integer) objs[5];
            //int mark3 =  (Integer) objs[3];
            //int mark4 = (Integer) objs[4];
            //int mark5 = (Integer) objs[5];


            EduInstitytionInfo pii = new EduInstitytionInfo();
            pii.PersonId = id;
            pii.AttestatInfo = title;
            pii.Mark3 = mark3;
            pii.Mark4 = mark4;
            pii.Mark5 = mark5;
            pii.IsMain = main;

            List<EduInstitytionInfo> lstMap = null;
            if (mapEduInstitytionInfo.containsKey(id))
                lstMap = mapEduInstitytionInfo.get(id);
            else {
                lstMap = new ArrayList<EduInstitytionInfo>();
                mapEduInstitytionInfo.put(id, lstMap);
            }
            lstMap.add(pii);
        }
    }

    private void _fillMapChosenEntranceDiscipline
            (
                    EnrollmentCampaign compaign
                    , Date date
                    , List<EntrantState> entrantState
            )
    {

        mapChosenEntranceDiscipline = new HashMap<Long, List<ChosenEntranceDiscipline>>();

        DQLSelectBuilder dqlIn = _getEntrantRequestIdDql(compaign, date, entrantState);


        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(ChosenEntranceDiscipline.class, "r")
                .column("r")
                .where(in(property(ChosenEntranceDiscipline.chosenEnrollmentDirection().entrantRequest().id().fromAlias("r")), dqlIn.buildQuery()));

        List<ChosenEntranceDiscipline> lst = dql.createStatement(getSession()).list();

        for (ChosenEntranceDiscipline cd : lst) {
            Long key = cd.getChosenEnrollmentDirection().getId();

            List<ChosenEntranceDiscipline> lstCE = null;
            if (mapChosenEntranceDiscipline.containsKey(key))
                lstCE = mapChosenEntranceDiscipline.get(key);
            else {
                lstCE = new ArrayList<ChosenEntranceDiscipline>();
                mapChosenEntranceDiscipline.put(key, lstCE);
            }
            lstCE.add(cd);
        }
    }

    private void _fillMapRequestedEnrollmentDirection
            (
                    EnrollmentCampaign compaign
                    , Date date
                    , List<EntrantState> entrantState
            )
    {
        mapRequestedEnrollmentDirection = new HashMap<Long, List<RequestedEnrollmentDirection>>();

        DQLSelectBuilder dqlIn = _getEntrantRequestIdDql(compaign, date, entrantState);


        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(RequestedEnrollmentDirection.class, "r")
                .column("r")
                .where(in(property(RequestedEnrollmentDirection.entrantRequest().id().fromAlias("r")), dqlIn.buildQuery()));


        List<RequestedEnrollmentDirection> lst = dql.createStatement(getSession()).list();

        for (RequestedEnrollmentDirection rd : lst) {
            Long key = rd.getEntrantRequest().getId();
            List<RequestedEnrollmentDirection> lstRd = null;
            if (mapRequestedEnrollmentDirection.containsKey(key))
                lstRd = mapRequestedEnrollmentDirection.get(key);
            else {
                lstRd = new ArrayList<RequestedEnrollmentDirection>();
                mapRequestedEnrollmentDirection.put(key, lstRd);
            }

            lstRd.add(rd);
        }
    }

    private void makeCache(EnrollmentCampaign compaingn, Date curDate,
                           List<EntrantState> entrantState)
    {
        _fillMapRequestedEnrollmentDirection(compaingn, curDate, entrantState);
    }

    private List<EntrantRequest> _getEntrantRequest(
            EnrollmentCampaign compaign
            , Date date
            , List<EntrantState> entrantState)
    {
        DQLSelectBuilder dqlIn = _getEntrantRequestIdDql(compaign, date, entrantState);
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EntrantRequest.class, "r")
                .column("r")
                .where(in(property(EntrantRequest.id().fromAlias("r")), dqlIn.buildQuery()))
                        // сортировки
                .order(EntrantRequest.entrant().personalNumber().fromAlias("r").s())
                .order(EntrantRequest.regNumber().fromAlias("r").s());
        List<EntrantRequest> lst = dql.createStatement(getSession()).list();
        return lst;
    }

    private void _setEnrolmentCompaignAttribute(Document doc,
                                                Element element, EnrollmentCampaign compaingn)
    {
        // атрибуты
        Attr attr_id = doc.createAttribute("enrollmentCampaignId");
        attr_id.setValue(Long.toString(compaingn.getId()));

        Attr attr_title = doc.createAttribute("enrollmentCampaignTitle");
        attr_title.setValue(compaingn.getTitle());

        element.setAttributeNode(attr_id);
        element.setAttributeNode(attr_title);

    }

}
