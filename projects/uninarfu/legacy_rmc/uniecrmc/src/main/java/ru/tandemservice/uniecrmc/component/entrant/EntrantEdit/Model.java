package ru.tandemservice.uniecrmc.component.entrant.EntrantEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniecrmc.entity.catalog.SpecialConditionsForEntrant;
import ru.tandemservice.uniecrmc.entity.entrant.EntrantExt;

import java.util.ArrayList;
import java.util.List;

public class Model extends ru.tandemservice.uniec.component.entrant.EntrantEdit.Model {
    private EntrantExt entrantExt;
    private List<SpecialConditionsForEntrant> entrant2SpecialConditions = new ArrayList<>();
    private ISelectModel specialConditionsForEntrantModel;

    public EntrantExt getEntrantExt() {
        return entrantExt;
    }

    public void setEntrantExt(EntrantExt entrantExt) {
        this.entrantExt = entrantExt;
    }

    public ISelectModel getSpecialConditionsForEntrantModel() {
        return specialConditionsForEntrantModel;
    }

    public void setSpecialConditionsForEntrantModel(ISelectModel specialConditionsForEntrantModel) {
        this.specialConditionsForEntrantModel = specialConditionsForEntrantModel;
    }

    public List<SpecialConditionsForEntrant> getEntrant2SpecialConditions() {
        return entrant2SpecialConditions;
    }

    public void setEntrant2SpecialConditions(List<SpecialConditionsForEntrant> entrant2SpecialConditions) {
        this.entrant2SpecialConditions = entrant2SpecialConditions;
    }
}
