package ru.tandemservice.uniecrmc.component.report.EntrantEduLevelDistribution.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniecrmc.entity.entrant.EntrantEduLevelDistributionReport;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.List;

public class Model
        implements IEnrollmentCampaignSelectModel
{
    private DynamicListDataSource<EntrantEduLevelDistributionReport> _dataSource;
    private IDataSettings _settings;
    private List<EnrollmentCampaign> _enrollmentCampaignList;

    public EnrollmentCampaign getEnrollmentCampaign() {
        return (EnrollmentCampaign) getSettings().get("enrollmentCampaign");
    }

    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign) {
        getSettings().set("enrollmentCampaign", enrollmentCampaign);
    }

    public DynamicListDataSource<EntrantEduLevelDistributionReport> getDataSource() {
        return this._dataSource;
    }

    public void setDataSource(DynamicListDataSource<EntrantEduLevelDistributionReport> dataSource) {
        this._dataSource = dataSource;
    }

    public IDataSettings getSettings() {
        return this._settings;
    }

    public void setSettings(IDataSettings settings) {
        this._settings = settings;
    }

    public List<EnrollmentCampaign> getEnrollmentCampaignList() {
        return this._enrollmentCampaignList;
    }

    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList) {
        this._enrollmentCampaignList = enrollmentCampaignList;
    }
}

