package ru.tandemservice.uniecrmc.dao.entrantRating;

import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class ListNode {

    @XmlAttribute
    public String compensationTypeId;

    @XmlAttribute
    public boolean hasAdmission;

    @XmlAttribute(required = false)
    public Integer plan;

    @XmlElement(name = "CompetitionGroups")
    public CompetitionGroupsNode groupsNode;

    public ListNode() {
    }

    public ListNode(EnrollmentDirection enrollmentDirection, boolean budget) {
        this.hasAdmission = budget ? enrollmentDirection.isBudget() : enrollmentDirection.isContract();
        this.plan = budget ? enrollmentDirection.getMinisterialPlan() : enrollmentDirection.getContractPlan();
        this.compensationTypeId = budget ? CompensationTypeCodes.COMPENSATION_TYPE_BUDGET : CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT;

        this.groupsNode = new CompetitionGroupsNode();
    }
}
