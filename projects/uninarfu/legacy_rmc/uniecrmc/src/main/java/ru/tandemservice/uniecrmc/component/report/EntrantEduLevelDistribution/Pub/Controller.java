package ru.tandemservice.uniecrmc.component.report.EntrantEduLevelDistribution.Pub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.shared.commonbase.base.util.UniMap;

public class Controller extends AbstractBusinessController<IDAO, Model> {
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = (Model) getModel(component);

        ((IDAO) getDao()).prepare(model);
    }

    public void onClickPrintReport(IBusinessComponent component) {
        activateInRoot(component, new ComponentActivator("ru.tandemservice.uni.component.reports.DownloadStorableReport", new UniMap().add("reportId", ((Model) getModel(component)).getReport().getId()).add("extension", "xls")));
    }
}
