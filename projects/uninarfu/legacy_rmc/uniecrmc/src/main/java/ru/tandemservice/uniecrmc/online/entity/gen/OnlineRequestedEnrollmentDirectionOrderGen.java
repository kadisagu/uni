package ru.tandemservice.uniecrmc.online.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection;
import ru.tandemservice.uniecrmc.online.entity.OnlineRequestedEnrollmentDirectionOrder;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Приоритет направления подготовки для онлайн абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OnlineRequestedEnrollmentDirectionOrderGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniecrmc.online.entity.OnlineRequestedEnrollmentDirectionOrder";
    public static final String ENTITY_NAME = "onlineRequestedEnrollmentDirectionOrder";
    public static final int VERSION_HASH = -1425891753;
    private static IEntityMeta ENTITY_META;

    public static final String L_ONLINE_REQUESTED_ENROLLMENT_DIRECTION = "onlineRequestedEnrollmentDirection";
    public static final String P_ORDER = "order";

    private OnlineRequestedEnrollmentDirection _onlineRequestedEnrollmentDirection;     // Выбранное онлайн направление приема
    private int _order;     // Приоритет

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выбранное онлайн направление приема. Свойство не может быть null.
     */
    @NotNull
    public OnlineRequestedEnrollmentDirection getOnlineRequestedEnrollmentDirection()
    {
        return _onlineRequestedEnrollmentDirection;
    }

    /**
     * @param onlineRequestedEnrollmentDirection Выбранное онлайн направление приема. Свойство не может быть null.
     */
    public void setOnlineRequestedEnrollmentDirection(OnlineRequestedEnrollmentDirection onlineRequestedEnrollmentDirection)
    {
        dirty(_onlineRequestedEnrollmentDirection, onlineRequestedEnrollmentDirection);
        _onlineRequestedEnrollmentDirection = onlineRequestedEnrollmentDirection;
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     */
    @NotNull
    public int getOrder()
    {
        return _order;
    }

    /**
     * @param order Приоритет. Свойство не может быть null.
     */
    public void setOrder(int order)
    {
        dirty(_order, order);
        _order = order;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OnlineRequestedEnrollmentDirectionOrderGen)
        {
            setOnlineRequestedEnrollmentDirection(((OnlineRequestedEnrollmentDirectionOrder)another).getOnlineRequestedEnrollmentDirection());
            setOrder(((OnlineRequestedEnrollmentDirectionOrder)another).getOrder());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OnlineRequestedEnrollmentDirectionOrderGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OnlineRequestedEnrollmentDirectionOrder.class;
        }

        public T newInstance()
        {
            return (T) new OnlineRequestedEnrollmentDirectionOrder();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "onlineRequestedEnrollmentDirection":
                    return obj.getOnlineRequestedEnrollmentDirection();
                case "order":
                    return obj.getOrder();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "onlineRequestedEnrollmentDirection":
                    obj.setOnlineRequestedEnrollmentDirection((OnlineRequestedEnrollmentDirection) value);
                    return;
                case "order":
                    obj.setOrder((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "onlineRequestedEnrollmentDirection":
                        return true;
                case "order":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "onlineRequestedEnrollmentDirection":
                    return true;
                case "order":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "onlineRequestedEnrollmentDirection":
                    return OnlineRequestedEnrollmentDirection.class;
                case "order":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OnlineRequestedEnrollmentDirectionOrder> _dslPath = new Path<OnlineRequestedEnrollmentDirectionOrder>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OnlineRequestedEnrollmentDirectionOrder");
    }
            

    /**
     * @return Выбранное онлайн направление приема. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.online.entity.OnlineRequestedEnrollmentDirectionOrder#getOnlineRequestedEnrollmentDirection()
     */
    public static OnlineRequestedEnrollmentDirection.Path<OnlineRequestedEnrollmentDirection> onlineRequestedEnrollmentDirection()
    {
        return _dslPath.onlineRequestedEnrollmentDirection();
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.online.entity.OnlineRequestedEnrollmentDirectionOrder#getOrder()
     */
    public static PropertyPath<Integer> order()
    {
        return _dslPath.order();
    }

    public static class Path<E extends OnlineRequestedEnrollmentDirectionOrder> extends EntityPath<E>
    {
        private OnlineRequestedEnrollmentDirection.Path<OnlineRequestedEnrollmentDirection> _onlineRequestedEnrollmentDirection;
        private PropertyPath<Integer> _order;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выбранное онлайн направление приема. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.online.entity.OnlineRequestedEnrollmentDirectionOrder#getOnlineRequestedEnrollmentDirection()
     */
        public OnlineRequestedEnrollmentDirection.Path<OnlineRequestedEnrollmentDirection> onlineRequestedEnrollmentDirection()
        {
            if(_onlineRequestedEnrollmentDirection == null )
                _onlineRequestedEnrollmentDirection = new OnlineRequestedEnrollmentDirection.Path<OnlineRequestedEnrollmentDirection>(L_ONLINE_REQUESTED_ENROLLMENT_DIRECTION, this);
            return _onlineRequestedEnrollmentDirection;
        }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.online.entity.OnlineRequestedEnrollmentDirectionOrder#getOrder()
     */
        public PropertyPath<Integer> order()
        {
            if(_order == null )
                _order = new PropertyPath<Integer>(OnlineRequestedEnrollmentDirectionOrderGen.P_ORDER, this);
            return _order;
        }

        public Class getEntityClass()
        {
            return OnlineRequestedEnrollmentDirectionOrder.class;
        }

        public String getEntityName()
        {
            return "onlineRequestedEnrollmentDirectionOrder";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
