package ru.tandemservice.uniecrmc.base.ext.EcEntrant.logic;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt;
import ru.tandemservice.uniec.base.bo.EcEntrant.logic.EcEntrantDao;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.ProfileKnowledge;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.util.EntrantRequestAddEditUtil;
import ru.tandemservice.uniec.util.EntrantUtil;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class EcEntrantDaoExt extends EcEntrantDao implements IEcEntrantDaoExt {

    @Override
    public void saveOrUpdateEntrantRequest(
            List<RequestedEnrollmentDirection> selectedList,
            List<RequestedEnrollmentDirection> forDeleteList,
            boolean addForm,
            EntrantRequest entrantRequest,
            Integer entrantRequestNumber,
            Integer entrantDirectionNumber,
            Map<Long, List<ProfileKnowledge>> profileKnowledgeMap,
            Map<Long, List<ProfileExaminationMark>> profileMarkMap,
            Map<Long, RequestedEnrollmentDirectionExt> directionExtMap)
    {


        if (selectedList.isEmpty()) {
            throw new ApplicationException("Не выбраны направления подготовки (специальности).");
        }
        Session session = getSession();
        EnrollmentCampaign enrollmentCampaign = entrantRequest.getEntrant().getEnrollmentCampaign();
        session.refresh(enrollmentCampaign);
        ErrorCollector errorCollector = ContextLocal.getUserContext().getErrorCollector();


        EntrantUtil.lockEnrollmentCampaign(session, entrantRequest.getEntrant());
        if (addForm) {
            if (entrantRequestNumber != null) {
                Criteria criteria = session.createCriteria(EntrantRequest.class, "request");
                criteria.createAlias("request.entrant", "entrant");
                criteria.add(Restrictions.eq("entrant.enrollmentCampaign", enrollmentCampaign));
                criteria.add(Restrictions.eq("request.regNumber", entrantRequestNumber));
                criteria.setProjection(Projections.rowCount());

                Number countNumber = (Number) criteria.uniqueResult();
                if ((countNumber != null) && (countNumber.intValue() > 0)) {
                    errorCollector.add("Номер заявления должен быть уникальным в рамках приемной кампании.", new String[]{"regNumber"});
                }
            }
            else {
                Criteria criteria = session.createCriteria(EntrantRequest.class, "request");
                criteria.createAlias("request.entrant", "entrant");
                criteria.add(Restrictions.eq("entrant.enrollmentCampaign", enrollmentCampaign));
                criteria.setProjection(Projections.max("regNumber"));

                Number maxNumber = (Number) criteria.uniqueResult();
                entrantRequestNumber = Integer.valueOf(maxNumber == null ? 1 : maxNumber.intValue() + 1);
            }
            entrantRequest.setRegNumber(entrantRequestNumber.intValue());
        }
        else {
            if (entrantRequestNumber == null) {
                throw new ApplicationException("Номер заявления должен быть указан.");
            }
            Criteria criteria = session.createCriteria(EntrantRequest.class, "request");
            criteria.createAlias("request.entrant", "entrant");
            criteria.add(Restrictions.eq("entrant.enrollmentCampaign", enrollmentCampaign));
            criteria.add(Restrictions.eq("request.regNumber", entrantRequestNumber));
            criteria.add(Restrictions.ne("request.id", entrantRequest.getId()));
            criteria.setProjection(Projections.rowCount());

            Number countNumber = (Number) criteria.uniqueResult();
            if ((countNumber != null) && (countNumber.intValue() > 0)) {
                errorCollector.add("Номер заявления должен быть уникальным в рамках приемной кампании.", new String[]{"regNumber"});
            }
            entrantRequest.setRegNumber(entrantRequestNumber.intValue());
        }
        if ((!enrollmentCampaign.isNumberOnDirectionManual()) || (selectedList.size() != 1)) {
            entrantDirectionNumber = null;
        }
        for (RequestedEnrollmentDirection item : selectedList) {
            if (!item.isTargetAdmission()) {
                item.setExternalOrgUnit(null);
                item.setTargetAdmissionKind(null);
            }
            if ((entrantDirectionNumber != null) && ((item.getId().longValue() < 0L) || (item.getNumber() != entrantDirectionNumber.intValue()))) {
                Criteria c = session.createCriteria(RequestedEnrollmentDirection.class);
                c.add(Restrictions.eq("enrollmentDirection", item.getEnrollmentDirection()));
                c.add(Restrictions.eq("number", entrantDirectionNumber));
                List<RequestedEnrollmentDirection> list = c.list();
                if (list.isEmpty()) {
                    item.setNumber(entrantDirectionNumber.intValue());
                }
                else {
                    errorCollector.add("№ на направлении/специальности «" + entrantDirectionNumber + "» уже занят абитуриентом «" + ((RequestedEnrollmentDirection) list.get(0)).getEntrantRequest().getEntrant().getPerson().getFullFio() + "».", new String[]{"directionNumber"});
                }
            }
        }
        EntrantRequestAddEditUtil.validateRequestedDirections(session, selectedList, forDeleteList, entrantRequest);
        if (errorCollector.hasErrors()) {
            return;
        }
        session.saveOrUpdate(entrantRequest);


        int budgetMaxPriority = -2147483648;
        int contractMaxPriority = -2147483648;
        for (RequestedEnrollmentDirection direction : getList(RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.entrantRequest().entrant(), entrantRequest.getEntrant(), new String[0])) {
            int priority = direction.getPriorityPerEntrant();
            if (direction.getCompensationType().isBudget()) {
                if (priority > budgetMaxPriority) {
                    budgetMaxPriority = priority;
                }
            }
            else if (priority > contractMaxPriority) {
                contractMaxPriority = priority;
            }
        }
        budgetMaxPriority++;
        contractMaxPriority++;
        for (RequestedEnrollmentDirection forDelete : forDeleteList) {
            forDelete.setProfileChosenEntranceDiscipline(null);
            session.update(forDelete);
        }
        session.flush();
        for (RequestedEnrollmentDirection forDelete : forDeleteList) {
            session.delete(forDelete);
        }
        session.flush();


        List<RequestedEnrollmentDirection> existedList = (List<RequestedEnrollmentDirection>) (addForm ? Collections.emptyList() : getList(RequestedEnrollmentDirection.class, "entrantRequest", entrantRequest, new String[]{"priority"}));


        boolean needChangePriorities = (addForm) || (!forDeleteList.isEmpty()) || (!Arrays.equals(selectedList.toArray(), existedList.toArray()));
        if (needChangePriorities) {
            int priority = -1;
            for (RequestedEnrollmentDirection item : existedList) {
                item.setPriority(priority);
                session.update(item);
                priority--;
            }
            session.flush();
        }
        int priority = 1;
        EntrantState activeState = (EntrantState) session.createCriteria(EntrantState.class).add(Restrictions.eq("code", "1")).uniqueResult();
        for (RequestedEnrollmentDirection item : selectedList) {

            List<ProfileKnowledge> profileKnowledgeList = profileKnowledgeMap == null ? null : profileKnowledgeMap.get(item.getId());
            RequestedEnrollmentDirectionExt directionExt = directionExtMap == null ? null : directionExtMap.get(item.getId());
            List<ProfileExaminationMark> profileMarkList = profileMarkMap == null ? null : profileMarkMap.get(item.getId());
            if (profileMarkList != null) {
                for (ProfileExaminationMark profileMark : profileMarkList) {
                    Criteria c = session.createCriteria(ProfileExaminationMark.class);
                    c.add(Restrictions.eq("entrant", profileMark.getEntrant()));
                    c.add(Restrictions.eq("discipline", profileMark.getDiscipline()));
                    ProfileExaminationMark savedProfileMark = (ProfileExaminationMark) c.uniqueResult();
                    if (savedProfileMark == null) {
                        session.save(profileMark);
                    }
                    else {
                        savedProfileMark.setMark(profileMark.getMark());
                        session.update(savedProfileMark);
                    }
                }
            }
            if (item.getId().longValue() < 0L) {
                item.setId(null);
                item.setPriorityPerEntrant(item.getCompensationType().isBudget() ? budgetMaxPriority++ : contractMaxPriority++);
                if (entrantDirectionNumber == null) {
                    Criteria c = session.createCriteria(RequestedEnrollmentDirection.class);
                    c.add(Restrictions.eq("enrollmentDirection", item.getEnrollmentDirection()));
                    c.setProjection(Projections.max("number"));
                    Number maxNumber = (Number) c.uniqueResult();


                    item.setNumber(maxNumber == null ? 1 : maxNumber.intValue() + 1);
                }
            }
            if (needChangePriorities) {
                item.setPriority(priority++);
            }
            if (item.getState() == null) {
                item.setState(activeState);
            }
            session.saveOrUpdate(item);

            if (directionExt != null) {
                directionExt.setRequestedEnrollmentDirection(item);
                session.saveOrUpdate(directionExt);
            }
            if (item.isOriginalDocumentHandedIn()) {
                updateOriginalDocuments(item, true);
            }
            if (profileKnowledgeList != null) {
                for (ProfileKnowledge profileKnowledge : profileKnowledgeList) {
                    Criteria c = session.createCriteria(RequestedProfileKnowledge.class);
                    c.add(Restrictions.eq("profileKnowledge", profileKnowledge));
                    c.add(Restrictions.eq("requestedEnrollmentDirection", item));
                    c.setProjection(Projections.rowCount());
                    Number number = (Number) c.uniqueResult();
                    int count = number == null ? 0 : number.intValue();
                    if (count == 0) {
                        RequestedProfileKnowledge requestedProfileKnowledge = new RequestedProfileKnowledge();
                        requestedProfileKnowledge.setProfileKnowledge(profileKnowledge);
                        requestedProfileKnowledge.setRequestedEnrollmentDirection(item);
                        session.save(requestedProfileKnowledge);
                    }
                }
            }
        }
        doNormalizePriorityPerEntrant(entrantRequest.getEntrant());
    }

}
