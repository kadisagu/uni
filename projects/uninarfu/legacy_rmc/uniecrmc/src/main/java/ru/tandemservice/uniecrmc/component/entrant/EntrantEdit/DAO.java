package ru.tandemservice.uniecrmc.component.entrant.EntrantEdit;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uniecrmc.component.entrant.EntrantUtils;
import ru.tandemservice.uniecrmc.entity.catalog.SpecialConditionsForEntrant;
import ru.tandemservice.uniecrmc.entity.entrant.Entrant2SpecialConditions;

import java.util.List;

public class DAO extends ru.tandemservice.uniec.component.entrant.EntrantEdit.DAO {
    @Override
    public void prepare(ru.tandemservice.uniec.component.entrant.EntrantEdit.Model model)
    {
        super.prepare(model);
        Model modelExt = (Model) model;
        List<Entrant2SpecialConditions> list = getList(Entrant2SpecialConditions.class, Entrant2SpecialConditions.entrant().s(), model.getEntrant());

        modelExt.setEntrantExt(EntrantUtils.getEntrantExt(model.getEntrant()));
        if (!CollectionUtils.isEmpty(list)) {
            List<SpecialConditionsForEntrant> conditions = CommonBaseUtil.getPropertiesList(list, Entrant2SpecialConditions.specialConditionsForEntrant().s());
            modelExt.setEntrant2SpecialConditions(conditions);
        }
        modelExt.setSpecialConditionsForEntrantModel(new LazySimpleSelectModel<>(SpecialConditionsForEntrant.class));

    }

    @Override
    public void update(ru.tandemservice.uniec.component.entrant.EntrantEdit.Model model)
    {
        super.update(model);
        Model modelExt = (Model) model;
        List<Entrant2SpecialConditions> list = getList(Entrant2SpecialConditions.class, Entrant2SpecialConditions.entrant().s(), model.getEntrant());
        getSession().saveOrUpdate(modelExt.getEntrantExt());
        for (Entrant2SpecialConditions item : list) {
            delete(item);
        }
        for (SpecialConditionsForEntrant item : modelExt.getEntrant2SpecialConditions()) {
            Entrant2SpecialConditions specialConditions = new Entrant2SpecialConditions();
            specialConditions.setEntrant(model.getEntrant());
            specialConditions.setSpecialConditionsForEntrant(item);
            getSession().saveOrUpdate(specialConditions);
        }
    }
}
