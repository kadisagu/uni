package ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.EcgCortegeDTO;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EnrollmentRecommendation;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;

import java.util.List;

public class EcgCortegeDTORMC extends EcgCortegeDTO implements IEcgCortegeRMC {

    private List<EnrollmentRecommendation> recommendations;
    private List<WrapChoseEntranseDiscipline> disciplines;
    private boolean haveSpecialRights;
    private Integer enrollmentPriority;

    public EcgCortegeDTORMC(IEntity entity, boolean targetAdmission,
                            TargetAdmissionKind targetAdmissionKind,
                            CompetitionKind competitionKind, Double finalMark,
                            Double profileMark, boolean graduatedProfileEduInstitution,
                            Double certificateAverageMark, String fio,
                            List<EnrollmentRecommendation> recommendations,
                            List<WrapChoseEntranseDiscipline> disciplines,
                            Boolean haveSpecialRights, Integer enrollmentPriority)
    {
        super(entity, targetAdmission, targetAdmissionKind, competitionKind, finalMark,
              profileMark, graduatedProfileEduInstitution, certificateAverageMark,
              fio);
        this.recommendations = recommendations;
        this.disciplines = disciplines;
        this.haveSpecialRights = haveSpecialRights != null && haveSpecialRights.booleanValue();
        this.enrollmentPriority = enrollmentPriority;
    }

    public String getRecommendationTitle() {
        if (this.getRecommendations() != null) {
            List<String> lst = CommonBaseUtil.getPropertiesList(this.getRecommendations(), EnrollmentRecommendation.shortTitle());
            return StringUtils.join(lst, ", ");
        }
        return "";
    }

    public List<EnrollmentRecommendation> getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(List<EnrollmentRecommendation> recommendations) {
        this.recommendations = recommendations;
    }

    public List<WrapChoseEntranseDiscipline> getDisciplines() {
        return disciplines;
    }

    public void setDisciplines(List<WrapChoseEntranseDiscipline> disciplines) {
        this.disciplines = disciplines;
    }

    public boolean isHaveSpecialRights() {
        return haveSpecialRights;
    }

    public void setHaveSpecialRights(boolean haveSpecialRights) {
        this.haveSpecialRights = haveSpecialRights;
    }

    @Override
    public String getDisciplinesHTMLDescription() {
        return null;
    }

    @Override
    public String getEntrantRequestsInfoHtml() {
        return "";
    }

    @Override
    public Integer getEnrollmentPriority() {
        return enrollmentPriority;
    }

    public void setEnrollmentPriority(Integer enrollmentPriority) {
        this.enrollmentPriority = enrollmentPriority;
    }

}
