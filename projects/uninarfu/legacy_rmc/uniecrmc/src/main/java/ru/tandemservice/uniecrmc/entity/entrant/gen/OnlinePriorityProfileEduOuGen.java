package ru.tandemservice.uniecrmc.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.entrant.ProfileEducationOrgUnit;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection;
import ru.tandemservice.uniecrmc.entity.entrant.OnlinePriorityProfileEduOu;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Приоритет профиля направления подготовки по ВНП онлайн абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OnlinePriorityProfileEduOuGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniecrmc.entity.entrant.OnlinePriorityProfileEduOu";
    public static final String ENTITY_NAME = "onlinePriorityProfileEduOu";
    public static final int VERSION_HASH = 1844665101;
    private static IEntityMeta ENTITY_META;

    public static final String L_ONLINE_REQUESTED_ENROLLMENT_DIRECTION = "onlineRequestedEnrollmentDirection";
    public static final String L_PROFILE_EDUCATION_ORG_UNIT = "profileEducationOrgUnit";
    public static final String P_PRIORITY = "priority";

    private OnlineRequestedEnrollmentDirection _onlineRequestedEnrollmentDirection;     // Выбранное онлайн направление приема
    private ProfileEducationOrgUnit _profileEducationOrgUnit;     // Профиль направления подготовки подразделения
    private int _priority;     // Приоритет

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выбранное онлайн направление приема. Свойство не может быть null.
     */
    @NotNull
    public OnlineRequestedEnrollmentDirection getOnlineRequestedEnrollmentDirection()
    {
        return _onlineRequestedEnrollmentDirection;
    }

    /**
     * @param onlineRequestedEnrollmentDirection Выбранное онлайн направление приема. Свойство не может быть null.
     */
    public void setOnlineRequestedEnrollmentDirection(OnlineRequestedEnrollmentDirection onlineRequestedEnrollmentDirection)
    {
        dirty(_onlineRequestedEnrollmentDirection, onlineRequestedEnrollmentDirection);
        _onlineRequestedEnrollmentDirection = onlineRequestedEnrollmentDirection;
    }

    /**
     * @return Профиль направления подготовки подразделения. Свойство не может быть null.
     */
    @NotNull
    public ProfileEducationOrgUnit getProfileEducationOrgUnit()
    {
        return _profileEducationOrgUnit;
    }

    /**
     * @param profileEducationOrgUnit Профиль направления подготовки подразделения. Свойство не может быть null.
     */
    public void setProfileEducationOrgUnit(ProfileEducationOrgUnit profileEducationOrgUnit)
    {
        dirty(_profileEducationOrgUnit, profileEducationOrgUnit);
        _profileEducationOrgUnit = profileEducationOrgUnit;
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет. Свойство не может быть null.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OnlinePriorityProfileEduOuGen)
        {
            setOnlineRequestedEnrollmentDirection(((OnlinePriorityProfileEduOu)another).getOnlineRequestedEnrollmentDirection());
            setProfileEducationOrgUnit(((OnlinePriorityProfileEduOu)another).getProfileEducationOrgUnit());
            setPriority(((OnlinePriorityProfileEduOu)another).getPriority());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OnlinePriorityProfileEduOuGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OnlinePriorityProfileEduOu.class;
        }

        public T newInstance()
        {
            return (T) new OnlinePriorityProfileEduOu();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "onlineRequestedEnrollmentDirection":
                    return obj.getOnlineRequestedEnrollmentDirection();
                case "profileEducationOrgUnit":
                    return obj.getProfileEducationOrgUnit();
                case "priority":
                    return obj.getPriority();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "onlineRequestedEnrollmentDirection":
                    obj.setOnlineRequestedEnrollmentDirection((OnlineRequestedEnrollmentDirection) value);
                    return;
                case "profileEducationOrgUnit":
                    obj.setProfileEducationOrgUnit((ProfileEducationOrgUnit) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "onlineRequestedEnrollmentDirection":
                        return true;
                case "profileEducationOrgUnit":
                        return true;
                case "priority":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "onlineRequestedEnrollmentDirection":
                    return true;
                case "profileEducationOrgUnit":
                    return true;
                case "priority":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "onlineRequestedEnrollmentDirection":
                    return OnlineRequestedEnrollmentDirection.class;
                case "profileEducationOrgUnit":
                    return ProfileEducationOrgUnit.class;
                case "priority":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OnlinePriorityProfileEduOu> _dslPath = new Path<OnlinePriorityProfileEduOu>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OnlinePriorityProfileEduOu");
    }
            

    /**
     * @return Выбранное онлайн направление приема. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.OnlinePriorityProfileEduOu#getOnlineRequestedEnrollmentDirection()
     */
    public static OnlineRequestedEnrollmentDirection.Path<OnlineRequestedEnrollmentDirection> onlineRequestedEnrollmentDirection()
    {
        return _dslPath.onlineRequestedEnrollmentDirection();
    }

    /**
     * @return Профиль направления подготовки подразделения. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.OnlinePriorityProfileEduOu#getProfileEducationOrgUnit()
     */
    public static ProfileEducationOrgUnit.Path<ProfileEducationOrgUnit> profileEducationOrgUnit()
    {
        return _dslPath.profileEducationOrgUnit();
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.OnlinePriorityProfileEduOu#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    public static class Path<E extends OnlinePriorityProfileEduOu> extends EntityPath<E>
    {
        private OnlineRequestedEnrollmentDirection.Path<OnlineRequestedEnrollmentDirection> _onlineRequestedEnrollmentDirection;
        private ProfileEducationOrgUnit.Path<ProfileEducationOrgUnit> _profileEducationOrgUnit;
        private PropertyPath<Integer> _priority;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выбранное онлайн направление приема. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.OnlinePriorityProfileEduOu#getOnlineRequestedEnrollmentDirection()
     */
        public OnlineRequestedEnrollmentDirection.Path<OnlineRequestedEnrollmentDirection> onlineRequestedEnrollmentDirection()
        {
            if(_onlineRequestedEnrollmentDirection == null )
                _onlineRequestedEnrollmentDirection = new OnlineRequestedEnrollmentDirection.Path<OnlineRequestedEnrollmentDirection>(L_ONLINE_REQUESTED_ENROLLMENT_DIRECTION, this);
            return _onlineRequestedEnrollmentDirection;
        }

    /**
     * @return Профиль направления подготовки подразделения. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.OnlinePriorityProfileEduOu#getProfileEducationOrgUnit()
     */
        public ProfileEducationOrgUnit.Path<ProfileEducationOrgUnit> profileEducationOrgUnit()
        {
            if(_profileEducationOrgUnit == null )
                _profileEducationOrgUnit = new ProfileEducationOrgUnit.Path<ProfileEducationOrgUnit>(L_PROFILE_EDUCATION_ORG_UNIT, this);
            return _profileEducationOrgUnit;
        }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.OnlinePriorityProfileEduOu#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(OnlinePriorityProfileEduOuGen.P_PRIORITY, this);
            return _priority;
        }

        public Class getEntityClass()
        {
            return OnlinePriorityProfileEduOu.class;
        }

        public String getEntityName()
        {
            return "onlinePriorityProfileEduOu";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
