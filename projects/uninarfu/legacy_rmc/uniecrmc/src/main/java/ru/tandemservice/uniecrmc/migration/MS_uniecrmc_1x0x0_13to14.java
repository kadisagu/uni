package ru.tandemservice.uniecrmc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Миграция исправления для уже зарегистрированных онлайн абитуриентов RM #6393
 * <p/>
 * Так как убрана возможность выбора места значения атрибута certificatePlace для онлайн-абитуриента
 * от веб-сервиса получаем entrantData.certificatePlaceCode = null
 * В мастере создания абитуриента на основе онлайн абитуриента данный атрибут является обязательным
 * поэтому проставим здесь вручную в значение кода для 1-ая волна (в TU Справочник: Типы экзаменов ЕГЭ)
 *
 * @author Ivan Anishchenko
 */
public class MS_uniecrmc_1x0x0_13to14 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.5.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.5.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception {

        if (tool.tableExists("onlineentrant_t") && tool.tableExists("stateexamtype_t")) {

            Statement stmt = tool.getConnection().createStatement();
            stmt.execute("SELECT id FROM stateexamtype_t WHERE code_p = '1'");
            ResultSet rs = stmt.getResultSet();

            // Убедимся в существовании записей с указанным кодом для значения "1-ая волна"
            if (rs.next()) {
                String updatesql = "UPDATE onlineentrant_t  SET certificateplace_id  = (SELECT id FROM stateexamtype_t WHERE code_p = '1') WHERE certificateplace_id IS NULL";
                tool.executeUpdate(updatesql);
            }
        }
    }
}