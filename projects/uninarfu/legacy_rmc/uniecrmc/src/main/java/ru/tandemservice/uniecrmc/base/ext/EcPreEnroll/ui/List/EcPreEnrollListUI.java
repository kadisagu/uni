package ru.tandemservice.uniecrmc.base.ext.EcPreEnroll.ui.List;

import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.EcDistributionUtil;
import ru.tandemservice.uniecrmc.base.ext.EcPreEnroll.util.PreEntrantDTORMC;
import ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionExt;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.base.bo.EcPreEnroll.EcPreEnrollManager;
import ru.tandemservice.uniec.base.bo.EcPreEnroll.ui.List.EcPreEnrollList;
import ru.tandemservice.uniec.base.bo.EcPreEnroll.util.PreEntrantDTO;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;

import java.util.Collection;

public class EcPreEnrollListUI extends ru.tandemservice.uniec.base.bo.EcPreEnroll.ui.List.EcPreEnrollListUI {


    private EntrantEnrollmentOrderType orderType;

    public Integer getSrPlan() {
        EnrollmentDirectionExt directionExt = UniDaoFacade.getCoreDao().get(EnrollmentDirectionExt.class, EnrollmentDirectionExt.enrollmentDirection(), this.getEnrollmentDirection());
        if (directionExt != null)
            return directionExt.getHaveSpecialRightsPlanBudget();
        return null;
    }

    public boolean isUsePriority() {
        return EcDistributionUtil.isUsePriority(getEnrollmentCampaign());
    }

    public String getDisciplines() {
        if (getConfig().getDataSource(EcPreEnrollList.PRE_ENTRANT_DS).getCurrent() instanceof PreEntrantDTORMC)
            return ((PreEntrantDTORMC) getConfig().getDataSource(EcPreEnrollList.PRE_ENTRANT_DS).getCurrent()).getDisciplinesHTMLDescription();
        else
            return "";
    }

    public EntrantEnrollmentOrderType getOrderType() {
        return orderType;
    }

    public void setOrderType(EntrantEnrollmentOrderType orderType) {
        this.orderType = orderType;
    }


    public void onChangeOrderTypeMain()
    {

    }

    // назначить тип приказа выделенным записям
    public void onClickSetOrderTypeToSelected()
    {
        BaseSearchListDataSource ds = (BaseSearchListDataSource) getConfig().getDataSource("preEntrantDS");
        Collection<IEntity> records = ((CheckboxColumn) ds.getLegacyDataSource().getColumn("selected")).getSelectedObjects();
        for (IEntity record : records) {
            PreEntrantDTO dto = (PreEntrantDTO) record;
            Long requestedEnrollmentDirectionId = dto.getId();

            if (!dto.isChangeOrderDisabled()) {
                // можно вносить изменения

                EntrantEnrollmentOrderType orderType = getOrderType();
                EducationOrgUnit educationOrgUnit = super.getEnrollmentDirection().getEducationOrgUnit();

                EcPreEnrollManager.instance().dao().doChangeOrderType(requestedEnrollmentDirectionId, educationOrgUnit, orderType);
            }
        }

    }
}
