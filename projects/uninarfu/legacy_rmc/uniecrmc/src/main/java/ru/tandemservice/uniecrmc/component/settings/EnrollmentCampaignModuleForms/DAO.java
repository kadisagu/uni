package ru.tandemservice.uniecrmc.component.settings.EnrollmentCampaignModuleForms;

import org.tandemframework.shared.commonbase.tapestry.validator.ParamRequiredValidator;

import java.util.ArrayList;

public class DAO extends ru.tandemservice.uniec.component.settings.EnrollmentCampaignModuleForms.DAO {

    @Override
    public void prepare(ru.tandemservice.uniec.component.settings.EnrollmentCampaignModuleForms.Model model) {
        super.prepare(model);

        Model myModel = (Model) model;
        myModel.setBlockList(new ArrayList<Block>());

        Block block = new Block("1", "Мастер добавления абитуриента: шаг «Полученное образование», обязательные для заполнения поля:");
        myModel.getBlockList().add(block);
        block.addProperty("entrant.wizard.EduInstitutionStep.field.seria", "Серия документа об образовании");
        block.addProperty("entrant.wizard.EduInstitutionStep.field.number", "Номер документа об образовании");
        block.addProperty("entrant.wizard.EduInstitutionStep.field.date", "Дата выдачи документа об образовании");
        block.addProperty("entrant.wizard.EduInstitutionStep.field.marks", "Статистика оценок");


    }

    @Override
    public void update(ru.tandemservice.uniec.component.settings.EnrollmentCampaignModuleForms.Model model) {
        super.update(model);

        Model myModel = (Model) model;
        for (Block block : myModel.getBlockList())
            for (Block.Property property : block.getPropertyList())
                ParamRequiredValidator.setRequired(property.getKey(), property.getValue());
    }
}
