package ru.tandemservice.uniecrmc.dao.moveoriginal;

public class WrapRequestedEnrollmentDirection {
    private Long id;
    private Long entrantRequestId;
    private Long enrollmentDirectionId;
    private String compensationTypeCode;
    private String competitionKindCode;
    private String entrantStateCode;
    private Integer entrantStatePriority;

    private Integer priority;
    private boolean targetAdmission;
    private boolean original;
    private double finalMark;
    private boolean endState;
    private String comment = "";

    private boolean originHasSet;
    // признак оригинала на старте обработки
    private boolean originalEtStart;

    private Long competitionGroupId;

    public WrapRequestedEnrollmentDirection
            (
                    Long id,
                    Long entrantRequestId,
                    Long enrollmentDirectionId,
                    String compensationTypeCode,
                    String competitionKindCode,
                    String entrantStateCode,
                    Integer priority,
                    boolean targetAdmission,
                    boolean original,
                    double finalMark,
                    Integer entrantStatePriority,
                    Long competitionGroupId

            )
    {

        this.id = id;
        this.entrantRequestId = entrantRequestId;
        this.enrollmentDirectionId = enrollmentDirectionId;
        this.compensationTypeCode = compensationTypeCode;
        this.competitionKindCode = competitionKindCode;
        this.entrantStateCode = entrantStateCode;
        this.priority = priority;
        this.targetAdmission = targetAdmission;
        this.original = original;
        this.finalMark = finalMark;
        this.entrantStatePriority = entrantStatePriority;
        this.originalEtStart = original;

        this.competitionGroupId = competitionGroupId;

        endState = false;
        originHasSet = false;

    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEntrantRequestId() {
        return entrantRequestId;
    }

    public void setEntrantRequestId(Long entrantRequestId) {
        this.entrantRequestId = entrantRequestId;
    }

    public Long getEnrollmentDirectionId() {
        return enrollmentDirectionId;
    }

    public void setEnrollmentDirectionId(Long enrollmentDirectionId) {
        this.enrollmentDirectionId = enrollmentDirectionId;
    }

    public String getCompensationTypeCode() {
        return compensationTypeCode;
    }

    public void setCompensationTypeCode(String compensationTypeCode) {
        this.compensationTypeCode = compensationTypeCode;
    }

    public String getCompetitionKindCode() {
        return competitionKindCode;
    }

    public void setCompetitionKindCode(String competitionKindCode) {
        this.competitionKindCode = competitionKindCode;
    }

    public String getEntrantStateCode() {
        return entrantStateCode;
    }

    public void setEntrantStateCode(String entrantStateCode) {
        this.entrantStateCode = entrantStateCode;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public boolean isTargetAdmission() {
        return targetAdmission;
    }

    public void setTargetAdmission(boolean targetAdmission) {
        this.targetAdmission = targetAdmission;
    }

    public boolean isOriginal() {
        return original;
    }

    public void setOriginal(boolean original) {
        this.original = original;
    }

    public double getFinalMark() {
        return finalMark;
    }

    public void setFinalMark(double finalMark) {
        this.finalMark = finalMark;
    }

    public boolean isEndState() {
        return endState;
    }

    public void setEndState(boolean endState) {
        this.endState = endState;
    }

    public Integer getEntrantStatePriority() {
        return entrantStatePriority;
    }

    public void setEntrantStatePriority(Integer entrantStatePriority) {
        this.entrantStatePriority = entrantStatePriority;
    }

    public boolean isOriginalEtStart() {
        return originalEtStart;
    }

    public void setOriginalEtStart(boolean originalEtStart) {
        this.originalEtStart = originalEtStart;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isOriginHasSet() {
        return originHasSet;
    }

    public void setOriginHasSet(boolean originHasSet) {
        this.originHasSet = originHasSet;
    }

    public Long getCompetitionGroupId() {
        return competitionGroupId;
    }

    public void setCompetitionGroupId(Long competitionGroupId) {
        this.competitionGroupId = competitionGroupId;
    }
}


