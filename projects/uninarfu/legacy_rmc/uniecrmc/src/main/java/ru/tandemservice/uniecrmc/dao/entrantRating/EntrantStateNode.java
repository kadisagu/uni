package ru.tandemservice.uniecrmc.dao.entrantRating;

import ru.tandemservice.uniec.entity.catalog.EntrantState;

import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

public class EntrantStateNode {

    @XmlElement(name = "EntrantState")
    public List<RowNode> list = new ArrayList<>();

    public EntrantStateNode() {
    }

    public void add(EntrantState entity) {
        RowNode node = new RowNode(entity.getCode(), entity.getTitle(), entity.getShortTitle(), entity.isSelectable(), entity.getPriority(), null);
        list.add(node);
    }
}
