package ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.Pub;

import ru.tandemservice.uniecrmc.base.ext.EcDistribution.EcDistributionExtManager;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.EntrantAdd.EcDistributionEntrantAddUI;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.EcDistributionUtil;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.IEcgCortegeRMC;
import ru.tandemservice.uniecrmc.entity.entrant.EntrantRecommendedSettings;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniec.base.bo.EcDistribution.ui.EntrantAdd.EcDistributionEntrantAdd;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgEntrantRateRowDTO;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.Hashtable;
import java.util.Map;

public class EcDistributionPubUI extends ru.tandemservice.uniec.base.bo.EcDistribution.ui.Pub.EcDistributionPubUI {

    private Map<Long, String> srQuotaMap;
    private boolean usePriority;

    @Override
    public void onComponentRefresh() {
        super.onComponentRefresh();
        EntrantRecommendedSettings settings = UniDaoFacade.getCoreDao().get(EntrantRecommendedSettings.class, EntrantRecommendedSettings.enrollmentCampaign(), getDistribution().getConfig().getEcgItem().getEnrollmentCampaign());

        if (settings != null && settings.isUsePriorityEntranceDiscipline()) {
            srQuotaMap = new Hashtable<Long, String>();
            EcDistributionExtManager.daoRMC().getQuotaHTMLDescription(this.getDistribution(), srQuotaMap);
            setUsePriority(settings.isUsePriorityEntranceDiscipline());
        }
    }

    public String getSrQuota() {
        Long id = ((EnrollmentDirection) getConfig().getDataSource("directionDS").getCurrent()).getId();
        return (String) this.srQuotaMap.get(id);
    }

    public String getDisciplines() {
        return ((IEcgCortegeRMC) getConfig().getDataSource("recommendedDS").getCurrent()).getDisciplinesHTMLDescription();
    }

    public boolean isUseProfileMark() {
        return !isUsePriority();
    }

    @Override
    public void onClickAddByTA() {
        if (!isUsePriority())
            super.onClickAddByTA();
        else {
            this._uiSupport.doRefresh();
            this._uiActivation.asRegion(EcDistributionEntrantAdd.class)
                    .parameter("distribution", this.getDistribution())
                    .parameter("rateRule", IEcgEntrantRateRowDTO.Rule.RULE_TARGET_ADMISSION)
                    .parameter(EcDistributionEntrantAddUI.RULE, EcDistributionUtil.Rule.RULE_TARGET_ADMISSION)
                    .activate();
        }
    }

    @Override
    public void onClickAddByOutOfCompetition() {
        if (!isUsePriority())
            super.onClickAddByOutOfCompetition();
        else {
            this._uiSupport.doRefresh();
            this._uiActivation.asRegion(EcDistributionEntrantAdd.class)
                    .parameter("distribution", this.getDistribution())
                    .parameter("rateRule", IEcgEntrantRateRowDTO.Rule.RULE_NON_COMPETITIVE)
                    .parameter(EcDistributionEntrantAddUI.RULE, EcDistributionUtil.Rule.RULE_WITHOUT_ENTRANCE_DISCIPLINES)
                    .activate();
        }
    }

    public void onClickAddBeyondCompetition() {
        this._uiSupport.doRefresh();
        this._uiActivation.asRegion(EcDistributionEntrantAdd.class)
                .parameter("distribution", this.getDistribution())
                .parameter("rateRule", IEcgEntrantRateRowDTO.Rule.RULE_NON_COMPETITIVE)
                .parameter(EcDistributionEntrantAddUI.RULE, EcDistributionUtil.Rule.RULE_NON_COMPETITIVE)
                .activate();
    }

    @Override
    public void onClickAddByCompetition() {
        if (!isUsePriority())
            super.onClickAddByCompetition();
        else {
            this._uiSupport.doRefresh();
            this._uiActivation.asRegion(EcDistributionEntrantAdd.class)
                    .parameter("distribution", this.getDistribution())
                    .parameter("rateRule", IEcgEntrantRateRowDTO.Rule.RULE_COMPETITIVE)
                    .parameter(EcDistributionEntrantAddUI.RULE, EcDistributionUtil.Rule.RULE_COMPETITIVE)
                    .activate();
        }
    }

    @Override
    public void onClickAddByCompetitionWithOriginal() {
        if (!isUsePriority())
            super.onClickAddByCompetitionWithOriginal();
        else {
            this._uiSupport.doRefresh();
            this._uiActivation.asRegion(EcDistributionEntrantAdd.class)
                    .parameter("distribution", this.getDistribution())
                    .parameter("rateRule", IEcgEntrantRateRowDTO.Rule.RULE_COMPETITIVE_WITH_ORIGINAL)
                    .parameter(EcDistributionEntrantAddUI.RULE, EcDistributionUtil.Rule.RULE_COMPETITIVE_WITH_ORIGINAL)
                    .activate();
        }
    }

    public Map<Long, String> getSrQuotaMap() {
        return srQuotaMap;
    }

    public void setSrQuotaMap(Map<Long, String> srQuotaMap) {
        this.srQuotaMap = srQuotaMap;
    }

    public boolean isUsePriority() {
        return usePriority;
    }

    public void setUsePriority(boolean usePriority) {
        this.usePriority = usePriority;
    }

}
