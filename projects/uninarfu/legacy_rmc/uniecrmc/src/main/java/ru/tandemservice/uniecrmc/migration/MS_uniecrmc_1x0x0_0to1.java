package ru.tandemservice.uniecrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.hibsupport.EntityIDGenerator;
import ru.tandemservice.uniecrmc.entity.entrant.EntrantRecommendedSettings;

import java.sql.ResultSet;
import java.sql.Statement;

public class MS_uniecrmc_1x0x0_0to1 extends IndependentMigrationScript {

    @Override
    public void run(DBTool tool) throws Exception {

        if (!tool.tableExists("entrantrecommendedsettings_t")) {
            // создать таблицу
            DBTable dbt = new DBTable("entrantrecommendedsettings_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("enrollmentcampaign_id", DBType.LONG).setNullable(false),
                                      new DBColumn("sprrtyentrncdscpln_p", DBType.BOOLEAN).setNullable(false)
            );
            tool.createTable(dbt);
            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("entrantRecommendedSettings");
        }

        if (tool.tableExists("enrollmentcampaign_t")) {
            short code = EntityRuntime.getMeta(EntrantRecommendedSettings.class).getEntityCode();

            Statement stmt = tool.getConnection().createStatement();

            stmt.execute("select id from enrollmentcampaign_t aaa " +
                                 "where aaa.id not in (select bbb.enrollmentcampaign_id from entrantrecommendedsettings_t bbb)");

            String insertQuery = "insert into entrantrecommendedsettings_t (id, discriminator, enrollmentcampaign_id, sprrtyentrncdscpln_p) values (?,?,?,?)";

            ResultSet rs = stmt.getResultSet();

            while (rs.next()) {
                tool.executeUpdate(insertQuery, EntityIDGenerator.generateNewId(code), code, rs.getLong(1), Boolean.FALSE);
            }
        }

    }

}
