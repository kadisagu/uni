package ru.tandemservice.uniec.ws;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ProfilePriorityData {

    public long profileEducationOrgUnitId;
    public long compensationTypeId;
    public int priority;

}
