package ru.tandemservice.uniecrmc.component.entrant.EnrollmentDirectionPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import ru.tandemservice.uniecrmc.entity.entrant.EntranceDisciplineExt;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;

public class Controller extends ru.tandemservice.uniec.component.entrant.EnrollmentDirectionPub.Controller {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        super.onRefreshComponent(component);

        Model model = (Model) getModel(component);
        prepareDataSource(model, model.getDataSource());

        for (DynamicListDataSource<EntranceDiscipline> dataSource : model.getDataSourceMap().values()) {
            prepareDataSource(model, dataSource);
        }
    }

    private void prepareDataSource(Model model, DynamicListDataSource<EntranceDiscipline> dataSource) {
        int index = dataSource.getColumn("choicableDisciplines").getNumber();

        dataSource.addColumn(new ActionColumn("Повысить приоритет", "up", "onClickUp").setVisible(model.isVisible()).setOrderable(false), index + 1);
        dataSource.addColumn(new ActionColumn("Понизить приоритет", "down", "onClickDown").setVisible(model.isVisible()).setOrderable(false), index + 2);
    }

    public void onClickUp(IBusinessComponent component) {
        EntranceDisciplineExt disciplineExt = UniDaoFacade.getCoreDao().get(EntranceDisciplineExt.class, EntranceDisciplineExt.entranceDiscipline().id(), ((Long) component.getListenerParameter()).longValue());
        CommonManager.instance().commonPriorityDao().doChangePriorityUp(disciplineExt.getId(), new Object[]{EntranceDisciplineExt.entranceDiscipline().enrollmentDirection(), ((Model) getModel(component)).getEnrollmentDirection()});
    }

    public void onClickDown(IBusinessComponent component) {
        EntranceDisciplineExt disciplineExt = UniDaoFacade.getCoreDao().get(EntranceDisciplineExt.class, EntranceDisciplineExt.entranceDiscipline().id(), ((Long) component.getListenerParameter()).longValue());
        CommonManager.instance().commonPriorityDao().doChangePriorityDown(disciplineExt.getId(), new Object[]{EntranceDisciplineExt.entranceDiscipline().enrollmentDirection(), ((Model) getModel(component)).getEnrollmentDirection()});
    }
}
