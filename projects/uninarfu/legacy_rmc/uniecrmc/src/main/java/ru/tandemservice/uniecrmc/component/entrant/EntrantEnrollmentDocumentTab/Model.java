package ru.tandemservice.uniecrmc.component.entrant.EntrantEnrollmentDocumentTab;

import ru.tandemservice.uniecrmc.entity.entrant.EntrantExt;

public class Model extends ru.tandemservice.uniec.component.entrant.EntrantEnrollmentDocumentTab.Model {
    private EntrantExt entrantExt = new EntrantExt();

    public EntrantExt getEntrantExt() {
        return entrantExt;
    }

    public void setEntrantExt(EntrantExt entrantExt) {
        this.entrantExt = entrantExt;
    }
}
