package ru.tandemservice.uniecrmc.component.catalog.targetAdmissionKind.TargetAdmissionKindAddEdit;

import ru.tandemservice.uniecrmc.entity.entrant.TargetAdmissionKindRMC;

public class DAO extends ru.tandemservice.uniec.component.catalog.targetAdmissionKind.TargetAdmissionKindAddEdit.DAO {

    @Override
    public void prepare(ru.tandemservice.uniec.component.catalog.targetAdmissionKind.TargetAdmissionKindAddEdit.Model model)
    {
        super.prepare(model);
        Model myModel = (Model) model;


        TargetAdmissionKindRMC targetAdmissionKindRMC = get(TargetAdmissionKindRMC.class, TargetAdmissionKindRMC.L_BASE, model.getCatalogItem());

        if (targetAdmissionKindRMC == null) {
            targetAdmissionKindRMC = new TargetAdmissionKindRMC();
            targetAdmissionKindRMC.setBase(model.getCatalogItem());
        }
        myModel.setTargetAdmissionKindRMC(targetAdmissionKindRMC);

    }

    @Override
    public void update(ru.tandemservice.uniec.component.catalog.targetAdmissionKind.TargetAdmissionKindAddEdit.Model model) {
        super.update(model);
        Model myModel = (Model) model;

        this.saveOrUpdate(myModel.getTargetAdmissionKindRMC());

    }

}
