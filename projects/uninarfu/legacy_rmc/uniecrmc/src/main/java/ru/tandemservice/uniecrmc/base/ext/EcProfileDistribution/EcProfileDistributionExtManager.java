package ru.tandemservice.uniecrmc.base.ext.EcProfileDistribution;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.uniecrmc.base.ext.EcProfileDistribution.logic.EcProfileDistributionExtDao;
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.logic.IEcProfileDistributionDao;

@Configuration
public class EcProfileDistributionExtManager extends BusinessObjectExtensionManager {

    @Bean
    @BeanOverride
    public IEcProfileDistributionDao dao() {
        return new EcProfileDistributionExtDao();
    }
}
