package ru.tandemservice.uniecrmc.component.entrant.EntrantRequestTab;

import ru.tandemservice.uniec.component.entrant.EntrantRequestTab.Model;

public interface IDAO extends ru.tandemservice.uniec.component.entrant.EntrantRequestTab.IDAO {

    public void updateSpecialRights(Model model, Long id);
}
