package ru.tandemservice.uniecrmc.component.report.EnrollmentResultByCategory.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniecrmc.entity.entrant.EnrollmentResultByCategoryReport;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.List;

public class Model implements IEnrollmentCampaignSelectModel {

    private IDataSettings settings;
    private DynamicListDataSource<EnrollmentResultByCategoryReport> dataSource;
    private List<EnrollmentCampaign> enrollmentCampaignList;

    public EnrollmentCampaign getEnrollmentCampaign() {
        return (EnrollmentCampaign) this.settings.get("enrollmentCampaign");
    }

    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign) {
        this.settings.set("enrollmentCampaign", enrollmentCampaign);
    }

    public IDataSettings getSettings() {
        return this.settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public DynamicListDataSource<EnrollmentResultByCategoryReport> getDataSource() {
        return this.dataSource;
    }

    public void setDataSource(DynamicListDataSource<EnrollmentResultByCategoryReport> dataSource) {
        this.dataSource = dataSource;
    }

    public List<EnrollmentCampaign> getEnrollmentCampaignList() {
        return this.enrollmentCampaignList;
    }

    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList) {
        this.enrollmentCampaignList = enrollmentCampaignList;
    }
}