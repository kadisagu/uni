package ru.tandemservice.uniecrmc.entity.entrant;

import ru.tandemservice.uniecrmc.entity.entrant.gen.EntrantEduLevelDistributionReportGen;
import ru.tandemservice.uni.IStorableReport;

/**
 * Распределение приема по специальностям 2014
 */
public class EntrantEduLevelDistributionReport extends EntrantEduLevelDistributionReportGen implements IStorableReport {
}