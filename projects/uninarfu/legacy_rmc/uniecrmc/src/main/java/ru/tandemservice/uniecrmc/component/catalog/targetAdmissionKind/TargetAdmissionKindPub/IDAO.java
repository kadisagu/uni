package ru.tandemservice.uniecrmc.component.catalog.targetAdmissionKind.TargetAdmissionKindPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;

public interface IDAO extends IDefaultCatalogPubDAO {

    void prepareProjectsDataSource(Model model);


}
