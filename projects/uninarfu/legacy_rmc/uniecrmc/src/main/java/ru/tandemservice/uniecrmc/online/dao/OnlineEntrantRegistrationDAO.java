package ru.tandemservice.uniecrmc.online.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.catalog.entity.*;
import ru.tandemservice.uniecrmc.entity.catalog.IndividualAchievements;
import ru.tandemservice.uniecrmc.entity.entrant.OnlinePriorityProfileEduOu;
import ru.tandemservice.uniecrmc.online.entity.OnlineEntrant2IndividualAchievement;
import ru.tandemservice.uniecrmc.online.entity.OnlineEntrantExt;
import ru.tandemservice.uniecrmc.online.entity.OnlineRequestedEnrollmentDirectionOrder;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.ProfileEducationOrgUnit;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection;
import ru.tandemservice.uniec.ws.*;

import java.util.*;

public class OnlineEntrantRegistrationDAO extends
        ru.tandemservice.uniec.dao.OnlineEntrantRegistrationDAO implements
        IOnlineEntrantRegistrationDAO
{

    @Override
    public void synchronizeUserProfilePriorities(long userId, List<ProfilePriorityData> priorityList) {
        List<OnlineRequestedEnrollmentDirection> directionList = getList(OnlineRequestedEnrollmentDirection.class, OnlineRequestedEnrollmentDirection.entrant().id(), userId);


        Map<String, OnlineRequestedEnrollmentDirection> mapDirection = new HashMap<>();
        for (OnlineRequestedEnrollmentDirection direction : directionList) {
            String key = "" + direction.getEnrollmentDirection().getId() + "," + direction.getCompensationType().getId();
            mapDirection.put(key, direction);
        }

        Map<String, List<OnlinePriorityProfileEduOu>> mapPriority = new HashMap<>();
        for (ProfilePriorityData data : priorityList) {
            ProfileEducationOrgUnit profileEOU = getNotNull(ProfileEducationOrgUnit.class, data.profileEducationOrgUnitId);
            String key = profileEOU.getEnrollmentDirection().getId() + "," + data.compensationTypeId;

            OnlineRequestedEnrollmentDirection direction = mapDirection.get(key);
            if (direction == null)
                continue;

            OnlinePriorityProfileEduOu priorityItem = new OnlinePriorityProfileEduOu();
            priorityItem.setOnlineRequestedEnrollmentDirection(direction);
            priorityItem.setProfileEducationOrgUnit(profileEOU);
            priorityItem.setPriority(data.priority);

            List<OnlinePriorityProfileEduOu> list = mapPriority.get(key);
            if (list == null)
                mapPriority.put(key, list = new ArrayList<>());
            list.add(priorityItem);
        }

        //удалим старые
        new DQLDeleteBuilder(OnlinePriorityProfileEduOu.class)
                .where(DQLExpressions.eqValue(DQLExpressions.property(OnlinePriorityProfileEduOu.onlineRequestedEnrollmentDirection().entrant().id()), userId))
                .createStatement(getSession())
                .execute();
        //вставим новые
        for (List<OnlinePriorityProfileEduOu> list : mapPriority.values()) {
            Collections.sort(list, OnlinePriorityProfileEduOu.COMPARATOR);

            int index = 1;
            for (OnlinePriorityProfileEduOu priorityItem : list) {
                priorityItem.setPriority(index++);
                getSession().saveOrUpdate(priorityItem);
            }
        }
    }

    @Override
    public List<ProfilePriorityData> getProfilePriorities(long userId, List<UserDirectionDataExt> userDirections) {
        Set<Long> enrollmentDirectionsIdSet = new HashSet<>();
        for (UserDirectionData data : userDirections)
            enrollmentDirectionsIdSet.add(data.enrollmentDirectionId);

        //профили для выбранных направлений
        List<ProfileEducationOrgUnit> profileList = getList(ProfileEducationOrgUnit.class, ProfileEducationOrgUnit.enrollmentDirection().id(), enrollmentDirectionsIdSet);
        Map<Long, List<ProfileEducationOrgUnit>> map = new HashMap<Long, List<ProfileEducationOrgUnit>>();
        for (ProfileEducationOrgUnit peo : profileList) {
            List<ProfileEducationOrgUnit> list = map.get(peo.getEnrollmentDirection().getId());
            if (list == null)
                map.put(peo.getEnrollmentDirection().getId(), list = new ArrayList<>());

            list.add(peo);
        }

        List<OnlinePriorityProfileEduOu> priorityList = getList(OnlinePriorityProfileEduOu.class, OnlinePriorityProfileEduOu.onlineRequestedEnrollmentDirection().entrant().id(), userId);
        Map<String, OnlinePriorityProfileEduOu> mapPriority = new HashMap<>();
        for (OnlinePriorityProfileEduOu item : priorityList) {
            String key = item.getProfileEducationOrgUnit().getId() + "," + item.getOnlineRequestedEnrollmentDirection().getCompensationType().getId();
            mapPriority.put(key, item);
        }

        //обходим все выбранные направления и заполняем профили
        List<ProfilePriorityData> resultList = new ArrayList<>();
        for (UserDirectionData data : userDirections) {
            List<ProfileEducationOrgUnit> peoList = map.get(data.enrollmentDirectionId);
            if (peoList == null)
                continue;

            int index = 1;
            for (ProfileEducationOrgUnit peo : peoList) {
                ProfilePriorityData priorityData = new ProfilePriorityData();
                priorityData.profileEducationOrgUnitId = peo.getId();
                priorityData.compensationTypeId = data.compensationTypeId;
                priorityData.priority = index++;

                String key = priorityData.profileEducationOrgUnitId + "," + priorityData.compensationTypeId;
                OnlinePriorityProfileEduOu existed = mapPriority.get(key);
                if (existed != null)
                    priorityData.priority = existed.getPriority();

                resultList.add(priorityData);
            }
        }

        return resultList;
    }

    @Override
    public OnlineEntrant createOrUpdateOnlineEntrant(long userId, EntrantDataExt entrantDataExt) {

        OnlineEntrant onlineEntrant = createOrUpdateOnlineEntrant(userId, (EntrantData) entrantDataExt);


        OnlineEntrantExt onlineEntrantExt = get(OnlineEntrantExt.class, OnlineEntrantExt.entrant().id().s(), onlineEntrant.getId());
        if (onlineEntrantExt == null) {
            onlineEntrantExt = new OnlineEntrantExt();
            onlineEntrantExt.setEntrant(onlineEntrant);
        }

        onlineEntrantExt.setSnilsNumber(entrantDataExt.getSnilsNumber());
        onlineEntrantExt.setNextOfKinRelationDegree(safeGet(RelationDegree.class, RelationDegree.id().s(), entrantDataExt.getKinRelationDegreeId()));
        onlineEntrantExt.setNextOfKinFirstName(entrantDataExt.getKinFirstName());
        onlineEntrantExt.setNextOfKinLastName(entrantDataExt.getKinLastName());
        onlineEntrantExt.setNextOfKinMiddleName(entrantDataExt.getKinMiddleName());
        onlineEntrantExt.setNextOfKinPhone(entrantDataExt.getKinPhone());
        onlineEntrantExt.setNextOfKinWorkPlace(entrantDataExt.getKinWorkPlace());
        onlineEntrantExt.setNextOfKinWorkPost(entrantDataExt.getKinWorkPost());
        onlineEntrantExt.setDescription(entrantDataExt.getDescription());

        // Индивидуальные достижения
        createOrUpdateIndividualAchievements(onlineEntrant, entrantDataExt);

        getSession().saveOrUpdate(onlineEntrantExt);

        return onlineEntrant;
    }

    @Override
    public EntrantDataExt getEntrantDataExt(long userId) {
        OnlineEntrant onlineEntrant = getOnlineEntrant(userId);
        if (onlineEntrant == null) {
            return null;
        }

        EntrantDataExt dataExt = new EntrantDataExt();

        fillEntrantData(dataExt, onlineEntrant);
        fillEntrantDataExt(dataExt, onlineEntrant);
        fillIndividualAchievements(dataExt, onlineEntrant);

        return dataExt;
    }

    protected void fillEntrantDataExt(EntrantDataExt dataExt, OnlineEntrant onlineEntrant) {
        OnlineEntrantExt onlineEntrantExt = get(OnlineEntrantExt.class, OnlineEntrantExt.entrant().id().s(), onlineEntrant.getId());
        if (onlineEntrantExt == null) {
            return;
        }

        dataExt.setSnilsNumber(onlineEntrantExt.getSnilsNumber());

        dataExt.setKinRelationDegreeId((Long) onlineEntrantExt.getProperty(OnlineEntrantExt.nextOfKinRelationDegree().id().s()));
        dataExt.setKinFirstName(onlineEntrantExt.getNextOfKinFirstName());
        dataExt.setKinLastName(onlineEntrantExt.getNextOfKinLastName());
        dataExt.setKinMiddleName(onlineEntrantExt.getNextOfKinMiddleName());
        dataExt.setKinPhone(onlineEntrantExt.getNextOfKinPhone());
        dataExt.setKinWorkPlace(onlineEntrantExt.getNextOfKinWorkPlace());
        dataExt.setKinWorkPost(onlineEntrantExt.getNextOfKinWorkPost());

        dataExt.setDescription(onlineEntrantExt.getDescription());

    }

    protected void fillIndividualAchievements(EntrantDataExt dataExt, OnlineEntrant onlineEntrant) {
        List<OnlineEntrant2IndividualAchievement> achievements = getList(OnlineEntrant2IndividualAchievement.class, OnlineEntrant2IndividualAchievement.entrant().id().s(), onlineEntrant.getId());
        if (achievements != null && !achievements.isEmpty()) {
            for (OnlineEntrant2IndividualAchievement achievement : achievements) {
                switch (achievement.getOrder()) {
                    case 1:
                        dataExt.setIndividualAchievementsId(achievement.getIndividualAchievements().getId());
                        dataExt.setIndividualAchievementsDesc(achievement.getText());
                        break;
                    case 2:
                        dataExt.setIndividualAchievementsId2(achievement.getIndividualAchievements().getId());
                        dataExt.setIndividualAchievementsDesc2(achievement.getText());
                        break;
                    case 3:
                        dataExt.setIndividualAchievementsId3(achievement.getIndividualAchievements().getId());
                        dataExt.setIndividualAchievementsDesc3(achievement.getText());
                        break;
                }
            }
        }

    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaignForOnline()
    {
        if (ApplicationRuntime.getProperty("econline.enrollmentcampaign.title") == null)
            // последная созданная
            return getLastEnrollmentCampaign();
        else {
            String title = ApplicationRuntime.getProperty("econline.enrollmentcampaign.title");
            EnrollmentCampaign ec = getUnique(EnrollmentCampaign.class, "title", title);
            if (ec != null)
                return ec;
            else
                throw new ApplicationException("В файле конфигурации неверно указан параметр econline.enrollmentcampaign.title");
        }
    }

    private EnrollmentCampaign getLastEnrollmentCampaign()
    {
        Criteria criteria = getSession().createCriteria(EnrollmentCampaign.class);
        criteria.addOrder(Order.desc(EnrollmentCampaign.id().s()));
        criteria.setMaxResults(1);
        return (EnrollmentCampaign) criteria.uniqueResult();
    }


    @Override
    public OnlineEntrant getOnlineEntrant(long userId)
    {
        EnrollmentCampaign ec = getEnrollmentCampaignForOnline();
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(OnlineEntrant.class, "e");

        // по пользователю
        dql.where(DQLExpressions.eq(DQLExpressions.property(OnlineEntrant.userId().fromAlias("e")), DQLExpressions.value(Long.valueOf(userId))));
        // по приемке
        dql.where(DQLExpressions.eq(DQLExpressions.property(OnlineEntrant.enrollmentCampaign().id().fromAlias("e")), DQLExpressions.value(ec.getId())));
        OnlineEntrant retVal = dql.createStatement(getSession()).uniqueResult();

        return retVal;

    }

    private boolean isOnlineEntrantEditable(OnlineEntrant onlineEntrant)
    {
        return onlineEntrant.getEntrant() == null;
    }

    @Override
    public List<UserDirectionDataExt> getUserDirectionsExt(long userId)
    {
        List<UserDirectionData> lst = super.getUserDirections(userId);

        List<UserDirectionDataExt> retVal = new ArrayList<>();

        for (UserDirectionData ud : lst) {
            UserDirectionDataExt udExt = new UserDirectionDataExt();
            udExt.fillByBase(ud);

            udExt.order = 0;

            retVal.add(udExt);
        }

        Collections.sort(retVal, new Comparator<UserDirectionDataExt>() {
                             @Override
                             public int compare(UserDirectionDataExt o1,
                                                UserDirectionDataExt o2)
                             {
                                 Integer order1 = o1.order;
                                 Integer order2 = o2.order;
                                 return order1.compareTo(order2);
                             }
                         }
        );


        return retVal;
    }


    @Override
    public void synchronizeUserDirections(long userId,
                                          UserDirectionDataExt[] userDirections)
    {
        OnlineEntrant onlineEntrant = getOnlineEntrant(userId);

        // удаляем все старые
        for (OnlineRequestedEnrollmentDirection direction : getList(
                OnlineRequestedEnrollmentDirection.class,
                OnlineRequestedEnrollmentDirection.entrant().s(), onlineEntrant)) {
            delete(direction);
        }

        // синхронизируем сессию с базой
        getSession().flush();

        // сохраняем новые
        for (UserDirectionDataExt userDirectionData : userDirections) {
            OnlineRequestedEnrollmentDirection direction = new OnlineRequestedEnrollmentDirection();
            direction.setEntrant(onlineEntrant);
            direction.setEnrollmentDirection(get(EnrollmentDirection.class,
                                                 userDirectionData.enrollmentDirectionId));
            direction.setCompensationType(get(CompensationType.class,
                                              userDirectionData.compensationTypeId));
            direction.setStudentCategory(get(StudentCategory.class,
                                             userDirectionData.studentCategoryId));
            direction.setCompetitionKind(get(CompetitionKind.class,
                                             userDirectionData.competitionKindId));
            direction.setTargetAdmission(userDirectionData.targetAdmission);
            direction
                    .setProfileWorkExperienceYears(userDirectionData.profileWorkExperienceYears);
            direction
                    .setProfileWorkExperienceMonths(userDirectionData.profileWorkExperienceMonths);
            direction
                    .setProfileWorkExperienceDays(userDirectionData.profileWorkExperienceDays);
            save(direction);

            OnlineRequestedEnrollmentDirectionOrder odOrder = new OnlineRequestedEnrollmentDirectionOrder();

            odOrder.setOnlineRequestedEnrollmentDirection(direction);
            odOrder.setOrder(userDirectionData.order);

            save(odOrder);
        }
    }

    protected void createOrUpdateIndividualAchievements(OnlineEntrant onlineEntrant, EntrantDataExt entrantDataExt) {

        Long achievementId = entrantDataExt.getIndividualAchievementsId();
        Long achievementId2 = entrantDataExt.getIndividualAchievementsId2();
        Long achievementId3 = entrantDataExt.getIndividualAchievementsId3();

        boolean hasAchievement = (achievementId != null) || (achievementId2 != null) || (achievementId3 != null);

        Map<Integer, OnlineEntrant2IndividualAchievement> achievementsMap = new HashMap<>();
        List<OnlineEntrant2IndividualAchievement> achievements = getList(OnlineEntrant2IndividualAchievement.class, OnlineEntrant2IndividualAchievement.entrant().id().s(), onlineEntrant.getId());
        if (achievements != null && !achievements.isEmpty()) {
            for (OnlineEntrant2IndividualAchievement achievement : achievements) {
                if (hasAchievement) {
                    achievementsMap.put(achievement.getOrder(), achievement);
                }
                else {
                    getSession().delete(achievement);
                }
            }
            getSession().flush();
        }

        if (hasAchievement) {
            OnlineEntrant2IndividualAchievement individualAchievement = achievementsMap.get(1);
            // Сохраняем или обновляем
            if (achievementId != null) {
                if (individualAchievement != null) {
                    individualAchievement.setText(entrantDataExt.getIndividualAchievementsDesc());
                }
                else {
                    individualAchievement = new OnlineEntrant2IndividualAchievement();
                    individualAchievement.setEntrant(onlineEntrant);
                    individualAchievement.setIndividualAchievements(safeGet(IndividualAchievements.class, IndividualAchievements.id().s(), entrantDataExt.getIndividualAchievementsId()));
                    individualAchievement.setText(entrantDataExt.getIndividualAchievementsDesc());
                    individualAchievement.setOrder(1);
                }

                getSession().saveOrUpdate(individualAchievement);

            }
            else if (individualAchievement != null) {
                getSession().delete(individualAchievement);
            }

            individualAchievement = achievementsMap.get(2);
            if (achievementId2 != null) {

                if (individualAchievement != null) {
                    individualAchievement.setText(entrantDataExt.getIndividualAchievementsDesc2());
                }
                else {
                    individualAchievement = new OnlineEntrant2IndividualAchievement();
                    individualAchievement.setEntrant(onlineEntrant);
                    individualAchievement.setIndividualAchievements(safeGet(IndividualAchievements.class, IndividualAchievements.id().s(), entrantDataExt.getIndividualAchievementsId2()));
                    individualAchievement.setText(entrantDataExt.getIndividualAchievementsDesc2());
                    individualAchievement.setOrder(2);
                }

                getSession().saveOrUpdate(individualAchievement);

            }
            else if (individualAchievement != null) {
                getSession().delete(individualAchievement);
            }

            individualAchievement = achievementsMap.get(3);
            if (achievementId3 != null) {
                if (individualAchievement != null) {
                    individualAchievement.setText(entrantDataExt.getIndividualAchievementsDesc3());
                }
                else {
                    individualAchievement = new OnlineEntrant2IndividualAchievement();
                    individualAchievement.setEntrant(onlineEntrant);
                    individualAchievement.setIndividualAchievements(safeGet(IndividualAchievements.class, IndividualAchievements.id().s(), entrantDataExt.getIndividualAchievementsId3()));
                    individualAchievement.setText(entrantDataExt.getIndividualAchievementsDesc3());
                    individualAchievement.setOrder(3);
                }

                getSession().saveOrUpdate(individualAchievement);

            }
            else if (individualAchievement != null) {
                getSession().delete(individualAchievement);
            }
        }
    }
}
