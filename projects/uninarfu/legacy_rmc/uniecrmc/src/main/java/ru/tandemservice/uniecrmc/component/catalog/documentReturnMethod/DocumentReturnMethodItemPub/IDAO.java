package ru.tandemservice.uniecrmc.component.catalog.documentReturnMethod.DocumentReturnMethodItemPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogItemPub.IDefaultCatalogItemPubDAO;
import ru.tandemservice.uniecrmc.entity.catalog.DocumentReturnMethod;

public interface IDAO extends IDefaultCatalogItemPubDAO<DocumentReturnMethod, Model> {
}
