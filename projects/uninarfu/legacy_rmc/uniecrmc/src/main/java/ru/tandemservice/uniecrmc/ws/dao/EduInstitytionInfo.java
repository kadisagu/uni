package ru.tandemservice.uniecrmc.ws.dao;

public class EduInstitytionInfo {
    public Long PersonId;

    public Double AvgMark()
    {

        Integer m3 = Mark3;
        Integer m4 = Mark4;
        Integer m5 = Mark5;

        m3 = (m3 != null) ? m3 : 0;
        m4 = (m4 != null) ? m4 : 0;
        m5 = (m5 != null) ? m5 : 0;

        int sum = m3 + m4 + m5;
        double total = m3 * 3 + m4 * 4 + m5 * 5;
        return (sum != 0) ? total / sum : null;

    }

    public Integer Mark3;
    public Integer Mark4;
    public Integer Mark5;
    public boolean IsMain;

    public String AttestatInfo;


}
