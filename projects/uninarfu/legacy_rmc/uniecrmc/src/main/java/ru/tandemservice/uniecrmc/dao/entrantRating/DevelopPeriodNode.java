package ru.tandemservice.uniecrmc.dao.entrantRating;

import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

public class DevelopPeriodNode {

    @XmlElement(name = "DevelopPeriod")
    public List<DevelopPeriodRow> list = new ArrayList<>();

    public DevelopPeriodNode() {
    }

    public void add(DevelopPeriod entity) {
        EduProgramDuration dur = entity.getEduProgramDuration();

        DevelopPeriodRow node = new DevelopPeriodRow(entity.getCode(), entity.getTitle(), entity.getLastCourse(), dur != null ? dur.getNumberOfYears() : null, dur != null ? dur.getNumberOfMonths() : null);
        list.add(node);
    }


    public static class DevelopPeriodRow extends RowNode {
        @XmlAttribute
        public int lastCourse;

        @XmlAttribute(required = false)
        public Integer years;

        @XmlAttribute(required = false)
        public Integer months;

        public DevelopPeriodRow() {
        }

        public DevelopPeriodRow(String code, String title, int lastCourse, Integer years, Integer months) {
            super(code, title, null);
            this.lastCourse = lastCourse;
            this.years = years;
            this.months = months;
        }
    }
}
