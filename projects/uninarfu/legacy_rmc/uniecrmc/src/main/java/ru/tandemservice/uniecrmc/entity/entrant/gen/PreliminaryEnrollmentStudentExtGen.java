package ru.tandemservice.uniecrmc.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniecrmc.entity.entrant.PreliminaryEnrollmentStudentExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение сущности Студент предварительного зачисления
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PreliminaryEnrollmentStudentExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniecrmc.entity.entrant.PreliminaryEnrollmentStudentExt";
    public static final String ENTITY_NAME = "preliminaryEnrollmentStudentExt";
    public static final int VERSION_HASH = -713463548;
    private static IEntityMeta ENTITY_META;

    public static final String L_PRELIMINARY_ENROLLMENT_STUDENT = "preliminaryEnrollmentStudent";
    public static final String P_HAVE_SPECIAL_RIGHTS = "haveSpecialRights";

    private PreliminaryEnrollmentStudent _preliminaryEnrollmentStudent;     // Студент предварительного зачисления
    private boolean _haveSpecialRights;     // Имеет особые права

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Студент предварительного зачисления. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public PreliminaryEnrollmentStudent getPreliminaryEnrollmentStudent()
    {
        return _preliminaryEnrollmentStudent;
    }

    /**
     * @param preliminaryEnrollmentStudent Студент предварительного зачисления. Свойство не может быть null и должно быть уникальным.
     */
    public void setPreliminaryEnrollmentStudent(PreliminaryEnrollmentStudent preliminaryEnrollmentStudent)
    {
        dirty(_preliminaryEnrollmentStudent, preliminaryEnrollmentStudent);
        _preliminaryEnrollmentStudent = preliminaryEnrollmentStudent;
    }

    /**
     * @return Имеет особые права. Свойство не может быть null.
     */
    @NotNull
    public boolean isHaveSpecialRights()
    {
        return _haveSpecialRights;
    }

    /**
     * @param haveSpecialRights Имеет особые права. Свойство не может быть null.
     */
    public void setHaveSpecialRights(boolean haveSpecialRights)
    {
        dirty(_haveSpecialRights, haveSpecialRights);
        _haveSpecialRights = haveSpecialRights;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PreliminaryEnrollmentStudentExtGen)
        {
            setPreliminaryEnrollmentStudent(((PreliminaryEnrollmentStudentExt)another).getPreliminaryEnrollmentStudent());
            setHaveSpecialRights(((PreliminaryEnrollmentStudentExt)another).isHaveSpecialRights());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PreliminaryEnrollmentStudentExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PreliminaryEnrollmentStudentExt.class;
        }

        public T newInstance()
        {
            return (T) new PreliminaryEnrollmentStudentExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "preliminaryEnrollmentStudent":
                    return obj.getPreliminaryEnrollmentStudent();
                case "haveSpecialRights":
                    return obj.isHaveSpecialRights();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "preliminaryEnrollmentStudent":
                    obj.setPreliminaryEnrollmentStudent((PreliminaryEnrollmentStudent) value);
                    return;
                case "haveSpecialRights":
                    obj.setHaveSpecialRights((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "preliminaryEnrollmentStudent":
                        return true;
                case "haveSpecialRights":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "preliminaryEnrollmentStudent":
                    return true;
                case "haveSpecialRights":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "preliminaryEnrollmentStudent":
                    return PreliminaryEnrollmentStudent.class;
                case "haveSpecialRights":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PreliminaryEnrollmentStudentExt> _dslPath = new Path<PreliminaryEnrollmentStudentExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PreliminaryEnrollmentStudentExt");
    }
            

    /**
     * @return Студент предварительного зачисления. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniecrmc.entity.entrant.PreliminaryEnrollmentStudentExt#getPreliminaryEnrollmentStudent()
     */
    public static PreliminaryEnrollmentStudent.Path<PreliminaryEnrollmentStudent> preliminaryEnrollmentStudent()
    {
        return _dslPath.preliminaryEnrollmentStudent();
    }

    /**
     * @return Имеет особые права. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.PreliminaryEnrollmentStudentExt#isHaveSpecialRights()
     */
    public static PropertyPath<Boolean> haveSpecialRights()
    {
        return _dslPath.haveSpecialRights();
    }

    public static class Path<E extends PreliminaryEnrollmentStudentExt> extends EntityPath<E>
    {
        private PreliminaryEnrollmentStudent.Path<PreliminaryEnrollmentStudent> _preliminaryEnrollmentStudent;
        private PropertyPath<Boolean> _haveSpecialRights;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Студент предварительного зачисления. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniecrmc.entity.entrant.PreliminaryEnrollmentStudentExt#getPreliminaryEnrollmentStudent()
     */
        public PreliminaryEnrollmentStudent.Path<PreliminaryEnrollmentStudent> preliminaryEnrollmentStudent()
        {
            if(_preliminaryEnrollmentStudent == null )
                _preliminaryEnrollmentStudent = new PreliminaryEnrollmentStudent.Path<PreliminaryEnrollmentStudent>(L_PRELIMINARY_ENROLLMENT_STUDENT, this);
            return _preliminaryEnrollmentStudent;
        }

    /**
     * @return Имеет особые права. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.PreliminaryEnrollmentStudentExt#isHaveSpecialRights()
     */
        public PropertyPath<Boolean> haveSpecialRights()
        {
            if(_haveSpecialRights == null )
                _haveSpecialRights = new PropertyPath<Boolean>(PreliminaryEnrollmentStudentExtGen.P_HAVE_SPECIAL_RIGHTS, this);
            return _haveSpecialRights;
        }

        public Class getEntityClass()
        {
            return PreliminaryEnrollmentStudentExt.class;
        }

        public String getEntityName()
        {
            return "preliminaryEnrollmentStudentExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
