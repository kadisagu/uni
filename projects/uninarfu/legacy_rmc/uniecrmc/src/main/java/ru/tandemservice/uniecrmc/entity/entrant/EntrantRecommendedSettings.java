package ru.tandemservice.uniecrmc.entity.entrant;

import ru.tandemservice.uniecrmc.dao.entrantRating.RatingBean;
import ru.tandemservice.uniecrmc.entity.entrant.gen.EntrantRecommendedSettingsGen;

/**
 * Алгоритм распределения (рейтинга абитуриентов) при равенстве суммы баллов
 */
public class EntrantRecommendedSettings extends EntrantRecommendedSettingsGen
{
    public boolean isDemonRunNow() {
        return RatingBean.instance().isActive(this.getEnrollmentCampaign().getId());
    }
}