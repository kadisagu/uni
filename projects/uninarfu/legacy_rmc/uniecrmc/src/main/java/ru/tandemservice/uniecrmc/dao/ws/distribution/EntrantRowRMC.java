package ru.tandemservice.uniecrmc.dao.ws.distribution;

import ru.tandemservice.uniec.ws.distribution.EnrollmentDistributionEnvironmentNode;

public class EntrantRowRMC extends EnrollmentDistributionEnvironmentNode.EntrantNode.EntrantRow {

    public boolean haveSpecialRights;
    public String recommendationTitle;

}
