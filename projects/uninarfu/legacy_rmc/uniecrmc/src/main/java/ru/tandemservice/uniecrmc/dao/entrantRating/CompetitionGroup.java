package ru.tandemservice.uniecrmc.dao.entrantRating;

import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.settings.TargetAdmissionPlanRelation;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;


public class CompetitionGroup {

    public static final String DEFAULT_TARGET_ADMISSION_KIND_CODE = "defCode";
    public static final String DEFAULT_TARGET_ADMISSION_KIND_CODE_BY_TU = "1";


    private AtomicInteger total;

    private CompetitionGroupType type;
    private String code;
    private Integer plan;
    private List<EcgCortegeRMC> acceptList = new ArrayList<>();

    public CompetitionGroup(TargetAdmissionPlanRelation rel, AtomicInteger total) {
        this.type = CompetitionGroupType.TARGET_ADMISSION;
        this.code = rel.getTargetAdmissionKind().getCode();
        this.total = total;
        this.plan = rel.getPlanValue();
    }

    public CompetitionGroup(CompetitionGroupType type, AtomicInteger total, Integer plan) {
        this.type = type;
        this.code = null;
        this.total = total;
        this.plan = plan;
    }

    public CompetitionGroup(CompetitionGroupType type, AtomicInteger total, Integer plan, String codeTA) {
        this.type = type;
        this.code = codeTA;
        this.total = total;
        this.plan = plan;
    }

    public CompetitionGroupType getType() {
        return this.type;
    }

    public Integer getPlan() {
        return this.plan;
    }

    public String getTargetAdmissionCode() {
        return this.code;
    }

    public void accept(List<EcgCortegeRMC> list) {
        AtomicInteger plan = new AtomicInteger(this.plan != null ? this.plan.intValue() : Integer.MAX_VALUE);

        for (int i = 0; i < list.size(); i++) {
            EcgCortegeRMC container = list.get(i);
            boolean accepted = false;

            if (this.type == CompetitionGroupType.TARGET_ADMISSION) {
                if (!container.isTargetAdmission())
                    continue;
                // vch видов целевого приема может и не быть
                // в этом случае нужно принять значение по умолчание - в тандеме для TargetAdmissionKind это "1"
                String currentTAKindCode = container.getTargetAdmissionKind() != null ? container.getTargetAdmissionKind().getCode() : DEFAULT_TARGET_ADMISSION_KIND_CODE_BY_TU;

                if (!currentTAKindCode.equals(this.code) && this.code != null)
                    continue;

                accepted = accept(list, i, plan);

            }
            else if (this.type == CompetitionGroupType.BEZ_EKZ) {
                if (!UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES.equals(container.getCompetitionKind().getCode()))
                    continue;

                accepted = accept(list, i, plan);

            }
            else if (this.type == CompetitionGroupType.SPECIAL_RIGHT) {
                if (!container.isHaveSpecialRights())
                    continue;

                accepted = accept(list, i, plan);

            }
            else {
                accepted = accept(list, i, plan);
            }

            if (accepted)
                i--;
        }

    }

    private boolean accept(List<EcgCortegeRMC> list, int index, AtomicInteger plan) {
        EcgCortegeRMC container = list.get(index);
        RequestedEnrollmentDirection red = (RequestedEnrollmentDirection) container.getEntity();
        boolean documentOk = red.isOriginalDocumentHandedIn() && !red.getEntrantRequest().isTakeAwayDocument();

        if (!documentOk) {
            container.setEnrolled(false);
            this.acceptList.add(container);
            list.remove(index);
            return true;
        }

        container.setEnrolled(plan.get() > 0 && this.total.get() > 0);
        this.acceptList.add(container);
        list.remove(index);

        if (container.isEnrolled()) {
            total.decrementAndGet();
            plan.decrementAndGet();
        }
        else {
            if (this.getType() != CompetitionGroupType.COMMON) {
                container = container.clone();
                container.setCommon();

                if (!list.contains(container))
                    list.add(container);

                //на всякий случай, если зациклится чтоб не переполнило память
                if (list.size() > 50000)
                    throw new ApplicationException(new StringBuilder()
                                                           .append("Ошибка расчета рейтинга.")
                                                           .append("Направление:").append(red.getTitle())
                                                           .append(", тип затрат:").append(red.getCompensationType().getTitle())
                                                           .append(", группа:").append(this.type)
                                                           .toString());
            }
        }

        return true;
    }

    public List<EcgCortegeRMC> getAcceptList() {
        return this.acceptList;
    }


    public static enum CompetitionGroupType {
        TARGET_ADMISSION("Целевой прием", 1),
        BEZ_EKZ("Без экзаменов", 2),
        SPECIAL_RIGHT("Специальные права", 3),
        COMMON("На общих основаниях", 4);

        private String title;
        private int id;

        private CompetitionGroupType(String title, int id) {
            this.id = id;
            this.title = title;
        }

        public String getTitle() {
            return title;
        }

        public String toString() {
            return title;
        }

        public int getId() {
            return id;
        }
    }
}