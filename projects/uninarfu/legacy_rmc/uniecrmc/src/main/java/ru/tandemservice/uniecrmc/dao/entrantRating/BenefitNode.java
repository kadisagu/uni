package ru.tandemservice.uniecrmc.dao.entrantRating;

import org.tandemframework.shared.person.catalog.entity.Benefit;

import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

public class BenefitNode {

    @XmlElement(name = "Benefit")
    public List<RowNode> list = new ArrayList<>();

    public BenefitNode() {
    }

    public void add(Benefit entity) {
        RowNode node = new RowNode(entity.getCode(), entity.getTitle(), entity.getShortTitle());
        list.add(node);
    }

    public void addShort(Benefit entity) {
        RowNode node = new RowNode(entity.getCode(), null, null);
        list.add(node);
    }
}
