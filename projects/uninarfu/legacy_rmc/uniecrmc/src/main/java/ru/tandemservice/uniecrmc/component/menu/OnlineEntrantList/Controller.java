package ru.tandemservice.uniecrmc.component.menu.OnlineEntrantList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;

public class Controller extends ru.tandemservice.uniec.component.menu.OnlineEntrantList.Controller {
    @Override
    public void prepareDataSource(IBusinessComponent component) {
        super.prepareDataSource(component);
        DynamicListDataSource<OnlineEntrant> dataSource = getModel(component).getDataSource();
        Integer columnsPos = dataSource.getColumn("entrantRegDate").getNumber();
        dataSource.addColumn(new SimpleColumn("E-mail", OnlineEntrant.email().s()), columnsPos + 1);
        dataSource.addColumn(new SimpleColumn("Мобильный телефон", OnlineEntrant.phoneMobile().s()), columnsPos + 2);
    }
}
