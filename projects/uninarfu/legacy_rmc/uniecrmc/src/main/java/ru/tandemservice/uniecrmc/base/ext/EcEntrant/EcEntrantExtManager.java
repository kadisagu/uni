package ru.tandemservice.uniecrmc.base.ext.EcEntrant;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.uniecrmc.base.ext.EcEntrant.logic.EcEntrantDaoExt;
import ru.tandemservice.uniec.base.bo.EcEntrant.logic.IEcEntrantDao;

@Configuration
public class EcEntrantExtManager extends BusinessObjectExtensionManager {

    @Bean
    @BeanOverride
    public IEcEntrantDao dao() {
        return new EcEntrantDaoExt();
    }
}
