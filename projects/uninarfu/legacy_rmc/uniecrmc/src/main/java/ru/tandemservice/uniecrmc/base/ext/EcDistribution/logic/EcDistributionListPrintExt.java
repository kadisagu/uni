package ru.tandemservice.uniecrmc.base.ext.EcDistribution.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.EcDistributionUtil;
import ru.tandemservice.uniecrmc.dao.ws.distribution.EntrantRowRMC;
import ru.tandemservice.uniecrmc.dao.ws.distribution.QuotaRowRMC;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.base.bo.EcDistribution.EcDistributionManager;
import ru.tandemservice.uniec.base.bo.EcDistribution.logic.EcDistributionListPrint;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgDistribution;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistribution;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.ws.distribution.EnrollmentDistributionEnvironmentNode;
import ru.tandemservice.uniec.ws.distribution.EnrollmentDistributionEnvironmentNode.BenefitNode.BenefitRow;
import ru.tandemservice.uniec.ws.distribution.EnrollmentDistributionEnvironmentNode.CompetitionKindNode.CompetitionKindRow;
import ru.tandemservice.uniec.ws.distribution.EnrollmentDistributionEnvironmentNode.DistributionNode.DistributionRow;
import ru.tandemservice.uniec.ws.distribution.EnrollmentDistributionEnvironmentNode.Row;
import ru.tandemservice.uniec.ws.distribution.EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow;
import ru.tandemservice.uniec.ws.distribution.IEnrollmentDistributionServiceDao;

import java.util.*;

public class EcDistributionListPrintExt extends EcDistributionListPrint {

    @Override
    public RtfDocument getPrintFormForDistributionPerDirection(byte[] template,
                                                               EcgDistribution distribution)
    {

        EnrollmentDirection enrollmentDirection = (EnrollmentDirection) distribution.getConfig().getEcgItem();

        if (!EcDistributionUtil.isUsePriority(enrollmentDirection.getEnrollmentCampaign()))
            return super.getPrintFormForDistributionPerDirection(template, distribution);

        RtfDocument document = new RtfReader().read(template);

        EducationOrgUnit educationOrgUnit = enrollmentDirection.getEducationOrgUnit();
        OrgUnit formativeOrgUnit = educationOrgUnit.getFormativeOrgUnit();
        String formativeOrgUnitTitle = StringUtils.isEmpty(formativeOrgUnit.getNominativeCaseTitle()) ? formativeOrgUnit.getTitle() : formativeOrgUnit.getNominativeCaseTitle();

        EnrollmentDistributionEnvironmentNode env = IEnrollmentDistributionServiceDao.INSTANCE.get().getEnrollmentDistributionEnvironmentNode(Collections.<IEcgDistribution>singleton(distribution));
        EnrollmentDistributionEnvironmentNode.DistributionNode.DistributionRow distributionRow = (EnrollmentDistributionEnvironmentNode.DistributionNode.DistributionRow) env.distribution.row.get(0);

        Map<String, EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow> targetAdmissionKindMap = new HashMap<String, EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow>();
        Map<String, EnrollmentDistributionEnvironmentNode.CompetitionKindNode.CompetitionKindRow> competitionKindMap = new HashMap<String, EnrollmentDistributionEnvironmentNode.CompetitionKindNode.CompetitionKindRow>();
        Map<String, EnrollmentDistributionEnvironmentNode.BenefitNode.BenefitRow> benefitMap = new HashMap<String, EnrollmentDistributionEnvironmentNode.BenefitNode.BenefitRow>();
        Map<String, EnrollmentDistributionEnvironmentNode.Row> documentStateMap = new HashMap<String, EnrollmentDistributionEnvironmentNode.Row>();
        Map<String, EnrollmentDistributionEnvironmentNode.Row> enrollmentStateMap = new HashMap<String, EnrollmentDistributionEnvironmentNode.Row>();
        EcDistributionUtil.invokePrivateMethod(this, EcDistributionListPrint.class, "fillTitleMap", new Object[]{env, targetAdmissionKindMap, competitionKindMap, benefitMap, documentStateMap, enrollmentStateMap});

        List<String[]> tableRow = new ArrayList<String[]>();
        Set<Integer> unionRowIndexList = new HashSet<Integer>();
        fillTableRowListForDistributionPerDirection(distributionRow, tableRow, unionRowIndexList, targetAdmissionKindMap, competitionKindMap, benefitMap, documentStateMap, enrollmentStateMap);

        RtfInjectModifier im = new RtfInjectModifier();
        im.put("title", formativeOrgUnitTitle);
        im.put("educationLevelTitle", enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle());
        im.put("developForm", new StringBuilder().append(educationOrgUnit.getDevelopForm().getTitle().toLowerCase()).append(" форма").toString());
        im.put("developCondition", educationOrgUnit.getDevelopCondition().getTitle().toLowerCase());
        im.put("developPeriod", educationOrgUnit.getDevelopPeriod().getTitle().toLowerCase());
        im.put("compensationType", distribution.getConfig().getCompensationType().getShortTitle().toLowerCase());

        EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow quotaRow = (EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow) distributionRow.quota.row.get(0);
        StringBuilder quotaTitle = new StringBuilder();
        quotaTitle.append("Количество мест ").append(distribution.getConfig().getCompensationType().isBudget() ? "на бюджет: " : "по договору: ").append(quotaRow.countBegin);

        int taQuotaCountBegin = 0;
        for (EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow.TaQuotaNode.TaQuotaRow row : quotaRow.taQuota.row) {
            taQuotaCountBegin += row.countBegin;
        }

        if (taQuotaCountBegin > 0) {
            quotaTitle.append(", Целевой прием: ");
            if (quotaRow.defaultTaKind)
                quotaTitle.append(taQuotaCountBegin);
            else {
                List<String> list = new ArrayList<String>();
                for (EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow.TaQuotaNode.TaQuotaRow row : quotaRow.taQuota.row)
                    list.add(new StringBuilder().append(((EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow) targetAdmissionKindMap.get(row.id)).shortTitle).append(" – ").append(row.countBegin).toString());
                quotaTitle.append(StringUtils.join(list, ", "));
            }
        }

        if (quotaRow instanceof QuotaRowRMC && ((QuotaRowRMC) quotaRow).srQuotaRow.countBegin > 0) {
            quotaTitle.append(", Квота для лиц с особыми правами: ")
                    .append(((QuotaRowRMC) quotaRow).srQuotaRow.countBegin);
        }

        im.put("quota", quotaTitle.toString());
        im.modify(document);
        fillTableData(document, document.getHeader().getColorTable().addColor(217, 217, 217), 4, tableRow, unionRowIndexList, distributionRow);

        return document;
    }

    @Override
    public RtfDocument getPrintFormForDistributionPerCompetitionGroup(
            byte[] template, IEcgDistribution distribution)
    {

        List<EnrollmentDirection> enrollmentDirections = EcDistributionManager.instance().dao().getDistributionDirectionList(distribution.getDistribution());

        if (!EcDistributionUtil.isUsePriority(enrollmentDirections.get(0).getEnrollmentCampaign()))
            return super.getPrintFormForDistributionPerCompetitionGroup(template,
                                                                        distribution);

        RtfDocument templateDocument = new RtfReader().read(template);

        RtfDocument result = RtfBean.getElementFactory().createRtfDocument();
        result.setHeader(templateDocument.getHeader());
        result.setSettings(templateDocument.getSettings());
        int grayColorIndex = result.getHeader().getColorTable().addColor(217, 217, 217);

        EnrollmentDistributionEnvironmentNode env = ((IEnrollmentDistributionServiceDao) IEnrollmentDistributionServiceDao.INSTANCE.get()).getEnrollmentDistributionEnvironmentNode(Collections.singleton(distribution));
        EnrollmentDistributionEnvironmentNode.DistributionNode.DistributionRow distributionRow = (EnrollmentDistributionEnvironmentNode.DistributionNode.DistributionRow) env.distribution.row.get(0);

        Map<String, EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow> targetAdmissionKindMap = new HashMap<String, EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow>();
        Map<String, EnrollmentDistributionEnvironmentNode.CompetitionKindNode.CompetitionKindRow> competitionKindMap = new HashMap<String, EnrollmentDistributionEnvironmentNode.CompetitionKindNode.CompetitionKindRow>();
        Map<String, EnrollmentDistributionEnvironmentNode.BenefitNode.BenefitRow> benefitMap = new HashMap<String, EnrollmentDistributionEnvironmentNode.BenefitNode.BenefitRow>();
        Map<String, EnrollmentDistributionEnvironmentNode.Row> documentStateMap = new HashMap<String, EnrollmentDistributionEnvironmentNode.Row>();
        Map<String, EnrollmentDistributionEnvironmentNode.Row> enrollmentStateMap = new HashMap<String, EnrollmentDistributionEnvironmentNode.Row>();
        EcDistributionUtil.invokePrivateMethod(this, EcDistributionListPrint.class, "fillTitleMap", new Object[]{env, targetAdmissionKindMap, competitionKindMap, benefitMap, documentStateMap, enrollmentStateMap});

        Map<String, EnrollmentDirection> directionMap = new HashMap<String, EnrollmentDirection>();
        for (EnrollmentDirection direction : enrollmentDirections) {
            directionMap.put(Long.toString(direction.getId().longValue()), direction);
        }
        String currentWave = Integer.toString(distributionRow.wave);
        String currentDate = DateFormatter.DEFAULT_DATE_FORMATTER.format(null != distribution.getApprovalDate() ? distribution.getApprovalDate() : new Date());
        String compensationType = distribution.getConfig().getCompensationType().isBudget() ? "на места, финансируемые из федерального бюджета" : "на места с оплатой стоимости обучения";
        String developForm = ((EnrollmentDirection) directionMap.get(((EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow) distributionRow.quota.row.get(0)).id)).getEducationOrgUnit().getDevelopForm().getGenCaseTitle();

        RtfInjectModifier im = new RtfInjectModifier();
        im.put("wave", currentWave);
        im.put("date", currentDate);
        im.put("title", distributionRow.ecgItemTitle);
        im.put("compensationType", compensationType);
        im.put("developForm", developForm);

        List<String> listA = new ArrayList<String>();
        List<String> listB = new ArrayList<String>();
        for (EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow quotaRow : distributionRow.quota.row) {
            EnrollmentDirection direction = (EnrollmentDirection) directionMap.get(quotaRow.id);

            (direction.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isSpecialityPrintTitleMode() ? listA : listB).add(
                    new StringBuilder().append(direction.getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle()).append(" (")
                            .append(direction.getEducationOrgUnit().getEducationLevelHighSchool().getShortTitle()).append(")").toString());
        }

        String listAtitle = listA.isEmpty() ? null : new StringBuilder().append(listA.size() == 1 ? "по специальности: " : "по специальностям: ").append(StringUtils.join(listA, ", ")).toString();
        String listBtitle = listB.isEmpty() ? null : new StringBuilder().append(listB.size() == 1 ? "по направлению: " : "по направлениям: ").append(StringUtils.join(listB, ", ")).toString();
        RtfString directionTitle = new RtfString();
        if (listAtitle == null)
            directionTitle.append(listBtitle);
        else if (listBtitle == null)
            directionTitle.append(listAtitle);
        else
            directionTitle.append(listBtitle).par().append(listAtitle);
        im.put("directionTitle", directionTitle);

        Map<String, String> id2planTitle = new HashMap<String, String>();
        int totalCountBegin = 0;
        int totalSrQuotaCountBegin = 0;
        int totalTaQuotaCountBegin = 0;
        Map<String, Integer> totalTaKindId2plan = new HashMap<String, Integer>();
        for (EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow quotaRow : distributionRow.quota.row) {
            StringBuilder quotaTitle = new StringBuilder();
            quotaTitle.append("Количество мест ").append(distribution.getConfig().getCompensationType().isBudget() ? "на бюджет: " : "по договору: ").append(quotaRow.countBegin);

            totalCountBegin += quotaRow.countBegin;

            int taQuotaCountBegin = 0;
            for (EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow.TaQuotaNode.TaQuotaRow row : quotaRow.taQuota.row) {
                taQuotaCountBegin += row.countBegin;
            }
            totalTaQuotaCountBegin += taQuotaCountBegin;

            if (taQuotaCountBegin > 0) {
                quotaTitle.append(", Целевой прием: ");
                if (quotaRow.defaultTaKind)
                    quotaTitle.append(taQuotaCountBegin);
                else {
                    List<String> list = new ArrayList<String>();
                    for (EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow.TaQuotaNode.TaQuotaRow row : quotaRow.taQuota.row) {
                        list.add(new StringBuilder().append(targetAdmissionKindMap.get(row.id).shortTitle).append(" – ").append(row.countBegin).toString());

                        Integer plan = (Integer) totalTaKindId2plan.get(row.id);
                        totalTaKindId2plan.put(row.id, Integer.valueOf((plan == null ? 0 : plan.intValue()) + row.countBegin));
                    }
                    quotaTitle.append(StringUtils.join(list, ", "));
                }
            }

            if (quotaRow instanceof QuotaRowRMC && ((QuotaRowRMC) quotaRow).srQuotaRow.countBegin > 0) {
                quotaTitle.append(", Квота для лиц с особыми правами: ")
                        .append(((QuotaRowRMC) quotaRow).srQuotaRow.countBegin);
                totalSrQuotaCountBegin += ((QuotaRowRMC) quotaRow).srQuotaRow.countBegin;
            }

            id2planTitle.put(quotaRow.id, quotaTitle.toString());
        }
        StringBuilder quotaTitle = new StringBuilder();
        quotaTitle.append("Количество мест ").append(distribution.getConfig().getCompensationType().isBudget() ? "на бюджет: " : "по договору: ").append(totalCountBegin);

        if (totalTaQuotaCountBegin > 0) {
            quotaTitle.append(", Целевой прием: ");
            if (totalTaKindId2plan.isEmpty())
                quotaTitle.append(totalTaQuotaCountBegin);
            else {
                List<String> list = new ArrayList<String>();
                for (EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow.TaQuotaNode.TaQuotaRow row : distributionRow.quota.row.get(0).taQuota.row) {
                    Integer plan = (Integer) totalTaKindId2plan.get(row.id);
                    list.add(new StringBuilder().append(targetAdmissionKindMap.get(row.id).shortTitle).append(" – ").append(plan == null ? 0 : plan.intValue()).toString());
                }
                quotaTitle.append(StringUtils.join(list, ", "));
            }
        }

        if (totalSrQuotaCountBegin > 0) {
            quotaTitle.append(", Квота для лиц с особыми правами: ")
                    .append(totalSrQuotaCountBegin);
        }

        im.put("planTitle", quotaTitle.toString());

        RtfDocument clone = templateDocument.getClone();
        im.modify(clone);

        List<String[]> tableRow = new ArrayList<String[]>();
        Set<Integer> unionRowIndexList = new HashSet<Integer>();
        fillTableRowListForDistributionPerCompetitionGroup(distributionRow, null, tableRow, unionRowIndexList, targetAdmissionKindMap, competitionKindMap, benefitMap, documentStateMap, enrollmentStateMap, directionMap);

        fillTableData(clone, grayColorIndex, 3, tableRow, unionRowIndexList, distributionRow);

        result.getElementList().addAll(clone.getElementList());

        for (EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow quotaRow : distributionRow.quota.row) {
            im = new RtfInjectModifier();
            im.put("wave", currentWave);
            im.put("date", currentDate);
            im.put("title", distributionRow.ecgItemTitle);
            im.put("compensationType", compensationType);
            im.put("developForm", developForm);

            EnrollmentDirection direction = (EnrollmentDirection) directionMap.get(quotaRow.id);
            EducationLevelsHighSchool hs = direction.getEducationOrgUnit().getEducationLevelHighSchool();
            im.put("directionTitle", new StringBuilder().append(hs.getEducationLevel().getLevelType().isSpecialityPrintTitleMode() ? "по специальности: " : "по направлению: ").append(hs.getPrintTitle()).append(" (").append(hs.getShortTitle()).append(")").toString());
            im.put("planTitle", (String) id2planTitle.get(quotaRow.id));

            clone = templateDocument.getClone();
            im.modify(clone);

            tableRow.clear();
            unionRowIndexList.clear();
            fillTableRowListForDistributionPerCompetitionGroup(distributionRow, quotaRow.id, tableRow, unionRowIndexList, targetAdmissionKindMap, competitionKindMap, benefitMap, documentStateMap, enrollmentStateMap, directionMap);

            fillTableData(clone, grayColorIndex, 3, tableRow, unionRowIndexList, distributionRow);

            result.getElementList().addAll(clone.getElementList());
        }

        return result;

    }

    @Override
    protected void fillTableRowListForDistributionPerDirection(
            DistributionRow distributionRow, List<String[]> tableRowList,
            Set<Integer> unionRowIndexList,
            Map<String, TargetAdmissionKindRow> targetAdmissionKindMap,
            Map<String, CompetitionKindRow> competitionKindMap,
            Map<String, BenefitRow> benefitMap,
            Map<String, Row> documentStateMap,
            Map<String, Row> enrollmentStateMap)
    {

        int currentRowKey = -1;
        int rowIndex = 0;
        int number = 1;

        for (EnrollmentDistributionEnvironmentNode.EntrantNode.EntrantRow row : distributionRow.entrant.row) {
            int rowKey = row.targetAdmissionKind == null ? Integer.parseInt(row.competitionKind == null ? "5" : row.competitionKind) : 0;

            if (row instanceof EntrantRowRMC && ((EntrantRowRMC) row).haveSpecialRights)
                rowKey = -10;

            if (currentRowKey != rowKey) {
                currentRowKey = rowKey;
                unionRowIndexList.add(Integer.valueOf(rowIndex));

                String title = "";
                if (row.targetAdmissionKind != null)
                    title = "Целевой прием";
                else if (row instanceof EntrantRowRMC && ((EntrantRowRMC) row).haveSpecialRights)
                    title = "Вне конкурса";
                else
                    title = competitionKindMap.get(row.competitionKind == null ? "5" : row.competitionKind).title;

                tableRowList.add(new String[]{null, title});
                rowIndex++;
                number = 1;
            }

            List<String> rowTitleList = new ArrayList<String>();

            rowTitleList.add(Integer.toString(number++));
            rowTitleList.add(row.regNumber);
            rowTitleList.add(row.fio);
            rowTitleList.add(row.finalMark);

            if (distributionRow.discipline.row.size() == 0)
                rowTitleList.add(null);
            else if (row.marks == null)
                for (int i = 0; i < distributionRow.discipline.row.size(); i++)
                    rowTitleList.add(null);
            else
                for (String mark : row.marks)
                    rowTitleList.add(mark.equals("-") ? null : mark);

            if (row instanceof EntrantRowRMC)
                rowTitleList.add(((EntrantRowRMC) row).recommendationTitle);
            else
                rowTitleList.add("");

            rowTitleList.add(row.documentState == null ? null : ((EnrollmentDistributionEnvironmentNode.Row) documentStateMap.get(row.documentState)).title);

            if (row.targetAdmissionKind != null)
                rowTitleList.add(((EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow) targetAdmissionKindMap.get(row.targetAdmissionKind)).shortTitle);
            else if (row instanceof EntrantRowRMC && ((EntrantRowRMC) row).haveSpecialRights)
                rowTitleList.add("Вне конкурса");
            else if (row.competitionKind == null)
                rowTitleList.add(null);
            else if ((row.competitionKind.equals("3")) && (row.benefitList != null) && (!row.benefitList.isEmpty())) {
                List<String> list = new ArrayList<String>();
                for (String benefit : row.benefitList)
                    list.add(((EnrollmentDistributionEnvironmentNode.BenefitNode.BenefitRow) benefitMap.get(benefit)).shortTitle);
                rowTitleList.add(new StringBuilder().append(((EnrollmentDistributionEnvironmentNode.CompetitionKindNode.CompetitionKindRow) competitionKindMap.get(row.competitionKind)).shortTitle).append(", ").append(StringUtils.join(list, ", ")).toString());
            }
            else
                rowTitleList.add(((EnrollmentDistributionEnvironmentNode.CompetitionKindNode.CompetitionKindRow) competitionKindMap.get(row.competitionKind)).shortTitle);

            rowTitleList.add(row.enrollmentState == null ? null : new StringBuilder().append(((EnrollmentDistributionEnvironmentNode.Row) enrollmentStateMap.get(row.enrollmentState)).title.toLowerCase()).append(row.orderNumber == null ? "" : new StringBuilder().append(", приказ №").append(row.orderNumber).toString()).toString());

            tableRowList.add(rowTitleList.toArray(new String[rowTitleList.size()]));
            rowIndex++;
        }

    }

    @Override
    protected void fillTableRowListForDistributionPerCompetitionGroup(
            DistributionRow distributionRow, String directionId,
            List<String[]> tableRowList, Set<Integer> unionRowIndexList,
            Map<String, TargetAdmissionKindRow> targetAdmissionKindMap,
            Map<String, CompetitionKindRow> competitionKindMap,
            Map<String, BenefitRow> benefitMap,
            Map<String, Row> documentStateMap,
            Map<String, Row> enrollmentStateMap,
            Map<String, EnrollmentDirection> directionMap)
    {

        int currentRowKey = -1;
        int rowIndex = 0;
        int number = 1;

        for (EnrollmentDistributionEnvironmentNode.EntrantNode.EntrantRow row : distributionRow.entrant.row) {
            if ((directionId == null) || ((row.enrollmentState != null) && (directionId.equals(row.enrollmentDirection)))) {
                int rowKey = row.targetAdmissionKind == null ? Integer.parseInt(row.competitionKind == null ? "5" : row.competitionKind) : 0;

                if (row instanceof EntrantRowRMC && ((EntrantRowRMC) row).haveSpecialRights)
                    rowKey = -10;

                if (currentRowKey != rowKey) {
                    currentRowKey = rowKey;
                    unionRowIndexList.add(Integer.valueOf(rowIndex));

                    String title = "";
                    if (row.targetAdmissionKind != null)
                        title = "Целевой прием";
                    else if (row instanceof EntrantRowRMC && ((EntrantRowRMC) row).haveSpecialRights)
                        title = "Вне конкурса";
                    else
                        title = competitionKindMap.get(row.competitionKind == null ? "5" : row.competitionKind).title;

                    tableRowList.add(new String[]{null, title});
                    rowIndex++;
                    number = 1;
                }

                List<String> rowTitleList = new ArrayList<String>();

                rowTitleList.add(Integer.toString(number++));
                rowTitleList.add(row.fio);
                rowTitleList.add(row.finalMark);

                if (distributionRow.discipline.row.size() == 0)
                    rowTitleList.add(null);
                else if (row.marks == null)
                    for (int i = 0; i < distributionRow.discipline.row.size(); i++)
                        rowTitleList.add(null);
                else
                    for (String mark : row.marks)
                        rowTitleList.add(mark.equals("-") ? null : mark);

                if (row instanceof EntrantRowRMC)
                    rowTitleList.add(((EntrantRowRMC) row).recommendationTitle);
                else
                    rowTitleList.add("");

                rowTitleList.add(row.documentState == null ? null : ((EnrollmentDistributionEnvironmentNode.Row) documentStateMap.get(row.documentState)).title);

                if (row.targetAdmissionKind != null)
                    rowTitleList.add(((EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow) targetAdmissionKindMap.get(row.targetAdmissionKind)).shortTitle);
                else if (row instanceof EntrantRowRMC && ((EntrantRowRMC) row).haveSpecialRights)
                    rowTitleList.add("Вне конкурса");
                else if (row.competitionKind == null)
                    rowTitleList.add(null);
                else if ((row.competitionKind.equals("3")) && (row.benefitList != null) && (!row.benefitList.isEmpty())) {
                    List<String> list = new ArrayList<String>();
                    for (String benefit : row.benefitList)
                        list.add(((EnrollmentDistributionEnvironmentNode.BenefitNode.BenefitRow) benefitMap.get(benefit)).shortTitle);
                    rowTitleList.add(new StringBuilder().append(((EnrollmentDistributionEnvironmentNode.CompetitionKindNode.CompetitionKindRow) competitionKindMap.get(row.competitionKind)).shortTitle).append(", ").append(StringUtils.join(list, ", ")).toString());
                }
                else
                    rowTitleList.add(((EnrollmentDistributionEnvironmentNode.CompetitionKindNode.CompetitionKindRow) competitionKindMap.get(row.competitionKind)).shortTitle);

                if (null != row.priorityIds) {
                    List<String> priorityList = new ArrayList<String>(row.priorityIds.size());
                    for (Long id : row.priorityIds)
                        priorityList.add(((EnrollmentDirection) directionMap.get(id.toString())).getEducationOrgUnit().getEducationLevelHighSchool().getShortTitle());
                    rowTitleList.add(StringUtils.join(priorityList, ", "));
                }
                else {
                    rowTitleList.add("");
                }

                rowTitleList.add(row.enrollmentState == null ? null : ((EnrollmentDirection) directionMap.get(row.enrollmentDirection)).getEducationOrgUnit().getEducationLevelHighSchool().getShortTitle());

                rowTitleList.add(row.enrollmentState == null ? null : new StringBuilder().append(((EnrollmentDistributionEnvironmentNode.Row) enrollmentStateMap.get(row.enrollmentState)).title.toLowerCase()).append((row.enrollmentState.equals("3")) && (row.preEnrollId != null) ? new StringBuilder().append(" (").append(((EnrollmentDirection) directionMap.get(row.preEnrollId)).getEducationOrgUnit().getEducationLevelHighSchool().getShortTitle()).append(")").toString() : "").append(row.orderNumber == null ? "" : new StringBuilder().append(", приказ №").append(row.orderNumber).toString()).toString());

                tableRowList.add(rowTitleList.toArray(new String[rowTitleList.size()]));

                rowIndex++;
            }
        }
    }
}
