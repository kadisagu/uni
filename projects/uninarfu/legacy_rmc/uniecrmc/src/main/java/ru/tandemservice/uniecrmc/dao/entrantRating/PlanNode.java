package ru.tandemservice.uniecrmc.dao.entrantRating;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

public class PlanNode {

    @XmlElement(name = "MinisterialPlan")
    public PlanRowNode ministerialPlan;

    @XmlElement(name = "ContractPlan")
    public PlanRowNode contractPlan;

    public PlanNode() {

    }

    public static class PlanRowNode {

        @XmlAttribute
        public String count;

        @XmlAttribute
        public String shortTitle;

        @XmlAttribute
        public String title;

        @XmlElement(name = "Quotas")
        public SRQuotaRow srQuotas;

        @XmlElement(name = "TargetAdmission")
        public TAQuotasNode taQuotas;

        public PlanRowNode() {
        }

        public PlanRowNode(String count, String shortTitle, String title, SRQuotaRow srQuotas, TAQuotasNode taQuotas) {
            this.count = count;
            this.shortTitle = shortTitle;
            this.title = title;
            this.srQuotas = srQuotas;
            this.taQuotas = taQuotas;
        }

        public static class SRQuotaRow {
            @XmlAttribute
            public String count;
            @XmlAttribute
            public String title;

            public SRQuotaRow() {
            }

            public SRQuotaRow(String count, String title) {
                this.count = count;
                this.title = title;
            }

        }

        public static class TAQuotasNode {
            @XmlAttribute
            public String count;
            @XmlAttribute
            public String title;

            @XmlElement(name = "TargetAdmissionKind")
            public List<TAQuotaRow> list = new ArrayList<TAQuotaRow>();

            public TAQuotasNode() {
            }

            public static class TAQuotaRow {
                @XmlAttribute(name = "id")
                public String code;
                @XmlAttribute
                public String count;

                public TAQuotaRow() {
                }

                public TAQuotaRow(String code, String count) {
                    this.code = code;
                    this.count = count;
                }
            }

            public TAQuotasNode(String count, String title) {
                this.count = count;
                this.title = title;
            }

        }
    }

}
