package ru.tandemservice.uniecrmc.component.entrant.EntrantPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniecrmc.entity.entrant.Entrant2IndividualAchievements;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = getModel(component);
        Long entrantId = ((ru.tandemservice.uniec.component.entrant.EntrantPub.Model) component.getModel(component.getName())).getEntrant().getId();
        model.getEntrant().setId(entrantId);

        ((IDAO) Controller.this.getDao()).prepare(model);

        prepareDataSource(component);
    }

    protected void prepareDataSource(IBusinessComponent component) {
        final Model model = getModel(component);
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<Entrant2IndividualAchievements> dataSource = new DynamicListDataSource<>(component, arg0 -> {
            ((IDAO) Controller.this.getDao()).prepareDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("Вид индивидуального достижения", Entrant2IndividualAchievements.individualAchievements().title()));
        dataSource.addColumn(new SimpleColumn("Описание", Entrant2IndividualAchievements.text()));

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "entrantPubExt:onClickEditAchievement").setPermissionKey("editEntrantData"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "entrantPubExt:onClickDeleteAchievement").setPermissionKey("editEntrantData"));

        model.setDataSource(dataSource);
    }

    public void onClickDeleteAchievement(IBusinessComponent component) {
        Long id = component.getListenerParameter();
        getDao().delete(id);
    }

    public void onClickEditAchievement(IBusinessComponent component) {
        Model model = getModel(component);

//        activate(component, new ComponentActivator(
//                ru.tandemservice.uniecrmc.component.entrant.IndividualAchievementsAddEdit.Model.class.getPackage().getName(),
//                new UniMap().add("entrantId", null).add("achievementId",  component.getListenerParameter())
//        ));

        ComponentActivator activator = new ComponentActivator(
                ru.tandemservice.uniecrmc.component.entrant.IndividualAchievementsAddEdit.Model.class.getPackage().getName(),
                new ParametersMap().add("entrantId", null).add("achievementId", component.getListenerParameter())
        );
        UserContext.getInstance().activateDesktop("PersonShellDialog", activator);
//
//		activate(component, new ComponentActivator(
//			ru.ramec.istu.component.entrant.IndividualAchievementsAddEdit.Model.class.getPackage().getName(),
//			new UniMap().add("entrantId", model.getEntrant().getId())
//		));
    }

    public void onClickAddAchievement(IBusinessComponent component) {
        Model model = getModel(component);

        ComponentActivator activator = new ComponentActivator(
                ru.tandemservice.uniecrmc.component.entrant.IndividualAchievementsAddEdit.Model.class.getPackage().getName(),
                new ParametersMap().add("entrantId", model.getEntrant().getId()).add("achievementId", null)
        );
        UserContext.getInstance().activateDesktop("PersonShellDialog", activator);

//		activate(component, new ComponentActivator(
//			ru.tandemservice.uniecrmc.component.entrant.IndividualAchievementsAddEdit.Model.class.getPackage().getName(),
//			new UniMap().add("entrantId", model.getEntrant().getId()).add("achievementId", null)
//		));
//        activateInRoot(component, new ComponentActivator(DocumentEntrantUtils.getDocumentAddComponent(model.getEntrantDocumentType().getIndex()),
//                new UniMap()
//                        .add("entrantId", model.getEntrantId())
//                        .add("entrantDocumentTypeId", model.getEntrantDocumentType().getId())
//        ));
    }

//	public void onClickPrintProtocol(IBusinessComponent component) {
//		Model model = getModel(component);
//
//		RtfDocument document = ((IDAO)Controller.this.getDao()).generateRtf(model);
//		Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(RtfUtil.toByteArray(document), "Протокол.rtf");
//		activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new UniMap().add("id", id).add("zip", Boolean.FALSE).add("extension", "rtf")));
//	}

}
