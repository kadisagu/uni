package ru.tandemservice.uniecrmc.component.report.EntrantEduLevelDistribution.List;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.uniecrmc.entity.entrant.EntrantEduLevelDistributionReport;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

public class DAO extends UniDao<Model>
        implements IDAO
{
    public void prepare(Model model) {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
    }

    public void prepareListDataSource(Model model) {
        EnrollmentCampaign enrollmentCampaign = (EnrollmentCampaign) model.getSettings().get("enrollmentCampaign");

        MQBuilder builder = new MQBuilder(EntrantEduLevelDistributionReport.ENTITY_CLASS, "r");
        builder.add(MQExpression.eq("r", "enrollmentCampaign", enrollmentCampaign));
        new OrderDescriptionRegistry("r").applyOrder(builder, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }

    public void deleteRow(IBusinessComponent component) {
        Long id = (Long) component.getListenerParameter();
        EntrantEduLevelDistributionReport report = (EntrantEduLevelDistributionReport) getNotNull(EntrantEduLevelDistributionReport.class, id);
        delete(report);
        delete(report.getContent());
    }
}

