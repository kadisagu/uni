package ru.tandemservice.uniecrmc.dao.entrantRating;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class CompetitionGroupNode {

    @XmlAttribute(name = "competitionGroupTypeId")
    public String competitionGroupTypeId;

    @XmlAttribute(required = false)
    public String targetAdmissionKindId;

    @XmlAttribute(required = false)
    public Integer plan;

    @XmlElement(name = "Entrants")
    public EntrantsNode entrantsNode;

    public CompetitionGroupNode() {
    }

    public CompetitionGroupNode(CompetitionGroup group) {
        this.competitionGroupTypeId = "" + group.getType().getId();
        //this.targetAdmissionKindId = group.getTargetAdmissionCode() != null && group.getTargetAdmissionCode().equals(CompetitionGroup.DEFAULT_TARGET_ADMISSION_KIND_CODE) ? null : group.getTargetAdmissionCode();
        // vch хрень какая-то!!!
        this.targetAdmissionKindId = group.getTargetAdmissionCode();// != null && group.getTargetAdmissionCode().equals(CompetitionGroup.DEFAULT_TARGET_ADMISSION_KIND_CODE) ? null : group.getTargetAdmissionCode();
        this.plan = group.getPlan();
        this.entrantsNode = new EntrantsNode();
    }

}
