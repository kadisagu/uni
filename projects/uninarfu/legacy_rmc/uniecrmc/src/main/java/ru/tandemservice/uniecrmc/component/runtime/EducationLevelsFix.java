package ru.tandemservice.uniecrmc.component.runtime;

import org.tandemframework.core.runtime.IRuntimeExtension;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Записывает в StructureEducationLevels.GROUPING_LEVEL_CODES ВСЕ(!!!) уровни из базы
 *
 * @author shura
 */
public class EducationLevelsFix implements IRuntimeExtension {

    @Override
    public void destroy() {
    }

    @Override
    public void init(Object object) {
        List<StructureEducationLevels> list = IUniBaseDao.instance.get().getCatalogItemList(StructureEducationLevels.class);
        List<String> codeList = UniUtils.getPropertiesList(list, StructureEducationLevels.code());

        Set<String> codeSet = Collections.unmodifiableSet(new HashSet<>(codeList));

        //StructureEducationLevels.GROUPING_LEVEL_CODES = codeSet;
        try {
            Field field = StructureEducationLevels.class.getField("GROUPING_LEVEL_CODES");
            if (field == null)
                return;

            int modifiers = field.getModifiers();
            modifiers = modifiers & ~Modifier.FINAL;

            Field modifierField = field.getClass().getDeclaredField("modifiers");
            modifierField.setAccessible(true);
            modifierField.setInt(field, modifiers);

            field.set(null, codeSet);
        }
        catch (Throwable ex) {
            ex.printStackTrace();
        }

    }

}
