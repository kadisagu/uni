package ru.tandemservice.uniecrmc.dao.entrantRating;

import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

public class CompetitionGroupsNode {

    @XmlElement(name = "CompetitionGroup")
    public List<CompetitionGroupNode> groupList = new ArrayList<>();

}
