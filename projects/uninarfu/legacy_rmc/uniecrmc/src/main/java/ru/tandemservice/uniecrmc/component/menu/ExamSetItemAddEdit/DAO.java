package ru.tandemservice.uniecrmc.component.menu.ExamSetItemAddEdit;

import org.hibernate.Session;
import ru.tandemservice.uniecrmc.entity.entrant.EntranceDisciplineExt;
import ru.tandemservice.uniecrmc.entity.entrant.EntrantRecommendedSettings;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.component.entrant.EntranceDisciplineAddEdit.EntranceDisciplineAddEditModel;
import ru.tandemservice.uniec.component.menu.ExamSetItemAddEdit.ExamSetItemAddEditModel;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineKind;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline2SetDisciplineRelation;
import ru.tandemservice.uniec.entity.examset.ExamSet;
import ru.tandemservice.uniec.entity.examset.ExamSetItem;
import ru.tandemservice.uniec.entity.settings.SetDiscipline;

import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

public class DAO extends ru.tandemservice.uniec.component.menu.ExamSetItemAddEdit.DAO {

    @Override
    public void update(EntranceDisciplineAddEditModel childModel) {
        ExamSetItemAddEditModel model = (ExamSetItemAddEditModel) childModel;

        boolean budget = model.getEntranceDiscipline().isBudget();
        boolean contract = model.getEntranceDiscipline().isContract();
        EntranceDisciplineKind kind = model.getEntranceDiscipline().getKind();
        EntranceDisciplineType type = model.getEntranceDiscipline().getType();
        SetDiscipline discipline = model.getEntranceDiscipline().getDiscipline();

        ExamSet examSet = model.getExamSet();
        List<ExamSetItem> examSetItems = examSet.getSetItemList();

        ExamSetItem examSetItem = new ExamSetItem(Long.valueOf(0L), budget, contract, kind, type, model.getEntranceDiscipline().getDiscipline(), model.getChoiceDisciplinesList());
        ListIterator<ExamSetItem> iterator;
        if (model.getExamSetItemId() == null) {
            examSetItems.add(examSetItem);
        }
        else {
            for (iterator = examSetItems.listIterator(); iterator.hasNext(); ) {
                ExamSetItem item = (ExamSetItem) iterator.next();
                if (item.getExamSetItemId().equals(model.getExamSetItemId())) {
                    iterator.remove();
                    iterator.add(examSetItem);
                }
            }
        }
        String newExamSetId = new ExamSet(Long.valueOf(0L), examSet.getEnrollmentCampaign(), examSet.getStudentCategory(), examSetItems).getExamSetId();
        model.setExamSetId(newExamSetId);

        if (model.getEntranceDisciplineList() != null) {
            for (EntranceDiscipline entranceDiscipline : model.getEntranceDisciplineList()) {
                entranceDiscipline.setBudget(budget);
                entranceDiscipline.setContract(contract);
                entranceDiscipline.setKind(kind);
                entranceDiscipline.setType(type);
                entranceDiscipline.setDiscipline(discipline);

                model.setEntranceDiscipline(entranceDiscipline);
                saveOrUpdate(model);
            }
        }
        else {
            for (EnrollmentDirection enrollmentDirection : model.getEntranceEducationOrgUnitList()) {
                EntranceDiscipline entranceDiscipline = new EntranceDiscipline();
                entranceDiscipline.setBudget(budget);
                entranceDiscipline.setContract(contract);
                entranceDiscipline.setKind(kind);
                entranceDiscipline.setType(type);
                entranceDiscipline.setStudentCategory(examSet.getStudentCategory());
                entranceDiscipline.setDiscipline(discipline);
                entranceDiscipline.setEnrollmentDirection(enrollmentDirection);

                model.setEntranceDiscipline(entranceDiscipline);
                saveOrUpdate(model);
            }
        }
    }

    public void saveOrUpdate(EntranceDisciplineAddEditModel model) {
        Session session = getSession();

        EntranceDiscipline entranceDiscipline = model.getEntranceDiscipline();
        entranceDiscipline.setStudentCategory(getStudentCategory(entranceDiscipline));
        session.saveOrUpdate(entranceDiscipline);

        List<EntranceDiscipline2SetDisciplineRelation> oldList = getList(EntranceDiscipline2SetDisciplineRelation.class, "entranceDiscipline", entranceDiscipline, new String[0]);
        List<SetDiscipline> newList = model.getChoiceDisciplinesList();

        Set<SetDiscipline> used = new HashSet<SetDiscipline>();
        for (EntranceDiscipline2SetDisciplineRelation rel : oldList) {
            used.add(rel.getSetDiscipline());
            if (!newList.contains(rel.getSetDiscipline()))
                session.delete(rel);
        }

        for (SetDiscipline discipline : newList) {
            if (!used.contains(discipline)) {
                EntranceDiscipline2SetDisciplineRelation rel = new EntranceDiscipline2SetDisciplineRelation();
                rel.setEntranceDiscipline(entranceDiscipline);
                rel.setSetDiscipline(discipline);
                session.save(rel);
            }
        }

        EntrantRecommendedSettings settings = get(EntrantRecommendedSettings.class, EntrantRecommendedSettings.enrollmentCampaign(), model.getEnrollmentCampaign());

        if (settings != null && settings.isUsePriorityEntranceDiscipline()) {
            EntranceDisciplineExt disciplineExt = new EntranceDisciplineExt();
            disciplineExt.setEntranceDiscipline(model.getEntranceDiscipline());
            disciplineExt.setPriority(getCount(EntranceDiscipline.class, EntranceDiscipline.L_ENROLLMENT_DIRECTION, model.getEntranceDiscipline().getEnrollmentDirection()) + 1);
            getSession().saveOrUpdate(disciplineExt);
        }

    }

    private StudentCategory getStudentCategory(EntranceDiscipline entranceDiscipline) {
        return !entranceDiscipline.getEnrollmentDirection().getEnrollmentCampaign().isExamSetDiff() ? (StudentCategory) getCatalogItem(StudentCategory.class, "1") : entranceDiscipline.getStudentCategory();
    }

}

