package ru.tandemservice.uniecrmc.component.settings.EnrollmentCampaignCopy;

import ru.tandemservice.uniecrmc.entity.entrant.EntrantRecommendedSettings;
import ru.tandemservice.uniec.component.settings.EnrollmentCampaignCopy.Model;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

public class DAO extends ru.tandemservice.uniec.component.settings.EnrollmentCampaignCopy.DAO {

    @Override
    public void update(Model model) {
        super.update(model);

        EntrantRecommendedSettings settingsToCopy = get(EntrantRecommendedSettings.class, EntrantRecommendedSettings.enrollmentCampaign(), model.getCampaignToCopy());

        EnrollmentCampaign enrollmentCampaign = model.getPeriod().getEnrollmentCampaign();

        EntrantRecommendedSettings settings = new EntrantRecommendedSettings();
        settings.setEnrollmentCampaign(enrollmentCampaign);
        settings.setUsePriorityEntranceDiscipline(settingsToCopy.isUsePriorityEntranceDiscipline());
        settings.setUseGroup(settingsToCopy.isUseGroup());

        getSession().save(settings);
    }
}
