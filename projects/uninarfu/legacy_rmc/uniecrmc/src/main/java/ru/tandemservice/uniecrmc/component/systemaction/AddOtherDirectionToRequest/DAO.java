package ru.tandemservice.uniecrmc.component.systemaction.AddOtherDirectionToRequest;

import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.dao.IEntrantDAO;
import ru.tandemservice.uniec.entity.catalog.codes.EntrantStateCodes;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DAO extends UniDao<Model> implements IDAO {
    @Override
    public void prepare(Model model)
    {
        // универсальная заполнячилка списка приемок
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        Object numberObj = model.getSettings().get("regNumber");
        if (numberObj == null)
            model.getSettings().set("regNumber", 0);
    }

    @Override
    public void update(Model model)
    {
    }


    @Override
    @Transactional
    public Long processEntrantRequest
            (
                    Long entrantRequestId
                    , Map<CompetitionGroup, List<EnrollmentDirection>> mapCG
            )
    {

        boolean isStrong = IsStrongRequest(entrantRequestId);
        if (!isStrong) {
            System.out.println("Не уникальная комбинация в заявлении " + entrantRequestId);
            return null;
        }


        EntrantRequest request = get(EntrantRequest.class, entrantRequestId);

        Map<Long, WrapAddDirection> mapAdd = new HashMap<>();

        // по заявлению нужно получить направления
        // для приема в правильном порядке
        List<RequestedEnrollmentDirection> rdList = getRequestedEnrollmentDirectionList(request);
        for (RequestedEnrollmentDirection rd : rdList) {
            CompetitionGroup competitionGroup = rd.getEnrollmentDirection().getCompetitionGroup();
            if (competitionGroup == null)
                continue; // не вероятно по определению

            // получим направления подготовки КГ
            if (!mapCG.containsKey(competitionGroup))
                continue;

            // в первой позиции должно быть rd в статусе активный или к зачислению
            // если в первой позиции иное, то такой абитуриент не обрабатывается
            if (rd.getState().getCode().equals(EntrantStateCodes.ACTIVE)
                    || rd.getState().getCode().equals(EntrantStateCodes.TO_BE_ENROLED))
            {
                // это правильный статус
            }
            else
                continue;


            List<EnrollmentDirection> edList = mapCG.get(competitionGroup);

            // проверяем, чего не хватает, формируем список добавления
            // считаем, что вступительные испытания, источники финансирования и так далее
            // нужно брать из НП с наивысшим приоритетом
            WrapAddDirection wrap = new WrapAddDirection(competitionGroup);
            Long key = competitionGroup.getId();
            if (mapAdd.containsKey(key))
                wrap = mapAdd.get(key);
            else {
                mapAdd.put(key, wrap);
                // шаблон добавления (по наивысшему приоритету)
                wrap.setRequestedEnrollmentDirection(rd);
            }
            List<EnrollmentDirection> lstToAdd = getEnrollmentDirectionToAdd(edList, rdList);
            for (EnrollmentDirection edAdd : lstToAdd) {
                wrap.AddEnrollmentDirection(edAdd);
            }
        }

        boolean hasAdd = false;
        // карта с добавлением готова
        for (Long key : mapAdd.keySet()) {
            WrapAddDirection wrap = mapAdd.get(key);
            hasAdd = AddEducationDirection(request, wrap);
        }


        if (hasAdd) {
            /*
			// пересчитываем абитуриентов
			IEntrantDAO idao = (IEntrantDAO) ApplicationRuntime.getBean(IEntrantDAO.class.getName());
			List<Long> entrantIds = new ArrayList<>();
			
			entrantIds.add(request.getEntrant().getId());
			idao.updateEntrantData(entrantIds );
			*/

            return request.getEntrant().getId();
        }
        else {
            System.out.println("Пропуск пересчета");
            return null;
        }
    }

    private boolean IsStrongRequest(Long entrantRequestId)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "red")
                .where(eq(property(RequestedEnrollmentDirection.entrantRequest().id().fromAlias("red")), value(entrantRequestId)))
                .where(DQLExpressions.isNotNull(property(RequestedEnrollmentDirection.enrollmentDirection().competitionGroup().fromAlias("red"))))

                        // признак целевика
                .column(RequestedEnrollmentDirection.targetAdmission().fromAlias("red").s())
                .column(RequestedEnrollmentDirection.compensationType().id().fromAlias("red").s())
                .column(RequestedEnrollmentDirection.competitionKind().id().fromAlias("red").s())
                .predicate(DQLPredicateType.distinct);
        List<RequestedEnrollmentDirection> lst = dql.createStatement(getSession()).list();

        if (lst != null && lst.size() == 1)
            return true;
        else
            return false;
    }

    @Transactional
    public void recalculatePart(List<Long> part)
    {
        IEntrantDAO idao = (IEntrantDAO) ApplicationRuntime.getBean(IEntrantDAO.class.getName());
        idao.updateEntrantData(part);
    }

    private boolean AddEducationDirection(
            EntrantRequest request,
            WrapAddDirection wrap)
    {
        boolean retVal = false;

        List<EnrollmentDirection> lst = wrap.getLstToAdd();
        int i = getMaxPriority(request, getSession());

        for (EnrollmentDirection ed : lst) {
            i++;


            RequestedEnrollmentDirection rdOrigin = wrap.getRequestedEnrollmentDirection();


            RequestedEnrollmentDirection rd = new RequestedEnrollmentDirection();

            //rd.setId((long) (-1*i));
            rd.setId(null);

            rd.setEntrantRequest(request);
            rd.setEnrollmentDirection(ed);

            // оригиналы не поданы (по умолчанию)
            rd.setOriginalDocumentHandedIn(false);

            rd.setCompensationType(wrap.getRequestedEnrollmentDirection().getCompensationType());
            rd.setCompetitionKind(wrap.getRequestedEnrollmentDirection().getCompetitionKind());
            rd.setStudentCategory(wrap.getRequestedEnrollmentDirection().getStudentCategory());
            rd.setTargetAdmission(wrap.getRequestedEnrollmentDirection().isTargetAdmission());
            rd.setTargetAdmissionKind(wrap.getRequestedEnrollmentDirection().getTargetAdmissionKind());

            rd.setProfileWorkExperienceYears(wrap.getRequestedEnrollmentDirection().getProfileWorkExperienceYears());
            rd.setProfileWorkExperienceMonths(wrap.getRequestedEnrollmentDirection().getProfileWorkExperienceMonths());
            rd.setProfileWorkExperienceDays(wrap.getRequestedEnrollmentDirection().getProfileWorkExperienceDays());
            rd.setRegDate(wrap.getRequestedEnrollmentDirection().getRegDate());

            rd.setPriority(i);
            rd.setState(wrap.getRequestedEnrollmentDirection().getState());

            // знать-бы нафига это
            int maxReg = getMaxRegNumber(ed, getSession());
            rd.setNumber(maxReg + 1);

            // если целевой прием не указан, то и не должно быть типа целевого приема
            if (!rd.isTargetAdmission())
                rd.setTargetAdmissionKind(null);


            // приоритет в разрезе вида затрат
            int priorityPerEntrant = _getNextPriorityPerEntrant(rd);
            rd.setPriorityPerEntrant(priorityPerEntrant);

            rd.setComment("Добавлено системным действием");
            saveOrUpdate(rd);
            //getSession().flush();


            // копируем расширение
            // ищем расширенные настройки
            RequestedEnrollmentDirectionExt edExt = get(RequestedEnrollmentDirectionExt.class, RequestedEnrollmentDirectionExt.L_REQUESTED_ENROLLMENT_DIRECTION, rdOrigin.getId());

            if (edExt != null && edExt.isHaveSpecialRights()) {
                RequestedEnrollmentDirectionExt extNew = new RequestedEnrollmentDirectionExt();
                extNew.setRequestedEnrollmentDirection(rd);
                extNew.setHaveSpecialRights(true);
                saveOrUpdate(extNew);
            }


            cloneChosenEntranceDiscipline(wrap.getRequestedEnrollmentDirection(), rd);

            retVal = true;
        }

        return retVal;
    }

    private int _getNextPriorityPerEntrant(RequestedEnrollmentDirection item)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(RequestedEnrollmentDirection.class, "ent")
                .column(DQLFunctions.max(DQLExpressions.property(RequestedEnrollmentDirection.priorityPerEntrant().fromAlias("ent"))))
                .where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("ent")), DQLExpressions.value(item.getEntrantRequest().getEntrant().getId())))
                .where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.compensationType().id().fromAlias("ent")), DQLExpressions.value(item.getCompensationType().getId())));

        Integer retVal = dql.createStatement(getSession()).uniqueResult();
        if (retVal == null)
            retVal = 0;

        retVal++;

        return retVal;
    }


    private void cloneChosenEntranceDiscipline
            (
                    RequestedEnrollmentDirection fromRD,
                    RequestedEnrollmentDirection toRD)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(ChosenEntranceDiscipline.class, "ced")
                .where(eq(property(ChosenEntranceDiscipline.chosenEnrollmentDirection().fromAlias("ced")), value(fromRD)))
                .column("ced");
        List<ChosenEntranceDiscipline> lst = dql.createStatement(getSession()).list();

        for (ChosenEntranceDiscipline ced : lst) {
            ChosenEntranceDiscipline cedNew = new ChosenEntranceDiscipline();

            cedNew.setChosenEnrollmentDirection(toRD);
            cedNew.setEnrollmentCampaignDiscipline(ced.getEnrollmentCampaignDiscipline());

            cedNew.setFinalMark(ced.getFinalMark());
            cedNew.setFinalMarkSource(ced.getFinalMarkSource());

            saveOrUpdate(cedNew);
        }

        // getSession().flush();
    }

    public static int getMaxPriority(EntrantRequest entrantRequest, Session session)
    {
        Number priority = (Number) session.createCriteria(RequestedEnrollmentDirection.class)
                .add(Restrictions.eq(RequestedEnrollmentDirection.L_ENTRANT_REQUEST, entrantRequest))
                .setProjection(Projections.max(RequestedEnrollmentDirection.P_PRIORITY))
                .uniqueResult();
        return priority == null ? 0 : priority.intValue();
    }

    public static int getMaxRegNumber(EnrollmentDirection enrollmentDirection, Session session)
    {
        Number regNumber = (Number) session.createCriteria(RequestedEnrollmentDirection.class)
                .add(Restrictions.eq(RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, enrollmentDirection))
                .setProjection(Projections.max(RequestedEnrollmentDirection.P_NUMBER))
                .uniqueResult();
        return regNumber == null ? 0 : regNumber.intValue();
    }

    private List<EnrollmentDirection> getEnrollmentDirectionToAdd
            (
                    List<EnrollmentDirection> edcompetitionGroupList,
                    List<RequestedEnrollmentDirection> rdList
            )
    {
        List<EnrollmentDirection> retVal = new ArrayList<>();

        for (EnrollmentDirection ed : edcompetitionGroupList) {
            boolean has = false;
            for (RequestedEnrollmentDirection rd : rdList) {
                EnrollmentDirection edHas = rd.getEnrollmentDirection();
                if (edHas.equals(ed))
                    has = true;
            }

            if (!has && !retVal.contains(ed))
                retVal.add(ed);
        }

        return retVal;
    }

    /**
     * выбранные направления по заявлению
     * (не зависимо от статусов, но имеющие конкурсные группы)
     *
     * @param request
     *
     * @return
     */
    private List<RequestedEnrollmentDirection> getRequestedEnrollmentDirectionList
    (
            EntrantRequest request
    )
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "red")
                .where(eq(property(RequestedEnrollmentDirection.entrantRequest().fromAlias("red")), value(request)))


                        // исключая целевой прием
                        // исключая целевой прием <vch> Саша Базжина попросила включить и целевикиов (30/07/2014)
                        //.where(DQLExpressions.ne(property(RequestedEnrollmentDirection.targetAdmission().fromAlias("red")), value(Boolean.TRUE)))

                        // правильные статусы
                        // .where(DQLExpressions.in(property(RequestedEnrollmentDirection.state().code().fromAlias("red")), getValidCodes()))

                .where(DQLExpressions.isNotNull(property(RequestedEnrollmentDirection.enrollmentDirection().competitionGroup().fromAlias("red"))))
                .column("red")
                        // сортировка по приоритету
                .order(property(RequestedEnrollmentDirection.priority().fromAlias("red")), OrderDirection.asc)
                .predicate(DQLPredicateType.distinct);


        List<RequestedEnrollmentDirection> lst = dql.createStatement(getSession()).list();
        return lst;

    }

    private List<String> getExcludeCodes()
    {
        // только правильные статусы
        List<String> validCode = new ArrayList<>();
        validCode.add(EntrantStateCodes.ENROLED);
        validCode.add(EntrantStateCodes.PRELIMENARY_ENROLLED);
        return validCode;
    }

    private List<String> getValidCodes()
    {
        // только правильные статусы
        List<String> validCode = new ArrayList<>();
        validCode.add(EntrantStateCodes.ACTIVE);
        validCode.add(EntrantStateCodes.TO_BE_ENROLED);


        return validCode;

    }

    @Override
    public List<Long> getEntrantRequestIds
            (
                    EnrollmentCampaign enrollmentCampaign
                    , int regNumber
            )
    {
        // исключить зачисленных и пред. зачисленных
        DQLSelectBuilder dqlNotIn = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "redNI")
                .where(eq(property(RequestedEnrollmentDirection.enrollmentDirection().enrollmentCampaign().fromAlias("redNI")), value(enrollmentCampaign)))
                        // пред зачисленные и зачисленные
                .where(DQLExpressions.in(property(RequestedEnrollmentDirection.state().code().fromAlias("redNI")), getExcludeCodes()))
                .column(RequestedEnrollmentDirection.entrantRequest().id().fromAlias("redNI").s())
                .predicate(DQLPredicateType.distinct);

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "red")
                .where(eq(property(RequestedEnrollmentDirection.enrollmentDirection().enrollmentCampaign().fromAlias("red")), value(enrollmentCampaign)))

                        // правильные статусы
                .where(DQLExpressions.in(property(RequestedEnrollmentDirection.state().code().fromAlias("red")), getValidCodes()))

                        // исключая зачисленных и пред зачисленных
                .where(DQLExpressions.notIn(property(RequestedEnrollmentDirection.entrantRequest().id().fromAlias("red")), dqlNotIn.getQuery()))

                        // исключая целевой прием <vch> Саша Базжина попросила включить и целевикиов (30/07/2014)
                        // .where(DQLExpressions.ne(property(RequestedEnrollmentDirection.targetAdmission().fromAlias("red")), value(Boolean.TRUE)))
                .where(DQLExpressions.isNotNull(property(RequestedEnrollmentDirection.enrollmentDirection().competitionGroup().fromAlias("red"))))
                .column(RequestedEnrollmentDirection.entrantRequest().id().fromAlias("red").s())
                .predicate(DQLPredicateType.distinct);

        if (regNumber > 0)
            dql.where(eq(property(RequestedEnrollmentDirection.entrantRequest().regNumber().fromAlias("red")), value(regNumber)));


        List<Long> lst = dql.createStatement(getSession()).list();

        return lst;
    }


    @Override
    public Map<CompetitionGroup, List<EnrollmentDirection>> getMapCompetitionGroup
            (
                    EnrollmentCampaign enrollmentCampaign
            )
    {

        Map<CompetitionGroup, List<EnrollmentDirection>> retVal = new HashMap<CompetitionGroup, List<EnrollmentDirection>>();

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "ed")
                .where(eq(property(EnrollmentDirection.enrollmentCampaign().fromAlias("ed")), value(enrollmentCampaign)))
                .where(DQLExpressions.isNotNull(property(EnrollmentDirection.competitionGroup().fromAlias("ed"))))
                .column("ed")
                .order(property(EnrollmentDirection.ministerialPlan().fromAlias("ed")), OrderDirection.desc)
                .order(property(EnrollmentDirection.contractPlan().fromAlias("ed")), OrderDirection.desc)
                .order(property(EnrollmentDirection.title().fromAlias("ed")), OrderDirection.asc)

                .predicate(DQLPredicateType.distinct);

        List<EnrollmentDirection> lst = dql.createStatement(getSession()).list();

        for (EnrollmentDirection ed : lst) {
            CompetitionGroup cg = ed.getCompetitionGroup();
            List<EnrollmentDirection> lstED;

            if (retVal.containsKey(cg))
                lstED = retVal.get(cg);
            else {
                lstED = new ArrayList<>();
                retVal.put(cg, lstED);
            }
            lstED.add(ed);
        }

        return retVal;
    }
}
