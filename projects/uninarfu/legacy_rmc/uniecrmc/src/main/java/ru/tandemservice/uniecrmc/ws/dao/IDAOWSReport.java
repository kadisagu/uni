package ru.tandemservice.uniecrmc.ws.dao;

public interface IDAOWSReport {

    /**
     * @return Список заявлений абитуриентов
     * по текущую дату
     */
    String getEntrantList(String enrolmentCompaignId);


    /**
     * @return Статистика по поданным заявлениям
     */
    String getEntrantRequestStatistic(String enrolmentCompaignId);

}
