package ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgCortegeDTO;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class EcgContegeComparatorRMC implements Comparator<IEcgCortegeDTO> {

    @Override
    public int compare(IEcgCortegeDTO o1, IEcgCortegeDTO o2) {

        IEcgCortegeRMC obj1 = (IEcgCortegeRMC) o1;
        IEcgCortegeRMC obj2 = (IEcgCortegeRMC) o2;

        if ((o1.isTargetAdmission()) && (o2.isTargetAdmission())) {
            int result = TargetAdmissionKind.ALL_LEVEL_PRIORITY_COMPARATOR.compare(o1.getTargetAdmissionKind(), o2.getTargetAdmissionKind());
            if (result != 0) {
                return result;
            }
        }
        else {
            int result = -Boolean.valueOf(obj1.isTargetAdmission()).compareTo(Boolean.valueOf(obj2.isTargetAdmission()));
            if (result != 0)
                return result;
        }

        int result = -Boolean.valueOf(obj1.getCompetitionKind().getCode().equals(UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES))
                .compareTo(Boolean.valueOf(obj2.getCompetitionKind().getCode().equals(UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES)));
        if (result != 0)
            return result;

        result = -Boolean.valueOf(obj1.isHaveSpecialRights()).compareTo(Boolean.valueOf(obj2.isHaveSpecialRights()));
        if (result != 0)
            return result;

        result = -UniBaseUtils.compare(obj1.getFinalMark(), obj2.getFinalMark(), true);
        if (result != 0)
            return result;

        Boolean entrRecom1 = obj1.getRecommendations() != null && !obj1.getRecommendations().isEmpty();
        Boolean entrRecom2 = obj2.getRecommendations() != null && !obj2.getRecommendations().isEmpty();
        List<WrapChoseEntranseDiscipline> marks1 = obj1.getDisciplines() == null ? new ArrayList<WrapChoseEntranseDiscipline>() : obj1.getDisciplines();
        List<WrapChoseEntranseDiscipline> marks2 = obj2.getDisciplines() == null ? new ArrayList<WrapChoseEntranseDiscipline>() : obj2.getDisciplines();

        int size = marks1.size() > marks2.size() ? marks2.size() : marks1.size();

        for (int i = 0; i < size; i++) {
            result = -UniBaseUtils.compare(marks1.get(i).getFinalMark(), marks2.get(i).getFinalMark(), true);
            if (result != 0)
                return result;
        }

        result = -entrRecom1.compareTo(entrRecom2);
        if (result != 0)
            return result;

        result = -UniBaseUtils.compare(obj1.getCertificateAverageMark(), obj2.getCertificateAverageMark(), true);
        if (result != 0)
            return result;

        return obj1.getFio().compareToIgnoreCase(obj2.getFio());
    }

}
