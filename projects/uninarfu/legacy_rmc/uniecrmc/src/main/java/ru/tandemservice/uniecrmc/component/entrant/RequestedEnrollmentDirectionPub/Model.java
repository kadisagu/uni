package ru.tandemservice.uniecrmc.component.entrant.RequestedEnrollmentDirectionPub;

import ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt;
import ru.tandemservice.uniecrmc.util.UniecrmcUtil;

public class Model extends ru.tandemservice.uniec.component.entrant.RequestedEnrollmentDirectionPub.Model {

    private RequestedEnrollmentDirectionExt directionExt;

    public boolean isUseGroup() {
        return UniecrmcUtil.isUseGroup(this.getEntrantRequest().getEntrant().getEnrollmentCampaign());
    }

    public RequestedEnrollmentDirectionExt getDirectionExt() {
        return directionExt;
    }

    public void setDirectionExt(RequestedEnrollmentDirectionExt directionExt) {
        this.directionExt = directionExt;
    }

}
