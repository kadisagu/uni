package ru.tandemservice.uniecrmc.component.report.EntrantEduLevelDistribution.List;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.uniecrmc.entity.entrant.EntrantEduLevelDistributionReport;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

public class Controller extends AbstractBusinessController<IDAO, Model> {
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = getModel(component);
        model.setSettings(component.getSettings());
        getDao().prepare(model);
        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component) {
        Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<EntrantEduLevelDistributionReport> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new IndicatorColumn("Иконка", null).defaultIndicator(new IndicatorColumn.Item("report", "Отчет")).setOrderable(false).setClickable(false).setRequired(true));
        dataSource.addColumn(new SimpleColumn("Дата формирования", "formingDate", DateFormatter.DATE_FORMATTER_WITH_TIME));
        dataSource.addColumn(new SimpleColumn("Формирующее подразделение", EntrantEduLevelDistributionReport.formativeOrgUnitTitle().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Направление подготовки", EntrantEduLevelDistributionReport.educationLevelsHighSchoolTitle().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Форма обучения", EntrantEduLevelDistributionReport.developFormTitle().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Условия освоения", EntrantEduLevelDistributionReport.developConditionTitle().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new IndicatorColumn("Печать", null, "onClickPrintReport").defaultIndicator(new IndicatorColumn.Item("printer", "Печать")).setImageHeader(false).setDisableSecondSubmit(false).setPermissionKey("printUniecStorableReport").setOrderable(false));
        dataSource.addColumn(new ActionColumn("Удалить", "delete", "onClickDeleteReport", "Удалить отчет от «{0}»?", new Object[]{"formingDate"}).setPermissionKey("deleteUniecStorableReport"));
        dataSource.setOrder("formingDate", OrderDirection.desc);
        model.setDataSource(dataSource);
    }

    public void onClickAddReport(IBusinessComponent component) {
        component.createRegion(new ComponentActivator("ru.tandemservice.uniecrmc.component.report.EntrantEduLevelDistribution.Add"));
    }

    public void onClickSearch(IBusinessComponent component) {
        component.saveSettings();
        getModel(component).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component) {
        EntrantFilterUtil.resetEnrollmentCampaignFilter(getModel(component));
        onClickSearch(component);
    }

    public void onClickPrintReport(IBusinessComponent component) throws Exception {
        activateInRoot(component, new ComponentActivator(IUniComponents.DOWNLOAD_STORABLE_REPORT, new UniMap().add("reportId", component.getListenerParameter()).add("extension", "xls")));
    }

    public void onClickDeleteReport(IBusinessComponent component) {
        getDao().deleteRow(component);
    }
}

