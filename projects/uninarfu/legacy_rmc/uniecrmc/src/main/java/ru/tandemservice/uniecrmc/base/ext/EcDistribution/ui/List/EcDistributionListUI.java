package ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.List;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.EcDistributionExtManager;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.base.bo.EcDistribution.EcDistributionManager;
import ru.tandemservice.uniec.base.bo.EcDistribution.ui.List.EcDistributionList;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.EcgDistributionDTO;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgDistribution;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistributionState;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class EcDistributionListUI extends ru.tandemservice.uniec.base.bo.EcDistribution.ui.List.EcDistributionListUI {

    private Map<Long, String> srQuotaMap = new Hashtable<Long, String>();

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        if (!isUsePriority()) {
            super.onBeforeDataSourceFetch(dataSource);
            return;
        }
        super.onBeforeDataSourceFetch(dataSource);
        if ((EcDistributionList.DISTRIBUTION_CG_DS.equals(dataSource.getName())) || (EcDistributionList.DISTRIBUTION_ED_DS.equals(dataSource.getName()))) {
            DataWrapper waveWrapper = (DataWrapper) this._uiSettings.get("wave");
            List<EcgDistributionDTO> list = EcDistributionManager.instance().dao().getDistributionDTOList(getEnrollmentCampaign(),
                                                                                                          (CompensationType) this._uiSettings.get("compensationType"),
                                                                                                          EcDistributionManager.DISTRIBUTION_CATEGORY_SECOND_HIGH_ADMISSION.equals(this._uiSettings.get("distributionCategory")),
                                                                                                          waveWrapper == null ? null : Integer.valueOf(waveWrapper.getId().intValue()),
                                                                                                          (EcgDistributionState) this._uiSettings.get("state"),
                                                                                                          (CompetitionGroup) this._uiSettings.get("competitionGroup"),
                                                                                                          getSelectedQualificationList(),
                                                                                                          getSelectedFormativeOrgUnitList(),
                                                                                                          getSelectedTerritorialOrgUnitList(),
                                                                                                          getSelectedEducationLevelHighSchoolList(),
                                                                                                          getSelectedDevelopFormList(),
                                                                                                          getSelectedDevelopConditionList(),
                                                                                                          getSelectedDevelopTechList(),
                                                                                                          getSelectedDevelopPeriodList());

            srQuotaMap = new Hashtable<Long, String>();

            Map<Long, String> sRQuotaMap = new Hashtable<Long, String>();

            for (EcgDistributionDTO item : list) {
                if ((item.getPersistentId() != null) && (!item.isDetailDistribution())) {
                    sRQuotaMap.clear();
                    EcDistributionExtManager.daoRMC().getQuotaHTMLDescription((IEcgDistribution) DataAccessServices.dao().getNotNull(item.getPersistentId()), sRQuotaMap);
                    srQuotaMap.put(item.getPersistentId(), sRQuotaMap.get(Long.valueOf(0L)));
                }
            }
        }
    }

    public boolean isUsePriority() {
        return EcDistributionExtManager.daoRMC().isUsePriority(this.getEnrollmentCampaign());
    }

    public String getSrQuota() {
        String dataSourceName = getEnrollmentCampaign().isUseCompetitionGroup() ? EcDistributionList.DISTRIBUTION_CG_DS : EcDistributionList.DISTRIBUTION_ED_DS;
        EcgDistributionDTO distributionDTO = (EcgDistributionDTO) getConfig().getDataSource(dataSourceName).getCurrent();
        if ((distributionDTO.getPersistentId() == null) || (distributionDTO.isDetailDistribution())) return null;
        return (String) this.srQuotaMap.get(distributionDTO.getPersistentId());
    }
}
