package ru.tandemservice.uniecrmc.component.entrant.RequestedEnrollmentDirectionAddEdit;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.mutable.MutableInt;
import org.hibernate.Criteria;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.EcDistributionUtil;
import ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt;
import ru.tandemservice.uniecrmc.util.UniecrmcUtil;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniec.base.bo.EcEntrant.EcEntrantManager;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.ui.EnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantRequestAddEditUtil;
import ru.tandemservice.uniec.util.EntrantUtil;

import java.util.*;


public class DAO extends ru.tandemservice.uniec.component.entrant.RequestedEnrollmentDirectionAddEdit.DAO {

    @Override
    public void prepare(final ru.tandemservice.uniec.component.entrant.RequestedEnrollmentDirectionAddEdit.Model model) {
        super.prepare(model);
        final Model myModel = (Model) model;

        if (!myModel.isUseGroup())
            return;

        if (model.isEditForm()) {
            RequestedEnrollmentDirectionExt directionExt = UniecrmcUtil.getRequestedEnrollmentDirectionExt(model.getRequestedEnrollmentDirection());
            if (directionExt != null) {
                myModel.setCourse(directionExt.getCourse());
                myModel.setTerm(directionExt.getTerm());
                myModel.setGroup(directionExt.getGroup());
            }
        }
        myModel.setCourseModel(new LazySimpleSelectModel<>(DevelopGridDAO.getCourseList()));
        myModel.setTermModel(new DQLFullCheckSelectModel(Term.title().s()) {

            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(DevelopGridTerm.class, "gr");

                FilterUtils.applySelectFilter(subBuilder, "gr", DevelopGridTerm.developGrid().developPeriod(), model.getDevelopPeriod());
                FilterUtils.applySelectFilter(subBuilder, "gr", DevelopGridTerm.course(), myModel.getCourse());

                subBuilder.column(DevelopGridTerm.term().id().fromAlias("gr").s());
                subBuilder.distinct();

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Term.class, alias)
                        .where(DQLExpressions.in(DQLExpressions.property(Term.id().fromAlias(alias)), subBuilder.buildQuery()))
                        .order(Term.code().fromAlias(alias).s());
                return builder;
            }
        });
        myModel.setGroupModel(new DQLFullCheckSelectModel(Group.title().s()) {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Group.class, alias);
                FilterUtils.applySelectFilter(builder, alias, Group.educationOrgUnit().formativeOrgUnit(), model.getFormativeOrgUnit());
                FilterUtils.applySelectFilter(builder, alias, Group.educationOrgUnit().territorialOrgUnit(), model.getTerritorialOrgUnit());
                FilterUtils.applySelectFilter(builder, alias, Group.educationOrgUnit().educationLevelHighSchool(), model.getEducationLevelsHighSchool());
                FilterUtils.applySelectFilter(builder, alias, Group.educationOrgUnit().developForm(), model.getDevelopForm());
                FilterUtils.applySelectFilter(builder, alias, Group.educationOrgUnit().developCondition(), model.getDevelopCondition());
                FilterUtils.applySelectFilter(builder, alias, Group.educationOrgUnit().developPeriod(), model.getDevelopPeriod());
                FilterUtils.applySelectFilter(builder, alias, Group.educationOrgUnit().developTech(), model.getDevelopTech());

                FilterUtils.applySimpleLikeFilter(builder, alias, Group.title(), filter);

                return builder;
            }
        });

    }

    @Override
    public void update(
            ru.tandemservice.uniec.component.entrant.RequestedEnrollmentDirectionAddEdit.Model model,
            ErrorCollector errorCollector)
    {

        Model myModel = (Model) model;

        if (!myModel.isUseGroup()) {
            super.update(model, errorCollector);
            return;
        }

        final Session session = getSession();

        EntrantUtil.lockEnrollmentCampaign(session, model.getEntrantRequest().getEntrant());

        session.refresh(model.getEnrollmentCampaign());
        FlushMode defaultFlushMode = session.getFlushMode();
        RequestedEnrollmentDirection requestedEnrollmentDirection = model.getRequestedEnrollmentDirection();

        session.setFlushMode(FlushMode.MANUAL);
        try {
            EnrollmentDirection enrollmentDirection = EnrollmentDirectionUtil.getEnrollmentDirection(model, session);
            if (enrollmentDirection == null) {
                throw new ApplicationException("Направление приема с указанными параметрами не найдено.");
            }
            session.refresh(enrollmentDirection);
            if ((!enrollmentDirection.isContract()) && (requestedEnrollmentDirection.getCompensationType() != null) && (!requestedEnrollmentDirection.getCompensationType().isBudget())) {
                throw new ApplicationException("Нет приема на контракт по выбранному направлению приема.");
            }
            if ((!enrollmentDirection.isBudget()) && (requestedEnrollmentDirection.getCompensationType() != null) && (requestedEnrollmentDirection.getCompensationType().isBudget())) {
                throw new ApplicationException("Нет приема на бюджет по выбранному направлению приема.");
            }
            if (!requestedEnrollmentDirection.isTargetAdmission()) {
                requestedEnrollmentDirection.setExternalOrgUnit(null);
                requestedEnrollmentDirection.setTargetAdmissionKind(null);
            }
            int budgetMaxPriority = -2147483648;
            int contractMaxPriority = -2147483648;
            for (RequestedEnrollmentDirection direction : getList(RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.entrantRequest().entrant(), model.getEntrantRequest().getEntrant(), new String[0])) {
                int priority = direction.getPriorityPerEntrant();
                if (direction.getCompensationType().isBudget()) {
                    if (priority > budgetMaxPriority) {
                        budgetMaxPriority = priority;
                    }
                }
                else if (priority > contractMaxPriority) {
                    contractMaxPriority = priority;
                }
            }
            budgetMaxPriority++;
            contractMaxPriority++;
            boolean needPriorityPerEntrantNormalization = false;
            if (model.isEditForm()) {
                MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r");
                if (model.getEnrollmentCampaign().isDirectionPerCompTypeDiff()) {
                    builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.compensationType().s(), requestedEnrollmentDirection.getCompensationType()));
                }
                builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.entrantRequest().entrant().s(), model.getEntrantRequest().getEntrant()));
                builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.enrollmentDirection().s(), enrollmentDirection));
                builder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.id().s(), requestedEnrollmentDirection.getId()));
                if (builder.getResultCount(session) > 0L) {
                    errorCollector.add("Направление приема с указанными параметрами уже выбрано.");
                    return;
                }
                if (model.getRegNumber() == null) {
                    int maxReg = getMaxRegNumber(enrollmentDirection, session);
                    requestedEnrollmentDirection.setNumber(maxReg + 1);
                }
                else if ((!model.getRegNumber().equals(Integer.valueOf(requestedEnrollmentDirection.getNumber()))) || (!enrollmentDirection.equals(requestedEnrollmentDirection.getEnrollmentDirection()))) {
                    Criteria c = session.createCriteria(RequestedEnrollmentDirection.class);
                    c.add(Restrictions.eq(RequestedEnrollmentDirection.enrollmentDirection().s(), enrollmentDirection));
                    c.add(Restrictions.eq(RequestedEnrollmentDirection.number().s(), model.getRegNumber()));
                    RequestedEnrollmentDirection checkDirection = (RequestedEnrollmentDirection) c.uniqueResult();
                    if (checkDirection == null) {
                        requestedEnrollmentDirection.setNumber(model.getRegNumber().intValue());
                    }
                    else {
                        errorCollector.add("№ на направлении/специальности «" + model.getRegNumber() + "» уже занят абитуриентом «" + checkDirection.getEntrantRequest().getEntrant().getPerson().getFullFio() + "».", new String[]{"regNumber"});
                    }
                }
                EntrantRequestAddEditUtil.validateRequestedDirections(getSession(), Collections.singletonList(requestedEnrollmentDirection), model.getEntrantRequest());
                if (errorCollector.hasErrors()) {
                    return;
                }
                if (!model.getOldCompensationType().equals(requestedEnrollmentDirection.getCompensationType())) {
                    requestedEnrollmentDirection.setPriorityPerEntrant(requestedEnrollmentDirection.getCompensationType().isBudget() ? budgetMaxPriority++ : contractMaxPriority++);
                    needPriorityPerEntrantNormalization = true;
                }
                if (!model.getOldCompetitionKind().equals(requestedEnrollmentDirection.getCompetitionKind())) {
                    requestedEnrollmentDirection.setProfileChosenEntranceDiscipline(null);
                    session.update(requestedEnrollmentDirection);
                    session.flush();
                    for (ChosenEntranceDiscipline discipline : getList(ChosenEntranceDiscipline.class, ChosenEntranceDiscipline.chosenEnrollmentDirection().s(), requestedEnrollmentDirection, new String[0])) {
                        session.delete(discipline);
                    }
                }
                EcEntrantManager.instance().dao().updateOriginalDocuments(requestedEnrollmentDirection, requestedEnrollmentDirection.isOriginalDocumentHandedIn());

//				updateProfileExaminationMark(model);
                EcDistributionUtil.invokePrivateMethod(this, ru.tandemservice.uniec.component.entrant.RequestedEnrollmentDirectionAddEdit.DAO.class, "updateProfileExaminationMark", new Object[]{model});

//				updateProfileKnowledge(requestedEnrollmentDirection, model.getProfileKnowledgeList());
                EcDistributionUtil.invokePrivateMethod(this, ru.tandemservice.uniec.component.entrant.RequestedEnrollmentDirectionAddEdit.DAO.class, "updateProfileKnowledge", new Object[]{requestedEnrollmentDirection, model.getProfileKnowledgeList()});

                requestedEnrollmentDirection.setEnrollmentDirection(enrollmentDirection);

                session.update(requestedEnrollmentDirection);

                createOrUpdateDirectionExt(requestedEnrollmentDirection, session, myModel);

                if (needPriorityPerEntrantNormalization) {
                    EcEntrantManager.instance().dao().doNormalizePriorityPerEntrant(model.getEntrantRequest().getEntrant());
                }
            }
            else {
                List<RequestedEnrollmentDirection> newDirections = new ArrayList<>();


                List<MultiKey> list = (List<MultiKey>) EcDistributionUtil.invokePrivateMethod(this, ru.tandemservice.uniec.component.entrant.RequestedEnrollmentDirectionAddEdit.DAO.class, "getAddingEnrollmentDirection", new Object[]{model, enrollmentDirection, errorCollector});//getAddingEnrollmentDirection(model, enrollmentDirection, errorCollector);
                if (errorCollector.hasErrors()) {
                    return;
                }
                int maxPriority = (int) EcDistributionUtil.invokePrivateMethod(this, ru.tandemservice.uniec.component.entrant.RequestedEnrollmentDirectionAddEdit.DAO.class, "getMaxPriority", new Object[]{model.getEntrantRequest()});//getMaxPriority(model.getEntrantRequest());


                Map<EnrollmentDirection, MutableInt> maxRegNumberMap = SafeMap.get(key -> new MutableInt(getMaxRegNumber(key, session)));
                EntrantState activeState = (EntrantState) getCatalogItem(EntrantState.class, "1");
                for (MultiKey item : list) {
                    RequestedEnrollmentDirection direction = new RequestedEnrollmentDirection();
                    direction.update(requestedEnrollmentDirection);

                    direction.setEnrollmentDirection((EnrollmentDirection) item.getKey(0));
                    direction.setCompensationType((CompensationType) item.getKey(1));
                    direction.setEntrantRequest(model.getEntrantRequest());
                    direction.setRegDate(new Date());
                    direction.setPriority(++maxPriority);
                    direction.setPriorityPerEntrant(direction.getCompensationType().isBudget() ? budgetMaxPriority++ : contractMaxPriority++);
                    direction.setState(activeState);


                    MutableInt maxRegNumber = (MutableInt) maxRegNumberMap.get(direction.getEnrollmentDirection());
                    maxRegNumber.increment();
                    direction.setNumber(maxRegNumber.intValue());

                    newDirections.add(direction);
                }
                EntrantRequestAddEditUtil.validateRequestedDirections(getSession(), newDirections, model.getEntrantRequest());
                if (errorCollector.hasErrors()) {
                    return;
                }
                for (RequestedEnrollmentDirection direction : newDirections) {
                    session.save(direction);
                    if (requestedEnrollmentDirection.isOriginalDocumentHandedIn()) {
                        EcEntrantManager.instance().dao().updateOriginalDocuments(direction, true);
                    }
                    EcDistributionUtil.invokePrivateMethod(this, ru.tandemservice.uniec.component.entrant.RequestedEnrollmentDirectionAddEdit.DAO.class, "updateProfileKnowledge", new Object[]{direction, model.getProfileKnowledgeList()});
//					updateProfileKnowledge(direction, model.getProfileKnowledgeList());
                    createOrUpdateDirectionExt(direction, session, myModel);
                }
//				updateProfileExaminationMark(model);
                EcDistributionUtil.invokePrivateMethod(this, ru.tandemservice.uniec.component.entrant.RequestedEnrollmentDirectionAddEdit.DAO.class, "updateProfileExaminationMark", new Object[]{model});
            }
        }
        finally {
            session.setFlushMode(defaultFlushMode);
        }


    }

    private void createOrUpdateDirectionExt(RequestedEnrollmentDirection direction, Session session, Model model) {
        RequestedEnrollmentDirectionExt directionExt = UniecrmcUtil.getRequestedEnrollmentDirectionExt(direction);
        if (directionExt == null) {
            directionExt = new RequestedEnrollmentDirectionExt();
            directionExt.setRequestedEnrollmentDirection(direction);
        }

        directionExt.setCourse(model.getCourse());
        directionExt.setTerm(model.getTerm());
        directionExt.setGroup(model.getGroup());

        session.saveOrUpdate(directionExt);
    }

}
