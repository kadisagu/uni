package ru.tandemservice.uniecrmc.base.ext.EcProfileDistribution.ui.EntrantAdd;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.EcDistributionUtil;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.EcgEntrantRateRowDTORMC;
import ru.tandemservice.uniecrmc.base.ext.EcProfileDistribution.ui.Pub.EcProfileDistributionPubUI;
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.EcProfileDistributionManager;
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.ui.EntrantAdd.EcProfileDistributionEntrantAdd;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.settings.NotUsedTargetAdmissionKind;

import java.util.List;
import java.util.Map;


public class EcProfileDistributionEntrantAddUI extends ru.tandemservice.uniec.base.bo.EcProfileDistribution.ui.EntrantAdd.EcProfileDistributionEntrantAddUI {

    private boolean usePriority;
    private boolean haveSpecialRights = false;

    @Override
    public void onComponentRefresh() {

        setUsePriority(EcDistributionUtil.isUsePriority(getDistribution().getConfig().getEcgItem().getEnrollmentCampaign()));

        if (!isUsePriority()) {
            super.onComponentRefresh();
            return;
        }

        switch (this.getRateRule().name()) {
            case EcProfileDistributionPubUI.RULE_TARGET_ADMISSION:
                this.setTitle("Добавление абитуриентов (поступающих по целевому приему) в распределение");
                break;
            case EcProfileDistributionPubUI.RULE_WITHOUT_ENTRANCE_DISCIPLINES:
                this.setTitle("Добавление абитуриентов (поступающих без ВИ) в распределение");
                break;
            case EcProfileDistributionPubUI.RULE_NON_COMPETITIVE: {
                this.setTitle("Добавление абитуриентов (поступающих вне конкурса) в распределение");
                this.setHaveSpecialRights(true);
                break;
            }
            case EcProfileDistributionPubUI.RULE_COMPETITIVE:
                this.setTitle("Добавление абитуриентов (поступающих на общих основаниях) в распределение");
                break;
            default:
                throw new IllegalArgumentException("Unknown rateRule: " + this.getRateRule());
        }

        this.setMinCount(-1);

        this.setHasDefaultChecked(false);

        List<TargetAdmissionKind> targetAdmissionKindList = new DQLSelectBuilder().fromEntity(TargetAdmissionKind.class, "e")
                .where(DQLExpressions.notIn(DQLExpressions.property(TargetAdmissionKind.id().fromAlias("e")), new DQLSelectBuilder().fromEntity(NotUsedTargetAdmissionKind.class, "nu")
                        .column(DQLExpressions.property(NotUsedTargetAdmissionKind.targetAdmissionKind().id().fromAlias("nu")))
                        .where(DQLExpressions.eq(DQLExpressions.property(NotUsedTargetAdmissionKind.enrollmentCampaign().fromAlias("nu")), DQLExpressions.value(this.getDistribution().getConfig().getEcgItem().getEnrollmentCampaign())))
                        .buildQuery()))
                .createStatement(_uiSupport.getSession()).list();

        Map<CompetitionKind, Integer> competitionKindPriorityMap = UniecDAOFacade.getSettingsDAO().getCompetitionKindPriorities(this.getDistribution().getConfig().getEcgItem().getEnrollmentCampaign());

        this.setRateList(EcProfileDistributionManager.instance().dao().getEntrantRateRowList(this.getDistribution(), targetAdmissionKindList, competitionKindPriorityMap, this.getRateRule()));

        EcDistributionUtil.invokePrivateMethod(this, ru.tandemservice.uniec.base.bo.EcProfileDistribution.ui.EntrantAdd.EcProfileDistributionEntrantAddUI.class, "doRefreshEntrantRateList", new Object[]{});

    }

    public String getDisciplines() {
        return ((EcgEntrantRateRowDTORMC) getConfig().getDataSource(EcProfileDistributionEntrantAdd.PRE_STUDENT_DS).getCurrent()).getDisciplinesHTMLDescription();
    }

    public boolean isUseProfileMark() {
        return !isUsePriority();
    }

    public boolean isUsePriority() {
        return usePriority;
    }

    public void setUsePriority(boolean usePriority) {
        this.usePriority = usePriority;
    }

    public boolean isHaveSpecialRights() {
        return haveSpecialRights;
    }

    public void setHaveSpecialRights(boolean haveSpecialRights) {
        this.haveSpecialRights = haveSpecialRights;
    }

}
