package ru.tandemservice.uniecrmc.component.menu.EntrantRatingSettings;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.sec.ISecurityService;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniecrmc.entity.entrant.EntrantRecommendedSettings;

public class Model {
    private DynamicListDataSource<EntrantRecommendedSettings> dataSource;

    public boolean isCanRun() {
        ISecurityService service = (ISecurityService) ApplicationRuntime.getBean("org.tandemframework.core.sec.ISecurityService");
        return service.check(service.getCommonSecurityObject(), UserContext.getInstance().getPrincipalContext(), "uniecrmcEntrantRatingSettingsRun");
    }

    public DynamicListDataSource<EntrantRecommendedSettings> getDataSource() {
        return dataSource;
    }

    public void setDataSource(
            DynamicListDataSource<EntrantRecommendedSettings> dataSource)
    {
        this.dataSource = dataSource;
    }


}
