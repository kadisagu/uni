package ru.tandemservice.uniec.ws;

import ru.tandemservice.uniecrmc.online.dao.UniecDaoFacade;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@WebService(serviceName = "EntrantRegistrationServiceExtRmc")
public class EntrantRegistrationServiceExtRmc extends ru.tandemservice.uniec.ws.EntrantRegistrationService {

    @WebMethod
    public void synchronizeUserProfilePriorities(long userId, ProfilePriorityData[] priorityList) {
        List<ProfilePriorityData> list = priorityList != null ? Arrays.asList(priorityList) : Collections.<ProfilePriorityData>emptyList();
        UniecDaoFacade.getOnlineEntrantRegistrationRmcDAO().synchronizeUserProfilePriorities(userId, list);
    }

    @WebMethod
    public void createOrUpdateOnlineEntrantRmc(long userId, EntrantDataExt entrantDataExt) {
        UniecDaoFacade.getOnlineEntrantRegistrationRmcDAO().createOrUpdateOnlineEntrant(userId, entrantDataExt);

    }

    @WebMethod
    public ProfilePriorityData[] getProfilePriorities(long userId, UserDirectionDataExt[] userDirections) {
        List<UserDirectionDataExt> list = userDirections != null ? Arrays.asList(userDirections) : Collections.<UserDirectionDataExt>emptyList();
        List<ProfilePriorityData> result = UniecDaoFacade.getOnlineEntrantRegistrationRmcDAO().getProfilePriorities(userId, list);
        return result.toArray(new ProfilePriorityData[0]);
    }

    @Override
    @WebMethod
    public EntrantDataExt getEntrantData(long userId) {
        return UniecDaoFacade.getOnlineEntrantRegistrationRmcDAO().getEntrantDataExt(userId);
    }

    @Override
    @WebMethod
    public UserDirectionDataExt[] getUserDirections(long userId)
    {
        List<UserDirectionDataExt> result = UniecDaoFacade.getOnlineEntrantRegistrationRmcDAO().getUserDirectionsExt(userId);
        return result.toArray(new UserDirectionDataExt[result.size()]);
    }

    @WebMethod
    public void synchronizeUserDirectionsExt(long userId, UserDirectionDataExt[] userDirections)
    {
        UniecDaoFacade.getOnlineEntrantRegistrationRmcDAO().synchronizeUserDirections(userId, (userDirections != null) ? userDirections : new UserDirectionDataExt[0]);
    }

}
