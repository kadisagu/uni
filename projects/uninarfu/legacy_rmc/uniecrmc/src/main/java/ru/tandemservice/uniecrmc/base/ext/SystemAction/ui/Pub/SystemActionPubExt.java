package ru.tandemservice.uniecrmc.base.ext.SystemAction.ui.Pub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.ui.Pub.SystemActionPub;

@Configuration
public class SystemActionPubExt extends BusinessComponentExtensionManager {

    public static final String ECRMC_SYSTEM_ACTION_PUB_ADDON_NAME = "ecrmcSystemActionPubAddon";

    @Autowired
    private SystemActionPub systemActionPub;

    @Bean
    public PresenterExtension presenterExtension() {
        return presenterExtensionBuilder(this.systemActionPub.presenterExtPoint())
                .addAddon(uiAddon(ECRMC_SYSTEM_ACTION_PUB_ADDON_NAME, EcRMCSystemActionPubAddon.class))
                .create();
    }

}
