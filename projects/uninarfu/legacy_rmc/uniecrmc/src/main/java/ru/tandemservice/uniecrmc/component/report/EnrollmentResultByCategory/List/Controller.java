package ru.tandemservice.uniecrmc.component.report.EnrollmentResultByCategory.List;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.uniecrmc.entity.entrant.EnrollmentResultByCategoryReport;
import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        model.setSettings(component.getSettings());
        ((IDAO) getDao()).prepare(model);

        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<EnrollmentResultByCategoryReport> dataSource = UniBaseUtils.createDataSource(component, (IListDataSourceDao<?>) getDao());
        dataSource.addColumn(new IndicatorColumn("Иконка", null).defaultIndicator(new IndicatorColumn.Item("report", "Отчет")).setOrderable(false).setClickable(false).setRequired(true));
        dataSource.addColumn(new SimpleColumn("Дата формирования", "formingDate", DateFormatter.DATE_FORMATTER_WITH_TIME));
        dataSource.addColumn(new IndicatorColumn("Печать", null, "onClickPrintReport").defaultIndicator(new IndicatorColumn.Item("printer", "Печать")).setImageHeader(false).setDisableSecondSubmit(false).setPermissionKey("printUniecStorableReport").setOrderable(false));
        dataSource.addColumn(new ActionColumn("Удалить", "delete", "onClickDeleteReport", "Удалить отчет от «{0}»?", new Object[]{"formingDate"}).setPermissionKey("deleteUniecStorableReport"));
        dataSource.setOrder("formingDate", OrderDirection.desc);

        model.setDataSource(dataSource);
    }

    public void onClickAddReport(IBusinessComponent component) {
        component.createRegion(new ComponentActivator("ru.tandemservice.uniecrmc.component.report.EnrollmentResultByCategory.Add"));
    }

    public void onClickSearch(IBusinessComponent component) {
        component.saveSettings();
        ((Model) getModel(component)).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component) {
        EntrantFilterUtil.resetEnrollmentCampaignFilter((IEnrollmentCampaignSelectModel) getModel(component));
        onClickSearch(component);
    }

    public void onClickPrintReport(IBusinessComponent component) throws Exception {
        activateInRoot(component, new ComponentActivator("ru.tandemservice.uni.component.reports.DownloadStorableReport", new UniMap().add("reportId", component.getListenerParameter()).add("extension", "xls").add("zip", Boolean.valueOf(false))));
    }

    public void onClickDeleteReport(IBusinessComponent component) {
        ((IDAO) getDao()).deleteRow(component);
    }

}
