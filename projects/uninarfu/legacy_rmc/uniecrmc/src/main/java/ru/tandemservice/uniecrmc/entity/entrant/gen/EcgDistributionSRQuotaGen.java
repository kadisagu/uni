package ru.tandemservice.uniecrmc.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistributionQuota;
import ru.tandemservice.uniecrmc.entity.entrant.EcgDistributionSRQuota;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Количество мест имеющих особые права по направлению приема в основном распределении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcgDistributionSRQuotaGen extends EntityBase
 implements INaturalIdentifiable<EcgDistributionSRQuotaGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniecrmc.entity.entrant.EcgDistributionSRQuota";
    public static final String ENTITY_NAME = "ecgDistributionSRQuota";
    public static final int VERSION_HASH = -1211704456;
    private static IEntityMeta ENTITY_META;

    public static final String L_DISTRIBUTION_QUOTA = "distributionQuota";
    public static final String P_QUOTA = "quota";

    private EcgDistributionQuota _distributionQuota;     // Количество мест по направлению приема в основном распределении
    private int _quota;     // Количество мест

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Количество мест по направлению приема в основном распределении. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EcgDistributionQuota getDistributionQuota()
    {
        return _distributionQuota;
    }

    /**
     * @param distributionQuota Количество мест по направлению приема в основном распределении. Свойство не может быть null и должно быть уникальным.
     */
    public void setDistributionQuota(EcgDistributionQuota distributionQuota)
    {
        dirty(_distributionQuota, distributionQuota);
        _distributionQuota = distributionQuota;
    }

    /**
     * @return Количество мест. Свойство не может быть null.
     */
    @NotNull
    public int getQuota()
    {
        return _quota;
    }

    /**
     * @param quota Количество мест. Свойство не может быть null.
     */
    public void setQuota(int quota)
    {
        dirty(_quota, quota);
        _quota = quota;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EcgDistributionSRQuotaGen)
        {
            if (withNaturalIdProperties)
            {
                setDistributionQuota(((EcgDistributionSRQuota)another).getDistributionQuota());
            }
            setQuota(((EcgDistributionSRQuota)another).getQuota());
        }
    }

    public INaturalId<EcgDistributionSRQuotaGen> getNaturalId()
    {
        return new NaturalId(getDistributionQuota());
    }

    public static class NaturalId extends NaturalIdBase<EcgDistributionSRQuotaGen>
    {
        private static final String PROXY_NAME = "EcgDistributionSRQuotaNaturalProxy";

        private Long _distributionQuota;

        public NaturalId()
        {}

        public NaturalId(EcgDistributionQuota distributionQuota)
        {
            _distributionQuota = ((IEntity) distributionQuota).getId();
        }

        public Long getDistributionQuota()
        {
            return _distributionQuota;
        }

        public void setDistributionQuota(Long distributionQuota)
        {
            _distributionQuota = distributionQuota;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EcgDistributionSRQuotaGen.NaturalId) ) return false;

            EcgDistributionSRQuotaGen.NaturalId that = (NaturalId) o;

            if( !equals(getDistributionQuota(), that.getDistributionQuota()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getDistributionQuota());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getDistributionQuota());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcgDistributionSRQuotaGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcgDistributionSRQuota.class;
        }

        public T newInstance()
        {
            return (T) new EcgDistributionSRQuota();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "distributionQuota":
                    return obj.getDistributionQuota();
                case "quota":
                    return obj.getQuota();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "distributionQuota":
                    obj.setDistributionQuota((EcgDistributionQuota) value);
                    return;
                case "quota":
                    obj.setQuota((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "distributionQuota":
                        return true;
                case "quota":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "distributionQuota":
                    return true;
                case "quota":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "distributionQuota":
                    return EcgDistributionQuota.class;
                case "quota":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EcgDistributionSRQuota> _dslPath = new Path<EcgDistributionSRQuota>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcgDistributionSRQuota");
    }
            

    /**
     * @return Количество мест по направлению приема в основном распределении. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EcgDistributionSRQuota#getDistributionQuota()
     */
    public static EcgDistributionQuota.Path<EcgDistributionQuota> distributionQuota()
    {
        return _dslPath.distributionQuota();
    }

    /**
     * @return Количество мест. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EcgDistributionSRQuota#getQuota()
     */
    public static PropertyPath<Integer> quota()
    {
        return _dslPath.quota();
    }

    public static class Path<E extends EcgDistributionSRQuota> extends EntityPath<E>
    {
        private EcgDistributionQuota.Path<EcgDistributionQuota> _distributionQuota;
        private PropertyPath<Integer> _quota;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Количество мест по направлению приема в основном распределении. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EcgDistributionSRQuota#getDistributionQuota()
     */
        public EcgDistributionQuota.Path<EcgDistributionQuota> distributionQuota()
        {
            if(_distributionQuota == null )
                _distributionQuota = new EcgDistributionQuota.Path<EcgDistributionQuota>(L_DISTRIBUTION_QUOTA, this);
            return _distributionQuota;
        }

    /**
     * @return Количество мест. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EcgDistributionSRQuota#getQuota()
     */
        public PropertyPath<Integer> quota()
        {
            if(_quota == null )
                _quota = new PropertyPath<Integer>(EcgDistributionSRQuotaGen.P_QUOTA, this);
            return _quota;
        }

        public Class getEntityClass()
        {
            return EcgDistributionSRQuota.class;
        }

        public String getEntityName()
        {
            return "ecgDistributionSRQuota";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
