package ru.tandemservice.uniecrmc.component.wizard.PersonalDataStep;

import org.tandemframework.shared.person.base.entity.PersonForeignLanguage;


public class DAO extends ru.tandemservice.uniec.component.wizard.PersonalDataStep.DAO {
    @Override
    public void prepareSelectLanguage(ru.tandemservice.uniec.component.wizard.PersonalDataStep.Model model) {
        super.prepareSelectLanguage(model);
        updateBasicLanguage(model);
    }

    @Override
    public void deleteLanguage(ru.tandemservice.uniec.component.wizard.PersonalDataStep.Model model, Long personLanguageId) {
        super.deleteLanguage(model, personLanguageId);
        updateBasicLanguage(model);
    }

    public void updateBasicLanguage(ru.tandemservice.uniec.component.wizard.PersonalDataStep.Model model) {
        Model modelExt = (Model) model;
        boolean hasBasic = false;

        for (PersonForeignLanguage language : model.getSelectedLanguageList()) {
            if (language.isMain())
                hasBasic = true;
        }
        modelExt.setHasBasicLanguage(hasBasic);

    }

}
