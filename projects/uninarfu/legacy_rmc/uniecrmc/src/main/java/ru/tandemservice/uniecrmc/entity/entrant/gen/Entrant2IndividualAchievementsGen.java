package ru.tandemservice.uniecrmc.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniecrmc.entity.catalog.IndividualAchievements;
import ru.tandemservice.uniecrmc.entity.entrant.Entrant2IndividualAchievements;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Индивидуальные достижения абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class Entrant2IndividualAchievementsGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniecrmc.entity.entrant.Entrant2IndividualAchievements";
    public static final String ENTITY_NAME = "entrant2IndividualAchievements";
    public static final int VERSION_HASH = 702816019;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT = "entrant";
    public static final String L_INDIVIDUAL_ACHIEVEMENTS = "individualAchievements";
    public static final String P_TEXT = "text";

    private Entrant _entrant;     // Абитуриент
    private IndividualAchievements _individualAchievements;     // Вид индивидуального достижения
    private String _text;     // Описание

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public Entrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Абитуриент. Свойство не может быть null.
     */
    public void setEntrant(Entrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Вид индивидуального достижения.
     */
    public IndividualAchievements getIndividualAchievements()
    {
        return _individualAchievements;
    }

    /**
     * @param individualAchievements Вид индивидуального достижения.
     */
    public void setIndividualAchievements(IndividualAchievements individualAchievements)
    {
        dirty(_individualAchievements, individualAchievements);
        _individualAchievements = individualAchievements;
    }

    /**
     * @return Описание.
     */
    public String getText()
    {
        return _text;
    }

    /**
     * @param text Описание.
     */
    public void setText(String text)
    {
        dirty(_text, text);
        _text = text;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof Entrant2IndividualAchievementsGen)
        {
            setEntrant(((Entrant2IndividualAchievements)another).getEntrant());
            setIndividualAchievements(((Entrant2IndividualAchievements)another).getIndividualAchievements());
            setText(((Entrant2IndividualAchievements)another).getText());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends Entrant2IndividualAchievementsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Entrant2IndividualAchievements.class;
        }

        public T newInstance()
        {
            return (T) new Entrant2IndividualAchievements();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrant":
                    return obj.getEntrant();
                case "individualAchievements":
                    return obj.getIndividualAchievements();
                case "text":
                    return obj.getText();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrant":
                    obj.setEntrant((Entrant) value);
                    return;
                case "individualAchievements":
                    obj.setIndividualAchievements((IndividualAchievements) value);
                    return;
                case "text":
                    obj.setText((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrant":
                        return true;
                case "individualAchievements":
                        return true;
                case "text":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrant":
                    return true;
                case "individualAchievements":
                    return true;
                case "text":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrant":
                    return Entrant.class;
                case "individualAchievements":
                    return IndividualAchievements.class;
                case "text":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Entrant2IndividualAchievements> _dslPath = new Path<Entrant2IndividualAchievements>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Entrant2IndividualAchievements");
    }
            

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.Entrant2IndividualAchievements#getEntrant()
     */
    public static Entrant.Path<Entrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Вид индивидуального достижения.
     * @see ru.tandemservice.uniecrmc.entity.entrant.Entrant2IndividualAchievements#getIndividualAchievements()
     */
    public static IndividualAchievements.Path<IndividualAchievements> individualAchievements()
    {
        return _dslPath.individualAchievements();
    }

    /**
     * @return Описание.
     * @see ru.tandemservice.uniecrmc.entity.entrant.Entrant2IndividualAchievements#getText()
     */
    public static PropertyPath<String> text()
    {
        return _dslPath.text();
    }

    public static class Path<E extends Entrant2IndividualAchievements> extends EntityPath<E>
    {
        private Entrant.Path<Entrant> _entrant;
        private IndividualAchievements.Path<IndividualAchievements> _individualAchievements;
        private PropertyPath<String> _text;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.Entrant2IndividualAchievements#getEntrant()
     */
        public Entrant.Path<Entrant> entrant()
        {
            if(_entrant == null )
                _entrant = new Entrant.Path<Entrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Вид индивидуального достижения.
     * @see ru.tandemservice.uniecrmc.entity.entrant.Entrant2IndividualAchievements#getIndividualAchievements()
     */
        public IndividualAchievements.Path<IndividualAchievements> individualAchievements()
        {
            if(_individualAchievements == null )
                _individualAchievements = new IndividualAchievements.Path<IndividualAchievements>(L_INDIVIDUAL_ACHIEVEMENTS, this);
            return _individualAchievements;
        }

    /**
     * @return Описание.
     * @see ru.tandemservice.uniecrmc.entity.entrant.Entrant2IndividualAchievements#getText()
     */
        public PropertyPath<String> text()
        {
            if(_text == null )
                _text = new PropertyPath<String>(Entrant2IndividualAchievementsGen.P_TEXT, this);
            return _text;
        }

        public Class getEntityClass()
        {
            return Entrant2IndividualAchievements.class;
        }

        public String getEntityName()
        {
            return "entrant2IndividualAchievements";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
