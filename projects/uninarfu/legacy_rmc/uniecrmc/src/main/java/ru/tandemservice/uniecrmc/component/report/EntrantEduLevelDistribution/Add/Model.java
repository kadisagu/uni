package ru.tandemservice.uniecrmc.component.report.EntrantEduLevelDistribution.Add;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniecrmc.entity.entrant.EntrantEduLevelDistributionReport;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;

import java.util.List;

public class Model {
    private EntrantEduLevelDistributionReport report = new EntrantEduLevelDistributionReport();
    private boolean formativeOrgUnitActive;
    private boolean educationLevelHighSchoolActive;
    private boolean developFormActive;

    private ISelectModel enrollmentCampaignModel;
    private ISelectModel formativeOrgUnitListModel;
    private ISelectModel educationLevelHighSchoolListModel;
    private ISelectModel developFormListModel;
    private ISelectModel developConditionListModel;

    private List<OrgUnit> formativeOrgUnitList;
    private List<EducationLevelsHighSchool> educationLevelHighSchoolList;
    private List<DevelopForm> developFormList;
    private List<DevelopCondition> developConditionList;

    public ISelectModel getDevelopConditionListModel() {
        return developConditionListModel;
    }

    public void setDevelopConditionListModel(ISelectModel developConditionListModel) {
        this.developConditionListModel = developConditionListModel;
    }

    public List<DevelopCondition> getDevelopConditionList() {
        return developConditionList;
    }

    public void setDevelopConditionList(List<DevelopCondition> developConditionList) {
        this.developConditionList = developConditionList;
    }

    public EntrantEduLevelDistributionReport getReport() {
        return report;
    }

    public void setReport(EntrantEduLevelDistributionReport report) {
        this.report = report;
    }

    public boolean isFormativeOrgUnitActive() {
        return formativeOrgUnitActive;
    }

    public void setFormativeOrgUnitActive(boolean formativeOrgUnitActive) {
        this.formativeOrgUnitActive = formativeOrgUnitActive;
    }

    public boolean isEducationLevelHighSchoolActive() {
        return educationLevelHighSchoolActive;
    }

    public void setEducationLevelHighSchoolActive(boolean educationLevelHighSchoolActive) {
        this.educationLevelHighSchoolActive = educationLevelHighSchoolActive;
    }

    public boolean isDevelopFormActive() {
        return developFormActive;
    }

    public void setDevelopFormActive(boolean developFormActive) {
        this.developFormActive = developFormActive;
    }

    public ISelectModel getEnrollmentCampaignModel() {
        return enrollmentCampaignModel;
    }

    public void setEnrollmentCampaignModel(ISelectModel enrollmentCampaignModel) {
        this.enrollmentCampaignModel = enrollmentCampaignModel;
    }

    public ISelectModel getFormativeOrgUnitListModel() {
        return formativeOrgUnitListModel;
    }

    public void setFormativeOrgUnitListModel(ISelectModel formativeOrgUnitListModel) {
        this.formativeOrgUnitListModel = formativeOrgUnitListModel;
    }

    public ISelectModel getEducationLevelHighSchoolListModel() {
        return educationLevelHighSchoolListModel;
    }

    public void setEducationLevelHighSchoolListModel(ISelectModel educationLevelHighSchoolListModel) {
        this.educationLevelHighSchoolListModel = educationLevelHighSchoolListModel;
    }

    public ISelectModel getDevelopFormListModel() {
        return developFormListModel;
    }

    public void setDevelopFormListModel(ISelectModel developFormListModel) {
        this.developFormListModel = developFormListModel;
    }

    public List<OrgUnit> getFormativeOrgUnitList() {
        return formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList) {
        this.formativeOrgUnitList = formativeOrgUnitList;
    }

    public List<EducationLevelsHighSchool> getEducationLevelHighSchoolList() {
        return educationLevelHighSchoolList;
    }

    public void setEducationLevelHighSchoolList(List<EducationLevelsHighSchool> educationLevelHighSchoolList) {
        this.educationLevelHighSchoolList = educationLevelHighSchoolList;
    }

    public List<DevelopForm> getDevelopFormList() {
        return developFormList;
    }

    public void setDevelopFormList(List<DevelopForm> developFormList) {
        this.developFormList = developFormList;
    }
}

