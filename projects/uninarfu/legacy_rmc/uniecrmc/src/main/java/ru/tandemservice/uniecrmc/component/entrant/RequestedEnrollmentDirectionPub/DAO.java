package ru.tandemservice.uniecrmc.component.entrant.RequestedEnrollmentDirectionPub;

import ru.tandemservice.uniecrmc.util.UniecrmcUtil;

public class DAO extends ru.tandemservice.uniec.component.entrant.RequestedEnrollmentDirectionPub.DAO {

    @Override
    public void prepare(ru.tandemservice.uniec.component.entrant.RequestedEnrollmentDirectionPub.Model model) {
        super.prepare(model);
        Model myModel = (Model) model;

        if (!myModel.isUseGroup())
            return;

        myModel.setDirectionExt(UniecrmcUtil.getRequestedEnrollmentDirectionExt(model.getRequestedEnrollmentDirection()));
    }
}
