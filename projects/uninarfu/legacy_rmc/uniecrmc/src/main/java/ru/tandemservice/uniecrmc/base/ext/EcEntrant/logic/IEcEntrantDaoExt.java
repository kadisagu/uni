package ru.tandemservice.uniecrmc.base.ext.EcEntrant.logic;

import ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt;
import ru.tandemservice.uniec.base.bo.EcEntrant.logic.IEcEntrantDao;
import ru.tandemservice.uniec.entity.catalog.ProfileKnowledge;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.ProfileExaminationMark;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.List;
import java.util.Map;

public interface IEcEntrantDaoExt extends IEcEntrantDao {

    public abstract void saveOrUpdateEntrantRequest(List<RequestedEnrollmentDirection> paramList1, List<RequestedEnrollmentDirection> paramList2, boolean paramBoolean, EntrantRequest paramEntrantRequest, Integer paramInteger1, Integer paramInteger2, Map<Long, List<ProfileKnowledge>> paramMap, Map<Long, List<ProfileExaminationMark>> paramMap1, Map<Long, RequestedEnrollmentDirectionExt> directionExtMap);
}
