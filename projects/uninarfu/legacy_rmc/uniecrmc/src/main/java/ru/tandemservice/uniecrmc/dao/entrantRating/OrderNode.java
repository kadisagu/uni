package ru.tandemservice.uniecrmc.dao.entrantRating;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import ru.tandemservice.uniec.entity.orders.AbstractEntrantExtract;

import javax.xml.bind.annotation.XmlAttribute;

public class OrderNode {

    @XmlAttribute
    public String orderNumber;

    @XmlAttribute
    public String orderDate;

    public OrderNode() {
    }

    public OrderNode(AbstractEntrantExtract extract) {
        this.orderNumber = extract.getParagraph().getOrder().getNumber() != null ? extract.getParagraph().getOrder().getNumber() : "";
        this.orderDate = extract.getParagraph().getOrder().getCommitDate() != null ? RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(extract.getParagraph().getOrder().getCommitDate()) : "";
    }
}
