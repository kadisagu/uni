package ru.tandemservice.uniecrmc.component.wizard;

import org.tandemframework.shared.commonbase.tapestry.validator.ParamRequiredValidator;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.codes.IdentityCardTypeCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;

import java.util.Arrays;
import java.util.List;

public class WizardUtil {

    public static boolean isRequired(String key) {
        return ParamRequiredValidator.isRequired(key);
    }

    public static List<IdentityCardType> getIdentityCardTypeList() {
        List<String> codeList = Arrays.asList(
                IdentityCardTypeCodes.PASPORT_GRAJDANINA_ROSSIYSKOY_FEDERATSII,
                IdentityCardTypeCodes.PASPORT_GRAJDANINA_INOSTRANNOGO_GOSUDARSTVA,
                IdentityCardTypeCodes.DRUGOY_DOKUMENT
        );

        return UniDaoFacade.getCoreDao().getList(IdentityCardType.class, IdentityCardType.code(), codeList);
    }

}
