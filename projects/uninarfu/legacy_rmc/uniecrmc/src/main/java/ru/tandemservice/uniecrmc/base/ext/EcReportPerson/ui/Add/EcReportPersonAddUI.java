package ru.tandemservice.uniecrmc.base.ext.EcReportPerson.ui.Add;

import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

public class EcReportPersonAddUI extends ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.EcReportPersonAddUI {

    @Override
    public void onComponentRefresh() {
        EnrollmentCampaign saved = getEntrantData() != null && getEntrantData().getEnrollmentCampaign() != null ? getEntrantData().getEnrollmentCampaign().getData() : null;
        super.onComponentRefresh();

        if (saved != null)
            getEntrantData().getEnrollmentCampaign().setData(saved);
    }
}
