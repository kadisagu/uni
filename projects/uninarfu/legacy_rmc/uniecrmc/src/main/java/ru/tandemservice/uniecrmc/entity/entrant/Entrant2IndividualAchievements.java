package ru.tandemservice.uniecrmc.entity.entrant;

import ru.tandemservice.uniecrmc.entity.entrant.gen.Entrant2IndividualAchievementsGen;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;

/**
 * Индивидуальные достижения абитуриента
 */
public class Entrant2IndividualAchievements extends Entrant2IndividualAchievementsGen {
    public String getUid(EntrantRequest entrantrequest)
    {
        Long idTemp = entrantrequest.getId() + this.getId();
        String uid = "IA:" + idTemp.hashCode();
        return uid;
    }
}