package ru.tandemservice.uniecrmc.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение сущности Направление подготовки (специальность) приема
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrollmentDirectionExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionExt";
    public static final String ENTITY_NAME = "enrollmentDirectionExt";
    public static final int VERSION_HASH = 1154189190;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_DIRECTION = "enrollmentDirection";
    public static final String P_HAVE_SPECIAL_RIGHTS_PLAN_BUDGET = "haveSpecialRightsPlanBudget";
    public static final String P_HAVE_SPECIAL_RIGHTS_PLAN_CONTRACT = "haveSpecialRightsPlanContract";
    public static final String P_ADVANSED_PLAN_BUDGET = "advansedPlanBudget";

    private EnrollmentDirection _enrollmentDirection;     // Направление подготовки (специальность) приема
    private Integer _haveSpecialRightsPlanBudget;     // План приема имеющих особые права при поступлении по бюджету
    private Integer _haveSpecialRightsPlanContract;     // План приема имеющих особые права при поступлении по договору
    private Integer _advansedPlanBudget;     // Дополнительный план приема на бюджет

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Направление подготовки (специальность) приема. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EnrollmentDirection getEnrollmentDirection()
    {
        return _enrollmentDirection;
    }

    /**
     * @param enrollmentDirection Направление подготовки (специальность) приема. Свойство не может быть null и должно быть уникальным.
     */
    public void setEnrollmentDirection(EnrollmentDirection enrollmentDirection)
    {
        dirty(_enrollmentDirection, enrollmentDirection);
        _enrollmentDirection = enrollmentDirection;
    }

    /**
     * @return План приема имеющих особые права при поступлении по бюджету.
     */
    public Integer getHaveSpecialRightsPlanBudget()
    {
        return _haveSpecialRightsPlanBudget;
    }

    /**
     * @param haveSpecialRightsPlanBudget План приема имеющих особые права при поступлении по бюджету.
     */
    public void setHaveSpecialRightsPlanBudget(Integer haveSpecialRightsPlanBudget)
    {
        dirty(_haveSpecialRightsPlanBudget, haveSpecialRightsPlanBudget);
        _haveSpecialRightsPlanBudget = haveSpecialRightsPlanBudget;
    }

    /**
     * @return План приема имеющих особые права при поступлении по договору.
     */
    public Integer getHaveSpecialRightsPlanContract()
    {
        return _haveSpecialRightsPlanContract;
    }

    /**
     * @param haveSpecialRightsPlanContract План приема имеющих особые права при поступлении по договору.
     */
    public void setHaveSpecialRightsPlanContract(Integer haveSpecialRightsPlanContract)
    {
        dirty(_haveSpecialRightsPlanContract, haveSpecialRightsPlanContract);
        _haveSpecialRightsPlanContract = haveSpecialRightsPlanContract;
    }

    /**
     * @return Дополнительный план приема на бюджет.
     */
    public Integer getAdvansedPlanBudget()
    {
        return _advansedPlanBudget;
    }

    /**
     * @param advansedPlanBudget Дополнительный план приема на бюджет.
     */
    public void setAdvansedPlanBudget(Integer advansedPlanBudget)
    {
        dirty(_advansedPlanBudget, advansedPlanBudget);
        _advansedPlanBudget = advansedPlanBudget;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrollmentDirectionExtGen)
        {
            setEnrollmentDirection(((EnrollmentDirectionExt)another).getEnrollmentDirection());
            setHaveSpecialRightsPlanBudget(((EnrollmentDirectionExt)another).getHaveSpecialRightsPlanBudget());
            setHaveSpecialRightsPlanContract(((EnrollmentDirectionExt)another).getHaveSpecialRightsPlanContract());
            setAdvansedPlanBudget(((EnrollmentDirectionExt)another).getAdvansedPlanBudget());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrollmentDirectionExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrollmentDirectionExt.class;
        }

        public T newInstance()
        {
            return (T) new EnrollmentDirectionExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentDirection":
                    return obj.getEnrollmentDirection();
                case "haveSpecialRightsPlanBudget":
                    return obj.getHaveSpecialRightsPlanBudget();
                case "haveSpecialRightsPlanContract":
                    return obj.getHaveSpecialRightsPlanContract();
                case "advansedPlanBudget":
                    return obj.getAdvansedPlanBudget();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentDirection":
                    obj.setEnrollmentDirection((EnrollmentDirection) value);
                    return;
                case "haveSpecialRightsPlanBudget":
                    obj.setHaveSpecialRightsPlanBudget((Integer) value);
                    return;
                case "haveSpecialRightsPlanContract":
                    obj.setHaveSpecialRightsPlanContract((Integer) value);
                    return;
                case "advansedPlanBudget":
                    obj.setAdvansedPlanBudget((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentDirection":
                        return true;
                case "haveSpecialRightsPlanBudget":
                        return true;
                case "haveSpecialRightsPlanContract":
                        return true;
                case "advansedPlanBudget":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentDirection":
                    return true;
                case "haveSpecialRightsPlanBudget":
                    return true;
                case "haveSpecialRightsPlanContract":
                    return true;
                case "advansedPlanBudget":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentDirection":
                    return EnrollmentDirection.class;
                case "haveSpecialRightsPlanBudget":
                    return Integer.class;
                case "haveSpecialRightsPlanContract":
                    return Integer.class;
                case "advansedPlanBudget":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrollmentDirectionExt> _dslPath = new Path<EnrollmentDirectionExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrollmentDirectionExt");
    }
            

    /**
     * @return Направление подготовки (специальность) приема. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionExt#getEnrollmentDirection()
     */
    public static EnrollmentDirection.Path<EnrollmentDirection> enrollmentDirection()
    {
        return _dslPath.enrollmentDirection();
    }

    /**
     * @return План приема имеющих особые права при поступлении по бюджету.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionExt#getHaveSpecialRightsPlanBudget()
     */
    public static PropertyPath<Integer> haveSpecialRightsPlanBudget()
    {
        return _dslPath.haveSpecialRightsPlanBudget();
    }

    /**
     * @return План приема имеющих особые права при поступлении по договору.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionExt#getHaveSpecialRightsPlanContract()
     */
    public static PropertyPath<Integer> haveSpecialRightsPlanContract()
    {
        return _dslPath.haveSpecialRightsPlanContract();
    }

    /**
     * @return Дополнительный план приема на бюджет.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionExt#getAdvansedPlanBudget()
     */
    public static PropertyPath<Integer> advansedPlanBudget()
    {
        return _dslPath.advansedPlanBudget();
    }

    public static class Path<E extends EnrollmentDirectionExt> extends EntityPath<E>
    {
        private EnrollmentDirection.Path<EnrollmentDirection> _enrollmentDirection;
        private PropertyPath<Integer> _haveSpecialRightsPlanBudget;
        private PropertyPath<Integer> _haveSpecialRightsPlanContract;
        private PropertyPath<Integer> _advansedPlanBudget;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Направление подготовки (специальность) приема. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionExt#getEnrollmentDirection()
     */
        public EnrollmentDirection.Path<EnrollmentDirection> enrollmentDirection()
        {
            if(_enrollmentDirection == null )
                _enrollmentDirection = new EnrollmentDirection.Path<EnrollmentDirection>(L_ENROLLMENT_DIRECTION, this);
            return _enrollmentDirection;
        }

    /**
     * @return План приема имеющих особые права при поступлении по бюджету.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionExt#getHaveSpecialRightsPlanBudget()
     */
        public PropertyPath<Integer> haveSpecialRightsPlanBudget()
        {
            if(_haveSpecialRightsPlanBudget == null )
                _haveSpecialRightsPlanBudget = new PropertyPath<Integer>(EnrollmentDirectionExtGen.P_HAVE_SPECIAL_RIGHTS_PLAN_BUDGET, this);
            return _haveSpecialRightsPlanBudget;
        }

    /**
     * @return План приема имеющих особые права при поступлении по договору.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionExt#getHaveSpecialRightsPlanContract()
     */
        public PropertyPath<Integer> haveSpecialRightsPlanContract()
        {
            if(_haveSpecialRightsPlanContract == null )
                _haveSpecialRightsPlanContract = new PropertyPath<Integer>(EnrollmentDirectionExtGen.P_HAVE_SPECIAL_RIGHTS_PLAN_CONTRACT, this);
            return _haveSpecialRightsPlanContract;
        }

    /**
     * @return Дополнительный план приема на бюджет.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionExt#getAdvansedPlanBudget()
     */
        public PropertyPath<Integer> advansedPlanBudget()
        {
            if(_advansedPlanBudget == null )
                _advansedPlanBudget = new PropertyPath<Integer>(EnrollmentDirectionExtGen.P_ADVANSED_PLAN_BUDGET, this);
            return _advansedPlanBudget;
        }

        public Class getEntityClass()
        {
            return EnrollmentDirectionExt.class;
        }

        public String getEntityName()
        {
            return "enrollmentDirectionExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
