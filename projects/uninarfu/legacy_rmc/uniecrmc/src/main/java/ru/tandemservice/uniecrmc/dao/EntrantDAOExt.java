package ru.tandemservice.uniecrmc.dao;

// vch  декомпилировано в рамках задачи 6280
// 2014/06/17

import org.hibernate.Session;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uniecrmc.entity.entrant.Discipline2RealizationWayRelationExt;
import ru.tandemservice.uniecrmc.util.EntrantDataUtilRmc;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniec.dao.EntrantDAO;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.examset.EnrollmentDirectionExamSetData;
import ru.tandemservice.uniec.entity.examset.ExamSetStructure;
import ru.tandemservice.uniec.entity.examset.ExamSetStructureItem;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.util.ExamSetUtil;
import ru.tandemservice.uniec.util.MarkDistributionUtil;
import ru.tandemservice.uniec.util.MarkStatistic;

import java.util.*;

public class EntrantDAOExt
        extends EntrantDAO
{

    /**
     * результат декомпиляции
     */
    @Override
    public void updateEntrantData(Collection<Long> entrantIds)
    {
        Map<EnrollmentCampaign, Set<Entrant>> campaign2entrantSet = new Hashtable<>();
        for (Long entrantId : entrantIds) {
            Entrant entrant = (Entrant) getSession().get(Entrant.class, entrantId);
            if (entrant != null) {
                Set<Entrant> set = (Set<Entrant>) campaign2entrantSet.get(entrant.getEnrollmentCampaign());
                if (set == null)
                    campaign2entrantSet.put(entrant.getEnrollmentCampaign(), set = new HashSet<>());
                set.add(entrant);
            }
        }

        for (Map.Entry<EnrollmentCampaign, Set<Entrant>> entry : campaign2entrantSet.entrySet()) {
            final EnrollmentCampaign enrollmentCampaign = (EnrollmentCampaign) entry.getKey();
            Set<Entrant> val = entry.getValue();
            BatchUtils.execute(val, 1000, new BatchUtils.Action<Entrant>() {
                public void execute(Collection<Entrant> elements)
                {
                    updateEntrantDataFast(enrollmentCampaign, elements);
                }
            });
        }
    }


    private void updateEntrantDataFast(EnrollmentCampaign enrollmentCampaign, Collection<Entrant> entrants)
    {
        Session session = getSession();
        // session.clear();

        // чистим кеш
        clearCache();

        MQBuilder builder = new MQBuilder("ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection", "r");
        builder.addJoinFetch("r", "enrollmentDirection", "direction");
        builder.addJoinFetch("r", "entrantRequest", "request");
        builder.add(MQExpression.in("r", "entrantRequest.entrant", entrants));

        EntrantDataUtilRmc dataUtil = new EntrantDataUtilRmc(session, enrollmentCampaign, builder, EntrantDataUtilRmc.DetailLevel.EXAM_PASS_WITH_MARK_STATISTICS);
        //int DISCIPLINE_SOURCE_INDEX = 0;
        //int APPEAL_SOURCE_INDEX = 1;
        Map<String, int[]> form2markSource = new Hashtable<>();
        form2markSource.put("3", new int[]{2, 3});
        form2markSource.put("2", new int[]{4, 5});
        form2markSource.put("4", new int[]{6, 7});
        form2markSource.put("5", new int[]{8, 9});

        Map<EnrollmentDirection, EnrollmentDirectionExamSetData> direction2examSetDataMap = ExamSetUtil.getDirectionExamSetDataMap(getSession(), enrollmentCampaign, null, null);
        double NO_FINAL_MARK = -1.0D;
        for (RequestedEnrollmentDirection direction : dataUtil.getDirectionSet()) {
            for (ChosenEntranceDiscipline chosen : dataUtil.getChosenEntranceDisciplineSet(direction)) {
                double finalMark = NO_FINAL_MARK;
                Integer finalMarkSource = null;

                MarkStatistic markStatistic = dataUtil.getMarkStatistic(chosen);

                if ((markStatistic.getScaledStateExamMark() != null) && (markStatistic.getScaledStateExamMark().doubleValue() > finalMark)) {
                    finalMark = markStatistic.getScaledStateExamMark().doubleValue();
                    String waveCode = markStatistic.getStateExamMark().getCertificate().getStateExamType().getCode();
                    finalMarkSource = Integer.valueOf("1".equals(waveCode) ? 10 : 11);
                }

                if ((markStatistic.getOlympiad() != null) && (markStatistic.getOlympiad().intValue() > finalMark)) {
                    finalMark = markStatistic.getOlympiad().intValue();
                    finalMarkSource = Integer.valueOf(1);
                }

                for (Map.Entry<SubjectPassForm, PairKey<ExamPassMark, ExamPassMarkAppeal>> entry : markStatistic.getInternalMarkMap().entrySet()) {
                    PairKey<ExamPassMark, ExamPassMarkAppeal> pairMark = entry.getValue();

                    if (pairMark != null) {
                        if (pairMark.getSecond() != null) {
                            if (((ExamPassMarkAppeal) pairMark.getSecond()).getMark() > finalMark) {
                                finalMark = ((ExamPassMarkAppeal) pairMark.getSecond()).getMark();
                                finalMarkSource = Integer.valueOf(((int[]) form2markSource.get(((ru.tandemservice.uniec.entity.catalog.SubjectPassForm) entry.getKey()).getCode()))[1]);
                            }

                        }
                        else if (((ExamPassMark) pairMark.getFirst()).getMark() > finalMark) {
                            finalMark = ((ExamPassMark) pairMark.getFirst()).getMark();
                            finalMarkSource = Integer.valueOf(((int[]) form2markSource.get(((ru.tandemservice.uniec.entity.catalog.SubjectPassForm) entry.getKey()).getCode()))[0]);
                        }

                    }

                }

                chosen.setFinalMark(finalMark == NO_FINAL_MARK ? null : Double.valueOf(finalMark));
                chosen.setFinalMarkSource(finalMarkSource);
                session.update(chosen);
            }

            EnrollmentDirectionExamSetData examSetData = (EnrollmentDirectionExamSetData) direction2examSetDataMap.get(direction.getEnrollmentDirection());

            ExamSetStructure examSetStructure = examSetData.getCategoryExamSetStructure(direction.getStudentCategory());

            List<ExamSetStructureItem> itemList = examSetStructure.getItemList();

            int i = 0;
            while ((i < itemList.size()) && (!((ExamSetStructureItem) itemList.get(i)).isProfile())) i++;
            if (i < itemList.size()) {
                ChosenEntranceDiscipline[] distribution = MarkDistributionUtil.getChosenDistribution(dataUtil.getChosenEntranceDisciplineSet(direction), itemList);
                direction.setProfileChosenEntranceDiscipline(distribution[i]);
                update(direction);
            }

        }

        MQBuilder originalBuilder = new MQBuilder("ru.tandemservice.uniec.entity.entrant.EntrantOriginalDocumentRelation", "o");
        originalBuilder.add(MQExpression.in("o", "requestedEnrollmentDirection", builder));
        List<EntrantOriginalDocumentRelation> lst = originalBuilder.getResultList(session);

        Map<Entrant, RequestedEnrollmentDirection> originalMap = new Hashtable<>();
        for (EntrantOriginalDocumentRelation item : lst) {
            originalMap.put(item.getEntrant(), item.getRequestedEnrollmentDirection());
        }

        Set<EntrantRequest> awayEntrantRequestSet = new HashSet<>();
        for (RequestedEnrollmentDirection direction : dataUtil.getDirectionSet()) {
            if (direction.getEntrantRequest().isTakeAwayDocument())
                awayEntrantRequestSet.add(direction.getEntrantRequest());
        }
        for (RequestedEnrollmentDirection direction : dataUtil.getDirectionSet()) {
            EntrantRequest entrantRequest = direction.getEntrantRequest();
            Entrant entrant = entrantRequest.getEntrant();

            if (awayEntrantRequestSet.contains(entrantRequest)) {
                direction.setOriginalDocumentHandedIn(false);
            }
            else {
                RequestedEnrollmentDirection original = (RequestedEnrollmentDirection) originalMap.get(entrant);
                if (original == null) {
                    direction.setOriginalDocumentHandedIn(false);
                }
                else if (enrollmentCampaign.isOriginalDocumentPerDirection()) {
                    CompetitionGroup competitionGroup = original.getEnrollmentDirection().getCompetitionGroup();
                    if (competitionGroup == null) {
                        direction.setOriginalDocumentHandedIn(direction.equals(original));
                    }
                    else
                        direction.setOriginalDocumentHandedIn((competitionGroup.equals(direction.getEnrollmentDirection().getCompetitionGroup())) && (original.getCompensationType().equals(direction.getCompensationType())));
                }
                else {
                    direction.setOriginalDocumentHandedIn(original.getEntrantRequest().equals(direction.getEntrantRequest()));
                }

            }

        }

        Map<String, EntrantState> code2state = new Hashtable<>();
        EntrantState state;
        for (Iterator<EntrantState> i$ = getCatalogItemList(EntrantState.class).iterator(); i$.hasNext(); code2state.put(state.getCode(), state)) state = (EntrantState) i$.next();

        Set<RequestedEnrollmentDirection> preEnrolledSet = new HashSet<>();
        Map<RequestedEnrollmentDirection, String> direction2extractState = new Hashtable<>();
        Map<RequestedEnrollmentDirection, InterviewResult> direction2interview = new Hashtable<>();

        MQBuilder preEnrolledBuilder = new MQBuilder("ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent", "p", new String[]{"requestedEnrollmentDirection"});
        preEnrolledBuilder.add(MQExpression.in("p", "requestedEnrollmentDirection", builder));
        List<RequestedEnrollmentDirection> lstRED = preEnrolledBuilder.getResultList(session);
        preEnrolledSet.addAll(lstRED);

        MQBuilder extractBuilder = new MQBuilder("ru.tandemservice.uniec.entity.orders.EnrollmentExtract", "e", new String[]{"entity.requestedEnrollmentDirection", "state.code"});
        extractBuilder.add(MQExpression.in("e", "entity.requestedEnrollmentDirection", builder));
        List<Object[]> listR2s = extractBuilder.getResultList(session);

        for (Object[] row : listR2s) {
            direction2extractState.put((RequestedEnrollmentDirection) row[0], (String) row[1]);
        }

        MQBuilder interviewBuilder = new MQBuilder("ru.tandemservice.uniec.entity.entrant.InterviewResult", "i");
        interviewBuilder.add(MQExpression.in("i", "requestedEnrollmentDirection", builder));
        List<InterviewResult> lstI = interviewBuilder.getResultList(session);
        for (InterviewResult interview : lstI) {
            direction2interview.put(interview.getRequestedEnrollmentDirection(), interview);
        }

        for (RequestedEnrollmentDirection direction : dataUtil.getDirectionSet()) {

            // признак - забрал документы
            if (direction.getEntrantRequest().isTakeAwayDocument()) {
                direction.setState((EntrantState) code2state.get("7"));
            }
            else {
                String stateCode = (String) direction2extractState.get(direction);

                // не совсем это верно, но Базжина согласовала с горным
                if (direction.getState() != null && "7".equals(direction.getState().getCode())) // забрал документы
                {
                    // ничего не делать!!! задрал документы, можно и рейтинг не считать
                }
                // ничего не делать!!! задрал документы, можно и рейтинг не считать
                else if ((stateCode != null)) {
                    // есть в выписках
                    boolean finished = "6".equals(stateCode);
                    direction.setState((EntrantState) code2state.get(finished ? "3" : "6"));
                }
                else if (preEnrolledSet.contains(direction)) {
                    // есть в списках пред зачисленных
                    direction.setState((EntrantState) code2state.get("5"));
                }
                else if ("1".equals(direction.getCompetitionKind().getCode())) {
                    direction.setState((EntrantState) code2state.get("4"));
                }
                else if ("2".equals(direction.getCompetitionKind().getCode())) {
                    InterviewResult interview = (InterviewResult) direction2interview.get(direction);
                    if (interview == null)
                        direction.setState((EntrantState) code2state.get("1"));
                    else
                        direction.setState((EntrantState) code2state.get(interview.isPassed() ? "4" : "2"));
                }
                else {
                    Set<ChosenEntranceDiscipline> chosenSet = dataUtil.getChosenEntranceDisciplineSet(direction);
                    if (chosenSet.isEmpty()) {
                        direction.setState((EntrantState) code2state.get("1"));
                    }
                    else {
                        boolean hasBadMark = false;
                        boolean hasNoMark = false;

                        for (ChosenEntranceDiscipline chosen : chosenSet) {
                            Double finalMark = chosen.getFinalMark();
                            if (finalMark == null) {
                                hasNoMark = true;
                            }
                            else if (finalMark.doubleValue() < getPassMarkForDiscipline(chosen, direction)) {
                                hasBadMark = true;
                            }
                        }

                        if (hasBadMark) {
                            direction.setState((EntrantState) code2state.get("2"));
                        }
                        else if (hasNoMark) {
                            direction.setState((EntrantState) code2state.get("1"));
                        }
                        else {
                            direction.setState((EntrantState) code2state.get("4"));
                        }
                    }
                }
            }
            session.update(direction);
        }

        Map<Entrant, EntrantState> entrant2state = new HashMap<>();
        for (Entrant entrant : entrants) {
            entrant2state.put(entrant, null);
        }
        for (RequestedEnrollmentDirection direction : dataUtil.getDirectionSet()) {
            Entrant entrant = direction.getEntrantRequest().getEntrant();
            EntrantState entrantState = (EntrantState) entrant2state.get(entrant);
            EntrantState directionState = direction.getState();

            if ((entrantState == null) || (directionState.getPriority() < entrantState.getPriority())) {
                entrant2state.put(entrant, directionState);
            }
        }
        for (Map.Entry<Entrant, EntrantState> entry : entrant2state.entrySet()) {
            ((Entrant) entry.getKey()).setState((EntrantState) entry.getValue());
            session.update(entry.getKey());
        }

        DQLSelectBuilder preStudentBuilder = new DQLSelectBuilder();
        preStudentBuilder.fromEntity(PreliminaryEnrollmentStudent.class, "preStudent");
        preStudentBuilder.joinPath(DQLJoinType.inner, "preStudent.requestedEnrollmentDirection", "direction");
        preStudentBuilder.joinPath(DQLJoinType.inner, "direction.entrantRequest", "request");
        preStudentBuilder.joinPath(DQLJoinType.inner, "request.entrant", "entrant");
        preStudentBuilder.joinEntity("entrant", DQLJoinType.left, Student.class, "student", "entrant.person=student.person");
        preStudentBuilder.where(DQLExpressions.in(DQLExpressions.property("request.entrant"), entrants));
        preStudentBuilder.column("entrant.id", "entrantId");
        preStudentBuilder.column("student", "student");
        preStudentBuilder.column("preStudent", "preStudent");

        Set<Long> entrantParallelIds = new HashSet<>();
        Map<Long, List<PreliminaryEnrollmentStudent>> entrantId2preStudentList = new Hashtable<>();
        List<Object[]> dataList = preStudentBuilder.createStatement(new DQLExecutionContext(session)).list();

        for (Object[] row : dataList) {
            Long entrantId = Long.valueOf(((Number) row[0]).longValue());
            Student student = (Student) row[1];
            PreliminaryEnrollmentStudent preStudent = (PreliminaryEnrollmentStudent) row[2];

            String studentCategoryCode = student == null ? null : student.getStudentCategory().getCode();
            String preStudentCategoryCode = preStudent.getStudentCategory().getCode();

            if (("1".equals(studentCategoryCode)) || ("1".equals(preStudentCategoryCode))) {
                entrantParallelIds.add(entrantId);
            }
            List<PreliminaryEnrollmentStudent> preStudentList = entrantId2preStudentList.get(entrantId);
            if (preStudentList == null)
                entrantId2preStudentList.put(entrantId, preStudentList = new ArrayList<>());
            preStudentList.add(preStudent);
        }

        boolean parallel;
        for (Map.Entry<Long, List<PreliminaryEnrollmentStudent>> entry : entrantId2preStudentList.entrySet()) {
            Long entrantId = (Long) entry.getKey();
            parallel = entrantParallelIds.contains(entrantId);
            for (PreliminaryEnrollmentStudent preStudent : entry.getValue()) {
                if ("2".equals(preStudent.getStudentCategory().getCode())) {
                    preStudent.setParallel((parallel) || (preStudent.isParallelLocked()));
                }
                else {
                    preStudent.setParallel(false);
                    preStudent.setParallelLocked(false);
                }
                session.update(preStudent);
            }
        }
    }

    @Override
    protected double getPassMarkForDiscipline
            (
                    ChosenEntranceDiscipline chosen,
                    RequestedEnrollmentDirection direction
            )
    {
        Discipline2RealizationWayRelation ecd = chosen.getEnrollmentCampaignDiscipline();
        double retVal = ecd.getPassMark();
        if (direction.getCompensationType().getCode().equals(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET))
            return retVal;
        else {
            // по договору другой балл
            Discipline2RealizationWayRelationExt ext = getExtD2RW(ecd);
            if (ext != null)
                retVal = ext.getPassMarkContract();

            return retVal;
        }

    }

    private Map<Discipline2RealizationWayRelation, Discipline2RealizationWayRelationExt> mapD2RWExt = new HashMap<>();

    protected Discipline2RealizationWayRelationExt getExtD2RW(Discipline2RealizationWayRelation d2rw)
    {
        if (mapD2RWExt.containsKey(d2rw))
            return mapD2RWExt.get(d2rw);
        else {
            Discipline2RealizationWayRelationExt retVal = get(Discipline2RealizationWayRelationExt.class, Discipline2RealizationWayRelationExt.L_DISCIPLINE2_REALIZATION_WAY_RELATION, d2rw);
            mapD2RWExt.put(d2rw, retVal);
            return retVal;
        }
    }

    protected void clearCache()
    {
        mapD2RWExt = new HashMap<>();
    }
}
