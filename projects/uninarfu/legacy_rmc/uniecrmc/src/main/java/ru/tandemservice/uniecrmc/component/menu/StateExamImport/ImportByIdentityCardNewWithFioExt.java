package ru.tandemservice.uniecrmc.component.menu.StateExamImport;

import jxl.Workbook;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.*;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.tools.PrivateAccessor;
import ru.tandemservice.uniec.component.menu.StateExamImport.ImportByIdentityCardNewWithFio;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.catalog.StateExamType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate;
import ru.tandemservice.uniec.entity.entrant.StateExamSubjectMark;
import ru.tandemservice.uniec.ui.EntrantStateExamCertificateNumberFormatter;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.regex.Pattern;

public class ImportByIdentityCardNewWithFioExt extends ImportByIdentityCardNewWithFio {
    private static final String SEPARATOR = "%";

    private static final String FILE_CHARSET = "Windows-1251";

    private static final String COMMENT_ROW_PREFIX = "Комментарий:";
    private static final Pattern VALUE_ERROR_PATTERN = Pattern.compile("ОШИБКА([0-9,. ]*)[(]([0-9,. ]*)[)]");

    private static final String STATUS_GOOD = "ДЕЙСТВУЮЩИЙ";
    private static final String STATUS_LESS_MINUMUM = "НИЖЕ МИНИМУМА";
    private static final String STATUS_NOT_FOUND = "НЕ НАЙДЕНО";
    private static final String STATUS_ANNUL_WITH_RETAKE = "АННУЛИРОВАН С ПРАВОМ ПЕРЕСДАЧИ";
    private static final String STATUS_ANNUL_NO_RETAKE = "АННУЛИРОВАН БЕЗ ПРАВА ПЕРЕСДАЧИ";
    private static final String STATUS_EXPIRED = "ИСТЕК СРОК";

    private static final String REPLY_ENTRANT_NOT_FOUND = "АБИТУРИЕНТА НЕТ В СИСТЕМЕ";
    private static final String REPLY_ENTRANT_CERT_DOUBLED = "ДУБЛЬ СВИДЕТЕЛЬСТВА";
    private static final String REPLY_FALSE = "ЛОЖНЫЕ СВИДЕТЕЛЬСТВА";
    private static final String REPLY_CERT_ADDED = "СОЗДАНО НОВОЕ";
    private static final String REPLY_MARKS_ADDED = "ДОБАВЛЕНЫ БАЛЛЫ";
    private static final String REPLY_MARKS_CHANGED = "ИЗМЕНЕНЫ БАЛЛЫ";
    private static final String REPLY_MARKS_DELETED = "УДАЛЕНЫ БАЛЛЫ";
    private static final String REPLY_APPROVED = "ПРОВЕРЕНО";
    private static final String REPLY_QUESTIONED = "ПОД ВОПРОСОМ";
    private static final String REPLY_DELETED = "УДАЛЕНО";
    private static final String REPLY_LOSER = "ДВОЕЧНИК";

    /**
     * Индексы столбцов в новом формате импортируемого файла
     */
    private static final int LAST_NAME_IDX = 0;
    private static final int FIRST_NAME_IDX = 1;
    private static final int MIDDLE_NAME_IDX = 2;
    private static final int ID_CARD_SERIA_IDX = 3;
    private static final int ID_CARD_NUMBER_IDX = 4;
    private static final int SUBJECT_NAME_IDX = 5;
    private static final int MARK_VALUE_IDX = 6;
    private static final int APPELATION_IDX = 10;

    private static final int STATUS_IDX = 9;
    private static final int CERT_NUM_IDX = 11;
    private static final int LAST_CELL_IDX = 12;

    private static final String FIRST_WAVE_CODE = "1";


    private static final String ZERO_NUMBER = "000000000";


    @Override
    public byte[] doExecuteImportByIdentityCard(EnrollmentCampaign enrollmentCampaign, final boolean deleteNotFound, byte[] importedContent) {
        try {
            final Session session = getSession();

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            WritableWorkbook workbook = Workbook.createWorkbook(out);

            WritableSheet sheet = workbook.createSheet("Результат импорта", 0);
            sheet.setColumnView(LAST_NAME_IDX, 16);
            sheet.setColumnView(FIRST_NAME_IDX, 16);
            sheet.setColumnView(MIDDLE_NAME_IDX, 16);
            sheet.setColumnView(SUBJECT_NAME_IDX, 16);
            sheet.setColumnView(STATUS_IDX, 16);

            WritableCellFormat FORMAT_RED = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 10, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.RED));

            StateExamType firstWave = get(StateExamType.class, StateExamType.code().s(), FIRST_WAVE_CODE);
            List<StateExamSubject> subjects = getCatalogItemListOrderByCode(StateExamSubject.class);
            Map<String, StateExamSubject> subjectsByNameMap = getSubjectsMapByName(subjects);
            Map<MultiKey, Long> entrantMap = new HashMap<>();
            final Map<Long, String[]> entrantDataMap = new HashMap<>();
            Map<Long, Map<String, Set<Long>>> certificateMap = new HashMap<>();
            Map<MultiKey, PairKey<Long, Integer>> markMap = new HashMap<>();
            Set<MultiKey> doubledPersons = new HashSet<>();
            PrivateAccessor.invokePrivateMethod(this, ImportByIdentityCardNewWithFio.class, "prepareCertificateData", new Object[]{enrollmentCampaign, entrantMap, entrantDataMap, certificateMap, markMap, doubledPersons});
            //в entrantDataMap не всё попало, прошерстим еще раз entrantMap:
            for (MultiKey entrantKey : entrantMap.keySet()) {
                Long entrantId = entrantMap.get(entrantKey);
                if (!entrantDataMap.containsKey(entrantId)) {
                    entrantDataMap.put(entrantId, new String[]{(String) entrantKey.getKey(2), "", "", (String) entrantKey.getKey(0), (String) entrantKey.getKey(1)});
                }
            }

            Set<Long> checkedEntrantIds = new HashSet<>();
            final Set<Long> statusNotFoundEntrantIds = new HashSet<>();
            Set<Long> approvedCertificateIds = new HashSet<>();
            Set<MultiKey> uniqCertNumberSubjectSet = new HashSet<>();

            int rownum = 1;
            try {
                BufferedReader in = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(importedContent), FILE_CHARSET));
                String[] headerItems = new String[]{"Фамилия", "Ммя", "Отчество", "Серия документа", "Номер документа", "Предмет", "Балл", "Год", "Регион", "Аппеляция", "Номер свидетельства", "Типографский номер"};
                fillRow(sheet, headerItems, 0, null);

                for (; ; ) {
                    String line = in.readLine();
                    if (line == null)
                        break;
                    if (line.startsWith(COMMENT_ROW_PREFIX)) {
                        sheet.addCell(new Label(0, rownum++, line));
                    }
                    else if (line.length() != 0) {
                        String[] row = line.split(SEPARATOR);

                        String lastName = row[LAST_NAME_IDX].toLowerCase().replace('ё', 'е');
                        String idcSeria = row[ID_CARD_SERIA_IDX];
                        String idcNumber = row[ID_CARD_NUMBER_IDX];
                        String status = row[STATUS_IDX].toUpperCase();
                        String subjectName = row[SUBJECT_NAME_IDX];
                        Integer actualMarkValue = Integer.parseInt(row[MARK_VALUE_IDX].replace(" ", ""));

                        if (!StringUtils.isEmpty(idcSeria))
                            idcSeria = String.format("%4s", idcSeria).replaceAll(" ", "0");
                        if (!StringUtils.isEmpty(idcNumber))
                            idcNumber = String.format("%6s", idcNumber).replaceAll(" ", "0");

                        String certNumber = (row.length > CERT_NUM_IDX) ? row[CERT_NUM_IDX].replaceAll("-", "") : "";
                        if (StringUtils.isEmpty(certNumber)) {
                            certNumber = "00" + ZERO_NUMBER + "00";
                        }
                        MultiKey entrantKey = new MultiKey(idcSeria, idcNumber, lastName);

                        if (doubledPersons.contains(entrantKey)) {
                            fillRow(sheet, row, rownum, FORMAT_RED);
                            ((Label) sheet.getCell(STATUS_IDX, rownum++)).setString(REPLY_ENTRANT_CERT_DOUBLED);
                        }
                        else {
                            Long entrantId = entrantMap.get(entrantKey);

                            if (null == entrantId) {
                                fillRow(sheet, row, rownum, FORMAT_RED);
                                ((Label) sheet.getCell(STATUS_IDX, rownum++)).setString(REPLY_ENTRANT_NOT_FOUND);
                            }
                            else {
                                checkedEntrantIds.add(entrantId);

                                if (STATUS_NOT_FOUND.equalsIgnoreCase(status)) {
                                    if (certificateMap.containsKey(entrantId)) {
                                        //всем свидетельствам ставится статус "ложные свидетельства"
                                        statusNotFoundEntrantIds.add(entrantId);
                                        Map<String, Set<Long>> entrantCertificateMap = certificateMap.get(entrantId);
                                        String[] entrantData = entrantDataMap.get(entrantId);
                                        if ((null != entrantCertificateMap) && (!entrantCertificateMap.isEmpty())) {
                                            List<String> certificateNumbers = new ArrayList<>(entrantCertificateMap.keySet());
                                            Collections.sort(certificateNumbers);

                                            for (String certNumberEntrant : certificateNumbers) {
                                                for (Long certificateId : entrantCertificateMap.get(certNumberEntrant)) {
                                                    printCertificateRow(sheet, rownum, certificateId, certNumberEntrant, null, null, entrantData, REPLY_FALSE);
                                                    for (int i = 0; i <= LAST_CELL_IDX; i++) {
                                                        if (Label.class.isInstance(sheet.getCell(i, rownum)))
                                                            ((Label) sheet.getCell(i, rownum)).setCellFormat(FORMAT_RED);
                                                    }
                                                    rownum++;
                                                }
                                            }
                                        }
                                    }
                                    else {
                                        fillRow(sheet, row, rownum++, null);
                                    }
                                }
                                else {
                                    if (!STATUS_GOOD.equalsIgnoreCase(status)) {
                                        fillRow(sheet, row, rownum++, null);
                                    }
                                    else {
                                        MultiKey certNumberSubjectKey = new MultiKey(idcSeria, idcNumber, lastName, certNumber, subjectName);
                                        if (!uniqCertNumberSubjectSet.add(certNumberSubjectKey)) {
                                            throw new ApplicationException(
                                                    "Невозможно импортировать файл, т.к. в свидетельстве с номером " +
                                                            new EntrantStateExamCertificateNumberFormatter().format(certNumber) +
                                                            " запись по предмету " +
                                                            subjectName +
                                                            " дублируется.");
                                        }

                                        Set<Long> certIds = certificateMap.containsKey(entrantId) ? certificateMap.get(entrantId).get(certNumber) : null;

                                        if (null != certIds) {
                                            //есть такой сертификат в БД, надо апдейтить его, из certificateMap его не нужно удалять(в отличие от старой версии):
                                            certificateMap.get(entrantId);
                                            boolean markChanged = false;
                                            boolean markDeleted = false;
                                            boolean markAdded = false;
                                            if (certIds.size() != 1) {
                                                throw new ApplicationException(
                                                        "Невозможно импортировать файл, т.к. у абитуриента " +
                                                                get(Entrant.class, entrantId).getPerson().getFullFio() +
                                                                " свидетельство с номером " +
                                                                get(EntrantStateExamCertificate.class, certIds.iterator().next()).getTitle() +
                                                                " дублируется в базе данных. Удалите дубликаты и повторите процедуру импорта."
                                                );
                                            }

                                            for (Long certId : certIds) {
                                                approvedCertificateIds.add(certId);
                                                StateExamSubject subject = subjectsByNameMap.get(subjectName);
                                                if (subject == null) {
                                                    throw new ApplicationException("Невозможно импортировать файл, т.к. дисциплины " + subjectName + " нет в системе");
                                                }
                                                MultiKey markKey = new MultiKey(certId, subject.getCode());
                                                PairKey<Long, Integer> markData = markMap.get(markKey);

                                                if ((markData == null) && (actualMarkValue != 0)) {
                                                    StateExamSubjectMark mark = new StateExamSubjectMark();
                                                    mark.setCertificate((EntrantStateExamCertificate) session.load(EntrantStateExamCertificate.class, certId));
                                                    mark.setSubject(subject);
                                                    mark.setMark(actualMarkValue);
                                                    session.save(mark);
                                                    markAdded = true;

                                                }
                                                else if ((markData != null) && (actualMarkValue == 0)) {
                                                    StateExamSubjectMark mark = get(StateExamSubjectMark.class, markData.getFirst());
                                                    session.delete(mark);
                                                    markDeleted = true;

                                                }
                                                else if ((markData != null) && ((markData.getSecond()).intValue() != actualMarkValue)) {
                                                    StateExamSubjectMark mark = get(StateExamSubjectMark.class, markData.getFirst());
                                                    mark.setMark(actualMarkValue);
                                                    session.update(mark);
                                                    markChanged = true;
                                                }

                                            }
                                            fillRow(sheet, row, rownum, null);

                                            Collection<String> replies = new ArrayList<>(4);
                                            if (markAdded) replies.add(REPLY_MARKS_ADDED);
                                            if (markChanged) replies.add(REPLY_MARKS_CHANGED);
                                            if (markDeleted) replies.add(REPLY_MARKS_DELETED);
                                            ((Label) sheet.getCell(STATUS_IDX, rownum++)).setString(replies.isEmpty() ? REPLY_APPROVED : StringUtils.join(replies, ", "));

                                        }
                                        else {
                                            //нет такого сертификата в БД, надо новый создавать
                                            EntrantStateExamCertificate certificate = new EntrantStateExamCertificate();
                                            certificate.setEntrant(get(Entrant.class, entrantId));
                                            certificate.setNumber(certNumber);
                                            certificate.setAccepted(true);
                                            certificate.setSent(true);
                                            certificate.setRegistrationDate(new Date());
                                            certificate.setStateExamType(firstWave);
                                            session.save(certificate);
                                            StateExamSubject subject = subjectsByNameMap.get(subjectName);
                                            if (subject != null && actualMarkValue != 0) {
                                                StateExamSubjectMark mark = new StateExamSubjectMark();
                                                mark.setCertificate(certificate);
                                                mark.setSubject(subject);
                                                mark.setMark(actualMarkValue);
                                                session.save(mark);
                                            }

                                            fillRow(sheet, row, rownum, null);
                                            ((Label) sheet.getCell(STATUS_IDX, rownum++)).setString(REPLY_CERT_ADDED);

                                            Map<String, Set<Long>> entrantCertMap = certificateMap.get(entrantId);
                                            if (entrantCertMap == null) {
                                                entrantCertMap = new HashMap<>();
                                                certificateMap.put(entrantId, entrantCertMap);
                                            }

                                            Set<Long> entrantIds = entrantCertMap.get(certNumber);
                                            if (entrantIds == null) {
                                                entrantIds = new HashSet<>();
                                                entrantCertMap.put(certNumber, entrantIds);
                                            }

                                            entrantIds.add(certificate.getId());
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (ApplicationException e) {
                throw e;
            }
            catch (Exception e) {
                logger.error(e.getMessage(), e);
                if (Debug.isDisplay()) throw e;
                throw new ApplicationException("Формат файла некорректен, загрузка данных невозможна.");
            }

            //проведем апдейт подтвержденных сертификатов:
            BatchUtils.execute(approvedCertificateIds, 500, new BatchUtils.Action<Long>() {
                public void execute(Collection<Long> elements) {
                    MQBuilder builder = new MQBuilder(EntrantStateExamCertificate.class.getName(), "cert");
                    builder.add(MQExpression.in("cert", EntrantStateExamCertificate.id().s(), elements));
                    List<EntrantStateExamCertificate> certificates = builder.getResultList(session);
                    for (EntrantStateExamCertificate certificate : certificates) {
                        certificate.setAccepted(true);
                        certificate.setSent(true);
                        session.update(certificate);
                    }
                }
            });

            //составим множество не принятых сертификатов:
            //проверим все сертификаты из certificateMap, которых нет в  approvedCertificateIds добавляем в not approved:
            Set<Long> notApprovedCertificateIds = new HashSet<>();
            Set<Long> sortedEntrantIdsSet = new HashSet<>();
            for (Long entrantId : checkedEntrantIds) {
                Map<String, Set<Long>> entrantCertificateMap = certificateMap.get(entrantId);
                if (null != entrantCertificateMap && !entrantCertificateMap.isEmpty()) {
                    for (String certNumber : entrantCertificateMap.keySet()) {
                        for (Long certId : entrantCertificateMap.get(certNumber)) {
                            if (!approvedCertificateIds.contains(certId)) {
                                notApprovedCertificateIds.add(certId);
                                sortedEntrantIdsSet.add(entrantId);
                            }
                        }
                    }
                }
            }

            //проведем апдейт не принятых сертификатов:
            BatchUtils.execute(notApprovedCertificateIds, 500, new BatchUtils.Action<Long>() {
                public void execute(Collection<Long> elements) {
                    MQBuilder builder = new MQBuilder(EntrantStateExamCertificate.class.getName(), "cert");
                    builder.add(MQExpression.in("cert", EntrantStateExamCertificate.id().s(), elements));
                    List<EntrantStateExamCertificate> certificates = builder.getResultList(session);
                    for (EntrantStateExamCertificate certificate : certificates) {
                        if (ImportByIdentityCardNewWithFioExt.this.doDeleteCertificate(deleteNotFound, certificate.getNumber(), certificate.getEntrant().getId(), statusNotFoundEntrantIds)) {
                            session.delete(certificate);
                        }
                        else {
                            certificate.setAccepted(false);
                            certificate.setSent(false);
                            session.update(certificate);
                        }
                    }
                }
            });
            List<Long> sortedEntrantIds = new ArrayList<>(sortedEntrantIdsSet);
            Collections.sort(sortedEntrantIds, new Comparator<Long>() {
                public int compare(Long o1, Long o2) {
                    return (entrantDataMap.get(o1))[0].compareTo((entrantDataMap.get(o2))[0]);
                }
            });

            //не принятые сертификаты запишем в дополнительные строки:
            for (Long entrantId : sortedEntrantIds) {
                Map<String, Set<Long>> entrantCertificateMap = certificateMap.get(entrantId);
                if ((null != entrantCertificateMap) && (!entrantCertificateMap.isEmpty())) {
                    String[] entrantData = entrantDataMap.get(entrantId);
                    List<String> certificateNumbers = new ArrayList<>(entrantCertificateMap.keySet());
                    Collections.sort(certificateNumbers);
                    for (String certNumber : certificateNumbers) {
                        if (!getActualNumber(certNumber).equals(ZERO_NUMBER))
                            for (Long certificateId : entrantCertificateMap.get(certNumber)) {
                                printCertificateRow(sheet, rownum++, certificateId, certNumber, null, null, entrantData, REPLY_QUESTIONED);
                            }
                    }
                }
            }

            //запищем в новые строки сертификаты с номером 000000 (ZERO_NUMBER)
            for (Long entrantId : sortedEntrantIds) {
                Map<String, Set<Long>> entrantCertificateMap = certificateMap.get(entrantId);
                if ((null != entrantCertificateMap) && (!entrantCertificateMap.isEmpty())) {
                    String[] entrantData = entrantDataMap.get(entrantId);
                    for (String certNumber : entrantCertificateMap.keySet()) {
                        if (getActualNumber(certNumber).equals(ZERO_NUMBER))
                            for (Long certificateId : entrantCertificateMap.get(certNumber)) {
                                printCertificateRow(
                                        sheet,
                                        rownum++,
                                        certificateId,
                                        certNumber,
                                        null,
                                        null,
                                        entrantData,
                                        doDeleteCertificate(deleteNotFound, certNumber, entrantId, statusNotFoundEntrantIds) ? REPLY_DELETED : REPLY_QUESTIONED
                                );
                            }
                    }
                }
            }

            workbook.write();
            workbook.close();
            return out.toByteArray();
        }
        catch (Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    private Map<String, StateExamSubject> getSubjectsMapByName(List<StateExamSubject> subjects) {
        Map<String, StateExamSubject> res = new HashMap<>();
        for (StateExamSubject subject : subjects) {
            res.put(subject.getTitle(), subject);
        }
        return res;
    }

    private static void fillRow(WritableSheet sheet, String[] row, int rownum, WritableCellFormat format) throws Exception {
        //вызываем статичный метод, поэтому передаем в первый аргумент null
        PrivateAccessor.invokePrivateMethod(null, ImportByIdentityCardNewWithFio.class, "fillRow", new Object[]{sheet, row, rownum, format});
    }

    private void printCertificateRow(
            WritableSheet sheet,
            int rownum,
            Long certificateId,
            String certNumber,
            StateExamSubject subject,
            Integer markValue,
            String[] entrantData,
            String status) throws jxl.write.WriteException
    {

        sheet.addCell(new Label(CERT_NUM_IDX, rownum, new EntrantStateExamCertificateNumberFormatter().format(certNumber)));
        //entrantData имеет вид: lastName, firstName, middleName, idcSeria, idcNumber
        for (int i = LAST_NAME_IDX; i < entrantData.length; i++) {
            sheet.addCell(new Label(i, rownum, entrantData[i]));
        }

        sheet.addCell(new Label(STATUS_IDX, rownum, status));
        if (markValue != null) {
            sheet.addCell(new Label(MARK_VALUE_IDX, rownum, markValue.toString()));
            sheet.addCell(new Label(APPELATION_IDX, rownum, "0"));
        }
        if (subject != null) {
            sheet.addCell(new Label(SUBJECT_NAME_IDX, rownum, subject.getTitle()));
        }
    }

    private boolean doDeleteCertificate(boolean deleteNotFound, String certificateNumber, Long entrantId, Set<Long> statusNotFoundEntrantIds) {
        return
                (deleteNotFound) &&
                        (getActualNumber(certificateNumber).equalsIgnoreCase(ZERO_NUMBER)) &&
                        (!statusNotFoundEntrantIds.contains(entrantId));
    }

    private String getActualNumber(String number) {
        return number.substring(2, 11);
    }

}
