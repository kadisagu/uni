package ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.AddEdit;

import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.EcDistributionExtManager;
import ru.tandemservice.uniec.base.bo.EcDistribution.EcDistributionManager;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistribution;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.Map;

public class EcDistributionAddEditUI extends ru.tandemservice.uniec.base.bo.EcDistribution.ui.AddEdit.EcDistributionAddEditUI {

    private Map<Long, Integer> srQuotaMap;

    @Override
    public void onComponentRefresh() {
        if (!isUsePriority()) {
            super.onComponentRefresh();
            return;
        }
        super.onComponentRefresh();
        if (this.isAddForm()) {
            this.srQuotaMap = EcDistributionExtManager.daoRMC().getNextSRQuota(this.getDistributionDTO().getConfigDTO());
        }
        else {
            EcgDistribution distribution = (EcgDistribution) DataAccessServices.dao().get(this.getDistributionDTO().getPersistentId());
            this.srQuotaMap = EcDistributionExtManager.daoRMC().getCurrentSRQuota(distribution);
        }
    }

    public boolean isUsePriority() {
        return EcDistributionExtManager.daoRMC().isUsePriority(this.getDistributionDTO().getEcgItem().getEnrollmentCampaign());
    }

    public int getCurrentQuotaSR() {
        Long id = ((EnrollmentDirection) getConfig().getDataSource("directionDS").getCurrent()).getId();
        Integer quota = (Integer) this.srQuotaMap.get(id);
        return quota == null ? 0 : quota.intValue();
    }

    public void setCurrentQuotaSR(int quota) {
        Long id = ((EnrollmentDirection) getConfig().getDataSource("directionDS").getCurrent()).getId();
        this.srQuotaMap.put(id, Integer.valueOf(quota));
    }

    public Map<Long, Integer> getSrQuotaMap() {
        return srQuotaMap;
    }

    public void setSrQuotaMap(Map<Long, Integer> srQuotaMap) {
        this.srQuotaMap = srQuotaMap;
    }

    @Override
    public void onClickApply() {
        if (!isUsePriority()) {
            super.onClickApply();
            return;
        }
        if (this.isAddForm()) {
            EcgDistribution distribution = EcDistributionManager.instance().dao().saveDistribution(this.getDistributionDTO().getConfigDTO(), this.getWave(), this.getQuotaMap(), this.getTaQuotaMap());
            EcDistributionExtManager.daoRMC().saveSRQuota(distribution.getId(), srQuotaMap);
        }
        else {
            EcDistributionManager.instance().dao().updateDistribution(this.getDistributionDTO().getPersistentId(), this.getQuotaMap(), this.getTaQuotaMap());
            EcDistributionExtManager.daoRMC().updateSRQuota(this.getDistributionDTO().getPersistentId(), srQuotaMap);
        }

        deactivate();
    }

}
