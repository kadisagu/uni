package ru.tandemservice.uniecrmc.component.entrant.EnrollmentDirectionList;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionExt;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniec.component.entrant.EnrollmentDirectionList.Model;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DAO extends ru.tandemservice.uniec.component.entrant.EnrollmentDirectionList.DAO {

    @Override
    public void prepareListDataSource(Model model) {
        super.prepareListDataSource(model);

        String okso = (String) model.getSettings().get("oksoNew");
        List<Long> ids = UniUtils.getIdList(model.getDataSource().getEntityList());

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrollmentDirectionExt.class, "ext")
                .where(DQLExpressions.in(DQLExpressions.property(EnrollmentDirectionExt.enrollmentDirection().id().fromAlias("ext")), ids));

        List<EnrollmentDirectionExt> lst = getList(builder);


        Map<Long, EnrollmentDirectionExt> map = new HashMap<Long, EnrollmentDirectionExt>();

        for (EnrollmentDirectionExt ext : lst) {
            map.put(ext.getEnrollmentDirection().getId(), ext);
        }
        if (okso != null) {
            DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "ed").column(DQLExpressions.property(EnrollmentDirection.id().fromAlias("ed")));
            FilterUtils.applySimpleLikeFilter(dql, "ed", EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().educationLevel().eduProgramSubject().subjectCode().s(), okso);
            List<Long> enrollmentDirectionList = getList(dql);
            List<EnrollmentDirection> toRemove = new ArrayList<>();
            for (EnrollmentDirection enrollmentDirection : model.getDataSource().getEntityList()) {
                if (!enrollmentDirectionList.contains(enrollmentDirection.getId())) {
                    toRemove.add(enrollmentDirection);
                }


            }
            model.getDataSource().getEntityList().removeAll(toRemove);
        }
        List<ViewWrapper<EnrollmentDirection>> patchedList = ViewWrapper.getPatchedList(model.getDataSource());

        for (ViewWrapper<EnrollmentDirection> wrapper : patchedList) {
            EnrollmentDirectionExt ext = map.get(wrapper.getId());

            Integer budget = null;
            Integer contract = null;
            Integer advansedPlanBudget = null;
            if (ext != null) {
                budget = ext.getHaveSpecialRightsPlanBudget();
                contract = ext.getHaveSpecialRightsPlanContract();
                advansedPlanBudget = ext.getAdvansedPlanBudget();
            }

            wrapper.setViewProperty("haveSpecialRightsPlanBudget", budget);
            wrapper.setViewProperty("haveSpecialRightsPlanContract", contract);
            wrapper.setViewProperty("advansedPlanBudget", advansedPlanBudget);
        }

    }
}
