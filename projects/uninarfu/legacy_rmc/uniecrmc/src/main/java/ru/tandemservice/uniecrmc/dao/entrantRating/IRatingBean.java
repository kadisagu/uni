package ru.tandemservice.uniecrmc.dao.entrantRating;


public interface IRatingBean {

    public void checkTime();

    public boolean isActive(Long enrollmentCampaignId);

    public void runNow(Long enrollmentCampaignId);
}
