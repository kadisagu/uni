package ru.tandemservice.uniecrmc.component.menu.EntrantRatingSettings;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniecrmc.dao.entrantRating.RatingBean;
import ru.tandemservice.uniecrmc.entity.entrant.EntrantRecommendedSettings;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
    }

    @Override
    public void prepareListDataSource(Model model) {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EntrantRecommendedSettings.class, "e");
        new DQLOrderDescriptionRegistry(EntrantRecommendedSettings.class, "e").applyOrder(dql, model.getDataSource().getEntityOrder());

        List<EntrantRecommendedSettings> list = dql.createStatement(getSession()).list();

        Map<Long, Long> mapInterval = new HashMap<Long, Long>();
        Map<Long, String> mapPath = new HashMap<Long, String>();
        Map<Long, EntrantRecommendedSettings> mapState = new HashMap<>();
        for (EntrantRecommendedSettings entity : list) {
            mapInterval.put(entity.getId(), (long) entity.getInterval());
            mapPath.put(entity.getId(), entity.getPath());
            mapState.put(entity.getId(), entity);
        }

        ((BlockColumn) model.getDataSource().getColumn("interval")).setValueMap(mapInterval);
        ((BlockColumn) model.getDataSource().getColumn("path")).setValueMap(mapPath);
        ((BlockColumn) model.getDataSource().getColumn("state")).setValueMap(mapState);

        UniUtils.createPage(model.getDataSource(), list);
    }

    @Override
    public void changeUse(Long id) {
        EntrantRecommendedSettings entity = getNotNull(id);
        entity.setUseDemon(!entity.isUseDemon());
        saveOrUpdate(entity);
    }

    @Override
    public void saveChanges(Model model, Long id) {
        Map<Long, Long> mapInterval = ((BlockColumn) model.getDataSource().getColumn("interval")).getValueMap();
        Map<Long, String> mapPath = ((BlockColumn) model.getDataSource().getColumn("path")).getValueMap();

        EntrantRecommendedSettings entity = getNotNull(id);
        entity.setPath(mapPath.get(id));

        int interval = mapInterval.get(id) != null ? mapInterval.get(id).intValue() : 0;
        if (entity.isUseDemon() && interval < 5)
            throw new ApplicationException("Интервал слишком мал");

        entity.setInterval(interval);

        saveOrUpdate(entity);
    }

    @Override
    public void runNow(Long id) {
        EntrantRecommendedSettings entity = getNotNull(id);
        RatingBean.instance().runNow(entity.getEnrollmentCampaign().getId());
    }
}
