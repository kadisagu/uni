package ru.tandemservice.uniecrmc.util;

import ru.tandemservice.uniecrmc.entity.entrant.EntrantRecommendedSettings;
import ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

public class UniecrmcUtil {

    public static boolean isUseGroup(EnrollmentCampaign campaign) {
        EntrantRecommendedSettings settings = UniDaoFacade.getCoreDao().get(EntrantRecommendedSettings.class, EntrantRecommendedSettings.enrollmentCampaign(), campaign);
        return settings != null && settings.isUseGroup();
    }

    public static RequestedEnrollmentDirectionExt getRequestedEnrollmentDirectionExt(RequestedEnrollmentDirection direction) {
        return UniDaoFacade.getCoreDao().get(RequestedEnrollmentDirectionExt.class, RequestedEnrollmentDirectionExt.requestedEnrollmentDirection(), direction);
    }
}
