package ru.tandemservice.uniec.component.report.EnrollmentResultByEGE.Add;

import org.apache.commons.lang.mutable.MutableDouble;
import org.apache.commons.lang.mutable.MutableInt;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.tools.PrivateAccessor;
import ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt;
import ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.util.EntrantDataUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class SumLevExt extends SumLev {

    /**
     * Формирующее подразделение (используется для группировки)
     */
    private OrgUnit formativeOrgUnit;

    public static Map<Long, RequestedEnrollmentDirectionExt> requestedEnrolmentDirectionExtMap = new HashMap<Long, RequestedEnrollmentDirectionExt>();

    public SumLevExt(EntrantDataUtil dataUtil, String title, Long id, int level, OrgUnit formativeOrgUnit) {
        super(dataUtil, title, id, level);
        this.formativeOrgUnit = formativeOrgUnit;
    }

    @Override
    void add(PreliminaryEnrollmentStudent preStudent) {

        Long key = preStudent.getId();
        MutableDouble sum = null;
        MutableInt count = null;

        if (getUsedSumIds().add(key)) {
            EntrantDataUtil entrantDataUtil = getEntrantDataUtil();

            int countFieldValue = getCount() + 1;
            PrivateAccessor.setPrivateField(this, SumLev.class, "_count", countFieldValue);

            if (preStudent.isTargetAdmission()) {
                int targetCountFieldValue = getTargetCount() + 1;
                PrivateAccessor.setPrivateField(this, SumLev.class, "_targetCount", targetCountFieldValue);
                sum = getTargetSum();
                count = getTargetSumCount();
            }
            else if (preStudent.getRequestedEnrollmentDirection().getCompetitionKind().getCode().equals("3") || haveSpecialRights(preStudent)) {
                int benefitCountFieldValue = getBenefitCount() + 1;
                PrivateAccessor.setPrivateField(this, SumLev.class, "_benefitCount", benefitCountFieldValue);
                sum = getBenefitSum();
                count = getBenefitSumCount();

            }
            else if (preStudent.getRequestedEnrollmentDirection().getCompetitionKind().getCode().equals("5")) {
                boolean hasOlympiadMark = false;
                for (ChosenEntranceDiscipline chosen : entrantDataUtil.getChosenEntranceDisciplineSet(preStudent.getRequestedEnrollmentDirection())) {
                    hasOlympiadMark = (hasOlympiadMark) || ((chosen.getFinalMarkSource() != null) && (chosen.getFinalMarkSource().equals(Integer.valueOf(1))));
                }

                if (hasOlympiadMark) {
                    int olympiadCountFieldValue = getOlympiadCount() + 1;
                    PrivateAccessor.setPrivateField(this, SumLev.class, "_olympiadCount", olympiadCountFieldValue);
                    sum = getOlympiadSum();
                    count = getOlympiadSumCount();
                }
                else {
                    sum = getCommonSum();
                    count = getCommonSumCount();
                }
            }
            if ((sum != null) && (count != null)) {
                for (ChosenEntranceDiscipline chosen : entrantDataUtil.getChosenEntranceDisciplineSet(preStudent.getRequestedEnrollmentDirection())) {
                    if ((chosen.getFinalMark() != null) && (chosen.getFinalMarkSource() != null)) {
                        int source = chosen.getFinalMarkSource().intValue();
                        if ((source == 10) || (source == 11) || (source == 8) || (source == 9)) {
                            sum.add(chosen.getFinalMark());
                            count.increment();
                        }
                    }
                }
            }
        }
    }

    public OrgUnit getFormativeOrgUnit() {
        return formativeOrgUnit;
    }

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit) {
        this.formativeOrgUnit = formativeOrgUnit;
    }

    public MutableDouble getCommonSum() {
        return (MutableDouble) PrivateAccessor.getPrivateField(this, SumLev.class, "_commonSum");
    }

    public MutableInt getCommonSumCount() {
        return (MutableInt) PrivateAccessor.getPrivateField(this, SumLev.class, "_commonSumCount");
    }

    public MutableDouble getBenefitSum() {
        return (MutableDouble) PrivateAccessor.getPrivateField(this, SumLev.class, "_benefitSum");
    }

    public MutableInt getBenefitSumCount() {
        return (MutableInt) PrivateAccessor.getPrivateField(this, SumLev.class, "_benefitSumCount");
    }

    public MutableDouble getOlympiadSum() {
        return (MutableDouble) PrivateAccessor.getPrivateField(this, SumLev.class, "_olympiadSum");
    }

    public MutableInt getOlympiadSumCount() {
        return (MutableInt) PrivateAccessor.getPrivateField(this, SumLev.class, "_olympiadSumCount");
    }

    public MutableDouble getTargetSum() {
        return (MutableDouble) PrivateAccessor.getPrivateField(this, SumLev.class, "_targetSum");
    }

    public MutableInt getTargetSumCount() {
        return (MutableInt) PrivateAccessor.getPrivateField(this, SumLev.class, "_targetSumCount");
    }

    public int getCount() {
        return (int) PrivateAccessor.getPrivateField(this, SumLev.class, "_count");
    }

    public int getBenefitCount() {
        return (int) PrivateAccessor.getPrivateField(this, SumLev.class, "_benefitCount");
    }

    public int getOlympiadCount() {
        return (int) PrivateAccessor.getPrivateField(this, SumLev.class, "_olympiadCount");
    }

    public int getTargetCount() {
        return (int) PrivateAccessor.getPrivateField(this, SumLev.class, "_targetCount");
    }

    public EntrantDataUtil getEntrantDataUtil() {
        return (EntrantDataUtil) PrivateAccessor.getPrivateField(this, SumLev.class, "_dataUtil");
    }

    @SuppressWarnings("unchecked")
    public Set<Long> getUsedSumIds() {
        return (Set<Long>) PrivateAccessor.getPrivateField(this, SumLev.class, "usedSumIds");
    }

    private boolean haveSpecialRights(PreliminaryEnrollmentStudent preliminaryEnrollmentStudent) {

        if (requestedEnrolmentDirectionExtMap == null || requestedEnrolmentDirectionExtMap.isEmpty())
            return false;

        RequestedEnrollmentDirectionExt requestedEnrollmentDirectionExt = requestedEnrolmentDirectionExtMap.get(preliminaryEnrollmentStudent.getRequestedEnrollmentDirection().getId());
        boolean haveSpecialRights = false;
        if (requestedEnrollmentDirectionExt != null && requestedEnrollmentDirectionExt.isHaveSpecialRights()) {
            haveSpecialRights = true;
        }

        return haveSpecialRights;
    }
}