package ru.tandemservice.uniecrmc.component.settings.EnrollmentCampaignAddEdit;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniecrmc.dao.UniecrmcDao;
import ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionRestrictionRMC;
import ru.tandemservice.uniecrmc.entity.entrant.EntrantRecommendedSettings;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.StructureEducationLevelsCodes;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistribution;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistributionConfig;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.settings.CompetitionGroupRestriction;
import ru.tandemservice.uniec.entity.settings.EnrollmentDirectionRestriction;

import java.util.*;

public class DAO extends ru.tandemservice.uniec.component.settings.EnrollmentCampaignAddEdit.DAO implements IDAO {

    @Override
    public void prepare(ru.tandemservice.uniec.component.settings.EnrollmentCampaignAddEdit.Model model) {
        Model myModel = (Model) model;
        super.prepare(model);

        List<StructureEducationLevels> levelList = getList(StructureEducationLevels.class, StructureEducationLevels.code(), Arrays.asList(StructureEducationLevelsCodes.HIGH_GOS3_GROUP, StructureEducationLevelsCodes.MIDDLE_GOS3_GROUP));
        myModel.setStrLevelList(levelList);

        loadRestrictions(myModel);

        if (!myModel.isFirstLoad())
            return;
        myModel.setFirstLoad(false);


        myModel.setOptionGroupList(Arrays.asList(new IdentifiableWrapper[]{new IdentifiableWrapper(Long.valueOf(0L), "Учитывать приоритет ВИ при распределении"), new IdentifiableWrapper(Long.valueOf(1L), "Формировать рейтинг по профильному предмету")}));
        myModel.setGroupList(Arrays.asList(new IdentifiableWrapper[]{new IdentifiableWrapper(Long.valueOf(0L), "Указывать"), new IdentifiableWrapper(Long.valueOf(1L), "Не указывать")}));

        if (model.getEnrollmentCampaignId() != null) {//первая загрузка
            EntrantRecommendedSettings settings = get(EntrantRecommendedSettings.class, EntrantRecommendedSettings.enrollmentCampaign(), model.getEnrollmentCampaign());

            myModel.setPriority(myModel.getOptionGroupList().get(settings.isUsePriorityEntranceDiscipline() ? 0 : 1));
            myModel.setUseGroup(myModel.getGroupList().get(settings.isUseGroup() ? 0 : 1));
            myModel.setUseLevelInRestrition(settings.isUseStructureLevelInRestriction());

            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EcgDistribution.class, "e").column("e");
            builder.joinPath(DQLJoinType.inner, EcgDistribution.config().fromAlias("e"), "config");

            if (model.getEnrollmentCampaign().isUseCompetitionGroup()) {
                builder.joinEntity("e", DQLJoinType.inner, CompetitionGroup.class, "g", DQLExpressions.eq(DQLExpressions.property(EcgDistributionConfig.ecgItem().id().fromAlias("config")), DQLExpressions.property(CompetitionGroup.id().fromAlias("g"))));
                builder.where(DQLExpressions.eq(DQLExpressions.property(CompetitionGroup.enrollmentCampaign().fromAlias("g")), DQLExpressions.value(model.getEnrollmentCampaign())));
            }
            else {
                builder.joinEntity("e", DQLJoinType.inner, EnrollmentDirection.class, "d", DQLExpressions.eq(DQLExpressions.property(EcgDistributionConfig.ecgItem().id().fromAlias("config")), DQLExpressions.property(EnrollmentDirection.id().fromAlias("d"))));
                builder.where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.enrollmentCampaign().fromAlias("d")), DQLExpressions.value(model.getEnrollmentCampaign())));
            }

            myModel.setDisabled(getCount(builder) > 0);
        }
        else {
            myModel.setPriority(myModel.getOptionGroupList().get(0));
            myModel.setUseGroup(myModel.getGroupList().get(1));
        }


    }

    @Override
    public void loadRestrictions(Model model) {
        getSession().clear();
        List<EnrollmentDirectionRestrictionRMC> restrictionList = getList(EnrollmentDirectionRestrictionRMC.class, EnrollmentDirectionRestrictionRMC.enrollmentCampaign().id(), model.getEnrollmentCampaignId(), EnrollmentDirectionRestrictionRMC.id().s());
        model.setRestrictionList(restrictionList);

        List<EnrollmentDirectionRestriction> list2 = getList(EnrollmentDirectionRestriction.class, EnrollmentDirectionRestriction.enrollmentCampaign().id(), model.getEnrollmentCampaignId(), EnrollmentDirectionRestriction.id().s());
        model.setDirectionRestrictionList(list2);

        List<CompetitionGroupRestriction> list3 = getList(CompetitionGroupRestriction.class, CompetitionGroupRestriction.enrollmentCampaign().id(), model.getEnrollmentCampaignId(), CompetitionGroupRestriction.id().s());
        model.setCompetitionGroupRestrictionList(list3);

        DevelopForm developForm = (DevelopForm) model.getDevelopFormList().findValues("").getObjects().get(0);//getCatalogItem(DevelopForm.class, DevelopFormCodes.FULL_TIME_FORM);
        CompensationType compensationType = model.getCompensationTypeList().get(0);//getCatalogItem(CompensationType.class, CompensationTypeCodes.COMPENSATION_TYPE_BUDGET);
        StructureEducationLevels structureEducationLevels = model.getStrLevelList().get(0);//getCatalogItem(StructureEducationLevels.class, StructureEducationLevelsCodes.HIGH_GOS3_GROUP);
        if (model.getRestrictionList().isEmpty()) {
            EnrollmentDirectionRestrictionRMC obj = new EnrollmentDirectionRestrictionRMC();
            obj.setDevelopForm(developForm);
            obj.setCompensationType(compensationType);
            obj.setStructureEducationLevels(structureEducationLevels);
            model.getRestrictionList().add(obj);
        }

        if (model.getDirectionRestrictionList() == null)
            model.setDirectionRestrictionList(new ArrayList<EnrollmentDirectionRestriction>());
        if (model.getDirectionRestrictionList().isEmpty()) {
            EnrollmentDirectionRestriction obj = new EnrollmentDirectionRestriction();
            obj.setDevelopForm(developForm);
            obj.setCompensationType(compensationType);
            model.getDirectionRestrictionList().add(obj);
        }

        if (model.getCompetitionGroupRestrictionList() == null)
            model.setCompetitionGroupRestrictionList(new ArrayList<CompetitionGroupRestriction>());
        if (model.getCompetitionGroupRestrictionList().isEmpty()) {
            CompetitionGroupRestriction obj = new CompetitionGroupRestriction();
            obj.setDevelopForm(developForm);
            obj.setCompensationType(compensationType);
            model.getCompetitionGroupRestrictionList().add(obj);
        }
    }

    @Override
    public void update(ru.tandemservice.uniec.component.settings.EnrollmentCampaignAddEdit.Model model) {
        Model myModel = (Model) model;
        super.update(model);

        EntrantRecommendedSettings settings = get(EntrantRecommendedSettings.class, EntrantRecommendedSettings.enrollmentCampaign(), model.getEnrollmentCampaign());
        if (settings == null) {
            settings = new EntrantRecommendedSettings();
            settings.setEnrollmentCampaign(model.getEnrollmentCampaign());
        }
        settings.setUseGroup(0L == myModel.getUseGroup().getId().longValue());
        settings.setUsePriorityEntranceDiscipline(0L == myModel.getPriority().getId().longValue());
        settings.setUseStructureLevelInRestriction(myModel.isUseLevelInRestrition());
        getSession().saveOrUpdate(settings);

        //ограничения EnrollmentDirectionRestrictionRMC
        new DQLDeleteBuilder(EnrollmentDirectionRestrictionRMC.class)
                .where(DQLExpressions.eqValue(DQLExpressions.property(EnrollmentDirectionRestrictionRMC.enrollmentCampaign()), model.getEnrollmentCampaign()))
                .createStatement(getSession())
                .execute();

        if (settings.isUseStructureLevelInRestriction()) {
            //используются наши ограничения, старые не нужны
            new DQLDeleteBuilder(EnrollmentDirectionRestriction.class)
                    .where(DQLExpressions.eqValue(DQLExpressions.property(EnrollmentDirectionRestriction.enrollmentCampaign()), model.getEnrollmentCampaign()))
                    .createStatement(getSession())
                    .execute();
            new DQLDeleteBuilder(CompetitionGroupRestriction.class)
                    .where(DQLExpressions.eqValue(DQLExpressions.property(CompetitionGroupRestriction.enrollmentCampaign()), model.getEnrollmentCampaign()))
                    .createStatement(getSession())
                    .execute();

            Set<MultiKey> used = new HashSet<>();
            for (EnrollmentDirectionRestrictionRMC restriction : myModel.getRestrictionList()) {
                if (!used.add(new MultiKey(restriction.getDevelopForm(), restriction.getCompensationType(), restriction.getStructureEducationLevels()))) {
                    throw new ApplicationException("Нельзя сохранить настройку приемной кампании, т.к. не должно быть одинаковых ключей (форма освоения, вид затрат и уровень образования) ограничений числа выбираемых конкурсных групп/направлений подготовки");
                }

                EnrollmentDirectionRestrictionRMC obj = new EnrollmentDirectionRestrictionRMC();
                obj.update(restriction);
                obj.setEnrollmentCampaign(model.getEnrollmentCampaign());
                getSession().saveOrUpdate(obj);
            }
        }

        if (settings.isUsePriorityEntranceDiscipline())
            UniecrmcDao.Instanse().fillPriorityED(model.getEnrollmentCampaign());

    }
}
