package ru.tandemservice.uniecrmc.dao.moveoriginal;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.List;

public interface IDAOMoveOriginal {

    @Transactional(propagation = Propagation.REQUIRED)
    public Long moveOriginalForRequest(List<WrapRequestedEnrollmentDirection> lstRED);

    public List<WrapRequestedEnrollmentDirection> getWrapRequestedEnrollmentDirection(
            Long entrantRequestId);

    public boolean hasState(List<WrapRequestedEnrollmentDirection> lstRED,
                            List<String> endEntrantState);

    public List<WrapEntrantRequest> doCalculate(EnrollmentCampaign campaign);

    @Transactional
    public void recalculatePart(List<Long> part);

}
