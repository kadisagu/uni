package ru.tandemservice.uniecrmc.component.systemaction.AddOtherDirectionToRequest;

import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.List;
import java.util.Map;

public interface IDAO extends IUniDao<Model> {
    @Transactional
    public Long processEntrantRequest
            (
                    Long entrantRequestId
                    , Map<CompetitionGroup, List<EnrollmentDirection>> mapCG
            );

    public List<Long> getEntrantRequestIds
            (
                    EnrollmentCampaign enrollmentCampaign
                    , int regNumber
            );


    public Map<CompetitionGroup, List<EnrollmentDirection>> getMapCompetitionGroup
            (
                    EnrollmentCampaign enrollmentCampaign
            );

    @Transactional
    public void recalculatePart(List<Long> part);
}
