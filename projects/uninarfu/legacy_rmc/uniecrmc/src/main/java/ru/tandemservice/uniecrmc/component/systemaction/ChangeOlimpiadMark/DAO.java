package ru.tandemservice.uniecrmc.component.systemaction.ChangeOlimpiadMark;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.dao.IEntrantDAO;
import ru.tandemservice.uniec.entity.entrant.Discipline2OlympiadDiplomaRelation;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.Arrays;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model)
    {
        // универсальная заполнячилка списка приемок
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        EnrollmentCampaign enrollmentCampaign = model.getSettings().get("enrollmentCampaign");
        if (enrollmentCampaign != null) {
            // Зачтение диплома олимпиады
            model.setOlympiadDiplomaRuleList(Arrays.asList(
                    new IdentifiableWrapper(Model.OLYMPIAD_DIPLOMA_FOR_DIRECTION, "За дисциплины по одному выбранному направлению приема"),
                    new IdentifiableWrapper(Model.OLYMPIAD_DIPLOMA_FOR_ALL_DIRECTIONS, "За дисциплины по всем выбранным направлениям приема")
            ));
            model.setOlympiadDiplomaRule(model.getOlympiadDiplomaRuleList().get(enrollmentCampaign.isOlympiadDiplomaForDirection() ? 0 : 1));
        }
    }

    @Override
    public void update(Model model)
    {
        EnrollmentCampaign enrollmentCampaign = model.getSettings().get("enrollmentCampaign");
        if (enrollmentCampaign != null) {

            boolean newVal = (Model.OLYMPIAD_DIPLOMA_FOR_DIRECTION == model.getOlympiadDiplomaRule().getId());

            if (newVal != enrollmentCampaign.isOlympiadDiplomaForDirection()) {
                enrollmentCampaign.setOlympiadDiplomaForDirection(newVal);
                // сохраняем настройки
                saveOrUpdate(enrollmentCampaign);

                // все в пересчет
                recalculateMark(enrollmentCampaign);
            }
        }
    }

    // пересчет оценок, только по тем заявам, у которых есть зачтение олимпиад
    private void recalculateMark(EnrollmentCampaign enrollmentCampaign)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(Discipline2OlympiadDiplomaRelation.class, "d2o")
                .where(eq(property(Discipline2OlympiadDiplomaRelation.enrollmentDirection().enrollmentCampaign().fromAlias("d2o")), value(enrollmentCampaign)))
                .column(Discipline2OlympiadDiplomaRelation.diploma().entrant().id().fromAlias("d2o").s(), "id")
                .predicate(DQLPredicateType.distinct);

        List<Long> lst = dql.createStatement(getSession()).list();

        // пересчитываем абитуриентов
        IEntrantDAO idao = (IEntrantDAO) ApplicationRuntime.getBean(IEntrantDAO.class.getName());
        idao.updateEntrantData(lst);

    }
}
