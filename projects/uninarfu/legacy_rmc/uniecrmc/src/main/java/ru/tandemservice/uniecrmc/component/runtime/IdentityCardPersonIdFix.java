package ru.tandemservice.uniecrmc.component.runtime;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.runtime.IRuntimeExtension;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniBaseDao;

import java.util.List;


/**
 * Проставляет person_id всем identitycard_t если это возможно
 *
 * @author shura
 */
public class IdentityCardPersonIdFix extends UniBaseDao implements IRuntimeExtension {

    @Override
    public void destroy() {
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
    public void init(Object arg0) {
        List<Person> personList = getList(Person.class, Person.identityCard().person(), (Person) null);

        for (Person person : personList) {
            IdentityCard ic = person.getIdentityCard();
            if (ic == null)
                continue;

            ic.setPerson(person);
            getSession().saveOrUpdate(ic);
        }

    }

}
