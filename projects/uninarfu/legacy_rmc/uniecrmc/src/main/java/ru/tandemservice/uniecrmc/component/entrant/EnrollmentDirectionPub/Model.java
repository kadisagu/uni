package ru.tandemservice.uniecrmc.component.entrant.EnrollmentDirectionPub;

import ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionExt;

public class Model extends ru.tandemservice.uniec.component.entrant.EnrollmentDirectionPub.Model {

    private EnrollmentDirectionExt enrollmentDirectionExt;
    private boolean visible = false;

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public EnrollmentDirectionExt getEnrollmentDirectionExt() {
        return enrollmentDirectionExt;
    }

    public void setEnrollmentDirectionExt(
            EnrollmentDirectionExt enrollmentDirectionExt)
    {
        this.enrollmentDirectionExt = enrollmentDirectionExt;
    }

}
