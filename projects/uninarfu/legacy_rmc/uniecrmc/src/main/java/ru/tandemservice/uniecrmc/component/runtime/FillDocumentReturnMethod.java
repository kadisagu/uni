package ru.tandemservice.uniecrmc.component.runtime;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.runtime.IRuntimeExtension;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniecrmc.entity.catalog.DocumentReturnMethod;
import ru.tandemservice.uniecrmc.entity.entrant.EntrantExt;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.entity.entrant.Entrant;

import java.util.List;

/**
 * Проставляет абитуриентам способ возврата документов.
 * Необходимо выполнить один раз после заполнения DocumentReturnMethod.
 * Логичнее миграцией, но она выполняется до заполнения справочника.
 */
public class FillDocumentReturnMethod extends UniBaseDao implements IRuntimeExtension {
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
    public void init(Object o) {

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EntrantExt.class, "en");
        List<EntrantExt> extList = dql.createStatement(getSession()).setMaxResults(1).list();

        if (CollectionUtils.isEmpty(extList)) {
            DocumentReturnMethod drm = getCatalogItem(DocumentReturnMethod.class, "1");
            if (drm == null)
                return;
            List<Entrant> entrList = getList(Entrant.class);
            for (Entrant entrant : entrList) {
                EntrantExt entrantExt = new EntrantExt();
                entrantExt.setEntrant(entrant);
                entrantExt.setDocumentReturnMethod(drm);
                save(entrantExt);
            }
        }


    }

    @Override
    public void destroy() {
    }
}
