package ru.tandemservice.uniecrmc.component.report.EntrantEduLevelDistribution.Pub;

import ru.tandemservice.uniecrmc.entity.entrant.EntrantEduLevelDistributionReport;
import ru.tandemservice.uni.dao.UniDao;

public class DAO extends UniDao<Model>
        implements IDAO
{
    public void prepare(Model model)
    {
        model.setReport((EntrantEduLevelDistributionReport) getNotNull(EntrantEduLevelDistributionReport.class, model.getReport().getId()));
    }
}
