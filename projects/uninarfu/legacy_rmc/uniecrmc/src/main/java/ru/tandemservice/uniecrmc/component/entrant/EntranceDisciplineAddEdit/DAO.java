package ru.tandemservice.uniecrmc.component.entrant.EntranceDisciplineAddEdit;

import ru.tandemservice.uniecrmc.entity.entrant.EntranceDisciplineExt;
import ru.tandemservice.uniecrmc.entity.entrant.EntrantRecommendedSettings;
import ru.tandemservice.uniec.component.entrant.EntranceDisciplineAddEdit.EntranceDisciplineAddEditModel;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;

public class DAO extends ru.tandemservice.uniec.component.entrant.EntranceDisciplineAddEdit.DAO {

    @Override
    public void update(EntranceDisciplineAddEditModel model) {
        super.update(model);

        EntrantRecommendedSettings settings = get(EntrantRecommendedSettings.class, EntrantRecommendedSettings.enrollmentCampaign(), model.getEnrollmentCampaign());

        if (settings != null && settings.isUsePriorityEntranceDiscipline()) {
            EntranceDisciplineExt disciplineExt = new EntranceDisciplineExt();
            disciplineExt.setEntranceDiscipline(model.getEntranceDiscipline());
            disciplineExt.setPriority(getCount(EntranceDiscipline.class, EntranceDiscipline.L_ENROLLMENT_DIRECTION, model.getEntranceDiscipline().getEnrollmentDirection()) + 1);
            getSession().saveOrUpdate(disciplineExt);
        }

    }
}
