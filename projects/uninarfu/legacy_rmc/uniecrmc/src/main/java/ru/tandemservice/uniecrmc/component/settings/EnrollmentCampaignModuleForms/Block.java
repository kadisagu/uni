package ru.tandemservice.uniecrmc.component.settings.EnrollmentCampaignModuleForms;

import org.tandemframework.shared.commonbase.tapestry.validator.ParamRequiredValidator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Block implements Comparable<Block>, Serializable {
    private static final long serialVersionUID = -948101688104104596L;

    private String key;
    private String title;
    private List<Property> propertyList = new ArrayList<>();

    public Block(String key, String title) {
        this.key = key;
        this.title = title;
    }

    public void addProperty(String key, String title) {
        Boolean value = Boolean.valueOf(ParamRequiredValidator.isRequired(key));
        this.propertyList.add(new Property(title, key, value));
    }

    public String getTitle() {
        return title;
    }


    public void setTitle(String title) {
        this.title = title;
    }


    public List<Property> getPropertyList() {
        return propertyList;
    }


    public void setPropertyList(List<Property> propertyList) {
        this.propertyList = propertyList;
    }

    @Override
    public int compareTo(Block another) {
        return another != null ? this.key.compareTo(another.key) : 1;
    }

    @Override
    public boolean equals(Object obj) {
        return compareTo((Block) obj) == 0;
    }

    /////////////////////////////////////////////////////////////////////////////////////

    public static class Property implements Comparable<Property>, Serializable {
        private static final long serialVersionUID = 6200759036605784481L;

        private String title;
        private String key;
        private Boolean value;

        public Property(String title, String key, Boolean value) {
            this.title = title;
            this.key = key;
            this.value = value;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public Boolean getValue() {
            return value;
        }

        public void setValue(Boolean value) {
            this.value = value;
        }

        @Override
        public boolean equals(Object obj) {
            return compareTo((Property) obj) == 0;
        }

        @Override
        public int compareTo(Property another) {
            return another != null ? this.key.compareTo(another.key) : 1;
        }

    }


}
