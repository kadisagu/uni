package ru.tandemservice.uniecrmc.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniecrmc.entity.entrant.ChangeOriginalDocumentRED;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Лог смены оригинала документов у выбранных направлений для приема
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ChangeOriginalDocumentREDGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniecrmc.entity.entrant.ChangeOriginalDocumentRED";
    public static final String ENTITY_NAME = "changeOriginalDocumentRED";
    public static final int VERSION_HASH = -1908622429;
    private static IEntityMeta ENTITY_META;

    public static final String L_OLD_ORIGIN = "oldOrigin";
    public static final String L_NEW_ORIGIN = "newOrigin";
    public static final String L_ENTRANT_REQUEST = "entrantRequest";
    public static final String P_DATE_CHANGE = "dateChange";
    public static final String P_COMMENT = "comment";

    private RequestedEnrollmentDirection _oldOrigin;     // Где ранее лежал оригинал
    private RequestedEnrollmentDirection _newOrigin;     // Новое расположение оригинала
    private EntrantRequest _entrantRequest;     // Заявление абитуриента
    private Date _dateChange;     // Дата замены
    private String _comment;     // Комментарий

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Где ранее лежал оригинал.
     */
    public RequestedEnrollmentDirection getOldOrigin()
    {
        return _oldOrigin;
    }

    /**
     * @param oldOrigin Где ранее лежал оригинал.
     */
    public void setOldOrigin(RequestedEnrollmentDirection oldOrigin)
    {
        dirty(_oldOrigin, oldOrigin);
        _oldOrigin = oldOrigin;
    }

    /**
     * @return Новое расположение оригинала. Свойство не может быть null.
     */
    @NotNull
    public RequestedEnrollmentDirection getNewOrigin()
    {
        return _newOrigin;
    }

    /**
     * @param newOrigin Новое расположение оригинала. Свойство не может быть null.
     */
    public void setNewOrigin(RequestedEnrollmentDirection newOrigin)
    {
        dirty(_newOrigin, newOrigin);
        _newOrigin = newOrigin;
    }

    /**
     * @return Заявление абитуриента. Свойство не может быть null.
     */
    @NotNull
    public EntrantRequest getEntrantRequest()
    {
        return _entrantRequest;
    }

    /**
     * @param entrantRequest Заявление абитуриента. Свойство не может быть null.
     */
    public void setEntrantRequest(EntrantRequest entrantRequest)
    {
        dirty(_entrantRequest, entrantRequest);
        _entrantRequest = entrantRequest;
    }

    /**
     * @return Дата замены. Свойство не может быть null.
     */
    @NotNull
    public Date getDateChange()
    {
        return _dateChange;
    }

    /**
     * @param dateChange Дата замены. Свойство не может быть null.
     */
    public void setDateChange(Date dateChange)
    {
        dirty(_dateChange, dateChange);
        _dateChange = dateChange;
    }

    /**
     * @return Комментарий.
     */
    @Length(max=2000)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ChangeOriginalDocumentREDGen)
        {
            setOldOrigin(((ChangeOriginalDocumentRED)another).getOldOrigin());
            setNewOrigin(((ChangeOriginalDocumentRED)another).getNewOrigin());
            setEntrantRequest(((ChangeOriginalDocumentRED)another).getEntrantRequest());
            setDateChange(((ChangeOriginalDocumentRED)another).getDateChange());
            setComment(((ChangeOriginalDocumentRED)another).getComment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ChangeOriginalDocumentREDGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ChangeOriginalDocumentRED.class;
        }

        public T newInstance()
        {
            return (T) new ChangeOriginalDocumentRED();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "oldOrigin":
                    return obj.getOldOrigin();
                case "newOrigin":
                    return obj.getNewOrigin();
                case "entrantRequest":
                    return obj.getEntrantRequest();
                case "dateChange":
                    return obj.getDateChange();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "oldOrigin":
                    obj.setOldOrigin((RequestedEnrollmentDirection) value);
                    return;
                case "newOrigin":
                    obj.setNewOrigin((RequestedEnrollmentDirection) value);
                    return;
                case "entrantRequest":
                    obj.setEntrantRequest((EntrantRequest) value);
                    return;
                case "dateChange":
                    obj.setDateChange((Date) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "oldOrigin":
                        return true;
                case "newOrigin":
                        return true;
                case "entrantRequest":
                        return true;
                case "dateChange":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "oldOrigin":
                    return true;
                case "newOrigin":
                    return true;
                case "entrantRequest":
                    return true;
                case "dateChange":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "oldOrigin":
                    return RequestedEnrollmentDirection.class;
                case "newOrigin":
                    return RequestedEnrollmentDirection.class;
                case "entrantRequest":
                    return EntrantRequest.class;
                case "dateChange":
                    return Date.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ChangeOriginalDocumentRED> _dslPath = new Path<ChangeOriginalDocumentRED>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ChangeOriginalDocumentRED");
    }
            

    /**
     * @return Где ранее лежал оригинал.
     * @see ru.tandemservice.uniecrmc.entity.entrant.ChangeOriginalDocumentRED#getOldOrigin()
     */
    public static RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> oldOrigin()
    {
        return _dslPath.oldOrigin();
    }

    /**
     * @return Новое расположение оригинала. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.ChangeOriginalDocumentRED#getNewOrigin()
     */
    public static RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> newOrigin()
    {
        return _dslPath.newOrigin();
    }

    /**
     * @return Заявление абитуриента. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.ChangeOriginalDocumentRED#getEntrantRequest()
     */
    public static EntrantRequest.Path<EntrantRequest> entrantRequest()
    {
        return _dslPath.entrantRequest();
    }

    /**
     * @return Дата замены. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.ChangeOriginalDocumentRED#getDateChange()
     */
    public static PropertyPath<Date> dateChange()
    {
        return _dslPath.dateChange();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniecrmc.entity.entrant.ChangeOriginalDocumentRED#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    public static class Path<E extends ChangeOriginalDocumentRED> extends EntityPath<E>
    {
        private RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> _oldOrigin;
        private RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> _newOrigin;
        private EntrantRequest.Path<EntrantRequest> _entrantRequest;
        private PropertyPath<Date> _dateChange;
        private PropertyPath<String> _comment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Где ранее лежал оригинал.
     * @see ru.tandemservice.uniecrmc.entity.entrant.ChangeOriginalDocumentRED#getOldOrigin()
     */
        public RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> oldOrigin()
        {
            if(_oldOrigin == null )
                _oldOrigin = new RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection>(L_OLD_ORIGIN, this);
            return _oldOrigin;
        }

    /**
     * @return Новое расположение оригинала. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.ChangeOriginalDocumentRED#getNewOrigin()
     */
        public RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> newOrigin()
        {
            if(_newOrigin == null )
                _newOrigin = new RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection>(L_NEW_ORIGIN, this);
            return _newOrigin;
        }

    /**
     * @return Заявление абитуриента. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.ChangeOriginalDocumentRED#getEntrantRequest()
     */
        public EntrantRequest.Path<EntrantRequest> entrantRequest()
        {
            if(_entrantRequest == null )
                _entrantRequest = new EntrantRequest.Path<EntrantRequest>(L_ENTRANT_REQUEST, this);
            return _entrantRequest;
        }

    /**
     * @return Дата замены. Свойство не может быть null.
     * @see ru.tandemservice.uniecrmc.entity.entrant.ChangeOriginalDocumentRED#getDateChange()
     */
        public PropertyPath<Date> dateChange()
        {
            if(_dateChange == null )
                _dateChange = new PropertyPath<Date>(ChangeOriginalDocumentREDGen.P_DATE_CHANGE, this);
            return _dateChange;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniecrmc.entity.entrant.ChangeOriginalDocumentRED#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(ChangeOriginalDocumentREDGen.P_COMMENT, this);
            return _comment;
        }

        public Class getEntityClass()
        {
            return ChangeOriginalDocumentRED.class;
        }

        public String getEntityName()
        {
            return "changeOriginalDocumentRED";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
