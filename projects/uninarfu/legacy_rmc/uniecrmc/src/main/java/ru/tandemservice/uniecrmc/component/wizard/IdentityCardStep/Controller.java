package ru.tandemservice.uniecrmc.component.wizard.IdentityCardStep;


import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInline;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInlineConfig;
import ru.tandemservice.uniec.component.wizard.IdentityCardStep.IDAO;
import ru.tandemservice.uniec.component.wizard.IdentityCardStep.Model;

public class Controller extends ru.tandemservice.uniec.component.wizard.IdentityCardStep.Controller {

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = (Model) getModel(component);
        ((IDAO) getDao()).prepare(model);

        AddressBaseEditInlineConfig addressConfig = new AddressBaseEditInlineConfig();

        addressConfig.setDetailedOnly(true);
        addressConfig.setDetailLevel(4);
        addressConfig.setInline(true);
        addressConfig.setAreaVisible(true);
        addressConfig.setFieldSetTitle("Адресные данные");
        addressConfig.setWithoutFieldSet(true);

        // тандем схватил не тот адрес
        if ((null != model.getEntrantModel().getIdentityCard()) && (null != model.getEntrantModel().getIdentityCard().getAddress()))
            addressConfig.setAddressBase(model.getEntrantModel().getIdentityCard().getAddress());

        component.createRegion("addressRegion", new ComponentActivator(AddressBaseEditInline.class.getSimpleName(), new UniMap().add("addressEditUIConfig", addressConfig)));
    }
}
