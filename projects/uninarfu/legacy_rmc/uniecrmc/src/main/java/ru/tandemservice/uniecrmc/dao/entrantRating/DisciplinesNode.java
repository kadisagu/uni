package ru.tandemservice.uniecrmc.dao.entrantRating;

import ru.tandemservice.uniecrmc.entity.entrant.EntranceDisciplineExt;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;

import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

public class DisciplinesNode {

    @XmlElement(name = "Discipline")
    public List<RowNode> disciplineList = new ArrayList<>();

    public DisciplinesNode() {
    }

    public void addDiscipline(EntranceDiscipline discipline, EntranceDisciplineExt disciplineExt) {
        RowNode node = new RowNode("" + discipline.getDiscipline().getId(), discipline.getDiscipline().getTitle(), discipline.getDiscipline().getShortTitle(), null, disciplineExt != null ? disciplineExt.getPriority() : null, null);
        disciplineList.add(node);
    }
}
