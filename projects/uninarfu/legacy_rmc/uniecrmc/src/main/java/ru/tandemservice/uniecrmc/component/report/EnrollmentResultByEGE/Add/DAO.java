package ru.tandemservice.uniecrmc.component.report.EnrollmentResultByEGE.Add;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.component.report.EnrollmentResultByEGE.Add.EnrollmentResultByEGEReportBuilderExt;
import ru.tandemservice.uniec.component.report.EnrollmentResultByEGE.Add.Model;
import ru.tandemservice.uniec.entity.report.EnrollmentResultByEGEReport;

import java.util.Date;

public class DAO extends ru.tandemservice.uniec.component.report.EnrollmentResultByEGE.Add.DAO {

    @Override
    public void update(Model model) {
        Session session = getSession();

        EnrollmentResultByEGEReport report = model.getReport();
        report.setFormingDate(new Date());

        if (model.isStudentCategoryActive()) {
            report.setStudentCategoryTitle(UniStringUtils.join(model.getStudentCategoryList(), "title", "; "));
        }
        if (model.isQualificationActive()) {
            report.setQualificationTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(model.getQualificationList(), "title"), "; "));
        }
        if (model.isCompensationTypeActive()) {
            report.setCompensationTypeTitle(model.getCompensationType().getShortTitle());
        }
        if (model.isFormativeOrgUnitActive()) {
            report.setFormativeOrgUnitTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(model.getFormativeOrgUnitList(), "fullTitle"), "; "));
        }
        if (model.isTerritorialOrgUnitActive()) {
            report.setTerritorialOrgUnitTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(model.getTerritorialOrgUnitList(), "territorialFullTitle"), "; "));
        }
        if (model.isEducationLevelHighSchoolActive()) {
            report.setEducationLevelHighSchoolTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(model.getEducationLevelHighSchoolList(), "displayableTitle"), "; "));
        }
        if (model.isDevelopFormActive()) {
            report.setDevelopFormTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(model.getDevelopFormList(), "title"), "; "));
        }
        if (model.isDevelopConditionActive()) {
            report.setDevelopConditionTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(model.getDevelopConditionList(), "title"), "; "));
        }
        if (model.isDevelopTechActive()) {
            report.setDevelopTechTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(model.getDevelopTechList(), "title"), "; "));
        }
        if (model.isDevelopPeriodActive()) {
            report.setDevelopPeriodTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(model.getDevelopPeriodList(), "title"), "; "));
        }
        if (model.isParallelActive()) {
            report.setExcludeParallel(Boolean.valueOf(model.getParallel().isTrue()));
        }

        DatabaseFile databaseFile = new EnrollmentResultByEGEReportBuilderExt(model, session).getContent();
        session.save(databaseFile);

        report.setContent(databaseFile);
        session.save(report);
    }
}
