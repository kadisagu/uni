package ru.tandemservice.uniecrmc.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniecrmc.entity.catalog.DocumentReturnMethod;
import ru.tandemservice.uniecrmc.entity.entrant.EntrantExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение сущности Абитуриент
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrantExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniecrmc.entity.entrant.EntrantExt";
    public static final String ENTITY_NAME = "entrantExt";
    public static final int VERSION_HASH = -729373183;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT = "entrant";
    public static final String L_DOCUMENT_RETURN_METHOD = "documentReturnMethod";
    public static final String P_ADDITIONAL_INFO_TEXT = "additionalInfoText";
    public static final String P_ENROLLMENT_PRIORITY = "enrollmentPriority";

    private Entrant _entrant;     // Абитуриент
    private DocumentReturnMethod _documentReturnMethod;     // Способ возврата принятых документов
    private String _additionalInfoText;     // Дополнительные индивидуальные достижения
    private Integer _enrollmentPriority;     // Приоритет при равенстве среднего балла аттестата для зачисления на СПО

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Абитуриент. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public Entrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Абитуриент. Свойство не может быть null и должно быть уникальным.
     */
    public void setEntrant(Entrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Способ возврата принятых документов.
     */
    public DocumentReturnMethod getDocumentReturnMethod()
    {
        return _documentReturnMethod;
    }

    /**
     * @param documentReturnMethod Способ возврата принятых документов.
     */
    public void setDocumentReturnMethod(DocumentReturnMethod documentReturnMethod)
    {
        dirty(_documentReturnMethod, documentReturnMethod);
        _documentReturnMethod = documentReturnMethod;
    }

    /**
     * @return Дополнительные индивидуальные достижения.
     */
    public String getAdditionalInfoText()
    {
        return _additionalInfoText;
    }

    /**
     * @param additionalInfoText Дополнительные индивидуальные достижения.
     */
    public void setAdditionalInfoText(String additionalInfoText)
    {
        dirty(_additionalInfoText, additionalInfoText);
        _additionalInfoText = additionalInfoText;
    }

    /**
     * @return Приоритет при равенстве среднего балла аттестата для зачисления на СПО.
     */
    public Integer getEnrollmentPriority()
    {
        return _enrollmentPriority;
    }

    /**
     * @param enrollmentPriority Приоритет при равенстве среднего балла аттестата для зачисления на СПО.
     */
    public void setEnrollmentPriority(Integer enrollmentPriority)
    {
        dirty(_enrollmentPriority, enrollmentPriority);
        _enrollmentPriority = enrollmentPriority;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntrantExtGen)
        {
            setEntrant(((EntrantExt)another).getEntrant());
            setDocumentReturnMethod(((EntrantExt)another).getDocumentReturnMethod());
            setAdditionalInfoText(((EntrantExt)another).getAdditionalInfoText());
            setEnrollmentPriority(((EntrantExt)another).getEnrollmentPriority());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrantExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrantExt.class;
        }

        public T newInstance()
        {
            return (T) new EntrantExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrant":
                    return obj.getEntrant();
                case "documentReturnMethod":
                    return obj.getDocumentReturnMethod();
                case "additionalInfoText":
                    return obj.getAdditionalInfoText();
                case "enrollmentPriority":
                    return obj.getEnrollmentPriority();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrant":
                    obj.setEntrant((Entrant) value);
                    return;
                case "documentReturnMethod":
                    obj.setDocumentReturnMethod((DocumentReturnMethod) value);
                    return;
                case "additionalInfoText":
                    obj.setAdditionalInfoText((String) value);
                    return;
                case "enrollmentPriority":
                    obj.setEnrollmentPriority((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrant":
                        return true;
                case "documentReturnMethod":
                        return true;
                case "additionalInfoText":
                        return true;
                case "enrollmentPriority":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrant":
                    return true;
                case "documentReturnMethod":
                    return true;
                case "additionalInfoText":
                    return true;
                case "enrollmentPriority":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrant":
                    return Entrant.class;
                case "documentReturnMethod":
                    return DocumentReturnMethod.class;
                case "additionalInfoText":
                    return String.class;
                case "enrollmentPriority":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrantExt> _dslPath = new Path<EntrantExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrantExt");
    }
            

    /**
     * @return Абитуриент. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantExt#getEntrant()
     */
    public static Entrant.Path<Entrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Способ возврата принятых документов.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantExt#getDocumentReturnMethod()
     */
    public static DocumentReturnMethod.Path<DocumentReturnMethod> documentReturnMethod()
    {
        return _dslPath.documentReturnMethod();
    }

    /**
     * @return Дополнительные индивидуальные достижения.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantExt#getAdditionalInfoText()
     */
    public static PropertyPath<String> additionalInfoText()
    {
        return _dslPath.additionalInfoText();
    }

    /**
     * @return Приоритет при равенстве среднего балла аттестата для зачисления на СПО.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantExt#getEnrollmentPriority()
     */
    public static PropertyPath<Integer> enrollmentPriority()
    {
        return _dslPath.enrollmentPriority();
    }

    public static class Path<E extends EntrantExt> extends EntityPath<E>
    {
        private Entrant.Path<Entrant> _entrant;
        private DocumentReturnMethod.Path<DocumentReturnMethod> _documentReturnMethod;
        private PropertyPath<String> _additionalInfoText;
        private PropertyPath<Integer> _enrollmentPriority;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Абитуриент. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantExt#getEntrant()
     */
        public Entrant.Path<Entrant> entrant()
        {
            if(_entrant == null )
                _entrant = new Entrant.Path<Entrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Способ возврата принятых документов.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantExt#getDocumentReturnMethod()
     */
        public DocumentReturnMethod.Path<DocumentReturnMethod> documentReturnMethod()
        {
            if(_documentReturnMethod == null )
                _documentReturnMethod = new DocumentReturnMethod.Path<DocumentReturnMethod>(L_DOCUMENT_RETURN_METHOD, this);
            return _documentReturnMethod;
        }

    /**
     * @return Дополнительные индивидуальные достижения.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantExt#getAdditionalInfoText()
     */
        public PropertyPath<String> additionalInfoText()
        {
            if(_additionalInfoText == null )
                _additionalInfoText = new PropertyPath<String>(EntrantExtGen.P_ADDITIONAL_INFO_TEXT, this);
            return _additionalInfoText;
        }

    /**
     * @return Приоритет при равенстве среднего балла аттестата для зачисления на СПО.
     * @see ru.tandemservice.uniecrmc.entity.entrant.EntrantExt#getEnrollmentPriority()
     */
        public PropertyPath<Integer> enrollmentPriority()
        {
            if(_enrollmentPriority == null )
                _enrollmentPriority = new PropertyPath<Integer>(EntrantExtGen.P_ENROLLMENT_PRIORITY, this);
            return _enrollmentPriority;
        }

        public Class getEntityClass()
        {
            return EntrantExt.class;
        }

        public String getEntityName()
        {
            return "entrantExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
