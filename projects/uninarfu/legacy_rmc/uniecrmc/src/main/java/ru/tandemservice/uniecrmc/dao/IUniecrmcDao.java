package ru.tandemservice.uniecrmc.dao;

import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

public interface IUniecrmcDao {

    public RequestedEnrollmentDirection getFirstRequestDirection(EntrantRequest request);

    public void fillPriorityED(EnrollmentCampaign campaign);

    public void fillPriorityED();

    /**
     * @return лог-файл
     */
    @Transactional
    public String changeCompetitionKindSpo();
}
