package ru.tandemservice.uniecrmc.base.ext.EcDistribution.logic;

import org.apache.commons.lang.mutable.MutableInt;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.EcDistributionUtil;
import ru.tandemservice.uniec.base.bo.EcDistribution.logic.IEcDistributionDao;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.*;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistribution;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public interface IEcDistributionExtDao extends IEcDistributionDao {

    public abstract Map<Long, Integer> getNextSRQuota(EcgConfigDTO paramEcgConfigDTO);

    public abstract Map<Long, Integer> getCurrentSRQuota(EcgDistribution paramEcgDistribution);

    public void saveSRQuota(Long distributionId, Map<Long, Integer> srQuotaMap);

    public void updateSRQuota(Long distributionId, Map<Long, Integer> srQuotaMap);

    public abstract void getQuotaHTMLDescription(IEcgDistribution distribution, Map<Long, String> srQuotaMap);

    public abstract List<? extends IEcgEntrantRateRowDTO> getEntrantRateRowList(IEcgDistribution distribution, List<TargetAdmissionKind> usedTaKindList, EcDistributionUtil.Rule rateRule);

    public Map<Long, Integer> getFreeSRQuota(IEcgDistribution distribution);

    public void saveEntrantRecommendedList(IEcgDistribution distribution, boolean targetAdmission, boolean haveSpecialRights, Collection<Long> chosenDirectionIds);

    public IEcgPlanChoiceResult getPossibleDirectionList(IEcgEntrantRateRowDTO rateRowDTO, Long preferableDirectionId, Map<Long, MutableInt> freeMap, Map<Long, MutableInt> srFreeMap);

    /**
     * Определяет по какой приемке работаем
     *
     * @param campaign
     *
     * @return
     */
    public boolean isUsePriority(EnrollmentCampaign campaign);

    /**
     * Дополнительные условия для исключения абитуриентов из распределения
     *
     * @param ec
     *
     * @return
     */
    public DQLSelectBuilder getDqlExcludeEntrantToEc(EnrollmentCampaign ec, IEcgDistribution distribution);

    public Comparator<IEcgCortegeDTO> getComparator(EnrollmentDirection direction);
}
