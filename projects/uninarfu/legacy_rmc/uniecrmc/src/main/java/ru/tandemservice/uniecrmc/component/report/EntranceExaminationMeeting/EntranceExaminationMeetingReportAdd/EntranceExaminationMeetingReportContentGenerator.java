package ru.tandemservice.uniecrmc.component.report.EntranceExaminationMeeting.EntranceExaminationMeetingReportAdd;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.*;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import jxl.write.Number;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreCollectionUtils.Pair;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.fias.base.entity.ICitizenship;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.component.report.EntranceExaminationMeeting.EntranceExaminationMeetingReportAdd.Model;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.entity.settings.EnrollmentCompetitionKind;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.EntrantDataUtil.DetailLevel;
import ru.tandemservice.uniec.util.ExamSetUtil;
import ru.tandemservice.uniec.util.MarkStatistic;

import java.io.ByteArrayOutputStream;
import java.lang.Boolean;
import java.util.*;
import java.util.Map.Entry;

public class EntranceExaminationMeetingReportContentGenerator extends ru.tandemservice.uniec.component.report.EntranceExaminationMeeting.EntranceExaminationMeetingReportAdd.EntranceExaminationMeetingReportContentGenerator {
    private Session _session;

    public EntranceExaminationMeetingReportContentGenerator(EntranceExaminationMeetingReport report, List<EnrollmentDirection> enrollmentDirections, List<StudentCategory> studentCategories, Session session) {
        super(report, enrollmentDirections, studentCategories, session);
        this._session = session;
    }


    public byte[] generateReportContent() throws Exception {
        MQBuilder requestedEnrollmentDirectionsBuilder = this.getRequestedEnrollmentDirectionsBuilder();
        List examDisciplines = this.getExamDisciplines(this._enrollmentDirections);
        List examDisciplinesShortTitles = CommonBaseUtil.getPropertiesList(examDisciplines, "shortTitle");
        int tableRowWidth = (new EntranceExaminationMeetingReportContentGenerator.FieldValues(examDisciplines.size())).getRowWidth();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        WritableWorkbook workbook = Workbook.createWorkbook(out, ws);
        WritableSheet sheet = workbook.createSheet("Отчет", 0);
        sheet.getSettings().setCopies(1);
        sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);
        sheet.getSettings().setScaleFactor(81);
        sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);
        sheet.getSettings().setFitWidth(1);
        sheet.getSettings().setFitHeight(1000);
        double margin = 0.39370078740157477D;
        sheet.getSettings().setTopMargin(margin);
        sheet.getSettings().setBottomMargin(margin);
        sheet.getSettings().setLeftMargin(margin);
        sheet.getSettings().setRightMargin(margin);
        this.createFormatMap();
        boolean TABLE_DATA_COLUMN = false;
        boolean TABLE_DATA_ROW = true;
        this.writeReportHeader(sheet, 0, 6, examDisciplinesShortTitles, tableRowWidth);
        byte row = 8;
        Integer[] rowHandler = new Integer[]{Integer.valueOf(row)};
        this.writeReportData(sheet, 0, rowHandler, requestedEnrollmentDirectionsBuilder, examDisciplines, tableRowWidth);
        Integer var16 = rowHandler[0];
        Integer var17 = rowHandler[0] = Integer.valueOf(rowHandler[0].intValue() + 1);
        this.customizeSheetBeforeDetailTable(sheet, rowHandler);
        var16 = rowHandler[0];
        var17 = rowHandler[0] = Integer.valueOf(rowHandler[0].intValue() + 1);
        this.writeEnrollmentDirectionDetailsTable(sheet, 1, rowHandler);
        sheet.getSettings().setPrintArea(0, 0, tableRowWidth, rowHandler[0].intValue());
        workbook.write();
        workbook.close();
        return out.toByteArray();
    }

    protected void customizeSheetBeforeDetailTable(WritableSheet sheet, Integer[] rowHandler) throws Exception {
    }

    private void createFormatMap() throws Exception {
        WritableFont arial8 = new WritableFont(WritableFont.ARIAL, 8);
        WritableFont arial8bold = new WritableFont(WritableFont.ARIAL, 8, WritableFont.BOLD);
        WritableFont arial8boldItalic = new WritableFont(WritableFont.ARIAL, 8, WritableFont.BOLD, true);
        this._formatMap = new Hashtable();
        WritableCellFormat reportTitleFormat = new WritableCellFormat(arial8bold);
        reportTitleFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        reportTitleFormat.setAlignment(Alignment.CENTRE);
        this._formatMap.put("reportTitle", reportTitleFormat);
        WritableCellFormat headerFormat = new WritableCellFormat(arial8);
        headerFormat.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        headerFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        headerFormat.setAlignment(Alignment.CENTRE);
        headerFormat.setWrap(true);
        this._formatMap.put("headerFormat", headerFormat);
        WritableCellFormat paramFormat = new WritableCellFormat(arial8);
        this._formatMap.put("paramFormat", paramFormat);
        WritableCellFormat rowFormat = new WritableCellFormat(arial8);
        rowFormat.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        rowFormat.setVerticalAlignment(VerticalAlignment.TOP);
        rowFormat.setWrap(true);
        this._formatMap.put("rowFormat", rowFormat);
        WritableCellFormat ga = new WritableCellFormat(arial8);
        ga.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        ga.setVerticalAlignment(VerticalAlignment.TOP);
        this._formatMap.put("ga", ga);
        this._formatMap.put("ta", ga);
        WritableCellFormat gaCompetitionKind = new WritableCellFormat(arial8boldItalic);
        gaCompetitionKind.setIndentation(1);
        gaCompetitionKind.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        gaCompetitionKind.setVerticalAlignment(VerticalAlignment.TOP);
        gaCompetitionKind.setAlignment(Alignment.LEFT);
        this._formatMap.put("gaCompetitionKind", gaCompetitionKind);
        WritableCellFormat gaFIO = new WritableCellFormat(arial8);
        gaFIO.setIndentation(2);
        gaFIO.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        gaFIO.setVerticalAlignment(VerticalAlignment.TOP);
        gaFIO.setAlignment(Alignment.LEFT);
        gaFIO.setWrap(true);
        this._formatMap.put("gaFIO", gaFIO);
        WritableCellFormat taKind = new WritableCellFormat(arial8);
        taKind.setIndentation(1);
        taKind.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        taKind.setVerticalAlignment(VerticalAlignment.TOP);
        taKind.setAlignment(Alignment.LEFT);
        this._formatMap.put("taKind", taKind);
        WritableCellFormat taPlace = new WritableCellFormat(arial8bold);
        taPlace.setIndentation(2);
        taPlace.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        taPlace.setVerticalAlignment(VerticalAlignment.TOP);
        taPlace.setAlignment(Alignment.LEFT);
        this._formatMap.put("taPlace", taPlace);
        WritableCellFormat taCompetitionKind = new WritableCellFormat(arial8boldItalic);
        taCompetitionKind.setIndentation(3);
        taCompetitionKind.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        taCompetitionKind.setVerticalAlignment(VerticalAlignment.TOP);
        taCompetitionKind.setAlignment(Alignment.LEFT);
        this._formatMap.put("taCompetitionKind", taCompetitionKind);
        WritableCellFormat taFIO = new WritableCellFormat(arial8);
        taFIO.setIndentation(4);
        taFIO.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        taFIO.setVerticalAlignment(VerticalAlignment.TOP);
        taFIO.setAlignment(Alignment.LEFT);
        taFIO.setWrap(true);
        this._formatMap.put("taFIO", taFIO);
        WritableCellFormat right = new WritableCellFormat(rowFormat);
        right.setAlignment(Alignment.RIGHT);
        this._formatMap.put("right", right);
        WritableCellFormat center = new WritableCellFormat(rowFormat);
        center.setAlignment(Alignment.CENTRE);
        this._formatMap.put("center", center);
    }

    private void writeReportHeader(WritableSheet sheet, int column, int row, List<String> examDisciplinesShortTitles, int tableRowWidth) throws Exception {
        WritableCellFormat reportTitleFormat = (WritableCellFormat) this._formatMap.get("reportTitle");
        WritableCellFormat paramFormat = (WritableCellFormat) this._formatMap.get("paramFormat");
        WritableCellFormat headerFormat = (WritableCellFormat) this._formatMap.get("headerFormat");
        sheet.addCell(new Label(0, 1, "Протокол заседания приемной комиссии", reportTitleFormat));
        sheet.addCell(new Label(0, 2, TopOrgUnit.getInstance().getTitle(), reportTitleFormat));
        sheet.addCell(new Label(0, 3, "__от  __.__.______ г", reportTitleFormat));
        sheet.mergeCells(0, 1, tableRowWidth, 1);
        sheet.mergeCells(0, 2, tableRowWidth, 2);
        sheet.mergeCells(0, 3, tableRowWidth, 3);
        sheet.addCell(new Label(0, 5, this.getDetails(), paramFormat));
        sheet.mergeCells(0, 5, tableRowWidth, 5);
        sheet.addCell(new Label(column, row, "№ п/п", headerFormat));
        sheet.mergeCells(column, row, column, row + 1);
        sheet.setColumnView(column, 4);
        ++column;
        sheet.addCell(new Label(column, row, "Фамилия имя отчество", headerFormat));
        sheet.mergeCells(column, row, column, row + 1);
        sheet.setColumnView(column, 30);
        ++column;
        sheet.addCell(new Label(column, row, "№ экзам листа", headerFormat));
        sheet.mergeCells(column, row, column, row + 1);
        sheet.setColumnView(column, 12);
        ++column;
        sheet.addCell(new Label(column, row, "Базовое образование", headerFormat));
        sheet.mergeCells(column, row, column, row + 1);
        sheet.setColumnView(column, 11);
        ++column;
        sheet.addCell(new Label(column, row, "Место жительства", headerFormat));
        sheet.mergeCells(column, row, column, row + 1);
        sheet.setColumnView(column, 16);
        ++column;
        sheet.addCell(new Label(column, row, "Иностр. гражданство", headerFormat));
        sheet.mergeCells(column, row, column, row + 1);
        sheet.setColumnView(column, 11);
        ++column;
        sheet.addCell(new Label(column, row, "Вид внеконкурс. приема", headerFormat));
        sheet.mergeCells(column, row, column, row + 1);
        sheet.setColumnView(column, 13);
        ++column;
        if (examDisciplinesShortTitles.isEmpty()) {
            sheet.addCell(new Label(column, row, "Результаты ЕГЭ", headerFormat));
            sheet.mergeCells(column, row, column, row + 1);
            sheet.setColumnView(column, 18);
            ++column;
            sheet.addCell(new Label(column, row, "Результаты экзаменов", headerFormat));
            sheet.mergeCells(column, row, column, row + 1);
            sheet.setColumnView(column, 18);
            ++column;
        }
        else {
            byte cellWidth;
            switch (examDisciplinesShortTitles.size()) {
                case 1:
                    cellWidth = 18;
                    break;
                case 2:
                    cellWidth = 9;
                    break;
                default:
                    cellWidth = 6;
            }

            sheet.addCell(new Label(column, row, "Результаты ЕГЭ", headerFormat));
            sheet.mergeCells(column, row, column + examDisciplinesShortTitles.size() - 1, row);

            int i;
            for (i = 0; i < examDisciplinesShortTitles.size(); ++i) {
                sheet.addCell(new Label(column + i, row + 1, (String) examDisciplinesShortTitles.get(i), headerFormat));
                sheet.setColumnView(column + i, cellWidth);
            }

            column += examDisciplinesShortTitles.size();
            sheet.addCell(new Label(column, row, "Результаты экзаменов", headerFormat));
            sheet.mergeCells(column, row, column + examDisciplinesShortTitles.size() - 1, row);

            for (i = 0; i < examDisciplinesShortTitles.size(); ++i) {
                sheet.addCell(new Label(column + i, row + 1, (String) examDisciplinesShortTitles.get(i), headerFormat));
                sheet.setColumnView(column + i, cellWidth);
            }

            column += examDisciplinesShortTitles.size();
        }

        sheet.addCell(new Label(column, row, "Сумма баллов", headerFormat));
        sheet.mergeCells(column, row, column, row + 1);
        sheet.setColumnView(column, 6);
        ++column;
        sheet.addCell(new Label(column, row, "Дополнительные преимущества", headerFormat));
        sheet.mergeCells(column, row, column, row + 1);
        sheet.setColumnView(column, 15);
        ++column;
        sheet.addCell(new Label(column, row, "Решение приемной комиссии", headerFormat));
        sheet.mergeCells(column, row, column, row + 1);
        sheet.setColumnView(column, 15);
    }

    private void writeReportData(WritableSheet sheet, int column, Integer[] row, MQBuilder requestedEnrollmentDirectionsBuilder, List<Discipline2RealizationWayRelation> examDisciplines, int tableRowWidth) throws Exception {
        this._session.createCriteria(TargetAdmissionKind.class).list();
        Map competitionKind2EnrollmentCompetitionKind = this.getCompetitionKind2EnrollmentCompetitionKind();
        EntranceExaminationMeetingReportContentGenerator.GeneralAdmission general = new EntranceExaminationMeetingReportContentGenerator.GeneralAdmission(sheet, this._formatMap, tableRowWidth, competitionKind2EnrollmentCompetitionKind);
        EntranceExaminationMeetingReportContentGenerator.TargetAdmission target = new EntranceExaminationMeetingReportContentGenerator.TargetAdmission(sheet, this._formatMap, tableRowWidth, competitionKind2EnrollmentCompetitionKind);
        List requestedEnrollmentDirections = requestedEnrollmentDirectionsBuilder.getResultList(this._session);
        Iterator studentHandler = requestedEnrollmentDirections.iterator();

        while (studentHandler.hasNext()) {
            RequestedEnrollmentDirection direction = (RequestedEnrollmentDirection) studentHandler.next();
            ((EntranceExaminationMeetingReportContentGenerator.Admission) (direction.isTargetAdmission() ? target : general)).put(direction);
        }

        general.direction2Fields = target.direction2Fields = this.getFields(requestedEnrollmentDirectionsBuilder, requestedEnrollmentDirections, examDisciplines);
        Integer[] studentHandler1 = new Integer[]{Integer.valueOf(1)};
        target.writeRows(column, row, studentHandler1);
        general.writeRows(column, row, studentHandler1);
    }

    private void writeEnrollmentDirectionDetailsTable(WritableSheet sheet, int column, Integer[] row) throws Exception {
        WritableCellFormat defaultFormat = new WritableCellFormat((CellFormat) this._formatMap.get("rowFormat"));
        defaultFormat.setBorder(Border.NONE, BorderLineStyle.NONE);
        defaultFormat.setWrap(false);
        WritableFont bold = new WritableFont(defaultFormat.getFont());
        bold.setBoldStyle(WritableFont.BOLD);
        Integer enrollmentDirection = row[0];
        Integer educationOrgUnit = row[0] = Integer.valueOf(row[0].intValue() + 1);
        sheet.addCell(new Label(column, enrollmentDirection.intValue(), "Отчет построен по направлениям подготовки (специальностям):", new WritableCellFormat(bold)));
        Iterator i$ = this._enrollmentDirections.iterator();

        while (i$.hasNext()) {
            EnrollmentDirection enrollmentDirection1 = (EnrollmentDirection) i$.next();
            EducationOrgUnit educationOrgUnit1 = enrollmentDirection1.getEducationOrgUnit();
            StringBuilder title = new StringBuilder();
            title.append(educationOrgUnit1.getEducationLevelHighSchool().getShortTitle()).append(" ");
            title.append("(").append(StringUtils.join(new String[]{educationOrgUnit1.getDevelopForm().getTitle().toLowerCase(), educationOrgUnit1.getDevelopCondition().getTitle().toLowerCase(), educationOrgUnit1.getDevelopPeriod().getTitle()}, ", ")).append(") ");
            title.append(educationOrgUnit1.getFormativeOrgUnit().getShortTitle()).append(" ");
            title.append("(").append(educationOrgUnit1.getTerritorialOrgUnit().getTerritorialTitle()).append(")");
            Integer var11 = row[0];
            Integer var12 = row[0] = Integer.valueOf(row[0].intValue() + 1);
            sheet.addCell(new Label(column, var11.intValue(), title.toString(), defaultFormat));
        }

    }

    private Map<CompetitionKind, EnrollmentCompetitionKind> getCompetitionKind2EnrollmentCompetitionKind() {
        Criteria criteria = this._session.createCriteria(EnrollmentCompetitionKind.class);
        criteria.add(Restrictions.eq("enrollmentCampaign", this._report.getEnrollmentCampaign()));
        criteria.add(Restrictions.eq("used", Boolean.valueOf(true)));
        HashMap result = new HashMap();
        Iterator i$ = criteria.list().iterator();

        while (i$.hasNext()) {
            EnrollmentCompetitionKind enrollmentCompetitionKind = (EnrollmentCompetitionKind) i$.next();
            result.put(enrollmentCompetitionKind.getCompetitionKind(), enrollmentCompetitionKind);
        }

        return result;
    }

    private String getDetails() {
        String developCondition = this._report.getDevelopCondition();
        boolean developConditionFilled = !StringUtils.isEmpty(developCondition);
        String developPeriod = this._report.getDevelopPeriod();
        boolean developPeriodFilled = !StringUtils.isEmpty(developPeriod);
        EducationLevelsHighSchool educationLevelsHighSchool = this._report.getEducationLevelsHighSchool();
        if (educationLevelsHighSchool == null) {
            educationLevelsHighSchool = ((EnrollmentDirection) this._enrollmentDirections.get(0)).getEducationOrgUnit().getEducationLevelHighSchool();
        }

        StringBuilder result = new StringBuilder();
        result.append("Специальность (направление): ").append(educationLevelsHighSchool.getPrintTitle()).append(" ");
        result.append("Отделение: ").append(this._report.getDevelopForm().getTitle()).append(" ");
        if (developConditionFilled || developPeriodFilled) {
            result.append("(");
            if (developConditionFilled) {
                result.append("Условие освоения: ").append(developCondition);
                if (developPeriodFilled) {
                    result.append("; ");
                }
            }

            if (developPeriodFilled) {
                result.append("Срок освоения: ").append(developPeriod);
            }

            result.append(") ");
        }

        result.append("Финансирование: ").append(this._report.getCompensationType().getShortTitle()).append(" ");
        if (!StringUtils.isEmpty(this._report.getStudentCategory())) {
            result.append("Категория поступающего: ").append(this._report.getStudentCategory().toLowerCase());
        }

        return result.toString();
    }

    private MQBuilder getRequestedEnrollmentDirectionsBuilder() {
        String enrollmentCampaignStage = this._report.getEnrollmentCampaignStage();
        MQBuilder builder;
        if (enrollmentCampaignStage.equals(Model.ENROLLMENT_CAMPAIGN_STAGE_DOCUMENTS)) {
            builder = new MQBuilder("ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection", "requestedEnrollmentDirection");
            this.createAliases(builder);
            builder.add(MQExpression.in("requestedEnrollmentDirection", RequestedEnrollmentDirection.enrollmentDirection().s(), this._enrollmentDirections));
            builder.add(MQExpression.eq("requestedEnrollmentDirection", RequestedEnrollmentDirection.compensationType().s(), this._report.getCompensationType()));
            builder.add(MQExpression.notEq("state", EntrantState.code().s(), "7"));
            builder.add(MQExpression.between("entrantRequest", EntrantRequest.regDate().s(), this._report.getRequestsFromDate(), CoreDateUtils.add(this._report.getRequestsToDate(), 5, 1)));
            builder.add(MQExpression.eq("entrant", Entrant.archival().s(), Boolean.valueOf(false)));
            if (!this._studentCategories.isEmpty()) {
                builder.add(MQExpression.in("requestedEnrollmentDirection", RequestedEnrollmentDirection.studentCategory().s(), this._studentCategories));
            }
        }
        else if (enrollmentCampaignStage.equals(Model.ENROLLMENT_CAMPAIGN_STAGE_EXAMS)) {
            builder = new MQBuilder("ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection", "requestedEnrollmentDirection");
            this.createAliases(builder);
            builder.add(MQExpression.in("requestedEnrollmentDirection", RequestedEnrollmentDirection.enrollmentDirection().s(), this._enrollmentDirections));
            builder.add(MQExpression.eq("requestedEnrollmentDirection", RequestedEnrollmentDirection.compensationType().s(), this._report.getCompensationType()));
            builder.add(MQExpression.notIn("state", EntrantState.code().s(), new Object[]{"1", "2", "7"}));
            builder.add(MQExpression.between("entrantRequest", EntrantRequest.regDate().s(), this._report.getRequestsFromDate(), CoreDateUtils.add(this._report.getRequestsToDate(), 5, 1)));
            builder.add(MQExpression.eq("entrant", Entrant.archival().s(), Boolean.valueOf(false)));
            if (!this._studentCategories.isEmpty()) {
                builder.add(MQExpression.in("requestedEnrollmentDirection", RequestedEnrollmentDirection.studentCategory().s(), this._studentCategories));
            }
        }
        else {
            if (!enrollmentCampaignStage.equals(Model.ENROLLMENT_CAMPAIGN_STAGE_ENROLLMENT)) {
                throw new RuntimeException("Unknown enrollment campaign stage");
            }

            ArrayList educationOrgUnits = new ArrayList();
            Iterator i$ = this._enrollmentDirections.iterator();

            while (i$.hasNext()) {
                EnrollmentDirection enrollmentDirection = (EnrollmentDirection) i$.next();
                educationOrgUnits.add(enrollmentDirection.getEducationOrgUnit());
            }

            builder = new MQBuilder("ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent", "preliminaryEnrollmentStudent", new String[]{PreliminaryEnrollmentStudent.requestedEnrollmentDirection().s()});
            builder.addJoinFetch("preliminaryEnrollmentStudent", PreliminaryEnrollmentStudent.requestedEnrollmentDirection().s(), "requestedEnrollmentDirection");
            this.createAliases(builder);
            builder.add(MQExpression.eq("preliminaryEnrollmentStudent", PreliminaryEnrollmentStudent.compensationType().s(), this._report.getCompensationType()));
            builder.add(MQExpression.in("preliminaryEnrollmentStudent", PreliminaryEnrollmentStudent.educationOrgUnit().s(), educationOrgUnits));
            builder.add(MQExpression.between("requestedEnrollmentDirection", RequestedEnrollmentDirection.regDate().s(), this._report.getRequestsFromDate(), CoreDateUtils.add(this._report.getRequestsToDate(), 5, 1)));
            builder.add(MQExpression.eq("entrant", Entrant.archival().s(), Boolean.valueOf(false)));
            if (!this._studentCategories.isEmpty()) {
                builder.add(MQExpression.in("preliminaryEnrollmentStudent", PreliminaryEnrollmentStudent.studentCategory().s(), this._studentCategories));
            }
        }

        return builder;
    }

    private void createAliases(MQBuilder builder) {
        builder.addJoinFetch("requestedEnrollmentDirection", RequestedEnrollmentDirection.entrantRequest().s(), "entrantRequest");
        builder.addJoinFetch("entrantRequest", EntrantRequest.entrant().s(), "entrant");
        builder.addJoinFetch("entrant", Entrant.person().s(), "person");
        builder.addJoinFetch("person", Person.identityCard().s(), "identityCard");
        builder.addJoinFetch("identityCard", IdentityCard.citizenship().s(), "citizenship");
        builder.addLeftJoinFetch("identityCard", IdentityCard.address().s(), "address");
        //builder.addLeftJoinFetch("address", Address.settlement().s(), "settlement");
        builder.addJoinFetch("requestedEnrollmentDirection", RequestedEnrollmentDirection.state().s(), "state");
    }

    private Map<RequestedEnrollmentDirection, FieldValues> getFields(MQBuilder requestedEnrollmentDirectionsBuilder, List<RequestedEnrollmentDirection> requestedEnrollmentDirections, List<Discipline2RealizationWayRelation> examDisciplines) {
        if (requestedEnrollmentDirectionsBuilder.getResultCount(this._session) == 0L) {
            return new HashMap();
        }
        else {
            HashMap direction2Fields = new HashMap();
            HashMap person2Direction = new HashMap();
            HashMap entrant2Direction = new HashMap();
            ArrayList entrantRequests = new ArrayList();
            EntrantDataUtil entrantDataUtil = new EntrantDataUtil(this._session, this._report.getEnrollmentCampaign(), requestedEnrollmentDirectionsBuilder, DetailLevel.EXAM_PASS_WITH_MARK_STATISTICS);
            Map direction2ProfileKnowledges = EntrantDataUtil.getProfileKnowledgeMap(this._session, requestedEnrollmentDirectionsBuilder);
            Map person2Benefits = EntrantDataUtil.getPersonBenefitMap(this._session, requestedEnrollmentDirectionsBuilder);
            Map entrant2EnrollmentRecomendations = EntrantDataUtil.getEnrollmentRecomendationMap(this._session, requestedEnrollmentDirectionsBuilder);

            Entrant i$;
            EntranceExaminationMeetingReportContentGenerator.FieldValues requestedEnrollmentDirection;
            for (Iterator entrantAndDiscipline2ExamMarks = requestedEnrollmentDirections.iterator(); entrantAndDiscipline2ExamMarks.hasNext(); requestedEnrollmentDirection.certificateAverageMark = entrantDataUtil.getAverageEduInstMark(i$.getId()))
            {
                RequestedEnrollmentDirection i = (RequestedEnrollmentDirection) entrantAndDiscipline2ExamMarks.next();
                direction2Fields.put(i, new EntranceExaminationMeetingReportContentGenerator.FieldValues(examDisciplines.size()));
                EntrantRequest discipline = i.getEntrantRequest();
                entrantRequests.add(discipline);
                i$ = discipline.getEntrant();
                entrant2Direction.put(i$, i);
                Person entry = i$.getPerson();
                person2Direction.put(entry, i);
                requestedEnrollmentDirection = (EntranceExaminationMeetingReportContentGenerator.FieldValues) direction2Fields.get(i);
                requestedEnrollmentDirection.personFullFio = entry.getFullFio();
                requestedEnrollmentDirection.regNumber = i.getStringNumber();
                requestedEnrollmentDirection.personBenefits = i.getCompetitionKind().getCode().equals("3") ? "" : null;
                requestedEnrollmentDirection.personRegistrationAddress = null != entry.getAddressRegistration() && entry.getAddressRegistration() instanceof AddressDetailed ? this.getAddressTitle(((AddressDetailed) entry.getAddressRegistration()).getSettlement()) : null;
                ICitizenship fieldValues = entry.getIdentityCard().getCitizenship();
                requestedEnrollmentDirection.personCitizenship = fieldValues.getCode() != 0 ? fieldValues.getTitle() : null;
                requestedEnrollmentDirection.profileMark = (Double) FastBeanUtils.getValue(i, RequestedEnrollmentDirection.profileChosenEntranceDiscipline().finalMark().s());
                Set markStatistic = (Set) direction2ProfileKnowledges.get(i);
                PersonEduInstitution stateExamMark = entry.getPersonEduInstitution();
                requestedEnrollmentDirection.personEducationLevelStage = markStatistic != null ? UniStringUtils.join(markStatistic, RequestedProfileKnowledge.profileKnowledge().shortTitle().s(), "; ") : (stateExamMark != null ? stateExamMark.getEducationLevel().getShortTitle() : null);
                Set enrollmentExamMark = (Set) person2Benefits.get(entry);
                requestedEnrollmentDirection.personBenefits = requestedEnrollmentDirection.personBenefits != null && enrollmentExamMark != null ? UniStringUtils.join(enrollmentExamMark, PersonBenefit.benefit().shortTitle().s(), "; ") : null;
                Set i$1 = (Set) entrant2EnrollmentRecomendations.get(i$);
                requestedEnrollmentDirection.entrantEnrolmentRecomendations = i$1 != null ? UniStringUtils.join(i$1, EntrantEnrolmentRecommendation.recommendation().shortTitle().s(), "; ") : null;
            }

            HashMap var30 = new HashMap();
            Iterator var31 = requestedEnrollmentDirections.iterator();

            MarkStatistic var42;
            while (var31.hasNext()) {
                RequestedEnrollmentDirection var33 = (RequestedEnrollmentDirection) var31.next();
                i$ = var33.getEntrantRequest().getEntrant();
                Iterator var36 = entrantDataUtil.getChosenEntranceDisciplineSet(var33).iterator();

                while (var36.hasNext()) {
                    ChosenEntranceDiscipline var38 = (ChosenEntranceDiscipline) var36.next();
                    if (examDisciplines.contains(var38.getEnrollmentCampaignDiscipline())) {
                        Pair var40 = new Pair(i$, var38.getEnrollmentCampaignDiscipline());
                        var42 = entrantDataUtil.getMarkStatistic(var38);
                        var30.put(var40, var42);
                    }
                }
            }

            for (int var32 = 0; var32 < examDisciplines.size(); ++var32) {
                Discipline2RealizationWayRelation var34 = (Discipline2RealizationWayRelation) examDisciplines.get(var32);
                Iterator var35 = direction2Fields.entrySet().iterator();

                while (var35.hasNext()) {
                    Entry var37 = (Entry) var35.next();
                    RequestedEnrollmentDirection var39 = (RequestedEnrollmentDirection) var37.getKey();
                    EntranceExaminationMeetingReportContentGenerator.FieldValues var41 = (EntranceExaminationMeetingReportContentGenerator.FieldValues) var37.getValue();
                    var42 = (MarkStatistic) var30.get(new Pair(var39.getEntrantRequest().getEntrant(), var34));
                    if (var42 != null) {
                        Double var43 = var42.getScaledStateExamMark();
                        Iterator var44 = (new HashMap(var42.getInternalMarkMap())).entrySet().iterator();

                        while (var44.hasNext()) {
                            Entry var46 = (Entry) var44.next();
                            PairKey markEntry = (PairKey) var46.getValue();
                            if (markEntry != null) {
                                SubjectPassForm marks = (SubjectPassForm) var46.getKey();
                                if (marks.getCode().equals("1") || marks.getCode().equals("5")) {
                                    ExamPassMark passForm = (ExamPassMark) markEntry.getFirst();
                                    ExamPassMarkAppeal mark = (ExamPassMarkAppeal) markEntry.getSecond();
                                    double appeal = mark != null ? mark.getMark() : passForm.getMark();
                                    if (var43 == null || appeal > var43.doubleValue()) {
                                        var43 = Double.valueOf(appeal);
                                    }
                                }
                            }
                        }

                        var41.stateExamMarks[var32] = var43;
                        Double var45 = Double.valueOf(4.9E-324D);
                        if (var42.getOlympiad() != null) {
                            var45 = Double.valueOf((double) var42.getOlympiad().intValue());
                        }
                        else {
                            Iterator var47 = (new HashMap(var42.getInternalMarkMap())).entrySet().iterator();

                            while (var47.hasNext()) {
                                Entry var48 = (Entry) var47.next();
                                PairKey var49 = (PairKey) var48.getValue();
                                if (var49 != null) {
                                    SubjectPassForm var50 = (SubjectPassForm) var48.getKey();
                                    if (!var50.getCode().equals("1") && !var50.getCode().equals("5")) {
                                        ExamPassMark var51 = (ExamPassMark) var49.getFirst();
                                        ExamPassMarkAppeal var52 = (ExamPassMarkAppeal) var49.getSecond();
                                        double finalMark = var52 != null ? var52.getMark() : var51.getMark();
                                        if (finalMark > var45.doubleValue()) {
                                            var45 = Double.valueOf(finalMark);
                                        }
                                    }
                                }
                            }
                        }

                        if (var45.doubleValue() != 4.9E-324D) {
                            var41.enrollmentExamMarks[var32] = var45;
                        }
                    }
                }
            }

            return direction2Fields;
        }
    }

    private String getAddressTitle(AddressItem settlement) {
        if (settlement == null) {
            return null;
        }
        else {
            String regionCode = settlement.getInheritedRegionCode();
            AddressItem zone = this.getZone(settlement);
            StringBuilder result = new StringBuilder();
            if (!StringUtils.isEmpty(regionCode)) {
                result.append("(").append(regionCode).append(") ");
            }

            result.append(settlement.getTitleWithType());
            if (zone != null) {
                result.append(" (").append(zone.getTitleWithType()).append(")");
            }

            return result.toString();
        }
    }

    private AddressItem getZone(AddressItem settlement) {
        AddressItem result = settlement;

        do {
            if ((result = result.getParent()) == null) {
                return null;
            }
        } while (result.getAddressType().getAddressLevel().getCode() != 1);

        return result;
    }

    public List<Discipline2RealizationWayRelation> getExamDisciplines(List<EnrollmentDirection> enrollmentDirections) {
        if (enrollmentDirections.isEmpty()) {
            return Collections.emptyList();
        }
        else {
            Map map = ExamSetUtil.getDirection2DisciplineMap(this._session, this._report.getEnrollmentCampaign(), this._studentCategories, this._report.getCompensationType());
            HashSet disciplineSet = new HashSet();
            Iterator list = enrollmentDirections.iterator();

            while (list.hasNext()) {
                EnrollmentDirection direction = (EnrollmentDirection) list.next();
                disciplineSet.addAll((Collection) map.get(direction));
            }

            ArrayList list1 = new ArrayList(disciplineSet);
            Collections.sort(list1, ITitled.TITLED_COMPARATOR);
            return list1;
        }
    }

    static class FieldValues {
        static final Double NO_FINAL_MARK = Double.valueOf(-1.0D);
        int examMarksNumber;
        Double profileMark;
        String personFullFio;
        String regNumber;
        String personEducationLevelStage;
        String personRegistrationAddress;
        String personCitizenship;
        String personBenefits;
        Double[] stateExamMarks;
        Double[] enrollmentExamMarks;
        Double totalMark;
        String entrantEnrolmentRecomendations;
        Double certificateAverageMark;

        Double getTotalMark() {
            if (this.totalMark != null) {
                return this.totalMark;
            }
            else {
                Double result = null;

                for (int i = 0; i < this.examMarksNumber; ++i) {
                    Double mark = this.max(this.stateExamMarks[i], this.enrollmentExamMarks[i]);
                    if (!mark.equals(NO_FINAL_MARK)) {
                        if (result == null) {
                            result = mark;
                        }
                        else {
                            result = Double.valueOf(result.doubleValue() + mark.doubleValue());
                        }
                    }
                }

                return this.totalMark = result;
            }
        }

        Double max(Double m1, Double m2) {
            if (m1 == null) {
                m1 = NO_FINAL_MARK;
            }

            if (m2 == null) {
                m2 = NO_FINAL_MARK;
            }

            return Double.valueOf(Math.max(m1.doubleValue(), m2.doubleValue()));
        }

        FieldValues(int examMarksNumber) {
            this.examMarksNumber = examMarksNumber;
            this.stateExamMarks = examMarksNumber != 0 ? new Double[examMarksNumber] : new Double[1];
            this.enrollmentExamMarks = examMarksNumber != 0 ? new Double[examMarksNumber] : new Double[1];
        }

        void writeRow(WritableSheet sheet, int studentNumber, int column, int row, WritableCellFormat fioFormat, Map<String, WritableCellFormat> formatMap) throws Exception {
            WritableCellFormat rowFormat = (WritableCellFormat) formatMap.get("rowFormat");
            sheet.addCell(new Number(column++, row, (double) studentNumber, rowFormat));
            sheet.addCell(new Label(column++, row, this.personFullFio, fioFormat));
            sheet.addCell(new Label(column++, row, this.regNumber, formatMap.get("center")));
            String[] arr$ = new String[]{this.personEducationLevelStage, this.personRegistrationAddress, this.personCitizenship, this.personBenefits};
            int len$ = arr$.length;

            int i$;
            for (i$ = 0; i$ < len$; ++i$) {
                String mark = arr$[i$];
                sheet.addCell(new Label(column++, row, mark, rowFormat));
            }

            Double[] var12 = this.stateExamMarks;
            len$ = var12.length;

            Double var13;
            for (i$ = 0; i$ < len$; ++i$) {
                var13 = var12[i$];
                sheet.addCell((WritableCell) (var13 != null && !var13.equals(NO_FINAL_MARK) ? new Number(column++, row, var13.doubleValue(), (CellFormat) formatMap.get("center")) : new Blank(column++, row, (CellFormat) formatMap.get("center"))));
            }

            var12 = this.enrollmentExamMarks;
            len$ = var12.length;

            for (i$ = 0; i$ < len$; ++i$) {
                var13 = var12[i$];
                sheet.addCell((WritableCell) (var13 != null && !var13.equals(NO_FINAL_MARK) ? new Number(column++, row, var13.doubleValue(), (CellFormat) formatMap.get("center")) : new Blank(column++, row, (CellFormat) formatMap.get("center"))));
            }

            sheet.addCell((WritableCell) (this.getTotalMark() != null ? new Number(column++, row, this.getTotalMark().doubleValue(), (CellFormat) formatMap.get("center")) : new Blank(column++, row, (CellFormat) formatMap.get("center"))));
            sheet.addCell(new Label(column++, row, this.entrantEnrolmentRecomendations, rowFormat));
            sheet.addCell(new Blank(column, row, rowFormat));
        }

        int getRowWidth() {
            return 9 + this.stateExamMarks.length + this.enrollmentExamMarks.length;
        }
    }

    static class TargetAdmission extends EntranceExaminationMeetingReportContentGenerator.Admission {
        Map<TargetAdmissionKind, Map<?, ?>> _structure;
        Map<CompetitionKind, EnrollmentCompetitionKind> competitionKind2EnrollmentCompetitionKind;

        TargetAdmission(WritableSheet sheet, Map<String, WritableCellFormat> formatMap, int tableRowWidth, Map<CompetitionKind, EnrollmentCompetitionKind> competitionKind2EnrollmentCompetitionKind) {
            super(sheet, formatMap, tableRowWidth);
            this._structure = new TreeMap(TargetAdmissionKind.PRIORITY_COMPARATOR);
            this.competitionKind2EnrollmentCompetitionKind = competitionKind2EnrollmentCompetitionKind;
        }

        void put(RequestedEnrollmentDirection requestedEnrollmentDirection) {
            EnrollmentCompetitionKind enrollmentCompetitionKind = (EnrollmentCompetitionKind) this.competitionKind2EnrollmentCompetitionKind.get(requestedEnrollmentDirection.getCompetitionKind());
            TargetAdmissionKind targetAdmissionKind = requestedEnrollmentDirection.getTargetAdmissionKind();
            TargetAdmissionKind parent = targetAdmissionKind != null ? targetAdmissionKind.getParent() : null;
            Map map1;
            Object map2;
            if (parent == null) {
                map1 = (Map) this._structure.get(targetAdmissionKind);
                if (map1 == null) {
                    map1 = new TreeMap();
                    this._structure.put(targetAdmissionKind, map1);
                }

                map2 = (List) ((Map) map1).get(enrollmentCompetitionKind);
                if (map2 == null) {
                    map2 = new ArrayList();
                    ((Map) map1).put(enrollmentCompetitionKind, map2);
                }

                ((List) map2).add(requestedEnrollmentDirection);
            }
            else {
                map1 = (Map) this._structure.get(parent);
                if (map1 == null) {
                    map1 = new TreeMap(TargetAdmissionKind.PRIORITY_COMPARATOR);
                    this._structure.put(parent, map1);
                }

                map2 = (Map) ((Map) map1).get(targetAdmissionKind);
                if (map2 == null) {
                    map2 = new TreeMap();
                    ((Map) map1).put(targetAdmissionKind, map2);
                }

                Object list = (List) ((Map) map2).get(enrollmentCompetitionKind);
                if (list == null) {
                    list = new ArrayList();
                    ((Map) map2).put(enrollmentCompetitionKind, list);
                }

                ((List) list).add(requestedEnrollmentDirection);
            }

        }

        void writeRows(int column, Integer[] row, Integer[] studentHandler) throws Exception {
            WritableCellFormat format1 = new WritableCellFormat((CellFormat) this.formatMap.get("taCompetitionKind"));
            format1.setIndentation(format1.getIndentation() - 1);
            WritableCellFormat format2 = new WritableCellFormat((CellFormat) this.formatMap.get("taFIO"));
            format1.setIndentation(format2.getIndentation() - 1);
            WritableCellFormat var10002 = (WritableCellFormat) this.formatMap.get("ta");
            Integer entry = row[0];
            Integer i$1 = row[0] = Integer.valueOf(row[0].intValue() + 1);
            this.writeTitleRow("ЦЕЛЕВОЙ ПРИЕМ", var10002, column, entry.intValue());
            Iterator i$ = this._structure.entrySet().iterator();

            while (i$.hasNext()) {
                Entry entry1 = (Entry) i$.next();
                String var10001;
                if (entry1.getKey() != null) {
                    var10001 = getTitle(entry1);
                    var10002 = (WritableCellFormat) this.formatMap.get("taKind");
                    Integer entry2 = row[0];
                    Integer i$2 = row[0] = Integer.valueOf(row[0].intValue() + 1);
                    this.writeTitleRow(var10001, var10002, column, entry2.intValue());
                }

                Iterator i$4 = ((Map) entry1.getValue()).entrySet().iterator();

                while (i$4.hasNext()) {
                    Entry entry21 = (Entry) i$4.next();
                    Integer entry3;
                    Integer i$3;
                    Integer fieldValues;
                    Integer var14;
                    Iterator i$5;
                    WritableSheet var25;
                    int var26;
                    if (entry21.getKey() instanceof EnrollmentCompetitionKind) {
                        var10001 = getTitle(entry21);
                        entry3 = row[0];
                        i$3 = row[0] = Integer.valueOf(row[0].intValue() + 1);
                        this.writeTitleRow(var10001, format1, column, entry3.intValue());
                        i$5 = this.getSortedFieldsList((List) entry21.getValue()).iterator();

                        while (i$5.hasNext()) {
                            EntranceExaminationMeetingReportContentGenerator.FieldValues entry32 = (EntranceExaminationMeetingReportContentGenerator.FieldValues) i$5.next();
                            var25 = this.sheet;
                            fieldValues = studentHandler[0];
                            var14 = studentHandler[0] = Integer.valueOf(studentHandler[0].intValue() + 1);
                            var26 = fieldValues.intValue();
                            fieldValues = row[0];
                            var14 = row[0] = Integer.valueOf(row[0].intValue() + 1);
                            entry32.writeRow(var25, var26, column, fieldValues.intValue(), format2, this.formatMap);
                        }
                    }
                    else {
                        if (entry21.getKey() != null) {
                            var10001 = getTitle(entry21);
                            var10002 = (WritableCellFormat) this.formatMap.get("taPlace");
                            entry3 = row[0];
                            i$3 = row[0] = Integer.valueOf(row[0].intValue() + 1);
                            this.writeTitleRow(var10001, var10002, column, entry3.intValue());
                        }

                        i$5 = ((Map) entry21.getValue()).entrySet().iterator();

                        while (i$5.hasNext()) {
                            Entry entry31 = (Entry) i$5.next();
                            var10001 = getTitle(entry31);
                            var10002 = (WritableCellFormat) this.formatMap.get("taCompetitionKind");
                            fieldValues = row[0];
                            var14 = row[0] = Integer.valueOf(row[0].intValue() + 1);
                            this.writeTitleRow(var10001, var10002, column, fieldValues.intValue());
                            Iterator i$6 = this.getSortedFieldsList((List) entry31.getValue()).iterator();

                            while (i$6.hasNext()) {
                                EntranceExaminationMeetingReportContentGenerator.FieldValues fieldValues1 = (EntranceExaminationMeetingReportContentGenerator.FieldValues) i$6.next();
                                var25 = this.sheet;
                                Integer var15 = studentHandler[0];
                                Integer var16 = studentHandler[0] = Integer.valueOf(studentHandler[0].intValue() + 1);
                                var26 = var15.intValue();
                                var15 = row[0];
                                var16 = row[0] = Integer.valueOf(row[0].intValue() + 1);
                                fieldValues1.writeRow(var25, var26, column, var15.intValue(), (WritableCellFormat) this.formatMap.get("taFIO"), this.formatMap);
                            }
                        }
                    }
                }
            }

        }
    }

    static class GeneralAdmission extends EntranceExaminationMeetingReportContentGenerator.Admission {
        Map<EnrollmentCompetitionKind, List<RequestedEnrollmentDirection>> structure = new TreeMap();
        Map<CompetitionKind, EnrollmentCompetitionKind> competitionKind2EnrollmentCompetitionKind;

        GeneralAdmission(WritableSheet sheet, Map<String, WritableCellFormat> formatMap, int tableRowWidth, Map<CompetitionKind, EnrollmentCompetitionKind> competitionKind2EnrollmentCompetitionKind) {
            super(sheet, formatMap, tableRowWidth);
            this.competitionKind2EnrollmentCompetitionKind = competitionKind2EnrollmentCompetitionKind;
        }

        void put(RequestedEnrollmentDirection requestedEnrollmentDirection) {
            EnrollmentCompetitionKind enrollmentCompetitionKind = (EnrollmentCompetitionKind) this.competitionKind2EnrollmentCompetitionKind.get(requestedEnrollmentDirection.getCompetitionKind());
            // от NPE
            if (enrollmentCompetitionKind == null) {
                return;
            }
            List list = (List) this.structure.get(enrollmentCompetitionKind);
            if (list == null) {
                list = new ArrayList();
                this.structure.put(enrollmentCompetitionKind, list);
            }

            ((List) list).add(requestedEnrollmentDirection);
        }

        void writeRows(int column, Integer[] row, Integer[] studentHandler) throws Exception {
            WritableCellFormat var10002 = (WritableCellFormat) this.formatMap.get("ga");
            Integer entry = row[0];
            Integer i$1 = row[0] = Integer.valueOf(row[0].intValue() + 1);
            this.writeTitleRow("ОБЩИЙ ПРИЕМ", var10002, column, entry.intValue());
            Iterator i$ = this.structure.entrySet().iterator();

            while (i$.hasNext()) {
                Entry entry1 = (Entry) i$.next();
                String var10001 = getTitle(entry1);
                var10002 = (WritableCellFormat) this.formatMap.get("gaCompetitionKind");
                Integer fieldValues = row[0];
                Integer var8 = row[0] = Integer.valueOf(row[0].intValue() + 1);
                this.writeTitleRow(var10001, var10002, column, fieldValues.intValue());
                Iterator i$2 = this.getSortedFieldsList((List) entry1.getValue()).iterator();

                while (i$2.hasNext()) {
                    EntranceExaminationMeetingReportContentGenerator.FieldValues fieldValues1 = (EntranceExaminationMeetingReportContentGenerator.FieldValues) i$2.next();
                    WritableSheet var14 = this.sheet;
                    Integer var9 = studentHandler[0];
                    Integer var10 = studentHandler[0] = Integer.valueOf(studentHandler[0].intValue() + 1);
                    int var15 = var9.intValue();
                    var9 = row[0];
                    var10 = row[0] = Integer.valueOf(row[0].intValue() + 1);
                    fieldValues1.writeRow(var14, var15, column, var9.intValue(), (WritableCellFormat) this.formatMap.get("gaFIO"), this.formatMap);
                }
            }

        }
    }

    abstract static class Admission {
        static final Comparator<FieldValues> COMPARATOR = new Comparator<FieldValues>() {
            public int compare(EntranceExaminationMeetingReportContentGenerator.FieldValues o1, EntranceExaminationMeetingReportContentGenerator.FieldValues o2) {
                int result = -UniBaseUtils.compare(o1.getTotalMark(), o2.getTotalMark(), true);
                if (result == 0) {
                    result = -UniBaseUtils.compare(o1.profileMark, o2.profileMark, true);
                }

                if (result == 0) {
                    result = -UniBaseUtils.compare(o1.certificateAverageMark, o2.certificateAverageMark, true);
                }

                if (result == 0) {
                    result = o1.personFullFio.compareToIgnoreCase(o2.personFullFio);
                }

                return result;
            }
        };
        Map<RequestedEnrollmentDirection, FieldValues> direction2Fields;
        WritableSheet sheet;
        Map<String, WritableCellFormat> formatMap;
        WritableCellFormat rowFormat;
        int tableRowWidth;

        protected Admission(WritableSheet sheet, Map<String, WritableCellFormat> formatMap, int tableRowWidth) {
            this.sheet = sheet;
            this.formatMap = formatMap;
            this.rowFormat = (WritableCellFormat) formatMap.get("rowFormat");
            this.tableRowWidth = tableRowWidth;
        }

        abstract void put(RequestedEnrollmentDirection var1);

        abstract void writeRows(int var1, Integer[] var2, Integer[] var3) throws Exception;

        static String getTitle(Entry<?, ?> entry) {
            return ((ITitled) entry.getKey()).getTitle();
        }

        List<FieldValues> getSortedFieldsList(List<RequestedEnrollmentDirection> requestedEnrollmentDirections) {
            ArrayList result = new ArrayList();
            Iterator i$ = requestedEnrollmentDirections.iterator();

            while (i$.hasNext()) {
                RequestedEnrollmentDirection direction = (RequestedEnrollmentDirection) i$.next();
                result.add(this.direction2Fields.get(direction));
            }

            Collections.sort(result, COMPARATOR);
            return result;
        }

        void writeTitleRow(String title, WritableCellFormat format, int column, int row) throws Exception {
            this.sheet.addCell(new Blank(column, row, this.rowFormat));
            this.sheet.addCell(new Label(column + 1, row, title, format));

            for (int i = 2; i <= this.tableRowWidth; ++i) {
                this.sheet.addCell(new Blank(column + i, row, this.rowFormat));
            }

        }
    }
}
