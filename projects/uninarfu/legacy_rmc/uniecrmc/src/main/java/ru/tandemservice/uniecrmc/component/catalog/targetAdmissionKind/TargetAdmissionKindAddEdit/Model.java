package ru.tandemservice.uniecrmc.component.catalog.targetAdmissionKind.TargetAdmissionKindAddEdit;

import ru.tandemservice.uniecrmc.entity.entrant.TargetAdmissionKindRMC;

public class Model extends ru.tandemservice.uniec.component.catalog.targetAdmissionKind.TargetAdmissionKindAddEdit.Model {
    public Model()
    {

    }

    private TargetAdmissionKindRMC _targetAdmissionKindRMC;

    public TargetAdmissionKindRMC getTargetAdmissionKindRMC()
    {
        return _targetAdmissionKindRMC;
    }

    public void setTargetAdmissionKindRMC(TargetAdmissionKindRMC targetAdmissionKindRMC)
    {
        _targetAdmissionKindRMC = targetAdmissionKindRMC;
    }
}
