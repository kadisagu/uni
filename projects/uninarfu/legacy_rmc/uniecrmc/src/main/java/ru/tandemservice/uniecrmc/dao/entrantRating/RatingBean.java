package ru.tandemservice.uniecrmc.dao.entrantRating;


import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import ru.tandemservice.uniecrmc.entity.entrant.EntrantRecommendedSettings;
import ru.tandemservice.uni.dao.UniBaseDao;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class RatingBean extends UniBaseDao implements IRatingBean {
    protected static IRatingBean INSTANCE;

    ExecutorService service = Executors.newFixedThreadPool(99);
    protected Map<Long, Task> taskMap = new LinkedHashMap<>();

    public static IRatingBean instance() {
        if (INSTANCE == null)
            INSTANCE = (IRatingBean) ApplicationRuntime.getBean(IRatingBean.class.getName());
        return INSTANCE;
    }

    public static final SyncDaemon DAEMON = new SyncDaemon(IRatingBean.class.getName(), 1 /* 1 раз в минуту*/, IRatingBean.class.getName()) {
        protected void main() {
            String isRun = ApplicationRuntime.getProperty("debug.rundemon");
            if (StringUtils.isEmpty(isRun) || !Boolean.parseBoolean(isRun))
                return;

            instance().checkTime();
        }
    };

    @Override
    public boolean isActive(Long enrollmentCampaignId) {
        Task task = taskMap.get(enrollmentCampaignId);
        return task != null && task.isActive();
    }

    @Override
    public synchronized void runNow(Long enrollmentCampaignId) {
        synchronized (taskMap) {
            Task task = taskMap.get(enrollmentCampaignId);

            if (task == null) {
                EntrantRecommendedSettings sett = getNotNull(EntrantRecommendedSettings.class, EntrantRecommendedSettings.enrollmentCampaign().id(), enrollmentCampaignId);
                taskMap.put(enrollmentCampaignId, task = new Task(enrollmentCampaignId, sett.getInterval(), sett.getPath(), sett.isUseDemon()));
                service.submit(task);
            }

            task.runNow();
        }
    }

    @Override
    public void checkTime() {
        synchronized (taskMap) {
            List<EntrantRecommendedSettings> list = getList(EntrantRecommendedSettings.class);
            for (EntrantRecommendedSettings sett : list) {
                Long ecId = sett.getEnrollmentCampaign().getId();

                Task task = taskMap.get(sett.getEnrollmentCampaign().getId());
                if (task == null) {
                    taskMap.put(ecId, task = new Task(ecId, sett.getInterval(), sett.getPath(), sett.isUseDemon()));
                    service.submit(task);
                }
                else
                    task.setParams(sett.getInterval(), sett.getPath(), sett.isUseDemon());
            }
        }
    }

}
