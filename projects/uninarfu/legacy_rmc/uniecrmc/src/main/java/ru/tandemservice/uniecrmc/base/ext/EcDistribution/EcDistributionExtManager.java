package ru.tandemservice.uniecrmc.base.ext.EcDistribution;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.logic.EcDistributionExtDao;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.logic.IEcDistributionExtDao;
import ru.tandemservice.uniec.base.bo.EcDistribution.EcDistributionManager;
import ru.tandemservice.uniec.base.bo.EcDistribution.logic.IEcDistributionDao;

@Configuration
public class EcDistributionExtManager extends BusinessObjectExtensionManager {

    public static EcDistributionExtManager instance() {
        return instance(EcDistributionExtManager.class);
    }

    public static IEcDistributionExtDao daoRMC()
    {
        return (IEcDistributionExtDao) EcDistributionManager.instance().dao();
    }

    @Bean
    @BeanOverride
    public IEcDistributionDao dao()
    {
        // БЛЯТТТТТЬЬЬ какая то хорошая душонка накакала где не надо <vch>
        return new EcDistributionExtDao();
        //daoRMC();
    }
}
