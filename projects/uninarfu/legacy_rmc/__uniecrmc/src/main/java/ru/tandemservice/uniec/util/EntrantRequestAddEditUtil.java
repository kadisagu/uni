package ru.tandemservice.uniec.util;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BooleanColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.InExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionRestrictionRMC;
import ru.tandemservice.uniecrmc.entity.entrant.EntrantRecommendedSettings;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.CompetitionGroupRestriction;
import ru.tandemservice.uniec.entity.settings.EnrollmentDirectionRestriction;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class EntrantRequestAddEditUtil {
    public static final String REQUESTED_DIRECTIONS_COLUMN = "direction";

    public static DynamicListDataSource<RequestedEnrollmentDirection> getSelectedRequestedEnrollmentDirectionDataSource(IBusinessComponent component, IListDataSourceDelegate delegate)
    {
        DynamicListDataSource<RequestedEnrollmentDirection> dataSource = new DynamicListDataSource<>(component, delegate);
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", new String[]{"enrollmentDirection", "educationOrgUnit", "formativeOrgUnit", "shortTitle"}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("direction", new String[]{"enrollmentDirection", "educationOrgUnit", "educationLevelHighSchool", "displayableTitle"}, "Направление подготовки (специальность)").setClickable(false).setOrderable(false));

        dataSource.addColumn(new SimpleColumn("Форма освоения", new String[]{"enrollmentDirection", "educationOrgUnit", "developForm", "title"}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Вид конкурса", new String[]{"competitionKind", "title"}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Контракт", new String[]{"compensationType", "shortTitle"}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new BooleanColumn("Сданы оригиналы", "originalDocumentHandedIn"));
        dataSource.addColumn(new ActionColumn("Повысить приоритет", "up", "onClickUp").setOrderable(false));
        dataSource.addColumn(new ActionColumn("Понизить приоритет", "down", "onClickDown").setOrderable(false));
        dataSource.addColumn(new ActionColumn("Удалить", "delete", "onClickDelete").setOrderable(false));
        return dataSource;
    }

    public static void processClickUp(List<RequestedEnrollmentDirection> list, Long id)
    {
        int i = 0;
        while ((i < list.size()) && (!((RequestedEnrollmentDirection) list.get(i)).getId().equals(id)))
            i++;
        if ((i == list.size()) || (i == 0))
            return;
        RequestedEnrollmentDirection item = (RequestedEnrollmentDirection) list.get(i - 1);
        list.set(i - 1, list.get(i));
        list.set(i, item);
    }

    public static void processClickDown(List<RequestedEnrollmentDirection> list, Long id)
    {
        int i = 0;
        while ((i < list.size()) && (!((RequestedEnrollmentDirection) list.get(i)).getId().equals(id)))
            i++;
        if ((i == list.size()) || (i == list.size() - 1))
            return;
        RequestedEnrollmentDirection item = (RequestedEnrollmentDirection) list.get(i + 1);
        list.set(i + 1, list.get(i));
        list.set(i, item);
    }

    public static RequestedEnrollmentDirection processClickDelete(List<RequestedEnrollmentDirection> list, Long id, Set<Long> existedIds, List<RequestedEnrollmentDirection> forDeleteList)
    {
        int i = 0;
        while ((i < list.size()) && (!((RequestedEnrollmentDirection) list.get(i)).getId().equals(id)))
            i++;
        if (i == list.size())
            return null;
        RequestedEnrollmentDirection forDelete = (RequestedEnrollmentDirection) list.get(i);
        if (existedIds.contains(forDelete.getId()))
            forDeleteList.add(forDelete);
        list.remove(i);
        return forDelete;
    }

    public static List<MultiKey> getAddingEnrollmentDirections(Session session, List<EnrollmentDirection> directionForAddingList, CompensationType compensationType, List<RequestedEnrollmentDirection> selectedRequestedEnrollmentDirectionList, boolean directionPerCompTypeDiff, boolean stateExamRestriction, Entrant entrant, List<RequestedEnrollmentDirection> forDeleteList)
    {
        List<MultiKey> result = new ArrayList<>();
        CompensationType budget = (CompensationType) UniDaoFacade.getCoreDao().getCatalogItem(CompensationType.class, "1");
        CompensationType contract = (CompensationType) UniDaoFacade.getCoreDao().getCatalogItem(CompensationType.class, "2");

        for (EnrollmentDirection item : directionForAddingList) {
            if (compensationType != null) {
                if (((item.isBudget()) && (compensationType.isBudget())) || ((item.isContract()) && (!compensationType.isBudget())))
                    result.add(new MultiKey(item, compensationType));
            }
            else {
                if (item.isBudget())
                    result.add(new MultiKey(item, budget));
                if (item.isContract()) {
                    result.add(new MultiKey(item, contract));
                }
            }

        }

        Set selected = new HashSet();
        for (RequestedEnrollmentDirection reqItem : selectedRequestedEnrollmentDirectionList) {
            if (directionPerCompTypeDiff) {
                selected.add(new MultiKey(reqItem.getEnrollmentDirection(), reqItem.getCompensationType()));
            }
            else {
                selected.add(new MultiKey(reqItem.getEnrollmentDirection(), budget));
                selected.add(new MultiKey(reqItem.getEnrollmentDirection(), contract));
            }

        }

        MQBuilder builder = new MQBuilder("ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection", "r");
        builder.add(MQExpression.eq("r", "entrantRequest.entrant", entrant));
        if (forDeleteList.size() > 0) {
            builder.add(new InExpression("r", forDeleteList.toArray(new RequestedEnrollmentDirection[forDeleteList.size()]), "not in"));
        }
        List<RequestedEnrollmentDirection> list = builder.getResultList(session);
        for (RequestedEnrollmentDirection reqItem : list) {
            if (directionPerCompTypeDiff) {
                selected.add(new MultiKey(reqItem.getEnrollmentDirection(), reqItem.getCompensationType()));
            }
            else {
                selected.add(new MultiKey(reqItem.getEnrollmentDirection(), budget));
                selected.add(new MultiKey(reqItem.getEnrollmentDirection(), contract));
            }

        }

        result.removeAll(selected);
        Set coveredList;
        Iterator iterator;
        if (stateExamRestriction) {
            coveredList = UniecDAOFacade.getExamSetDAO().getCoveredEnrollmentDirection(entrant);

            for (iterator = result.iterator(); iterator.hasNext(); ) {
                if (!coveredList.contains(((MultiKey) iterator.next()).getKey(0)))
                    iterator.remove();
            }
        }
        return result;
    }

    public static void mergeSelectedDirections(List<RequestedEnrollmentDirection> alreadySelected, List<RequestedEnrollmentDirection> justSelected)
    {
        List budgetList = new ArrayList();
        List contractList = new ArrayList();
        for (RequestedEnrollmentDirection item : justSelected) {
            if (item.getCompensationType().isBudget())
                budgetList.add(item);
            else {
                contractList.add(item);
            }
        }
        int lastBudgetIndex = alreadySelected.size() - 1;
        while ((lastBudgetIndex >= 0) && (!((RequestedEnrollmentDirection) alreadySelected.get(lastBudgetIndex)).getCompensationType().isBudget())) {
            lastBudgetIndex--;
        }

        alreadySelected.addAll(lastBudgetIndex + 1, budgetList);

        int lastContractIndex = alreadySelected.size() - 1;
        while ((lastContractIndex >= 0) && (((RequestedEnrollmentDirection) alreadySelected.get(lastContractIndex)).getCompensationType().isBudget())) {
            lastContractIndex--;
        }

        if (lastContractIndex < 0) lastContractIndex = alreadySelected.size() - 1;
        alreadySelected.addAll(lastContractIndex + 1, contractList);
    }

    public static boolean validateRequestedDirections(Session session, List<RequestedEnrollmentDirection> directions, EntrantRequest entrantRequest)
    {
        return validateRequestedDirections(session, directions, Collections.<RequestedEnrollmentDirection>emptyList(), entrantRequest);
    }

    public static boolean validateRequestedDirections(Session session, List<RequestedEnrollmentDirection> directions, List<RequestedEnrollmentDirection> forDelete, EntrantRequest entrantRequest) {
        EnrollmentCampaign ec = entrantRequest.getEntrant().getEnrollmentCampaign();
        EntrantRecommendedSettings settings = IUniBaseDao.instance.get().get(EntrantRecommendedSettings.class, EntrantRecommendedSettings.enrollmentCampaign(), entrantRequest.getEntrant().getEnrollmentCampaign());

        boolean ok = validateRequestedDirections_old(session, directions, forDelete, entrantRequest);
        if (settings == null || !settings.isUseStructureLevelInRestriction())
            return ok;


        ErrorCollector errorCollector = ContextLocal.getUserContext().getErrorCollector();

        //проверка по EnrollmentDirectionRestrictionRMC
        //все направления
        List<RequestedEnrollmentDirection> all = IUniBaseDao.instance.get().getList(RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.entrantRequest(), entrantRequest);
        for (RequestedEnrollmentDirection d : directions)
            if (!all.contains(d))
                all.add(d);
        //all.addAll(directions);
        all.removeAll(forDelete);

        //подбиваем сумму по каждой "группе"
        Map<MultiKey, AtomicInteger> map = new HashMap<MultiKey, AtomicInteger>();
        Map<MultiKey, Set<OrgUnit>> mapOU = new HashMap<MultiKey, Set<OrgUnit>>();
        for (RequestedEnrollmentDirection red : all) {
            CompetitionGroup competitionGroup = red.getEnrollmentDirection().getCompetitionGroup();
            if (ec.isUseCompetitionGroup() && competitionGroup == null)
                continue;


            MultiKey key = getKeyRED(
                    red.getEnrollmentDirection().getEducationOrgUnit().getDevelopForm(),
                    red.getCompensationType(),
                    red.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType());

            if (!map.containsKey(key))
                map.put(key, new AtomicInteger(0));
            map.get(key).incrementAndGet();

            if (mapOU.get(key) == null)
                mapOU.put(key, new HashSet<OrgUnit>());
            mapOU.get(key).add(red.getEnrollmentDirection().getEducationOrgUnit().getFormativeOrgUnit());

        }

        List<EnrollmentDirectionRestrictionRMC> restrictionList = IUniBaseDao.instance.get().getList(EnrollmentDirectionRestrictionRMC.class, EnrollmentDirectionRestrictionRMC.enrollmentCampaign(), ec);
        for (EnrollmentDirectionRestrictionRMC restriction : restrictionList) {
            MultiKey key = getKeyRED(restriction.getDevelopForm(), restriction.getCompensationType(), restriction.getStructureEducationLevels());
            AtomicInteger value = map.get(key);
            if (value == null)
                continue;

            if (!ec.isUseCompetitionGroup()) {
                if ((restriction.getMaxEnrollmentDirection() != null) && value.get() > restriction.getMaxEnrollmentDirection().intValue())
                    errorCollector.add(new StringBuilder()
                                               .append("Нельзя выбрать абитуриенту указанные направления подготовки (специальности), т.к. для формы освоения (")
                                               .append(restriction.getDevelopForm().getTitle())
                                               .append("), вида возмещения затрат (")
                                               .append(restriction.getCompensationType().getShortTitle())
                                               .append(") и уровня образования ")
                                               .append(restriction.getStructureEducationLevels().getTitle())
                                               .append(") общее число выбранных абитуриентом направлений подготовки для приема не может превышать ")
                                               .append(restriction.getMaxEnrollmentDirection())
                                               .toString()
                    );
                if (restriction.getMaxFormativeOrgUnit() != null && mapOU.get(key).size() > restriction.getMaxFormativeOrgUnit())
                    errorCollector.add("Нельзя выбрать абитуриенту указанное направление подготовки (специальность), т.к. для заданных формы освоения и вида возмещения затрат (" + restriction.getDevelopForm().getTitle() + ", " + restriction.getCompensationType().getShortTitle() + ") общее число выбранных абитуриентом формирующих подразделений не может превышать " + restriction.getMaxFormativeOrgUnit());

            }
            else {
                if (restriction.getMaxCompetitionGroup() == null)
                    continue;
                if (value.get() > restriction.getMaxCompetitionGroup().intValue())
                    errorCollector.add(new StringBuilder()
                                               .append("Нельзя выбрать абитуриенту указанные направления подготовки (специальности), т.к. для формы освоения (")
                                               .append(restriction.getDevelopForm().getTitle())
                                               .append("), вида возмещения затрат (")
                                               .append(restriction.getCompensationType().getShortTitle())
                                               .append(") и уровня образования ")
                                               .append(restriction.getStructureEducationLevels().getTitle())
                                               .append(") общее число выбранных конкурсных групп для абитуриента не может превышать ")
                                               .append(restriction.getMaxCompetitionGroup())
                                               .toString());
            }
        }

        return !errorCollector.isHasFieldErrors();
    }


    public static MultiKey getKeyRED(DevelopForm developForm, CompensationType compensationType, StructureEducationLevels level) {
        while (level.getParent() != null)
            level = level.getParent();

        return new MultiKey(developForm, compensationType, level);
    }

    public static boolean validateRequestedDirections_old(Session session, List<RequestedEnrollmentDirection> directions, List<RequestedEnrollmentDirection> forDelete, EntrantRequest entrantRequest)
    {
        EnrollmentCampaign ec = entrantRequest.getEntrant().getEnrollmentCampaign();
        ErrorCollector errorCollector = ContextLocal.getUserContext().getErrorCollector();

        Set<RequestedEnrollmentDirection> theoreticalDirectionSet = new HashSet();
        if (entrantRequest.getId() != null)
            theoreticalDirectionSet.addAll(session.createCriteria(RequestedEnrollmentDirection.class).add(Restrictions.eq("entrantRequest", entrantRequest)).list());
        theoreticalDirectionSet.addAll(directions);
        theoreticalDirectionSet.removeAll(forDelete);
        Map key2data;
        if (ec.isUseCompetitionGroup()) {
            Set competitionGroupSet = new HashSet();
            for (RequestedEnrollmentDirection item : theoreticalDirectionSet) {
                if (item.getEnrollmentDirection().getCompetitionGroup() != null)
                    competitionGroupSet.add(item.getEnrollmentDirection().getCompetitionGroup());
            }
            if ((ec.getMaxCompetitionGroup() != null) && (competitionGroupSet.size() > ec.getMaxCompetitionGroup().intValue())) {
                errorCollector.add("Нельзя выбрать абитуриенту указанные направления подготовки (специальности), т.к. общее число выбранных для абитуриента конкурсных групп в заявлении не может превышать " + ec.getMaxCompetitionGroup() + ".");
            }

            key2data = new HashMap();
            for (RequestedEnrollmentDirection item : theoreticalDirectionSet) {
                CompetitionGroup competitionGroup = item.getEnrollmentDirection().getCompetitionGroup();
                if (competitionGroup != null) {
                    MultiKey key = new MultiKey(item.getEnrollmentDirection().getEducationOrgUnit().getDevelopForm(), item.getCompensationType());
                    Set set = (Set) key2data.get(key);
                    if (set == null)
                        key2data.put(key, set = new HashSet());
                    set.add(competitionGroup);
                }

            }

            List<CompetitionGroupRestriction> restrictionList = session.createCriteria(CompetitionGroupRestriction.class).add(Restrictions.eq("enrollmentCampaign", ec)).list();
            for (CompetitionGroupRestriction restriction : restrictionList) {
                session.refresh(restriction);
                MultiKey key = new MultiKey(restriction.getDevelopForm(), restriction.getCompensationType());
                Set set = (Set) key2data.get(key);
                if (set != null) {
                    if ((restriction.getMaxCompetitionGroup() != null) && (set.size() > restriction.getMaxCompetitionGroup().intValue()))
                        errorCollector.add("Нельзя выбрать абитуриенту указанные направления подготовки (специальности), т.к. для формы освоения (" + restriction.getDevelopForm().getTitle() + ") и вида возмещения затрат (" + restriction.getCompensationType().getShortTitle() + ") общее число выбранных конкурсных групп для абитуриента не может превышать " + restriction.getMaxCompetitionGroup() + ".");
                }
            }
        }
        else {
            Map direction2ministerialDirectionMap = new HashMap();
            Set enrollmentDirectionSet = new HashSet();
            Set ministerialDirectionSet = new HashSet();
            for (RequestedEnrollmentDirection item : theoreticalDirectionSet) {
                EducationLevels eduLevel = item.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();
                while ((eduLevel != null) && (!eduLevel.getLevelType().isGroupingLevel()))
                    eduLevel = eduLevel.getParentLevel();
                if (eduLevel == null)
                    throw new RuntimeException("Can't find ministerial direction for RequestedEnrollmentDirection.id='" + item.getId() + "'");
                enrollmentDirectionSet.add(item.getEnrollmentDirection());
                ministerialDirectionSet.add(eduLevel);
                direction2ministerialDirectionMap.put(item, eduLevel);
            }

            int ENROLLMENT_DIRECTION_INDEX = 0;
            int MINISTERIAL_DIRECTION_INDEX = 1;
            int FORMATIVE_ORGUNIT_INDEX = 2;
            key2data = new HashMap();
            for (RequestedEnrollmentDirection item : theoreticalDirectionSet) {
                MultiKey key = new MultiKey(item.getEnrollmentDirection().getEducationOrgUnit().getDevelopForm(), item.getCompensationType());
                Set[] data = (Set[]) key2data.get(key);
                if (data == null) {
                    key2data.put(key, data = new Set[]{new HashSet(), new HashSet(), new HashSet()});
                }
                EducationLevels eduLevel = (EducationLevels) direction2ministerialDirectionMap.get(item);
                if (eduLevel == null) {
                    throw new RuntimeException("Can't find ministerial direction for RequestedEnrollmentDirection.id='" + item.getId() + "'");
                }
                data[0].add(item.getEnrollmentDirection());
                data[1].add(eduLevel);
                data[2].add(item.getEnrollmentDirection().getEducationOrgUnit().getFormativeOrgUnit());
            }

            if ((ec.getMaxEnrollmentDirection() != null) && (enrollmentDirectionSet.size() > ec.getMaxEnrollmentDirection().intValue())) {
                errorCollector.add("Нельзя выбрать абитуриенту указанное направление подготовки (специальность), т.к. общее число выбранных абитуриентом направлений подготовки для приема в заявлении не может превышать " + ec.getMaxEnrollmentDirection() + ".");
            }

            if ((ec.getMaxMinisterialDirection() != null) && (ministerialDirectionSet.size() > ec.getMaxMinisterialDirection().intValue())) {
                errorCollector.add("Нельзя выбрать абитуриенту указанное направление подготовки (специальность), т.к. общее число выбранных абитуриентом направлений подготовки по классификатору в заявлении не может превышать " + ec.getMaxMinisterialDirection() + ".");
            }

            List<EnrollmentDirectionRestriction> restrictionList = session.createCriteria(EnrollmentDirectionRestriction.class).add(Restrictions.eq("enrollmentCampaign", ec)).list();
            for (EnrollmentDirectionRestriction restriction : restrictionList) {
                session.refresh(restriction);
                MultiKey key = new MultiKey(restriction.getDevelopForm(), restriction.getCompensationType());
                Set[] data = (Set[]) key2data.get(key);
                if (data != null) {
                    if ((restriction.getMaxEnrollmentDirection() != null) && (data[0].size() > restriction.getMaxEnrollmentDirection().intValue())) {
                        errorCollector.add("Нельзя выбрать абитуриенту указанное направление подготовки (специальность), т.к. для заданных формы освоения и вида возмещения затрат (" + restriction.getDevelopForm().getTitle() + ", " + restriction.getCompensationType().getShortTitle() + ") общее число выбранных абитуриентом направлений подготовки для приема не может превышать " + restriction.getMaxEnrollmentDirection() + ".");
                    }
                    if ((restriction.getMaxMinisterialDirection() != null) && (data[1].size() > restriction.getMaxMinisterialDirection().intValue())) {
                        errorCollector.add("Нельзя выбрать абитуриенту указанное направление подготовки (специальность), т.к. для заданных формы освоения и вида возмещения затрат (" + restriction.getDevelopForm().getTitle() + ", " + restriction.getCompensationType().getShortTitle() + ") общее число выбранных абитуриентом направлений подготовки по классификатору не может превышать " + restriction.getMaxMinisterialDirection() + ".");
                    }
                    if ((restriction.getMaxFormativeOrgUnit() != null) && (data[2].size() > restriction.getMaxFormativeOrgUnit().intValue())) {
                        errorCollector.add("Нельзя выбрать абитуриенту указанное направление подготовки (специальность), т.к. для заданных формы освоения и вида возмещения затрат (" + restriction.getDevelopForm().getTitle() + ", " + restriction.getCompensationType().getShortTitle() + ") общее число выбранных абитуриентом формирующих подразделений не может превышать " + restriction.getMaxFormativeOrgUnit() + ".");
                    }
                }
            }
        }

        DQLSelectBuilder builder = (DQLSelectBuilder) ((DQLSelectBuilder) new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "e")).where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().entrant().fromAlias("e")), DQLExpressions.value(entrantRequest.getEntrant())));

        if (entrantRequest.getId() != null) {
            builder.where(DQLExpressions.ne(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().fromAlias("e")), DQLExpressions.value(entrantRequest)));
        }
        Set<RequestedEnrollmentDirection> theoreticalFullDirectionSet = new HashSet();
        theoreticalFullDirectionSet.addAll(theoreticalDirectionSet);
        List<RequestedEnrollmentDirection> list = builder.createStatement(session).list();
        theoreticalFullDirectionSet.addAll(list);

        if (ec.isOneDirectionForTargetAdmission()) {
            int count = 0;
            for (RequestedEnrollmentDirection item : theoreticalFullDirectionSet) {
                if (item.isTargetAdmission())
                    count++;
            }
            if (count > 1) {
                errorCollector.add("Нельзя выбрать абитуриенту более одного направления подготовки (специальности) с условием поступления по целевому приему.");
            }
        }

        if (ec.isOneDirectionForOutOfCompetition()) {
            int count = 0;
            for (RequestedEnrollmentDirection item : theoreticalFullDirectionSet) {
                if ("3".equals(item.getCompetitionKind().getCode()))
                    count++;
            }
            if (count > 1) {
                errorCollector.add("Нельзя выбрать абитуриенту более одного направления подготовки (специальности) с условием поступления по внеконкурсному приему.");
            }
        }
        return !errorCollector.hasErrors();
    }
}