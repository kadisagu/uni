package ru.tandemservice.uniec.component.report.EntrantResidenceDistrib.Add;

import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.RtfParagraph;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressDistrict;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.fias.base.entity.AddressRu;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.*;

public class EntrantResidenceDistribReportBuilder {
    static final int COMP_TYPE_ID = 0;
    static final int SETTLEMENT_ID = 1;
    static final int DISTRICT_ID = 2;
    static final int COUNT = 3;
    static final int EDU_OU = 4;
    static final int BUDGET_IDX = 0;
    static final int CONTRACT_IDX = 1;
    private Model _model;
    private Session _session;

    EntrantResidenceDistribReportBuilder(Model model, Session session) {
        this._model = model;
        this._session = session;
    }

    DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private byte[] buildReport()
    {
        RtfDocument document = loadTemplate();
        RtfDocument clone = document.getClone();
        Iterator iterator;
        if (this._model.isSeparateEduOrgUnit()) {
            RtfSearchResult searchResult = UniRtfUtil.findRtfTableMark(document, "T");
            document.getElementList().remove(searchResult.getIndex());

            writeCommonHeader(document);

            Map<EducationOrgUnit, List<Object[]>> eduOU2dataMap = getDataByEduOrgUnit();

            for (iterator = eduOU2dataMap.entrySet().iterator(); iterator.hasNext(); ) {
                Map.Entry entry = (Map.Entry) iterator.next();

                ReportDataModel reportDataModel = prepareReportDataModel((List) entry.getValue());

                writeTableByEduOrgUnit((EducationOrgUnit) entry.getKey(), reportDataModel, document, clone);
                if (iterator.hasNext())
                    document.getElementList().add(RtfBean.getElementFactory().createRtfControl(1073));
            }
        }
        else {
            ReportDataModel reportDataModel = prepareReportDataModel(getData());

            writeCommonHeader(document);
            writeTable(reportDataModel, document, clone);
        }

        return RtfUtil.toByteArray(document);
    }

    protected RtfDocument writeTableByEduOrgUnit(EducationOrgUnit eduOrgUnit, ReportDataModel reportDataModel, RtfDocument document, RtfDocument emptyClone)
    {
        RtfDocument clone = emptyClone.getClone();

        clone.getElementList().remove(1);
        clone.getElementList().remove(1);
        clone.getElementList().remove(1);
        Iterator iterator;
        if ((clone.getElementList().get(0) != null) && (((IRtfElement) clone.getElementList().get(0)).toString().contains("vuz")) && ((clone.getElementList().get(0) instanceof RtfParagraph))) {
            for (iterator = ((RtfParagraph) clone.getElementList().get(0)).getElementList().iterator(); iterator.hasNext(); ) {
                if (((IRtfElement) iterator.next()).toString().contains("\\qc")) {
                    iterator.remove();
                }
            }
        }

        RtfString eduOUTitle = new RtfString().append(98, 0).append(1292).append(eduOrgUnit.getEducationLevelHighSchool().getFullTitle()).append(" (").append(eduOrgUnit.getDevelopCombinationFullTitle()).append(")").append(716).append(eduOrgUnit.getFormativeOrgUnit().getNominativeCaseTitle()).append(" (").append(eduOrgUnit.getTerritorialOrgUnit().getTerritorialFullTitle()).append(")");

        new RtfInjectModifier().put("vuz", eduOUTitle).modify(clone);

        RtfDocument table = writeTable(reportDataModel, clone, emptyClone);
        document.getElementList().add(table);

        return document;
    }

    protected RtfDocument writeTable(final ReportDataModel reportDataModel, RtfDocument document, RtfDocument emptyClone)
    {
        RtfDocument clone = emptyClone.getClone();

        final int blackIndex = document.getHeader().getColorTable().addColor(0, 0, 0);
        final int grayIndex = document.getHeader().getColorTable().addColor(153, 153, 153);

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("S1", Integer.toString(reportDataModel.getSum()[0]));
        injectModifier.put("S2", Integer.toString(reportDataModel.getSum()[1]));
        injectModifier.put("S3", Integer.toString(reportDataModel.getSum()[0] + reportDataModel.getSum()[1]));
        injectModifier.put("S4", "100");
        injectModifier.modify(document);

        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T", new String[0][]);
        tableModifier.put("T", new RtfRowIntercepterBase() {
            public void beforeModify(RtfTable table, int currentRowIndex)
            {
                RtfRow templateRow = (RtfRow) table.getRowList().get(currentRowIndex);

                List<RtfRow> newRowList = new ArrayList<RtfRow>();

                if (reportDataModel.getAcademyCity() != null) {
                    if ((reportDataModel.getHighSchoolCity()[0] != 0) || (reportDataModel.getHighSchoolCity()[1] != 0)) {
                        newRowList.add(EntrantResidenceDistribReportBuilder.createRow(templateRow, 0, blackIndex, reportDataModel.getAcademyCity().getTitleWithType(), reportDataModel.getHighSchoolCity()[0], reportDataModel.getHighSchoolCity()[1], reportDataModel.getTotalSum(), null, null));
                    }

                    for (AddressDistrict district : reportDataModel.getDistrictList()) {
                        int[] value = (int[]) reportDataModel.getDistrict2count().get(district);
                        newRowList.add(EntrantResidenceDistribReportBuilder.createRow(templateRow, 181, grayIndex, district.getTitle(), value[0], value[1], reportDataModel.getTotalSum(), null, null));
                    }

                    if ((reportDataModel.getHighSchoolRegion()[0] != 0) || (reportDataModel.getHighSchoolRegion()[1] != 0)) {
                        newRowList.add(EntrantResidenceDistribReportBuilder.createRow(templateRow, 0, blackIndex, new StringBuilder().append(reportDataModel.getAcademyRegion().getTitleWithType()).append(reportDataModel.getAcademyRegion().equals(reportDataModel.getAcademyCity()) ? " (регион)" : "").toString(), reportDataModel.getHighSchoolRegion()[0], reportDataModel.getHighSchoolRegion()[1], reportDataModel.getTotalSum(), reportDataModel.getAcademyRegion().getId(), reportDataModel.getArea2percentMap()));
                    }
                }

                for (AddressItem area : reportDataModel.getAreaList()) {
                    int[] value = (int[]) reportDataModel.getArea2count().get(area);
                    newRowList.add(EntrantResidenceDistribReportBuilder.createRow(templateRow, 0, blackIndex, area.getTitleWithType(), value[0], value[1], reportDataModel.getTotalSum(), area.getId(), reportDataModel.getArea2percentMap()));
                }

                if ((reportDataModel.getForeignCountry()[0] != 0) || (reportDataModel.getForeignCountry()[1] != 0)) {
                    newRowList.add(EntrantResidenceDistribReportBuilder.createRow(templateRow, 0, blackIndex, "Другие государства", reportDataModel.getForeignCountry()[0], reportDataModel.getForeignCountry()[1], reportDataModel.getTotalSum(), null, null));
                }
                table.getRowList().addAll(currentRowIndex + 1, newRowList);
            }
        });
        tableModifier.modify(document);

        if (this._model.getDetail().isTrue()) {
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(1078));
            clone.getElementList().remove(1);
            clone.getElementList().remove(1);

            if (reportDataModel.getArea2city2count().containsKey(reportDataModel.getAcademyRegion())) {
                reportDataModel.getAreaList().add(0, reportDataModel.getAcademyRegion());
            }
            for (AddressItem area : reportDataModel.getAreaList()) {
                final Map<AddressItem, int[]> map = reportDataModel.getArea2city2count().get(area);

                if (map != null) {
                    int[] subSum = new int[2];
                    for (int[] value : map.values()) {
                        subSum[0] += value[0];
                        subSum[1] += value[1];
                    }

                    final List<AddressItem> cityList = new ArrayList<AddressItem>(map.keySet());
                    Collections.sort(cityList, ITitled.TITLED_COMPARATOR);

                    RtfDocument doc = clone.getClone();

                    RtfInjectModifier im = new RtfInjectModifier();
                    im.put("vuz", area.getTitleWithType() + " (детализация по населенным пунктам)");
                    im.put("S1", Integer.toString(subSum[0]));
                    im.put("S2", Integer.toString(subSum[1]));
                    im.put("S3", Integer.toString(subSum[0] + subSum[1]));
                    im.put("S4", reportDataModel.getArea2percentMap().get(area.getId()));

                    RtfTableModifier tm = new RtfTableModifier();
                    tm.put("T", new String[0][]);
                    tm.put("T", new RtfRowIntercepterBase() {
                        public void beforeModify(RtfTable table, int currentRowIndex)
                        {
                            RtfRow templateRow = (RtfRow) table.getRowList().get(currentRowIndex);

                            List<RtfRow> newRowList = new ArrayList<RtfRow>();

                            for (AddressItem city : cityList) {
                                int[] value = (int[]) map.get(city);
                                newRowList.add(EntrantResidenceDistribReportBuilder.createRow(templateRow, 0, blackIndex, city.getTitleWithType(), value[0], value[1], reportDataModel.getTotalSum(), null, null));
                            }

                            table.getRowList().addAll(currentRowIndex + 1, newRowList);
                        }
                    });
                    im.modify(doc);
                    tm.modify(doc);

                    document.getElementList().add(doc);
                }
            }
        }

        return document;
    }

    protected RtfDocument writeCommonHeader(RtfDocument document)
    {
        TopOrgUnit academy = TopOrgUnit.getInstance();

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("vuz", academy.getTitle());
        injectModifier.put("stage", this._model.getReport().getEnrollmentCampaignStage());

        injectModifier.modify(document);

        return document;
    }

    private static RtfRow createRow(RtfRow templateRow, int indentValue, int colorIndex, String title, int budget, int contract, int totalSum, Long areaId, Map<Long, String> area2persentMap)
    {
        IRtfElement left = RtfBean.getElementFactory().createRtfControl(1292);
        IRtfElement center = RtfBean.getElementFactory().createRtfControl(1288);
        IRtfControl color = RtfBean.getElementFactory().createRtfControl(193, colorIndex);

        IRtfControl indent = RtfBean.getElementFactory().createRtfControl(715, indentValue);

        IRtfElement titleText = RtfBean.getElementFactory().createRtfText(title);
        IRtfElement budgetText = RtfBean.getElementFactory().createRtfText(Integer.toString(budget));
        IRtfElement contractText = RtfBean.getElementFactory().createRtfText(Integer.toString(contract));
        IRtfElement sumText = RtfBean.getElementFactory().createRtfText(Integer.toString(budget + contract));
        String percent = new DoubleFormatter(1, true).format(Double.valueOf((budget + contract) * 100.0D / totalSum));
        if (area2persentMap != null)
            area2persentMap.put(areaId, percent);
        IRtfElement percentText = RtfBean.getElementFactory().createRtfText(percent);

        RtfRow row = templateRow.getClone();
        List<RtfCell> list = row.getCellList();

        ((RtfCell) list.get(0)).setElementList(Arrays.asList(new IRtfElement[]{color, left, indent, titleText}));
        ((RtfCell) list.get(1)).setElementList(Arrays.asList(new IRtfElement[]{color, center, budgetText}));
        ((RtfCell) list.get(2)).setElementList(Arrays.asList(new IRtfElement[]{color, contractText}));
        ((RtfCell) list.get(3)).setElementList(Arrays.asList(new IRtfElement[]{color, sumText}));
        ((RtfCell) list.get(4)).setElementList(Arrays.asList(new IRtfElement[]{color, percentText}));
        return row;
    }

    private Map<EducationOrgUnit, List<Object[]>> getDataByEduOrgUnit()
    {
        DQLSelectBuilder dataDQL = createDataDQL();

        dataDQL.column(DQLExpressions.property("t.ouId"), "ouId_alias");
        dataDQL.group(DQLExpressions.property("t.ouId"));

        List<Object[]> data = dataDQL.createStatement(new DQLExecutionContext(this._session)).list();

        Map<EducationOrgUnit, List<Object[]>> resultMap = new TreeMap<EducationOrgUnit, List<Object[]>>(new EntityComparator(new EntityOrder[]{new EntityOrder(EducationOrgUnit.developForm().code().s()), new EntityOrder(EducationOrgUnit.title().s()), new EntityOrder(EducationOrgUnit.developCondition().code().s()), new EntityOrder(EducationOrgUnit.developTech().code().s()), new EntityOrder(EducationOrgUnit.developPeriod().code().s()), new EntityOrder(EducationOrgUnit.id().s())}));
        for (Object[] o : data) {
            Long eduOUId = (Long) o[4];

            SafeMap.safeGet(resultMap, (EducationOrgUnit) this._session.get(EducationOrgUnit.class, eduOUId), ArrayList.class).add(o);
        }

        return resultMap;
    }

    private List<Object[]> getData()
    {
        List<Object[]> data = createDataDQL().createStatement(new DQLExecutionContext(this._session)).list();

        List<Long> settlementIds = new ArrayList<Long>();
        for (Object[] row : data) {
            settlementIds.add(Long.valueOf(((Number) row[1]).longValue()));
        }

        BatchUtils.execute(settlementIds, 1000, new BatchUtils.Action<Long>() {
            public void execute(Collection<Long> elements)
            {
                if (elements.isEmpty()) return;
                MQBuilder builder = new MQBuilder("org.tandemframework.shared.fias.base.entity.AddressItem", "a");
                builder.addLeftJoinFetch("a", "parent", "p1");
                builder.addLeftJoinFetch("p1", "parent", "p2");
                builder.addLeftJoinFetch("p2", "parent", "p3");
                builder.add(MQExpression.in("a", "id", new ArrayList<Long>(elements)));
            }
        });
        return data;
    }

    private DQLSelectBuilder createDataDQL()
    {
        DQLSelectBuilder builder;
        switch (this._model.getEnrollmentCampaignStage().getId().intValue()) {
            case 0:
                builder = getRequestedBuilder();
                builder.joinPath(DQLJoinType.inner, "d.state", "state");
                builder.where(DQLExpressions.ne(DQLExpressions.property("state.code"), "7"));
                builder.group("request.id");
                break;
            case 1:
                builder = getRequestedBuilder();
                builder.joinPath(DQLJoinType.inner, "d.state", "state");
                builder.where(DQLExpressions.notIn(DQLExpressions.property("state.code"), Arrays.asList(new String[]{"7", "2", "1"})));

                builder.group(DQLExpressions.property("request.id"));
                break;
            case 2:
                builder = getPreliminaryBuilder();
                builder.group(DQLExpressions.property("p.id"));
                break;
            default:
                throw new RuntimeException("Unknown report stage!");
        }

        builder.group("ct.id");
        builder.group("settlement.id");
        builder.group(DQLExpressions.property("district.id"));
        builder.group(DQLExpressions.property("ou.id"));

        builder.column(DQLExpressions.property("ct.id"), "ctId");
        builder.column(DQLExpressions.property("settlement.id"), "settlementId");
        builder.column(DQLExpressions.property("district.id"), "districtId");
        builder.column(DQLExpressions.property("ou.id"), "ouId");

        return new DQLSelectBuilder().column(DQLExpressions.property("t.ctId"), "ctId_alias")
                .column(DQLExpressions.property("t.settlementId"), "settlementId_alias")
                .column(DQLExpressions.property("t.districtId"), "districtId_alias")
                .column(DQLFunctions.countStar(), "count")
                .fromDataSource(builder.buildQuery(), "t")
                .group("t.ctId")
                .group("t.settlementId")
                .group("t.districtId");
    }

    private DQLSelectBuilder getRequestedBuilder()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(RequestedEnrollmentDirection.class, "d");
        builder.joinPath(DQLJoinType.inner, "d.entrantRequest", "request");
        builder.joinPath(DQLJoinType.inner, "d.enrollmentDirection", "ed");
        builder.joinPath(DQLJoinType.inner, "ed.educationOrgUnit", "ou");
        builder.joinPath(DQLJoinType.inner, "d.compensationType", "ct");

        patchEduOrgUnit(builder, "d.studentCategory", "d");

        return builder;
    }

    private DQLSelectBuilder getPreliminaryBuilder()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(PreliminaryEnrollmentStudent.class, "p");
        builder.joinPath(DQLJoinType.inner, "p.requestedEnrollmentDirection", "d");
        builder.joinPath(DQLJoinType.inner, "d.entrantRequest", "request");
        builder.joinPath(DQLJoinType.inner, "p.educationOrgUnit", "ou");
        builder.joinPath(DQLJoinType.inner, "p.compensationType", "ct");

        if ((this._model.isParallelActive()) && (this._model.getParallel().isTrue())) {
            builder.where(DQLExpressions.eq(DQLExpressions.property("p.parallel"), DQLExpressions.value(Boolean.FALSE)));
        }
        builder.where(DQLExpressions.ne(DQLExpressions.property("d.state.code"), "7"));
        patchEduOrgUnit(builder, "p.studentCategory", "p");

        return builder;
    }

    private void patchEduOrgUnit(DQLSelectBuilder builder, String studentCategoryAlias, String alias)
    {
        builder.joinPath(DQLJoinType.inner, "request.entrant", "entrant");
        builder.joinPath(DQLJoinType.inner, "entrant.person", "person");
        builder.joinPath(DQLJoinType.inner, "person.identityCard", "identityCard");
        builder.joinPath(DQLJoinType.inner, "identityCard.address", "address");
        builder.joinEntity("address", DQLJoinType.inner, AddressDetailed.class, "addressDetailed", DQLExpressions.eq(DQLExpressions.property("address.id"), DQLExpressions.property("addressDetailed.id")));
        builder.joinEntity("address", DQLJoinType.left, AddressRu.class, "addressRu", DQLExpressions.eq(DQLExpressions.property("address.id"), DQLExpressions.property("addressRu.id")));
        builder.joinPath(DQLJoinType.inner, "addressDetailed.settlement", "settlement");
        builder.joinPath(DQLJoinType.left, "addressRu.district", "district");
        builder.joinPath(DQLJoinType.inner, "ou.educationLevelHighSchool", "hs");
        builder.joinPath(DQLJoinType.inner, "hs.educationLevel", "el");
        builder.where(DQLExpressions.eq(DQLExpressions.property("entrant.archival"), DQLExpressions.value(Boolean.FALSE)));
        builder.where(DQLExpressions.eq(DQLExpressions.property("entrant.enrollmentCampaign"), DQLExpressions.value(this._model.getReport().getEnrollmentCampaign())));
        builder.where(DQLExpressions.betweenDays("request.regDate", this._model.getReport().getDateFrom(), this._model.getReport().getDateTo()));

        if (this._model.isStudentCategoryActive())
            builder.where(DQLExpressions.in(DQLExpressions.property(studentCategoryAlias), this._model.getStudentCategoryList()));
        if (this._model.isQualificationActive())
            builder.where(DQLExpressions.in(DQLExpressions.property("el.qualification"), this._model.getQualificationList()));
        if (this._model.isFormativeOrgUnitActive())
            builder.where(DQLExpressions.in(DQLExpressions.property("ou.formativeOrgUnit"), this._model.getFormativeOrgUnitList()));
        if (this._model.isTerritorialOrgUnitActive()) {
            if (this._model.getTerritorialOrgUnitList().isEmpty())
                builder.where(DQLExpressions.isNull(DQLExpressions.property("ou.territorialOrgUnit")));
            else
                builder.where(DQLExpressions.in(DQLExpressions.property("ou.territorialOrgUnit"), this._model.getTerritorialOrgUnitList()));
        }
        if (this._model.isDevelopFormActive())
            builder.where(DQLExpressions.in(DQLExpressions.property("ou.developForm"), this._model.getDevelopFormList()));
        if (this._model.isDevelopConditionActive())
            builder.where(DQLExpressions.in(DQLExpressions.property("ou.developCondition"), this._model.getDevelopConditionList()));
        if (this._model.isDevelopTechActive())
            builder.where(DQLExpressions.in(DQLExpressions.property("ou.developTech"), this._model.getDevelopTechList()));
        if (this._model.isDevelopPeriodActive())
            builder.where(DQLExpressions.in(DQLExpressions.property("ou.developPeriod"), this._model.getDevelopPeriodList()));
    }

    protected ReportDataModel prepareReportDataModel(List<Object[]> data)
    {
        return new ReportDataModel(data);
    }

    protected RtfDocument loadTemplate()
    {
        IScriptItem templateDocument = (IScriptItem) UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, "entrantResidenceDistrib");
        return new RtfReader().read(templateDocument.getCurrentTemplate());
    }

    protected class ReportDataModel {
        private final List<Object[]> _data;
        private final TopOrgUnit _academy;
        private final AddressItem _academyCity;
        private final AddressDetailed _academyAddress;
        private final AddressItem _academyRegion;
        private final int[] _highSchoolCity = new int[2];
        private final int[] _highSchoolRegion = new int[2];
        private final Map<AddressDistrict, int[]> _district2count = new Hashtable<AddressDistrict, int[]>();
        private final Map<AddressItem, int[]> _area2count = new Hashtable<AddressItem, int[]>();
        private final Map<AddressItem, Map<AddressItem, int[]>> _area2city2count = new Hashtable<AddressItem, Map<AddressItem, int[]>>();
        private final int[] _foreignCountry = new int[2];
        private final List<AddressDistrict> _districtList;
        private final List<AddressItem> _areaList;
        private final int[] _sum = new int[2];
        private final int _totalSum;
        private final Map<Long, String> _area2percentMap = new Hashtable<Long, String>();

        public ReportDataModel(List<Object[]> data)
        {
            this._data = data;

            this._academy = TopOrgUnit.getInstance();
            this._academyAddress = this._academy.getAddress();
            this._academyCity = (this._academyAddress == null ? null : this._academyAddress.getSettlement());
            AddressItem temp = this._academyCity;
            if (temp != null)
                while (temp.getParent() != null)
                    temp = temp.getParent();
            this._academyRegion = temp;
            long budgetId = ((CompensationType) UniDaoFacade.getCoreDao().getCatalogItem(CompensationType.class, "1")).getId().longValue();

            for (Object[] row : this._data) {
                long settlementId = ((Number) row[1]).longValue();
                long compTypeId = ((Number) row[0]).longValue();
                Long districtId = row[2] == null ? null : Long.valueOf(((Number) row[2]).longValue());
                int count = ((Number) row[3]).intValue();

                AddressItem settlement = (AddressItem) EntrantResidenceDistribReportBuilder.this._session.get(AddressItem.class, Long.valueOf(settlementId));
                AddressItem region = settlement;
                while (region.getParent() != null) region = region.getParent();

                if (settlement.getCountry().getCode() == 0) {
                    if (settlement.equals(this._academyCity)) {
                        this._highSchoolCity[(compTypeId == budgetId ? 0 : 1)] += count;

                        if (districtId != null) {
                            AddressDistrict district = (AddressDistrict) EntrantResidenceDistribReportBuilder.this._session.get(AddressDistrict.class, districtId);
                            int[] value = (int[]) this._district2count.get(district);
                            if (value == null)
                                this._district2count.put(district, value = new int[2]);
                            value[(compTypeId == budgetId ? 0 : 1)] += count;
                        }
                    }
                    else if (region.equals(this._academyRegion)) {
                        this._highSchoolRegion[(compTypeId == budgetId ? 0 : 1)] += count;

                        if (EntrantResidenceDistribReportBuilder.this._model.getDetail().isTrue()) {
                            Map<AddressItem, int[]> map = this._area2city2count.get(region);
                            if (map == null)
                                this._area2city2count.put(region, map = new Hashtable<AddressItem, int[]>());
                            int[] mapValue = (int[]) map.get(settlement);
                            if (mapValue == null)
                                map.put(settlement, mapValue = new int[2]);
                            mapValue[(compTypeId == budgetId ? 0 : 1)] += count;
                        }
                    }
                    else {
                        int[] value = (int[]) this._area2count.get(region);
                        if (value == null)
                            this._area2count.put(region, value = new int[2]);
                        value[(compTypeId == budgetId ? 0 : 1)] += count;

                        if (EntrantResidenceDistribReportBuilder.this._model.getDetail().isTrue()) {
                            Map<AddressItem, int[]> map = this._area2city2count.get(region);
                            if (map == null)
                                this._area2city2count.put(region, map = new Hashtable<AddressItem, int[]>());
                            int[] mapValue = (int[]) map.get(settlement);
                            if (mapValue == null)
                                map.put(settlement, mapValue = new int[2]);
                            mapValue[(compTypeId == budgetId ? 0 : 1)] += count;
                        }
                    }
                }
                else {
                    this._foreignCountry[(compTypeId == budgetId ? 0 : 1)] += count;
                }
            }

            this._districtList = new ArrayList<AddressDistrict>(this._district2count.keySet());
            Collections.sort(this._districtList, ITitled.TITLED_COMPARATOR);

            this._areaList = new ArrayList<AddressItem>(this._area2count.keySet());
            Collections.sort(this._areaList, ITitled.TITLED_COMPARATOR);

            this._sum[0] += this._highSchoolCity[0] + this._highSchoolRegion[0] + this._foreignCountry[0];
            this._sum[1] += this._highSchoolCity[1] + this._highSchoolRegion[1] + this._foreignCountry[1];
            for (int[] entry : this._area2count.values()) {
                this._sum[0] += entry[0];
                this._sum[1] += entry[1];
            }
            this._totalSum = (this._sum[0] + this._sum[1]);
        }

        public TopOrgUnit getAcademy()
        {
            return this._academy;
        }

        public AddressItem getAcademyCity()
        {
            return this._academyCity;
        }

        public AddressDetailed getAcademyAddress()
        {
            return this._academyAddress;
        }

        public AddressItem getAcademyRegion()
        {
            return this._academyRegion;
        }

        public int[] getHighSchoolCity()
        {
            return this._highSchoolCity;
        }

        public int[] getHighSchoolRegion()
        {
            return this._highSchoolRegion;
        }

        public Map<AddressDistrict, int[]> getDistrict2count()
        {
            return this._district2count;
        }

        public Map<AddressItem, int[]> getArea2count()
        {
            return this._area2count;
        }

        public Map<AddressItem, Map<AddressItem, int[]>> getArea2city2count()
        {
            return this._area2city2count;
        }

        public int[] getForeignCountry()
        {
            return this._foreignCountry;
        }

        public int[] getSum()
        {
            return this._sum;
        }

        public int getTotalSum()
        {
            return this._totalSum;
        }

        public Map<Long, String> getArea2percentMap()
        {
            return this._area2percentMap;
        }

        public List<AddressDistrict> getDistrictList()
        {
            return this._districtList;
        }

        public List<AddressItem> getAreaList()
        {
            return this._areaList;
        }
    }
}