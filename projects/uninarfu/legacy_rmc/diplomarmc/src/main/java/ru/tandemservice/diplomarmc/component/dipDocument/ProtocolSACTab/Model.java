package ru.tandemservice.diplomarmc.component.dipDocument.ProtocolSACTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.diplomarmc.entity.RelProtocolSACToStudent;

@State({@Bind(key = "publisherId", binding = "studentId"),
        @Bind(key = "selectedDataTab", binding = "selectedDataTab")})
public class Model {

    private Long studentId;
    private DynamicListDataSource<RelProtocolSACToStudent> dataSource;
    private String selectedDataTab;

    public String getSelectedDataTab() {
        return selectedDataTab;
    }

    public void setSelectedDataTab(String selectedDataTab) {
        this.selectedDataTab = selectedDataTab;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public DynamicListDataSource<RelProtocolSACToStudent> getDataSource() {
        return dataSource;
    }

    public void setDataSource(
            DynamicListDataSource<RelProtocolSACToStudent> dataSource)
    {
        this.dataSource = dataSource;
    }


}
