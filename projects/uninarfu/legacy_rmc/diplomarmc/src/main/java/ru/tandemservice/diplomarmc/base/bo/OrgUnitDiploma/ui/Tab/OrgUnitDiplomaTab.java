package ru.tandemservice.diplomarmc.base.bo.OrgUnitDiploma.ui.Tab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;

@Configuration
public class OrgUnitDiplomaTab extends BusinessComponentManager {
    public static final String TAB_LIST = "diplomaTabList";
    public static final String PROTOCOLSAC_TAB = "protocolSacTab";
    public static final String REPORTS_TAB = "bookCheckTab";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder().create();
    }

    @Bean
    public TabPanelExtPoint tabPanelExtPoint() {
        return tabPanelExtPointBuilder(TAB_LIST)
                .addTab(componentTab(PROTOCOLSAC_TAB, "OxProtokolGAK")
                                .parameters("ui:params")
                                .permissionKey("ui:secModel.diplomarmcrmc_protokolsac_tab"))
                .addTab(componentTab(REPORTS_TAB, "OrgUnitDiplomaReportList")
                                .parameters("ui:params")
                                .permissionKey("ui:secModel.diplomarmcrmc_reportrs_tab"))
                .create();
    }
}
