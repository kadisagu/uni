package ru.tandemservice.diplomarmc.component.dipDocument.ProtocolSACPub;

import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.diplomarmc.entity.RelProtocolSACToStudent;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        if (model.getId() != null)
            model.setProtocolSAC(getNotNull(model.getId()));
    }

    @SuppressWarnings("unchecked")
    @Override
    public void prepareListDataSource(Model model) {
        DynamicListDataSource<RelProtocolSACToStudent> dataSource = model.getDataSource();

        DQLSelectBuilder builder = createBuilder(model);

        DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(RelProtocolSACToStudent.class, "rel");
        orderRegistry.applyOrder(builder, dataSource.getEntityOrder());

        if (model.getValueMap() != null) {
            ((BlockColumn<String>) dataSource.getColumn("themaVKREdit")).setValueMap(model.getValueMap());
            model.setValueMap(null);
        }
        else
            ((BlockColumn<String>) dataSource.getColumn("themaVKREdit")).setValueMap(makeMap(model));

        dataSource.getColumn("themaVKREdit").setVisible(model.isSpecifyThemaVKR());
        dataSource.getColumn("themaVKR").setVisible(!model.isSpecifyThemaVKR());

        UniBaseUtils.createPage(dataSource, builder, getSession());
    }

    public DQLSelectBuilder createBuilder(Model model) {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RelProtocolSACToStudent.class, "rel")
                .where(DQLExpressions.eq(DQLExpressions.property(RelProtocolSACToStudent.protocolSAC().id().fromAlias("rel")), DQLExpressions.value(model.getProtocolSAC().getId())));
        return builder;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void update(Model model) {
        DynamicListDataSource<RelProtocolSACToStudent> dataSource = model.getDataSource();
        Map<Long, String> themaMap = ((BlockColumn<String>) dataSource.getColumn("themaVKREdit")).getValueMap();

        for (Long id : themaMap.keySet()) {
            RelProtocolSACToStudent rel = getNotNull(id);
            rel.setThemaVKR(themaMap.get(id));
            saveOrUpdate(rel);
        }
    }

    public Map<Long, String> makeMap(Model model) {
        Map<Long, String> themaMap = new HashMap<>();

        List<RelProtocolSACToStudent> list = getList(createBuilder(model));

        for (RelProtocolSACToStudent rel : list) {
            themaMap.put(rel.getId(), rel.getThemaVKR());
        }

        return themaMap;
    }

    public void updateHons(Model model, Long id) {
        RelProtocolSACToStudent rel = getNotNull(id);
        if (rel.getHons())
            rel.setHons(false);
        else
            rel.setHons(true);

        saveOrUpdate(rel);
    }
}
