package ru.tandemservice.diplomarmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.diplomarmc.entity.OrgUnitExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение сущности орг структура
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OrgUnitExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.diplomarmc.entity.OrgUnitExt";
    public static final String ENTITY_NAME = "orgUnitExt";
    public static final int VERSION_HASH = -6519821;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT = "orgUnit";
    public static final String P_RENAMING = "renaming";
    public static final String P_YEAR_RENAMING = "yearRenaming";

    private OrgUnit _orgUnit;     // Подразделение
    private String _renaming;     // Сведения о переименовании
    private Integer _yearRenaming;     // Год переименования

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Подразделение. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null и должно быть уникальным.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Сведения о переименовании.
     */
    @Length(max=2000)
    public String getRenaming()
    {
        return _renaming;
    }

    /**
     * @param renaming Сведения о переименовании.
     */
    public void setRenaming(String renaming)
    {
        dirty(_renaming, renaming);
        _renaming = renaming;
    }

    /**
     * @return Год переименования.
     */
    public Integer getYearRenaming()
    {
        return _yearRenaming;
    }

    /**
     * @param yearRenaming Год переименования.
     */
    public void setYearRenaming(Integer yearRenaming)
    {
        dirty(_yearRenaming, yearRenaming);
        _yearRenaming = yearRenaming;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OrgUnitExtGen)
        {
            setOrgUnit(((OrgUnitExt)another).getOrgUnit());
            setRenaming(((OrgUnitExt)another).getRenaming());
            setYearRenaming(((OrgUnitExt)another).getYearRenaming());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OrgUnitExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OrgUnitExt.class;
        }

        public T newInstance()
        {
            return (T) new OrgUnitExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "renaming":
                    return obj.getRenaming();
                case "yearRenaming":
                    return obj.getYearRenaming();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "renaming":
                    obj.setRenaming((String) value);
                    return;
                case "yearRenaming":
                    obj.setYearRenaming((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "orgUnit":
                        return true;
                case "renaming":
                        return true;
                case "yearRenaming":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "orgUnit":
                    return true;
                case "renaming":
                    return true;
                case "yearRenaming":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "renaming":
                    return String.class;
                case "yearRenaming":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OrgUnitExt> _dslPath = new Path<OrgUnitExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OrgUnitExt");
    }
            

    /**
     * @return Подразделение. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.diplomarmc.entity.OrgUnitExt#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Сведения о переименовании.
     * @see ru.tandemservice.diplomarmc.entity.OrgUnitExt#getRenaming()
     */
    public static PropertyPath<String> renaming()
    {
        return _dslPath.renaming();
    }

    /**
     * @return Год переименования.
     * @see ru.tandemservice.diplomarmc.entity.OrgUnitExt#getYearRenaming()
     */
    public static PropertyPath<Integer> yearRenaming()
    {
        return _dslPath.yearRenaming();
    }

    public static class Path<E extends OrgUnitExt> extends EntityPath<E>
    {
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PropertyPath<String> _renaming;
        private PropertyPath<Integer> _yearRenaming;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подразделение. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.diplomarmc.entity.OrgUnitExt#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Сведения о переименовании.
     * @see ru.tandemservice.diplomarmc.entity.OrgUnitExt#getRenaming()
     */
        public PropertyPath<String> renaming()
        {
            if(_renaming == null )
                _renaming = new PropertyPath<String>(OrgUnitExtGen.P_RENAMING, this);
            return _renaming;
        }

    /**
     * @return Год переименования.
     * @see ru.tandemservice.diplomarmc.entity.OrgUnitExt#getYearRenaming()
     */
        public PropertyPath<Integer> yearRenaming()
        {
            if(_yearRenaming == null )
                _yearRenaming = new PropertyPath<Integer>(OrgUnitExtGen.P_YEAR_RENAMING, this);
            return _yearRenaming;
        }

        public Class getEntityClass()
        {
            return OrgUnitExt.class;
        }

        public String getEntityName()
        {
            return "orgUnitExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
