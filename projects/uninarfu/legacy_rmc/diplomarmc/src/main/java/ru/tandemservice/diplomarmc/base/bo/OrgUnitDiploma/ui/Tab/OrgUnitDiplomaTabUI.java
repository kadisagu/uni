package ru.tandemservice.diplomarmc.base.bo.OrgUnitDiploma.ui.Tab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.dao.UniDaoFacade;

import java.util.HashMap;
import java.util.Map;

@State({
        @Bind(key = "orgUnitId", binding = "orgUnitId"),
        @Bind(key = "selectedTab", binding = "selectedTab")
})
public class OrgUnitDiplomaTabUI extends UIPresenter {
    private Long orgUnitId;
    private OrgUnit orgUnit = new OrgUnit();
    private OrgUnitSecModel secModel;

    private String selectedTab;
    private Map<String, Object> params;


    @Override
    public void onComponentRefresh() {
        setParams(new HashMap<String, Object>());
        orgUnit = UniDaoFacade.getCoreDao().get(OrgUnit.class, orgUnitId);
        secModel = new OrgUnitSecModel(orgUnit);
        getParams().put("orgUnitId", orgUnitId);
    }

    public Long getOrgUnitId() {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId) {
        this.orgUnitId = orgUnitId;
    }

    public OrgUnit getOrgUnit() {
        return orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit) {
        this.orgUnit = orgUnit;
    }

    public OrgUnitSecModel getSecModel() {
        return secModel;
    }

    public void setSecModel(OrgUnitSecModel secModel) {
        this.secModel = secModel;
    }

    public String getSelectedTab() {
        return selectedTab;
    }

    public void setSelectedTab(String selectedTab) {
        this.selectedTab = selectedTab;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }
}
