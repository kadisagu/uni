package ru.tandemservice.diplomarmc.base.bo.OrgUnitDiploma.ui.ReportList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.diplomarmc.base.bo.OrgUnitDiploma.logic.ReportBeanDefinition;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Configuration
public class OrgUnitDiplomaReportList extends BusinessComponentManager {
    public static final String LIST_DS = "listDS";
    public static final String REPORT_LIST_BEAN_NAME = "diplomarmcOrgUnitReportDefinitionList";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(LIST_DS, columnListExtPoint(), entityDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint columnListExtPoint() {
        return columnListExtPointBuilder(LIST_DS)
                .addColumn(publisherColumn("title", "reportDefinition.title")
                                   .publisherLinkResolver(new IPublisherLinkResolver() {
                                       @Override
                                       public Object getParameters(IEntity iEntity) {
                                           return new UniMap().add("publisherId", ((OrgUnitDiplomaReportListUI.ReportDefinitionWrapper) iEntity).getOrgUnitId());
                                       }

                                       @Override
                                       public String getComponentName(IEntity iEntity) {
                                           return ((OrgUnitDiplomaReportListUI.ReportDefinitionWrapper) iEntity).getReportDefinition().getComponentName();
                                       }
                                   })
                                   .create()
                )
                .create();
    }

    @Bean
    IBusinessHandler<DSInput, DSOutput> entityDSHandler() {
        return new DefaultSearchDataSourceHandler(getName()) {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context) {
                Long orgUnitId = context.get("orgUnitId");
                List<OrgUnitDiplomaReportListUI.ReportDefinitionWrapper> wrappers = new ArrayList<OrgUnitDiplomaReportListUI.ReportDefinitionWrapper>();
                Long id = 0L;
                for (ReportBeanDefinition repDef : (List<ReportBeanDefinition>) ApplicationRuntime.getBean(REPORT_LIST_BEAN_NAME)) {
                    wrappers.add(new OrgUnitDiplomaReportListUI.ReportDefinitionWrapper(id++, repDef, orgUnitId));
                }

                Collections.sort(wrappers, ITitled.TITLED_COMPARATOR);

                return ListOutputBuilder.get(input, wrappers).pageable(false).build();
            }
        };
    }
}
