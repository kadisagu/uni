package ru.tandemservice.diplomarmc.component.dipDocument.ProtocolSACTab;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.diplomarmc.entity.RelProtocolSACToStudent;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepareListDataSource(Model model) {
        DynamicListDataSource<RelProtocolSACToStudent> dataSource = model.getDataSource();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RelProtocolSACToStudent.class, "rel").column("rel")
                .where(DQLExpressions.eq(DQLExpressions.property(RelProtocolSACToStudent.student().id().fromAlias("rel")), DQLExpressions.value(model.getStudentId())));

        DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(RelProtocolSACToStudent.class, "rel");
        orderRegistry.applyOrder(builder, dataSource.getEntityOrder());

        UniBaseUtils.createPage(dataSource, builder, getSession());
    }
}
