package ru.tandemservice.diplomarmc.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.*;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;

import java.util.Map;

public class OrgstructPermissionModifier implements ISecurityConfigMetaMapModifier {

    @Override
    public void modify(final Map<String, SecurityConfigMeta> securityConfigMetaMap)
    {
        final SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("diplomarmc");
        config.setName("diplomarmc-orgstruct-sec-config");
        config.setTitle("");

        final ModuleGlobalGroupMeta mggOrgstruct = PermissionMetaUtil.createModuleGlobalGroup(config, "orgstructModuleGlobalGroup", "Модуль «Орг. структура»");
        final ModuleLocalGroupMeta mlgOrgstruct = PermissionMetaUtil.createModuleLocalGroup(config, "orgstructLocalGroup", "Модуль «Орг. структура»");

        for (final OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class, OrgUnitType.P_TITLE)) {
            final String code = description.getCode();
            final ClassGroupMeta gcg = PermissionMetaUtil.createClassGroup(mggOrgstruct, code + "PermissionClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());
            final ClassGroupMeta lcg = PermissionMetaUtil.createClassGroup(mlgOrgstruct, code + "LocalClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());

            final PermissionGroupMeta pgRmcDiplomaTab = PermissionMetaUtil.createPermissionGroup(config, code + "RmcDiplomaPermissionGroup", "Вкладка «Дипломирование РАМЭК»");
            PermissionMetaUtil.createGroupRelation(config, pgRmcDiplomaTab.getName(), gcg.getName());
            PermissionMetaUtil.createGroupRelation(config, pgRmcDiplomaTab.getName(), lcg.getName());
            PermissionMetaUtil.createPermission(pgRmcDiplomaTab, "orgUnit_viewRmcDiploma_" + code, "Просмотр");

            // Вкладка "Протоколы ГАК" на табе "Дипломирование"
            PermissionGroupMeta permissoinGroupProtokolSAK = PermissionMetaUtil.createPermissionGroup(pgRmcDiplomaTab, code + "RmcDiplomaTabProtocolSACPermissionGroup", "Вкладка «Протоколы ГАК»");
            PermissionMetaUtil.createPermission(permissoinGroupProtokolSAK, "diplomarmcrmc_protokolsac_tab_" + code, "Просмотр");

            // Вкладка "Ведомость сверки" на табе "Дипломирование"
            PermissionGroupMeta permissoinGroupBookCheck = PermissionMetaUtil.createPermissionGroup(pgRmcDiplomaTab, code + "RmcDiplomaTabBookCheckPermissionGroup", "Вкладка «Ведомость сверки»");
            PermissionMetaUtil.createPermission(permissoinGroupBookCheck, "diplomarmcrmc_reportrs_tab_" + code, "Просмотр");
        }

        securityConfigMetaMap.put(config.getName(), config);
    }
}
