package ru.tandemservice.diplomarmc.base.ext.OrgUnit.ui.NewEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.NewEdit.OrgUnitNewEditUI;
import ru.tandemservice.diplomarmc.entity.OrgUnitExt;
import ru.tandemservice.uni.dao.UniDaoFacade;

public class OrgUnitNewEditExtUI extends UIAddon {

    public OrgUnitNewEditExtUI(IUIPresenter presenter, String name, String componentId) {
        super(presenter, name, componentId);
    }

    private OrgUnitNewEditUI parentUI = getPresenter();

    private OrgUnitExt orgUnitExt;

    @Override
    public void onComponentRefresh() {
        parentUI.onComponentRefresh();
        setOrgUnitExt(setOrgUnit());
    }


    public OrgUnitExt setOrgUnit() {
        OrgUnitExt orgUnitExt = UniDaoFacade.getCoreDao().get(OrgUnitExt.class, OrgUnitExt.orgUnit(), parentUI.getOrgUnit());
        if (orgUnitExt != null) {
            return orgUnitExt;
        }
        else {
            orgUnitExt = new OrgUnitExt();
            orgUnitExt.setOrgUnit(parentUI.getOrgUnit());
            return orgUnitExt;
        }
    }

    public void onClickSave()
    {
        OrgUnitManager.instance().dao().saveOrUpdateOrgUnit(parentUI.getOrgUnit());
        UniDaoFacade.getCoreDao().saveOrUpdate(orgUnitExt);
        parentUI.deactivate();
    }


    public OrgUnitExt getOrgUnitExt() {
        return orgUnitExt;
    }


    public void setOrgUnitExt(OrgUnitExt orgUnitExt) {
        this.orgUnitExt = orgUnitExt;
    }


}
