package ru.tandemservice.diplomarmc.base.bo.OrgUnitDiploma.ui.ReportList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.diplomarmc.base.bo.OrgUnitDiploma.logic.ReportBeanDefinition;
import ru.tandemservice.uni.dao.UniDaoFacade;

import java.util.HashMap;
import java.util.Map;

@State({
        @Bind(key = "orgUnitId", binding = "orgUnitId")
})
public class OrgUnitDiplomaReportListUI extends UIPresenter {

    private Long orgUnitId;
    private OrgUnit orgUnit = new OrgUnit();
    private OrgUnitSecModel secModel;
    private Map<String, Object> params;

    @Override
    public void onComponentRefresh() {
        setParams(new HashMap<String, Object>());
        orgUnit = UniDaoFacade.getCoreDao().get(OrgUnit.class, orgUnitId);
        secModel = new OrgUnitSecModel(orgUnit);
        getParams().put("orgUnitId", orgUnitId);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource ds) {
        if (ds.getName().equals(OrgUnitDiplomaReportList.LIST_DS)) {
            ds.put("orgUnitId", getOrgUnitId());
        }
    }

    public static class ReportDefinitionWrapper extends IdentifiableWrapper {
        private ReportBeanDefinition reportDefinition;
        private Long orgUnitId;

        public ReportDefinitionWrapper(Long id, ReportBeanDefinition reportDefinition, Long orgUnitId) {
            super(id, reportDefinition.getTitle());
            this.reportDefinition = reportDefinition;
            this.orgUnitId = orgUnitId;
        }

        public ReportBeanDefinition getReportDefinition() {
            return reportDefinition;
        }

        public Long getOrgUnitId() {
            return orgUnitId;
        }
    }

    public Long getOrgUnitId() {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId) {
        this.orgUnitId = orgUnitId;
    }

    public OrgUnit getOrgUnit() {
        return orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit) {
        this.orgUnit = orgUnit;
    }

    public OrgUnitSecModel getSecModel() {
        return secModel;
    }

    public void setSecModel(OrgUnitSecModel secModel) {
        this.secModel = secModel;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }
}
