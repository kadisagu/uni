package ru.tandemservice.diplomarmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.diplomarmc.entity.ProtocolSAC;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Протокол ГАК
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ProtocolSACGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.diplomarmc.entity.ProtocolSAC";
    public static final String ENTITY_NAME = "protocolSAC";
    public static final int VERSION_HASH = -755084818;
    private static IEntityMeta ENTITY_META;

    public static final String P_NUMBER = "number";
    public static final String P_DATE = "date";
    public static final String L_QUALIFICATIONS = "qualifications";
    public static final String L_EMPLOYEE = "employee";
    public static final String L_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String L_EDUCATION_LEVEL = "educationLevel";

    private String _number;     // Номер протокола ГАК
    private Date _date;     // Дата составления протокола
    private Qualifications _qualifications;     // Квалификация
    private Employee _employee;     // Председатель ГАК
    private OrgUnit _formativeOrgUnit;     // Формирующее подразделение
    private EducationLevels _educationLevel;     // Напрваление подготовки ГОС

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Номер протокола ГАК.
     */
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер протокола ГАК.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Дата составления протокола.
     */
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Дата составления протокола.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Квалификация.
     */
    public Qualifications getQualifications()
    {
        return _qualifications;
    }

    /**
     * @param qualifications Квалификация.
     */
    public void setQualifications(Qualifications qualifications)
    {
        dirty(_qualifications, qualifications);
        _qualifications = qualifications;
    }

    /**
     * @return Председатель ГАК.
     */
    public Employee getEmployee()
    {
        return _employee;
    }

    /**
     * @param employee Председатель ГАК.
     */
    public void setEmployee(Employee employee)
    {
        dirty(_employee, employee);
        _employee = employee;
    }

    /**
     * @return Формирующее подразделение.
     */
    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подразделение.
     */
    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * @return Напрваление подготовки ГОС.
     */
    public EducationLevels getEducationLevel()
    {
        return _educationLevel;
    }

    /**
     * @param educationLevel Напрваление подготовки ГОС.
     */
    public void setEducationLevel(EducationLevels educationLevel)
    {
        dirty(_educationLevel, educationLevel);
        _educationLevel = educationLevel;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ProtocolSACGen)
        {
            setNumber(((ProtocolSAC)another).getNumber());
            setDate(((ProtocolSAC)another).getDate());
            setQualifications(((ProtocolSAC)another).getQualifications());
            setEmployee(((ProtocolSAC)another).getEmployee());
            setFormativeOrgUnit(((ProtocolSAC)another).getFormativeOrgUnit());
            setEducationLevel(((ProtocolSAC)another).getEducationLevel());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ProtocolSACGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ProtocolSAC.class;
        }

        public T newInstance()
        {
            return (T) new ProtocolSAC();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "number":
                    return obj.getNumber();
                case "date":
                    return obj.getDate();
                case "qualifications":
                    return obj.getQualifications();
                case "employee":
                    return obj.getEmployee();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "educationLevel":
                    return obj.getEducationLevel();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "qualifications":
                    obj.setQualifications((Qualifications) value);
                    return;
                case "employee":
                    obj.setEmployee((Employee) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((OrgUnit) value);
                    return;
                case "educationLevel":
                    obj.setEducationLevel((EducationLevels) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "number":
                        return true;
                case "date":
                        return true;
                case "qualifications":
                        return true;
                case "employee":
                        return true;
                case "formativeOrgUnit":
                        return true;
                case "educationLevel":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "number":
                    return true;
                case "date":
                    return true;
                case "qualifications":
                    return true;
                case "employee":
                    return true;
                case "formativeOrgUnit":
                    return true;
                case "educationLevel":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "number":
                    return String.class;
                case "date":
                    return Date.class;
                case "qualifications":
                    return Qualifications.class;
                case "employee":
                    return Employee.class;
                case "formativeOrgUnit":
                    return OrgUnit.class;
                case "educationLevel":
                    return EducationLevels.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ProtocolSAC> _dslPath = new Path<ProtocolSAC>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ProtocolSAC");
    }
            

    /**
     * @return Номер протокола ГАК.
     * @see ru.tandemservice.diplomarmc.entity.ProtocolSAC#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Дата составления протокола.
     * @see ru.tandemservice.diplomarmc.entity.ProtocolSAC#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.diplomarmc.entity.ProtocolSAC#getQualifications()
     */
    public static Qualifications.Path<Qualifications> qualifications()
    {
        return _dslPath.qualifications();
    }

    /**
     * @return Председатель ГАК.
     * @see ru.tandemservice.diplomarmc.entity.ProtocolSAC#getEmployee()
     */
    public static Employee.Path<Employee> employee()
    {
        return _dslPath.employee();
    }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.diplomarmc.entity.ProtocolSAC#getFormativeOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return Напрваление подготовки ГОС.
     * @see ru.tandemservice.diplomarmc.entity.ProtocolSAC#getEducationLevel()
     */
    public static EducationLevels.Path<EducationLevels> educationLevel()
    {
        return _dslPath.educationLevel();
    }

    public static class Path<E extends ProtocolSAC> extends EntityPath<E>
    {
        private PropertyPath<String> _number;
        private PropertyPath<Date> _date;
        private Qualifications.Path<Qualifications> _qualifications;
        private Employee.Path<Employee> _employee;
        private OrgUnit.Path<OrgUnit> _formativeOrgUnit;
        private EducationLevels.Path<EducationLevels> _educationLevel;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Номер протокола ГАК.
     * @see ru.tandemservice.diplomarmc.entity.ProtocolSAC#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(ProtocolSACGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Дата составления протокола.
     * @see ru.tandemservice.diplomarmc.entity.ProtocolSAC#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(ProtocolSACGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.diplomarmc.entity.ProtocolSAC#getQualifications()
     */
        public Qualifications.Path<Qualifications> qualifications()
        {
            if(_qualifications == null )
                _qualifications = new Qualifications.Path<Qualifications>(L_QUALIFICATIONS, this);
            return _qualifications;
        }

    /**
     * @return Председатель ГАК.
     * @see ru.tandemservice.diplomarmc.entity.ProtocolSAC#getEmployee()
     */
        public Employee.Path<Employee> employee()
        {
            if(_employee == null )
                _employee = new Employee.Path<Employee>(L_EMPLOYEE, this);
            return _employee;
        }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.diplomarmc.entity.ProtocolSAC#getFormativeOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new OrgUnit.Path<OrgUnit>(L_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return Напрваление подготовки ГОС.
     * @see ru.tandemservice.diplomarmc.entity.ProtocolSAC#getEducationLevel()
     */
        public EducationLevels.Path<EducationLevels> educationLevel()
        {
            if(_educationLevel == null )
                _educationLevel = new EducationLevels.Path<EducationLevels>(L_EDUCATION_LEVEL, this);
            return _educationLevel;
        }

        public Class getEntityClass()
        {
            return ProtocolSAC.class;
        }

        public String getEntityName()
        {
            return "protocolSAC";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
