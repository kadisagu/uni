package ru.tandemservice.diplomarmc.component.dipDocument.ProtocolSACTab;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.diplomarmc.entity.RelProtocolSACToStudent;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        prepareDataSource(component);
    }

    public void prepareDataSource(IBusinessComponent component) {
        final Model model = (Model) getModel(component);
        if (model.getDataSource() != null) return;
        DynamicListDataSource<RelProtocolSACToStudent> dataSource = new DynamicListDataSource<>(component, component1 -> {
            ((IDAO) Controller.this.getDao()).prepareListDataSource(model);
        });

        PublisherLinkColumn linkColumn = new PublisherLinkColumn("Номер протокола", RelProtocolSACToStudent.protocolSAC().number());

        linkColumn.setResolver(new IPublisherLinkResolver() {
            @Override
            public String getComponentName(IEntity ientity) {
                return "ru.tandemservice.diplomarmc.component.dipDocument.ProtocolSACPub";
            }

            @Override
            public Object getParameters(IEntity entity) {
                return ParametersMap.createWith("id", ((RelProtocolSACToStudent) entity).getProtocolSAC().getId());
            }
        });

        dataSource.addColumn(linkColumn);
        dataSource.addColumn(new DateColumn("Дата", RelProtocolSACToStudent.protocolSAC().date(), "dd.MM.yyyy").setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Квалификация", RelProtocolSACToStudent.protocolSAC().qualifications().title()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Председатель", RelProtocolSACToStudent.protocolSAC().employee().fullFio()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Тема ВКР", RelProtocolSACToStudent.themaVKR()));
        dataSource.addColumn(new BooleanColumn("С отличием", RelProtocolSACToStudent.hons()));

        model.setDataSource(dataSource);
    }

}
