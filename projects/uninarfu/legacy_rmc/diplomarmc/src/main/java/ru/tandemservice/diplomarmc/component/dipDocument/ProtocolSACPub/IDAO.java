package ru.tandemservice.diplomarmc.component.dipDocument.ProtocolSACPub;

import ru.tandemservice.uni.dao.IUniDao;

import java.util.Map;

public interface IDAO extends IUniDao<Model> {
    public void updateHons(Model model, Long id);

    public Map<Long, String> makeMap(Model model);
}
