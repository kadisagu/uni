package ru.tandemservice.diplomarmc.component.orgunit.StudentToProtocolSAC;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.shared.commonbase.base.util.UniMap;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        ((IDAO) getDao()).prepare((Model) getModel(component));
    }

    public void onClickSave(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        ((IDAO) getDao()).update(model);
        deactivate(component);

        activate(component, "ru.tandemservice.diplomarmc.component.dipDocument.ProtocolSACPub", new UniMap().add("id", model.getProtocolSAC().getId()));
    }
}
