package ru.tandemservice.diplomarmc.migration;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_diplomarmc_1x0x0_0to1 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.10"),
                        new ScriptDependency("org.tandemframework.shared", "1.1.3"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.1.3")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {

        ////////////////////////////////////////////////////////////////////////////////
        // модуль unidip отключен - удаляем все его сущности

        // убедиться, что модуль и в самом деле удален
        if (ApplicationRuntime.hasModule("unidip"))
            throw new RuntimeException("Module 'unidip' is not deleted");

        MigrationUtils.removeModuleFromVersion_s(tool, "unidip");

        // удалить сущность dipStuExcludeExtract
        {
            // удалить таблицу
            tool.dropTable("dip_stu_exclude_extract", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dipStuExcludeExtract");

        }

        // удалить сущность diplomaTemplate
        {
            // удалить таблицу
            tool.dropTable("diploma_template", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("diplomaTemplate");

        }

        // удалить сущность diplomaObject
        {
            // удалить таблицу
            tool.dropTable("diploma_object", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("diplomaObject");

        }

        // удалить сущность diplomaEpvTemplateDefaultRel
        {
            // удалить таблицу
            tool.dropTable("dip_epv_template_rel", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("diplomaEpvTemplateDefaultRel");

        }

        // удалить сущность diplomaStateExamRow
        {
            // удалить таблицу
            tool.dropTable("dip_state_exam_row", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("diplomaStateExamRow");

        }

        // удалить сущность diplomaQualifWorkRow
        {
            // удалить таблицу
            tool.dropTable("dip_qualif_work_row", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("diplomaQualifWorkRow");

        }

        // удалить сущность diplomaPracticeRow
        {
            // удалить таблицу
            tool.dropTable("dip_practice_row", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("diplomaPracticeRow");

        }

        // удалить сущность diplomaDisciplineRow
        {
            // удалить таблицу
            tool.dropTable("dip_disc_row", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("diplomaDisciplineRow");

        }

        // удалить сущность diplomaCourseWorkRow
        {
            // удалить таблицу
            tool.dropTable("dip_c_work_row", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("diplomaCourseWorkRow");

        }

        // удалить сущность diplomaContentRow
        {
            // удалить таблицу
            tool.dropTable("diploma_content_row", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("diplomaContentRow");

        }

        // удалить сущность diplomaContentRegElPartFControlAction
        {
            // удалить таблицу
            tool.dropTable("diploma_rowrepfca", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("diplomaContentRegElPartFControlAction");

        }

        // удалить сущность diplomaContent
        {
            // удалить таблицу
            tool.dropTable("diploma_content", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("diplomaContent");

        }

        // удалить сущность dipDocumentType
        {
            // удалить таблицу
            tool.dropTable("dip_c_type", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dipDocumentType");

        }

        // удалить сущность dipDocTemplate
        {
            // удалить таблицу
            tool.dropTable("dip_doc_template", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dipDocTemplate");

        }

        // удалить сущность dipAggregationMethod
        {
            // удалить таблицу
            tool.dropTable("dip_c_aggregation", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dipAggregationMethod");

        }

    }
}