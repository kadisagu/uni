package ru.tandemservice.diplomarmc.base.bo.OrgUnitDiploma;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

@Configuration
public class OrgUnitDiplomaManager extends BusinessObjectManager {
    public static OrgUnitDiplomaManager instance() {
        return instance(OrgUnitDiplomaManager.class);
    }
}
