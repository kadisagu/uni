package ru.tandemservice.diplomarmc.component.dipDocument.ProtocolSACPub;

import org.tandemframework.core.component.Activator;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.diplomarmc.entity.RelProtocolSACToStudent;

import java.util.Map;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        ((IDAO) getDao()).prepare((Model) getModel(component));
        prepareDataSource(component);
    }

    public void prepareDataSource(IBusinessComponent component) {
        final Model model = (Model) getModel(component);
        if (model.getDataSource() != null) return;
        DynamicListDataSource<RelProtocolSACToStudent> dataSource = new DynamicListDataSource<>(component, component1 -> {
            ((IDAO) Controller.this.getDao()).prepareListDataSource(model);
        });

        PublisherLinkColumn linkColumn = new PublisherLinkColumn("ФИО", RelProtocolSACToStudent.student().person().fullFio());

        linkColumn.setResolver(new IPublisherLinkResolver() {
            @Override
            public String getComponentName(IEntity ientity) {
                return "ru.tandemservice.uni.component.student.StudentPub";
            }

            @Override
            public Object getParameters(IEntity entity) {
                ParametersMap params = new ParametersMap();
                params.put(PublisherActivator.PUBLISHER_ID_KEY, ((RelProtocolSACToStudent) entity).getStudent().getId());
                params.put(ru.tandemservice.uni.component.student.StudentPub.Model.SELECTED_TAB_BIND_KEY, "studentTab");
                return params;
            }
        });

        dataSource.addColumn(linkColumn);
        dataSource.addColumn(new SimpleColumn("Личный номер", RelProtocolSACToStudent.student().personalNumber()));
        dataSource.addColumn(new SimpleColumn("Состояние", RelProtocolSACToStudent.student().status().title()));
        dataSource.addColumn(new BlockColumn<String>("themaVKREdit", "Тема ВКР").setOrderable(false).setWidth("150").setVisible(model.isSpecifyThemaVKR()));
        dataSource.addColumn(new SimpleColumn("Тема ВКР", RelProtocolSACToStudent.themaVKR()).setVisible(!model.isSpecifyThemaVKR()));
        dataSource.addColumn(new SimpleColumn("Квалификация", RelProtocolSACToStudent.protocolSAC().qualifications().title()).setClickable(false));
        dataSource.addColumn(new ToggleColumn("Диплом с отличием", RelProtocolSACToStudent.hons()).setListener("onClickToggleHons"));
        dataSource.addColumn(new SimpleColumn("Направление/специальность ГОС", RelProtocolSACToStudent.protocolSAC().educationLevel().displayableTitle()));
        dataSource.addColumn(new SimpleColumn("Профиль/специализация", RelProtocolSACToStudent.student().educationOrgUnit().educationLevelHighSchool().displayableTitle()));
        dataSource.addColumn(new SimpleColumn("Форма освоения", RelProtocolSACToStudent.student().educationOrgUnit().developForm().title()));
        dataSource.addColumn(new ActionColumn("Удалить", "delete", "onClickDeleteItem", "Удалить элемент «{0}» ?", new Object[]{RelProtocolSACToStudent.student().person().fullFio()})
                                     .setPermissionKey("diplomarmc_relP2Sremove"));


        model.setDataSource(dataSource);
    }

    public void onClickDeleteItem(IBusinessComponent component) {
        getDao().deleteRow(component);
    }

    @SuppressWarnings("unchecked")
    public void onClickToggleHons(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        Map<Long, String> map = ((BlockColumn<String>) model.getDataSource().getColumn("themaVKREdit")).getValueMap();
        model.setValueMap(map);

        ((IDAO) getDao()).updateHons(model, (Long) component.getListenerParameter());
    }

    public void onClickSave(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        ((IDAO) getDao()).update(model);
        model.setSpecifyThemaVKR(false);
        model.getDataSource().refresh();
    }

    public void onClickHonsEdit(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        model.getDataSource().refresh();
    }

    public void onClickThemaVKREdit(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        model.setSpecifyThemaVKR(true);
        model.getDataSource().refresh();
    }

    public void onClickCancel(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        model.setSpecifyThemaVKR(false);
        model.getDataSource().refresh();
    }

    public void onClickProtocolSACEdit(IBusinessComponent component) {
        Model model = (Model) getModel(component);

        Activator activator = new ComponentActivator("ru.tandemservice.diplomarmc.component.orgunit.StudentToProtocolSAC",
                                                     new ParametersMap().add("protocolSACId", model.getProtocolSAC().getId())
        );

        UserContext.getInstance().activateDesktop("PersonShellDialog", activator);
    }
}
