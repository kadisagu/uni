package ru.tandemservice.diplomarmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.diplomarmc.entity.EppStateEduStandardExt;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение сущности Государственный образовательный стандарт
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppStateEduStandardExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.diplomarmc.entity.EppStateEduStandardExt";
    public static final String ENTITY_NAME = "eppStateEduStandardExt";
    public static final int VERSION_HASH = 2144735876;
    private static IEntityMeta ENTITY_META;

    public static final String L_EPP_STATE_EDU_STANDARD = "eppStateEduStandard";
    public static final String L_DEVELOP_PERIOD = "developPeriod";

    private EppStateEduStandard _eppStateEduStandard;     // Государственный образовательный стандарт
    private DevelopPeriod _developPeriod;     // Нормативный срок обучения по очной форме

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Государственный образовательный стандарт. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EppStateEduStandard getEppStateEduStandard()
    {
        return _eppStateEduStandard;
    }

    /**
     * @param eppStateEduStandard Государственный образовательный стандарт. Свойство не может быть null и должно быть уникальным.
     */
    public void setEppStateEduStandard(EppStateEduStandard eppStateEduStandard)
    {
        dirty(_eppStateEduStandard, eppStateEduStandard);
        _eppStateEduStandard = eppStateEduStandard;
    }

    /**
     * @return Нормативный срок обучения по очной форме. Свойство не может быть null.
     */
    @NotNull
    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }

    /**
     * @param developPeriod Нормативный срок обучения по очной форме. Свойство не может быть null.
     */
    public void setDevelopPeriod(DevelopPeriod developPeriod)
    {
        dirty(_developPeriod, developPeriod);
        _developPeriod = developPeriod;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppStateEduStandardExtGen)
        {
            setEppStateEduStandard(((EppStateEduStandardExt)another).getEppStateEduStandard());
            setDevelopPeriod(((EppStateEduStandardExt)another).getDevelopPeriod());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppStateEduStandardExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppStateEduStandardExt.class;
        }

        public T newInstance()
        {
            return (T) new EppStateEduStandardExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eppStateEduStandard":
                    return obj.getEppStateEduStandard();
                case "developPeriod":
                    return obj.getDevelopPeriod();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eppStateEduStandard":
                    obj.setEppStateEduStandard((EppStateEduStandard) value);
                    return;
                case "developPeriod":
                    obj.setDevelopPeriod((DevelopPeriod) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eppStateEduStandard":
                        return true;
                case "developPeriod":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eppStateEduStandard":
                    return true;
                case "developPeriod":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eppStateEduStandard":
                    return EppStateEduStandard.class;
                case "developPeriod":
                    return DevelopPeriod.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppStateEduStandardExt> _dslPath = new Path<EppStateEduStandardExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppStateEduStandardExt");
    }
            

    /**
     * @return Государственный образовательный стандарт. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.diplomarmc.entity.EppStateEduStandardExt#getEppStateEduStandard()
     */
    public static EppStateEduStandard.Path<EppStateEduStandard> eppStateEduStandard()
    {
        return _dslPath.eppStateEduStandard();
    }

    /**
     * @return Нормативный срок обучения по очной форме. Свойство не может быть null.
     * @see ru.tandemservice.diplomarmc.entity.EppStateEduStandardExt#getDevelopPeriod()
     */
    public static DevelopPeriod.Path<DevelopPeriod> developPeriod()
    {
        return _dslPath.developPeriod();
    }

    public static class Path<E extends EppStateEduStandardExt> extends EntityPath<E>
    {
        private EppStateEduStandard.Path<EppStateEduStandard> _eppStateEduStandard;
        private DevelopPeriod.Path<DevelopPeriod> _developPeriod;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Государственный образовательный стандарт. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.diplomarmc.entity.EppStateEduStandardExt#getEppStateEduStandard()
     */
        public EppStateEduStandard.Path<EppStateEduStandard> eppStateEduStandard()
        {
            if(_eppStateEduStandard == null )
                _eppStateEduStandard = new EppStateEduStandard.Path<EppStateEduStandard>(L_EPP_STATE_EDU_STANDARD, this);
            return _eppStateEduStandard;
        }

    /**
     * @return Нормативный срок обучения по очной форме. Свойство не может быть null.
     * @see ru.tandemservice.diplomarmc.entity.EppStateEduStandardExt#getDevelopPeriod()
     */
        public DevelopPeriod.Path<DevelopPeriod> developPeriod()
        {
            if(_developPeriod == null )
                _developPeriod = new DevelopPeriod.Path<DevelopPeriod>(L_DEVELOP_PERIOD, this);
            return _developPeriod;
        }

        public Class getEntityClass()
        {
            return EppStateEduStandardExt.class;
        }

        public String getEntityName()
        {
            return "eppStateEduStandardExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
