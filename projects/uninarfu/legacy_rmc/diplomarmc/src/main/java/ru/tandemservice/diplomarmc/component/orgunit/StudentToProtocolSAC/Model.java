package ru.tandemservice.diplomarmc.component.orgunit.StudentToProtocolSAC;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.diplomarmc.entity.ProtocolSAC;
import ru.tandemservice.uni.entity.catalog.Qualifications;

import java.util.Collections;
import java.util.List;

@Input(
        {
                @Bind(key = "ids", binding = "studentIds"),
                @Bind(key = "orgUnitId", binding = "orgUnitId"),
                @Bind(key = "protocolSACId", binding = "protocolSACId"),
                @Bind(key = "educationLevel", binding = "protocolSAC.educationLevel"),
                @Bind(key = "qualification", binding = "protocolSAC.qualifications")
        }
)
public class Model {

    private ProtocolSAC protocolSAC = new ProtocolSAC();
    private List<Long> studentIds = Collections.emptyList();
    private List<Qualifications> qualificationsList;
    private List<Employee> employeeList;
    private Long orgUnitId;
    private ISelectModel formativeOrgUnitModel;
    private ISelectModel educationLevelsModel;
    private Long protocolSACId;

    public ISelectModel getEducationLevelsModel() {
        return educationLevelsModel;
    }

    public void setEducationLevelsModel(ISelectModel educationLevelsModel) {
        this.educationLevelsModel = educationLevelsModel;
    }

    public Long getProtocolSACId() {
        return protocolSACId;
    }

    public void setProtocolSACId(Long protocolSACId) {
        this.protocolSACId = protocolSACId;
    }

    public ISelectModel getFormativeOrgUnitModel() {
        return formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel) {
        this.formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public Long getOrgUnitId() {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId) {
        this.orgUnitId = orgUnitId;
    }

    public ProtocolSAC getProtocolSAC() {
        return protocolSAC;
    }

    public void setProtocolSAC(ProtocolSAC protocolSAC) {
        this.protocolSAC = protocolSAC;
    }

    public List<Long> getStudentIds() {
        return studentIds;
    }

    public void setStudentIds(List<Long> studentIds) {
        this.studentIds = studentIds;
    }

    public List<Qualifications> getQualificationsList() {
        return qualificationsList;
    }

    public void setQualificationsList(List<Qualifications> qualificationsList) {
        this.qualificationsList = qualificationsList;
    }

    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(List<Employee> employeeList) {
        this.employeeList = employeeList;
    }
}
