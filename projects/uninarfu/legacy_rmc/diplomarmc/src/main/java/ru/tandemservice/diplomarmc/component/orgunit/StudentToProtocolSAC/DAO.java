package ru.tandemservice.diplomarmc.component.orgunit.StudentToProtocolSAC;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.diplomarmc.entity.ProtocolSAC;
import ru.tandemservice.diplomarmc.entity.RelProtocolSACToStudent;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.codes.StructureEducationLevelsCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;

import java.util.*;

public class DAO extends UniDao<Model> {

    private static final Set<String> STRUCTURE_EDUCATION_LEVELS_CODES = Collections.unmodifiableSet(new LinkedHashSet<String>(Arrays.asList(new String[]{
            StructureEducationLevelsCodes.HIGH_GOS2_GROUP_MASTER_PROFILE, StructureEducationLevelsCodes.HIGH_GOS2_GROUP_BACHELOR_PROFILE,
//	        StructureEducationLevelsCodes.HIGH_GOS2_GROUP_SPECIALTY_SPECIALIZATION, StructureEducationLevelsCodes.HIGH_GOS2_GROUP_SPECIALTY_FIELD_SPECIALTY_SPECIALIZATION,
            StructureEducationLevelsCodes.HIGH_GOS3_GROUP_BACHELOR_PROFILE, StructureEducationLevelsCodes.HIGH_GOS3_GROUP_MASTER_PROFILE, //StructureEducationLevelsCodes.HIGH_GOS3_GROUP_SPECIALIZATION
    })));

    @Override
    public void prepare(Model model) {
        if (model.getProtocolSACId() != null)
            model.setProtocolSAC((ProtocolSAC) getNotNull(model.getProtocolSACId()));
        if (model.getOrgUnitId() != null) {
            OrgUnit orgUnit = (OrgUnit) getNotNull(model.getOrgUnitId());
            model.getProtocolSAC().setFormativeOrgUnit(orgUnit);
        }
        model.getProtocolSAC().setDate(new Date());
        model.setEmployeeList(getList(Employee.class));
        model.setQualificationsList(getCatalogItemListOrderByCode(Qualifications.class));
        model.setFormativeOrgUnitModel(new UniQueryFullCheckSelectModel(new String[]{OrgUnit.title().s()}) {

            @Override
            protected MQBuilder query(String alias, String filter) {
                MQBuilder subBuilder = new MQBuilder(Student.ENTITY_CLASS, "s")
                        .addJoin("s", Student.educationOrgUnit(), "edu")
                        .addJoin("edu", EducationOrgUnit.formativeOrgUnit(), "fou");
                subBuilder.getSelectAliasList().clear();
                subBuilder.addSelect("fou.id");
                subBuilder.setNeedDistinct(true);

                MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, alias)
                        .add(MQExpression.in(alias, OrgUnit.id(), subBuilder));

                if (!StringUtils.isEmpty(filter))
                    builder.add(
                            MQExpression.like(alias, OrgUnit.title(), filter)
                    );
                return builder;
            }
        });

        model.setEducationLevelsModel(new UniQueryFullCheckSelectModel(new String[]{EducationLevels.displayableTitle().s()}) {

            @Override
            protected MQBuilder query(String alias, String filter) {
                MQBuilder builder = new MQBuilder(EducationLevels.ENTITY_CLASS, alias)
                        .add(MQExpression.notIn(alias, EducationLevels.levelType().code(), STRUCTURE_EDUCATION_LEVELS_CODES));


                if (!StringUtils.isEmpty(filter))
                    builder.add(
                            MQExpression.like(alias, EducationLevels.displayableTitle(), filter)
                    );
                return builder;
            }
        });
    }

    @Override
    public void update(Model model) {
        saveOrUpdate(model.getProtocolSAC());
        if (model.getProtocolSACId() == null) {
            for (Long id : model.getStudentIds()) {
                Student student = getNotNull(id);
                RelProtocolSACToStudent rel = new RelProtocolSACToStudent();
                rel.setProtocolSAC(model.getProtocolSAC());
                rel.setStudent(student);
                rel.setThemaVKR(student.getFinalQualifyingWorkTheme());
                rel.setHons(false);
                saveOrUpdate(rel);
            }
        }
    }

}
