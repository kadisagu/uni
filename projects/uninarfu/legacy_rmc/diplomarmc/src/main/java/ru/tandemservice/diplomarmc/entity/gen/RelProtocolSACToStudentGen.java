package ru.tandemservice.diplomarmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.diplomarmc.entity.ProtocolSAC;
import ru.tandemservice.diplomarmc.entity.RelProtocolSACToStudent;
import ru.tandemservice.uni.entity.employee.Student;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущость-связь Протокол ГАК - Студент
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RelProtocolSACToStudentGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.diplomarmc.entity.RelProtocolSACToStudent";
    public static final String ENTITY_NAME = "relProtocolSACToStudent";
    public static final int VERSION_HASH = -290799479;
    private static IEntityMeta ENTITY_META;

    public static final String L_PROTOCOL_S_A_C = "protocolSAC";
    public static final String L_STUDENT = "student";
    public static final String P_THEMA_V_K_R = "themaVKR";
    public static final String P_HONS = "hons";

    private ProtocolSAC _protocolSAC;     // Протокол ГАК
    private Student _student;     // Студент
    private String _themaVKR;     // Тема ВКР
    private Boolean _hons;     // С отличием

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Протокол ГАК.
     */
    public ProtocolSAC getProtocolSAC()
    {
        return _protocolSAC;
    }

    /**
     * @param protocolSAC Протокол ГАК.
     */
    public void setProtocolSAC(ProtocolSAC protocolSAC)
    {
        dirty(_protocolSAC, protocolSAC);
        _protocolSAC = protocolSAC;
    }

    /**
     * @return Студент.
     */
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Тема ВКР.
     */
    @Length(max=1000)
    public String getThemaVKR()
    {
        return _themaVKR;
    }

    /**
     * @param themaVKR Тема ВКР.
     */
    public void setThemaVKR(String themaVKR)
    {
        dirty(_themaVKR, themaVKR);
        _themaVKR = themaVKR;
    }

    /**
     * @return С отличием.
     */
    public Boolean getHons()
    {
        return _hons;
    }

    /**
     * @param hons С отличием.
     */
    public void setHons(Boolean hons)
    {
        dirty(_hons, hons);
        _hons = hons;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RelProtocolSACToStudentGen)
        {
            setProtocolSAC(((RelProtocolSACToStudent)another).getProtocolSAC());
            setStudent(((RelProtocolSACToStudent)another).getStudent());
            setThemaVKR(((RelProtocolSACToStudent)another).getThemaVKR());
            setHons(((RelProtocolSACToStudent)another).getHons());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RelProtocolSACToStudentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RelProtocolSACToStudent.class;
        }

        public T newInstance()
        {
            return (T) new RelProtocolSACToStudent();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "protocolSAC":
                    return obj.getProtocolSAC();
                case "student":
                    return obj.getStudent();
                case "themaVKR":
                    return obj.getThemaVKR();
                case "hons":
                    return obj.getHons();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "protocolSAC":
                    obj.setProtocolSAC((ProtocolSAC) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "themaVKR":
                    obj.setThemaVKR((String) value);
                    return;
                case "hons":
                    obj.setHons((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "protocolSAC":
                        return true;
                case "student":
                        return true;
                case "themaVKR":
                        return true;
                case "hons":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "protocolSAC":
                    return true;
                case "student":
                    return true;
                case "themaVKR":
                    return true;
                case "hons":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "protocolSAC":
                    return ProtocolSAC.class;
                case "student":
                    return Student.class;
                case "themaVKR":
                    return String.class;
                case "hons":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RelProtocolSACToStudent> _dslPath = new Path<RelProtocolSACToStudent>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RelProtocolSACToStudent");
    }
            

    /**
     * @return Протокол ГАК.
     * @see ru.tandemservice.diplomarmc.entity.RelProtocolSACToStudent#getProtocolSAC()
     */
    public static ProtocolSAC.Path<ProtocolSAC> protocolSAC()
    {
        return _dslPath.protocolSAC();
    }

    /**
     * @return Студент.
     * @see ru.tandemservice.diplomarmc.entity.RelProtocolSACToStudent#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Тема ВКР.
     * @see ru.tandemservice.diplomarmc.entity.RelProtocolSACToStudent#getThemaVKR()
     */
    public static PropertyPath<String> themaVKR()
    {
        return _dslPath.themaVKR();
    }

    /**
     * @return С отличием.
     * @see ru.tandemservice.diplomarmc.entity.RelProtocolSACToStudent#getHons()
     */
    public static PropertyPath<Boolean> hons()
    {
        return _dslPath.hons();
    }

    public static class Path<E extends RelProtocolSACToStudent> extends EntityPath<E>
    {
        private ProtocolSAC.Path<ProtocolSAC> _protocolSAC;
        private Student.Path<Student> _student;
        private PropertyPath<String> _themaVKR;
        private PropertyPath<Boolean> _hons;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Протокол ГАК.
     * @see ru.tandemservice.diplomarmc.entity.RelProtocolSACToStudent#getProtocolSAC()
     */
        public ProtocolSAC.Path<ProtocolSAC> protocolSAC()
        {
            if(_protocolSAC == null )
                _protocolSAC = new ProtocolSAC.Path<ProtocolSAC>(L_PROTOCOL_S_A_C, this);
            return _protocolSAC;
        }

    /**
     * @return Студент.
     * @see ru.tandemservice.diplomarmc.entity.RelProtocolSACToStudent#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Тема ВКР.
     * @see ru.tandemservice.diplomarmc.entity.RelProtocolSACToStudent#getThemaVKR()
     */
        public PropertyPath<String> themaVKR()
        {
            if(_themaVKR == null )
                _themaVKR = new PropertyPath<String>(RelProtocolSACToStudentGen.P_THEMA_V_K_R, this);
            return _themaVKR;
        }

    /**
     * @return С отличием.
     * @see ru.tandemservice.diplomarmc.entity.RelProtocolSACToStudent#getHons()
     */
        public PropertyPath<Boolean> hons()
        {
            if(_hons == null )
                _hons = new PropertyPath<Boolean>(RelProtocolSACToStudentGen.P_HONS, this);
            return _hons;
        }

        public Class getEntityClass()
        {
            return RelProtocolSACToStudent.class;
        }

        public String getEntityName()
        {
            return "relProtocolSACToStudent";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
