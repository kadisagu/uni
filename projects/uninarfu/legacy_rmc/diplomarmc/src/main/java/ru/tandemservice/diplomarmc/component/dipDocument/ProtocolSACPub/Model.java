package ru.tandemservice.diplomarmc.component.dipDocument.ProtocolSACPub;

import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.diplomarmc.entity.ProtocolSAC;
import ru.tandemservice.diplomarmc.entity.RelProtocolSACToStudent;

import java.util.Map;

@State({@org.tandemframework.core.component.Bind(key = "id", binding = "id")})
public class Model {

    private Long id;
    private ProtocolSAC protocolSAC;
    private DynamicListDataSource<RelProtocolSACToStudent> dataSource;
    private Map<Long, String> valueMap;

    private boolean specifyThemaVKR = false;

    public boolean isSpecifyThemaVKR() {
        return specifyThemaVKR;
    }

    public void setSpecifyThemaVKR(boolean specifyThemaVKR) {
        this.specifyThemaVKR = specifyThemaVKR;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ProtocolSAC getProtocolSAC() {
        return protocolSAC;
    }

    public void setProtocolSAC(ProtocolSAC protocolSAC) {
        this.protocolSAC = protocolSAC;
    }

    public DynamicListDataSource<RelProtocolSACToStudent> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<RelProtocolSACToStudent> dataSource) {
        this.dataSource = dataSource;
    }

    public Map<Long, String> getValueMap() {
        return valueMap;
    }

    public void setValueMap(Map<Long, String> valueMap) {
        this.valueMap = valueMap;
    }

}
