package ru.tandemservice.diplomarmc.base.ext.OrgUnit.ui.NewEdit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.IPresenterExtensionBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.NewEdit.OrgUnitNewEdit;

@Configuration
public class OrgUnitNewEditExt extends BusinessComponentExtensionManager {

    @Autowired
    private OrgUnitNewEdit parent;

    @Bean
    public PresenterExtension presenterExtension() {
        IPresenterExtensionBuilder pi = presenterExtensionBuilder(parent.presenterExtPoint());
        pi.addAddon(uiAddon("orgUnitNewEditAddon", OrgUnitNewEditExtUI.class));
        return pi.create();
    }
}
