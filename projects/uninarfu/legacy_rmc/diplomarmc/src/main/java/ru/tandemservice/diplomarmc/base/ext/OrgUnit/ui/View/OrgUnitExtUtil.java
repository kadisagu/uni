package ru.tandemservice.diplomarmc.base.ext.OrgUnit.ui.View;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.diplomarmc.entity.OrgUnitExt;
import ru.tandemservice.uni.dao.UniDaoFacade;

public class OrgUnitExtUtil {

    public static OrgUnitExt getOrgUnitExt(OrgUnit orgUnit) {

        OrgUnitExt orgUnitExt = UniDaoFacade.getCoreDao().get(OrgUnitExt.class, OrgUnitExt.orgUnit(), orgUnit);

        if (orgUnitExt != null)
            return orgUnitExt;

        orgUnitExt = new OrgUnitExt();
        orgUnitExt.setOrgUnit(orgUnit);

        return orgUnitExt;
    }

}
