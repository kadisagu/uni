package ru.tandemservice.brsrmc.component.journaltestresult.JournalTestResultList;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.brsrmc.entity.JournalTestResult;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

public class Model {
    // Формирующее подразделение (с поиском, одиночный выбор)
    private ISelectModel formativeOrgUnitModel;
    private OrgUnit formativeOrgUnit;
    // Академическая группа (с поимском, зависит от формирующего подразделения)
    private ISelectModel groupModel;
    private Group group;
    // Список студнтов(одиночный выбор, зависит от фп и академ группы)
    private ISelectModel studentModel;
    private Student student;
    // Вид активности (дистинкт от JournalTestResult)
    private ISelectModel activityModel;
    // Учебный год
    private ISelectModel yearModel;
    private EducationYear year;
    // Часть учебного года
    private ISelectModel yearPartModel;
    private YearDistributionPart yearPart;

    private IDataSettings settings;

    private DynamicListDataSource<JournalTestResult> dataSource;

    public ISelectModel getFormativeOrgUnitModel() {
        return formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel) {
        this.formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public OrgUnit getFormativeOrgUnit() {
        return formativeOrgUnit;
    }

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit) {
        this.formativeOrgUnit = formativeOrgUnit;
    }

    public DynamicListDataSource<JournalTestResult> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<JournalTestResult> dataSource) {
        this.dataSource = dataSource;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public ISelectModel getGroupModel() {
        return groupModel;
    }

    public void setGroupModel(ISelectModel groupModel) {
        this.groupModel = groupModel;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public ISelectModel getStudentModel() {
        return studentModel;
    }

    public void setStudentModel(ISelectModel studentModel) {
        this.studentModel = studentModel;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public ISelectModel getActivityModel() {
        return activityModel;
    }

    public void setActivityModel(ISelectModel activityModel) {
        this.activityModel = activityModel;
    }

    public ISelectModel getYearModel() {
        return yearModel;
    }

    public void setYearModel(ISelectModel yearModel) {
        this.yearModel = yearModel;
    }

    public EducationYear getYear() {
        return year;
    }

    public void setYear(EducationYear year) {
        this.year = year;
    }

    public ISelectModel getYearPartModel() {
        return yearPartModel;
    }

    public void setYearPartModel(ISelectModel yearPartModel) {
        this.yearPartModel = yearPartModel;
    }

    public YearDistributionPart getYearPart() {
        return yearPart;
    }

    public void setYearPart(YearDistributionPart yearPart) {
        this.yearPart = yearPart;
    }
}
