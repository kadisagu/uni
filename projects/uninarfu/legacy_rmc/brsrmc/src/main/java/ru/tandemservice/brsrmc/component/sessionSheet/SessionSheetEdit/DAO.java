package ru.tandemservice.brsrmc.component.sessionSheet.SessionSheetEdit;

import ru.tandemservice.brsrmc.util.BrsUtil;

public class DAO extends ru.tandemservice.unisession.component.sessionSheet.SessionSheetEdit.DAO {
    @Override
    public void prepare(ru.tandemservice.unisession.component.sessionSheet.SessionSheetEdit.Model model) {
        super.prepare(model);
        Model myModel = (Model) model;
        myModel.setAlternative(BrsUtil.getUseRecordBookInSessionMode());
    }
}
