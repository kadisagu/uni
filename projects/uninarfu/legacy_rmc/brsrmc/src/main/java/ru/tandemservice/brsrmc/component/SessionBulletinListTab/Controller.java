package ru.tandemservice.brsrmc.component.SessionBulletinListTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.brsrmc.util.BrsUtil;

public class Controller extends ru.tandemservice.unisession.component.orgunit.SessionBulletinListTab.Controller {

    @Override
    public void onClickMark(IBusinessComponent component) {
        boolean useRb = BrsUtil.getUseRecordBookInSessionMode();
        if (useRb) {
            ContextLocal.createDesktop(new ComponentActivator(
                    ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinMark.Model.COMPONENT_NAME,
                    (new UniMap()).add(PublisherActivator.PUBLISHER_ID_KEY, component.<Long>getListenerParameter())), null);
        }
        else {
            super.onClickMark(component);
        }
    }
}
