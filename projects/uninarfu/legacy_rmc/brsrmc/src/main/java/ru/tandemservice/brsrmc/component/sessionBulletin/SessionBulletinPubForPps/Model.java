package ru.tandemservice.brsrmc.component.sessionBulletin.SessionBulletinPubForPps;

public class Model extends ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPubForPps.Model {
    private boolean alternative;

    public boolean isAlternative() {
        return alternative;
    }

    public void setAlternative(boolean alternative) {
        this.alternative = alternative;
    }
}
