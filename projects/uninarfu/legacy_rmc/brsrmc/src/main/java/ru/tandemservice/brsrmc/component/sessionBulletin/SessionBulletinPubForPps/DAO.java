package ru.tandemservice.brsrmc.component.sessionBulletin.SessionBulletinPubForPps;

import ru.tandemservice.brsrmc.util.BrsUtil;

public class DAO extends ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPubForPps.DAO {
    @Override
    public void prepare(ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPubForPps.Model model) {
        super.prepare(model);    //To change body of overridden methods use File | Settings | File Templates.
        Model myModel = (Model) model;
        myModel.setAlternative(BrsUtil.getUseRecordBookInSessionMode());
    }
}
