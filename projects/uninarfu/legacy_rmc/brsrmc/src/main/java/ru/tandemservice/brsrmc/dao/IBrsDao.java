package ru.tandemservice.brsrmc.dao;

import ru.tandemservice.brsrmc.ws.types.StudentMarkWs;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.ISessionMarkDAO;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;

import java.util.Map;

public interface IBrsDao extends IUniBaseDao {
    public ISessionMarkDAO.MarkData getVoidMarkData();

    public Map<Long, StudentMarkWs> getJournalMarkData(SessionDocument doc);

    public ISessionMarkDAO.MarkData prepareMarkDataForUpdate(SessionDocumentSlot slot, StudentMarkWs markWs);
}
