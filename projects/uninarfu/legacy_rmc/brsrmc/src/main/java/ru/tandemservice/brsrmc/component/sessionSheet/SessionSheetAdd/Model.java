package ru.tandemservice.brsrmc.component.sessionSheet.SessionSheetAdd;

public class Model extends ru.tandemservice.unisession.component.sessionSheet.SessionSheetAdd.Model {
    private boolean alternative;

    public boolean isAlternative() {
        return alternative;
    }

    public void setAlternative(boolean alternative) {
        this.alternative = alternative;
    }
}
