package ru.tandemservice.brsrmc.component.settings.UseRecordBookInSession;

import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import ru.tandemservice.uni.dao.UniDao;

public class DAO extends UniDao<Model> implements IDAO, IUseRecordBookInSessionKey {

    @Override
    public void prepare(Model model) {
        IDataSettings settings = DataSettingsFacade.getSettings(OWNER_KEY);
        Object useValue = settings.get(SETTING_KEY);

        if (useValue != null) {
            model.setUse((Boolean) useValue);
        }
    }

    @Override
    public void saveSettings(Model model) {
        IDataSettings settings = DataSettingsFacade.getSettings(OWNER_KEY);
        settings.set(SETTING_KEY, model.isUse());
        DataSettingsFacade.saveSettings(settings);
    }
}
