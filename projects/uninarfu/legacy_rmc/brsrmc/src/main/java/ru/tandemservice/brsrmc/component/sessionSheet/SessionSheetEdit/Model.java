package ru.tandemservice.brsrmc.component.sessionSheet.SessionSheetEdit;

public class Model extends ru.tandemservice.unisession.component.sessionSheet.SessionSheetEdit.Model {
    private boolean alternative;

    public boolean isAlternative() {
        return alternative;
    }

    public void setAlternative(boolean alternative) {
        this.alternative = alternative;
    }

    public boolean isNotAlternative() {
        return !alternative;
    }
}
