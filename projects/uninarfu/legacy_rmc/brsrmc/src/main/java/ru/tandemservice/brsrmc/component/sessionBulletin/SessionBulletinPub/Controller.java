package ru.tandemservice.brsrmc.component.sessionBulletin.SessionBulletinPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.util.UniMap;

public class Controller extends ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPub.Controller {
    public void onClickMarkOX(IBusinessComponent component) {
        IDAO dao = (IDAO) getDao();
        Model model = (Model) getModel(component);
        if (!dao.hasTutors(model)) {
            throw new ApplicationException("Нельзя вызывать журнал без указания преподавателя. ");
        }
        ContextLocal.createDesktop(new ComponentActivator(
                                           ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinMark.Model.COMPONENT_NAME,
                                           new UniMap().add(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(component).getBulletin().getId())),
                                   null);
    }

}
