package ru.tandemservice.brsrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.brsrmc.entity.JournalTestResult;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Результаты тестирования
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class JournalTestResultGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.brsrmc.entity.JournalTestResult";
    public static final String ENTITY_NAME = "journalTestResult";
    public static final int VERSION_HASH = 159649909;
    private static IEntityMeta ENTITY_META;

    public static final String P_SOURCE_MARK_ID = "sourceMarkId";
    public static final String P_STUDENT_ID = "studentId";
    public static final String P_ACTIVITY_ID = "activityId";
    public static final String P_RESULT_MARK = "resultMark";
    public static final String P_ERROR_CODE = "errorCode";
    public static final String P_ERROR_TEXT = "errorText";
    public static final String P_ACTIVITY = "activity";
    public static final String P_ACTIVITY_CODE = "activityCode";
    public static final String P_DOCUMENT_ID = "documentId";
    public static final String P_DATE = "date";

    private Long _sourceMarkId;     // id оценки
    private Long _studentId;     // id студента
    private Long _activityId;     // id колонки
    private Integer _resultMark;     // оценка, которая будет в журнале в итоге(после просчитывания пропорций)
    private String _errorCode;     // код ошибки
    private String _errorText;     // текст ошибки
    private String _activity;     // активити
    private String _activityCode;     // код деятельности
    private Long _documentId;     // id документа
    private Date _date;     // дата выполнения действия

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return id оценки.
     */
    public Long getSourceMarkId()
    {
        return _sourceMarkId;
    }

    /**
     * @param sourceMarkId id оценки.
     */
    public void setSourceMarkId(Long sourceMarkId)
    {
        dirty(_sourceMarkId, sourceMarkId);
        _sourceMarkId = sourceMarkId;
    }

    /**
     * @return id студента.
     */
    public Long getStudentId()
    {
        return _studentId;
    }

    /**
     * @param studentId id студента.
     */
    public void setStudentId(Long studentId)
    {
        dirty(_studentId, studentId);
        _studentId = studentId;
    }

    /**
     * @return id колонки.
     */
    public Long getActivityId()
    {
        return _activityId;
    }

    /**
     * @param activityId id колонки.
     */
    public void setActivityId(Long activityId)
    {
        dirty(_activityId, activityId);
        _activityId = activityId;
    }

    /**
     * @return оценка, которая будет в журнале в итоге(после просчитывания пропорций).
     */
    public Integer getResultMark()
    {
        return _resultMark;
    }

    /**
     * @param resultMark оценка, которая будет в журнале в итоге(после просчитывания пропорций).
     */
    public void setResultMark(Integer resultMark)
    {
        dirty(_resultMark, resultMark);
        _resultMark = resultMark;
    }

    /**
     * @return код ошибки.
     */
    @Length(max=255)
    public String getErrorCode()
    {
        return _errorCode;
    }

    /**
     * @param errorCode код ошибки.
     */
    public void setErrorCode(String errorCode)
    {
        dirty(_errorCode, errorCode);
        _errorCode = errorCode;
    }

    /**
     * @return текст ошибки.
     */
    @Length(max=255)
    public String getErrorText()
    {
        return _errorText;
    }

    /**
     * @param errorText текст ошибки.
     */
    public void setErrorText(String errorText)
    {
        dirty(_errorText, errorText);
        _errorText = errorText;
    }

    /**
     * @return активити.
     */
    public String getActivity()
    {
        return _activity;
    }

    /**
     * @param activity активити.
     */
    public void setActivity(String activity)
    {
        dirty(_activity, activity);
        _activity = activity;
    }

    /**
     * @return код деятельности.
     */
    @Length(max=255)
    public String getActivityCode()
    {
        return _activityCode;
    }

    /**
     * @param activityCode код деятельности.
     */
    public void setActivityCode(String activityCode)
    {
        dirty(_activityCode, activityCode);
        _activityCode = activityCode;
    }

    /**
     * @return id документа.
     */
    public Long getDocumentId()
    {
        return _documentId;
    }

    /**
     * @param documentId id документа.
     */
    public void setDocumentId(Long documentId)
    {
        dirty(_documentId, documentId);
        _documentId = documentId;
    }

    /**
     * @return дата выполнения действия.
     */
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date дата выполнения действия.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof JournalTestResultGen)
        {
            setSourceMarkId(((JournalTestResult)another).getSourceMarkId());
            setStudentId(((JournalTestResult)another).getStudentId());
            setActivityId(((JournalTestResult)another).getActivityId());
            setResultMark(((JournalTestResult)another).getResultMark());
            setErrorCode(((JournalTestResult)another).getErrorCode());
            setErrorText(((JournalTestResult)another).getErrorText());
            setActivity(((JournalTestResult)another).getActivity());
            setActivityCode(((JournalTestResult)another).getActivityCode());
            setDocumentId(((JournalTestResult)another).getDocumentId());
            setDate(((JournalTestResult)another).getDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends JournalTestResultGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) JournalTestResult.class;
        }

        public T newInstance()
        {
            return (T) new JournalTestResult();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "sourceMarkId":
                    return obj.getSourceMarkId();
                case "studentId":
                    return obj.getStudentId();
                case "activityId":
                    return obj.getActivityId();
                case "resultMark":
                    return obj.getResultMark();
                case "errorCode":
                    return obj.getErrorCode();
                case "errorText":
                    return obj.getErrorText();
                case "activity":
                    return obj.getActivity();
                case "activityCode":
                    return obj.getActivityCode();
                case "documentId":
                    return obj.getDocumentId();
                case "date":
                    return obj.getDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "sourceMarkId":
                    obj.setSourceMarkId((Long) value);
                    return;
                case "studentId":
                    obj.setStudentId((Long) value);
                    return;
                case "activityId":
                    obj.setActivityId((Long) value);
                    return;
                case "resultMark":
                    obj.setResultMark((Integer) value);
                    return;
                case "errorCode":
                    obj.setErrorCode((String) value);
                    return;
                case "errorText":
                    obj.setErrorText((String) value);
                    return;
                case "activity":
                    obj.setActivity((String) value);
                    return;
                case "activityCode":
                    obj.setActivityCode((String) value);
                    return;
                case "documentId":
                    obj.setDocumentId((Long) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "sourceMarkId":
                        return true;
                case "studentId":
                        return true;
                case "activityId":
                        return true;
                case "resultMark":
                        return true;
                case "errorCode":
                        return true;
                case "errorText":
                        return true;
                case "activity":
                        return true;
                case "activityCode":
                        return true;
                case "documentId":
                        return true;
                case "date":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "sourceMarkId":
                    return true;
                case "studentId":
                    return true;
                case "activityId":
                    return true;
                case "resultMark":
                    return true;
                case "errorCode":
                    return true;
                case "errorText":
                    return true;
                case "activity":
                    return true;
                case "activityCode":
                    return true;
                case "documentId":
                    return true;
                case "date":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "sourceMarkId":
                    return Long.class;
                case "studentId":
                    return Long.class;
                case "activityId":
                    return Long.class;
                case "resultMark":
                    return Integer.class;
                case "errorCode":
                    return String.class;
                case "errorText":
                    return String.class;
                case "activity":
                    return String.class;
                case "activityCode":
                    return String.class;
                case "documentId":
                    return Long.class;
                case "date":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<JournalTestResult> _dslPath = new Path<JournalTestResult>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "JournalTestResult");
    }
            

    /**
     * @return id оценки.
     * @see ru.tandemservice.brsrmc.entity.JournalTestResult#getSourceMarkId()
     */
    public static PropertyPath<Long> sourceMarkId()
    {
        return _dslPath.sourceMarkId();
    }

    /**
     * @return id студента.
     * @see ru.tandemservice.brsrmc.entity.JournalTestResult#getStudentId()
     */
    public static PropertyPath<Long> studentId()
    {
        return _dslPath.studentId();
    }

    /**
     * @return id колонки.
     * @see ru.tandemservice.brsrmc.entity.JournalTestResult#getActivityId()
     */
    public static PropertyPath<Long> activityId()
    {
        return _dslPath.activityId();
    }

    /**
     * @return оценка, которая будет в журнале в итоге(после просчитывания пропорций).
     * @see ru.tandemservice.brsrmc.entity.JournalTestResult#getResultMark()
     */
    public static PropertyPath<Integer> resultMark()
    {
        return _dslPath.resultMark();
    }

    /**
     * @return код ошибки.
     * @see ru.tandemservice.brsrmc.entity.JournalTestResult#getErrorCode()
     */
    public static PropertyPath<String> errorCode()
    {
        return _dslPath.errorCode();
    }

    /**
     * @return текст ошибки.
     * @see ru.tandemservice.brsrmc.entity.JournalTestResult#getErrorText()
     */
    public static PropertyPath<String> errorText()
    {
        return _dslPath.errorText();
    }

    /**
     * @return активити.
     * @see ru.tandemservice.brsrmc.entity.JournalTestResult#getActivity()
     */
    public static PropertyPath<String> activity()
    {
        return _dslPath.activity();
    }

    /**
     * @return код деятельности.
     * @see ru.tandemservice.brsrmc.entity.JournalTestResult#getActivityCode()
     */
    public static PropertyPath<String> activityCode()
    {
        return _dslPath.activityCode();
    }

    /**
     * @return id документа.
     * @see ru.tandemservice.brsrmc.entity.JournalTestResult#getDocumentId()
     */
    public static PropertyPath<Long> documentId()
    {
        return _dslPath.documentId();
    }

    /**
     * @return дата выполнения действия.
     * @see ru.tandemservice.brsrmc.entity.JournalTestResult#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    public static class Path<E extends JournalTestResult> extends EntityPath<E>
    {
        private PropertyPath<Long> _sourceMarkId;
        private PropertyPath<Long> _studentId;
        private PropertyPath<Long> _activityId;
        private PropertyPath<Integer> _resultMark;
        private PropertyPath<String> _errorCode;
        private PropertyPath<String> _errorText;
        private PropertyPath<String> _activity;
        private PropertyPath<String> _activityCode;
        private PropertyPath<Long> _documentId;
        private PropertyPath<Date> _date;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return id оценки.
     * @see ru.tandemservice.brsrmc.entity.JournalTestResult#getSourceMarkId()
     */
        public PropertyPath<Long> sourceMarkId()
        {
            if(_sourceMarkId == null )
                _sourceMarkId = new PropertyPath<Long>(JournalTestResultGen.P_SOURCE_MARK_ID, this);
            return _sourceMarkId;
        }

    /**
     * @return id студента.
     * @see ru.tandemservice.brsrmc.entity.JournalTestResult#getStudentId()
     */
        public PropertyPath<Long> studentId()
        {
            if(_studentId == null )
                _studentId = new PropertyPath<Long>(JournalTestResultGen.P_STUDENT_ID, this);
            return _studentId;
        }

    /**
     * @return id колонки.
     * @see ru.tandemservice.brsrmc.entity.JournalTestResult#getActivityId()
     */
        public PropertyPath<Long> activityId()
        {
            if(_activityId == null )
                _activityId = new PropertyPath<Long>(JournalTestResultGen.P_ACTIVITY_ID, this);
            return _activityId;
        }

    /**
     * @return оценка, которая будет в журнале в итоге(после просчитывания пропорций).
     * @see ru.tandemservice.brsrmc.entity.JournalTestResult#getResultMark()
     */
        public PropertyPath<Integer> resultMark()
        {
            if(_resultMark == null )
                _resultMark = new PropertyPath<Integer>(JournalTestResultGen.P_RESULT_MARK, this);
            return _resultMark;
        }

    /**
     * @return код ошибки.
     * @see ru.tandemservice.brsrmc.entity.JournalTestResult#getErrorCode()
     */
        public PropertyPath<String> errorCode()
        {
            if(_errorCode == null )
                _errorCode = new PropertyPath<String>(JournalTestResultGen.P_ERROR_CODE, this);
            return _errorCode;
        }

    /**
     * @return текст ошибки.
     * @see ru.tandemservice.brsrmc.entity.JournalTestResult#getErrorText()
     */
        public PropertyPath<String> errorText()
        {
            if(_errorText == null )
                _errorText = new PropertyPath<String>(JournalTestResultGen.P_ERROR_TEXT, this);
            return _errorText;
        }

    /**
     * @return активити.
     * @see ru.tandemservice.brsrmc.entity.JournalTestResult#getActivity()
     */
        public PropertyPath<String> activity()
        {
            if(_activity == null )
                _activity = new PropertyPath<String>(JournalTestResultGen.P_ACTIVITY, this);
            return _activity;
        }

    /**
     * @return код деятельности.
     * @see ru.tandemservice.brsrmc.entity.JournalTestResult#getActivityCode()
     */
        public PropertyPath<String> activityCode()
        {
            if(_activityCode == null )
                _activityCode = new PropertyPath<String>(JournalTestResultGen.P_ACTIVITY_CODE, this);
            return _activityCode;
        }

    /**
     * @return id документа.
     * @see ru.tandemservice.brsrmc.entity.JournalTestResult#getDocumentId()
     */
        public PropertyPath<Long> documentId()
        {
            if(_documentId == null )
                _documentId = new PropertyPath<Long>(JournalTestResultGen.P_DOCUMENT_ID, this);
            return _documentId;
        }

    /**
     * @return дата выполнения действия.
     * @see ru.tandemservice.brsrmc.entity.JournalTestResult#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(JournalTestResultGen.P_DATE, this);
            return _date;
        }

        public Class getEntityClass()
        {
            return JournalTestResult.class;
        }

        public String getEntityName()
        {
            return "journalTestResult";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
