package ru.tandemservice.brsrmc.ws.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ActivityColumns")
public class ActivityColumns {
    @XmlElement(name = "activity")
    private List<ActivityWs> activities;

    public ActivityColumns() {
        activities = new ArrayList<>();
    }

    public List<ActivityWs> getActivities() {
        return activities;
    }

    public void setActivities(List<ActivityWs> activities) {
        this.activities = activities;
    }


}
