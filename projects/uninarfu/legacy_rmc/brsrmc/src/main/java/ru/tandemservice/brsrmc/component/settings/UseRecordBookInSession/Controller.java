package ru.tandemservice.brsrmc.component.settings.UseRecordBookInSession;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = component.getModel();
        ((IDAO) getDao()).prepare(model);
    }

    public void onClickChange(IBusinessComponent component) {
        Model model = component.getModel();
        ((IDAO) getDao()).saveSettings(model);
    }
}
