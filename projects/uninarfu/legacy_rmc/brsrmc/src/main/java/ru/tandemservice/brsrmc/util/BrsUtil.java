package ru.tandemservice.brsrmc.util;

import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import ru.tandemservice.brsrmc.component.settings.UseRecordBookInSession.IUseRecordBookInSessionKey;

import java.util.*;

public class BrsUtil {

    /**
     * Состояние значения настройки "Использование журналов преподавателя в сессии"
     *
     * @return
     */
    public static boolean getUseRecordBookInSessionMode() {
        IDataSettings settings = DataSettingsFacade.getSettings(IUseRecordBookInSessionKey.OWNER_KEY);
        Object useValue = settings.get(IUseRecordBookInSessionKey.SETTING_KEY);
        if (useValue == null) {
            return false;
        }

        return (Boolean) useValue;
    }

    /**
     * Вычисляем позиции студента в своей группе по рейтингу в семестр и итоговому рейтингу
     *
     * @param studentIds
     * @param journalMarkMap
     *
     * @return
     */
    public static Map<Long, JournalMarkOXWrapper> fillStudentRaiting(List<Long> studentIds, Map<Long, JournalMarkOXWrapper> journalMarkMap) {

        // Берем проекцию данных журналов на группу
        List<JournalMarkOXWrapper> journalMarkProjection = new ArrayList<>();
        for (Long studentId : studentIds) {
            JournalMarkOXWrapper journalMarkOXWrapper = journalMarkMap.get(studentId);
            if (journalMarkOXWrapper != null) {
                journalMarkProjection.add(journalMarkOXWrapper);
            }
        }

        // По рейтингу в семестр
        Collections.sort(journalMarkProjection,
                         new Comparator<JournalMarkOXWrapper>() {
                             @Override
                             public int compare(JournalMarkOXWrapper w1, JournalMarkOXWrapper w2) {
                                 if (w1.getSessionRaiting() != null && w2.getSessionRaiting() != null) {
                                     return w2.getSessionRaiting().compareTo(w1.getSessionRaiting());
                                 }
                                 else if (w1.getSessionRaiting() != null) {
                                     return w1.getSessionRaiting();
                                 }
                                 else {
                                     return w2.getSessionRaiting();
                                 }
                             }
                         });

        journalMarkProjection = fillRaitingPlace(journalMarkProjection, Boolean.FALSE);

        // По итоговому рейтингу
        Collections.sort(journalMarkProjection,
                         new Comparator<JournalMarkOXWrapper>() {
                             @Override
                             public int compare(JournalMarkOXWrapper w1, JournalMarkOXWrapper w2) {
                                 if (w1.getTotalRaiting() != null && w2.getTotalRaiting() != null) {
                                     return w2.getTotalRaiting().compareTo(w1.getTotalRaiting());
                                 }
                                 else if (w1.getTotalRaiting() != null) {
                                     return w1.getTotalRaiting();
                                 }
                                 else {
                                     return w2.getTotalRaiting();
                                 }
                             }
                         });

        journalMarkProjection = fillRaitingPlace(journalMarkProjection, Boolean.TRUE);

        // переинициализуем уже с заполнеными позициями
        for (JournalMarkOXWrapper projectionItem : journalMarkProjection) {
            journalMarkMap.put(projectionItem.getStudentId(), projectionItem);
        }

        return journalMarkMap;
    }

    public static List<JournalMarkOXWrapper> fillRaitingPlace(List<JournalMarkOXWrapper> journalMarkList, boolean byTotalRaiting) {
        int position = 1;
        int criteria = -1;
        for (JournalMarkOXWrapper item : journalMarkList) {
            // по итоговому рейтингу
            if (byTotalRaiting) {
                if (item.getTotalRaiting() == null) {
                    item.setTotalRaitingPlace(null);
                    continue;
                }
                if (criteria != -1) {
                    if (item.getTotalRaiting() == criteria) {
                        item.setTotalRaitingPlace(position);
                    }
                    else {
                        item.setTotalRaitingPlace(++position);
                        criteria = item.getTotalRaiting();
                    }
                }
                else {
                    criteria = item.getTotalRaiting();
                    item.setTotalRaitingPlace(position);
                }
            }
            // по рейтингу в семестр
            else {
                if (item.getSessionRaiting() == null) {
                    item.setSessionRaitingPlace(null);
                    continue;
                }
                if (criteria != -1) {
                    if (item.getSessionRaiting() == criteria) {
                        item.setSessionRaitingPlace(position);
                    }
                    else {
                        item.setSessionRaitingPlace(++position);
                        criteria = item.getSessionRaiting();
                    }
                }
                else {
                    criteria = item.getSessionRaiting();
                    item.setSessionRaitingPlace(position);
                }
            }
        }

        return journalMarkList;
    }
}
