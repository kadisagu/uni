package ru.tandemservice.brsrmc.ws.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "JournalTestResultWs")
public class JournalTestResultWs {
    public enum ErrCode {
        DISABLED_EDIT,
        NOT_FOUND
    }

    ;
    public static final String ERR_MSG_DISABLED_EDIT = "данную оценку нельзя редактировать";
    public static final String ERR_MSG_NOT_FOUND = "не найдено оценки для данного студента и данной деятельности";
    /**
     * id оценки
     */
    private long sourceMarkId;
    /**
     * id студента
     */
    private Long studentId;
    /**
     * id колонки деятельности (RBActivityColumn)
     */
    private Long activityId;
    /**
     * оценка, которая будет в журнале в итоге(после просчитывания пропорций)
     */
    private Integer resultMark;

    /**
     * активити *
     */
    private String activity;
    /**
     * код активити *
     */
    private String activityCode;
    /**
     * id документа сессии *
     */
    private Long documentId;
    /**
     * дата выполнения действия*
     */
    private Date date;

    /**
     * код ошибки
     */
    private ErrCode errCode;
    private String errMessage;


    public JournalTestResultWs() {
    }

    public JournalTestResultWs(Long sourceMarkId, Long studentId, Long activityId, Integer resultMark, String activity, String activityCode, Long documentId, Date date) {
        super();
        this.sourceMarkId = sourceMarkId;
        this.studentId = studentId;
        this.activityId = activityId;
        this.resultMark = resultMark;
        this.activity = activity;
        this.activityCode = activityCode;
        this.documentId = documentId;
        this.date = date;
    }

    public JournalTestResultWs(Long sourceMarkId, Long studentId, Long activityId, ErrCode errCode, String errMessage) {
        super();
        this.sourceMarkId = sourceMarkId;
        this.studentId = studentId;
        this.activityId = activityId;
        this.errCode = errCode;
        this.errMessage = errMessage;
    }

    public long getSourceMarkId() {
        return sourceMarkId;
    }

    public void setSourceMarkId(long sourceMarkId) {
        this.sourceMarkId = sourceMarkId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Integer getResultMark() {
        return resultMark;
    }

    public void setResultMark(Integer resultMark) {
        this.resultMark = resultMark;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getActivityCode() {
        return activityCode;
    }

    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }

    public Long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ErrCode getErrCode() {
        return errCode;
    }

    public void setErrCode(ErrCode errCode) {
        this.errCode = errCode;
    }

    public String getErrMessage() {
        return errMessage;
    }

    public void setErrMessage(String errMessage) {
        this.errMessage = errMessage;
    }


}
