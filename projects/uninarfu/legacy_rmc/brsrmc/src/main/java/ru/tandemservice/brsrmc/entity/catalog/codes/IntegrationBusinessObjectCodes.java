package ru.tandemservice.brsrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Бизнес объект в интеграции"
 * Имя сущности : integrationBusinessObject
 * Файл data.xml : eventsrmc.data.xml
 */
public interface IntegrationBusinessObjectCodes
{
    /** Константа кода (code) элемента : Тестовый бизнес объект (title) */
    String TEST_OBJECT = "1";
    /** Константа кода (code) элемента : Ведомость (БРС) (title) */
    String SESSION_BULLETIN_B_O = "brsrmc.sb.1";
    /** Константа кода (code) элемента : Экзаменационный лист (БРС) (title) */
    String SESSION_SHEET_B_O = "brsrmc.sb.2";

    Set<String> CODES = ImmutableSet.of(TEST_OBJECT, SESSION_BULLETIN_B_O, SESSION_SHEET_B_O);
}
