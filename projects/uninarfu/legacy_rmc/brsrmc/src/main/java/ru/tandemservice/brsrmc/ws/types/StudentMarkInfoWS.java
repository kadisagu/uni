package ru.tandemservice.brsrmc.ws.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StudentMarkInfoWS")
public class StudentMarkInfoWS {

    private Long studentId;
    private Integer sessionRaiting;
    private Integer totalRaiting;
    private List<MarkInfoWrapperWS> studentMarkInfo;

    public StudentMarkInfoWS() {
    }

    /**
     * @return Студент
     */
    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    /**
     * @return Средний семестровый рейтинг (Сумма баллов по дисциплинам в
     * документах сессии за семестр)
     */
    public Integer getSessionRaiting() {
        return sessionRaiting;
    }

    public void setSessionRaiting(Integer sessionRaiting) {
        this.sessionRaiting = sessionRaiting;
    }

    /**
     * @return Средний итоговый рейтинг (Сумма баллов по дисциплинам в
     * документах сессии за все семестры)
     */
    public Integer getTotalRaiting() {
        return totalRaiting;
    }

    public void setTotalRaiting(Integer totalRaiting) {
        this.totalRaiting = totalRaiting;
    }

    public List<MarkInfoWrapperWS> getStudentMarkInfo() {
        return studentMarkInfo;
    }

    public void setStudentMarkInfo(
            List<MarkInfoWrapperWS> studentMarkInfo)
    {
        this.studentMarkInfo = studentMarkInfo;
    }
}
