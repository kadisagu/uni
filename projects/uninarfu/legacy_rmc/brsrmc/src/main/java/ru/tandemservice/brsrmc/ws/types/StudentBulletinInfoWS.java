package ru.tandemservice.brsrmc.ws.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StudentBulletinInfoWS")
public class StudentBulletinInfoWS {

    private Long studentId;
    private Long bulletinId;
    private Integer attestation1Sum;
    private Integer attestation2Sum;
    private Integer attestation3Sum;
    private Integer promotionSum;
    private Integer finalTestSum;
    private Integer totalSum;

    public StudentBulletinInfoWS() {
    }

    /**
     * @return студент
     */
    public Long getStudentId() {
        return studentId;
    }

    /**
     * @return Ведомость
     */
    public Long getBulletinId() {
        return bulletinId;
    }

    /**
     * @return сумма баллов по всем источникам деятельности Аттестация 1
     */
    public Integer getAttestation1Sum() {
        return attestation1Sum;
    }

    /**
     * @return сумма баллов по всем источникам деятельности Аттестация 2
     */
    public Integer getAttestation2Sum() {
        return attestation2Sum;
    }

    /**
     * @return сумма баллов по всем источникам деятельности Аттестация 3
     */
    public Integer getAttestation3Sum() {
        return attestation3Sum;
    }

    /**
     * @return сумма баллов по всем источникам деятельности Поощрения
     */
    public Integer getPromotionSum() {
        return promotionSum;
    }

    /**
     * @return сумма баллов по всем источникам деятельности Итоговый тест
     */
    public Integer getFinalTestSum() {
        return finalTestSum;
    }

    /**
     * @return Итоговая сумма для текущего и итогового контроля (для всех
     * присутствующих здесь деятельностей)
     */
    public Integer getTotalSum() {
        return totalSum;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public void setBulletinId(Long bulletinId) {
        this.bulletinId = bulletinId;
    }

    public void setAttestation1Sum(Integer attestation1Sum) {
        this.attestation1Sum = attestation1Sum;
    }

    public void setAttestation2Sum(Integer attestation2Sum) {
        this.attestation2Sum = attestation2Sum;
    }

    public void setPromotionSum(Integer promotionSum) {
        this.promotionSum = promotionSum;
    }

    public void setFinalTestSum(Integer finalTestSum) {
        this.finalTestSum = finalTestSum;
    }

    public void setAttestation3Sum(Integer attestation3Sum) {
        this.attestation3Sum = attestation3Sum;
    }

    public void setTotalSum(Integer totalSum) {
        this.totalSum = totalSum;
    }
}

