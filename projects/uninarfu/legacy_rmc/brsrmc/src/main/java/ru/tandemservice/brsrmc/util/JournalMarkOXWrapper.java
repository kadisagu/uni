package ru.tandemservice.brsrmc.util;

import org.apache.commons.collections.keyvalue.MultiKey;

import java.util.HashMap;
import java.util.Map;

public class JournalMarkOXWrapper {

    private Long studentId;
    private Integer sessionRaiting;
    private Integer totalRaiting;
    private Integer sessionRaitingPlace;
    private Integer totalRaitingPlace;

    private Map<MultiKey, Integer> journalMarkMap = new HashMap<>();

    public JournalMarkOXWrapper(Long studentId, Integer sessionRaiting, Integer totalRaiting) {
        this.studentId = studentId;
        this.sessionRaiting = sessionRaiting;
        this.totalRaiting = totalRaiting;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Integer getSessionRaiting() {
        return sessionRaiting;
    }

    public void setSessionRaiting(Integer sessionRaiting) {
        this.sessionRaiting = sessionRaiting;
    }

    public Integer getTotalRaiting() {
        return totalRaiting;
    }

    public void setTotalRaiting(Integer totalRaiting) {
        this.totalRaiting = totalRaiting;
    }

    public Map<MultiKey, Integer> getJournalMarkMap() {
        return journalMarkMap;
    }

    public void setJournalMarkMap(Map<MultiKey, Integer> journalMarkMap) {
        this.journalMarkMap = journalMarkMap;
    }

//DEV-6870 do not used, I hope.
//	public Integer getJournalMark(ISessionSummaryBulletinStudent student, ISessionSummaryBulletinAction action){
//		return this.journalMarkMap.get(key(student.getStudent().getId(), action.getControlActionType().getId(), action.getDiscipline().getId()));
//	}

    public Integer getJournalMark(Long studentId, Long caTypeId, Long regElementPartId) {
        return this.journalMarkMap.get(key(studentId, caTypeId, regElementPartId));
    }

    public MultiKey key(Long studentId, Long caTypeId, Long regElementPartId) {
        return new MultiKey(studentId, caTypeId, regElementPartId);
    }

    public Integer getSessionRaitingPlace() {
        return sessionRaitingPlace;
    }

    public void setSessionRaitingPlace(Integer sessionRaitingPlace) {
        this.sessionRaitingPlace = sessionRaitingPlace;
    }

    public Integer getTotalRaitingPlace() {
        return totalRaitingPlace;
    }

    public void setTotalRaitingPlace(Integer totalRaitingPlace) {
        this.totalRaitingPlace = totalRaitingPlace;
    }
}
