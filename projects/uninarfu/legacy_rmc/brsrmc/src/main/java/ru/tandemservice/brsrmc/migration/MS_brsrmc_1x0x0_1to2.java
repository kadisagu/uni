package ru.tandemservice.brsrmc.migration;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_brsrmc_1x0x0_1to2 extends IndependentMigrationScript
{
    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // модуль eventsrmc отключен - удаляем все его сущности

        // убедиться, что модуль и в самом деле удален
        {
            if (ApplicationRuntime.hasModule("eventsrmc"))
                throw new RuntimeException("Module 'eventsrmc' is not deleted");
        }

        // удалить сущность wsType
        {
            // удалить таблицу
            tool.dropTable("wstype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("wsType");

        }

        // удалить сущность wsProxyType
        {
            // удалить таблицу
            tool.dropTable("wsproxytype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("wsProxyType");

        }

        // удалить сущность statusDaemonProcessingWs
        {
            // удалить таблицу
            tool.dropTable("statusdaemonprocessingws_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("statusDaemonProcessingWs");

        }

        // удалить сущность requestInfo
        {
            // удалить таблицу
            tool.dropTable("requestinfo_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("requestInfo");

        }

        // удалить сущность internalNotification
        {
            // удалить таблицу
            tool.dropTable("internalnotification_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("internalNotification");

        }

        // удалить сущность internalEntityType
        {
            // удалить таблицу
            tool.dropTable("internalentitytype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("internalEntityType");

        }

        // удалить сущность integrationType
        {
            // удалить таблицу
            tool.dropTable("integrationtype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("integrationType");

        }

        // удалить сущность integrationEventType
        {
            // удалить таблицу
            tool.dropTable("integrationeventtype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("integrationEventType");

        }

        // удалить сущность integrationBusinessObject
        {
            // удалить таблицу
            tool.dropTable("integrationbusinessobject_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("integrationBusinessObject");

        }

        // удалить сущность externalSystemWS
        {
            // удалить таблицу
            tool.dropTable("externalsystemws_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("externalSystemWS");

        }

        // удалить сущность externalSystemNeedEntityType
        {
            // удалить таблицу
            tool.dropTable("externalsystemneedentitytype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("externalSystemNeedEntityType");

        }

        // удалить сущность externalSystem
        {
            // удалить таблицу
            tool.dropTable("externalsystem_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("externalSystem");

        }

        // удалить сущность externalNotificationEventType
        {
            // удалить таблицу
            tool.dropTable("xtrnlntfctnevnttyp_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("externalNotificationEventType");

        }

        // удалить сущность externalNotification
        {
            // удалить таблицу
            tool.dropTable("externalnotification_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("externalNotification");

        }

        // удалить сущность externalEntityType
        {
            // удалить таблицу
            tool.dropTable("externalentitytype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("externalEntityType");

        }

        // удалить сущность externalEntitiesMap
        {
            // удалить таблицу
            tool.dropTable("externalentitiesmap_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("externalEntitiesMap");

        }

        // удалить сущность eventsErrorCodes
        {
            // удалить таблицу
            tool.dropTable("eventserrorcodes_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("eventsErrorCodes");

        }

        // удалить сущность entityManagerTaskParam
        {
            // удалить таблицу
            tool.dropTable("entitymanagertaskparam_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("entityManagerTaskParam");

        }

        // удалить сущность entityManagerTask
        {
            // удалить таблицу
            tool.dropTable("entitymanagertask_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("entityManagerTask");

        }

        // удалить сущность entityManagerLog
        {
            // удалить таблицу
            tool.dropTable("entitymanagerlog_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("entityManagerLog");

        }

        // удалить сущность entityForDaemonWS
        {
            // удалить таблицу
            tool.dropTable("entityfordaemonws_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("entityForDaemonWS");

        }

        // удалить сущность entityForDaemonDirection
        {
            // удалить таблицу
            tool.dropTable("entityfordaemondirection_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("entityForDaemonDirection");

        }

        // удалить сущность daemonWsLog
        {
            // удалить таблицу
            tool.dropTable("daemonwslog_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("daemonWsLog");

        }

        MigrationUtils.removeModuleFromVersion_s(tool, "eventsrmc");
    }
}