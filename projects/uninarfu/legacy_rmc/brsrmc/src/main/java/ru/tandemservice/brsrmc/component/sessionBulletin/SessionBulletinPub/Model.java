package ru.tandemservice.brsrmc.component.sessionBulletin.SessionBulletinPub;

public class Model extends ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPub.Model {

    private boolean alternative;

    public boolean isAlternative() {
        return alternative;
    }

    public void setAlternative(boolean alternative) {
        this.alternative = alternative;
    }
}
