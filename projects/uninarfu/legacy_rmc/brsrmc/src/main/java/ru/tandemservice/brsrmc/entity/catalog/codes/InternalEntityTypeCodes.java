package ru.tandemservice.brsrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип сущности (внутр. уведомления)"
 * Имя сущности : internalEntityType
 * Файл data.xml : eventsrmc.data.xml
 */
public interface InternalEntityTypeCodes
{
    /** Константа кода (code) элемента : Тест (событие) журнала преподавателя (title) */
    String BRS_JURNAL_TEST = "brsrmc.jurnal.test";

    Set<String> CODES = ImmutableSet.of(BRS_JURNAL_TEST);
}
