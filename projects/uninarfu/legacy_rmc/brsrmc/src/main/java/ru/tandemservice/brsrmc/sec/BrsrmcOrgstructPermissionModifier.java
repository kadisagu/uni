package ru.tandemservice.brsrmc.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.PermissionGroupMeta;
import org.tandemframework.sec.meta.SecurityConfigMeta;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;

import java.util.Map;

public class BrsrmcOrgstructPermissionModifier implements ISecurityConfigMetaMapModifier {
    @Override
    public void modify(final Map<String, SecurityConfigMeta> securityConfigMetaMap)
    {
        final SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("uni");
        config.setName("brsrmc-orgstruct-sec-config");
        config.setTitle("");

        for (final OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class, OrgUnitType.P_TITLE)) {
            final String code = description.getCode();

            final PermissionGroupMeta pgSessionTab = PermissionMetaUtil.createPermissionGroup(config, code + "SessionTabPermissionGroup", "Вкладка «Сессия»");
            final PermissionGroupMeta pgJournalRestrictTab = PermissionMetaUtil.createPermissionGroup(pgSessionTab, code + "JournalRestrictTabPG", "Вкладка «Ограничения в журналах БРС»");
            PermissionMetaUtil.createPermission(pgJournalRestrictTab, "journalRestricTabView_" + code, "Просмотр");
        }

        securityConfigMetaMap.put(config.getName(), config);
    }

}
