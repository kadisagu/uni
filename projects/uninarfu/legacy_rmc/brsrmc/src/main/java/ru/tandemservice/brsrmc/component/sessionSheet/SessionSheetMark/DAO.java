package ru.tandemservice.brsrmc.component.sessionSheet.SessionSheetMark;

import ru.tandemservice.brsrmc.dao.BrsrmcDaoFacade;
import ru.tandemservice.brsrmc.dao.IBrsDao;
import ru.tandemservice.brsrmc.util.BrsUtil;
import ru.tandemservice.brsrmc.ws.types.StudentMarkWs;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.ISessionMarkDAO;

import java.util.Map;

public class DAO extends ru.tandemservice.unisession.component.sessionSheet.SessionSheetMark.DAO implements IDAO {
    @Override
    public void prepare(ru.tandemservice.unisession.component.sessionSheet.SessionSheetMark.Model model) {
        super.prepare(model);
        Model myModel = (Model) model;
        myModel.setAlternative(BrsUtil.getUseRecordBookInSessionMode());
    }

    @Override
    protected ISessionMarkDAO.MarkData prepareMarkDataForUpdate(ru.tandemservice.unisession.component.sessionSheet.SessionSheetMark.Model model) {
        Model myModel = (Model) model;
        if (myModel.isAlternative()) {
            IBrsDao brsDao = BrsrmcDaoFacade.getBrsDao();
            Map<Long, StudentMarkWs> student2Mark = brsDao.getJournalMarkData(model.getSheet());

            ISessionMarkDAO.MarkData markData = brsDao.prepareMarkDataForUpdate(
                    model.getSlot(),
                    student2Mark.get(model.getSlot().getStudentWpeCAction().getStudentWpe().getStudent().getId()));
            ISessionMarkDAO.MarkData prevMarkData = super.prepareMarkDataForUpdate(model);
            if (markData != null) {
                return markData;
            }
            else if (prevMarkData != null) {
                return brsDao.getVoidMarkData();
            }
            //оценку не обновляем
            return prevMarkData;
        }
        else {
            return super.prepareMarkDataForUpdate(model);
        }
    }
}
