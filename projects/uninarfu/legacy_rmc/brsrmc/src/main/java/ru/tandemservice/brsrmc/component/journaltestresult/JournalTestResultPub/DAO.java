package ru.tandemservice.brsrmc.component.journaltestresult.JournalTestResultPub;

import ru.tandemservice.brsrmc.entity.JournalTestResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        JournalTestResult journalTestResult = model.getJournal();

        Student student = get(journalTestResult.getStudentId());
        SessionBulletinDocument document = get(journalTestResult.getDocumentId());

        model.setStudent(student);
        model.setDocument(document);
    }
}
