package ru.tandemservice.brsrmc.component.sessionBulletin.SessionBulletinPub;

import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.brsrmc.util.BrsUtil;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;


public class DAO extends ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPub.DAO implements IDAO {

    public boolean hasTutors(Model model) {
        SessionComission comission = model.getBulletin().getCommission();

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(SessionComissionPps.class, "e")
                .where(DQLExpressions.eq(DQLExpressions.property(SessionComissionPps.commission().fromAlias("e").s()), DQLExpressions.value(comission)));
        Number countTutors = dql.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();
        return countTutors.intValue() > 0;
    }

    @Override
    public void prepare(ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPub.Model model) {
        super.prepare(model);
        Model myModel = (Model) model;
        myModel.setAlternative(BrsUtil.getUseRecordBookInSessionMode());
    }

}
