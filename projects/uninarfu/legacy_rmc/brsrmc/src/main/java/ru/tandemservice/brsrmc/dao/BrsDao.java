package ru.tandemservice.brsrmc.dao;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import ru.tandemservice.brsrmc.util.OXMarkData;
import ru.tandemservice.brsrmc.ws.ISessionJournalWS;
import ru.tandemservice.brsrmc.ws.types.StudentMarkWs;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.ISessionMarkDAO;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BrsDao extends UniBaseDao implements IBrsDao {
    @Override
    public ISessionMarkDAO.MarkData getVoidMarkData() {
        return new ISessionMarkDAO.MarkData() {
            @Override
            public Date getPerformDate() {
                return null;
            }

            @Override
            public Double getPoints() {
                return null;
            }

            @Override
            public SessionMarkCatalogItem getMarkValue() {
                return null;
            }

            @Override
            public String getComment() {
                return null;
            }
        };
    }

    @Override
    public Map<Long, StudentMarkWs> getJournalMarkData(SessionDocument doc) {
        List<StudentMarkWs> studentMarks;
        ISessionJournalWS journalWs = (ISessionJournalWS) ApplicationRuntime.getBean("journalClient");
        Map<Long, StudentMarkWs> student2Mark = new HashMap<Long, StudentMarkWs>();
        try {
            studentMarks = journalWs.getMarksFromJournal(doc.getId());
        }
        catch (Exception e) {
            throw new ApplicationException(e.getMessage());
        }
        if (studentMarks != null) {
            for (StudentMarkWs sm : studentMarks) {
                student2Mark.put(sm.getStudentId(), sm);
            }
        }
        return student2Mark;
    }

    @Override
    public ISessionMarkDAO.MarkData prepareMarkDataForUpdate(SessionDocumentSlot slot, StudentMarkWs markWs) {
        if (markWs != null) {
            OXMarkData res = new OXMarkData(markWs, new Date(), slot);
            return res;
        }
        return null;
    }
}
