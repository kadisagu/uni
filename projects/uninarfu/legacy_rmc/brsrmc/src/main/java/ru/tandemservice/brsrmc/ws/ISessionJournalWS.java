package ru.tandemservice.brsrmc.ws;

import ru.tandemservice.brsrmc.ws.types.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.List;

@WebService(targetNamespace = "http://brs.vuz.ramec.ru/", name = "sessionjournalws")
public interface ISessionJournalWS {
    public final static String WS_SESSION_JOURNAL_BEAN_NAME = "journalClient";

    /**
     * Строит журнал преподавателя по данным ведомости. Все идентификаторы из TU
     */
    @WebMethod(operationName = "buildSessionJournal")
    @WebResult(name = "journalId")
    public Long buildSessionJournal(
            @WebParam(name = "sessionDoc") Long sessionDocId,
            @WebParam(name = "docType") SessionDocType docType,
            @WebParam(name = "studIds") List<Long> studIds,
            @WebParam(name = "eduYearId") Long eduYearId,
            @WebParam(name = "yearDistrPartId") Long yearDistrPartId,
            @WebParam(name = "eduLevelId") Long eduLevelId,
            @WebParam(name = "developFormId") Long developFormId,
            @WebParam(name = "controlActionTypeId") Long controlActionTypeId,
            @WebParam(name = "regStructureId") Long regStructureId) throws Exception;

    @WebMethod(operationName = "deleteSessionJournal")
    public void deleteSessionJournal(@WebParam(name = "sessionDoc") Long sessionDocId) throws Exception;

    @WebMethod(operationName = "getMarksFromJournal")
    @WebResult(name = "marksList")
    public List<StudentMarkWs> getMarksFromJournal(@WebParam(name = "sessionDoc") Long sessionDocId) throws Exception;

    /**
     * Данные о баллах по журналам OX для документов сессии для студентов в
     * рамках указанных параметров сессии(год и часть года)
     *
     * @param studentIds      cписок id студентов
     * @param eduYearId       id учебного года
     * @param yearDistrPartId id части учебного года
     *
     * @return
     *
     * @throws Exception
     */
    @WebMethod(operationName = "getStudentMarkInfoFromJournal")
    @WebResult(name = "studentMarkInfo")
    public List<StudentMarkInfoWS> getStudentMarkInfoFromJournal(
            @WebParam(name = "studentIds") List<Long> studentIds,
            @WebParam(name = "eduYearId") Long eduYearId,
            @WebParam(name = "yearDistrPartId") Long yearDistrPartId)
            throws Exception;

    /**
     * Данные для ведомости из соответсвующего журнала OX
     *
     * @param sessionDocIds
     *
     * @return
     *
     * @throws Exception
     */
    @WebMethod(operationName = "getBulletinInfoFromJournal")
    @WebResult(name = "bulletinInfoList")
    public List<StudentBulletinInfoWS> getBulletinInfoFromJournal(@WebParam(name = "sessionDocIds") List<Long> sessionDocIds) throws Exception;


    /**
     * Список документов сесии со вложенным списком ид деятельностей, где есть колонка "Тестирование"
     *
     * @param sessionDocIds
     *
     * @return
     *
     * @throws Exception
     */
    @WebMethod(operationName = "getTestingActivities")
    @WebResult(name = "sessionJournal")
    public List<JournalActivityColumnsWS> getTestingActivities(@WebParam(name = "sessionDocIds") List<Long> sessionDocIds) throws Exception;

    /**
     * Обновляет оценки за Тестирование
     *
     * @param testingMarks
     *
     * @return
     */
    @WebMethod(operationName = "updateTesingMarks")
    @WebResult(name = "updateTesingMarksResult")
    public List<JournalTestResultWs> updateTesingMarks(@WebParam(name = "testingMarks") List<JournalTestWs> testingMarks) throws Exception;

}
