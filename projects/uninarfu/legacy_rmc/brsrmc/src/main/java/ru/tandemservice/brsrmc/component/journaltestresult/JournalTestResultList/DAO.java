package ru.tandemservice.brsrmc.component.journaltestresult.JournalTestResultList;

import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.brsrmc.entity.JournalTestResult;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {
    @Override
    public void prepare(final Model model) {
        model.setFormativeOrgUnitModel(new BaseSingleSelectModel() {
            @Override
            public ListResult findValues(String filter) {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(OrgUnit.class, "ou");

                builder.order(DQLExpressions.property(OrgUnit.fullTitle().fromAlias("ou")));

                if (filter != null) {
                    builder.where(
                            DQLExpressions.like(DQLExpressions.property(OrgUnit.fullTitle().fromAlias("ou")), DQLExpressions.value("%" + filter + "%"))
                    );
                }

                DQLSelectBuilder inclBuilder = new DQLSelectBuilder()
                        .fromEntity(EducationOrgUnit.class, "b")
                        .column(DQLExpressions.property(EducationOrgUnit.formativeOrgUnit().id().fromAlias("b")));
                builder.where(DQLExpressions.in(
                        DQLExpressions.property(OrgUnit.id().fromAlias("ou")),
                        inclBuilder.buildQuery()));

                List<OrgUnit> list = builder.createStatement(getSession()).list();


                return new ListResult<>(list);
            }


            @Override
            public Object getValue(Object primaryKey) {
                if (primaryKey == null) {
                    return null;
                }

                return get(OrgUnit.class, (Long) primaryKey);
            }
        });

        model.setGroupModel(new BaseSingleSelectModel() {
            @Override
            public Object getValue(Object primaryKey) {
                if (primaryKey == null) {
                    return null;
                }

                return get(Group.class, (Long) primaryKey);
            }

            @Override
            public ListResult findValues(String filter) {
                OrgUnit formativeOrgUnit = model.getFormativeOrgUnit();
                if (formativeOrgUnit == null) {
                    return new ListResult<>(new ArrayList<>());
                }

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Group.class, "g");


                if (filter != null) {
                    builder.where(
                            DQLExpressions.like(DQLExpressions.property(Group.title().fromAlias("g")), DQLExpressions.value("%" + filter + "%"))
                    );
                }

                builder.where(
                        DQLExpressions.or(
                                DQLExpressions.eq(DQLExpressions.property(Group.educationOrgUnit().formativeOrgUnit().fromAlias("g")), DQLExpressions.value(formativeOrgUnit)),
                                DQLExpressions.eq(DQLExpressions.property(Group.educationOrgUnit().territorialOrgUnit().fromAlias("g")), DQLExpressions.value(formativeOrgUnit)
                                ))
                );

                List<Group> list = builder.createStatement(getSession()).list();

                return new ListResult<>(list);
            }
        });

        model.setStudentModel(new BaseSingleSelectModel() {
            @Override
            public Object getValue(Object primaryKey) {
                if (primaryKey == null) {
                    return null;
                }

                return get(Student.class, (Long) primaryKey);
            }

            @Override
            public ListResult findValues(String filter) {
                Group group = model.getGroup();
                if (group == null) {
                    return new ListResult<>(new ArrayList<>());
                }

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student.class, "s");

                if (filter != null) {
                    builder.where(
                            DQLExpressions.like(
                                    DQLExpressions.property(Student.person().identityCard().fullFio().fromAlias("s")), DQLExpressions.value("%" + filter + "%"))
                    );
                }

                builder.where(
                        DQLExpressions.eq(
                                DQLExpressions.property(Student.group().fromAlias("s")),
                                DQLExpressions.value(group)
                        )
                );

                List<Student> list = builder.createStatement(getSession()).list();
                return new ListResult<>(list);
            }

        });

        model.setActivityModel(new FullCheckSelectModel() {
            @Override
            public ListResult findValues(String filter) {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(JournalTestResult.class, "j")
                        .column(DQLExpressions.property(JournalTestResult.activity().fromAlias("j")))
                        .distinct();

                if (filter != null) {
                    FilterUtils.applySimpleLikeFilter(builder, "j", JournalTestResult.activity().s(), filter);
                }

                List<String> list = builder.createStatement(getSession()).list();

                List<IdentifiableWrapper> iwlist = new ArrayList<>();
                int i = 0;
                for (String s : list) {
                    iwlist.add(new IdentifiableWrapper((long) i, s));
                    i++;
                }

                return new ListResult<>(iwlist);
            }
        });

        model.setYearModel(new BaseSingleSelectModel() {
            @Override
            public Object getValue(Object primaryKey) {
                if (primaryKey != null) {
                    return get(EducationYear.class, (Long) primaryKey);
                }

                return null;
            }

            @Override
            public ListResult findValues(String filter) {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(EducationYear.class, "ey");

                FilterUtils.applySimpleLikeFilter(builder, "ey", EducationYear.title(), filter);

                List<EducationYear> list = getList(builder);
                return new ListResult<>(list);
            }
        });

        model.setYearPartModel(new BaseSingleSelectModel() {
            @Override
            public Object getValue(Object primaryKey) {
                return get(YearDistributionPart.class, (Long) primaryKey);
            }

            @Override
            public ListResult findValues(String filter) {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(YearDistributionPart.class, "ep");

                FilterUtils.applySimpleLikeFilter(builder, "ep", YearDistributionPart.title(), filter);

                return new ListResult<>(builder.createStatement(getSession()).<YearDistributionPart>list());
            }
        });
    }


    @Override
    public void prepareDataSource(Model model) {

    }

    @Override
    public void prepareListDataSource(Model model) {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(JournalTestResult.class, "j")
                .column("j")
                .joinEntity("j", DQLJoinType.left, Student.class, "student",
                            DQLExpressions.eq(
                                    DQLExpressions.property(Student.id().fromAlias("student")),
                                    DQLExpressions.property(JournalTestResult.studentId().fromAlias("j"))
                            )
                )
                .joinEntity("student", DQLJoinType.left, Person.class, "person",
                            DQLExpressions.eq(
                                    DQLExpressions.property(Student.person().id().fromAlias("student")),
                                    DQLExpressions.property(Person.id().fromAlias("person"))
                            )
                )
                .joinEntity("person", DQLJoinType.left, IdentityCard.class, "ic",
                            DQLExpressions.eq(
                                    DQLExpressions.property(IdentityCard.id().fromAlias("ic")),
                                    DQLExpressions.property(Person.identityCard().id().fromAlias("person"))
                            )
                )
                .joinEntity("j", DQLJoinType.left, SessionBulletinDocument.class, "sdocument",
                            DQLExpressions.eq(
                                    DQLExpressions.property(SessionBulletinDocument.id().fromAlias("sdocument")),
                                    DQLExpressions.property(JournalTestResult.documentId().fromAlias("j"))
                            )
                );


        IDataSettings settings = model.getSettings();

        Date fromDate = settings.get("fromDate");
        Date toDate = settings.get("toDate");
        IdentifiableWrapper iwrapper = settings.get("activity");
        String acitivity = iwrapper != null ? iwrapper.getTitle() : "";
        OrgUnit orgUnit = model.getFormativeOrgUnit();
        Group group = model.getGroup();
        Student student = model.getStudent();
        EducationYear educationYear = model.getYear();
        YearDistributionPart yearDistributionPart = model.getYearPart();

        IDQLExpression property = DQLExpressions.property(JournalTestResult.date().fromAlias("j"));

        if (fromDate != null && toDate != null) {
            builder.where(DQLExpressions.between(
                    property,
                    DQLExpressions.valueDate(fromDate),
                    DQLExpressions.valueDate(toDate)
            ));
        }
        else if (fromDate != null) {
            builder.where(DQLExpressions.ge(property, DQLExpressions.valueDate(fromDate)));
        }
        else if (toDate != null) {
            builder.where(DQLExpressions.le(property, DQLExpressions.valueDate(toDate)));
        }

        EntityOrder entityOrder = model.getDataSource().getEntityOrder();
        if (entityOrder != null) {
            String columnName = entityOrder.getColumnName();
            if (columnName.equals("student.fullFio")) {
                builder.order(Student.person().identityCard().lastName().s(), entityOrder.getDirection());
                builder.order(Student.person().identityCard().firstName().s(), entityOrder.getDirection());
                builder.order(Student.person().identityCard().middleName().s(), entityOrder.getDirection());
            }
            else {
                builder.order("j." + columnName, entityOrder.getDirection());
            }
        }

        if (orgUnit != null) {
            builder.where(
                    DQLExpressions.eq(DQLExpressions.property(Student.educationOrgUnit().formativeOrgUnit().fromAlias("student")), DQLExpressions.value(orgUnit))
            );
        }

        if (group != null) {
            builder.where(
                    DQLExpressions.eq(
                            DQLExpressions.property(Student.group().fromAlias("student")),
                            DQLExpressions.value(group)
                    )
            );
        }

        if (student != null) {
            builder.where(
                    DQLExpressions.eq(
                            DQLExpressions.property(JournalTestResult.studentId().fromAlias("j")),
                            DQLExpressions.value(student.getId())
                    )
            );
        }

        if (educationYear != null) {
            builder.where(
                    DQLExpressions.eq(
                            DQLExpressions.property(SessionBulletinDocument.sessionObject().educationYear().fromAlias("sdocument")),
                            DQLExpressions.value(educationYear)
                    )
            );
        }

        if (yearDistributionPart != null) {
            builder.where(
                    DQLExpressions.eq(
                            DQLExpressions.property(SessionBulletinDocument.sessionObject().yearDistributionPart().fromAlias("sdocument")),
                            DQLExpressions.value(yearDistributionPart)
                    )
            );
        }

        FilterUtils.applySimpleLikeFilter(builder, "j", JournalTestResult.activity(), acitivity);


        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());

        List<ViewWrapper<JournalTestResult>> wrapper = ViewWrapper.getPatchedList(model.getDataSource());
        Iterator<ViewWrapper<JournalTestResult>> iterator = wrapper.iterator();

        while (iterator.hasNext()) {

            ViewWrapper<JournalTestResult> rows = (ViewWrapper) iterator.next();

            // Добавляем студента
            Long studentId = rows.getEntity().getStudentId();
            if (studentId != null) {
                rows.setViewProperty("student", IUniBaseDao.instance.get().get(Student.class, studentId));
            }

            // Добавляем документ сессии
            Long documentId = rows.getEntity().getDocumentId();
            if (documentId != null) {
                rows.setViewProperty("document", IUniBaseDao.instance.get().get(SessionBulletinDocument.class, documentId));
            }
        }
    }
}
