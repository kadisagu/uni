package ru.tandemservice.brsrmc.component.journaltestresult.JournalTestResultPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController<IDAO, Model> {
    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = component.getModel();
        getDao().prepare(model);
    }
}
