package ru.tandemservice.brsrmc.ws.types;

import javax.xml.bind.annotation.XmlType;

@XmlType(name = "MarkInfoWrapperWS")
public class MarkInfoWrapperWS {

    private Integer mark;
    private Long controlActionTypeId;
    private Long regElementPartId;

    public MarkInfoWrapperWS() {
    }

    /**
     * @return итоговый балл из журнала
     */
    public Integer getMark() {
        return mark;
    }

    public void setMark(Integer mark) {
        this.mark = mark;
    }

    /**
     * @return форма контроля
     */
    public Long getControlActionTypeId() {
        return controlActionTypeId;
    }

    public void setControlActionTypeId(Long controlActionTypeId) {
        this.controlActionTypeId = controlActionTypeId;
    }

    /**
     * @return часть дисциплины
     */
    public Long getRegElementPartId() {
        return regElementPartId;
    }

    public void setRegElementPartId(Long regElementPartId) {
        this.regElementPartId = regElementPartId;
    }
}