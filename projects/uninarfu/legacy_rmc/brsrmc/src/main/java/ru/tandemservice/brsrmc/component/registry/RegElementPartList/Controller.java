package ru.tandemservice.brsrmc.component.registry.RegElementPartList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.shared.commonbase.base.util.UniMap;

public class Controller extends ru.tandemservice.uniepp.component.registry.RegElementPartList.Controller {

    public void onClickDisciplineCard(IBusinessComponent component) {
        Long id = (Long) component.getListenerParameter();
        ContextLocal.createDesktop(new ComponentActivator("OxDisciplinePartCurrentCard", (new UniMap()).add("publisherId", id)), null);
//        ContextLocal.createDesktop("PersonShellDialog", new ComponentActivator("OxScholarshipType", (new UniMap()).add("publisherId", id)));
    }

}
