package ru.tandemservice.brsrmc.component.settings.UseRecordBookInSession;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {

    public void saveSettings(Model model);
}
