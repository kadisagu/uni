package ru.tandemservice.brsrmc.component.journaltestresult.JournalTestResultList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.brsrmc.entity.JournalTestResult;
import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;

import java.util.HashMap;
import java.util.Map;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        final Model model = this.getModel(component);
        model.setSettings(component.getSettings());
        this.getDao().prepare(model);

        if (model.getDataSource() == null) {
            DynamicListDataSource<JournalTestResult> dataSource = UniBaseUtils.createDataSource(component, (IListDataSourceDao) this.getDao());

            // Дата (сортируется)
            dataSource.addColumn(new DateColumn("Дата", JournalTestResult.date(), "dd.MM.yyyy").setOrderable(true));
            // Студент (сортируется по фио, в виде публикатора)
            PublisherLinkColumn studentColumn = new PublisherLinkColumn("Студент", "student.fullFio");
            studentColumn.setResolver(new IPublisherLinkResolver() {
                @Override
                public String getComponentName(IEntity entity) {
                    return "ru.tandemservice.uni.component.student.StudentPub";
                }

                @Override
                public Object getParameters(IEntity entity) {
                    Map params = new HashMap();
                    params.put(PublisherActivator.PUBLISHER_ID_KEY, ((Student) entity.getProperty("student")).getId());
                    params.put(ru.tandemservice.uni.component.student.StudentPub.Model.SELECTED_TAB_BIND_KEY, "studentTab");
                    return params;
                }
            });
            dataSource.addColumn(studentColumn.setOrderable(true));
            dataSource.setOrder(studentColumn, OrderDirection.asc);
            // Документ сессии (в виде публикатора)
            PublisherLinkColumn documentColumn = new PublisherLinkColumn("Документ сессии", "document.title");
            documentColumn.setResolver(new IPublisherLinkResolver() {
                @Override
                public Object getParameters(IEntity entity) {
                    Map params = new HashMap();
                    params.put("publisherId", ((SessionBulletinDocument) entity.getProperty("document")).getId());
                    return params;
                }

                @Override
                public String getComponentName(IEntity entity) {
                    return ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPub.Model.COMPONENT_NAME;
                }
            });
            dataSource.addColumn(documentColumn.setOrderable(false));
            // Активити (текстовое поле)
            dataSource.addColumn(new SimpleColumn("Активити", JournalTestResult.activity()).setOrderable(false));
            // Оценка
            dataSource.addColumn(new SimpleColumn("Оценка", JournalTestResult.resultMark()).setOrderable(false));
            // Описание ошибки
            dataSource.addColumn(new SimpleColumn("Описание ошибки", JournalTestResult.errorText()).setOrderable(false));
            // Публикатор на сущность JournalTestResult
            dataSource.addColumn(new ActionColumn("Просмотр", ActionColumn.EDIT, "onClickView").setPermissionKey("journalTestResultView"));

            model.setDataSource(dataSource);
        }
    }

    public void onClickSearch(IBusinessComponent component) {
        component.saveSettings();
        this.getModel(component).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component) {
        Model model = this.getModel(component);
        model.getSettings().clear();
    }

    public void onClickView(IBusinessComponent component) {
        Long id = component.getListenerParameter();

        JournalTestResult journal = getDao().get(id);
        activate(component,
                 new ComponentActivator(
                         "ru.tandemservice.brsrmc.component.journaltestresult.JournalTestResultPub",
                         new UniMap()
                                 .add(ru.tandemservice.brsrmc.component.journaltestresult.JournalTestResultPub.Model.BIND_JOURNAL, journal)
                 )
        );
    }
}
