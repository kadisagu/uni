package ru.tandemservice.brsrmc.ws.types;

import javax.xml.bind.annotation.XmlType;

@XmlType(name = "StudentMarkWs")
public class StudentMarkWs {
    public StudentMarkWs() {
    }

    private Long studentId;
    private Integer points;

    private String gradeScale5Id;
    private String gradeScale2Id;

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public String getGradeScale5Id() {
        return gradeScale5Id;
    }

    public void setGradeScale5Id(String gradeScale5Id) {
        this.gradeScale5Id = gradeScale5Id;
    }

    public String getGradeScale2Id() {
        return gradeScale2Id;
    }

    public void setGradeScale2Id(String gradeScale2Id) {
        this.gradeScale2Id = gradeScale2Id;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }
}
