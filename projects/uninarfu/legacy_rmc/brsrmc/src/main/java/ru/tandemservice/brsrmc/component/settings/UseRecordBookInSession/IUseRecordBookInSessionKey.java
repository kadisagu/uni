package ru.tandemservice.brsrmc.component.settings.UseRecordBookInSession;

public interface IUseRecordBookInSessionKey {
    public final static String OWNER_KEY = "UseRecordBookInSession";
    public final static String SETTING_KEY = "useRecordBookInSessionSetting";
}
