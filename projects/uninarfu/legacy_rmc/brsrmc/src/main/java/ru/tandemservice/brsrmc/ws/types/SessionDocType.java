package ru.tandemservice.brsrmc.ws.types;

/**
 * Различные виды документов сессии
 */
public enum SessionDocType {
    /**
     * Ведомость сессии
     */
    SESSION_BULLETIN_DOCUMENT,
    /**
     * Ведомость пересдач
     */
    SESSION_RETAKE_DOCUMENT,
    /**
     * Экзаменационный лист
     */
    SESSION_SHEET_DOCUMENT,
    /**
     * Экзаменационная карточка
     */
    SESSION_LIST_DOCUMENT,
    /**
     * Внутреннее перезачтение
     */
    SESSION_TRANSFER_INSIDE_DOCUMENT,
    /**
     * Внешнее перезачтение
     */
    SESSION_TRANSFER_OUTSIDE_DOCUMENT,
    /**
     * Зачетка студента
     */
    SESSION_STUDENT_GRADEBOOK_DOCUMENT,
    /**
     * Объект сессия
     */
    SESSION_OBJECT;
}
