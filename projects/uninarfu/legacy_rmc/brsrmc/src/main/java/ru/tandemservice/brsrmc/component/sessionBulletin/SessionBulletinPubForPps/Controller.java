package ru.tandemservice.brsrmc.component.sessionBulletin.SessionBulletinPubForPps;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.shared.commonbase.base.util.UniMap;

public class Controller extends ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPubForPps.Controller {
    public void onClickMarkOX(IBusinessComponent component) {
        Model model = (Model) getModel(component);

        ContextLocal.createDesktop(new ComponentActivator(
                ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinMark.Model.COMPONENT_NAME,
                (new UniMap()).add(PublisherActivator.PUBLISHER_ID_KEY, model.getBulletin().getId())), null);
    }

}
