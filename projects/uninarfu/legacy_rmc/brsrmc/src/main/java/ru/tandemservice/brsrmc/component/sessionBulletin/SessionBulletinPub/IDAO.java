package ru.tandemservice.brsrmc.component.sessionBulletin.SessionBulletinPub;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public interface IDAO extends ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPub.IDAO {
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean hasTutors(Model model);

}
