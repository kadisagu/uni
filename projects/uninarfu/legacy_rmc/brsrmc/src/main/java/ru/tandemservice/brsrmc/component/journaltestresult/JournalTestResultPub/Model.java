package ru.tandemservice.brsrmc.component.journaltestresult.JournalTestResultPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.brsrmc.entity.JournalTestResult;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;

@Input({
        @Bind(key = Model.BIND_JOURNAL, binding = "journal")
})
public class Model {
    public static final String BIND_JOURNAL = "journal";

    private JournalTestResult journal;
    private Student student;
    private SessionBulletinDocument document;


    public static String getBindJournal() {
        return BIND_JOURNAL;
    }

    public JournalTestResult getJournal() {
        return journal;
    }

    public void setJournal(JournalTestResult journal) {
        this.journal = journal;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public SessionBulletinDocument getDocument() {
        return document;
    }

    public void setDocument(SessionBulletinDocument document) {
        this.document = document;
    }
}
