package ru.tandemservice.brsrmc.component.PpsSessionBulletinList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.brsrmc.util.BrsUtil;

public class Controller extends ru.tandemservice.unisession.component.pps.PpsSessionBulletinList.Controller {
    @Override
    public void onClickMark(IBusinessComponent context) {
        boolean useRb = BrsUtil.getUseRecordBookInSessionMode();
        if (useRb) {
            ContextLocal.createDesktop(new ComponentActivator(
                    ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinMark.Model.COMPONENT_NAME,
                    (new UniMap()).add(PublisherActivator.PUBLISHER_ID_KEY, context.<Long>getListenerParameter())), null);
        }
        else {
            super.onClickMark(context);
        }
    }
}
