package ru.tandemservice.brsrmc.ws.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "JournalTestWs")
public class JournalTestWs {
    private Long sourceMarkId;
    /**
     * id студента
     */
    private Long studentId;
    /**
     * id колонки деятельности (RBActivityColumn)
     */
    private Long activityId;
    /**
     * оценка за Тестирование
     */
    private Integer testMark;
    /**
     * максимально возможная оценка за тестирование
     */
    private Integer maxMark;
    /**
     * активити *
     */
    private String activity;
    /**
     * код активити *
     */
    private String activityCode;
    /**
     * id документа сессии *
     */
    private Long documentId;
    /**
     * дата выполнения действия*
     */
    private Date date;


    public JournalTestWs() {
    }

    public JournalTestWs(Long sourceMarkId, Long studentId, Long activityId, Integer testMark, Integer maxMark) {
        this.sourceMarkId = sourceMarkId;
        this.studentId = studentId;
        this.activityId = activityId;
        this.testMark = testMark;
        this.maxMark = maxMark;
    }

    public Long getSourceMarkId() {
        return sourceMarkId;
    }

    public void setSourceMarkId(Long sourceMarkId) {
        this.sourceMarkId = sourceMarkId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Integer getTestMark() {
        return testMark;
    }

    public void setTestMark(Integer testMark) {
        this.testMark = testMark;
    }

    public Integer getMaxMark() {
        return maxMark;
    }

    public void setMaxMark(Integer maxMark) {
        this.maxMark = maxMark;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getActivityCode() {
        return activityCode;
    }

    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }

    public Long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
