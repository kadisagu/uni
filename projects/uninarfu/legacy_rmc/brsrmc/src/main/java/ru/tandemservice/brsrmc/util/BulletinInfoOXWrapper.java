package ru.tandemservice.brsrmc.util;

public class BulletinInfoOXWrapper {

    private Integer attestation1Sum;
    private Integer attestation2Sum;
    private Integer attestation3Sum;
    private Integer promotionSum;
    private Integer finalTestSum;
    private Integer totalSum;

    public BulletinInfoOXWrapper(Integer attestation1Sum, Integer attestation2Sum, Integer attestation3Sum, Integer promotionSum, Integer finalTestSum, Integer totalSum) {
        this.attestation1Sum = attestation1Sum;
        this.attestation2Sum = attestation2Sum;
        this.attestation3Sum = attestation3Sum;
        this.promotionSum = promotionSum;
        this.finalTestSum = finalTestSum;
        this.totalSum = totalSum;
    }

    public Integer getAttestation1Sum() {
        return attestation1Sum;
    }

    public Integer getAttestation2Sum() {
        return attestation2Sum;
    }

    public Integer getAttestation3Sum() {
        return attestation3Sum;
    }

    public Integer getPromotionSum() {
        return promotionSum;
    }

    public Integer getFinalTestSum() {
        return finalTestSum;
    }

    public Integer getTotalSum() {
        return totalSum;
    }

    public void setAttestation1Sum(Integer attestation1Sum) {
        this.attestation1Sum = attestation1Sum;
    }

    public void setAttestation2Sum(Integer attestation2Sum) {
        this.attestation2Sum = attestation2Sum;
    }

    public void setAttestation3Sum(Integer attestation3Sum) {
        this.attestation3Sum = attestation3Sum;
    }

    public void setPromotionSum(Integer promotionSum) {
        this.promotionSum = promotionSum;
    }

    public void setFinalTestSum(Integer finalTestSum) {
        this.finalTestSum = finalTestSum;
    }

    public void setTotalSum(Integer totalSum) {
        this.totalSum = totalSum;
    }

}
