package ru.tandemservice.brsrmc.component.journaltestresult.JournalTestResultList;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {
    void prepareDataSource(Model model);
}
