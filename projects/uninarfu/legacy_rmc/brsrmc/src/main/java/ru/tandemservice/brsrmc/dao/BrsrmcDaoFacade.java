package ru.tandemservice.brsrmc.dao;

import org.tandemframework.core.runtime.ApplicationRuntime;

public class BrsrmcDaoFacade {
    public BrsrmcDaoFacade() {
    }

    public static IBrsDao getBrsDao() {
        if (_brsDao == null) {
            _brsDao = (IBrsDao) ApplicationRuntime.getBean("brsDao");
        }
        return _brsDao;
    }

    private static IBrsDao _brsDao;

}
