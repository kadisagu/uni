package ru.tandemservice.brsrmc.ws.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "JournalActivityColumnsWS")
public class JournalActivityColumnsWS {
    private Long docId;
    private ActivityColumns activityColumns;

    public JournalActivityColumnsWS() {
    }

    public JournalActivityColumnsWS(Long docId) {
        this.docId = docId;
        activityColumns = new ActivityColumns();
    }

    public Long getDocId() {
        return docId;
    }

    public void setDocId(Long docId) {
        this.docId = docId;
    }

    public ActivityColumns getActivityColumns() {
        return activityColumns;
    }

    public void setActivityColumns(ActivityColumns activityColumns) {
        this.activityColumns = activityColumns;
    }
}
