package ru.tandemservice.brsrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_brsrmc_1x0x0_0to1 extends IndependentMigrationScript {

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность journalTestResult

        // создана новая сущность
        if (tool.tableExists("ox_journaltestresult_t")) {
            // создать таблицу
            DBTable dbt = new DBTable("ox_journaltestresult_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("sourcemarkid_p", DBType.LONG),
                                      new DBColumn("studentid_p", DBType.LONG),
                                      new DBColumn("activityid_p", DBType.LONG),
                                      new DBColumn("resultmark_p", DBType.INTEGER),
                                      new DBColumn("errorcode_p", DBType.createVarchar(255)),
                                      new DBColumn("errortext_p", DBType.createVarchar(255))
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("journalTestResult");

        }


    }
}