package ru.tandemservice.brsrmc.component.sessionSheet.SessionSheetMark;

public class Model extends ru.tandemservice.unisession.component.sessionSheet.SessionSheetMark.Model {
    private boolean alternative;

    public boolean isAlternative() {
        return alternative;
    }

    public void setAlternative(boolean alternative) {
        this.alternative = alternative;
    }
}
