package ru.tandemservice.brsrmc.ws.impl;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.tandemframework.core.runtime.ApplicationRuntime;
import ru.tandemservice.brsrmc.util.BulletinInfoOXWrapper;
import ru.tandemservice.brsrmc.util.JournalMarkOXWrapper;
import ru.tandemservice.brsrmc.ws.ISessionJournalWS;
import ru.tandemservice.brsrmc.ws.types.MarkInfoWrapperWS;
import ru.tandemservice.brsrmc.ws.types.StudentBulletinInfoWS;
import ru.tandemservice.brsrmc.ws.types.StudentMarkInfoWS;
import ru.tandemservice.uni.dao.UniBaseDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class SessionJournalWSManager extends UniBaseDao implements
        ISessionJournalWSManager
{

    public static ISessionJournalWSManager instance;

    public static ISessionJournalWSManager getInstance() {

        if (instance == null) {
            instance = (ISessionJournalWSManager) ApplicationRuntime.getBean(ISessionJournalWSManager.class.getName());
        }
        return instance;
    }

    @Override
    public Map<Long, JournalMarkOXWrapper> getJournalMarkData(List<Long> studentIds, Long eduYearId, Long yearDistrPartId)
            throws Exception
    {

        Map<Long, JournalMarkOXWrapper> journalMarkData = new HashMap<Long, JournalMarkOXWrapper>();

        ISessionJournalWS journalWS = (ISessionJournalWS) ApplicationRuntime.getBean(ISessionJournalWS.WS_SESSION_JOURNAL_BEAN_NAME);
        List<StudentMarkInfoWS> wsResult = journalWS.getStudentMarkInfoFromJournal(studentIds, eduYearId, yearDistrPartId);

        if (wsResult != null) {
            for (StudentMarkInfoWS studentMarkInfoWS : wsResult) {
                Long studentId = studentMarkInfoWS.getStudentId();
                JournalMarkOXWrapper journalMarkWrapper = new JournalMarkOXWrapper(studentId, studentMarkInfoWS.getSessionRaiting(), studentMarkInfoWS.getTotalRaiting());
                for (MarkInfoWrapperWS markInfoWrapperWS : studentMarkInfoWS.getStudentMarkInfo()) {
                    MultiKey key = journalMarkWrapper.key(studentId, markInfoWrapperWS.getControlActionTypeId(), markInfoWrapperWS.getRegElementPartId());
                    journalMarkWrapper.getJournalMarkMap().put(key, markInfoWrapperWS.getMark());
                }
                journalMarkData.put(studentId, journalMarkWrapper);
            }
        }

        return journalMarkData;
    }

    @Override
    public Map<MultiKey, BulletinInfoOXWrapper> getBulletinInfoOXJournal(List<Long> sessionDocIds) throws Exception {
        Map<MultiKey, BulletinInfoOXWrapper> bulletinJournalData = new HashMap<>();

        ISessionJournalWS journalWS = (ISessionJournalWS) ApplicationRuntime.getBean(ISessionJournalWS.WS_SESSION_JOURNAL_BEAN_NAME);
        List<StudentBulletinInfoWS> wsResult = journalWS.getBulletinInfoFromJournal(sessionDocIds);
        if (wsResult != null) {
            for (StudentBulletinInfoWS studentBulletinInfoWS : wsResult) {
                MultiKey key = new MultiKey(studentBulletinInfoWS.getBulletinId(), studentBulletinInfoWS.getStudentId());

                BulletinInfoOXWrapper bulletinInfoWrapper = new BulletinInfoOXWrapper(
                        studentBulletinInfoWS.getAttestation1Sum(),
                        studentBulletinInfoWS.getAttestation2Sum(),
                        studentBulletinInfoWS.getAttestation3Sum(),
                        studentBulletinInfoWS.getPromotionSum(),
                        studentBulletinInfoWS.getFinalTestSum(),
                        studentBulletinInfoWS.getTotalSum());

                bulletinJournalData.put(key, bulletinInfoWrapper);
            }
        }

        return bulletinJournalData;
    }

}
