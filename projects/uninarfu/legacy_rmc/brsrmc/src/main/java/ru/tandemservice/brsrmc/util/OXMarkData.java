package ru.tandemservice.brsrmc.util;

import org.apache.commons.lang.StringUtils;
import ru.tandemservice.brsrmc.ws.types.StudentMarkWs;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.uniepp.entity.catalog.codes.EppGradeScaleCodes;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.ISessionMarkDAO;
import ru.tandemservice.unisession.dao.document.ISessionDocumentBaseDAO;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;

import java.util.Date;

public class OXMarkData implements ISessionMarkDAO.MarkData {
    private StudentMarkWs markWs;
    private Date performDate;
    private SessionMarkCatalogItem markValue;

    public OXMarkData(StudentMarkWs markWs, Date performDate, SessionDocumentSlot slot) {
        this.markWs = markWs;
        this.performDate = performDate;

        EppGradeScale scale = ISessionDocumentBaseDAO.instance.get().getGradeScale(slot.getStudentWpeCAction());
        if (scale.getCode().equals(EppGradeScaleCodes.SCALE_2) &&
                !StringUtils.isEmpty(markWs.getGradeScale2Id()))
        {
            this.markValue = UniDaoFacade.getCoreDao().get(SessionMarkCatalogItem.class, Long.valueOf(markWs.getGradeScale2Id()));
        }
        else if (scale.getCode().equals(EppGradeScaleCodes.SCALE_5) &&
                !StringUtils.isEmpty(markWs.getGradeScale5Id()))
        {
            this.markValue = UniDaoFacade.getCoreDao().get(SessionMarkCatalogItem.class, Long.valueOf(markWs.getGradeScale5Id()));
        }
    }

    @Override
    public Date getPerformDate() {
        return performDate;
    }

    @Override
    public Double getPoints() {
        return markWs.getPoints().doubleValue();
    }

    @Override
    public SessionMarkCatalogItem getMarkValue() {
        return markValue;
    }

    @Override
    public String getComment() {
        return "";
    }

}
