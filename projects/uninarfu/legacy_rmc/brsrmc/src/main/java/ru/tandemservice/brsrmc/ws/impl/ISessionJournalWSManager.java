package ru.tandemservice.brsrmc.ws.impl;

import org.apache.commons.collections.keyvalue.MultiKey;
import ru.tandemservice.brsrmc.util.BulletinInfoOXWrapper;
import ru.tandemservice.brsrmc.util.JournalMarkOXWrapper;

import java.util.List;
import java.util.Map;

public interface ISessionJournalWSManager {

    /**
     * Данные по баллам и рейтингам из журналов OX по документам сессии
     *
     * @param studentIds      список id студентов
     * @param eduYearId       id учебного года
     * @param yearDistrPartId id части учебного года
     *
     * @return
     *
     * @throws Exception
     */
    public Map<Long, JournalMarkOXWrapper> getJournalMarkData(
            List<Long> studentIds,
            Long eduYearId,
            Long yearDistrPartId) throws Exception;

    /**
     * Данные для ведомости из журналов OX
     *
     * @param sessionDocIds список id ведомостей
     *
     * @return
     *
     * @throws Exception
     */
    public Map<MultiKey, BulletinInfoOXWrapper> getBulletinInfoOXJournal(List<Long> sessionDocIds) throws Exception;
}
