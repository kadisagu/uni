package ru.tandemservice.uniepprmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uniepprmc.entity.PaymentsImportedFile;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Импортированный файл платежей по договорам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PaymentsImportedFileGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepprmc.entity.PaymentsImportedFile";
    public static final String ENTITY_NAME = "paymentsImportedFile";
    public static final int VERSION_HASH = 512530232;
    private static IEntityMeta ENTITY_META;

    public static final String P_USER = "user";
    public static final String P_IMPORT_DATE = "importDate";
    public static final String P_IMPORT_TYPE = "importType";
    public static final String P_FILE_NAME = "fileName";
    public static final String L_IMPORTED_FILE = "importedFile";
    public static final String L_RESULT_FILE = "resultFile";

    private String _user;     // Пользователь
    private Date _importDate;     // Дата импорта
    private String _importType;     // Тип импорта
    private String _fileName;     // Название файла
    private DatabaseFile _importedFile;     // Импортированный файл
    private DatabaseFile _resultFile;     // Результирующий файл

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Пользователь.
     */
    @Length(max=255)
    public String getUser()
    {
        return _user;
    }

    /**
     * @param user Пользователь.
     */
    public void setUser(String user)
    {
        dirty(_user, user);
        _user = user;
    }

    /**
     * @return Дата импорта. Свойство не может быть null.
     */
    @NotNull
    public Date getImportDate()
    {
        return _importDate;
    }

    /**
     * @param importDate Дата импорта. Свойство не может быть null.
     */
    public void setImportDate(Date importDate)
    {
        dirty(_importDate, importDate);
        _importDate = importDate;
    }

    /**
     * @return Тип импорта. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getImportType()
    {
        return _importType;
    }

    /**
     * @param importType Тип импорта. Свойство не может быть null.
     */
    public void setImportType(String importType)
    {
        dirty(_importType, importType);
        _importType = importType;
    }

    /**
     * @return Название файла. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFileName()
    {
        return _fileName;
    }

    /**
     * @param fileName Название файла. Свойство не может быть null.
     */
    public void setFileName(String fileName)
    {
        dirty(_fileName, fileName);
        _fileName = fileName;
    }

    /**
     * @return Импортированный файл. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getImportedFile()
    {
        return _importedFile;
    }

    /**
     * @param importedFile Импортированный файл. Свойство не может быть null.
     */
    public void setImportedFile(DatabaseFile importedFile)
    {
        dirty(_importedFile, importedFile);
        _importedFile = importedFile;
    }

    /**
     * @return Результирующий файл. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getResultFile()
    {
        return _resultFile;
    }

    /**
     * @param resultFile Результирующий файл. Свойство не может быть null.
     */
    public void setResultFile(DatabaseFile resultFile)
    {
        dirty(_resultFile, resultFile);
        _resultFile = resultFile;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PaymentsImportedFileGen)
        {
            setUser(((PaymentsImportedFile)another).getUser());
            setImportDate(((PaymentsImportedFile)another).getImportDate());
            setImportType(((PaymentsImportedFile)another).getImportType());
            setFileName(((PaymentsImportedFile)another).getFileName());
            setImportedFile(((PaymentsImportedFile)another).getImportedFile());
            setResultFile(((PaymentsImportedFile)another).getResultFile());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PaymentsImportedFileGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PaymentsImportedFile.class;
        }

        public T newInstance()
        {
            return (T) new PaymentsImportedFile();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "user":
                    return obj.getUser();
                case "importDate":
                    return obj.getImportDate();
                case "importType":
                    return obj.getImportType();
                case "fileName":
                    return obj.getFileName();
                case "importedFile":
                    return obj.getImportedFile();
                case "resultFile":
                    return obj.getResultFile();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "user":
                    obj.setUser((String) value);
                    return;
                case "importDate":
                    obj.setImportDate((Date) value);
                    return;
                case "importType":
                    obj.setImportType((String) value);
                    return;
                case "fileName":
                    obj.setFileName((String) value);
                    return;
                case "importedFile":
                    obj.setImportedFile((DatabaseFile) value);
                    return;
                case "resultFile":
                    obj.setResultFile((DatabaseFile) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "user":
                        return true;
                case "importDate":
                        return true;
                case "importType":
                        return true;
                case "fileName":
                        return true;
                case "importedFile":
                        return true;
                case "resultFile":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "user":
                    return true;
                case "importDate":
                    return true;
                case "importType":
                    return true;
                case "fileName":
                    return true;
                case "importedFile":
                    return true;
                case "resultFile":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "user":
                    return String.class;
                case "importDate":
                    return Date.class;
                case "importType":
                    return String.class;
                case "fileName":
                    return String.class;
                case "importedFile":
                    return DatabaseFile.class;
                case "resultFile":
                    return DatabaseFile.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PaymentsImportedFile> _dslPath = new Path<PaymentsImportedFile>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PaymentsImportedFile");
    }
            

    /**
     * @return Пользователь.
     * @see ru.tandemservice.uniepprmc.entity.PaymentsImportedFile#getUser()
     */
    public static PropertyPath<String> user()
    {
        return _dslPath.user();
    }

    /**
     * @return Дата импорта. Свойство не может быть null.
     * @see ru.tandemservice.uniepprmc.entity.PaymentsImportedFile#getImportDate()
     */
    public static PropertyPath<Date> importDate()
    {
        return _dslPath.importDate();
    }

    /**
     * @return Тип импорта. Свойство не может быть null.
     * @see ru.tandemservice.uniepprmc.entity.PaymentsImportedFile#getImportType()
     */
    public static PropertyPath<String> importType()
    {
        return _dslPath.importType();
    }

    /**
     * @return Название файла. Свойство не может быть null.
     * @see ru.tandemservice.uniepprmc.entity.PaymentsImportedFile#getFileName()
     */
    public static PropertyPath<String> fileName()
    {
        return _dslPath.fileName();
    }

    /**
     * @return Импортированный файл. Свойство не может быть null.
     * @see ru.tandemservice.uniepprmc.entity.PaymentsImportedFile#getImportedFile()
     */
    public static DatabaseFile.Path<DatabaseFile> importedFile()
    {
        return _dslPath.importedFile();
    }

    /**
     * @return Результирующий файл. Свойство не может быть null.
     * @see ru.tandemservice.uniepprmc.entity.PaymentsImportedFile#getResultFile()
     */
    public static DatabaseFile.Path<DatabaseFile> resultFile()
    {
        return _dslPath.resultFile();
    }

    public static class Path<E extends PaymentsImportedFile> extends EntityPath<E>
    {
        private PropertyPath<String> _user;
        private PropertyPath<Date> _importDate;
        private PropertyPath<String> _importType;
        private PropertyPath<String> _fileName;
        private DatabaseFile.Path<DatabaseFile> _importedFile;
        private DatabaseFile.Path<DatabaseFile> _resultFile;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Пользователь.
     * @see ru.tandemservice.uniepprmc.entity.PaymentsImportedFile#getUser()
     */
        public PropertyPath<String> user()
        {
            if(_user == null )
                _user = new PropertyPath<String>(PaymentsImportedFileGen.P_USER, this);
            return _user;
        }

    /**
     * @return Дата импорта. Свойство не может быть null.
     * @see ru.tandemservice.uniepprmc.entity.PaymentsImportedFile#getImportDate()
     */
        public PropertyPath<Date> importDate()
        {
            if(_importDate == null )
                _importDate = new PropertyPath<Date>(PaymentsImportedFileGen.P_IMPORT_DATE, this);
            return _importDate;
        }

    /**
     * @return Тип импорта. Свойство не может быть null.
     * @see ru.tandemservice.uniepprmc.entity.PaymentsImportedFile#getImportType()
     */
        public PropertyPath<String> importType()
        {
            if(_importType == null )
                _importType = new PropertyPath<String>(PaymentsImportedFileGen.P_IMPORT_TYPE, this);
            return _importType;
        }

    /**
     * @return Название файла. Свойство не может быть null.
     * @see ru.tandemservice.uniepprmc.entity.PaymentsImportedFile#getFileName()
     */
        public PropertyPath<String> fileName()
        {
            if(_fileName == null )
                _fileName = new PropertyPath<String>(PaymentsImportedFileGen.P_FILE_NAME, this);
            return _fileName;
        }

    /**
     * @return Импортированный файл. Свойство не может быть null.
     * @see ru.tandemservice.uniepprmc.entity.PaymentsImportedFile#getImportedFile()
     */
        public DatabaseFile.Path<DatabaseFile> importedFile()
        {
            if(_importedFile == null )
                _importedFile = new DatabaseFile.Path<DatabaseFile>(L_IMPORTED_FILE, this);
            return _importedFile;
        }

    /**
     * @return Результирующий файл. Свойство не может быть null.
     * @see ru.tandemservice.uniepprmc.entity.PaymentsImportedFile#getResultFile()
     */
        public DatabaseFile.Path<DatabaseFile> resultFile()
        {
            if(_resultFile == null )
                _resultFile = new DatabaseFile.Path<DatabaseFile>(L_RESULT_FILE, this);
            return _resultFile;
        }

        public Class getEntityClass()
        {
            return PaymentsImportedFile.class;
        }

        public String getEntityName()
        {
            return "paymentsImportedFile";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
