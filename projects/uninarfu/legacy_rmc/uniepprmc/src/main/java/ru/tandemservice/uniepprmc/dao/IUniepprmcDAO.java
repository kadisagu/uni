package ru.tandemservice.uniepprmc.dao;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public interface IUniepprmcDAO {

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void fillOurgUnit() throws Exception;
}
