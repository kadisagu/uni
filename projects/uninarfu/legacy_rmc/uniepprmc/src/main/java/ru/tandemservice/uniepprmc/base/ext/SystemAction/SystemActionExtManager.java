package ru.tandemservice.uniepprmc.base.ext.SystemAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.SystemActionManager;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.util.SystemActionDefinition;
import ru.tandemservice.uniepprmc.base.ext.SystemAction.ui.Pub.SystemActionPubExt;


@Configuration
public class SystemActionExtManager extends BusinessObjectExtensionManager {

    @Autowired
    private SystemActionManager _systemActionManager;

    @Bean
    public ItemListExtension<SystemActionDefinition> buttonListExtension()
    {
        return itemListExtension(_systemActionManager.buttonListExtPoint())
                .add("uniepp_fillOrgUnit", new SystemActionDefinition("uniepp", "fillOrgUnit", "onClickFillOrgUnit", SystemActionPubExt.SYSTEM_ACTION_PUB_ADDON_NAME))
                .create();
    }

}
