package ru.tandemservice.uniepprmc.dao;

import org.hibernate.Session;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.uni.util.ReportRenderer;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UniepprmcDAO extends UniBaseDao implements IUniepprmcDAO {

    private static IUniepprmcDAO uniepprmcDAO;

    public static IUniepprmcDAO instance() {
        if (uniepprmcDAO == null)
            uniepprmcDAO = (IUniepprmcDAO) ApplicationRuntime.getBean("uniepprmcDAO");
        return uniepprmcDAO;
    }

    @Override
    public void fillOurgUnit() throws Exception {

        Session session = getSession();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppEduPlan.class, "e")
                .column("e")
                .joinPath(DQLJoinType.inner, EppEduPlan.owner().fromAlias("e"), "ou")
                .where(DQLExpressions.isNull(DQLExpressions.property(EppEduPlan.owner().fromAlias("e"))));

        DQLSelectBuilder formOUBuilder = new DQLSelectBuilder().fromEntity(OrgUnitToKindRelation.class, "rel")
                .where(DQLExpressions.eqValue(DQLExpressions.property(OrgUnitToKindRelation.orgUnitKind().code().fromAlias("rel")), UniDefines.CATALOG_ORGUNIT_KIND_FORMING))
                .column(OrgUnitToKindRelation.orgUnit().fromAlias("rel").s());

        List<OrgUnit> formOUList = formOUBuilder.createStatement(session).list();

        List<EppEduPlan> eduPlanList = builder.createStatement(session).list();

        List<String> errorList = new ArrayList<String>();
        /*
		 * в системе стоит ограничение на редактирование УП (запрещается редкатирвоание в состоянии Согласовано)
		 * поэтому подразделение устанавливаем напрямую sql запросом
		 */
        IEntityMeta meta = EntityRuntime.getMeta(EppEduPlan.class);
        PreparedStatement statement = session.connection().prepareStatement("update " + meta.getTableName() + " set owner_id=? where id=?");

        for (EppEduPlan eduPlan : eduPlanList) {
            OrgUnit formOU = getFormOU(eduPlan.getOwner(), formOUList);

            if (formOU == null) {
                errorList.add(new StringBuilder().append("Для УП ")
                                      .append(eduPlan.getTitle())
                                      .append(" не найдено формирующее подразделение.")
                                      .toString()
                );
                continue;
            }

            statement.setLong(1, formOU.getId());
            statement.setLong(2, eduPlan.getId());
            statement.executeUpdate();
        }
        statement.close();
        if (!errorList.isEmpty())
            printError(errorList);

    }

    private void printError(List<String> errorList) {
        RtfDocument errorReport = RtfBean.getElementFactory().createRtfDocument();
        IRtfControl par = RtfBean.getElementFactory().createRtfControl(IRtfData.PAR);
        for (String text : errorList) {
            IRtfText studText = RtfBean.getElementFactory().createRtfText(text);
            errorReport.addElement(studText);
            errorReport.getElementList().addAll(Arrays.asList(par, par));
        }
        BusinessComponentUtils.downloadDocument(new ReportRenderer("Ошибки.rtf", errorReport, false), true);
    }

    private OrgUnit getFormOU(OrgUnit orgUnit, List<OrgUnit> formOUList) {
        if (formOUList.contains(orgUnit))
            return orgUnit;

        OrgUnit ou = orgUnit;

        while (ou.getParent() != null) {
            ou = ou.getParent();
            if (formOUList.contains(ou))
                return ou;
        }

        return null;
    }

}
