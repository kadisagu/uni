package ru.tandemservice.uniepprmc.base.ext.SystemAction.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.ui.Pub.SystemActionPubUI;
import ru.tandemservice.uniepprmc.dao.UniepprmcDAO;


public class SystemActionPubAddon extends UIAddon {

    public SystemActionPubAddon(IUIPresenter presenter, String name,
                                String componentId)
    {
        super(presenter, name, componentId);

    }

    SystemActionPubUI parent = getPresenter();

    public void onClickFillOrgUnit() throws Exception {
        UniepprmcDAO.instance().fillOurgUnit();
    }


}
