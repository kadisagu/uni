package ru.tandemservice.loadrmc.base.bo.OrgUnitLoad.ui.ReportList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.loadrmc.base.bo.OrgUnitLoad.logic.ReportBeanDefinition;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.OrgUnitKind;
import ru.tandemservice.uniepp.entity.settings.EppTutorOrgUnit;

import java.util.*;

@Configuration
public class OrgUnitLoadReportList extends BusinessComponentManager {
    public static final String LIST_DS = "listDS";
    public static final String REPORT_LIST_BEAN_NAME = "loadrmcOrgUnitReportDefinitionList";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(LIST_DS, columnListExtPoint(), entityDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint columnListExtPoint() {
        return columnListExtPointBuilder(LIST_DS)
                .addColumn(publisherColumn("title", "reportDefinition.title")
                                   .publisherLinkResolver(new IPublisherLinkResolver() {
                                       @Override
                                       public Object getParameters(IEntity iEntity) {
                                           return new UniMap().add("publisherId", ((OrgUnitLoadReportListUI.ReportDefinitionWrapper) iEntity).getOrgUnitId());
                                       }

                                       @Override
                                       public String getComponentName(IEntity iEntity) {
                                           return ((OrgUnitLoadReportListUI.ReportDefinitionWrapper) iEntity).getReportDefinition().getComponentName();
                                       }
                                   })
                                   .create()
                )
                .create();
    }

    @Bean
    IBusinessHandler<DSInput, DSOutput> entityDSHandler() {
        return new DefaultSearchDataSourceHandler(getName()) {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context) {
                Long orgUnitId = context.get("orgUnitId");
                OrgUnit orgUnit = UniDaoFacade.getCoreDao().get(OrgUnit.class, orgUnitId);
                Set<String> kindSet = new HashSet<String>();
                for (OrgUnitKind orgUnitKind : UniDaoFacade.getOrgstructDao().getOrgUnitKindList(orgUnit))
                    kindSet.add(orgUnitKind.getCode());

                Boolean isFormative = kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_FORMING);
                Boolean isProducing = kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING);
                DQLSelectBuilder dql = new DQLSelectBuilder()
                        .fromEntity(EppTutorOrgUnit.class, "t")
                        .column("t")
                        .where(DQLExpressions.eq(DQLExpressions.property(EppTutorOrgUnit.orgUnit().fromAlias("t")), DQLExpressions.value(orgUnit)));
                Boolean isTutor = UniDaoFacade.getCoreDao().getList(dql).size() > 0;

                List<OrgUnitLoadReportListUI.ReportDefinitionWrapper> wrappers = new ArrayList<OrgUnitLoadReportListUI.ReportDefinitionWrapper>();
                Long id = 0L;
                for (ReportBeanDefinition repDef : (List<ReportBeanDefinition>) ApplicationRuntime.getBean(REPORT_LIST_BEAN_NAME)) {
                    if ((repDef.getFormative() && isFormative) ||
                            (repDef.getProducing() && isProducing) ||
                            (repDef.getTutor() && isTutor))
                    {
                        OrgUnitLoadReportListUI.ReportDefinitionWrapper w = new OrgUnitLoadReportListUI.ReportDefinitionWrapper(id++, repDef, orgUnitId);
                        wrappers.add(w);
                    }
                }

                Collections.sort(wrappers, ITitled.TITLED_COMPARATOR);

                return ListOutputBuilder.get(input, wrappers).pageable(false).build();
            }
        };
    }
}
