package ru.tandemservice.loadrmc.entity.catalog;

import ru.tandemservice.loadrmc.entity.catalog.gen.EmployeeWageRatePartGen;

/**
 * Доля ставки
 */
public class EmployeeWageRatePart extends EmployeeWageRatePartGen {
    public Double getValueAsDouble() {
        String value = getTitle();
        if (value == null) {
            return 0D;
        }
        else
            try {
                return Double.parseDouble(value);
            }
            catch (Exception ex) {
                return 0D;
            }
    }

    public void setValueAsDouble(Double value) {
        if (value == null)
            setTitle("0");
        else
            setTitle(value.toString());
    }
}