package ru.tandemservice.loadrmc.base.bo.OrgUnitLoad.ui.Tab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.OrgUnitKind;
import ru.tandemservice.uniepp.entity.settings.EppTutorOrgUnit;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@State({
        @Bind(key = "orgUnitId", binding = "orgUnitId"),
        @Bind(key = "selectedTab", binding = "selectedTab")
})
public class OrgUnitLoadTabUI extends UIPresenter {
    private Long orgUnitId;
    private OrgUnit orgUnit = new OrgUnit();
    private OrgUnitSecModel secModel;
    private boolean isFormative = false;
    private boolean isProducing = false;
    private boolean isTutor = false;

    private String selectedTab;
    private Map<String, Object> params;

    @Override
    public void onComponentRefresh() {
        setParams(new HashMap<String, Object>());
        orgUnit = UniDaoFacade.getCoreDao().get(OrgUnit.class, orgUnitId);
        Set<String> kindSet = new HashSet<String>();
        for (OrgUnitKind orgUnitKind : UniDaoFacade.getOrgstructDao().getOrgUnitKindList(orgUnit))
            kindSet.add(orgUnitKind.getCode());
        isFormative = kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_FORMING);
        isProducing = kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING);
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppTutorOrgUnit.class, "t")
                .column("t")
                .where(DQLExpressions.eq(DQLExpressions.property(EppTutorOrgUnit.orgUnit().fromAlias("t")), DQLExpressions.value(orgUnit)));
        isTutor = UniDaoFacade.getCoreDao().getList(dql).size() > 0;

        UniDaoFacade.getCoreDao().get(EppTutorOrgUnit.class, orgUnitId);

        secModel = new OrgUnitSecModel(orgUnit);
        getParams().put("orgUnitId", orgUnitId);

    }

    public boolean isFormative() {
        return isFormative;
    }

    public boolean isProducing() {
        return isProducing;
    }

    public boolean isTutor() {
        return isTutor && !isFormative();
    }

    public Long getOrgUnitId() {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId) {
        this.orgUnitId = orgUnitId;
    }

    public OrgUnit getOrgUnit() {
        return orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit) {
        this.orgUnit = orgUnit;
    }

    public OrgUnitSecModel getSecModel() {
        return secModel;
    }

    public void setSecModel(OrgUnitSecModel secModel) {
        this.secModel = secModel;
    }

    public String getSelectedTab() {
        return selectedTab;
    }

    public void setSelectedTab(String selectedTab) {
        this.selectedTab = selectedTab;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

}
