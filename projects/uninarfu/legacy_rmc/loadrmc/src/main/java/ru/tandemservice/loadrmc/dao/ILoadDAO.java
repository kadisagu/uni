package ru.tandemservice.loadrmc.dao;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.loadrmc.entity.EmployeePostRatePartSource;

import java.util.List;

public interface ILoadDAO {
    public List<EmployeePostRatePartSource> getEmployeeLoad(EmployeePost empPost);
}
