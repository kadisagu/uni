package ru.tandemservice.loadrmc.component.catalog.employeeWageRatePart.EmployeeWageRatePartPub;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.loadrmc.entity.catalog.EmployeeWageRatePart;


public class Controller extends DefaultCatalogPubController<EmployeeWageRatePart, Model, IDAO> {

    @Override
    protected DynamicListDataSource<EmployeeWageRatePart> createListDataSource(
            IBusinessComponent context)
    {

        Model model = (Model) getModel(context);
        DynamicListDataSource<EmployeeWageRatePart> dataSource = new DynamicListDataSource<EmployeeWageRatePart>(context, this);

        dataSource.addColumn(new SimpleColumn("Наименование", "title").setClickable(false), 1);
        dataSource.addColumn(new SimpleColumn("Значение", "value").setClickable(false), 1);
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
        if (model.isUserCatalog()) {
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", ICatalogItem.CATALOG_ITEM_TITLE).setPermissionKey(model.getCatalogItemDelete()).setDisableHandler(model.getDisabledEntityHandler()));
        }

        return dataSource;
    }

    public void onClickToggleUse(IBusinessComponent component)
    {
        getDao().updateInUse(getModel(component), (Long) component.getListenerParameter());
    }
}
