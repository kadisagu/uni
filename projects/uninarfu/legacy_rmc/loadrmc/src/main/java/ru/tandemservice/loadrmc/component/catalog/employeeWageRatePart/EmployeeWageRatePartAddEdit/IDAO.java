package ru.tandemservice.loadrmc.component.catalog.employeeWageRatePart.EmployeeWageRatePartAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.loadrmc.entity.catalog.EmployeeWageRatePart;


public interface IDAO extends IDefaultCatalogAddEditDAO<EmployeeWageRatePart, Model> {

}
