package ru.tandemservice.loadrmc.component.catalog.employeeWageRatePart.EmployeeWageRatePartPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;
import ru.tandemservice.loadrmc.entity.catalog.EmployeeWageRatePart;

public interface IDAO extends IDefaultCatalogPubDAO<EmployeeWageRatePart, Model> {

    void updateInUse(Model model, Long id);
}
