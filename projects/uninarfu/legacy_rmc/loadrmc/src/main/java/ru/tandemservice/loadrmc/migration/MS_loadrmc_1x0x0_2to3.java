package ru.tandemservice.loadrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_loadrmc_1x0x0_2to3 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.12"),
                        new ScriptDependency("org.tandemframework.shared", "1.2.5"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.2.5")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность employeeWageRatePart

        // создано обязательное свойство value
        {
            // создать колонку
            tool.createColumn("employeewageratepart_t", new DBColumn("value_p", DBType.DOUBLE));

            // TODO: программист должен подтвердить это действие
//			if( true )
//				throw new UnsupportedOperationException("Confirm me: set default value for required property");

            // задать значение по умолчанию

            java.lang.String[] titles = {"0.25", "0.5", "0.75", "1"};
            java.lang.Double[] defaultValue = {0.25d, 0.5d, 0.75d, 1d};
            java.lang.Double other = 0d;

            for (int i = 0; i < 4; i++)
                tool.executeUpdate("update employeewageratepart_t set value_p=? where title_p=?", defaultValue[i], titles[i]);
            tool.executeUpdate("update employeewageratepart_t set value_p=? where value_p is null", other);
            // сделать колонку NOT NULL
            tool.setColumnNullable("employeewageratepart_t", "value_p", false);

        }


    }
}