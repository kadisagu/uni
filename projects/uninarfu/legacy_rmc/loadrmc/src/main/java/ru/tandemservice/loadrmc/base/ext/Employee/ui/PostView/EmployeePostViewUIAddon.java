package ru.tandemservice.loadrmc.base.ext.Employee.ui.PostView;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostView.EmployeePostViewUI;
import ru.tandemservice.loadrmc.dao.LoadDAO;
import ru.tandemservice.loadrmc.entity.EmployeePostRatePartSource;
import ru.tandemservice.uni.dao.UniDaoFacade;

import java.util.ArrayList;
import java.util.List;

public class EmployeePostViewUIAddon extends UIAddon {
    private EmployeePostViewUI parent = getPresenter();
    private List<EmployeePostRatePartSource> partList;
    private List<EmployeePostRatePartSource> delList;
    private EmployeePostRatePartSource currentRow;

    private boolean isEdit = false;

    public EmployeePostViewUIAddon(IUIPresenter presenter, String name, String componentId) {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentPrepareRender() {


    }

    @Override
    public void onComponentRefresh() {
        parent.getSettings().set("employeePost", parent.getEmployeePost());

        setPartList(LoadDAO.getInstance().getEmployeeLoad(parent.getEmployeePost()));
        delList = new ArrayList<EmployeePostRatePartSource>();
    }

    public void onClickEditEmployeePostRates() {
        isEdit = !isEdit;
    }

    public void onClickDelete() {
        EmployeePostRatePartSource toRemove = getListenerParameter();
        delList.add(toRemove);
        getPartList().remove(toRemove);
    }

    public void onClickAdd() {
        EmployeePostRatePartSource p = new EmployeePostRatePartSource();
        p.setEmployeePost(parent.getEmployeePost());
        getPartList().add(p);
    }

    public void onClickSave() {
        Double sumLoads = 0.;
        for (EmployeePostRatePartSource p : getPartList()) {
            sumLoads += p.getEmployeeWageRatePart().getValue();
        }
        if (sumLoads > 1) {
            throw new ApplicationException("Ставка не должна превышать единицы");
        }

        for (EmployeePostRatePartSource p : getPartList()) {
            UniDaoFacade.getCoreDao().saveOrUpdate(p);
        }
        for (EmployeePostRatePartSource p : delList) {
            if (p.getId() != null) {
                UniDaoFacade.getCoreDao().delete(p);
            }
        }
        delList.clear();
        setEdit(false);
    }

    public void onClickCancel() {
        setEdit(false);
        this._presenter.getSupport().doRefresh();
    }

    public boolean isEdit() {
        return isEdit;
    }

    public boolean isNotEdit() {
        return !isEdit;
    }

    public void setEdit(boolean edit) {
        isEdit = edit;
    }

    public List<EmployeePostRatePartSource> getPartList() {
        return partList;
    }

    public void setPartList(List<EmployeePostRatePartSource> partList) {
        this.partList = partList;
    }

    public EmployeePostRatePartSource getCurrentRow() {
        return currentRow;
    }

    public void setCurrentRow(EmployeePostRatePartSource currentRow) {
        this.currentRow = currentRow;
    }
}
