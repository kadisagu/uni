package ru.tandemservice.loadrmc.base.ext.Employee.logic;

import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import ru.tandemservice.loadrmc.entity.catalog.EmployeeWageRatePart;

public class WageRateDSHandler extends DefaultComboDataSourceHandler {

    public WageRateDSHandler(String ownerId) {
        super(ownerId, EmployeeWageRatePart.class);
    }

}
