package ru.tandemservice.loadrmc.base.bo.EmployeeLoadrmc.ui.PostRatesEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;

@Input({
        @Bind(key = EmployeeLoadrmcPostRatesEditUI.EMPLOYEE_POST_ID, binding = "employeePostId")
})
public class EmployeeLoadrmcPostRatesEditUI extends UIPresenter {
    public static final String EMPLOYEE_POST_ID = "employeePostId";

    public Long getEmployeePostId() {
        return employeePostId;
    }

    public void setEmployeePostId(Long employeePostId) {
        this.employeePostId = employeePostId;
    }

    private Long employeePostId;
    private EmployeePost employeePost;

    @Override
    public void onComponentRefresh()
    {
        EmployeePost ep = DataAccessServices.dao().getNotNull(employeePostId);
        ep.update(employeePost);
    }

    // Getters & Setters

    public Long getEmployeeId()
    {
        return employeePostId;
    }

    public void setEmployeeId(Long employeeId)
    {
        employeePostId = employeeId;
    }

    // Listeners

    public void onClickApply()
    {
//        EmployeeManager.instance().dao().updateEmployee(employeePostId, employeePost);

        deactivate();
    }
}
