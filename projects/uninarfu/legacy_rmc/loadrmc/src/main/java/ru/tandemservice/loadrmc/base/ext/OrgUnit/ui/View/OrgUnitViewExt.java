package ru.tandemservice.loadrmc.base.ext.OrgUnit.ui.View;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitView;

@Configuration
public class OrgUnitViewExt extends BusinessComponentExtensionManager {
    @Autowired
    public OrgUnitView _orgUnitView;


    @Bean
    public TabPanelExtension tabPanelExtension() {
        return tabPanelExtensionBuilder(_orgUnitView.orgUnitTabPanelExtPoint())
                .addTab(componentTab("abstractOrgUnit_rmcLoad", "OrgUnitLoadTab").permissionKey("ui:secModel.orgUnit_viewRmcLoad").parameters("mvel:['orgUnitId':presenter.orgUnit.id]"))
                .create();
    }
}
