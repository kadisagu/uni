package ru.tandemservice.loadrmc.base.bo.EmployeeLoadrmc;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

@Configuration
public class EmployeeLoadrmcManager extends BusinessObjectManager {
    public static EmployeeLoadrmcManager instance()
    {
        return instance(EmployeeLoadrmcManager.class);
    }
}
