package ru.tandemservice.loadrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.loadrmc.entity.EmployeePostRatePartSource;
import ru.tandemservice.loadrmc.entity.catalog.EmployeeWageRatePart;
import ru.tandemservice.uni.entity.catalog.GlobalFinancingSource;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Доля ставки от источника финансирования
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeePostRatePartSourceGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.loadrmc.entity.EmployeePostRatePartSource";
    public static final String ENTITY_NAME = "employeePostRatePartSource";
    public static final int VERSION_HASH = 343095962;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_POST = "employeePost";
    public static final String L_EMPLOYEE_WAGE_RATE_PART = "employeeWageRatePart";
    public static final String L_GLOBAL_FINANCING_SOURCE = "globalFinancingSource";

    private EmployeePost _employeePost;     // Сотрудник
    private EmployeeWageRatePart _employeeWageRatePart;     // Доля ставки
    private GlobalFinancingSource _globalFinancingSource;     // Источник финансирования

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сотрудник.
     */
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Сотрудник.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    /**
     * @return Доля ставки.
     */
    public EmployeeWageRatePart getEmployeeWageRatePart()
    {
        return _employeeWageRatePart;
    }

    /**
     * @param employeeWageRatePart Доля ставки.
     */
    public void setEmployeeWageRatePart(EmployeeWageRatePart employeeWageRatePart)
    {
        dirty(_employeeWageRatePart, employeeWageRatePart);
        _employeeWageRatePart = employeeWageRatePart;
    }

    /**
     * @return Источник финансирования.
     */
    public GlobalFinancingSource getGlobalFinancingSource()
    {
        return _globalFinancingSource;
    }

    /**
     * @param globalFinancingSource Источник финансирования.
     */
    public void setGlobalFinancingSource(GlobalFinancingSource globalFinancingSource)
    {
        dirty(_globalFinancingSource, globalFinancingSource);
        _globalFinancingSource = globalFinancingSource;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EmployeePostRatePartSourceGen)
        {
            setEmployeePost(((EmployeePostRatePartSource)another).getEmployeePost());
            setEmployeeWageRatePart(((EmployeePostRatePartSource)another).getEmployeeWageRatePart());
            setGlobalFinancingSource(((EmployeePostRatePartSource)another).getGlobalFinancingSource());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeePostRatePartSourceGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeePostRatePartSource.class;
        }

        public T newInstance()
        {
            return (T) new EmployeePostRatePartSource();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "employeePost":
                    return obj.getEmployeePost();
                case "employeeWageRatePart":
                    return obj.getEmployeeWageRatePart();
                case "globalFinancingSource":
                    return obj.getGlobalFinancingSource();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
                case "employeeWageRatePart":
                    obj.setEmployeeWageRatePart((EmployeeWageRatePart) value);
                    return;
                case "globalFinancingSource":
                    obj.setGlobalFinancingSource((GlobalFinancingSource) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "employeePost":
                        return true;
                case "employeeWageRatePart":
                        return true;
                case "globalFinancingSource":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "employeePost":
                    return true;
                case "employeeWageRatePart":
                    return true;
                case "globalFinancingSource":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "employeePost":
                    return EmployeePost.class;
                case "employeeWageRatePart":
                    return EmployeeWageRatePart.class;
                case "globalFinancingSource":
                    return GlobalFinancingSource.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeePostRatePartSource> _dslPath = new Path<EmployeePostRatePartSource>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeePostRatePartSource");
    }
            

    /**
     * @return Сотрудник.
     * @see ru.tandemservice.loadrmc.entity.EmployeePostRatePartSource#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    /**
     * @return Доля ставки.
     * @see ru.tandemservice.loadrmc.entity.EmployeePostRatePartSource#getEmployeeWageRatePart()
     */
    public static EmployeeWageRatePart.Path<EmployeeWageRatePart> employeeWageRatePart()
    {
        return _dslPath.employeeWageRatePart();
    }

    /**
     * @return Источник финансирования.
     * @see ru.tandemservice.loadrmc.entity.EmployeePostRatePartSource#getGlobalFinancingSource()
     */
    public static GlobalFinancingSource.Path<GlobalFinancingSource> globalFinancingSource()
    {
        return _dslPath.globalFinancingSource();
    }

    public static class Path<E extends EmployeePostRatePartSource> extends EntityPath<E>
    {
        private EmployeePost.Path<EmployeePost> _employeePost;
        private EmployeeWageRatePart.Path<EmployeeWageRatePart> _employeeWageRatePart;
        private GlobalFinancingSource.Path<GlobalFinancingSource> _globalFinancingSource;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сотрудник.
     * @see ru.tandemservice.loadrmc.entity.EmployeePostRatePartSource#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

    /**
     * @return Доля ставки.
     * @see ru.tandemservice.loadrmc.entity.EmployeePostRatePartSource#getEmployeeWageRatePart()
     */
        public EmployeeWageRatePart.Path<EmployeeWageRatePart> employeeWageRatePart()
        {
            if(_employeeWageRatePart == null )
                _employeeWageRatePart = new EmployeeWageRatePart.Path<EmployeeWageRatePart>(L_EMPLOYEE_WAGE_RATE_PART, this);
            return _employeeWageRatePart;
        }

    /**
     * @return Источник финансирования.
     * @see ru.tandemservice.loadrmc.entity.EmployeePostRatePartSource#getGlobalFinancingSource()
     */
        public GlobalFinancingSource.Path<GlobalFinancingSource> globalFinancingSource()
        {
            if(_globalFinancingSource == null )
                _globalFinancingSource = new GlobalFinancingSource.Path<GlobalFinancingSource>(L_GLOBAL_FINANCING_SOURCE, this);
            return _globalFinancingSource;
        }

        public Class getEntityClass()
        {
            return EmployeePostRatePartSource.class;
        }

        public String getEntityName()
        {
            return "employeePostRatePartSource";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
