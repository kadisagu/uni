package ru.tandemservice.loadrmc.base.ext.Employee;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import ru.tandemservice.loadrmc.base.ext.Employee.logic.FinancingDSHandler;
import ru.tandemservice.loadrmc.base.ext.Employee.logic.WageRateDSHandler;
import ru.tandemservice.loadrmc.entity.catalog.EmployeeWageRatePart;
import ru.tandemservice.uni.entity.catalog.GlobalFinancingSource;

@Configuration
public class EmployeeManagerExt
        extends BusinessObjectExtensionManager
{

    public static EmployeeManagerExt instance() {
        return instance(EmployeeManagerExt.class);
    }


    @Bean
    public IDefaultComboDataSourceHandler wageRateDSHandler() {
        return new WageRateDSHandler(getName());
    }

    @Bean
    public UIDataSourceConfig wageRateDSConfig()
    {
        return SelectDSConfig.with("wageRateDS", getName()).dataSourceClass(SelectDataSource.class).handler(wageRateDSHandler()).addColumn(EmployeeWageRatePart.title().s()).create();
    }

    @Bean
    public IDefaultComboDataSourceHandler financingDSHandler() {
        return new FinancingDSHandler(getName());
    }

    @Bean
    public UIDataSourceConfig financingDSConfig()
    {
        return SelectDSConfig.with("financingDS", getName()).dataSourceClass(SelectDataSource.class).handler(financingDSHandler()).addColumn(GlobalFinancingSource.title().s()).create();
    }


}
