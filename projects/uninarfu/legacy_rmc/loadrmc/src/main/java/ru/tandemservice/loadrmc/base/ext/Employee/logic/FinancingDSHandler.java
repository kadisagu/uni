package ru.tandemservice.loadrmc.base.ext.Employee.logic;

import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import ru.tandemservice.uni.entity.catalog.GlobalFinancingSource;

public class FinancingDSHandler extends DefaultComboDataSourceHandler {

    public FinancingDSHandler(String ownerId) {
        super(ownerId, GlobalFinancingSource.class);
    }

}
