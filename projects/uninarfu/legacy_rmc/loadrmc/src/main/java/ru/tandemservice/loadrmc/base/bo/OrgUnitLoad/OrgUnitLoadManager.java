package ru.tandemservice.loadrmc.base.bo.OrgUnitLoad;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

@Configuration
public class OrgUnitLoadManager extends BusinessObjectManager {
    public static OrgUnitLoadManager instance() {
        return instance(OrgUnitLoadManager.class);
    }
}
