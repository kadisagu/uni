package ru.tandemservice.loadrmc.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.*;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;

import java.util.Map;

public class OrgstructPermissionModifier implements ISecurityConfigMetaMapModifier {
    @Override
    public void modify(Map<String, SecurityConfigMeta> securityConfigMetaMap) {
        final SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("loadrmc");
        config.setName("loadrmc-orgstruct-sec-config");
        config.setTitle("");

        final ModuleGlobalGroupMeta mggOrgstruct = PermissionMetaUtil.createModuleGlobalGroup(config, "orgstructModuleGlobalGroup", "Модуль «Орг. структура»");
        final ModuleLocalGroupMeta mlgOrgstruct = PermissionMetaUtil.createModuleLocalGroup(config, "orgstructLocalGroup", "Модуль «Орг. структура»");

        for (final OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class, OrgUnitType.P_TITLE)) {
            final String code = description.getCode();
            final ClassGroupMeta gcg = PermissionMetaUtil.createClassGroup(mggOrgstruct, code + "PermissionClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());
            final ClassGroupMeta lcg = PermissionMetaUtil.createClassGroup(mlgOrgstruct, code + "LocalClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());

            final PermissionGroupMeta pgRmcLoadTab = PermissionMetaUtil.createPermissionGroup(config, code + "RmcLoadPermissionGroup", "Вкладка «Нагрузка»");
            PermissionMetaUtil.createGroupRelation(config, pgRmcLoadTab.getName(), gcg.getName());
            PermissionMetaUtil.createGroupRelation(config, pgRmcLoadTab.getName(), lcg.getName());
            PermissionMetaUtil.createPermission(pgRmcLoadTab, "orgUnit_viewRmcLoad_" + code, "Просмотр");

            // Вкладка "Учебная нагрузка" на табе "Нагрузка"
            PermissionGroupMeta permissoinGroupEduLoad = PermissionMetaUtil.createPermissionGroup(pgRmcLoadTab, code + "RmcLoadTabEduLoadPermissionGroup", "Вкладка «Нагрузка по РУП»");
            PermissionMetaUtil.createPermission(permissoinGroupEduLoad, "loadrmc_edu_tab_" + code, "Просмотр");

            // Вкладка "Неучебная нагрузка" на табе "Нагрузка"
            PermissionGroupMeta permissoinGroupNoEduLoad = PermissionMetaUtil.createPermissionGroup(pgRmcLoadTab, code + "RmcLoadTabNoEduLoadPermissionGroup", "Вкладка «Прочая нагрузка»");
            PermissionMetaUtil.createPermission(permissoinGroupNoEduLoad, "loadrmc_no_edu_tab_" + code, "Просмотр");

            // Вкладка "Неучебная нагрузка" на табе "Нагрузка"
            PermissionGroupMeta permissoinGroupNoEduCathLoad = PermissionMetaUtil.createPermissionGroup(pgRmcLoadTab, code + "RmcLoadTabNoEduLoadCathPermissionGroup", "Вкладка «Прочая нагрузка (каф.)»");
            PermissionMetaUtil.createPermission(permissoinGroupNoEduCathLoad, "loadrmc_no_edu_cath_tab_" + code, "Просмотр");


            // Вкладка "ПАГС" на табе "Нагрузка"
            PermissionGroupMeta permissoinGroupPlanGroup = PermissionMetaUtil.createPermissionGroup(pgRmcLoadTab, code + "RmcLoadTabPlanGroupPermissionGroup", "Вкладка «ПАГС»");
            PermissionMetaUtil.createPermission(permissoinGroupPlanGroup, "loadrmc_pg_tab_" + code, "Просмотр");

            // Вкладка "Формирование ПУГС" на табе "Нагрузка"
            PermissionGroupMeta permissoinBuildPAGLoad = PermissionMetaUtil.createPermissionGroup(pgRmcLoadTab, code + "RmcLoadTabBuildPAGPermissionGroup", "Вкладка «Формирование ПУГС»");
            PermissionMetaUtil.createPermission(permissoinBuildPAGLoad, "loadrmc_build_pag_tab_" + code, "Просмотр");

            // Вкладка "ПУГС" на табе "Нагрузка"
            PermissionGroupMeta permissoinPlanActGroupLoad = PermissionMetaUtil.createPermissionGroup(pgRmcLoadTab, code + "RmcLoadTabPlanActGroupPermissionGroup", "Вкладка «ПУГС»");
            PermissionMetaUtil.createPermission(permissoinPlanActGroupLoad, "loadrmc_pag_tab_" + code, "Просмотр");

            // Вкладка "Наборы ПУГС" на табе "Нагрузка"
            PermissionGroupMeta permissoinGroupPagBatchLoad = PermissionMetaUtil.createPermissionGroup(pgRmcLoadTab, code + "RmcLoadTabPagBatchPermissionGroup", "Вкладка «Наборы ПУГС»");
            PermissionMetaUtil.createPermission(permissoinGroupPagBatchLoad, "loadrmc_pag_batch_tab_" + code, "Просмотр");

            //Вкладка "Отчеты" на табе "Нагрузка"
            PermissionGroupMeta permissoinReports = PermissionMetaUtil.createPermissionGroup(pgRmcLoadTab, code + "RmcLoadTabReportsPermissionGroup", "Вкладка «Отчеты»");
            PermissionMetaUtil.createPermission(permissoinReports, "loadrmc_reportrs_tab_" + code, "Просмотр");

        }

        securityConfigMetaMap.put(config.getName(), config);
    }
}
