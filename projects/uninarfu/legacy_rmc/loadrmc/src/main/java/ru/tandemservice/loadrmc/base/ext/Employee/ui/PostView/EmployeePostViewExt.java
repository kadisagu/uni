package ru.tandemservice.loadrmc.base.ext.Employee.ui.PostView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.block.BlockListExtension;
import org.tandemframework.caf.ui.config.block.IBlockListExtensionBuilder;
import org.tandemframework.caf.ui.config.block.IHtmlBlockBuilder;
import org.tandemframework.caf.ui.config.button.ButtonListExtension;
import org.tandemframework.caf.ui.config.button.IButtonListExtensionBuilder;
import org.tandemframework.caf.ui.config.presenter.IPresenterExtensionBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostView.EmployeePostView;
import ru.tandemservice.loadrmc.base.ext.Employee.EmployeeManagerExt;

@Configuration
public class EmployeePostViewExt extends BusinessComponentExtensionManager {
    @Autowired
    private EmployeePostView parent;

    public static final String EMPLOYEE_POST_RATES_BLOCK = "employeePostRatesBlock";
    public static final String EMPLOYEE_POST_RATES_EDIT_DATA_BUTTON = "employeePostRatesEditDataButton";
    public static final String PARTS_DS = "partsDS";

    @Bean
    public PresenterExtension presenterExtension()
    {
        IPresenterExtensionBuilder pi = presenterExtensionBuilder(parent.presenterExtPoint());
        pi.addAddon(uiAddon("ui_addon", EmployeePostViewUIAddon.class));
        pi.addDataSource(EmployeeManagerExt.instance().wageRateDSConfig());
        pi.addDataSource(EmployeeManagerExt.instance().financingDSConfig());
        return pi.create();
    }


    @Bean
    public BlockListExtension blockListExtension() {
        IBlockListExtensionBuilder be = blockListExtensionBuilder(parent.employeePostPubBlockListExtPoint());
        IHtmlBlockBuilder block = htmlBlock(EMPLOYEE_POST_RATES_BLOCK, "EmployeePostRatesBlock").permissionKey("ui:secModel.viewEmployeePost");
        be.addAllAfter(EmployeePostView.EMPLOYEE_POST_ATTRS_BLOCK);
        be.addBlock(block);

        return be.create();
    }


    @Bean
    public ButtonListExtension employeePostDataActionButtonListExtPoint() {
        IButtonListExtensionBuilder be = buttonListExtensionBuilder(parent.employeePostDataActionButtonListExtPoint());
        be.addButton(submitButton(EMPLOYEE_POST_RATES_EDIT_DATA_BUTTON, "ui_addon:onClickEditEmployeePostRates")
                             .permissionKey("ui:secModel.editEmployeeRate")
                             .visible("addon:ui_addon.notEdit"));
        return be.create();
    }


}
