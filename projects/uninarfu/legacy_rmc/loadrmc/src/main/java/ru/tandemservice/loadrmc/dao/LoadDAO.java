package ru.tandemservice.loadrmc.dao;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.loadrmc.entity.EmployeePostRatePartSource;
import ru.tandemservice.uni.dao.UniBaseDao;

import java.util.List;

public class LoadDAO extends UniBaseDao implements ILoadDAO {
    public static ILoadDAO getInstance() {
        return (ILoadDAO) ApplicationRuntime.getBean("loadDao");
    }

    @Override
    public List<EmployeePostRatePartSource> getEmployeeLoad(EmployeePost empPost) {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EmployeePostRatePartSource.class, "ep");
        dql.where(DQLExpressions.eq(
                DQLExpressions.property(EmployeePostRatePartSource.employeePost().fromAlias("ep")),
                DQLExpressions.value(empPost)
        ));
        return getList(dql);
    }
}
