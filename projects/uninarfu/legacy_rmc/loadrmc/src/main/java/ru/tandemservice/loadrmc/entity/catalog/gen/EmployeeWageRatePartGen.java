package ru.tandemservice.loadrmc.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.loadrmc.entity.catalog.EmployeeWageRatePart;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Доля ставки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeWageRatePartGen extends EntityBase
 implements INaturalIdentifiable<EmployeeWageRatePartGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.loadrmc.entity.catalog.EmployeeWageRatePart";
    public static final String ENTITY_NAME = "employeeWageRatePart";
    public static final int VERSION_HASH = 267737528;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_VALUE = "value";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private double _value;     // Доля ставки
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Доля ставки. Свойство не может быть null.
     */
    @NotNull
    public double getValue()
    {
        return _value;
    }

    /**
     * @param value Доля ставки. Свойство не может быть null.
     */
    public void setValue(double value)
    {
        dirty(_value, value);
        _value = value;
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EmployeeWageRatePartGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EmployeeWageRatePart)another).getCode());
            }
            setValue(((EmployeeWageRatePart)another).getValue());
            setTitle(((EmployeeWageRatePart)another).getTitle());
        }
    }

    public INaturalId<EmployeeWageRatePartGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EmployeeWageRatePartGen>
    {
        private static final String PROXY_NAME = "EmployeeWageRatePartNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EmployeeWageRatePartGen.NaturalId) ) return false;

            EmployeeWageRatePartGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeWageRatePartGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeWageRatePart.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeWageRatePart();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "value":
                    return obj.getValue();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "value":
                    obj.setValue((Double) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "value":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "value":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "value":
                    return Double.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeWageRatePart> _dslPath = new Path<EmployeeWageRatePart>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeWageRatePart");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.loadrmc.entity.catalog.EmployeeWageRatePart#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Доля ставки. Свойство не может быть null.
     * @see ru.tandemservice.loadrmc.entity.catalog.EmployeeWageRatePart#getValue()
     */
    public static PropertyPath<Double> value()
    {
        return _dslPath.value();
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.loadrmc.entity.catalog.EmployeeWageRatePart#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EmployeeWageRatePart> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<Double> _value;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.loadrmc.entity.catalog.EmployeeWageRatePart#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EmployeeWageRatePartGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Доля ставки. Свойство не может быть null.
     * @see ru.tandemservice.loadrmc.entity.catalog.EmployeeWageRatePart#getValue()
     */
        public PropertyPath<Double> value()
        {
            if(_value == null )
                _value = new PropertyPath<Double>(EmployeeWageRatePartGen.P_VALUE, this);
            return _value;
        }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.loadrmc.entity.catalog.EmployeeWageRatePart#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EmployeeWageRatePartGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EmployeeWageRatePart.class;
        }

        public String getEntityName()
        {
            return "employeeWageRatePart";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
