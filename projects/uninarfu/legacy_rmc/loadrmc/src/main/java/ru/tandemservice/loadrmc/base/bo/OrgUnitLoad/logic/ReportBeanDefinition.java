package ru.tandemservice.loadrmc.base.bo.OrgUnitLoad.logic;

public class ReportBeanDefinition {
    private String id;
    private String componentName;
    private String title;
    private String permissionKey;

    /**
     * нужно ли добавлять отчет на формирующих подразделениях
     */
    private Boolean isFormative = false;
    /**
     * нужно ли добавлять отчет на выпускающих подразделениях
     */
    private Boolean isProducing = false;
    /**
     * нужно ли добавлять отчет на читающих подразделениях
     */
    private Boolean isTutor = false;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPermissionKey() {
        return permissionKey;
    }

    public void setPermissionKey(String permissionKey) {
        this.permissionKey = permissionKey;
    }

    public Boolean getFormative() {
        return isFormative;
    }

    public void setFormative(Boolean formative) {
        isFormative = formative;
    }

    public Boolean getProducing() {
        return isProducing;
    }

    public void setProducing(Boolean producing) {
        isProducing = producing;
    }

    public Boolean getTutor() {
        return isTutor;
    }

    public void setTutor(Boolean tutor) {
        isTutor = tutor;
    }
}
