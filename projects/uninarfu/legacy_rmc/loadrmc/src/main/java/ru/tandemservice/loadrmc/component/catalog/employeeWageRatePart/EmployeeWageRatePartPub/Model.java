package ru.tandemservice.loadrmc.component.catalog.employeeWageRatePart.EmployeeWageRatePartPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.loadrmc.entity.catalog.EmployeeWageRatePart;

public class Model extends DefaultCatalogPubModel<EmployeeWageRatePart> {

    public static final Long SHOW_USE_CODE = Long.valueOf(0L);
    public static final Long SHOW_NON_USE_CODE = Long.valueOf(1L);

    private ISelectModel usedModel;

    public ISelectModel getUsedModel() {
        return usedModel;
    }

    public void setUsedModel(ISelectModel usedModel) {
        this.usedModel = usedModel;
    }

}
