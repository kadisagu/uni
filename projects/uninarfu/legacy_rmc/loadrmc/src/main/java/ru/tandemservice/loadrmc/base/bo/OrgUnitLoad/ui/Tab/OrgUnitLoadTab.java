package ru.tandemservice.loadrmc.base.bo.OrgUnitLoad.ui.Tab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;

@Configuration
public class OrgUnitLoadTab extends BusinessComponentManager {
    public static final String TAB_LIST = "loadTabList";
    public static final String REPORTS_TAB = "reportsTab";

    public static final String EDU_TAB = "eduTab";
    public static final String NO_EDU_TAB = "noEduTab";
    public static final String NO_EDU_CATH_TAB = "noEduCathTab";

    public static final String PG_TAB = "PGTab";
    public static final String BUILD_PAG_TAB = "buildPagTab";
    public static final String BUILD_PAG_F_TAB = "buildPagFTab";

    public static final String PAG_TAB = "PagTab";
    public static final String PAG_F_TAB = "PagFTab";

    public static final String PAG_BATCH_TAB = "PAGBatchTab";
    public static final String PAG_BATCH_F_TAB = "PAGBatchFTab";


    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder().create();
    }

    @Bean
    public TabPanelExtPoint tabPanelExtPoint() {
        return tabPanelExtPointBuilder(TAB_LIST)
                .addTab(componentTab(REPORTS_TAB, "OrgUnitLoadReportList")
                                .parameters("ui:params")
                                .permissionKey("ui:secModel.loadrmc_reportrs_tab"))
                .addTab(componentTab(PG_TAB, "OxPlannedGroup")
                                .visible("ui:formative")
                                .permissionKey("ui:secModel.loadrmc_pg_tab"))

                .addTab(componentTab(BUILD_PAG_F_TAB, "OxBuildPAGUtil")
                                .parameters("mvel:['publisherIdParameterName':'formOrgUnit']")
                                .visible("ui:formative")
                                .permissionKey("ui:secModel.loadrmc_build_pag_tab"))
                .addTab(componentTab(BUILD_PAG_TAB, "OxBuildPAGUtil")
                                .visible("ui:tutor")
                                .permissionKey("ui:secModel.loadrmc_build_pag_tab"))

                .addTab(componentTab(PAG_F_TAB, "OxPlannedActualGroup")
                                .parameters("mvel:['publisherIdParameterName':'educationOrgUnit_formativeorgunit']")
                                .visible("ui:formative")
                                .permissionKey("ui:secModel.loadrmc_pag_tab"))
                .addTab(componentTab(PAG_TAB, "OxPlannedActualGroup")
                                .visible("ui:tutor")
                                .permissionKey("ui:secModel.loadrmc_pag_tab"))

                .addTab(componentTab(PAG_BATCH_F_TAB, "OxPAGBatchVariant")
                                .parameters("mvel:['publisherIdParameterName':'orgUnit_parent']")
                                .visible("ui:formative")
                                .permissionKey("ui:secModel.loadrmc_pag_batch_tab"))
                .addTab(componentTab(PAG_BATCH_TAB, "OxPAGBatchVariant")
                                .visible("ui:tutor")
                                .permissionKey("ui:secModel.loadrmc_pag_batch_tab"))

                .addTab(componentTab(EDU_TAB, "OxBuildPagEmployeeLoadUtil")
                                .parameters("ui:params")
                                .visible("ui:tutor")
                                .permissionKey("ui:secModel.loadrmc_edu_tab"))
                .addTab(componentTab(NO_EDU_TAB, "OxBuildEmployeeNoEduLoadUtil")
                                .parameters("ui:params")
                                .visible("ui:tutor")
                                .permissionKey("ui:secModel.loadrmc_no_edu_tab"))
                .addTab(componentTab(NO_EDU_CATH_TAB, "OxBuildCathedraNoEduLoadUtil")
                                .parameters("ui:params")
                                .visible("ui:formative")
                                .permissionKey("ui:secModel.loadrmc_no_edu_cath_tab"))

                .create();
    }
}
