package ru.tandemservice.loadrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Доля ставки"
 * Имя сущности : employeeWageRatePart
 * Файл data.xml : catalogs.data.xml
 */
public interface EmployeeWageRatePartCodes
{
    /** Константа кода (code) элемента : quarter (code). Название (title) : 0.25 */
    String QUARTER = "quarter";
    /** Константа кода (code) элемента : half (code). Название (title) : 0.5 */
    String HALF = "half";
    /** Константа кода (code) элемента : three-quarter (code). Название (title) : 0.75 */
    String THREE_QUARTER = "three-quarter";
    /** Константа кода (code) элемента : full (code). Название (title) : 1 */
    String FULL = "full";

    Set<String> CODES = ImmutableSet.of(QUARTER, HALF, THREE_QUARTER, FULL);
}
