package ru.tandemservice.unisessionrmc.base.ext.SessionTransfer.logic;

import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.shared.person.catalog.entity.EduInstitution;

public class EduInstitutionDSHandler extends DefaultComboDataSourceHandler {

    public EduInstitutionDSHandler(String ownerId) {
        super(ownerId, EduInstitution.class, EduInstitution.P_TITLE);
    }


}
