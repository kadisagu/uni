package ru.tandemservice.unisessionrmc.component.settings.DisciplineModule;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModule;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model)
    {
    }

    @Override
    public void process(Model model)
    {

    }

    @Override
    public void processModuleInNewTransaction(EppRegistryModule row)
    {
    }

}
