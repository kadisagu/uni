package ru.tandemservice.unisessionrmc.base.ext.SessionTransfer.ui.OutsideAddEdit;

import org.tandemframework.shared.person.catalog.entity.EduInstitution;
import ru.tandemservice.unisessionrmc.entity.SessionTransferOutsideOperationExt;
import ru.tandemservice.unisession.base.bo.SessionTransfer.logic.ISessionTransferDao;

public class SessionTransferOutOpWrapperExt extends ru.tandemservice.unisession.base.bo.SessionTransfer.ui.OutsideAddEdit.SessionTransferOutOpWrapper implements ISessionTransferDao.SessionTransferOutsideOperationInfo {

    private EduInstitution eduInstitution;

    public SessionTransferOutOpWrapperExt() {
        super();
    }

    public SessionTransferOutOpWrapperExt(SessionTransferOutsideOperationExt operationExt) {
        super(operationExt.getSessionTransferOutsideOperation(), null);
        setEduInstitution(operationExt.getEduInstitution());
    }

    public SessionTransferOutOpWrapperExt(SessionTransferOutOpWrapperExt other) {
        super(other);
        setEduInstitution(other.getEduInstitution());
    }


    public EduInstitution getEduInstitution() {
        return eduInstitution;
    }

    public void setEduInstitution(EduInstitution eduInstitution) {
        this.eduInstitution = eduInstitution;
    }


}
