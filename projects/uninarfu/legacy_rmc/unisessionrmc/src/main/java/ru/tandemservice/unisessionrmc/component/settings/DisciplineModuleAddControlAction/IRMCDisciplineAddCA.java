package ru.tandemservice.unisessionrmc.component.settings.DisciplineModuleAddControlAction;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModule;

public interface IRMCDisciplineAddCA {

    // ru.tandemservice.unisessionrmc.component.settings.DisciplineModuleAddControlAction.IRMCDisciplineAddCA
    public static final SpringBeanCache<IRMCDisciplineAddCA> instance = new SpringBeanCache<IRMCDisciplineAddCA>(IRMCDisciplineAddCA.class.getName());

    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
    void processModuleInNewTransaction(EppRegistryModule row, Model model);

    void process(Model model) throws Exception;
}
