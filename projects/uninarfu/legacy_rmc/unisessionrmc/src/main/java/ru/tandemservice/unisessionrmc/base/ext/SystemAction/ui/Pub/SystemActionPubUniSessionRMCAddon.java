package ru.tandemservice.unisessionrmc.base.ext.SystemAction.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.component.Activator;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.IBusinessController;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.ui.Pub.SystemActionPubUI;

public class SystemActionPubUniSessionRMCAddon extends UIAddon {

    public SystemActionPubUniSessionRMCAddon(IUIPresenter presenter,
                                             String name, String componentId)
    {
        super(presenter, name, componentId);

    }

    SystemActionPubUI parent = getPresenter();

    public void onDisciplineModule() {

        // вызываем другой компонент
        activateInRoot(new ComponentActivator(
                ru.tandemservice.unisessionrmc.IRamecUniSessionRmcComponents.UNIDS_SYSTEM_ACTION_DISCIPLINE_MODULE));
    }

    public void onDisciplineModuleAddControlAction() {
        // вызываем другой компонент
        activateInRoot(new ComponentActivator(
                ru.tandemservice.unisessionrmc.IRamecUniSessionRmcComponents.UNIDS_SYSTEM_ACTION_DISCIPLINE_MODULE_ADD_CONTROL_ACTION));
    }

    public void onClickPerezachtenie() {
        // вызываем другой компонент
        activateInRoot(new ComponentActivator(
                ru.tandemservice.unisessionrmc.IRamecUniSessionRmcComponents.UNIDS_SYSTEM_PEREZACHTENIE));
    }

    private void activateInRoot(Activator activator) {

        IBusinessComponent currentComponent = parent.getUserContext().getCurrentComponent();

        IBusinessController controller = currentComponent.getController();
        controller.activateInRoot(currentComponent, activator);
    }
}
