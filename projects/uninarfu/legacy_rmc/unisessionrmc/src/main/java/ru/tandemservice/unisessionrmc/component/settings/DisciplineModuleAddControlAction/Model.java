package ru.tandemservice.unisessionrmc.component.settings.DisciplineModuleAddControlAction;

import ru.tandemservice.uniepp.entity.registry.EppRegistryModule;

import java.util.List;

public class Model {
    public static final Object MUTEX = new Object();
    private int processedModule = 0;
    private String title = "";
    private List<EppRegistryModule> rows;

    private boolean processAllModule = false;


    public void setProcessAllModule(boolean processAllModule) {
        this.processAllModule = processAllModule;
    }


    public boolean getProcessAllModule() {
        return processAllModule;
    }


    public void setRows(List<EppRegistryModule> rows) {
        this.rows = rows;
    }


    public List<EppRegistryModule> getRows() {
        return rows;
    }


    public void setTitle(String title) {
        this.title = title;
    }


    public String getTitle() {
        return title;
    }

    public void AddTitle(String msg)
    {
        title += msg;
    }


    public void setProcessedModule(int processedModule) {
        this.processedModule = processedModule;
    }


    public int getProcessedModule() {
        return processedModule;
    }

    public void AddProcessed()
    {
        processedModule++;
    }
}
