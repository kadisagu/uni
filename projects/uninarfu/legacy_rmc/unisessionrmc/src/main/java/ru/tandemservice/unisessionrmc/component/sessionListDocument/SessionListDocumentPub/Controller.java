package ru.tandemservice.unisessionrmc.component.sessionListDocument.SessionListDocumentPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.BooleanColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;

public class Controller extends ru.tandemservice.unisession.component.sessionListDocument.SessionListDocumentPub.Controller {
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        super.onRefreshComponent(component);


        final DynamicListDataSource<SessionDocumentSlot> dataSource = getModel(component).getDataSource();
        dataSource.getColumns().set(3, new BooleanColumn("В сессию", SessionDocumentSlot.inSession().s()));
    }
}
