package ru.tandemservice.unisessionrmc.component.settings.DisciplineModule;

import org.hibernate.Session;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.catalog.*;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.plan.data.IEppEpvRow;
import ru.tandemservice.uniepp.entity.registry.*;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

//import ru.tandemservice.uniepp.entity.registry.EppRegistryDisciplineLoad;


public class RMCDisciplineDAO extends UniBaseDao implements IRMCDisciplineDAO {

    private Map<Long, Map<Long, IEppEpvBlockWrapper>> map = new HashMap<Long, Map<Long, IEppEpvBlockWrapper>>();
    private List<EppControlActionType> controlActionTypes = null;
    private Map<Long, StateChange> mapShangeState = new HashMap<Long, StateChange>();


    private Map<Long, DisciplineInfo> mapDI = new HashMap<Long, DisciplineInfo>();


    @Override
    public void process(Model model)
    {

        map.clear();
        mapDI.clear();

        controlActionTypes = this.getCatalogItemListOrderByCode(EppControlActionType.class);
        // 1 - получим полный список всех модулей
        List<EppRegistryModule> rows = _getModuleList();

        model.setRows(rows);

    }


    @Override
    public void processModuleInNewTransaction(EppRegistryModule row, Model model)
    {
        _processModule(row, model);
    }


    /**
     * вычисляем аудиторную нагрузку модуля
     *
     * @param module
     * @param session
     *
     * @return
     */
    private Long _getModuleLoad(EppRegistryModule module, Session session)
    {
        Long retVal = (long) 0;

        // получим часы нагрузок из модуля
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppRegistryModuleALoad.class, "mlt")
                .addColumn("mlt")
                        // только от нужного модуля
                .where(eq(property(EppRegistryModuleALoad.module().fromAlias("mlt")), value(module)));

        List<EppRegistryModuleALoad> rows = dql.createStatement(session).list();

        for (EppRegistryModuleALoad row : rows) {
            retVal += row.getLoad();
        }
        return retVal;
    }

    /**
     * Обработка отдельно взятого модуля
     *
     * @param row
     */
    private void _processModule(EppRegistryModule module, Model model)
    {

        final Session session = getSession();

        mapShangeState = new HashMap<Long, StateChange>();
        System.out.println(module.getNumber() + " " + module.getTitle() + " *******************************");

        Long _load = _getModuleLoad(module, session);

        if (!model.getProcessAllModule() && _load > 0 && module.getTitle().indexOf("#") == -1) {
            System.out.println(module.getTitle() + " не обрабатываем ");
            return;
        }

        // проверяем фильтр по названию
        if (model.getModuleTitle() != null && model.getModuleTitle() != "") {
            if (module.getTitle().indexOf(model.getModuleTitle()) == -1)
                return;
        }

        if (model.getPassProcessed() && module.getTitle().indexOf("#") != -1)
            return;


        if (model.getPassNotNull() && _load > 0)
            return;


        model.AddProcessed();


        // зная модуль - найдем все части, которым он пренадлежит
        // на основе части - дисциплина, по дисциплине - версии УПов
        DQLSelectBuilder dqlMP = new DQLSelectBuilder()
                .fromEntity(EppRegistryElementPartModule.class, "mp")
                .addColumn("mp")
                        // только от нужного модуля
                .where(eq(property(EppRegistryElementPartModule.module().fromAlias("mp")), value(module)));

        List<EppRegistryElementPartModule> rowsModulePart = dqlMP.createStatement(session).list();

        // модуль должен принадлежать одной части, если это не так - не обрабатываем модуль
        // аналитики пока не знают, что делать с такими модулями
        if (rowsModulePart.size() > 1) {
            System.out.println("Более 1-го вхождения");
            return;
        }

        // цикл лишний - модуль всегда принадлежит одной части
        for (EppRegistryElementPartModule modulePart : rowsModulePart) {
            // часть дисциплины
            EppRegistryElementPart regPart = modulePart.getPart();

            EppRegistryElement reg = regPart.getRegistryElement();
            Long eppRegElementId = reg.getId();

            if (!map.containsKey(eppRegElementId)) {
                // по дисциплине смотрим все версии УПов
                // найдем все версии УП, где есть эта дисциплина
                final Set<Long> versionIds = new HashSet<Long>();


                // сменили сущности - нехорошие клоуны
                    /*
			        final DQLSelectBuilder discRowDQL = new DQLSelectBuilder()
			        .fromEntity(EppEpvDisciplineBaseRow.class, "row")
			        .addColumn(DQLExpressions.property(EppEpvDisciplineBaseRow.owner().eduPlanVersion().id().fromAlias("row")))
			        .where(DQLExpressions.eq(DQLExpressions.property(EppEpvDisciplineBaseRow.registryElement().id().fromAlias("row")), DQLExpressions.value(eppRegElementId)));
			        versionIds.addAll(discRowDQL.createStatement(session).<Long>list());

			        final DQLSelectBuilder actionRowDQL = new DQLSelectBuilder()
			        .fromEntity(EppEpvActionRow.class, "row")
			        .addColumn(DQLExpressions.property(EppEpvActionRow.owner().eduPlanVersion().id().fromAlias("row")))
			        .where(DQLExpressions.eq(DQLExpressions.property(EppEpvActionRow.registryElement().id().fromAlias("row")), DQLExpressions.value(eppRegElementId)));
			        versionIds.addAll(actionRowDQL.createStatement(session).<Long>list());
			         */

                if (true) {
                    throw new UnsupportedOperationException("Refactor this if need. Entity EppEpvTemplateRow is dropped from product.");
                /*final DQLSelectBuilder discRowDQL = new DQLSelectBuilder()
                        .fromEntity(EppEpvTemplateRow.class, "row")
                        .addColumn(DQLExpressions.property(EppEpvRegistryRow.owner().eduPlanVersion().id().fromAlias("row")))
                        .where(DQLExpressions.eq(DQLExpressions.property(EppEpvRegistryRow.registryElement().id().fromAlias("row")), DQLExpressions.value(eppRegElementId)));
                versionIds.addAll(discRowDQL.createStatement(session).<Long>list());*/
                }


                // получим строки этих версий в готовом виде
                final Map<Long, IEppEpvBlockWrapper> eduPlanVersionDataMap = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockDataMap(versionIds, true);

                // у нас модули сортированы по нумерам
                // при добавлении в кеш нового, все старые можно уничтожать (иначе теряем скорость и течет память)
                map.clear();

                map.put(eppRegElementId, eduPlanVersionDataMap);
            }
            else {
                System.out.println("Есть сведения в кеше");
            }

            Map<Long, IEppEpvBlockWrapper> eduPlanVersionDataMap = map.get(eppRegElementId);

            // получим версию, в которой задействовано больше всего часов
            IEppEpvBlockWrapper maxVersion = _getMaxVersion(eppRegElementId, eduPlanVersionDataMap);

            // мы знаем номер части дисциплины
            int partNumber = regPart.getNumber();


            if (maxVersion != null) {
                System.out.println(maxVersion.getVersion().getTitle());

                boolean _has = false;

                // идем по читаемым семестрам и находим что нам надо:
                for (final IEppEpvRowWrapper rowWrapper : maxVersion.getRowMap().values()) {

                    // проверяем, что у нас eppEpvRegistryRow
                    if (!this.checkDisc(eppRegElementId, rowWrapper))
                        continue;

                    final List<Integer> activeTermSet = new ArrayList<Integer>(rowWrapper.getActiveTermSet());
                    Collections.sort(activeTermSet);

                    int part = 1;
                    for (final Integer term : activeTermSet) {
                        if (part == partNumber) {
                            // можно анализировать
                            _has = true;
                            _doAction(term, eppRegElementId, reg, regPart, rowWrapper, module, session);
                        }
                        part++;
                    }
                }

                if (!_has) {
                    // нет распределения в соответствующей части
                    System.out.println("Нет распределения в соотв. части");
                    _setModuleLabel(module, session);
                    session.update(module);
                    session.flush();
                }
            }
            else {
                // нет версии УП для данного модуля
                System.out.println("Нет версии УПа");
                _setModuleLabel(module, session);
                session.update(module);
                session.flush();
            }
        }


        if (mapShangeState.values().size() != 0) {
            for (StateChange st : mapShangeState.values()) {
                EppRegistryElement reg = st.getReg();
                reg.setProperty("state", getCatalogItem(EppState.class, st.getOldState()));
                session.update(reg);
                session.flush();
            }
        }

        session.clear();

    }

    private void _setModuleLabel
            (
                    EppRegistryModule module
                    , Session session
            )
    {

        if (module.getTitle().indexOf("#") == -1) {
            String _title = module.getTitle();
            _title += " #";
            module.setTitle(_title);
        }
    }

    /**
     * Прописываем в модуль и часть дисциплины нужные нам данные
     *
     * @param term
     * @param eppRegElementId
     * @param reg
     * @param regPart
     * @param rowWrapper
     */
    private void _doAction
    (
            Integer term
            , Long eppRegElementId
            , EppRegistryElement reg
            , EppRegistryElementPart regPart
            , IEppEpvRowWrapper rowWrapper
            , EppRegistryModule module
            , Session session
    )
    {
        // установим надпись на модуле
        _setModuleLabel(module, session);

        // перенос часов по видам нагрузки
        _copyTermLoad(term, module, rowWrapper, session);

        session.update(module);
        session.flush();

        _doDisciplinePart(term, reg, regPart, rowWrapper, module, session);
    }

    private void _doDisciplinePart(Integer term,
                                   EppRegistryElement reg,
                                   EppRegistryElementPart regPart, IEppEpvRowWrapper wrapper,
                                   EppRegistryModule module
            , Session session
    )
    {

        String old_state = "";
        if (!reg.getState().getCode().equals("1")) {
            old_state = reg.getState().getCode();

            reg.setProperty("state", getCatalogItem(EppState.class, "1"));
            session.update(reg);
            session.flush();


            if (!mapShangeState.containsKey(reg.getId())) {
                StateChange value = new StateChange();
                value.setOldState(old_state);
                value.setReg(reg);
                mapShangeState.put(reg.getId(), value);
            }
        }

        List<EppControlActionType> caList = new ArrayList<EppControlActionType>();

        // 1 - проставим формы контроля
        for (final EppControlActionType controlActionType : controlActionTypes) {
            final int actionSize = wrapper.getActionSize(term, controlActionType.getFullCode());
            if (actionSize == 0)
                continue;

            caList.add(controlActionType);

            DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(EppRegistryElementPartFControlAction.class, "caction")
                    .addColumn("caction")
                            // только от нужного модуля
                    .where(eq(property(EppRegistryElementPartFControlAction.controlAction().id().fromAlias("caction")), value(controlActionType.getId())))
                    .where(eq(property(EppRegistryElementPartFControlAction.part().fromAlias("caction")), value(regPart)));
            List<EppRegistryElementPartFControlAction> rows = dql.createStatement(session).list();

            if (rows.size() == 0) {
                EppFControlActionType controlAction = getCatalogItem(EppFControlActionType.class, controlActionType.getCode());

                if (controlAction != null) {
                    // контрольное мероприятие нужно добавить
                    EppRegistryElementPartFControlAction caction = new EppRegistryElementPartFControlAction();
                    caction.setPart(regPart);
                    caction.setControlAction(controlAction);

                    session.save(caction);
                    session.flush();


                }

            }
        }

        // 2 - просмотрим все контрольные мероприятия части - и если там есть лишнее, удалим
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppRegistryElementPartFControlAction.class, "caction")
                .addColumn("caction")
                .where(eq(property(EppRegistryElementPartFControlAction.part().fromAlias("caction")), value(regPart)));

        List<EppRegistryElementPartFControlAction> rows = dql.createStatement(session).list();

        for (EppRegistryElementPartFControlAction ca : rows) {
            boolean _drop = true;

            for (EppControlActionType ct : caList) {
                if (ct.getCode().equals(ca.getControlAction().getCode()))
                    _drop = false;
            }

            if (_drop) {
                session.delete(ca);
                session.flush();
            }
        }

        // из Упа читаем часы по видам нагрузки
        double audit = wrapper.getTotalInTermLoad(term, EppELoadType.FULL_CODE_AUDIT);
        double selfwork = wrapper.getTotalInTermLoad(term, EppELoadType.FULL_CODE_SELFWORK);

        double _totalSelfWorkH = _getTotalSelfWork(reg, session);
        DisciplineInfo di = mapDI.get(reg.getId());


        if (selfwork == 0) {
            // для каждой из частей нужно знать аудиторную нагрузку
            // не распределено
            // ищем самостоятельную из нагрузки на саму дисциплину
            if (_totalSelfWorkH > 0) {
                // самостоят часы - пропорционально аудиторным
                double _partLoad = Math.floor(di.getSalfeWorkTotal() * audit / di.getAuditWorkTotal());

                // в крайних частях нужно учесть возможное несовпадение по итоговым часам
                selfwork = di.AddSelfWorkPart(regPart.getNumber(), _partLoad);
            }
            else {
                selfwork = di.AddSelfWorkPart(regPart.getNumber(), 0);
            }
        }
        else
            selfwork = mapDI.get(reg.getId()).AddSelfWorkPart(regPart.getNumber(), selfwork);


        // нужно проверить - а можно-ли редактировать часть
        try {
            // все часы расчитаны
            regPart.setSizeAsDouble(audit + selfwork);
            session.update(regPart);
            session.flush();

        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

    private double _getTotalSelfWork(EppRegistryElement reg, Session session)
    {


        Long _id = reg.getId();
        DisciplineInfo di = null;

        if (mapDI.containsKey(_id))
            di = mapDI.get(_id);
        else {
            di = new DisciplineInfo(_id, reg.getParts());
            mapDI.put(_id, di);
        }

        if (di.getSalfeWorkTotal() == 0) {
            boolean _hasRow = false;
            EppRegistryElementLoad salfeWorkRow = null;

            // ищем самостоятельную из нагрузки на саму дисциплину
            DQLSelectBuilder dqlD = new DQLSelectBuilder()
                    .fromEntity(EppRegistryElementLoad.class, "load")
                    .addColumn("load")
                            // только от нужного модуля
                    .where(eq(property(EppRegistryElementLoad.registryElement().fromAlias("load")), value(reg)))
                    .where(eq(property(EppRegistryElementLoad.loadType().code().fromAlias("load")), value("2")))
                    .where(eq(property(EppRegistryElementLoad.loadType().catalogCode().fromAlias("load")), value("eppELoadType")));
            List<EppRegistryElementLoad> rowsLoad = dqlD.createStatement(session).list();


            if (rowsLoad.size() == 1) {
                _hasRow = true;
                salfeWorkRow = rowsLoad.get(0);

                // можно расчитать
                EppRegistryElementLoad _dTotal = rowsLoad.get(0);
                di.setSalfeWorkTotal(_dTotal.getLoadAsDouble());
            }

            // считаем аудиторные часы
            // из итоговой отнимаем аудиторную, код=1
            DQLSelectBuilder dqlDAudit = new DQLSelectBuilder()
                    .fromEntity(EppRegistryElementLoad.class, "load")
                    .addColumn("load")
                            // только от нужного модуля
                    .where(eq(property(EppRegistryElementLoad.registryElement().fromAlias("load")), value(reg)))
                    .where(eq(property(EppRegistryElementLoad.loadType().code().fromAlias("load")), value("1")))
                    .where(eq(property(EppRegistryElementLoad.loadType().catalogCode().fromAlias("load")), value("eppELoadType")));
            List<EppRegistryElementLoad> rowsLoadAudit = dqlDAudit.createStatement(session).list();

            if (rowsLoadAudit.size() == 1) {
                // можно расчитать
                EppRegistryElementLoad _dTotal = rowsLoadAudit.get(0);
                di.setAuditWorkTotal(_dTotal.getLoadAsDouble());

                if (di.getSalfeWorkTotal() == 0) {
                    di.setSalfeWorkTotal(reg.getSizeAsDouble() - _dTotal.getLoadAsDouble());

                    // у дисциплины добавляем самостоятельные часы
                    if (di.getSalfeWorkTotal() > 0) {
                        if (_hasRow) {
                            salfeWorkRow.setLoadAsDouble(di.getSalfeWorkTotal());
                            session.update(salfeWorkRow);
                            session.flush();
                        }
                        else {
                            // назначим часы
                            EppRegistryElementLoad dload = new EppRegistryElementLoad();
                            dload.setRegistryElement(reg);
                            dload.setLoadAsDouble(di.getSalfeWorkTotal());
                            EppELoadType loadType = getByCode(EppELoadType.class, "2");

                            dload.setLoadType(loadType);
                            session.save(dload);
                            session.flush();
                        }
                    }

                }
            }
        }

        return di.getSalfeWorkTotal();
    }


    private void _copyTermLoad(Integer term, EppRegistryModule module,
                               IEppEpvRowWrapper wrapper, Session session)
    {

        // из Упа читаем все виды нагрузки
        double lecture = wrapper.getTotalInTermLoad(term, EppALoadType.FULL_CODE_TOTAL_LECTURES);
        double practize = wrapper.getTotalInTermLoad(term, EppALoadType.FULL_CODE_TOTAL_PRACTICE);
        double labs = wrapper.getTotalInTermLoad(term, EppALoadType.FULL_CODE_TOTAL_LABS);

        _setLoad(EppALoadType.TYPE_LECTURES, term, module, wrapper, lecture, session);
        _setLoad(EppALoadType.TYPE_LABS, term, module, wrapper, labs, session);
        _setLoad(EppALoadType.TYPE_PRACTICE, term, module, wrapper, practize, session);


    }

    private void _setLoad(String typeLoad, Integer term,
                          EppRegistryModule module, IEppEpvRowWrapper wrapper, double load, Session session)
    {
        // получим часы нагрузок из модуля
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppRegistryModuleALoad.class, "mlt")
                .addColumn("mlt")
                        // только от нужного модуля
                .where(eq(property(EppRegistryModuleALoad.module().fromAlias("mlt")), value(module)))
                .where(eq(property(EppRegistryModuleALoad.loadType().code().fromAlias("mlt")), value(typeLoad)));
        List<EppRegistryModuleALoad> rows = dql.createStatement(session).list();

        if (rows.size() > 0) {
            for (EppRegistryModuleALoad row : rows) {
                row.setLoadAsDouble(load);
                session.update(row);
            }
        }
        else {
            EppRegistryModuleALoad oload = new EppRegistryModuleALoad();
            EppALoadType loadType = getByCode(EppALoadType.class, typeLoad);

            oload.setLoadType(loadType);
            oload.setModule(module);
            oload.setLoadAsDouble(load);

            session.save(oload);
        }

    }

    private IEppEpvBlockWrapper _getMaxVersion
            (
                    Long eppRegElementId
                    , Map<Long, IEppEpvBlockWrapper> eduPlanVersionDataMap
            )
    {
        IEppEpvBlockWrapper retVal = null;
        double _maxLoad = 0;
        for (final IEppEpvBlockWrapper epvWrapper : eduPlanVersionDataMap.values()) {
            double _totalLoad = 0;
            for (final IEppEpvRowWrapper rowWrapper : epvWrapper.getRowMap().values()) {
                if (!this.checkDisc(eppRegElementId, rowWrapper)) {
                    continue;
                }

                final List<Integer> activeTermSet = new ArrayList<Integer>(rowWrapper.getActiveTermSet());
                Collections.sort(activeTermSet);
                for (final Integer term : activeTermSet) {
                    _totalLoad += rowWrapper.getTotalInTermLoad(term, EppELoadType.FULL_CODE_AUDIT);
                }
            }
            if (_totalLoad >= _maxLoad) {
                _maxLoad = _totalLoad;
                retVal = epvWrapper;
            }
        }
        return retVal;
    }

    private boolean checkDisc(final Long eppRegElementId, final IEppEpvRowWrapper rowWrapper)
    {
        final IEppEpvRow row = rowWrapper.getRow();
        Long discId = null;
	        /*
	        if (row instanceof EppEpvDisciplineBaseRow) {
	            discId = ((EppEpvDisciplineBaseRow)row).getRegistryElement().getId();
	            
	        } else if (row instanceof EppEpvActionRow) {
	            discId = ((EppEpvActionRow)row).getRegistryElement().getId();
	        }
	        */
        if (row instanceof EppEpvRegistryRow) {
            discId = ((EppEpvRegistryRow) row).getRegistryElement().getId();
        }
        return eppRegElementId.equals(discId);
    }

    /**
     * Возвращает список модулей, которые нам нужно обрабатывать
     *
     * @return
     */
    private List<EppRegistryModule> _getModuleList()
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppRegistryModule.class, "row")
                .order(property(EppRegistryModule.number().fromAlias("row")))
                .addColumn("row");
        final Session session = getSession();

        List<EppRegistryModule> rows = dql.createStatement(session).list();
        session.clear();

        return rows;
    }


}
