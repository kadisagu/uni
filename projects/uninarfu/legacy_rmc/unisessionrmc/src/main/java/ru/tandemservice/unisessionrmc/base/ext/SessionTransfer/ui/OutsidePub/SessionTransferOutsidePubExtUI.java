package ru.tandemservice.unisessionrmc.base.ext.SessionTransfer.ui.OutsidePub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import ru.tandemservice.unisessionrmc.base.ext.SessionTransfer.ui.OutsideAddEdit.SessionTransferOutOpWrapperExt;
import ru.tandemservice.unisessionrmc.entity.SessionTransferOutsideOperationExt;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.OutsidePub.SessionTransferOutsidePubUI;
import ru.tandemservice.unisession.entity.document.SessionTransferOutsideDocument;
import ru.tandemservice.unisession.entity.document.SessionTransferOutsideOperation;

import java.util.ArrayList;
import java.util.List;

public class SessionTransferOutsidePubExtUI extends UIAddon {

    public SessionTransferOutsidePubExtUI(IUIPresenter presenter, String name,
                                          String componentId)
    {
        super(presenter, name, componentId);
    }

    private SessionTransferOutsidePubUI parentUI = getPresenter();

    private SessionTransferOutOpWrapperExt currentRow;
    private List<SessionTransferOutOpWrapperExt> rowList;

    @Override
    public void onComponentRefresh() {
        parentUI.setDocument(IUniBaseDao.instance.get().get(SessionTransferOutsideDocument.class, parentUI.getDocument().getId()));
        setRowList(new ArrayList<>());
        List<SessionTransferOutsideOperation> operations = IUniBaseDao.instance.get().getList(SessionTransferOutsideOperation.class,
                                                                                              SessionTransferOutsideOperation.targetMark().slot().document().s(), parentUI.getDocument(),
                                                                                              SessionTransferOutsideOperation.targetMark().slot().studentWpeCAction().studentWpe().year().educationYear().intValue().s(),
                                                                                              SessionTransferOutsideOperation.targetMark().slot().studentWpeCAction().studentWpe().term().intValue().s(),
                                                                                              SessionTransferOutsideOperation.targetMark().slot().studentWpeCAction().studentWpe().registryElementPart().registryElement().title().s(),
                                                                                              SessionTransferOutsideOperation.targetMark().slot().studentWpeCAction().type().priority().s()
        );
        for (SessionTransferOutsideOperation operation : operations)
            getRowList().add(new SessionTransferOutOpWrapperExt(getOperationExt(operation)));
    }


    public SessionTransferOutsideOperationExt getOperationExt(SessionTransferOutsideOperation operation) {

        SessionTransferOutsideOperationExt operationExt = UniDaoFacade.getCoreDao().get(SessionTransferOutsideOperationExt.class, SessionTransferOutsideOperationExt.sessionTransferOutsideOperation(), operation);
        if (operationExt != null)
            return operationExt;
        else {
            operationExt = new SessionTransferOutsideOperationExt();
            operationExt.setSessionTransferOutsideOperation(operation);
            return operationExt;
        }

    }

    public SessionTransferOutOpWrapperExt getCurrentRow() {
        return currentRow;
    }

    public void setCurrentRow(SessionTransferOutOpWrapperExt currentRow) {
        this.currentRow = currentRow;
    }

    public List<SessionTransferOutOpWrapperExt> getRowList() {
        return rowList;
    }

    public void setRowList(List<SessionTransferOutOpWrapperExt> rowList) {
        this.rowList = rowList;
    }


}
