package ru.tandemservice.unisessionrmc.component.settings.Perezachtenie;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void processInNewTransaction(MarkStudentDisciplineInfo mi) throws Exception;

    void doAction(Model model);

}
