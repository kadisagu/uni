package ru.tandemservice.unisessionrmc.component.settings.DisciplineModuleAddControlAction;

import org.hibernate.Session;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.entity.catalog.EppIControlActionType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModule;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModuleIControlAction;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class RMCDisciplineAddCA extends UniBaseDao implements IRMCDisciplineAddCA {

    private EppIControlActionType controlAction = null;

    @Override
    public void processModuleInNewTransaction(EppRegistryModule row, Model model)
    {
        _processModule(row, model);
    }

    private void _processModule(EppRegistryModule row, Model model)
    {

        if (!model.getProcessAllModule() && row.getTitle().indexOf("#") == -1)
            return;

        final Session session = getSession();

        // 1 - то типу КМ все отобрано заранее,
        // так что можно сходу добавить

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppRegistryModuleIControlAction.class, "caction")
                .addColumn("caction")
                        // только от нужного модуля
                .where(eq(property(EppRegistryModuleIControlAction.controlAction().id().fromAlias("caction")), value(controlAction.getId())))
                .where(eq(property(EppRegistryModuleIControlAction.module().fromAlias("caction")), value(row)));

        List<EppRegistryModuleIControlAction> rows = dql.createStatement(session).list();

        EppRegistryModuleIControlAction newrow = null;

        if (rows.size() > 0) {
            newrow = rows.get(0);
            newrow.setAmount(3);
            session.update(newrow);
        }
        else {
            newrow = new EppRegistryModuleIControlAction();
            newrow.setControlAction(controlAction);
            newrow.setModule(row);
            newrow.setAmount(3);
            session.save(newrow);
        }

        session.flush();
        session.clear();

    }

    @Override
    public void process(Model model) throws Exception
    {

        controlAction = getByCode(EppIControlActionType.class, "12");

        if (controlAction == null)
            throw new Exception("Нет в справочнике контрольного мероприятия с кодом 13");


        // 1 - получим полный список всех модулей
        List<EppRegistryModule> rows = _getModuleList();

        model.setRows(rows);

    }

    private List<EppRegistryModule> _getModuleList()
    {


        //1 - найдем все модули, где есть нужное контрольное мероприятие
        final DQLSelectBuilder s = new DQLSelectBuilder();
        s.fromEntity(EppRegistryModuleIControlAction.class, "b");
        s.addColumn(DQLExpressions.property(EppRegistryModuleIControlAction.module().id().fromAlias("b")));
        s.where(eq(property(EppRegistryModuleIControlAction.controlAction().fromAlias("b")), value(controlAction)));


        //2 - только модули без
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppRegistryModule.class, "row")
                        // notIn
                .where(DQLExpressions.notIn(DQLExpressions.property("row", "id"), s.getQuery()))
                .order(property(EppRegistryModule.number().fromAlias("row")))
                .addColumn("row");
        final Session session = getSession();

        List<EppRegistryModule> rows = dql.createStatement(session).list();
        session.clear();

        return rows;

    }

}
