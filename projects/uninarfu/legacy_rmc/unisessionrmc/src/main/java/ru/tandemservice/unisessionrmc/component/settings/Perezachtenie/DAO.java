package ru.tandemservice.unisessionrmc.component.settings.Perezachtenie;

import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionMark.SessionMarkManager;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.ISessionMarkDAO.MarkData;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.*;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DAO extends UniDao<Model> implements IDAO {
    @Override
    public void prepare(Model model)
    {
        EducationYear ec = get(EducationYear.class, EducationYear.current().s(), Boolean.TRUE);
        model.setEducationYear(ec);
    }

    @Override
    public void doAction(Model model)
    {
        Map<String, MarkStudentDisciplineInfo> map = new HashMap<String, MarkStudentDisciplineInfo>();
        Session session = getSession();

        List<Object[]> lst = getPerezachtenieAndBullitenDQL(model).createStatement(session).<Object[]>list();
        for (Object[] objs : lst) {
            if (objs[0] == null || objs[1] == null || objs[2] == null) {
                if (objs[0] != null)
                    System.out.println("StId = " + objs[0]);

                continue;
            }

            Long idStudent = (Long) objs[0];
            Long idDiscipline = (Long) objs[1];
            int idPart = (Integer) objs[2];
            Date performDate = null;

            Long idSdSlot = null;

            Long idtransfer = null;
            if (objs[3] != null) {
                idtransfer = (Long) objs[3];

                if (objs[9] != null)
                    performDate = (Date) objs[9];
            }

            Long idbulletin = null;
            if (objs[4] != null) {
                idbulletin = (Long) objs[4];
                if (objs[7] != null)
                    idSdSlot = (Long) objs[7];
            }

            Long idgradebook = null;
            if (objs[5] != null)
                idgradebook = (Long) objs[5];

            Long smId = null;
            if (objs[6] != null)
                smId = (Long) objs[6];

            // есть итоговая оценка
            if (smId != null) {
                boolean inSession = false;
                if (objs[8] != null)
                    inSession = (Boolean) objs[8];

                if (!inSession)
                    idgradebook = null;
            }

            String key = "";

            //try
            //{
            key = Long.toString(idStudent) + Long.toString(idDiscipline) + Integer.toString(idPart);
            //}
            //catch (Exception ex)
            /*
				{
				System.out.print(ex.getMessage());
				throw ex;
			}
			*/
            MarkStudentDisciplineInfo mi = null;

            if (map.containsKey(key))
                mi = map.get(key);
            else {
                mi = new MarkStudentDisciplineInfo();
                map.put(key, mi);
            }

            mi.setIdStudent(idStudent);
            mi.setDisciplineId(idDiscipline);
            mi.setDisciplinePart(idPart);

            mi.setInfo(idtransfer, idbulletin, idgradebook, idSdSlot, smId, performDate);


        }

        for (MarkStudentDisciplineInfo mi : map.values()) {
            if (mi.needSetMark()) {
                processInNewTransaction(mi);
            }
        }
    }

    private DQLSelectBuilder getPerezachtenieAndBullitenDQL(Model model)
    {

        final DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppStudentWpeCAction.class, "eppSlot")
                        // .addColumn("eppSlot")
                        //.addColumn(property(EppStudentWpeCAction.id().fromAlias("eppSlot")))

                .joinPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().student().fromAlias("eppSlot"), "student")

                        // выборка самого студента
                        // .where(in(property(EppStudentWpeCAction.slot().student().student().fromAlias("eppSlot")), dqlStudent.getQuery()))

                        // только актуальные мероприятия
                .where(isNull(EppStudentWpeCAction.removalDate().fromAlias("eppSlot")))

                        // год
                .where(DQLExpressions.eq(DQLExpressions.property(EppStudentWpeCAction.studentWpe().year().educationYear().fromAlias("eppSlot")), DQLExpressions.value(model.getEducationYear())))

                        // прицепим все комбинации оценок
                .joinEntity("eppSlot", DQLJoinType.left, SessionDocumentSlot.class, "sdslot", DQLExpressions.eq
                                    (
                                            DQLExpressions.property("sdslot." + SessionDocumentSlot.L_STUDENT_WPE_C_ACTION),
                                            DQLExpressions.property("eppSlot.id")
                                    )
                )


                .joinEntity("sdslot", DQLJoinType.left, SessionDocument.class, "sdoc", DQLExpressions.eq
                                    (
                                            DQLExpressions.property("sdslot." + SessionDocumentSlot.L_DOCUMENT),
                                            DQLExpressions.property("sdoc.id")
                                    )
                )


                .joinEntity("sdslot", DQLJoinType.left, SessionMark.class, "mark", DQLExpressions.eq
                                    (
                                            DQLExpressions.property("mark." + SessionMark.L_SLOT),
                                            DQLExpressions.property("sdslot.id")
                                    )
                )

                        // прицепим документы (ведомость + перезачтение) sdslot
                .joinEntity("sdslot", DQLJoinType.left, SessionBulletinDocument.class, "bulletin", DQLExpressions.eq
                                    (
                                            DQLExpressions.property("bulletin.id"),
                                            DQLExpressions.property("sdoc.id")
                                    )
                )
                .joinEntity("sdslot", DQLJoinType.left, SessionTransferDocument.class, "transfer", DQLExpressions.eq
                                    (
                                            DQLExpressions.property("transfer.id"),
                                            DQLExpressions.property("sdoc.id")
                                    )
                )

                        // зачетная книжка SessionStudentGradeBookDocument
                .joinEntity("sdslot", DQLJoinType.left, SessionStudentGradeBookDocument.class, "gradebook", DQLExpressions.eq
                                    (
                                            DQLExpressions.property("gradebook.id"),
                                            DQLExpressions.property("sdoc.id")
                                    )
                )

                        // цепляем дисциплину реестра
                .joinPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().registryElementPart().registryElement().fromAlias("eppSlot"), "regelem")
                .joinEntity("regelem", DQLJoinType.left, EppRegistryDiscipline.class, "regdiscipline", DQLExpressions.eq
                                    (
                                            DQLExpressions.property("regdiscipline.id"),
                                            DQLExpressions.property("regelem.id")
                                    )
                )

                        // собираем признаки - есть перезачтение
                        // id студента
                .addColumn(property(EppStudentWpeCAction.studentWpe().student().id().fromAlias("eppSlot")))
                        // данные по дисциплине реестра (id дисциплтны)
                .addColumn(property(EppRegistryDiscipline.id().fromAlias("regdiscipline")))
                        // часть дисциплины
                .addColumn(property(EppStudentWpeCAction.studentWpe().registryElementPart().number().fromAlias("eppSlot")))


                        // документ перезачтения
                .addColumn(property(SessionTransferDocument.id().fromAlias("transfer")))
                        // документ - ведомость
                .addColumn(property(SessionBulletinDocument.id().fromAlias("bulletin")))
                        //
                .addColumn(property(SessionStudentGradeBookDocument.id().fromAlias("gradebook")))
                        // наличие итоговой оценки
                        //.addColumn("mark")
                .addColumn(property(SessionMark.id().fromAlias("mark")))
                .addColumn(property(SessionDocumentSlot.id().fromAlias("sdslot")))
                        // признак - итоговая оценка в сессию
                .addColumn(property(SessionDocumentSlot.inSession().fromAlias("sdslot")))
                        // дата получения оценки
                .addColumn(property(SessionMark.performDate().fromAlias("mark")));


        return dql;
    }

    @Override
    public void processInNewTransaction(MarkStudentDisciplineInfo mi)
    {
        Session session = getSession();

        // поставим оценку
        SessionBulletinDocument bulliten = get(SessionBulletinDocument.class, mi.getBulletinId());

        if (!bulliten.isClosed() && bulliten.isEditable()) {
            if (mi.getIdSdSlot() != null && bulliten.getCommission() != null) {
                // проверим наличие pps в комиссии

                final DQLSelectBuilder tutorDQL = new DQLSelectBuilder()
                        .fromEntity(SessionComissionPps.class, "rel")
                        .addColumn("rel")
                        .joinPath(DQLJoinType.inner, SessionComissionPps.commission().fromAlias("rel"), "comm")
                        .where(eq(property(SessionComission.id().fromAlias("comm")), DQLExpressions.value(bulliten.getCommission())));
                List<SessionComissionPps> lstPps = tutorDQL.createStatement(session).<SessionComissionPps>list();
                ;


                if (lstPps.size() != 0) {
                    SessionMarkCatalogItem mark = getByCode(SessionMarkCatalogItem.class, "state.4");
                    SessionDocumentSlot slot = get(SessionDocumentSlot.class, mi.getIdSdSlot());

                    SessionDocument sd = slot.getDocument();

                    Date _pdate = new Date();
                    if (mi.getPerformDate() != null)
                        _pdate = mi.getPerformDate();

                    if (sd instanceof SessionBulletinDocument) {
                        SessionBulletinDocument sb = (SessionBulletinDocument) sd;

                        if (sb.getPerformDate() != null)
                            _pdate = sb.getPerformDate();
                    }

                    try {
                        // можно ставить оценку
                        MarkData md = getMark(mark, _pdate);
                        SessionMarkManager.instance().regularMarkDao().saveOrUpdateMark(slot, md);
                        session.flush();
                    }
                    catch (Exception ex) {
                        throw new ApplicationException("Ошибка док: " + bulliten.getTitle() + " " + slot.getActualStudent().getPerson().getFio() + " " + ex.getMessage());
                    }
                }
            }
        }
    }

    private MarkData getMark(SessionMarkCatalogItem mark, Date pdate)
    {
        final SessionMarkCatalogItem mi = mark;
        final Date pd = pdate;

        return new MarkData() {

            @Override
            public Double getPoints()
            {
                return null;
            }

            @Override
            public Date getPerformDate()
            {
                return pd;
            }

            @Override
            public SessionMarkCatalogItem getMarkValue()
            {
                return mi;
            }

            @Override
            public String getComment()
            {
                return null;
            }
        };
    }
}
