package ru.tandemservice.unisessionrmc;

public interface IRamecUniSessionRmcComponents {
    // ru.tandemservice.unisessionrmc.IRamecUniSessionRmcComponents
    String UNIDS_SYSTEM_ACTION_DISCIPLINE_MODULE = "ru.tandemservice.unisessionrmc.component.settings.DisciplineModule";
    String UNIDS_SYSTEM_ACTION_DISCIPLINE_MODULE_ADD_CONTROL_ACTION = "ru.tandemservice.unisessionrmc.component.settings.DisciplineModuleAddControlAction";
    String UNIDS_SYSTEM_PEREZACHTENIE = "ru.tandemservice.unisessionrmc.component.settings.Perezachtenie";
}
