package ru.tandemservice.unisessionrmc.component.settings.DisciplineModule;

import java.util.HashMap;
import java.util.Map;

public class DisciplineInfo {

    private Long eppRegistryElementId = (long) 0;
    private int parts = 0;
    private double salfeWorkTotal = 0;
    private double auditWorkTotal = 0;

    private Map<Integer, Double> mapSalfwork = new HashMap<Integer, Double>();


    public DisciplineInfo(Long eppRegistryElementId, int parts)
    {
        setEppRegistryElementId(eppRegistryElementId);
        setParts(parts);
    }

    public Double AddSelfWorkPart(int part, double salfWork)
    {
        if (mapSalfwork.size() == (parts - 1)) {
            // добавлена крайняя часть
            // можно ничего не считать - крайняя часть - это из итого - отнять все предыд

            Double rw = salfeWorkTotal;
            for (Double d : mapSalfwork.values()) {
                rw -= d;
            }
            mapSalfwork.put(part, rw);
            return rw;
        }
        else {
            mapSalfwork.put(part, salfWork);
            return salfWork;
        }
    }

    public void setEppRegistryElementId(Long eppRegistryElementId) {
        this.eppRegistryElementId = eppRegistryElementId;
    }

    public Long getEppRegistryElementId() {
        return eppRegistryElementId;
    }

    public void setSalfeWorkTotal(double salfeWorkTotal) {
        this.salfeWorkTotal = salfeWorkTotal;
    }

    public double getSalfeWorkTotal() {
        return salfeWorkTotal;
    }

    public void setParts(int parts) {
        this.parts = parts;
    }

    public int getParts() {
        return parts;
    }

    public void setAuditWorkTotal(double auditWorkTotal) {
        this.auditWorkTotal = auditWorkTotal;
    }

    public double getAuditWorkTotal() {
        return auditWorkTotal;
    }


}
