package ru.tandemservice.unisessionrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.catalog.entity.EduInstitution;
import ru.tandemservice.unisession.entity.document.SessionTransferOutsideOperation;
import ru.tandemservice.unisessionrmc.entity.SessionTransferOutsideOperationExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение перезачтенного мероприятия из другого ОУ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionTransferOutsideOperationExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisessionrmc.entity.SessionTransferOutsideOperationExt";
    public static final String ENTITY_NAME = "sessionTransferOutsideOperationExt";
    public static final int VERSION_HASH = 844690613;
    private static IEntityMeta ENTITY_META;

    public static final String L_SESSION_TRANSFER_OUTSIDE_OPERATION = "sessionTransferOutsideOperation";
    public static final String L_EDU_INSTITUTION = "eduInstitution";

    private SessionTransferOutsideOperation _sessionTransferOutsideOperation;     // Перезачтенное мероприятие из другого ОУ
    private EduInstitution _eduInstitution;     // Образовательно учреждение

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Перезачтенное мероприятие из другого ОУ. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public SessionTransferOutsideOperation getSessionTransferOutsideOperation()
    {
        return _sessionTransferOutsideOperation;
    }

    /**
     * @param sessionTransferOutsideOperation Перезачтенное мероприятие из другого ОУ. Свойство не может быть null и должно быть уникальным.
     */
    public void setSessionTransferOutsideOperation(SessionTransferOutsideOperation sessionTransferOutsideOperation)
    {
        dirty(_sessionTransferOutsideOperation, sessionTransferOutsideOperation);
        _sessionTransferOutsideOperation = sessionTransferOutsideOperation;
    }

    /**
     * @return Образовательно учреждение.
     */
    public EduInstitution getEduInstitution()
    {
        return _eduInstitution;
    }

    /**
     * @param eduInstitution Образовательно учреждение.
     */
    public void setEduInstitution(EduInstitution eduInstitution)
    {
        dirty(_eduInstitution, eduInstitution);
        _eduInstitution = eduInstitution;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SessionTransferOutsideOperationExtGen)
        {
            setSessionTransferOutsideOperation(((SessionTransferOutsideOperationExt)another).getSessionTransferOutsideOperation());
            setEduInstitution(((SessionTransferOutsideOperationExt)another).getEduInstitution());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionTransferOutsideOperationExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionTransferOutsideOperationExt.class;
        }

        public T newInstance()
        {
            return (T) new SessionTransferOutsideOperationExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "sessionTransferOutsideOperation":
                    return obj.getSessionTransferOutsideOperation();
                case "eduInstitution":
                    return obj.getEduInstitution();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "sessionTransferOutsideOperation":
                    obj.setSessionTransferOutsideOperation((SessionTransferOutsideOperation) value);
                    return;
                case "eduInstitution":
                    obj.setEduInstitution((EduInstitution) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "sessionTransferOutsideOperation":
                        return true;
                case "eduInstitution":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "sessionTransferOutsideOperation":
                    return true;
                case "eduInstitution":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "sessionTransferOutsideOperation":
                    return SessionTransferOutsideOperation.class;
                case "eduInstitution":
                    return EduInstitution.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionTransferOutsideOperationExt> _dslPath = new Path<SessionTransferOutsideOperationExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionTransferOutsideOperationExt");
    }
            

    /**
     * @return Перезачтенное мероприятие из другого ОУ. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisessionrmc.entity.SessionTransferOutsideOperationExt#getSessionTransferOutsideOperation()
     */
    public static SessionTransferOutsideOperation.Path<SessionTransferOutsideOperation> sessionTransferOutsideOperation()
    {
        return _dslPath.sessionTransferOutsideOperation();
    }

    /**
     * @return Образовательно учреждение.
     * @see ru.tandemservice.unisessionrmc.entity.SessionTransferOutsideOperationExt#getEduInstitution()
     */
    public static EduInstitution.Path<EduInstitution> eduInstitution()
    {
        return _dslPath.eduInstitution();
    }

    public static class Path<E extends SessionTransferOutsideOperationExt> extends EntityPath<E>
    {
        private SessionTransferOutsideOperation.Path<SessionTransferOutsideOperation> _sessionTransferOutsideOperation;
        private EduInstitution.Path<EduInstitution> _eduInstitution;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Перезачтенное мероприятие из другого ОУ. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisessionrmc.entity.SessionTransferOutsideOperationExt#getSessionTransferOutsideOperation()
     */
        public SessionTransferOutsideOperation.Path<SessionTransferOutsideOperation> sessionTransferOutsideOperation()
        {
            if(_sessionTransferOutsideOperation == null )
                _sessionTransferOutsideOperation = new SessionTransferOutsideOperation.Path<SessionTransferOutsideOperation>(L_SESSION_TRANSFER_OUTSIDE_OPERATION, this);
            return _sessionTransferOutsideOperation;
        }

    /**
     * @return Образовательно учреждение.
     * @see ru.tandemservice.unisessionrmc.entity.SessionTransferOutsideOperationExt#getEduInstitution()
     */
        public EduInstitution.Path<EduInstitution> eduInstitution()
        {
            if(_eduInstitution == null )
                _eduInstitution = new EduInstitution.Path<EduInstitution>(L_EDU_INSTITUTION, this);
            return _eduInstitution;
        }

        public Class getEntityClass()
        {
            return SessionTransferOutsideOperationExt.class;
        }

        public String getEntityName()
        {
            return "sessionTransferOutsideOperationExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
