package ru.tandemservice.unisessionrmc.base.ext.SessionTransfer.logic;

import org.hibernate.Session;
import ru.tandemservice.unisessionrmc.base.ext.SessionTransfer.ui.OutsideAddEdit.SessionTransferOutOpWrapperExt;
import ru.tandemservice.unisessionrmc.entity.SessionTransferOutsideOperationExt;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unisession.base.bo.SessionMark.SessionMarkManager;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.ISessionMarkDAO;
import ru.tandemservice.unisession.base.bo.SessionTransfer.logic.ISessionTransferDao;
import ru.tandemservice.unisession.base.bo.SessionTransfer.logic.SessionTransferDao;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionTransferOperation;
import ru.tandemservice.unisession.entity.document.SessionTransferOutsideDocument;
import ru.tandemservice.unisession.entity.document.SessionTransferOutsideOperation;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionSlotMarkGradeValue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SessionTransferExtDao extends SessionTransferDao implements ISessionTransferDao {

    @Override
    public void saveOrUpdateOutsideTransfer(
            SessionTransferOutsideDocument document, final Date markDate,
            List<? extends ISessionTransferDao.SessionTransferOutsideOperationInfo> rowList)
    {

        Session session = getSession();

        if (document.getId() != null) {
            List<SessionDocumentSlot> slotList = new ArrayList<>();
            for (SessionTransferOperation operation : getList(SessionTransferOperation.class, SessionTransferOperation.targetMark().slot().document().s(), document, new String[0])) {
                delete(operation);
                slotList.add(operation.getTargetMark().getSlot());
            }
            session.flush();
            for (SessionDocumentSlot slot : slotList)
                delete(slot);
            session.flush();
        }

        saveOrUpdate(document);

        SessionComission comission = new SessionComission();
        save(comission);

        for (final ISessionTransferDao.SessionTransferOutsideOperationInfo item : rowList) {
            SessionDocumentSlot slot = new SessionDocumentSlot();
            slot.setDocument(document);
            slot.setStudentWpeCAction(item.getEppSlot());
            slot.setCommission(comission);
            slot.setInSession(true);
            save(slot);

            SessionMark mark = SessionMarkManager
                    .instance()
                    .regularMarkDao()
                    .saveOrUpdateMark(slot, new ISessionMarkDAO.MarkData() {

                        @Override
                        public Date getPerformDate() {
                            return markDate;
                        }

                        @Override
                        public Double getPoints() {
                            return item.getRating() != null ? item.getRating() : null;
                        }

                        @Override
                        public SessionMarkCatalogItem getMarkValue() {
                            return item.getMark();
                        }

                        @Override
                        public String getComment() {
                            return null;
                        }
                    });

            SessionTransferOutsideOperation operation = new SessionTransferOutsideOperation();
            operation.setTargetMark((SessionSlotMarkGradeValue) mark);
            operation.setDiscipline(item.getSourceDiscipline());
            operation.setControlAction(item.getSourceControlAction());
            operation.setMark(item.getSourceMark());
            operation.setComment(item.getComment());
            save(operation);

            SessionTransferOutsideOperationExt operationExt = getOperationExt(operation);
            if (operationExt != null) {
                operationExt.setEduInstitution(((SessionTransferOutOpWrapperExt) item).getEduInstitution());
            }
            else {
                operationExt = new SessionTransferOutsideOperationExt();
                operationExt.setSessionTransferOutsideOperation(operation);
                operationExt.setEduInstitution(((SessionTransferOutOpWrapperExt) item).getEduInstitution());
            }
            saveOrUpdate(operationExt);
        }

    }

    public SessionTransferOutsideOperationExt getOperationExt(SessionTransferOutsideOperation operation) {

        SessionTransferOutsideOperationExt operationExt = UniDaoFacade.getCoreDao().get(SessionTransferOutsideOperationExt.class, SessionTransferOutsideOperationExt.sessionTransferOutsideOperation(), operation);
        if (operationExt != null)
            return operationExt;
        else
            return null;
    }
}
