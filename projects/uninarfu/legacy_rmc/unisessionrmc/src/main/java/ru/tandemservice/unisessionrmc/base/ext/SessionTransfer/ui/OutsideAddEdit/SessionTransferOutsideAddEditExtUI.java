package ru.tandemservice.unisessionrmc.base.ext.SessionTransfer.ui.OutsideAddEdit;

import com.google.common.collect.ImmutableList;
import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unisessionrmc.entity.SessionTransferOutsideOperationExt;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisession.base.bo.SessionMark.SessionMarkManager;
import ru.tandemservice.unisession.base.bo.SessionTransfer.SessionTransferManager;
import ru.tandemservice.unisession.base.bo.SessionTransfer.logic.StudentByGroupOrgUnitComboDSHandler;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.OutsideAddEdit.SessionTransferOutsideAddEdit;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.OutsideAddEdit.SessionTransferOutsideAddEditUI;
import ru.tandemservice.unisession.entity.document.SessionTransferOutsideDocument;
import ru.tandemservice.unisession.entity.document.SessionTransferOutsideOperation;
import ru.tandemservice.unisession.util.selectModel.SessionTermModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SessionTransferOutsideAddEditExtUI extends UIAddon {

    public SessionTransferOutsideAddEditExtUI(IUIPresenter presenter,
                                              String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    private SessionTransferOutsideAddEditUI parentUI = getPresenter();

    private SessionTransferOutOpWrapperExt currentRow;
    private SessionTransferOutOpWrapperExt editedRow;
    private List<SessionTransferOutOpWrapperExt> rowList;

    @Override
    public void onComponentRefresh() {
        ICommonDAO dao = DataAccessServices.dao();

        parentUI.setStudent(parentUI.getStudentId() == null ? null : dao.<Student>getNotNull(parentUI.getStudentId()));
        parentUI.setDocument(parentUI.getDocumentId() == null ? null : dao.<SessionTransferOutsideDocument>getNotNull(parentUI.getDocumentId()));
        parentUI.setOrgUnit(parentUI.getOrgUnitId() == null ? null : dao.<OrgUnit>getNotNull(parentUI.getOrgUnitId()));
        setRowList(new ArrayList<>());

        setEditedRow(null);
        parentUI.setEditedRowIndex(null);
        parentUI.setRowAdded(false);

        if (isAddForm()) {
            // форма добавления внешнего перезачтения
            parentUI.setEditForm(false);
            parentUI.setDocument(new SessionTransferOutsideDocument());
            parentUI.getDocument().setFormingDate(new Date());

            if (parentUI.getStudentId() != null) {
                parentUI.getDocument().setTargetStudent(parentUI.getStudent());
                parentUI.getDocument().setOrgUnit(parentUI.getStudent().getEducationOrgUnit().getGroupOrgUnit());
            }
        }
        else {
            // форма редактирования внешнего перезачтения
            parentUI.setEditForm(true);
            parentUI.setStudent(parentUI.getDocument().getTargetStudent());
            List<SessionTransferOutsideOperation> operations = dao.getList(SessionTransferOutsideOperation.class,
                                                                           SessionTransferOutsideOperation.targetMark().slot().document(), parentUI.getDocument(),
                                                                           SessionTransferOutsideOperation.targetMark().slot().studentWpeCAction().studentWpe().year().educationYear().intValue().s(),
                                                                           SessionTransferOutsideOperation.targetMark().slot().studentWpeCAction().studentWpe().term().intValue().s(),
                                                                           SessionTransferOutsideOperation.targetMark().slot().studentWpeCAction().studentWpe().registryElementPart().registryElement().title().s(),
                                                                           SessionTransferOutsideOperation.targetMark().slot().studentWpeCAction().type().priority().s()
            );
            for (SessionTransferOutsideOperation operation : operations) {
                getRowList().add(new SessionTransferOutOpWrapperExt(getOperationExt(operation)));
                parentUI.setMarkDate(operation.getTargetMark().getPerformDate());
            }
        }

        if (parentUI.getStudent() != null)
            parentUI.setTermModel(SessionTermModel.createForStudent(parentUI.getStudent()));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        if (SessionTransferOutsideAddEdit.DS_EPP_SLOT.equals(dataSource.getName())) {
            dataSource.put(SessionTransferOutsideAddEdit.KEY_STUDENT, parentUI.getStudent());
            if (null != getEditedRow() && null != getEditedRow().getTerm()) {
                dataSource.put(SessionTransferOutsideAddEdit.KEY_EDU_YEAR, getEditedRow().getTerm().getYear());
                dataSource.put(SessionTransferOutsideAddEdit.KEY_YEAR_PART, getEditedRow().getTerm().getPart());
            }
        }
        else if (SessionTransferOutsideAddEdit.STUDENT_DS.equals(dataSource.getName())) {
            dataSource.put(StudentByGroupOrgUnitComboDSHandler.ORG_UNIT, parentUI.getOrgUnit());
        }
    }

    private boolean isAddForm() {
        return parentUI.getDocumentId() == null;
    }

    public SessionTransferOutsideOperationExt getOperationExt(SessionTransferOutsideOperation operation) {

        SessionTransferOutsideOperationExt operationExt = UniDaoFacade.getCoreDao().get(SessionTransferOutsideOperationExt.class, SessionTransferOutsideOperationExt.sessionTransferOutsideOperation(), operation);
        if (operationExt != null)
            return operationExt;
        else {
            operationExt = new SessionTransferOutsideOperationExt();
            operationExt.setSessionTransferOutsideOperation(operation);
            return operationExt;
        }

    }

    public void onSelectStudent()
    {
        // выбирать студента можно только на форме добавления и при не указанном студенте
        if (!parentUI.isNeedSelectStudent())
            throw new IllegalStateException("Student changing is not allowed.");

        parentUI.getDocument().setTargetStudent(parentUI.getStudent());
        parentUI.getDocument().setOrgUnit(parentUI.getStudent().getEducationOrgUnit().getGroupOrgUnit());
        parentUI.setTermModel(SessionTermModel.createForStudent(parentUI.getStudent()));
        setRowList(new ArrayList<>());
        setEditedRow(null);
        parentUI.setEditedRowIndex(null);
    }

    public void onClickApply()
    {
        if (parentUI.getStudent().getEducationOrgUnit().getGroupOrgUnit() == null) {
            throw new ApplicationException("Нельзя добавить документ о перезачтении студенту, т.к. для его направления подготовки не задан ответственный деканат, который необходим для сохранения документа о перезачтении. Укажите деканат в настройке «Ответственные подразделения за направления подготовки (специальности)».");
        }
        SessionTransferManager.instance().dao().saveOrUpdateOutsideTransfer(parentUI.getDocument(), parentUI.getMarkDate(), getRowList());
        parentUI.deactivate();
    }

    public void onClickAddRow()
    {
        if (getEditedRow() != null) return;
        final SessionTransferOutOpWrapperExt newRow = new SessionTransferOutOpWrapperExt();
        setEditedRow(newRow);

        getRowList().add(new SessionTransferOutOpWrapperExt());
        parentUI.setEditedRowIndex(getRowList().size() - 1);

        parentUI.setRowAdded(true);
    }

    public void onClickSaveRow()
    {
        if (parentUI.getEditedRowIndex() != null && getRowList().size() > parentUI.getEditedRowIndex() && parentUI.getEditedRowIndex() >= 0)
            getRowList().set(parentUI.getEditedRowIndex(), getEditedRow());
        setEditedRow(null);
        parentUI.setRowAdded(false);
    }

    public void onClickCancelEdit()
    {
        if (parentUI.isRowAdded() && !getRowList().isEmpty())
            getRowList().remove(getRowList().size() - 1);
        parentUI.setEditedRowIndex(null);
        setEditedRow(null);
        parentUI.setRowAdded(false);
    }

    public void onClickEditRow()
    {
        if (getEditedRow() != null) return;
        SessionTransferOutOpWrapperExt baseRow = findRow();
        setEditedRow(new SessionTransferOutOpWrapperExt(baseRow));
        parentUI.setEditedRowIndex(getRowList().indexOf(baseRow));
        prepareTargetMarkModel();
        parentUI.setRowAdded(false);
    }

    public void onClickDeleteRow()
    {
        SessionTransferOutOpWrapperExt row = findRow();
        if (null != row)
            getRowList().remove(row);
    }

    private SessionTransferOutOpWrapperExt findRow()
    {
        Integer id = getListenerParameter();
        SessionTransferOutOpWrapperExt row = null;
        for (SessionTransferOutOpWrapperExt r : getRowList())
            if (r.getId() == id)
                row = r;
        return row;
    }

    public boolean isEditMode()
    {
        return getEditedRow() != null;
    }

    public boolean isListEmpty()
    {
        return CollectionUtils.isEmpty(getRowList());
    }

    public boolean isCurrentRowInEditMode()
    {
        return getEditedRow() != null && parentUI.getEditedRowIndex().equals(getRowList().indexOf(getCurrentRow()));
    }

    public void prepareTargetMarkModel()
    {
        parentUI.setMarkModel(null == getEditedRow() || null == getEditedRow().getEppSlot() ?
                                      new LazySimpleSelectModel<>(ImmutableList.of()) :
                                      SessionMarkManager.instance().regularMarkDao().prepareSimpleMarkModel(getEditedRow().getEppSlot(), null, true)
        );
    }

    public SessionTransferOutOpWrapperExt getCurrentRow() {
        return currentRow;
    }

    public void setCurrentRow(SessionTransferOutOpWrapperExt currentRow) {
        this.currentRow = currentRow;
    }

    public SessionTransferOutOpWrapperExt getEditedRow() {
        return editedRow;
    }

    public void setEditedRow(SessionTransferOutOpWrapperExt editedRow) {
        this.editedRow = editedRow;
    }

    public List<SessionTransferOutOpWrapperExt> getRowList() {
        return rowList;
    }

    public void setRowList(List<SessionTransferOutOpWrapperExt> rowList) {
        this.rowList = rowList;
    }

}
