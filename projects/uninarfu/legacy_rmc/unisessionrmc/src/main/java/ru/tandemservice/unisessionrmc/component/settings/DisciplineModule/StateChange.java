package ru.tandemservice.unisessionrmc.component.settings.DisciplineModule;

import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

public class StateChange {

    private String oldState = "";

    private EppRegistryElement reg = null;


    public void setOldState(String oldState) {
        this.oldState = oldState;
    }

    public String getOldState() {
        return oldState;
    }

    public void setReg(EppRegistryElement reg) {
        this.reg = reg;
    }

    public EppRegistryElement getReg() {
        return reg;
    }


}
