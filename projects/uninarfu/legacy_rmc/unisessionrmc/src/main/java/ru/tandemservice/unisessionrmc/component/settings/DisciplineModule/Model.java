package ru.tandemservice.unisessionrmc.component.settings.DisciplineModule;

import ru.tandemservice.uniepp.entity.registry.EppRegistryModule;

import java.util.List;

public class Model {
    public static final Object MUTEX = new Object();

    private boolean passProcessed = true;
    private boolean passNotNull = true;
    private boolean processAllModule = false;

    private String moduleTitle = "Укажите часть названия";

    private int processedModule = 0;

    private String title = "";


    private List<EppRegistryModule> rows;

    public void setRows(List<EppRegistryModule> rows) {
        this.rows = rows;
    }


    public List<EppRegistryModule> getRows() {
        return rows;
    }


    public void setPassProcessed(boolean passProcessed) {
        this.passProcessed = passProcessed;
    }


    public boolean getPassProcessed() {
        return passProcessed;
    }


    public void setPassNotNull(boolean passNotNull) {
        this.passNotNull = passNotNull;
    }


    public boolean getPassNotNull() {
        return passNotNull;
    }


    public void setModuleTitle(String moduleTitle) {
        this.moduleTitle = moduleTitle;
    }


    public String getModuleTitle() {
        return moduleTitle;
    }


    public void setTitle(String title) {
        this.title = title;
    }


    public String getTitle() {
        return title;
    }

    public void AddTitle(String msg)
    {
        title += msg;
    }


    public void setProcessedModule(int processedModule) {
        this.processedModule = processedModule;
    }


    public int getProcessedModule() {
        return processedModule;
    }

    public void AddProcessed()
    {
        processedModule++;

    }


    public void setProcessAllModule(boolean processAllModule) {
        this.processAllModule = processAllModule;
    }


    public boolean getProcessAllModule() {
        return processAllModule;
    }
}
