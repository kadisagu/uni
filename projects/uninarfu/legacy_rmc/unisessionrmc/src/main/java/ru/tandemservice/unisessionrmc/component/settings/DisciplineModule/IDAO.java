package ru.tandemservice.unisessionrmc.component.settings.DisciplineModule;

import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModule;

public interface IDAO extends IUniDao<Model> {

    void processModuleInNewTransaction(EppRegistryModule row);

    void process(Model model);
}
