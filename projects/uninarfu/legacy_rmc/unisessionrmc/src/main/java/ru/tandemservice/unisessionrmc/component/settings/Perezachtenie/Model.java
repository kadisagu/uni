package ru.tandemservice.unisessionrmc.component.settings.Perezachtenie;

import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

public class Model {

    private EducationYear educationYear;

    public void setEducationYear(EducationYear educationYear) {
        this.educationYear = educationYear;
    }

    public EducationYear getEducationYear() {
        return educationYear;
    }

}
