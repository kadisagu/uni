package ru.tandemservice.unisessionrmc.base.ext.SessionTransfer.ui.OutsideAddEdit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.IPresenterExtensionBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unisessionrmc.base.ext.SessionTransfer.SessionTransferManagerExt;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.OutsideAddEdit.SessionTransferOutsideAddEdit;

@Configuration
public class SessionTransferOutsideAddEditExt extends BusinessComponentExtensionManager {

    @Autowired
    private SessionTransferOutsideAddEdit parent;

    @Bean
    public PresenterExtension presenterExtension()
    {
        IPresenterExtensionBuilder pi = presenterExtensionBuilder(parent.presenterExtPoint());
        pi.addAddon(uiAddon("ui_addon", SessionTransferOutsideAddEditExtUI.class));
        pi.addDataSource(SessionTransferManagerExt.instance().eduInstitutionDSConfig());
        return pi.create();
    }
}
