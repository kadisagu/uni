package ru.tandemservice.unisessionrmc.base.ext.SessionTransfer.ui.OutsidePub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.IPresenterExtensionBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.OutsidePub.SessionTransferOutsidePub;

@Configuration
public class SessionTransferOutsidePubExt extends BusinessComponentExtensionManager {

    @Autowired
    private SessionTransferOutsidePub parent;


    @Bean
    public PresenterExtension presenterExtension()
    {

        IPresenterExtensionBuilder pi = presenterExtensionBuilder(parent.presenterExtPoint());
        pi.addAddon(uiAddon("ui_addon", SessionTransferOutsidePubExtUI.class));
        return pi.create();
    }


}
