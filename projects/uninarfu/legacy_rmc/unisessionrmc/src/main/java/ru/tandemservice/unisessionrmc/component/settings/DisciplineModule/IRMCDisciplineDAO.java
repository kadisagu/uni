package ru.tandemservice.unisessionrmc.component.settings.DisciplineModule;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModule;


public interface IRMCDisciplineDAO {

    public static final SpringBeanCache<IRMCDisciplineDAO> instance = new SpringBeanCache<IRMCDisciplineDAO>(IRMCDisciplineDAO.class.getName());


    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
    void processModuleInNewTransaction(EppRegistryModule row, Model model);

    void process(Model model);

}
