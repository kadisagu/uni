package ru.tandemservice.unisessionrmc.component.settings.DisciplineModuleAddControlAction;

import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.process.*;
import ru.tandemservice.uni.util.BackgroundProcessHolder;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModule;

import java.util.List;


public class Controller extends AbstractBusinessController<IDAO, Model> {
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        // прежде всего в модели активируем орг юнит
        getDao().prepare(model);
    }

    public void onGenerate(final IBusinessComponent component)
    {
        final Model model = getModel(component);

        model.setTitle("");
        model.setProcessedModule(0);

        // getDao().process(getModel(component));
        try {
            IRMCDisciplineAddCA.instance.get().process(getModel(component));
        }
        catch (Exception e) {
            model.AddTitle(e.getMessage());
            return;
        }
        final List<EppRegistryModule> rows = getModel(component).getRows();

        synchronized (Model.MUTEX) {
            final IBackgroundProcess process = new BackgroundProcessBase() {
                @Override
                public ProcessResult run(final ProcessState state) {

                    try {

                        try {
                            Thread.sleep(1000);
                        }
                        catch (final Throwable t) {
                        }

                        final IEventServiceLock eventLock = CoreServices.eventService().lock();
                        try {
                            state.setMaxValue(100);
                            state.setCurrentValue(0);
                            state.setDisplayMode(ProcessDisplayMode.percent);

                            int i = 0;

                            for (EppRegistryModule row : rows) {
                                state.setCurrentValue(100 * i / rows.size());
                                Thread.yield();

                                try {
                                    // обработаем каждый модуль
                                    IRMCDisciplineAddCA.instance.get().processModuleInNewTransaction(row, model);

                                    row = null;
                                }
                                catch (Exception ex) {
                                    String msg = "Ошибка при обработке модуля " + row.getTitle() + " " + ex.getMessage();
                                    model.AddTitle(msg);
                                    throw ex;
                                }

                                i++;
                            }
                            return null; // закрываем диалог


                        }
                        finally {
                            model.AddTitle("Обработано " + Integer.toString(model.getProcessedModule()) + " модулей ");
                            eventLock.release();

                        }
                    }
                    catch (final Throwable t) {

                        return new ProcessResult("Произошла ошибка " + t.getMessage(), true); // закрываем диалог
                    }

                }
            };
            new BackgroundProcessHolder().start("Обработка учебных модулей", process);
        }

    }
}
