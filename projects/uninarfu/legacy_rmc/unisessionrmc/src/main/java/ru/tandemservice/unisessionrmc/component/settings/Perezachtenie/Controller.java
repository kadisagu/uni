package ru.tandemservice.unisessionrmc.component.settings.Perezachtenie;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        // прежде всего в модели активируем орг юнит
        getDao().prepare(model);
    }

    public void onClick(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().doAction(model);
        deactivate(component);

    }
}