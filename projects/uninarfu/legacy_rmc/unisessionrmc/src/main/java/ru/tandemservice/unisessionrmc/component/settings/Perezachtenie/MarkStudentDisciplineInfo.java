package ru.tandemservice.unisessionrmc.component.settings.Perezachtenie;

import java.util.Date;

/**
 * Инфа по оценкам студента (включая итоги и перезачтения)
 *
 * @author vch
 */
public class MarkStudentDisciplineInfo {

    private Long idStudent;
    private Long disciplineId;
    private int disciplinePart;

    private Long transferId = null;
    private Long bulletinId = null;
    private Long gradebookId = null;
    private Long idSdSlot = null;
    private Date performDate = null;

    public void setIdStudent(Long idStudent) {
        this.idStudent = idStudent;
    }

    public Long getIdStudent() {
        return idStudent;
    }

    public void setDisciplineId(Long disciplineId) {
        this.disciplineId = disciplineId;
    }

    public Long getDisciplineId() {
        return disciplineId;
    }

    public void setDisciplinePart(int disciplinePart) {
        this.disciplinePart = disciplinePart;
    }

    public int getDisciplinePart() {
        return disciplinePart;
    }

    public void setTransferId(Long transferId) {
        this.transferId = transferId;
    }

    public Long getTransferId() {
        return transferId;
    }

    public void setBulletinId(Long bulletinId) {
        this.bulletinId = bulletinId;
    }

    public Long getBulletinId() {
        return bulletinId;
    }

    public void setGradebookId(Long gradebookId) {
        this.gradebookId = gradebookId;
    }

    public Long getGradebookId() {
        return gradebookId;
    }

    public void setInfo(Long idtransfer, Long idbulletin, Long idgradebook, Long idSdSlot
            , Long smId
            , Date performDate)
    {
        // случай - оценка есть
        if (smId != null) {
            if (idtransfer != null)
                setTransferId(idtransfer);

            if (idgradebook != null)
                setGradebookId(idgradebook);

            if (idbulletin != null) {
                // скинуть признак наличия ведомости (эта оценка в ведомости)
                // ставить ее снова не надо - скинем признак
                setBulletinId(null);
            }
        }
        else {
            // нет оценки
            if (idbulletin != null)
                setBulletinId(idbulletin);
        }

        // если есть ведомость и в ней нет оценки - можно проставить признак наличия слота
        if (idbulletin != null && idSdSlot != null && smId == null)
            setIdSdSlot(idSdSlot);

        if (performDate != null)
            setPerformDate(performDate);

    }


    /**
     * Нужно-ли студенту ставить оценку в ведомость
     *
     * @return
     */
    public boolean needSetMark()
    {
        if (getGradebookId() != null && getTransferId() != null && getBulletinId() != null)
            return true;
        else
            return false;

    }

    public void setIdSdSlot(Long idSdSlot) {
        this.idSdSlot = idSdSlot;
    }

    public Long getIdSdSlot() {
        return idSdSlot;
    }

    public void setPerformDate(Date performDate) {
        this.performDate = performDate;
    }

    public Date getPerformDate() {
        return performDate;
    }
}
