package ru.tandemservice.unisessionrmc.base.ext.SessionTransfer;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.config.meta.BeanOverride;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.shared.person.catalog.entity.EduInstitution;
import ru.tandemservice.unisessionrmc.base.ext.SessionTransfer.logic.EduInstitutionDSHandler;
import ru.tandemservice.unisessionrmc.base.ext.SessionTransfer.logic.SessionTransferExtDao;
import ru.tandemservice.unisession.base.bo.SessionTransfer.logic.ISessionTransferDao;


@Configuration
public class SessionTransferManagerExt extends BusinessObjectExtensionManager {

    public static SessionTransferManagerExt instance() {
        return instance(SessionTransferManagerExt.class);
    }

    @Bean
    public UIDataSourceConfig eduInstitutionDSConfig()
    {
        return SelectDSConfig.with("eduInstitutionDS", getName()).dataSourceClass(SelectDataSource.class).handler(eduInstitutionDSHandler()).addColumn(EduInstitution.title().s()).create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> eduInstitutionDSHandler()
    {
        return new EduInstitutionDSHandler(getName());
    }

    @Bean
    @BeanOverride
    public ISessionTransferDao dao()
    {
        return new SessionTransferExtDao();
    }


}
