package ru.tandemservice.fisrmc.dao.packApplications;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.tandemservice.fisrmc.dao.XMLCompetitiveGroup;
import ru.tandemservice.fisrmc.entity.catalog.EducationdirectionSpravType;
import ru.tandemservice.uniecrmc.entity.entrant.Entrant2IndividualAchievements;
import ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.Date;
import java.util.List;

public interface IApplications {

    /**
     * упаковать одно заявление абитуриента
     *
     * @param doc
     * @param entrantrequest
     *
     * @return
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
    public Element packApplication
    (
            Document doc
            , EntrantRequest entrantrequest
            , boolean isEntranceTestResult
            , boolean appDateWithoutTime

            , Date dateRequestFrom
            , Date dateRequestTo
            , boolean whithoutSendedInFis
            , boolean whithoutEntrantInOrder


            , List<XMLCompetitiveGroup> lstCompetitiveGroup
            , List<EnrollmentCampaign> enrollmentCampaigns
            , EducationdirectionSpravType spravType
    );


    /**
     * все заявления
     * по текущим приемкам
     *
     * @param doc
     *
     * @return
     */
    public Element packApplications
    (
            boolean testMode
            , boolean isEntranceTestResult
            , boolean appDateWithoutTime

            , Date dateRequestFrom
            , Date dateRequestTo
            , boolean whithoutSendedInFis
            , boolean whithoutEntrantInOrder

            , List<EnrollmentCampaign> enrollmentCampaigns
            , EducationdirectionSpravType spravType
            , Document doc
    );


    /**
     * предоставлены оригиналы документов
     *
     * @return
     */
    public boolean isOriginalDocuments(EntrantRequest entrantrequest);


    /**
     * Дата предоставления оригиналов документов
     *
     * @param entrantrequest
     *
     * @return
     */
    public String getOriginDocumentDateString(EntrantRequest entrantrequest);

    /**
     * DQL выражение для получения списка заявлений
     *
     * @param ec
     *
     * @return
     */
    public DQLSelectBuilder getEntrantRequestDQLList(
            List<EnrollmentCampaign> ecList
            , boolean notRf
            , boolean benefitStudents
            , boolean isOrdersOfAdmission

            , List<Integer> regNumber
            , List<Integer> excludeRegNumber

            , Date dateRequestFrom
            , Date dateRequestTo
            , boolean whithoutSendedInFis
            , boolean whithoutEntrantInOrder

            , boolean applayOrderBy
            , boolean testMode
            , List<XMLCompetitiveGroup> testModeCompetitiveGroupList
    );


    public void MakeMapRequestedEnrollmentDirectionExt
            (
                    List<EnrollmentCampaign> enrollmentCampaigns
            );


    public RequestedEnrollmentDirectionExt getRequestedEnrollmentDirectionExt(RequestedEnrollmentDirection red);

    /**
     * Очистить кеш
     */
    public void clearCache();

    public void makeCache();

    /**
     * Список индивидуальных достижений
     *
     * @param entrant
     *
     * @return
     */
    public List<Entrant2IndividualAchievements> getEntrantIndividualAchievements(Entrant entrant);

    /**
     * наличие индивидуального достижения
     *
     * @param entrant
     *
     * @return
     */
    public boolean hasIndividualAchievements(Entrant entrant);


}
