package ru.tandemservice.fisrmc.entity;

import ru.tandemservice.fisrmc.entity.gen.EducationdirectionfisGen;

/**
 * Направления подготовки
 */
public class Educationdirectionfis extends EducationdirectionfisGen {
    public boolean isEqualByCodes(Educationdirectionfis entity)
    {
        String ugsCodeThis = "";
        String ugsCode = "";

        if (entity.getUGSCode() != null)
            ugsCode = entity.getUGSCode();

        if (this.getUGSCode() != null)
            ugsCodeThis = this.getUGSCode();

        if (entity.getCodefis().equals(this.getCodefis())
                && entity.getQualificationCode().equals(this.getQualificationCode())
                && entity.getPeriod().equals(this.getPeriod())
                && ugsCodeThis.equals(ugsCode)
                )
            return true;
        else
            return false;

    }

    public boolean isEqualAll(Educationdirectionfis entity)
    {
        String ugsNameThis = "";
        String ugsName = "";

        if (entity.getUGSName() != null)
            ugsName = entity.getUGSName();

        if (this.getUGSName() != null)
            ugsNameThis = this.getUGSName();

        if (isEqualByCodes(entity)
                && ugsName.equals(ugsNameThis)
                )
            return true;
        else
            return false;
    }
}