package ru.tandemservice.fisrmc.base.bo.EdOrgunit2DirectionFisRel.ui.Edit;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.fisrmc.base.bo.EdOrgunit2DirectionFisRel.EdOrgunit2DirectionFisRelManager;
import ru.tandemservice.fisrmc.entity.EdOrgunit2DirectionFis;
import ru.tandemservice.fisrmc.entity.Educationdirectionfis;
import ru.tandemservice.fisrmc.entity.catalog.EducationdirectionSpravType;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

@State({
        @Bind(key = "educationOrgUnitID", binding = "educationOrgUnitID")
        , @Bind(key = "spravTypeFilterID", binding = "spravTypeFilterID")
}
)
public class EdOrgunit2DirectionFisRelEditUI extends UIPresenter {

    private Long educationOrgUnitID;
    private Long spravTypeFilterID;
    private EducationOrgUnit educationOrgUnit;
    private DataWrapper dataWrapper;

    private EdOrgunit2DirectionFis edOrgunit2DirectionFis = null;

    @Override
    public void onComponentRefresh() {

        if (educationOrgUnitID == null || spravTypeFilterID == null)
            throw new ApplicationException("Не указаны обязательные параметры");

        EducationdirectionSpravType spravType = DataAccessServices.dao().get(EducationdirectionSpravType.class, spravTypeFilterID);
        setEducationOrgUnit(DataAccessServices.dao().get(EducationOrgUnit.class, educationOrgUnitID));

        if (educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject() == null)
            throw new ApplicationException("Направление подготовки не сопоставлено с ОП");


        Educationdirectionfis educationdirectionfis = EdOrgunit2DirectionFisRelManager.instance().modifyDao().getEducationdirectionfis(educationOrgUnit, spravType);

        DataWrapper w = new DataWrapper(educationOrgUnit);
        w.setProperty("eou", educationOrgUnit);
        w.setProperty("rel", educationdirectionfis);
        w.setProperty("hasRelationToFis", educationdirectionfis != null ? true : false);
        setDataWrapper(w);


        w.setProperty("spravType", spravType);


        String codeFilter = educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject().getSubjectCode();

        // если у нас специализация
        if (educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getEduProgramSpecialization() != null)
            codeFilter = educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getEduProgramSpecialization().getProgramSubject().getSubjectCode();

        _uiSettings.set("eduProgCodeFilter", codeFilter);


        String qualificationsFilter = educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getQualification().getCode();
        _uiSettings.set("qualificationsFilter", qualificationsFilter);


    }


    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {

		/*
        Map<String, Object> settingMap = _uiSettings.getAsMap(
				"oksoCodeFilter"
				);
		dataSource.putAll(settingMap);
		dataSource.put("qualificationsFilter", educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getQualification().getCode());	
		*/

        dataSource.put("qualificationsFilter", _uiSettings.get("qualificationsFilter"));
        dataSource.put("eduProgCodeFilter", _uiSettings.get("eduProgCodeFilter"));
        // dataSource.put("eduProgCodeSpecFilter", _uiSettings.get("eduProgCodeSpecFilter"));
        dataSource.put("spravTypeFilterID", spravTypeFilterID);
    }


    public void relationToFIS()
    {
        check();

        Long directionFisId = getListenerParameterAsLong();
        if (directionFisId != null) {
            EdOrgunit2DirectionFisRelManager.instance().modifyDao().saveOrUpdateRelation(getEducationOrgUnit(), DataAccessServices.dao().get(Educationdirectionfis.class, directionFisId));
			/*
			if(getDataWrapper().getBoolean("hasRelationToFis"))
			{
				EdOrgunit2DirectionFisRelManager.instance().modifyDao().update(getEducationOrgUnit(), DataAccessServices.dao().get(Educationdirectionfis.class,directionFisId));
			}else
				EdOrgunit2DirectionFisRelManager.instance().modifyDao().save(getEducationOrgUnit(), DataAccessServices.dao().get(Educationdirectionfis.class,directionFisId));
				*/
        }
        getUserContext().getCurrentComponent().refresh();
    }

    private void check()
    {
        if (edOrgunit2DirectionFis != null) {
            if (edOrgunit2DirectionFis.getEducationDirectionFis() != null) {
                String comment = edOrgunit2DirectionFis.getEducationDirectionFis().getExportComment();
                if (comment != null && comment.contains("удал"))
                    throw new ApplicationException("Нельзя пересопоставить выбранное НП, в ФИС конфликт записей. Исправье существующее НП ФИС");
            }
        }

    }

    public void deleteRelationFIS()
    {
        check();

        EdOrgunit2DirectionFisRelManager.instance().modifyDao().deleteRelation(getEducationOrgUnitID(), getSpravTypeFilterID());
        getUserContext().getCurrentComponent().refresh();
    }


    public Long getEducationOrgUnitID() {
        return educationOrgUnitID;
    }


    public void setEducationOrgUnitID(Long educationOrgUnitID) {
        this.educationOrgUnitID = educationOrgUnitID;
    }


    public EducationOrgUnit getEducationOrgUnit() {
        return educationOrgUnit;
    }


    public void setEducationOrgUnit(EducationOrgUnit educationOrgUnit) {
        this.educationOrgUnit = educationOrgUnit;
    }


    public DataWrapper getDataWrapper() {
        return dataWrapper;
    }


    public void setDataWrapper(DataWrapper dataWrapper) {
        this.dataWrapper = dataWrapper;
    }


    public Long getSpravTypeFilterID() {
        return spravTypeFilterID;
    }


    public void setSpravTypeFilterID(Long spravTypeFilterID) {
        this.spravTypeFilterID = spravTypeFilterID;
    }
}
