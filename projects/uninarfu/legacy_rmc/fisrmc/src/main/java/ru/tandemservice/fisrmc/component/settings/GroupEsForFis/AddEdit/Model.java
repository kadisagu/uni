package ru.tandemservice.fisrmc.component.settings.GroupEsForFis.AddEdit;


import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.fisrmc.entity.GroupEsForFis;
import ru.tandemservice.fisrmc.entity.GroupEsForFis2Es;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.catalog.EducationSubject;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.List;

@Input({
        @Bind(key = "groupId", binding = "groupEsForFis.id"),
        @Bind(key = "directionIds", binding = "enrollmentDirectionIdList"),
        @Bind(key = "enrollmentCampaignId", binding = "enrollmentCampaingId")
})
public class Model implements IEnrollmentCampaignSelectModel {
    private IDataSettings settings;
    private ISelectModel enrollmentDirectionModel;
    private ISelectModel qualificationModel;
    private ISelectModel formativeOrgUnitModel;
    private DynamicListDataSource<EducationSubject> educationSubjectDS;
    private DynamicListDataSource<GroupEsForFis2Es> groupEsForFis2EsDS;

    private GroupEsForFis groupEsForFis = new GroupEsForFis();
    private List<EnrollmentCampaign> enrollmentCampaignList;
    private List<Long> enrollmentDirectionIdList;
    private Long enrollmentCampaingId;
    private Boolean editForm = false;
    private Boolean filterForm;
    private Boolean addAveragePoints = false;

    protected Boolean initFilters()
    {
        if (groupEsForFis.getId() == null && (enrollmentDirectionIdList == null || enrollmentDirectionIdList.size() == 0))
            return true;
        else
            return false;
    }

    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return (EnrollmentCampaign) this.settings.get("enrollmentCampaign");
    }

    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        this.settings.set("enrollmentCampaign", enrollmentCampaign);
    }

    public ISelectModel getEnrollmentDirectionModel() {
        return enrollmentDirectionModel;
    }

    public void setEnrollmentDirectionModel(ISelectModel enrollmentDirectionModel) {
        this.enrollmentDirectionModel = enrollmentDirectionModel;
    }

    public GroupEsForFis getGroupEsForFis() {
        return groupEsForFis;
    }

    public void setGroupEsForFis(GroupEsForFis groupEsForFis) {
        this.groupEsForFis = groupEsForFis;
    }

    public DynamicListDataSource<EducationSubject> getEducationSubjectDS() {
        return educationSubjectDS;
    }

    public void setEducationSubjectDS(DynamicListDataSource<EducationSubject> educationSubjectDS) {
        this.educationSubjectDS = educationSubjectDS;
    }

    public Boolean getEditForm() {
        return editForm;
    }

    public void setEditForm(Boolean editForm) {
        this.editForm = editForm;
    }

    public DynamicListDataSource<GroupEsForFis2Es> getGroupEsForFis2EsDS() {
        return groupEsForFis2EsDS;
    }

    public void setGroupEsForFis2EsDS(DynamicListDataSource<GroupEsForFis2Es> groupEsForFis2EsDS) {
        this.groupEsForFis2EsDS = groupEsForFis2EsDS;
    }

    public ISelectModel getQualificationModel() {
        return qualificationModel;
    }

    public void setQualificationModel(ISelectModel qualificationModel) {
        this.qualificationModel = qualificationModel;
    }

    public ISelectModel getFormativeOrgUnitModel() {
        return formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel) {
        this.formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public List<Long> getEnrollmentDirectionIdList() {
        return enrollmentDirectionIdList;
    }

    public void setEnrollmentDirectionIdList(List<Long> enrollmentDirectionIdList) {
        this.enrollmentDirectionIdList = enrollmentDirectionIdList;
    }

    public Boolean getFilterForm() {
        return filterForm;
    }

    public void setFilterForm(Boolean filterForm) {
        this.filterForm = filterForm;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public List<EnrollmentCampaign> getEnrollmentCampaignList() {
        return enrollmentCampaignList;
    }

    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList) {
        this.enrollmentCampaignList = enrollmentCampaignList;
    }

    public Long getEnrollmentCampaingId() {
        return enrollmentCampaingId;
    }

    public void setEnrollmentCampaingId(Long enrollmentCampaingId) {
        this.enrollmentCampaingId = enrollmentCampaingId;
    }

    public Boolean getAddAveragePoints() {
        return addAveragePoints;
    }

    public void setAddAveragePoints(Boolean addAveragePoints) {
        this.addAveragePoints = addAveragePoints;
    }
}
