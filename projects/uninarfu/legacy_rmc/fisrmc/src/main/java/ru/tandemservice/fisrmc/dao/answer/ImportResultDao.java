package ru.tandemservice.fisrmc.dao.answer;

import com.google.common.base.Strings;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import ru.tandemservice.fisrmc.component.UploadFis2TU.Pub.DefaultParser;
import ru.tandemservice.fisrmc.dao.PackUnpackToFis;
import ru.tandemservice.fisrmc.entity.FisAnswerAction;
import ru.tandemservice.fisrmc.entity.FisAnswerTofisPackages;
import ru.tandemservice.fisrmc.entity.FisCheckEntrantRequest;
import ru.tandemservice.fisrmc.entity.catalog.FisAppStatusCode;
import ru.tandemservice.fisrmc.entity.catalog.codes.FisAppStatusCodeCodes;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ImportResultDao extends UniBaseDao implements IImportResultDao {


    protected FisAppStatusCode statusOk;
    protected FisAppStatusCode statusBad;

    @Override
    @Transactional
    public void doAction
            (
                    FisAnswerTofisPackages answer
                    , boolean testMode
            ) throws Exception
    {
        if (this.statusOk == null)
            this.statusOk = getCatalogItem(FisAppStatusCode.class, FisAppStatusCodeCodes.NO_ERROR);
        if (this.statusBad == null)
            this.statusBad = getCatalogItem(FisAppStatusCode.class, FisAppStatusCodeCodes.HAS_ERROR);

        if (answer.getXmlAnswer().getContent() == null)
            throw new ApplicationException("Файл ответа ФИС пуст.");
        if (answer.getFisPackages().getXmlPackage().getContent() == null)
            throw new ApplicationException("Файл пакета пуст.");

        //загружаем данные из xml
        Map<String, EntrantRequestInfo> requestMap = getRequestList(answer);

        // общие ошибки заявления
        Map<String, BadEntrantRequestInfo> answerMap = getAnswerList(answer);

        // ошибки проверки вступительных испытаний
        Map<String, BadEntrantRequestInfo> answerMapEntranceTestResults = getAnswerListEntranceTestResults(answer);

        // ошибки по включенным в приказ
        Map<String, BadEntrantRequestInfo> answerMapOrderOfAdmissionResults = getAnswerListOrderOfAdmissionResults(answer);


        //проставляем результат проверки(FisCheckEntrantRequest) для каждого заявления из пакета в ФИС
        List<String> errorList = new ArrayList<>();
        List<String> logList = new ArrayList<>();

        String comment = "";

        if (testMode) {
            comment += "Обработка в тестовом режиме (в базу данных изменения не вносятся);";
            logList.add(comment);
        }

        process(requestMap, answerMap, answerMapEntranceTestResults, answerMapOrderOfAdmissionResults, errorList, logList, testMode, answer);

        //сохраняем результат обработки ответа
        FisAnswerAction action = new FisAnswerAction();

        action.setFisAnswerTofisPackages(answer);
        action.setDate(new Date());
        action.setErrorReport(saveReport(errorList));
        action.setActionReport(saveReport(logList));


        if (errorList.isEmpty())
            comment += " Ошибок нет;";

        if (logList.isEmpty())
            comment += " Отчет пуст;";

        action.setComment(comment);


        save(action);
    }

    private Map<String, BadEntrantRequestInfo> getAnswerListOrderOfAdmissionResults(
            FisAnswerTofisPackages answer) throws Exception
    {
        Map<String, BadEntrantRequestInfo> result = new HashMap<>();
        byte[] content = answer.getXmlAnswer().getContent();
        String xml = new String(content, "UTF-8");

        if (xml.contains(AnswerParserOrdersOfAdmission.ROOT)
                && xml.contains(AnswerParserOrdersOfAdmission.LOG)
                && (!xml.contains(AnswerParserOrdersOfAdmission.FAILED + ">")
                || !xml.contains(AnswerParserOrdersOfAdmission.ORDERS_OfAdmission + ">")
                || !xml.contains(AnswerParserOrdersOfAdmission.APPLICATION + ">"))
                )
        {
            // нет ответа
            // значит нет ошибок (просто все удачно)
        }
        else {
            List<Map<String, Object>> list = DefaultParser.parse(new AnswerParserOrdersOfAdmission(xml));
            for (Map<String, Object> map : list) {
                BadEntrantRequestInfo info = new BadEntrantRequestInfo((String) map.get("number")
                        , (String) map.get("date")
                        , (String) map.get("code")
                        , (String) map.get("message")
                        , null
                );
                result.put(getKey(info.getNumber(), info.getDate()), info);
            }
        }
        return result;


    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Сохраняет rtf на основе списка сообщений
     *
     * @param list
     *
     * @return
     */
    protected DatabaseFile saveReport(List<String> list) {
        if (list == null || list.isEmpty())
            return null;

        TemplateDocument template = getCatalogItem(TemplateDocument.class, PackUnpackToFis.SUSPEND_ERRORS_TEMPLATE);
        RtfDocument resultRtf = new RtfReader().read(template.getContent());
        RtfTableModifier tableModifier = new RtfTableModifier();

        String[][] tableData = new String[list.size()][1];
        int idx = 0;
        for (String err : list) {
            tableData[idx][0] = err;
            idx++;
        }
        tableModifier.put("T", tableData);
        tableModifier.modify(resultRtf);

        DatabaseFile dFile = new DatabaseFile();
        dFile.setContent(RtfUtil.toByteArray(resultRtf));
        dFile.setFilename("rtf");
        saveOrUpdate(dFile);

        return dFile;
    }

    /**
     * проставляет результат проверки(FisCheckEntrantRequest) для каждого заявления из пакета в ФИС
     *
     * @param requestMap данные из пакета в ФИС
     * @param answerMap  данные ответа из ФИС
     * @param errorList
     * @param logList
     * @param answer
     */
    protected void process
    (
            Map<String, EntrantRequestInfo> requestMap
            , Map<String, BadEntrantRequestInfo> answerMap
            , Map<String, BadEntrantRequestInfo> answerMapEntranceTestResults
            , Map<String, BadEntrantRequestInfo> answerMapOrderOfAdmissionResults
            , List<String> errorList
            , List<String> logList
            , boolean testMode
            , FisAnswerTofisPackages answer
    )

    {

        boolean isEntrantRequest = getTagState(requestMap, EntrantRequestInfo.ENTRANTREQUEST);
        boolean isOrder = getTagState(requestMap, EntrantRequestInfo.ORDERSOFADMISSION);


        // получим map с результатами проверки fisCheckEntrantRequest
        Map<String, EntrantRequestInfo> mapCheckEntrantRequest = makeMapCheckEntrantRequest();

        // список ключей, по которым удаляем
        List<String> removeKey = new ArrayList<>();
        _doAnswer(requestMap, mapCheckEntrantRequest, answerMap, errorList, logList, testMode, removeKey);
        _doAnswer(requestMap, mapCheckEntrantRequest, answerMapEntranceTestResults, errorList, logList, testMode, removeKey);
        _doAnswer(requestMap, mapCheckEntrantRequest, answerMapOrderOfAdmissionResults, errorList, logList, testMode, removeKey);

        for (String key : removeKey) {
            if (mapCheckEntrantRequest.containsKey(key))
                mapCheckEntrantRequest.remove(key);

            if (requestMap.containsKey(key))
                requestMap.remove(key);
        }

        // заявления из файла для отправки в ФИС
        // в коллекции остались только те, по которым не было ошибок
        for (Map.Entry<String, EntrantRequestInfo> entry : requestMap.entrySet()) {
            EntrantRequestInfo info = entry.getValue();
            // нет такого заявления
            Long id = info.getEntityId();
            if (id == 0L)
                continue;
            // пишем, только если запись новая
            if (mapCheckEntrantRequest.containsKey(info.getKey()) && mapCheckEntrantRequest.get(info.getKey()).getPriorityError() == 1) {
                // запись существует, можно не повторять
                // на дату внимания не обращаем
            }
            else {
                if (!testMode) {
                    EntrantRequest entrantRequest = get(EntrantRequest.class, id);
                    //удалим старую запись (строго свои)
                    if (isEntrantRequest)
                        deleteCheckEntrantRequest(id, "1");

                    if (isOrder)
                        deleteCheckEntrantRequest(id, "3001");

                    //записываем новый статус (только если нет иного)
                    insertCheckEntrantRequest(entrantRequest, null, null, this.statusOk);
                }
            }
        }
    }

    private boolean getTagState(Map<String, EntrantRequestInfo> requestMap,
                                String tag)
    {


        for (EntrantRequestInfo er : requestMap.values()) {
            if (er.getTag() != null && !er.getTag().equals("")) {
                if (er.getTag().equals(tag))
                    return true;
            }
        }
        return false;
    }

    /**
     * Все заявления из базы
     *
     * @return
     */
    private Map<String, EntrantRequestInfo> makeMapAllEntrantRequest()
    {
        Map<String, EntrantRequestInfo> map = new HashMap<>();

        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EntrantRequest.class, "ent")

                .column(DQLExpressions.property(EntrantRequest.id().fromAlias("ent")))
                .column(DQLExpressions.property(EntrantRequest.regNumber().fromAlias("ent")))
                .column(DQLExpressions.property(EntrantRequest.regDate().fromAlias("ent")));

        List<Object[]> rows = dql.createStatement(getSession()).list();
        for (Object[] objs : rows) {
            if (objs[0] != null && objs[1] != null && objs[2] != null) {
                Long id = (Long) objs[0];
                Integer regNumber = (Integer) objs[1];
                Date regDate = (Date) objs[2];

                EntrantRequestInfo eri = new EntrantRequestInfo(
                        id
                        , Integer.toString(regNumber)
                        , regDate
                );
                map.put(eri.getKey(), eri);
            }
        }
        return map;
    }

    private void _doAnswer(
            Map<String, EntrantRequestInfo> requestMap
            , Map<String, EntrantRequestInfo> mapCheckEntrantRequest
            , Map<String, BadEntrantRequestInfo> answerMap
            , List<String> errorList
            , List<String> logList
            , boolean testMode
            , List<String> removeKey)
    {

        // записываем коды ошибок
        for (Map.Entry<String, BadEntrantRequestInfo> entry : answerMap.entrySet()) {
            String key = entry.getKey();
            BadEntrantRequestInfo info = entry.getValue();

            // игнорируемые коды ошибок
            if (info.getCode().equals("4"))
                continue;

            EntrantRequestInfo found = requestMap.get(key);
            if (found == null) {
                errorList.add("Пакет выгрузки в ФИС не содержит заявление " + info.getNumber() + ", дата " + RussianDateFormatUtils.STANDARD_DATE_TIME_FORMAT.format(info.getDate()));
                continue;
            }

            // исключим ошибочного из коллекции
            //mapCheckEntrantRequest.remove(found.getKey());
            removeKey.add(found.getKey());


            if (!testMode && found.getEntityId() != 0L) {
                EntrantRequest entrantRequest = getNotNull(found.getEntityId());
                //удалим старые записи
                deleteCheckEntrantRequest(found.getEntityId(), info.getCode());
                //записываем новый статус
                insertCheckEntrantRequest(entrantRequest, info.getCode(), info.getMessage(), this.statusBad);
            }

            String logMsg = "Заявление " + found.getNumber() + ", ошибки фис: (" + info.getCode() + ") : " + info.getMessage();
            if (info.getComment() != null && !info.getComment().equals(""))
                logMsg += info.getComment();

            logList.add(logMsg);

            // удалим из коллекции то, что содержало ошибки
            // mapCheckEntrantRequest.remove(found.getKey());
            //requestMap.remove(key);
        }


    }


    private Map<String, EntrantRequestInfo> makeMapCheckEntrantRequest()
    {
        Map<String, EntrantRequestInfo> map = new HashMap<>();

        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(FisCheckEntrantRequest.class, "ent")

                .column(DQLExpressions.property(FisCheckEntrantRequest.entrantRequest().id().fromAlias("ent")))
                .column(DQLExpressions.property(FisCheckEntrantRequest.entrantRequest().regNumber().fromAlias("ent")))
                .column(DQLExpressions.property(FisCheckEntrantRequest.entrantRequest().regDate().fromAlias("ent")))

                .column(DQLFunctions.max(DQLExpressions.property(FisCheckEntrantRequest.fisAppStatusCode().priority().fromAlias("ent"))));

        dql.group(DQLExpressions.property(FisCheckEntrantRequest.entrantRequest().id().fromAlias("ent")));
        dql.group(DQLExpressions.property(FisCheckEntrantRequest.entrantRequest().regNumber().fromAlias("ent")));
        dql.group(DQLExpressions.property(FisCheckEntrantRequest.entrantRequest().regDate().fromAlias("ent")));


        List<Object[]> rows = dql.createStatement(getSession()).list();
        for (Object[] objs : rows) {
            if (objs[0] != null && objs[1] != null && objs[2] != null && objs[3] != null) {
                Long id = (Long) objs[0];
                Integer regNumber = (Integer) objs[1];
                Date regDate = (Date) objs[2];
                Integer priorInt = (Integer) objs[3];

                EntrantRequestInfo eri = new EntrantRequestInfo(
                        id
                        , Integer.toString(regNumber)
                        , regDate
                );
                eri.setPriorityError(priorInt);

                map.put(eri.getKey(), eri);
            }
        }
        return map;
    }

    /**
     * удаляет результат проверки для заявления
     *
     * @param entrantRequest
     */
    protected void deleteCheckEntrantRequest(Long entrantRequestId, String errorCode)
    {
        // chuk 2013/04/18
        // по вызову get должна буть получена уникальная запись, если записей>1 должно быть исключение
        /*
		FisCheckEntrantRequest check = get(FisCheckEntrantRequest.class, FisCheckEntrantRequest.entrantRequest(), entrantRequest);
		if (check != null)
			delete(check);
		*/
        Integer errCode = 0;
        if (errorCode != null && !errorCode.equals(""))
            errCode = Integer.parseInt(errorCode);

        DQLDeleteBuilder ddb = new DQLDeleteBuilder(FisCheckEntrantRequest.class)
                .where(DQLExpressions.eq(DQLExpressions.property(FisCheckEntrantRequest.entrantRequest().id()), DQLExpressions.value(entrantRequestId)));

        // удаляем строго свои ошибки
        if (errCode > 3000) {
            ddb.where(DQLExpressions.or
                    (
                            DQLExpressions.gt(DQLExpressions.property(FisCheckEntrantRequest.errorCode()), DQLExpressions.value(3000))
                            , DQLExpressions.eq(DQLExpressions.property(FisCheckEntrantRequest.errorCode()), DQLExpressions.value(0))
                    ));
        }


        if (errCode > 0 && errCode < 2999) {
            ddb.where(DQLExpressions.ge(DQLExpressions.property(FisCheckEntrantRequest.errorCode()), DQLExpressions.value(0)));
            ddb.where(DQLExpressions.lt(DQLExpressions.property(FisCheckEntrantRequest.errorCode()), DQLExpressions.value(2999)));
        }


        ddb.createStatement(getSession())
                .execute();

    }

    /**
     * сохраняет результат проверки для заявления
     *
     * @param entrantRequest
     * @param errorCode
     * @param errorMessage
     * @param status
     */
    protected void insertCheckEntrantRequest(EntrantRequest entrantRequest, String errorCode, String errorMessage, FisAppStatusCode status) {
        FisCheckEntrantRequest check = new FisCheckEntrantRequest();

        Integer errCode = 0;
        if (errorCode != null)
            errCode = Integer.parseInt(errorCode);

        check.setCheckDate(new Date());
        check.setEntrantRequest(entrantRequest);
        check.setFisAppStatusCode(status);
        check.setErrorCode(errCode);
        check.setErrorMessage(errorMessage);

        save(check);
    }

    /**
     * загружает ошибки из xml ответа
     *
     * @param answer
     *
     * @return
     *
     * @throws Exception
     */
    protected Map<String, EntrantRequestInfo> getRequestList(FisAnswerTofisPackages answer) throws Exception {
        Map<String, EntrantRequestInfo> result = new HashMap<>();

        byte[] content = answer.getFisPackages().getXmlPackage().getContent();
        String xml = new String(content, "UTF-8");

        List<Map<String, Object>> list = DefaultParser.parse(new EntrantRequestParser(xml));
        for (Map<String, Object> map : list) {
            EntrantRequestInfo info = new EntrantRequestInfo((String) map.get("uid"), (String) map.get("number"), (String) map.get("date"));
            info.setTag(EntrantRequestInfo.ENTRANTREQUEST);

            result.put(getKey(info.getNumber(), info.getDate()), info);

        }

        boolean hasId_0 = false;
        // а если в документе есть раздел с включенными в приказ ?
        List<Map<String, Object>> listOrder = DefaultParser.parse(new EntrantRequestOrderOfAdmissionParser(xml));
        for (Map<String, Object> map : listOrder) {
            hasId_0 = true;
            EntrantRequestInfo info = new EntrantRequestInfo("0", (String) map.get("number"), (String) map.get("date"));
            info.setTag(EntrantRequestInfo.ORDERSOFADMISSION);

            String key = getKey(info.getNumber(), info.getDate());
            if (!result.containsKey(key))
                result.put(key, info);
        }

        if (hasId_0) {
            // все заявления из тандема
            Map<String, EntrantRequestInfo> mapAllEntrantRequest = makeMapAllEntrantRequest();
            for (EntrantRequestInfo er : result.values()) {
                if (er.getEntityId() == 0L) {
                    if (mapAllEntrantRequest.containsKey(er.getKey())) {
                        Long id = mapAllEntrantRequest.get(er.getKey()).getEntityId();
                        er.setEntityId(id);
                    }
                    else {
                        // не найти такое заявление, возможно загружен xml не от этой базы
                    }
                }
            }
        }
        return result;
    }

    /**
     * загружает все заявления из пакета в ФИС
     *
     * @param answer
     *
     * @return
     *
     * @throws Exception
     */
    protected Map<String, BadEntrantRequestInfo> getAnswerList(FisAnswerTofisPackages answer) throws Exception {
        Map<String, BadEntrantRequestInfo> result = new HashMap<>();

        byte[] content = answer.getXmlAnswer().getContent();
        String xml = new String(content, "UTF-8");

        // элементарные проверки на наличие данных
        // FAILED + "-" + APPLICATIONS + "-" + APPLICATION;
        if (xml.contains(AnswerParser.ROOT)
                && xml.contains(AnswerParser.LOG)
                && (!xml.contains(AnswerParser.FAILED + ">")
                || !xml.contains(AnswerParser.APPLICATIONS + ">")
                || !xml.contains(AnswerParser.APPLICATION + ">"))
                )
        {
            // нет ответа
            // значит нет ошибок (просто все удачно)
        }
        else {
            List<Map<String, Object>> list = DefaultParser.parse(new AnswerParser(xml));
            for (Map<String, Object> map : list) {
                BadEntrantRequestInfo info = new BadEntrantRequestInfo((String) map.get("number")
                        , (String) map.get("date")
                        , (String) map.get("code")
                        , (String) map.get("message")
                        , null
                );
                result.put(getKey(info.getNumber(), info.getDate()), info);
            }
        }
        return result;
    }


    // ошибки результатов вступительных испытаний
    protected Map<String, BadEntrantRequestInfo> getAnswerListEntranceTestResults(FisAnswerTofisPackages answer) throws Exception {
        Map<String, BadEntrantRequestInfo> result = new HashMap<>();

        byte[] content = answer.getXmlAnswer().getContent();
        String xml = new String(content, "UTF-8");

        if (xml.contains(AnswerParseEntranceTestResult.ROOT)
                && xml.contains(AnswerParseEntranceTestResult.LOG)
                && (!xml.contains(AnswerParseEntranceTestResult.FAILED + ">")
                || !xml.contains(AnswerParseEntranceTestResult.ENTRANCETestResults + ">")
                || !xml.contains(AnswerParseEntranceTestResult.ENTRANCETestResult + ">"))
                )
        {
            // нет ответа
            // значит нет ошибок (просто все удачно)
        }
        else {
            List<Map<String, Object>> list = DefaultParser.parse(new AnswerParseEntranceTestResult(xml));
            for (Map<String, Object> map : list) {

                // subjectName
                // resultValue
                String comment = "";
                if (map.get("subjectName") != null)
                    comment += "Дисц: " + map.get("subjectName");

                if (map.get("resultValue") != null)
                    comment += " оценка: " + map.get("resultValue");


                BadEntrantRequestInfo info = new BadEntrantRequestInfo(
                        (String) map.get("number")
                        , (String) map.get("date")
                        , (String) map.get("code")
                        , (String) map.get("message")
                        , comment
                );
                result.put(getKey(info.getNumber(), info.getDate()), info);
            }
        }
        return result;
    }


    public static final SimpleDateFormat SDF_NO_TIME = new SimpleDateFormat("yyyy-MM-dd");

    protected static String getKey(Integer number, Date date)
    {
        // при формировании ключа желательно исключить время!!!
        // так как пакеты могут формироваться с временем и без времени
        return number + "-" + SDF_NO_TIME.format(date);

    }

    ///////////////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////////////////

    protected static class EntrantRequestInfo {

        public static String ENTRANTREQUEST = "EntrantRequest";
        public static String ORDERSOFADMISSION = "OrderOfAdmission";

        public static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        private Long id;
        private Integer number;
        private Date date;
        private Integer priorityError = 0;
        private String tag = "";


        public EntrantRequestInfo(String idStr, String numberStr, String dateStr) throws ParseException {
            this.id = Long.parseLong(idStr);
            this.number = Integer.parseInt(numberStr);
            this.date = SDF.parse(dateStr);


        }

        public boolean isOrderOfAdmission()
        {
			if (Strings.isNullOrEmpty(this.getTag()))
				return false;
            return this.getTag().equals(ORDERSOFADMISSION);
        }

        public boolean isEntrantRequest()
        {
			if (Strings.isNullOrEmpty(this.getTag()))
				return false;
			return this.getTag().equals(ENTRANTREQUEST);
        }


        public EntrantRequestInfo(Long id, String numberStr, Date date)
        {
            this.id = id;
            this.number = Integer.parseInt(numberStr);
            this.date = date;

        }

        public Long getEntityId() {
            return id;
        }

        public void setEntityId(Long id) {
            this.id = id;
        }

        public Integer getNumber() {
            return number;
        }

        public void setNumber(Integer number) {
            this.number = number;
        }

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public void setPriorityError(Integer priorityError) {
            this.priorityError = priorityError;
        }

        public Integer getPriorityError() {
            return priorityError;
        }

        public String getKey()
        {
            return ImportResultDao.getKey(getNumber(), getDate());
        }

        public void setTag(String tag) {
            this.tag = tag;
        }

        public String getTag() {
            return tag;
        }

    }
    ///////////////////////////////////////////////////////////////////////////////////////

    protected static class BadEntrantRequestInfo {
        public static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

        private Integer number;
        private Date date;
        private String code;
        private String message;
        private String comment = null;

        public BadEntrantRequestInfo
                (
                        String numberStr
                        , String dateStr
                        , String code
                        , String message
                        , String comment
                ) throws ParseException
        {
            this.number = Integer.parseInt(numberStr);
            this.date = SDF.parse(dateStr);
            this.code = code;
            this.message = message;
            this.comment = comment;
        }

        public Integer getNumber() {
            return number;
        }

        public void setNumber(Integer number) {
            this.number = number;
        }

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getComment() {
            return comment;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////
}
