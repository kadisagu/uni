package ru.tandemservice.fisrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_fisrmc_1x0x0_14to15 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.10"),
                        new ScriptDependency("org.tandemframework.shared", "1.1.3"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.1.3")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность fisCheckEgeSertificate

        // изменен тип свойства errorCodeEge
        {
            // изменить тип колонки
            tool.executeUpdate("delete from fischeckegesertificate_t");
            tool.dropColumn("fischeckegesertificate_t", "errorcodeege_p");
            tool.createColumn("fischeckegesertificate_t", new DBColumn("errorcodeege_p", DBType.INTEGER));


        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fisCheckEntrantRequest

        // изменен тип свойства errorCode
        {
            tool.executeUpdate("delete from fischeckentrantrequest_t");
            // изменить тип колонки
            tool.dropColumn("fischeckentrantrequest_t", "errorcode_p");
            tool.createColumn("fischeckentrantrequest_t", new DBColumn("errorcode_p", DBType.INTEGER));

        }


    }
}