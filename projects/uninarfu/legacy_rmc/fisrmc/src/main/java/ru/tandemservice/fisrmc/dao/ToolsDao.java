package ru.tandemservice.fisrmc.dao;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.tandemservice.fisrmc.entity.catalog.Ecdisciplinefis;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ToolsDao {

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    private static SimpleDateFormat sdfTime = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    // T00:00:00.0000000

    public static void PackUid
            (
                    Document doc
                    , Element root
                    , String uid
            )
    {

        Element uidElement = doc.createElement("UID");
        root.appendChild(uidElement);
        uidElement.setTextContent(uid);
    }

    public static void PackToElement
            (
                    Document doc
                    , Element root
                    , String attrName
                    , String title)
    {

        Element newElem = doc.createElement(attrName);
        root.appendChild(newElem);
        newElem.setTextContent(title);

    }

    public static void PackToElement
            (
                    Document doc
                    , Element root
                    , String attrName
                    , boolean boolVal)
    {
        String sval = Boolean.toString(boolVal).toLowerCase();
        PackToElement(doc, root, attrName, sval);
    }


    public static void PackOriginalReceived
            (
                    Document doc
                    , Element root
                    , boolean boolVal
            )
    {
        PackToElement(doc, root, "OriginalReceived", boolVal);
    }


    public static void PackToElement
            (
                    Document doc
                    , Element root
                    , String attrName
                    , Date dateVal
                    , boolean whithTime
                    , boolean setTimeTo_0)
    {

        String sval = "";
        if (dateVal != null) {
            if (whithTime) {
                if (setTimeTo_0)
                    sval = DateToString(dateVal) + "T00:00:00.0000000";
                else
                    sval = DateTimeToString(dateVal);
            }
            else
                sval = DateToString(dateVal);
            PackToElement(doc, root, attrName, sval);
        }
    }

    public static void PackYearToElement
            (
                    Document doc
                    , Element root
                    , String attrName
                    , Date dateVal)
    {

        Calendar cal = Calendar.getInstance();
        cal.setTime(dateVal);
        int year = cal.get(Calendar.YEAR);
        PackToElement(doc, root, attrName, Integer.toString(year));
    }

    public static int getYear(Date date)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int year = cal.get(Calendar.YEAR);

        return year;
    }

    public static Date addDays(Date date, int days)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); //minus number would decrement the days
        return cal.getTime();
    }

    public static Date addYear(Date date, int year)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.YEAR, year); //minus number would decrement the days
        return cal.getTime();
    }


    public static Date addMonth(Date date, int month)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, month); //minus number would decrement the days
        return cal.getTime();
    }


    public static String DateToString(Date date)
    {
        return sdf.format(date);
    }

    public static String DateTimeToString(Date date)
    {
        return sdfTime.format(date);
    }

    public static void PackEcdisciplinefis
            (
                    Document doc
                    , Element root
                    , Ecdisciplinefis discipline
            )
    {
        Element testSubject = doc.createElement("EntranceTestSubject");
        root.appendChild(testSubject);

        if (discipline.getId() == null) {
            String subjName = discipline.getTitle();

            if (subjName.length() > 50)
                subjName = subjName.substring(0, 50);

            ToolsDao.PackToElement(doc, testSubject, "SubjectName", subjName);
        }
        else
            ToolsDao.PackToElement(doc, testSubject, "SubjectID", discipline.getCode());


    }


}
