package ru.tandemservice.fisrmc.dao.packApplications;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.collections.map.MultiKeyMap;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.tandemservice.fisrmc.dao.*;
import ru.tandemservice.fisrmc.dao.packApplicationCommonBenefit.ApplicationCommonBenefit;
import ru.tandemservice.fisrmc.dao.packEduDocuments.PackEduDocuments;
import ru.tandemservice.fisrmc.entity.FisCheckEgeSertificate;
import ru.tandemservice.fisrmc.entity.FisCheckEntrantRequest;
import ru.tandemservice.fisrmc.entity.catalog.Compensationtypefis;
import ru.tandemservice.fisrmc.entity.catalog.Developformfis;
import ru.tandemservice.fisrmc.entity.catalog.Ecrequestfis;
import ru.tandemservice.fisrmc.entity.catalog.EducationdirectionSpravType;
import ru.tandemservice.fisrmc.entity.catalog.codes.FisAppStatusCodeCodes;
import ru.tandemservice.fisrmc.entity.catalog.codes.FisEgeStatusCodeCodes;
import ru.tandemservice.uniecrmc.entity.entrant.Entrant2IndividualAchievements;
import ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;

import java.util.*;
import java.util.Map.Entry;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class Applications
        extends UniBaseDao
        implements IApplications
{

    private static IApplications _IApplications = null;

    public static IApplications Instanse()
    {
        if (_IApplications == null)
            _IApplications = (IApplications) ApplicationRuntime.getBean("packApplications");
        return _IApplications;
    }


    @Override
    public Element packApplication(Document doc
            , EntrantRequest entrantrequest
            , boolean isEntranceTestResult
            , boolean appDateWithoutTime

            , Date dateRequestFrom
            , Date dateRequestTo
            , boolean whithoutSendedInFis
            , boolean whithoutEntrantInOrder

            , List<XMLCompetitiveGroup> lstCompetitiveGroup
            , List<EnrollmentCampaign> enrollmentCampaigns
            , EducationdirectionSpravType spravType
    )
    {


        Element application = doc.createElement("Application");

        // uid
        ToolsDao.PackUid(doc, application, Long.toString(entrantrequest.getId()));

        // номер заявления
        String requestNumber = Integer.toString(entrantrequest.getRegNumber());
        ToolsDao.PackToElement(doc, application, "ApplicationNumber", requestNumber);

        // дата отзыва зявления
        if (entrantrequest.isTakeAwayDocument()) {
            Date takeAwayDate = getTakeAwayDate(entrantrequest);
            ToolsDao.PackToElement(doc, application, "LastDenyDate", takeAwayDate, true, true);
        }

        // дата регистрации заявления
        ToolsDao.PackToElement(doc, application, "RegistrationDate", entrantrequest.getRegDate(), true, appDateWithoutTime);

        // нуждается в общежитии
        boolean isNeedDormitory = entrantrequest.getEntrant().getPerson().isNeedDormitory();
        ToolsDao.PackToElement(doc, application, "NeedHostel", isNeedDormitory);

        // статус заявления
        EntrantState entrantState = getEntrantRequestState(entrantrequest);
        Ecrequestfis ecrequestfis = SpravDao.Instanse().getEcrequestfis(entrantState);
        // если статус заявления не найдется - будет исключение
        ToolsDao.PackToElement(doc, application, "StatusID", ecrequestfis.getCode());

        // сначала в рамках заявления нужно получить все направления для приема
        List<EnrollmentDirection> lstEnrollmentDirections = getEnrollmentDirection(entrantrequest, enrollmentCampaigns);
        // имея lstEnrollmentDirections, получим все конкурсные группы, в которых участвуют данные направления подготовки

        List<String> lstCompetitiveGroupsUID = getCompetitiveGroupsUID(lstEnrollmentDirections, lstCompetitiveGroup);
        List<String> lstCompetitiveGroupItemsUID = getCompetitiveGroupItemsUID(lstEnrollmentDirections, lstCompetitiveGroup);

        // конкурсные группы, в которые попадает заявление
        List<XMLCompetitiveGroup> lstCG = getCompetitiveGroupsList(lstEnrollmentDirections, lstCompetitiveGroup);

        // если не сопоставленыв направления подготовки, то абитуриента такого - не пакуем
        if (lstCompetitiveGroupItemsUID == null || lstCompetitiveGroupItemsUID.size() == 0)
            return null;


        _pacStringList(doc, application, "SelectedCompetitiveGroups", "CompetitiveGroupID", lstCompetitiveGroupsUID);
        _pacStringList(doc, application, "SelectedCompetitiveGroupItems", "CompetitiveGroupItemID", lstCompetitiveGroupItemsUID);


        // FinSourceAndEduForms
        Element attrFinSourceAndEduForms = packFinSourceAndEduForms(doc, application, entrantrequest, enrollmentCampaigns, spravType, lstCG);
        application.appendChild(attrFinSourceAndEduForms);

        // entrant
        Element entrant = EntrantPack.Instanse().packEntrant(doc, entrantrequest.getEntrant());
        application.appendChild(entrant);

        // ApplicationDocuments
        // внимание - в тандеме может не быть документа об образовании
        if (PackUnpackToFis.DAEMON_PARAMS.isSuspendErrorDoc() && entrantrequest.getEntrant().getPerson().getPersonEduInstitution() == null) {
            // не пакуем такого абитуриента
            PackUnpackToFis.AddPkgError("У " + entrantrequest.getEntrant().getPerson().getIdentityCard().getFullFio() + " нет документа об образовании");
            return null;
        }
        Element appDoc = ApplicationDocuments.Instanse().PackApplicationDocuments(doc, entrantrequest);
        application.appendChild(appDoc);


        // упаковываем данные о льготах
        // льготы указываются в направлениях для приема
        if (_hasBenefit(entrantrequest)) {
            // с 2014 года не надо проверять кол-во льгот
            Element applicationCommonBenefit = ApplicationCommonBenefit.Instanse().packApplicationCommonBenefit(
                    doc
                    , entrantrequest
                    , lstCG
                    , getRequestedEnrollmentDirectionBenefitList(entrantrequest)
            );


            if (applicationCommonBenefit != null)
                application.appendChild(applicationCommonBenefit);

        }

        if (isEntranceTestResult) //упаковывать результат вступительных испытаний
        {
            try {
                Element elem = PackEntranceTestResult.Instanse().packEntranceTestResult(
                        doc
                        , entrantrequest
                        , lstEnrollmentDirections
                        , lstCG);
                if (elem != null)
                    application.appendChild(elem);
            }
            catch (Exception ex) {
                String errMsg = "";
                if (ex.getMessage() != null)
                    errMsg = ex.getMessage();

                throw new ApplicationException("Для заявления " + entrantrequest.getRegNumber() + " не упакованы результаты вступительных испытаний " + errMsg);
            }
        }

        // <TODO> хеш код заявления
        /*
		XMLOutputter outp = new XMLOutputter();
		List lst = new ArrayList();
		lst.add(application);
		String s = outp.outputString(lst);
		System.out.println(s);
		*/
        IApplications idao = Applications.Instanse();
        if (idao.hasIndividualAchievements(entrantrequest.getEntrant())) {

            Element iaRoot = doc.createElement("IndividualAchievements");

            List<Entrant2IndividualAchievements> lst = idao.getEntrantIndividualAchievements(entrantrequest.getEntrant());
            for (Entrant2IndividualAchievements ia : lst) {
                Element el = packIA(doc, iaRoot, ia, entrantrequest);
                if (el != null)
                    iaRoot.appendChild(el);
            }

            application.appendChild(iaRoot);
        }

        return application;
    }

    private Element packIA(
            Document doc
            , Element iaRoot
            , Entrant2IndividualAchievements ia
            , EntrantRequest entrantrequest)
    {
        Element retVal = doc.createElement("IndividualAchievement");
        String uid = "IAUID:" + ia.getUid(entrantrequest);

        ToolsDao.PackToElement(doc, retVal, "IAUID", uid);

        String name = "Без имени";

        if (ia.getIndividualAchievements() != null && ia.getIndividualAchievements().getTitle() != null && !ia.getIndividualAchievements().getTitle().isEmpty())
            name = ia.getIndividualAchievements().getTitle();

        ToolsDao.PackToElement(doc, retVal, "IAName", name);
        ToolsDao.PackToElement(doc, retVal, "IADocumentUID", ia.getUid(entrantrequest));

        return retVal;
    }


    private List<RequestedEnrollmentDirection> getRequestedEnrollmentDirectionBenefitList
            (EntrantRequest entrantrequest)

    {
        List<RequestedEnrollmentDirection> retVal = new ArrayList<RequestedEnrollmentDirection>();

        Long key = entrantrequest.getId();
        List<RequestedEnrollmentDirection> rd = new ArrayList<RequestedEnrollmentDirection>();

        if (mapRequestedEnrollmentDirectionMarkOlimpiad.containsKey(key))
            rd = mapRequestedEnrollmentDirectionMarkOlimpiad.get(key);

        if (mapRequestedEnrollmentDirectionBenefit.containsKey(key))
            rd.addAll(mapRequestedEnrollmentDirectionBenefit.get(key));


        if (mapRequestedEnrollmentDirectionNoExam.containsKey(key))
            rd.addAll(mapRequestedEnrollmentDirectionNoExam.get(key));


        if (rd != null && rd.size() > 0) {
            for (RequestedEnrollmentDirection item : rd) {
                if (!retVal.contains(item))
                    retVal.add(item);
            }
        }
        return retVal;
    }


    private boolean _hasBenefit(EntrantRequest entrantrequest)
    {

        // если вид конкурса - вне конкурса, должна быть олимпиада, факт ее наличия не
        // проверяем
        // if (entrantrequest.get)

        Long key = entrantrequest.getId();

        if (mapRequestedEnrollmentDirectionNoExam.containsKey(key))
            return true;
        if (mapRequestedEnrollmentDirectionMarkOlimpiad.containsKey(key))
            return true;
        if (mapRequestedEnrollmentDirectionBenefit.containsKey(key))
            return true;
        return false;
    }

    private Element packFinSourceAndEduForms(
            Document doc
            , Element application
            , EntrantRequest entrantrequest
            , List<EnrollmentCampaign> enrollmentCampaigns
            , EducationdirectionSpravType spravType
            , List<XMLCompetitiveGroup> lstCompetitiveGroup
    )
    {

        ITargetOrganizationDAO targetOrganizationDAO = TargetOrganizationDAO.Instanse();

        _makeMapRequestedEnrollmentDirection(enrollmentCampaigns);

        Element root = doc.createElement("FinSourceAndEduForms");
        MultiKeyMap map = new MultiKeyMap();

        // получим все выбранные направления для приема
        List<RequestedEnrollmentDirection> lst = mapRequestedEnrollmentDirection.get(entrantrequest.getId());

        if (lst == null)
            throw new ApplicationException("Для заявления " + entrantrequest.getRegNumber() + " не указаны направления для приема");

        for (RequestedEnrollmentDirection red : lst) {
            RequestedEnrollmentDirectionExt redExt = null;
            if (mapRequestedEnrollmentDirectionExt.containsKey(red.getId()))
                redExt = mapRequestedEnrollmentDirectionExt.get(red.getId());

            Compensationtypefis ctf = SpravDao.Instanse().getCompensationtypefis(red, redExt);
            Developformfis df = SpravDao.Instanse().getDevelopformfis(red.getEnrollmentDirection().getEducationOrgUnit().getDevelopForm());
            String idTarget = null;

            if (red.isTargetAdmission()) {
                TargetAdmissionKind tg = red.getTargetAdmissionKind();
                if (tg != null) {
                    String parentTarget = null;

                    // нужно проверить в планах на прием, есть - ли цифры по данной организации
                    // если цифр нет, значит план приема на целевиков заполнен с ошибкой
                    // не надо такое грузить
                    for (XMLCompetitiveGroup xcg : lstCompetitiveGroup) {
                        List<XMLTargetOrganization> lstTargetOrganizations = targetOrganizationDAO.getTargetOrganizations(xcg, spravType);

                        for (XMLTargetOrganization xtg : lstTargetOrganizations) {
                            // корень целевого приема
                            if (xtg.getTargetadmissionkind() != null && xtg.getTargetadmissionkind().getParent() == null)
                                // корень целевого приема
                                parentTarget = xtg.getUid();

                            // точное попадание
                            if (xtg.getTargetadmissionkind() != null && xtg.getTargetadmissionkind().getId().equals(tg.getId()))
                                idTarget = xtg.getUid();
                        }
                    }

                    if (idTarget == null) {
                        // не задан план приема для организации целевого приема
                        // есть общий итог по целевому приему
                        if (parentTarget != null)
                            idTarget = parentTarget;
                    }
                }
                else {
                    PackUnpackToFis.AddPkgError("Для заявления " + entrantrequest.getRegNumber() + " не указана организация целевого приема");

                    // организацию не указали, зададим корень целевого приема
                    // иначе нельзя (в фис возникает ошибка)
                    for (XMLCompetitiveGroup xcg : lstCompetitiveGroup) {
                        List<XMLTargetOrganization> lstTargetOrganizations = targetOrganizationDAO.getTargetOrganizations(xcg, spravType);
                        for (XMLTargetOrganization xtg : lstTargetOrganizations) {
                            if (xtg.getTargetadmissionkind() != null && xtg.getTargetadmissionkind().getParent() == null)
                                // корень целевого приема
                                idTarget = xtg.getUid();
                        }
                    }

                }

            }
            else {

            }


            MultiKey multiKey = new MultiKey
                    (
                            ctf.getId()
                            , df.getId()
                            , idTarget
                    );

            if (!map.containsKey(multiKey))
                map.put(multiKey, multiKey);
        }

        // просмотрим ключи
        for (Object entry : map.entrySet()) {
            Element parent = doc.createElement("FinSourceEduForm");
            root.appendChild(parent);

            Entry<MultiKey, List> set = (Entry<MultiKey, List>) entry;
            MultiKey mk = set.getKey();

            Compensationtypefis ctf = get(Compensationtypefis.class, (Long) mk.getKey(0));
            Developformfis df = get(Developformfis.class, (Long) mk.getKey(1));
            String targetId = null;
            if (mk.getKey(2) != null)
                targetId = (String) mk.getKey(2);

            ToolsDao.PackToElement(doc, parent, "FinanceSourceID", ctf.getCode());
            ToolsDao.PackToElement(doc, parent, "EducationFormID", df.getCode());

            if (targetId != null) {
                // есть целевой прием - пакуем ID
                ToolsDao.PackToElement(doc, parent, "TargetOrganizationUID", targetId);
            }
        }
        return root;
    }


    private Map<Long, List<RequestedEnrollmentDirection>> mapRequestedEnrollmentDirection = null;

    // а тут связь с RequestedEnrollmentDirection
    private Map<Long, RequestedEnrollmentDirectionExt> mapRequestedEnrollmentDirectionExt = null;

    private void _makeMapRequestedEnrollmentDirection
            (
                    List<EnrollmentCampaign> enrollmentCampaigns
            )
    {
        MakeMapRequestedEnrollmentDirectionExt(enrollmentCampaigns);

        if (mapRequestedEnrollmentDirection != null)
            return;


        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(RequestedEnrollmentDirection.class, "red");
        dql.fetchPath(DQLJoinType.inner, RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developForm().fromAlias("red"), "devform");
        dql.fetchPath(DQLJoinType.inner, RequestedEnrollmentDirection.entrantRequest().fromAlias("red"), "entrantrequest");
        dql.column("red");

        dql.where(in(property(RequestedEnrollmentDirection.entrantRequest().entrant().enrollmentCampaign().fromAlias("red")), enrollmentCampaigns));
        List<RequestedEnrollmentDirection> lst = dql.createStatement(getSession()).list();

        mapRequestedEnrollmentDirection = new HashMap<>();
        for (RequestedEnrollmentDirection red : lst) {
            Long key = red.getEntrantRequest().getId();

            List<RequestedEnrollmentDirection> lstRED;
            if (mapRequestedEnrollmentDirection.containsKey(key))
                lstRED = mapRequestedEnrollmentDirection.get(key);
            else {
                lstRED = new ArrayList<>();
                mapRequestedEnrollmentDirection.put(key, lstRED);
            }

            lstRED.add(red);
        }
    }

    @Override
    public void MakeMapRequestedEnrollmentDirectionExt
            (
                    List<EnrollmentCampaign> enrollmentCampaigns
            )
    {
        if (mapRequestedEnrollmentDirectionExt != null)
            return;


        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(RequestedEnrollmentDirectionExt.class, "red");
        dql.fetchPath(DQLJoinType.inner, RequestedEnrollmentDirectionExt.requestedEnrollmentDirection().enrollmentDirection().educationOrgUnit().developForm().fromAlias("red"), "devform");
        dql.fetchPath(DQLJoinType.inner, RequestedEnrollmentDirectionExt.requestedEnrollmentDirection().entrantRequest().fromAlias("red"), "entrantrequest");
        dql.column("red");

        dql.where(in(property(RequestedEnrollmentDirectionExt.requestedEnrollmentDirection().entrantRequest().entrant().enrollmentCampaign().fromAlias("red")), enrollmentCampaigns));
        List<RequestedEnrollmentDirectionExt> lst = dql.createStatement(getSession()).list();

        mapRequestedEnrollmentDirectionExt = new HashMap<>();
        for (RequestedEnrollmentDirectionExt red : lst) {
            Long key = red.getRequestedEnrollmentDirection().getId();

            if (!mapRequestedEnrollmentDirectionExt.containsKey(key))
                mapRequestedEnrollmentDirectionExt.put(key, red);
        }
    }

    @Override
    public RequestedEnrollmentDirectionExt getRequestedEnrollmentDirectionExt(RequestedEnrollmentDirection red)
    {
        if (mapRequestedEnrollmentDirectionExt == null)
            MakeMapRequestedEnrollmentDirectionExt(PackUnpackToFis.DAEMON_PARAMS.getEnrollmentCampaigns());

        Long key = red.getId();

        if (mapRequestedEnrollmentDirectionExt.containsKey(key))
            return mapRequestedEnrollmentDirectionExt.get(key);
        else
            return null;
    }


    private void _pacStringList
            (
                    Document doc
                    , Element application
                    , String parent
                    , String chields
                    , List<String> lstUid
            )
    {
        Element _el_parent = doc.createElement(parent);
        application.appendChild(_el_parent);

        for (String str : lstUid) {
            Element _el_chields = doc.createElement(chields);
            _el_chields.setTextContent(str);
            _el_parent.appendChild(_el_chields);
        }
    }


    private List<String> getCompetitiveGroupItemsUID
            (
                    List<EnrollmentDirection> lstEnrollmentDirections
                    , List<XMLCompetitiveGroup> lstCompetitiveGroup)
    {
        List<String> retVal = new ArrayList<String>();
        for (XMLCompetitiveGroup xcg : lstCompetitiveGroup) {
            List<XMLCompetitiveGroupItem> lst = xcg.getCompetitiveGroupItem();
            for (XMLCompetitiveGroupItem xitem : lst) {
                List<EnrollmentDirection> elist = xitem.getEnrollmentDirectionsList();
                for (EnrollmentDirection ed : elist) {
                    // ищем совпадение
                    if (lstEnrollmentDirections.contains(ed)) {
                        String uid = xitem.getUid();
                        if (!retVal.contains(uid))
                            retVal.add(uid);
                    }
                }
            }
        }
        return retVal;
    }


    /**
     * список UID конкурсных групп, в которых есть направление для приема
     *
     * @param lstEnrollmentDirections
     * @param lstCompetitiveGroup
     *
     * @return
     */
    private List<String> getCompetitiveGroupsUID
    (
            List<EnrollmentDirection> lstEnrollmentDirections,
            List<XMLCompetitiveGroup> lstCompetitiveGroup)
    {

        List<String> retVal = new ArrayList<String>();
        for (XMLCompetitiveGroup xcg : lstCompetitiveGroup) {
            List<XMLCompetitiveGroupItem> lst = xcg.getCompetitiveGroupItem();
            for (XMLCompetitiveGroupItem xitem : lst) {
                List<EnrollmentDirection> elist = xitem.getEnrollmentDirectionsList();
                for (EnrollmentDirection ed : elist) {
                    // ищем совпадение
                    if (lstEnrollmentDirections.contains(ed)) {
                        String uid = xcg.getUid();

                        if (!retVal.contains(uid))
                            retVal.add(uid);
                    }
                }
            }
        }
        return retVal;
    }

    private List<XMLCompetitiveGroup> getCompetitiveGroupsList
            (
                    List<EnrollmentDirection> lstEnrollmentDirections,
                    List<XMLCompetitiveGroup> lstCompetitiveGroup)
    {

        List<XMLCompetitiveGroup> retVal = new ArrayList<XMLCompetitiveGroup>();
        for (XMLCompetitiveGroup xcg : lstCompetitiveGroup) {
            List<XMLCompetitiveGroupItem> lst = xcg.getCompetitiveGroupItem();
            for (XMLCompetitiveGroupItem xitem : lst) {
                List<EnrollmentDirection> elist = xitem.getEnrollmentDirectionsList();
                for (EnrollmentDirection ed : elist) {
                    // ищем совпадение
                    if (lstEnrollmentDirections.contains(ed)) {
                        if (!retVal.contains(xcg))
                            retVal.add(xcg);
                    }
                }
            }
        }
        return retVal;
    }


    Map<Long, List<EnrollmentDirection>> mapEnrollmentDirection = null; // new HashMap<EntrantRequest, List<EnrollmentDirection>>();

    private List<EnrollmentDirection> getEnrollmentDirection
            (
                    EntrantRequest entrantrequest
                    , List<EnrollmentCampaign> enrollmentCampaigns
            )
    {

        if (mapEnrollmentDirection == null) {
            mapEnrollmentDirection = new HashMap<Long, List<EnrollmentDirection>>();

            DQLSelectBuilder dql = new DQLSelectBuilder();
            dql.fromEntity(RequestedEnrollmentDirection.class, "red");

            dql.fetchPath(DQLJoinType.inner, RequestedEnrollmentDirection.enrollmentDirection().fromAlias("red"), "ed");
            dql.fetchPath(DQLJoinType.inner, RequestedEnrollmentDirection.entrantRequest().fromAlias("red"), "erequest");

            dql.column("red");

            //dql.where(eq(property(RequestedEnrollmentDirection.entrantRequest().fromAlias("red")), value(entrantrequest)));
            dql.where(in(property(RequestedEnrollmentDirection.entrantRequest().entrant().enrollmentCampaign().fromAlias("red")), enrollmentCampaigns));

            List<RequestedEnrollmentDirection> lst = dql.createStatement(getSession()).list();

            int size = lst.size();
            int i = 0;
            for (RequestedEnrollmentDirection red : lst) {
                i++;
                System.out.println("fill map " + i + " is " + size);

                List<EnrollmentDirection> edList = null;
                Long key = red.getEntrantRequest().getId();

                if (mapEnrollmentDirection.containsKey(key))
                    edList = mapEnrollmentDirection.get(key);
                else {
                    edList = new ArrayList<EnrollmentDirection>();
                    mapEnrollmentDirection.put(key, edList);
                }

                EnrollmentDirection ed = red.getEnrollmentDirection();

                if (!edList.contains(ed))
                    edList.add(ed);
            }
        }

        Long key = entrantrequest.getId();
        if (mapEnrollmentDirection.containsKey(key))
            return mapEnrollmentDirection.get(key);
        else
            return new ArrayList<EnrollmentDirection>();
    }


    private Map<Long, Integer> mapEntrantRequestState = null;//new HashMap<Long, Integer>();
    private Map<Integer, EntrantState> mapEntrantState = new HashMap<Integer, EntrantState>();


    private EntrantState getEntrantRequestState(EntrantRequest entrantrequest)
    {

        if (mapEntrantRequestState == null) {
            mapEntrantRequestState = new HashMap<Long, Integer>();

            DQLSelectBuilder dql = new DQLSelectBuilder();
            dql.fromEntity(RequestedEnrollmentDirection.class, "red")
                    .column(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().id().fromAlias("red")))
                    .column(DQLFunctions.min(DQLExpressions.property(RequestedEnrollmentDirection.state().priority().fromAlias("red"))));
            // dql.where(eq(property(RequestedEnrollmentDirection.entrantRequest().fromAlias("red")), value(entrantrequest)));
            dql.group(property(RequestedEnrollmentDirection.entrantRequest().id().fromAlias("red")));


            List<Object[]> rows = dql.createStatement(getSession()).list();
            for (Object[] objs : rows) {
                Long id = (Long) objs[0];
                Integer priorInt = (Integer) objs[1];

                if (!mapEntrantRequestState.containsKey(id))
                    mapEntrantRequestState.put(id, priorInt);
            }

        }

        Integer priorInt = 5;
        Long id = entrantrequest.getId();
        if (mapEntrantRequestState.containsKey(id))
            priorInt = mapEntrantRequestState.get(id);

        if (mapEntrantState.containsKey(priorInt))
            return mapEntrantState.get(priorInt);
        else {
            EntrantState estate = get(EntrantState.class, EntrantState.P_PRIORITY, priorInt);
            mapEntrantState.put(priorInt, estate);

            return estate;
        }
    }


    /**
     * Дата, когда абитуриенто отозвал заявление
     *
     * @param entrantrequest
     *
     * @return
     */
    private Date getTakeAwayDate(EntrantRequest entrantrequest)
    {
        Date retVal = entrantrequest.getRegDate();
        return ToolsDao.addDays(retVal, 1);
    }


    @Override

    public Element packApplications
            (
                    boolean testMode
                    , boolean isEntranceTestResult
                    , boolean appDateWithoutTime

                    , Date dateRequestFrom
                    , Date dateRequestTo
                    , boolean whithoutSendedInFis
                    , boolean whithoutEntrantInOrder

                    , List<EnrollmentCampaign> enrollmentCampaigns
                    , EducationdirectionSpravType spravType
                    , Document doc
            )
    {

        // создаем кеши
        makeCache();
        PackEgeDocuments.Instanse().makeCache();
        PackEduDocuments.Instanse().makeCache();

        if (PackUnpackToFis.DAEMON_PARAMS.isEntranceTestResult())
            PackEntranceTestResult.Instanse().makeCache();

        Element applications = doc.createElement("Applications");
        // получим список приемных кампаний
        List<EnrollmentCampaign> ecList = enrollmentCampaigns;


        // получим конкурсные группы
        List<XMLCompetitiveGroup> lstCompetitiveGroup
                = CompetitiveGroupsDAO.Instanse().getCompetitiveGroups(ecList, spravType, PackUnpackToFis.DAEMON_PARAMS.isSuspendCompetitiveGroup());

        int abitCount = 0;
        int abitPosition = 0;
        for (EnrollmentCampaign ec : ecList) {

            List<EntrantRequest> entrantRequestList = getEntrantRequestList(ec);

            int size = entrantRequestList.size();
            for (EntrantRequest er : entrantRequestList) {

                abitPosition++;
                // нужно пропустить если
                if (testMode
                        &&
                        (
                                abitCount > PackUnpackToFis.ABIT_COUNT
                                        || abitPosition < PackUnpackToFis.ABIT_START_AT
                        )
                        )
                    continue;


                abitCount++;
                PackUnpackToFis.MSG_LOG = "Pack " + abitCount + " of " + size + " abitPosition = " + abitPosition + " abitCount = " + abitCount;

                Element application = packApplication(
                        doc
                        , er
                        , isEntranceTestResult
                        , appDateWithoutTime
                        , dateRequestFrom
                        , dateRequestTo
                        , whithoutSendedInFis
                        , whithoutEntrantInOrder
                        , lstCompetitiveGroup
                        , ecList
                        , spravType
                );

                if (application != null)
                    applications.appendChild(application);
            }
        }

        // удаляем кеши
        PackEgeDocuments.Instanse().clearCache();
        PackEduDocuments.Instanse().clearCache();
        PackEntranceTestResult.Instanse().clearCache();

        return applications;
    }


    /**
     * DQL билдер для получения списка заявлений
     *
     * @param ec
     *
     * @return
     */
    @Override
    public DQLSelectBuilder getEntrantRequestDQLList
    (
            List<EnrollmentCampaign> ecList
            , boolean notRf
            , boolean benefitStudents
            , boolean isOrdersOfAdmission

            , List<Integer> regNumber
            , List<Integer> excludeRegNumber

            , Date dateRequestFrom
            , Date dateRequestTo
            , boolean whithoutSendedInFis
            , boolean whithoutEntrantInOrder

            , boolean applayOrderBy
            , boolean testMode
            , List<XMLCompetitiveGroup> testModeCompetitiveGroupList
    )
    {
        // если у нас режим отладки,
        // то отберем только тех абитуриентов (заявления), которые попали в нужные нам конкурсные группы
        DQLSelectBuilder dqlIn = null;
        List<EnrollmentDirection> edArray = new ArrayList<EnrollmentDirection>();

        if (testMode
                && testModeCompetitiveGroupList != null
                && testModeCompetitiveGroupList.size() > 0
                )
        {

            // соберем id всех направлений подготовки для приема
            List<XMLCompetitiveGroup> lst = PackUnpackToFis.TESTMODE_CompetitiveGroup_List;

            for (XMLCompetitiveGroup xcg : lst) {
                List<XMLCompetitiveGroupItem> items = xcg.getCompetitiveGroupItem();
                for (XMLCompetitiveGroupItem xitem : items) {
                    List<EnrollmentDirection> edlist = xitem.getEnrollmentDirectionsList();
                    for (EnrollmentDirection ed : edlist) {
                        if (!edArray.contains(ed))
                            edArray.add(ed);
                    }
                }
            }
            dqlIn = new DQLSelectBuilder();
            dqlIn.fromEntity(RequestedEnrollmentDirection.class, "red");

            // заява
            dqlIn.column(property(RequestedEnrollmentDirection.entrantRequest().fromAlias("red")));
            // только по направлениям подготовки из конкурсных групп
            dqlIn.where(in(property(RequestedEnrollmentDirection.enrollmentDirection().fromAlias("red")), edArray));
        }


        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EntrantRequest.class, "er");
        dql.column("er");
        dql.where(in(property(EntrantRequest.entrant().enrollmentCampaign().id().fromAlias("er")), ecList));
        dql.where(eq(property(EntrantRequest.entrant().archival().fromAlias("er")), value(Boolean.FALSE)));

        if (applayOrderBy)
            dql.order(DQLExpressions.property(EntrantRequest.regNumber().fromAlias("er")), OrderDirection.asc);

        if (testMode
                && testModeCompetitiveGroupList != null
                && testModeCompetitiveGroupList.size() > 0
                )
            dql.where(in(property(EntrantRequest.id().fromAlias("er")), dqlIn.getQuery()));

        if (notRf)
            // только не паспорта РФ
            dql.where(ne(property(EntrantRequest.entrant().person().identityCard().cardType().code().fromAlias("er")), value("1")));


        if (!regNumber.isEmpty())
            dql.where(in(property(EntrantRequest.regNumber().fromAlias("er")), regNumber));


        if (!excludeRegNumber.isEmpty())
            dql.where(notIn(property(EntrantRequest.regNumber().fromAlias("er")), excludeRegNumber));

        if (dateRequestFrom != null)
            dql.where(ge(property(EntrantRequest.entrant().registrationDate().fromAlias("er")), valueDate(dateRequestFrom)));

        if (dateRequestTo != null)
            dql.where(le(property(EntrantRequest.entrant().registrationDate().fromAlias("er")), valueDate(dateRequestTo)));


        // возьмем только заявления с льготными студентами
        if (benefitStudents) {
            List<Long> ids = getBenefitEntrant(ecList);
            // есть оценки не только по ЕГЭ
            // есть вид конкурса, отличный от стандартного
            dql.where(in(property(EntrantRequest.id().fromAlias("er")), ids));
			/*
			List<Integer> ids = new ArrayList<Integer>();
			ids.add(10431);
			ids.add(10432);
			*/
            // dql.where(in(property(EntrantRequest.regNumber().fromAlias("er")), ids ));
        }

        // только включенные в приказ
        if (isOrdersOfAdmission)
            dql.where(in(property(EntrantRequest.id().fromAlias("er")), _getRequestInOrder().getQuery()));

        if (whithoutSendedInFis) {
            // исключить заявления, удачно переданные в ФИС
            DQLSelectBuilder dqlInFis = new DQLSelectBuilder();
            dqlInFis.fromEntity(EntrantRequest.class, "erfis");

            // <!-- результаты проверки заявлений/свидетельств ЕГЭ -->
            dqlInFis.joinEntity("erfis", DQLJoinType.inner, FisCheckEntrantRequest.class, "checkEr"
                    , DQLExpressions.eq(DQLExpressions.property(EntrantRequest.id().fromAlias("erfis"))
                    , DQLExpressions.property(FisCheckEntrantRequest.entrantRequest().id().fromAlias("checkEr"))));

            // джойним абитуриента
            dqlInFis.joinEntity("erfis", DQLJoinType.inner, Entrant.class, "entrantCheck"
                    , DQLExpressions.eq(DQLExpressions.property(EntrantRequest.entrant().id().fromAlias("erfis"))
                    , DQLExpressions.property(Entrant.id().fromAlias("entrantCheck"))));

            // джойним егэ сертификаты
            dqlInFis.joinEntity("erfis", DQLJoinType.inner, EntrantStateExamCertificate.class, "egesertificate"
                    , DQLExpressions.eq(DQLExpressions.property(EntrantRequest.entrant().id().fromAlias("erfis"))
                    , DQLExpressions.property(EntrantStateExamCertificate.entrant().id().fromAlias("egesertificate"))));


            // <entity name="fisCheckEgeSertificate" title="Проверка сертификатов ЕГЭ">
            dqlInFis.joinEntity("egesertificate", DQLJoinType.inner, FisCheckEgeSertificate.class, "checkEge"
                    , DQLExpressions.eq(DQLExpressions.property(EntrantStateExamCertificate.id().fromAlias("egesertificate"))
                    , DQLExpressions.property(FisCheckEgeSertificate.entrantStateExamCertificate().id().fromAlias("checkEge"))));

            // фильтры по статусам
            dqlInFis.where(eq(property(FisCheckEgeSertificate.fisEgeStatusCode().code().fromAlias("checkEge")), value(FisEgeStatusCodeCodes.NO_ERROR)));
            dqlInFis.where(eq(property(FisCheckEntrantRequest.fisAppStatusCode().code().fromAlias("checkEr")), DQLExpressions.value(FisAppStatusCodeCodes.NO_ERROR)));

            dqlInFis.column(property(EntrantRequest.id().fromAlias("erfis")));

            dql.where(notIn(property(EntrantRequest.id().fromAlias("er")), dqlInFis.getQuery()));

            // для отладки выводим массив номеров
            //List<Object> objs = dqlInFis.createStatement(getSession()).list();
            //System.out.println("уже передано в фис " + objs.size());


        }

        // исключить абитуриентов, включенных в приказ
        if (whithoutEntrantInOrder)
            dql.where(notIn(property(EntrantRequest.id().fromAlias("er")), _getRequestInOrder().getQuery()));

        return dql;
    }


    /**
     * qdl запрос по всем заявлениям, включенным в приказ
     *
     * @return
     */
    private DQLSelectBuilder _getRequestInOrder()
    {
        DQLSelectBuilder dqlInOrder = new DQLSelectBuilder();
        // выписка
        dqlInOrder.fromEntity(EnrollmentExtract.class, "extractfilter")
                // признак включения в приказ
                .where(DQLExpressions.isNotNull(DQLExpressions.property(EnrollmentExtract.paragraph().order().fromAlias("extractfilter"))))
                        // заявление
                .column(property(EnrollmentExtract.entity().requestedEnrollmentDirection().entrantRequest().fromAlias("extractfilter")));

        return dqlInOrder;
    }

    /**
     * @return
     */
    private List<Long> getBenefitEntrant(List<EnrollmentCampaign> ecList)
    {

        // оценки по олимпиадам
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(ChosenEntranceDiscipline.class, "ced")
                .column(property(ChosenEntranceDiscipline.chosenEnrollmentDirection().entrantRequest().id().fromAlias("ced")))
                        // источник оценки - зачтение олимпиады
                .where(DQLExpressions.eq(DQLExpressions.property(ChosenEntranceDiscipline.finalMarkSource().fromAlias("ced")), value(1L)))
                .where(DQLExpressions.in(DQLExpressions.property(ChosenEntranceDiscipline.enrollmentCampaignDiscipline().enrollmentCampaign().id().fromAlias("ced")), ecList));
        List<Long> lst = dql.createStatement(getSession()).list();

        List<Long> retVal = new ArrayList<Long>();

        // виды конкурса отличные от обычного
        DQLSelectBuilder dqlCt = new DQLSelectBuilder();
        dqlCt.fromEntity(RequestedEnrollmentDirection.class, "red");
        dqlCt.column(property(RequestedEnrollmentDirection.entrantRequest().id().fromAlias("red")));
        // не на общих основаниях
        dqlCt.where(ne(property(RequestedEnrollmentDirection.competitionKind().code().fromAlias("red")), value("5")));
        // по нужным нам заявлениям
        dqlCt.where(in(property(RequestedEnrollmentDirection.enrollmentDirection().enrollmentCampaign().id().fromAlias("red")), ecList));

        List<Long> lstRD = dqlCt.createStatement(getSession()).list();
        lst.addAll(lstRD);

        for (Long key : lst) {
            if (!retVal.contains(key))
                retVal.add(key);
        }
        return lst;
    }

    /**
     * Получаем список заявлений
     *
     * @param ec
     *
     * @return
     */
    private List<EntrantRequest> getEntrantRequestList(EnrollmentCampaign ec)
    {
        List<EnrollmentCampaign> ecList = new ArrayList<EnrollmentCampaign>();
        ecList.add(ec);

        DQLSelectBuilder dql = _getEntrantRequestDQLList(true, ecList);

        List<EntrantRequest> lst = dql.createStatement(getSession()).list();
        return lst;
    }

    private DQLSelectBuilder _getEntrantRequestDQLList(
            boolean applayOrderBy
            , List<EnrollmentCampaign> ecList)
    {

        return getEntrantRequestDQLList
                (
                        ecList
                        , PackUnpackToFis.DAEMON_PARAMS.isNotRf()
                        , PackUnpackToFis.DAEMON_PARAMS.isBenefitRequest()
                        , PackUnpackToFis.DAEMON_PARAMS.isOrdersOfAdmission()
                        , PackUnpackToFis.DAEMON_PARAMS.getRegNumbers()
                        , PackUnpackToFis.DAEMON_PARAMS.getExcludeRegNumbers()

                        , PackUnpackToFis.DAEMON_PARAMS.getDateRequestFrom()
                        , PackUnpackToFis.DAEMON_PARAMS.getDateRequestTo()
                        , PackUnpackToFis.DAEMON_PARAMS.isWhithoutSendedInFis()
                        , PackUnpackToFis.DAEMON_PARAMS.isWhithoutEntrantInOrder()
                        , applayOrderBy
                        , PackUnpackToFis.DAEMON_PARAMS.isTestMode()
                        , PackUnpackToFis.TESTMODE_CompetitiveGroup_List
                );
    }


    @Override
    public boolean isOriginalDocuments(EntrantRequest entrantrequest)
    {
        List<RequestedEnrollmentDirection> lst = mapRequestedEnrollmentDirection.get(entrantrequest.getId());

        for (RequestedEnrollmentDirection rd : lst) {
            if (rd.isOriginalDocumentHandedIn())
                return true;
        }

        return false;
    }


    @Override
    public String getOriginDocumentDateString(EntrantRequest entrantrequest) {

        if (isOriginalDocuments(entrantrequest)) {
            return ToolsDao.DateToString(entrantrequest.getRegDate());
        }
        else
            return null;
    }


    public void clearCache()
    {
        mapEnrollmentDirection = null;
        mapEntrantRequestState = null;
        mapEntrantState = new HashMap<Integer, EntrantState>();
        mapRequestedEnrollmentDirection = null;
        mapRequestedEnrollmentDirectionExt = null;
        mapRequestedEnrollmentDirectionNoExam = new HashMap<>();

        mapRequestedEnrollmentDirectionBenefit = new HashMap<Long, List<RequestedEnrollmentDirection>>();
        mapRequestedEnrollmentDirectionMarkOlimpiad = new HashMap<Long, List<RequestedEnrollmentDirection>>();

        mapIndividualAchievements = new HashMap<>();

        TargetOrganizationDAO.Instanse().clearCache();
        ApplicationCommonBenefit.Instanse().clearCache();

    }


    /**
     * создать кеш
     */
    public void makeCache()
    {
        // без вступ испытаний
        _makeCacheRequestedEnrollmentDirectionNoExam();

        // выбранные направления для приема по льготным условиям (не на общих основаниях)
        _makeCacheRequestedEnrollmentDirectionBenefit();

        // кеш по заявлениям, у которых есть зачтение олимпиады
        _makeCacheMarkOlimpiad();

        // индивидуальные достижения
        _makeCacheIndividualAchievements();

        ApplicationCommonBenefit.Instanse().makeCache();
    }


    // Есть диплом учасника олимпиады и он зачтен на одно
    // из направлений для приема
    // факт зачтения оценки значения не имеет
    Map<Long, List<RequestedEnrollmentDirection>> mapRequestedEnrollmentDirectionMarkOlimpiad = new HashMap<Long, List<RequestedEnrollmentDirection>>();
//	private void _makeCacheMarkOlimpiadOld() 
//	{
//	
//		List<EnrollmentCampaign> ecList = PackUnpackToFis.DAEMON_PARAMS.getEnrollmentCampaigns();
//		DQLSelectBuilder dqlIn = getEntrantRequestDQLList(ecList, false);
//		
//		
//		DQLSelectBuilder dql = new DQLSelectBuilder()
//		.fromEntity(ChosenEntranceDiscipline.class, "ced")
//		
//		.column(property(ChosenEntranceDiscipline.chosenEnrollmentDirection().fromAlias("ced")))
//		
//		// только по нужным заявлениям
//		.where(DQLExpressions.in(DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().entrantRequest().id().fromAlias("ced")),dqlIn.getQuery()))
//		
//		// источник оценки - зачтение олимпиады
//		.where(DQLExpressions.eq(DQLExpressions.property(ChosenEntranceDiscipline.finalMarkSource().fromAlias("ced")),value(1L)))
//		.order(DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().priority().fromAlias("ced")), OrderDirection.desc);
//		
//		List<RequestedEnrollmentDirection> lst = dql.createStatement(getSession()).list();
//		
//		for (RequestedEnrollmentDirection rd : lst)
//		{
//			// rd.getEnrollmentDirection();
//			
//			List<RequestedEnrollmentDirection> lstRequest = null;
//			long key = rd.getEntrantRequest().getId();
//			
//			if (mapRequestedEnrollmentDirectionMarkOlimpiad.containsKey(key))
//				lstRequest = mapRequestedEnrollmentDirectionMarkOlimpiad.get(key);
//			else
//			{
//				lstRequest = new ArrayList<RequestedEnrollmentDirection>();
//				mapRequestedEnrollmentDirectionMarkOlimpiad.put(key, lstRequest);
//			}
//		
//			if (!lstRequest.contains(rd))
//				lstRequest.add(rd);
//		}
//		
//	}

    private void _makeCacheMarkOlimpiad()
    {
        List<EnrollmentCampaign> ecList = PackUnpackToFis.DAEMON_PARAMS.getEnrollmentCampaigns();
        DQLSelectBuilder dqlIn = _getEntrantRequestDQLList(false, ecList);


        DQLSelectBuilder dqlRd = new DQLSelectBuilder();
        dqlRd.fromEntity(RequestedEnrollmentDirection.class, "red2")
                .joinPath(DQLJoinType.inner, RequestedEnrollmentDirection.enrollmentDirection().fromAlias("red2"), "ed2")
                .joinPath(DQLJoinType.inner, RequestedEnrollmentDirection.entrantRequest().entrant().fromAlias("red2"), "entrant2")

                        // прицепим дипломы
                .joinEntity("entrant2", DQLJoinType.inner, OlympiadDiploma.class, "diploma"
                        , DQLExpressions.eq(DQLExpressions.property(OlympiadDiploma.entrant().id().fromAlias("diploma")), DQLExpressions.property(Entrant.id().fromAlias("entrant2"))))

                        // 	зачтение диплома
                .joinEntity("diploma", DQLJoinType.inner, Discipline2OlympiadDiplomaRelation.class, "d2o", DQLExpressions.and(
                        DQLExpressions.eq(DQLExpressions.property("d2o", Discipline2OlympiadDiplomaRelation.diploma().id()), DQLExpressions.property(OlympiadDiploma.id().fromAlias("diploma"))),
                        DQLExpressions.eq(DQLExpressions.property("d2o", Discipline2OlympiadDiplomaRelation.enrollmentDirection().id()), DQLExpressions.property(EnrollmentDirection.id().fromAlias("ed2")))))


                .where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().id().fromAlias("red2")), dqlIn.getQuery()))


                        // по приоритетам в обратную сторону
                .order(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().id().fromAlias("red2")), OrderDirection.desc)
                .order(DQLExpressions.property(RequestedEnrollmentDirection.priority().fromAlias("red2")), OrderDirection.desc)
                .column("red2");


        List<RequestedEnrollmentDirection> lst = dqlRd.createStatement(getSession()).list();

        for (RequestedEnrollmentDirection rd : lst) {
            // rd.getEnrollmentDirection();

            List<RequestedEnrollmentDirection> lstRequest = null;
            long key = rd.getEntrantRequest().getId();

            if (mapRequestedEnrollmentDirectionMarkOlimpiad.containsKey(key))
                lstRequest = mapRequestedEnrollmentDirectionMarkOlimpiad.get(key);
            else {
                lstRequest = new ArrayList<RequestedEnrollmentDirection>();
                mapRequestedEnrollmentDirectionMarkOlimpiad.put(key, lstRequest);
            }

            if (!lstRequest.contains(rd))
                lstRequest.add(rd);
        }

    }


    // выбранные направления для приема по льготным условиям (не на общих основаниях)
    // и надо исключить олимпиды
    Map<Long, List<RequestedEnrollmentDirection>> mapRequestedEnrollmentDirectionBenefit = new HashMap<Long, List<RequestedEnrollmentDirection>>();

    private void _makeCacheRequestedEnrollmentDirectionBenefit() {

        List<EnrollmentCampaign> ecList = PackUnpackToFis.DAEMON_PARAMS.getEnrollmentCampaigns();
        DQLSelectBuilder dqlIn = _getEntrantRequestDQLList(false, ecList);

        // считаем все направления для приема, по которым вид конкурса отличен от
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(RequestedEnrollmentDirection.class, "red");
		
		/*
		List<String> lstcompetitionKind = new ArrayList<String>();
		lstcompetitionKind.add("5"); // на общих основаниях 
		lstcompetitionKind.add("1"); // без вступительных испытаний (это олимпиады)
		*/
        // не на общих основаниях
        // dql.where(notIn(property(RequestedEnrollmentDirection.competitionKind().code().fromAlias("red")), lstcompetitionKind ));

        // с 2014 года льготы только на направлениях у которого галочка - особые права
        DQLSelectBuilder dqlInBenefit = new DQLSelectBuilder();
        dqlInBenefit.fromEntity(RequestedEnrollmentDirectionExt.class, "benefit")
                // имеет особые права (читай льготы)
                .where(eq(property(RequestedEnrollmentDirectionExt.haveSpecialRights().fromAlias("benefit")), DQLExpressions.value(Boolean.TRUE)))
                        // только с особыми правами
                .column(property(RequestedEnrollmentDirectionExt.requestedEnrollmentDirection().id().fromAlias("benefit")));

        // только особые права
        dql.where(in(property(RequestedEnrollmentDirection.id().fromAlias("red")), dqlInBenefit.getQuery()));

        // по нужным нам заявлениям
        dql.where(in(property(RequestedEnrollmentDirection.entrantRequest().id().fromAlias("red")), dqlIn.getQuery()));
        dql.order(DQLExpressions.property(RequestedEnrollmentDirection.priority().fromAlias("red")), OrderDirection.desc);
        List<RequestedEnrollmentDirection> lst = dql.createStatement(getSession()).list();

        for (RequestedEnrollmentDirection rd : lst) {
            List<RequestedEnrollmentDirection> lstRequest = null;
            long key = rd.getEntrantRequest().getId();

            if (mapRequestedEnrollmentDirectionBenefit.containsKey(key))
                lstRequest = mapRequestedEnrollmentDirectionBenefit.get(key);
            else {
                lstRequest = new ArrayList<RequestedEnrollmentDirection>();
                mapRequestedEnrollmentDirectionBenefit.put(key, lstRequest);
            }

            if (!lstRequest.contains(rd))
                lstRequest.add(rd);
        }
    }


    Map<Long, List<Entrant2IndividualAchievements>> mapIndividualAchievements = new HashMap<>();

    private void _makeCacheIndividualAchievements()
    {
        List<EnrollmentCampaign> ecList = PackUnpackToFis.DAEMON_PARAMS.getEnrollmentCampaigns();

        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(Entrant2IndividualAchievements.class, "red");

        // по нужным приемкам
        dql.where(in(property(Entrant2IndividualAchievements.entrant().enrollmentCampaign().fromAlias("red")), ecList));

        List<Entrant2IndividualAchievements> lst = dql.createStatement(getSession()).list();

        for (Entrant2IndividualAchievements rd : lst) {
            List<Entrant2IndividualAchievements> lstIA = null;
            long key = rd.getEntrant().getId();


            if (mapIndividualAchievements.containsKey(key))
                lstIA = mapIndividualAchievements.get(key);
            else {
                lstIA = new ArrayList<Entrant2IndividualAchievements>();
                mapIndividualAchievements.put(key, lstIA);
            }

            if (!lstIA.contains(rd))
                lstIA.add(rd);
        }
    }

    @Override
    public List<Entrant2IndividualAchievements> getEntrantIndividualAchievements(Entrant entrant)
    {
        long key = entrant.getId();
        if (mapIndividualAchievements.containsKey(key))
            return mapIndividualAchievements.get(key);
        else
            return null;
    }

    @Override
    public boolean hasIndividualAchievements(Entrant entrant)
    {
        List<Entrant2IndividualAchievements> lst = getEntrantIndividualAchievements(entrant);
        if (lst == null || lst.isEmpty())
            return false;
        else
            return true;
    }

    // абитуриенты без вступительных испытаний
    Map<Long, List<RequestedEnrollmentDirection>> mapRequestedEnrollmentDirectionNoExam = new HashMap<Long, List<RequestedEnrollmentDirection>>();

    private void _makeCacheRequestedEnrollmentDirectionNoExam()
    {
        List<EnrollmentCampaign> ecList = PackUnpackToFis.DAEMON_PARAMS.getEnrollmentCampaigns();
        DQLSelectBuilder dqlIn = _getEntrantRequestDQLList(false, ecList);

        // считаем все направления для приема, по которым вид конкурса отличен от
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(RequestedEnrollmentDirection.class, "red");

        List<String> lstcompetitionKind = new ArrayList<String>();
        //lstcompetitionKind.add("5"); // на общих основаниях
        lstcompetitionKind.add("1"); // без вступительных испытаний (это олимпиады)

        dql.where(in(property(RequestedEnrollmentDirection.competitionKind().code().fromAlias("red")), lstcompetitionKind));

        // по нужным нам заявлениям
        dql.where(in(property(RequestedEnrollmentDirection.entrantRequest().id().fromAlias("red")), dqlIn.getQuery()));
        dql.order(DQLExpressions.property(RequestedEnrollmentDirection.priority().fromAlias("red")), OrderDirection.desc);
        List<RequestedEnrollmentDirection> lst = dql.createStatement(getSession()).list();

        for (RequestedEnrollmentDirection rd : lst) {
            List<RequestedEnrollmentDirection> lstRequest = null;
            long key = rd.getEntrantRequest().getId();

            if (mapRequestedEnrollmentDirectionNoExam.containsKey(key))
                lstRequest = mapRequestedEnrollmentDirectionNoExam.get(key);
            else {
                lstRequest = new ArrayList<RequestedEnrollmentDirection>();
                mapRequestedEnrollmentDirectionNoExam.put(key, lstRequest);
            }

            if (!lstRequest.contains(rd))
                lstRequest.add(rd);
        }
    }

}
