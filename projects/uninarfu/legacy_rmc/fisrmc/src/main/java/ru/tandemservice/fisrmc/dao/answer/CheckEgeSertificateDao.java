package ru.tandemservice.fisrmc.dao.answer;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.fisrmc.component.UploadFis2TU.Pub.DefaultParser;
import ru.tandemservice.fisrmc.entity.FisAnswerTofisPackages;
import ru.tandemservice.fisrmc.entity.FisCheckEgeSertificate;
import ru.tandemservice.fisrmc.entity.catalog.FisEgeStatusCode;
import ru.tandemservice.fisrmc.entity.catalog.codes.FisEgeStatusCodeCodes;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.catalog.StateExamType;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate;
import ru.tandemservice.uniec.entity.entrant.StateExamSubjectMark;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class CheckEgeSertificateDao extends BaseDao implements ICheckEgeSertificateDao {

    protected FisEgeStatusCode statusOk;
    protected FisEgeStatusCode statusBadCommon;
    protected FisEgeStatusCode statusBadMark;
    protected StateExamType stateExamType;

    @Override
    @Transactional
    public void doAction(FisAnswerTofisPackages answer, boolean testMode) throws Exception {
        if (this.statusOk == null)
            this.statusOk = getCatalogItem(FisEgeStatusCode.class, FisEgeStatusCodeCodes.NO_ERROR);
        if (this.statusBadCommon == null)
            this.statusBadCommon = getCatalogItem(FisEgeStatusCode.class, FisEgeStatusCodeCodes.ERROR_COMMON);
        if (this.statusBadMark == null)
            this.statusBadMark = getCatalogItem(FisEgeStatusCode.class, FisEgeStatusCodeCodes.ERROR_MARK);
        if (this.stateExamType == null)
            this.stateExamType = getCatalogItem(StateExamType.class, UniecDefines.STATE_EXAM_TYPE_FIRST);

        Map<String, Long> okCheckMap = getOkCertMap();

        List<String> errorList = new ArrayList<>();
        List<String> logList = new ArrayList<>();

        processCheckList(answer, okCheckMap, testMode, errorList, logList);
        processNewCerts(answer, okCheckMap, testMode, errorList, logList);

        String comment = "";
        if (testMode) {
            comment += "Обработка в тестовом режиме (в базу данных изменения не вносятся);";
            logList.add(comment);
        }

        if (errorList.isEmpty())
            comment += " Ошибок нет;";
        if (logList.isEmpty())
            comment += " Отчет пуст;";

        saveAction(answer, errorList, logList, comment);
    }

    //////////////////////// GetEgeDocuments ///////////////////////////////////////////////////////////////////////

    protected void processNewCerts(FisAnswerTofisPackages answer, Map<String, Long> okCheckMap, boolean testMode, List<String> errorList, List<String> logList) throws Exception {
        List<EgeSertificateInfo> getList = getGetSertificateList(answer);

        Map<String, StateExamSubject> cacheSubject = new HashMap<>();

        for (EgeSertificateInfo info : getList) {
            //кривой серт не добавляем
            if (!"0".equals(info.getStatus()))
                continue;

            //есть ошибки
            if (!StringUtils.isEmpty(info.getError()))
                continue;

            //уже существует
            EntrantRequest entrantRequest = getEntrantRequest(info.getRequestNumber(), info.getRequestDate());
            if (entrantRequest == null) {
                errorList.add("Не найдено заявление " + info.getRequestNumber() + " для нового сертификата " + info.getNumber());
                continue;
            }
            EntrantStateExamCertificate cert = getCertificate(info.getNumber(), entrantRequest.getEntrant());

            // если сертификат есть, ничего не исправляем уходит как есть
            if (cert != null) {
                checkMarks(cert, info, cacheSubject, errorList, logList);
                continue;
            }

            cert = new EntrantStateExamCertificate();

            cert.setEntrant(entrantRequest.getEntrant());
            cert.setNumber(info.getNumber().replace("-", ""));
            cert.setRegistrationDate(new Date());
            cert.setOriginal(false);
            cert.setStateExamType(this.stateExamType);
            //cert.setSent(true);
            //cert.setIssuanceDate(new Date());//TODO: а какая дата выдачи? есть только год

            logList.add("Добавлен сертификат " + info.getNumber() + " для заявления " + entrantRequest.getRegNumber());

            //сохраняем оценки
            List<StateExamSubjectMark> addList = new ArrayList<>();
            for (int i = 0; i < info.getDisciplineList().size(); i++) {
                StateExamSubjectMark mark = new StateExamSubjectMark();
                mark.setMark(info.getMarkList().get(i));
                String subjTitle = info.getDisciplineList().get(i);

                if (!cacheSubject.containsKey(subjTitle)) {
                    StateExamSubject subject = get(StateExamSubject.class, StateExamSubject.title(), subjTitle);
                    cacheSubject.put(subjTitle, subject);
                }

                StateExamSubject subject = cacheSubject.get(subjTitle);
                if (subject == null) {
                    errorList.add("Не найден предмет " + subjTitle + " для сертификата " + info.getNumber());
                    continue;
                }
                mark.setSubject(subject);

                addList.add(mark);
                logList.add("Добавлена оценка " + mark.getMark() + " по предмету " + mark.getSubject().getTitle() + " для сертификата " + info.getNumber());
            }

            if (!testMode) {
                save(cert);
                // getSession().flush();
                for (StateExamSubjectMark mark : addList) {
                    mark.setCertificate(cert);
                    save(mark);
                }
                insertCheckEge(cert, info.getStatus(), null);

                //getSession().flush();
            }
        }
    }

    protected boolean checkMarks(EntrantStateExamCertificate cert, EgeSertificateInfo info, Map<String, StateExamSubject> cacheSubject, List<String> errorList, List<String> logList) {
        /*
		List<StateExamSubjectMark> markList = getList(StateExamSubjectMark.class, StateExamSubjectMark.certificate(), cert);
		for (StateExamSubjectMark mark : markList) {
			//TODO: checkmarks
		}
		*/
        return true;
    }

    protected List<EgeSertificateInfo> getGetSertificateList(FisAnswerTofisPackages answer) throws Exception {
        byte[] content = answer.getXmlAnswer().getContent();
        if (content == null)
            throw new ApplicationException("Файл ответа пуст");
        String xml = new String(content, "UTF-8");

        //загрузка общих данных по сертификатам
        List<Map<String, Object>> list = DefaultParser.parse(new GetEgeDocumentParser(xml));

        Map<String, EgeSertificateInfo> certMap = new HashMap<>();
        for (Map<String, Object> map : list) {
            String requestNumber = (String) map.get("number");
            String requestDate = (String) map.get("date");
            String error = (String) map.get("error");

            List<String> numberList = (List<String>) map.get("docNumber");
            if (numberList == null)
                numberList = new ArrayList<>();

            for (String number : numberList) {
                EgeSertificateInfo info = new EgeSertificateInfo(requestNumber, requestDate, number, null);
                info.setError(error);

                certMap.put(info.getNumber(), info);
            }
        }

        //загрузка оценок и др. инфы
        list = DefaultParser.parse(new GetEgeDocumentParser.GetEgeDocumentAdditionalParser(xml));
        for (Map<String, Object> map : list) {
            String certNumber = (String) map.get("docNumber");
            String certStatus = (String) map.get("docStatus");
            Integer certYear = (map.get("docYear") != null) ? Integer.parseInt((String) map.get("docYear")) : null;

            if (!"0".equals(certStatus))
                continue;

            EgeSertificateInfo info = certMap.get(certNumber);
            info.setNumber(certNumber);
            info.setStatus(certStatus);
            info.setYear(certYear);

            List<String> discList = (List<String>) map.get("subject");
            if (discList == null)
                discList = new ArrayList<>();
            List<String> markList = (List<String>) map.get("mark");

            for (int i = 0; i < discList.size(); i++) {
                String discipline = discList.get(i);

                // добавляем только уникальные дисциплины
                if (!info.getDisciplineList().contains(discipline)) {
                    info.getDisciplineList().add(discipline);
                    info.getMarkList().add(Double.valueOf(markList.get(i)).intValue());
                }
            }
        }

        return new ArrayList<>(certMap.values());
    }

    ////////////////////////// EgeDocumentCheckResults /////////////////////////////

    protected void processCheckList(FisAnswerTofisPackages answer, Map<String, Long> okCheckMap, boolean testMode, List<String> errorList, List<String> logList) throws Exception {
        List<EgeSertificateInfo> checkList = getCheckSertificateList(answer);

        for (EgeSertificateInfo info : checkList) {
            boolean isOk = "0".equals(info.getStatus());

            //серт без ошибок и есть такая же запись
            if (isOk && okCheckMap.containsKey(info.getNumber().replace("-", "")))
                continue;

            EntrantRequest entrantRequest = getEntrantRequest(info.getRequestNumber(), info.getRequestDate());
            if (entrantRequest == null) {
                errorList.add("Не найдено заявление " + info.getRequestNumber() + " для сертификата " + info.getNumber());
                continue;
            }
            EntrantStateExamCertificate cert = getCertificate(info.getNumber(), entrantRequest.getEntrant());
            if (cert == null) {
                errorList.add("Не найден сертификат " + info.getNumber() + " для заявления " + info.getRequestNumber());
                continue;
            }

            String markComment = "";
            if (!info.getMarkCommentList().isEmpty())
                markComment += StringUtils.join(info.getMarkCommentList(), ", ");

            FisCheckEgeSertificate check;
            if (!testMode) {
                deleteCheckEge(cert.getId());

                String checkComment = "";
                checkComment = info.getMessage();
                if (!markComment.isEmpty()) {
                    if (checkComment == null)
                        checkComment = markComment;
                    else
                        checkComment += " " + markComment;
                }
                check = insertCheckEge(cert, info.getStatus(), checkComment);
                if (isOk)
                    okCheckMap.put(check.getEntrantStateExamCertificate().getNumber().replace("-", ""), check.getEntrantStateExamCertificate().getId());
            }

            String logStr = "Проверен сертификат: " + info.getNumber() + ", заявление " + info.getRequestNumber() + " статус ФИС: " + info.getStatus();
            if (!StringUtils.isEmpty(info.getMessage()))
                logStr += ", ошибка: " + info.getMessage();

            if (!markComment.isEmpty())
                logStr += markComment;

            logList.add(logStr);
        }
    }

    protected EntrantStateExamCertificate getCertificate(String certNumber, Entrant entrant) {
        if (certNumber.equals("78-000005456-11")) certNumber.length();

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EntrantStateExamCertificate.class, "e")
                .where(DQLExpressions.eq(
                        DQLExpressions.property(EntrantStateExamCertificate.entrant().fromAlias("e")),
                        DQLExpressions.value(entrant)
                ))
                .where(DQLExpressions.eq(
                        DQLExpressions.property(EntrantStateExamCertificate.number().fromAlias("e")),
                        DQLExpressions.value(certNumber.replace("-", ""))
                ))
                .column("e");

        List<EntrantStateExamCertificate> list = getList(dql);
        return (list.size() == 1) ? list.get(0) : null;
    }

    protected FisCheckEgeSertificate insertCheckEge(EntrantStateExamCertificate cert, String status, String message) {
        FisCheckEgeSertificate check = new FisCheckEgeSertificate();

        check.setCheckDate(new Date());
        check.setEntrantStateExamCertificate(cert);

        if ("0".equals(status)) //ok
            check.setFisEgeStatusCode(this.statusOk);
        else if ("2".equals(status)) //плохая оценка
            check.setFisEgeStatusCode(this.statusBadMark);
        else
            check.setFisEgeStatusCode(this.statusBadCommon);

        check.setErrorCodeEge(Integer.parseInt(status));
        check.setErrorMessageEge(message);

        saveOrUpdate(check);
        return check;
    }

    protected void deleteCheckEge(Long certId) {
        new DQLDeleteBuilder(FisCheckEgeSertificate.class)
                .where(DQLExpressions.eq(DQLExpressions.property(FisCheckEgeSertificate.entrantStateExamCertificate().id()), DQLExpressions.value(certId)))
                .createStatement(getSession())
                .execute();
    }

    protected List<EgeSertificateInfo> getCheckSertificateList(FisAnswerTofisPackages answer) throws Exception {
        byte[] content = answer.getXmlAnswer().getContent();
        if (content == null)
            throw new ApplicationException("Файл ответа пуст");
        String xml = new String(content, "UTF-8");

        //загрузка общих данных по сертификатам
        List<Map<String, Object>> list = DefaultParser.parse(new CheckEgeDocumentParser(xml));

        Map<String, EgeSertificateInfo> certMap = new HashMap<>();
        for (Map<String, Object> map : list) {
            String requestNumber = (String) map.get("number");
            String requestDate = (String) map.get("date");

            List<String> numberList = (List<String>) map.get("docNumber");
            if (numberList == null)
                numberList = new ArrayList<>();

            for (String number : numberList) {
                EgeSertificateInfo info = new EgeSertificateInfo(requestNumber, requestDate, number, null);
                certMap.put(info.getNumber(), info);
            }
        }

        //загрузка статусов сертификатов
        list = DefaultParser.parse(new CheckEgeDocumentParser.CheckEgeDocumentAdditionalParser(xml));
        for (Map<String, Object> map : list) {
            String certNumber = (String) map.get("docNumber");
            String certStatus = (String) map.get("docStatus");
            String certMessage = (String) map.get("docMessage");

            EgeSertificateInfo info = certMap.get(certNumber);
            info.status = certStatus;
            info.message = certMessage;

            List<String> discList = (List<String>) map.get("markcomment");
            if (discList != null) {
                for (String markComment : discList) {
                    if (!info.getMarkCommentList().contains(markComment))
                        info.getMarkCommentList().add(markComment);
                }
            }
        }

        return new ArrayList<>(certMap.values());
    }

    protected Map<String, Long> getOkCertMap() {
        Map<String, Long> mapResult = new HashMap<>();

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(FisCheckEgeSertificate.class, "e")
                .where(DQLExpressions.eq(
                        DQLExpressions.property(FisCheckEgeSertificate.fisEgeStatusCode().code().fromAlias("e")),
                        DQLExpressions.value(FisEgeStatusCodeCodes.NO_ERROR)
                ))
                .column(FisCheckEgeSertificate.entrantStateExamCertificate().id().fromAlias("e").s())
                .column(FisCheckEgeSertificate.entrantStateExamCertificate().number().fromAlias("e").s());

        List<Object[]> list = getList(dql);
        for (Object[] arr : list) {
            Long id = (Long) arr[0];
            String number = (String) arr[1];

            mapResult.put(number, id);
        }

        return mapResult;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    protected static class EgeSertificateInfo {
        public static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        private Integer requestNumber;
        private Date requestDate;

        private String number;
        private String status;
        private Integer year;
        private String message;
        private String error;

        private List<String> disciplineList = new ArrayList<>();
        private List<Integer> markList = new ArrayList<>();
        private List<String> markCommentList = new ArrayList<>();

        public EgeSertificateInfo(String requestNumberStr, String requestDateStr, String number, String status) throws ParseException {
            this.requestNumber = Integer.parseInt(requestNumberStr);
            this.requestDate = SDF.parse(requestDateStr);

            this.number = number;
            this.status = status;
        }

        public String getError() {
            return error;
        }

        public void setError(String error) {
            this.error = error;
        }

        public Integer getRequestNumber() {
            return requestNumber;
        }

        public void setRequestNumber(Integer requestNumber) {
            this.requestNumber = requestNumber;
        }

        public Date getRequestDate() {
            return requestDate;
        }

        public void setRequestDate(Date requestDate) {
            this.requestDate = requestDate;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<String> getDisciplineList() {
            return disciplineList;
        }

        public void setDisciplineList(List<String> disciplineList) {
            this.disciplineList = disciplineList;
        }

        public List<Integer> getMarkList() {
            return markList;
        }

        public void setMarkList(List<Integer> markList) {
            this.markList = markList;
        }

        public Integer getYear() {
            return year;
        }

        public void setYear(Integer year) {
            this.year = year;
        }

        public void setMarkCommentList(List<String> markCommentList) {
            this.markCommentList = markCommentList;
        }

        public List<String> getMarkCommentList() {
            return markCommentList;
        }

    }
}
