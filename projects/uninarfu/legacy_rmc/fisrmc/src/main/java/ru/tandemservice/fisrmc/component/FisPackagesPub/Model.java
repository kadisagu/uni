package ru.tandemservice.fisrmc.component.FisPackagesPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.fisrmc.entity.FisAnswerTofisPackages;
import ru.tandemservice.fisrmc.entity.FisPackages;

@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "pkgId")
})

public class Model {
    private IDataSettings settings;

    Long pkgId;
    FisPackages pkg;

    private DynamicListDataSource<FisAnswerTofisPackages> dataSource;


    public Long getPkgId() {
        return pkgId;
    }

    public void setPkgId(Long pkgId) {
        this.pkgId = pkgId;
    }

    public FisPackages getPkg() {
        return pkg;
    }

    public void setPkg(FisPackages pkg) {
        this.pkg = pkg;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setDataSource(DynamicListDataSource<FisAnswerTofisPackages> dataSource) {
        this.dataSource = dataSource;
    }

    public DynamicListDataSource<FisAnswerTofisPackages> getDataSource() {
        return dataSource;
    }
}
