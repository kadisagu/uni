package ru.tandemservice.fisrmc.component.settings.EnrollmentCampaignCopy;

public class Model extends ru.tandemservice.uniec.component.settings.EnrollmentCampaignCopy.Model {

    private String requestPrefix;

    public String getRequestPrefix() {
        return requestPrefix;
    }

    public void setRequestPrefix(String requestPrefix) {
        this.requestPrefix = requestPrefix;
    }

}
