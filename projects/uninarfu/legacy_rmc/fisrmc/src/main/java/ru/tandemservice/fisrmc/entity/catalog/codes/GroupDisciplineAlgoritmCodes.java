package ru.tandemservice.fisrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Алгоритм расчета итогового балла по группе дисциплин"
 * Имя сущности : groupDisciplineAlgoritm
 * Файл data.xml : catalogs.data.xml
 */
public interface GroupDisciplineAlgoritmCodes
{
    /** Константа кода (code) элемента : сумма баллов (title) */
    String SUMM = "0";

    Set<String> CODES = ImmutableSet.of(SUMM);
}
