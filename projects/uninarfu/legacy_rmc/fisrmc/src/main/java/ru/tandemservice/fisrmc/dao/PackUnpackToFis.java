package ru.tandemservice.fisrmc.dao;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.tandemservice.fisrmc.dao.packApplications.Applications;
import ru.tandemservice.fisrmc.entity.DisciplineMinMark2Olimpiad;
import ru.tandemservice.fisrmc.entity.Educationdirectionfis;
import ru.tandemservice.fisrmc.entity.FisPackages;
import ru.tandemservice.fisrmc.entity.FisPackages2enrollmentCampaign;
import ru.tandemservice.fisrmc.entity.catalog.codes.EnrolmentcomnainfisCodes;
import ru.tandemservice.fisrmc.entity.catalog.*;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaignPeriod;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class PackUnpackToFis
        extends UniBaseDao
        implements IPackUnpackToFis
{
    public static final String SUSPEND_ERRORS_TEMPLATE = "fisrmcSuspendedErrors";
    public static final String LOCKER_NAME = "IPackUnpackToFis.lock";

    public static String MSG_LOG = "";
    public static Long LAST_ID = 0L;
    private static List<String> xmlPkgErrors = new ArrayList<>();

    public static void AddPkgError(String msg)
    {
        if (!xmlPkgErrors.contains(msg))
            xmlPkgErrors.add(msg);
    }

    public static final SyncDaemon DAEMON = new SyncDaemon(PackUnpackToFis.class.getName(), 1000 * 24 * 60 /* 1 раз в 3 года*/, LOCKER_NAME) {
        @Override
        protected void main()
        {
            if (!DaemonParams.NEED_RUN)
                return;
            try {
                // получим bean
                if (DAEMON_PARAMS != null) {
                    MSG_LOG = "Начали работу";
                    xmlPkgErrors = new ArrayList<>();

                    FisPackages pkg = PackUnpackToFis.Instanse().PackEnrollmentCampaignToDatabase
                            (
                                    DAEMON_PARAMS
                            );

                    LAST_ID = pkg.getId();
                    MSG_LOG = "Упаковка завершена " + new Date();
                }

            }
            catch (Exception e) {

                this.logger.error(e.getMessage(), e);
                Debug.exception(e.getMessage(), e);

                MSG_LOG = "Ошибка упаковки " + e.getMessage();
                AddPkgError(e.getMessage());

                PackUnpackToFis.Instanse().saveCurrentFisPackages(
                        DAEMON_PARAMS
                        , null
                );
            }
            finally {
                DaemonParams.NEED_RUN = false;
            }
        }
    };

    public static int CG_COUNT = 0;
    public static int ABIT_COUNT = 0;
    public static int ABIT_START_AT = 0;
    public static List<XMLCompetitiveGroup> TESTMODE_CompetitiveGroup_List = null;


    public static DaemonParams DAEMON_PARAMS = null;

    private static IPackUnpackToFis _IPackUnpackToFis = null;

    public static IPackUnpackToFis Instanse()
    {


        if (_IPackUnpackToFis == null)
            _IPackUnpackToFis = (IPackUnpackToFis) ApplicationRuntime.getBean("fisDao");
        return _IPackUnpackToFis;
    }


    /**
     * Сведения об объеме и структуре прием
     */
    private Element packAdmissionInfo
    (
            List<EnrollmentCampaign> lst
            , EducationdirectionSpravType spravType
            , Document doc
    )
    {
        Element admissionInfo = doc.createElement("AdmissionInfo");


        Element admissionVolume = packAdmissionVolume(lst, spravType, doc);
        admissionInfo.appendChild(admissionVolume);

        Element competitiveGroups = packCompetitiveGroups(lst, spravType, doc);
        admissionInfo.appendChild(competitiveGroups);


        return admissionInfo;
    }


    private Element packCompetitiveGroups(List<EnrollmentCampaign> lst
            , EducationdirectionSpravType spravType
            , Document doc)
    {
        Element competitiveGroups = doc.createElement("CompetitiveGroups");
        List<XMLCompetitiveGroup> lstCompetitiveGroups = CompetitiveGroupsDAO.Instanse().getCompetitiveGroups(lst, spravType, DAEMON_PARAMS.isSuspendCompetitiveGroup());

        int i = 0;

        if (DAEMON_PARAMS.isTestMode())
            TESTMODE_CompetitiveGroup_List = new ArrayList<>();

        List<String> hashName = new ArrayList<>();
        List<String> hashCompetitiveGroupTargetItem = new ArrayList<>();

        int nextName = 0;


        // в тестовом режиме прогоняем не более n конкурсных групп
        for (XMLCompetitiveGroup competitiveGroup : lstCompetitiveGroups) {
            if (i > (CG_COUNT - 1) && DAEMON_PARAMS.isTestMode())
                continue;

            if (DAEMON_PARAMS.isTestMode())
                TESTMODE_CompetitiveGroup_List.add(competitiveGroup);

            i++;
            /*
			// сформируем перечень направлений подготовки для конкурсной группы
			//<TODO> при этом нужно исключить целевой прием
			List<XMLCompetitiveGroupItem> items 
				= CompetitiveGroupsDAO.Instanse().getCompetitiveGroupItems(competitiveGroup);
			competitiveGroup.setCompetitiveGroupItem(items);

			if (items==null || items.size()==0)
				continue;
			*/

            Element competitiveGroupElement = doc.createElement("CompetitiveGroup");
            competitiveGroups.appendChild(competitiveGroupElement);

            _packUid(doc, competitiveGroupElement, competitiveGroup.getUid());
            _packCampaignUID(doc, competitiveGroupElement, competitiveGroup.getEnrollmentCampaign());
            _packCourse(doc, competitiveGroupElement, competitiveGroup.getEnrollmentCampaign());

            String tempName = competitiveGroup.getName();
            if (hashName.contains(tempName)) {
                nextName++;
                tempName += "(" + nextName + ")";

            }

            hashName.add(tempName);

            _packName(doc, competitiveGroupElement, tempName);

            Element itemsroot = doc.createElement("Items");
            competitiveGroupElement.appendChild(itemsroot);

            // перечень направлений подготовки
            // в конкурсной группе
            List<XMLCompetitiveGroupItem> items = competitiveGroup.getCompetitiveGroupItem();
            for (XMLCompetitiveGroupItem competitiveGroupItem : items) {
                // создаем у Items Дочерние CompetitiveGroupItem
                packCompetitiveGroupItem(doc, itemsroot, competitiveGroupItem);
            }

            // сведения о целевом наборе
            ITargetOrganizationDAO targetOrganizationDAO = TargetOrganizationDAO.Instanse();
            List<XMLTargetOrganization> lstTargetOrganizations = targetOrganizationDAO.getTargetOrganizations(competitiveGroup, spravType);
            if (lstTargetOrganizations != null && lstTargetOrganizations.size() > 0) {
                // упаковска сведений по целевому приему
                packTargetOrganizationItem(doc, competitiveGroupElement, competitiveGroup, lstTargetOrganizations, hashCompetitiveGroupTargetItem);
                System.out.println("есть целевые организации");
            }


            // сведения о льготах
            List<XMLCommonBenefit> lstCommonBenefit = CommonBenefitDAO.Instanse().getCommonOrEntranceTestBenefit(competitiveGroup, null, true);
            packCommonOrEntranceTestBenefit(doc, competitiveGroupElement, lstCommonBenefit, true, null);

            // вступительные испытания конкурсной группы
            List<XMLEntranceTestItem> lstEntranceTestItem = EntranceTestItemDAO.Instanse().getEntranceTestItem(competitiveGroup);
            Element elementEntranceTestItem = packEntranceTestItem(doc, lstEntranceTestItem);
            if (elementEntranceTestItem != null)
                competitiveGroupElement.appendChild(elementEntranceTestItem);
        }

        return competitiveGroups;
    }


    private Element packEntranceTestItem
            (
                    Document doc
                    , List<XMLEntranceTestItem> lstEntranceTestItem
            )
    {
        if (lstEntranceTestItem == null || lstEntranceTestItem.size() == 0)
            return null;


        Element retVal = doc.createElement("EntranceTestItems");

        for (XMLEntranceTestItem item : lstEntranceTestItem) {
            // item.get
            Element itemElem = doc.createElement("EntranceTestItem");
            retVal.appendChild(itemElem);

            _packUid(doc, itemElem, item.getUid());
            ToolsDao.PackToElement(doc, itemElem, "EntranceTestTypeID", item.getEntrancedisciplinetypefis().getCode());

            ToolsDao.PackToElement(doc, itemElem, "Form", item.getForm());

            ToolsDao.PackToElement(doc, itemElem, "MinScore", Integer.toString(item.getMinScore()));


            Element testSubject = doc.createElement("EntranceTestSubject");
            itemElem.appendChild(testSubject);

            if (item.getEcdisciplinefis().getId() == null) {
                String subjName = item.getEcdisciplinefis().getTitle();

                if (subjName.length() > 50)
                    subjName = subjName.substring(0, 50);

                ToolsDao.PackToElement(doc, testSubject, "SubjectName", subjName);
            }
            else
                ToolsDao.PackToElement(doc, testSubject, "SubjectID", item.getEcdisciplinefis().getCode());

            if (item.getEntranceTestBenefit() != null && !item.getEntranceTestBenefit().isEmpty())
                packCommonOrEntranceTestBenefit(doc, itemElem, item.getEntranceTestBenefit(), false, item);

        }
        return retVal;
    }


    private void packCommonOrEntranceTestBenefit(
            Document doc
            , Element root
            , List<XMLCommonBenefit> lstCommonBenefit
            , boolean isCommon
            , XMLEntranceTestItem entranceTestItem)
    {

        if (lstCommonBenefit == null || lstCommonBenefit.size() == 0)
            return;

        String rootName = "CommonBenefit";
        String rootName2 = "CommonBenefitItem";

        if (!isCommon) {
            rootName = "EntranceTestBenefits";
            rootName2 = "EntranceTestBenefitItem";
        }

        Element commonBenefit = doc.createElement(rootName);
        root.appendChild(commonBenefit);

        for (XMLCommonBenefit item : lstCommonBenefit) {

            EnrollmentCampaign ec = item.getCompetitiveGroup().getEnrollmentCampaign();

            Element commonBenefitItem = doc.createElement(rootName2);
            commonBenefit.appendChild(commonBenefitItem);

            _packUid(doc, commonBenefitItem, item.getUid());
            _packToElement(doc, commonBenefitItem, "BenefitKindID", item.getCompetitionkindfis().getCode());

            String _isForAllOlympics = "0";
            if (item.isForAllOlympics())
                _isForAllOlympics = "1";

            _packToElement(doc, commonBenefitItem, "IsForAllOlympics", _isForAllOlympics);


            Element olympicDiplomTypes = doc.createElement("OlympicDiplomTypes");
            commonBenefitItem.appendChild(olympicDiplomTypes);

            List<Olimpiaddiplomatypefis> lstDiplomaTypes = item.getOlimpicDiplomTypes();

            for (Olimpiaddiplomatypefis diplomaType : lstDiplomaTypes) {
                _packToElement(doc, olympicDiplomTypes, "OlympicDiplomTypeID", diplomaType.getCode());
            }

            if (!lstDiplomaTypes.isEmpty()) {
                if (entranceTestItem != null
                        && !isCommon
                        && entranceTestItem.getEcdisciplinefis().getId() != null
                        )
                {
                    Integer minMark = getMinMarkForOlimpiad(entranceTestItem.getEcdisciplinefis(), ec);
                    _packToElement(doc, commonBenefitItem, "MinEgeMark", minMark.toString());
                }

                if (isCommon) {
                    // вступительные испытания всей конкурсной группы
                    List<XMLEntranceTestItem> lstEntranceTestItem = EntranceTestItemDAO.Instanse().getEntranceTestItem(item.getCompetitiveGroup());
                    List<XMLEntranceTestItem> lst = new ArrayList<>();

                    // только дисциплины ФИС
                    if (lstEntranceTestItem != null && !lstEntranceTestItem.isEmpty()) {
                        for (XMLEntranceTestItem testItem : lstEntranceTestItem) {
                            if (testItem.getEcdisciplinefis().getId() != null)
                                lst.add(testItem);
                        }
                    }

                    if (lst != null && !lst.isEmpty()) {
                        Element minEgeMarks = doc.createElement("MinEgeMarks");
                        commonBenefitItem.appendChild(minEgeMarks);

                        for (XMLEntranceTestItem testItem : lst) {
                            Integer minMark = getMinMarkForOlimpiad(testItem.getEcdisciplinefis(), ec);

                            Element minMarks = doc.createElement("MinMarks");
                            minEgeMarks.appendChild(minMarks);

                            _packToElement(doc, minMarks, "SubjectID", testItem.getEcdisciplinefis().getCode());
                            _packToElement(doc, minMarks, "MinMark", minMark.toString());
                        }
                    }
                }
            }

        }
    }


    private Integer getMinMarkForOlimpiad(Ecdisciplinefis ecdisciplinefis, EnrollmentCampaign ec)
    {
        DisciplineMinMark2Olimpiad rel = SpravDao.Instanse().getDisciplineMinMark2Olimpiad(ecdisciplinefis, ec);
        return rel.getMinMark();
    }


    private void _packUid
            (
                    Document doc
                    , Element root
                    , String uid
            )
    {

        ToolsDao.PackUid(doc, root, uid);
    }


    /**
     * lstTargetOrganizations - организации целевого приема внутри конкурсной группы
     *
     * @param doc
     * @param root
     * @param competitiveGroup
     * @param lstTargetOrganizations
     * @param hashCompetitiveGroupTargetItem
     */
    private void packTargetOrganizationItem(
            Document doc
            , Element root
            , XMLCompetitiveGroup competitiveGroup
            , List<XMLTargetOrganization> lstTargetOrganizations
            , List<String> hashCompetitiveGroupTargetItem

    )
    {
        // проверки на null пройдены выше
        Element targetOrganizationsElement = doc.createElement("TargetOrganizations");
        root.appendChild(targetOrganizationsElement);

        for (XMLTargetOrganization item : lstTargetOrganizations) {
            if (item.getItems() == null || item.getItems().size() == 0)
                continue;

            Element targetOrganizationElement = doc.createElement("TargetOrganization");
            targetOrganizationsElement.appendChild(targetOrganizationElement);

            _packUid(doc, targetOrganizationElement, item.getUid());

            // TargetOrganizationName
            _packToElement(
                    doc, targetOrganizationElement
                    , "TargetOrganizationName", item.getTargetadmissionkind().getTitle());


            Element itemElement = doc.createElement("Items");
            targetOrganizationElement.appendChild(itemElement);

            if (item.getItems() != null && item.getItems().size() > 0) {
                // по направлениям подготовки организаций целевого приема
                for (XMLTargetOrganizationItem xitem : item.getItems()) {
                    Element competitiveGroupTargetItem = doc.createElement("CompetitiveGroupTargetItem");
                    itemElement.appendChild(competitiveGroupTargetItem);

                    String uid = xitem.getUid();
                    if (hashCompetitiveGroupTargetItem.contains(uid))
                        uid += "(" + hashCompetitiveGroupTargetItem.size() + ")";
                    else
                        hashCompetitiveGroupTargetItem.add(uid);

                    _packUid(doc, competitiveGroupTargetItem, uid);
                    _packEducationLevelId(doc, competitiveGroupTargetItem, xitem.getEceducationlevelfis());
                    _packDirectionID(doc, competitiveGroupTargetItem, xitem.getEducationdirectionfis());


                    // расписать по числам приема
                    List<EnrollmentDirection> edlist = xitem.getEnrollmentDirectionsList();


                    Long _numberTargetO = AdmissionVolumeDAO.Instanse().getNumberTargetO(xitem.getTargetadmissionkind(), edlist);
                    _packVolumeInfo("NumberTargetO", _numberTargetO, doc, competitiveGroupTargetItem);

                    Long _numberTargetOZ = AdmissionVolumeDAO.Instanse().getNumberTargetOZ(xitem.getTargetadmissionkind(), edlist);
                    _packVolumeInfo("NumberTargetOZ", _numberTargetOZ, doc, competitiveGroupTargetItem);

                    Long _numberTargetZ = AdmissionVolumeDAO.Instanse().getNumberTargetZ(xitem.getTargetadmissionkind(), edlist);
                    _packVolumeInfo("NumberTargetZ", _numberTargetZ, doc, competitiveGroupTargetItem);
                }

            }
        }

    }


    private void _packToElement
            (
                    Document doc
                    , Element root
                    , String attrName
                    , String title)
    {

        ToolsDao.PackToElement(doc, root, attrName, title);
    }


    private void packCompetitiveGroupItem
            (
                    Document doc
                    , Element itemsroot
                    , XMLCompetitiveGroupItem competitiveGroupItem
            )
    {

        List<EnrollmentDirection> directionList = competitiveGroupItem.getEnrollmentDirectionsList();
        // получаем суммы по приемам
        Long _numberBudgetO = AdmissionVolumeDAO.Instanse().getNumberBudgetO(directionList);
        Long _numberBudgetOZ = AdmissionVolumeDAO.Instanse().getNumberBudgetOZ(directionList);
        Long _numberBudgetZ = AdmissionVolumeDAO.Instanse().getNumberBudgetZ(directionList);
        Long _numberPaidO = AdmissionVolumeDAO.Instanse().getNumberPaidO(directionList);
        Long _numberPaidOZ = AdmissionVolumeDAO.Instanse().getNumberPaidOZ(directionList);
        Long _numberPaidZ = AdmissionVolumeDAO.Instanse().getNumberPaidZ(directionList);


        Long _NumberQuotaO = AdmissionVolumeDAO.Instanse().getNumberQuotaO(directionList);
        Long _NumberQuotaOZ = AdmissionVolumeDAO.Instanse().getNumberQuotaOZ(directionList);
        Long _NumberQuotaZ = AdmissionVolumeDAO.Instanse().getNumberQuotaZ(directionList);


        Long _total = 0L;
        _total += _numberBudgetO;
        _total += _numberBudgetOZ;
        _total += _numberBudgetZ;
        _total += _numberPaidO;
        _total += _numberPaidOZ;
        _total += _numberPaidZ;

        _total += _NumberQuotaO;
        _total += _NumberQuotaOZ;
        _total += _NumberQuotaZ;


        if (_total == 0L) {
            // фигня случается, что делать не ясно
            // return;
            System.out.println("сумма приемов по " + competitiveGroupItem.getEducationdirectionfis().getTitle());
        }

        // <TODO> вроде нужно исключить целевой прием (это возможно, если все планы приема кроме целевиков == 0)
        Element elem = doc.createElement("CompetitiveGroupItem");
        itemsroot.appendChild(elem);

        _packVolumeInfo("NumberBudgetO", _numberBudgetO, doc, elem);
        _packVolumeInfo("NumberBudgetOZ", _numberBudgetOZ, doc, elem);
        _packVolumeInfo("NumberBudgetZ", _numberBudgetZ, doc, elem);
        _packVolumeInfo("NumberPaidO", _numberPaidO, doc, elem);
        _packVolumeInfo("NumberPaidOZ", _numberPaidOZ, doc, elem);
        _packVolumeInfo("NumberPaidZ", _numberPaidZ, doc, elem);

        _packVolumeInfo("NumberQuotaOZ", _NumberQuotaOZ, doc, elem);
        _packVolumeInfo("NumberQuotaO", _NumberQuotaO, doc, elem);
        _packVolumeInfo("NumberQuotaZ", _NumberQuotaZ, doc, elem);

        _packUid(doc, elem, competitiveGroupItem.getUid());

        _packEducationLevelId(doc, elem, competitiveGroupItem.getEceducationlevelfis());
        // competitiveGroupItem

        // направление подготовки, справочник 10
        _packDirectionID(doc, elem, competitiveGroupItem.getEducationdirectionfis());


    }


    private void _packDirectionID(
            Document doc
            , Element root
            , Educationdirectionfis educationdirectionfis)
    {
        String txt = educationdirectionfis.getCode();

        Element elem = doc.createElement("DirectionID");
        root.appendChild(elem);
        elem.setTextContent(txt);

    }


    private void _packEducationLevelId(Document doc, Element root,
                                       Eceducationlevelfis eceducationlevelfis)
    {

        String txt = eceducationlevelfis.getCode();

        Element elem = doc.createElement("EducationLevelID");
        root.appendChild(elem);
        elem.setTextContent(txt);
    }


    private void _packName(Document doc, Element root,
                           String name)
    {

        Element elem = doc.createElement("Name");
        elem.setTextContent(name);
        root.appendChild(elem);
    }

    private void _packCourse(
            Document doc
            , Element root
            , EnrollmentCampaign enrollmentCampaign)
    {
        Course course = getCourse(enrollmentCampaign);

        Element courseElem = doc.createElement("Course");
        courseElem.setTextContent(course.getTitle());
        root.appendChild(courseElem);
    }


    private void _packCampaignUID(
            Document doc
            , Element root
            , EnrollmentCampaign enrollmentCampaign)
    {
        Element elem = doc.createElement("CampaignUID");
        root.appendChild(elem);

        elem.setTextContent(Long.toString(enrollmentCampaign.getId()));

    }


    private Element packAdmissionVolume(List<EnrollmentCampaign> lst
            , EducationdirectionSpravType spravType
            , Document doc)
    {
        Element admissionVolume = doc.createElement("AdmissionVolume");

        // получим все направления приема
        List<XMLEnrollmentDirection> lstED =
                SpravDao.Instanse().getEnrollmentDirections(lst, spravType);

        for (XMLEnrollmentDirection ed : lstED) {
            Element item = doc.createElement("Item");
            admissionVolume.appendChild(item);

            String UID = "v:" + ed.getUid();

            Element uid = doc.createElement("UID");
            uid.setTextContent(UID);
            item.appendChild(uid);

            Element campaignUID = doc.createElement("CampaignUID");
            campaignUID.setTextContent(Long.toString(ed.getEnrollmentCampaign().getId()));
            item.appendChild(campaignUID);

            Element educationLevelID = doc.createElement("EducationLevelID");
            item.appendChild(educationLevelID);

            Eceducationlevelfis eduLevel = ed.getEceducationlevelfis();
            educationLevelID.setTextContent(eduLevel.getCode());

            Element course = doc.createElement("Course");
            item.appendChild(course);

            Course corseTU = SpravDao.Instanse().getCourse(ed.getEnrollmentCampaign());
            course.setTextContent(Integer.toString(corseTU.getIntValue()));


            _packDirectionID(doc, item, ed.getEducationdirectionfis());


            // получим все направления приема, из направлений приема получим цифры приема
            List<EnrollmentDirection> lstEnrolmentDirection = ed.getEnrollmentDirectionsList();

            // получаем суммы по приемам
            Long _numberBudgetO = AdmissionVolumeDAO.Instanse().getNumberBudgetO(lstEnrolmentDirection);
            _packVolumeInfo("NumberBudgetO", _numberBudgetO, doc, item);

            Long _numberBudgetOZ = AdmissionVolumeDAO.Instanse().getNumberBudgetOZ(lstEnrolmentDirection);
            _packVolumeInfo("NumberBudgetOZ", _numberBudgetOZ, doc, item);


            Long _numberBudgetZ = AdmissionVolumeDAO.Instanse().getNumberBudgetZ(lstEnrolmentDirection);
            _packVolumeInfo("NumberBudgetZ", _numberBudgetZ, doc, item);


            Long _numberPaidO = AdmissionVolumeDAO.Instanse().getNumberPaidO(lstEnrolmentDirection);
            _packVolumeInfo("NumberPaidO", _numberPaidO, doc, item);

            Long _numberPaidOZ = AdmissionVolumeDAO.Instanse().getNumberPaidOZ(lstEnrolmentDirection);
            _packVolumeInfo("NumberPaidOZ", _numberPaidOZ, doc, item);

            Long _numberPaidZ = AdmissionVolumeDAO.Instanse().getNumberPaidZ(lstEnrolmentDirection);
            _packVolumeInfo("NumberPaidZ", _numberPaidZ, doc, item);

            Long _numberTargetO = AdmissionVolumeDAO.Instanse().getNumberTargetO(lstEnrolmentDirection);
            _packVolumeInfo("NumberTargetO", _numberTargetO, doc, item);

            Long _numberTargetOZ = AdmissionVolumeDAO.Instanse().getNumberTargetOZ(lstEnrolmentDirection);
            _packVolumeInfo("NumberTargetOZ", _numberTargetOZ, doc, item);

            Long _numberTargetZ = AdmissionVolumeDAO.Instanse().getNumberTargetZ(lstEnrolmentDirection);
            _packVolumeInfo("NumberTargetZ", _numberTargetZ, doc, item);


            Long _NumberQuotaO = AdmissionVolumeDAO.Instanse().getNumberQuotaO(lstEnrolmentDirection);
            _packVolumeInfo("NumberQuotaO", _NumberQuotaO, doc, item);

            Long _NumberQuotaOZ = AdmissionVolumeDAO.Instanse().getNumberQuotaOZ(lstEnrolmentDirection);
            _packVolumeInfo("NumberQuotaOZ", _NumberQuotaOZ, doc, item);

            Long _NumberQuotaZ = AdmissionVolumeDAO.Instanse().getNumberQuotaZ(lstEnrolmentDirection);
            _packVolumeInfo("NumberQuotaZ", _NumberQuotaZ, doc, item);


        }
        return admissionVolume;
    }


    private void _packVolumeInfo(String attrName, Long volume, Document doc, Element item)
    {
        if (volume != null && volume > 0) {
            Element volumeAttr = doc.createElement(attrName);
            item.appendChild(volumeAttr);
            volumeAttr.setTextContent(Long.toString(volume));
        }
    }

    /**
     * Упаковка приемной компании в xml
     */
    public Element packEnrollmentCampaign(EnrollmentCampaign ec, Document doc)
    {
        Element campaign = doc.createElement("Campaign");

        Element uid = doc.createElement("UID");
        campaign.appendChild(uid);
        uid.setTextContent(Long.toString(ec.getId()));

        Element name = doc.createElement("Name");
        name.setTextContent(ec.getTitle());
        campaign.appendChild(name);

        Element yearStart = doc.createElement("YearStart");
        yearStart.setTextContent(Integer.toString(ec.getEducationYear().getIntValue()));
        campaign.appendChild(yearStart);


        Element yearEnd = doc.createElement("YearEnd");
        yearEnd.setTextContent(Integer.toString(ec.getEducationYear().getIntValue()));
        campaign.appendChild(yearEnd);

        // developForm
        Element educationForms = packEducationForms(doc, ec);
        campaign.appendChild(educationForms);

        // упаковываем статус приемной компании
        Element statusID = packEnrollmentCampaignState(doc, ec);
        campaign.appendChild(statusID);

        // educationLevels или квалификации
        Element educationLevels = packEducationLevels(doc, ec);
        campaign.appendChild(educationLevels);

        // CampaignDates
        Element campaignDates = packCampaignDates(doc, ec);
        campaign.appendChild(campaignDates);

        return campaign;
    }


    /**
     * Даты приемной компании
     */
    private Element packCampaignDates(Document doc, EnrollmentCampaign ec)
    {

        //составляем заголовки на основе
        // 1 - курса (всегда из приемки)
        // 2 - уровня образования
        // 3 - формы обучения
        // 4 - источника финансирования (там же целевой прием)

        // эти сведения вычисляем на основе направлений подготовки для приема
        // в рамках года - нужен UID в качестве УИД можно взять контрольную сумму

        // даты пока определяем по факту - приказы уже есть, даты берем из реальных приказов по приемке

        List<EnrollmentDirection> lst = getEnrollmentDirection(ec);

        Course course = getCourse(ec);

        List<XmlCampaignDate> lstXmlCampaignDate = new ArrayList<>();

        for (EnrollmentDirection ed : lst) {

            Eceducationlevelfis eceducationlevelfis = SpravDao.Instanse().getEceducationlevelfis(ed.getEducationOrgUnit());
            Developformfis developformfis = SpravDao.Instanse().getDevelopformfis(ed.getEducationOrgUnit().getDevelopForm());

            System.out.println(ed.getEducationOrgUnit().getDevelopForm().getTitle());


            // источники финансирования (у одного направления для приема может быть несколько источников финансирования)
            List<Compensationtypefis> lstCompensationType = SpravDao.Instanse().getListCompensationtypefis(ed);

            for (Compensationtypefis compensationtype : lstCompensationType) {
                List<String> stageList = SpravDao.Instanse().getStageList(eceducationlevelfis, developformfis, compensationtype);

                for (String stage : stageList) {
                    XmlCampaignDate cd
                            = new XmlCampaignDate(course, eceducationlevelfis, developformfis, compensationtype, stage);

                    if (!lstXmlCampaignDate.contains(cd))
                        lstXmlCampaignDate.add(cd);
                }
            }
        }

        // упаковка
		/*
		 * - 
<CampaignDates>
 <CampaignDate>
  <UID>1</UID> 
  <Course>1</Course> 
  <EducationLevelID>2</EducationLevelID> 
  <EducationFormID>11</EducationFormID> 
  <EducationSourceID>14</EducationSourceID> 
  <Stage>1</Stage> 
  <DateStart>2012-06-20</DateStart> 
  <DateEnd>2012-07-25</DateEnd> 
  <DateOrder>2012-08-05</DateOrder> 
  </CampaignDate>

		 */
        Element campaignDates = doc.createElement("CampaignDates");

        for (XmlCampaignDate cd : lstXmlCampaignDate) {
            Element campaignDate = doc.createElement("CampaignDate");
            campaignDates.appendChild(campaignDate);

            Element uID = doc.createElement("UID");
            campaignDate.appendChild(uID);
            uID.setTextContent(Integer.toString(cd.hashCode()));

            Element courseElem = doc.createElement("Course");
            campaignDate.appendChild(courseElem);
            courseElem.setTextContent(Integer.toString(cd.getCourse().getIntValue()));

            Element educationLevelID = doc.createElement("EducationLevelID");
            campaignDate.appendChild(educationLevelID);
            educationLevelID.setTextContent(cd.getEceducationlevelfis().getCode());

            Element educationFormID = doc.createElement("EducationFormID");
            campaignDate.appendChild(educationFormID);
            educationFormID.setTextContent(cd.getDevelopformfis().getCode());

            Element educationSourceID = doc.createElement("EducationSourceID");
            campaignDate.appendChild(educationSourceID);
            educationSourceID.setTextContent(cd.getCompensationtypefis().getCode());


            if (!cd.getStage().equals("0")) {
                Element stage = doc.createElement("Stage");
                campaignDate.appendChild(stage);
                stage.setTextContent(cd.getStage());
            }


            EnrollmentCampaignPeriod ep = ec.getPeriodList().get(0);

            // даты приемки
            Element dateStart = doc.createElement("DateStart");
            campaignDate.appendChild(dateStart);
            dateStart.setTextContent(ToolsDao.DateToString(ep.getDateFrom()));

            Element dateEnd = doc.createElement("DateEnd");
            campaignDate.appendChild(dateEnd);
            dateEnd.setTextContent(ToolsDao.DateToString(ep.getDateTo()));


            // получим дату приказа
            Date orderDate = SpravDao.Instanse().getOrderDate(cd, ec);

            Element dateOrder = doc.createElement("DateOrder");
            campaignDate.appendChild(dateOrder);
            dateOrder.setTextContent(ToolsDao.DateToString(orderDate));


        }

        return campaignDates;
    }


    private List<EnrollmentDirection> getEnrollmentDirection(EnrollmentCampaign ec)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EnrollmentDirection.class, "ed");
        dql.column(property("ed"));
        dql.where(eq(property(EnrollmentDirection.enrollmentCampaign().fromAlias("ed")), value(ec)));

        return dql.createStatement(getSession()).list();
    }
	
	/*
	private List<EnrollmentDirection> getEnrollmentDirection(List<EnrollmentCampaign> lst)
	{
		DQLSelectBuilder dql = new DQLSelectBuilder();
		dql.fromEntity(EnrollmentDirection.class, "ed");
		dql.addColumn("ed");
		dql.where(in(property(EnrollmentDirection.enrollmentCampaign().fromAlias("ed")), lst));
		
		List<EnrollmentDirection> retVal = dql.createStatement(getSession()).list();
		
		return retVal;
	}
	*/

    private Element packEducationLevels(Document doc, EnrollmentCampaign ec)
    {
		/*
		 * - <EducationLevels>
				- <EducationLevel>
				  <Course>1</Course> 
				  <EducationLevelID>2</EducationLevelID> 
				  </EducationLevel>
				  
				- <EducationLevel>
				  <Course>1</Course> 
				  <EducationLevelID>5</EducationLevelID> 
				  </EducationLevel>
				- <EducationLevel>
				  <Course>1</Course> 
				  <EducationLevelID>4</EducationLevelID> 
				  </EducationLevel>
			  </EducationLevels>

		 */

        // курс получаем из настроек приемной компании
        Course course = getCourse(ec);


        // далее получим список квалификаций для всех направлений подготовки в рамках
        // приемной компании
        List<EducationOrgUnit> lst = getEducationOrgUnit(ec);
        List<ICatalogItem> lstFis = SpravDao.Instanse().getEducationLevelFis(lst);

        Element educationLevels = doc.createElement("EducationLevels");

        for (ICatalogItem item : lstFis) {
            Element educationLevel = doc.createElement("EducationLevel");
            educationLevels.appendChild(educationLevel);

            Element courseElem = doc.createElement("Course");
            courseElem.setTextContent(course.getTitle());
            educationLevel.appendChild(courseElem);

            Element educationLevelID = doc.createElement("EducationLevelID");
            educationLevelID.setTextContent(item.getCode());
            educationLevel.appendChild(educationLevelID);
        }
        return educationLevels;
    }


    private Course getCourse(EnrollmentCampaign ec)
    {
        //<TODO> в дальнейшем нужно честно расчитывать из приемки
        // всегда 1

        return DevelopGridDAO.getCourseMap().get(1);
    }

    /**
     * Упаковать статус приемной кампании
     */
    private Element packEnrollmentCampaignState
    (
            Document doc
            , EnrollmentCampaign ec
    )
    {

        Enrolmentcomnainfis state = getEnrollmentCampaignState(ec);

        Element statusID = doc.createElement("StatusID");
        statusID.setTextContent(state.getCode());

        return statusID;
    }

    private Enrolmentcomnainfis getEnrollmentCampaignState(EnrollmentCampaign ec)
    {
        //<TODO>
        // учитывая обстоятельства - всегда завершена

        return getByCode(Enrolmentcomnainfis.class, EnrolmentcomnainfisCodes.IN_PROCESS);
    }

    private Element packEducationForms(Document doc, EnrollmentCampaign ec)
    {
        Element educationForms = doc.createElement("EducationForms");
        // для ec найдем developform
        List<DevelopForm> lst = getDevelopForm(ec);
        List<ICatalogItem> educationFormsList = SpravDao.Instanse().getEducationFormFis(lst);

        if (educationFormsList != null) {
            for (ICatalogItem educationForm : educationFormsList) {
                Element educationFormID = doc.createElement("EducationFormID");
                educationFormID.setTextContent(educationForm.getCode());
                educationForms.appendChild(educationFormID);
            }
        }
        return educationForms;
    }


    private List<DevelopForm> getDevelopForm(EnrollmentCampaign ec)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EnrollmentDirection.class, "ed");
        dql.joinPath(DQLJoinType.inner, EnrollmentDirection.educationOrgUnit().developForm().fromAlias("ed"), "developform");
        dql.column("developform");
        dql.where(eq(property(EnrollmentDirection.enrollmentCampaign().fromAlias("ed")), value(ec)));

        List<DevelopForm> lst = dql.createStatement(getSession()).list();

        List<DevelopForm> lstRetVal = new ArrayList<>();

        for (DevelopForm df : lst) {
            if (!lstRetVal.contains(df))
                lstRetVal.add(df);
        }
        return lstRetVal;
    }


    private List<EducationOrgUnit> getEducationOrgUnit(EnrollmentCampaign ec)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EnrollmentDirection.class, "ed");
        dql.joinPath(DQLJoinType.inner, EnrollmentDirection.educationOrgUnit().fromAlias("ed"), "educationorgunit");
        dql.column("educationorgunit");
        dql.where(eq(property(EnrollmentDirection.enrollmentCampaign().fromAlias("ed")), value(ec)));

        List<EducationOrgUnit> lst = dql.createStatement(getSession()).list();

        List<EducationOrgUnit> lstRetVal = new ArrayList<>();

        for (EducationOrgUnit df : lst) {
            if (!lstRetVal.contains(df))
                lstRetVal.add(df);
        }
        return lstRetVal;
    }


    public String XmlToString(Document doc) {
        String output;

        try {

            // в строку
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer;

            transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

            StringWriter writer = new StringWriter();

            transformer.transform(new DOMSource(doc), new StreamResult(writer));
            //String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
            output = writer.getBuffer().toString();
        }
        catch (Exception ex) {
            output = "error is " + ex.getMessage();
        }
        return output;
    }


    //public static boolean IN_PROCESS = false;

    @Override
    public Document PackEnrollmentCampaign
            (
                    DaemonParams daemonParams
            )
    {

        EducationdirectionSpravType spravType = daemonParams.getSpravType();

        // скинем кеши
        Applications.Instanse().clearCache();
        SpravDao.Instanse().clearCache();
        TESTMODE_CompetitiveGroup_List = null;

        // перечитаем список приемных кампаний
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EnrollmentCampaign.class, "ec")
                .column("ec")
                .where(DQLExpressions.in(DQLExpressions.property(EnrollmentCampaign.id().fromAlias("ec")), daemonParams.getEnrollmentCampaigns()));
        List<EnrollmentCampaign> lstEnrollmentCampaign = dql.createStatement(getSession()).list();
        daemonParams.setEnrollmentCampaigns(lstEnrollmentCampaign);


        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
        try {
            docBuilder = docFactory.newDocumentBuilder();

        }
        catch (ParserConfigurationException e) {
            //return "<error>" + e.getMessage()+ "</error>";
            throw new ApplicationException(e.getMessage());
        }

        // root elements
        Document doc = docBuilder.newDocument();

        Element root = doc.createElement("Root");
        doc.appendChild(root);

        Element authData = doc.createElement("AuthData");
        root.appendChild(authData);

        ToolsDao.PackToElement(doc, authData, "Login", daemonParams.getUserName());
        ToolsDao.PackToElement(doc, authData, "Pass", daemonParams.getUserPassword());

        Element rootElement = doc.createElement("PackageData");
        root.appendChild(rootElement);


        PackUnpackToFis.MSG_LOG = "упаковка CampaignInfo";

        if (daemonParams.isCampaignInfo()) {
            // часть 1
            Element campaignInfo = doc.createElement("CampaignInfo");
            rootElement.appendChild(campaignInfo);

            Element campaigns = doc.createElement("Campaigns");
            campaignInfo.appendChild(campaigns);

            for (EnrollmentCampaign ec : lstEnrollmentCampaign) {
                Element elem = packEnrollmentCampaign(ec, doc);
                campaigns.appendChild(elem);
            }
            ///////////////
        }

        PackUnpackToFis.MSG_LOG = "упаковка AdmissionInfo";
        if (daemonParams.isAdmissionInfo()) {
            // часть 2
            Element admissionInfo = packAdmissionInfo(lstEnrollmentCampaign, spravType, doc);
            rootElement.appendChild(admissionInfo);
            //////////////
        }

        PackUnpackToFis.MSG_LOG = "упаковка заявлений";
        if (daemonParams.isApplications()) {
            // часть 3
            Element applications = Applications.Instanse().packApplications(
                    daemonParams.isTestMode()
                    , daemonParams.isEntranceTestResult()
                    , daemonParams.isAppDateWithoutTime()
                    , daemonParams.getDateRequestFrom()
                    , daemonParams.getDateRequestTo()
                    , daemonParams.isWhithoutSendedInFis()
                    , daemonParams.isWhithoutEntrantInOrder()
                    , lstEnrollmentCampaign
                    , spravType
                    , doc
            );
            rootElement.appendChild(applications);
            //////////////
        }


        PackUnpackToFis.MSG_LOG = "упаковка приказов";
        if (daemonParams.isOrdersOfAdmission()) {
            // часть 3
            Element ordersOfAdmission = OrdersOfAdmission.Instanse().packOrdersOfAdmission(
                    doc
                    , lstEnrollmentCampaign
                    , spravType
                    , daemonParams.isAppDateWithoutTime()
            );
            if (ordersOfAdmission != null)
                rootElement.appendChild(ordersOfAdmission);


        }


        return doc;

    }

    @Override
    public FisPackages PackEnrollmentCampaignToDatabase(
            DaemonParams daemonParams
    ) throws TransformerException, IOException
    {

        Writer outWriter = new StringWriter();

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

        Document doc = PackEnrollmentCampaign(
                daemonParams
        );
        StreamResult result = new StreamResult(outWriter);
        DOMSource source = new DOMSource(doc);

        transformer.transform(source, result);
        outWriter.flush();


        return saveCurrentFisPackages(
                daemonParams
                , outWriter.toString().getBytes()
        );
    }

    /**
     * Сам FisPackages не сохраняется, это нужно делать в вызывающей функции
     */
    protected void packErrorsList(FisPackages pkg) {

        if (xmlPkgErrors.size() > 0) {
            TemplateDocument template = getCatalogItem(TemplateDocument.class, SUSPEND_ERRORS_TEMPLATE);

            RtfDocument resultRtf = new RtfReader().read(template.getContent());
            RtfTableModifier tableModifier = new RtfTableModifier();

            String[][] tableData = new String[xmlPkgErrors.size()][1];
            int idx = 0;
            for (String err : xmlPkgErrors) {
                tableData[idx][0] = err;
                idx++;
            }
            tableModifier.put("T", tableData);
            tableModifier.modify(resultRtf);

            DatabaseFile errorsFile = new DatabaseFile();
            errorsFile.setContent(RtfUtil.toByteArray(resultRtf));
            errorsFile.setFilename("errors");
            save(errorsFile);

            pkg.setXmlErrors(errorsFile);
        }
    }

    @Override
    public FisPackages saveCurrentFisPackages(
            DaemonParams daemonParams
            , byte[] xml)
    {

        FisPackages pkg = new FisPackages();

        if (daemonParams.getSpravType() == null)
            throw new ApplicationException("Не указан тип справочников ФИС");
        pkg.setSpravType(daemonParams.getSpravType());

        String dsk = "";
        if (daemonParams.isApplications()) {
            // исключить время в заявлении
            if (daemonParams.isAppDateWithoutTime())
                dsk += "В заявл. исключено время; ";


            // прочие параметры
            if (daemonParams.getDateRequestFrom() != null)
                dsk += " Дата рег. с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(daemonParams.getDateRequestFrom()) + "; ";

            if (daemonParams.getDateRequestTo() != null)
                dsk += " Дата рег. по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(daemonParams.getDateRequestTo()) + "; ";


            if (daemonParams.isWhithoutEntrantInOrder())
                dsk += " Без заявлений 'в приказе'; ";

            if (daemonParams.isWhithoutSendedInFis())
                dsk += " Без ранее переданных в ФИС; ";


            if (!daemonParams.getRegNumbers().isEmpty())
                dsk += "заяв №: " + StringUtils.join(daemonParams.getRegNumbers(), ",") + "; ";

            if (daemonParams.isBenefitRequest())
                dsk += " льготные виды конкурса; ";

            if (daemonParams.isNotRf())
                dsk += " абитур. не РФ; ";

            if (!daemonParams.getExcludeRegNumbers().isEmpty())
                dsk += "исключить заяв. №: " + StringUtils.join(daemonParams.getExcludeRegNumbers(), ",") + "; ";
        }

        pkg.setDescription(dsk);


        pkg.setFormingDate(new Date());
        pkg.setCampaignInfo(daemonParams.isCampaignInfo());
        pkg.setAdmissionInfo(daemonParams.isAdmissionInfo());
        pkg.setApplications(daemonParams.isApplications());

        pkg.setEntranceTestResult(daemonParams.isEntranceTestResult());
        pkg.setTestApplicationMode(daemonParams.isTestMode());
        pkg.setOrdersOfAdmission(daemonParams.isOrdersOfAdmission());

        StringBuilder sbEnrCamps = new StringBuilder();
        List<EnrollmentCampaign> lstEnrollmentCampaign = daemonParams.getEnrollmentCampaigns();

        for (EnrollmentCampaign cpg : lstEnrollmentCampaign) {
            if (sbEnrCamps.length() > 0) {
                sbEnrCamps.append(", ");
            }
            sbEnrCamps.append(cpg.getTitle());
        }

        pkg.setEnrollmentCampaign(sbEnrCamps.toString());

        if (daemonParams.isTestMode()) {
            Integer abitStart = ABIT_START_AT;
            Integer abitCount = ABIT_COUNT;
            pkg.setTestAppModeData(abitStart.toString() + " " + abitCount.toString());
        }
        pkg.setSuspendCompetitiveGroup(daemonParams.isSuspendCompetitiveGroup());
        pkg.setSuspendErrorDoc(daemonParams.isSuspendErrorDoc());
        pkg.setAbfuscateData(daemonParams.isAbfuscateData());

        DatabaseFile xmlPackageFile = null;
        if (xml != null) {
            xmlPackageFile = new DatabaseFile();
            xmlPackageFile.setContent(xml);
            xmlPackageFile.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_XML);
            xmlPackageFile.setFilename("fis");
            save(xmlPackageFile);
        }

        pkg.setXmlPackage(xmlPackageFile);

        packErrorsList(pkg);
        save(pkg);

        //теперь можем работать с fisPackages2EnrollmentCampaign:
        for (EnrollmentCampaign cpg : lstEnrollmentCampaign) {
            FisPackages2enrollmentCampaign f2c = new FisPackages2enrollmentCampaign();
            f2c.setEnrollmentCampaign(cpg);
            f2c.setFisPackages(pkg);
            save(f2c);
        }


        return pkg;
    }

}
