package ru.tandemservice.fisrmc.entity.catalog;

import ru.tandemservice.fisrmc.entity.catalog.gen.OlimpiatfisGen;

/**
 * Олимпиады
 */
public class Olimpiatfis extends OlimpiatfisGen {
    public boolean isEqualByCodes(Olimpiatfis entity)
    {
        if (entity.getOlympicNumber().equals(this.getOlympicNumber())
                && entity.getEcolimpiadlevelfis().equals(this.getEcolimpiadlevelfis())
                )
            return true;
        else
            return false;
    }

    public boolean isEqualAll(Olimpiatfis entity)
    {
        if (isEqualByCodes(entity)
                && entity.getTitle().equals(this.getTitle())
                )
            return true;
        else
            return false;
    }
}