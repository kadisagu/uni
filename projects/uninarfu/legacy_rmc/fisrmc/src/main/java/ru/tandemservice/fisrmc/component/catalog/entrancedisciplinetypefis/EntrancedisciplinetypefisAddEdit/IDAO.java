package ru.tandemservice.fisrmc.component.catalog.entrancedisciplinetypefis.EntrancedisciplinetypefisAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.fisrmc.entity.catalog.Entrancedisciplinetypefis;

public interface IDAO extends IDefaultCatalogAddEditDAO<Entrancedisciplinetypefis, Model> {

}
