package ru.tandemservice.fisrmc.dao.packApplications;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.tandemservice.fisrmc.dao.XMLCompetitiveGroup;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;

import java.util.List;

public interface IPackEntranceTestResult {


    /**
     * результаты вступительных испытаний
     *
     * @param doc
     * @param entrantRequest
     * @param lstEnrollmentDirections
     * @param xmlCompetitiveGroupList
     *
     * @return
     */
    public Element packEntranceTestResult(
            Document doc
            , EntrantRequest entrantRequest
            , List<EnrollmentDirection> lstEnrollmentDirections
            , List<XMLCompetitiveGroup> xmlCompetitiveGroupList
    );

    public void makeCache();

    public void clearCache();

}
