package ru.tandemservice.fisrmc.component.TestWeb.Delete;

public class Model extends ru.tandemservice.fisrmc.component.TestWeb.Model
{

    @Override
    public boolean isDeletePackageType() {
        return true;
    }

    public boolean isDeleteApplication() {
        if (this.getSettings().get("deleteApplication") == null)
            this.getSettings().set("deleteApplication", Boolean.TRUE);
        return (Boolean) this.getSettings().get("deleteApplication");
    }

    public void setDeleteApplication(boolean deleteApplication) {
        this.getSettings().set("deleteApplication", deleteApplication);
    }

    public boolean isDeleteOrdersOfAdmission() {
        if (this.getSettings().get("deleteOrdersOfAdmission") == null)
            this.getSettings().set("deleteOrdersOfAdmission", Boolean.TRUE);
        return (Boolean) this.getSettings().get("deleteOrdersOfAdmission");
    }

    public void setDeleteOrdersOfAdmission(boolean deleteOrdersOfAdmission) {
        this.getSettings().set("deleteOrdersOfAdmission", deleteOrdersOfAdmission);
    }

    public boolean isDeleteCompetitiveGroupItems() {
        if (this.getSettings().get("deleteCompetitiveGroupItems") == null)
            this.getSettings().set("deleteCompetitiveGroupItems", Boolean.TRUE);
        return (Boolean) this.getSettings().get("deleteCompetitiveGroupItems");
    }

    public void setDeleteCompetitiveGroupItems(boolean deleteCompetitiveGroupItems) {
        this.getSettings().set("deleteCompetitiveGroupItems", deleteCompetitiveGroupItems);
    }

    public boolean isDeleteEntranceTestResults() {
        if (this.getSettings().get("deleteEntranceTestResults") == null)
            this.getSettings().set("deleteEntranceTestResults", Boolean.TRUE);
        return (Boolean) this.getSettings().get("deleteEntranceTestResults");
    }

    public void setDeleteEntranceTestResults(boolean deleteEntranceTestResults) {
        this.getSettings().set("deleteEntranceTestResults", deleteEntranceTestResults);
    }

    public boolean isDeleteApplicationCommonBenefits() {
        if (this.getSettings().get("deleteApplicationCommonBenefits") == null)
            this.getSettings().set("deleteApplicationCommonBenefits", Boolean.TRUE);
        return (Boolean) this.getSettings().get("deleteApplicationCommonBenefits");
    }

    public void setDeleteApplicationCommonBenefits(boolean deleteApplicationCommonBenefits) {
        this.getSettings().set("deleteApplicationCommonBenefits", deleteApplicationCommonBenefits);
    }


}
