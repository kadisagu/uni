package ru.tandemservice.fisrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_fisrmc_1x0x0_7to8 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.10"),
                        new ScriptDependency("org.tandemframework.shared", "1.1.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.1.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность educationdirectionfis

        // удалено свойство educationLevels
        {

            // удалить колонку
            tool.dropColumn("educationdirectionfis_t", "educationlevels_id");

        }

        // создано свойство codeOld10
        {
            // создать колонку
            tool.createColumn("educationdirectionfis_t", new DBColumn("codeold10_p", DBType.createVarchar(255)));

        }

        // изменен тип свойства code
        {
            // изменить тип колонки
            tool.changeColumnType("educationdirectionfis_t", "code_p", DBType.createVarchar(255));

        }

        //  свойство code стало необязательным
        {
            // сделать колонку NULL
            tool.setColumnNullable("educationdirectionfis_t", "code_p", true);

        }

        // изменен тип свойства title
        {
            // изменить тип колонки
            tool.changeColumnType("educationdirectionfis_t", "title_p", DBType.TEXT);

        }


    }
}