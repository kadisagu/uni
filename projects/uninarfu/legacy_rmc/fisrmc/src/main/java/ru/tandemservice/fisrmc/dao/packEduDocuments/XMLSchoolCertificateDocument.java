package ru.tandemservice.fisrmc.dao.packEduDocuments;

import org.tandemframework.shared.person.base.entity.PersonEduInstitution;

import java.util.Date;

/**
 * аттестат о среднем (полном) общем образовании
 *
 * @author vch
 */
public class XMLSchoolCertificateDocument
        extends XMLBaseEducationDocument
{
    /**
     * Год окончания, не обязательно
     */
    private Integer endYear = null;

    public XMLSchoolCertificateDocument
            (
                    String uid
                    , boolean originalReceived
                    , Date originalReceivedDate
                    , String documentSeries
                    , String documentNumber
                    , Date documentDate
                    , String documentOrganization
                    , Integer endYear
                    , PersonEduInstitution personEduInstitution
            )
    {
        super(uid, originalReceived, originalReceivedDate, documentSeries,
              documentNumber, documentDate, documentOrganization, personEduInstitution);

        setEndYear(endYear);
    }

    public void setEndYear(Integer endYear) {
        this.endYear = endYear;
    }

    public Integer getEndYear() {
        return endYear;
    }

}
