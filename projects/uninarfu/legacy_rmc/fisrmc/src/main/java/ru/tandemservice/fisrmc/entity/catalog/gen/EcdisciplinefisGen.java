package ru.tandemservice.fisrmc.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.fisrmc.entity.catalog.Ecdisciplinefis;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Общеобразовательные предметы
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcdisciplinefisGen extends EntityBase
 implements INaturalIdentifiable<EcdisciplinefisGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.fisrmc.entity.catalog.Ecdisciplinefis";
    public static final String ENTITY_NAME = "ecdisciplinefis";
    public static final int VERSION_HASH = 509545941;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_CODE_T_U = "codeTU";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _codeTU; 
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getCodeTU()
    {
        return _codeTU;
    }

    /**
     * @param codeTU 
     */
    public void setCodeTU(String codeTU)
    {
        dirty(_codeTU, codeTU);
        _codeTU = codeTU;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EcdisciplinefisGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((Ecdisciplinefis)another).getCode());
            }
            setCodeTU(((Ecdisciplinefis)another).getCodeTU());
            setTitle(((Ecdisciplinefis)another).getTitle());
        }
    }

    public INaturalId<EcdisciplinefisGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EcdisciplinefisGen>
    {
        private static final String PROXY_NAME = "EcdisciplinefisNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EcdisciplinefisGen.NaturalId) ) return false;

            EcdisciplinefisGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcdisciplinefisGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Ecdisciplinefis.class;
        }

        public T newInstance()
        {
            return (T) new Ecdisciplinefis();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "codeTU":
                    return obj.getCodeTU();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "codeTU":
                    obj.setCodeTU((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "codeTU":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "codeTU":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "codeTU":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Ecdisciplinefis> _dslPath = new Path<Ecdisciplinefis>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Ecdisciplinefis");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.fisrmc.entity.catalog.Ecdisciplinefis#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return 
     * @see ru.tandemservice.fisrmc.entity.catalog.Ecdisciplinefis#getCodeTU()
     */
    public static PropertyPath<String> codeTU()
    {
        return _dslPath.codeTU();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.fisrmc.entity.catalog.Ecdisciplinefis#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends Ecdisciplinefis> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _codeTU;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.fisrmc.entity.catalog.Ecdisciplinefis#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EcdisciplinefisGen.P_CODE, this);
            return _code;
        }

    /**
     * @return 
     * @see ru.tandemservice.fisrmc.entity.catalog.Ecdisciplinefis#getCodeTU()
     */
        public PropertyPath<String> codeTU()
        {
            if(_codeTU == null )
                _codeTU = new PropertyPath<String>(EcdisciplinefisGen.P_CODE_T_U, this);
            return _codeTU;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.fisrmc.entity.catalog.Ecdisciplinefis#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EcdisciplinefisGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return Ecdisciplinefis.class;
        }

        public String getEntityName()
        {
            return "ecdisciplinefis";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
