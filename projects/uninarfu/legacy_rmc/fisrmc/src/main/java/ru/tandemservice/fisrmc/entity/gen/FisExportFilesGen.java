package ru.tandemservice.fisrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.fisrmc.entity.FisExportFiles;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Файл из ФИС, содержащий полные сведения по ОУ (включая описание приемок, направлений подготовки, заявлений и т.д.)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FisExportFilesGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.fisrmc.entity.FisExportFiles";
    public static final String ENTITY_NAME = "fisExportFiles";
    public static final int VERSION_HASH = -194944228;
    private static IEntityMeta ENTITY_META;

    public static final String P_DATE = "date";
    public static final String P_DESCRIPTION = "description";
    public static final String L_XML_PACKAGE = "xmlPackage";

    private Date _date;     // Дата
    private String _description;     // Описание
    private DatabaseFile _xmlPackage;     // Пакет из ФИС

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата. Свойство не может быть null.
     */
    @NotNull
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Дата. Свойство не может быть null.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Описание.
     */
    @Length(max=1200)
    public String getDescription()
    {
        return _description;
    }

    /**
     * @param description Описание.
     */
    public void setDescription(String description)
    {
        dirty(_description, description);
        _description = description;
    }

    /**
     * @return Пакет из ФИС.
     */
    public DatabaseFile getXmlPackage()
    {
        return _xmlPackage;
    }

    /**
     * @param xmlPackage Пакет из ФИС.
     */
    public void setXmlPackage(DatabaseFile xmlPackage)
    {
        dirty(_xmlPackage, xmlPackage);
        _xmlPackage = xmlPackage;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FisExportFilesGen)
        {
            setDate(((FisExportFiles)another).getDate());
            setDescription(((FisExportFiles)another).getDescription());
            setXmlPackage(((FisExportFiles)another).getXmlPackage());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FisExportFilesGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FisExportFiles.class;
        }

        public T newInstance()
        {
            return (T) new FisExportFiles();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "date":
                    return obj.getDate();
                case "description":
                    return obj.getDescription();
                case "xmlPackage":
                    return obj.getXmlPackage();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "description":
                    obj.setDescription((String) value);
                    return;
                case "xmlPackage":
                    obj.setXmlPackage((DatabaseFile) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "date":
                        return true;
                case "description":
                        return true;
                case "xmlPackage":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "date":
                    return true;
                case "description":
                    return true;
                case "xmlPackage":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "date":
                    return Date.class;
                case "description":
                    return String.class;
                case "xmlPackage":
                    return DatabaseFile.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FisExportFiles> _dslPath = new Path<FisExportFiles>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FisExportFiles");
    }
            

    /**
     * @return Дата. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisExportFiles#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Описание.
     * @see ru.tandemservice.fisrmc.entity.FisExportFiles#getDescription()
     */
    public static PropertyPath<String> description()
    {
        return _dslPath.description();
    }

    /**
     * @return Пакет из ФИС.
     * @see ru.tandemservice.fisrmc.entity.FisExportFiles#getXmlPackage()
     */
    public static DatabaseFile.Path<DatabaseFile> xmlPackage()
    {
        return _dslPath.xmlPackage();
    }

    public static class Path<E extends FisExportFiles> extends EntityPath<E>
    {
        private PropertyPath<Date> _date;
        private PropertyPath<String> _description;
        private DatabaseFile.Path<DatabaseFile> _xmlPackage;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisExportFiles#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(FisExportFilesGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Описание.
     * @see ru.tandemservice.fisrmc.entity.FisExportFiles#getDescription()
     */
        public PropertyPath<String> description()
        {
            if(_description == null )
                _description = new PropertyPath<String>(FisExportFilesGen.P_DESCRIPTION, this);
            return _description;
        }

    /**
     * @return Пакет из ФИС.
     * @see ru.tandemservice.fisrmc.entity.FisExportFiles#getXmlPackage()
     */
        public DatabaseFile.Path<DatabaseFile> xmlPackage()
        {
            if(_xmlPackage == null )
                _xmlPackage = new DatabaseFile.Path<DatabaseFile>(L_XML_PACKAGE, this);
            return _xmlPackage;
        }

        public Class getEntityClass()
        {
            return FisExportFiles.class;
        }

        public String getEntityName()
        {
            return "fisExportFiles";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
