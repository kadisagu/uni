package ru.tandemservice.fisrmc.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.fisrmc.entity.catalog.Ecolimpiadlevelfis;
import ru.tandemservice.fisrmc.entity.catalog.Olimpiatfis;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Олимпиады
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OlimpiatfisGen extends EntityBase
 implements INaturalIdentifiable<OlimpiatfisGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.fisrmc.entity.catalog.Olimpiatfis";
    public static final String ENTITY_NAME = "olimpiatfis";
    public static final int VERSION_HASH = -1969337774;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_CODE_T_U = "codeTU";
    public static final String P_OLYMPIC_NUMBER = "OlympicNumber";
    public static final String L_ECOLIMPIADLEVELFIS = "ecolimpiadlevelfis";
    public static final String P_YEAR = "Year";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _codeTU; 
    private String _OlympicNumber; 
    private Ecolimpiadlevelfis _ecolimpiadlevelfis;     // Уровень олимпиады
    private String _Year; 
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getCodeTU()
    {
        return _codeTU;
    }

    /**
     * @param codeTU  Свойство не может быть null.
     */
    public void setCodeTU(String codeTU)
    {
        dirty(_codeTU, codeTU);
        _codeTU = codeTU;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getOlympicNumber()
    {
        return _OlympicNumber;
    }

    /**
     * @param OlympicNumber  Свойство не может быть null.
     */
    public void setOlympicNumber(String OlympicNumber)
    {
        dirty(_OlympicNumber, OlympicNumber);
        _OlympicNumber = OlympicNumber;
    }

    /**
     * @return Уровень олимпиады. Свойство не может быть null.
     */
    @NotNull
    public Ecolimpiadlevelfis getEcolimpiadlevelfis()
    {
        return _ecolimpiadlevelfis;
    }

    /**
     * @param ecolimpiadlevelfis Уровень олимпиады. Свойство не может быть null.
     */
    public void setEcolimpiadlevelfis(Ecolimpiadlevelfis ecolimpiadlevelfis)
    {
        dirty(_ecolimpiadlevelfis, ecolimpiadlevelfis);
        _ecolimpiadlevelfis = ecolimpiadlevelfis;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getYear()
    {
        return _Year;
    }

    /**
     * @param Year 
     */
    public void setYear(String Year)
    {
        dirty(_Year, Year);
        _Year = Year;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OlimpiatfisGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((Olimpiatfis)another).getCode());
            }
            setCodeTU(((Olimpiatfis)another).getCodeTU());
            setOlympicNumber(((Olimpiatfis)another).getOlympicNumber());
            setEcolimpiadlevelfis(((Olimpiatfis)another).getEcolimpiadlevelfis());
            setYear(((Olimpiatfis)another).getYear());
            setTitle(((Olimpiatfis)another).getTitle());
        }
    }

    public INaturalId<OlimpiatfisGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<OlimpiatfisGen>
    {
        private static final String PROXY_NAME = "OlimpiatfisNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof OlimpiatfisGen.NaturalId) ) return false;

            OlimpiatfisGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OlimpiatfisGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Olimpiatfis.class;
        }

        public T newInstance()
        {
            return (T) new Olimpiatfis();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "codeTU":
                    return obj.getCodeTU();
                case "OlympicNumber":
                    return obj.getOlympicNumber();
                case "ecolimpiadlevelfis":
                    return obj.getEcolimpiadlevelfis();
                case "Year":
                    return obj.getYear();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "codeTU":
                    obj.setCodeTU((String) value);
                    return;
                case "OlympicNumber":
                    obj.setOlympicNumber((String) value);
                    return;
                case "ecolimpiadlevelfis":
                    obj.setEcolimpiadlevelfis((Ecolimpiadlevelfis) value);
                    return;
                case "Year":
                    obj.setYear((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "codeTU":
                        return true;
                case "OlympicNumber":
                        return true;
                case "ecolimpiadlevelfis":
                        return true;
                case "Year":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "codeTU":
                    return true;
                case "OlympicNumber":
                    return true;
                case "ecolimpiadlevelfis":
                    return true;
                case "Year":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "codeTU":
                    return String.class;
                case "OlympicNumber":
                    return String.class;
                case "ecolimpiadlevelfis":
                    return Ecolimpiadlevelfis.class;
                case "Year":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Olimpiatfis> _dslPath = new Path<Olimpiatfis>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Olimpiatfis");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.fisrmc.entity.catalog.Olimpiatfis#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.catalog.Olimpiatfis#getCodeTU()
     */
    public static PropertyPath<String> codeTU()
    {
        return _dslPath.codeTU();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.catalog.Olimpiatfis#getOlympicNumber()
     */
    public static PropertyPath<String> OlympicNumber()
    {
        return _dslPath.OlympicNumber();
    }

    /**
     * @return Уровень олимпиады. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.catalog.Olimpiatfis#getEcolimpiadlevelfis()
     */
    public static Ecolimpiadlevelfis.Path<Ecolimpiadlevelfis> ecolimpiadlevelfis()
    {
        return _dslPath.ecolimpiadlevelfis();
    }

    /**
     * @return 
     * @see ru.tandemservice.fisrmc.entity.catalog.Olimpiatfis#getYear()
     */
    public static PropertyPath<String> Year()
    {
        return _dslPath.Year();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.fisrmc.entity.catalog.Olimpiatfis#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends Olimpiatfis> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _codeTU;
        private PropertyPath<String> _OlympicNumber;
        private Ecolimpiadlevelfis.Path<Ecolimpiadlevelfis> _ecolimpiadlevelfis;
        private PropertyPath<String> _Year;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.fisrmc.entity.catalog.Olimpiatfis#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(OlimpiatfisGen.P_CODE, this);
            return _code;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.catalog.Olimpiatfis#getCodeTU()
     */
        public PropertyPath<String> codeTU()
        {
            if(_codeTU == null )
                _codeTU = new PropertyPath<String>(OlimpiatfisGen.P_CODE_T_U, this);
            return _codeTU;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.catalog.Olimpiatfis#getOlympicNumber()
     */
        public PropertyPath<String> OlympicNumber()
        {
            if(_OlympicNumber == null )
                _OlympicNumber = new PropertyPath<String>(OlimpiatfisGen.P_OLYMPIC_NUMBER, this);
            return _OlympicNumber;
        }

    /**
     * @return Уровень олимпиады. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.catalog.Olimpiatfis#getEcolimpiadlevelfis()
     */
        public Ecolimpiadlevelfis.Path<Ecolimpiadlevelfis> ecolimpiadlevelfis()
        {
            if(_ecolimpiadlevelfis == null )
                _ecolimpiadlevelfis = new Ecolimpiadlevelfis.Path<Ecolimpiadlevelfis>(L_ECOLIMPIADLEVELFIS, this);
            return _ecolimpiadlevelfis;
        }

    /**
     * @return 
     * @see ru.tandemservice.fisrmc.entity.catalog.Olimpiatfis#getYear()
     */
        public PropertyPath<String> Year()
        {
            if(_Year == null )
                _Year = new PropertyPath<String>(OlimpiatfisGen.P_YEAR, this);
            return _Year;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.fisrmc.entity.catalog.Olimpiatfis#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(OlimpiatfisGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return Olimpiatfis.class;
        }

        public String getEntityName()
        {
            return "olimpiatfis";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
