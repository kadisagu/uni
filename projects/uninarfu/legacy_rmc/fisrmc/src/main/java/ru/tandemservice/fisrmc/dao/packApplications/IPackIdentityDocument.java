package ru.tandemservice.fisrmc.dao.packApplications;

import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;

public interface IPackIdentityDocument {


    /**
     * Необходимо создать
     * xml елемент <IdentityDocument>
     * в соответствии с описанием на странице 26
     * и возвратить его из данного метода
     *
     * @param doc
     * @param identityCard
     *
     * @return
     */
    public Element packIdentityDocument
    (
            Document doc
            , EntrantRequest entrantrequest
            , IdentityCard identityCard
    );
}
