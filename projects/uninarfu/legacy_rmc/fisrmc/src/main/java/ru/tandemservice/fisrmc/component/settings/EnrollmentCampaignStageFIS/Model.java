package ru.tandemservice.fisrmc.component.settings.EnrollmentCampaignStageFIS;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.fisrmc.entity.EnrollmentCampaignStageFIS;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.List;

public class Model implements IEnrollmentCampaignSelectModel {

    private DynamicListDataSource<EnrollmentCampaignStageFIS> dataSource;
    private List<EnrollmentCampaign> enrollmentCampaignList;
    private IDataSettings settings;

    public DynamicListDataSource<EnrollmentCampaignStageFIS> getDataSource() {
        return dataSource;
    }

    public void setDataSource(
            DynamicListDataSource<EnrollmentCampaignStageFIS> dataSource)
    {
        this.dataSource = dataSource;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign() {

        return (EnrollmentCampaign) this.settings.get("enrollmentCampaign");
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList() {
        return enrollmentCampaignList;
    }

    @Override
    public IDataSettings getSettings() {
        return this.settings;
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentcampaign) {
        this.settings.set("enrollmentCampaign", enrollmentcampaign);
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> list) {
        this.enrollmentCampaignList = list;
    }

}
