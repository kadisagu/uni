package ru.tandemservice.fisrmc.component.UploadFis2TU.Pub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BooleanColumn;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.fisrmc.entity.FisExportFileAction;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.unibase.UniBaseUtils;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = component.getModel();
        getDao().prepare(model);

        if (model.getSettings() == null)
            model.setSettings(component.getSettings());

        prepareListDateSource(component);
    }

    private void prepareListDateSource(IBusinessComponent component) {
        Model model = component.getModel();
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<FisExportFileAction> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Дата", FisExportFileAction.date(), DateFormatter.DATE_FORMATTER_WITH_TIME).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Операция", FisExportFileAction.action()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Описание", FisExportFileAction.description()).setClickable(false));
        dataSource.addColumn(new BooleanColumn("Нет ошибок", FisExportFileAction.ok()).setClickable(false));
        dataSource.addColumn(new IndicatorColumn("Отчет", null, "onClickDownloadErrors").defaultIndicator(new IndicatorColumn.Item("printer", "Печать")).setImageHeader(false).setDisableSecondSubmit(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteAction"));

        model.setDataSource(dataSource);
    }

    public void onClickCheckEntrantRequestDate(IBusinessComponent component) throws Exception {
        component.saveSettings();
        Model model = component.getModel();
        getDao().checkEntrantRequestDate(model);
    }

    public void onClickEdit(IBusinessComponent component) {
        Model model = component.getModel();
        activate(
                component,
                new ComponentActivator("ru.tandemservice.fisrmc.component.UploadFis2TU.AddEdit", new ParametersMap().add("publisherId", model.getEntity().getId()))
        );
    }

    public void onClickDownload(IBusinessComponent component) {
        Model model = component.getModel();
        byte[] content = model.getEntity().getXmlPackage().getContent();
        if (null == content)
            throw new ApplicationException("Запрашиваемый файл пуст.");
        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(content, model.getEntity().getXmlPackage().getFilename());
        activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap().add("id", id)));
    }

    public void onClickDeleteAction(IBusinessComponent component) {
        Long id = component.getListenerParameter();
        getDao().delete(id);
    }

    public void onClickDownloadErrors(IBusinessComponent component) {
        Model model = component.getModel();
        FisExportFileAction action = model.getDataSource().getRecordById((Long) component.getListenerParameter());
        if (null == action.getLog() || null == action.getLog().getContent()) {
            component.activateWarning("Файл отчета пуст");
            return;
        }

        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(action.getLog().getContent(), "error.rtf");
        activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap().add("id", id)));
    }
}
