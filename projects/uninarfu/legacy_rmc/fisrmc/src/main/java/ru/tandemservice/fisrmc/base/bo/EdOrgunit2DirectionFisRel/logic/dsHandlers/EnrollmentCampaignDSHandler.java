package ru.tandemservice.fisrmc.base.bo.EdOrgunit2DirectionFisRel.logic.dsHandlers;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

public class EnrollmentCampaignDSHandler extends DefaultComboDataSourceHandler {


    public EnrollmentCampaignDSHandler(String ownerId) {
        super(ownerId, EnrollmentCampaign.class);
    }


    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> arg0) {


    }


    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> arg0) {
        super.prepareOrders(arg0);
    }


}
