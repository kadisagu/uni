package ru.tandemservice.fisrmc.dao.dataForDelete;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.tandemservice.fisrmc.dao.ToolsDao;
import ru.tandemservice.fisrmc.dao.packApplications.Applications;
import ru.tandemservice.fisrmc.entity.FisPackages;
import ru.tandemservice.fisrmc.entity.FisPackages2enrollmentCampaign;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

public class DataForDeleteDAO extends UniBaseDao implements IDataForDeleteDAO {

    public static final String ELEMENT_ROOT = "Root";
    public static final String ELEMENT_DATA_FOR_DELETE = "DataForDelete";

    public static final String ELEMENT_APPLICATIONS = "Applications";

    public static final String ELEMENT_APPLICATION = "Application";
    public static final String ELEMENT_APPLICATION_NUMBER = "ApplicationNumber";
    public static final String ELEMENT_APPLICATION_DATE = "RegistrationDate";

    public static final String ELEMENT_ORDERS_OF_ADMISSION = "OrdersOfAdmission";

    private String msgLog = "";
    private DaemonParamsDeletePkg daemonParams;

    public void setParamns(DaemonParamsDeletePkg params) {
        this.daemonParams = params;
    }

    public void setMsgLog(String msg) {
        this.msgLog = msg;
    }

    public String getMsgLog() {
        return msgLog;
    }

    @Override
    @Transactional
    public FisPackages process() throws Exception {
        Writer outWriter = new StringWriter();
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

        Document doc = createDocument();

        StreamResult out = new StreamResult(outWriter);
        DOMSource source = new DOMSource(doc);
        transformer.transform(source, out);
        outWriter.flush();

        return saveDocument(outWriter.toString().getBytes());
    }

    @Override
    @Transactional
    public FisPackages saveDocument(byte[] content) {
        FisPackages pkg = new FisPackages();
        pkg.setFormingDate(new Date());
        pkg.setDeletePkgType(true);

        StringBuilder sb = new StringBuilder();
        if (daemonParams.isDeleteApplication()) {
            if (daemonParams.isAppDateWithoutTime())
                sb.append("Дата рег. заяв. без времени ").append(";");

            if (!daemonParams.getRegNumbers().isEmpty())
                sb.append("заяв №: ").append(StringUtils.join(daemonParams.getRegNumbers(), ",")).append(";");

            if (daemonParams.isBenefitRequest())
                sb.append(" льготные виды конкурса; ");

            if (daemonParams.isNotRF())
                sb.append(" абитур. не РФ; ");

            if (!daemonParams.getExcludeRegNumbers().isEmpty())
                sb.append("исключить заяв. №: ").append(StringUtils.join(daemonParams.getExcludeRegNumbers(), ",")).append(";");

        }

        pkg.setDescription(sb.toString());
        pkg.setDeleteApplication(daemonParams.isDeleteApplication());
        pkg.setDeleteOrdersOfAdmission(daemonParams.isDeleteOrdersOfAdmission());
        pkg.setDeleteCompetitiveGroupItems(daemonParams.isDeleteCompetitiveGroupItems());
        pkg.setDeleteEntranceTestResults(daemonParams.isDeleteEntranceTestResults());
        pkg.setDeleteApplicationCommonBenefits(daemonParams.isDeleteApplicationCommonBenefits());

        pkg.setEnrollmentCampaign(CommonBaseStringUtil.join(daemonParams.getEnrollmentCampaigns(), EnrollmentCampaign.title(), ", "));

        DatabaseFile xmlPackageFile = null;
        if (content != null) {
            xmlPackageFile = new DatabaseFile();
            xmlPackageFile.setContent(content);
            xmlPackageFile.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_XML);
            xmlPackageFile.setFilename("fis");

            saveOrUpdate(xmlPackageFile);
        }

        pkg.setXmlPackage(xmlPackageFile);
        pkg.setXmlErrors(saveErrors());
        saveOrUpdate(pkg);

        for (EnrollmentCampaign enrollmentCampaign : daemonParams.getEnrollmentCampaigns()) {
            FisPackages2enrollmentCampaign f2c = new FisPackages2enrollmentCampaign();
            f2c.setEnrollmentCampaign(enrollmentCampaign);
            f2c.setFisPackages(pkg);

            saveOrUpdate(f2c);
        }

        return pkg;
    }

    protected DatabaseFile saveErrors() {
        return null;
    }

    protected Document createDocument() throws Exception {
        List<EnrollmentCampaign> campaignList = getList(EnrollmentCampaign.class, UniBaseUtils.getIdList(daemonParams.getEnrollmentCampaigns()));

        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        Document document = docBuilder.newDocument();

        Element root = document.createElement(ELEMENT_ROOT);
        document.appendChild(root);

        fillAuthData(document, root);

        Element dataForDeleteElement = document.createElement(ELEMENT_DATA_FOR_DELETE);
        root.appendChild(dataForDeleteElement);

        if (daemonParams.isDeleteApplication()) {

            fillDeleteApplication(
                    document
                    , dataForDeleteElement
                    , daemonParams.isAppDateWithoutTime()
                    , campaignList
            );
        }

        // удаление абитуриентов, включенных в приказ
        if (daemonParams.isDeleteOrdersOfAdmission()) {
            fillDeleteOrdersOfAdmission(
                    document
                    , dataForDeleteElement
                    , daemonParams.isAppDateWithoutTime()
                    , campaignList
            );
        }

        if (daemonParams.isDeleteCompetitiveGroupItems()) {
            fillDeleteCompetitiveGroupItems
                    (
                            document
                            , dataForDeleteElement
                            , campaignList
                    );
        }

        return document;
    }


    private void fillDeleteCompetitiveGroupItems(Document document
            , Element parent
            , List<EnrollmentCampaign> campaignList)
    {


        return;
        /*
		EducationdirectionSpravType sType = getByCode(EducationdirectionSpravType.class, EducationdirectionSpravTypeCodes.TITLE_2014_TEST);
		List<XMLCompetitiveGroup> lstCompetitiveGroups = CompetitiveGroupsDAO.Instanse().getCompetitiveGroups(campaignList, sType, true);

		Element applicationsElement = document.createElement("CompetitiveGroups");
		parent.appendChild(applicationsElement);
		
		for (XMLCompetitiveGroup cg : lstCompetitiveGroups)
		{
			cg.getUid();
		}
		*/

    }

    protected void fillDeleteOrdersOfAdmission
            (
                    Document document
                    , Element parent
                    , boolean appDateWithoutTime
                    , List<EnrollmentCampaign> campaignList
            )
    {
        setMsgLog("Удаление включенных в приказ");

        Element applicationsElement = document.createElement(ELEMENT_ORDERS_OF_ADMISSION);
        parent.appendChild(applicationsElement);

        List<Object[]> list = getEntrantList(true);
        for (Object[] arr : list) {
            Integer regNumber = (Integer) arr[0];
            Date regDate = (Date) arr[1];

            Element applicationElement = document.createElement(ELEMENT_APPLICATION);
            applicationsElement.appendChild(applicationElement);

            ToolsDao.PackToElement(document, applicationElement, ELEMENT_APPLICATION_NUMBER, "" + regNumber);
            ToolsDao.PackToElement(document, applicationElement, ELEMENT_APPLICATION_DATE, regDate, true, appDateWithoutTime);
        }
    }


    protected void fillDeleteApplication
            (
                    Document document
                    , Element parent
                    , boolean appDateWithoutTime
                    , List<EnrollmentCampaign> campaignList
            )
    {
        setMsgLog("упаковка заявлений");

        Element applicationsElement = document.createElement(ELEMENT_APPLICATIONS);
        parent.appendChild(applicationsElement);

        List<Object[]> list = getEntrantList(false);
        for (Object[] arr : list) {
            Integer regNumber = (Integer) arr[0];
            Date regDate = (Date) arr[1];

            Element applicationElement = document.createElement(ELEMENT_APPLICATION);
            applicationsElement.appendChild(applicationElement);

            ToolsDao.PackToElement(document, applicationElement, ELEMENT_APPLICATION_NUMBER, "" + regNumber);
            ToolsDao.PackToElement(document, applicationElement, ELEMENT_APPLICATION_DATE, regDate, true, appDateWithoutTime);
        }
    }

    private List<Object[]> getEntrantList(boolean isOrderOfAdmission)
    {

        // dql списка абитуриентов
        DQLSelectBuilder dqlIn = Applications.Instanse().getEntrantRequestDQLList(
                daemonParams.getEnrollmentCampaigns()
                , daemonParams.isNotRF()
                , daemonParams.isBenefitRequest()

                , isOrderOfAdmission

                , daemonParams.getRegNumbers()
                , daemonParams.getExcludeRegNumbers()

                , null // дата с
                , null // дата по
                , false
                , false
                , false
                , false
                , null);

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EntrantRequest.class, "erList")
                .where(in(property(EntrantRequest.id().fromAlias("erList")), dqlIn.getQuery()))
                .column(DQLExpressions.property(EntrantRequest.regNumber().fromAlias("erList")))
                .column(DQLExpressions.property(EntrantRequest.regDate().fromAlias("erList")));


        return getList(dql);

    }

    protected void fillAuthData(Document document, Element parent) {
        Element authData = document.createElement("AuthData");
        parent.appendChild(authData);

        ToolsDao.PackToElement(document, authData, "Login", daemonParams.getUserName());
        ToolsDao.PackToElement(document, authData, "Pass", daemonParams.getUserPassword());
    }

}
