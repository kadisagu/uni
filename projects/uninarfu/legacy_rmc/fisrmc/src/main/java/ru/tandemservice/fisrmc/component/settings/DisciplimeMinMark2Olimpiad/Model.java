package ru.tandemservice.fisrmc.component.settings.DisciplimeMinMark2Olimpiad;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.fisrmc.entity.catalog.Ecdisciplinefis;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.List;

public class Model
        implements IEnrollmentCampaignSelectModel
{
    private DynamicListDataSource<Ecdisciplinefis> dataSource;
    private IDataSettings _settings;
    private List<EnrollmentCampaign> _enrollmentCampaignList;


    public DynamicListDataSource<Ecdisciplinefis> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<Ecdisciplinefis> dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign() {
        return (EnrollmentCampaign) getSettings().get(ENROLLMENT_CAMPAIGN_FILTER_NAME);
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList() {
        return _enrollmentCampaignList;
    }

    @Override
    public IDataSettings getSettings() {

        return _settings;
    }

    public void setSettings(IDataSettings settings) {
        _settings = settings;
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign) {
        getSettings().set(ENROLLMENT_CAMPAIGN_FILTER_NAME, enrollmentCampaign);

    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList) {
        _enrollmentCampaignList = enrollmentCampaignList;

    }

}
