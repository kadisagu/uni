package ru.tandemservice.fisrmc.component.settings.EnrollmentCampaignAddEdit;

import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.fisrmc.entity.EnrollmentCampaignFisExt;
import ru.tandemservice.fisrmc.util.FisrmcUtil;

public class DAO extends ru.tandemservice.uniecrmc.component.settings.EnrollmentCampaignAddEdit.DAO {

    @Override
    public void prepare(ru.tandemservice.uniec.component.settings.EnrollmentCampaignAddEdit.Model model) {
        super.prepare(model);
        Model myModel = (Model) model;
        EnrollmentCampaignFisExt enrollmentCampaignFisExt = FisrmcUtil.getEnrollmentCampaignFisExt(model.getEnrollmentCampaign());
        myModel.setEnrollmentCampaignFisExt(enrollmentCampaignFisExt);
    }

    @Override
    public void update(ru.tandemservice.uniec.component.settings.EnrollmentCampaignAddEdit.Model model) {
        super.update(model);
        Model myModel = (Model) model;
        if (FisrmcUtil.isValidRequestPrefix(myModel.getEnrollmentCampaignFisExt().getRequestPrefix()))
            saveOrUpdate(myModel.getEnrollmentCampaignFisExt());
        else
            throw new ApplicationException("Префикс номера заявления введен неверно. Значение должно быть больше 0 и меньше 100");
    }

}
