package ru.tandemservice.fisrmc.component.ImportFIS2TU;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import ru.tandemservice.fisrmc.entity.*;
import ru.tandemservice.fisrmc.entity.catalog.*;
import ru.tandemservice.uni.dao.UniDao;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DAO extends UniDao<Model> implements IDAO {

    public static String NEW_SPRAV_10 = "новый справочник";
    public static String NEW_SPRAV_10_delete = "удалено в ФИС";


    private boolean reloadSprav = false;

    private List<String> exsistCodes = new ArrayList<String>();
    private List<String> exsistCodesEcdisciplinefis = new ArrayList<String>();
    private List<String> exsistCodesCountry = new ArrayList<String>();
    private List<String> exsistCodeseError = new ArrayList<String>();
    private List<String> exsistCodesOlimpiat = new ArrayList<String>();

    private EducationdirectionSpravType spravType = null;

    private int _droppedRow = 0;

    @Override
    public void prepare(Model model)
    {
        model.setSpravTypeModel(
                new LazySimpleSelectModel<EducationdirectionSpravType>(EducationdirectionSpravType.class).setSortProperty(EducationdirectionSpravType.title().s()));

    }

    @Override
    public void update(Model model) {
        reloadSprav = model.getReloadSprav();
        exsistCodes = new ArrayList<String>();
        exsistCodesEcdisciplinefis = new ArrayList<String>();
        exsistCodesCountry = new ArrayList<String>();
        exsistCodeseError = new ArrayList<String>();
        exsistCodesOlimpiat = new ArrayList<String>();
        _droppedRow = 0;
        spravType = model.getSpravType();

        InputStream streem = null;
        if (model.getFile() == null)
            throw new ApplicationException("Не выбран файл справочника");

        streem = model.getFile().getStream();

        String str;

        try {
            byte[] content = IOUtils.toByteArray(streem);
            str = new String(content, "UTF-8");
            model.setXmldoc(str);

            streem = new ByteArrayInputStream(str.getBytes());

        }
        catch (IOException e) {

            throw new ApplicationException(e.getMessage(), e);
        }

        List<XMLParser.ItemData> list = new ArrayList<XMLParser.ItemData>();

        try {
            list = XMLParser.parse(streem);
        }
        catch (Exception e) {
            throw new ApplicationException("Ошибка парсинга " + e.getMessage(),
                                           e);
        }
        Map<String, String> codesTUByTitleMap = getCodesTUByTitleMap();


        for (XMLParser.ItemData item : list)
            process(item, codesTUByTitleMap, spravType);

        getSession().flush();

        if (exsistCodes.size() > 0)
            _dropUnuseEducationDirection();
        if (exsistCodesEcdisciplinefis.size() > 0)
            _dropUnuseEcdisciplinefis();
        if (exsistCodeseError.size() > 0)
            _dropUnuseError();

        if (exsistCodesOlimpiat.size() > 0)
            _dropUnuseOlimpiat();
        if (exsistCodesCountry.size() > 0)
            _dropUnuseCountry();
    }

    private void _dropUnuseOlimpiat() {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(
                Olimpiatfis.class, "o");
        List<Olimpiatfis> lst = builder.createStatement(getSession()).list();

        for (Olimpiatfis olimpiat : lst) {
            if (!exsistCodesOlimpiat.contains(olimpiat.getCode())) {
                _dropRelation(olimpiat);
                delete(olimpiat);
            }
        }
    }

    private void _dropUnuseError() {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(
                Errorcodesfis.class, "e");
        List<Errorcodesfis> lst = builder.createStatement(getSession()).list();

        for (Errorcodesfis error : lst) {
            if (!exsistCodeseError.contains(error.getCode())) {
                delete(error);
            }
        }
    }

    private void _dropUnuseCountry() {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(
                Countryfis.class, "ec");
        // .where(DQLExpressions.notIn(DQLExpressions.property(Countryfis.code().fromAlias("edf")),
        // exsistCodesCountry ));
        List<Countryfis> lst = builder.createStatement(getSession()).list();

        for (Countryfis country : lst) {
            if (!exsistCodesCountry.contains(country.getCode())) {
                delete(country);
            }
        }
    }

    /**
     * уничтожаем неиспользуемые записи в Educationdirectionfis
     */
    private void _dropUnuseEducationDirection() {

        // так глупо, потомучто in имеет ограницения в TU не более 2000
        // вхождений

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(
                Educationdirectionfis.class, "edf");
        // учитываем тип справочника (в понимании фис)
        builder.where(DQLExpressions.eq(DQLExpressions.property(Educationdirectionfis.spravType().fromAlias("edf")), DQLExpressions.value(this.spravType)));

        List<Educationdirectionfis> educationdirectionfis = builder
                .createStatement(getSession()).list();

        for (Educationdirectionfis edf : educationdirectionfis) {
            if (!exsistCodes.contains(edf.getCode()))
                delete(edf);
        }
    }

    private void _dropUnuseEcdisciplinefis() {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(
                Ecdisciplinefis.class, "ef");
        List<Ecdisciplinefis> ecdisciplinefis = builder.createStatement(
                getSession()).list();

        for (Ecdisciplinefis ef : ecdisciplinefis) {
            if (!exsistCodesEcdisciplinefis.contains(ef.getCode())) {
                _dropRelation(ef);
                delete(ef);
            }
        }
    }

    private void process(XMLParser.ItemData item
            , Map<String, String> codesTUByTitleMap
            , EducationdirectionSpravType spravType
    )
    {
        if (Educationdirectionfis.ENTITY_NAME.equals(item.getEntityName())) {
            if (reloadSprav)
                processReloadEducationDirection(item.getProperties(), spravType);
            else
                processEducationDirection(item.getProperties(), spravType);
        }
        else if (Countryfis.ENTITY_NAME.equals(item.getEntityName())) {
            processCountries(item.getProperties(), codesTUByTitleMap);
        }
        else if (Ecdisciplinefis.ENTITY_NAME.equals(item.getEntityName())) {
            processEcdisciplinefis(item.getProperties(), reloadSprav);
        }
        else if (Errorcodesfis.ENTITY_NAME.equals(item.getEntityName())) {
            processErrorcodesfis(item.getProperties());
        }
        else if (Olimpiatfis.ENTITY_NAME.equals(item.getEntityName()))
            processOlimpiatfis(item.getProperties(), reloadSprav);

        // TODO: add other dictionaries
    }


    private void processReloadEducationDirection(
            Map<String, Object> properties
            , EducationdirectionSpravType spravType)
    {
        // обработка одной позиции
        String code = getPropertyName(properties.get("ID").toString());
        // прописываются все коды нового справочника (возможны повторы со старым)
        exsistCodes.add(code);

        // полностью логика вставки
        processEducationDirection(properties, spravType);

    }


    /**
     * ищем наиболее подходящее
     *
     * @param _entityTemp
     *
     * @return
     */
    private Educationdirectionfis _findPosition
    (
            Educationdirectionfis find
            , EducationdirectionSpravType spravType
    )
    {
        String codefis = find.getCodefis();
        String qualificationCode = find.getQualificationCode();
        String title = find.getTitle();

        // 1 ищем в сопоставленных направлениях (полное совпадение, включая имя)
        DQLSelectBuilder dqle2e = new DQLSelectBuilder().fromEntity(
                EdOrgunit2DirectionFis.class, "edf");
        dqle2e.where(DQLExpressions.eq(DQLExpressions.property(EdOrgunit2DirectionFis.educationDirectionFis().Codefis().fromAlias("edf")), DQLExpressions.value(codefis)))
                .where(DQLExpressions.eq(DQLExpressions.property(EdOrgunit2DirectionFis.educationDirectionFis().QualificationCode().fromAlias("edf")), DQLExpressions.value(qualificationCode)))
                .where(DQLExpressions.eq(DQLExpressions.property(EdOrgunit2DirectionFis.educationDirectionFis().title().fromAlias("edf")), DQLExpressions.value(title)))
                .setPredicate(DQLPredicateType.distinct)
                .column(DQLExpressions.property(EdOrgunit2DirectionFis.educationDirectionFis().fromAlias("edf")));

        List<Educationdirectionfis> lstCompare = dqle2e
                .createStatement(getSession()).list();

        if (!lstCompare.isEmpty() && lstCompare.size() == 1)
            // полное совпадение - нужно переписать код
            return lstCompare.get(0);

        // в сопоставленных направлениях нет полного совпадения
        // сущ. записи нас не особо интересуют, можно просто удалить
        return null;


//		DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(
//				Educationdirectionfis.class, "edf");
//		
//		dql.where(DQLExpressions.eq(DQLExpressions.property(Educationdirectionfis.Codefis().fromAlias("edf")), DQLExpressions.value(find.getCodefis())))
//		   .where(DQLExpressions.eq(DQLExpressions.property(Educationdirectionfis.QualificationCode().fromAlias("edf")), DQLExpressions.value(find.getQualificationCode())));
//		
//		List<Educationdirectionfis> lst = dql
//				.createStatement(getSession()).list();
//
//		
//		if (lst.isEmpty())
//			// не найдено
//			return null;
//		else
//		{
//			if (lst.size()==1)
//				return lst.get(0); 
//			else
//			{
//				// записей >1 проверим, с чем сопоставлено
//				// если запись окажется одна - выходим из цикла
//				DQLSelectBuilder dqle2e = new DQLSelectBuilder().fromEntity(
//						EdOrgunit2DirectionFis.class, "edf");
//				dqle2e.where(DQLExpressions.in(DQLExpressions.property(EdOrgunit2DirectionFis.educationDirectionFis().fromAlias("edf")), lst))
//				.setPredicate(DQLPredicateType.distinct)
//				.column(DQLExpressions.property(EdOrgunit2DirectionFis.educationDirectionFis().fromAlias("edf")));
//
//				List<Educationdirectionfis> lstCompare = dqle2e
//						.createStatement(getSession()).list();
//				
//				if (!lstCompare.isEmpty() && lstCompare.size()==1)
//					return lstCompare.get(0);
//				
//				// либо не сопоставлено, либо сопоставлено >1 раза
//				
//				Educationdirectionfis findPos = null;
//				int fCount=0;
//				// поискать до полного совпадения
//				for (Educationdirectionfis elevel : lst)
//				{
//					// по имени не ищем
//					// проверяем по ugscode
//					if (elevel.getUGSCode()!=null && elevel.getUGSCode()!="")
//					{
//						if (elevel.getUGSCode().equals(find.getUGSCode()))
//						{
//							fCount++;
//							findPos = elevel;
//						}
//					}
//					else
//					{
//						// считаем, что найдено
//						fCount++;
//						findPos = elevel;
//					}
//				}
//				
//				if (fCount==1)
//					return findPos;
//				else
//					return null;
//					
//			}
//		}
    }

//	/**
//	 * в режиме - перегрузить справочник
//	 * 
//	 * @param properties
//	 */
//	private void processReloadEducationDirection_old(Map<String, Object> properties) 
//	{
//		// обработка одной позиции
//		String code = getPropertyName(properties.get("ID").toString());
//		exsistCodes.add(code);
//		
//		Educationdirectionfis entity = this.getCatalogItem(
//				Educationdirectionfis.class, code);
//
//		if (entity == null) {
//			entity = new Educationdirectionfis();
//			
//			_fillEduDirectionEntity(properties, entity);
//			
//			saveOrUpdate(entity);
//		} else 
//		{
//			// сущность есть, проверяем необходимость внесения изменений
//			Educationdirectionfis _entityTemp = _fillEduDirectionEntity(properties, null);
//			if (_entityTemp.isEqualByCodes(entity)) {
//
//				if (!_entityTemp.isEqualAll(entity)) {
//					_copyEducationdirectionfis(_entityTemp, entity);
//					saveOrUpdate(entity);
//				} else
//					// ну делать ничего не надо
//					return;
//			} else {
//				// по кодам не совпало
//				// необходимо переписать, связь сопоставления порушить
//				_copyEducationdirectionfis(_entityTemp, entity);
//				saveOrUpdate(entity);
//
//				// если все равно, кроме codefic
//				_dropRelation(entity);
//			}
//		}
//	}

    private void _dropRelation(Object entity) {
        if (entity instanceof Educationdirectionfis) {
            List<EdOrgunit2DirectionFis> lst = getList(
                    EdOrgunit2DirectionFis.class,
                    EdOrgunit2DirectionFis.L_EDUCATION_DIRECTION_FIS,
                    (Educationdirectionfis) entity);
            for (EdOrgunit2DirectionFis ed : lst) {
                _droppedRow++;
                delete(ed);
            }
        }
        else if (entity instanceof Ecdisciplinefis) {
            List<Olimpiatfis2ecdisciplinefis> listO2E = getList(
                    Olimpiatfis2ecdisciplinefis.class,
                    Olimpiatfis2ecdisciplinefis.L_ECDISCIPLINEFIS,
                    (Ecdisciplinefis) entity);
            for (Olimpiatfis2ecdisciplinefis o2e : listO2E) {
                _droppedRow++;
                delete(o2e);
            }
            List<StateExamSubject2Ecdisciplinefis> listSES2E = getList(
                    StateExamSubject2Ecdisciplinefis.class,
                    StateExamSubject2Ecdisciplinefis.L_ECDISCIPLINEFIS,
                    (Ecdisciplinefis) entity);
            for (StateExamSubject2Ecdisciplinefis ses2e : listSES2E) {
                _droppedRow++;
                delete(ses2e);
            }
            List<EducationSubject2Ecdisciplinefis> listES2E = getList(
                    EducationSubject2Ecdisciplinefis.class,
                    EducationSubject2Ecdisciplinefis.L_ECDISCIPLINEFIS,
                    (Ecdisciplinefis) entity);
            for (EducationSubject2Ecdisciplinefis es2e : listES2E) {
                _droppedRow++;
                delete(es2e);
            }
        }
        else if (entity instanceof Olimpiatfis) {
            List<Olimpiatfis2ecdisciplinefis> listO2E = getList(
                    Olimpiatfis2ecdisciplinefis.class,
                    Olimpiatfis2ecdisciplinefis.L_OLIMPIATFIS,
                    (Olimpiatfis) entity);
            for (Olimpiatfis2ecdisciplinefis o2e : listO2E) {
                _droppedRow++;
                delete(o2e);
            }

        }
    }

    private void _copyEducationdirectionfis(Educationdirectionfis from,
                                            Educationdirectionfis to)
    {
        to.setCodefis(from.getCodefis());
        to.setQualificationCode(from.getQualificationCode());
        to.setPeriod(from.getPeriod());
        to.setUGSCode(from.getUGSCode());
        to.setUGSName(from.getUGSName());
    }

    private Educationdirectionfis _fillEduDirectionEntity(
            Map<String, Object> properties, Educationdirectionfis entity)
    {

        if (entity == null)
            entity = new Educationdirectionfis();

        _fillEntityBase(properties, entity);
        //entity.setEducationLevels(null);
        return entity;
    }

    private void processOlimpiatfis(Map<String, Object> properties,
                                    boolean reload)
    {
        // обработка одной позиции
        if (properties.get("OlympicID") == null)
            return;

        String code = getPropertyName(properties.get("OlympicID").toString());
        System.out.println(code);


        if (reload)
            exsistCodesOlimpiat.add(code);

        String olympicLevelID = "1";
        if (properties.get("OlympicLevelID") != null)
            olympicLevelID = getPropertyName(properties.get("OlympicLevelID").toString());
        ;

        Ecolimpiadlevelfis levelFis = getCatalogItem(Ecolimpiadlevelfis.class, olympicLevelID);
        if (levelFis == null)
            return;

        Olimpiatfis entity = this.getCatalogItem(Olimpiatfis.class, code);
        if (entity == null)
            entity = new Olimpiatfis();

        _fillEntityBase(properties, entity);
        entity.setEcolimpiadlevelfis(levelFis);
        entity.setCodeTU("");
        saveOrUpdate(entity);

        _fillOlimpiadRelation(properties, entity);

        getSession().flush();
    }

    private void _fillOlimpiadRelation(Map<String, Object> properties, Olimpiatfis entity)
    {
        // получим список дисциплин
        List mapSubjects = null;
        if (properties.get("Subjects") != null)
            mapSubjects = (List) properties.get("Subjects");
        else
            return;
        List<PairKey<String, String>> lstSubject = new ArrayList<>();

        for (Object subject : mapSubjects) {
            // subject
            Map<String, Object> mapSubject = (Map<String, Object>) subject;
            for (Object obj : mapSubject.values()) {
                List subjectProp = (List) obj;
                String SubjectID = "";
                String LevelID = "";

                for (Object prop : subjectProp) {
                    // или Subject или Level
                    if (prop != null && prop instanceof Map) {
                        Map propMap = (Map) prop;

                        if (propMap.keySet().contains("SubjectID"))
                            SubjectID = (String) propMap.get("SubjectID");

                        if (propMap.keySet().contains("LevelID"))
                            LevelID = (String) propMap.get("LevelID");
                    }
                }
                // бывает, что level не указан, тогда левел должен быть у самой олимпиады
                if (LevelID.isEmpty())
                    LevelID = entity.getEcolimpiadlevelfis().getCode();

                // формируем пары (дисциплина, уровень олимпиады)
                PairKey<String, String> pair = new PairKey<String, String>(SubjectID, LevelID);

                if (!lstSubject.contains(pair))
                    lstSubject.add(pair);
            }

        }

        // получим уже имеющиеся связи
        List<Olimpiatfis2ecdisciplinefis> listO2E = getList(
                Olimpiatfis2ecdisciplinefis.class,
                Olimpiatfis2ecdisciplinefis.L_OLIMPIATFIS,
                entity);

        for (Olimpiatfis2ecdisciplinefis o2e : listO2E) {
            String SubjectID = o2e.getEcdisciplinefis().getCode();
            String LevelID = o2e.getEcolimpiadlevelfis().getCode();

            PairKey<String, String> keyFind = new PairKey<String, String>(SubjectID, LevelID);


            if (!lstSubject.contains(keyFind))
                // такая связь не нужна
                delete(o2e);
            else
                // связь существует
                lstSubject.remove(keyFind);
        }


        // проверяем наличие связей
        // если связи нет - создаем
        for (PairKey<String, String> pKey : lstSubject) {
            String SubjectID = pKey.getFirst();
            String LevelID = pKey.getSecond();

            // ищем дисциплину
            Ecdisciplinefis ecdisciplinefis = getCatalogItem(Ecdisciplinefis.class, SubjectID);
            if (ecdisciplinefis == null)
                throw new ApplicationException("По коду " + SubjectID + " не найден общеобразовательный предмет.");

            Ecolimpiadlevelfis ecolimpiadlevelfis = getCatalogItem(Ecolimpiadlevelfis.class, LevelID);
            if (ecolimpiadlevelfis == null)
                throw new ApplicationException("По коду " + LevelID + " не найден уровень олимпиады.");

            Olimpiatfis2ecdisciplinefis o2d = new Olimpiatfis2ecdisciplinefis();
            o2d.setEcdisciplinefis(ecdisciplinefis);
            o2d.setOlimpiatfis(entity);
            o2d.setEcolimpiadlevelfis(ecolimpiadlevelfis);

            saveOrUpdate(o2d);
        }
    }


    /**
     * Загрузка справочника НП
     *
     * @param properties
     */
    private void processEducationDirection(
            Map<String, Object> properties
            , EducationdirectionSpravType spravType
    )
    {

        String code = getPropertyName(properties.get("ID").toString());
        List<Educationdirectionfis> entityList = findEducationDirection(code, spravType);
        //this.get(Educationdirectionfis.class, Educationdirectionfis.P_CODE, code);

        Educationdirectionfis entity = null;
        if (entityList != null && entityList.size() == 1)
            entity = entityList.get(0);
        else {
            // если оказалось 2 одинаковых записи - это жопа какая-то
            // нужно как-то удалить лишнее???
            if (entityList != null && entityList.size() > 1) {
                // есть лишние записи
                // понимаю, что сносим часть настроек (возможно)
                int i = 0;
                for (Educationdirectionfis ed : entityList) {
                    if (i != 0)
                        delete(ed);
                    i++;
                }
            }
        }


        if (entity == null)
            entity = new Educationdirectionfis();

        _fillEntityBase(properties, entity);

        // тип справочника
        entity.setSpravType(spravType);
        saveOrUpdate(entity);
    }

    // найти нужное нам направление подготовки
    private List<Educationdirectionfis> findEducationDirection(String code,
                                                               EducationdirectionSpravType spravType)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(
                Educationdirectionfis.class, "e");

        builder.where(DQLExpressions.eq(DQLExpressions.property(Educationdirectionfis.spravType().fromAlias("e")), DQLExpressions.value(spravType)));
        builder.where(DQLExpressions.eq(DQLExpressions.property(Educationdirectionfis.code().fromAlias("e")), DQLExpressions.value(code)));

        List<Educationdirectionfis> lst = builder.createStatement(getSession()).list();
        return lst;
    }

    private void processEcdisciplinefis(Map<String, Object> properties, boolean reload) {
        // обработка одной позиции
        String code = getPropertyName(properties.get("ID").toString());

        if (reload)
            exsistCodesEcdisciplinefis.add(code);

        Ecdisciplinefis _entityTemp = _fillEcdisciplinefisEntity(properties, null);
        Ecdisciplinefis entity = this.getCatalogItem(Ecdisciplinefis.class, code);

        if (entity == null)
            entity = new Ecdisciplinefis();
        else {
            if (!_entityTemp.getTitle().toLowerCase().equals(entity.getTitle().toLowerCase()))
                // по названию не совпало
                _dropRelation(entity);
        }
        _copyEcdisciplinefis(_entityTemp, entity);
        saveOrUpdate(entity);
    }

    private Ecdisciplinefis _fillEcdisciplinefisEntity(
            Map<String, Object> properties, Ecdisciplinefis entity)
    {
        if (entity == null)
            entity = new Ecdisciplinefis();

        _fillEntityBase(properties, entity);

        entity.setCodeTU("");
        return entity;
    }

    private void _copyEcdisciplinefis(Ecdisciplinefis from, Ecdisciplinefis to) {

        to.setCode(from.getCode());
        to.setCodeTU(from.getCodeTU());
        to.setTitle(from.getTitle());
    }

    private String getPropertyName(String propertyName) {
        if ("Name".equals(propertyName) || "OlympicName".equals(propertyName))
            return "title";
        if ("Code".equals(propertyName))
            return "codefis";
        if ("ID".equals(propertyName) || "OlympicID".equals(propertyName))
            return "code";
        if ("UGSCode".equals(propertyName) || "UGSName".equals(propertyName)
                || "OlympicNumber".equals(propertyName)
                || "OlympicLevelID".equals(propertyName))
            return propertyName;

        return WordUtils.uncapitalize(propertyName);
    }

    private final static Map<String, String> HARDCODE_CODESTU_BY_CODE_MAP = new HashMap<String, String>();

    static {
        HARDCODE_CODESTU_BY_CODE_MAP.put("1", "0"); // Российская Федерация
        HARDCODE_CODESTU_BY_CODE_MAP.put("228", "149"); // ВЕНЕСУЭЛА
        HARDCODE_CODESTU_BY_CODE_MAP.put("232", "223"); // МАКЕДОНИЯ
        HARDCODE_CODESTU_BY_CODE_MAP.put("233", "264"); // ПАЛЕСТИНА
        HARDCODE_CODESTU_BY_CODE_MAP.put("99", "208"); // КОТ Д`ИВУАР
    }

    private Map<String, String> getCodesTUByTitleMap() {
        Map<String, String> result = new HashMap<String, String>();
        List<AddressCountry> items = getList(AddressCountry.class);
        if (items != null) {
            for (AddressCountry item : items) {
                String title = StringUtils.deleteWhitespace(item.getTitle())
                        .toLowerCase();
                result.put(title, String.valueOf(item.getCode()));
            }
        }
        return result;
    }

    private void processErrorcodesfis(Map<String, Object> properties) {
        String code = getPropertyName(properties.get("ID").toString());
        if (reloadSprav)
            exsistCodeseError.add(code);
        Errorcodesfis entity = this.getCatalogItem(Errorcodesfis.class, code);
        if (entity == null)
            entity = new Errorcodesfis();

        _fillEntityBase(properties, entity);

        entity.setCodeTU(code);
        saveOrUpdate(entity);
    }

    private EntityBase _fillEntityBase(Map<String, Object> properties, EntityBase entity)
    {
        for (Map.Entry<String, Object> entry : properties.entrySet()) {
            String name = getPropertyName(entry.getKey());
            if ("#text".equals(name))
                continue;

            if (_hasProperty(entity, name)) {
                String value = entry.getValue().toString();
                entity.setProperty(name, value);
            }
            else
                System.out.println("свойство " + name + " для сущности " + entity.getClass().getName() + " не найдено ");
        }
        return entity;
    }

    private Map<String, Boolean> mapProp = new HashMap<String, Boolean>();

    private boolean _hasProperty(EntityBase entity, String name)
    {
        boolean retVal = false;
        String key = entity.getClass().getName() + name;

        if (mapProp.containsKey(key))
            return mapProp.get(key);
        try {
            entity.getProperty(name);
            // если ошибки нет - свойство есть
            retVal = true;
        }
        catch (Exception ex) {
            retVal = false;
        }
        mapProp.put(key, retVal);
        return retVal;
    }

    private void processCountries(Map<String, Object> properties,
                                  Map<String, String> codesTUByTitleMap)
    {
        String code = getPropertyName(properties.get("ID").toString());

        if (reloadSprav)
            exsistCodesCountry.add(code);

        Countryfis entity = this.getCatalogItem(Countryfis.class, code);
        if (entity == null)
            entity = new Countryfis();

        _fillEntityBase(properties, entity);

        String title = (entity.getTitle() == null) ? "" : StringUtils
                .deleteWhitespace(entity.getTitle()).toLowerCase();

        String codeTU = codesTUByTitleMap.get(title);

        // отключаю хард код частично
        if (codeTU == null)
            codeTU = HARDCODE_CODESTU_BY_CODE_MAP.get(entity.getCode());

        entity.setCodeTU(codeTU);
        saveOrUpdate(entity);
    }
}
