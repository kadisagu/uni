package ru.tandemservice.fisrmc.component.settings.Eceducationlevelfis2TU;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import ru.tandemservice.fisrmc.entity.EduProgramQualificationToFis;
import ru.tandemservice.fisrmc.entity.QualificationToFis;
import ru.tandemservice.fisrmc.entity.catalog.Eceducationlevelfis;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model)
    {

        model.setQualificationValueList(
                new DQLFullCheckSelectModel(Qualifications.code().s(), Qualifications.title().s()) {
                    @Override
                    protected DQLSelectBuilder query(String alias, String filter)
                    {
                        DQLSelectBuilder builder = new DQLSelectBuilder()
                                .fromEntity(Qualifications.class, alias)
                                        //.where(DQLExpressions.eqValue( DQLExpressions.property(Entrant.enrollmentCampaign().fromAlias(alias)), model.getEnrollmentCampaign() ))
                                .order(DQLExpressions.property(Qualifications.title().fromAlias(alias)));
                        if (filter != null && !filter.isEmpty()) {
                            builder.where(DQLExpressions.likeUpper(
                                    DQLExpressions.property(Qualifications.title().fromAlias(alias)),
                                    DQLExpressions.value(CoreStringUtils.escapeLike(filter))
                            ));
                        }
                        return builder;
                    }
                });

        model.setQualificationOPValueList(
                new DQLFullCheckSelectModel(EduProgramQualification.qualificationLevelCode().s(), EduProgramQualification.title().s()) {
                    @Override
                    protected DQLSelectBuilder query(String alias, String filter)
                    {
                        DQLSelectBuilder builder = new DQLSelectBuilder()
                                .fromEntity(EduProgramQualification.class, alias)
                                        //.where(DQLExpressions.eqValue( DQLExpressions.property(Entrant.enrollmentCampaign().fromAlias(alias)), model.getEnrollmentCampaign() ))

                                .order(DQLExpressions.property(EduProgramQualification.qualificationLevelCode().fromAlias(alias)))
                                .order(DQLExpressions.property(EduProgramQualification.code().fromAlias(alias)))
                                .order(DQLExpressions.property(EduProgramQualification.title().fromAlias(alias)));

                        if (filter != null && !filter.isEmpty()) {
                            builder.where(DQLExpressions.likeUpper(
                                    DQLExpressions.property(EduProgramQualification.title().fromAlias(alias)),
                                    DQLExpressions.value(CoreStringUtils.escapeLike(filter))
                            ));
                        }

                        builder.where(DQLExpressions.exists(EducationLevelsHighSchool.class,
                                                            EducationLevelsHighSchool.assignedQualification().s(), DQLExpressions.property(alias)));

                        return builder;
                    }
                });


    }

    @SuppressWarnings("unchecked")
    @Override
    public void prepareListDataSource(Model model)
    {
        DynamicListDataSource<Eceducationlevelfis> dataSource = model.getDataSource();

        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(Eceducationlevelfis.class, "en")
                .order(DQLExpressions.property(Eceducationlevelfis.title().fromAlias("en")), OrderDirection.desc);
        UniBaseUtils.createPage(dataSource, builder, getSession());
        List<Eceducationlevelfis> lst = getList(builder);

        BlockColumn<List<Qualifications>> columnQ = (BlockColumn<List<Qualifications>>) dataSource.getColumn("qualifications");
        BlockColumn<List<EduProgramQualification>> columnEPQ = (BlockColumn<List<EduProgramQualification>>) dataSource.getColumn("eduProgramQualification");


        //dataSource.getColumnValue();

        Map<Long, List<Qualifications>> mapQ = new HashMap<>();
        Map<Long, List<EduProgramQualification>> mapEPQ = new HashMap<>();


        for (Eceducationlevelfis elf : lst) {
            DQLSelectBuilder qList = new DQLSelectBuilder()
                    .fromEntity(QualificationToFis.class, "en")
                    .column(DQLExpressions.property(QualificationToFis.qualifications().fromAlias("en")))
                    .where(DQLExpressions.eq(DQLExpressions.property(QualificationToFis.eceducationlevelfis().fromAlias("en")), DQLExpressions.value(elf)))
                    .order(DQLExpressions.property(QualificationToFis.qualifications().title().fromAlias("en")));

            List<Qualifications> lstQ = getList(qList);
            mapQ.put(elf.getId(), lstQ);


            DQLSelectBuilder edQList = new DQLSelectBuilder()
                    .fromEntity(EduProgramQualificationToFis.class, "en")
                    .column(DQLExpressions.property(EduProgramQualificationToFis.qualifications().fromAlias("en")))
                    .where(DQLExpressions.eq(DQLExpressions.property(EduProgramQualificationToFis.eceducationlevelfis().fromAlias("en")), DQLExpressions.value(elf)))
                    .order(DQLExpressions.property(EduProgramQualificationToFis.qualifications().title().fromAlias("en")));

            List<EduProgramQualification> lstEDQ = getList(edQList);
            mapEPQ.put(elf.getId(), lstEDQ);

        }

        columnQ.setValueMap(mapQ);
        columnEPQ.setValueMap(mapEPQ);

    }


    @SuppressWarnings("unchecked")
    public void update(Model model)
    {
        DynamicListDataSource<Eceducationlevelfis> dataSource = model.getDataSource();

        BlockColumn<List<Qualifications>> columnQ = (BlockColumn<List<Qualifications>>) dataSource.getColumn("qualifications");
        BlockColumn<List<EduProgramQualification>> columnEPQ = (BlockColumn<List<EduProgramQualification>>) dataSource.getColumn("eduProgramQualification");

        Map<Long, List<Qualifications>> mapQ = columnQ.getValueMap();
        Map<Long, List<EduProgramQualification>> mapOPQ = columnEPQ.getValueMap();


        List<Eceducationlevelfis> lst = dataSource.getEntityList();


        for (Eceducationlevelfis level : lst) {
            List<Qualifications> lstQ = mapQ.get(level.getId());
            List<EduProgramQualification> lstOPQ = mapOPQ.get(level.getId());


            saveOrUpdateQ(level, lstQ);
            saveOrUpdateQOP(level, lstOPQ);
        }
    }

    private void saveOrUpdateQOP(Eceducationlevelfis level,
                                 List<EduProgramQualification> lstQ)
    {
        // получим актуальный набор
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(EduProgramQualificationToFis.class, "en")
                .where(DQLExpressions.eq(DQLExpressions.property(EduProgramQualificationToFis.eceducationlevelfis().fromAlias("en")), DQLExpressions.value(level)))
                .column(DQLExpressions.property(EduProgramQualificationToFis.qualifications().fromAlias("en")));

        List<EduProgramQualification> exsist = getList(builder);

        // то, что надо
        for (EduProgramQualification q : lstQ) {
            if (!exsist.contains(q)) {
                String code = "auto:" + q.getCode() + ":" + level.getCode();

                EduProgramQualificationToFis rel = new EduProgramQualificationToFis();
                rel.setQualifications(q);
                rel.setEceducationlevelfis(level);
                rel.setCode(code);

                saveOrUpdate(rel);

            }
            else
                exsist.remove(q);
        }

        for (EduProgramQualification q : exsist) {
            DQLDeleteBuilder ddb = new DQLDeleteBuilder(EduProgramQualificationToFis.class)
                    .where(DQLExpressions.eq(DQLExpressions.property(EduProgramQualificationToFis.qualifications().id()), DQLExpressions.value(q.getId())))
                    .where(DQLExpressions.eq(DQLExpressions.property(EduProgramQualificationToFis.eceducationlevelfis().id()), DQLExpressions.value(level.getId())));

            ddb.createStatement(getSession())
                    .execute();

        }
    }

    private void saveOrUpdateQ(Eceducationlevelfis level,
                               List<Qualifications> lstQ)
    {
        // получим актуальный набор
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(QualificationToFis.class, "en")
                .where(DQLExpressions.eq(DQLExpressions.property(QualificationToFis.eceducationlevelfis().fromAlias("en")), DQLExpressions.value(level)))
                .column(DQLExpressions.property(QualificationToFis.qualifications().fromAlias("en")));

        List<Qualifications> exsist = getList(builder);

        // то, что надо
        for (Qualifications q : lstQ) {
            if (!exsist.contains(q)) {
                String code = "auto:" + q.getCode() + ":" + level.getCode();

                QualificationToFis rel = new QualificationToFis();
                rel.setQualifications(q);
                rel.setEceducationlevelfis(level);
                rel.setCode(code);

                saveOrUpdate(rel);

            }
            else
                exsist.remove(q);

        }

        for (Qualifications q : exsist) {
            DQLDeleteBuilder ddb = new DQLDeleteBuilder(QualificationToFis.class)
                    .where(DQLExpressions.eq(DQLExpressions.property(QualificationToFis.qualifications().id()), DQLExpressions.value(q.getId())))
                    .where(DQLExpressions.eq(DQLExpressions.property(QualificationToFis.eceducationlevelfis().id()), DQLExpressions.value(level.getId())));

            ddb.createStatement(getSession())
                    .execute();

        }
    }

}
