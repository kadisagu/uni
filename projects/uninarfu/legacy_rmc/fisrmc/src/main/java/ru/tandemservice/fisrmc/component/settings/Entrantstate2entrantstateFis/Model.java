package ru.tandemservice.fisrmc.component.settings.Entrantstate2entrantstateFis;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.fisrmc.entity.Entrantstate2entrantstateFis;
import ru.tandemservice.fisrmc.entity.catalog.Ecrequestfis;
import ru.tandemservice.uniec.entity.catalog.EntrantState;

import java.util.List;

public class Model {

    private String selectedPage;
    private DynamicListDataSource<Entrantstate2entrantstateFis> dataSource;
    private IDataSettings settings;

    private List<EntrantState> entrantStateList;
    private List<Ecrequestfis> ecrequestfisList;
    private List<Entrantstate2entrantstateFis> states;


    public String getSelectedPage() {
        return selectedPage;
    }

    public void setSelectedPage(String selectedPage) {
        this.selectedPage = selectedPage;
    }

    public DynamicListDataSource<Entrantstate2entrantstateFis> getDataSource() {
        return dataSource;
    }

    public void setDataSource(
            DynamicListDataSource<Entrantstate2entrantstateFis> dataSource)
    {
        this.dataSource = dataSource;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public List<Ecrequestfis> getEcrequestfisList() {
        return ecrequestfisList;
    }

    public void setEcrequestfisList(List<Ecrequestfis> lst) {
        this.ecrequestfisList = lst;
    }

    public List<EntrantState> getEntrantStateList() {
        return entrantStateList;
    }

    public void setEntrantStateList(List<EntrantState> lst) {
        this.entrantStateList = lst;
    }

    public List<Entrantstate2entrantstateFis> getStates() {
        return states;
    }

    public void setStates(List<Entrantstate2entrantstateFis> states) {
        this.states = states;
    }


}
