package ru.tandemservice.fisrmc.component.TestWeb;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO
        extends IUniDao<Model>
{
    public Long getLastAddedPackage(Model model);

}
