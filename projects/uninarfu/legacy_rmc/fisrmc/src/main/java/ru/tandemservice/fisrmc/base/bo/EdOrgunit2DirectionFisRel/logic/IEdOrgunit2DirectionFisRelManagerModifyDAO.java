package ru.tandemservice.fisrmc.base.bo.EdOrgunit2DirectionFisRel.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.fisrmc.entity.Educationdirectionfis;
import ru.tandemservice.fisrmc.entity.catalog.EducationdirectionSpravType;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

import java.util.List;


public interface IEdOrgunit2DirectionFisRelManagerModifyDAO extends INeedPersistenceSupport {

    void saveOrUpdateRelation(EducationOrgUnit educationOrgUnit, Educationdirectionfis educationdirectionfis);
    //void update(EducationOrgUnit educationOrgUnit, Educationdirectionfis educationdirectionfis);

    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
    void persistRelation(List<DataWrapper> dataSourceRecords, EducationdirectionSpravType spravType);

    void deleteRelation(Long educationOrgUnitID, Long SpravTypeId);

    public Educationdirectionfis getEducationdirectionfis(
            EducationOrgUnit eduOu
            , EducationdirectionSpravType spravType);

}
