package ru.tandemservice.fisrmc.dao;

import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.List;

public interface IAdmissionVolumeDAO {

    /**
     * Бюджетные места очной формы
     *
     * @param lstEnrolmentDirection
     *
     * @return
     */
    Long getNumberBudgetO(List<EnrollmentDirection> lstEnrolmentDirection);

    /**
     * Бюджетные места очно/заочной формы
     *
     * @param lstEnrolmentDirection
     *
     * @return
     */
    Long getNumberBudgetOZ(List<EnrollmentDirection> lstEnrolmentDirection);

    /**
     * Бюджетные места заочной формы
     *
     * @param lstEnrolmentDirection
     *
     * @return
     */
    Long getNumberBudgetZ(List<EnrollmentDirection> lstEnrolmentDirection);

    /**
     * Места с оплатой обучения очной формы
     *
     * @param lstEnrolmentDirection
     *
     * @return
     */
    Long getNumberPaidO(List<EnrollmentDirection> lstEnrolmentDirection);

    /**
     * Места с оплатой обучения очно заочной формы
     *
     * @param lstEnrolmentDirection
     *
     * @return
     */
    Long getNumberPaidOZ(List<EnrollmentDirection> lstEnrolmentDirection);

    /**
     * места с оплатой обучения заочной формы
     *
     * @param lstEnrolmentDirection
     *
     * @return
     */
    Long getNumberPaidZ(List<EnrollmentDirection> lstEnrolmentDirection);

    /**
     * Квота приема лиц, имеющих особое право (очная форма)
     *
     * @param lstEnrolmentDirection
     *
     * @return
     */
    Long getNumberQuotaO(List<EnrollmentDirection> lstEnrolmentDirection);

    /**
     * Квота приема лиц, имеющих особое право (очно-заочная форма)
     *
     * @param lstEnrolmentDirection
     *
     * @return
     */
    Long getNumberQuotaOZ(List<EnrollmentDirection> lstEnrolmentDirection);

    /**
     * Квота приема лиц, имеющих особое право (заочная форма)
     *
     * @param lstEnrolmentDirection
     *
     * @return
     */
    Long getNumberQuotaZ(List<EnrollmentDirection> lstEnrolmentDirection);


    /**
     * места целевого приема очной формы
     *
     * @param lstEnrolmentDirection
     *
     * @return
     */
    Long getNumberTargetO(List<EnrollmentDirection> lstEnrolmentDirection);

    /**
     * места целевого приема очно заочной формы
     *
     * @param lstEnrolmentDirection
     *
     * @return
     */
    Long getNumberTargetOZ(List<EnrollmentDirection> lstEnrolmentDirection);

    /**
     * места целевого приема заочной формы
     *
     * @param lstEnrolmentDirection
     *
     * @return
     */
    Long getNumberTargetZ(List<EnrollmentDirection> lstEnrolmentDirection);

    Long getNumberTargetO(TargetAdmissionKind targetadmissionkind,
                          List<EnrollmentDirection> edlist);

    Long getNumberTargetOZ(TargetAdmissionKind targetadmissionkind,
                           List<EnrollmentDirection> edlist);

    Long getNumberTargetZ(TargetAdmissionKind targetadmissionkind,
                          List<EnrollmentDirection> edlist);


}
