package ru.tandemservice.fisrmc.component.settings.EnrollmentCampaignCopy;

import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.fisrmc.entity.EnrollmentCampaignFisExt;
import ru.tandemservice.fisrmc.util.FisrmcUtil;

public class DAO extends ru.tandemservice.uniecrmc.component.settings.EnrollmentCampaignCopy.DAO {

    @Override
    public void update(ru.tandemservice.uniec.component.settings.EnrollmentCampaignCopy.Model model) {
        super.update(model);
        Model myModel = (Model) model;
        EnrollmentCampaignFisExt enrollmentCampaignFisExt = FisrmcUtil.getEnrollmentCampaignFisExt(model.getPeriod().getEnrollmentCampaign());
        if (FisrmcUtil.isValidRequestPrefix(myModel.getRequestPrefix())) {
            enrollmentCampaignFisExt.setRequestPrefix(myModel.getRequestPrefix());
            saveOrUpdate(enrollmentCampaignFisExt);
        }
        else
            throw new ApplicationException("Префикс номера заявления введен неверно. Значение должно быть больше 0 и меньше 100");
    }
}
