package ru.tandemservice.fisrmc.dao;

import java.util.List;


/**
 * dao для работы с наборами вступительных испытаний
 *
 * @author vch
 */
public interface IEntranceTestItemDAO {

    /**
     * получить наборы вступительных испытаний
     * для конкурсной группы
     *
     * @param competitiveGroup
     *
     * @return
     */
    List<XMLEntranceTestItem> getEntranceTestItem(XMLCompetitiveGroup competitiveGroup);

}
