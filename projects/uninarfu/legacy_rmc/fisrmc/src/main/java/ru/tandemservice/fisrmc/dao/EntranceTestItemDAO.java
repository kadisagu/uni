package ru.tandemservice.fisrmc.dao;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.fisrmc.entity.GroupEsForFis;
import ru.tandemservice.fisrmc.entity.catalog.Ecdisciplinefis;
import ru.tandemservice.fisrmc.entity.catalog.Entrancedisciplinetypefis;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.entity.catalog.EducationSubject;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineType;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationFormRelation;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EntranceTestItemDAO
        extends UniBaseDao
        implements IEntranceTestItemDAO
{
    private static IEntranceTestItemDAO _IEntranceTestItemDAO = null;

    public static IEntranceTestItemDAO Instanse()
    {
        if (_IEntranceTestItemDAO == null)
            _IEntranceTestItemDAO = (IEntranceTestItemDAO) ApplicationRuntime.getBean("entranceTestItemDAO");
        return _IEntranceTestItemDAO;
    }

    @Override
    public List<XMLEntranceTestItem> getEntranceTestItem(XMLCompetitiveGroup competitiveGroup) {

        List<EnrollmentDirection> directions = SpravDao.Instanse().getTandemEnrollmentDirections(competitiveGroup);

        List<XMLEntranceTestItem> entranceTestItems = new ArrayList<XMLEntranceTestItem>();

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EntranceDiscipline.class, "ed")
                .where(DQLExpressions.in(DQLExpressions.property(EntranceDiscipline.enrollmentDirection().fromAlias("ed")), directions))
                .setPredicate(DQLPredicateType.distinct);

        Map<String, XMLEntranceTestItem> map = new HashMap<String, XMLEntranceTestItem>();
        List<EntranceDiscipline> entranceDisciplines = builder.createStatement(getSession()).list();

        if (entranceDisciplines == null || entranceDisciplines.isEmpty()) {
            String msg = "";
            msg = " Для КГ:  " + competitiveGroup.getName() + " не указаны вступительные испытания ";
            PackUnpackToFis.AddPkgError(msg);
        }

        for (EntranceDiscipline discipline : entranceDisciplines) {

            // Дисциплина набора вступительных испытаний
            List<Discipline2RealizationWayRelation> disciplines = discipline.getDiscipline().getDisciplines();

            // это формы сдачи
            if (disciplines == null || disciplines.isEmpty()) {
                // если нет формы сдачи, то нельзя проимпортить заявы
                String msg = " Для КГ:  " + competitiveGroup.getName() + " у дисциплины  " + discipline.getTitle() + " не указаны формы сдачи";
                PackUnpackToFis.AddPkgError(msg);
            }

            for (Discipline2RealizationWayRelation wayRelation : disciplines) {
                // Форма сдачи дисциплины набора вступительных испытаний
                List<Discipline2RealizationFormRelation> formRelations = getList(Discipline2RealizationFormRelation.class, Discipline2RealizationFormRelation.discipline(), wayRelation);

                if (formRelations == null || formRelations.isEmpty()) {
                    // если нет формы сдачи, то нельзя проимпортить заявы
                    String msg = " Для КГ:  " + competitiveGroup.getName() + " у дисциплины  " + discipline.getTitle() + " не указаны формы сдачи";
                    PackUnpackToFis.AddPkgError(msg);
                }

                for (Discipline2RealizationFormRelation formRelation : formRelations) {
                    Double passMark = wayRelation.getPassMark();

                    //Общеобразовательные предметы - Предмет(Тандем)
                    EducationSubject educationSubject = wayRelation.getEducationSubject();

                    // если у нас объединение групп дисциплин?
                    GroupEsForFis groupDiscipline = SpravDao.Instanse().getSingleGroupEsForFis(educationSubject, directions);

                    // формируем уникальный id
                    // на основе типа вступительного испытания
                    // формы здачи
                    // самой дисциплины
                    SubjectPassForm passForm = null;
                    EntranceDisciplineType disciplineType = discipline.getType();
                    Ecdisciplinefis ecdisciplinefis = null;

                    String disciplineUidPart = "";

                    if (groupDiscipline == null) {
                        ecdisciplinefis = SpravDao.Instanse().getEcdisciplinefis(educationSubject);
                        passForm = formRelation.getSubjectPassForm();
                        disciplineUidPart = "s" + educationSubject.getCode();
                    }
                    else {
                        ecdisciplinefis = SpravDao.Instanse().getEcdisciplinefisForGroup(groupDiscipline);

                        // что делать с SubjectPassForm и EntranceDisciplineType?
                        // обсудить с Серегой!!!

                        // disciplineType всегда code=1 "Обычное"
                        disciplineType = (EntranceDisciplineType) SpravDao.Instanse().getEntityByCode(EntranceDisciplineType.class, "1");

                        // всегда тестирование code=2
                        passForm = (SubjectPassForm) SpravDao.Instanse().getEntityByCode(SubjectPassForm.class, "2");

                        disciplineUidPart = ecdisciplinefis.getCode();

                    }

                    //Тип вступительных испытаний
                    Entrancedisciplinetypefis entrancedisciplinetypefis = SpravDao.Instanse().getEntrancedisciplinetypefis(disciplineType);


                    // складываем по формам сдачи
                    String uid = "kg" + competitiveGroup.getUid()
                            // + "pf" +passForm.getCode()
                            + "dt" + disciplineType.getCode()
                            + disciplineUidPart;

                    XMLEntranceTestItem xmlEntranceTestItem = null;
                    if (!map.containsKey(uid)) {

                        // для competitiveGroup возможно есть льготы
                        // например право засчитывать вступительные испытания
                        ICommonBenefitDAO ida = CommonBenefitDAO.Instanse();

                        List<XMLCommonBenefit> lst = ida.getCommonOrEntranceTestBenefit(competitiveGroup, uid, false);
                        xmlEntranceTestItem = new XMLEntranceTestItem(uid, entrancedisciplinetypefis, passMark.intValue(), ecdisciplinefis, lst);

                        map.put(uid, xmlEntranceTestItem);
                        entranceTestItems.add(xmlEntranceTestItem);
                    }
                    else
                        // содержит
                        xmlEntranceTestItem = map.get(uid);

                    // добавим форму сдачи
                    xmlEntranceTestItem.addForm(passForm.getShortTitle());

                }
            }
        }

        return entranceTestItems;
    }

}
