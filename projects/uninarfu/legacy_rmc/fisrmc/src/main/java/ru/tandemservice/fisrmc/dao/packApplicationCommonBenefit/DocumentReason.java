package ru.tandemservice.fisrmc.dao.packApplicationCommonBenefit;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.tandemservice.fisrmc.dao.PackUnpackToFis;
import ru.tandemservice.fisrmc.dao.SpravDao;
import ru.tandemservice.fisrmc.dao.ToolsDao;
import ru.tandemservice.fisrmc.dao.packApplications.Applications;
import ru.tandemservice.fisrmc.entity.Olimpiatfis2ecdisciplinefis;
import ru.tandemservice.fisrmc.entity.catalog.Entrantdocumenttypefis;
import ru.tandemservice.fisrmc.entity.catalog.Olimpiaddiplomatypefis;
import ru.tandemservice.fisrmc.entity.catalog.Olimpiatfis;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.OlympiadDiploma;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class DocumentReason
        extends UniBaseDao
        implements IDocumentReason
{

    private static IDocumentReason _IDocumentReason = null;

    public static IDocumentReason Instanse()
    {
        if (_IDocumentReason == null)
            _IDocumentReason = (IDocumentReason) ApplicationRuntime.getBean("documentReason");
        return _IDocumentReason;
    }

    @Override
    public void makeCache()
    {
    }

    @Override
    public void clearCache()
    {
    }


    @Override
    public Element packOlympicDocument(Document doc
            , EntrantRequest entrantrequest
                                       // , RequestedEnrollmentDirection requestedEnrollmentDirection
                                       // , Entrantdocumenttypefis entrantdocumenttypefis
            , boolean isOrigin
            , OlympiadDiploma olympiadDiploma
            , Olimpiatfis olimpiatfis)
    {
        Element result = doc.createElement("OlympicDocument");

        Map<String, String> xmlMap = new LinkedHashMap<String, String>();

        // так нельзя, этот код не будет уникальным
        // причина проста, одна фис олимпиада может быть сопоставлена с несколькими
        // тандемовскими olimpiatfis.getId() брать нельзя
        //xmlMap.put("UID", String.valueOf(olimpiatfis.getId()));
        ToolsDao.PackUid(doc, result, Long.toString(olympiadDiploma.getId()));

        // такая ситуация не возможна в принципе
        //if (requestedEnrollmentDirection!=null)
        //xmlMap.put("OriginalReceived", String.valueOf(requestedEnrollmentDirection.isOriginalDocumentHandedIn()).toLowerCase());
        //boolean isOrigin = requestedEnrollmentDirection.isOriginalDocumentHandedIn();
        ToolsDao.PackToElement(doc, result, "OriginalReceived", isOrigin);

        if (isOrigin)
            // 	xmlMap.put("OriginalReceivedDate", ToolsDao.DateToString(entrantrequest.getRegDate()));
            xmlMap.put("OriginalReceivedDate", Applications.Instanse().getOriginDocumentDateString(entrantrequest));

        _packOlimpiadNumber(xmlMap, entrantrequest, olympiadDiploma);
        _packOlimpiadSeria(xmlMap, entrantrequest, olympiadDiploma);


        // c 2013 года обязательно заполнение поля серия
        // если серии нет, то указать 000
        // это выснил Устюгов Дима из Горново (основание - ответ специалиста горячей линии)


        //дата выдачи документа
        if (olympiadDiploma.getIssuanceDate() != null)
            xmlMap.put("DocumentDate", ToolsDao.DateToString(olympiadDiploma.getIssuanceDate()));

        // тип диплома
        // откуда фис может знать о тандемовских кодах
        // в документе на странице 16 дана ссылка - из этого вроде понятно, что у нас
        // для нас это olympiadDiplomaDegree
        // формально - верно, просто совпало
        // но по жизни, нужно найти соотв. сущность
        // а если тут был заложен умысел, но не грех и комент написал
        // типа, понимаю - нужен справочник 18, посмотрел, он совпадает по кодам
        // пишу код из olympiadDiplomaDegree
        Olimpiaddiplomatypefis diplomaTypeFis = SpravDao.Instanse().getOlimpiaddiplomatypefis(olympiadDiploma.getDegree());
        xmlMap.put("DiplomaTypeID", diplomaTypeFis.getCode());

        //олимпиада ФИС
        xmlMap.put("OlympicID", olimpiatfis.getCode());

        for (Entry<String, String> set : xmlMap.entrySet()) {
            Element createElement = doc.createElement(set.getKey());
            createElement.setTextContent(set.getValue());
            result.appendChild(createElement);
        }

        return result;
    }

    @Override
    public Element packOlympicTotalDocument(
            Document doc
            , EntrantRequest entrantrequest
            // , RequestedEnrollmentDirection requestedEnrollmentDirection,
            // Entrantdocumenttypefis entrantdocumenttypefis,
            , boolean isOrigin
            , OlympiadDiploma olympiadDiploma
            , Olimpiatfis olimpiatfis)
    {
        Element result = doc.createElement("OlympicTotalDocument");
        Map<String, String> xmlMap = new LinkedHashMap<String, String>();

        // не уникальный номер в рамках ис
        // xmlMap.put("UID", String.valueOf(olimpiatfis.getId()));
        ToolsDao.PackUid(doc, result, Long.toString(olympiadDiploma.getId()));

        // по аналогии с предыдущим кодом
        //boolean isOrigin = requestedEnrollmentDirection.isOriginalDocumentHandedIn();
        ToolsDao.PackToElement(doc, result, "OriginalReceived", isOrigin);
        if (isOrigin)
            xmlMap.put("OriginalReceivedDate", Applications.Instanse().getOriginDocumentDateString(entrantrequest));

		/*
		if (requestedEnrollmentDirection!=null)
			xmlMap.put("OriginalReceived", String.valueOf(requestedEnrollmentDirection.isOriginalDocumentHandedIn()).toLowerCase());
		if (requestedEnrollmentDirection.isOriginalDocumentHandedIn())
			xmlMap.put("OriginalReceivedDate", ToolsDao.DateToString(entrantrequest.getRegDate()));
		*/

        _packOlimpiadNumber(xmlMap, entrantrequest, olympiadDiploma);
        _packOlimpiadSeria(xmlMap, entrantrequest, olympiadDiploma);


        //адрес проведения
        if (olympiadDiploma.getSettlement() != null)
            xmlMap.put("OlympicPlace", olympiadDiploma.getSettlement().getFullTitle());

        //тип олимпиады
        // аналогия выше
        //xmlMap.put("DiplomaTypeID", olympiadDiploma.getDegree().getCode());
        Olimpiaddiplomatypefis diplomaTypeFis = SpravDao.Instanse().getOlimpiaddiplomatypefis(olympiadDiploma.getDegree());
        xmlMap.put("DiplomaTypeID", diplomaTypeFis.getCode());


        for (Entry<String, String> set : xmlMap.entrySet()) {
            Element createElement = doc.createElement(set.getKey());
            createElement.setTextContent(set.getValue());
            result.appendChild(createElement);
        }

        //берем профильный предмет олимпиады

        // есть сомнения, скорей всего зависит от вида конкурса
        // есть в тандеме - зачтение предмета олимпиады
        // но попробовать можно и так
        List<Olimpiatfis2ecdisciplinefis> o2dList = getList(Olimpiatfis2ecdisciplinefis.class, Olimpiatfis2ecdisciplinefis.olimpiatfis(), olimpiatfis);
        Element sub = doc.createElement("Subjects");
        for (Olimpiatfis2ecdisciplinefis o2d : o2dList) {
            Element subData = doc.createElement("SubjectBriefData");
            sub.appendChild(subData);
            ToolsDao.PackToElement(doc, subData, "SubjectID", o2d.getEcdisciplinefis().getCode());
        }
        result.appendChild(sub);
        return result;
    }

    private void _packOlimpiadNumber(
            Map<String, String> xmlMap
            , EntrantRequest entrantrequest, OlympiadDiploma olympiadDiploma
    )
    {

        //номер
        String docNumber = "";
        if (olympiadDiploma.getNumber() != null && olympiadDiploma.getNumber().trim() != "")
            docNumber = olympiadDiploma.getNumber();
        else {
            docNumber = Long.toString(olympiadDiploma.getId());

            String msg = "Для диплома олимпиады не указан номер. Заявление № " + entrantrequest.getStringNumber();
            if (PackUnpackToFis.DAEMON_PARAMS.isSuspendErrorDoc())
                PackUnpackToFis.AddPkgError(msg);
            else
                throw new ApplicationException(msg);

        }
        xmlMap.put("DocumentNumber", docNumber);


    }

    private void _packOlimpiadSeria(
            Map<String, String> xmlMap
            , EntrantRequest entrantrequest
            , OlympiadDiploma olympiadDiploma
    )
    {

        String docSeria = "";
        if (olympiadDiploma.getSeria() != null && olympiadDiploma.getSeria().trim() != "")
            docSeria = olympiadDiploma.getSeria();
        else {
            boolean isTotalOlimpiad = false;

            if (olympiadDiploma != null
                    && olympiadDiploma.getDiplomaType() != null
                    && olympiadDiploma.getDiplomaType().getCode().equals("1"))
                isTotalOlimpiad = true;

            if (!isTotalOlimpiad)
                docSeria = "000"; // для обычных олимпиад горячая линия хочет 000
            else
                docSeria = "12345"; // для всеросийский оставляем по старому

            String msg = "Для диплома олимпиады не указана серия. Заявление № " + entrantrequest.getStringNumber();

            if (PackUnpackToFis.DAEMON_PARAMS.isSuspendErrorDoc())
                PackUnpackToFis.AddPkgError(msg);
            else {
                if (isTotalOlimpiad)
                    throw new ApplicationException(msg);
                else
                    PackUnpackToFis.AddPkgError(msg);
            }

        }
        xmlMap.put("DocumentSeries", docSeria);


    }

    @Override
    public Element packCustomDocument(
            Document doc
            , EntrantRequest entrantrequest
            , RequestedEnrollmentDirection requestedEnrollmentDirection
            , Entrantdocumenttypefis entrantdocumenttypefis
            , List<PersonBenefit> listBenefit
    )
    {
        // обрабатывем только первую льготу по списку
        // причина проста, фис может приняь только одну льготу
        PersonBenefit benefit = listBenefit.get(0);

        Element result = doc.createElement("CustomDocument");
        Map<String, String> xmlMap = new LinkedHashMap<String, String>();

        // льгота на персоне, у одной персоны может быть несколько заявления
        // посему - id делаем составной
        // xmlMap.put("UID", String.valueOf(benefit.getId()));
        String uid = "rn:" + entrantrequest.getRegNumber() + ":" + Long.toString(benefit.getId());
        ToolsDao.PackUid(doc, result, uid);

        // фантастика - есть всегда
        //if (requestedEnrollmentDirection!=null)

        // по аналогии с кодом выше
        boolean isOrigin = requestedEnrollmentDirection.isOriginalDocumentHandedIn();
        ToolsDao.PackToElement(doc, result, "OriginalReceived", isOrigin);
        if (isOrigin)
            xmlMap.put("OriginalReceivedDate", Applications.Instanse().getOriginDocumentDateString(entrantrequest));

		/*
		xmlMap.put("OriginalReceived", String.valueOf(requestedEnrollmentDirection.isOriginalDocumentHandedIn()).toLowerCase());
		if (requestedEnrollmentDirection.isOriginalDocumentHandedIn())
			xmlMap.put("OriginalReceivedDate", ToolsDao.DateToString(entrantrequest.getRegDate()));
		*/


        if (benefit.getSeria() != null && benefit.getSeria() != "")
            xmlMap.put("DocumentSeries", benefit.getSeria());

        if (benefit.getNumber() != null && benefit.getNumber() != "")
            xmlMap.put("DocumentNumber", benefit.getNumber());

        if (benefit.getDate() != null)
            xmlMap.put("DocumentDate", ToolsDao.DateToString(benefit.getDate()));

        if (benefit.getIssuancePlace() != null)
            xmlMap.put("DocumentOrganization", benefit.getIssuancePlace());

        // это скорее поле title а не code
        //xmlMap.put("DocumentTypeNameText", entrantdocumenttypefis.getCode());
        xmlMap.put("DocumentTypeNameText", entrantdocumenttypefis.getTitle());


        // дополнительные сведения по документу
        String additionalInfo = benefit.getBenefit().getTitle();
        if (benefit.getBasic() != null && benefit.getBasic() != "")
            additionalInfo += " (" + benefit.getBasic() + ")";

        if (additionalInfo != null && additionalInfo != "")
            xmlMap.put("AdditionalInfo", additionalInfo);

        for (Entry<String, String> set : xmlMap.entrySet()) {
            Element createElement = doc.createElement(set.getKey());
            createElement.setTextContent(set.getValue());
            result.appendChild(createElement);
        }
        return result;
    }
}
