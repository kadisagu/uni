package ru.tandemservice.fisrmc.component.settings.EnrollmentCampaignStageFIS.AddEdit;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.fisrmc.entity.EnrollmentCampaignStageFIS;
import ru.tandemservice.fisrmc.entity.catalog.StageFis;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

public class DAO extends UniDao<Model> {

    @Override
    public void prepare(Model model) {
        if (model.getEntityId() != null) {
            EnrollmentCampaignStageFIS entity = getNotNull(EnrollmentCampaignStageFIS.class, model.getEntityId());
            model.setEnrollmentCampaign(entity.getEnrollmentCampaign());
            model.setStageFis(entity.getStage());
            model.setDateOrder(entity.getDateOrder());
        }
        if (model.getEnrollmentCampaignId() != null) {
            EnrollmentCampaign enrollmentcampaign = getNotNull(EnrollmentCampaign.class, model.getEnrollmentCampaignId());
            model.setEnrollmentCampaign(enrollmentcampaign);
        }

        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.setStageFisModel(new LazySimpleSelectModel<StageFis>(StageFis.class));
    }

    @Override
    public void update(Model model) {
        EnrollmentCampaignStageFIS entity = getEnrollmentCampaignStageFIS(model.getEnrollmentCampaign(), model.getStageFis());
        if (entity == null) {
            entity = new EnrollmentCampaignStageFIS();
            entity.setEnrollmentCampaign(model.getEnrollmentCampaign());
            entity.setStage(model.getStageFis());
            entity.setDateOrder(model.getDateOrder());
        }
        else {
            entity.setEnrollmentCampaign(model.getEnrollmentCampaign());
            entity.setStage(model.getStageFis());
            entity.setDateOrder(model.getDateOrder());
        }
        saveOrUpdate(entity);
    }

    public EnrollmentCampaignStageFIS getEnrollmentCampaignStageFIS(EnrollmentCampaign enrollmentCampaign, StageFis stageFis) {
        MQBuilder builder = new MQBuilder(EnrollmentCampaignStageFIS.ENTITY_CLASS, "e")
                .add(MQExpression.and(MQExpression.eq("e", EnrollmentCampaignStageFIS.enrollmentCampaign(), enrollmentCampaign),
                                      MQExpression.eq("e", EnrollmentCampaignStageFIS.stage(), stageFis)));
        return (EnrollmentCampaignStageFIS) builder.uniqueResult(getSession());
    }

}
