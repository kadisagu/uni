package ru.tandemservice.fisrmc.component.UploadFis2TU.AddEdit;

import org.apache.commons.io.IOUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.fisrmc.entity.FisExportFiles;
import ru.tandemservice.uni.dao.UniDao;

import java.io.InputStream;
import java.util.Date;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model)
    {
        if (model.getEntity().getId() != null)
            model.setEntity(getNotNull(FisExportFiles.class, model.getEntity().getId()));
    }

    @Override
    public void update(Model model) {
        if (model.getEntity().getXmlPackage() == null && model.getFile() == null)
            throw new ApplicationException("Поле файл обязательно для заполнения");

        try {
            if (model.getFile() != null) {
                InputStream input = model.getFile().getStream();
                byte[] content = IOUtils.toByteArray(input);

                DatabaseFile file = new DatabaseFile();
                file.setContent(content);
                file.setFilename(model.getFile().getFileName());

                saveOrUpdate(file);
                model.getEntity().setXmlPackage(file);

                if (model.getEntity().getDescription() == null || model.getEntity().getDescription() == "")
                    model.getEntity().setDescription(model.getFile().getFileName());

            }
        }
        catch (Exception ex) {
            throw new ApplicationException("Ошибка загрузки файла", ex);
        }

        if (model.getEntity().getDate() == null)
            model.getEntity().setDate(new Date());

        saveOrUpdate(model.getEntity());
    }
}
