package ru.tandemservice.fisrmc.component.dataChecking.FisCheckEntrantRequest;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.fisrmc.component.dataChecking.FisCheckEntrantRequest.List.Model;
import ru.tandemservice.fisrmc.entity.FisCheckEntrantRequest;

import java.util.ArrayList;
import java.util.List;

public class PrintBean implements IPrintFormCreator<Model> {

    @Override
    public RtfDocument createPrintForm(byte[] template, Model model)
    {
        RtfDocument document = new RtfReader().read(template);
        createTableModifier(model).modify(document);

        return document;
    }

    protected RtfTableModifier createTableModifier(Model model)
    {
        RtfTableModifier tableModifier = new RtfTableModifier();
        List<FisCheckEntrantRequest> entityList = model.getDataSource().getEntityList();
        List<String[]> list = new ArrayList<String[]>();
        int i = 0;
        for (FisCheckEntrantRequest entity : entityList) {
            String[] row = new String[]{
                    String.valueOf(++i),
                    RussianDateFormatUtils.STANDARD_DATE_TIME_FORMAT.format(entity.getCheckDate()),
                    String.valueOf(entity.getEntrantRequest().getRegNumber()),
                    entity.getEntrantRequest().getEntrant().getPerson().getFullFio(),
                    entity.getFisAppStatusCode().getTitle(),
                    Integer.toString(entity.getErrorCode()),
                    entity.getErrorMessage()
            };
            list.add(row);
        }
        tableModifier.put("T", list.toArray(new String[][]{}));
        return tableModifier;
    }

}
