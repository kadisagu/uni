package ru.tandemservice.fisrmc.dao;

import ru.tandemservice.fisrmc.entity.catalog.EducationdirectionSpravType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.List;

public interface ICompetitiveGroupsDAO {

    /**
     * список конкурсных групп
     *
     * @param lst
     *
     * @return
     */
    List<XMLCompetitiveGroup> getCompetitiveGroups
    (
            List<EnrollmentCampaign> lst
            , EducationdirectionSpravType spravType
            , boolean suspendCompetitiveGroup
    );

    /**
     * Перечень направлений подготовки конкурсной группы
     * <p/>
     * Если в XMLCompetitiveGroup не используются конкурсныне группы
     * , то на выход одно направление подготовки по ФИС, данное направление находим
     * из XMLCompetitiveGroup.getUID (это id направления подготовки для приема)
     * если useCompetitiveGroup = true, то getUID это id конкурсной группы, стало быть
     * по id конкурсной группы смотрим набор направлений подготовки для приема
     *
     * @param competitiveGroup
     *
     * @return
     */
    List<XMLCompetitiveGroupItem> getCompetitiveGroupItems
    (
            XMLCompetitiveGroup competitiveGroup
            , EducationdirectionSpravType spravType
    );

}
