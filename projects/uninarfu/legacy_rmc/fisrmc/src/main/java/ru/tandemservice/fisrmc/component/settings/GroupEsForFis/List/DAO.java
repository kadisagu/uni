package ru.tandemservice.fisrmc.component.settings.GroupEsForFis.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.fisrmc.entity.GroupEsForFis;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(final Model model) {

        EntrantFilterUtil.prepareEnrollmentCampaignFilterWithSecurity(model, getSession());
        model.setQualificationModel(new QualificationModel(getSession()));
        model.setFormativeOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING, null));
        model.setTerritorialOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL, null));

        model.setEnrollmentDirectionModel(new DQLFullCheckSelectModel() {

            @Override
            protected DQLSelectBuilder query(String alias, String filter) {

                IDataSettings settings = model.getSettings();
                List<EnrollmentCampaign> enrollmentCampaignList = settings.get("enrollmentCampaignList");
                List<Qualifications> qualificationList = settings.get("qualificationList");
                OrgUnit territorialOU = settings.get("territorialOrgUnit");
                OrgUnit formativeOU = settings.get("formativeOrgUnit");

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, alias);

                FilterUtils.applySimpleLikeFilter(builder, alias, EnrollmentDirection.title(), filter);

                if (enrollmentCampaignList != null && enrollmentCampaignList.size() > 0)
                    builder.where(DQLExpressions.in(
                            DQLExpressions.property(EnrollmentDirection.enrollmentCampaign().fromAlias(alias)),
                            enrollmentCampaignList
                    ));
                if (qualificationList != null && qualificationList.size() > 0)
                    builder.where(DQLExpressions.in(
                            DQLExpressions.property(EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().fromAlias(alias)),
                            qualificationList
                    ));
                if (territorialOU != null)
                    builder.where(DQLExpressions.eq(
                            DQLExpressions.property(EnrollmentDirection.educationOrgUnit().territorialOrgUnit().fromAlias(alias)),
                            DQLExpressions.value(territorialOU)
                    ));
                if (formativeOU != null)
                    builder.where(DQLExpressions.eq(
                            DQLExpressions.property(EnrollmentDirection.educationOrgUnit().formativeOrgUnit().fromAlias(alias)),
                            DQLExpressions.value(formativeOU)
                    ));
                return builder;
            }
        });
    }

    @Override
    public void prepareListDataSource(Model model) {
        IDataSettings settings = model.getSettings();
        EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();
        List<Qualifications> qualificationList = settings.get("qualificationList");
        List<EnrollmentDirection> enrollmentDirectionList = settings.get("enrollmentDirectionList");
        OrgUnit territorialOU = settings.get("territorialOrgUnit");
        OrgUnit formativeOU = settings.get("formativeOrgUnit");
        String groupName = settings.get("groupName");

        DynamicListDataSource<GroupEsForFis> dataSource = model.getDataSource();

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(GroupEsForFis.class, "ge")
                .column("ge");

        FilterUtils.applySimpleLikeFilter(builder, "ge", GroupEsForFis.groupName(), groupName);
        FilterUtils.applySelectFilter(builder, "ge", GroupEsForFis.enrollmentDirection().educationOrgUnit().formativeOrgUnit(), formativeOU);
        FilterUtils.applySelectFilter(builder, "ge", GroupEsForFis.enrollmentDirection().educationOrgUnit().territorialOrgUnit(), territorialOU);
        FilterUtils.applySelectFilter(builder, "ge", GroupEsForFis.enrollmentDirection().enrollmentCampaign(), enrollmentCampaign);
        FilterUtils.applySelectFilter(builder, "ge", GroupEsForFis.enrollmentDirection(), enrollmentDirectionList);
        FilterUtils.applySelectFilter(builder, "ge", GroupEsForFis.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().educationLevel().qualification(), qualificationList);
        // сортировка
        DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(GroupEsForFis.class, "ge");
        orderRegistry.applyOrder(builder, dataSource.getEntityOrder());

        UniBaseUtils.createPage(dataSource, builder, getSession());
    }
}
