package ru.tandemservice.fisrmc.dao;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.fisrmc.entity.Educationdirectionfis;
import ru.tandemservice.fisrmc.entity.catalog.Eceducationlevelfis;
import ru.tandemservice.fisrmc.entity.catalog.EducationdirectionSpravType;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.settings.TargetAdmissionPlanRelation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * DAO для работы с инфой по целевому приему
 *
 * @author vch
 */
public class TargetOrganizationDAO extends UniBaseDao
        implements ITargetOrganizationDAO
{
    private static ITargetOrganizationDAO _ITargetOrganizationDAO = null;

    public static ITargetOrganizationDAO Instanse()
    {
        if (_ITargetOrganizationDAO == null)
            _ITargetOrganizationDAO = (ITargetOrganizationDAO) ApplicationRuntime.getBean("targetOrganizationDAO");
        return _ITargetOrganizationDAO;
    }


    private Map<String, List<XMLTargetOrganization>> mapTO = new HashMap<String, List<XMLTargetOrganization>>();

    public void clearCache()
    {
        mapTO = new HashMap<String, List<XMLTargetOrganization>>();
    }


    @Override
    public List<XMLTargetOrganization> getTargetOrganizations(XMLCompetitiveGroup competitiveGroup, EducationdirectionSpravType spravType)
    {

        String mapKey = competitiveGroup.getUid();
        if (mapTO.containsKey(mapKey))
            return mapTO.get(mapKey);

        // направления подготовки конкурсной группы
        List<EnrollmentDirection> directions = SpravDao.Instanse().getTandemEnrollmentDirections(competitiveGroup);
        if (directions.isEmpty()) {
            mapTO.put(mapKey, null);
            return null;
        }

        List<XMLTargetOrganization> retVal = new ArrayList<XMLTargetOrganization>();

        // получим список организаций целевого приема в рамках направлений подготовки
        // конкурсной группы
        // результат нужен уникальный

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(TargetAdmissionPlanRelation.class, "rel")
                .addColumn(DQLExpressions.property(TargetAdmissionPlanRelation.targetAdmissionKind().fromAlias("rel")))
                .where(DQLExpressions.in(DQLExpressions.property(TargetAdmissionPlanRelation.entranceEducationOrgUnit().fromAlias("rel")), directions))
                .where(DQLExpressions.gt(DQLExpressions.property(TargetAdmissionPlanRelation.planValue().fromAlias("rel")), DQLExpressions.value(0)))
                .setPredicate(DQLPredicateType.distinct);

        List<TargetAdmissionKind> lstTest = dql.createStatement(getSession()).list();
        System.out.print(lstTest.size());

        DQLSelectBuilder dqlTg = new DQLSelectBuilder()
                .fromEntity(TargetAdmissionKind.class, "tg");
        dqlTg.where(DQLExpressions.in(DQLExpressions.property(TargetAdmissionKind.id().fromAlias("td")), dql.getQuery()));

        List<TargetAdmissionKind> lst = dql.createStatement(getSession()).list();

        for (TargetAdmissionKind tg : lst) {
            XMLTargetOrganization xtg = new XMLTargetOrganization(competitiveGroup, tg);
            retVal.add(xtg);

            // все направления подготовки для организации целевого приема
            List<EnrollmentDirection> lstEd = getEnrollmentDirection(competitiveGroup, tg);

            Map<XMLEnrollmentDirectionKey, List<EnrollmentDirection>> map = getMapEnrollmentDirection(lstEd, spravType);

            List<XMLTargetOrganizationItem> lstItems = new ArrayList<XMLTargetOrganizationItem>();
            for (XMLEnrollmentDirectionKey keyMap : map.keySet()) {
                XMLTargetOrganizationItem item
                        = new XMLTargetOrganizationItem
                        (
                                competitiveGroup
                                , tg
                                , keyMap.getEceducationlevelfis()
                                , keyMap.getEducationdirectionfis()
                                , keyMap.getEnrollmentCampaign()
                                , map.get(keyMap)
                        );

                lstItems.add(item);
            }
            xtg.setItems(lstItems);
        }

        mapTO.put(mapKey, retVal);
        return retVal;
    }

    public Map<XMLEnrollmentDirectionKey, List<EnrollmentDirection>> getMapEnrollmentDirection(
            List<EnrollmentDirection> lst
            , EducationdirectionSpravType spravType
    )
    {
        Map<XMLEnrollmentDirectionKey, List<EnrollmentDirection>> map = new HashMap<XMLEnrollmentDirectionKey, List<EnrollmentDirection>>();
        for (EnrollmentDirection ed : lst) {
            EnrollmentCampaign enrollmentCampaign = ed.getEnrollmentCampaign();
            Course course = SpravDao.Instanse().getCourse(enrollmentCampaign);
            Eceducationlevelfis eceducationlevelfis = SpravDao.Instanse().getEceducationlevelfis(ed.getEducationOrgUnit());
            Educationdirectionfis educationdirectionfis = SpravDao.Instanse().getEducationdirectionfis(ed.getEducationOrgUnit(), spravType);

            if (eceducationlevelfis == null)
                continue;


            XMLEnrollmentDirectionKey keyMap = new XMLEnrollmentDirectionKey(
                    enrollmentCampaign
                    , course
                    , eceducationlevelfis
                    , educationdirectionfis
            );

            List<EnrollmentDirection> lstxed = null;

            if (map.containsKey(keyMap))
                lstxed = map.get(keyMap);
            else {
                lstxed = new ArrayList<EnrollmentDirection>();
                map.put(keyMap, lstxed);
            }
            lstxed.add(ed);
        }
        return map;
    }

    private List<EnrollmentDirection> getEnrollmentDirection
            (
                    XMLCompetitiveGroup competitiveGroup
                    , TargetAdmissionKind tg
            )
    {
        List<EnrollmentDirection> directions = SpravDao.Instanse().getTandemEnrollmentDirections(competitiveGroup);

        // получим список организаций целевого приема в рамках приемной кампании
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(TargetAdmissionPlanRelation.class, "rel")
                .addColumn(DQLExpressions.property(TargetAdmissionPlanRelation.entranceEducationOrgUnit().fromAlias("rel")))

                .where(DQLExpressions.in(DQLExpressions.property(TargetAdmissionPlanRelation.entranceEducationOrgUnit().id().fromAlias("rel")), directions))
                .where(DQLExpressions.gt(DQLExpressions.property(TargetAdmissionPlanRelation.planValue().fromAlias("rel")), DQLExpressions.value(0)));

        DQLSelectBuilder dqlEd = new DQLSelectBuilder()
                .fromEntity(EnrollmentDirection.class, "ed");
        dqlEd.where(DQLExpressions.in(DQLExpressions.property(EnrollmentDirection.id().fromAlias("ed")), dql.getQuery()));

        List<EnrollmentDirection> lst = dql.createStatement(getSession()).list();
        return lst;
    }


//	@Override
//	public List<XMLTargetOrganizationItem> getTargetOrganizationsList(
//			XMLCompetitiveGroup competitiveGroup) {
//
//		// для конкурсной шруппы ищем направления подготовки для приема
//		List<EnrollmentDirection> directions = SpravDao.Instanse().getTandemEnrollmentDirections(competitiveGroup);
//		if(directions.isEmpty())
//			return null;
//		
//		List<XMLTargetOrganizationItem> organizationItems = new ArrayList<XMLTargetOrganizationItem>();
//
//		//Сократим поиск, исключив НП у которых нет данных по направлению на целевой прием
//		DQLSelectBuilder trimBuilder = new DQLSelectBuilder()
//		.fromEntity(TargetAdmissionPlanRelation.class, "rel")
//		.addColumn("e")
//		.joinEntity("rel", DQLJoinType.inner, EnrollmentDirection.class, "e", DQLExpressions.eq(DQLExpressions.property(TargetAdmissionPlanRelation.entranceEducationOrgUnit().fromAlias("rel")), DQLExpressions.property(EnrollmentDirection.id().fromAlias("e"))))
//		.where(DQLExpressions.in(DQLExpressions.property(EnrollmentDirection.id().fromAlias("e")), DQLExpressions.value(directions)))
//		.where(DQLExpressions.gt(DQLExpressions.property(TargetAdmissionPlanRelation.planValue().fromAlias("rel")), DQLExpressions.value(0)));
//
//		
//		directions = trimBuilder.createStatement(getSession()).list();
//		
//		// направления подготовки для приема (для которых есть целевой прием)
//		for(EnrollmentDirection enrollmentDirection : directions)
//		{
//			// уровень образования
//			Eceducationlevelfis eceducationlevelfis = SpravDao.Instanse().getEceducationlevelfis(enrollmentDirection.getEducationOrgUnit());
//			
//			// направление подготовки по ФИС
//			Educationdirectionfis educationdirectionfis = SpravDao.Instanse().getEducationdirectionfis(enrollmentDirection.getEducationOrgUnit());
//			
//			if(educationdirectionfis == null) {
//				//EducationLevels educationLevel = enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();
//				continue;
//				//UserContext.getInstance().getErrorCollector().addError("Не найден элемент справочника Educationdirectionfis для EducationLevels code="+educationLevel.getCode() + ", " + educationLevel.getFullTitle());
//				//throw new ApplicationException("Не найден элемент справочника Educationdirectionfis для EducationLevels code="+educationLevel.getCode() + ", " + educationLevel.getFullTitle());
//			}
//
//			List<EnrollmentDirection> tandemEnrollmentDirections = SpravDao.Instanse().getTandemEnrollmentDirections(educationdirectionfis.getEducationLevels(), enrollmentDirection.getEnrollmentCampaign());
//
//			/**
//			 * Сумма целевого приема по органихациям
//			 */
//			int budgetSum = 0;
//			/** 
//			 * Сумма всего по целевому приему (поле 'Целевой прием')
//			 */
//			int bugetTargetSum = 0;
//
//			int contractSum = 0;
//			int contractTargetSum = 0;
//
//			/**
//			 * для каждой  enrollmentdirection_t суммируем  targetadmissionplanbudget_p и targetadmissionplancontract_p, 
//			 * получаем число абитуриентов, зачисляемое по ЦП.
//			 */
//			budgetSum = getSumTargetPlan(enrollmentDirection, true);
//			bugetTargetSum = getSumTotalTargetPlan(enrollmentDirection, true);
//
//			contractSum = getSumTargetPlan(enrollmentDirection, false);
//			contractTargetSum = getSumTotalTargetPlan(enrollmentDirection, false);
//
//			List<TargetAdmissionPlanRelation> plans = getList(TargetAdmissionPlanRelation.class,TargetAdmissionPlanRelation.entranceEducationOrgUnit(),enrollmentDirection);
//
//			//Если суммы >0
//			if(budgetSum>0 || contractSum>0){
//
//				for(TargetAdmissionPlanRelation planRelation : plans) {
//										
//					XMLTargetOrganizationItem xmlTargetOrganizationItem = 
//							new XMLTargetOrganizationItem(planRelation.getTargetAdmissionKind(),eceducationlevelfis, educationdirectionfis, enrollmentDirection.getEnrollmentCampaign(), tandemEnrollmentDirections);
//					xmlTargetOrganizationItem.setBudgetPlan(planRelation.isBudget() ? planRelation.getPlanValue() : 0);
//					xmlTargetOrganizationItem.setContractPlan(!planRelation.isBudget() ? planRelation.getPlanValue() : 0);
//					organizationItems.add(xmlTargetOrganizationItem);					
//				}
//
//
//			}
//
//			/**
//			 *  суммируем все получившиеся числа по budget_p и planvalue_p, смотрим - если числа
//			 *  не совпадают с тем, что указано у целевиков в enrollmentdirection_t,
//			 *  создаем еще одну XMLTargetOrganizationItem для targetadmissionkind_t с code=1, туда дописываем недостающее (знак может быть и минус).
//			 */
//			if(budgetSum != bugetTargetSum || contractSum != contractTargetSum){
//
//				TargetAdmissionKind kind = getCatalogItem(TargetAdmissionKind.class,"1");
//
//				XMLTargetOrganizationItem xmlTargetOrganizationItem = 
//						new XMLTargetOrganizationItem(kind,eceducationlevelfis, educationdirectionfis, enrollmentDirection.getEnrollmentCampaign(), tandemEnrollmentDirections);
//
//				xmlTargetOrganizationItem.setBudgetPlan(bugetTargetSum - budgetSum);
//				xmlTargetOrganizationItem.setContractPlan(contractTargetSum - contractSum);
//				organizationItems.add(xmlTargetOrganizationItem);
//
//			}
//
//		}
//
//		return organizationItems;
//	}


//	@Override
//	public int getSumTargetPlan(EnrollmentDirection direction, boolean isBudget) {
//
//		DQLSelectBuilder sumBuilder = new DQLSelectBuilder()
//		.fromEntity(EnrollmentDirection.class, "e")
//		.joinEntity("e", DQLJoinType.inner, TargetAdmissionPlanRelation.class, "rel", DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.id().fromAlias("e")), DQLExpressions.property(TargetAdmissionPlanRelation.entranceEducationOrgUnit().id().fromAlias("rel"))))
//		//План приема
//		.addColumn(DQLFunctions.sum(DQLExpressions.property(TargetAdmissionPlanRelation.planValue().fromAlias("rel"))))			
//		.where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.id().fromAlias("e")), DQLExpressions.value(direction)))
//		//Сумма  кроме значения 'Целевой прием'
//		.where(DQLExpressions.eq(DQLExpressions.property(TargetAdmissionPlanRelation.budget().fromAlias("rel")), DQLExpressions.value(isBudget)))
//		.where(DQLExpressions.ne(DQLExpressions.property(TargetAdmissionPlanRelation.targetAdmissionKind().code().fromAlias("rel")), DQLExpressions.value(1)));
//		List<Object> list = sumBuilder.createStatement(getSession()).list();
//
//		return list.get(0) != null ? ((Long)list.get(0)).intValue() : 0;
//	}


//	@Override
//	public int getSumTotalTargetPlan(EnrollmentDirection direction, boolean isBudget) {
//
//		DQLSelectBuilder sumBuilder = new DQLSelectBuilder()
//		.fromEntity(EnrollmentDirection.class, "e")
//		.joinEntity("e", DQLJoinType.inner, TargetAdmissionPlanRelation.class, "rel", DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.id().fromAlias("e")), DQLExpressions.property(TargetAdmissionPlanRelation.entranceEducationOrgUnit().id().fromAlias("rel"))))
//		//План приема
//		.addColumn(DQLFunctions.sum(DQLExpressions.property(TargetAdmissionPlanRelation.planValue().fromAlias("rel"))))			
//		.where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.id().fromAlias("e")), DQLExpressions.value(direction)))
//		//Сумма  со значением 'Целевой прием'
//		.where(DQLExpressions.eq(DQLExpressions.property(TargetAdmissionPlanRelation.budget().fromAlias("rel")), DQLExpressions.value(isBudget)))
//		.where(DQLExpressions.eq(DQLExpressions.property(TargetAdmissionPlanRelation.targetAdmissionKind().code().fromAlias("rel")), DQLExpressions.value(1)));
//
//		List<Object> list = sumBuilder.createStatement(getSession()).list();
//
//		return list.get(0) != null ? ((Long)list.get(0)).intValue() : 0;
//	}

}
