package ru.tandemservice.fisrmc.component.settings.Eceducationlevelfis2TU;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import ru.tandemservice.fisrmc.entity.catalog.Eceducationlevelfis;

public class Model {

    private DynamicListDataSource<Eceducationlevelfis> dataSource;
    private IMultiSelectModel qualificationValueList;
    private IMultiSelectModel qualificationOPValueList;


    public DynamicListDataSource<Eceducationlevelfis> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<Eceducationlevelfis> dataSource) {
        this.dataSource = dataSource;
    }

    public IMultiSelectModel getQualificationValueList() {
        return qualificationValueList;
    }

    public void setQualificationValueList(IMultiSelectModel qualificationValueList) {
        this.qualificationValueList = qualificationValueList;
    }

    public IMultiSelectModel getQualificationOPValueList() {
        return qualificationOPValueList;
    }

    public void setQualificationOPValueList(IMultiSelectModel qualificationOPValueList) {
        this.qualificationOPValueList = qualificationOPValueList;
    }


}
