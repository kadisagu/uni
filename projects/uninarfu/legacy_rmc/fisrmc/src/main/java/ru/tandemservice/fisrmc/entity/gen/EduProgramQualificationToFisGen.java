package ru.tandemservice.fisrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.fisrmc.entity.EduProgramQualificationToFis;
import ru.tandemservice.fisrmc.entity.catalog.Eceducationlevelfis;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Квалификации НП к ФИС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduProgramQualificationToFisGen extends EntityBase
 implements INaturalIdentifiable<EduProgramQualificationToFisGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.fisrmc.entity.EduProgramQualificationToFis";
    public static final String ENTITY_NAME = "eduProgramQualificationToFis";
    public static final int VERSION_HASH = -1808564634;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String L_ECEDUCATIONLEVELFIS = "eceducationlevelfis";
    public static final String L_QUALIFICATIONS = "qualifications";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private Eceducationlevelfis _eceducationlevelfis;     // Уровень образования
    private EduProgramQualification _qualifications;     // Квалификация профессионального образования
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Уровень образования. Свойство не может быть null.
     */
    @NotNull
    public Eceducationlevelfis getEceducationlevelfis()
    {
        return _eceducationlevelfis;
    }

    /**
     * @param eceducationlevelfis Уровень образования. Свойство не может быть null.
     */
    public void setEceducationlevelfis(Eceducationlevelfis eceducationlevelfis)
    {
        dirty(_eceducationlevelfis, eceducationlevelfis);
        _eceducationlevelfis = eceducationlevelfis;
    }

    /**
     * @return Квалификация профессионального образования. Свойство не может быть null.
     */
    @NotNull
    public EduProgramQualification getQualifications()
    {
        return _qualifications;
    }

    /**
     * @param qualifications Квалификация профессионального образования. Свойство не может быть null.
     */
    public void setQualifications(EduProgramQualification qualifications)
    {
        dirty(_qualifications, qualifications);
        _qualifications = qualifications;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EduProgramQualificationToFisGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EduProgramQualificationToFis)another).getCode());
            }
            setEceducationlevelfis(((EduProgramQualificationToFis)another).getEceducationlevelfis());
            setQualifications(((EduProgramQualificationToFis)another).getQualifications());
            setTitle(((EduProgramQualificationToFis)another).getTitle());
        }
    }

    public INaturalId<EduProgramQualificationToFisGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EduProgramQualificationToFisGen>
    {
        private static final String PROXY_NAME = "EduProgramQualificationToFisNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EduProgramQualificationToFisGen.NaturalId) ) return false;

            EduProgramQualificationToFisGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduProgramQualificationToFisGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduProgramQualificationToFis.class;
        }

        public T newInstance()
        {
            return (T) new EduProgramQualificationToFis();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "eceducationlevelfis":
                    return obj.getEceducationlevelfis();
                case "qualifications":
                    return obj.getQualifications();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "eceducationlevelfis":
                    obj.setEceducationlevelfis((Eceducationlevelfis) value);
                    return;
                case "qualifications":
                    obj.setQualifications((EduProgramQualification) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "eceducationlevelfis":
                        return true;
                case "qualifications":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "eceducationlevelfis":
                    return true;
                case "qualifications":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "eceducationlevelfis":
                    return Eceducationlevelfis.class;
                case "qualifications":
                    return EduProgramQualification.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EduProgramQualificationToFis> _dslPath = new Path<EduProgramQualificationToFis>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduProgramQualificationToFis");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.fisrmc.entity.EduProgramQualificationToFis#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Уровень образования. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.EduProgramQualificationToFis#getEceducationlevelfis()
     */
    public static Eceducationlevelfis.Path<Eceducationlevelfis> eceducationlevelfis()
    {
        return _dslPath.eceducationlevelfis();
    }

    /**
     * @return Квалификация профессионального образования. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.EduProgramQualificationToFis#getQualifications()
     */
    public static EduProgramQualification.Path<EduProgramQualification> qualifications()
    {
        return _dslPath.qualifications();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.fisrmc.entity.EduProgramQualificationToFis#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EduProgramQualificationToFis> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private Eceducationlevelfis.Path<Eceducationlevelfis> _eceducationlevelfis;
        private EduProgramQualification.Path<EduProgramQualification> _qualifications;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.fisrmc.entity.EduProgramQualificationToFis#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EduProgramQualificationToFisGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Уровень образования. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.EduProgramQualificationToFis#getEceducationlevelfis()
     */
        public Eceducationlevelfis.Path<Eceducationlevelfis> eceducationlevelfis()
        {
            if(_eceducationlevelfis == null )
                _eceducationlevelfis = new Eceducationlevelfis.Path<Eceducationlevelfis>(L_ECEDUCATIONLEVELFIS, this);
            return _eceducationlevelfis;
        }

    /**
     * @return Квалификация профессионального образования. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.EduProgramQualificationToFis#getQualifications()
     */
        public EduProgramQualification.Path<EduProgramQualification> qualifications()
        {
            if(_qualifications == null )
                _qualifications = new EduProgramQualification.Path<EduProgramQualification>(L_QUALIFICATIONS, this);
            return _qualifications;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.fisrmc.entity.EduProgramQualificationToFis#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EduProgramQualificationToFisGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EduProgramQualificationToFis.class;
        }

        public String getEntityName()
        {
            return "eduProgramQualificationToFis";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
