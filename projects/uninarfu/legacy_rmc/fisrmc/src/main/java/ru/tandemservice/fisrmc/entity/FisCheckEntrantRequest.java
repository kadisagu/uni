package ru.tandemservice.fisrmc.entity;

import ru.tandemservice.fisrmc.entity.gen.FisCheckEntrantRequestGen;

/**
 * Проверка заявлений по ответу ФИС
 * <p/>
 * Если заявление загружено без ошибок, то обязательно наличие кода fisAppStatusCode=1 'Проверено, ошибок нет'
 */
public class FisCheckEntrantRequest extends FisCheckEntrantRequestGen
{
}