package ru.tandemservice.fisrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.fisrmc.entity.FisPackages;
import ru.tandemservice.fisrmc.entity.FisPackages2enrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность-связь между fisPackages и приемными кампаниями
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FisPackages2enrollmentCampaignGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.fisrmc.entity.FisPackages2enrollmentCampaign";
    public static final String ENTITY_NAME = "fisPackages2enrollmentCampaign";
    public static final int VERSION_HASH = 1596004015;
    private static IEntityMeta ENTITY_META;

    public static final String L_FIS_PACKAGES = "fisPackages";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";

    private FisPackages _fisPackages;     // Сущность для хранения сформированных пакетов
    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сущность для хранения сформированных пакетов.
     */
    public FisPackages getFisPackages()
    {
        return _fisPackages;
    }

    /**
     * @param fisPackages Сущность для хранения сформированных пакетов.
     */
    public void setFisPackages(FisPackages fisPackages)
    {
        dirty(_fisPackages, fisPackages);
        _fisPackages = fisPackages;
    }

    /**
     * @return Приемная кампания.
     */
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FisPackages2enrollmentCampaignGen)
        {
            setFisPackages(((FisPackages2enrollmentCampaign)another).getFisPackages());
            setEnrollmentCampaign(((FisPackages2enrollmentCampaign)another).getEnrollmentCampaign());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FisPackages2enrollmentCampaignGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FisPackages2enrollmentCampaign.class;
        }

        public T newInstance()
        {
            return (T) new FisPackages2enrollmentCampaign();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "fisPackages":
                    return obj.getFisPackages();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "fisPackages":
                    obj.setFisPackages((FisPackages) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "fisPackages":
                        return true;
                case "enrollmentCampaign":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "fisPackages":
                    return true;
                case "enrollmentCampaign":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "fisPackages":
                    return FisPackages.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FisPackages2enrollmentCampaign> _dslPath = new Path<FisPackages2enrollmentCampaign>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FisPackages2enrollmentCampaign");
    }
            

    /**
     * @return Сущность для хранения сформированных пакетов.
     * @see ru.tandemservice.fisrmc.entity.FisPackages2enrollmentCampaign#getFisPackages()
     */
    public static FisPackages.Path<FisPackages> fisPackages()
    {
        return _dslPath.fisPackages();
    }

    /**
     * @return Приемная кампания.
     * @see ru.tandemservice.fisrmc.entity.FisPackages2enrollmentCampaign#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    public static class Path<E extends FisPackages2enrollmentCampaign> extends EntityPath<E>
    {
        private FisPackages.Path<FisPackages> _fisPackages;
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сущность для хранения сформированных пакетов.
     * @see ru.tandemservice.fisrmc.entity.FisPackages2enrollmentCampaign#getFisPackages()
     */
        public FisPackages.Path<FisPackages> fisPackages()
        {
            if(_fisPackages == null )
                _fisPackages = new FisPackages.Path<FisPackages>(L_FIS_PACKAGES, this);
            return _fisPackages;
        }

    /**
     * @return Приемная кампания.
     * @see ru.tandemservice.fisrmc.entity.FisPackages2enrollmentCampaign#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

        public Class getEntityClass()
        {
            return FisPackages2enrollmentCampaign.class;
        }

        public String getEntityName()
        {
            return "fisPackages2enrollmentCampaign";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
