package ru.tandemservice.fisrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.fisrmc.entity.GroupEsForFis;
import ru.tandemservice.fisrmc.entity.GroupEsForFis2Es;
import ru.tandemservice.uniec.entity.catalog.EducationSubject;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Дисциплины группы для ФИС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class GroupEsForFis2EsGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.fisrmc.entity.GroupEsForFis2Es";
    public static final String ENTITY_NAME = "groupEsForFis2Es";
    public static final int VERSION_HASH = -1108947781;
    private static IEntityMeta ENTITY_META;

    public static final String L_GROUP_ES_FOR_FIS = "groupEsForFis";
    public static final String L_EDUCATION_SUBJECT = "educationSubject";

    private GroupEsForFis _groupEsForFis;     // Группы дисциплин для передачи в ФИС
    private EducationSubject _educationSubject;     // Предмет

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Группы дисциплин для передачи в ФИС. Свойство не может быть null.
     */
    @NotNull
    public GroupEsForFis getGroupEsForFis()
    {
        return _groupEsForFis;
    }

    /**
     * @param groupEsForFis Группы дисциплин для передачи в ФИС. Свойство не может быть null.
     */
    public void setGroupEsForFis(GroupEsForFis groupEsForFis)
    {
        dirty(_groupEsForFis, groupEsForFis);
        _groupEsForFis = groupEsForFis;
    }

    /**
     * @return Предмет. Свойство не может быть null.
     */
    @NotNull
    public EducationSubject getEducationSubject()
    {
        return _educationSubject;
    }

    /**
     * @param educationSubject Предмет. Свойство не может быть null.
     */
    public void setEducationSubject(EducationSubject educationSubject)
    {
        dirty(_educationSubject, educationSubject);
        _educationSubject = educationSubject;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof GroupEsForFis2EsGen)
        {
            setGroupEsForFis(((GroupEsForFis2Es)another).getGroupEsForFis());
            setEducationSubject(((GroupEsForFis2Es)another).getEducationSubject());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends GroupEsForFis2EsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) GroupEsForFis2Es.class;
        }

        public T newInstance()
        {
            return (T) new GroupEsForFis2Es();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "groupEsForFis":
                    return obj.getGroupEsForFis();
                case "educationSubject":
                    return obj.getEducationSubject();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "groupEsForFis":
                    obj.setGroupEsForFis((GroupEsForFis) value);
                    return;
                case "educationSubject":
                    obj.setEducationSubject((EducationSubject) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "groupEsForFis":
                        return true;
                case "educationSubject":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "groupEsForFis":
                    return true;
                case "educationSubject":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "groupEsForFis":
                    return GroupEsForFis.class;
                case "educationSubject":
                    return EducationSubject.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<GroupEsForFis2Es> _dslPath = new Path<GroupEsForFis2Es>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "GroupEsForFis2Es");
    }
            

    /**
     * @return Группы дисциплин для передачи в ФИС. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.GroupEsForFis2Es#getGroupEsForFis()
     */
    public static GroupEsForFis.Path<GroupEsForFis> groupEsForFis()
    {
        return _dslPath.groupEsForFis();
    }

    /**
     * @return Предмет. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.GroupEsForFis2Es#getEducationSubject()
     */
    public static EducationSubject.Path<EducationSubject> educationSubject()
    {
        return _dslPath.educationSubject();
    }

    public static class Path<E extends GroupEsForFis2Es> extends EntityPath<E>
    {
        private GroupEsForFis.Path<GroupEsForFis> _groupEsForFis;
        private EducationSubject.Path<EducationSubject> _educationSubject;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Группы дисциплин для передачи в ФИС. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.GroupEsForFis2Es#getGroupEsForFis()
     */
        public GroupEsForFis.Path<GroupEsForFis> groupEsForFis()
        {
            if(_groupEsForFis == null )
                _groupEsForFis = new GroupEsForFis.Path<GroupEsForFis>(L_GROUP_ES_FOR_FIS, this);
            return _groupEsForFis;
        }

    /**
     * @return Предмет. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.GroupEsForFis2Es#getEducationSubject()
     */
        public EducationSubject.Path<EducationSubject> educationSubject()
        {
            if(_educationSubject == null )
                _educationSubject = new EducationSubject.Path<EducationSubject>(L_EDUCATION_SUBJECT, this);
            return _educationSubject;
        }

        public Class getEntityClass()
        {
            return GroupEsForFis2Es.class;
        }

        public String getEntityName()
        {
            return "groupEsForFis2Es";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
