package ru.tandemservice.fisrmc.component.settings.DisciplimeMinMark2Olimpiad;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.fisrmc.entity.catalog.Ecdisciplinefis;
import ru.tandemservice.unibase.UniBaseUtils;

public class Controller
        extends AbstractBusinessController<IDAO, Model>
{

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setSettings(component.getSettings());

        getDao().prepare(model);
        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<Ecdisciplinefis> dataSource = UniBaseUtils.createDataSource(component, getDao());
        createColumns(dataSource);
        model.setDataSource(dataSource);
    }

    private void createColumns
            (
                    DynamicListDataSource<Ecdisciplinefis> dataSource
            )
    {
        dataSource.addColumn(new SimpleColumn("Дисциплина", Ecdisciplinefis.title()).setOrderable(false));
        dataSource.addColumn(new BlockColumn("minMark", "Минимальный юалл для зачтения олимпиады"));
    }

    public void onClickApply(IBusinessComponent component)
    {
        IDAO dao = getDao();
        Model model = getModel(component);
        dao.update(model);

    }

    public void onRefresh(IBusinessComponent component)
    {
        component.saveSettings();
    }


}
