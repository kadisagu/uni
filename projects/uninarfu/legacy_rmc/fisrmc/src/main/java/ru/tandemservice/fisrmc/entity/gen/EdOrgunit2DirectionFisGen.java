package ru.tandemservice.fisrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.fisrmc.entity.EdOrgunit2DirectionFis;
import ru.tandemservice.fisrmc.entity.Educationdirectionfis;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность-связь НП и НП ФИС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EdOrgunit2DirectionFisGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.fisrmc.entity.EdOrgunit2DirectionFis";
    public static final String ENTITY_NAME = "edOrgunit2DirectionFis";
    public static final int VERSION_HASH = -1875501775;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDUCATION_DIRECTION_FIS = "educationDirectionFis";
    public static final String L_EDUCATION_ORG_UNIT = "educationOrgUnit";

    private Educationdirectionfis _educationDirectionFis;     // Направления подготовки
    private EducationOrgUnit _educationOrgUnit;     // Параметры обучения студентов по направлению подготовки (НПП)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Направления подготовки. Свойство не может быть null.
     */
    @NotNull
    public Educationdirectionfis getEducationDirectionFis()
    {
        return _educationDirectionFis;
    }

    /**
     * @param educationDirectionFis Направления подготовки. Свойство не может быть null.
     */
    public void setEducationDirectionFis(Educationdirectionfis educationDirectionFis)
    {
        dirty(_educationDirectionFis, educationDirectionFis);
        _educationDirectionFis = educationDirectionFis;
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnit()
    {
        return _educationOrgUnit;
    }

    /**
     * @param educationOrgUnit Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     */
    public void setEducationOrgUnit(EducationOrgUnit educationOrgUnit)
    {
        dirty(_educationOrgUnit, educationOrgUnit);
        _educationOrgUnit = educationOrgUnit;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EdOrgunit2DirectionFisGen)
        {
            setEducationDirectionFis(((EdOrgunit2DirectionFis)another).getEducationDirectionFis());
            setEducationOrgUnit(((EdOrgunit2DirectionFis)another).getEducationOrgUnit());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EdOrgunit2DirectionFisGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EdOrgunit2DirectionFis.class;
        }

        public T newInstance()
        {
            return (T) new EdOrgunit2DirectionFis();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "educationDirectionFis":
                    return obj.getEducationDirectionFis();
                case "educationOrgUnit":
                    return obj.getEducationOrgUnit();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "educationDirectionFis":
                    obj.setEducationDirectionFis((Educationdirectionfis) value);
                    return;
                case "educationOrgUnit":
                    obj.setEducationOrgUnit((EducationOrgUnit) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "educationDirectionFis":
                        return true;
                case "educationOrgUnit":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "educationDirectionFis":
                    return true;
                case "educationOrgUnit":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "educationDirectionFis":
                    return Educationdirectionfis.class;
                case "educationOrgUnit":
                    return EducationOrgUnit.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EdOrgunit2DirectionFis> _dslPath = new Path<EdOrgunit2DirectionFis>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EdOrgunit2DirectionFis");
    }
            

    /**
     * @return Направления подготовки. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.EdOrgunit2DirectionFis#getEducationDirectionFis()
     */
    public static Educationdirectionfis.Path<Educationdirectionfis> educationDirectionFis()
    {
        return _dslPath.educationDirectionFis();
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.EdOrgunit2DirectionFis#getEducationOrgUnit()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
    {
        return _dslPath.educationOrgUnit();
    }

    public static class Path<E extends EdOrgunit2DirectionFis> extends EntityPath<E>
    {
        private Educationdirectionfis.Path<Educationdirectionfis> _educationDirectionFis;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnit;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Направления подготовки. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.EdOrgunit2DirectionFis#getEducationDirectionFis()
     */
        public Educationdirectionfis.Path<Educationdirectionfis> educationDirectionFis()
        {
            if(_educationDirectionFis == null )
                _educationDirectionFis = new Educationdirectionfis.Path<Educationdirectionfis>(L_EDUCATION_DIRECTION_FIS, this);
            return _educationDirectionFis;
        }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.EdOrgunit2DirectionFis#getEducationOrgUnit()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
        {
            if(_educationOrgUnit == null )
                _educationOrgUnit = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT, this);
            return _educationOrgUnit;
        }

        public Class getEntityClass()
        {
            return EdOrgunit2DirectionFis.class;
        }

        public String getEntityName()
        {
            return "edOrgunit2DirectionFis";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
