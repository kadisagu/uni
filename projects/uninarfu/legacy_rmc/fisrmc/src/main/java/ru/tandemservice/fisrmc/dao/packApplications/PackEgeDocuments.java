package ru.tandemservice.fisrmc.dao.packApplications;


import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.tandemservice.fisrmc.dao.PackUnpackToFis;
import ru.tandemservice.fisrmc.dao.SpravDao;
import ru.tandemservice.fisrmc.dao.ToolsDao;
import ru.tandemservice.fisrmc.entity.catalog.Ecdisciplinefis;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate;
import ru.tandemservice.uniec.entity.entrant.StateExamSubjectMark;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

public class PackEgeDocuments extends UniBaseDao implements IPackEgeDocuments {

    private static IPackEgeDocuments _IPackEgeDocuments = null;

    public static IPackEgeDocuments Instanse()
    {
        if (_IPackEgeDocuments == null)
            _IPackEgeDocuments = (IPackEgeDocuments) ApplicationRuntime.getBean("packEgeDocuments");
        return _IPackEgeDocuments;
    }


    private Map<Entrant, List<EntrantStateExamCertificate>> mapEgeCertificate = new HashMap<Entrant, List<EntrantStateExamCertificate>>();
    private Map<EntrantStateExamCertificate, List<StateExamSubjectMark>> mapEgeCertificateMark = new HashMap<EntrantStateExamCertificate, List<StateExamSubjectMark>>();

    // для кеширования id сертификата в зависимотси от оценки
    private Map<String, String> mapMarkValue = new HashMap<String, String>();

    /**
     * создать кеш сертификатов и оценок по сертификатам
     */
    public void makeCache()
    {
        _makeMapEgeCertificate();
        _makeMapEgeCertificateMark();
    }

    private void _makeMapEgeCertificateMark()
    {
        mapEgeCertificateMark = new HashMap<EntrantStateExamCertificate, List<StateExamSubjectMark>>();

        DQLSelectBuilder dqlEntrantRequest = Applications.Instanse().getEntrantRequestDQLList(
                PackUnpackToFis.DAEMON_PARAMS.getEnrollmentCampaigns()
                , PackUnpackToFis.DAEMON_PARAMS.isNotRf()
                , PackUnpackToFis.DAEMON_PARAMS.isBenefitRequest()
                , PackUnpackToFis.DAEMON_PARAMS.isOrdersOfAdmission()
                , PackUnpackToFis.DAEMON_PARAMS.getRegNumbers()
                , PackUnpackToFis.DAEMON_PARAMS.getExcludeRegNumbers()

                , PackUnpackToFis.DAEMON_PARAMS.getDateRequestFrom()
                , PackUnpackToFis.DAEMON_PARAMS.getDateRequestTo()
                , PackUnpackToFis.DAEMON_PARAMS.isWhithoutSendedInFis()
                , PackUnpackToFis.DAEMON_PARAMS.isWhithoutEntrantInOrder()

                , false
                , PackUnpackToFis.DAEMON_PARAMS.isTestMode()
                , PackUnpackToFis.TESTMODE_CompetitiveGroup_List
        );

        DQLSelectBuilder dqlEntrant = new DQLSelectBuilder();
        dqlEntrant.fromEntity(EntrantRequest.class, "eRequest");
        // абитуриент
        dqlEntrant.column(DQLExpressions.property(EntrantRequest.entrant().fromAlias("eRequest")));
        dqlEntrant.where(in(property(EntrantRequest.id().fromAlias("eRequest")), dqlEntrantRequest.getQuery()));


        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(StateExamSubjectMark.class, "mark");
        dql.column("mark");
        dql.where(in(property(StateExamSubjectMark.certificate().entrant().id().fromAlias("mark")), dqlEntrant.getQuery()));
        List<StateExamSubjectMark> marks = dql.createStatement(getSession()).list();

        for (StateExamSubjectMark mark : marks) {
            EntrantStateExamCertificate certificate = mark.getCertificate();

            List<StateExamSubjectMark> lst = null;
            if (mapEgeCertificateMark.containsKey(certificate))
                lst = mapEgeCertificateMark.get(certificate);
            else {
                lst = new ArrayList<StateExamSubjectMark>();
                mapEgeCertificateMark.put(certificate, lst);
            }
            lst.add(mark);
        }
    }

    private void _makeMapEgeCertificate()
    {
        mapEgeCertificate = new HashMap<Entrant, List<EntrantStateExamCertificate>>();

        DQLSelectBuilder dqlEntrantRequest = Applications.Instanse().getEntrantRequestDQLList
                (
                        PackUnpackToFis.DAEMON_PARAMS.getEnrollmentCampaigns()
                        , PackUnpackToFis.DAEMON_PARAMS.isNotRf()
                        , PackUnpackToFis.DAEMON_PARAMS.isBenefitRequest()
                        , PackUnpackToFis.DAEMON_PARAMS.isOrdersOfAdmission()
                        , PackUnpackToFis.DAEMON_PARAMS.getRegNumbers()
                        , PackUnpackToFis.DAEMON_PARAMS.getExcludeRegNumbers()

                        , PackUnpackToFis.DAEMON_PARAMS.getDateRequestFrom()
                        , PackUnpackToFis.DAEMON_PARAMS.getDateRequestTo()
                        , PackUnpackToFis.DAEMON_PARAMS.isWhithoutSendedInFis()
                        , PackUnpackToFis.DAEMON_PARAMS.isWhithoutEntrantInOrder()

                        , false
                        , PackUnpackToFis.DAEMON_PARAMS.isTestMode()
                        , PackUnpackToFis.TESTMODE_CompetitiveGroup_List
                );

        DQLSelectBuilder dqlEntrant = new DQLSelectBuilder();
        dqlEntrant.fromEntity(EntrantRequest.class, "eRequest");
        // абитуриент
        dqlEntrant.column(DQLExpressions.property(EntrantRequest.entrant().fromAlias("eRequest")));
        dqlEntrant.where(in(property(EntrantRequest.id().fromAlias("eRequest")), dqlEntrantRequest.getQuery()));


        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EntrantStateExamCertificate.class, "ege");
        dql.column("ege");
        dql.where(in(property(EntrantStateExamCertificate.entrant().id().fromAlias("ege")), dqlEntrant.getQuery()));
        List<EntrantStateExamCertificate> egeDocuments = dql.createStatement(getSession()).list();


        for (EntrantStateExamCertificate certificate : egeDocuments) {
            // кладем в кеш
            Entrant entrant = certificate.getEntrant();

            List<EntrantStateExamCertificate> lst = null;
            if (mapEgeCertificate.containsKey(entrant))
                lst = mapEgeCertificate.get(entrant);
            else {
                lst = new ArrayList<EntrantStateExamCertificate>();
                mapEgeCertificate.put(entrant, lst);
            }
            lst.add(certificate);
        }
    }

    /**
     * удалить кеши
     */
    public void clearCache()
    {
        mapEgeCertificate = new HashMap<Entrant, List<EntrantStateExamCertificate>>();
        mapEgeCertificateMark = new HashMap<EntrantStateExamCertificate, List<StateExamSubjectMark>>();
        mapMarkValue = new HashMap<String, String>();

    }


    @Override
    public Element packEgeDocuments(Document doc, Entrant entrant) {

        List<EntrantStateExamCertificate> egeDocuments = null;
        if (mapEgeCertificate.containsKey(entrant))
            egeDocuments = mapEgeCertificate.get(entrant);

        if (egeDocuments == null || egeDocuments.size() == 0)
            return null;


        Element rooElement = doc.createElement("EgeDocuments");
        for (EntrantStateExamCertificate egeDocument : egeDocuments) {

            Element egeDocumentElem = doc.createElement("EgeDocument");
            rooElement.appendChild(egeDocumentElem);

            ToolsDao.PackUid(doc, egeDocumentElem, Long.toString(egeDocument.getId()));
            ToolsDao.PackOriginalReceived(doc, egeDocumentElem, egeDocument.isOriginal());

            if (egeDocument.isOriginal()) {
                // только в этом случае есть смысл формировать данный атрибут
                // по дате регистрации абитуриента
                String date = ToolsDao.DateToString(entrant.getRegistrationDate());
                ToolsDao.PackToElement(doc, egeDocumentElem, "OriginalReceivedDate", date);
            }

            String _docNumber = egeDocument.getNumber();

            // зададим правильный формат
            if (_docNumber != null && _docNumber.length() == 13)
                // можно форматировать номер
                _docNumber = String.valueOf(_docNumber).replaceFirst("(\\d{2})(\\d{9})(\\d+)", "$1-$2-$3");

            ToolsDao.PackToElement(doc, egeDocumentElem, "DocumentNumber", _docNumber);

            if (egeDocument.getIssuanceDate() == null) {
                // попробуем из номера документа получить год
                String egeYear = _getEgeYearByDocNumber(_docNumber);
                if (egeYear != null) {
                    // восстановили год по номеру
                    ToolsDao.PackToElement(doc, egeDocumentElem, "DocumentYear", egeYear);
                }
                else {
                    if (PackUnpackToFis.DAEMON_PARAMS.isSuspendErrorDoc()) {
                        ToolsDao.PackYearToElement(doc, egeDocumentElem, "DocumentYear", new Date());
                        PackUnpackToFis.AddPkgError("Для персоны " + entrant.getPerson().getFullFio() + " не указана дата выдачи сертификата ЕГЭ");
                    }
                    else {
                        throw new ApplicationException("Для персоны " + entrant.getPerson().getFullFio() + " не указана дата выдачи сертификата ЕГЭ");
                    }
                }
            }
            else {
                // некоторые даты могут быть указаны некорректно
                // если год указан, проверим год с годом номера заявления
                Calendar cal = Calendar.getInstance();
                cal.setTime(egeDocument.getIssuanceDate());
                int year = cal.get(Calendar.YEAR);

                String yearString = Integer.toString(year);
                String yearStringByDocNumber = _getEgeYearByDocNumber(_docNumber);

                if (yearStringByDocNumber != null && !yearStringByDocNumber.equals(yearString)) {
                    // предупреждение
                    PackUnpackToFis.AddPkgError("Для персоны " + entrant.getPerson().getFullFio() + " с ошибкой указан год получения сертификата ЕГЭ (год не соотв. номеру)");
                }
                // Дата указана, пакуем как дату
                ToolsDao.PackToElement(doc, egeDocumentElem, "DocumentDate", egeDocument.getIssuanceDate(), false, false);

                // ToolsDao.PackYearToElement(doc, egeDocumentElem, "DocumentYear", egeDocument.getIssuanceDate());
            }

            Element subjectsElem = doc.createElement("Subjects");
            egeDocumentElem.appendChild(subjectsElem);

            List<StateExamSubjectMark> subjectMarks = mapEgeCertificateMark.get(egeDocument);

            for (StateExamSubjectMark subjectMark : subjectMarks) {

                Element subjectDataElem = doc.createElement("SubjectData");
                subjectsElem.appendChild(subjectDataElem);

                Ecdisciplinefis ecdisciplinefis = SpravDao.Instanse().getEcdisciplinefis(subjectMark.getSubject());
                ToolsDao.PackToElement(doc, subjectDataElem, "SubjectID", ecdisciplinefis.getCode());
                ToolsDao.PackToElement(doc, subjectDataElem, "Value", String.valueOf(subjectMark.getMark()));

                // мап для формирования источника оценки
                putEgeMarkToMap(egeDocument, entrant, ecdisciplinefis.getCode(), String.valueOf(subjectMark.getMark()));

            }
        }

        return rooElement;
    }

    @Override
    public String getEgeSertificateId(Entrant entrant, String subjectID, String value)
    {
        String key = Long.toString(entrant.getId()) + subjectID + value;
        if (mapMarkValue.containsKey(key))
            return mapMarkValue.get(key);
        else
            return null;
    }

    private void putEgeMarkToMap(EntrantStateExamCertificate egeDocument, Entrant entrant, String subjectID, String value)
    {
        String key = Long.toString(entrant.getId()) + subjectID + value;
        if (!mapMarkValue.containsKey(key))
            mapMarkValue.put(key, Long.toString(egeDocument.getId()));
    }

    private String _getEgeYearByDocNumber(String docNumber)
    {
        if (docNumber == null || docNumber.length() < 5)
            return null;

        String year = "20" + docNumber.substring(docNumber.length() - 2);

        if (year.equals("2010")
                || year.equals("2011")
                || year.equals("2012")
                || year.equals("2013")
                || year.equals("2014")
                || year.equals("2015")
                || year.equals("2016")
                )
            return year;
        else
            return null;
    }

}
