package ru.tandemservice.fisrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_fisrmc_1x0x0_5to6 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.10"),
                        new ScriptDependency("org.tandemframework.shared", "1.1.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.1.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность olympiadDiploma2DiplomaFis

        // создана новая сущность
        {
            // создать таблицу
            String tableName = "olympiaddiploma2diplomafis_t";
            DBTable dbt = new DBTable(tableName,
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("olympiaddiploma_id", DBType.LONG).setNullable(false),
                                      new DBColumn("olimpiatfis_id", DBType.LONG).setNullable(false)
            );

            if (!tool.tableExists(tableName))
                tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("olympiadDiploma2DiplomaFis");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность olimpiatfis

        // удалено свойство OlympicLevelID
        {
            // TODO: программист должен подтвердить это действие
            /*		if( true )
				throw new UnsupportedOperationException("Confirm me: delete column");
			 */
            // удалить колонку
            if (tool.columnExists("olimpiatfis_t", "olympiclevelid_p"))
                tool.dropColumn("olimpiatfis_t", "olympiclevelid_p");

        }

        // создано обязательное свойство ecolimpiadlevelfis
        {
            // создать колонку
            if (!tool.columnExists("olimpiatfis_t", "ecolimpiadlevelfis_id"))
                tool.createColumn("olimpiatfis_t", new DBColumn("ecolimpiadlevelfis_id", DBType.LONG));


            // TODO: программист должен подтвердить это действие
			/*			if( true )
				throw new UnsupportedOperationException("Confirm me: set default value for required property");
			 */

            Statement stmt = tool.getConnection().createStatement();
            stmt.execute("select id from ecolimpiadlevelfis_t where code_p='1'");
            ResultSet rs = stmt.getResultSet();


            // задать значение по умолчанию
            Long defaultEcolimpiadlevelfis = 0L;        // TODO: задайте NOT NULL значение!
            //Пока так, всем проставим елемент справочника ecolimpiadlevelfis_t исправиться после импорта xml файла
            if (rs.next()) {
                defaultEcolimpiadlevelfis = rs.getLong(1);
            }

            tool.executeUpdate("update olimpiatfis_t set ecolimpiadlevelfis_id=? where ecolimpiadlevelfis_id is null", defaultEcolimpiadlevelfis);

            // сделать колонку NOT NULL
            tool.setColumnNullable("olimpiatfis_t", "ecolimpiadlevelfis_id", false);

        }


    }
}