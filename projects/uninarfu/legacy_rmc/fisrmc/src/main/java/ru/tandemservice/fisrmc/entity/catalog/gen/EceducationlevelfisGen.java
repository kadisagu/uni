package ru.tandemservice.fisrmc.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.fisrmc.entity.catalog.Eceducationlevelfis;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Уровень образования
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EceducationlevelfisGen extends EntityBase
 implements INaturalIdentifiable<EceducationlevelfisGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.fisrmc.entity.catalog.Eceducationlevelfis";
    public static final String ENTITY_NAME = "eceducationlevelfis";
    public static final int VERSION_HASH = 1227684861;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_CODE_T_U = "codeTU";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _codeTU; 
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getCodeTU()
    {
        return _codeTU;
    }

    /**
     * @param codeTU 
     */
    public void setCodeTU(String codeTU)
    {
        dirty(_codeTU, codeTU);
        _codeTU = codeTU;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EceducationlevelfisGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((Eceducationlevelfis)another).getCode());
            }
            setCodeTU(((Eceducationlevelfis)another).getCodeTU());
            setTitle(((Eceducationlevelfis)another).getTitle());
        }
    }

    public INaturalId<EceducationlevelfisGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EceducationlevelfisGen>
    {
        private static final String PROXY_NAME = "EceducationlevelfisNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EceducationlevelfisGen.NaturalId) ) return false;

            EceducationlevelfisGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EceducationlevelfisGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Eceducationlevelfis.class;
        }

        public T newInstance()
        {
            return (T) new Eceducationlevelfis();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "codeTU":
                    return obj.getCodeTU();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "codeTU":
                    obj.setCodeTU((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "codeTU":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "codeTU":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "codeTU":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Eceducationlevelfis> _dslPath = new Path<Eceducationlevelfis>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Eceducationlevelfis");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.fisrmc.entity.catalog.Eceducationlevelfis#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return 
     * @see ru.tandemservice.fisrmc.entity.catalog.Eceducationlevelfis#getCodeTU()
     */
    public static PropertyPath<String> codeTU()
    {
        return _dslPath.codeTU();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.fisrmc.entity.catalog.Eceducationlevelfis#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends Eceducationlevelfis> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _codeTU;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.fisrmc.entity.catalog.Eceducationlevelfis#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EceducationlevelfisGen.P_CODE, this);
            return _code;
        }

    /**
     * @return 
     * @see ru.tandemservice.fisrmc.entity.catalog.Eceducationlevelfis#getCodeTU()
     */
        public PropertyPath<String> codeTU()
        {
            if(_codeTU == null )
                _codeTU = new PropertyPath<String>(EceducationlevelfisGen.P_CODE_T_U, this);
            return _codeTU;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.fisrmc.entity.catalog.Eceducationlevelfis#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EceducationlevelfisGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return Eceducationlevelfis.class;
        }

        public String getEntityName()
        {
            return "eceducationlevelfis";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
