package ru.tandemservice.fisrmc.dao;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.fisrmc.entity.catalog.Competitionkindfis;
import ru.tandemservice.fisrmc.entity.catalog.Olimpiaddiplomatypefis;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.entrant.Discipline2OlympiadDiplomaRelation;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.ArrayList;
import java.util.List;

public class CommonBenefitDAO
        extends UniBaseDao
        implements ICommonBenefitDAO
{
    private static ICommonBenefitDAO _ICommonBenefitDAO = null;

    public static ICommonBenefitDAO Instanse()
    {
        if (_ICommonBenefitDAO == null)
            _ICommonBenefitDAO = (ICommonBenefitDAO) ApplicationRuntime.getBean("commonBenefitDAO");
        return _ICommonBenefitDAO;
    }

    @Override
    public List<XMLCommonBenefit> getCommonOrEntranceTestBenefit(XMLCompetitiveGroup competitiveGroup, String parenId, boolean isCommon)
    {

        List<XMLCommonBenefit> benefits = new ArrayList<XMLCommonBenefit>();
        List<EnrollmentDirection> directions = SpravDao.Instanse().getTandemEnrollmentDirections(competitiveGroup);

        if (isCommon) {
            List<String> notInQual = new ArrayList<>();
            notInQual.add("51");
            notInQual.add("52");

            DQLSelectBuilder selectBuilder = new DQLSelectBuilder()
                    .fromEntity(RequestedEnrollmentDirection.class, "rd")
                    .where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().fromAlias("rd")), directions))
                    .where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.competitionKind().code().fromAlias("rd")), DQLExpressions.value(UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES)))

                            // для спо олимпиады не нужнв в льготах (Афонин 2014/06/19)
                    .where(DQLExpressions.notIn(DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().code().fromAlias("rd")), notInQual))


                    .setPredicate(DQLPredicateType.distinct);
            List<RequestedEnrollmentDirection> requestedEnrollmentDirections = selectBuilder.createStatement(getSession()).list();


            if (requestedEnrollmentDirections != null && requestedEnrollmentDirections.size() > 0) {
                //в качестве competitionkindfis позицию одноименного справочника с кодом = 1
                XMLCommonBenefit xmlCommonBenefit_1 = _getCommonBenefit(competitiveGroup,
                                                                        getCatalogItem(Competitionkindfis.class, "1"), parenId);
                benefits.add(xmlCommonBenefit_1);

            }
        }
        else {
            // зачтение предметов
            DQLSelectBuilder dqlOlimpiad = new DQLSelectBuilder();
            dqlOlimpiad.fromEntity(Discipline2OlympiadDiplomaRelation.class, "d2o")
                    .where(DQLExpressions.in(DQLExpressions.property(Discipline2OlympiadDiplomaRelation.enrollmentDirection().fromAlias("d2o")), directions));
            List<Discipline2OlympiadDiplomaRelation> lstD2O = dqlOlimpiad.createStatement(getSession()).list();

            if (!lstD2O.isEmpty() && lstD2O.size() > 0) {
                // Добавляем - без вступительный испытаний, иначе стенд не принимает зачтение дисциплины
                XMLCommonBenefit xmlCommonBenefit_3 = _getCommonBenefit(competitiveGroup, getCatalogItem(Competitionkindfis.class, "3"), parenId);
                benefits.add(xmlCommonBenefit_3);
            }
        }

        return benefits;
    }

    private XMLCommonBenefit _getCommonBenefit(
            XMLCompetitiveGroup competitiveGroup
            , Competitionkindfis catalogItem
            , String parentId
    )
    {
        XMLCommonBenefit xmlCommonBenefit = new XMLCommonBenefit(competitiveGroup, catalogItem, parentId);
        //в качестве isForAllOlympics = true
        xmlCommonBenefit.setForAllOlympics(true);
        //в качестве List<Olimpiaddiplomatypefis> пишим позиции из одноименного справочника с кодами 1 и 2
        List<Olimpiaddiplomatypefis> olimpicDiplomTypes =
                getList(new DQLSelectBuilder().fromEntity(Olimpiaddiplomatypefis.class, "ol").where(DQLExpressions.in(DQLExpressions.property(Olimpiaddiplomatypefis.code().fromAlias("ol")), new String[]{"1", "2"})));
        xmlCommonBenefit.setOlimpicDiplomTypes(olimpicDiplomTypes);

        return xmlCommonBenefit;
    }
}
