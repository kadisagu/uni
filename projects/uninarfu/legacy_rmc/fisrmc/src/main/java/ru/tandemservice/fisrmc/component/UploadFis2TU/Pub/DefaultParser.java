package ru.tandemservice.fisrmc.component.UploadFis2TU.Pub;

import org.apache.commons.lang.StringUtils;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.StringReader;
import java.util.*;

public abstract class DefaultParser extends DefaultHandler {

    // fill in child
    protected String elementPath;
    // fill in child
    protected Map<String, String> propertyMap = new HashMap<String, String>();

    private Stack<String> stack = new Stack<String>();
    private String xml;
    private boolean inElement = false;
    private boolean inCharacters = false;
    private String currProperty = null;

    private List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
    private Map<String, Object> info = new HashMap<String, Object>();
    protected List<String> listProperty = new ArrayList<String>();

    public static List<Map<String, Object>> parse(DefaultParser parser) throws Exception {
        parser.runParse();
        return parser.getResultList();
    }

    public DefaultParser(String xml) {
        this.xml = xml;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        stack.push(qName);

        String path = getPath();
        if (path.equals(this.elementPath)) {
            info = new HashMap<String, Object>();
            inElement = true;

        }
        else if (this.propertyMap.containsKey(path)) {
            this.currProperty = this.propertyMap.get(path);
        }
        this.inCharacters = false;
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (getPath().equals(this.elementPath)) {
            this.resultList.add(info);
            inElement = false;
        }

        stack.pop();
        this.currProperty = null;
        this.inCharacters = false;
    }

    @Override
    public void characters(char[] ac, int i, int j) throws SAXException {
        if (this.currProperty == null)
            return;

        boolean isList = this.listProperty.contains(currProperty);
        String value = new String(ac, i, j);

        String prefix = "";
        if (this.inCharacters) {
            if (isList) {
                List<String> list = (List<String>) info.get(this.currProperty);
                prefix = list.get(list.size() - 1);
            }
            else {
                prefix = (String) info.get(this.currProperty);
            }
        }

        if (isList) {
            List<String> list = (List<String>) info.get(this.currProperty);
            if (list == null) {
                list = new ArrayList<String>();
                info.put(this.currProperty, list);
            }

            if (prefix.isEmpty())
                list.add(value);
            else
                list.set(list.size() - 1, prefix + value);
        }
        else
            info.put(this.currProperty, prefix + value);

        this.inCharacters = true;
    }

    protected void runParse() throws Exception {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();
        InputSource is = new InputSource(new StringReader(xml));
        saxParser.parse(is, this);
    }

    protected List<Map<String, Object>> getResultList() {
        return this.resultList;
    }

    protected String getPath() {
        return StringUtils.join(this.stack, "-");
    }
}
