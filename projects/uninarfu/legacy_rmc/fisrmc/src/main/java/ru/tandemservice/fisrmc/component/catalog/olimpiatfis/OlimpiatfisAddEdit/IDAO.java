package ru.tandemservice.fisrmc.component.catalog.olimpiatfis.OlimpiatfisAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.fisrmc.entity.catalog.Olimpiatfis;

public interface IDAO extends IDefaultCatalogAddEditDAO<Olimpiatfis, Model> {

}