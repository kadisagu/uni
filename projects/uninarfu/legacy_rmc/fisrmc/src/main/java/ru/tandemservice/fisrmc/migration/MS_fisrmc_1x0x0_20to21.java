package ru.tandemservice.fisrmc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_fisrmc_1x0x0_20to21 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.5.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.5.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность educationdirectionfis

        //  свойство Codefis стало необязательным
        //  свойство NewCode стало необязательным
//    	{
//			// сделать колонку NULL
//			tool.setColumnNullable("educationdirectionfis_t", "codefis_p", true);
//			tool.setColumnNullable("educationdirectionfis_t", "newcode_p", true);
//		}

    }
}