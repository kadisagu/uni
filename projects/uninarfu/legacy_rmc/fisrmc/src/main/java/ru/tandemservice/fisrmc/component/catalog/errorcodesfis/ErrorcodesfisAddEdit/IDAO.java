package ru.tandemservice.fisrmc.component.catalog.errorcodesfis.ErrorcodesfisAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.fisrmc.entity.catalog.Errorcodesfis;

public interface IDAO extends IDefaultCatalogAddEditDAO<Errorcodesfis, Model> {

}
