package ru.tandemservice.fisrmc.dao.packEduDocuments;

import org.tandemframework.shared.person.base.entity.PersonEduInstitution;

import java.util.Date;

/**
 * диплом об основном общем образовании
 *
 * @author belarin
 */
public class XMLSchoolCertificateBasicDocument
        extends XMLBaseEducationDocument
{


    public XMLSchoolCertificateBasicDocument
            (
                    String uid
                    , boolean originalReceived
                    , Date originalReceivedDate
                    , String documentSeries
                    , String documentNumber
                    , Date documentDate
                    , String documentOrganization
                    , PersonEduInstitution personEduInstitution
            )
    {
        super(uid, originalReceived, originalReceivedDate, documentSeries,
              documentNumber, documentDate, documentOrganization, personEduInstitution);

    }


}
