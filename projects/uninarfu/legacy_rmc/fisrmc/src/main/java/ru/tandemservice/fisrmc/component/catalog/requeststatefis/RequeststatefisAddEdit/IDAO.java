package ru.tandemservice.fisrmc.component.catalog.requeststatefis.RequeststatefisAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.fisrmc.entity.catalog.Requeststatefis;

public interface IDAO extends IDefaultCatalogAddEditDAO<Requeststatefis, Model> {

}
