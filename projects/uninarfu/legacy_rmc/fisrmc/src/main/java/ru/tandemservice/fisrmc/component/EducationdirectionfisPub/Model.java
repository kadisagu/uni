package ru.tandemservice.fisrmc.component.EducationdirectionfisPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import ru.tandemservice.fisrmc.entity.Educationdirectionfis;

@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "educationdirectionfisId")
})

public class Model {

    private Long educationdirectionfisId = null;
    private Educationdirectionfis educationdirectionfis = null;

    public void setEducationdirectionfisId(Long educationdirectionfisId) {
        this.educationdirectionfisId = educationdirectionfisId;
    }

    public Long getEducationdirectionfisId() {
        return educationdirectionfisId;
    }

    public void setEducationdirectionfis(Educationdirectionfis educationdirectionfis) {
        this.educationdirectionfis = educationdirectionfis;
    }

    public Educationdirectionfis getEducationdirectionfis() {
        return educationdirectionfis;
    }
}
