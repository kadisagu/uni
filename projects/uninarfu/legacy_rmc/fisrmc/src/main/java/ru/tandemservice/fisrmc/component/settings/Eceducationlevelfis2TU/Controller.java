package ru.tandemservice.fisrmc.component.settings.Eceducationlevelfis2TU;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.fisrmc.entity.catalog.Eceducationlevelfis;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;


public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);
        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        Model model = (Model) getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<Eceducationlevelfis> dataSource = UniBaseUtils.createDataSource(component, getDao());
        createColumns(dataSource);
        model.setDataSource(dataSource);
    }

    private void createColumns
            (
                    DynamicListDataSource<Eceducationlevelfis> dataSource
            )
    {
        dataSource.addColumn(new SimpleColumn("Код", Eceducationlevelfis.code()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Название", Eceducationlevelfis.title()).setOrderable(false));
        dataSource.addColumn(new BlockColumn<Qualifications>("qualifications", "Квалификации НП"));
        dataSource.addColumn(new BlockColumn<EduProgramQualification>("eduProgramQualification", "Квалификации ОП"));

    }

    public void onClickApply(IBusinessComponent component)
    {
        IDAO dao = (IDAO) getDao();
        Model model = (Model) getModel(component);
        dao.update(model);

    }

}
