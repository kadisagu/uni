package ru.tandemservice.fisrmc.dao;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.fisrmc.entity.Educationdirectionfis;
import ru.tandemservice.fisrmc.entity.catalog.Eceducationlevelfis;
import ru.tandemservice.fisrmc.entity.catalog.EducationdirectionSpravType;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CompetitiveGroupsDAO
        extends UniBaseDao
        implements ICompetitiveGroupsDAO
{

    private static ICompetitiveGroupsDAO _ICompetitiveGroupsDAO = null;

    public static ICompetitiveGroupsDAO Instanse()
    {
        if (_ICompetitiveGroupsDAO == null)
            _ICompetitiveGroupsDAO = (ICompetitiveGroupsDAO) ApplicationRuntime.getBean("competitiveGroupsDAO");
        return _ICompetitiveGroupsDAO;
    }

    @Override
    /**
     * возвращает спислк конкурсных групп
     */
    public List<XMLCompetitiveGroup> getCompetitiveGroups(
            List<EnrollmentCampaign> lst
            , EducationdirectionSpravType spravType
            , boolean suspendCompetitiveGroup
    )
    {
        // примечания:
        // в рамках приемной кампании может и не быть конкурсных
        // групп
        // если конкурсных групп нет, то необходимо создать столько конкурсных групп
        // сколько направлений подготовки в рамках приемной кампании

        // как узнать, что конкурсных групп нет
        // 1 - в настройках приемки есть галочка
        // 2 - в направлениях для приема (enrollmentdirection_t) есть ссылка
        // competitiongroup_t
        // если на направлении подготовки для приема в ссылке null - значит такое направление подготовки в отдельную конкурсную группу


        // дополнения:
        /*
		 * XMLCompetitiveGroup - это информация по конкурсным группам, которую надо передать в ФИС.
		 * В Тандеме, если конкурсные группы используются, то нужно создать столько 
		 * XMLCompetitiveGroup, сколько конкурсных групп,
		 * если конкурсные группы не используются (в понимании танднма), то нужно
		 * создать столько XMLCompetitiveGroup, сколько у нас направлений для приема
		 * 
		 */
        ArrayList<XMLCompetitiveGroup> xmlCompetitiveGroups = new ArrayList<>();

        for (EnrollmentCampaign campaign : lst) {
            /**
             * В Тандеме, если конкурсные группы используются, то нужно создать столько
             *  XMLCompetitiveGroup, сколько конкурсных групп,
             */
            boolean useCG = campaign.isUseCompetitionGroup();
            if (suspendCompetitiveGroup)
                useCG = false;
            if (useCG) {
                List<CompetitionGroup> competitionGroups = new DQLSelectBuilder()
                        .fromEntity(CompetitionGroup.class, "cg")
                        .where(DQLExpressions.in(DQLExpressions.property(CompetitionGroup.enrollmentCampaign().fromAlias("cg")), lst))
                                //есть competitionGroup
                        .createStatement(getSession()).list();

                if (competitionGroups != null && !competitionGroups.isEmpty()) {
                    for (CompetitionGroup competitionGroup : competitionGroups) {
                        XMLCompetitiveGroup cg = new XMLCompetitiveGroup(competitionGroup.getId().toString(),useCG,campaign);

                        List<XMLCompetitiveGroupItem> items
                                = getCompetitiveGroupItems(cg, spravType);
                        cg.setCompetitiveGroupItem(items);

                        if (items != null && items.size() > 0)
                            //2) если конкурсные группы есть, то id из competitiongroup_t
                            xmlCompetitiveGroups.add(cg);
                    }
                }
            }
            /**
             * если конкурсные группы не используются (в понимании танднма), то нужно
             * создать столько XMLCompetitiveGroup, сколько у нас направлений для приема
             */
            else {
                List<EnrollmentDirection> directions = getList(EnrollmentDirection.class, EnrollmentDirection.enrollmentCampaign(), campaign);
                for (EnrollmentDirection direction : directions) {
                    XMLCompetitiveGroup cg = new XMLCompetitiveGroup(direction.getId().toString(),
                                                                     useCG,
                                                                     direction.getEnrollmentCampaign());

                    List<XMLCompetitiveGroupItem> items
                            = getCompetitiveGroupItems(cg, spravType);
                    cg.setCompetitiveGroupItem(items);

                    if (items != null && items.size() > 0)
                        //1) если конкурсных групп нет, то id из enrollmentdirection_t
                        xmlCompetitiveGroups.add(cg);
                }
            }
        }

        return xmlCompetitiveGroups;
    }

    @Override
    public List<XMLCompetitiveGroupItem> getCompetitiveGroupItems(
            XMLCompetitiveGroup competitiveGroup
            , EducationdirectionSpravType spravType
    )
    {
        List<XMLCompetitiveGroupItem> xmlGroupItems = new ArrayList<>();
        List<EnrollmentDirection> directions = SpravDao.Instanse().getTandemEnrollmentDirections(competitiveGroup);

        Map<XMLEnrollmentDirectionKey, List<EnrollmentDirection>> map = new HashMap<>();

        for (EnrollmentDirection enrollmentDirection : directions) {
            Educationdirectionfis educationdirectionfis = SpravDao.Instanse().getEducationdirectionfis(enrollmentDirection.getEducationOrgUnit(), spravType);
            EnrollmentCampaign enrollmentCampaign = competitiveGroup.getEnrollmentCampaign();
            Course course = SpravDao.Instanse().getCourse(enrollmentCampaign);
            Eceducationlevelfis eceducationlevelfis = SpravDao.Instanse().getEceducationlevelfis(enrollmentDirection.getEducationOrgUnit());

            if (educationdirectionfis == null)
                continue;

            XMLEnrollmentDirectionKey keyMap = new XMLEnrollmentDirectionKey
                    (enrollmentCampaign
                            , course
                            , eceducationlevelfis
                            , educationdirectionfis);

            List<EnrollmentDirection> lstxed;

            if (map.containsKey(keyMap))
                lstxed = map.get(keyMap);
            else {
                lstxed = new ArrayList<>();
                map.put(keyMap, lstxed);
            }

            Integer plan = 0;
            if (enrollmentDirection.getMinisterialPlan() != null && enrollmentDirection.getMinisterialPlan() > 0)
                plan += enrollmentDirection.getMinisterialPlan();

            if (enrollmentDirection.getContractPlan() != null && enrollmentDirection.getContractPlan() > 0)
                plan += enrollmentDirection.getContractPlan();

            if (enrollmentDirection.getListenerPlan() != null && enrollmentDirection.getListenerPlan() > 0)
                plan += enrollmentDirection.getListenerPlan();


            if (enrollmentDirection.getSecondHighEducationPlan() != null && enrollmentDirection.getSecondHighEducationPlan() > 0)
                plan += enrollmentDirection.getSecondHighEducationPlan();

            if (plan > 0)
                lstxed.add(enrollmentDirection);
        }

        for (XMLEnrollmentDirectionKey keyMap : map.keySet()) {
            List<EnrollmentDirection> enrollmentDirectionsList = map.get(keyMap);

            if (enrollmentDirectionsList.size() > 0) {
                XMLCompetitiveGroupItem xed = new XMLCompetitiveGroupItem(
                        competitiveGroup
                        , keyMap.getEceducationlevelfis()
                        , keyMap.getEducationdirectionfis()
                        , keyMap.getEnrollmentCampaign()
                        , enrollmentDirectionsList);
                xmlGroupItems.add(xed);
            }
        }
        return xmlGroupItems;
    }

}
