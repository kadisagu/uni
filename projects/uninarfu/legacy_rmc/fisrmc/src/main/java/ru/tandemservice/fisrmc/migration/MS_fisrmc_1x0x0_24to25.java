package ru.tandemservice.fisrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_fisrmc_1x0x0_24to25 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.5.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.5.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность eduProgramQualificationToFis

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("eduprogramqualificationtofis_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("eceducationlevelfis_id", DBType.LONG).setNullable(false),
                                      new DBColumn("qualifications_id", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("eduProgramQualificationToFis");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность qualificationToFis

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("qualificationtofis_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("eceducationlevelfis_id", DBType.LONG).setNullable(false),
                                      new DBColumn("qualifications_id", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("qualificationToFis");

        }


    }
}