package ru.tandemservice.fisrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.fisrmc.entity.FisExportFileAction;
import ru.tandemservice.fisrmc.entity.FisExportFiles;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Лог действий по обработке файла
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FisExportFileActionGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.fisrmc.entity.FisExportFileAction";
    public static final String ENTITY_NAME = "fisExportFileAction";
    public static final int VERSION_HASH = -1912558252;
    private static IEntityMeta ENTITY_META;

    public static final String L_FIS_EXPORT_FILES = "fisExportFiles";
    public static final String P_DATE = "date";
    public static final String P_ACTION = "action";
    public static final String P_OK = "ok";
    public static final String P_DESCRIPTION = "description";
    public static final String L_LOG = "log";

    private FisExportFiles _fisExportFiles;     // Файл из ФИС
    private Date _date;     // Дата
    private String _action;     // Действие
    private boolean _ok;     // Обработано без ошибок
    private String _description;     // Описание
    private DatabaseFile _log;     // Пакет из ФИС

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Файл из ФИС. Свойство не может быть null.
     */
    @NotNull
    public FisExportFiles getFisExportFiles()
    {
        return _fisExportFiles;
    }

    /**
     * @param fisExportFiles Файл из ФИС. Свойство не может быть null.
     */
    public void setFisExportFiles(FisExportFiles fisExportFiles)
    {
        dirty(_fisExportFiles, fisExportFiles);
        _fisExportFiles = fisExportFiles;
    }

    /**
     * @return Дата. Свойство не может быть null.
     */
    @NotNull
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Дата. Свойство не может быть null.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Действие. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getAction()
    {
        return _action;
    }

    /**
     * @param action Действие. Свойство не может быть null.
     */
    public void setAction(String action)
    {
        dirty(_action, action);
        _action = action;
    }

    /**
     * @return Обработано без ошибок. Свойство не может быть null.
     */
    @NotNull
    public boolean isOk()
    {
        return _ok;
    }

    /**
     * @param ok Обработано без ошибок. Свойство не может быть null.
     */
    public void setOk(boolean ok)
    {
        dirty(_ok, ok);
        _ok = ok;
    }

    /**
     * @return Описание.
     */
    @Length(max=1200)
    public String getDescription()
    {
        return _description;
    }

    /**
     * @param description Описание.
     */
    public void setDescription(String description)
    {
        dirty(_description, description);
        _description = description;
    }

    /**
     * @return Пакет из ФИС.
     */
    public DatabaseFile getLog()
    {
        return _log;
    }

    /**
     * @param log Пакет из ФИС.
     */
    public void setLog(DatabaseFile log)
    {
        dirty(_log, log);
        _log = log;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FisExportFileActionGen)
        {
            setFisExportFiles(((FisExportFileAction)another).getFisExportFiles());
            setDate(((FisExportFileAction)another).getDate());
            setAction(((FisExportFileAction)another).getAction());
            setOk(((FisExportFileAction)another).isOk());
            setDescription(((FisExportFileAction)another).getDescription());
            setLog(((FisExportFileAction)another).getLog());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FisExportFileActionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FisExportFileAction.class;
        }

        public T newInstance()
        {
            return (T) new FisExportFileAction();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "fisExportFiles":
                    return obj.getFisExportFiles();
                case "date":
                    return obj.getDate();
                case "action":
                    return obj.getAction();
                case "ok":
                    return obj.isOk();
                case "description":
                    return obj.getDescription();
                case "log":
                    return obj.getLog();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "fisExportFiles":
                    obj.setFisExportFiles((FisExportFiles) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "action":
                    obj.setAction((String) value);
                    return;
                case "ok":
                    obj.setOk((Boolean) value);
                    return;
                case "description":
                    obj.setDescription((String) value);
                    return;
                case "log":
                    obj.setLog((DatabaseFile) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "fisExportFiles":
                        return true;
                case "date":
                        return true;
                case "action":
                        return true;
                case "ok":
                        return true;
                case "description":
                        return true;
                case "log":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "fisExportFiles":
                    return true;
                case "date":
                    return true;
                case "action":
                    return true;
                case "ok":
                    return true;
                case "description":
                    return true;
                case "log":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "fisExportFiles":
                    return FisExportFiles.class;
                case "date":
                    return Date.class;
                case "action":
                    return String.class;
                case "ok":
                    return Boolean.class;
                case "description":
                    return String.class;
                case "log":
                    return DatabaseFile.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FisExportFileAction> _dslPath = new Path<FisExportFileAction>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FisExportFileAction");
    }
            

    /**
     * @return Файл из ФИС. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisExportFileAction#getFisExportFiles()
     */
    public static FisExportFiles.Path<FisExportFiles> fisExportFiles()
    {
        return _dslPath.fisExportFiles();
    }

    /**
     * @return Дата. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisExportFileAction#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Действие. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisExportFileAction#getAction()
     */
    public static PropertyPath<String> action()
    {
        return _dslPath.action();
    }

    /**
     * @return Обработано без ошибок. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisExportFileAction#isOk()
     */
    public static PropertyPath<Boolean> ok()
    {
        return _dslPath.ok();
    }

    /**
     * @return Описание.
     * @see ru.tandemservice.fisrmc.entity.FisExportFileAction#getDescription()
     */
    public static PropertyPath<String> description()
    {
        return _dslPath.description();
    }

    /**
     * @return Пакет из ФИС.
     * @see ru.tandemservice.fisrmc.entity.FisExportFileAction#getLog()
     */
    public static DatabaseFile.Path<DatabaseFile> log()
    {
        return _dslPath.log();
    }

    public static class Path<E extends FisExportFileAction> extends EntityPath<E>
    {
        private FisExportFiles.Path<FisExportFiles> _fisExportFiles;
        private PropertyPath<Date> _date;
        private PropertyPath<String> _action;
        private PropertyPath<Boolean> _ok;
        private PropertyPath<String> _description;
        private DatabaseFile.Path<DatabaseFile> _log;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Файл из ФИС. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisExportFileAction#getFisExportFiles()
     */
        public FisExportFiles.Path<FisExportFiles> fisExportFiles()
        {
            if(_fisExportFiles == null )
                _fisExportFiles = new FisExportFiles.Path<FisExportFiles>(L_FIS_EXPORT_FILES, this);
            return _fisExportFiles;
        }

    /**
     * @return Дата. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisExportFileAction#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(FisExportFileActionGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Действие. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisExportFileAction#getAction()
     */
        public PropertyPath<String> action()
        {
            if(_action == null )
                _action = new PropertyPath<String>(FisExportFileActionGen.P_ACTION, this);
            return _action;
        }

    /**
     * @return Обработано без ошибок. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisExportFileAction#isOk()
     */
        public PropertyPath<Boolean> ok()
        {
            if(_ok == null )
                _ok = new PropertyPath<Boolean>(FisExportFileActionGen.P_OK, this);
            return _ok;
        }

    /**
     * @return Описание.
     * @see ru.tandemservice.fisrmc.entity.FisExportFileAction#getDescription()
     */
        public PropertyPath<String> description()
        {
            if(_description == null )
                _description = new PropertyPath<String>(FisExportFileActionGen.P_DESCRIPTION, this);
            return _description;
        }

    /**
     * @return Пакет из ФИС.
     * @see ru.tandemservice.fisrmc.entity.FisExportFileAction#getLog()
     */
        public DatabaseFile.Path<DatabaseFile> log()
        {
            if(_log == null )
                _log = new DatabaseFile.Path<DatabaseFile>(L_LOG, this);
            return _log;
        }

        public Class getEntityClass()
        {
            return FisExportFileAction.class;
        }

        public String getEntityName()
        {
            return "fisExportFileAction";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
