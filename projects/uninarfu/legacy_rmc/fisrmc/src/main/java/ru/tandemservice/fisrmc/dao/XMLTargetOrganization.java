package ru.tandemservice.fisrmc.dao;

import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;

import java.util.Date;
import java.util.List;

/**
 * сведения о целевом наборе
 * страница 9 из описания сервиса фис
 *
 * @author vch
 */
public class XMLTargetOrganization {

    /**
     * вид целевого приема
     * какая организация прислала заявку
     */
    private TargetAdmissionKind targetadmissionkind = null;
    private XMLCompetitiveGroup competitiveGroup = null;


    private List<XMLTargetOrganizationItem> items = null;

    private String _uid = null;

    public XMLTargetOrganization
            (
                    XMLCompetitiveGroup competitiveGroup
                    , TargetAdmissionKind targetadmissionkind
            )
    {

        // competitiveGroup.getEnrollmentCampaign().getId();

        this.setCompetitiveGroup(competitiveGroup);
        this.setTargetadmissionkind(targetadmissionkind);
    }

    public void setTargetadmissionkind(TargetAdmissionKind targetadmissionkind) {
        this.targetadmissionkind = targetadmissionkind;
    }

    public TargetAdmissionKind getTargetadmissionkind() {
        return targetadmissionkind;
    }

    public void setItems(List<XMLTargetOrganizationItem> items) {
        this.items = items;
    }

    public List<XMLTargetOrganizationItem> getItems() {
        return items;
    }

    public String getUid()
    {
        if (_uid != null)
            return _uid;
        else {
            // uid должен зависеть от года проведения приемки
            Date startDate = competitiveGroup.getEnrollmentCampaign().getStartDate();

            if (ToolsDao.getYear(startDate) < 2013) {
                _uid = Long.toString(targetadmissionkind.getId());
            }
            else {
                // в приемках 2013 - новый алгоритм
                String key = "";
                key += Long.toString(targetadmissionkind.getId());
                key += Long.toString(competitiveGroup.getEnrollmentCampaign().getId());
                _uid = "more2013hk_" + key.hashCode();
            }

            return _uid;
        }
    }

    public void setCompetitiveGroup(XMLCompetitiveGroup competitiveGroup) {
        this.competitiveGroup = competitiveGroup;
    }

    public XMLCompetitiveGroup getCompetitiveGroup() {
        return competitiveGroup;
    }
}
