package ru.tandemservice.fisrmc.dao;

import ru.tandemservice.fisrmc.entity.catalog.Compensationtypefis;
import ru.tandemservice.fisrmc.entity.catalog.Developformfis;
import ru.tandemservice.fisrmc.entity.catalog.Eceducationlevelfis;
import ru.tandemservice.uni.entity.catalog.Course;


public class XmlCampaignDate {

    private Course course;

    private Eceducationlevelfis eceducationlevelfis;

    private Developformfis developformfis;

    private Compensationtypefis compensationtypefis;

    private String stage = "";

    public XmlCampaignDate
            (
                    Course course
                    , Eceducationlevelfis eceducationlevelfis
                    , Developformfis developformfis
                    , Compensationtypefis compensationtypefis
                    , String stage
            )
    {
        this.setCourse(course);
        this.setEceducationlevelfis(eceducationlevelfis);
        this.setDevelopformfis(developformfis);
        this.setCompensationtypefis(compensationtypefis);
        this.setStage(stage);

    }

    public String getCode()
    {
        String code = "";
        if (course != null)
            code += "course=" + Integer.toString(course.getIntValue());

        if (eceducationlevelfis != null)
            code += ";eceducationlevelfis=" + eceducationlevelfis.getCode();


        if (developformfis != null)
            code += ";developformfis=" + developformfis.getCode();


        if (compensationtypefis != null)
            code += ";compensationtypefis=" + compensationtypefis.getCode();

        code += ";stage=" + stage;

        return code;
    }

    @Override
    public int hashCode()
    {
        return getCode().hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof XmlCampaignDate) {
            int hc = ((XmlCampaignDate) obj).hashCode();
            int hcExsit = this.hashCode();

            if (hc == hcExsit)
                return true;
            else
                return false;
        }
        else
            return super.equals(obj);
    }


    public void setCourse(Course course) {
        this.course = course;
    }


    public Course getCourse() {
        return course;
    }


    public void setEceducationlevelfis(Eceducationlevelfis eceducationlevelfis) {
        this.eceducationlevelfis = eceducationlevelfis;
    }


    public Eceducationlevelfis getEceducationlevelfis() {
        return eceducationlevelfis;
    }


    public void setDevelopformfis(Developformfis developformfis) {
        this.developformfis = developformfis;
    }


    public Developformfis getDevelopformfis() {
        return developformfis;
    }


    public void setCompensationtypefis(Compensationtypefis compensationtypefis) {
        this.compensationtypefis = compensationtypefis;
    }


    public Compensationtypefis getCompensationtypefis() {
        return compensationtypefis;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public String getStage() {
        return stage;
    }

}
