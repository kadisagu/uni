package ru.tandemservice.fisrmc.component.catalog.developformfis.DevelopformfisAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.fisrmc.entity.catalog.Developformfis;

public interface IDAO extends IDefaultCatalogAddEditDAO<Developformfis, Model> {

}
