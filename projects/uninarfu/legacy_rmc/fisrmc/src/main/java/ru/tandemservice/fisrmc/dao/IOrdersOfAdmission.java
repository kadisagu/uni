package ru.tandemservice.fisrmc.dao;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.tandemservice.fisrmc.entity.catalog.EducationdirectionSpravType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.List;

public interface IOrdersOfAdmission {

    /**
     * Упаковать сведения по заявлениям, включенные в приказ
     *
     * @param doc
     * @param entrantrequests
     *
     * @return
     */
    public Element packOrdersOfAdmission
    (
            Document doc
            , List<EnrollmentCampaign> ecList
            , EducationdirectionSpravType spravType
            , Boolean isAppDateWithoutTime
    );

}
