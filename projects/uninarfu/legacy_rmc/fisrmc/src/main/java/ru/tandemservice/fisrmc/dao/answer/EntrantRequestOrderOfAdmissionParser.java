package ru.tandemservice.fisrmc.dao.answer;

import ru.tandemservice.fisrmc.component.UploadFis2TU.Pub.DefaultParser;

public class EntrantRequestOrderOfAdmissionParser extends DefaultParser
{
    public static final String ROOT = "Root";
    public static final String DATA = "PackageData";
    public static final String ORDERS_OfAdmission = "OrdersOfAdmission";
    public static final String ORDER_OfAdmission = "OrderOfAdmission";
    public static final String APPLICATION = "Application";

    public static final String APPLICATION_NUMBER = "ApplicationNumber";
    public static final String APPLICATION_DATE = "RegistrationDate";

    /*

    - <OrdersOfAdmission>
    - <OrderOfAdmission>
    - <Application>
      <ApplicationNumber>40310</ApplicationNumber>
      <RegistrationDate>2012-07-07T15:13:00</RegistrationDate>
      </Application>
*/
    public EntrantRequestOrderOfAdmissionParser(String xml) {
        super(xml);

        this.elementPath = ROOT + "-" + DATA + "-" + ORDERS_OfAdmission + "-" + ORDER_OfAdmission + "-" + APPLICATION;

        this.propertyMap.put(
                this.elementPath + "-" + APPLICATION_NUMBER,
                "number"
        );
        this.propertyMap.put(
                this.elementPath + "-" + APPLICATION_DATE,
                "date"
        );

    }

}
