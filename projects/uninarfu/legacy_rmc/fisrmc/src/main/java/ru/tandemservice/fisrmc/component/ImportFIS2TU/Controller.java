package ru.tandemservice.fisrmc.component.ImportFIS2TU;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        ((IDAO) getDao()).prepare(getModel(component));

        if (model.getSettings() == null)
            model.setSettings(component.getSettings());

    }

    public void onClickApply(IBusinessComponent component)
    {
        Model model = (Model) getModel(component);
        ((IDAO) getDao()).update(model);

        // deactivate(component);
    }
}
