package ru.tandemservice.fisrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.fisrmc.entity.StateExamSubject2Ecdisciplinefis;
import ru.tandemservice.fisrmc.entity.catalog.Ecdisciplinefis;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность-связь Предмет ЕГЭ и Общеобразовательные прдметы (ФИС)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StateExamSubject2EcdisciplinefisGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.fisrmc.entity.StateExamSubject2Ecdisciplinefis";
    public static final String ENTITY_NAME = "stateExamSubject2Ecdisciplinefis";
    public static final int VERSION_HASH = -606858405;
    private static IEntityMeta ENTITY_META;

    public static final String L_STATE_EXAM_SUBJECT = "stateExamSubject";
    public static final String L_ECDISCIPLINEFIS = "ecdisciplinefis";

    private StateExamSubject _stateExamSubject;     // Предмет ЕГЭ
    private Ecdisciplinefis _ecdisciplinefis;     // Общеобразовательные предметы

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Предмет ЕГЭ. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public StateExamSubject getStateExamSubject()
    {
        return _stateExamSubject;
    }

    /**
     * @param stateExamSubject Предмет ЕГЭ. Свойство не может быть null и должно быть уникальным.
     */
    public void setStateExamSubject(StateExamSubject stateExamSubject)
    {
        dirty(_stateExamSubject, stateExamSubject);
        _stateExamSubject = stateExamSubject;
    }

    /**
     * @return Общеобразовательные предметы. Свойство не может быть null.
     */
    @NotNull
    public Ecdisciplinefis getEcdisciplinefis()
    {
        return _ecdisciplinefis;
    }

    /**
     * @param ecdisciplinefis Общеобразовательные предметы. Свойство не может быть null.
     */
    public void setEcdisciplinefis(Ecdisciplinefis ecdisciplinefis)
    {
        dirty(_ecdisciplinefis, ecdisciplinefis);
        _ecdisciplinefis = ecdisciplinefis;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StateExamSubject2EcdisciplinefisGen)
        {
            setStateExamSubject(((StateExamSubject2Ecdisciplinefis)another).getStateExamSubject());
            setEcdisciplinefis(((StateExamSubject2Ecdisciplinefis)another).getEcdisciplinefis());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StateExamSubject2EcdisciplinefisGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StateExamSubject2Ecdisciplinefis.class;
        }

        public T newInstance()
        {
            return (T) new StateExamSubject2Ecdisciplinefis();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "stateExamSubject":
                    return obj.getStateExamSubject();
                case "ecdisciplinefis":
                    return obj.getEcdisciplinefis();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "stateExamSubject":
                    obj.setStateExamSubject((StateExamSubject) value);
                    return;
                case "ecdisciplinefis":
                    obj.setEcdisciplinefis((Ecdisciplinefis) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "stateExamSubject":
                        return true;
                case "ecdisciplinefis":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "stateExamSubject":
                    return true;
                case "ecdisciplinefis":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "stateExamSubject":
                    return StateExamSubject.class;
                case "ecdisciplinefis":
                    return Ecdisciplinefis.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StateExamSubject2Ecdisciplinefis> _dslPath = new Path<StateExamSubject2Ecdisciplinefis>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StateExamSubject2Ecdisciplinefis");
    }
            

    /**
     * @return Предмет ЕГЭ. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.fisrmc.entity.StateExamSubject2Ecdisciplinefis#getStateExamSubject()
     */
    public static StateExamSubject.Path<StateExamSubject> stateExamSubject()
    {
        return _dslPath.stateExamSubject();
    }

    /**
     * @return Общеобразовательные предметы. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.StateExamSubject2Ecdisciplinefis#getEcdisciplinefis()
     */
    public static Ecdisciplinefis.Path<Ecdisciplinefis> ecdisciplinefis()
    {
        return _dslPath.ecdisciplinefis();
    }

    public static class Path<E extends StateExamSubject2Ecdisciplinefis> extends EntityPath<E>
    {
        private StateExamSubject.Path<StateExamSubject> _stateExamSubject;
        private Ecdisciplinefis.Path<Ecdisciplinefis> _ecdisciplinefis;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Предмет ЕГЭ. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.fisrmc.entity.StateExamSubject2Ecdisciplinefis#getStateExamSubject()
     */
        public StateExamSubject.Path<StateExamSubject> stateExamSubject()
        {
            if(_stateExamSubject == null )
                _stateExamSubject = new StateExamSubject.Path<StateExamSubject>(L_STATE_EXAM_SUBJECT, this);
            return _stateExamSubject;
        }

    /**
     * @return Общеобразовательные предметы. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.StateExamSubject2Ecdisciplinefis#getEcdisciplinefis()
     */
        public Ecdisciplinefis.Path<Ecdisciplinefis> ecdisciplinefis()
        {
            if(_ecdisciplinefis == null )
                _ecdisciplinefis = new Ecdisciplinefis.Path<Ecdisciplinefis>(L_ECDISCIPLINEFIS, this);
            return _ecdisciplinefis;
        }

        public Class getEntityClass()
        {
            return StateExamSubject2Ecdisciplinefis.class;
        }

        public String getEntityName()
        {
            return "stateExamSubject2Ecdisciplinefis";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
