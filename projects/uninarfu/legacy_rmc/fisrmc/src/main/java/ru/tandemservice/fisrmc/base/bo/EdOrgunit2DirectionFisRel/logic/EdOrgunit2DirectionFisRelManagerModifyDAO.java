package ru.tandemservice.fisrmc.base.bo.EdOrgunit2DirectionFisRel.logic;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.fisrmc.entity.EdOrgunit2DirectionFis;
import ru.tandemservice.fisrmc.entity.Educationdirectionfis;
import ru.tandemservice.fisrmc.entity.catalog.EducationdirectionSpravType;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

import java.util.List;


public class EdOrgunit2DirectionFisRelManagerModifyDAO extends CommonDAO implements IEdOrgunit2DirectionFisRelManagerModifyDAO {

    @Override
    public void saveOrUpdateRelation
            (
                    EducationOrgUnit educationOrgUnit
                    , Educationdirectionfis educationdirectionfis
            )
    {

        EducationdirectionSpravType spravType = educationdirectionfis.getSpravType();
        if (spravType == null)
            throw new ApplicationException("У направления подготовки ФИС " + educationdirectionfis.getTitle() + " не указан тип справочника");


        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EdOrgunit2DirectionFis.class, "edf")
                        // направление для приема
                .where(DQLExpressions.eq(DQLExpressions.property(EdOrgunit2DirectionFis.educationOrgUnit().fromAlias("edf")), DQLExpressions.value(educationOrgUnit)))
                        // тип справочника
                .where(DQLExpressions.eq(DQLExpressions.property(EdOrgunit2DirectionFis.educationDirectionFis().spravType().fromAlias("edf")), DQLExpressions.value(spravType)));

        // НП фис
        //.where(DQLExpressions.eq(DQLExpressions.property(EdOrgunit2DirectionFis.educationDirectionFis().fromAlias("edf")), DQLExpressions.value( educationdirectionfis )));
        List<EdOrgunit2DirectionFis> lst = getList(builder);

        // в рамках года мы уже с кем-то сопоставлены
        if (lst.size() > 0) {
            // уникальность в описании сушности
            for (EdOrgunit2DirectionFis rel : lst) {
                if (!rel.getEducationDirectionFis().equals(educationdirectionfis)) {
                    rel.setEducationDirectionFis(educationdirectionfis);
                    update(rel);
                    getSession().flush();
                }
            }

        }
        else {
            // создаем новое
            EdOrgunit2DirectionFis relDirectionFis = new EdOrgunit2DirectionFis();
            relDirectionFis.setEducationDirectionFis(educationdirectionfis);
            relDirectionFis.setEducationOrgUnit(educationOrgUnit);

            saveOrUpdate(relDirectionFis);
            getSession().flush();
        }
    }

//	public void update(EducationOrgUnit educationOrgUnit,Educationdirectionfis educationdirectionfis) 
//	{
//
//		EducationdirectionSpravType spravType = educationdirectionfis.getSpravType();
//		if (spravType==null)
//			throw new ApplicationException("У направления подготовки ФИС " + educationdirectionfis.getTitle() + " не указан тип справочника");
//
//		
//		DQLUpdateBuilder dqlUpdateBuilder = new DQLUpdateBuilder(EdOrgunit2DirectionFis.class)
//		.where(DQLExpressions.eq(DQLExpressions.property(EdOrgunit2DirectionFis.educationOrgUnit()), DQLExpressions.value(educationOrgUnit)))
//		.where(DQLExpressions.eq(DQLExpressions.property(EdOrgunit2DirectionFis.educationDirectionFis().spravType()), DQLExpressions.value(spravType)))
//		
//		.set("educationDirectionFis", DQLExpressions.value(educationdirectionfis));
//		dqlUpdateBuilder.createStatement(getSession()).execute();
//
//	}

    @Override
    /**
     * сей метод сопоставляет направления подготовки с ФИС
     */
    public void persistRelation(
            List<DataWrapper> dataSourceRecords
            , EducationdirectionSpravType spravType)
    {


        for (DataWrapper wrapper : dataSourceRecords) {

            EducationOrgUnit educationOrgUnit = wrapper.get("eou");
            EdOrgunit2DirectionFis rel = wrapper.get("rel");

            if (educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject() == null) {
                // нет ОП, ну и не надо
                continue;
            }

            String subjectCode_main = educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject().getSubjectCode();
            String subjectCode_spec = null;

            if (educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getEduProgramSpecialization() != null)
                subjectCode_spec = educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getEduProgramSpecialization().getProgramSubject().getSubjectCode();

            String findCode = subjectCode_main;
            if (subjectCode_spec != null)
                findCode = subjectCode_spec;


            DQLSelectBuilder builder = new DQLSelectBuilder()
                    .fromEntity(Educationdirectionfis.class, "edf")
                            // фильтр квалификации
                    .where(DQLExpressions.eq(DQLExpressions.property(Educationdirectionfis.QualificationCode().fromAlias("edf")), DQLExpressions.value(educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getQualification().getCode())))

                            // по коду
                    .where(DQLExpressions.or
                            (
                                    DQLExpressions.eq(DQLExpressions.property(Educationdirectionfis.Codefis().fromAlias("edf")), DQLExpressions.value(findCode))
                                    , DQLExpressions.eq(DQLExpressions.property(Educationdirectionfis.NewCode().fromAlias("edf")), DQLExpressions.value(findCode))
                            ))

                            // по типу справочника
                    .where(DQLExpressions.eq(DQLExpressions.property(Educationdirectionfis.spravType().fromAlias("edf")), DQLExpressions.value(spravType)));

            List<Educationdirectionfis> educationdirectionfis = builder.createStatement(getSession()).list();

            if (educationdirectionfis.size() == 1) {
                Educationdirectionfis edFis = educationdirectionfis.get(0);

                if (rel != null && rel.getEducationDirectionFis().equals(edFis)) {
                    // все и так хорошо
                    // ничего не надо
                }
                else
                    saveOrUpdateRelation(educationOrgUnit, edFis);
            }
            else {
                if (educationdirectionfis.size() > 1) {
                    // получили несколько записей
                    // возможны разные сроки освоения
                    Educationdirectionfis edfCompare = null;
                    for (Educationdirectionfis edf : educationdirectionfis) {
                        String dp = educationOrgUnit.getDevelopPeriod().getTitle().toLowerCase().trim();
                        if (edf.getPeriod().toLowerCase().trim().equals(dp))
                            // совпало по названию и периоду
                            edfCompare = edf;
                    }
                    if (edfCompare != null) {
                        if (rel != null && rel.getEducationDirectionFis().equals(edfCompare)) {
                            // все и так хорошо
                            // ничего не надо
                        }
                        else
                            saveOrUpdateRelation(educationOrgUnit, edfCompare);
                    }
                }
            }
        }

    }

    @Override
    public void deleteRelation(Long educationOrgUnitID, Long spravType)
    {

        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(EdOrgunit2DirectionFis.class)
                .where(DQLExpressions.eq(DQLExpressions.property(EdOrgunit2DirectionFis.educationOrgUnit().id()), DQLExpressions.value(educationOrgUnitID)))
                .where(DQLExpressions.eq(DQLExpressions.property(EdOrgunit2DirectionFis.educationDirectionFis().spravType().id()), DQLExpressions.value(spravType)));


        deleteBuilder.createStatement(getSession()).execute();
    }

    @Override
    public Educationdirectionfis getEducationdirectionfis(
            EducationOrgUnit eduOu
            , EducationdirectionSpravType spravType)
    {
        List<EdOrgunit2DirectionFis> lst = new DQLSelectBuilder()
                .fromEntity(EdOrgunit2DirectionFis.class, "e")
                .where(DQLExpressions.eq(DQLExpressions.property(EdOrgunit2DirectionFis.educationOrgUnit().fromAlias("e")), DQLExpressions.value(eduOu)))
                .where(DQLExpressions.eq(DQLExpressions.property(EdOrgunit2DirectionFis.educationDirectionFis().spravType().fromAlias("e")), DQLExpressions.value(spravType)))
                .createStatement(getSession()).list();

        Educationdirectionfis retVal = null;
        for (EdOrgunit2DirectionFis edf : lst) {
            if (edf.getEducationDirectionFis() != null)
                retVal = edf.getEducationDirectionFis();
        }
        return retVal;
    }


}
