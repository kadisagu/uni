package ru.tandemservice.fisrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.fisrmc.entity.FisCheckEgeSertificate;
import ru.tandemservice.fisrmc.entity.catalog.FisEgeStatusCode;
import ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проверка сертификатов ЕГЭ
 *
 * Если сертификат загружен без ошибок, то обязательно наличие кода fisEgeStatusCode=0 'Ошибок нет'
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FisCheckEgeSertificateGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.fisrmc.entity.FisCheckEgeSertificate";
    public static final String ENTITY_NAME = "fisCheckEgeSertificate";
    public static final int VERSION_HASH = -1795150200;
    private static IEntityMeta ENTITY_META;

    public static final String P_CHECK_DATE = "checkDate";
    public static final String L_ENTRANT_STATE_EXAM_CERTIFICATE = "entrantStateExamCertificate";
    public static final String L_FIS_EGE_STATUS_CODE = "fisEgeStatusCode";
    public static final String P_ERROR_CODE_EGE = "errorCodeEge";
    public static final String P_ERROR_MESSAGE_EGE = "errorMessageEge";

    private Date _checkDate;     // Дата проверки
    private EntrantStateExamCertificate _entrantStateExamCertificate;     // Сертификат ЕГЭ
    private FisEgeStatusCode _fisEgeStatusCode;     // Статус проверки ЕГЭ
    private Integer _errorCodeEge;     // код ошибки из фис
    private String _errorMessageEge;     // Описание ошибки из фис

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата проверки. Свойство не может быть null.
     */
    @NotNull
    public Date getCheckDate()
    {
        return _checkDate;
    }

    /**
     * @param checkDate Дата проверки. Свойство не может быть null.
     */
    public void setCheckDate(Date checkDate)
    {
        dirty(_checkDate, checkDate);
        _checkDate = checkDate;
    }

    /**
     * @return Сертификат ЕГЭ. Свойство не может быть null.
     */
    @NotNull
    public EntrantStateExamCertificate getEntrantStateExamCertificate()
    {
        return _entrantStateExamCertificate;
    }

    /**
     * @param entrantStateExamCertificate Сертификат ЕГЭ. Свойство не может быть null.
     */
    public void setEntrantStateExamCertificate(EntrantStateExamCertificate entrantStateExamCertificate)
    {
        dirty(_entrantStateExamCertificate, entrantStateExamCertificate);
        _entrantStateExamCertificate = entrantStateExamCertificate;
    }

    /**
     * @return Статус проверки ЕГЭ. Свойство не может быть null.
     */
    @NotNull
    public FisEgeStatusCode getFisEgeStatusCode()
    {
        return _fisEgeStatusCode;
    }

    /**
     * @param fisEgeStatusCode Статус проверки ЕГЭ. Свойство не может быть null.
     */
    public void setFisEgeStatusCode(FisEgeStatusCode fisEgeStatusCode)
    {
        dirty(_fisEgeStatusCode, fisEgeStatusCode);
        _fisEgeStatusCode = fisEgeStatusCode;
    }

    /**
     * @return код ошибки из фис.
     */
    public Integer getErrorCodeEge()
    {
        return _errorCodeEge;
    }

    /**
     * @param errorCodeEge код ошибки из фис.
     */
    public void setErrorCodeEge(Integer errorCodeEge)
    {
        dirty(_errorCodeEge, errorCodeEge);
        _errorCodeEge = errorCodeEge;
    }

    /**
     * @return Описание ошибки из фис.
     */
    @Length(max=1200)
    public String getErrorMessageEge()
    {
        return _errorMessageEge;
    }

    /**
     * @param errorMessageEge Описание ошибки из фис.
     */
    public void setErrorMessageEge(String errorMessageEge)
    {
        dirty(_errorMessageEge, errorMessageEge);
        _errorMessageEge = errorMessageEge;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FisCheckEgeSertificateGen)
        {
            setCheckDate(((FisCheckEgeSertificate)another).getCheckDate());
            setEntrantStateExamCertificate(((FisCheckEgeSertificate)another).getEntrantStateExamCertificate());
            setFisEgeStatusCode(((FisCheckEgeSertificate)another).getFisEgeStatusCode());
            setErrorCodeEge(((FisCheckEgeSertificate)another).getErrorCodeEge());
            setErrorMessageEge(((FisCheckEgeSertificate)another).getErrorMessageEge());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FisCheckEgeSertificateGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FisCheckEgeSertificate.class;
        }

        public T newInstance()
        {
            return (T) new FisCheckEgeSertificate();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "checkDate":
                    return obj.getCheckDate();
                case "entrantStateExamCertificate":
                    return obj.getEntrantStateExamCertificate();
                case "fisEgeStatusCode":
                    return obj.getFisEgeStatusCode();
                case "errorCodeEge":
                    return obj.getErrorCodeEge();
                case "errorMessageEge":
                    return obj.getErrorMessageEge();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "checkDate":
                    obj.setCheckDate((Date) value);
                    return;
                case "entrantStateExamCertificate":
                    obj.setEntrantStateExamCertificate((EntrantStateExamCertificate) value);
                    return;
                case "fisEgeStatusCode":
                    obj.setFisEgeStatusCode((FisEgeStatusCode) value);
                    return;
                case "errorCodeEge":
                    obj.setErrorCodeEge((Integer) value);
                    return;
                case "errorMessageEge":
                    obj.setErrorMessageEge((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "checkDate":
                        return true;
                case "entrantStateExamCertificate":
                        return true;
                case "fisEgeStatusCode":
                        return true;
                case "errorCodeEge":
                        return true;
                case "errorMessageEge":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "checkDate":
                    return true;
                case "entrantStateExamCertificate":
                    return true;
                case "fisEgeStatusCode":
                    return true;
                case "errorCodeEge":
                    return true;
                case "errorMessageEge":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "checkDate":
                    return Date.class;
                case "entrantStateExamCertificate":
                    return EntrantStateExamCertificate.class;
                case "fisEgeStatusCode":
                    return FisEgeStatusCode.class;
                case "errorCodeEge":
                    return Integer.class;
                case "errorMessageEge":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FisCheckEgeSertificate> _dslPath = new Path<FisCheckEgeSertificate>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FisCheckEgeSertificate");
    }
            

    /**
     * @return Дата проверки. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisCheckEgeSertificate#getCheckDate()
     */
    public static PropertyPath<Date> checkDate()
    {
        return _dslPath.checkDate();
    }

    /**
     * @return Сертификат ЕГЭ. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisCheckEgeSertificate#getEntrantStateExamCertificate()
     */
    public static EntrantStateExamCertificate.Path<EntrantStateExamCertificate> entrantStateExamCertificate()
    {
        return _dslPath.entrantStateExamCertificate();
    }

    /**
     * @return Статус проверки ЕГЭ. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisCheckEgeSertificate#getFisEgeStatusCode()
     */
    public static FisEgeStatusCode.Path<FisEgeStatusCode> fisEgeStatusCode()
    {
        return _dslPath.fisEgeStatusCode();
    }

    /**
     * @return код ошибки из фис.
     * @see ru.tandemservice.fisrmc.entity.FisCheckEgeSertificate#getErrorCodeEge()
     */
    public static PropertyPath<Integer> errorCodeEge()
    {
        return _dslPath.errorCodeEge();
    }

    /**
     * @return Описание ошибки из фис.
     * @see ru.tandemservice.fisrmc.entity.FisCheckEgeSertificate#getErrorMessageEge()
     */
    public static PropertyPath<String> errorMessageEge()
    {
        return _dslPath.errorMessageEge();
    }

    public static class Path<E extends FisCheckEgeSertificate> extends EntityPath<E>
    {
        private PropertyPath<Date> _checkDate;
        private EntrantStateExamCertificate.Path<EntrantStateExamCertificate> _entrantStateExamCertificate;
        private FisEgeStatusCode.Path<FisEgeStatusCode> _fisEgeStatusCode;
        private PropertyPath<Integer> _errorCodeEge;
        private PropertyPath<String> _errorMessageEge;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата проверки. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisCheckEgeSertificate#getCheckDate()
     */
        public PropertyPath<Date> checkDate()
        {
            if(_checkDate == null )
                _checkDate = new PropertyPath<Date>(FisCheckEgeSertificateGen.P_CHECK_DATE, this);
            return _checkDate;
        }

    /**
     * @return Сертификат ЕГЭ. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisCheckEgeSertificate#getEntrantStateExamCertificate()
     */
        public EntrantStateExamCertificate.Path<EntrantStateExamCertificate> entrantStateExamCertificate()
        {
            if(_entrantStateExamCertificate == null )
                _entrantStateExamCertificate = new EntrantStateExamCertificate.Path<EntrantStateExamCertificate>(L_ENTRANT_STATE_EXAM_CERTIFICATE, this);
            return _entrantStateExamCertificate;
        }

    /**
     * @return Статус проверки ЕГЭ. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisCheckEgeSertificate#getFisEgeStatusCode()
     */
        public FisEgeStatusCode.Path<FisEgeStatusCode> fisEgeStatusCode()
        {
            if(_fisEgeStatusCode == null )
                _fisEgeStatusCode = new FisEgeStatusCode.Path<FisEgeStatusCode>(L_FIS_EGE_STATUS_CODE, this);
            return _fisEgeStatusCode;
        }

    /**
     * @return код ошибки из фис.
     * @see ru.tandemservice.fisrmc.entity.FisCheckEgeSertificate#getErrorCodeEge()
     */
        public PropertyPath<Integer> errorCodeEge()
        {
            if(_errorCodeEge == null )
                _errorCodeEge = new PropertyPath<Integer>(FisCheckEgeSertificateGen.P_ERROR_CODE_EGE, this);
            return _errorCodeEge;
        }

    /**
     * @return Описание ошибки из фис.
     * @see ru.tandemservice.fisrmc.entity.FisCheckEgeSertificate#getErrorMessageEge()
     */
        public PropertyPath<String> errorMessageEge()
        {
            if(_errorMessageEge == null )
                _errorMessageEge = new PropertyPath<String>(FisCheckEgeSertificateGen.P_ERROR_MESSAGE_EGE, this);
            return _errorMessageEge;
        }

        public Class getEntityClass()
        {
            return FisCheckEgeSertificate.class;
        }

        public String getEntityName()
        {
            return "fisCheckEgeSertificate";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
