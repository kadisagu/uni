package ru.tandemservice.fisrmc.component.catalog.identitycardtypefis.IdentitycardtypefisAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.fisrmc.entity.catalog.Identitycardtypefis;

public interface IDAO extends IDefaultCatalogAddEditDAO<Identitycardtypefis, Model> {

}
