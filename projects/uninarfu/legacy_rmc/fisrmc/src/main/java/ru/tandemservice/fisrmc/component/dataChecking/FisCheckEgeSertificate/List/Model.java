package ru.tandemservice.fisrmc.component.dataChecking.FisCheckEgeSertificate.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.List;

public class Model implements IEnrollmentCampaignSelectModel {

    private IDataSettings settings;
    private DynamicListDataSource dataSource;
    private List<EnrollmentCampaign> enrollmentCampaignList;
    private IMultiSelectModel errorCodeModel;
    private IMultiSelectModel fisEgeStatusCodeModel;

    public IMultiSelectModel getFisEgeStatusCodeModel() {
        return fisEgeStatusCodeModel;
    }

    public void setFisEgeStatusCodeModel(IMultiSelectModel fisEgeStatusCodeModel) {
        this.fisEgeStatusCodeModel = fisEgeStatusCodeModel;
    }

    public DynamicListDataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(
            DynamicListDataSource dataSource)
    {
        this.dataSource = dataSource;
    }

    public IMultiSelectModel getErrorCodeModel() {
        return errorCodeModel;
    }

    public void setErrorCodeModel(IMultiSelectModel errorCodeModel) {
        this.errorCodeModel = errorCodeModel;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign() {
        return (EnrollmentCampaign) this.settings.get("enrollmentCampaign");
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList() {
        return enrollmentCampaignList;
    }

    @Override
    public IDataSettings getSettings() {
        return settings;
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentcampaign) {
        this.settings.set("enrollmentCampaign", enrollmentcampaign);
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> list) {
        this.enrollmentCampaignList = list;
    }

}
