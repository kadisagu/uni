package ru.tandemservice.fisrmc.dao.answer;

import ru.tandemservice.fisrmc.component.UploadFis2TU.Pub.DefaultParser;

public class CheckEgeDocumentParser extends DefaultParser
{

    public CheckEgeDocumentParser(String xml) {
        super(xml);

        this.elementPath = "ImportedAppCheckResultPackage-EgeDocumentCheckResults-EgeDocumentCheckResult";

        this.propertyMap.put(
                this.elementPath + "-Application-ApplicationNumber",
                "number"
        );
        this.propertyMap.put(
                this.elementPath + "-Application-RegistrationDate",
                "date"
        );

        this.propertyMap.put(
                this.elementPath + "-EgeDocuments-EgeDocument-DocumentNumber",
                "docNumber"
        );
        this.listProperty.add("docNumber");
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    public static class CheckEgeDocumentAdditionalParser extends DefaultParser {

        public CheckEgeDocumentAdditionalParser(String xml) {
            super(xml);

            this.elementPath = "ImportedAppCheckResultPackage-EgeDocumentCheckResults-EgeDocumentCheckResult-EgeDocuments-EgeDocument";

            this.propertyMap.put(
                    this.elementPath + "-DocumentNumber",
                    "docNumber"
            );
            this.propertyMap.put(
                    this.elementPath + "-StatusCode",
                    "docStatus"
            );
            this.propertyMap.put(
                    this.elementPath + "-StatusMessage",
                    "docMessage"
            );

			/*
			 * <EgeDocument>
          <StatusCode>2</StatusCode>
          <StatusMessage>Есть ошибки в оценках</StatusMessage>
          <DocumentNumber>66-000015819-12</DocumentNumber>
          <DocumentDate>2011-12-31T20:00:00.0000000Z</DocumentDate>
          <CorrectResults>
            <CorrectResultItemDto>
              <SubjectName>Математика</SubjectName>
              <Score>52</Score>
            </CorrectResultItemDto>
          </CorrectResults>
          <IncorrectResults>
            <IncorrectResultItemDto>
              <SubjectName>Физика</SubjectName>
              <Score>59</Score>
              <Comment>В АИС ФБС для предмета Физика 43,0, а не 59.</Comment>
			 */
            //List<String> discList = (List<String>)map.get("subject");
            this.propertyMap.put(
                    this.elementPath + "-IncorrectResults-IncorrectResultItemDto-Comment",
                    "markcomment"
            );
            this.listProperty.add("markcomment");
        }

    }


}
