package ru.tandemservice.fisrmc.component.settings.GroupEsForFis.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.fisrmc.entity.GroupEsForFis2Es;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.catalog.EducationSubject;

import java.util.ArrayList;
import java.util.List;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        final Model model = getModel(component);
        prepareDataSource(component);
        model.setSettings(component.getSettings());
        getDao().prepare(model);
    }

    protected void prepareDataSource(final IBusinessComponent component) {
        Model model = getModel(component);

        if (model.getEducationSubjectDS() != null && model.getGroupEsForFis2EsDS() != null)
            return;

        DynamicListDataSource<EducationSubject> dataSource = UniBaseUtils.createDataSource(component, getDao());

        DynamicListDataSource<GroupEsForFis2Es> groupDS = UniBaseUtils.createDataSource(component, getDao());
        prepareDataSourceEdColumns(dataSource);
        prepareDataSourceGrColumns(groupDS);
        model.setEducationSubjectDS(dataSource);
        model.setGroupEsForFis2EsDS(groupDS);
    }

    protected void prepareDataSourceEdColumns(DynamicListDataSource<EducationSubject> dataSource)
    {
        dataSource.addColumn(new CheckboxColumn("select"));
        dataSource.addColumn(new SimpleColumn("Название предмета", EducationSubject.title()).setOrderable(false));
    }

    protected void prepareDataSourceGrColumns(DynamicListDataSource<GroupEsForFis2Es> dataSource)
    {
        dataSource.addColumn(new SimpleColumn("Название предмета", GroupEsForFis2Es.educationSubject().title()).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Удалить", "delete", "onClickDeleteObject", "Удалить строку?"));
    }

    public void onClickMove(IBusinessComponent component)
    {
        Model model = getModel(component);
        CheckboxColumn checkboxColumn = (CheckboxColumn) model.getEducationSubjectDS().getColumn("select");
        List<IEntity> list = new ArrayList<IEntity>(checkboxColumn.getSelectedObjects());
        if (list.isEmpty() || list.size() == 0) {
            throw new ApplicationException("Необходимо выбрать хоть один предмет.");
        }
        getDao().moveGroups(model);
    }

    public void onClickMoveAll(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().moveAllGroups(model);
    }

    public void onClickDeleteObject(IBusinessComponent component)
    {
        getDao().deleteRow(component);
    }

    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().update(model);
        deactivate(component);
    }
}
