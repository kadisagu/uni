package ru.tandemservice.fisrmc.dao.packApplicationCommonBenefit;

import org.tandemframework.shared.person.base.entity.PersonBenefit;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.tandemservice.fisrmc.dao.interfaces.ICache;
import ru.tandemservice.fisrmc.entity.catalog.Entrantdocumenttypefis;
import ru.tandemservice.fisrmc.entity.catalog.Olimpiatfis;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.OlympiadDiploma;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.List;

public interface IDocumentReason
        extends ICache
{

	/*
     * Примечания:
	 * Как получить признак - сданы оригиналы
	 * if (requestedEnrollmentDirection!=null)
	 * 	requestedEnrollmentDirection.isOriginalDocumentHandedIn();
	 * 
	 * Если сданы оригиналы, то дату подачи оригиналов берем из даты подачи заявления
	 * ToolsDao.DateToString(entrantrequest.getRegDate());
	 */

    /**
     * упаковка обычной олимпиады
     *
     * @return
     */
    public Element packOlympicDocument
    (
            Document doc
            , EntrantRequest entrantrequest
            // , RequestedEnrollmentDirection requestedEnrollmentDirection
            , boolean isOrigin
            , OlympiadDiploma olympiadDiploma
            , Olimpiatfis olimpiatfis
    );

    /**
     * упаковка всеросийской олимпиады
     *
     * @param entrantrequest
     * @param requestedEnrollmentDirection
     * @param olympiadDiploma
     *
     * @return
     */
    public Element packOlympicTotalDocument
    (Document doc
            , EntrantRequest entrantrequest
     //, RequestedEnrollmentDirection requestedEnrollmentDirection
            , boolean isOrigin
            , OlympiadDiploma olympiadDiploma
            , Olimpiatfis olimpiatfis
    );


    /**
     * @param entrantrequest
     * @param requestedEnrollmentDirection
     * @param entrantdocumenttypefis
     * @param listBenefit
     *
     * @return
     */
    public Element packCustomDocument
    (Document doc
            , EntrantRequest entrantrequest
            , RequestedEnrollmentDirection requestedEnrollmentDirection
            , Entrantdocumenttypefis entrantdocumenttypefis
            , List<PersonBenefit> listBenefit
    );


}
