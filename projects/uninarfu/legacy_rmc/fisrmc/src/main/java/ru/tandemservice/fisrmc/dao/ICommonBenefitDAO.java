package ru.tandemservice.fisrmc.dao;

import java.util.List;

public interface ICommonBenefitDAO {

    /**
     * сведения об общей льготе (по олимпиадам школьников)
     *
     * @param competitiveGroup
     *
     * @return
     */
    List<XMLCommonBenefit> getCommonOrEntranceTestBenefit(XMLCompetitiveGroup competitiveGroup, String parentId, boolean isCommon);

}
