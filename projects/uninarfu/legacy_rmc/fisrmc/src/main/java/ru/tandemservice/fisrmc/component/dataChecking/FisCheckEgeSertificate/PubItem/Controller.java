package ru.tandemservice.fisrmc.component.dataChecking.FisCheckEgeSertificate.PubItem;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        ((IDAO) getDao()).prepare((Model) getModel(component));
    }

}
