package ru.tandemservice.fisrmc.migration;

import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import ru.tandemservice.fisrmc.entity.EntranceDisciplineType2Entrancedisciplinetypefis;

public class MS_fisrmc_1x0x0_0to1 extends IndependentMigrationScript {

    @Override
    public void run(DBTool tool) throws Exception {


        /**
         * RM# 2632
         * Удалим записи из таблицы настройки ФИС -> Справочники -> Настройка «Типы вступительных испытаний»
         */
        IEntityMeta meta = EntityRuntime.getMeta(EntranceDisciplineType2Entrancedisciplinetypefis.class);
        String tableName = meta.getTableName();

        if (tool.tableExists(tableName)) {

            tool.executeUpdate("delete  from " + tableName);

        }

    }

}
