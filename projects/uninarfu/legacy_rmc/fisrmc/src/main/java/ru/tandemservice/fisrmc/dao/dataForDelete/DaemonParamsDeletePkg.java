package ru.tandemservice.fisrmc.dao.dataForDelete;

import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.ArrayList;
import java.util.List;

/**
 * Параметры запуска демона
 */
public class DaemonParamsDeletePkg {
    public static boolean NEED_RUN = false;
    private boolean deleteApplication;
    private boolean deleteOrdersOfAdmission;
    private boolean deleteCompetitiveGroupItems;
    private boolean deleteEntranceTestResults;
    private boolean deleteApplicationCommonBenefits;

    private List<EnrollmentCampaign> enrollmentCampaigns = null;
    private String userName;
    private String userPassword;

    private List<Integer> regNumbers = new ArrayList<>();
    private List<Integer> excludeRegNumbers = new ArrayList<>();
    private boolean notRF = false;
    private boolean benefitRequest = false;

    private boolean appDateWithoutTime = false;

    public DaemonParamsDeletePkg(
            String userName
            , String userPassword
            , List<Integer> regNumbers
            , List<Integer> excludeRegNumbers
            , boolean notRF
            , boolean benefitRequest
            , List<EnrollmentCampaign> enrollmentCampaigns
            , boolean deleteApplication
            , boolean deleteOrdersOfAdmission
            , boolean deleteCompetitiveGroupItems
            , boolean deleteEntranceTestResults
            , boolean deleteApplicationCommonBenefits
            , boolean appDateWithoutTime
    )
    {
        this.appDateWithoutTime = appDateWithoutTime;

        this.userName = userName;
        this.userPassword = userPassword;
        this.enrollmentCampaigns = enrollmentCampaigns;
        this.notRF = notRF;
        this.benefitRequest = benefitRequest;
        this.regNumbers = regNumbers;
        this.excludeRegNumbers = excludeRegNumbers;

        this.deleteApplication = deleteApplication;
        this.deleteOrdersOfAdmission = deleteOrdersOfAdmission;
        this.deleteCompetitiveGroupItems = deleteCompetitiveGroupItems;
        this.deleteEntranceTestResults = deleteEntranceTestResults;
        this.deleteApplicationCommonBenefits = deleteApplicationCommonBenefits;
    }

    public boolean isDeleteApplication() {
        return deleteApplication;
    }

    public void setDeleteApplication(boolean deleteApplication) {
        this.deleteApplication = deleteApplication;
    }

    public boolean isDeleteOrdersOfAdmission() {
        return deleteOrdersOfAdmission;
    }

    public void setDeleteOrdersOfAdmission(boolean deleteOrdersOfAdmission) {
        this.deleteOrdersOfAdmission = deleteOrdersOfAdmission;
    }

    public boolean isDeleteCompetitiveGroupItems() {
        return deleteCompetitiveGroupItems;
    }

    public void setDeleteCompetitiveGroupItems(boolean deleteCompetitiveGroupItems) {
        this.deleteCompetitiveGroupItems = deleteCompetitiveGroupItems;
    }

    public boolean isDeleteEntranceTestResults() {
        return deleteEntranceTestResults;
    }

    public void setDeleteEntranceTestResults(boolean deleteEntranceTestResults) {
        this.deleteEntranceTestResults = deleteEntranceTestResults;
    }

    public boolean isDeleteApplicationCommonBenefits() {
        return deleteApplicationCommonBenefits;
    }

    public void setDeleteApplicationCommonBenefits(
            boolean deleteApplicationCommonBenefits)
    {
        this.deleteApplicationCommonBenefits = deleteApplicationCommonBenefits;
    }

    public List<EnrollmentCampaign> getEnrollmentCampaigns() {
        return enrollmentCampaigns;
    }

    public void setEnrollmentCampaigns(List<EnrollmentCampaign> enrollmentCampaigns) {
        this.enrollmentCampaigns = enrollmentCampaigns;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public List<Integer> getRegNumbers() {
        return regNumbers;
    }

    public void setRegNumbers(List<Integer> regNumbers) {
        this.regNumbers = regNumbers;
    }

    public List<Integer> getExcludeRegNumbers() {
        return excludeRegNumbers;
    }

    public void setExcludeRegNumbers(List<Integer> excludeRegNumbers) {
        this.excludeRegNumbers = excludeRegNumbers;
    }

    public boolean isNotRF() {
        return notRF;
    }

    public void setNotRF(boolean notRF) {
        this.notRF = notRF;
    }

    public boolean isBenefitRequest() {
        return benefitRequest;
    }

    public void setBenefitRequest(boolean benefitRequest) {
        this.benefitRequest = benefitRequest;
    }

    public void setAppDateWithoutTime(boolean appDateWithoutTime) {
        this.appDateWithoutTime = appDateWithoutTime;
    }

    public boolean isAppDateWithoutTime() {
        return appDateWithoutTime;
    }

}
