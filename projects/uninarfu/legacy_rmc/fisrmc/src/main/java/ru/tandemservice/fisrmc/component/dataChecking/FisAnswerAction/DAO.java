package ru.tandemservice.fisrmc.component.dataChecking.FisAnswerAction;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.fisrmc.entity.FisAnswerAction;
import ru.tandemservice.fisrmc.entity.catalog.FisAnswerType;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.Date;
import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        model.setFisAnswerTypeModel(new LazySimpleSelectModel<>(FisAnswerType.class).setSortProperty("title"));
    }

    @Override
    public void prepareListDataSource(Model model) {
        DynamicListDataSource<FisAnswerAction> dataSource = model.getDataSource();

        DQLSelectBuilder builder = createBuilder(model);

        DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(FisAnswerAction.class, "faa");
        orderRegistry.applyOrder(builder, model.getDataSource().getEntityOrder());

        UniBaseUtils.createPage(dataSource, builder, getSession());
    }

    protected DQLSelectBuilder createBuilder(Model model) {
        IDataSettings settings = model.getSettings();
        Date fromDate = settings.get("fromDate");
        Date toDate = settings.get("toDate");
        List<FisAnswerType> fisAnswerTypeFilter = settings.get("fisAnswerTypeFilter");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FisAnswerAction.class, "faa");

        FilterUtils.applySelectFilter(builder, "faa", FisAnswerAction.fisAnswerTofisPackages().fisAnswerType().s(), fisAnswerTypeFilter);

        if (fromDate != null)
            builder.where(DQLExpressions.ge(
                    DQLExpressions.property(FisAnswerAction.date().fromAlias("faa")),
                    DQLExpressions.valueDate(fromDate)
            ));
        if (toDate != null)
            builder.where(DQLExpressions.le(
                    DQLExpressions.property(FisAnswerAction.date().fromAlias("faa")),
                    DQLExpressions.valueDate(toDate)
            ));
        return builder;
    }
}
