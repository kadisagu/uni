package ru.tandemservice.fisrmc.dao.packApplications;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.tandemservice.fisrmc.dao.PackUnpackToFis;
import ru.tandemservice.fisrmc.dao.SpravDao;
import ru.tandemservice.fisrmc.dao.ToolsDao;
import ru.tandemservice.fisrmc.entity.catalog.Sexfis;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.entity.entrant.Entrant;

public class EntrantPack extends UniBaseDao implements IEntrantPack {
    private static IEntrantPack _IEntrantPack = null;

    public static IEntrantPack Instanse() {
        if (_IEntrantPack == null)
            _IEntrantPack = (IEntrantPack) ApplicationRuntime
                    .getBean("entrantPack");
        return _IEntrantPack;
    }

    @SuppressWarnings("unused")
    @Override
    public Element packEntrant(Document doc, Entrant entrant) {
        Element result = doc.createElement("Entrant");
        Person person = entrant.getPerson();
        IdentityCard identityCard = person.getIdentityCard();

        // uid
        ToolsDao.PackUid(doc, result, Long.toString(entrant.getId()));

        String firstName = StringUtils.defaultString(identityCard
                                                             .getFirstName());

        String middleName = StringUtils.defaultString(identityCard
                                                              .getMiddleName());

        String lastName = StringUtils.defaultString(identityCard.getLastName());

        if (PackUnpackToFis.DAEMON_PARAMS.isAbfuscateData()) {
            lastName = "Иванов2 " + Long.toString(entrant.getId());
            firstName = "Иван2";
            middleName = "Иванович2";

        }

        Sexfis sexfis = SpravDao.Instanse().getSexFis(identityCard.getSex());
        String genderId = StringUtils.defaultString(sexfis.getCode());

        String customInformation = StringUtils.defaultString(entrant
                                                                     .getAdditionalInfo());
        String snils = StringUtils.defaultString(person.getSnilsNumber());
        ToolsDao.PackToElement(doc, result, "FirstName", firstName);

        // поле не обязательное
        if (middleName != null && middleName.length() > 0)
            ToolsDao.PackToElement(doc, result, "MiddleName", middleName);
        ToolsDao.PackToElement(doc, result, "LastName", lastName);

        ToolsDao.PackToElement(doc, result, "GenderID", genderId);

        ToolsDao.PackToElement(doc, result, "CustomInformation",
                               customInformation);

        // отключаю снилс
        // <TODO> в задаче http://80.246.218.229/redmine/issues/3136
        // дано описание ситуации
        snils = null;
        if (snils != null && snils.length() > 0)
            ToolsDao.PackToElement(doc, result, "Snils", snils);

        return result;
    }
}