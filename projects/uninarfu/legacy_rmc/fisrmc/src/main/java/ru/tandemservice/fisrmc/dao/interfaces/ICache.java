package ru.tandemservice.fisrmc.dao.interfaces;

/**
 * сброс /создание кеша
 *
 * @author vch
 */
public interface ICache {
    public void makeCache();

    public void clearCache();
}
