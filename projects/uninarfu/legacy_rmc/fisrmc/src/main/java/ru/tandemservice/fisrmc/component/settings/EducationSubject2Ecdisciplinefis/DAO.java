package ru.tandemservice.fisrmc.component.settings.EducationSubject2Ecdisciplinefis;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.fisrmc.entity.EducationSubject2Ecdisciplinefis;
import ru.tandemservice.fisrmc.entity.catalog.Ecdisciplinefis;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.catalog.EducationSubject;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DAO extends UniDao<Model> implements IDAO {


    @Override
    public void prepare(Model model)
    {
        model.setEcdisciplinefisList(getCatalogItemList(Ecdisciplinefis.class));
        model.setEducationSubjectsList(getList(EducationSubject.class));
    }

    public void prepareListDataSource(Model model)
    {
        prepareListData(model);

        List disciplines = model.getEducationSubjectsList();
        DynamicListDataSource dataSource = model.getDataSource();
        dataSource.setCountRow(disciplines.size());
        UniBaseUtils.createPage(dataSource, disciplines);
    }

    @SuppressWarnings("unchecked")
    private void prepareListData(Model model)
    {
        List<EducationSubject2Ecdisciplinefis> disciplines = getList(EducationSubject2Ecdisciplinefis.class, EducationSubject2Ecdisciplinefis.educationSubject().title().s());

        model.setDisciplines(disciplines);

        ((BlockColumn) model.getDataSource().getColumn("ecdisciplinefis")).setValueMap(getDisciplineId2ExamSubjectMap(disciplines));
    }

    @SuppressWarnings("unchecked")
    private Map<Long, Ecdisciplinefis> getDisciplineId2ExamSubjectMap(List<EducationSubject2Ecdisciplinefis> disciplines)
    {


        List<Object[]> list = Collections.emptyList();
        if (!disciplines.isEmpty()) {
            Criteria criteria = getSession().createCriteria(EducationSubject2Ecdisciplinefis.class);
            //criteria.add(Restrictions.in("discipline", disciplines));

            ProjectionList projections = Projections.projectionList();
            projections.add(Projections.property(EducationSubject2Ecdisciplinefis.educationSubject().id().s()));
            projections.add(Projections.property(EducationSubject2Ecdisciplinefis.ecdisciplinefis().s()));
            criteria.setProjection(projections);
            list = criteria.list();
        }
        Map result = new HashMap();
        for (Object[] row : list) {
            result.put((Long) row[0], (Ecdisciplinefis) row[1]);
        }
        return result;
    }

	/*	  private List<Discipline2RealizationWayRelation> getDisciplines(EnrollmentCampaign enrollmentCampaign)
      {
	    Criteria criteria = getSession().createCriteria(Discipline2RealizationWayRelation.class);
	    criteria.add(Restrictions.eq("enrollmentCampaign", enrollmentCampaign));
	    criteria.setFetchMode("educationSubject", FetchMode.JOIN);
	    criteria.setFetchMode("subjectPassWay", FetchMode.JOIN);
	    return criteria.list();
	  }*/

    public void update(Model model)
    {
        DynamicListDataSource dataSource = model.getDataSource();
        Map disciplineId2StateExamSubject = ((BlockColumn) dataSource.getColumn("ecdisciplinefis")).getValueMap();
        List<EducationSubject> educationSubjects;
        try {
            educationSubjects = dataSource.getEntityList();
        }
        catch (Throwable e) {
            throw new RuntimeException(e);
        }

        Session session = getSession();

        for (EducationSubject subject : educationSubjects) {

            //session.update(discipline);

            Ecdisciplinefis disEcdisciplinefis = (Ecdisciplinefis) disciplineId2StateExamSubject.get(subject.getId());

            EducationSubject2Ecdisciplinefis relEcdisciplinefis = get(EducationSubject2Ecdisciplinefis.class, EducationSubject2Ecdisciplinefis.educationSubject(), subject);

            if (disEcdisciplinefis != null) {
                if (relEcdisciplinefis != null) {
                    relEcdisciplinefis.setEcdisciplinefis(disEcdisciplinefis);
                }
                else {
                    relEcdisciplinefis = new EducationSubject2Ecdisciplinefis();
                    relEcdisciplinefis.setEducationSubject(subject);
                    relEcdisciplinefis.setEcdisciplinefis(disEcdisciplinefis);
                }
                session.saveOrUpdate(relEcdisciplinefis);
            }
            else if (relEcdisciplinefis != null) {
                session.delete(relEcdisciplinefis);
            }
        }
    }


}
