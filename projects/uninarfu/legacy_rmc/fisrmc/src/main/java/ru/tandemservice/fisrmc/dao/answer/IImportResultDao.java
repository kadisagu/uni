package ru.tandemservice.fisrmc.dao.answer;

import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.fisrmc.entity.FisAnswerTofisPackages;

public interface IImportResultDao {
    public static final String BEAN_NAME = IImportResultDao.class.getName();

    @Transactional
    public void doAction(FisAnswerTofisPackages answer, boolean testMode) throws Exception;
}
