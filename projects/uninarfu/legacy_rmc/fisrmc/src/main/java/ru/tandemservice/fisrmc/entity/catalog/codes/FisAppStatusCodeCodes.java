package ru.tandemservice.fisrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Статус проверки заявления"
 * Имя сущности : fisAppStatusCode
 * Файл data.xml : catalogs.data.xml
 */
public interface FisAppStatusCodeCodes
{
    /** Константа кода (code) элемента : Проверено, ошибок нет (title) */
    String NO_ERROR = "1";
    /** Константа кода (code) элемента : Есть ошибки (title) */
    String HAS_ERROR = "2";

    Set<String> CODES = ImmutableSet.of(NO_ERROR, HAS_ERROR);
}
