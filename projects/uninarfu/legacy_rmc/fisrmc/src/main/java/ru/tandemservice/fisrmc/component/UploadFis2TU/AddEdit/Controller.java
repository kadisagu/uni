package ru.tandemservice.fisrmc.component.UploadFis2TU.AddEdit;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = component.getModel();
        getDao().prepare(model);
    }

    public void onClickApply(IBusinessComponent component) {
        Model model = component.getModel();
        getDao().update(model);
        deactivate(component);
    }

    public void onClickDownload(IBusinessComponent component) {
        Model model = component.getModel();
        byte[] content = model.getEntity().getXmlPackage().getContent();
        if (null == content)
            throw new ApplicationException("Запрашиваемый файл пуст.");
        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(content, model.getEntity().getXmlPackage().getFilename());
        activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new UniMap().add("id", id)));
    }

}
