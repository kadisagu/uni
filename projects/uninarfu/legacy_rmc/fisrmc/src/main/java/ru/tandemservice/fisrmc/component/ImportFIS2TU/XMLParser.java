package ru.tandemservice.fisrmc.component.ImportFIS2TU;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class XMLParser {

    public static List<ItemData> parse(InputStream stream) throws Exception {

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setValidating(false);
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(stream);

        List<ItemData> list = new ArrayList<ItemData>();

        NodeList nod = doc.getElementsByTagName("Code");
        Node cod = nod.item(0);
        String code = cod.getTextContent().trim();

        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("1", "ecdisciplinefis");
        hashMap.put("2", "eceducationlevelfis");
        hashMap.put("3", "ecolimpiadlevelfis");
        hashMap.put("4", "ecrequestfis");
        hashMap.put("5", "sexfis");
        hashMap.put("6", "makrsoursefis");
        hashMap.put("7", "countryfis");
        hashMap.put("10", "educationdirectionfis");
        hashMap.put("11", "entrancedisciplinetypefis");
        hashMap.put("12", "requeststatefis");
        hashMap.put("13", "documentstatefis");
        hashMap.put("14", "developformfis");
        hashMap.put("15", "compensationtypefis");
        hashMap.put("17", "errorcodesfis");
        hashMap.put("18", "olimpiaddiplomatypefis");
        hashMap.put("19", "olimpiatfis");
        hashMap.put("22", "identitycardtypefis");
        hashMap.put("23", "disabledtypefis");
        hashMap.put("30", "competitionkindfis");
        hashMap.put("31", "entrantdocumenttypefis");
        hashMap.put("33", "examdocumenttypefis");
        hashMap.put("34", "enrolmentcomnainfis");

        String entityName = hashMap.get(code);

        NodeList node = doc.getElementsByTagName("DictionaryItem");

        // отдельно по элементам справочника DictionaryItem
        for (int i = 0; i < node.getLength(); i++) {
            HashMap<String, Object> map = new HashMap<String, Object>();
            Element element = (Element) node.item(i);
            NodeList nodeList = element.getChildNodes();

            // внутри элемента DictionaryItem
            for (int j = 0; j < nodeList.getLength(); j++) {
                Node nodeDI = nodeList.item(j);
                String nodeName = nodeDI.getNodeName().trim();

                if (nodeName.equals("#text"))
                    continue;

                Map<String, Object> subNode = new HashMap<>();

                parseNode(subNode, nodeDI);

                if (subNode.keySet().size() == 1) {
                    String key = subNode.keySet().iterator().next();
                    map.put(nodeName, subNode.get(key));
                }
                else
                    map.put(nodeName, subNode);
            }

            ItemData data = new ItemData();
            data.entityName = entityName;
            data.properties = map;

            list.add(data);
        }
        return list;
    }

    // рекурсивно парсим
    private static void parseNode
    (
            Map<String, Object> map
            , Node node
    )
    {
        NodeList subNodes = node.getChildNodes();
        if (subNodes.getLength() > 1) {
            for (int k = 1; k < subNodes.getLength(); k++) {
                Node subNode2 = subNodes.item(k);

                Map<String, Object> subMap = new HashMap<>();
                String name = node.getNodeName().trim();

                if (name.equals("#text"))
                    continue;

                List<Object> lst = null;
                if (map.get(name) == null) {
                    lst = new ArrayList<>();
                    map.put(name, lst);
                }
                else
                    lst = (List<Object>) map.get(name);

                lst.add(subMap);

                parseNode(subMap, subNode2);
            }
        }
        else {
            // дочерних элементов нет, обычное свойство
            String nName = node.getNodeName().trim();
            if (!nName.equals("#text"))
                map.put(node.getNodeName().trim(), node.getTextContent().trim());
        }
    }

    public static class ItemData {
        private String entityName;
        private Map<String, Object> properties = new HashMap<String, Object>();

        public String getEntityName()
        {
            return entityName;
        }

        public void setEntityName(String entityName)
        {
            this.entityName = entityName;
        }

        public Map<String, Object> getProperties()
        {
            return properties;
        }

        public void setProperties(Map<String, Object> properties)
        {
            this.properties = properties;
        }
    }
}
