package ru.tandemservice.fisrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_fisrmc_1x0x0_25to26 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.5.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.5.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность eduProgramQualificationToFis

        // создано обязательное свойство code
        {
            // создать колонку
            tool.createColumn("eduprogramqualificationtofis_t", new DBColumn("code_p", DBType.createVarchar(255)));

            // задать значение по умолчанию
            java.lang.String defaultCode = "0";
            tool.executeUpdate("update eduprogramqualificationtofis_t set code_p=? where code_p is null", defaultCode);

            // сделать колонку NOT NULL
            tool.setColumnNullable("eduprogramqualificationtofis_t", "code_p", false);

        }

        // создано свойство title
        {
            // создать колонку
            tool.createColumn("eduprogramqualificationtofis_t", new DBColumn("title_p", DBType.createVarchar(1200)));

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность qualificationToFis

        // создано обязательное свойство code
        {
            // создать колонку
            tool.createColumn("qualificationtofis_t", new DBColumn("code_p", DBType.createVarchar(255)));
            // задать значение по умолчанию
            java.lang.String defaultCode = "0";
            tool.executeUpdate("update qualificationtofis_t set code_p=? where code_p is null", defaultCode);

            // сделать колонку NOT NULL
            tool.setColumnNullable("qualificationtofis_t", "code_p", false);

        }

        // создано свойство title
        {
            // создать колонку
            tool.createColumn("qualificationtofis_t", new DBColumn("title_p", DBType.createVarchar(1200)));

        }


    }
}