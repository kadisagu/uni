package ru.tandemservice.fisrmc.component.UploadFis2TU.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.fisrmc.entity.FisExportFiles;

public class Model {

    private IDataSettings settings;
    private DynamicListDataSource<FisExportFiles> dataSource;

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public DynamicListDataSource<FisExportFiles> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<FisExportFiles> dataSourse) {
        this.dataSource = dataSourse;
    }


}
