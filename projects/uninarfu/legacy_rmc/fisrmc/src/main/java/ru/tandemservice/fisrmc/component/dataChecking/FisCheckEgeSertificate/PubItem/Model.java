package ru.tandemservice.fisrmc.component.dataChecking.FisCheckEgeSertificate.PubItem;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import ru.tandemservice.fisrmc.entity.FisCheckEgeSertificate;


@State(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entityId"))
public class Model {

    private Long entityId;
    private FisCheckEgeSertificate checkEgeSertificate;
    private String requestNambers;

    public String getRequestNambers() {
        return requestNambers;
    }

    public void setRequestNambers(String requestNambers) {
        this.requestNambers = requestNambers;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public FisCheckEgeSertificate getCheckEgeSertificate() {
        return checkEgeSertificate;
    }

    public void setCheckEgeSertificate(FisCheckEgeSertificate checkEgeSertificate) {
        this.checkEgeSertificate = checkEgeSertificate;
    }

}
