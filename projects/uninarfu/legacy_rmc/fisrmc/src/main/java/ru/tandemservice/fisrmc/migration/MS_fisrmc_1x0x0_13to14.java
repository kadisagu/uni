package ru.tandemservice.fisrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_fisrmc_1x0x0_13to14 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.10"),
                        new ScriptDependency("org.tandemframework.shared", "1.1.3"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.1.3")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность fisAppStatusCode

        // создано свойство priority
        {
            // создать колонку
            tool.createColumn("fisappstatuscode_t", new DBColumn("priority_p", DBType.INTEGER));

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fisEgeStatusCode

        // создано свойство priority
        {
            // создать колонку
            tool.createColumn("fisegestatuscode_t", new DBColumn("priority_p", DBType.INTEGER));

        }


    }
}