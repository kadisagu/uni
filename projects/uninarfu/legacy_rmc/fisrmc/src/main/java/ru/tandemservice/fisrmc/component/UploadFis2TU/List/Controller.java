package ru.tandemservice.fisrmc.component.UploadFis2TU.List;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.fisrmc.entity.FisExportFiles;
import ru.tandemservice.unibase.UniBaseUtils;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = component.getModel();
        model.setSettings(component.getSettings());

        prepareListDateSource(component);
    }

    private void prepareListDateSource(IBusinessComponent component) {
        Model model = component.getModel();
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<FisExportFiles> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Дата загрузки", FisExportFiles.date(), DateFormatter.DATE_FORMATTER_WITH_TIME).setClickable(true));
        dataSource.addColumn(new SimpleColumn("Описание", FisExportFiles.description()).setClickable(false));
        //dataSource.addColumn(new SimpleColumn("Файл", FisExportFiles.xmlPackage().filename()).setClickable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete"));

        model.setDataSource(dataSource);
    }

    public void onClickAdd(IBusinessComponent component) {
        activate(
                component,
                new ComponentActivator("ru.tandemservice.fisrmc.component.UploadFis2TU.AddEdit", new ParametersMap().add("publisherId", null))
        );
    }

    public void onClickEdit(IBusinessComponent component) {
        Long id = component.getListenerParameter();
        activate(
                component,
                new ComponentActivator("ru.tandemservice.fisrmc.component.UploadFis2TU.AddEdit", new ParametersMap().add("publisherId", id))
        );
    }

    public void onClickDelete(IBusinessComponent component) {
        Long id = component.getListenerParameter();
        getDao().delete(id);
    }
}
