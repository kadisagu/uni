package ru.tandemservice.fisrmc.dao.packApplications;

import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.PairKey;
import org.tandemframework.shared.person.base.entity.Person;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.tandemservice.fisrmc.dao.*;
import ru.tandemservice.fisrmc.dao.packApplicationCommonBenefit.DocumentReason;
import ru.tandemservice.fisrmc.dao.packApplicationCommonBenefit.IDocumentReason;
import ru.tandemservice.fisrmc.entity.GroupEsForFis;
import ru.tandemservice.fisrmc.entity.catalog.Ecdisciplinefis;
import ru.tandemservice.fisrmc.entity.catalog.Entrancedisciplinetypefis;
import ru.tandemservice.fisrmc.entity.catalog.Makrsoursefis;
import ru.tandemservice.fisrmc.entity.catalog.Olimpiatfis;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.entity.catalog.EducationSubject;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineType;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.entity.settings.Group2DisciplineRelation;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class PackEntranceTestResult extends UniBaseDao implements IPackEntranceTestResult {

    private static IPackEntranceTestResult _IPackEntranceTestResult = null;

    public static IPackEntranceTestResult Instanse()
    {
        if (_IPackEntranceTestResult == null)
            _IPackEntranceTestResult = (IPackEntranceTestResult) ApplicationRuntime.getBean("packEntranceTestResult");
        return _IPackEntranceTestResult;
    }

    /**
     * карта для расчета средних оценок
     */
    Map<Long, List<Integer>> mapAvgMark = new HashMap<Long, List<Integer>>();


    Map<Long, List<ChosenEntranceDiscipline>> mapCD = new HashMap<Long, List<ChosenEntranceDiscipline>>();

    Map<String, Entrancedisciplinetypefis> mapEntrancedisciplinetypefis = new HashMap<String, Entrancedisciplinetypefis>();

    public void makeCache()
    {

        DQLSelectBuilder dqlEntrantRequest = Applications.Instanse().getEntrantRequestDQLList
                (
                        PackUnpackToFis.DAEMON_PARAMS.getEnrollmentCampaigns()
                        , PackUnpackToFis.DAEMON_PARAMS.isNotRf()
                        , PackUnpackToFis.DAEMON_PARAMS.isBenefitRequest()
                        , PackUnpackToFis.DAEMON_PARAMS.isOrdersOfAdmission()
                        , PackUnpackToFis.DAEMON_PARAMS.getRegNumbers()
                        , PackUnpackToFis.DAEMON_PARAMS.getExcludeRegNumbers()

                        , PackUnpackToFis.DAEMON_PARAMS.getDateRequestFrom()
                        , PackUnpackToFis.DAEMON_PARAMS.getDateRequestTo()
                        , PackUnpackToFis.DAEMON_PARAMS.isWhithoutSendedInFis()
                        , PackUnpackToFis.DAEMON_PARAMS.isWhithoutEntrantInOrder()

                        , false
                        , PackUnpackToFis.DAEMON_PARAMS.isTestMode()
                        , PackUnpackToFis.TESTMODE_CompetitiveGroup_List
                );

        DQLSelectBuilder selectBuilder = new DQLSelectBuilder()
                .fromEntity(ChosenEntranceDiscipline.class, "ced")
                .where(DQLExpressions.in(
                               DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().entrantRequest().id().fromAlias("ced"))
                               , dqlEntrantRequest.getQuery())
                );

		/*
        .where(DQLExpressions.in(
				DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().enrollmentDirection().id().fromAlias("ced"))
				,lstEnrollmentDirections)
				);
		*/

        List<ChosenEntranceDiscipline> disciplines = selectBuilder.createStatement(getSession()).list();
        for (ChosenEntranceDiscipline discipline : disciplines) {
            Long requestId = discipline.getChosenEnrollmentDirection().getEntrantRequest().getId();

            List<ChosenEntranceDiscipline> lst = null;
            if (mapCD.containsKey(requestId))
                lst = mapCD.get(requestId);
            else {
                lst = new ArrayList<ChosenEntranceDiscipline>();
                mapCD.put(requestId, lst);
            }
            if (!lst.contains(discipline))
                lst.add(discipline);
        }


        _fillMapAvgMark(dqlEntrantRequest);

    }

    /**
     * формируем карту для расчета средней оценки
     *
     * @param dqlEntrantRequest
     */
    private void _fillMapAvgMark(DQLSelectBuilder dqlEntrantRequest)
    {
        DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(EntrantRequest.class, "er")
                .column(property(EntrantRequest.entrant().person().fromAlias("er")))
                .where(DQLExpressions.in(
                               DQLExpressions.property(EntrantRequest.id().fromAlias("er"))
                               , dqlEntrantRequest.getQuery())
                );

        // основной документ об образовании
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(Person.class, "pei")
                .column(property(Person.id().fromAlias("pei")))
                .column(property(Person.personEduInstitution().mark3().fromAlias("pei")))
                .column(property(Person.personEduInstitution().mark4().fromAlias("pei")))
                .column(property(Person.personEduInstitution().mark5().fromAlias("pei")))
                .where(DQLExpressions.in(
                               DQLExpressions.property(Person.id().fromAlias("pei"))
                               , dqlIn.getQuery())
                )
                .where(DQLExpressions.isNotNull(DQLExpressions.property(Person.personEduInstitution().fromAlias("pei"))));


        List<Object[]> lst = dql.createStatement(getSession()).list();

        for (Object[] objs : lst) {
            Long id = (Long) objs[0];

            Integer mark3 = 0;
            Integer mark4 = 0;
            Integer mark5 = 0;

            if (objs[1] != null)
                mark3 = (Integer) objs[1];

            if (objs[2] != null)
                mark4 = (Integer) objs[2];

            if (objs[3] != null)
                mark5 = (Integer) objs[3];

            List<Integer> set = new ArrayList<Integer>();
            set.add(mark3);
            set.add(mark4);
            set.add(mark5);

            mapAvgMark.put(id, set);
        }
    }

    public void clearCache()
    {
        mapCD = new HashMap<Long, List<ChosenEntranceDiscipline>>();
        mapEntrancedisciplinetypefis = new HashMap<String, Entrancedisciplinetypefis>();
        mapAvgMark = new HashMap<Long, List<Integer>>();
    }


    @Override
    public Element packEntranceTestResult(
            Document doc
            , EntrantRequest entrantRequest
            , List<EnrollmentDirection> lstEnrollmentDirections
            , List<XMLCompetitiveGroup> xmlCompetitiveGroupList
    )
    {
        HashMap<String, XMLEntranceTestResult> map = new HashMap<String, XMLEntranceTestResult>();
        // lstEnrollmentDirections - выбранные направления для приема
        // xmlCompetitiveGroupList - те конкурсные группы, в которые попали выбранные направления для приема
        // выбираем все вступительные испытания, по выбранным направлениям для прием
        Long id = entrantRequest.getId();
        List<ChosenEntranceDiscipline> disciplines = mapCD.get(id);

        // нет вступительных испытаний
        if (disciplines == null)
            return null;
        disciplines = _filtrate(disciplines, lstEnrollmentDirections);

        // нет вступительных испытаний
        if (disciplines == null)
            return null;

        // добавить сортировку по ID
        // для того, чтобы избегать многократного перетирания абитуриента в ФИС (это не обязательно)
        Collections.sort(
                disciplines,
                new EntityComparator<ChosenEntranceDiscipline>(new EntityOrder(ChosenEntranceDiscipline.id().s()))
        );


        // в цикле по дисциплинам
        for (ChosenEntranceDiscipline ced : disciplines) {
            // ced.getChosenEnrollmentDirection();

            // балл по дисциплине
            int finalMark = 0;
            if (ced.getFinalMark() != null)
                finalMark = ced.getFinalMark().intValue();

            // если оценки нет - пропускаем
            if (finalMark == 0)
                continue;

            // источник оценки
            int resultSourseTypeId = ced.getFinalMarkSource();
            // источник оценки по справочнику ФИС
            Makrsoursefis makrsoursefis = SpravDao.Instanse().getMakrsoursefis(resultSourseTypeId);

            if (makrsoursefis == null)
                throw new ApplicationException("Для заявления " + entrantRequest.getStringNumber() + " не удалось определить источник получения оценки");

            // дисциплина вступительного испытания
            Discipline2RealizationWayRelation wayRelation = ced.getEnrollmentCampaignDiscipline();

            EducationSubject educationSubject = wayRelation.getEducationSubject();

            EnrollmentDirection ed = ced.getChosenEnrollmentDirection().getEnrollmentDirection();

            // группы дисциалин
            // ищем с учетом направления подготовки
            ArrayList<EnrollmentDirection> alEd = new ArrayList<EnrollmentDirection>();
            alEd.add(ed);

            GroupEsForFis groupDiscipline = SpravDao.Instanse().getSingleGroupEsForFis(educationSubject, alEd);


            EntranceDisciplineType disciplineType = null;

            Ecdisciplinefis ecdisciplinefis = null;
            Entrancedisciplinetypefis entrancedisciplinetypefis = null;
            if (groupDiscipline == null) {
                ecdisciplinefis = SpravDao.Instanse().getEcdisciplinefis(educationSubject);

                // поиск типа вступительного испытания
                StudentCategory studentCategory = ced.getChosenEnrollmentDirection().getStudentCategory();
                // тип вступительного испытания
                entrancedisciplinetypefis = _getEntrancedisciplinetypefis(entrantRequest, ed, studentCategory, wayRelation);

            }
            else {
                ecdisciplinefis = SpravDao.Instanse().getEcdisciplinefisForGroup(groupDiscipline);

                // disciplineType всегда code=1 "Обычное"
                disciplineType = (EntranceDisciplineType) SpravDao.Instanse().getEntityByCode(EntranceDisciplineType.class, "1");
                //Тип вступительных испытаний
                entrancedisciplinetypefis = SpravDao.Instanse().getEntrancedisciplinetypefis(disciplineType);
                // если дисциплина из группы дисциплин, значит источник оценки переопределяем
                // берем код=2, "Вступительное испытание ОУ"
                makrsoursefis = (Makrsoursefis) SpravDao.Instanse().getEntityByCode(Makrsoursefis.class, "2");
            }

            // получим направление для приема, по направлению для приема определим
            // конкурсные группы (список конкурсных групп)

            List<XMLCompetitiveGroup> competitiveGroupList = getCompetitiveGroupsList(ed, xmlCompetitiveGroupList);

            for (XMLCompetitiveGroup xcg : competitiveGroupList) {
                if (entrancedisciplinetypefis == null) {
                    // варнинг высыпан в методе _getEntrancedisciplinetypefis(entrantRequest, ed, studentCategory, wayRelation);
                    // в ижевске такая ситуация возникает, если некооректо назначены вступительные испытания
                    continue;
                }

                // формируем МАР
                XMLEntranceTestResult testItem = new XMLEntranceTestResult
                        (
                                Long.toString(ced.getId())
                                , finalMark
                                , makrsoursefis
                                , ecdisciplinefis
                                , entrancedisciplinetypefis
                                , xcg
                        );

                String key = testItem.getKey();
                // uid нужно расчитать
                if (!map.containsKey(key))
                    map.put(key, testItem);
                else
                    testItem = map.get(key);


                // в зависимости от источника оценки - готовим коллекцию
                // если олимпиада
                if (makrsoursefis.getCode().equals("3")) {
                    testItem.AddChosenEntranceDiscipline(ced);
                }

                if (groupDiscipline != null) {
                    // добавим ключ, пара значение
                    // вступительное испытание + оценка
                    PairKey<EducationSubject, Integer> pairKey = new PairKey<EducationSubject, Integer>(educationSubject, finalMark);
                    testItem.addEducationSubjectMarkInfo(pairKey);

                    // данные по средней оценке аттестата
                    // желательно проверить флаг - выгружать средний балл аттестата

                    Long personId = entrantRequest.getEntrant().getPerson().getId();
                    if (mapAvgMark.containsKey(personId)) {
                        List<Integer> set = mapAvgMark.get(personId);
                        testItem.setAvgPersonEduInstitutionMark(set);
                    }
                }
            }
        }

        // запаковка в xml
        Element elemEntranceTestResults = null;

        Set<String> keys = map.keySet();
        if (!keys.isEmpty()) {
            elemEntranceTestResults = doc.createElement("EntranceTestResults");

            // можно паковать
            for (String key : keys) {
                Element element = doc.createElement("EntranceTestResult");
                elemEntranceTestResults.appendChild(element);

                XMLEntranceTestResult testResult = map.get(key);
                // uid
                ToolsDao.PackUid(doc, element, testResult.getUid());

                // ResultValue
                ToolsDao.PackToElement(doc, element, "ResultValue", Integer.toString(testResult.getFinalMark()));


                // ResultSouceTypeID
                ToolsDao.PackToElement(doc, element, "ResultSourceTypeID", testResult.getMakrsoursefis().getCode());
                ToolsDao.PackToElement(doc, element, "EntranceTestTypeID", testResult.getEntrancedisciplinetypefis().getCode());
                ToolsDao.PackToElement(doc, element, "CompetitiveGroupID", testResult.getXmlCompetitiveGroup().getUid());
                // упаковка дисциплины
                Ecdisciplinefis discipline = testResult.getEcdisciplinefis();
                ToolsDao.PackEcdisciplinefis(doc, element, discipline);


                // если основание оценки - олимпиада - упакуем и диплом
                // учасника олимпиады
                if (testResult.getMakrsoursefis().getCode().equals("3")) {
                    Element elementRd = doc.createElement("ResultDocument");
                    Element reasonElement = _packOlimpicDocument(doc, elementRd, entrantRequest, testResult, lstEnrollmentDirections);

                    boolean has = false;
                    if (reasonElement != null) {
                        has = true;
                        elementRd.appendChild(reasonElement);
                    }

                    if (has)
                        element.appendChild(elementRd);
                }


                // PackEgeDocuments
                // свидетельство ЕГЭ
                if (testResult.getMakrsoursefis().getCode().equals("1")) {
                    String egeId = PackEgeDocuments.Instanse().getEgeSertificateId(entrantRequest.getEntrant()
                            , discipline.getCode()
                            , Integer.toString(testResult.getFinalMark())
                    );

                    if (egeId != null) {
                        // источник оценки
                        Element elementRd = doc.createElement("ResultDocument");
                        ToolsDao.PackToElement(doc, elementRd, "EgeDocumentID", egeId);
                        element.appendChild(elementRd);
                    }
                }
            }
        }

        return elemEntranceTestResults;
    }

    // зачтение диплома олимпиады
    private Element _packOlimpicDocument(
            Document doc
            , Element elementRd
            , EntrantRequest entrantRequest
            , XMLEntranceTestResult testResult
            , List<EnrollmentDirection> lstEnrollmentDirections
    )
    {

        List<OlympiadDiploma> lstDiplomas = _getOlimpiadList(entrantRequest, lstEnrollmentDirections, testResult.getChosenEntranceDisciplineList());

        if (lstDiplomas.isEmpty()) {
            String msg = "По заявлению  " + entrantRequest.getStringNumber() + " не найдена олимпиада для формирования источника оценки";
            if (PackUnpackToFis.DAEMON_PARAMS.isSuspendErrorDoc()) {
                PackUnpackToFis.AddPkgError(msg);
                return null;
            }
            else
                throw new ApplicationException(msg);
        }
        else {
            // первую по списку
            OlympiadDiploma diploma = lstDiplomas.get(0);

            // оригиналы в целом на заявление
            boolean isOrigin = Applications.Instanse().isOriginalDocuments(entrantRequest);
            Olimpiatfis olimpiatfis = SpravDao.Instanse().getOlimpiadFisByOlimpiadTu(diploma);
            if (olimpiatfis == null) {
                String msg = "Диплом олимпиады не сопоставлен со справочником ФИС " + entrantRequest.getStringNumber();
                if (PackUnpackToFis.DAEMON_PARAMS.isSuspendErrorDoc()) {
                    PackUnpackToFis.AddPkgError(msg);
                    return null;
                }
                else
                    throw new ApplicationException(msg);
            }


            IDocumentReason iDaoPack = DocumentReason.Instanse();
            // всеросийская
            if (diploma.getDiplomaType() != null && diploma.getDiplomaType().getCode().equals("1")) {
                return iDaoPack.packOlympicTotalDocument(
                        doc
                        , entrantRequest
                        , isOrigin
                        , diploma
                        , olimpiatfis);
            }
            else {
                return iDaoPack.packOlympicDocument(
                        doc
                        , entrantRequest
                        , isOrigin
                        , diploma
                        , olimpiatfis);
            }
        }
    }

    private List<OlympiadDiploma> _getOlimpiadList
            (
                    EntrantRequest entrantRequest
                    , List<EnrollmentDirection> lstEnrollmentDirections
                    , List<ChosenEntranceDiscipline> lstChosenEntranceDiscipline)
    {
        DQLSelectBuilder dqlIn = null;

        // у нас зачтение олимпиады
        if (lstChosenEntranceDiscipline != null) {
            dqlIn = new DQLSelectBuilder()
                    .fromEntity(ChosenEntranceDiscipline.class, "cd")
                    .column(property(ChosenEntranceDiscipline.enrollmentCampaignDiscipline().fromAlias("cd")))
                    .where(DQLExpressions.in(DQLExpressions.property(
                            ChosenEntranceDiscipline.id().fromAlias("cd")), lstChosenEntranceDiscipline));
        }

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(Discipline2OlympiadDiplomaRelation.class, "d2o");

        if (dqlIn != null) {
            dql.where(DQLExpressions.in(DQLExpressions.property(
                    Discipline2OlympiadDiplomaRelation.discipline().id().fromAlias("d2o")), dqlIn.getQuery()));
        }

        dql.where(DQLExpressions.in(DQLExpressions.property(
                Discipline2OlympiadDiplomaRelation.enrollmentDirection().id().fromAlias("d2o")), lstEnrollmentDirections))


                // по абитуриенту
                .where(DQLExpressions.eq(DQLExpressions.property(
                        Discipline2OlympiadDiplomaRelation.diploma().entrant().fromAlias("d2o")), value(entrantRequest.getEntrant())))


                .setPredicate(DQLPredicateType.distinct)
                .column(property(Discipline2OlympiadDiplomaRelation.diploma().fromAlias("d2o")));

        List<OlympiadDiploma> lstDiploma = dql.createStatement(getSession()).list();

        return lstDiploma;
    }

    private Entrancedisciplinetypefis _getEntrancedisciplinetypefis(
            EntrantRequest entrantRequest
            , EnrollmentDirection ed
            , StudentCategory studentCategory
            , Discipline2RealizationWayRelation wayRelation)
    {

        String key = Long.toString(wayRelation.getId()) + Long.toString(ed.getId());

        if (mapEntrancedisciplinetypefis.containsKey(key))
            return mapEntrancedisciplinetypefis.get(key);

        List<EntranceDiscipline> entranceDisciplines = new ArrayList<EntranceDiscipline>();


        List<Group2DisciplineRelation> lstGroup = getList(Group2DisciplineRelation.class, Group2DisciplineRelation.discipline(), wayRelation);
        DQLSelectBuilder dql = null;
        // у нас группа дисциплин
        if (!lstGroup.isEmpty()) {
            dql = new DQLSelectBuilder();
            Group2DisciplineRelation groupDiscipline = lstGroup.get(0);
            dql.fromEntity(EntranceDiscipline.class, "ed");
            dql.where(DQLExpressions.eq(
                    DQLExpressions.property(EntranceDiscipline.discipline().id().fromAlias("ed")), DQLExpressions.value(groupDiscipline.getGroup().getId())));
            // по направлению для приема
            dql.where(DQLExpressions.eq(
                    DQLExpressions.property(EntranceDiscipline.enrollmentDirection().id().fromAlias("ed")), DQLExpressions.value(ed.getId())));
            entranceDisciplines = dql.createStatement(getSession()).list();
        }

        // как показали настройки Ижевск, есть ситуации, когда дисциплина назначена в группу, а используется
        // как обычная дисциплина - автономно
        // даже, если была группа дисциплин, но она пуста ищем все равно
        if (entranceDisciplines.isEmpty()) {
            dql = new DQLSelectBuilder();
            // дисциплина не из группы дисциплин
            dql.fromEntity(EntranceDiscipline.class, "ed");

            dql.where(DQLExpressions.eq(
                    DQLExpressions.property(EntranceDiscipline.discipline().id().fromAlias("ed")), DQLExpressions.value(wayRelation.getId())));

            // по направлению для приема
            dql.where(DQLExpressions.eq(
                    DQLExpressions.property(EntranceDiscipline.enrollmentDirection().id().fromAlias("ed")), DQLExpressions.value(ed.getId())));

            entranceDisciplines = dql.createStatement(getSession()).list();

        }

        // затычка (есть еще дисциплины по выбору)
        if (entranceDisciplines.isEmpty()) {
            dql = new DQLSelectBuilder();
            dql.fromEntity(EntranceDiscipline2SetDisciplineRelation.class, "ed");
            dql.column(DQLExpressions.property(EntranceDiscipline2SetDisciplineRelation.entranceDiscipline().fromAlias("ed")));
            // по setDiscipline
            dql.where(DQLExpressions.eq(
                    DQLExpressions.property(EntranceDiscipline2SetDisciplineRelation.setDiscipline().id().fromAlias("ed")), DQLExpressions.value(wayRelation.getId())));

            // по направлению для приема
            dql.where(DQLExpressions.eq(
                    DQLExpressions.property(EntranceDiscipline2SetDisciplineRelation.entranceDiscipline().enrollmentDirection().id().fromAlias("ed")), DQLExpressions.value(ed.getId())));
            entranceDisciplines = dql.createStatement(getSession()).list();
        }

        Entrancedisciplinetypefis entrancedisciplinetypefis = null;
        EntranceDiscipline entranceiscipline = null;
        if (!entranceDisciplines.isEmpty()) {
            entranceiscipline = null;
            if (entranceDisciplines.size() > 1 && studentCategory != null) {
                // нужен доп фильтр по категории поступающего
                for (EntranceDiscipline ediscipline : entranceDisciplines) {
                    if (entranceiscipline == null
                            && ediscipline.getStudentCategory() != null
                            && ediscipline.getStudentCategory().getId().equals(studentCategory.getId()))
                        entranceiscipline = ediscipline;
                }
            }

            if (entranceiscipline == null)
                entranceiscipline = entranceDisciplines.get(0);

            entrancedisciplinetypefis = SpravDao.Instanse().getEntrancedisciplinetypefis(entranceiscipline.getType());
        }

        // для ижевска
        // если entranceiscipline == null
        // значит можно взводить не ошибку, а просто не паковать такую оценку
        // хотя это ошибка тандема <TODO> пусть тандем и правит
        if (entranceiscipline == null) {
            PackUnpackToFis.AddPkgError("У заявления " + entrantRequest.getStringNumber() + " есть некорректно выбранное вступительное испытание " + wayRelation.getTitle() + " для " + ed.getTitle());
            return null;
        }

        if (entrancedisciplinetypefis == null)
            throw new ApplicationException("Для заявления " + entrantRequest.getStringNumber() + " для итоговой оценки не удалость определить тип вступительного испытания");

        mapEntrancedisciplinetypefis.put(key, entrancedisciplinetypefis);

        return entrancedisciplinetypefis;
    }

    private List<ChosenEntranceDiscipline> _filtrate
            (
                    List<ChosenEntranceDiscipline> disciplines
                    , List<EnrollmentDirection> lstEnrollmentDirections
            )
    {
        List<ChosenEntranceDiscipline> retVal = new ArrayList<ChosenEntranceDiscipline>();

        for (ChosenEntranceDiscipline cd : disciplines) {
            Long idEd = cd.getChosenEnrollmentDirection().getEnrollmentDirection().getId();

            boolean needAdd = false;
            for (EnrollmentDirection ed : lstEnrollmentDirections) {
                if (idEd.equals(ed.getId()))
                    needAdd = true;
            }

            if (needAdd)
                retVal.add(cd);
        }
        return retVal;
    }

    private List<XMLCompetitiveGroup> getCompetitiveGroupsList
            (
                    EnrollmentDirection enrollmentDirection,
                    List<XMLCompetitiveGroup> lstCompetitiveGroup
            )
    {

        List<XMLCompetitiveGroup> retVal = new ArrayList<XMLCompetitiveGroup>();
        for (XMLCompetitiveGroup xcg : lstCompetitiveGroup) {
            List<XMLCompetitiveGroupItem> lst = xcg.getCompetitiveGroupItem();
            for (XMLCompetitiveGroupItem xitem : lst) {
                List<EnrollmentDirection> elist = xitem.getEnrollmentDirectionsList();
                for (EnrollmentDirection ed : elist) {
                    // ищем совпадение
                    if (enrollmentDirection.getId().equals(ed.getId())) {
                        if (!retVal.contains(xcg))
                            retVal.add(xcg);
                    }
                }
            }
        }
        return retVal;
    }

//		
//	public Element __packEntranceTestResultTODO(Document doc, EntrantRequest entrantRequest) {
//
//		/**
//		 * Логика вычисления итоговой оценки: 
//		 * Итоговый рейтинг по каждому выбранному направлению подготовки (специальности) должен вычисляться автоматически, 
//		 * при этом в зачет должен идти максимальный балл по каждой из дисциплин, за исключением баллов по апелляции.
//		 * 
//		 * Если имеем по выбранной дисциплине олимпиаду, которая защитывается по данному направлению подготовки, 
//		 * тогда выставляем максимальный балл по данной дисциплине (берется из сужности "Discipline2RealizationWayRelation.maxMark" как правило это 100)
//		 */
//
//		/**
//		 * Получим список выбранных вступительных испытаний
//		 */
//
//		Element rooElement = doc.createElement("EntranceTestResults");
//
//		DQLSelectBuilder selectBuilder = new DQLSelectBuilder()
//		.fromEntity(ChosenEntranceDiscipline.class, "ced")		
//		.where(DQLExpressions.eq(DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().entrantRequest().fromAlias("ced")),DQLExpressions.value(entrantRequest)));
//
//
//		List<ChosenEntranceDiscipline> disciplines = selectBuilder.createStatement(getSession()).list();
//
//		for(ChosenEntranceDiscipline chosenEntranceDiscipline : disciplines){
//
//			
//			Discipline2RealizationWayRelation discipline2RealizationWayRelation = chosenEntranceDiscipline.getEnrollmentCampaignDiscipline();
//
//			EducationSubject educationSubject = discipline2RealizationWayRelation.getEducationSubject();
//
//
//			DQLSelectBuilder stateExamSubjectB = new DQLSelectBuilder()
//			.column(DQLFunctions.max(DQLExpressions.property(StateExamSubjectMark.mark().fromAlias("sesm"))))
//			.fromEntity(StateExamSubjectMark.class, "sesm")
//
//			.joinEntity("sesm", DQLJoinType.full, ConversionScale.class, "cs", DQLExpressions.eq(DQLExpressions.property(StateExamSubjectMark.subject().id().fromAlias("sesm")), DQLExpressions.property(ConversionScale.subject().fromAlias("cs"))))
//
//			.setPredicate(DQLPredicateType.distinct)
//			.where(DQLExpressions.eq(DQLExpressions.property(StateExamSubjectMark.certificate().accepted().fromAlias("sesm")), DQLExpressions.value(true)))
//			.where(DQLExpressions.eq(DQLExpressions.property(StateExamSubjectMark.certificate().entrant().fromAlias("sesm")),DQLExpressions.value(entrantRequest.getEntrantId())))
//			.where(DQLExpressions.eq(DQLExpressions.property(ConversionScale.discipline().fromAlias("cs")), DQLExpressions.value(discipline2RealizationWayRelation)));
//
//			//Оценка по ЕГЭ
//			Integer stateExamMark = stateExamSubjectB.createStatement(getSession()).uniqueResult();
//
//
//
//			List<Object[]> list = new DQLSelectBuilder()
//			.column(DQLExpressions.property(ExamPassMark.mark().fromAlias("epm")))
//			.column(DQLExpressions.property(ExamPassMarkAppeal.mark().fromAlias("epma")))
//			.column(DQLExpressions.property(ExamPassMark.examPassDiscipline().subjectPassForm().fromAlias("epm")))
//			.column("d2o.id")
//
//
//			.fromEntity(ExamPassMark.class, "epm")
//			/** Зачтение предмета олимпиады **/
//			.joinEntity("epm", DQLJoinType.full, Discipline2OlympiadDiplomaRelation.class, "d2o", DQLExpressions.eq(DQLExpressions.property(Discipline2OlympiadDiplomaRelation.discipline().fromAlias("d2o")),DQLExpressions.value(discipline2RealizationWayRelation)))
//			.joinEntity("epm", DQLJoinType.full, ExamPassMarkAppeal.class, "epma", DQLExpressions.eq(DQLExpressions.property(ExamPassMarkAppeal.examPassMark().fromAlias("epma")), DQLExpressions.property(ExamPassMark.id().fromAlias("epm"))))
//
//			.where(DQLExpressions.eq(DQLExpressions.property(ExamPassMark.examPassDiscipline().enrollmentCampaignDiscipline().fromAlias("epm")), DQLExpressions.value(discipline2RealizationWayRelation)))
//			.where(DQLExpressions.eq(DQLExpressions.property(ExamPassMark.examPassDiscipline().entrantExamList().entrant().fromAlias("epm")), DQLExpressions.value(entrantRequest.getEntrant())))
//			.setPredicate(DQLPredicateType.distinct)
//			.createStatement(getSession()).list();
//
//
//			/** Критерии сопоставления оснований для оценки * MAKRSOURSEFIS_T - subjectpassform_t *
//			1	Свидетельство ЕГЭ = (ЕГЭ - 1, ЕГЭ (вуз) - 5)
//			2	Вступительное испытание ОУ	= (Тестирование - 2, Экзамен - 3, Собеседование - 4)
//			3	Диплом победителя/призера олимпиады = (Наличие олимпиады по данной дициплине)
//			4	Справка ГИА = отсутствует
//			 **/		
//
//			int resultValue = 0;
//			Makrsoursefis makrsoursefis = null;
//			EntranceDisciplineType entranceTestTypeID = null;
//
//			for(Object[] data : list){
//				Object isOlimpPresent = data[3];
//				//Если есть олимпиада
//				if(isOlimpPresent != null){
//					//Если есть олимпиада тогда присвоим макс балл и выйдем из цикла
//					resultValue = discipline2RealizationWayRelation.getMaxMark();
//					makrsoursefis = getCatalogItem(Makrsoursefis.class, "3");
//					break;
//				}
//
//
//				Double mark = (Double) data[0];
//				Double markA = (Double) data[1];
//				SubjectPassForm subjectPassForm = (SubjectPassForm) data[2];		
//				//Если есть аппеляция, она перекрывает основную оценку
//				Double totalSubjectMark = (markA != null ? markA : mark);
//
//				stateExamMark = stateExamMark!= null? stateExamMark : 0;
//				totalSubjectMark = totalSubjectMark!=null ? totalSubjectMark : 0;
//
//
//				if(stateExamMark>totalSubjectMark){
//					//Если макс. балл по вступительному испытанию
//					resultValue = stateExamMark;
//					makrsoursefis = subjectPassForm.getCode().equals(SubjectPassFormCodes.STATE_EXAM_INTERNAL) ? getCatalogItem(Makrsoursefis.class, "1") :  getCatalogItem(Makrsoursefis.class, "2");
//				}
//				else if(totalSubjectMark > 0){
//					//иначе свидетельство ЕГЭ
//					resultValue = totalSubjectMark.intValue();
//					makrsoursefis = getCatalogItem(Makrsoursefis.class, "1");					
//				}
//
//
//			}
//
//			//Если нет оценок пропучтим цикл
//			if(resultValue == 0 || makrsoursefis == null)
//				continue;
//
//
//			/** Определение типа дисциплины **/
//			
//					
//			List<Group2DisciplineRelation> list2 = getList(Group2DisciplineRelation.class,Group2DisciplineRelation.discipline(),discipline2RealizationWayRelation);
//			if(!list2.isEmpty()){
//				
//				
//				EntranceDiscipline entranceDiscipline = get(EntranceDiscipline.class,EntranceDiscipline.id(),list2.get(0).getGroup().getId());
//				if(entranceDiscipline!=null)
//				entranceTestTypeID  = entranceDiscipline.getType();
//			}
//			else{
//				List<EntranceDiscipline> list4 = getList(EntranceDiscipline.class,EntranceDiscipline.discipline(),discipline2RealizationWayRelation);
//				if(!list4.isEmpty())
//				entranceTestTypeID = list4.get(0).getType();
//			}
//
//
//			/**
//			 * Упаковка полученных результатов в XML элемент
//			 */
//
//			Element entrTestResElem = doc.createElement("EntranceTestResult");
//			rooElement.appendChild(entrTestResElem);
//
//			//ID дисциплины
//			ToolsDao.PackUid(doc, entrTestResElem, Long.toString(discipline2RealizationWayRelation.getId()));
//
//			//Итоговая оценка
//			ToolsDao.PackToElement(doc, entrTestResElem, "ResultValue", String.valueOf(resultValue));
//
//			//ИД основание для оценки
//			ToolsDao.PackToElement(doc, entrTestResElem, "ResultSourceTypeID", String.valueOf(makrsoursefis.getId()));
//
//
//			//Дисциплина вступительного испытания
//			Ecdisciplinefis ecdisciplinefis = SpravDao.Instanse().getEcdisciplinefis(educationSubject);
//			//Либо ID, либо title если ID пусто
//			ToolsDao.PackToElement(doc, entrTestResElem, "EntranceTestSubject", ecdisciplinefis != null ? String.valueOf(ecdisciplinefis.getCode()) : educationSubject.getTitle());
//
//			if(entranceTestTypeID!=null){
//				Entrancedisciplinetypefis entrancedisciplinetypefis = SpravDao.Instanse().getEntrancedisciplinetypefis(entranceTestTypeID);
//				//ИД типа вступительного испытания
//				ToolsDao.PackToElement(doc, entrTestResElem, "EntranceTestTypeID", entrancedisciplinetypefis.getId().toString());
//			}
//
//
//			EnrollmentDirection enrollmentDirection = chosenEntranceDiscipline.getChosenEnrollmentDirection().getEnrollmentDirection();	
//
//			ToolsDao.PackToElement(doc, entrTestResElem, "CompetitiveGroupID", enrollmentDirection.getId().toString());			
//
//		}
//
//
//		/**
//		 * TODO
//		 * ToolsDao.PackToElement(doc, entrTestResElem, "ResultDocument", "");
//		 * 
//		 * Выбор сведения об основании для оценки
//		 * (непонятен смысл этого поля, если есть уже ResultSourceTypeID и там записан 
//		 * ID основания для оценки)
//		 */
//
//		return rooElement;
//	}

}
