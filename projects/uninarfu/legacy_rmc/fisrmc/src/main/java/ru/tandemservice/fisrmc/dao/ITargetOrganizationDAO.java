package ru.tandemservice.fisrmc.dao;

import ru.tandemservice.fisrmc.entity.catalog.EducationdirectionSpravType;

import java.util.List;

// import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

public interface ITargetOrganizationDAO {
    /*
	public int getSumTargetPlan(EnrollmentDirection direction, boolean isBudget);
	public int getSumTotalTargetPlan(EnrollmentDirection direction, boolean isBudget);
	*/

//	/**
//	 * сведения о целевом наборе
//	 * 
//	 * для конкурсной группы - смотрим направления подготовки для приема. 
//	 * Если у направлений подготовки для приема есть 
//	 * связь с 'targetadmissionplanrelation_t' - План приема по ЦП
//	 * и указаны числа >0 (sum(planvalue_p) для всех зхначений budget_p в рамках направления подготовки для приема),
//	 * то возвращаем данные, иначе на выход null
//	 * 
//	 * для каждой  enrollmentdirection_t суммируем  targetadmissionplanbudget_p и targetadmissionplancontract_p, 
//	 * получаем число абитуриентов, зачисляемое по ЦП.
//	 * 
//	 *  Далее, смотрим в targetadmissionplanrelation_t и формируем 
//	 *  XMLTargetOrganizationItem для каждой targetadmissionkind_t (одна targetadmissionkind_t - один XMLTargetOrganizationItem)
//	 *  
//	 *  суммируем все получившиеся числа по budget_p и planvalue_p, смотрим - если числа
//	 *  не совпадают с тем, что указано у целевиков в enrollmentdirection_t,
//	 *  создаем еще одну XMLTargetOrganizationItem для targetadmissionkind_t с code=1, туда дописываем недостающее (знак может быть и минус).
//	 *  
//	 * @param competitiveGroup
//	 * @return
//	 */
//	public List<XMLTargetOrganizationItem> getTargetOrganizationsList(
//			XMLCompetitiveGroup competitiveGroup);

    public List<XMLTargetOrganization> getTargetOrganizations(XMLCompetitiveGroup competitiveGroup, EducationdirectionSpravType spravType);

    public void clearCache();
}
