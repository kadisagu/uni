package ru.tandemservice.fisrmc.component.catalog.directoryfis.DirectoryfisAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.fisrmc.entity.catalog.Directoryfis;

public interface IDAO extends IDefaultCatalogAddEditDAO<Directoryfis, Model> {

}
