package ru.tandemservice.fisrmc.dao.packApplications;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.codes.IdentityCardTypeCodes;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.tandemservice.fisrmc.dao.PackUnpackToFis;
import ru.tandemservice.fisrmc.dao.SpravDao;
import ru.tandemservice.fisrmc.dao.ToolsDao;
import ru.tandemservice.fisrmc.entity.catalog.Countryfis;
import ru.tandemservice.fisrmc.entity.catalog.Identitycardtypefis;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PackIdentityDocument
        extends UniBaseDao
        implements IPackIdentityDocument
{

    private static IPackIdentityDocument _IPackIdentityDocument = null;

    public static IPackIdentityDocument Instanse()
    {
        if (_IPackIdentityDocument == null)
            _IPackIdentityDocument = (IPackIdentityDocument) ApplicationRuntime.getBean("packIdentityDocument");
        return _IPackIdentityDocument;
    }


    @Override
    public Element packIdentityDocument(
            Document doc
            , EntrantRequest entrantrequest
            , IdentityCard identityCard
    )
    {

        IdentityCardType cardType = identityCard.getCardType();
        String seria_code = cardType.getCode();

        Map<String, String> xmlValueMap = new LinkedHashMap<String, String>();

        //Идентификатор в ИС ОУ
        // xmlValueMap.put("UID", identityCard.getId().toString());

        boolean isOrigin = Applications.Instanse().isOriginalDocuments(entrantrequest);

        //Признак предоставления оригиналов
        xmlValueMap.put("OriginalReceived", Boolean.toString(isOrigin).toLowerCase());

        if (isOrigin)
            //Дата предоставления оигинала документов (пока неясно какое значение сюда подставлять)
            xmlValueMap.put("OriginalReceivedDate", Applications.Instanse().getOriginDocumentDateString(entrantrequest));

        //Дата предоставления оигинала документов
        String seria = identityCard.getSeria();


        //Номер документа
        String number = identityCard.getNumber();
        if (PackUnpackToFis.DAEMON_PARAMS.isAbfuscateData()) {


            Random r = new Random();

            Long rndNumberId = r.nextLong();
            Long rndSeriaId = r.nextLong();

            while (rndNumberId < 0)
                rndNumberId = r.nextLong();

            while (rndSeriaId < 0)
                rndSeriaId = r.nextLong();

            // далее замесим случайным числом
            if (number == null || number.length() == 0)
                number = "012345";

            // спрячем номер документа
            if (number != null && number.length() > 0)
                number = Long.toString(rndNumberId).substring(0, number.length());


            if (seria != null && seria.length() > 0)
                seria = Long.toString(rndSeriaId).substring(0, seria.length());

            // ждя известных типов документов исправляем серии документов
            // например у загран паспорта РФ серия всегда = "61"
            if (seria_code.equals(IdentityCardTypeCodes.ROSSIYSKIY_ZAGRANICHNYY_PASPORT))
                seria = "61";

            if (seria_code.equals(IdentityCardTypeCodes.ROSSIYSKIY_ZAGRANICHNYY_PASPORT))
                seria = "61";

            // паспорт иностранного графданина
            if (seria_code.equals(IdentityCardTypeCodes.PASPORT_GRAJDANINA_INOSTRANNOGO_GOSUDARSTVA))
                seria = "NRF";

            // другой документ
            if (seria_code.equals(IdentityCardTypeCodes.DRUGOY_DOKUMENT))
                seria = "ДД";

            // другой документ
            if (seria_code.equals(IdentityCardTypeCodes.DRUGOY_DOKUMENT))
                seria = "ДД";

            // вид на жительство (образец = 82 http://zig-migracia.ru/obrazec-vida-na-jitelstvo.html)
            if (seria_code.equals(IdentityCardTypeCodes.VID_NA_JITELSTVO))
                seria = "82";

            if (seria == null || seria.length() == 0)
                seria = "БН";


        }

        if (seria != null && (seria.trim()).length() > 0)
            xmlValueMap.put("DocumentSeries", seria);

        xmlValueMap.put("DocumentNumber", number != null ? number : "");

        //Дата выдачи документа
        Date issuanceDate = identityCard.getIssuanceDate();
        if (issuanceDate == null) {
            if (PackUnpackToFis.DAEMON_PARAMS.isSuspendErrorDoc()) {
                issuanceDate = new Date();
                issuanceDate = ToolsDao.addYear(issuanceDate, -50);
                PackUnpackToFis.AddPkgError("Для персоны " + identityCard.getPerson().getFullFio() + " не указана дата выдачи УЛ");
            }
            else
                throw new ApplicationException("Для персоны " + identityCard.getPerson().getFullFio() + " не указана дата выдачи УЛ");
        }

        xmlValueMap.put("DocumentDate", ToolsDao.DateToString(issuanceDate));


        //Кем выдан (не обязательно)
        String issuancePlace = identityCard.getIssuancePlace();
        if (issuancePlace != null && issuancePlace.length() > 0)
            xmlValueMap.put("DocumentOrganization", issuancePlace);

        //Ид типа документа, удостоверяющего личность
        Identitycardtypefis identitycardtypeFis = SpravDao.Instanse().getIdentitycardtypeFis(cardType);
        xmlValueMap.put("IdentityDocumentTypeID", identitycardtypeFis.getCode());

        //ИД гражданства (страна)
        Countryfis countryFis = SpravDao.Instanse().getCountryFis(identityCard.getCitizenship());
        if (countryFis == null)
            throw new ApplicationException("Для перосны " + identityCard.getPerson().getFullFio() + " в удостоверении личности не указано гражданство");

        String countryFisStr = countryFis.getCode();
        xmlValueMap.put("NationalityTypeID", countryFisStr);

        //Дата рождения
        Date birthDate = identityCard.getBirthDate();
        if (birthDate == null) {
            if (PackUnpackToFis.DAEMON_PARAMS.isSuspendErrorDoc()) {
                birthDate = new Date();
                birthDate = ToolsDao.addYear(birthDate, -40);
                PackUnpackToFis.AddPkgError("Для перосны " + identityCard.getPerson().getFullFio() + " не указана дата рождения ");
            }
            else
                throw new ApplicationException("Для перосны " + identityCard.getPerson().getFullFio() + " не указана дата рождения ");
        }

        xmlValueMap.put("BirthDate", ToolsDao.DateToString(birthDate));

        //Место рождения необязательно
        String birthPlace = identityCard.getBirthPlace();
        if (birthPlace != null && birthPlace.length() > 0)
            xmlValueMap.put("BirthPlace", birthPlace);

        //Код подразделения (необязательно)
        String issuanceCode = identityCard.getIssuanceCode();

        // код подразделения ставим только правильный
        if (
                issuanceCode != null
                        && issuanceCode.contains("-")
                        && seria_code.equals(IdentityCardTypeCodes.PASPORT_GRAJDANINA_ROSSIYSKOY_FEDERATSII)
                )
        {
            // паттерн на код подразделения
            Pattern pattern = Pattern.compile("\\d{3}-\\d{3}");
            Matcher matcher = pattern.matcher(issuanceCode);

            if (matcher.matches())
                xmlValueMap.put("SubdivisionCode", issuanceCode);
        }
        Element rootElement = doc.createElement("IdentityDocument");

        for (Entry<String, String> set : xmlValueMap.entrySet()) {
            Element createElement = doc.createElement(set.getKey());
            createElement.setTextContent(set.getValue());
            rootElement.appendChild(createElement);
        }

        return rootElement;
    }

}
