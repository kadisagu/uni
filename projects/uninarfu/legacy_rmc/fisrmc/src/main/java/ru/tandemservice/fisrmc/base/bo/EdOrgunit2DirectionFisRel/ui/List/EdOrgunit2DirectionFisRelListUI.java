package ru.tandemservice.fisrmc.base.bo.EdOrgunit2DirectionFisRel.ui.List;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.fisrmc.base.bo.EdOrgunit2DirectionFisRel.EdOrgunit2DirectionFisRelManager;
import ru.tandemservice.fisrmc.base.bo.EdOrgunit2DirectionFisRel.ui.Edit.EdOrgunit2DirectionFisRelEdit;
import ru.tandemservice.fisrmc.entity.catalog.EducationdirectionSpravType;

import java.util.List;
import java.util.Map;

public class EdOrgunit2DirectionFisRelListUI extends UIPresenter {

    @Override
    public void onComponentRefresh() {

        super.onComponentRefresh();

    }


    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {

        Map<String, Object> settingMap = _uiSettings.getAsMap(
                "enrollmentCampaignFilter",
                "qualificationsFilter",
                "oksoCodeFilter",
                "oksoCodeParentFilter",
                "directionTitleFilter",
                "directionTitleFilterFis",
                "isSuccessfullyHandOverSession",
                "exportComment",
                "spravTypeFilter",
                "eduProgTitleFilter",
                "eduProgCodeSpecFilter",
                "eduProgCodeFilter"
        );
        dataSource.putAll(settingMap);


    }

    public void onEditRelation() {

        Long spravTypeFilterID = getSettings().getEntityId("spravTypeFilter");

        saveSettings();
        _uiActivation.asCurrent(EdOrgunit2DirectionFisRelEdit.class)
                .parameter("educationOrgUnitID", getListenerParameterAsLong())
                .parameter("spravTypeFilterID", spravTypeFilterID)

                .activate();
    }

    public void relationToFIS() {

        if (getSettings().getEntityId("spravTypeFilter") == null)
            throw new ApplicationException("Не указан тип справочника");

        List<DataWrapper> dataSourceRecords = _uiConfig.getDataSource("edOrgunit2DirectionFisRelDS").getRecords();

        Long spravTypeFilterID = getSettings().getEntityId("spravTypeFilter");
        EducationdirectionSpravType spravType = DataAccessServices.dao().get(EducationdirectionSpravType.class, spravTypeFilterID);

        EdOrgunit2DirectionFisRelManager.instance().modifyDao().persistRelation(dataSourceRecords, spravType);

    }

}
