package ru.tandemservice.fisrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.fisrmc.entity.FisPackages;
import ru.tandemservice.fisrmc.entity.catalog.EducationdirectionSpravType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность для хранения сформированных пакетов
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FisPackagesGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.fisrmc.entity.FisPackages";
    public static final String ENTITY_NAME = "fisPackages";
    public static final int VERSION_HASH = 2070218566;
    private static IEntityMeta ENTITY_META;

    public static final String P_FORMING_DATE = "formingDate";
    public static final String P_CAMPAIGN_INFO = "campaignInfo";
    public static final String P_ADMISSION_INFO = "admissionInfo";
    public static final String P_APPLICATIONS = "applications";
    public static final String P_APP_DATE_WITHOUT_TIME = "appDateWithoutTime";
    public static final String P_ENTRANCE_TEST_RESULT = "entranceTestResult";
    public static final String P_TEST_APPLICATION_MODE = "testApplicationMode";
    public static final String P_TEST_APP_MODE_DATA = "testAppModeData";
    public static final String P_SUSPEND_COMPETITIVE_GROUP = "suspendCompetitiveGroup";
    public static final String P_SUSPEND_ERROR_DOC = "suspendErrorDoc";
    public static final String P_ABFUSCATE_DATA = "abfuscateData";
    public static final String P_ORDERS_OF_ADMISSION = "ordersOfAdmission";
    public static final String P_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_SPRAV_TYPE = "spravType";
    public static final String P_GLOBAL_ERROR = "globalError";
    public static final String P_DESCRIPTION = "description";
    public static final String L_XML_PACKAGE = "xmlPackage";
    public static final String L_XML_ERRORS = "xmlErrors";
    public static final String P_FIS_PKG_NUMBER = "fisPkgNumber";
    public static final String P_FIS_PRIM = "fisPrim";
    public static final String P_DELETE_PKG_TYPE = "deletePkgType";
    public static final String P_DELETE_APPLICATION = "deleteApplication";
    public static final String P_DELETE_ORDERS_OF_ADMISSION = "deleteOrdersOfAdmission";
    public static final String P_DELETE_COMPETITIVE_GROUP_ITEMS = "deleteCompetitiveGroupItems";
    public static final String P_DELETE_ENTRANCE_TEST_RESULTS = "deleteEntranceTestResults";
    public static final String P_DELETE_APPLICATION_COMMON_BENEFITS = "deleteApplicationCommonBenefits";

    private Date _formingDate;     // Дата формирования пакета
    private Boolean _campaignInfo;     // Информация о приемной кампании
    private Boolean _admissionInfo;     // Сведения об объеме и структуре приема
    private Boolean _applications;     // Заявления
    private Boolean _appDateWithoutTime;     // Дата регистрации заявления без времени
    private Boolean _entranceTestResult;     // Оценки в заявлении
    private Boolean _testApplicationMode;     // Загрузка в тестовом режиме
    private String _testAppModeData;     // Сведения о загрузке в тестовом режиме
    private Boolean _suspendCompetitiveGroup;     // Принудительно отменить конкурсные группы
    private Boolean _suspendErrorDoc;     // Подавлять ошибки неполных данных
    private Boolean _abfuscateData;     // Скрывать персональные данные
    private Boolean _ordersOfAdmission;     // Включенные в приказ
    private String _enrollmentCampaign;     // Приемные кампании
    private EducationdirectionSpravType _spravType;     // Тип справочника
    private Boolean _globalError;     // Есть ошибка при формировании пакета
    private String _description;     // Примечание выгрузки
    private DatabaseFile _xmlPackage;     // Пакет выгрузки
    private DatabaseFile _xmlErrors;     // Ошибки формирования пакета
    private String _fisPkgNumber;     // Номер пакета в ФИС
    private String _fisPrim;     // Примечание из ФИС
    private boolean _deletePkgType = false;     // Тип пакета-удаление данных
    private Boolean _deleteApplication;     // Удаление заявлений
    private Boolean _deleteOrdersOfAdmission;     // Удаление включенных в приказ
    private Boolean _deleteCompetitiveGroupItems;     // Удаление направлений подготовки конкурсных групп
    private Boolean _deleteEntranceTestResults;     // Удаление результатов вступительных испытаний
    private Boolean _deleteApplicationCommonBenefits;     // Удаление общих льгот абитуриента

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата формирования пакета.
     */
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования пакета.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Информация о приемной кампании.
     */
    public Boolean getCampaignInfo()
    {
        return _campaignInfo;
    }

    /**
     * @param campaignInfo Информация о приемной кампании.
     */
    public void setCampaignInfo(Boolean campaignInfo)
    {
        dirty(_campaignInfo, campaignInfo);
        _campaignInfo = campaignInfo;
    }

    /**
     * @return Сведения об объеме и структуре приема.
     */
    public Boolean getAdmissionInfo()
    {
        return _admissionInfo;
    }

    /**
     * @param admissionInfo Сведения об объеме и структуре приема.
     */
    public void setAdmissionInfo(Boolean admissionInfo)
    {
        dirty(_admissionInfo, admissionInfo);
        _admissionInfo = admissionInfo;
    }

    /**
     * @return Заявления.
     */
    public Boolean getApplications()
    {
        return _applications;
    }

    /**
     * @param applications Заявления.
     */
    public void setApplications(Boolean applications)
    {
        dirty(_applications, applications);
        _applications = applications;
    }

    /**
     * @return Дата регистрации заявления без времени.
     */
    public Boolean getAppDateWithoutTime()
    {
        return _appDateWithoutTime;
    }

    /**
     * @param appDateWithoutTime Дата регистрации заявления без времени.
     */
    public void setAppDateWithoutTime(Boolean appDateWithoutTime)
    {
        dirty(_appDateWithoutTime, appDateWithoutTime);
        _appDateWithoutTime = appDateWithoutTime;
    }

    /**
     * @return Оценки в заявлении.
     */
    public Boolean getEntranceTestResult()
    {
        return _entranceTestResult;
    }

    /**
     * @param entranceTestResult Оценки в заявлении.
     */
    public void setEntranceTestResult(Boolean entranceTestResult)
    {
        dirty(_entranceTestResult, entranceTestResult);
        _entranceTestResult = entranceTestResult;
    }

    /**
     * @return Загрузка в тестовом режиме.
     */
    public Boolean getTestApplicationMode()
    {
        return _testApplicationMode;
    }

    /**
     * @param testApplicationMode Загрузка в тестовом режиме.
     */
    public void setTestApplicationMode(Boolean testApplicationMode)
    {
        dirty(_testApplicationMode, testApplicationMode);
        _testApplicationMode = testApplicationMode;
    }

    /**
     * @return Сведения о загрузке в тестовом режиме.
     */
    @Length(max=255)
    public String getTestAppModeData()
    {
        return _testAppModeData;
    }

    /**
     * @param testAppModeData Сведения о загрузке в тестовом режиме.
     */
    public void setTestAppModeData(String testAppModeData)
    {
        dirty(_testAppModeData, testAppModeData);
        _testAppModeData = testAppModeData;
    }

    /**
     * @return Принудительно отменить конкурсные группы.
     */
    public Boolean getSuspendCompetitiveGroup()
    {
        return _suspendCompetitiveGroup;
    }

    /**
     * @param suspendCompetitiveGroup Принудительно отменить конкурсные группы.
     */
    public void setSuspendCompetitiveGroup(Boolean suspendCompetitiveGroup)
    {
        dirty(_suspendCompetitiveGroup, suspendCompetitiveGroup);
        _suspendCompetitiveGroup = suspendCompetitiveGroup;
    }

    /**
     * @return Подавлять ошибки неполных данных.
     */
    public Boolean getSuspendErrorDoc()
    {
        return _suspendErrorDoc;
    }

    /**
     * @param suspendErrorDoc Подавлять ошибки неполных данных.
     */
    public void setSuspendErrorDoc(Boolean suspendErrorDoc)
    {
        dirty(_suspendErrorDoc, suspendErrorDoc);
        _suspendErrorDoc = suspendErrorDoc;
    }

    /**
     * @return Скрывать персональные данные.
     */
    public Boolean getAbfuscateData()
    {
        return _abfuscateData;
    }

    /**
     * @param abfuscateData Скрывать персональные данные.
     */
    public void setAbfuscateData(Boolean abfuscateData)
    {
        dirty(_abfuscateData, abfuscateData);
        _abfuscateData = abfuscateData;
    }

    /**
     * @return Включенные в приказ.
     */
    public Boolean getOrdersOfAdmission()
    {
        return _ordersOfAdmission;
    }

    /**
     * @param ordersOfAdmission Включенные в приказ.
     */
    public void setOrdersOfAdmission(Boolean ordersOfAdmission)
    {
        dirty(_ordersOfAdmission, ordersOfAdmission);
        _ordersOfAdmission = ordersOfAdmission;
    }

    /**
     * @return Приемные кампании.
     */
    @Length(max=255)
    public String getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемные кампании.
     */
    public void setEnrollmentCampaign(String enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Тип справочника.
     */
    public EducationdirectionSpravType getSpravType()
    {
        return _spravType;
    }

    /**
     * @param spravType Тип справочника.
     */
    public void setSpravType(EducationdirectionSpravType spravType)
    {
        dirty(_spravType, spravType);
        _spravType = spravType;
    }

    /**
     * @return Есть ошибка при формировании пакета.
     */
    public Boolean getGlobalError()
    {
        return _globalError;
    }

    /**
     * @param globalError Есть ошибка при формировании пакета.
     */
    public void setGlobalError(Boolean globalError)
    {
        dirty(_globalError, globalError);
        _globalError = globalError;
    }

    /**
     * @return Примечание выгрузки.
     */
    @Length(max=255)
    public String getDescription()
    {
        return _description;
    }

    /**
     * @param description Примечание выгрузки.
     */
    public void setDescription(String description)
    {
        dirty(_description, description);
        _description = description;
    }

    /**
     * @return Пакет выгрузки.
     */
    public DatabaseFile getXmlPackage()
    {
        return _xmlPackage;
    }

    /**
     * @param xmlPackage Пакет выгрузки.
     */
    public void setXmlPackage(DatabaseFile xmlPackage)
    {
        dirty(_xmlPackage, xmlPackage);
        _xmlPackage = xmlPackage;
    }

    /**
     * @return Ошибки формирования пакета.
     */
    public DatabaseFile getXmlErrors()
    {
        return _xmlErrors;
    }

    /**
     * @param xmlErrors Ошибки формирования пакета.
     */
    public void setXmlErrors(DatabaseFile xmlErrors)
    {
        dirty(_xmlErrors, xmlErrors);
        _xmlErrors = xmlErrors;
    }

    /**
     * @return Номер пакета в ФИС.
     */
    @Length(max=255)
    public String getFisPkgNumber()
    {
        return _fisPkgNumber;
    }

    /**
     * @param fisPkgNumber Номер пакета в ФИС.
     */
    public void setFisPkgNumber(String fisPkgNumber)
    {
        dirty(_fisPkgNumber, fisPkgNumber);
        _fisPkgNumber = fisPkgNumber;
    }

    /**
     * @return Примечание из ФИС.
     */
    public String getFisPrim()
    {
        return _fisPrim;
    }

    /**
     * @param fisPrim Примечание из ФИС.
     */
    public void setFisPrim(String fisPrim)
    {
        dirty(_fisPrim, fisPrim);
        _fisPrim = fisPrim;
    }

    /**
     * @return Тип пакета-удаление данных. Свойство не может быть null.
     */
    @NotNull
    public boolean isDeletePkgType()
    {
        return _deletePkgType;
    }

    /**
     * @param deletePkgType Тип пакета-удаление данных. Свойство не может быть null.
     */
    public void setDeletePkgType(boolean deletePkgType)
    {
        dirty(_deletePkgType, deletePkgType);
        _deletePkgType = deletePkgType;
    }

    /**
     * @return Удаление заявлений.
     */
    public Boolean getDeleteApplication()
    {
        return _deleteApplication;
    }

    /**
     * @param deleteApplication Удаление заявлений.
     */
    public void setDeleteApplication(Boolean deleteApplication)
    {
        dirty(_deleteApplication, deleteApplication);
        _deleteApplication = deleteApplication;
    }

    /**
     * @return Удаление включенных в приказ.
     */
    public Boolean getDeleteOrdersOfAdmission()
    {
        return _deleteOrdersOfAdmission;
    }

    /**
     * @param deleteOrdersOfAdmission Удаление включенных в приказ.
     */
    public void setDeleteOrdersOfAdmission(Boolean deleteOrdersOfAdmission)
    {
        dirty(_deleteOrdersOfAdmission, deleteOrdersOfAdmission);
        _deleteOrdersOfAdmission = deleteOrdersOfAdmission;
    }

    /**
     * @return Удаление направлений подготовки конкурсных групп.
     */
    public Boolean getDeleteCompetitiveGroupItems()
    {
        return _deleteCompetitiveGroupItems;
    }

    /**
     * @param deleteCompetitiveGroupItems Удаление направлений подготовки конкурсных групп.
     */
    public void setDeleteCompetitiveGroupItems(Boolean deleteCompetitiveGroupItems)
    {
        dirty(_deleteCompetitiveGroupItems, deleteCompetitiveGroupItems);
        _deleteCompetitiveGroupItems = deleteCompetitiveGroupItems;
    }

    /**
     * @return Удаление результатов вступительных испытаний.
     */
    public Boolean getDeleteEntranceTestResults()
    {
        return _deleteEntranceTestResults;
    }

    /**
     * @param deleteEntranceTestResults Удаление результатов вступительных испытаний.
     */
    public void setDeleteEntranceTestResults(Boolean deleteEntranceTestResults)
    {
        dirty(_deleteEntranceTestResults, deleteEntranceTestResults);
        _deleteEntranceTestResults = deleteEntranceTestResults;
    }

    /**
     * @return Удаление общих льгот абитуриента.
     */
    public Boolean getDeleteApplicationCommonBenefits()
    {
        return _deleteApplicationCommonBenefits;
    }

    /**
     * @param deleteApplicationCommonBenefits Удаление общих льгот абитуриента.
     */
    public void setDeleteApplicationCommonBenefits(Boolean deleteApplicationCommonBenefits)
    {
        dirty(_deleteApplicationCommonBenefits, deleteApplicationCommonBenefits);
        _deleteApplicationCommonBenefits = deleteApplicationCommonBenefits;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FisPackagesGen)
        {
            setFormingDate(((FisPackages)another).getFormingDate());
            setCampaignInfo(((FisPackages)another).getCampaignInfo());
            setAdmissionInfo(((FisPackages)another).getAdmissionInfo());
            setApplications(((FisPackages)another).getApplications());
            setAppDateWithoutTime(((FisPackages)another).getAppDateWithoutTime());
            setEntranceTestResult(((FisPackages)another).getEntranceTestResult());
            setTestApplicationMode(((FisPackages)another).getTestApplicationMode());
            setTestAppModeData(((FisPackages)another).getTestAppModeData());
            setSuspendCompetitiveGroup(((FisPackages)another).getSuspendCompetitiveGroup());
            setSuspendErrorDoc(((FisPackages)another).getSuspendErrorDoc());
            setAbfuscateData(((FisPackages)another).getAbfuscateData());
            setOrdersOfAdmission(((FisPackages)another).getOrdersOfAdmission());
            setEnrollmentCampaign(((FisPackages)another).getEnrollmentCampaign());
            setSpravType(((FisPackages)another).getSpravType());
            setGlobalError(((FisPackages)another).getGlobalError());
            setDescription(((FisPackages)another).getDescription());
            setXmlPackage(((FisPackages)another).getXmlPackage());
            setXmlErrors(((FisPackages)another).getXmlErrors());
            setFisPkgNumber(((FisPackages)another).getFisPkgNumber());
            setFisPrim(((FisPackages)another).getFisPrim());
            setDeletePkgType(((FisPackages)another).isDeletePkgType());
            setDeleteApplication(((FisPackages)another).getDeleteApplication());
            setDeleteOrdersOfAdmission(((FisPackages)another).getDeleteOrdersOfAdmission());
            setDeleteCompetitiveGroupItems(((FisPackages)another).getDeleteCompetitiveGroupItems());
            setDeleteEntranceTestResults(((FisPackages)another).getDeleteEntranceTestResults());
            setDeleteApplicationCommonBenefits(((FisPackages)another).getDeleteApplicationCommonBenefits());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FisPackagesGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FisPackages.class;
        }

        public T newInstance()
        {
            return (T) new FisPackages();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "formingDate":
                    return obj.getFormingDate();
                case "campaignInfo":
                    return obj.getCampaignInfo();
                case "admissionInfo":
                    return obj.getAdmissionInfo();
                case "applications":
                    return obj.getApplications();
                case "appDateWithoutTime":
                    return obj.getAppDateWithoutTime();
                case "entranceTestResult":
                    return obj.getEntranceTestResult();
                case "testApplicationMode":
                    return obj.getTestApplicationMode();
                case "testAppModeData":
                    return obj.getTestAppModeData();
                case "suspendCompetitiveGroup":
                    return obj.getSuspendCompetitiveGroup();
                case "suspendErrorDoc":
                    return obj.getSuspendErrorDoc();
                case "abfuscateData":
                    return obj.getAbfuscateData();
                case "ordersOfAdmission":
                    return obj.getOrdersOfAdmission();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "spravType":
                    return obj.getSpravType();
                case "globalError":
                    return obj.getGlobalError();
                case "description":
                    return obj.getDescription();
                case "xmlPackage":
                    return obj.getXmlPackage();
                case "xmlErrors":
                    return obj.getXmlErrors();
                case "fisPkgNumber":
                    return obj.getFisPkgNumber();
                case "fisPrim":
                    return obj.getFisPrim();
                case "deletePkgType":
                    return obj.isDeletePkgType();
                case "deleteApplication":
                    return obj.getDeleteApplication();
                case "deleteOrdersOfAdmission":
                    return obj.getDeleteOrdersOfAdmission();
                case "deleteCompetitiveGroupItems":
                    return obj.getDeleteCompetitiveGroupItems();
                case "deleteEntranceTestResults":
                    return obj.getDeleteEntranceTestResults();
                case "deleteApplicationCommonBenefits":
                    return obj.getDeleteApplicationCommonBenefits();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "campaignInfo":
                    obj.setCampaignInfo((Boolean) value);
                    return;
                case "admissionInfo":
                    obj.setAdmissionInfo((Boolean) value);
                    return;
                case "applications":
                    obj.setApplications((Boolean) value);
                    return;
                case "appDateWithoutTime":
                    obj.setAppDateWithoutTime((Boolean) value);
                    return;
                case "entranceTestResult":
                    obj.setEntranceTestResult((Boolean) value);
                    return;
                case "testApplicationMode":
                    obj.setTestApplicationMode((Boolean) value);
                    return;
                case "testAppModeData":
                    obj.setTestAppModeData((String) value);
                    return;
                case "suspendCompetitiveGroup":
                    obj.setSuspendCompetitiveGroup((Boolean) value);
                    return;
                case "suspendErrorDoc":
                    obj.setSuspendErrorDoc((Boolean) value);
                    return;
                case "abfuscateData":
                    obj.setAbfuscateData((Boolean) value);
                    return;
                case "ordersOfAdmission":
                    obj.setOrdersOfAdmission((Boolean) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((String) value);
                    return;
                case "spravType":
                    obj.setSpravType((EducationdirectionSpravType) value);
                    return;
                case "globalError":
                    obj.setGlobalError((Boolean) value);
                    return;
                case "description":
                    obj.setDescription((String) value);
                    return;
                case "xmlPackage":
                    obj.setXmlPackage((DatabaseFile) value);
                    return;
                case "xmlErrors":
                    obj.setXmlErrors((DatabaseFile) value);
                    return;
                case "fisPkgNumber":
                    obj.setFisPkgNumber((String) value);
                    return;
                case "fisPrim":
                    obj.setFisPrim((String) value);
                    return;
                case "deletePkgType":
                    obj.setDeletePkgType((Boolean) value);
                    return;
                case "deleteApplication":
                    obj.setDeleteApplication((Boolean) value);
                    return;
                case "deleteOrdersOfAdmission":
                    obj.setDeleteOrdersOfAdmission((Boolean) value);
                    return;
                case "deleteCompetitiveGroupItems":
                    obj.setDeleteCompetitiveGroupItems((Boolean) value);
                    return;
                case "deleteEntranceTestResults":
                    obj.setDeleteEntranceTestResults((Boolean) value);
                    return;
                case "deleteApplicationCommonBenefits":
                    obj.setDeleteApplicationCommonBenefits((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "formingDate":
                        return true;
                case "campaignInfo":
                        return true;
                case "admissionInfo":
                        return true;
                case "applications":
                        return true;
                case "appDateWithoutTime":
                        return true;
                case "entranceTestResult":
                        return true;
                case "testApplicationMode":
                        return true;
                case "testAppModeData":
                        return true;
                case "suspendCompetitiveGroup":
                        return true;
                case "suspendErrorDoc":
                        return true;
                case "abfuscateData":
                        return true;
                case "ordersOfAdmission":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "spravType":
                        return true;
                case "globalError":
                        return true;
                case "description":
                        return true;
                case "xmlPackage":
                        return true;
                case "xmlErrors":
                        return true;
                case "fisPkgNumber":
                        return true;
                case "fisPrim":
                        return true;
                case "deletePkgType":
                        return true;
                case "deleteApplication":
                        return true;
                case "deleteOrdersOfAdmission":
                        return true;
                case "deleteCompetitiveGroupItems":
                        return true;
                case "deleteEntranceTestResults":
                        return true;
                case "deleteApplicationCommonBenefits":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "formingDate":
                    return true;
                case "campaignInfo":
                    return true;
                case "admissionInfo":
                    return true;
                case "applications":
                    return true;
                case "appDateWithoutTime":
                    return true;
                case "entranceTestResult":
                    return true;
                case "testApplicationMode":
                    return true;
                case "testAppModeData":
                    return true;
                case "suspendCompetitiveGroup":
                    return true;
                case "suspendErrorDoc":
                    return true;
                case "abfuscateData":
                    return true;
                case "ordersOfAdmission":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "spravType":
                    return true;
                case "globalError":
                    return true;
                case "description":
                    return true;
                case "xmlPackage":
                    return true;
                case "xmlErrors":
                    return true;
                case "fisPkgNumber":
                    return true;
                case "fisPrim":
                    return true;
                case "deletePkgType":
                    return true;
                case "deleteApplication":
                    return true;
                case "deleteOrdersOfAdmission":
                    return true;
                case "deleteCompetitiveGroupItems":
                    return true;
                case "deleteEntranceTestResults":
                    return true;
                case "deleteApplicationCommonBenefits":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "formingDate":
                    return Date.class;
                case "campaignInfo":
                    return Boolean.class;
                case "admissionInfo":
                    return Boolean.class;
                case "applications":
                    return Boolean.class;
                case "appDateWithoutTime":
                    return Boolean.class;
                case "entranceTestResult":
                    return Boolean.class;
                case "testApplicationMode":
                    return Boolean.class;
                case "testAppModeData":
                    return String.class;
                case "suspendCompetitiveGroup":
                    return Boolean.class;
                case "suspendErrorDoc":
                    return Boolean.class;
                case "abfuscateData":
                    return Boolean.class;
                case "ordersOfAdmission":
                    return Boolean.class;
                case "enrollmentCampaign":
                    return String.class;
                case "spravType":
                    return EducationdirectionSpravType.class;
                case "globalError":
                    return Boolean.class;
                case "description":
                    return String.class;
                case "xmlPackage":
                    return DatabaseFile.class;
                case "xmlErrors":
                    return DatabaseFile.class;
                case "fisPkgNumber":
                    return String.class;
                case "fisPrim":
                    return String.class;
                case "deletePkgType":
                    return Boolean.class;
                case "deleteApplication":
                    return Boolean.class;
                case "deleteOrdersOfAdmission":
                    return Boolean.class;
                case "deleteCompetitiveGroupItems":
                    return Boolean.class;
                case "deleteEntranceTestResults":
                    return Boolean.class;
                case "deleteApplicationCommonBenefits":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FisPackages> _dslPath = new Path<FisPackages>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FisPackages");
    }
            

    /**
     * @return Дата формирования пакета.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Информация о приемной кампании.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getCampaignInfo()
     */
    public static PropertyPath<Boolean> campaignInfo()
    {
        return _dslPath.campaignInfo();
    }

    /**
     * @return Сведения об объеме и структуре приема.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getAdmissionInfo()
     */
    public static PropertyPath<Boolean> admissionInfo()
    {
        return _dslPath.admissionInfo();
    }

    /**
     * @return Заявления.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getApplications()
     */
    public static PropertyPath<Boolean> applications()
    {
        return _dslPath.applications();
    }

    /**
     * @return Дата регистрации заявления без времени.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getAppDateWithoutTime()
     */
    public static PropertyPath<Boolean> appDateWithoutTime()
    {
        return _dslPath.appDateWithoutTime();
    }

    /**
     * @return Оценки в заявлении.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getEntranceTestResult()
     */
    public static PropertyPath<Boolean> entranceTestResult()
    {
        return _dslPath.entranceTestResult();
    }

    /**
     * @return Загрузка в тестовом режиме.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getTestApplicationMode()
     */
    public static PropertyPath<Boolean> testApplicationMode()
    {
        return _dslPath.testApplicationMode();
    }

    /**
     * @return Сведения о загрузке в тестовом режиме.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getTestAppModeData()
     */
    public static PropertyPath<String> testAppModeData()
    {
        return _dslPath.testAppModeData();
    }

    /**
     * @return Принудительно отменить конкурсные группы.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getSuspendCompetitiveGroup()
     */
    public static PropertyPath<Boolean> suspendCompetitiveGroup()
    {
        return _dslPath.suspendCompetitiveGroup();
    }

    /**
     * @return Подавлять ошибки неполных данных.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getSuspendErrorDoc()
     */
    public static PropertyPath<Boolean> suspendErrorDoc()
    {
        return _dslPath.suspendErrorDoc();
    }

    /**
     * @return Скрывать персональные данные.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getAbfuscateData()
     */
    public static PropertyPath<Boolean> abfuscateData()
    {
        return _dslPath.abfuscateData();
    }

    /**
     * @return Включенные в приказ.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getOrdersOfAdmission()
     */
    public static PropertyPath<Boolean> ordersOfAdmission()
    {
        return _dslPath.ordersOfAdmission();
    }

    /**
     * @return Приемные кампании.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getEnrollmentCampaign()
     */
    public static PropertyPath<String> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Тип справочника.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getSpravType()
     */
    public static EducationdirectionSpravType.Path<EducationdirectionSpravType> spravType()
    {
        return _dslPath.spravType();
    }

    /**
     * @return Есть ошибка при формировании пакета.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getGlobalError()
     */
    public static PropertyPath<Boolean> globalError()
    {
        return _dslPath.globalError();
    }

    /**
     * @return Примечание выгрузки.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getDescription()
     */
    public static PropertyPath<String> description()
    {
        return _dslPath.description();
    }

    /**
     * @return Пакет выгрузки.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getXmlPackage()
     */
    public static DatabaseFile.Path<DatabaseFile> xmlPackage()
    {
        return _dslPath.xmlPackage();
    }

    /**
     * @return Ошибки формирования пакета.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getXmlErrors()
     */
    public static DatabaseFile.Path<DatabaseFile> xmlErrors()
    {
        return _dslPath.xmlErrors();
    }

    /**
     * @return Номер пакета в ФИС.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getFisPkgNumber()
     */
    public static PropertyPath<String> fisPkgNumber()
    {
        return _dslPath.fisPkgNumber();
    }

    /**
     * @return Примечание из ФИС.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getFisPrim()
     */
    public static PropertyPath<String> fisPrim()
    {
        return _dslPath.fisPrim();
    }

    /**
     * @return Тип пакета-удаление данных. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#isDeletePkgType()
     */
    public static PropertyPath<Boolean> deletePkgType()
    {
        return _dslPath.deletePkgType();
    }

    /**
     * @return Удаление заявлений.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getDeleteApplication()
     */
    public static PropertyPath<Boolean> deleteApplication()
    {
        return _dslPath.deleteApplication();
    }

    /**
     * @return Удаление включенных в приказ.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getDeleteOrdersOfAdmission()
     */
    public static PropertyPath<Boolean> deleteOrdersOfAdmission()
    {
        return _dslPath.deleteOrdersOfAdmission();
    }

    /**
     * @return Удаление направлений подготовки конкурсных групп.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getDeleteCompetitiveGroupItems()
     */
    public static PropertyPath<Boolean> deleteCompetitiveGroupItems()
    {
        return _dslPath.deleteCompetitiveGroupItems();
    }

    /**
     * @return Удаление результатов вступительных испытаний.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getDeleteEntranceTestResults()
     */
    public static PropertyPath<Boolean> deleteEntranceTestResults()
    {
        return _dslPath.deleteEntranceTestResults();
    }

    /**
     * @return Удаление общих льгот абитуриента.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getDeleteApplicationCommonBenefits()
     */
    public static PropertyPath<Boolean> deleteApplicationCommonBenefits()
    {
        return _dslPath.deleteApplicationCommonBenefits();
    }

    public static class Path<E extends FisPackages> extends EntityPath<E>
    {
        private PropertyPath<Date> _formingDate;
        private PropertyPath<Boolean> _campaignInfo;
        private PropertyPath<Boolean> _admissionInfo;
        private PropertyPath<Boolean> _applications;
        private PropertyPath<Boolean> _appDateWithoutTime;
        private PropertyPath<Boolean> _entranceTestResult;
        private PropertyPath<Boolean> _testApplicationMode;
        private PropertyPath<String> _testAppModeData;
        private PropertyPath<Boolean> _suspendCompetitiveGroup;
        private PropertyPath<Boolean> _suspendErrorDoc;
        private PropertyPath<Boolean> _abfuscateData;
        private PropertyPath<Boolean> _ordersOfAdmission;
        private PropertyPath<String> _enrollmentCampaign;
        private EducationdirectionSpravType.Path<EducationdirectionSpravType> _spravType;
        private PropertyPath<Boolean> _globalError;
        private PropertyPath<String> _description;
        private DatabaseFile.Path<DatabaseFile> _xmlPackage;
        private DatabaseFile.Path<DatabaseFile> _xmlErrors;
        private PropertyPath<String> _fisPkgNumber;
        private PropertyPath<String> _fisPrim;
        private PropertyPath<Boolean> _deletePkgType;
        private PropertyPath<Boolean> _deleteApplication;
        private PropertyPath<Boolean> _deleteOrdersOfAdmission;
        private PropertyPath<Boolean> _deleteCompetitiveGroupItems;
        private PropertyPath<Boolean> _deleteEntranceTestResults;
        private PropertyPath<Boolean> _deleteApplicationCommonBenefits;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата формирования пакета.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(FisPackagesGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Информация о приемной кампании.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getCampaignInfo()
     */
        public PropertyPath<Boolean> campaignInfo()
        {
            if(_campaignInfo == null )
                _campaignInfo = new PropertyPath<Boolean>(FisPackagesGen.P_CAMPAIGN_INFO, this);
            return _campaignInfo;
        }

    /**
     * @return Сведения об объеме и структуре приема.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getAdmissionInfo()
     */
        public PropertyPath<Boolean> admissionInfo()
        {
            if(_admissionInfo == null )
                _admissionInfo = new PropertyPath<Boolean>(FisPackagesGen.P_ADMISSION_INFO, this);
            return _admissionInfo;
        }

    /**
     * @return Заявления.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getApplications()
     */
        public PropertyPath<Boolean> applications()
        {
            if(_applications == null )
                _applications = new PropertyPath<Boolean>(FisPackagesGen.P_APPLICATIONS, this);
            return _applications;
        }

    /**
     * @return Дата регистрации заявления без времени.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getAppDateWithoutTime()
     */
        public PropertyPath<Boolean> appDateWithoutTime()
        {
            if(_appDateWithoutTime == null )
                _appDateWithoutTime = new PropertyPath<Boolean>(FisPackagesGen.P_APP_DATE_WITHOUT_TIME, this);
            return _appDateWithoutTime;
        }

    /**
     * @return Оценки в заявлении.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getEntranceTestResult()
     */
        public PropertyPath<Boolean> entranceTestResult()
        {
            if(_entranceTestResult == null )
                _entranceTestResult = new PropertyPath<Boolean>(FisPackagesGen.P_ENTRANCE_TEST_RESULT, this);
            return _entranceTestResult;
        }

    /**
     * @return Загрузка в тестовом режиме.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getTestApplicationMode()
     */
        public PropertyPath<Boolean> testApplicationMode()
        {
            if(_testApplicationMode == null )
                _testApplicationMode = new PropertyPath<Boolean>(FisPackagesGen.P_TEST_APPLICATION_MODE, this);
            return _testApplicationMode;
        }

    /**
     * @return Сведения о загрузке в тестовом режиме.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getTestAppModeData()
     */
        public PropertyPath<String> testAppModeData()
        {
            if(_testAppModeData == null )
                _testAppModeData = new PropertyPath<String>(FisPackagesGen.P_TEST_APP_MODE_DATA, this);
            return _testAppModeData;
        }

    /**
     * @return Принудительно отменить конкурсные группы.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getSuspendCompetitiveGroup()
     */
        public PropertyPath<Boolean> suspendCompetitiveGroup()
        {
            if(_suspendCompetitiveGroup == null )
                _suspendCompetitiveGroup = new PropertyPath<Boolean>(FisPackagesGen.P_SUSPEND_COMPETITIVE_GROUP, this);
            return _suspendCompetitiveGroup;
        }

    /**
     * @return Подавлять ошибки неполных данных.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getSuspendErrorDoc()
     */
        public PropertyPath<Boolean> suspendErrorDoc()
        {
            if(_suspendErrorDoc == null )
                _suspendErrorDoc = new PropertyPath<Boolean>(FisPackagesGen.P_SUSPEND_ERROR_DOC, this);
            return _suspendErrorDoc;
        }

    /**
     * @return Скрывать персональные данные.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getAbfuscateData()
     */
        public PropertyPath<Boolean> abfuscateData()
        {
            if(_abfuscateData == null )
                _abfuscateData = new PropertyPath<Boolean>(FisPackagesGen.P_ABFUSCATE_DATA, this);
            return _abfuscateData;
        }

    /**
     * @return Включенные в приказ.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getOrdersOfAdmission()
     */
        public PropertyPath<Boolean> ordersOfAdmission()
        {
            if(_ordersOfAdmission == null )
                _ordersOfAdmission = new PropertyPath<Boolean>(FisPackagesGen.P_ORDERS_OF_ADMISSION, this);
            return _ordersOfAdmission;
        }

    /**
     * @return Приемные кампании.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getEnrollmentCampaign()
     */
        public PropertyPath<String> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new PropertyPath<String>(FisPackagesGen.P_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Тип справочника.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getSpravType()
     */
        public EducationdirectionSpravType.Path<EducationdirectionSpravType> spravType()
        {
            if(_spravType == null )
                _spravType = new EducationdirectionSpravType.Path<EducationdirectionSpravType>(L_SPRAV_TYPE, this);
            return _spravType;
        }

    /**
     * @return Есть ошибка при формировании пакета.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getGlobalError()
     */
        public PropertyPath<Boolean> globalError()
        {
            if(_globalError == null )
                _globalError = new PropertyPath<Boolean>(FisPackagesGen.P_GLOBAL_ERROR, this);
            return _globalError;
        }

    /**
     * @return Примечание выгрузки.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getDescription()
     */
        public PropertyPath<String> description()
        {
            if(_description == null )
                _description = new PropertyPath<String>(FisPackagesGen.P_DESCRIPTION, this);
            return _description;
        }

    /**
     * @return Пакет выгрузки.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getXmlPackage()
     */
        public DatabaseFile.Path<DatabaseFile> xmlPackage()
        {
            if(_xmlPackage == null )
                _xmlPackage = new DatabaseFile.Path<DatabaseFile>(L_XML_PACKAGE, this);
            return _xmlPackage;
        }

    /**
     * @return Ошибки формирования пакета.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getXmlErrors()
     */
        public DatabaseFile.Path<DatabaseFile> xmlErrors()
        {
            if(_xmlErrors == null )
                _xmlErrors = new DatabaseFile.Path<DatabaseFile>(L_XML_ERRORS, this);
            return _xmlErrors;
        }

    /**
     * @return Номер пакета в ФИС.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getFisPkgNumber()
     */
        public PropertyPath<String> fisPkgNumber()
        {
            if(_fisPkgNumber == null )
                _fisPkgNumber = new PropertyPath<String>(FisPackagesGen.P_FIS_PKG_NUMBER, this);
            return _fisPkgNumber;
        }

    /**
     * @return Примечание из ФИС.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getFisPrim()
     */
        public PropertyPath<String> fisPrim()
        {
            if(_fisPrim == null )
                _fisPrim = new PropertyPath<String>(FisPackagesGen.P_FIS_PRIM, this);
            return _fisPrim;
        }

    /**
     * @return Тип пакета-удаление данных. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#isDeletePkgType()
     */
        public PropertyPath<Boolean> deletePkgType()
        {
            if(_deletePkgType == null )
                _deletePkgType = new PropertyPath<Boolean>(FisPackagesGen.P_DELETE_PKG_TYPE, this);
            return _deletePkgType;
        }

    /**
     * @return Удаление заявлений.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getDeleteApplication()
     */
        public PropertyPath<Boolean> deleteApplication()
        {
            if(_deleteApplication == null )
                _deleteApplication = new PropertyPath<Boolean>(FisPackagesGen.P_DELETE_APPLICATION, this);
            return _deleteApplication;
        }

    /**
     * @return Удаление включенных в приказ.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getDeleteOrdersOfAdmission()
     */
        public PropertyPath<Boolean> deleteOrdersOfAdmission()
        {
            if(_deleteOrdersOfAdmission == null )
                _deleteOrdersOfAdmission = new PropertyPath<Boolean>(FisPackagesGen.P_DELETE_ORDERS_OF_ADMISSION, this);
            return _deleteOrdersOfAdmission;
        }

    /**
     * @return Удаление направлений подготовки конкурсных групп.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getDeleteCompetitiveGroupItems()
     */
        public PropertyPath<Boolean> deleteCompetitiveGroupItems()
        {
            if(_deleteCompetitiveGroupItems == null )
                _deleteCompetitiveGroupItems = new PropertyPath<Boolean>(FisPackagesGen.P_DELETE_COMPETITIVE_GROUP_ITEMS, this);
            return _deleteCompetitiveGroupItems;
        }

    /**
     * @return Удаление результатов вступительных испытаний.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getDeleteEntranceTestResults()
     */
        public PropertyPath<Boolean> deleteEntranceTestResults()
        {
            if(_deleteEntranceTestResults == null )
                _deleteEntranceTestResults = new PropertyPath<Boolean>(FisPackagesGen.P_DELETE_ENTRANCE_TEST_RESULTS, this);
            return _deleteEntranceTestResults;
        }

    /**
     * @return Удаление общих льгот абитуриента.
     * @see ru.tandemservice.fisrmc.entity.FisPackages#getDeleteApplicationCommonBenefits()
     */
        public PropertyPath<Boolean> deleteApplicationCommonBenefits()
        {
            if(_deleteApplicationCommonBenefits == null )
                _deleteApplicationCommonBenefits = new PropertyPath<Boolean>(FisPackagesGen.P_DELETE_APPLICATION_COMMON_BENEFITS, this);
            return _deleteApplicationCommonBenefits;
        }

        public Class getEntityClass()
        {
            return FisPackages.class;
        }

        public String getEntityName()
        {
            return "fisPackages";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
