package ru.tandemservice.fisrmc.dao;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.fias.base.entity.ICitizenship;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.fisrmc.entity.DisciplineMinMark2Olimpiad;
import ru.tandemservice.fisrmc.entity.Educationdirectionfis;
import ru.tandemservice.fisrmc.entity.GroupEsForFis;
import ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt;
import ru.tandemservice.fisrmc.entity.catalog.*;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.catalog.*;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.orders.AbstractEntrantOrder;

import java.util.Date;
import java.util.List;

public interface ISpravDAO {

    public List<ICatalogItem> getEducationFormFis(List<DevelopForm> developForms);

    public List<ICatalogItem> getEducationLevelFis(List<EducationOrgUnit> eduOu);

    public Eceducationlevelfis getEceducationlevelfis(EducationOrgUnit ou);

    public Developformfis getDevelopformfis(DevelopForm developForm);

    public Educationdirectionfis getEducationdirectionfis
            (
                    EducationOrgUnit eduOu
                    , EducationdirectionSpravType spravType
            );

    public List<EnrollmentDirection> getTandemEnrollmentDirections(EducationLevels educationLevels, EnrollmentCampaign campaign);

    public List<EnrollmentDirection> getTandemEnrollmentDirections(XMLCompetitiveGroup xmlCompetitiveGroup);

    public Identitycardtypefis getIdentitycardtypeFis(IdentityCardType identityCardType);

    public Countryfis getCountryFis(ICitizenship citizenship);

    public Entrancedisciplinetypefis getEntrancedisciplinetypefis(EntranceDisciplineType entranceDisciplineType);

    public Ecdisciplinefis getEcdisciplinefis(EducationSubject educationSubject);

    public Ecdisciplinefis getEcdisciplinefis(StateExamSubject stateExamSubject);

    /**
     * Получить диплом участника олимпиады по справочнику фис
     *
     * @param diploma
     *
     * @return
     */
    public Olimpiatfis getOlimpiadFisByOlimpiadTu(OlympiadDiploma diploma);

    /**
     * Источники финансирования для направления
     * подготовки приема
     *
     * @param ed
     *
     * @return
     */
    public List<Compensationtypefis> getListCompensationtypefis(EnrollmentDirection ed);


    public List<String> getStageList
            (
                    Eceducationlevelfis eceducationlevelfis
                    , Developformfis developform
                    , Compensationtypefis compensationtype
            );

    /**
     * Дата приказа для
     * информации по приемной компании
     *
     * @param cd
     * @param ec
     *
     * @return
     */
    public Date getOrderDate(XmlCampaignDate cd, EnrollmentCampaign ec);


    /**
     * Направления подготовки в соответствии с приемной компанией
     *
     * @param lst
     *
     * @return
     */
    public List<XMLEnrollmentDirection> getEnrollmentDirections
    (
            List<EnrollmentCampaign> lst
            , EducationdirectionSpravType spravType
    );


    public CompetitionGroup getCompetitiongroup(Long id);

    public EnrollmentDirection getEnrollmentDirection(Long id);

    public Course getCourse(EnrollmentCampaign ec);

    public Ecrequestfis getEcrequestfis(EntrantState entrantState);

    /**
     * Вид возмещения затрат на выбранном направлении для приема
     *
     * @param red
     *
     * @return
     */
    public Compensationtypefis getCompensationtypefis
    (
            RequestedEnrollmentDirection red
            , RequestedEnrollmentDirectionExt redExt
    );

	/*
    public Compensationtypefis getCompensationtypefis
	(
			CompensationType compensationType 
	);
	*/

    public Sexfis getSexFis
            (
                    Sex sex
            );


    /**
     * Сбросить кеш
     */
    public void clearCache();

    public Makrsoursefis getMakrsoursefis(int finalMarkSource);

    public Competitionkindfis getCompetitionkindfis(
            CompetitionKind competitionKind);

    public IEntity getEntityByCode(Class clazz, String code);

    /**
     * Этап проведения приемной компании
     * на основе даты приказа
     *
     * @param order
     * @param enrollmentCampaign
     *
     * @return
     */
    public String getStageByOrder(AbstractEntrantOrder order, EnrollmentCampaign enrollmentCampaign);

    public Olimpiaddiplomatypefis getOlimpiaddiplomatypefis(
            OlympiadDiplomaDegree degree);

    /**
     * получить группу дисциплин
     *
     * @param educationSubject
     * @param directions
     *
     * @return
     */
    public List<GroupEsForFis> getGroupEsForFis(
            EducationSubject educationSubject
            , List<EnrollmentDirection> directions);


    public GroupEsForFis getSingleGroupEsForFis(
            EducationSubject educationSubject
            , List<EnrollmentDirection> directions);


    /**
     * Получить заглушку дисциплины
     *
     * @param groupDiscipline
     *
     * @return
     */
    public Ecdisciplinefis getEcdisciplinefisForGroup
    (
            GroupEsForFis groupDiscipline);


    public DisciplineMinMark2Olimpiad getDisciplineMinMark2Olimpiad(Ecdisciplinefis ecdisciplinefis, EnrollmentCampaign ec);

}
