package ru.tandemservice.fisrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.fisrmc.entity.DisciplineMinMark2Olimpiad;
import ru.tandemservice.fisrmc.entity.catalog.Ecdisciplinefis;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Минимальный балл для зачтения олимпиады
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DisciplineMinMark2OlimpiadGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.fisrmc.entity.DisciplineMinMark2Olimpiad";
    public static final String ENTITY_NAME = "disciplineMinMark2Olimpiad";
    public static final int VERSION_HASH = 588251642;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_ECDISCIPLINEFIS = "ecdisciplinefis";
    public static final String P_MIN_MARK = "minMark";

    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private Ecdisciplinefis _ecdisciplinefis;     // Общеобразовательные предметы
    private Integer _minMark;     // Минимальный балл

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Общеобразовательные предметы. Свойство не может быть null.
     */
    @NotNull
    public Ecdisciplinefis getEcdisciplinefis()
    {
        return _ecdisciplinefis;
    }

    /**
     * @param ecdisciplinefis Общеобразовательные предметы. Свойство не может быть null.
     */
    public void setEcdisciplinefis(Ecdisciplinefis ecdisciplinefis)
    {
        dirty(_ecdisciplinefis, ecdisciplinefis);
        _ecdisciplinefis = ecdisciplinefis;
    }

    /**
     * @return Минимальный балл.
     */
    public Integer getMinMark()
    {
        return _minMark;
    }

    /**
     * @param minMark Минимальный балл.
     */
    public void setMinMark(Integer minMark)
    {
        dirty(_minMark, minMark);
        _minMark = minMark;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DisciplineMinMark2OlimpiadGen)
        {
            setEnrollmentCampaign(((DisciplineMinMark2Olimpiad)another).getEnrollmentCampaign());
            setEcdisciplinefis(((DisciplineMinMark2Olimpiad)another).getEcdisciplinefis());
            setMinMark(((DisciplineMinMark2Olimpiad)another).getMinMark());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DisciplineMinMark2OlimpiadGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DisciplineMinMark2Olimpiad.class;
        }

        public T newInstance()
        {
            return (T) new DisciplineMinMark2Olimpiad();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "ecdisciplinefis":
                    return obj.getEcdisciplinefis();
                case "minMark":
                    return obj.getMinMark();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "ecdisciplinefis":
                    obj.setEcdisciplinefis((Ecdisciplinefis) value);
                    return;
                case "minMark":
                    obj.setMinMark((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "ecdisciplinefis":
                        return true;
                case "minMark":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "ecdisciplinefis":
                    return true;
                case "minMark":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "ecdisciplinefis":
                    return Ecdisciplinefis.class;
                case "minMark":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DisciplineMinMark2Olimpiad> _dslPath = new Path<DisciplineMinMark2Olimpiad>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DisciplineMinMark2Olimpiad");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.DisciplineMinMark2Olimpiad#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Общеобразовательные предметы. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.DisciplineMinMark2Olimpiad#getEcdisciplinefis()
     */
    public static Ecdisciplinefis.Path<Ecdisciplinefis> ecdisciplinefis()
    {
        return _dslPath.ecdisciplinefis();
    }

    /**
     * @return Минимальный балл.
     * @see ru.tandemservice.fisrmc.entity.DisciplineMinMark2Olimpiad#getMinMark()
     */
    public static PropertyPath<Integer> minMark()
    {
        return _dslPath.minMark();
    }

    public static class Path<E extends DisciplineMinMark2Olimpiad> extends EntityPath<E>
    {
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private Ecdisciplinefis.Path<Ecdisciplinefis> _ecdisciplinefis;
        private PropertyPath<Integer> _minMark;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.DisciplineMinMark2Olimpiad#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Общеобразовательные предметы. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.DisciplineMinMark2Olimpiad#getEcdisciplinefis()
     */
        public Ecdisciplinefis.Path<Ecdisciplinefis> ecdisciplinefis()
        {
            if(_ecdisciplinefis == null )
                _ecdisciplinefis = new Ecdisciplinefis.Path<Ecdisciplinefis>(L_ECDISCIPLINEFIS, this);
            return _ecdisciplinefis;
        }

    /**
     * @return Минимальный балл.
     * @see ru.tandemservice.fisrmc.entity.DisciplineMinMark2Olimpiad#getMinMark()
     */
        public PropertyPath<Integer> minMark()
        {
            if(_minMark == null )
                _minMark = new PropertyPath<Integer>(DisciplineMinMark2OlimpiadGen.P_MIN_MARK, this);
            return _minMark;
        }

        public Class getEntityClass()
        {
            return DisciplineMinMark2Olimpiad.class;
        }

        public String getEntityName()
        {
            return "disciplineMinMark2Olimpiad";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
