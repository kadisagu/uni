package ru.tandemservice.fisrmc.component.catalog.ecrequestfis.EcrequestfisAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.fisrmc.entity.catalog.Ecrequestfis;

public interface IDAO extends IDefaultCatalogAddEditDAO<Ecrequestfis, Model> {

}
