package ru.tandemservice.fisrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Статус проверки свидетельства ЕГЭ"
 * Имя сущности : fisEgeStatusCode
 * Файл data.xml : catalogs.data.xml
 */
public interface FisEgeStatusCodeCodes
{
    /** Константа кода (code) элемента : ошибок нет (title) */
    String NO_ERROR = "0";
    /** Константа кода (code) элемента : Общая ошибка (title) */
    String ERROR_COMMON = "1";
    /** Константа кода (code) элемента : Ошибка в оценках (title) */
    String ERROR_MARK = "2";

    Set<String> CODES = ImmutableSet.of(NO_ERROR, ERROR_COMMON, ERROR_MARK);
}
