package ru.tandemservice.fisrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.fisrmc.entity.EnrollmentCampaignFisExt;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Приемная кампания (расширенные настройки ФИС)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrollmentCampaignFisExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.fisrmc.entity.EnrollmentCampaignFisExt";
    public static final String ENTITY_NAME = "enrollmentCampaignFisExt";
    public static final int VERSION_HASH = -2093993510;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_REQUEST_PREFIX = "requestPrefix";

    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private String _requestPrefix;     // Префикс номера заявления абитуриента

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null и должно быть уникальным.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Префикс номера заявления абитуриента.
     */
    @Length(max=255)
    public String getRequestPrefix()
    {
        return _requestPrefix;
    }

    /**
     * @param requestPrefix Префикс номера заявления абитуриента.
     */
    public void setRequestPrefix(String requestPrefix)
    {
        dirty(_requestPrefix, requestPrefix);
        _requestPrefix = requestPrefix;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrollmentCampaignFisExtGen)
        {
            setEnrollmentCampaign(((EnrollmentCampaignFisExt)another).getEnrollmentCampaign());
            setRequestPrefix(((EnrollmentCampaignFisExt)another).getRequestPrefix());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrollmentCampaignFisExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrollmentCampaignFisExt.class;
        }

        public T newInstance()
        {
            return (T) new EnrollmentCampaignFisExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "requestPrefix":
                    return obj.getRequestPrefix();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "requestPrefix":
                    obj.setRequestPrefix((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "requestPrefix":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "requestPrefix":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "requestPrefix":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrollmentCampaignFisExt> _dslPath = new Path<EnrollmentCampaignFisExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrollmentCampaignFisExt");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.fisrmc.entity.EnrollmentCampaignFisExt#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Префикс номера заявления абитуриента.
     * @see ru.tandemservice.fisrmc.entity.EnrollmentCampaignFisExt#getRequestPrefix()
     */
    public static PropertyPath<String> requestPrefix()
    {
        return _dslPath.requestPrefix();
    }

    public static class Path<E extends EnrollmentCampaignFisExt> extends EntityPath<E>
    {
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<String> _requestPrefix;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.fisrmc.entity.EnrollmentCampaignFisExt#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Префикс номера заявления абитуриента.
     * @see ru.tandemservice.fisrmc.entity.EnrollmentCampaignFisExt#getRequestPrefix()
     */
        public PropertyPath<String> requestPrefix()
        {
            if(_requestPrefix == null )
                _requestPrefix = new PropertyPath<String>(EnrollmentCampaignFisExtGen.P_REQUEST_PREFIX, this);
            return _requestPrefix;
        }

        public Class getEntityClass()
        {
            return EnrollmentCampaignFisExt.class;
        }

        public String getEntityName()
        {
            return "enrollmentCampaignFisExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
