package ru.tandemservice.fisrmc.dao;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.ICitizenship;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt;
import ru.tandemservice.fisrmc.entity.*;
import ru.tandemservice.fisrmc.entity.catalog.*;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.catalog.*;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.orders.AbstractEntrantOrder;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


public class SpravDao
        extends UniBaseDao
        implements ISpravDAO
{


    private static ISpravDAO _ISpravDAO = null;

    public static ISpravDAO Instanse()
    {


        if (_ISpravDAO == null)
            _ISpravDAO = (ISpravDAO) ApplicationRuntime.getBean("spravDao");
        return _ISpravDAO;
    }

    private Map<CompensationType, Compensationtypefis> mapCompensationType = new HashMap<>();
    private Map<EntrantState, Ecrequestfis> mapEntrantState = new HashMap<>();
    private Map<DevelopForm, Developformfis> mapDevelopForm = new HashMap<>();


    private Map<Long, IEntity> mapEntity = new HashMap<>();
    private Map<String, IEntity> mapEntityByCode = new HashMap<>();


    @Override
    /**
     * 14 справочник
     */
    public List<ICatalogItem> getEducationFormFis(List<DevelopForm> developForms)
    {
        List<ICatalogItem> retVal = new ArrayList<>();
        for (DevelopForm item : developForms) {
            Developformfis itemFis = getDevelopformfis(item);
            retVal.add(itemFis);
        }
        return retVal;
    }

    public Developformfis getDevelopformfis(DevelopForm developForm)
    {

        if (mapDevelopForm.containsKey(developForm))
            return mapDevelopForm.get(developForm);
        else {
            Developformfis itemFis = get(Developformfis.class, "codeTU", developForm.getCode());
            if (itemFis == null)
                throw new ApplicationException("Для справочника 14 ФИС не найдено соответствие по коду TU = " + developForm.getCode());

            mapDevelopForm.put(developForm, itemFis);
            return itemFis;
        }
    }


    public Eceducationlevelfis getEceducationlevelfis(EducationOrgUnit ou)
    {
        // ou.getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject().get
        String key = "Eceducationlevelfis" + Long.toString(ou.getId());

        if (mapEntityByCode.containsKey(key))
            return (Eceducationlevelfis) mapEntityByCode.get(key);
        // в 2014 году сопоставление через справочник qualificationToFis и eduProgramQualificationToFis
        // приоритет у eduProgramQualificationToFis

        Qualifications qualification = ou.getEducationLevelHighSchool().getEducationLevel().getQualification();
        EduProgramQualification programQualification = null;
        String qualCode = qualification.getCode();

        programQualification = ou.getEducationLevelHighSchool().getAssignedQualification();
        if (programQualification != null && programQualification.getTitle() != null)
            qualCode += " ОП Квалификация " + programQualification.getTitle();

        Eceducationlevelfis item = null;

        if (programQualification != null) {
            List<EduProgramQualificationToFis> rel2OP = getList(EduProgramQualificationToFis.class, EduProgramQualificationToFis.L_QUALIFICATIONS, programQualification);
            if (rel2OP != null && rel2OP.size() == 1)
                item = rel2OP.get(0).getEceducationlevelfis();

            if (rel2OP != null && rel2OP.size() > 1)
                throw new ApplicationException("Для квалификации " + qualCode + " есть неоднозначное сопоставление");
        }

        if (item == null) // даже если есть квалификация ОП, но сопоставления нет
        {
            // тогда по связи с НП
            List<QualificationToFis> rel2NP = getList(QualificationToFis.class, QualificationToFis.L_QUALIFICATIONS, qualification);
            if (rel2NP != null && rel2NP.size() == 1)
                item = rel2NP.get(0).getEceducationlevelfis();

            if (rel2NP != null && rel2NP.size() > 1)
                throw new ApplicationException("Для квалификации " + qualCode + " есть неоднозначное сопоставление");

        }


        if (item == null)
            throw new ApplicationException("Код квалификации " + qualCode + " не сопоставлен с кодами fis");
        else {
            mapEntityByCode.put(key, item);
            return item;
        }
    }

    @Override
    /**
     * 2-ой код справочника
     */
    public List<ICatalogItem> getEducationLevelFis
            (
                    List<EducationOrgUnit> eduOu
            )
    {
        List<ICatalogItem> retVal = new ArrayList<>();

        for (EducationOrgUnit ou : eduOu) {
            Eceducationlevelfis item = getEceducationlevelfis(ou);
            if (item != null) {
                if (!retVal.contains(item))
                    retVal.add(item);
            }
        }
        return retVal;
    }

    /**
     * id источника финансирования, справочник 15 ФИС
     * используем в датах проведения приемной кампани
     * <p/>
     * 14 = Бюджетные места
     * 20 = Квота приема лиц, имеющих особое право
     * 15 = С оплатой обучения
     * 16 = Целевой прием
     */
    public List<Compensationtypefis> getListCompensationtypefis(EnrollmentDirection ed)
    {

        // вычитаем расширенные настройки
        //EnrollmentDirectionExt enrollmentDirectionExt = get(EnrollmentDirectionExt.class, EnrollmentDirectionExt.L_ENROLLMENT_DIRECTION, ed);

        // целевой прием выделяем отдельно
        List<Compensationtypefis> retVal = new ArrayList<>();

        if (ed.getMinisterialPlan() != null && ed.getMinisterialPlan() > 0) {
            if (ed.getTargetAdmissionPlanBudget() != null && ed.getTargetAdmissionPlanBudget() >= ed.getMinisterialPlan()) {
                // только целевой прием
                Compensationtypefis item = getCompensationtypefisByCode("16");
                if (!retVal.contains(item))
                    retVal.add(item);
            }
            else {
                // и бюджет и целевой возможен

                // бюджет
                Compensationtypefis item = getCompensationtypefisByCode("14");
                if (!retVal.contains(item))
                    retVal.add(item);

                if (ed.getTargetAdmissionPlanBudget() != null && ed.getTargetAdmissionPlanBudget() > 0) {
                    // есть и целевой
                    Compensationtypefis itemTarget = getCompensationtypefisByCode("16");
                    if (!retVal.contains(itemTarget))
                        retVal.add(itemTarget);
                }
            }
        }

        if (ed.getContractPlan() != null && ed.getContractPlan() > 0) {
            if (ed.getTargetAdmissionPlanContract() != null && ed.getTargetAdmissionPlanContract() >= ed.getContractPlan()) {
                // только целевой прием
                Compensationtypefis item = getCompensationtypefisByCode("16");
                if (!retVal.contains(item))
                    retVal.add(item);
            }
            else {
                // и контракт и целевой возможен

                // контракт
                Compensationtypefis item = getCompensationtypefisByCode("15");
                if (!retVal.contains(item))
                    retVal.add(item);

                if (ed.getTargetAdmissionPlanContract() != null && ed.getTargetAdmissionPlanContract() > 0) {
                    // есть и целевой
                    Compensationtypefis itemTarget = getCompensationtypefisByCode("16");
                    if (!retVal.contains(itemTarget))
                        retVal.add(itemTarget);
                }
            }

        }

        // а может быть указаны и квоты особых студентов
        // тестирование на Тюмени показало, что такое дело использовать нельзя
        // для дат проведения ПК <vch> 2014/06/05

//		if (enrollmentDirectionExt!=null
//				&& 
//				(
//					   (enrollmentDirectionExt.getHaveSpecialRightsPlanBudget()!=null	&& enrollmentDirectionExt.getHaveSpecialRightsPlanBudget()>0)
//					|| (enrollmentDirectionExt.getHaveSpecialRightsPlanContract()!=null	&& enrollmentDirectionExt.getHaveSpecialRightsPlanContract()>0)
//				)
//			   )
//		{
//			Compensationtypefis item = getCompensationtypefisByCode("20");
//			if (!retVal.contains(item))
//				retVal.add(item);
//		}

        return retVal;
    }

    private Map<String, List<String>> mapStage = new HashMap<>();

    @Override
    public List<String> getStageList
            (
                    Eceducationlevelfis eceducationlevelfis
                    , Developformfis developform
                    , Compensationtypefis compensationtype
            )

    {

        // кешируем
        String key = Long.toString(eceducationlevelfis.getId())
                + Long.toString(developform.getId())
                + Long.toString(compensationtype.getId());

        if (mapStage.containsKey(key))
            return mapStage.get(key);
        else {

            boolean has = false;
            List<String> retVal = new ArrayList<>();

            // бакалавриат специалитет
            // для заочной формы stage не создаем
            if (
                    (compensationtype.getCode().equals("14")) && // только бюджет
                            !developform.getCode().equals("10") &&  // не заочная
                            // 2- бакалавр 5 - специалитет
                            (eceducationlevelfis.getCode().equals("2") || eceducationlevelfis.getCode().equals("5") || eceducationlevelfis.getCode().equals("19"))
                    )
            {
                has = true;
                retVal.add("1"); //<TODO> Оставляем только статус 2
                retVal.add("2");
            }

            if (!has)
                retVal.add("0");

            mapStage.put(key, retVal);
            return retVal;
        }
    }

    private Map<String, Date> mapStageDate = new HashMap<>();

    @Override
    /**
     * Даты приказов
     *
     */
    public Date getOrderDate(XmlCampaignDate cd, EnrollmentCampaign ec)
    {

        String cdKey = "";
        if (cd != null)
            cdKey = cd.getStage();
        String key = Long.toString(ec.getId()) + cdKey;


        if (mapStageDate.containsKey(key))
            return mapStageDate.get(key);
        else {
            // ищем по таблицам

            StageFis stage = null;
            stage = getByCode(StageFis.class, cdKey.equals("2") ? "2" : "1");

            List<EnrollmentCampaignStageFIS> stageDateList = new DQLSelectBuilder()
                    .fromEntity(EnrollmentCampaignStageFIS.class, "e")
                    .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentCampaignStageFIS.enrollmentCampaign().fromAlias("e")), DQLExpressions.value(ec)))
                    .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentCampaignStageFIS.stage().fromAlias("e")), DQLExpressions.value(stage)))
                    .createStatement(getSession()).list();

            Date retVal = null;
            if (!stageDateList.isEmpty()) {
                EnrollmentCampaignStageFIS row = stageDateList.get(0);
                if (row.getDateOrder() != null)
                    retVal = row.getDateOrder();
            }

            if (retVal != null)
                return retVal;
            else
                throw new ApplicationException("Ошибка получения даты приказа, проверте настройки этапов приемки");
        }
    }


    @Override
    public Course getCourse(EnrollmentCampaign ec)
    {
        //<TODO> в дальнейшем нужно честно расчитывать из приемки
        // всегда 1
        if (mapEntity.containsKey(ec.getId()))
            return (Course) mapEntity.get(ec.getId());

        Course retVal = DevelopGridDAO.getCourseMap().get(1);

        mapEntity.put(ec.getId(), retVal);
        return retVal;
    }


    public List<XMLEnrollmentDirection> getEnrollmentDirections(List<EnrollmentCampaign> lst, EducationdirectionSpravType spravType)
    {

        List<XMLEnrollmentDirection> xmlDirections = new ArrayList<>();

        List<EnrollmentDirection> directions = new DQLSelectBuilder()
                .fromEntity(EnrollmentDirection.class, "e")
                .where(DQLExpressions.in(DQLExpressions.property(EnrollmentDirection.enrollmentCampaign().fromAlias("e")), lst))
                .createStatement(getSession()).list();

        Map<XMLEnrollmentDirectionKey, List<EnrollmentDirection>> map = new HashMap<>();

        if (directions != null)
            for (EnrollmentDirection enrollmentDirection : directions) {
                EnrollmentCampaign ec = enrollmentDirection.getEnrollmentCampaign();
                Course corseTU = getCourse(ec);
                Eceducationlevelfis eduLevel = getEceducationlevelfis(enrollmentDirection.getEducationOrgUnit());
                Educationdirectionfis edfis = getEducationdirectionfis(enrollmentDirection.getEducationOrgUnit(), spravType);

                if (edfis == null)
                    continue;


                XMLEnrollmentDirectionKey keyMap = new XMLEnrollmentDirectionKey(ec
                        , corseTU, eduLevel, edfis);

                List<EnrollmentDirection> lstxed = null;

                if (map.containsKey(keyMap))
                    lstxed = map.get(keyMap);
                else {
                    lstxed = new ArrayList<>();
                    map.put(keyMap, lstxed);
                }
                lstxed.add(enrollmentDirection);
            }

        for (XMLEnrollmentDirectionKey keyMap : map.keySet()) {
            List<EnrollmentDirection> enrollmentDirectionsList = map.get(keyMap);
            XMLEnrollmentDirection xed = new XMLEnrollmentDirection(
                    keyMap.getEceducationlevelfis()
                    , keyMap.getEducationdirectionfis()
                    , keyMap.getEnrollmentCampaign()
                    , enrollmentDirectionsList);

            xmlDirections.add(xed);
        }
        return xmlDirections;
    }


    @Override
    public CompetitionGroup getCompetitiongroup(Long id) {

        return get(CompetitionGroup.class, id);
    }


    @Override
    public EnrollmentDirection getEnrollmentDirection(Long id) {

        return get(EnrollmentDirection.class, id);
    }

    /**
     * пока нет уверенности в том что
     * связь один-к-одному, поэтому беру список
     * и первый элемент
     */
    @Override
    public Educationdirectionfis getEducationdirectionfis(
            EducationOrgUnit eduOu
            , EducationdirectionSpravType spravType)
    {
        // проверяем в кеше (в 2014 году к коду прибавим тип справочника)
        String key = "Educationdirectionfis" + Long.toString(eduOu.getId()) + "st=" + Long.toString(spravType.getId());

        if (mapEntityByCode.containsKey(key))
            return (Educationdirectionfis) mapEntityByCode.get(key);

        else {

            //List<EdOrgunit2DirectionFis> lst = getList(EdOrgunit2DirectionFis.class,EdOrgunit2DirectionFis.L_EDUCATION_ORG_UNIT, eduOu);
            List<EdOrgunit2DirectionFis> lst = new DQLSelectBuilder()
                    .fromEntity(EdOrgunit2DirectionFis.class, "e")
                    .where(DQLExpressions.eq(DQLExpressions.property(EdOrgunit2DirectionFis.educationOrgUnit().fromAlias("e")), DQLExpressions.value(eduOu)))
                    .where(DQLExpressions.eq(DQLExpressions.property(EdOrgunit2DirectionFis.educationDirectionFis().spravType().fromAlias("e")), DQLExpressions.value(spravType)))
                    .createStatement(getSession()).list();


            Educationdirectionfis retVal = null;
            for (EdOrgunit2DirectionFis edf : lst) {
                if (edf.getEducationDirectionFis() != null)
                    retVal = edf.getEducationDirectionFis();
            }
            if (retVal != null)
                mapEntityByCode.put(key, retVal);
            return retVal;
        }
    }


    @Override
    public List<EnrollmentDirection> getTandemEnrollmentDirections(EducationLevels educationLevels, EnrollmentCampaign campaign) {

        List<EnrollmentDirection> enrollmentDirectionsList = new DQLSelectBuilder()
                .fromEntity(EnrollmentDirection.class, "e")
                .where(DQLExpressions.in(DQLExpressions.property(EnrollmentDirection.enrollmentCampaign().fromAlias("e")), DQLExpressions.value(campaign)))
                .where(DQLExpressions.in(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().educationLevel().fromAlias("e")), DQLExpressions.value(educationLevels)))
                .createStatement(getSession()).list();

        return enrollmentDirectionsList;
    }


    @Override
    public List<EnrollmentDirection> getTandemEnrollmentDirections(XMLCompetitiveGroup xmlCompetitiveGroup) {
        if (xmlCompetitiveGroup.getUid() == null)
            return Collections.emptyList();

        IEntity compGroup = get(Long.valueOf(xmlCompetitiveGroup.getUid()));

        List<EnrollmentDirection> directions = new ArrayList<>();

        //Если используются КГ
        if (compGroup instanceof CompetitionGroup) {
            //Получаем НП на основе конкурсной группы
            directions = getList(EnrollmentDirection.class, EnrollmentDirection.competitionGroup(), (CompetitionGroup) compGroup);

        }
        else {
            //иначе НП = КГ (случай, когда в приемной компании не используются конкурсные группы)
            directions.add(get(EnrollmentDirection.class, Long.valueOf(xmlCompetitiveGroup.getUid())));
        }
        return directions;
    }

    Map<String, Identitycardtypefis> mapIdentitycardtypefis = new HashMap<>();

    /**
     * Тип цдостоверения личности для фис
     */
    @Override
    public Identitycardtypefis getIdentitycardtypeFis(IdentityCardType identityCardType) {

        String key = identityCardType.getCode();

        if (mapIdentitycardtypefis.containsKey(key))
            return mapIdentitycardtypefis.get(key);

        List<Identitycardtypefis> identitycardtypefis = getList(Identitycardtypefis.class, Identitycardtypefis.codeTU(), identityCardType.getCode());
        if (identitycardtypefis.isEmpty())
            throw new ApplicationException("Не найдено сопоставление в справочнике Тип документа, удостоверяющего личность(ФИС), поля: " + identityCardType.getTitle() + ", " + identityCardType.getCode());

        mapIdentitycardtypefis.put(key, identitycardtypefis.get(0));

        return identitycardtypefis.get(0);
    }


    @Override
    public Countryfis getCountryFis(ICitizenship citizenship) {
        if (citizenship == null || !(citizenship instanceof AddressCountry))
            return null;

        if (mapEntity.containsKey(citizenship.getId()))
            return (Countryfis) mapEntity.get(citizenship.getId());

        List<Countryfis> countryfis = getList(Countryfis.class, Countryfis.codeTU(), String.valueOf(citizenship.getCode()));
        if (countryfis.isEmpty())
            throw new ApplicationException("Не найдено сопоставление в справочнике Страны(ФИС), поля: " + citizenship.getTitle() + ", " + citizenship.getCode());

        mapEntity.put(citizenship.getId(), countryfis.get(0));
        return countryfis.get(0);
    }

    @Override
    public Ecrequestfis getEcrequestfis
            (
                    EntrantState entrantState
            )
    {

        if (mapEntrantState.containsKey(entrantState))
            return mapEntrantState.get(entrantState);
        else {

            List<Entrantstate2entrantstateFis> lst
                    = getList(Entrantstate2entrantstateFis.class, Entrantstate2entrantstateFis.L_ENTRANT_STATE, entrantState);

            Ecrequestfis retVal = null;
            for (Entrantstate2entrantstateFis edf : lst) {
                if (edf.getEcrequestfis() != null)
                    retVal = edf.getEcrequestfis();
            }


            if (retVal == null)
                throw new ApplicationException("Для сущности 'Статус заявления из TU не найдено соответствия в ФИС' TU=" + entrantState.getTitle());

            mapEntrantState.put(entrantState, retVal);
            return retVal;
        }
    }

    private Map<String, Compensationtypefis> mapCompensationtypefisByCode = new HashMap<>();

    private Compensationtypefis getCompensationtypefisByCode(String code)
    {
        if (mapCompensationtypefisByCode.containsKey(code))
            return mapCompensationtypefisByCode.get(code);

        Compensationtypefis retVal = getByCode(Compensationtypefis.class, code);
        mapCompensationtypefisByCode.put(code, retVal);

        return retVal;
    }


    @Override
    public Compensationtypefis getCompensationtypefis
            (
                    RequestedEnrollmentDirection red
                    , RequestedEnrollmentDirectionExt redExt
            )
    {
        if (red.isTargetAdmission())
            return getCompensationtypefisByCode("16");

        // проверка на особые права
        // у нас может быть возмещение затрат - бюджет, но в направлении признак - особые права (что делать?)
        // хрен его знает
        if (redExt != null && redExt.isHaveSpecialRights())
            return getCompensationtypefisByCode("20");

        return getCompensationtypefis(red.getCompensationType());
    }

    private Compensationtypefis getCompensationtypefis
            (
                    CompensationType compensationType
            )
    {
        Compensationtypefis itemFis;

        if (mapCompensationType.containsKey(compensationType))
            return mapCompensationType.get(compensationType);
        else
            itemFis = get(Compensationtypefis.class, Compensationtypefis.P_CODE_T_U, compensationType.getCode());

        if (itemFis == null)
            throw new ApplicationException("Для справочника ФИС Compensationtypefis  не найдено соответствие по коду TU = " + compensationType.getCode());
        else {
            mapCompensationType.put(compensationType, itemFis);
            return itemFis;
        }


    }

    @Override
    public Entrancedisciplinetypefis getEntrancedisciplinetypefis(EntranceDisciplineType entranceDisciplineType)
    {
        Long id = entranceDisciplineType.getId();
        if (mapEntity.containsKey(id))
            return (Entrancedisciplinetypefis) mapEntity.get(id);

        EntranceDisciplineType2Entrancedisciplinetypefis item = get(EntranceDisciplineType2Entrancedisciplinetypefis.class, EntranceDisciplineType2Entrancedisciplinetypefis.L_ENTRANCE_DISCIPLINE_TYPE, entranceDisciplineType);
        if (item == null || item.getEntrancedisciplinetypefis() == null)
            throw new ApplicationException("Не найдено сопоставление в справочнике Entrancedisciplinetypefis c объектом EntranceDisciplineType EntranceDisciplineType.title=" + entranceDisciplineType.getTitle());

        mapEntity.put(id, item.getEntrancedisciplinetypefis());
        return item.getEntrancedisciplinetypefis();
    }


    Map<String, Ecdisciplinefis> mapEcdisciplinefis = new HashMap<>();

    @Override
    public Ecdisciplinefis getEcdisciplinefis(EducationSubject educationSubject)
    {
        String key = educationSubject.getCode();
        if (mapEcdisciplinefis.containsKey(key))
            return mapEcdisciplinefis.get(key);

        EducationSubject2Ecdisciplinefis item = get(EducationSubject2Ecdisciplinefis.class, EducationSubject2Ecdisciplinefis.L_EDUCATION_SUBJECT, educationSubject);
        if (item == null || item.getEcdisciplinefis() == null) {
            /**
             * Если в справочнике не найден Ecdisciplinefis,
             * можно создать new Ecdisciplinefis()
             * Ecdisciplinefis.id() = null
             * Ecdisciplinefis.title() = EducationSubject.title()
             */
            Ecdisciplinefis retVal = new Ecdisciplinefis();
            retVal.setId(null);

            retVal.setCode("fd" + educationSubject.getCode());

            retVal.setCodeTU(educationSubject.getCode());
            retVal.setTitle(educationSubject.getTitle());

            mapEcdisciplinefis.put(key, retVal);

            return retVal;
        }
        else {
            Ecdisciplinefis retVal = item.getEcdisciplinefis();

            // ни в коем случае не переписывать!!!
            // возникает косяк при сопоставлении
            // retVal.setCodeTU(educationSubject.getCode());
            mapEcdisciplinefis.put(key, retVal);

            return retVal;
        }
    }


    Map<Long, Ecdisciplinefis> mapEcdisciplinefisForGroup = new HashMap<>();

    /**
     * фейковая дисциплина для группы
     */
    public Ecdisciplinefis getEcdisciplinefisForGroup
    (
            GroupEsForFis groupDiscipline
    )
    {
        Long key = groupDiscipline.getId();

        if (mapEcdisciplinefisForGroup.containsKey(key))
            return mapEcdisciplinefisForGroup.get(key);
        else {
            // делаем фейк
            /**
             * Если в справочнике не найден Ecdisciplinefis,
             * можно создать new Ecdisciplinefis()
             * Ecdisciplinefis.id() = null
             * Ecdisciplinefis.title() = EducationSubject.title()
             */
            Ecdisciplinefis retVal = new Ecdisciplinefis();
            retVal.setId(null);
            retVal.setCode("hk" + Integer.toString(groupDiscipline.getId().hashCode()));
            retVal.setCodeTU(Long.toString(groupDiscipline.getId()));
            retVal.setTitle(groupDiscipline.getGroupName());

            mapEcdisciplinefisForGroup.put(key, retVal);

            return retVal;
        }
    }


    private Map<String, Ecdisciplinefis> mapEducationSubject2Ecdisciplinefis = new HashMap<>();

    @Override
    /**
     * По предмету ЕГЭ (справочник вуза)
     * получаем предмет ЕГЭ (по фис)
     */
    public Ecdisciplinefis getEcdisciplinefis(StateExamSubject stateExamSubject)
    {
        String key = stateExamSubject.getCode();

        if (mapEducationSubject2Ecdisciplinefis.containsKey(key))
            return mapEducationSubject2Ecdisciplinefis.get(key);


        /**
         * связь один к одному
         * если сопоставления нет - ошибка
         */
        StateExamSubject2Ecdisciplinefis item = get(StateExamSubject2Ecdisciplinefis.class, StateExamSubject2Ecdisciplinefis.L_STATE_EXAM_SUBJECT, stateExamSubject);
        if (item == null || item.getEcdisciplinefis() == null)
            throw new ApplicationException("Не найдено сопоставление в справочнике EducationSubject c объектом stateExamSubject stateExamSubject.title=" + stateExamSubject.getTitle());

        mapEducationSubject2Ecdisciplinefis.put(key, item.getEcdisciplinefis());

        return item.getEcdisciplinefis();
    }

    Map<String, Sexfis> mapSexFis = new HashMap<>();

    public Sexfis getSexFis
            (
                    Sex sex
            )
    {
        if (sex == null)
            return null;

        String key = sex.getCode();
        if (mapSexFis.containsKey(key))
            return mapSexFis.get(key);


        // на основании кода
        Sexfis item = get(Sexfis.class, Sexfis.P_CODE_T_U, sex.getCode());

        if (item == null)
            throw new ApplicationException("Не найдено сопоставление в справочнике Sexfis c объектом Sex code=" + sex.getCode());

        mapSexFis.put(key, item);
        return item;

    }

    @Override
    public Competitionkindfis getCompetitionkindfis
            (
                    CompetitionKind competitionKind
            )
    {
        if (competitionKind == null)
            return null;

        if (mapEntity.containsKey(competitionKind.getId()))
            return (Competitionkindfis) mapEntity.get(competitionKind.getId());

        Competitionkindfis retVal = null;

        // без вступительных испытаний
        if (competitionKind.getCode().equals("1"))
            retVal = getByCode(Competitionkindfis.class, "1");

        // вне конкурса
        if (competitionKind.getCode().equals("3"))
            retVal = getByCode(Competitionkindfis.class, "4");

        if (retVal != null)
            mapEntity.put(competitionKind.getId(), retVal);

        return retVal;
    }

    public IEntity getEntityByCode(Class clazz, String code)
    {
        String key = clazz.getName() + code;
        if (mapEntityByCode.containsKey(key))
            return mapEntityByCode.get(key);
        else {
            IEntity entity = getByCode(clazz, code);
            mapEntityByCode.put(key, entity);
            return entity;
        }
    }


    public Olimpiatfis getOlimpiadFisByOlimpiadTu(OlympiadDiploma diploma)
    {
        Long key = diploma.getId();

        if (mapEntity.containsKey(key)) {
            if (mapEntity.get(key) == null)
                return null;
            else
                return (Olimpiatfis) mapEntity.get(key);
        }
        else {
            // нужно найте
            OlympiadDiploma2DiplomaFis o2oTU = get(OlympiadDiploma2DiplomaFis.class, OlympiadDiploma2DiplomaFis.L_OLYMPIAD_DIPLOMA, diploma);
            Olimpiatfis retVal = null;

            if (o2oTU != null)
                retVal = o2oTU.getOlimpiatfis();
            mapEntity.put(key, retVal);
            return retVal;
        }
    }

    public void clearCache()
    {
        mapCompensationType = new HashMap<>();
        mapEntrantState = new HashMap<>();
        mapDevelopForm = new HashMap<>();
        mapEcdisciplinefis = new HashMap<>();
        mapSexFis = new HashMap<>();
        mapEducationSubject2Ecdisciplinefis = new HashMap<>();
        mapIdentitycardtypefis = new HashMap<>();
        mapEntity = new HashMap<>();
        mapCompensationtypefisByCode = new HashMap<>();
        mapEntityByCode = new HashMap<>();
        mapStage = new HashMap<>();
        mapStageOrder = new HashMap<>();
        mapStageDate = new HashMap<>();
        mapDisciplineForGroup = null;
        mapEcdisciplinefisForGroup = new HashMap<>();

    }

    private Map<String, Makrsoursefis> _mapMakrsoursefis = new HashMap<>();

    @Override
    public Makrsoursefis getMakrsoursefis(int finalMarkSource)
    {
        /*
	    int FINAL_MARK_OLYMPIAD = 1;   // по диплому олимпиады
	    int FINAL_MARK_DISCIPLINE_EXAM = 2;      // по дисциплине (экзамен)
	    int FINAL_MARK_APPEAL_EXAM = 3;          // по апелляции (экзамен)
	    int FINAL_MARK_DISCIPLINE_TEST = 4;      // по дисциплине (тестирование)
	    int FINAL_MARK_APPEAL_TEST = 5;          // по апелляции (тестирование)
	    int FINAL_MARK_DISCIPLINE_INTERVIEW = 6; // по дисциплине (собеседование)
	    int FINAL_MARK_APPEAL_INTERVIEW = 7;     // по апелляции (собеседование)
	    int FINAL_MARK_STATE_EXAM_INTERNAL = 8;  // по дисциплине (ЕГЭ (вуз))
	    int FINAL_MARK_STATE_EXAM_INTERNAL_APPEAL = 9;  // по апелляции (ЕГЭ (вуз))
	    int FINAL_MARK_STATE_EXAM_WAVE1 = 10; // финальная оценка вычислена по ЕГЭ (1-ая волна)
	    int FINAL_MARK_STATE_EXAM_WAVE2 = 11; // финальная оценка вычислена по ЕГЭ (2-ая волна)
	    */

        String key = Integer.toString(finalMarkSource);
        if (_mapMakrsoursefis.containsKey(key))
            return _mapMakrsoursefis.get(key);

        Makrsoursefis ms = null;
        switch (finalMarkSource) {

            case 1:// по диплому олимпиады
                ms = getByCode(Makrsoursefis.class, "3");
                break;
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                ms = getByCode(Makrsoursefis.class, "2");
                break;
            case 8:
            case 9:
            case 10:
            case 11:
                ms = getByCode(Makrsoursefis.class, "1");
                break;
        }
        _mapMakrsoursefis.put(key, ms);
        return ms;
    }


    private Map<String, String> mapStageOrder = new HashMap<>();

    public String getStageByOrder(AbstractEntrantOrder order, EnrollmentCampaign enrollmentCampaign)
    {
        String key = Long.toString(order.getId())
                + Long.toString(enrollmentCampaign.getId());

        if (mapStageOrder.containsKey(key))
            return mapStageOrder.get(key);
        else {
            // Получим дату приказа
            // по дате приказа - запрос по убыванию - первая позиция - это и есть stage
            EnrollmentCampaign ec = get(EnrollmentCampaign.class, enrollmentCampaign.getId());
            AbstractEntrantOrder orderAbstract = get(AbstractEntrantOrder.class, order.getId());


            DQLSelectBuilder dql = new DQLSelectBuilder();
            dql.fromEntity(EnrollmentCampaignStageFIS.class, "cstage");
            dql.where(eq(property(EnrollmentCampaignStageFIS.enrollmentCampaign().fromAlias("cstage")), value(ec)));

            // дата <= даты приказа
            dql.where(le(property(EnrollmentCampaignStageFIS.dateOrder().fromAlias("cstage")), valueDate(orderAbstract.getCommitDate())));
            dql.order(property(EnrollmentCampaignStageFIS.dateOrder().fromAlias("cstage")), OrderDirection.desc);

            List<EnrollmentCampaignStageFIS> lst = getList(dql);

            String retVal = "1";

            if (!lst.isEmpty() && lst.size() > 0)
                retVal = lst.get(0).getStage().getCode();

            mapStageOrder.put(key, retVal);

            return retVal;
        }
    }


    public Olimpiaddiplomatypefis getOlimpiaddiplomatypefis
            (
                    OlympiadDiplomaDegree degree)
    {

        // 2 справочника полностью совпали по кодам
        Long key = degree.getId();
        if (mapEntity.containsKey(key))
            return (Olimpiaddiplomatypefis) mapEntity.get(key);
        else {
            Olimpiaddiplomatypefis retVal = get(Olimpiaddiplomatypefis.class, Olimpiaddiplomatypefis.P_CODE, degree.getCode());

            if (retVal == null)
                throw new ApplicationException("Не найдено сопоставление для справочника Olimpiaddiplomatypefis (справочник 18)");

            mapEntity.put(key, retVal);
            return retVal;
        }

    }


    Map<GroupEsForFis, Map<EnrollmentDirection, List<EducationSubject>>> mapDisciplineForGroup = null; //new HashMap<String, Map<EnrollmentDirection, List<EducationSubject>>>();

    /**
     * Получить группу дисциплин для объединения в ФИС
     */
    public List<GroupEsForFis> getGroupEsForFis(
            EducationSubject educationSubject
            , List<EnrollmentDirection> directions)
    {
        if (mapDisciplineForGroup == null)
            fillMapDisciplineFroup();

        List<GroupEsForFis> retVal = new ArrayList<>();

        if (directions == null || directions.isEmpty())
            return retVal;

        // ищем
        Set<GroupEsForFis> keyList = mapDisciplineForGroup.keySet();
        for (GroupEsForFis key : keyList) {
            Map<EnrollmentDirection, List<EducationSubject>> subMap = mapDisciplineForGroup.get(key);

            for (EnrollmentDirection ed : subMap.keySet()) {
                if (directions.contains(ed)) {
                    // есть смысл перебирать
                    List<EducationSubject> lst = subMap.get(ed);

                    if (lst.contains(educationSubject)) {
                        // есть дисциплина в наборе
                        if (!retVal.contains(key))
                            retVal.add(key);
                    }
                }
            }
        }
        return retVal;
    }

    public GroupEsForFis getSingleGroupEsForFis
            (
                    EducationSubject educationSubject
                    , List<EnrollmentDirection> directions
            )
    {

        List<GroupEsForFis> groupDisciplineList
                = getGroupEsForFis(educationSubject, directions);

        GroupEsForFis groupDiscipline = null;
        if (!groupDisciplineList.isEmpty())
            groupDiscipline = groupDisciplineList.get(0);

        if (groupDisciplineList.size() > 1) {
            String _edList = "";
            String _groupList = "";

            for (EnrollmentDirection ed : directions)
                _edList += ed.getTitle() + "; ";

            for (GroupEsForFis g : groupDisciplineList)
                _groupList += g.getGroupName() + "; ";
            // варнинг, пересечение сопоставления дисциплин
            PackUnpackToFis.AddPkgError("Для дисциплины " + educationSubject.getTitle() + " из набора ВИ для НП " + _edList + " " + "Найдено несколько сопоставлений с разными группами дисциплин: " + _groupList);
        }

        return groupDiscipline;

    }

    public DisciplineMinMark2Olimpiad getDisciplineMinMark2Olimpiad(Ecdisciplinefis ecdisciplinefis, EnrollmentCampaign enrollmentCampaign)
    {
        String codeEcd;
        if (ecdisciplinefis.getId() != null)
            codeEcd = ecdisciplinefis.getId().toString();
        else
            codeEcd = ecdisciplinefis.getCode() + " " + ecdisciplinefis.getCodeTU();

        String code = codeEcd + ":" + enrollmentCampaign.getId().toString();

        if (mapEntityByCode.containsKey(code))
            return (DisciplineMinMark2Olimpiad) mapEntityByCode.get(code);
        {

            String err = "Для дисциплины " + ecdisciplinefis.getTitle() + " не указан минимальный балл для зачтения олимпиады";

            // ищем
            DQLSelectBuilder qList = new DQLSelectBuilder()
                    .fromEntity(DisciplineMinMark2Olimpiad.class, "en")
                    .where(DQLExpressions.eq(DQLExpressions.property(DisciplineMinMark2Olimpiad.enrollmentCampaign().fromAlias("en")), DQLExpressions.value(enrollmentCampaign)))
                    .where(DQLExpressions.eq(DQLExpressions.property(DisciplineMinMark2Olimpiad.ecdisciplinefis().fromAlias("en")), DQLExpressions.value(ecdisciplinefis)));

            DisciplineMinMark2Olimpiad ent = qList.createStatement(getSession()).uniqueResult();

            if (ent != null && ent.getMinMark() != null) {
                mapEntityByCode.put(code, ent);
                return ent;
            }
            else {

                PackUnpackToFis.AddPkgError(err);
                throw new ApplicationException(err);

            }


        }

    }

    /**
     * заполняем кеш групп дисциплин
     */
    private void fillMapDisciplineFroup()
    {

        mapDisciplineForGroup = new HashMap<>();


        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(GroupEsForFis.class, "e");

        List<GroupEsForFis> lst = getList(dql);

        for (GroupEsForFis group : lst) {

            String _groupName = group.getGroupName();
            if (_groupName != null) {
                Map<EnrollmentDirection, List<EducationSubject>> map;
                if (mapDisciplineForGroup.containsKey(group))
                    map = mapDisciplineForGroup.get(group);
                else {
                    map = new HashMap<>();
                    mapDisciplineForGroup.put(group, map);
                }

                List<EducationSubject> lstES;
                if (map.containsKey(group.getEnrollmentDirection()))
                    lstES = map.get(group.getEnrollmentDirection());
                else {
                    lstES = new ArrayList<>();
                    map.put(group.getEnrollmentDirection(), lstES);
                }

                // сами предметы
                DQLSelectBuilder dqlRef = new DQLSelectBuilder();
                dqlRef.fromEntity(GroupEsForFis2Es.class, "e");
                dqlRef.column("e");
                dqlRef.where(eq(property(GroupEsForFis2Es.groupEsForFis().fromAlias("e")), value(group)));

                List<GroupEsForFis2Es> lstSubject = getList(dqlRef);

                for (GroupEsForFis2Es gef : lstSubject) {
                    if (!lstES.contains(gef.getEducationSubject()))
                        lstES.add(gef.getEducationSubject());
                }
            }
        }
    }

}
