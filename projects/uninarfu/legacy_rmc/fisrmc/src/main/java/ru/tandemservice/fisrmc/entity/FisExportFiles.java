package ru.tandemservice.fisrmc.entity;

import ru.tandemservice.fisrmc.entity.gen.FisExportFilesGen;

/**
 * Файл из ФИС, содержащий полные сведения по ОУ (включая описание приемок, направлений подготовки, заявлений и т.д.)
 */
public class FisExportFiles extends FisExportFilesGen
{
}