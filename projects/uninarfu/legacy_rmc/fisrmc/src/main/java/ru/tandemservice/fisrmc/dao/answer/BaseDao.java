package ru.tandemservice.fisrmc.dao.answer;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import ru.tandemservice.fisrmc.dao.PackUnpackToFis;
import ru.tandemservice.fisrmc.entity.FisAnswerAction;
import ru.tandemservice.fisrmc.entity.FisAnswerTofisPackages;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class BaseDao extends UniBaseDao {
    public static final SimpleDateFormat SDF_NO_TIME = new SimpleDateFormat("yyyy-MM-dd");


    /**
     * Поиск EntrantRequest по номеру и дате
     *
     * @param number
     * @param date
     *
     * @return
     */
    protected EntrantRequest getEntrantRequest(Integer number, Date date)
    {

        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EntrantRequest.class, "ereq");
        dql.column("ereq");
        dql.where(eq(property(EntrantRequest.regNumber().fromAlias("ereq")), value(number)));

        List<EntrantRequest> list = dql.createStatement(getSession()).list();

        for (EntrantRequest entrantRequest : list) {
            if (SDF_NO_TIME.format(date).equals(SDF_NO_TIME.format(entrantRequest.getRegDate())))
                return entrantRequest;
        }

        return null;
    }

    /**
     * Сохраняет результат обработки
     *
     * @param answer
     * @param errorList
     * @param logList
     * @param comment
     */
    protected void saveAction(FisAnswerTofisPackages answer, List<String> errorList, List<String> logList, String comment) {
        //сохраняем результат обработки ответа
        FisAnswerAction action = new FisAnswerAction();

        action.setFisAnswerTofisPackages(answer);
        action.setDate(new Date());
        action.setErrorReport(saveReport(errorList));
        action.setActionReport(saveReport(logList));
        action.setComment(comment);

        save(action);
    }

    /**
     * Сохраняет rtf на основе списка сообщений
     *
     * @param list
     *
     * @return
     */
    protected DatabaseFile saveReport(List<String> list) {
        if (list == null || list.isEmpty())
            return null;

        TemplateDocument template = getCatalogItem(TemplateDocument.class, PackUnpackToFis.SUSPEND_ERRORS_TEMPLATE);
        RtfDocument resultRtf = new RtfReader().read(template.getContent());
        RtfTableModifier tableModifier = new RtfTableModifier();

        String[][] tableData = new String[list.size()][1];
        int idx = 0;
        for (String err : list) {
            tableData[idx][0] = err;
            idx++;
        }
        tableModifier.put("T", tableData);
        tableModifier.modify(resultRtf);

        DatabaseFile dFile = new DatabaseFile();
        dFile.setContent(RtfUtil.toByteArray(resultRtf));
        dFile.setFilename("rtf");
        saveOrUpdate(dFile);

        return dFile;
    }
}
