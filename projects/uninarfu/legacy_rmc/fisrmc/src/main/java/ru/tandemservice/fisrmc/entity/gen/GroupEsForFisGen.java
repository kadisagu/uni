package ru.tandemservice.fisrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.fisrmc.entity.GroupEsForFis;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Группы дисциплин для передачи в ФИС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class GroupEsForFisGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.fisrmc.entity.GroupEsForFis";
    public static final String ENTITY_NAME = "groupEsForFis";
    public static final int VERSION_HASH = -30100504;
    private static IEntityMeta ENTITY_META;

    public static final String P_GROUP_NAME = "groupName";
    public static final String L_ENROLLMENT_DIRECTION = "enrollmentDirection";

    private String _groupName;     // имя группы дисциплин
    private EnrollmentDirection _enrollmentDirection;     // Направление подготовки (специальность) приема

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return имя группы дисциплин. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getGroupName()
    {
        return _groupName;
    }

    /**
     * @param groupName имя группы дисциплин. Свойство не может быть null.
     */
    public void setGroupName(String groupName)
    {
        dirty(_groupName, groupName);
        _groupName = groupName;
    }

    /**
     * @return Направление подготовки (специальность) приема. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentDirection getEnrollmentDirection()
    {
        return _enrollmentDirection;
    }

    /**
     * @param enrollmentDirection Направление подготовки (специальность) приема. Свойство не может быть null.
     */
    public void setEnrollmentDirection(EnrollmentDirection enrollmentDirection)
    {
        dirty(_enrollmentDirection, enrollmentDirection);
        _enrollmentDirection = enrollmentDirection;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof GroupEsForFisGen)
        {
            setGroupName(((GroupEsForFis)another).getGroupName());
            setEnrollmentDirection(((GroupEsForFis)another).getEnrollmentDirection());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends GroupEsForFisGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) GroupEsForFis.class;
        }

        public T newInstance()
        {
            return (T) new GroupEsForFis();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "groupName":
                    return obj.getGroupName();
                case "enrollmentDirection":
                    return obj.getEnrollmentDirection();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "groupName":
                    obj.setGroupName((String) value);
                    return;
                case "enrollmentDirection":
                    obj.setEnrollmentDirection((EnrollmentDirection) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "groupName":
                        return true;
                case "enrollmentDirection":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "groupName":
                    return true;
                case "enrollmentDirection":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "groupName":
                    return String.class;
                case "enrollmentDirection":
                    return EnrollmentDirection.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<GroupEsForFis> _dslPath = new Path<GroupEsForFis>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "GroupEsForFis");
    }
            

    /**
     * @return имя группы дисциплин. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.GroupEsForFis#getGroupName()
     */
    public static PropertyPath<String> groupName()
    {
        return _dslPath.groupName();
    }

    /**
     * @return Направление подготовки (специальность) приема. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.GroupEsForFis#getEnrollmentDirection()
     */
    public static EnrollmentDirection.Path<EnrollmentDirection> enrollmentDirection()
    {
        return _dslPath.enrollmentDirection();
    }

    public static class Path<E extends GroupEsForFis> extends EntityPath<E>
    {
        private PropertyPath<String> _groupName;
        private EnrollmentDirection.Path<EnrollmentDirection> _enrollmentDirection;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return имя группы дисциплин. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.GroupEsForFis#getGroupName()
     */
        public PropertyPath<String> groupName()
        {
            if(_groupName == null )
                _groupName = new PropertyPath<String>(GroupEsForFisGen.P_GROUP_NAME, this);
            return _groupName;
        }

    /**
     * @return Направление подготовки (специальность) приема. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.GroupEsForFis#getEnrollmentDirection()
     */
        public EnrollmentDirection.Path<EnrollmentDirection> enrollmentDirection()
        {
            if(_enrollmentDirection == null )
                _enrollmentDirection = new EnrollmentDirection.Path<EnrollmentDirection>(L_ENROLLMENT_DIRECTION, this);
            return _enrollmentDirection;
        }

        public Class getEntityClass()
        {
            return GroupEsForFis.class;
        }

        public String getEntityName()
        {
            return "groupEsForFis";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
