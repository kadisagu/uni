package ru.tandemservice.fisrmc.component.dataChecking.FisCheckEntrantRequest.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import ru.tandemservice.fisrmc.entity.FisCheckEntrantRequest;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.List;

public class Model implements IEnrollmentCampaignSelectModel {

    private IDataSettings settings;
    private DynamicListDataSource<FisCheckEntrantRequest> dataSource;
    private List<EnrollmentCampaign> enrollmentCampaignList;
    private IMultiSelectModel errorCodeModel;
    private IMultiSelectModel fisAppStatusCodeModel;

    public IMultiSelectModel getFisAppStatusCodeModel() {
        return fisAppStatusCodeModel;
    }

    public void setFisAppStatusCodeModel(IMultiSelectModel fisAppStatusCodeModel) {
        this.fisAppStatusCodeModel = fisAppStatusCodeModel;
    }

    @Override
    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public DynamicListDataSource<FisCheckEntrantRequest> getDataSource() {
        return dataSource;
    }

    public void setDataSource(
            DynamicListDataSource<FisCheckEntrantRequest> dataSource)
    {
        this.dataSource = dataSource;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList() {
        return enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(
            List<EnrollmentCampaign> enrollmentCampaignList)
    {
        this.enrollmentCampaignList = enrollmentCampaignList;
    }

    public IMultiSelectModel getErrorCodeModel() {
        return errorCodeModel;
    }

    public void setErrorCodeModel(IMultiSelectModel errorCodeModel) {
        this.errorCodeModel = errorCodeModel;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign() {
        return (EnrollmentCampaign) this.settings.get("enrollmentCampaign");
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentcampaign) {
        this.settings.set("enrollmentCampaign", enrollmentcampaign);
    }

}
