package ru.tandemservice.fisrmc.dao;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.tandemservice.fisrmc.dao.packApplications.Applications;
import ru.tandemservice.fisrmc.dao.packApplications.IApplications;
import ru.tandemservice.fisrmc.entity.Educationdirectionfis;
import ru.tandemservice.fisrmc.entity.catalog.Compensationtypefis;
import ru.tandemservice.fisrmc.entity.catalog.Developformfis;
import ru.tandemservice.fisrmc.entity.catalog.Eceducationlevelfis;
import ru.tandemservice.fisrmc.entity.catalog.EducationdirectionSpravType;
import ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;


public class OrdersOfAdmission
        extends UniBaseDao
        implements IOrdersOfAdmission
{

    private static IOrdersOfAdmission _IOrdersOfAdmission = null;

    public static IOrdersOfAdmission Instanse()
    {
        if (_IOrdersOfAdmission == null)
            _IOrdersOfAdmission = (IOrdersOfAdmission) ApplicationRuntime.getBean("packOrdersOfAdmission");
        return _IOrdersOfAdmission;
    }

    private List<EnrollmentExtract> getExtractList(List<EnrollmentCampaign> ecList)
    {
        // заявления с учетом фильтра
        DQLSelectBuilder dqlIn = Applications.Instanse().getEntrantRequestDQLList
                (
                        PackUnpackToFis.DAEMON_PARAMS.getEnrollmentCampaigns()
                        , PackUnpackToFis.DAEMON_PARAMS.isNotRf()
                        , PackUnpackToFis.DAEMON_PARAMS.isBenefitRequest()
                        , PackUnpackToFis.DAEMON_PARAMS.isOrdersOfAdmission()
                        , PackUnpackToFis.DAEMON_PARAMS.getRegNumbers()
                        , PackUnpackToFis.DAEMON_PARAMS.getExcludeRegNumbers()

                        , PackUnpackToFis.DAEMON_PARAMS.getDateRequestFrom()
                        , PackUnpackToFis.DAEMON_PARAMS.getDateRequestTo()
                        , PackUnpackToFis.DAEMON_PARAMS.isWhithoutSendedInFis()
                        , PackUnpackToFis.DAEMON_PARAMS.isWhithoutEntrantInOrder()
                        , false
                        , PackUnpackToFis.DAEMON_PARAMS.isTestMode()
                        , PackUnpackToFis.TESTMODE_CompetitiveGroup_List
                );

        DQLSelectBuilder dql = new DQLSelectBuilder();

        // выписка
        dql.fromEntity(EnrollmentExtract.class, "extract");

        // студент пред. зачисления - фильтр по заявлениям
        dql.where(in(property(EnrollmentExtract.entity().requestedEnrollmentDirection().entrantRequest().fromAlias("extract")), dqlIn.getQuery()));

        // признак включения в приказ
        dql.where(DQLExpressions.isNotNull(DQLExpressions.property(EnrollmentExtract.paragraph().order().fromAlias("extract"))));

        List<EnrollmentExtract> lst = dql.createStatement(getSession()).list();

        return lst;
    }


//	private List<Object[]> getDataEnrollmentDirectionList(List<EnrollmentCampaign> ecList) 
//	{
//		DQLSelectBuilder dql = Applications.Instanse().getEntrantRequestDQLList(ecList, true);
//
//		dql.joinEntity("er", DQLJoinType.inner, RequestedEnrollmentDirection.class, "red", DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().fromAlias("red")), DQLExpressions.property(EntrantRequest.id().fromAlias("er"))));
//
//		dql.joinPath(DQLJoinType.inner, RequestedEnrollmentDirection.enrollmentDirection().fromAlias("red"),"edir");
//
//		/**
//		 * Приказы
//		 */
//		dql.joinEntity("red", DQLJoinType.inner, PreliminaryEnrollmentStudent.class, "pes", DQLExpressions.eq(DQLExpressions.property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().fromAlias("pes")), DQLExpressions.property("red")));
//
//		dql.joinEntity("pes", DQLJoinType.inner, EnrollmentExtract.class, "eExtr", DQLExpressions.eq(DQLExpressions.property(EnrollmentExtract.entity().fromAlias("eExtr")), DQLExpressions.property("pes")));
//		//Абитуриенты, включенные в приказ
//		dql.where(DQLExpressions.isNotNull(DQLExpressions.property(EnrollmentExtract.paragraph().order().fromAlias("eExtr"))));
//
//		dql.column("eExtr");
//
//		//Приказ
//		dql.joinPath(DQLJoinType.inner, EnrollmentExtract.paragraph().order().fromAlias("eExtr"),"ord");
//
//		//Stage - этап проведения приемной компании. (приджойним по приемке и дате приказа) может быть null 
//		dql.joinEntity("edir", DQLJoinType.left, EnrollmentCampaignStageFIS.class, "ecf", DQLExpressions.and(
//				DQLExpressions.eq(DQLExpressions.property(EnrollmentCampaignStageFIS.enrollmentCampaign().fromAlias("ecf")), DQLExpressions.property(EnrollmentDirection.enrollmentCampaign().fromAlias("edir"))),
//				DQLExpressions.eq(DQLExpressions.property(EnrollmentCampaignStageFIS.dateOrder().fromAlias("ecf")), DQLExpressions.property(AbstractEntrantOrder.commitDate().fromAlias("ord")))));
//
//		dql.column("ecf");			
//
//		return dql.createStatement(getSession()).list();
//	}	
//	

    @Override
    public Element packOrdersOfAdmission(
            Document doc
            , List<EnrollmentCampaign> ecList
            , EducationdirectionSpravType spravType
            , Boolean isAppDateWithoutTime
    )
    {

        IApplications idaoApp = Applications.Instanse();
        idaoApp.MakeMapRequestedEnrollmentDirectionExt(ecList);

        // выписки по абитуриентам
        List<EnrollmentExtract> extracts = getExtractList(ecList);

        if (extracts.isEmpty()) {
            PackUnpackToFis.AddPkgError("Нет сведений о приказах, сформировать OrdersOfAdmission нельзя");
            return null;
        }

        Element rooElement = doc.createElement("OrdersOfAdmission");

        int i = 0;
        int size = extracts.size();

        for (EnrollmentExtract extract : extracts) {

            i++;
            PackUnpackToFis.MSG_LOG = "Extracr " + i + " of " + size;

            Element orderOfAdmissionElem = doc.createElement("OrderOfAdmission");
            rooElement.appendChild(orderOfAdmissionElem);

            EntrantRequest entrantRequest = extract.getEntity().getRequestedEnrollmentDirection().getEntrantRequest();
            //extract.getParagraph().getOrder().get
            RequestedEnrollmentDirection requestEnrolmentDirection = extract.getEntity().getRequestedEnrollmentDirection();
            EducationOrgUnit educationOu = extract.getEntity().getRequestedEnrollmentDirection().getEnrollmentDirection().getEducationOrgUnit();

            // данные по заявлению
            Element applicationElem = doc.createElement("Application");
            {
                orderOfAdmissionElem.appendChild(applicationElem);
                ToolsDao.PackToElement(doc, applicationElem, "ApplicationNumber", String.valueOf(entrantRequest.getRegNumber()));
                ToolsDao.PackToElement(doc, applicationElem, "RegistrationDate", entrantRequest.getRegDate(), true, isAppDateWithoutTime);
            }

            //Упаковка DirectionID
            {
                Educationdirectionfis educationOuFis = SpravDao.Instanse().getEducationdirectionfis(educationOu, spravType);

                if (educationOuFis == null)
                    throw new ApplicationException("Направление подготовки " + educationOu.getTitle());

                ToolsDao.PackToElement(doc, orderOfAdmissionElem, "DirectionID", educationOuFis.getCode());
            }

            //Упаковка EducationFormID
            DevelopForm developForm = educationOu.getDevelopForm();
            Developformfis developFormFis = SpravDao.Instanse().getDevelopformfis(developForm);
            ToolsDao.PackToElement(doc, orderOfAdmissionElem, "EducationFormID", developFormFis.getCode());

            //Упаковка FinanceSourceID
                /*
				CompensationType compensationType = requestEnrolmentDirection.getCompensationType();
				Compensationtypefis compensationTypeFis = SpravDao.Instanse().getCompensationtypefis(compensationType);
				*/

            // нужно знать расширение
            RequestedEnrollmentDirectionExt redExt = idaoApp.getRequestedEnrollmentDirectionExt(requestEnrolmentDirection);


            Compensationtypefis compensationTypeFis = SpravDao.Instanse().getCompensationtypefis(requestEnrolmentDirection, redExt);
            ToolsDao.PackToElement(doc, orderOfAdmissionElem, "FinanceSourceID", compensationTypeFis.getCode());


            //Упаковка EducationLevelID
            Eceducationlevelfis educationLevelFis = SpravDao.Instanse().getEceducationlevelfis(educationOu);
            ToolsDao.PackToElement(doc, orderOfAdmissionElem, "EducationLevelID", educationLevelFis.getCode());


            //"Вне конкурса" - константа = 3
            if (requestEnrolmentDirection.getCompetitionKind().getCode().equals("3")
                    || compensationTypeFis.getCode().equals("20")) // или льготники
                ToolsDao.PackToElement(doc, orderOfAdmissionElem, "IsBeneficiary", true);


            // определим stage
            List<String> lstStage = SpravDao.Instanse().getStageList(educationLevelFis, developFormFis, compensationTypeFis);
            if (lstStage.size() > 1) {
                // проведение в 2 этапа, нужна дата приказа
                // определим номер этапа
                String stage = SpravDao.Instanse().getStageByOrder(extract.getParagraph().getOrder(), entrantRequest.getEntrant().getEnrollmentCampaign());
                ToolsDao.PackToElement(doc, orderOfAdmissionElem, "Stage", stage);
            }
        }
        return rooElement;
    }

//	public Element packOrdersOfAdmissionOld(Document doc,
//			List<EnrollmentCampaign> ecList) 
//	{
//		Map<EducationOrgUnit,Educationdirectionfis> mapDirectionID = new HashMap<EducationOrgUnit, Educationdirectionfis>();
//		Map<DevelopForm,Developformfis> mapEducationFormID = new HashMap<DevelopForm,Developformfis>();
//		Map<CompensationType,Compensationtypefis> mapFinanceSourceID = new HashMap<CompensationType,Compensationtypefis>();
//		Map<EducationOrgUnit,Eceducationlevelfis > mapEducationLevelID = new HashMap<EducationOrgUnit,Eceducationlevelfis>();
//
//
//		List<Object[]> directions = getDataEnrollmentDirectionList(ecList);
//
//		if (directions==null || directions.size()==0)
//			return null;
//
//		Element rooElement = doc.createElement("OrdersOfAdmission");
//
//		for(Object[] data : directions){
//			EntrantRequest entrantRequest= (EntrantRequest) data[0];
//
//			EnrollmentExtract extract = (EnrollmentExtract) data[1];
//			
//			//extract.getParagraph().getOrder().get
//			
//			EnrollmentCampaignStageFIS campaignStageFIS = (EnrollmentCampaignStageFIS) data[2];
//
//			RequestedEnrollmentDirection direction = extract.getEntity().getRequestedEnrollmentDirection();
//
//			EducationOrgUnit educationOrgUnit = direction.getEnrollmentDirection().getEducationOrgUnit();
//
//			Element orderOfAdmissionElem = doc.createElement("OrderOfAdmission");
//			rooElement.appendChild(orderOfAdmissionElem);
//
//			Element applicationElem = doc.createElement("Application");
//			{
//				orderOfAdmissionElem.appendChild(applicationElem);
//				ToolsDao.PackToElement(doc, applicationElem, "ApplicationNumber", String.valueOf(entrantRequest.getRegNumber()));
//				ToolsDao.PackToElement(doc, applicationElem, "RegistrationDate", ToolsDao.DateToString(entrantRequest.getRegDate()));
//			}
//
//			//Упаковка DirectionID
//			{
//				if(!mapDirectionID.containsKey(educationOrgUnit)){
//					mapDirectionID.put(educationOrgUnit, SpravDao.Instanse().getEducationdirectionfis(educationOrgUnit));
//				}
//
//				ToolsDao.PackToElement(doc, orderOfAdmissionElem, "DirectionID", String.valueOf(mapDirectionID.get(educationOrgUnit).getId()));
//			}
//
//			//Упаковка EducationFormID
//			{
//				DevelopForm developForm = educationOrgUnit.getDevelopForm();
//				if(!mapEducationFormID.containsKey(developForm)){
//					mapEducationFormID.put(developForm, SpravDao.Instanse().getDevelopformfis(developForm));
//				}
//				ToolsDao.PackToElement(doc, orderOfAdmissionElem, "EducationFormID", String.valueOf(mapEducationFormID.get(developForm).getId()));
//			}
//			
//			//Упаковка FinanceSourceID
//			{
//				CompensationType compensationType = direction.getCompensationType();
//				if(!mapFinanceSourceID.containsKey(compensationType)){
//					mapFinanceSourceID.put(compensationType, SpravDao.Instanse().getCompensationtypefis(compensationType));
//				}
//				
//				ToolsDao.PackToElement(doc, orderOfAdmissionElem, "FinanceSourceID", String.valueOf(mapFinanceSourceID.get(compensationType).getId()));
//			}
//			
//			//Упаковка EducationLevelID
//			{
//				if(!mapEducationLevelID.containsKey(educationOrgUnit)){
//					mapEducationLevelID.put(educationOrgUnit,SpravDao.Instanse().getEceducationlevelfis(educationOrgUnit));
//				}
//
//				ToolsDao.PackToElement(doc, orderOfAdmissionElem, "EducationLevelID", String.valueOf(mapEducationLevelID.get(educationOrgUnit).getId()));
//			}
//			
//			String title = "";
//			if(campaignStageFIS!= null)
//				title = campaignStageFIS.getStage().getTitle();
//
//			ToolsDao.PackToElement(doc, orderOfAdmissionElem, "Stage", title);
//
//			//"Вне конкурса"
//			ToolsDao.PackToElement(doc, orderOfAdmissionElem, "IsBeneficiary", direction.getCompetitionKind().getCode().equals("4"));
//
//		}
//
//		return rooElement;
//	}

}
