package ru.tandemservice.fisrmc.dao;

import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.List;


/**
 * Информация по конкурсной группе
 *
 * @author vch
 */
public class XMLCompetitiveGroup {

    private String _name = null;

    /**
     * uid конкурсной группы
     * в качестве uid следует использовать:
     * 1) если конкурсных групп нет, то id из enrollmentdirection_t
     * 2) если конкурсные группы есть, то id из competitiongroup_t
     * <p/>
     * При этом не забываем ставить булевый флаг в useCompetitiveGroup
     */
    private String uid;

    private EnrollmentCampaign enrollmentCampaign;

    /**
     * Направления подготовки конкурсной группы
     */
    private List<XMLCompetitiveGroupItem> competitiveGroupItem;

    /**
     * используется конкурсная группа или нет
     * зависит от настроек приемной кампании
     * // как узнать, что конкурсных групп нет
     * // 1 - в настройках приемки есть галочка
     * // 2 - в направлениях для приема (enrollmentdirection_t) есть ссылка
     * // competitiongroup_t
     * // если на направлении подготовки для приема в ссылке null - значит такое направление подготовки в отдельную конкурсную группу
     */
    private boolean useCompetitiveGroup = false;

    public XMLCompetitiveGroup(String uid, boolean useCompetitiveGroup, EnrollmentCampaign enrollmentCampaign)
    {
        this.uid = uid;
        this.setEnrollmentCampaign(enrollmentCampaign);
        this.useCompetitiveGroup = useCompetitiveGroup;
    }


    public void setUid(String uid) {
        this.uid = uid;
    }


    public String getUid() {
        return uid;
    }


    public void setCompetitiveGroupItem(List<XMLCompetitiveGroupItem> competitiveGroupItem) {
        this.competitiveGroupItem = competitiveGroupItem;
    }


    public List<XMLCompetitiveGroupItem> getCompetitiveGroupItem() {
        return competitiveGroupItem;
    }


    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign) {
        this.enrollmentCampaign = enrollmentCampaign;
    }


    public EnrollmentCampaign getEnrollmentCampaign() {
        return enrollmentCampaign;
    }


    public void setUseCompetitiveGroup(boolean useCompetitiveGroup) {
        this.useCompetitiveGroup = useCompetitiveGroup;
    }


    public boolean isUseCompetitiveGroup() {
        return useCompetitiveGroup;
    }

    /**
     * имя конкурсной группы
     *
     * @return
     */
    public String getName()
    {
        if (_name != null)
            return _name;

        Long id = Long.parseLong(uid);
        String title = null;
        if (isUseCompetitiveGroup()) {
            title = SpravDao.Instanse().getCompetitiongroup(id).getTitle();
            if (title == null)
                title = "Конкурсная группа";
        }
        else {
            EnrollmentDirection ed = SpravDao.Instanse().getEnrollmentDirection(id);

            String developPeriod = "";
            if (ed.getEducationOrgUnit().getDevelopPeriod() != null)
                developPeriod = ed.getEducationOrgUnit().getDevelopPeriod().getTitle();
            title = SpravDao.Instanse().getEnrollmentDirection(id).getTitle();
            if (title == null)
                title = "По направлению подготовки";

            title += " (" + developPeriod + ")";
        }

        // кладем в кеш
        _name = title;
        return _name;
    }


    @Override
    public int hashCode()
    {
        return getUid().hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof XMLCompetitiveGroup) {
            int hc = ((XMLCompetitiveGroup) obj).hashCode();
            int hcExsit = this.hashCode();

            if (hc == hcExsit)
                return true;
            else
                return false;
        }
        else
            return super.equals(obj);
    }

}
