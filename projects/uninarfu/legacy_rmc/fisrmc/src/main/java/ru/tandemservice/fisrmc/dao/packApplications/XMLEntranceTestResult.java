package ru.tandemservice.fisrmc.dao.packApplications;

import org.tandemframework.shared.commonbase.base.util.PairKey;
import ru.tandemservice.fisrmc.dao.XMLCompetitiveGroup;
import ru.tandemservice.fisrmc.entity.catalog.Ecdisciplinefis;
import ru.tandemservice.fisrmc.entity.catalog.Entrancedisciplinetypefis;
import ru.tandemservice.fisrmc.entity.catalog.Makrsoursefis;
import ru.tandemservice.uniec.entity.catalog.EducationSubject;
import ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline;

import java.util.ArrayList;
import java.util.List;


public class XMLEntranceTestResult {
    private String uid = "";
    private int finalMark = 0;
    private Makrsoursefis makrsoursefis = null;
    private Ecdisciplinefis ecdisciplinefis = null;
    private Entrancedisciplinetypefis entrancedisciplinetypefis = null;
    private XMLCompetitiveGroup xmlCompetitiveGroup = null;

    private List<ChosenEntranceDiscipline> chosenEntranceDisciplineList = new ArrayList<ChosenEntranceDiscipline>();
    private List<PairKey<EducationSubject, Integer>> listMarkSourse = new ArrayList<PairKey<EducationSubject, Integer>>();

    private List<Integer> avgPersonEduInstitutionMark = null;

    public XMLEntranceTestResult
            (
                    String uid
                    , int finalMark
                    , Makrsoursefis makrsoursefis
                    , Ecdisciplinefis ecdisciplinefis
                    , Entrancedisciplinetypefis entrancedisciplinetypefis
                    , XMLCompetitiveGroup xmlCompetitiveGroup
            )
    {
        setUid(uid);
        setFinalMark(finalMark);
        setMakrsoursefis(makrsoursefis);
        setEntrancedisciplinetypefis(entrancedisciplinetypefis);
        setXmlCompetitiveGroup(xmlCompetitiveGroup);
        setEcdisciplinefis(ecdisciplinefis);

    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUid()
    {
        if (uid == null)
            return getKey();
        else
            return uid;
    }

    public String getKey()
    {
        // нужно расчитать
        String key = "";
        // основание для оценки
        if (makrsoursefis != null)
            key += "ms:" + makrsoursefis.getCode() + ":";

        String ecdisciplinefisCode = "";
        if (ecdisciplinefis != null) {
            if (ecdisciplinefis.getId() != null)
                ecdisciplinefisCode = ecdisciplinefis.getCode();
            else
                // созданная дисциплина (не сопоставлена)
                ecdisciplinefisCode = "ns" + ecdisciplinefis.getCodeTU();
        }
        key += "d:" + ecdisciplinefisCode + ":";
        if (entrancedisciplinetypefis != null)
            key += "t:" + entrancedisciplinetypefis.getCode() + ":";

        if (xmlCompetitiveGroup != null)
            key += "g:" + xmlCompetitiveGroup.getUid();

        return key;

    }

    public void setFinalMark(int finalMark) {
        this.finalMark = finalMark;
    }

    public int getFinalMark()
    {
        // итоговая оценка может считаться и на основе
        // объединения дисциплин в группы для ФИС

        if (listMarkSourse.isEmpty())
            return finalMark;
        else {
            // тупо сложить
            // на потом - завести БИН расчета итоговой оценки по группе дисциплин
            int _finalMark = 0;
            for (PairKey<EducationSubject, Integer> pk : listMarkSourse) {
                Integer mark = pk.getSecond();
                _finalMark += mark;
            }

            // средний балл аттестата
            if (avgPersonEduInstitutionMark != null && !avgPersonEduInstitutionMark.isEmpty()) {
                // считаем средний балл
                int m3 = avgPersonEduInstitutionMark.get(0);
                int m4 = avgPersonEduInstitutionMark.get(1);
                int m5 = avgPersonEduInstitutionMark.get(2);

                int sum = m3 + m4 + m5;
                int total = m3 * 3 + m4 * 4 + m5 * 5;
                Double avg = Math.ceil((sum != 0) ? (double) total / sum : 0);
                int avgInt = avg.intValue();

                _finalMark += avgInt;
            }
            return _finalMark;
        }
    }

    public void setMakrsoursefis(Makrsoursefis makrsoursefis) {
        this.makrsoursefis = makrsoursefis;
    }

    public Makrsoursefis getMakrsoursefis() {
        return makrsoursefis;
    }

    public void setEcdisciplinefis(Ecdisciplinefis ecdisciplinefis) {
        this.ecdisciplinefis = ecdisciplinefis;
    }

    public Ecdisciplinefis getEcdisciplinefis() {
        return ecdisciplinefis;
    }

    public void setEntrancedisciplinetypefis(Entrancedisciplinetypefis entrancedisciplinetypefis) {
        this.entrancedisciplinetypefis = entrancedisciplinetypefis;
    }

    public Entrancedisciplinetypefis getEntrancedisciplinetypefis() {
        return entrancedisciplinetypefis;
    }

    public void setXmlCompetitiveGroup(XMLCompetitiveGroup xmlCompetitiveGroup) {
        this.xmlCompetitiveGroup = xmlCompetitiveGroup;
    }

    public XMLCompetitiveGroup getXmlCompetitiveGroup() {
        return xmlCompetitiveGroup;
    }

    public void AddChosenEntranceDiscipline(ChosenEntranceDiscipline cd)
    {
        if (!chosenEntranceDisciplineList.contains(cd))
            chosenEntranceDisciplineList.add(cd);
    }

    public List<ChosenEntranceDiscipline> getChosenEntranceDisciplineList() {
        return chosenEntranceDisciplineList;
    }

    public void addEducationSubjectMarkInfo
            (
                    PairKey<EducationSubject, Integer> pairKey
            )
    {
        listMarkSourse.add(pairKey);
    }

    public List<PairKey<EducationSubject, Integer>> getListMarkSourse()
    {
        return listMarkSourse;
    }

    public void setAvgPersonEduInstitutionMark
            (
                    List<Integer> avgPersonEduInstitutionMark
            )
    {
        this.avgPersonEduInstitutionMark = avgPersonEduInstitutionMark;
    }

    public List<Integer> getAvgPersonEduInstitutionMark()
    {
        return avgPersonEduInstitutionMark;
    }
}
