package ru.tandemservice.fisrmc.component.catalog.examdocumenttypefis.ExamdocumenttypefisAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.fisrmc.entity.catalog.Examdocumenttypefis;

public interface IDAO extends IDefaultCatalogAddEditDAO<Examdocumenttypefis, Model> {

}
