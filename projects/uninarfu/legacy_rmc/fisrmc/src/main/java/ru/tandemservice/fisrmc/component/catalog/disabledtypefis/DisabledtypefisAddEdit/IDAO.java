package ru.tandemservice.fisrmc.component.catalog.disabledtypefis.DisabledtypefisAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.fisrmc.entity.catalog.Disabledtypefis;

public interface IDAO extends IDefaultCatalogAddEditDAO<Disabledtypefis, Model> {

}
