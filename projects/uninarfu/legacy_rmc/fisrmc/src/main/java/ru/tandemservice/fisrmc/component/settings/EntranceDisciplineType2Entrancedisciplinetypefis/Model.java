package ru.tandemservice.fisrmc.component.settings.EntranceDisciplineType2Entrancedisciplinetypefis;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.fisrmc.entity.EntranceDisciplineType2Entrancedisciplinetypefis;
import ru.tandemservice.fisrmc.entity.catalog.Entrancedisciplinetypefis;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineType;

import java.util.List;

public class Model {

    private String selectedPage;
    private DynamicListDataSource<EntranceDisciplineType2Entrancedisciplinetypefis> dataSource;
    private IDataSettings settings;

    private List<EntranceDisciplineType> entranceDisciplineTypesList;
    private List<Entrancedisciplinetypefis> entrancedisciplinetypefisList;
    private List<EntranceDisciplineType2Entrancedisciplinetypefis> disciplineTypes;

    public String getSelectedPage() {
        return selectedPage;
    }

    public void setSelectedPage(String selectedPage) {
        this.selectedPage = selectedPage;
    }

    public DynamicListDataSource<EntranceDisciplineType2Entrancedisciplinetypefis> getDataSource() {
        return dataSource;
    }

    public void setDataSource(
            DynamicListDataSource<EntranceDisciplineType2Entrancedisciplinetypefis> dataSource)
    {
        this.dataSource = dataSource;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public List<EntranceDisciplineType> getEntranceDisciplineTypesList() {
        return entranceDisciplineTypesList;
    }

    public void setEntranceDisciplineTypesList(
            List<EntranceDisciplineType> entranceDisciplineTypesList)
    {
        this.entranceDisciplineTypesList = entranceDisciplineTypesList;
    }

    public List<Entrancedisciplinetypefis> getEntrancedisciplinetypefisList() {
        return entrancedisciplinetypefisList;
    }

    public void setEntrancedisciplinetypefisList(
            List<Entrancedisciplinetypefis> entrancedisciplinetypefisList)
    {
        this.entrancedisciplinetypefisList = entrancedisciplinetypefisList;
    }

    public List<EntranceDisciplineType2Entrancedisciplinetypefis> getDisciplineTypes() {
        return disciplineTypes;
    }

    public void setDisciplineTypes(List<EntranceDisciplineType2Entrancedisciplinetypefis> disciplineTypes) {
        this.disciplineTypes = disciplineTypes;
    }


}
