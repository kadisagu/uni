package ru.tandemservice.fisrmc.component.settings.DisciplimeMinMark2Olimpiad;

import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.fisrmc.entity.DisciplineMinMark2Olimpiad;
import ru.tandemservice.fisrmc.entity.catalog.Ecdisciplinefis;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
    }


    @SuppressWarnings("unchecked")
    @Override
    public void prepareListDataSource(Model model)
    {
        EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();
        /*
		DisciplineMinMark2Olimpiad d2o;
		Ecdisciplinefis edf = d2o.getEcdisciplinefis();
		*/
        DynamicListDataSource<Ecdisciplinefis> dataSource = model.getDataSource();

        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(Ecdisciplinefis.class, "en")
                .order(DQLExpressions.property(Ecdisciplinefis.code().fromAlias("en")));

        List<Ecdisciplinefis> lstDisc = getList(builder);

        UniBaseUtils.createPage(dataSource, lstDisc);

        if (lstDisc != null && lstDisc.size() > 0)
            dataSource.setCountRow(lstDisc.size());


        List<Ecdisciplinefis> lst = getList(builder);
        BlockColumn columnMark = (BlockColumn) dataSource.getColumn("minMark");

        Map<Long, Integer> mapValue = new HashMap<>();

        for (Ecdisciplinefis discipline : lst) {
            DQLSelectBuilder qList = new DQLSelectBuilder()
                    .fromEntity(DisciplineMinMark2Olimpiad.class, "en")
                    .where(DQLExpressions.eq(DQLExpressions.property(DisciplineMinMark2Olimpiad.enrollmentCampaign().fromAlias("en")), DQLExpressions.value(enrollmentCampaign)))
                    .where(DQLExpressions.eq(DQLExpressions.property(DisciplineMinMark2Olimpiad.ecdisciplinefis().fromAlias("en")), DQLExpressions.value(discipline)));

            List<DisciplineMinMark2Olimpiad> lstQ = getList(qList);
            Integer minMark = 65;

            if (lstQ != null && lstQ.size() > 0) {
                minMark = lstQ.get(0).getMinMark();
            }

            mapValue.put(discipline.getId(), minMark);

        }
        columnMark.setValueMap(mapValue);
    }


    @SuppressWarnings("unchecked")
    @Override
    public void update(Model model)
    {
        DynamicListDataSource<Ecdisciplinefis> dataSource = model.getDataSource();

        BlockColumn column = (BlockColumn) dataSource.getColumn("minMark");
        Map<Long, Integer> map = column.getValueMap();
        List<Ecdisciplinefis> lst = dataSource.getEntityList();

        EnrollmentCampaign ec = model.getEnrollmentCampaign();

        for (Ecdisciplinefis discipline : lst) {
            Integer minMark = map.get(discipline.getId());
            saveOrUpdate(ec, discipline, minMark);
        }
    }

    private void saveOrUpdate(
            EnrollmentCampaign enrollmentCampaign
            , Ecdisciplinefis discipline
            , Integer minMark)
    {

        DQLSelectBuilder qList = new DQLSelectBuilder()
                .fromEntity(DisciplineMinMark2Olimpiad.class, "en")
                .where(DQLExpressions.eq(DQLExpressions.property(DisciplineMinMark2Olimpiad.enrollmentCampaign().fromAlias("en")), DQLExpressions.value(enrollmentCampaign)))
                .where(DQLExpressions.eq(DQLExpressions.property(DisciplineMinMark2Olimpiad.ecdisciplinefis().fromAlias("en")), DQLExpressions.value(discipline)));


        DisciplineMinMark2Olimpiad ent = qList.createStatement(getSession()).uniqueResult();
        if (ent == null) {
            ent = new DisciplineMinMark2Olimpiad();
            ent.setEcdisciplinefis(discipline);
            ent.setEnrollmentCampaign(enrollmentCampaign);
        }

        ent.setMinMark(minMark);
        saveOrUpdate(ent);
    }
}
