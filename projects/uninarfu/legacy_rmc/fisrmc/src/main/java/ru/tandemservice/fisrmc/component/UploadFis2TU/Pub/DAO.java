package ru.tandemservice.fisrmc.component.UploadFis2TU.Pub;

import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.fisrmc.entity.FisExportFileAction;
import ru.tandemservice.fisrmc.entity.FisExportFiles;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        model.setEntity(getNotNull(FisExportFiles.class, model.getEntity().getId()));
    }

    @Override
    @Transactional
    public void checkEntrantRequestDate(Model model) throws Exception
    {
        // String xml = new String(model.getEntity().getXmlPackage().getContent());
        EntrantRequestDateParser.doProcess(model.getEntity(), model.getTestMode());
    }

    @Override
    public void prepareListDataSource(Model model) {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(FisExportFileAction.class, "e")
                .where(DQLExpressions.eq(
                        DQLExpressions.property(FisExportFileAction.fisExportFiles().id().fromAlias("e")),
                        DQLExpressions.value(model.getEntity().getId())
                ))
                .column("e");

        DQLOrderDescriptionRegistry order = new DQLOrderDescriptionRegistry(FisExportFiles.class, "e");
        order.applyOrder(dql, model.getDataSource().getEntityOrder());

        UniBaseUtils.createPage(model.getDataSource(), dql, getSession());
    }

}
