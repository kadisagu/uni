package ru.tandemservice.fisrmc.entity;

import ru.tandemservice.fisrmc.entity.gen.FisCheckEgeSertificateGen;

/**
 * Проверка сертификатов ЕГЭ
 * <p/>
 * Если сертификат загружен без ошибок, то обязательно наличие кода fisEgeStatusCode=0 'Ошибок нет'
 */
public class FisCheckEgeSertificate extends FisCheckEgeSertificateGen {
}