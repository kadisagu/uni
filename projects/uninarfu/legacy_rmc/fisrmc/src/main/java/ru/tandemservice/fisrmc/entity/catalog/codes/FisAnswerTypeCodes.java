package ru.tandemservice.fisrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип ответа ФИС"
 * Имя сущности : fisAnswerType
 * Файл data.xml : catalogs.data.xml
 */
public interface FisAnswerTypeCodes
{
    /** Константа кода (code) элемента : Проверка заявлений (title) */
    String CHECK_APPLICATION = "1";
    /** Константа кода (code) элемента : Результат импорта (title) */
    String IMPORT_RESULT = "2";
    /** Константа кода (code) элемента : Результат удаления (title) */
    String DELETE_RESULT = "3";
    /** Константа кода (code) элемента : Проверка пакета (title) */
    String VALIDATE = "4";

    Set<String> CODES = ImmutableSet.of(CHECK_APPLICATION, IMPORT_RESULT, DELETE_RESULT, VALIDATE);
}
