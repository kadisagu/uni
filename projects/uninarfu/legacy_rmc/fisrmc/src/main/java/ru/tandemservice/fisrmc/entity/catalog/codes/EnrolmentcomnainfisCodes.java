package ru.tandemservice.fisrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Статус приемной кампании"
 * Имя сущности : enrolmentcomnainfis
 * Файл data.xml : catalogs.data.xml
 */
public interface EnrolmentcomnainfisCodes
{
    /** Константа кода (code) элемента : Набор не начался (title) */
    String NOT_START = "0";
    /** Константа кода (code) элемента : Идет набор (title) */
    String IN_PROCESS = "1";
    /** Константа кода (code) элемента : Завершена (title) */
    String END = "2";

    Set<String> CODES = ImmutableSet.of(NOT_START, IN_PROCESS, END);
}
