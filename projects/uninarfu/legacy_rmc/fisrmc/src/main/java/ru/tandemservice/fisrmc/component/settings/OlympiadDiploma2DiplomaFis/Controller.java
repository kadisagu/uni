package ru.tandemservice.fisrmc.component.settings.OlympiadDiploma2DiplomaFis;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.BooleanColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.entrant.OlympiadDiploma;

public class Controller extends AbstractBusinessController<IDAO, Model> {


    @Override
    public void onRefreshComponent(IBusinessComponent component) {

        Model model = getModel(component);

        prepareDataSource(component);

        model.setSettings(component.getSettings());

        getDao().prepare(model);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null) {
            return;
        }
        DynamicListDataSource<OlympiadDiploma> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Олимпиада", "title").setClickable(false));
        dataSource.addColumn(new BooleanColumn("Сопоставлено", "hasRelation"));
        dataSource.addColumn(new BlockColumn("catalogValue", "Олимпиада ФИС").setWidth(40));

        model.setDataSource(dataSource);
    }

    public void onClickApply(IBusinessComponent component)
    {
        IDAO dao = getDao();
        Model model = getModel(component);
        ErrorCollector errors = component.getUserContext().getErrorCollector();

        dao.validate(model, errors);
        if (!errors.hasErrors()) {
            dao.update(model);
        }
    }

    public void onRefresh(IBusinessComponent component)
    {
        component.saveSettings();
    }
}
