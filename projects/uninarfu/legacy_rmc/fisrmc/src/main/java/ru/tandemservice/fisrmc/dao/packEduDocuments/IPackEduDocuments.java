package ru.tandemservice.fisrmc.dao.packEduDocuments;

import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;

public interface IPackEduDocuments {

    /**
     * упаковать документы об образовании
     * по абитуриенту
     *
     * @param doc
     * @param entrantrequest
     *
     * @return
     */
    public Element PackEduDocumentsToXml
    (
            Document doc
            , EntrantRequest entrantrequest
    );


    public Element PackPersonEduInstitution(Document doc,
                                            PersonEduInstitution personEduInstitution);

    public void makeCache();

    public void clearCache();
}
