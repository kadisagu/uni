package ru.tandemservice.fisrmc.dao.packEduDocuments;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import org.tandemframework.shared.person.catalog.entity.EducationLevelStage;
import org.tandemframework.shared.person.catalog.entity.codes.EducationLevelStageCodes;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.tandemservice.fisrmc.dao.PackUnpackToFis;
import ru.tandemservice.fisrmc.dao.ToolsDao;
import ru.tandemservice.fisrmc.dao.packApplications.Applications;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.entity.entrant.EntrantOriginalDocumentRelation;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;


public class PackEduDocuments
        extends UniBaseDao
        implements IPackEduDocuments
{

    private static IPackEduDocuments _IPackEduDocuments = null;

    public static IPackEduDocuments Instanse()
    {
        if (_IPackEduDocuments == null)
            _IPackEduDocuments = (IPackEduDocuments) ApplicationRuntime.getBean("packEduDocuments");
        return _IPackEduDocuments;
    }

    private EntrantRequest entrantRequest;
    private boolean isOriginalReceived;


    private Map<Long, Integer> mapOriginalCount = new HashMap<Long, Integer>();

    public void makeCache()
    {
        DQLSelectBuilder dqlEntrantRequest = Applications.Instanse().getEntrantRequestDQLList
                (
                        PackUnpackToFis.DAEMON_PARAMS.getEnrollmentCampaigns()
                        , PackUnpackToFis.DAEMON_PARAMS.isNotRf()
                        , PackUnpackToFis.DAEMON_PARAMS.isBenefitRequest()
                        , PackUnpackToFis.DAEMON_PARAMS.isOrdersOfAdmission()
                        , PackUnpackToFis.DAEMON_PARAMS.getRegNumbers()
                        , PackUnpackToFis.DAEMON_PARAMS.getExcludeRegNumbers()

                        , PackUnpackToFis.DAEMON_PARAMS.getDateRequestFrom()
                        , PackUnpackToFis.DAEMON_PARAMS.getDateRequestTo()
                        , PackUnpackToFis.DAEMON_PARAMS.isWhithoutSendedInFis()
                        , PackUnpackToFis.DAEMON_PARAMS.isWhithoutEntrantInOrder()

                        , false
                        , PackUnpackToFis.DAEMON_PARAMS.isTestMode()
                        , PackUnpackToFis.TESTMODE_CompetitiveGroup_List
                );


        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EntrantOriginalDocumentRelation.class, "origindocument");
        dql.where(in(property(EntrantOriginalDocumentRelation.requestedEnrollmentDirection().entrantRequest().id().fromAlias("origindocument")), dqlEntrantRequest.getQuery()));

        dql.column(EntrantOriginalDocumentRelation.requestedEnrollmentDirection().entrantRequest().id().fromAlias("origindocument").s());
        dql.column(DQLFunctions.count("origindocument"));
        dql.group(DQLExpressions.property(EntrantOriginalDocumentRelation.requestedEnrollmentDirection().entrantRequest().id().fromAlias("origindocument")));

        List<Object[]> list = getList(dql);

        for (Object[] line : list) {
            Long id = (Long) line[0];
            Integer count_origin = Integer.parseInt(line[1].toString());
            if (count_origin == null)
                count_origin = 0;

            // в мап
            if (!mapOriginalCount.containsKey(id))
                mapOriginalCount.put(id, count_origin);
        }

    }

    public void clearCache()
    {
        mapOriginalCount = new HashMap<Long, Integer>();
    }

	
	/*
	 * Сведения об образовании брать тут:
	 * таблица 'personeduinstitution_t' - Законченное учебное заведение
	 * 
	 * Следует иметь в виду, что у одной персоны может быть 
	 * несколько, но для ФИС (в данной версии) мы упаковываем всегда основное, основное получаем из персоны
	 */


    @Override
    public Element PackEduDocumentsToXml
            (
                    Document doc
                    , EntrantRequest entrantrequest
            )
    {

        this.entrantRequest = entrantrequest;
        int countOriginals = 0; //getCount(EntrantOriginalDocumentRelation.class,EntrantOriginalDocumentRelation.entrant().s(),entrantrequest.getEntrant());

        if (mapOriginalCount.containsKey(this.entrantRequest.getId()))
            countOriginals = mapOriginalCount.get(this.entrantRequest.getId());

        /**
         * Признак предоставления оригиналов документов
         */
        this.isOriginalReceived = countOriginals > 0 ? true : false;


        Person person = entrantrequest.getEntrant().getPerson();

        PersonEduInstitution personEduInstitution = person.getPersonEduInstitution();


        if (personEduInstitution == null) {
            String msg = "Для персоны " + person.getFullFio() + " не указаны сведения об образовании";
            if (PackUnpackToFis.DAEMON_PARAMS.isSuspendErrorDoc())
                PackUnpackToFis.AddPkgError(msg);
            else
                throw new ApplicationException(msg);
        }

        // чук 2012/03/05 id документа об образовании должно быть уникальным в рамках всех приемных компаний
        // поэтому вариантов 2 либо исключить id либо в id дописать id приемной компании
        Element eduInstitution = PackPersonEduInstitution(doc, personEduInstitution);

        if (eduInstitution == null) {
            String msg = "Для персоны " + person.getFullFio() + " не сформирован XML документ об образовании ";
            if (PackUnpackToFis.DAEMON_PARAMS.isSuspendErrorDoc())
                PackUnpackToFis.AddPkgError(msg);
            else
                throw new ApplicationException(msg);
        }

        return eduInstitution;
    }

    @Override
    public Element PackPersonEduInstitution
            (
                    Document doc
                    , PersonEduInstitution personEduInstitution
            )
    {
		/*
		 * select
			from
			educationlevelstage_t
			where
			parent_id is null
		 */
        EducationLevelStage stage = personEduInstitution.getEducationLevelStage();
        if (stage.getParent() != null)
            stage = stage.getParent();

        Element retVal = null;

        // в зависимости от ступени образования, формируем
        // атрибуты со страниц 27-33
        // при этом как хочется видеть реализацию вида:

        // Среднее (полное) общее образование
        if (stage.getCode().equals(EducationLevelStageCodes.MIDDLE_SCHOOL)) {
            XMLSchoolCertificateDocument xmlWrapper = getXMLSchoolCertificateDocument(personEduInstitution);
            retVal = packToXmlSchoolCertificateDocument(doc, xmlWrapper);
        }

        // и так далее по ступеням образования

        // Высшее проффесиональное образование
        // unieducationrmc ставлю в лоб, модуль unieducationrmc
        // подключать нельзя, у нас есть горный, там этого модуля нет
        if (
                stage.getCode().equals(EducationLevelStageCodes.HIGH_PROF)
                        || stage.getCode().equals(EducationLevelStageCodes.PROF_PREP)
                        || stage.getCode().equals("unieducationrmc.10") // Высшее образование
                        || stage.getCode().equals("unieducationrmc.11") // Бакалавриат ВО
                        || stage.getCode().equals("unieducationrmc.12") // Магистратура ВО
                        || stage.getCode().equals("unieducationrmc.13") // Специалитет ВО
                )
        {
            XMLHighEduDiplomaDocument xmlWrapper = getXMLHighEduDiplomaDocument(personEduInstitution);
            retVal = packToXmlHighEduDiplomaDocument(doc, xmlWrapper);
        }

        // Среднее проффесиональное образование
        if (stage.getCode().equals(EducationLevelStageCodes.MIDDLE_PROF)) {
            XMLMiddleEduDiplomaDocument xmlWrapper = getXMLMiddleEduDiplomaDocument(personEduInstitution);
            retVal = packToXmlMiddleEduDiplomaDocument(doc, xmlWrapper);
        }

        // Начальное проффесиональное образование
        if (stage.getCode().equals(EducationLevelStageCodes.BEGIN_PROF)) {
            XMLBasicDiplomaDocument xmlWrapper = getXMLBasicDiplomaDocument(personEduInstitution);
            retVal = packToXmlBasicDiplomaDocument(doc, xmlWrapper);
        }

        // Неполное высшее проффесиональное образование
        if (stage.getCode().equals(EducationLevelStageCodes.NEPOLNOE_VYSSHEE)) {
            XMLIncomplHighEduDiplomaDocument xmlWrapper = getXMLIncomplHighEduDiplomaDocument(personEduInstitution);
            retVal = packToXmlIncomplHighEduDiplomaDocument(doc, xmlWrapper);
        }

        // Академическая справка (как вспомогательный документ)
        // предоставляется, если абитуриент зачисляется на 2-ой и более высокий курс
        // на данном этапе ее вообще не формируем

        /**
         * не нашел подходящего уровня образования для сопоставления
         * выбрал "Дополнительное образование"
         */
		
		/*
		if (stage.getCode().equals(EducationLevelStageCodes.ADDITIONAL))
		{
			XMLAcademicDiplomaDocument xmlWrapper = getXMLAcademicDiplomaDocument(personEduInstitution);
			retVal = packToXmlAcademicDiplomaDocument(doc, xmlWrapper);
		}
		*/


        // Основное общее образование
        if (stage.getCode().equals(EducationLevelStageCodes.MAIN_SCHOOL)) {
            XMLSchoolCertificateBasicDocument xmlWrapper = getXMLSchoolCertificateBasicDocument(personEduInstitution);
            retVal = packToXmlSchoolCertificateBasicDocument(doc, xmlWrapper);
        }

        if (retVal == null) {
            String msg = personEduInstitution.getPerson().getFullFio() + " Для документа об образовании с типом " + stage.getTitle() + " нет аналога в ФИС ";
            if (PackUnpackToFis.DAEMON_PARAMS.isSuspendErrorDoc())
                PackUnpackToFis.AddPkgError(msg);
            else
                throw new ApplicationException(msg);

        }
        return retVal;
    }


    private Element packToXmlSchoolCertificateDocument(Document doc, XMLSchoolCertificateDocument xmlWrapper)
    {
        Element element = doc.createElement("SchoolCertificateDocument");

        // не обязательно
        if (xmlWrapper.getEndYear() != null
                && _validEndYear(xmlWrapper.getEndYear()))
            ToolsDao.PackToElement(doc, element, "EndYear", Integer.toString(xmlWrapper.getEndYear()));

        packBaseWrapper(doc, element, xmlWrapper);

        return element;
    }


    private Element packToXmlHighEduDiplomaDocument(Document doc, XMLHighEduDiplomaDocument xmlWrapper)
    {

        Element element = doc.createElement("HighEduDiplomaDocument");

        // необязательно
        if (xmlWrapper.getEndYear() != null
                && _validEndYear(xmlWrapper.getEndYear()))
            ToolsDao.PackToElement(doc, element, "EndYear", Integer.toString(xmlWrapper.getEndYear()));

        // необязательно
        if (xmlWrapper.getRegistrationNumber() != null)
            ToolsDao.PackToElement(doc, element, "RegistrationNumber", xmlWrapper.getRegistrationNumber());

        packBaseWrapper(doc, element, xmlWrapper);

        return element;
    }

    private Element packToXmlMiddleEduDiplomaDocument(Document doc, XMLMiddleEduDiplomaDocument xmlWrapper)
    {
        Element element = doc.createElement("MiddleEduDiplomaDocument");

        // не обязательно
        if (xmlWrapper.getEndYear() != null
                && _validEndYear(xmlWrapper.getEndYear()))
            ToolsDao.PackToElement(doc, element, "EndYear", Integer.toString(xmlWrapper.getEndYear()));

        // не обязательно
        if (xmlWrapper.getRegistrationNumber() != null)
            ToolsDao.PackToElement(doc, element, "RegistrationNumber", xmlWrapper.getRegistrationNumber());

        packBaseWrapper(doc, element, xmlWrapper);

        return element;
    }

    private boolean _validEndYear(Integer endYear)
    {

        if (endYear == null)
            return false;

        if (endYear < 1974)
            return false;

        int year = ToolsDao.getYear(new Date());

        if (endYear > year)
            return false;

        return true;
    }

    private Element packToXmlBasicDiplomaDocument(Document doc, XMLBasicDiplomaDocument xmlWrapper)
    {
        Element element = doc.createElement("BasicDiplomaDocument");

        // не обязательно
        if (xmlWrapper.getEndYear() != null
                && _validEndYear(xmlWrapper.getEndYear()))
            ToolsDao.PackToElement(doc, element, "EndYear", Integer.toString(xmlWrapper.getEndYear()));

        // не обязательно
        if (xmlWrapper.getRegistrationNumber() != null)
            ToolsDao.PackToElement(doc, element, "RegistrationNumber", xmlWrapper.getRegistrationNumber());

        packBaseWrapper(doc, element, xmlWrapper);

        return element;
    }

    private Element packToXmlIncomplHighEduDiplomaDocument(Document doc, XMLIncomplHighEduDiplomaDocument xmlWrapper)
    {
        Element element = doc.createElement("IncomplHighEduDiplomaDocument");

        // не обязательно
        if (xmlWrapper.getRegistrationNumber() != null)
            ToolsDao.PackToElement(doc, element, "RegistrationNumber", xmlWrapper.getRegistrationNumber());

        packBaseWrapper(doc, element, xmlWrapper);
        return element;
    }

    private Element packToXmlAcademicDiplomaDocument(Document doc, XMLAcademicDiplomaDocument xmlWrapper)
    {

        Element element = doc.createElement("AcademicDiplomaDocument");

        // не обязательно
        if (xmlWrapper.getRegistrationNumber() != null)
            ToolsDao.PackToElement(doc, element, "RegistrationNumber", xmlWrapper.getRegistrationNumber());

        packBaseWrapper(doc, element, xmlWrapper);

        return element;
    }

    private Element packToXmlSchoolCertificateBasicDocument(Document doc, XMLSchoolCertificateBasicDocument xmlWrapper)
    {
        Element element = doc.createElement("SchoolCertificateBasicDocument");
        packBaseWrapper(doc, element, xmlWrapper);

        return element;
    }

    private void packBaseWrapper(Document doc, Element element, XMLBaseEducationDocument xmlWrapper)
    {
        // для документов об образовании это поле не является обязательным - посему отключаю
        // чук 2013/03/05
        // ToolsDao.PackUid(doc, element, xmlWrapper.getUid());

        ToolsDao.PackOriginalReceived(doc, element, xmlWrapper.isOriginalReceived());

        if (xmlWrapper.isOriginalReceived() && xmlWrapper.getOriginalReceivedDate() != null)
            ToolsDao.PackToElement(doc, element, "OriginalReceivedDate", xmlWrapper.getOriginalReceivedDate(), false, false);

        // обязательное поле
        String docSeries = "б/с";
        if (xmlWrapper.getDocumentSeries() != null)
            docSeries = xmlWrapper.getDocumentSeries();
        else {
            // варнинг документа об образовании снимаю
			/*
            String err = "У документа об образовании для " + xmlWrapper.getPersonEduInstitution().getPerson().getFullFio() + " не указана серия документа";
            if(PackUnpackToFis.DAEMON_PARAMS.isSuspendErrorDoc())
                PackUnpackToFis.AddPkgError(err);
			else
                throw new ApplicationException(err);
            */
        }
        ToolsDao.PackToElement(doc, element, "DocumentSeries", docSeries);

        // обязательное поле
        String docNumber = "б/н";
        if (xmlWrapper.getDocumentNumber() != null)
            docNumber = xmlWrapper.getDocumentNumber();
        else {
            String err = "У документа об образовании для " + xmlWrapper.getPersonEduInstitution().getPerson().getFullFio() + " не указан номер документа";
            if (PackUnpackToFis.DAEMON_PARAMS.isSuspendErrorDoc())
                PackUnpackToFis.AddPkgError(err);
            else
                throw new ApplicationException(err);
        }
        ToolsDao.PackToElement(doc, element, "DocumentNumber", docNumber);

        if (xmlWrapper.getDocumentDate() != null)
            ToolsDao.PackToElement(doc, element, "DocumentDate", xmlWrapper.getDocumentDate(), false, false);

        if (xmlWrapper.getDocumentOrganization() != null)
            ToolsDao.PackToElement(doc, element, "DocumentOrganization", xmlWrapper.getDocumentOrganization());

    }

    private XMLSchoolCertificateDocument getXMLSchoolCertificateDocument(PersonEduInstitution personEduInstitution)
    {
        String uid = personEduInstitution.getId().toString();
        String documentSeries = personEduInstitution.getSeria();
        String documentNumber = personEduInstitution.getNumber();
        Date documentDate = personEduInstitution.getIssuanceDate();
        String documentOrganization = personEduInstitution.getEduInstitution() != null ? personEduInstitution.getEduInstitution().getTitle() : null;
        int yearEnd = personEduInstitution.getYearEnd();
        return new XMLSchoolCertificateDocument(uid, isOriginalReceived, entrantRequest.getRegDate(), documentSeries, documentNumber, documentDate, documentOrganization, yearEnd, personEduInstitution);
    }


    private XMLSchoolCertificateBasicDocument getXMLSchoolCertificateBasicDocument(
            PersonEduInstitution personEduInstitution)
    {
        String uid = personEduInstitution.getId().toString();
        String documentSeries = personEduInstitution.getSeria();
        String documentNumber = personEduInstitution.getNumber();
        Date documentDate = personEduInstitution.getIssuanceDate();
        String documentOrganization = personEduInstitution.getEduInstitution() != null ? personEduInstitution.getEduInstitution().getTitle() : null;
        return new XMLSchoolCertificateBasicDocument(uid, isOriginalReceived, entrantRequest.getRegDate(), documentSeries, documentNumber, documentDate, documentOrganization, personEduInstitution);

    }

    private XMLAcademicDiplomaDocument getXMLAcademicDiplomaDocument(
            PersonEduInstitution personEduInstitution)
    {
        String uid = personEduInstitution.getId().toString();
        String documentSeries = personEduInstitution.getSeria();
        String documentNumber = personEduInstitution.getNumber();
        Date documentDate = personEduInstitution.getIssuanceDate();
        String documentOrganization = personEduInstitution.getEduInstitution() != null ? personEduInstitution.getEduInstitution().getTitle() : null;

        //TODO
        String registrationNumber = null;
        return new XMLAcademicDiplomaDocument(uid, isOriginalReceived, entrantRequest.getRegDate(), documentSeries, documentNumber, documentDate, documentOrganization, registrationNumber, personEduInstitution);

    }

    private XMLIncomplHighEduDiplomaDocument getXMLIncomplHighEduDiplomaDocument(
            PersonEduInstitution personEduInstitution)
    {

        String uid = personEduInstitution.getId().toString();
        String documentSeries = personEduInstitution.getSeria();
        String documentNumber = personEduInstitution.getNumber();
        Date documentDate = personEduInstitution.getIssuanceDate();
        String documentOrganization = personEduInstitution.getEduInstitution() != null ? personEduInstitution.getEduInstitution().getTitle() : null;

        //TODO
        String registrationNumber = null;
        return new XMLIncomplHighEduDiplomaDocument(uid, isOriginalReceived, entrantRequest.getRegDate(), documentSeries, documentNumber, documentDate, documentOrganization, registrationNumber, personEduInstitution);

    }

    private XMLBasicDiplomaDocument getXMLBasicDiplomaDocument(
            PersonEduInstitution personEduInstitution)
    {

        String uid = personEduInstitution.getId().toString();
        String documentSeries = personEduInstitution.getSeria();
        String documentNumber = personEduInstitution.getNumber();
        Date documentDate = personEduInstitution.getIssuanceDate();
        String documentOrganization = personEduInstitution.getEduInstitution() != null ? personEduInstitution.getEduInstitution().getTitle() : null;
        int yearEnd = personEduInstitution.getYearEnd();

        //TODO
        String registrationNumber = null;
        return new XMLBasicDiplomaDocument(uid, isOriginalReceived, entrantRequest.getRegDate(), documentSeries, documentNumber, documentDate, documentOrganization, yearEnd, registrationNumber, personEduInstitution);

    }

    private XMLMiddleEduDiplomaDocument getXMLMiddleEduDiplomaDocument(
            PersonEduInstitution personEduInstitution)
    {

        String uid = personEduInstitution.getId().toString();
        String documentSeries = personEduInstitution.getSeria();
        String documentNumber = personEduInstitution.getNumber();
        Date documentDate = personEduInstitution.getIssuanceDate();
        String documentOrganization = personEduInstitution.getEduInstitution() != null ? personEduInstitution.getEduInstitution().getTitle() : null;
        int yearEnd = personEduInstitution.getYearEnd();

        //TODO
        String registrationNumber = null;
        return new XMLMiddleEduDiplomaDocument(uid, isOriginalReceived, entrantRequest.getRegDate(), documentSeries, documentNumber, documentDate, documentOrganization, yearEnd, registrationNumber, personEduInstitution);

    }

    private XMLHighEduDiplomaDocument getXMLHighEduDiplomaDocument(
            PersonEduInstitution personEduInstitution)
    {

        String uid = personEduInstitution.getId().toString();
        String documentSeries = personEduInstitution.getSeria();
        String documentNumber = personEduInstitution.getNumber();
        Date documentDate = personEduInstitution.getIssuanceDate();
        String documentOrganization = personEduInstitution.getEduInstitution() != null ? personEduInstitution.getEduInstitution().getTitle() : null;
        int yearEnd = personEduInstitution.getYearEnd();

        //TODO
        String registrationNumber = null;
        return new XMLHighEduDiplomaDocument(uid, isOriginalReceived, entrantRequest.getRegDate(), documentSeries, documentNumber, documentDate, documentOrganization, yearEnd, registrationNumber, personEduInstitution);

    }

}
