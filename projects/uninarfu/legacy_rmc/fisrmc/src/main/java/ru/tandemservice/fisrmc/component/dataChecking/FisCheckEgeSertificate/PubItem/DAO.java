package ru.tandemservice.fisrmc.component.dataChecking.FisCheckEgeSertificate.PubItem;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.fisrmc.entity.FisCheckEgeSertificate;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;

import java.util.ArrayList;
import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        model.setCheckEgeSertificate((FisCheckEgeSertificate) getNotNull(model.getEntityId()));

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EntrantRequest.class, "er")
                .where(DQLExpressions.eq(DQLExpressions.property(EntrantRequest.entrant().fromAlias("er")), DQLExpressions.value(model.getCheckEgeSertificate().getEntrantStateExamCertificate().getEntrant())))
                .where(DQLExpressions.eq(DQLExpressions.property(EntrantRequest.entrant().enrollmentCampaign().fromAlias("er")), DQLExpressions.value(model.getCheckEgeSertificate().getEntrantStateExamCertificate().getEntrant().getEnrollmentCampaign())));
        List<EntrantRequest> list = getList(builder);
        List<String> numberList = new ArrayList<String>();
        for (EntrantRequest request : list) {
            numberList.add(request.getStringNumber());
        }
        model.setRequestNambers(StringUtils.join(numberList, ", "));
    }
}
