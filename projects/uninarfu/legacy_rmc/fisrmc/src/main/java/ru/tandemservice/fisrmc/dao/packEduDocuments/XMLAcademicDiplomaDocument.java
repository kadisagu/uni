package ru.tandemservice.fisrmc.dao.packEduDocuments;

import org.tandemframework.shared.person.base.entity.PersonEduInstitution;

import java.util.Date;

/**
 * академическая справка
 *
 * @author belarin
 */
public class XMLAcademicDiplomaDocument
        extends XMLBaseEducationDocument
{

    /**
     * Регистрационный номер, не обязательно
     */
    private String registrationNumber;

    public XMLAcademicDiplomaDocument
            (
                    String uid
                    , boolean originalReceived
                    , Date originalReceivedDate
                    , String documentSeries
                    , String documentNumber
                    , Date documentDate
                    , String documentOrganization
                    , String registrationNumber
                    , PersonEduInstitution personEduInstitution
            )
    {
        super(uid, originalReceived, originalReceivedDate, documentSeries,
              documentNumber, documentDate, documentOrganization, personEduInstitution);

        setRegistrationNumber(registrationNumber);
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }


}
