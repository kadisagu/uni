package ru.tandemservice.fisrmc.dao;

import ru.tandemservice.fisrmc.entity.catalog.EducationdirectionSpravType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Параметры запуска демона
 *
 * @author vch
 */
public class DaemonParams {

    public static boolean NEED_RUN = false;

    private boolean campaignInfo;
    private boolean admissionInfo;
    private boolean applications;

    private boolean entranceTestResult;

    private boolean testMode;
    private boolean ordersOfAdmission;
    private boolean benefitRequest;
    private List<EnrollmentCampaign> enrollmentCampaigns = null;
    private String userName;
    private String userPassword;
    private List<Integer> regNumbers = new ArrayList<>();
    private List<Integer> excludeRegNumbers = new ArrayList<>();


    private boolean notRf = false;
    private boolean suspendErrorDoc = false;
    private boolean abfuscateData = true;
    private boolean suspendCompetitiveGroup = false;
    private boolean appDateWithoutTime = false;

    private Date dateRequestFrom = null;
    private Date dateRequestTo = null;
    private boolean whithoutSendedInFis = false;
    private boolean whithoutEntrantInOrder = false;

    private EducationdirectionSpravType spravType = null;


    public DaemonParams
            (
                    String userName
                    , String userPassword
                    , boolean campaignInfo
                    , boolean admissionInfo
                    , boolean applications
                    , boolean entranceTestResult
                    , boolean testMode
                    , boolean ordersOfAdmission
                    , boolean benefitRequest

                    , boolean notRf
                    , boolean suspendErrorDoc
                    , boolean abfuscateData
                    , boolean suspendCompetitiveGroup
                    , boolean appDateWithoutTime

                    , List<Integer> regNumbers
                    , List<Integer> excludeRegNumbers

                    , Date dateRequestFrom
                    , Date dateRequestTo
                    , boolean whithoutSendedInFis
                    , boolean whithoutEntrantInOrder

                    , List<EnrollmentCampaign> enrollmentCampaigns
                    , EducationdirectionSpravType spravType
            )
    {
        this.setAppDateWithoutTime(appDateWithoutTime);
        this.setNotRf(notRf);
        this.setSuspendErrorDoc(suspendErrorDoc);
        this.setAbfuscateData(abfuscateData);
        this.setSuspendCompetitiveGroup(suspendCompetitiveGroup);

        setCampaignInfo(campaignInfo);
        setAdmissionInfo(admissionInfo);
        setApplications(applications);
        setTestMode(testMode);
        setOrdersOfAdmission(ordersOfAdmission);
        setEnrollmentCampaigns(enrollmentCampaigns);
        setUserName(userName);
        setUserPassword(userPassword);
        setBenefitRequest(benefitRequest);
        setEntranceTestResult(entranceTestResult);
        setRegNumbers(regNumbers);
        setExcludeRegNumbers(excludeRegNumbers);


        this.dateRequestFrom = dateRequestFrom;
        this.dateRequestTo = dateRequestTo;
        this.whithoutSendedInFis = whithoutSendedInFis;
        this.whithoutEntrantInOrder = whithoutEntrantInOrder;
        this.setSpravType(spravType);
    }

    public void setCampaignInfo(boolean campaignInfo) {
        this.campaignInfo = campaignInfo;
    }

    public boolean isCampaignInfo() {
        return campaignInfo;
    }

    public void setAdmissionInfo(boolean admissionInfo) {
        this.admissionInfo = admissionInfo;
    }

    public boolean isAdmissionInfo() {
        return admissionInfo;
    }

    public void setApplications(boolean applications) {
        this.applications = applications;
    }

    public boolean isApplications() {
        return applications;
    }

    public void setTestMode(boolean testMode) {
        this.testMode = testMode;
    }

    public boolean isTestMode() {
        return testMode;
    }

    public void setOrdersOfAdmission(boolean ordersOfAdmission) {
        this.ordersOfAdmission = ordersOfAdmission;
    }

    public boolean isOrdersOfAdmission() {
        return ordersOfAdmission;
    }

    public void setEnrollmentCampaigns(List<EnrollmentCampaign> enrollmentCampaigns) {
        this.enrollmentCampaigns = enrollmentCampaigns;
    }

    public List<EnrollmentCampaign> getEnrollmentCampaigns() {
        return enrollmentCampaigns;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setEntranceTestResult(boolean entranceTestResult) {
        this.entranceTestResult = entranceTestResult;
    }

    public boolean isEntranceTestResult() {
        return entranceTestResult;
    }

    public void setBenefitRequest(boolean benefitRequest) {
        this.benefitRequest = benefitRequest;
    }

    public boolean isBenefitRequest() {
        return benefitRequest;
    }


    public void setRegNumbers(List<Integer> regNumbers) {
        this.regNumbers = regNumbers;
    }

    public List<Integer> getRegNumbers() {
        return regNumbers;
    }

    public void setExcludeRegNumbers(List<Integer> excludeRegNumbers) {
        this.excludeRegNumbers = excludeRegNumbers;
    }

    public List<Integer> getExcludeRegNumbers() {
        return excludeRegNumbers;
    }

    public void setNotRf(boolean notRf) {
        this.notRf = notRf;
    }

    public boolean isNotRf() {
        return notRf;
    }

    public void setSuspendErrorDoc(boolean suspendErrorDoc) {
        this.suspendErrorDoc = suspendErrorDoc;
    }

    public boolean isSuspendErrorDoc() {
        return suspendErrorDoc;
    }

    public void setAbfuscateData(boolean abfuscateData) {
        this.abfuscateData = abfuscateData;
    }

    public boolean isAbfuscateData() {
        return abfuscateData;
    }

    public void setSuspendCompetitiveGroup(boolean suspendCompetitiveGroup) {
        this.suspendCompetitiveGroup = suspendCompetitiveGroup;
    }

    public boolean isSuspendCompetitiveGroup() {
        return suspendCompetitiveGroup;
    }

    public void setAppDateWithoutTime(boolean appDateWithoutTime) {
        this.appDateWithoutTime = appDateWithoutTime;
    }

    public boolean isAppDateWithoutTime() {
        return appDateWithoutTime;
    }

    public void setDateRequestFrom(Date dateRequestFrom) {
        this.dateRequestFrom = dateRequestFrom;
    }

    public Date getDateRequestFrom() {
        return dateRequestFrom;
    }

    public void setDateRequestTo(Date dateRequestTo) {
        this.dateRequestTo = dateRequestTo;
    }

    public Date getDateRequestTo() {
        return dateRequestTo;
    }

    public void setWhithoutSendedInFis(boolean whithoutSendedInFis) {
        this.whithoutSendedInFis = whithoutSendedInFis;
    }

    public boolean isWhithoutSendedInFis() {
        return whithoutSendedInFis;
    }

    public void setWhithoutEntrantInOrder(boolean whithoutEntrantInOrder) {
        this.whithoutEntrantInOrder = whithoutEntrantInOrder;
    }

    public boolean isWhithoutEntrantInOrder() {
        return whithoutEntrantInOrder;
    }

    public EducationdirectionSpravType getSpravType() {
        return spravType;
    }

    public void setSpravType(EducationdirectionSpravType spravType) {
        this.spravType = spravType;
    }
}
