package ru.tandemservice.fisrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.fisrmc.entity.Educationdirectionfis;
import ru.tandemservice.fisrmc.entity.catalog.EducationdirectionSpravType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Направления подготовки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EducationdirectionfisGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.fisrmc.entity.Educationdirectionfis";
    public static final String ENTITY_NAME = "educationdirectionfis";
    public static final int VERSION_HASH = 1479163330;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_CODE_OLD10 = "codeOld10";
    public static final String P_TITLE = "title";
    public static final String P_CODEFIS = "Codefis";
    public static final String P_NEW_CODE = "NewCode";
    public static final String P_QUALIFICATION_CODE = "QualificationCode";
    public static final String P_PERIOD = "Period";
    public static final String P_U_G_S_CODE = "UGSCode";
    public static final String P_U_G_S_NAME = "UGSName";
    public static final String P_EXPORT_COMMENT = "exportComment";
    public static final String L_SPRAV_TYPE = "spravType";

    private String _code; 
    private String _codeOld10; 
    private String _title; 
    private String _Codefis; 
    private String _NewCode; 
    private String _QualificationCode; 
    private String _Period; 
    private String _UGSCode; 
    private String _UGSName; 
    private String _exportComment; 
    private EducationdirectionSpravType _spravType;     // Тип справочника

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code 
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getCodeOld10()
    {
        return _codeOld10;
    }

    /**
     * @param codeOld10 
     */
    public void setCodeOld10(String codeOld10)
    {
        dirty(_codeOld10, codeOld10);
        _codeOld10 = codeOld10;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title 
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getCodefis()
    {
        return _Codefis;
    }

    /**
     * @param Codefis 
     */
    public void setCodefis(String Codefis)
    {
        dirty(_Codefis, Codefis);
        _Codefis = Codefis;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getNewCode()
    {
        return _NewCode;
    }

    /**
     * @param NewCode 
     */
    public void setNewCode(String NewCode)
    {
        dirty(_NewCode, NewCode);
        _NewCode = NewCode;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getQualificationCode()
    {
        return _QualificationCode;
    }

    /**
     * @param QualificationCode  Свойство не может быть null.
     */
    public void setQualificationCode(String QualificationCode)
    {
        dirty(_QualificationCode, QualificationCode);
        _QualificationCode = QualificationCode;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getPeriod()
    {
        return _Period;
    }

    /**
     * @param Period 
     */
    public void setPeriod(String Period)
    {
        dirty(_Period, Period);
        _Period = Period;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getUGSCode()
    {
        return _UGSCode;
    }

    /**
     * @param UGSCode 
     */
    public void setUGSCode(String UGSCode)
    {
        dirty(_UGSCode, UGSCode);
        _UGSCode = UGSCode;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getUGSName()
    {
        return _UGSName;
    }

    /**
     * @param UGSName 
     */
    public void setUGSName(String UGSName)
    {
        dirty(_UGSName, UGSName);
        _UGSName = UGSName;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getExportComment()
    {
        return _exportComment;
    }

    /**
     * @param exportComment 
     */
    public void setExportComment(String exportComment)
    {
        dirty(_exportComment, exportComment);
        _exportComment = exportComment;
    }

    /**
     * @return Тип справочника. Свойство не может быть null.
     */
    @NotNull
    public EducationdirectionSpravType getSpravType()
    {
        return _spravType;
    }

    /**
     * @param spravType Тип справочника. Свойство не может быть null.
     */
    public void setSpravType(EducationdirectionSpravType spravType)
    {
        dirty(_spravType, spravType);
        _spravType = spravType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EducationdirectionfisGen)
        {
            setCode(((Educationdirectionfis)another).getCode());
            setCodeOld10(((Educationdirectionfis)another).getCodeOld10());
            setTitle(((Educationdirectionfis)another).getTitle());
            setCodefis(((Educationdirectionfis)another).getCodefis());
            setNewCode(((Educationdirectionfis)another).getNewCode());
            setQualificationCode(((Educationdirectionfis)another).getQualificationCode());
            setPeriod(((Educationdirectionfis)another).getPeriod());
            setUGSCode(((Educationdirectionfis)another).getUGSCode());
            setUGSName(((Educationdirectionfis)another).getUGSName());
            setExportComment(((Educationdirectionfis)another).getExportComment());
            setSpravType(((Educationdirectionfis)another).getSpravType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EducationdirectionfisGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Educationdirectionfis.class;
        }

        public T newInstance()
        {
            return (T) new Educationdirectionfis();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "codeOld10":
                    return obj.getCodeOld10();
                case "title":
                    return obj.getTitle();
                case "Codefis":
                    return obj.getCodefis();
                case "NewCode":
                    return obj.getNewCode();
                case "QualificationCode":
                    return obj.getQualificationCode();
                case "Period":
                    return obj.getPeriod();
                case "UGSCode":
                    return obj.getUGSCode();
                case "UGSName":
                    return obj.getUGSName();
                case "exportComment":
                    return obj.getExportComment();
                case "spravType":
                    return obj.getSpravType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "codeOld10":
                    obj.setCodeOld10((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "Codefis":
                    obj.setCodefis((String) value);
                    return;
                case "NewCode":
                    obj.setNewCode((String) value);
                    return;
                case "QualificationCode":
                    obj.setQualificationCode((String) value);
                    return;
                case "Period":
                    obj.setPeriod((String) value);
                    return;
                case "UGSCode":
                    obj.setUGSCode((String) value);
                    return;
                case "UGSName":
                    obj.setUGSName((String) value);
                    return;
                case "exportComment":
                    obj.setExportComment((String) value);
                    return;
                case "spravType":
                    obj.setSpravType((EducationdirectionSpravType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "codeOld10":
                        return true;
                case "title":
                        return true;
                case "Codefis":
                        return true;
                case "NewCode":
                        return true;
                case "QualificationCode":
                        return true;
                case "Period":
                        return true;
                case "UGSCode":
                        return true;
                case "UGSName":
                        return true;
                case "exportComment":
                        return true;
                case "spravType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "codeOld10":
                    return true;
                case "title":
                    return true;
                case "Codefis":
                    return true;
                case "NewCode":
                    return true;
                case "QualificationCode":
                    return true;
                case "Period":
                    return true;
                case "UGSCode":
                    return true;
                case "UGSName":
                    return true;
                case "exportComment":
                    return true;
                case "spravType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "codeOld10":
                    return String.class;
                case "title":
                    return String.class;
                case "Codefis":
                    return String.class;
                case "NewCode":
                    return String.class;
                case "QualificationCode":
                    return String.class;
                case "Period":
                    return String.class;
                case "UGSCode":
                    return String.class;
                case "UGSName":
                    return String.class;
                case "exportComment":
                    return String.class;
                case "spravType":
                    return EducationdirectionSpravType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Educationdirectionfis> _dslPath = new Path<Educationdirectionfis>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Educationdirectionfis");
    }
            

    /**
     * @return 
     * @see ru.tandemservice.fisrmc.entity.Educationdirectionfis#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return 
     * @see ru.tandemservice.fisrmc.entity.Educationdirectionfis#getCodeOld10()
     */
    public static PropertyPath<String> codeOld10()
    {
        return _dslPath.codeOld10();
    }

    /**
     * @return 
     * @see ru.tandemservice.fisrmc.entity.Educationdirectionfis#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return 
     * @see ru.tandemservice.fisrmc.entity.Educationdirectionfis#getCodefis()
     */
    public static PropertyPath<String> Codefis()
    {
        return _dslPath.Codefis();
    }

    /**
     * @return 
     * @see ru.tandemservice.fisrmc.entity.Educationdirectionfis#getNewCode()
     */
    public static PropertyPath<String> NewCode()
    {
        return _dslPath.NewCode();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.Educationdirectionfis#getQualificationCode()
     */
    public static PropertyPath<String> QualificationCode()
    {
        return _dslPath.QualificationCode();
    }

    /**
     * @return 
     * @see ru.tandemservice.fisrmc.entity.Educationdirectionfis#getPeriod()
     */
    public static PropertyPath<String> Period()
    {
        return _dslPath.Period();
    }

    /**
     * @return 
     * @see ru.tandemservice.fisrmc.entity.Educationdirectionfis#getUGSCode()
     */
    public static PropertyPath<String> UGSCode()
    {
        return _dslPath.UGSCode();
    }

    /**
     * @return 
     * @see ru.tandemservice.fisrmc.entity.Educationdirectionfis#getUGSName()
     */
    public static PropertyPath<String> UGSName()
    {
        return _dslPath.UGSName();
    }

    /**
     * @return 
     * @see ru.tandemservice.fisrmc.entity.Educationdirectionfis#getExportComment()
     */
    public static PropertyPath<String> exportComment()
    {
        return _dslPath.exportComment();
    }

    /**
     * @return Тип справочника. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.Educationdirectionfis#getSpravType()
     */
    public static EducationdirectionSpravType.Path<EducationdirectionSpravType> spravType()
    {
        return _dslPath.spravType();
    }

    public static class Path<E extends Educationdirectionfis> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _codeOld10;
        private PropertyPath<String> _title;
        private PropertyPath<String> _Codefis;
        private PropertyPath<String> _NewCode;
        private PropertyPath<String> _QualificationCode;
        private PropertyPath<String> _Period;
        private PropertyPath<String> _UGSCode;
        private PropertyPath<String> _UGSName;
        private PropertyPath<String> _exportComment;
        private EducationdirectionSpravType.Path<EducationdirectionSpravType> _spravType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return 
     * @see ru.tandemservice.fisrmc.entity.Educationdirectionfis#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EducationdirectionfisGen.P_CODE, this);
            return _code;
        }

    /**
     * @return 
     * @see ru.tandemservice.fisrmc.entity.Educationdirectionfis#getCodeOld10()
     */
        public PropertyPath<String> codeOld10()
        {
            if(_codeOld10 == null )
                _codeOld10 = new PropertyPath<String>(EducationdirectionfisGen.P_CODE_OLD10, this);
            return _codeOld10;
        }

    /**
     * @return 
     * @see ru.tandemservice.fisrmc.entity.Educationdirectionfis#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EducationdirectionfisGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return 
     * @see ru.tandemservice.fisrmc.entity.Educationdirectionfis#getCodefis()
     */
        public PropertyPath<String> Codefis()
        {
            if(_Codefis == null )
                _Codefis = new PropertyPath<String>(EducationdirectionfisGen.P_CODEFIS, this);
            return _Codefis;
        }

    /**
     * @return 
     * @see ru.tandemservice.fisrmc.entity.Educationdirectionfis#getNewCode()
     */
        public PropertyPath<String> NewCode()
        {
            if(_NewCode == null )
                _NewCode = new PropertyPath<String>(EducationdirectionfisGen.P_NEW_CODE, this);
            return _NewCode;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.Educationdirectionfis#getQualificationCode()
     */
        public PropertyPath<String> QualificationCode()
        {
            if(_QualificationCode == null )
                _QualificationCode = new PropertyPath<String>(EducationdirectionfisGen.P_QUALIFICATION_CODE, this);
            return _QualificationCode;
        }

    /**
     * @return 
     * @see ru.tandemservice.fisrmc.entity.Educationdirectionfis#getPeriod()
     */
        public PropertyPath<String> Period()
        {
            if(_Period == null )
                _Period = new PropertyPath<String>(EducationdirectionfisGen.P_PERIOD, this);
            return _Period;
        }

    /**
     * @return 
     * @see ru.tandemservice.fisrmc.entity.Educationdirectionfis#getUGSCode()
     */
        public PropertyPath<String> UGSCode()
        {
            if(_UGSCode == null )
                _UGSCode = new PropertyPath<String>(EducationdirectionfisGen.P_U_G_S_CODE, this);
            return _UGSCode;
        }

    /**
     * @return 
     * @see ru.tandemservice.fisrmc.entity.Educationdirectionfis#getUGSName()
     */
        public PropertyPath<String> UGSName()
        {
            if(_UGSName == null )
                _UGSName = new PropertyPath<String>(EducationdirectionfisGen.P_U_G_S_NAME, this);
            return _UGSName;
        }

    /**
     * @return 
     * @see ru.tandemservice.fisrmc.entity.Educationdirectionfis#getExportComment()
     */
        public PropertyPath<String> exportComment()
        {
            if(_exportComment == null )
                _exportComment = new PropertyPath<String>(EducationdirectionfisGen.P_EXPORT_COMMENT, this);
            return _exportComment;
        }

    /**
     * @return Тип справочника. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.Educationdirectionfis#getSpravType()
     */
        public EducationdirectionSpravType.Path<EducationdirectionSpravType> spravType()
        {
            if(_spravType == null )
                _spravType = new EducationdirectionSpravType.Path<EducationdirectionSpravType>(L_SPRAV_TYPE, this);
            return _spravType;
        }

        public Class getEntityClass()
        {
            return Educationdirectionfis.class;
        }

        public String getEntityName()
        {
            return "educationdirectionfis";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
