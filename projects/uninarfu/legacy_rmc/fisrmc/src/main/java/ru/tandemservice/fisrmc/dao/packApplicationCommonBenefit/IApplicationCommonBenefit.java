package ru.tandemservice.fisrmc.dao.packApplicationCommonBenefit;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.tandemservice.fisrmc.dao.XMLCompetitiveGroup;
import ru.tandemservice.fisrmc.dao.interfaces.ICache;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.List;

public interface IApplicationCommonBenefit
        extends ICache
{
//	public Element packApplicationCommonBenefitOld2013
//	(
//			Document doc
//			, EntrantRequest entrantrequest
//			, List<EnrollmentDirection> lstEnrollmentDirection 
//			, List<XMLCompetitiveGroup> lstCompetitiveGroup
//			, List<RequestedEnrollmentDirection> requestedEnrollmentDirectionBenefitList
//			, boolean hasPersonBenefit
//			, boolean hasOlimpiad
//	);


    /**
     * упаковать сведения о льготах
     * Льготы теперь можно паковать как по олимпиадам
     * (общая льгота только для олимпиады - вне конкурса)
     * так и общие льготы
     *
     * @param doc
     * @param entrantrequest
     * @param lstCompetitiveGroup
     * @param requestedEnrollmentDirectionBenefit
     * @param requestedEnrollmentDirectionOlimpiad
     *
     * @return
     */
    public Element packApplicationCommonBenefit
    (
            Document doc
            , EntrantRequest entrantrequest
            , List<XMLCompetitiveGroup> lstCompetitiveGroup
            , List<RequestedEnrollmentDirection> red
    );

	
	/*
    public List<ChosenEntranceDiscipline> getChosenEntranceDisciplineOlimpiad(RequestedEnrollmentDirection requestedEnrollmentDirection );
	*/

}
