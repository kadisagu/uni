package ru.tandemservice.fisrmc.component.catalog.ecolimpiadlevelfis.EcolimpiadlevelfisAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.fisrmc.entity.catalog.Ecolimpiadlevelfis;

public interface IDAO extends IDefaultCatalogAddEditDAO<Ecolimpiadlevelfis, Model> {

}
