package ru.tandemservice.fisrmc.component.FisAnswerTofisPackagesAddEdit;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.fisrmc.entity.FisAnswerAction;
import ru.tandemservice.fisrmc.entity.FisAnswerTofisPackages;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unibase.UniBaseUtils;

public class Controller
        extends AbstractBusinessController<IDAO, Model>
{

    @Override
    public void onRefreshComponent(IBusinessComponent ibusinesscomponent)
    {
        Model model = this.getModel(ibusinesscomponent);
        getDao().prepare(model);

        prepareDataSource(ibusinesscomponent);

		/*
		model.getFisAnswerTofisPackages().getAnswerDate();
		model.getFisAnswerTofisPackages().getFisAnswerType().getTitle();
		model.getFisAnswerTofisPackages().getFisPrim();
		*/

        if (model.getSettings() == null)
            model.setSettings(ibusinesscomponent.getSettings());


    }

    public void onClickSave(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().update(model);

        // переход в режим редактирования
        // просто ничего ни делаем
    }


    public void onDeleteEntity(IBusinessComponent context)
    {
        Long removeId = (Long) context.getListenerParameter();
        getDao().delete(removeId);
    }


    public void onClickDownloadReport(IBusinessComponent context)
    {
        FisAnswerAction pkg = UniDaoFacade.getCoreDao().get((Long) context.getListenerParameter());
        DatabaseFile dbFile = pkg.getActionReport();

        if (dbFile != null && dbFile.getContent() != null) {
            Integer temporaryId = PrintReportTemporaryStorage.registerTemporaryPrintForm(dbFile.getContent(), "Отчет_" + pkg.getId() + ".rtf");
            activateInRoot(context, new ComponentActivator(IUniComponents.PRINT_REPORT,
                                                           (new UniMap())
                                                                   .add("id", temporaryId)
                                                                   .add("zip", Boolean.FALSE)
                                                                   .add("extension", "rft"))
            );
        }
        else {
            throw new ApplicationException("Отчет пуст");
        }
    }


    public void onClickAction(IBusinessComponent context)
    {
        context.saveSettings();
        getDao().doAction((Model) context.getModel());
    }

    public void onClickSaveExsist(IBusinessComponent context)
    {
        Model model = getModel(context);
        FisAnswerTofisPackages entity = model.getFisAnswerTofisPackages();

        if (entity != null)
            getDao().saveOrUpdate(entity);
    }

    public void onClickDownloadReportError(IBusinessComponent context)
    {
        FisAnswerAction pkg = UniDaoFacade.getCoreDao().get((Long) context.getListenerParameter());
        DatabaseFile dbFile = pkg.getErrorReport();

        if (dbFile != null && dbFile.getContent() != null) {
            Integer temporaryId = PrintReportTemporaryStorage.registerTemporaryPrintForm(dbFile.getContent(), "Ошибки_" + pkg.getId() + ".rtf");
            activateInRoot(context, new ComponentActivator(IUniComponents.PRINT_REPORT,
                                                           (new UniMap())
                                                                   .add("id", temporaryId)
                                                                   .add("zip", Boolean.FALSE)
                                                                   .add("extension", "rft"))
            );
        }
        else {
            throw new ApplicationException("Лог ошибок пуст");
        }

    }


    protected void prepareDataSource(IBusinessComponent context)
    {
        Model model = getModel(context);

        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<FisAnswerAction> dataSourse = UniBaseUtils.createDataSource(context, getDao());

        dataSourse.addColumn(new SimpleColumn("Дата", FisAnswerAction.P_DATE, DateFormatter.DATE_FORMATTER_WITH_TIME).setClickable(false).setOrderable(true));
        dataSourse.addColumn(new SimpleColumn("Тип ответа", FisAnswerAction.fisAnswerTofisPackages().fisAnswerType().title().s()).setClickable(false).setOrderable(false));
        dataSourse.addColumn(new SimpleColumn("Примечание", FisAnswerAction.comment().s()).setClickable(false).setOrderable(false));

        dataSourse.addColumn(new IndicatorColumn("Скачать отчет", null, "onClickDownloadReport").defaultIndicator(new IndicatorColumn.Item("printer", "Скачать")).setImageHeader(false).setDisableSecondSubmit(false).setOrderable(false));
        dataSourse.addColumn(new IndicatorColumn("Скачать ошибки", null, "onClickDownloadReportError").defaultIndicator(new IndicatorColumn.Item("printer", "Скачать")).setImageHeader(false).setDisableSecondSubmit(false).setOrderable(false));

        dataSourse.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onDeleteEntity", "Удалить ответ?"));
        model.setDataSource(dataSourse);
    }

}
