package ru.tandemservice.fisrmc.dao.answer;

import ru.tandemservice.fisrmc.component.UploadFis2TU.Pub.DefaultParser;

public class EntrantRequestParser extends DefaultParser
{
    public static final String ROOT = "Root";
    public static final String DATA = "PackageData";
    public static final String APPLICATIONS = "Applications";
    public static final String APPLICATION = "Application";
    public static final String APPLICATION_UID = "UID";
    public static final String APPLICATION_NUMBER = "ApplicationNumber";
    public static final String APPLICATION_DATE = "RegistrationDate";


    /*
     * - <OrdersOfAdmission>
       - <OrderOfAdmission>
     */
    public EntrantRequestParser(String xml) {
        super(xml);

        this.elementPath = ROOT + "-" + DATA + "-" + APPLICATIONS + "-" + APPLICATION;

        this.propertyMap.put(
                this.elementPath + "-" + APPLICATION_UID,
                "uid"
        );
        this.propertyMap.put(
                this.elementPath + "-" + APPLICATION_NUMBER,
                "number"
        );
        this.propertyMap.put(
                this.elementPath + "-" + APPLICATION_DATE,
                "date"
        );

    }

}
