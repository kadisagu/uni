package ru.tandemservice.fisrmc.dao;

import ru.tandemservice.fisrmc.entity.Educationdirectionfis;
import ru.tandemservice.fisrmc.entity.catalog.Eceducationlevelfis;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.List;

/**
 * Направления подготовки целевого приема
 *
 * @author vch
 */
public class XMLTargetOrganizationItem
        extends XMLEnrollmentDirection
{
    /**
     * Организация, приславшая заявку на целевой прием
     */
    private TargetAdmissionKind targetadmissionkind = null;


//	/**
//	 * план приема на бюджет (целевиков)
//	 */
//	private int budgetPlan = 0;
//	
//	/**
//	 * План приема на контракт (целевиков)
//	 */
//	private int contractPlan = 0;


    private XMLCompetitiveGroup competitiveGroup = null;

    public XMLTargetOrganizationItem
            (
                    XMLCompetitiveGroup competitiveGroup,
                    TargetAdmissionKind targetadmissionkind,
                    Eceducationlevelfis eceducationlevelfis,
                    Educationdirectionfis educationdirectionfis,
                    EnrollmentCampaign enrollmentCampaign,
                    List<EnrollmentDirection> enrollmentDirectionsList
            )
    {
        super(eceducationlevelfis, educationdirectionfis, enrollmentCampaign,
              enrollmentDirectionsList);
        setTargetadmissionkind(targetadmissionkind);

        setCompetitiveGroup(competitiveGroup);

    }

//	public void setBudgetPlan(int budgetPlan) {
//		this.budgetPlan = budgetPlan;
//	}
//
//	public int getBudgetPlan() {
//		return budgetPlan;
//	}
//
//	public void setContractPlan(int contractPlan) {
//		this.contractPlan = contractPlan;
//	}
//
//	public int getContractPlan() {
//		return contractPlan;
//	}

    public void setTargetadmissionkind(TargetAdmissionKind targetadmissionkind) {
        this.targetadmissionkind = targetadmissionkind;
    }

    public TargetAdmissionKind getTargetadmissionkind() {
        return targetadmissionkind;
    }


    public String getUid()
    {
        // у одного getEducationdirectionfis может быть несколько направлений
        // подготовки для приема по версии тандема
        // это нужно учитывать при формировании ключа

        String key = Long.toString(targetadmissionkind.getId());
        key += Long.toString(getEceducationlevelfis().getId());
        key += Long.toString(getEducationdirectionfis().getId());
        key += ":" + getCompetitiveGroup().getUid();
        return "hk:" + key.hashCode();
    }

    public void setCompetitiveGroup(XMLCompetitiveGroup competitiveGroup) {
        this.competitiveGroup = competitiveGroup;
    }

    public XMLCompetitiveGroup getCompetitiveGroup() {
        return competitiveGroup;
    }
}
