package ru.tandemservice.fisrmc.component.catalog.olimpiatfis.OlimpiatfisAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.fisrmc.entity.catalog.Ecolimpiadlevelfis;
import ru.tandemservice.fisrmc.entity.catalog.Olimpiatfis;


public class Model extends DefaultCatalogAddEditModel<Olimpiatfis> {

    private ISelectModel ecolimpiadlevelfisModel;
    private Ecolimpiadlevelfis ecolimpiadlevelfis;

    public ISelectModel getEcolimpiadlevelfisModel() {
        return ecolimpiadlevelfisModel;
    }

    public void setEcolimpiadlevelfisModel(ISelectModel ecolimpiadlevelfisModel) {
        this.ecolimpiadlevelfisModel = ecolimpiadlevelfisModel;
    }

    public Ecolimpiadlevelfis getEcolimpiadlevelfis() {
        return ecolimpiadlevelfis;
    }

    public void setEcolimpiadlevelfis(Ecolimpiadlevelfis ecolimpiadlevelfis) {
        this.ecolimpiadlevelfis = ecolimpiadlevelfis;
    }
}
