package ru.tandemservice.fisrmc.component.FisAnswerTofisPackagesAddEdit;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO
        extends IUniDao<Model>
{

    public void createNewFisAnswerAction(Model model);

    public void doAction(Model model);
}
