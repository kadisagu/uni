package ru.tandemservice.fisrmc.component.settings.EnrollmentCampaignAddEdit;

import ru.tandemservice.fisrmc.entity.EnrollmentCampaignFisExt;


public class Model extends ru.tandemservice.uniecrmc.component.settings.EnrollmentCampaignAddEdit.Model {

    private EnrollmentCampaignFisExt enrollmentCampaignFisExt;

    public EnrollmentCampaignFisExt getEnrollmentCampaignFisExt() {
        return enrollmentCampaignFisExt;
    }

    public void setEnrollmentCampaignFisExt(
            EnrollmentCampaignFisExt enrollmentCampaignFisExt)
    {
        this.enrollmentCampaignFisExt = enrollmentCampaignFisExt;
    }

}
