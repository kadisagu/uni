package ru.tandemservice.fisrmc.component.dataChecking.FisCheckEntrantRequest.List;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import ru.tandemservice.fisrmc.entity.FisCheckEntrantRequest;
import ru.tandemservice.fisrmc.entity.catalog.FisAppStatusCode;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.setFisAppStatusCodeModel(new LazySimpleSelectModel<>(FisAppStatusCode.class).setSortProperty("title"));
        model.setErrorCodeModel(new LazySimpleSelectModel<>(getErrorCodeList()));
    }

    private List<DataWrapper> getErrorCodeList() {
        MQBuilder builder = new MQBuilder(FisCheckEntrantRequest.ENTITY_CLASS, "e")
                .addOrder("e", FisCheckEntrantRequest.errorCode());
        builder.getSelectAliasList().clear();
        builder.addSelect(FisCheckEntrantRequest.errorCode().s());
        builder.setNeedDistinct(true);
        List<Integer> errorList = getList(builder);

        List<DataWrapper> result = new ArrayList<>();
        long id = 1L;

        for (Integer errorCode : errorList) {
            if (errorCode != null) {
                DataWrapper dw = new DataWrapper(id++, Integer.toString(errorCode));
                result.add(dw);
            }
        }

        return result;
    }

    @Override
    public void prepareListDataSource(Model model) {
        DynamicListDataSource<FisCheckEntrantRequest> dataSource = model.getDataSource();

        DQLSelectBuilder builder = createBuilder(model);

        DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(FisCheckEntrantRequest.class, "fer");
        orderRegistry.applyOrder(builder, model.getDataSource().getEntityOrder());

        UniBaseUtils.createPage(dataSource, builder, getSession());
    }

    protected DQLSelectBuilder createBuilder(Model model) {
        IDataSettings settings = model.getSettings();

        Date fromDate = settings.get("fromDate");
        Date toDate = settings.get("toDate");
        EnrollmentCampaign enrollmentCampaign = settings.get("enrollmentCampaign");
        List<FisAppStatusCode> fisAppStatusCodeFilter = settings.get("fisAppStatusCodeFilter");
        List<DataWrapper> errorCodeFilter = settings.get("errorCodeFilter");
        Number requestNumberFilter = settings.get("requestNumberFilter");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FisCheckEntrantRequest.class, "fer")
                .where(DQLExpressions.eq(DQLExpressions.property(FisCheckEntrantRequest.entrantRequest().entrant().enrollmentCampaign().fromAlias("fer")), DQLExpressions.value(enrollmentCampaign)));
        if (fromDate != null)
            builder.where(DQLExpressions.ge(
                    DQLExpressions.property(FisCheckEntrantRequest.checkDate().fromAlias("fer")),
                    DQLExpressions.valueDate(fromDate)
            ));
        if (toDate != null)
            builder.where(DQLExpressions.le(
                    DQLExpressions.property(FisCheckEntrantRequest.checkDate().fromAlias("fer")),
                    DQLExpressions.valueDate(toDate)
            ));

        FilterUtils.applySelectFilter(builder, "fer", FisCheckEntrantRequest.fisAppStatusCode().s(), fisAppStatusCodeFilter);

        if (errorCodeFilter != null && !errorCodeFilter.isEmpty()) {
            List<Integer> errorList = new ArrayList<>();

            for (DataWrapper dataWrapper : errorCodeFilter) {
                if (dataWrapper.getTitle() != null)
                    errorList.add(Integer.parseInt(dataWrapper.getTitle()));
            }
            FilterUtils.applySelectFilter(builder, "fer", FisCheckEntrantRequest.errorCode().s(), errorList);
        }

        if (requestNumberFilter != null)
            builder.where(DQLExpressions.eq(DQLExpressions.property(FisCheckEntrantRequest.entrantRequest().regNumber().fromAlias("fer")), DQLExpressions.value(Integer.valueOf(requestNumberFilter.intValue()))));

        return builder;
    }

    public RtfDocument createDocument(Model model) {
        IPrintFormCreator componentPrint = (IPrintFormCreator) ApplicationRuntime.getBean("fisCheckEntrantRequest");

        TemplateDocument template = getCatalogItem(TemplateDocument.class, "fisCheckEntrantRequest");

        return componentPrint.createPrintForm(template.getContent(), model);
    }
}
