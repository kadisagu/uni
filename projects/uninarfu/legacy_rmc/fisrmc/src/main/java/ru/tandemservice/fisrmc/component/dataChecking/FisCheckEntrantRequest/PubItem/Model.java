package ru.tandemservice.fisrmc.component.dataChecking.FisCheckEntrantRequest.PubItem;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import ru.tandemservice.fisrmc.entity.FisCheckEntrantRequest;

@State(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entityId"))
public class Model {

    private Long entityId;
    private FisCheckEntrantRequest checkEntrantRequest;

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public FisCheckEntrantRequest getCheckEntrantRequest() {
        return checkEntrantRequest;
    }

    public void setCheckEntrantRequest(FisCheckEntrantRequest checkEntrantRequest) {
        this.checkEntrantRequest = checkEntrantRequest;
    }

}
