package ru.tandemservice.fisrmc.component.settings.StateExamSubject2Ecdisciplinefis;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.fisrmc.entity.EducationSubject2Ecdisciplinefis;
import ru.tandemservice.fisrmc.entity.StateExamSubject2Ecdisciplinefis;
import ru.tandemservice.fisrmc.entity.catalog.Ecdisciplinefis;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;

import java.util.List;

public class Model {

    private String selectedPage;
    private DynamicListDataSource<EducationSubject2Ecdisciplinefis> dataSource;
    private IDataSettings settings;

    private List<StateExamSubject> stateExamSubjectsList;
    private List<Ecdisciplinefis> ecdisciplinefisList;
    private List<StateExamSubject2Ecdisciplinefis> disciplines;


    public String getSelectedPage() {
        return selectedPage;
    }

    public void setSelectedPage(String selectedPage) {
        this.selectedPage = selectedPage;
    }

    public DynamicListDataSource<EducationSubject2Ecdisciplinefis> getDataSource() {
        return dataSource;
    }

    public void setDataSource(
            DynamicListDataSource<EducationSubject2Ecdisciplinefis> dataSource)
    {
        this.dataSource = dataSource;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public List<Ecdisciplinefis> getEcdisciplinefisList() {
        return ecdisciplinefisList;
    }

    public void setEcdisciplinefisList(List<Ecdisciplinefis> ecdisciplinefisList) {
        this.ecdisciplinefisList = ecdisciplinefisList;
    }

    public List<StateExamSubject> getStateExamSubjectsList() {
        return stateExamSubjectsList;
    }

    public void setStateExamSubjectsList(List<StateExamSubject> stateExamSubjectsList) {
        this.stateExamSubjectsList = stateExamSubjectsList;
    }

    public List<StateExamSubject2Ecdisciplinefis> getDisciplines() {
        return disciplines;
    }

    public void setDisciplines(List<StateExamSubject2Ecdisciplinefis> disciplines) {
        this.disciplines = disciplines;
    }


}
