package ru.tandemservice.fisrmc.component.settings.DisciplineFisSettings;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.runtime.BusinessComponentRuntime;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.source.SimpleListDataSource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Controller extends AbstractBusinessController<IDAO, Model> {


    private static final String[] COMPONENTS = {
            "ru.tandemservice.fisrmc.component.settings.EducationSubject2Ecdisciplinefis",
            "ru.tandemservice.fisrmc.component.settings.StateExamSubject2Ecdisciplinefis",
            "ru.tandemservice.fisrmc.component.settings.EntranceDisciplineType2Entrancedisciplinetypefis",
            "ru.tandemservice.fisrmc.component.settings.Entrantstate2entrantstateFis",
            "ru.tandemservice.fisrmc.component.settings.EnrollmentCampaignStageFIS",
            "ru.tandemservice.fisrmc.component.settings.OlympiadDiploma2DiplomaFis",
            "ru.tandemservice.fisrmc.component.settings.GroupEsForFis.List",
            "ru.tandemservice.fisrmc.component.settings.Eceducationlevelfis2TU",
            "ru.tandemservice.fisrmc.component.settings.DisciplimeMinMark2Olimpiad"

    };


    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = (Model) getModel(component);

        ((IDAO) getDao()).prepare(model);

        prepareListDataSource(component);


    }

    private void prepareListDataSource(IBusinessComponent component) {

        Model model = (Model) getModel(component);
        if (model.getDataSource() != null) return;


        List list = new ArrayList();
        for (int i = 0; i < COMPONENTS.length; i++) {
            list.add(new IdentifiableWrapper(new Long(i), BusinessComponentRuntime.getInstance().getComponentMeta(COMPONENTS[i]).getTitle()));
        }
        SimpleListDataSource dataSource = new SimpleListDataSource(list);
        PublisherLinkColumn linkColumn = new PublisherLinkColumn("Название", "title");
        linkColumn.setResolver(new IPublisherLinkResolver() {
            public Object getParameters(IEntity entity)
            {
                return new HashMap();
            }

            public String getComponentName(IEntity entity)
            {
                return Controller.COMPONENTS[entity.getId().intValue()];
            }
        });
        dataSource.addColumn(linkColumn.setOrderable(false));
        dataSource.setCountRow(COMPONENTS.length);
        model.setDataSource(dataSource);

    }


}
