package ru.tandemservice.fisrmc.component.TestWeb.Delete;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BooleanColumn;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.fisrmc.dao.dataForDelete.DaemonParamsDeletePkg;
import ru.tandemservice.fisrmc.dao.dataForDelete.IDataForDeleteDAO;
import ru.tandemservice.fisrmc.entity.FisPackages;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.ArrayList;
import java.util.List;


public class Controller extends ru.tandemservice.fisrmc.component.TestWeb.Controller
{

    @Override
    public void onClickGetState(IBusinessComponent context) {
        ErrorCollector errors = context.getUserContext().getErrorCollector();
        IDataForDeleteDAO dao = (IDataForDeleteDAO) ApplicationRuntime.getBean(IDataForDeleteDAO.BEAN_NAME);

        if (!DaemonParamsDeletePkg.NEED_RUN)
            errors.add("Служба не запущена " + dao.getMsgLog());
        else
            errors.add(dao.getMsgLog());
    }

    @Override
    public void onClickPackToFile(IBusinessComponent component) {
        Model model = component.getModel();
        component.saveSettings();
        IDataForDeleteDAO dao = (IDataForDeleteDAO) ApplicationRuntime.getBean(IDataForDeleteDAO.BEAN_NAME);

        ErrorCollector errors = component.getUserContext().getErrorCollector();
        if (DaemonParamsDeletePkg.NEED_RUN) {
            errors.add("Служба уже исполняется " + dao.getMsgLog());
            return;
        }

        try {
            List<EnrollmentCampaign> campaignList = (List<EnrollmentCampaign>) model.getSettings().get("enrollmentCampaignList");

            List<Integer> regNumbers = new ArrayList<>();
            List<Integer> excludeRegNumbers = new ArrayList<>();
            _parseStringToArray(model.getRegNumber(), regNumbers);
            _parseStringToArray(model.getExcludeRegNumber(), excludeRegNumbers);

            DaemonParamsDeletePkg params = new DaemonParamsDeletePkg
                    (
                            model.getFisUserName(),
                            model.getFisPwd(),
                            regNumbers,
                            excludeRegNumbers,
                            model.getNotRfMode(),
                            model.getBenefitRequest(),
                            campaignList,
                            model.isDeleteApplication(),
                            model.isDeleteOrdersOfAdmission(),
                            model.isDeleteCompetitiveGroupItems(),
                            model.isDeleteEntranceTestResults(),
                            model.isDeleteApplicationCommonBenefits(),
                            model.getAppDateWithoutTime()
                    );

            dao.setMsgLog("Процесс стартован");
            DaemonParamsDeletePkg.NEED_RUN = true;
            dao.setParamns(params);
            dao.DAEMON.wakeUpDaemon();

        }
        catch (Exception ex) {
            throw new ApplicationException("Ошибка импорта в файл " + ex.getMessage(), ex);
        }
    }

    @Override
    protected void createColumns(DynamicListDataSource dataSource) {
        dataSource.addColumn(new SimpleColumn("Дата формирования", FisPackages.P_FORMING_DATE, DateFormatter.DATE_FORMATTER_WITH_TIME).setClickable(true));
        //dataSource.addColumn(new SimpleColumn("Сведения о загрузке в тестовом режиме", FisPackages.P_TEST_APP_MODE_DATA).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Примечание выгрузки", FisPackages.P_DESCRIPTION).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Приемная кампания", FisPackages.P_ENROLLMENT_CAMPAIGN).setClickable(false));
        //dataSource.addColumn(new BooleanColumn("Информация о приемной кампании", FisPackages.P_CAMPAIGN_INFO).setClickable(false));
        //dataSource.addColumn(new BooleanColumn("Сведения об объеме и структуре приема", FisPackages.P_ADMISSION_INFO).setClickable(false));
        //dataSource.addColumn(new BooleanColumn("Заявления", FisPackages.P_APPLICATIONS).setClickable(false));
        //dataSource.addColumn(new BooleanColumn("Оценки в заявлении", FisPackages.P_ENTRANCE_TEST_RESULT).setClickable(false));
        //dataSource.addColumn(new BooleanColumn("Принудительно отменить конкурсные группы", FisPackages.P_SUSPEND_COMPETITIVE_GROUP).setClickable(false));
        //dataSource.addColumn(new BooleanColumn("Подавлять ошибки неполных данных", FisPackages.P_SUSPEND_ERROR_DOC).setClickable(false));
        //dataSource.addColumn(new BooleanColumn("Скрывать персональные данные", FisPackages.P_ABFUSCATE_DATA).setClickable(false));
        //dataSource.addColumn(new BooleanColumn("Включенные в приказ", FisPackages.P_ORDERS_OF_ADMISSION).setClickable(false));

        dataSource.addColumn(new BooleanColumn("Удаление заявлений", FisPackages.P_DELETE_APPLICATION).setClickable(false));
        dataSource.addColumn(new BooleanColumn("Удаление включенных в приказ", FisPackages.P_DELETE_ORDERS_OF_ADMISSION).setClickable(false));
        dataSource.addColumn(new BooleanColumn("Удаление направлений подготовки конкурсных групп", FisPackages.P_DELETE_COMPETITIVE_GROUP_ITEMS).setClickable(false));
        dataSource.addColumn(new BooleanColumn("Удаление результатов вступительных испытаний", FisPackages.P_DELETE_ENTRANCE_TEST_RESULTS).setClickable(false));
        dataSource.addColumn(new BooleanColumn("Удаление общих льгот абитуриента", FisPackages.P_DELETE_APPLICATION_COMMON_BENEFITS).setClickable(false));

        dataSource.addColumn(new BooleanColumn("Есть ошибки", FisPackages.P_GLOBAL_ERROR).setClickable(false));
        dataSource.addColumn(new IndicatorColumn("Скачать пакет", null, "onClickDownloadPackage").defaultIndicator(new IndicatorColumn.Item("printer", "Печать")).setImageHeader(false).setDisableSecondSubmit(false).setOrderable(false));
        dataSource.addColumn(new IndicatorColumn("Скачать ошибки формирования пакета", null, "onClickDownloadErrors").defaultIndicator(new IndicatorColumn.Item("printer", "Печать")).setImageHeader(false).setDisableSecondSubmit(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Удалить пакет", ActionColumn.DELETE, "onDeleteEntity", "Удалить пакет?"));
    }
}
