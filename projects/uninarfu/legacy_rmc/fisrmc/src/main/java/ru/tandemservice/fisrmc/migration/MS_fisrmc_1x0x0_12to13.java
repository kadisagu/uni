package ru.tandemservice.fisrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import ru.tandemservice.fisrmc.entity.FisPackages;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_fisrmc_1x0x0_12to13 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.10"),
                        new ScriptDependency("org.tandemframework.shared", "1.1.3"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.1.3")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        IEntityMeta meta = EntityRuntime.getMeta(FisPackages.class);

        ////////////////////////////////////////////////////////////////////////////////
        if (tool.tableExists(meta.getTableName())) {
            // сущность fisPackages
            // создано свойство appDateWithoutTime
            // создать колонку
            tool.createColumn("fispackages_t", new DBColumn("appdatewithouttime_p", DBType.BOOLEAN));
        }


    }
}