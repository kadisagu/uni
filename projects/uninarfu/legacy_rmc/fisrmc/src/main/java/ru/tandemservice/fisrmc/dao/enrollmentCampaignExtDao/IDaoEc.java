package ru.tandemservice.fisrmc.dao.enrollmentCampaignExtDao;

import ru.tandemservice.fisrmc.entity.EnrollmentCampaignFisExt;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;

public interface IDaoEc {

    /**
     * Расширенные настройки приемки
     *
     * @param campaign
     *
     * @return
     */
    public EnrollmentCampaignFisExt getEnrollmentCampaignFisExt(EnrollmentCampaign campaign);

    public EnrollmentCampaignFisExt getEnrollmentCampaignFisExt(EnrollmentCampaign campaign, boolean createIfNull);

    /**
     * генерирует новый номер заявления абитуриента
     *
     * @param entrant
     *
     * @return
     */
    public int getEntrantRequestNewNumber(Entrant entrant, int number);

    public int getEntrantRequestNewNumber(Entrant entrant, int number, boolean systemAction);

    public int getEntrantRequestNewNumber(Entrant entrant, int number, boolean systemAction, boolean checkUnique);


    public int getEntrantRequestCount(Entrant entrant, boolean systemAction);


}
