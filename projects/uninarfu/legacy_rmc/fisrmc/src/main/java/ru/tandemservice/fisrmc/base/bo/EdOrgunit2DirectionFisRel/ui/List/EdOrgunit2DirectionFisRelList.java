package ru.tandemservice.fisrmc.base.bo.EdOrgunit2DirectionFisRel.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import ru.tandemservice.fisrmc.base.bo.EdOrgunit2DirectionFisRel.logic.dsHandlers.EdOrgunit2DirectionDSHandler;
import ru.tandemservice.fisrmc.base.bo.EdOrgunit2DirectionFisRel.logic.dsHandlers.EnrollmentCampaignDSHandler;
import ru.tandemservice.fisrmc.base.bo.EdOrgunit2DirectionFisRel.logic.dsHandlers.QualificationsDSHandler;
import ru.tandemservice.fisrmc.base.bo.EdOrgunit2DirectionFisRel.logic.dsHandlers.SpravTypeDSHandler;
import ru.tandemservice.fisrmc.entity.EdOrgunit2DirectionFis;
import ru.tandemservice.fisrmc.entity.catalog.EducationdirectionSpravType;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

@Configuration
public class EdOrgunit2DirectionFisRelList extends BusinessComponentManager {


    @Bean
    public ColumnListExtPoint edOrgunit2DirectionFisRelDS()
    {
        return columnListExtPointBuilder("edOrgunit2DirectionFisRelDS")

                .addColumn(textColumn("qualificationCode", "eou." + EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification().code()).order().create())
                .addColumn(textColumn("okso", "eou." + EducationOrgUnit.educationLevelHighSchool().educationLevel().okso()).order().create())
                .addColumn(textColumn("inheritedOkso", "eou." + EducationOrgUnit.educationLevelHighSchool().educationLevel().inheritedOkso()).order().create())

                        // для новых ОП
                .addColumn(textColumn("subjectcode", "eou." + EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSubject().subjectCode()).order().create())
                .addColumn(textColumn("specsubjectcode", "eou." + EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSpecialization().programSubject().subjectCode()).order().create())


                .addColumn(textColumn("orgUnitTitle", "eou." + EducationOrgUnit.educationLevelHighSchool().title()).order().create())
                .addColumn(textColumn("developPeriod", "eou." + EducationOrgUnit.developPeriod().title()).order().create())

                        // коменты к экспорту
                .addColumn(textColumn("exportComment", "rel." + EdOrgunit2DirectionFis.educationDirectionFis().exportComment()).order().create())

                .addColumn(booleanColumn("relationToFis", "hasRelationToFis"))

                .addColumn(textColumn("fisId", "rel." + EdOrgunit2DirectionFis.educationDirectionFis().code()).order().create())
                .addColumn(textColumn("fisCode", "rel." + EdOrgunit2DirectionFis.educationDirectionFis().Codefis()).order().create())
                .addColumn(textColumn("fisNewCode", "rel." + EdOrgunit2DirectionFis.educationDirectionFis().NewCode()).order().create())
                .addColumn(textColumn("fisName", "rel." + EdOrgunit2DirectionFis.educationDirectionFis().title()).order().create())
                .addColumn(textColumn("qualificationCodeFIS", "rel." + EdOrgunit2DirectionFis.educationDirectionFis().QualificationCode()).order().create())
                .addColumn(textColumn("developPeriodFIS", "rel." + EdOrgunit2DirectionFis.educationDirectionFis().Period()).order().create())

                .addColumn(textColumn("fisUGSCode", "rel." + EdOrgunit2DirectionFis.educationDirectionFis().UGSCode()).order().create())
                .addColumn(textColumn("fisUGSName", "rel." + EdOrgunit2DirectionFis.educationDirectionFis().UGSName()).order().create())


                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, "onEditRelation").disabled(false).create())

                .create();
    }

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS("edOrgunit2DirectionFisRelDS", edOrgunit2DirectionFisRelDS()).handler(edOrgunit2DirectionDSHandler()))
                .addDataSource(selectDS("enrollmentCampaignDS", enrollmentCampaignDSHandler()))
                .addDataSource(selectDS("qualificationsDS", qualificationsDSHandler()))
                .addDataSource(selectDS("spravTypeDS", spravTypeDSHandler()))
                .create();
    }


    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> edOrgunit2DirectionDSHandler()
    {
        return new EdOrgunit2DirectionDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> enrollmentCampaignDSHandler()
    {
        return new EnrollmentCampaignDSHandler(getName());
    }


    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> qualificationsDSHandler()
    {
        return new QualificationsDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> spravTypeDSHandler()
    {
        SpravTypeDSHandler retVal = new SpravTypeDSHandler(getName());
        retVal.setOrderByProperty(EducationdirectionSpravType.title().s());
        return retVal;
    }


}
