package ru.tandemservice.fisrmc.util;

import org.apache.cxf.common.util.StringUtils;
import ru.tandemservice.fisrmc.entity.EnrollmentCampaignFisExt;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

public class FisrmcUtil {

    public static EnrollmentCampaignFisExt getEnrollmentCampaignFisExt(EnrollmentCampaign enrollmentCampaign) {
        EnrollmentCampaignFisExt enrollmentCampaignFisExt = UniDaoFacade.getCoreDao().get(EnrollmentCampaignFisExt.class, EnrollmentCampaignFisExt.enrollmentCampaign(), enrollmentCampaign);
        if (enrollmentCampaignFisExt == null) {
            enrollmentCampaignFisExt = new EnrollmentCampaignFisExt();
            enrollmentCampaignFisExt.setEnrollmentCampaign(enrollmentCampaign);
        }
        return enrollmentCampaignFisExt;
    }

    public static boolean isValidRequestPrefix(String requestPrefix) {
        if (!StringUtils.isEmpty(requestPrefix)) {
            Integer prefix = Integer.parseInt(requestPrefix);
            if (prefix < 1 || prefix > 99)
                return false;
        }
        return true;
    }

}
