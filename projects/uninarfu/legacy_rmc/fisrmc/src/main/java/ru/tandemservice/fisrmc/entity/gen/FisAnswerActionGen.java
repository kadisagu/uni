package ru.tandemservice.fisrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.fisrmc.entity.FisAnswerAction;
import ru.tandemservice.fisrmc.entity.FisAnswerTofisPackages;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * ФИС действия над ответом
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FisAnswerActionGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.fisrmc.entity.FisAnswerAction";
    public static final String ENTITY_NAME = "fisAnswerAction";
    public static final int VERSION_HASH = -345080563;
    private static IEntityMeta ENTITY_META;

    public static final String P_DATE = "date";
    public static final String L_FIS_ANSWER_TOFIS_PACKAGES = "fisAnswerTofisPackages";
    public static final String P_COMMENT = "comment";
    public static final String L_ACTION_REPORT = "actionReport";
    public static final String L_ERROR_REPORT = "errorReport";

    private Date _date;     // Дата обработки
    private FisAnswerTofisPackages _fisAnswerTofisPackages;     // Ответы ФИС на пакеты из TU
    private String _comment; 
    private DatabaseFile _actionReport;     // Отчет обработки
    private DatabaseFile _errorReport;     // Ошибки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата обработки. Свойство не может быть null.
     */
    @NotNull
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Дата обработки. Свойство не может быть null.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Ответы ФИС на пакеты из TU. Свойство не может быть null.
     */
    @NotNull
    public FisAnswerTofisPackages getFisAnswerTofisPackages()
    {
        return _fisAnswerTofisPackages;
    }

    /**
     * @param fisAnswerTofisPackages Ответы ФИС на пакеты из TU. Свойство не может быть null.
     */
    public void setFisAnswerTofisPackages(FisAnswerTofisPackages fisAnswerTofisPackages)
    {
        dirty(_fisAnswerTofisPackages, fisAnswerTofisPackages);
        _fisAnswerTofisPackages = fisAnswerTofisPackages;
    }

    /**
     * @return 
     */
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment 
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * @return Отчет обработки.
     */
    public DatabaseFile getActionReport()
    {
        return _actionReport;
    }

    /**
     * @param actionReport Отчет обработки.
     */
    public void setActionReport(DatabaseFile actionReport)
    {
        dirty(_actionReport, actionReport);
        _actionReport = actionReport;
    }

    /**
     * @return Ошибки.
     */
    public DatabaseFile getErrorReport()
    {
        return _errorReport;
    }

    /**
     * @param errorReport Ошибки.
     */
    public void setErrorReport(DatabaseFile errorReport)
    {
        dirty(_errorReport, errorReport);
        _errorReport = errorReport;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FisAnswerActionGen)
        {
            setDate(((FisAnswerAction)another).getDate());
            setFisAnswerTofisPackages(((FisAnswerAction)another).getFisAnswerTofisPackages());
            setComment(((FisAnswerAction)another).getComment());
            setActionReport(((FisAnswerAction)another).getActionReport());
            setErrorReport(((FisAnswerAction)another).getErrorReport());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FisAnswerActionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FisAnswerAction.class;
        }

        public T newInstance()
        {
            return (T) new FisAnswerAction();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "date":
                    return obj.getDate();
                case "fisAnswerTofisPackages":
                    return obj.getFisAnswerTofisPackages();
                case "comment":
                    return obj.getComment();
                case "actionReport":
                    return obj.getActionReport();
                case "errorReport":
                    return obj.getErrorReport();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "fisAnswerTofisPackages":
                    obj.setFisAnswerTofisPackages((FisAnswerTofisPackages) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "actionReport":
                    obj.setActionReport((DatabaseFile) value);
                    return;
                case "errorReport":
                    obj.setErrorReport((DatabaseFile) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "date":
                        return true;
                case "fisAnswerTofisPackages":
                        return true;
                case "comment":
                        return true;
                case "actionReport":
                        return true;
                case "errorReport":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "date":
                    return true;
                case "fisAnswerTofisPackages":
                    return true;
                case "comment":
                    return true;
                case "actionReport":
                    return true;
                case "errorReport":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "date":
                    return Date.class;
                case "fisAnswerTofisPackages":
                    return FisAnswerTofisPackages.class;
                case "comment":
                    return String.class;
                case "actionReport":
                    return DatabaseFile.class;
                case "errorReport":
                    return DatabaseFile.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FisAnswerAction> _dslPath = new Path<FisAnswerAction>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FisAnswerAction");
    }
            

    /**
     * @return Дата обработки. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisAnswerAction#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Ответы ФИС на пакеты из TU. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisAnswerAction#getFisAnswerTofisPackages()
     */
    public static FisAnswerTofisPackages.Path<FisAnswerTofisPackages> fisAnswerTofisPackages()
    {
        return _dslPath.fisAnswerTofisPackages();
    }

    /**
     * @return 
     * @see ru.tandemservice.fisrmc.entity.FisAnswerAction#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @return Отчет обработки.
     * @see ru.tandemservice.fisrmc.entity.FisAnswerAction#getActionReport()
     */
    public static DatabaseFile.Path<DatabaseFile> actionReport()
    {
        return _dslPath.actionReport();
    }

    /**
     * @return Ошибки.
     * @see ru.tandemservice.fisrmc.entity.FisAnswerAction#getErrorReport()
     */
    public static DatabaseFile.Path<DatabaseFile> errorReport()
    {
        return _dslPath.errorReport();
    }

    public static class Path<E extends FisAnswerAction> extends EntityPath<E>
    {
        private PropertyPath<Date> _date;
        private FisAnswerTofisPackages.Path<FisAnswerTofisPackages> _fisAnswerTofisPackages;
        private PropertyPath<String> _comment;
        private DatabaseFile.Path<DatabaseFile> _actionReport;
        private DatabaseFile.Path<DatabaseFile> _errorReport;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата обработки. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisAnswerAction#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(FisAnswerActionGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Ответы ФИС на пакеты из TU. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisAnswerAction#getFisAnswerTofisPackages()
     */
        public FisAnswerTofisPackages.Path<FisAnswerTofisPackages> fisAnswerTofisPackages()
        {
            if(_fisAnswerTofisPackages == null )
                _fisAnswerTofisPackages = new FisAnswerTofisPackages.Path<FisAnswerTofisPackages>(L_FIS_ANSWER_TOFIS_PACKAGES, this);
            return _fisAnswerTofisPackages;
        }

    /**
     * @return 
     * @see ru.tandemservice.fisrmc.entity.FisAnswerAction#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(FisAnswerActionGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @return Отчет обработки.
     * @see ru.tandemservice.fisrmc.entity.FisAnswerAction#getActionReport()
     */
        public DatabaseFile.Path<DatabaseFile> actionReport()
        {
            if(_actionReport == null )
                _actionReport = new DatabaseFile.Path<DatabaseFile>(L_ACTION_REPORT, this);
            return _actionReport;
        }

    /**
     * @return Ошибки.
     * @see ru.tandemservice.fisrmc.entity.FisAnswerAction#getErrorReport()
     */
        public DatabaseFile.Path<DatabaseFile> errorReport()
        {
            if(_errorReport == null )
                _errorReport = new DatabaseFile.Path<DatabaseFile>(L_ERROR_REPORT, this);
            return _errorReport;
        }

        public Class getEntityClass()
        {
            return FisAnswerAction.class;
        }

        public String getEntityName()
        {
            return "fisAnswerAction";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
