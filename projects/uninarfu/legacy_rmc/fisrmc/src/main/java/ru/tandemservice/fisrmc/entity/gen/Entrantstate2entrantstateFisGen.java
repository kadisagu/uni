package ru.tandemservice.fisrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.fisrmc.entity.Entrantstate2entrantstateFis;
import ru.tandemservice.fisrmc.entity.catalog.Ecrequestfis;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность-связь статус абитуриента и статус абитуриента ФИС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class Entrantstate2entrantstateFisGen extends EntityBase
 implements INaturalIdentifiable<Entrantstate2entrantstateFisGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.fisrmc.entity.Entrantstate2entrantstateFis";
    public static final String ENTITY_NAME = "entrantstate2entrantstateFis";
    public static final int VERSION_HASH = -1982489694;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String L_ECREQUESTFIS = "ecrequestfis";
    public static final String L_ENTRANT_STATE = "entrantState";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private Ecrequestfis _ecrequestfis;     // Статус заявления
    private EntrantState _entrantState;     // Состояние абитуриента
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Статус заявления. Свойство не может быть null.
     */
    @NotNull
    public Ecrequestfis getEcrequestfis()
    {
        return _ecrequestfis;
    }

    /**
     * @param ecrequestfis Статус заявления. Свойство не может быть null.
     */
    public void setEcrequestfis(Ecrequestfis ecrequestfis)
    {
        dirty(_ecrequestfis, ecrequestfis);
        _ecrequestfis = ecrequestfis;
    }

    /**
     * @return Состояние абитуриента. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EntrantState getEntrantState()
    {
        return _entrantState;
    }

    /**
     * @param entrantState Состояние абитуриента. Свойство не может быть null и должно быть уникальным.
     */
    public void setEntrantState(EntrantState entrantState)
    {
        dirty(_entrantState, entrantState);
        _entrantState = entrantState;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof Entrantstate2entrantstateFisGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((Entrantstate2entrantstateFis)another).getCode());
            }
            setEcrequestfis(((Entrantstate2entrantstateFis)another).getEcrequestfis());
            setEntrantState(((Entrantstate2entrantstateFis)another).getEntrantState());
            setTitle(((Entrantstate2entrantstateFis)another).getTitle());
        }
    }

    public INaturalId<Entrantstate2entrantstateFisGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<Entrantstate2entrantstateFisGen>
    {
        private static final String PROXY_NAME = "Entrantstate2entrantstateFisNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof Entrantstate2entrantstateFisGen.NaturalId) ) return false;

            Entrantstate2entrantstateFisGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends Entrantstate2entrantstateFisGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Entrantstate2entrantstateFis.class;
        }

        public T newInstance()
        {
            return (T) new Entrantstate2entrantstateFis();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "ecrequestfis":
                    return obj.getEcrequestfis();
                case "entrantState":
                    return obj.getEntrantState();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "ecrequestfis":
                    obj.setEcrequestfis((Ecrequestfis) value);
                    return;
                case "entrantState":
                    obj.setEntrantState((EntrantState) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "ecrequestfis":
                        return true;
                case "entrantState":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "ecrequestfis":
                    return true;
                case "entrantState":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "ecrequestfis":
                    return Ecrequestfis.class;
                case "entrantState":
                    return EntrantState.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Entrantstate2entrantstateFis> _dslPath = new Path<Entrantstate2entrantstateFis>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Entrantstate2entrantstateFis");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.fisrmc.entity.Entrantstate2entrantstateFis#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Статус заявления. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.Entrantstate2entrantstateFis#getEcrequestfis()
     */
    public static Ecrequestfis.Path<Ecrequestfis> ecrequestfis()
    {
        return _dslPath.ecrequestfis();
    }

    /**
     * @return Состояние абитуриента. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.fisrmc.entity.Entrantstate2entrantstateFis#getEntrantState()
     */
    public static EntrantState.Path<EntrantState> entrantState()
    {
        return _dslPath.entrantState();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.fisrmc.entity.Entrantstate2entrantstateFis#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends Entrantstate2entrantstateFis> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private Ecrequestfis.Path<Ecrequestfis> _ecrequestfis;
        private EntrantState.Path<EntrantState> _entrantState;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.fisrmc.entity.Entrantstate2entrantstateFis#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(Entrantstate2entrantstateFisGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Статус заявления. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.Entrantstate2entrantstateFis#getEcrequestfis()
     */
        public Ecrequestfis.Path<Ecrequestfis> ecrequestfis()
        {
            if(_ecrequestfis == null )
                _ecrequestfis = new Ecrequestfis.Path<Ecrequestfis>(L_ECREQUESTFIS, this);
            return _ecrequestfis;
        }

    /**
     * @return Состояние абитуриента. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.fisrmc.entity.Entrantstate2entrantstateFis#getEntrantState()
     */
        public EntrantState.Path<EntrantState> entrantState()
        {
            if(_entrantState == null )
                _entrantState = new EntrantState.Path<EntrantState>(L_ENTRANT_STATE, this);
            return _entrantState;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.fisrmc.entity.Entrantstate2entrantstateFis#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(Entrantstate2entrantstateFisGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return Entrantstate2entrantstateFis.class;
        }

        public String getEntityName()
        {
            return "entrantstate2entrantstateFis";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
