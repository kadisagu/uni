package ru.tandemservice.fisrmc.dao;

import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;
import ru.tandemservice.fisrmc.entity.FisPackages;

import javax.xml.transform.TransformerException;
import java.io.IOException;

public interface IPackUnpackToFis {

//	/**
//	 * Список приемных компаний
//	 * @return
//	 */
//	public List<EnrollmentCampaign> getEnrollmentCampaignList();
//	
//	
//	/**
//	 * Тестовая упаковка
//	 * @return
//	 */
//	String testPackEnrollmentCampaign ();


    /**
     *
     */
    Document PackEnrollmentCampaign
    (
            DaemonParams daemonParams
    );


//	void PackEnrollmentCampaignToFile 
//	(
//			DaemonParams daemonParams
//	); 

    @Transactional(readOnly = false)
    FisPackages PackEnrollmentCampaignToDatabase
            (
                    DaemonParams daemonParams
            ) throws TransformerException, IOException;

    public FisPackages saveCurrentFisPackages(
            DaemonParams daemonParams
            , byte[] xml);
}
