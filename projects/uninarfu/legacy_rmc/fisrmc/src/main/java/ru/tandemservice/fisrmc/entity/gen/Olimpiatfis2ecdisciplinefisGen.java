package ru.tandemservice.fisrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.fisrmc.entity.Olimpiatfis2ecdisciplinefis;
import ru.tandemservice.fisrmc.entity.catalog.Ecdisciplinefis;
import ru.tandemservice.fisrmc.entity.catalog.Ecolimpiadlevelfis;
import ru.tandemservice.fisrmc.entity.catalog.Olimpiatfis;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность-связь олимпиад и дисциплин
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class Olimpiatfis2ecdisciplinefisGen extends EntityBase
 implements INaturalIdentifiable<Olimpiatfis2ecdisciplinefisGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.fisrmc.entity.Olimpiatfis2ecdisciplinefis";
    public static final String ENTITY_NAME = "olimpiatfis2ecdisciplinefis";
    public static final int VERSION_HASH = -752036429;
    private static IEntityMeta ENTITY_META;

    public static final String L_ECDISCIPLINEFIS = "ecdisciplinefis";
    public static final String L_OLIMPIATFIS = "olimpiatfis";
    public static final String L_ECOLIMPIADLEVELFIS = "ecolimpiadlevelfis";

    private Ecdisciplinefis _ecdisciplinefis;     // Общеобразовательные предметы
    private Olimpiatfis _olimpiatfis;     // Олимпиады
    private Ecolimpiadlevelfis _ecolimpiadlevelfis;     // Уровень олимпиады

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Общеобразовательные предметы. Свойство не может быть null.
     */
    @NotNull
    public Ecdisciplinefis getEcdisciplinefis()
    {
        return _ecdisciplinefis;
    }

    /**
     * @param ecdisciplinefis Общеобразовательные предметы. Свойство не может быть null.
     */
    public void setEcdisciplinefis(Ecdisciplinefis ecdisciplinefis)
    {
        dirty(_ecdisciplinefis, ecdisciplinefis);
        _ecdisciplinefis = ecdisciplinefis;
    }

    /**
     * @return Олимпиады. Свойство не может быть null.
     */
    @NotNull
    public Olimpiatfis getOlimpiatfis()
    {
        return _olimpiatfis;
    }

    /**
     * @param olimpiatfis Олимпиады. Свойство не может быть null.
     */
    public void setOlimpiatfis(Olimpiatfis olimpiatfis)
    {
        dirty(_olimpiatfis, olimpiatfis);
        _olimpiatfis = olimpiatfis;
    }

    /**
     * @return Уровень олимпиады. Свойство не может быть null.
     */
    @NotNull
    public Ecolimpiadlevelfis getEcolimpiadlevelfis()
    {
        return _ecolimpiadlevelfis;
    }

    /**
     * @param ecolimpiadlevelfis Уровень олимпиады. Свойство не может быть null.
     */
    public void setEcolimpiadlevelfis(Ecolimpiadlevelfis ecolimpiadlevelfis)
    {
        dirty(_ecolimpiadlevelfis, ecolimpiadlevelfis);
        _ecolimpiadlevelfis = ecolimpiadlevelfis;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof Olimpiatfis2ecdisciplinefisGen)
        {
            if (withNaturalIdProperties)
            {
                setEcdisciplinefis(((Olimpiatfis2ecdisciplinefis)another).getEcdisciplinefis());
                setOlimpiatfis(((Olimpiatfis2ecdisciplinefis)another).getOlimpiatfis());
                setEcolimpiadlevelfis(((Olimpiatfis2ecdisciplinefis)another).getEcolimpiadlevelfis());
            }
        }
    }

    public INaturalId<Olimpiatfis2ecdisciplinefisGen> getNaturalId()
    {
        return new NaturalId(getEcdisciplinefis(), getOlimpiatfis(), getEcolimpiadlevelfis());
    }

    public static class NaturalId extends NaturalIdBase<Olimpiatfis2ecdisciplinefisGen>
    {
        private static final String PROXY_NAME = "Olimpiatfis2ecdisciplinefisNaturalProxy";

        private Long _ecdisciplinefis;
        private Long _olimpiatfis;
        private Long _ecolimpiadlevelfis;

        public NaturalId()
        {}

        public NaturalId(Ecdisciplinefis ecdisciplinefis, Olimpiatfis olimpiatfis, Ecolimpiadlevelfis ecolimpiadlevelfis)
        {
            _ecdisciplinefis = ((IEntity) ecdisciplinefis).getId();
            _olimpiatfis = ((IEntity) olimpiatfis).getId();
            _ecolimpiadlevelfis = ((IEntity) ecolimpiadlevelfis).getId();
        }

        public Long getEcdisciplinefis()
        {
            return _ecdisciplinefis;
        }

        public void setEcdisciplinefis(Long ecdisciplinefis)
        {
            _ecdisciplinefis = ecdisciplinefis;
        }

        public Long getOlimpiatfis()
        {
            return _olimpiatfis;
        }

        public void setOlimpiatfis(Long olimpiatfis)
        {
            _olimpiatfis = olimpiatfis;
        }

        public Long getEcolimpiadlevelfis()
        {
            return _ecolimpiadlevelfis;
        }

        public void setEcolimpiadlevelfis(Long ecolimpiadlevelfis)
        {
            _ecolimpiadlevelfis = ecolimpiadlevelfis;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof Olimpiatfis2ecdisciplinefisGen.NaturalId) ) return false;

            Olimpiatfis2ecdisciplinefisGen.NaturalId that = (NaturalId) o;

            if( !equals(getEcdisciplinefis(), that.getEcdisciplinefis()) ) return false;
            if( !equals(getOlimpiatfis(), that.getOlimpiatfis()) ) return false;
            if( !equals(getEcolimpiadlevelfis(), that.getEcolimpiadlevelfis()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEcdisciplinefis());
            result = hashCode(result, getOlimpiatfis());
            result = hashCode(result, getEcolimpiadlevelfis());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEcdisciplinefis());
            sb.append("/");
            sb.append(getOlimpiatfis());
            sb.append("/");
            sb.append(getEcolimpiadlevelfis());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends Olimpiatfis2ecdisciplinefisGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Olimpiatfis2ecdisciplinefis.class;
        }

        public T newInstance()
        {
            return (T) new Olimpiatfis2ecdisciplinefis();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "ecdisciplinefis":
                    return obj.getEcdisciplinefis();
                case "olimpiatfis":
                    return obj.getOlimpiatfis();
                case "ecolimpiadlevelfis":
                    return obj.getEcolimpiadlevelfis();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "ecdisciplinefis":
                    obj.setEcdisciplinefis((Ecdisciplinefis) value);
                    return;
                case "olimpiatfis":
                    obj.setOlimpiatfis((Olimpiatfis) value);
                    return;
                case "ecolimpiadlevelfis":
                    obj.setEcolimpiadlevelfis((Ecolimpiadlevelfis) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "ecdisciplinefis":
                        return true;
                case "olimpiatfis":
                        return true;
                case "ecolimpiadlevelfis":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "ecdisciplinefis":
                    return true;
                case "olimpiatfis":
                    return true;
                case "ecolimpiadlevelfis":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "ecdisciplinefis":
                    return Ecdisciplinefis.class;
                case "olimpiatfis":
                    return Olimpiatfis.class;
                case "ecolimpiadlevelfis":
                    return Ecolimpiadlevelfis.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Olimpiatfis2ecdisciplinefis> _dslPath = new Path<Olimpiatfis2ecdisciplinefis>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Olimpiatfis2ecdisciplinefis");
    }
            

    /**
     * @return Общеобразовательные предметы. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.Olimpiatfis2ecdisciplinefis#getEcdisciplinefis()
     */
    public static Ecdisciplinefis.Path<Ecdisciplinefis> ecdisciplinefis()
    {
        return _dslPath.ecdisciplinefis();
    }

    /**
     * @return Олимпиады. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.Olimpiatfis2ecdisciplinefis#getOlimpiatfis()
     */
    public static Olimpiatfis.Path<Olimpiatfis> olimpiatfis()
    {
        return _dslPath.olimpiatfis();
    }

    /**
     * @return Уровень олимпиады. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.Olimpiatfis2ecdisciplinefis#getEcolimpiadlevelfis()
     */
    public static Ecolimpiadlevelfis.Path<Ecolimpiadlevelfis> ecolimpiadlevelfis()
    {
        return _dslPath.ecolimpiadlevelfis();
    }

    public static class Path<E extends Olimpiatfis2ecdisciplinefis> extends EntityPath<E>
    {
        private Ecdisciplinefis.Path<Ecdisciplinefis> _ecdisciplinefis;
        private Olimpiatfis.Path<Olimpiatfis> _olimpiatfis;
        private Ecolimpiadlevelfis.Path<Ecolimpiadlevelfis> _ecolimpiadlevelfis;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Общеобразовательные предметы. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.Olimpiatfis2ecdisciplinefis#getEcdisciplinefis()
     */
        public Ecdisciplinefis.Path<Ecdisciplinefis> ecdisciplinefis()
        {
            if(_ecdisciplinefis == null )
                _ecdisciplinefis = new Ecdisciplinefis.Path<Ecdisciplinefis>(L_ECDISCIPLINEFIS, this);
            return _ecdisciplinefis;
        }

    /**
     * @return Олимпиады. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.Olimpiatfis2ecdisciplinefis#getOlimpiatfis()
     */
        public Olimpiatfis.Path<Olimpiatfis> olimpiatfis()
        {
            if(_olimpiatfis == null )
                _olimpiatfis = new Olimpiatfis.Path<Olimpiatfis>(L_OLIMPIATFIS, this);
            return _olimpiatfis;
        }

    /**
     * @return Уровень олимпиады. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.Olimpiatfis2ecdisciplinefis#getEcolimpiadlevelfis()
     */
        public Ecolimpiadlevelfis.Path<Ecolimpiadlevelfis> ecolimpiadlevelfis()
        {
            if(_ecolimpiadlevelfis == null )
                _ecolimpiadlevelfis = new Ecolimpiadlevelfis.Path<Ecolimpiadlevelfis>(L_ECOLIMPIADLEVELFIS, this);
            return _ecolimpiadlevelfis;
        }

        public Class getEntityClass()
        {
            return Olimpiatfis2ecdisciplinefis.class;
        }

        public String getEntityName()
        {
            return "olimpiatfis2ecdisciplinefis";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
