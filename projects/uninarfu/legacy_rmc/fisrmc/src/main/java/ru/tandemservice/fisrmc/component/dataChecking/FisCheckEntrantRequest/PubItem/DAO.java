package ru.tandemservice.fisrmc.component.dataChecking.FisCheckEntrantRequest.PubItem;

import ru.tandemservice.fisrmc.entity.FisCheckEntrantRequest;
import ru.tandemservice.uni.dao.UniDao;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        model.setCheckEntrantRequest(get(FisCheckEntrantRequest.class, model.getEntityId()));
    }
}
