package ru.tandemservice.fisrmc.component.settings.EntranceDisciplineType2Entrancedisciplinetypefis;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.fisrmc.entity.EntranceDisciplineType2Entrancedisciplinetypefis;
import ru.tandemservice.fisrmc.entity.catalog.Entrancedisciplinetypefis;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineType;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DAO extends UniDao<Model> implements IDAO {


    @Override
    public void prepare(Model model)
    {
        model.setEntrancedisciplinetypefisList(getCatalogItemList(Entrancedisciplinetypefis.class));
        model.setEntranceDisciplineTypesList(getList(EntranceDisciplineType.class));
    }

    public void prepareListDataSource(Model model)
    {
        prepareListData(model);

        List disciplineTypes = model.getEntranceDisciplineTypesList();
        DynamicListDataSource dataSource = model.getDataSource();
        dataSource.setCountRow(disciplineTypes.size());
        UniBaseUtils.createPage(dataSource, disciplineTypes);
    }

    @SuppressWarnings("unchecked")
    private void prepareListData(Model model)
    {
        List<EntranceDisciplineType2Entrancedisciplinetypefis> disciplineTypes = getList(EntranceDisciplineType2Entrancedisciplinetypefis.class, EntranceDisciplineType2Entrancedisciplinetypefis.entranceDisciplineType().title().s());

        model.setDisciplineTypes(disciplineTypes);

        ((BlockColumn) model.getDataSource().getColumn("entrancedisciplinetypefis")).setValueMap(getDisciplineId2ExamSubjectMap(disciplineTypes));
    }

    @SuppressWarnings("unchecked")
    private Map<Long, Entrancedisciplinetypefis> getDisciplineId2ExamSubjectMap(List<EntranceDisciplineType2Entrancedisciplinetypefis> disciplineTypes)
    {


        List<Object[]> list = Collections.emptyList();
        if (!disciplineTypes.isEmpty()) {
            Criteria criteria = getSession().createCriteria(EntranceDisciplineType2Entrancedisciplinetypefis.class);

            ProjectionList projections = Projections.projectionList();
            projections.add(Projections.property(EntranceDisciplineType2Entrancedisciplinetypefis.entranceDisciplineType().id().s()));
            projections.add(Projections.property(EntranceDisciplineType2Entrancedisciplinetypefis.entrancedisciplinetypefis().s()));
            criteria.setProjection(projections);
            list = criteria.list();
        }
        Map result = new HashMap();
        for (Object[] row : list) {
            result.put((Long) row[0], (Entrancedisciplinetypefis) row[1]);
        }
        return result;
    }


    public void update(Model model)
    {
        DynamicListDataSource dataSource = model.getDataSource();
        Map disciplineId2StateExamSubject = ((BlockColumn) dataSource.getColumn("entrancedisciplinetypefis")).getValueMap();
        List<EntranceDisciplineType> stateExamSubjects;
        try {
            stateExamSubjects = dataSource.getEntityList();
        }
        catch (Throwable e) {
            throw new RuntimeException(e);
        }

        Session session = getSession();

        for (EntranceDisciplineType entranceDisciplineType : stateExamSubjects) {

            Entrancedisciplinetypefis disEcdisciplinefis = (Entrancedisciplinetypefis) disciplineId2StateExamSubject.get(entranceDisciplineType.getId());

            EntranceDisciplineType2Entrancedisciplinetypefis type2FisType = get(EntranceDisciplineType2Entrancedisciplinetypefis.class, EntranceDisciplineType2Entrancedisciplinetypefis.entranceDisciplineType(), entranceDisciplineType);

            if (disEcdisciplinefis != null) {
                if (type2FisType != null) {
                    type2FisType.setEntrancedisciplinetypefis(disEcdisciplinefis);
                }
                else {
                    type2FisType = new EntranceDisciplineType2Entrancedisciplinetypefis();
                    type2FisType.setEntranceDisciplineType(entranceDisciplineType);
                    type2FisType.setEntrancedisciplinetypefis(disEcdisciplinefis);
                }
                session.saveOrUpdate(type2FisType);
            }
            else if (type2FisType != null) {
                session.delete(type2FisType);
            }
        }
    }


}
