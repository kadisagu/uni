package ru.tandemservice.fisrmc.component.catalog.ecdisciplinefis.EcdisciplinefisAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.fisrmc.entity.catalog.Ecdisciplinefis;

public interface IDAO extends IDefaultCatalogAddEditDAO<Ecdisciplinefis, Model> {

}
