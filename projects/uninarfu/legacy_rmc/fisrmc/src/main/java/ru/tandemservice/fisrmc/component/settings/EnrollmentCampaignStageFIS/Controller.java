package ru.tandemservice.fisrmc.component.settings.EnrollmentCampaignStageFIS;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.fisrmc.entity.EnrollmentCampaignStageFIS;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        model.setSettings(component.getSettings());
        ((IDAO) getDao()).prepare(model);
        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component) {
        final Model model = (Model) getModel(component);

        if (model.getDataSource() != null) return;

        DynamicListDataSource<EnrollmentCampaignStageFIS> dataSource = new DynamicListDataSource<>(component, component1 -> {
            ((IDAO) Controller.this.getDao()).prepareListDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("Приемная кампания", EnrollmentCampaignStageFIS.enrollmentCampaign().title()));
        dataSource.addColumn(new SimpleColumn("Этап", EnrollmentCampaignStageFIS.stage().title()));
        dataSource.addColumn(new SimpleColumn("Дата приказа", EnrollmentCampaignStageFIS.dateOrder(), DateFormatter.DEFAULT_DATE_FORMATTER));
        dataSource.addColumn(new ActionColumn("Редактировать", "edit", "onClickEditItem").setPermissionKey("modifyEnrollmentCampaignStageFIS"));
        dataSource.addColumn(new ActionColumn("Удалить", "delete", "onClickDeleteItem", "Удалить этап?").setPermissionKey("modifyEnrollmentCampaignStageFIS"));

        model.setDataSource(dataSource);
    }

    public void onClickSearch(IBusinessComponent component)
    {
        component.saveSettings();
        ((Model) getModel(component)).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component)
    {
        Model model = (Model) getModel(component);
        model.getSettings().clear();
        ((IDAO) getDao()).prepare(model);
        onClickSearch(component);
    }

    public void onClickDeleteItem(IBusinessComponent component) {
        ((IDAO) getDao()).deleteRow(component);
    }

    public void onClickAdd(IBusinessComponent component) {
        Model model = (Model) getModel(component);

        component.createRegion(new ComponentActivator("ru.tandemservice.fisrmc.component.settings.EnrollmentCampaignStageFIS.AddEdit", (new UniMap())
                .add("enrollmentCampaignId", model.getEnrollmentCampaign().getId())));
    }

    public void onClickEditItem(IBusinessComponent component) {
        component.createRegion(new ComponentActivator("ru.tandemservice.fisrmc.component.settings.EnrollmentCampaignStageFIS.AddEdit", (new UniMap())
                .add("entityId", component.getListenerParameter())));
    }
}
