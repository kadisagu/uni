package ru.tandemservice.fisrmc.component.catalog.olimpiatfis.OlimpiatfisPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.fisrmc.entity.catalog.Olimpiatfis;

public class Controller extends DefaultCatalogPubController<Olimpiatfis, Model, IDAO> {


    @Override
    protected void addColumnsBeforeEditColumn(IBusinessComponent context, DynamicListDataSource<Olimpiatfis> dataSource) {

        dataSource.addColumn(new SimpleColumn("Код Тандем", Olimpiatfis.P_CODE_T_U).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Номер", Olimpiatfis.P_OLYMPIC_NUMBER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Уровень", Olimpiatfis.ecolimpiadlevelfis().title()).setClickable(false));
    }
}
