package ru.tandemservice.fisrmc.component.catalog.competitionkindfis.CompetitionkindfisAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.fisrmc.entity.catalog.Competitionkindfis;

public interface IDAO extends IDefaultCatalogAddEditDAO<Competitionkindfis, Model> {

}
