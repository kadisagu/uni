package ru.tandemservice.fisrmc.component.dataChecking.FisAnswerAction;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import ru.tandemservice.fisrmc.entity.FisAnswerAction;

public class Model {

    private IDataSettings settings;
    private DynamicListDataSource<FisAnswerAction> dataSource;
    private IMultiSelectModel fisAnswerTypeModel;

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public DynamicListDataSource<FisAnswerAction> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<FisAnswerAction> dataSource) {
        this.dataSource = dataSource;
    }

    public IMultiSelectModel getFisAnswerTypeModel() {
        return fisAnswerTypeModel;
    }

    public void setFisAnswerTypeModel(IMultiSelectModel fisAnswerTypeModel) {
        this.fisAnswerTypeModel = fisAnswerTypeModel;
    }

}
