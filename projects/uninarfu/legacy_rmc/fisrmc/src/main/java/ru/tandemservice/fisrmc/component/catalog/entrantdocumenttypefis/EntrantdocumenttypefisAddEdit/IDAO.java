package ru.tandemservice.fisrmc.component.catalog.entrantdocumenttypefis.EntrantdocumenttypefisAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.fisrmc.entity.catalog.Entrantdocumenttypefis;

public interface IDAO extends IDefaultCatalogAddEditDAO<Entrantdocumenttypefis, Model> {

}
