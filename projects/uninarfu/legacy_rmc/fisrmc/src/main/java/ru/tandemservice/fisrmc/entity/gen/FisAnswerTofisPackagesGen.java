package ru.tandemservice.fisrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.fisrmc.entity.FisAnswerTofisPackages;
import ru.tandemservice.fisrmc.entity.FisPackages;
import ru.tandemservice.fisrmc.entity.catalog.FisAnswerType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Ответы ФИС на пакеты из TU
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FisAnswerTofisPackagesGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.fisrmc.entity.FisAnswerTofisPackages";
    public static final String ENTITY_NAME = "fisAnswerTofisPackages";
    public static final int VERSION_HASH = 347506034;
    private static IEntityMeta ENTITY_META;

    public static final String P_ANSWER_DATE = "answerDate";
    public static final String L_FIS_PACKAGES = "fisPackages";
    public static final String L_FIS_ANSWER_TYPE = "fisAnswerType";
    public static final String P_FIS_PRIM = "fisPrim";
    public static final String L_XML_ANSWER = "xmlAnswer";

    private Date _answerDate;     // Дата ответа
    private FisPackages _fisPackages;     // Сущность для хранения сформированных пакетов
    private FisAnswerType _fisAnswerType;     // Тип ответа ФИС
    private String _fisPrim;     // Примечание из ФИС
    private DatabaseFile _xmlAnswer;     // Ответ ФИС

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата ответа. Свойство не может быть null.
     */
    @NotNull
    public Date getAnswerDate()
    {
        return _answerDate;
    }

    /**
     * @param answerDate Дата ответа. Свойство не может быть null.
     */
    public void setAnswerDate(Date answerDate)
    {
        dirty(_answerDate, answerDate);
        _answerDate = answerDate;
    }

    /**
     * @return Сущность для хранения сформированных пакетов. Свойство не может быть null.
     */
    @NotNull
    public FisPackages getFisPackages()
    {
        return _fisPackages;
    }

    /**
     * @param fisPackages Сущность для хранения сформированных пакетов. Свойство не может быть null.
     */
    public void setFisPackages(FisPackages fisPackages)
    {
        dirty(_fisPackages, fisPackages);
        _fisPackages = fisPackages;
    }

    /**
     * @return Тип ответа ФИС. Свойство не может быть null.
     */
    @NotNull
    public FisAnswerType getFisAnswerType()
    {
        return _fisAnswerType;
    }

    /**
     * @param fisAnswerType Тип ответа ФИС. Свойство не может быть null.
     */
    public void setFisAnswerType(FisAnswerType fisAnswerType)
    {
        dirty(_fisAnswerType, fisAnswerType);
        _fisAnswerType = fisAnswerType;
    }

    /**
     * @return Примечание из ФИС.
     */
    public String getFisPrim()
    {
        return _fisPrim;
    }

    /**
     * @param fisPrim Примечание из ФИС.
     */
    public void setFisPrim(String fisPrim)
    {
        dirty(_fisPrim, fisPrim);
        _fisPrim = fisPrim;
    }

    /**
     * @return Ответ ФИС.
     */
    public DatabaseFile getXmlAnswer()
    {
        return _xmlAnswer;
    }

    /**
     * @param xmlAnswer Ответ ФИС.
     */
    public void setXmlAnswer(DatabaseFile xmlAnswer)
    {
        dirty(_xmlAnswer, xmlAnswer);
        _xmlAnswer = xmlAnswer;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FisAnswerTofisPackagesGen)
        {
            setAnswerDate(((FisAnswerTofisPackages)another).getAnswerDate());
            setFisPackages(((FisAnswerTofisPackages)another).getFisPackages());
            setFisAnswerType(((FisAnswerTofisPackages)another).getFisAnswerType());
            setFisPrim(((FisAnswerTofisPackages)another).getFisPrim());
            setXmlAnswer(((FisAnswerTofisPackages)another).getXmlAnswer());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FisAnswerTofisPackagesGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FisAnswerTofisPackages.class;
        }

        public T newInstance()
        {
            return (T) new FisAnswerTofisPackages();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "answerDate":
                    return obj.getAnswerDate();
                case "fisPackages":
                    return obj.getFisPackages();
                case "fisAnswerType":
                    return obj.getFisAnswerType();
                case "fisPrim":
                    return obj.getFisPrim();
                case "xmlAnswer":
                    return obj.getXmlAnswer();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "answerDate":
                    obj.setAnswerDate((Date) value);
                    return;
                case "fisPackages":
                    obj.setFisPackages((FisPackages) value);
                    return;
                case "fisAnswerType":
                    obj.setFisAnswerType((FisAnswerType) value);
                    return;
                case "fisPrim":
                    obj.setFisPrim((String) value);
                    return;
                case "xmlAnswer":
                    obj.setXmlAnswer((DatabaseFile) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "answerDate":
                        return true;
                case "fisPackages":
                        return true;
                case "fisAnswerType":
                        return true;
                case "fisPrim":
                        return true;
                case "xmlAnswer":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "answerDate":
                    return true;
                case "fisPackages":
                    return true;
                case "fisAnswerType":
                    return true;
                case "fisPrim":
                    return true;
                case "xmlAnswer":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "answerDate":
                    return Date.class;
                case "fisPackages":
                    return FisPackages.class;
                case "fisAnswerType":
                    return FisAnswerType.class;
                case "fisPrim":
                    return String.class;
                case "xmlAnswer":
                    return DatabaseFile.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FisAnswerTofisPackages> _dslPath = new Path<FisAnswerTofisPackages>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FisAnswerTofisPackages");
    }
            

    /**
     * @return Дата ответа. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisAnswerTofisPackages#getAnswerDate()
     */
    public static PropertyPath<Date> answerDate()
    {
        return _dslPath.answerDate();
    }

    /**
     * @return Сущность для хранения сформированных пакетов. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisAnswerTofisPackages#getFisPackages()
     */
    public static FisPackages.Path<FisPackages> fisPackages()
    {
        return _dslPath.fisPackages();
    }

    /**
     * @return Тип ответа ФИС. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisAnswerTofisPackages#getFisAnswerType()
     */
    public static FisAnswerType.Path<FisAnswerType> fisAnswerType()
    {
        return _dslPath.fisAnswerType();
    }

    /**
     * @return Примечание из ФИС.
     * @see ru.tandemservice.fisrmc.entity.FisAnswerTofisPackages#getFisPrim()
     */
    public static PropertyPath<String> fisPrim()
    {
        return _dslPath.fisPrim();
    }

    /**
     * @return Ответ ФИС.
     * @see ru.tandemservice.fisrmc.entity.FisAnswerTofisPackages#getXmlAnswer()
     */
    public static DatabaseFile.Path<DatabaseFile> xmlAnswer()
    {
        return _dslPath.xmlAnswer();
    }

    public static class Path<E extends FisAnswerTofisPackages> extends EntityPath<E>
    {
        private PropertyPath<Date> _answerDate;
        private FisPackages.Path<FisPackages> _fisPackages;
        private FisAnswerType.Path<FisAnswerType> _fisAnswerType;
        private PropertyPath<String> _fisPrim;
        private DatabaseFile.Path<DatabaseFile> _xmlAnswer;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата ответа. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisAnswerTofisPackages#getAnswerDate()
     */
        public PropertyPath<Date> answerDate()
        {
            if(_answerDate == null )
                _answerDate = new PropertyPath<Date>(FisAnswerTofisPackagesGen.P_ANSWER_DATE, this);
            return _answerDate;
        }

    /**
     * @return Сущность для хранения сформированных пакетов. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisAnswerTofisPackages#getFisPackages()
     */
        public FisPackages.Path<FisPackages> fisPackages()
        {
            if(_fisPackages == null )
                _fisPackages = new FisPackages.Path<FisPackages>(L_FIS_PACKAGES, this);
            return _fisPackages;
        }

    /**
     * @return Тип ответа ФИС. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisAnswerTofisPackages#getFisAnswerType()
     */
        public FisAnswerType.Path<FisAnswerType> fisAnswerType()
        {
            if(_fisAnswerType == null )
                _fisAnswerType = new FisAnswerType.Path<FisAnswerType>(L_FIS_ANSWER_TYPE, this);
            return _fisAnswerType;
        }

    /**
     * @return Примечание из ФИС.
     * @see ru.tandemservice.fisrmc.entity.FisAnswerTofisPackages#getFisPrim()
     */
        public PropertyPath<String> fisPrim()
        {
            if(_fisPrim == null )
                _fisPrim = new PropertyPath<String>(FisAnswerTofisPackagesGen.P_FIS_PRIM, this);
            return _fisPrim;
        }

    /**
     * @return Ответ ФИС.
     * @see ru.tandemservice.fisrmc.entity.FisAnswerTofisPackages#getXmlAnswer()
     */
        public DatabaseFile.Path<DatabaseFile> xmlAnswer()
        {
            if(_xmlAnswer == null )
                _xmlAnswer = new DatabaseFile.Path<DatabaseFile>(L_XML_ANSWER, this);
            return _xmlAnswer;
        }

        public Class getEntityClass()
        {
            return FisAnswerTofisPackages.class;
        }

        public String getEntityName()
        {
            return "fisAnswerTofisPackages";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
