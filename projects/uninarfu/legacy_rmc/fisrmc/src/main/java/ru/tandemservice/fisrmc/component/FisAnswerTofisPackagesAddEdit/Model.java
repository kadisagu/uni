package ru.tandemservice.fisrmc.component.FisAnswerTofisPackagesAddEdit;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.fisrmc.entity.FisAnswerAction;
import ru.tandemservice.fisrmc.entity.FisAnswerTofisPackages;
import ru.tandemservice.fisrmc.entity.catalog.FisAnswerType;

@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "fisAnswerTofisPackagesId")
        , @Bind(key = "fisPackagesId", binding = "fisPackagesId")
        , @Bind(key = "inarea", binding = "inarea")
})
public class Model {

    private ISelectModel fisAnswerTypeModel;

    private String fisPrim;
    private IUploadFile file;
    private FisAnswerType fisAnswerType;
    private Long fisAnswerTofisPackagesId;
    private Long fisPackagesId;
    private boolean inarea = false;

    private FisAnswerTofisPackages fisAnswerTofisPackages;

    private DynamicListDataSource<FisAnswerAction> dataSource;

    private IDataSettings settings;

    public void setFisAnswerTofisPackagesId(Long fisAnswerTofisPackagesId) {
        this.fisAnswerTofisPackagesId = fisAnswerTofisPackagesId;
    }

    public Long getFisAnswerTofisPackagesId() {
        return fisAnswerTofisPackagesId;
    }

    public void setFisPackagesId(Long fisPackagesId) {
        this.fisPackagesId = fisPackagesId;
    }

    public Long getFisPackagesId() {
        return fisPackagesId;
    }

    public void setFisAnswerTypeModel(ISelectModel fisAnswerTypeModel) {
        this.fisAnswerTypeModel = fisAnswerTypeModel;
    }

    public ISelectModel getFisAnswerTypeModel() {
        return fisAnswerTypeModel;
    }

    public boolean isAddForm()
    {
        if (fisAnswerTofisPackagesId != null)
            return false;
        else
            return true;
    }

    public void setFisAnswerType(FisAnswerType fisAnswerType)
    {
        this.fisAnswerType = fisAnswerType;
    }

    public FisAnswerType getFisAnswerType()
    {
        return fisAnswerType;
    }

    public void setFisPrim(String fisPrim)
    {
        this.fisPrim = fisPrim;
    }

    public String getFisPrim()
    {
        return fisPrim;
    }

    public void setFile(IUploadFile file) {
        this.file = file;
    }

    public IUploadFile getFile() {
        return file;
    }

    public void setDataSource(DynamicListDataSource<FisAnswerAction> dataSource) {
        this.dataSource = dataSource;
    }

    public DynamicListDataSource<FisAnswerAction> getDataSource() {
        return dataSource;
    }

    public void setFisAnswerTofisPackages(FisAnswerTofisPackages fisAnswerTofisPackages) {
        this.fisAnswerTofisPackages = fisAnswerTofisPackages;
    }

    public FisAnswerTofisPackages getFisAnswerTofisPackages() {
        return fisAnswerTofisPackages;
    }

    public void setInarea(boolean inarea) {
        this.inarea = inarea;
    }

    public boolean isInarea() {
        return inarea;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setTestMode(boolean testMode) {
        this.getSettings().set("testMode", testMode);
    }


    public boolean getTestMode() {
        if (this.getSettings().get("testMode") == null) {
            this.getSettings().set("testMode", Boolean.FALSE);
        }
        return (Boolean) this.getSettings().get("testMode");
    }


}
