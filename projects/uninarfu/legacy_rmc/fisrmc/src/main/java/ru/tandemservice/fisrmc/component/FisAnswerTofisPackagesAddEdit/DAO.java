package ru.tandemservice.fisrmc.component.FisAnswerTofisPackagesAddEdit;

import org.apache.commons.io.IOUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.fisrmc.dao.answer.ICheckEgeSertificateDao;
import ru.tandemservice.fisrmc.dao.answer.IImportResultDao;
import ru.tandemservice.fisrmc.entity.FisAnswerAction;
import ru.tandemservice.fisrmc.entity.FisAnswerTofisPackages;
import ru.tandemservice.fisrmc.entity.FisPackages;
import ru.tandemservice.fisrmc.entity.catalog.FisAnswerType;
import ru.tandemservice.fisrmc.entity.catalog.codes.FisAnswerTypeCodes;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

public class DAO extends UniDao<Model> implements IDAO {


    @Override
    public void doAction(Model model) {

		/*
		try 
		{
			doActions(entity);
		} catch(Exception ex) {
			throw new ApplicationException("Ошибка обработки ответа", ex);
			//TODO: что делать???
		}
		*/
        FisAnswerTofisPackages fisAnswerTofisPackages = model.getFisAnswerTofisPackages();
        if (FisAnswerTypeCodes.IMPORT_RESULT.equals(fisAnswerTofisPackages.getFisAnswerType().getCode())) {
            IImportResultDao dao = (IImportResultDao) ApplicationRuntime.getBean(IImportResultDao.BEAN_NAME);

            try {
                dao.doAction(fisAnswerTofisPackages, model.getTestMode());
            }
            catch (Exception ex) {
                throw new ApplicationException("Ошибка обработки ответа", ex);
            }

        }
        else if (FisAnswerTypeCodes.CHECK_APPLICATION.equals(fisAnswerTofisPackages.getFisAnswerType().getCode())) {
            ICheckEgeSertificateDao dao = (ICheckEgeSertificateDao) ApplicationRuntime.getBean(ICheckEgeSertificateDao.BEAN_NAME);

            try {
                dao.doAction(fisAnswerTofisPackages, model.getTestMode());
            }
            catch (Exception ex) {
                ex.printStackTrace();
                throw new ApplicationException("Ошибка обработки ответа " + ex.getMessage(), ex);
            }

        }
        else
            throw new ApplicationException("Тип ответа " + fisAnswerTofisPackages.getFisAnswerType().getTitle() + " не предусматривает обработку");

    }

    @Override
    public void createNewFisAnswerAction(Model model)
    {

        FisAnswerAction fisAnswerAction = new FisAnswerAction();

        fisAnswerAction.setDate(new Date());
        fisAnswerAction.setComment("test");
        fisAnswerAction.setFisAnswerTofisPackages(model.getFisAnswerTofisPackages());

        save(fisAnswerAction);
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(FisAnswerAction.class, "pkg");
        dql.column("pkg");

        dql.where(DQLExpressions.eq(
                DQLExpressions.property(FisAnswerAction.fisAnswerTofisPackages().fromAlias("pkg")),
                DQLExpressions.value(model.getFisAnswerTofisPackages())
        ));

        // применим сортировку
        DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(FisAnswerAction.class, "pkg");

        orderRegistry.applyOrder(dql, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), dql, getSession());
    }

    @Override
    public void prepare(Model model)
    {
        model.setFisAnswerTypeModel(new LazySimpleSelectModel<>(FisAnswerType.class));

        if (model.getFisAnswerTofisPackagesId() != null) {
            FisAnswerTofisPackages entity = get(FisAnswerTofisPackages.class, model.getFisAnswerTofisPackagesId());
            model.setFisAnswerTofisPackages(entity);
        }
    }

    @Override
    public void update(Model model)
    {

        if (model.isAddForm()) {
            // в режиме добавления
            FisAnswerTofisPackages entity = new FisAnswerTofisPackages();

            entity.setAnswerDate(new Date());
            entity.setFisPrim(model.getFisPrim());
            entity.setFisAnswerType(model.getFisAnswerType());

            FisPackages fisPackages = get(FisPackages.class, model.getFisPackagesId());
            entity.setFisPackages(fisPackages);


            // файл с данными
            DatabaseFile file = new DatabaseFile();
            InputStream streem;
            if (model.getFile() != null) {
                streem = model.getFile().getStream();

                byte[] bts;

                try {
                    bts = IOUtils.toByteArray(streem);
                }
                catch (IOException e) {
                    throw new ApplicationException(e.getMessage());
                }

                file.setContent(bts);
                save(file);
                entity.setXmlAnswer(file);
            }

            saveOrUpdate(entity);
            model.setFisAnswerTofisPackagesId(entity.getId());
            model.setFisAnswerTofisPackages(entity);

        }
    }
	
	/*
	private void doActions(FisAnswerTofisPackages answer) throws Exception 
	{
		
	}
	*/

}
