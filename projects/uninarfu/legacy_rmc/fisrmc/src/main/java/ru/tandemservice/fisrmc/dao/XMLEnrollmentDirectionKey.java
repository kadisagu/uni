package ru.tandemservice.fisrmc.dao;

import ru.tandemservice.fisrmc.entity.Educationdirectionfis;
import ru.tandemservice.fisrmc.entity.catalog.Eceducationlevelfis;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

public class XMLEnrollmentDirectionKey {

    private EnrollmentCampaign enrollmentCampaign = null;
    private Course course = null;
    private Eceducationlevelfis eceducationlevelfis = null;
    private Educationdirectionfis educationdirectionfis = null;

    public XMLEnrollmentDirectionKey
            (
                    EnrollmentCampaign enrollmentCampaign,
                    Course course,
                    Eceducationlevelfis eceducationlevelfis,
                    Educationdirectionfis educationdirectionfis
            )
    {
        this.setEnrollmentCampaign(enrollmentCampaign);
        this.setCourse(course);
        this.setEceducationlevelfis(eceducationlevelfis);
        this.setEducationdirectionfis(educationdirectionfis);
    }

    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign) {
        this.enrollmentCampaign = enrollmentCampaign;
    }

    public EnrollmentCampaign getEnrollmentCampaign() {
        return enrollmentCampaign;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Course getCourse() {
        return course;
    }

    public void setEceducationlevelfis(Eceducationlevelfis eceducationlevelfis) {
        this.eceducationlevelfis = eceducationlevelfis;
    }

    public Eceducationlevelfis getEceducationlevelfis() {
        return eceducationlevelfis;
    }

    public void setEducationdirectionfis(Educationdirectionfis educationdirectionfis) {
        this.educationdirectionfis = educationdirectionfis;
    }

    public Educationdirectionfis getEducationdirectionfis() {
        return educationdirectionfis;
    }


    @Override
    public int hashCode()
    {

        String hash = Long.toString(enrollmentCampaign.getId());
        hash += Long.toString(course.getId());
        hash += Long.toString(eceducationlevelfis.getId());
        hash += Long.toString(educationdirectionfis.getId());
        return hash.hashCode();

    }

    @Override
    public boolean equals(Object obj)
    {

        if (obj instanceof XMLEnrollmentDirectionKey) {
            if (this.hashCode() == obj.hashCode())
                return true;
            else
                return false;
        }
        else
            return super.equals(obj);
    }
}
