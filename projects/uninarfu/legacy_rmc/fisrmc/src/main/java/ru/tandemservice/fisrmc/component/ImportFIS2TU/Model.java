package ru.tandemservice.fisrmc.component.ImportFIS2TU;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.fisrmc.entity.catalog.EducationdirectionSpravType;

public class Model {

    private String xmldoc = "";

    private IUploadFile file;

    private boolean reloadSprav;

    private ISelectModel spravTypeModel;

    private IDataSettings settings;

    public IUploadFile getFile() {
        return file;
    }

    public void setFile(IUploadFile file) {
        this.file = file;
    }

    public void setXmldoc(String xmldoc) {
        this.xmldoc = xmldoc;
    }

    public String getXmldoc() {
        return xmldoc;
    }

    public void setReloadSprav(boolean reloadSprav) {
        this.reloadSprav = reloadSprav;
    }

    public boolean getReloadSprav() {
        return reloadSprav;
    }

    public ISelectModel getSpravTypeModel() {
        return spravTypeModel;
    }

    public void setSpravTypeModel(ISelectModel spravTypeModel) {
        this.spravTypeModel = spravTypeModel;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public EducationdirectionSpravType getSpravType()
    {
        return settings.get("spravType");
    }
}
