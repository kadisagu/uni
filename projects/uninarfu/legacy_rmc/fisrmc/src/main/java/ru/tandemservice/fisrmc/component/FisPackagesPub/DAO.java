package ru.tandemservice.fisrmc.component.FisPackagesPub;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.fisrmc.entity.FisAnswerTofisPackages;
import ru.tandemservice.fisrmc.entity.FisPackages;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;

public class DAO extends UniDao<Model> implements IDAO {
    @Override
    public void prepare(Model model)
    {
        model.setPkg(getNotNull(FisPackages.class, model.getPkgId()));
    }

    @Override
    public void update(Model model)
    {
        saveOrUpdate(model.getPkg());
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(FisAnswerTofisPackages.class, "pkg");
        dql.column("pkg");

        // фильтр по пакету
        dql.where(DQLExpressions.eq(
                DQLExpressions.property(FisAnswerTofisPackages.fisPackages().fromAlias("pkg")),
                DQLExpressions.value(model.getPkg())
        ));

        // применим сортировку
        DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(FisAnswerTofisPackages.class, "pkg");

        orderRegistry.applyOrder(dql, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), dql, getSession());
    }
}
