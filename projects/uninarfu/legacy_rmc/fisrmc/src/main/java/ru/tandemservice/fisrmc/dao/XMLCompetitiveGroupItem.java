package ru.tandemservice.fisrmc.dao;

import ru.tandemservice.fisrmc.entity.Educationdirectionfis;
import ru.tandemservice.fisrmc.entity.catalog.Eceducationlevelfis;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.List;

/**
 * Направления подготовки конкурсной группы
 *
 * @author vch
 *         <p/>
 *         примечание: на одно направление подготовки ФИС
 *         может быть несколько направлений подготовки для приема (см. базовый класс)
 */
public class XMLCompetitiveGroupItem
        extends XMLEnrollmentDirection
{

    private XMLCompetitiveGroup xmlCompetitiveGroup;

    public XMLCompetitiveGroupItem(
            XMLCompetitiveGroup xmlCompetitiveGroup,
            Eceducationlevelfis eceducationlevelfis,
            Educationdirectionfis educationdirectionfis,
            EnrollmentCampaign enrollmentCampaign,
            List<EnrollmentDirection> enrollmentDirectionsList)
    {
        super(eceducationlevelfis, educationdirectionfis, enrollmentCampaign,
              enrollmentDirectionsList);

        this.xmlCompetitiveGroup = xmlCompetitiveGroup;
    }

    public void setXmlCompetitiveGroup(XMLCompetitiveGroup xmlCompetitiveGroup) {
        this.xmlCompetitiveGroup = xmlCompetitiveGroup;
    }

    public XMLCompetitiveGroup getXmlCompetitiveGroup() {
        return xmlCompetitiveGroup;
    }


    public String getUid()
    {
        String rv = xmlCompetitiveGroup.getUid() + this.getEducationdirectionfis().getId();
        return "sum:" + rv.toString().hashCode();
    }

}
