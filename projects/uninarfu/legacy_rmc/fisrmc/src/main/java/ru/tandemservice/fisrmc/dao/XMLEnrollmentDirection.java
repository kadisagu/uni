package ru.tandemservice.fisrmc.dao;

import ru.tandemservice.fisrmc.entity.Educationdirectionfis;
import ru.tandemservice.fisrmc.entity.catalog.Eceducationlevelfis;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.ArrayList;
import java.util.List;


/**
 * Информация по направлениям подготовки
 *
 * @author vch
 */
public class XMLEnrollmentDirection {

    /**
     * направление подготовки
     */
    private Educationdirectionfis educationdirectionfis = null;

    private EnrollmentCampaign enrollmentCampaign = null;
    private List<EnrollmentDirection> enrollmentDirectionsList = new ArrayList<EnrollmentDirection>();

    /**
     * уровень образования (Бакалавр, Магистр и так далее)
     */
    private Eceducationlevelfis eceducationlevelfis = null;

    public XMLEnrollmentDirection
            (
                    Eceducationlevelfis eceducationlevelfis
                    , Educationdirectionfis educationdirectionfis
                    , EnrollmentCampaign enrollmentCampaign
                    , List<EnrollmentDirection> enrollmentDirectionsList
            )
    {
        this.eceducationlevelfis = eceducationlevelfis;
        this.setEducationdirectionfis(educationdirectionfis);
        this.setEnrollmentCampaign(enrollmentCampaign);
        this.setEnrollmentDirectionsList(enrollmentDirectionsList);
    }

    public void setEducationdirectionfis(Educationdirectionfis educationdirectionfis) {
        this.educationdirectionfis = educationdirectionfis;
    }

    public Educationdirectionfis getEducationdirectionfis() {
        return educationdirectionfis;
    }

    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign) {
        this.enrollmentCampaign = enrollmentCampaign;
    }

    public EnrollmentCampaign getEnrollmentCampaign() {
        return enrollmentCampaign;
    }

    public void setEnrollmentDirectionsList(List<EnrollmentDirection> enrollmentDirectionsList) {
        this.enrollmentDirectionsList = enrollmentDirectionsList;
    }

    public List<EnrollmentDirection> getEnrollmentDirectionsList() {
        return enrollmentDirectionsList;
    }

    public void setEceducationlevelfis(Eceducationlevelfis eceducationlevelfis) {
        this.eceducationlevelfis = eceducationlevelfis;
    }

    public Eceducationlevelfis getEceducationlevelfis() {
        return eceducationlevelfis;
    }

    public String getUid()
    {
        Long id = eceducationlevelfis.getId()
                + educationdirectionfis.getId()
                + enrollmentCampaign.getId();

        return Long.toString(id);
    }
}
