package ru.tandemservice.fisrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.fisrmc.entity.EntranceDisciplineType2Entrancedisciplinetypefis;
import ru.tandemservice.fisrmc.entity.catalog.Entrancedisciplinetypefis;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность-связь Типы вступительных испытаний (Тандем) и Тип вступительных испытаний (ФИС)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntranceDisciplineType2EntrancedisciplinetypefisGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.fisrmc.entity.EntranceDisciplineType2Entrancedisciplinetypefis";
    public static final String ENTITY_NAME = "entranceDisciplineType2Entrancedisciplinetypefis";
    public static final int VERSION_HASH = 446082305;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANCE_DISCIPLINE_TYPE = "entranceDisciplineType";
    public static final String L_ENTRANCEDISCIPLINETYPEFIS = "entrancedisciplinetypefis";

    private EntranceDisciplineType _entranceDisciplineType;     // Тип вступительного испытания
    private Entrancedisciplinetypefis _entrancedisciplinetypefis;     // Тип вступительных испытаний

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип вступительного испытания. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EntranceDisciplineType getEntranceDisciplineType()
    {
        return _entranceDisciplineType;
    }

    /**
     * @param entranceDisciplineType Тип вступительного испытания. Свойство не может быть null и должно быть уникальным.
     */
    public void setEntranceDisciplineType(EntranceDisciplineType entranceDisciplineType)
    {
        dirty(_entranceDisciplineType, entranceDisciplineType);
        _entranceDisciplineType = entranceDisciplineType;
    }

    /**
     * @return Тип вступительных испытаний. Свойство не может быть null.
     */
    @NotNull
    public Entrancedisciplinetypefis getEntrancedisciplinetypefis()
    {
        return _entrancedisciplinetypefis;
    }

    /**
     * @param entrancedisciplinetypefis Тип вступительных испытаний. Свойство не может быть null.
     */
    public void setEntrancedisciplinetypefis(Entrancedisciplinetypefis entrancedisciplinetypefis)
    {
        dirty(_entrancedisciplinetypefis, entrancedisciplinetypefis);
        _entrancedisciplinetypefis = entrancedisciplinetypefis;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntranceDisciplineType2EntrancedisciplinetypefisGen)
        {
            setEntranceDisciplineType(((EntranceDisciplineType2Entrancedisciplinetypefis)another).getEntranceDisciplineType());
            setEntrancedisciplinetypefis(((EntranceDisciplineType2Entrancedisciplinetypefis)another).getEntrancedisciplinetypefis());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntranceDisciplineType2EntrancedisciplinetypefisGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntranceDisciplineType2Entrancedisciplinetypefis.class;
        }

        public T newInstance()
        {
            return (T) new EntranceDisciplineType2Entrancedisciplinetypefis();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entranceDisciplineType":
                    return obj.getEntranceDisciplineType();
                case "entrancedisciplinetypefis":
                    return obj.getEntrancedisciplinetypefis();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entranceDisciplineType":
                    obj.setEntranceDisciplineType((EntranceDisciplineType) value);
                    return;
                case "entrancedisciplinetypefis":
                    obj.setEntrancedisciplinetypefis((Entrancedisciplinetypefis) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entranceDisciplineType":
                        return true;
                case "entrancedisciplinetypefis":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entranceDisciplineType":
                    return true;
                case "entrancedisciplinetypefis":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entranceDisciplineType":
                    return EntranceDisciplineType.class;
                case "entrancedisciplinetypefis":
                    return Entrancedisciplinetypefis.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntranceDisciplineType2Entrancedisciplinetypefis> _dslPath = new Path<EntranceDisciplineType2Entrancedisciplinetypefis>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntranceDisciplineType2Entrancedisciplinetypefis");
    }
            

    /**
     * @return Тип вступительного испытания. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.fisrmc.entity.EntranceDisciplineType2Entrancedisciplinetypefis#getEntranceDisciplineType()
     */
    public static EntranceDisciplineType.Path<EntranceDisciplineType> entranceDisciplineType()
    {
        return _dslPath.entranceDisciplineType();
    }

    /**
     * @return Тип вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.EntranceDisciplineType2Entrancedisciplinetypefis#getEntrancedisciplinetypefis()
     */
    public static Entrancedisciplinetypefis.Path<Entrancedisciplinetypefis> entrancedisciplinetypefis()
    {
        return _dslPath.entrancedisciplinetypefis();
    }

    public static class Path<E extends EntranceDisciplineType2Entrancedisciplinetypefis> extends EntityPath<E>
    {
        private EntranceDisciplineType.Path<EntranceDisciplineType> _entranceDisciplineType;
        private Entrancedisciplinetypefis.Path<Entrancedisciplinetypefis> _entrancedisciplinetypefis;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип вступительного испытания. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.fisrmc.entity.EntranceDisciplineType2Entrancedisciplinetypefis#getEntranceDisciplineType()
     */
        public EntranceDisciplineType.Path<EntranceDisciplineType> entranceDisciplineType()
        {
            if(_entranceDisciplineType == null )
                _entranceDisciplineType = new EntranceDisciplineType.Path<EntranceDisciplineType>(L_ENTRANCE_DISCIPLINE_TYPE, this);
            return _entranceDisciplineType;
        }

    /**
     * @return Тип вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.EntranceDisciplineType2Entrancedisciplinetypefis#getEntrancedisciplinetypefis()
     */
        public Entrancedisciplinetypefis.Path<Entrancedisciplinetypefis> entrancedisciplinetypefis()
        {
            if(_entrancedisciplinetypefis == null )
                _entrancedisciplinetypefis = new Entrancedisciplinetypefis.Path<Entrancedisciplinetypefis>(L_ENTRANCEDISCIPLINETYPEFIS, this);
            return _entrancedisciplinetypefis;
        }

        public Class getEntityClass()
        {
            return EntranceDisciplineType2Entrancedisciplinetypefis.class;
        }

        public String getEntityName()
        {
            return "entranceDisciplineType2Entrancedisciplinetypefis";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
