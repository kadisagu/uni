package ru.tandemservice.fisrmc.dao.packEduDocuments;

import org.tandemframework.shared.person.base.entity.PersonEduInstitution;

import java.util.Date;

/**
 * диплом о среднем проффесиональном образовании
 *
 * @author belarin
 */
public class XMLMiddleEduDiplomaDocument
        extends XMLBaseEducationDocument
{
    /**
     * Год окончания, не обязательно
     */
    private Integer endYear = null;

    /**
     * Регистрационный номер, не обязательно
     */
    private String registrationNumber;

    public XMLMiddleEduDiplomaDocument
            (
                    String uid
                    , boolean originalReceived
                    , Date originalReceivedDate
                    , String documentSeries
                    , String documentNumber
                    , Date documentDate
                    , String documentOrganization
                    , Integer endYear
                    , String registrationNumber
                    , PersonEduInstitution personEduInstitution
            )
    {
        super(uid, originalReceived, originalReceivedDate, documentSeries,
              documentNumber, documentDate, documentOrganization, personEduInstitution);

        setEndYear(endYear);
        setRegistrationNumber(registrationNumber);
    }

    public void setEndYear(Integer endYear) {
        this.endYear = endYear;
    }

    public Integer getEndYear()
    {
        return endYear;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

}
