package ru.tandemservice.fisrmc.component.settings.EnrollmentCampaignStageFIS;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.fisrmc.entity.EnrollmentCampaignStageFIS;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

public class DAO extends UniDao<Model> {

    @Override
    public void prepare(Model model) {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
    }

    @Override
    public void prepareListDataSource(Model model) {
        MQBuilder builder = new MQBuilder(EnrollmentCampaignStageFIS.ENTITY_CLASS, "e");

        EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();
        if (enrollmentCampaign != null) {
            builder.add(MQExpression.eq("e", EnrollmentCampaignStageFIS.enrollmentCampaign(), enrollmentCampaign));
        }

        builder.applyOrder(model.getDataSource().getEntityOrder());

        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }

}
