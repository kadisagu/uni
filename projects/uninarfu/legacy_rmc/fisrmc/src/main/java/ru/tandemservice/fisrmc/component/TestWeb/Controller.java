package ru.tandemservice.fisrmc.component.TestWeb;


import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BooleanColumn;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.fisrmc.dao.DaemonParams;
import ru.tandemservice.fisrmc.dao.PackUnpackToFis;
import ru.tandemservice.fisrmc.entity.FisPackages;
import ru.tandemservice.fisrmc.entity.catalog.EducationdirectionSpravType;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Controller
        extends AbstractBusinessController<IDAO, Model>
{

    @Override
    public void onRefreshComponent(IBusinessComponent context)
    {
/*
        String id = context.getUserContext().getHttpSessionId();
		Collection<HttpSession> lst = HttpSessionStorage.getSessions();
		for (HttpSession session : lst)
		{
			if (session.getId().equals(id))
			{
				System.out.print("current session");
			}
		}
	*/

        Model model = getModel(context);
        getDao().prepare(model);

        if (model.getSettings() == null)
            model.setSettings(context.getSettings());

        prepareDataSource(context);

    }


    public void onClickGetState(IBusinessComponent context)
    {
        ErrorCollector errors = context.getUserContext().getErrorCollector();

        if (!DaemonParams.NEED_RUN)
            errors.add("Служба не запущена " + PackUnpackToFis.MSG_LOG);
        else
            errors.add(PackUnpackToFis.MSG_LOG);

    }


    public void onClickTest(IBusinessComponent context)
    {
        // Model model = getModel(context);
        // model.setRequest(PackUnpackToFis.Instanse().testPackEnrollmentCampaign());
    }

    public void onClickPackToFile(IBusinessComponent context)
    {

        Model model = getModel(context);
        context.saveSettings();

        ErrorCollector errors = context.getUserContext().getErrorCollector();

        if (DaemonParams.NEED_RUN) {
            errors.add("Служба уже исполняется " + PackUnpackToFis.MSG_LOG);
            return;
        }
        try {

            // <TODO> Блокируем лишние флажки в UI
            model.setAbfuscateData(false);
            model.setTestApplicationMode(false);
            model.setEntranceTestResult(true); // оценки в заявлениях


            @SuppressWarnings("unchecked")
            List<EnrollmentCampaign> lst = model.getSettings().get("enrollmentCampaignList");


            PackUnpackToFis.CG_COUNT = model.getCgCount();
            PackUnpackToFis.ABIT_COUNT = model.getAbitCount();
            PackUnpackToFis.ABIT_START_AT = model.getStartAbit();

            List<Integer> regNumbers = new ArrayList<>();
            List<Integer> excludeRegNumbers = new ArrayList<>();

            _parseStringToArray(model.getRegNumber(), regNumbers);
            _parseStringToArray(model.getExcludeRegNumber(), excludeRegNumbers);

            IDataSettings settings = model.getSettings();

            Date dateFrom = settings.get("registeredFromFilter");
            Date dateTo = settings.get("registeredToFilter");

            EducationdirectionSpravType spravType = model.getSettings().get("spravType");

            DaemonParams params = new DaemonParams(

                    model.getFisUserName()
                    , model.getFisPwd()
                    , model.getCampaignInfo()
                    , model.getAdmissionInfo()
                    , model.getApplications()
                    , model.getEntranceTestResult()
                    , model.getTestApplicationMode()
                    , model.getOrdersOfAdmission()
                    , model.getBenefitRequest()
                    , model.getNotRfMode()
                    , model.getSuspendErrorDoc()
                    , model.getAbfuscateData()
                    , model.getSuspendCompetitiveGroup()
                    , model.getAppDateWithoutTime()
                    , regNumbers
                    , excludeRegNumbers
                    , dateFrom
                    , dateTo
                    , model.getWhithoutSendedInFis()
                    , model.getWhithoutEntrantInOrder()
                    , lst
                    , spravType);

            PackUnpackToFis.MSG_LOG = "Процесс стартован";
            PackUnpackToFis.DAEMON_PARAMS = params;

            // стартуем
            DaemonParams.NEED_RUN = true;
            PackUnpackToFis.DAEMON.wakeUpDaemon();

        }
        catch (Exception ex) {
            throw new ApplicationException("Ошибка импорта в файл " + ex.getMessage(), ex);
        }
    }


    protected void _parseStringToArray(String numbersString, List<Integer> intArray)
    {
        if (numbersString != null && !numbersString.equals("")) {
            try {
                String[] rNums = numbersString.split(",");
                for (String rn : rNums) {
                    intArray.add(Integer.parseInt(rn.trim()));
                }
            }
            catch (Exception ex) {
                throw new ApplicationException("Номер заявления введен не корректно");
            }
        }

    }


    public void onClickGetFile(IBusinessComponent component)
    {
        Model model = component.getModel();
        Long lastInsertedId;// = PackUnpackToFis.LAST_ID;
        //if (lastInsertedId == 0L) {
            lastInsertedId = getDao().getLastAddedPackage(model);
        //}

        if (lastInsertedId != null && lastInsertedId != 0L) {
            FisPackages pkg = UniDaoFacade.getCoreDao().get(lastInsertedId);
            byte[] content = pkg.getXmlPackage().getContent();
            if (null == content)
                throw new ApplicationException("Файл с результатами импорта пуст");
            Integer temporaryId = PrintReportTemporaryStorage.registerTemporaryPrintForm(content, "fis.xml");

            activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT,
                                                             (new UniMap())
                                                                     .add("id", temporaryId)
                                                                     .add("zip", Boolean.FALSE)
                                                                     .add("extension", "xml"))
            );
        }
        else {
            throw new ApplicationException("Файл с результатами импорта еще не создан");
        }

/*
		    String path = ApplicationRuntime.getAppInstallPath();
	    	
	    	File file = new File(path, "impexp/fis/fromTU.xml");
	    	if (file.exists())
	    	{
	    		// можно прочитать содержимое
	    		try {
					byte[] bts = FileUtils.readFileToByteArray(file);
					 Integer id 
			    	  = PrintReportTemporaryStorage.registerTemporaryPrintForm(bts, "fis.xml");
			          activateInRoot(component, new ComponentActivator("ru.tandemservice.uni.component.reports.PrintReport", (new UniMap()).add("id", id).add("zip", Boolean.FALSE).add("extension", "xml")));
					
				} catch (IOException e) 
				{
					throw CoreExceptionUtils.getRuntimeException(e);
				}
	    	}
	    	else
	    	{
	    		//ErrorCollector errors = component.getUserContext().getErrorCollector();
	    		throw new ApplicationException("Файл с результатами импорта еще не создан");
	    	}
	    	*/

    }

    public void onDeleteEntity(IBusinessComponent context) {
        PackUnpackToFis.LAST_ID = 0L;
        getDao().deleteRow(context);
    }

    public void onClickDownloadPackage(IBusinessComponent context) {
        FisPackages pkg = UniDaoFacade.getCoreDao().get((Long) context.getListenerParameter());
        DatabaseFile dbFile = pkg.getXmlPackage();
        if (dbFile != null && dbFile.getContent() != null) {
            Integer temporaryId = PrintReportTemporaryStorage.registerTemporaryPrintForm(dbFile.getContent(), "fis.xml");
            activateInRoot(context, new ComponentActivator(IUniComponents.PRINT_REPORT,
                                                           (new UniMap())
                                                                   .add("id", temporaryId)
                                                                   .add("zip", Boolean.FALSE)
                                                                   .add("extension", "xml"))
            );
        }
        else {
            throw new ApplicationException("xml пакет пуст");
        }
    }

    public void onClickDownloadErrors(IBusinessComponent context) {
        FisPackages pkg = UniDaoFacade.getCoreDao().get((Long) context.getListenerParameter());
        DatabaseFile dbFile = pkg.getXmlErrors();
        if (dbFile != null && dbFile.getContent() != null) {
            Integer temporaryId = PrintReportTemporaryStorage.registerTemporaryPrintForm(dbFile.getContent(), "errors.rtf");
            activateInRoot(context, new ComponentActivator(IUniComponents.PRINT_REPORT,
                                                           (new UniMap())
                                                                   .add("id", temporaryId)
                                                                   .add("zip", Boolean.FALSE)
                                                                   .add("extension", "rtf"))
            );
        }
        else {
            throw new ApplicationException("файл с ошибками пуст");
        }
    }

    public void onClickApply(IBusinessComponent context) {
        context.saveSettings();
        onRefreshComponent(context);
    }

    protected void prepareDataSource(IBusinessComponent context)
    {

        Model model = getModel(context);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<FisPackages> dataSource = UniBaseUtils.createDataSource(context, getDao());
        createColumns(dataSource);

        model.setDataSource(dataSource);
    }

    protected void createColumns(DynamicListDataSource<FisPackages> dataSource) {
        dataSource.addColumn(new SimpleColumn("Дата формирования", FisPackages.P_FORMING_DATE, DateFormatter.DATE_FORMATTER_WITH_TIME).setClickable(true));
        dataSource.addColumn(new SimpleColumn("Сведения о загрузке в тестовом режиме", FisPackages.P_TEST_APP_MODE_DATA).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Примечание выгрузки", FisPackages.P_DESCRIPTION).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Приемная кампания", FisPackages.P_ENROLLMENT_CAMPAIGN).setClickable(false));

        // тип справочника
        dataSource.addColumn(new SimpleColumn("Тип справочников", FisPackages.spravType().title().s()).setClickable(false));


        //dataSource.addColumn(new BooleanColumn("Загрузка в тестовом режиме", FisPackages.P_TEST_APPLICATION_MODE).setClickable(false));
        dataSource.addColumn(new BooleanColumn("Информация о приемной кампании", FisPackages.P_CAMPAIGN_INFO).setClickable(false));
        dataSource.addColumn(new BooleanColumn("Сведения об объеме и структуре приема", FisPackages.P_ADMISSION_INFO).setClickable(false));
        dataSource.addColumn(new BooleanColumn("Заявления", FisPackages.P_APPLICATIONS).setClickable(false));
        dataSource.addColumn(new BooleanColumn("Оценки в заявлении", FisPackages.P_ENTRANCE_TEST_RESULT).setClickable(false));
        dataSource.addColumn(new BooleanColumn("Принудительно отменить конкурсные группы", FisPackages.P_SUSPEND_COMPETITIVE_GROUP).setClickable(false));
        dataSource.addColumn(new BooleanColumn("Подавлять ошибки неполных данных", FisPackages.P_SUSPEND_ERROR_DOC).setClickable(false));
        //dataSource.addColumn(new BooleanColumn("Скрывать персональные данные", FisPackages.P_ABFUSCATE_DATA).setClickable(false));
        dataSource.addColumn(new BooleanColumn("Включенные в приказ", FisPackages.P_ORDERS_OF_ADMISSION).setClickable(false));
        dataSource.addColumn(new BooleanColumn("Есть ошибки", FisPackages.P_GLOBAL_ERROR).setClickable(false));
        dataSource.addColumn(new IndicatorColumn("Скачать пакет", null, "onClickDownloadPackage").defaultIndicator(new IndicatorColumn.Item("printer", "Печать")).setImageHeader(false).setDisableSecondSubmit(false).setOrderable(false));
        dataSource.addColumn(new IndicatorColumn("Скачать ошибки формирования пакета", null, "onClickDownloadErrors").defaultIndicator(new IndicatorColumn.Item("printer", "Печать")).setImageHeader(false).setDisableSecondSubmit(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Удалить пакет", ActionColumn.DELETE, "onDeleteEntity", "Удалить пакет?"));

    }
}
