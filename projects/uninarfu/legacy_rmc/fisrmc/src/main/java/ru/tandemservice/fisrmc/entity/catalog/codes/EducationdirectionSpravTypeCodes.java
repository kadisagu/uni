package ru.tandemservice.fisrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип справочника направлений подготовки ФИС"
 * Имя сущности : educationdirectionSpravType
 * Файл data.xml : catalogs.data.xml
 */
public interface EducationdirectionSpravTypeCodes
{
    /** Константа кода (code) элемента : 2013 (title) */
    String TITLE_2013 = "2013";
    /** Константа кода (code) элемента : 2014 Тест (title) */
    String TITLE_2014_TEST = "2014Test";
    /** Константа кода (code) элемента : 2014 Прод (title) */
    String TITLE_2014_PROD = "2014Prod";

    Set<String> CODES = ImmutableSet.of(TITLE_2013, TITLE_2014_TEST, TITLE_2014_PROD);
}
