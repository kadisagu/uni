package ru.tandemservice.fisrmc.component.settings.GroupEsForFis.AddEdit;


import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import ru.tandemservice.fisrmc.entity.GroupEsForFis;
import ru.tandemservice.fisrmc.entity.GroupEsForFis2Es;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.catalog.EducationSubject;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {
    @Override
    public void prepare(final Model model)
    {
        if (model.getGroupEsForFis().getId() != null) {
            model.setGroupEsForFis(get(GroupEsForFis.class, model.getGroupEsForFis().getId()));
            model.setEditForm(true);
        }

        model.setFilterForm(model.initFilters());

        EntrantFilterUtil.prepareEnrollmentCampaignFilterWithSecurity(model, getSession());
        model.setQualificationModel(new QualificationModel(getSession()));
        model.setFormativeOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING, null));
        model.setEnrollmentDirectionModel(new DQLFullCheckSelectModel() {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, alias);

                if (!model.getEditForm() && model.getEnrollmentDirectionIdList().size() > 0) {
                    builder.where(DQLExpressions.in(
                            DQLExpressions.property(EnrollmentDirection.id().fromAlias(alias)),
                            model.getEnrollmentDirectionIdList()
                    ));
                }

                if (model.getFilterForm()) {
                    IDataSettings settings = model.getSettings();

                    Object enrollmentCampaignList = model.getEnrollmentCampaign();
                    Object qualificationList = settings.get("qualificationList");
                    Object formativeOU = settings.get("formativeOrgUnit");

                    FilterUtils.applySelectFilter(builder, alias, EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, enrollmentCampaignList);
                    FilterUtils.applySelectFilter(builder, alias, EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().educationLevel().qualification(), qualificationList);
                    FilterUtils.applySelectFilter(builder, alias, EnrollmentDirection.educationOrgUnit().formativeOrgUnit(), formativeOU);
                }

                FilterUtils.applySimpleLikeFilter(builder, alias, EnrollmentDirection.title(), filter);
                return builder;
            }
        });
        if (model.getEnrollmentCampaingId() != null)
            model.setEnrollmentCampaign(get(EnrollmentCampaign.class, model.getEnrollmentCampaingId()));

    }

    @Override
    public void prepareListDataSource(Model model)
    {
        EnrollmentDirection direction = model.getGroupEsForFis().getEnrollmentDirection();
        List<EducationSubject> lst = new ArrayList<>();
        List<EntranceDiscipline> disciplines;

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EntranceDiscipline.class, "ed")
                .column("ed");

        if (direction != null) {
            builder.where(DQLExpressions.eq(
                    DQLExpressions.property(EntranceDiscipline.enrollmentDirection().fromAlias("ed")),
                    DQLExpressions.value(direction)
            ));

            disciplines = builder.createStatement(getSession()).list();

            for (EntranceDiscipline discipline : disciplines) {
                for (Discipline2RealizationWayRelation discipline2RWR : discipline.getDiscipline().getDisciplines()) {
                    lst.add(discipline2RWR.getEducationSubject());
                }
            }
        }

        Collections.sort(lst, (educationSubject, educationSubject2) -> {
            String nameSubject1 = educationSubject.getTitle();
            String nameSubject2 = educationSubject2.getTitle();

            return nameSubject1.compareTo(nameSubject2);
        });

        DynamicListDataSource<EducationSubject> dataSource = model.getEducationSubjectDS();
        UniBaseUtils.createPage(dataSource, lst);

        DQLSelectBuilder builder1 = new DQLSelectBuilder()
                .fromEntity(GroupEsForFis2Es.class, "gr")
                .where(DQLExpressions.eq(
                        DQLExpressions.property(GroupEsForFis2Es.groupEsForFis().fromAlias("gr")),
                        DQLExpressions.value(model.getGroupEsForFis())
                ))
                .column("gr");

        DynamicListDataSource<GroupEsForFis2Es> groupDS = model.getGroupEsForFis2EsDS();
        UniBaseUtils.createPage(groupDS, builder1, getSession());
    }

    @Override
    public void moveGroups(Model model)
    {
        if (model.getGroupEsForFis().getGroupName() == null || model.getGroupEsForFis().getEnrollmentDirection() == null) {
            throw new ApplicationException("Заполните необходимые поля");
        }

        saveOrUpdate(model.getGroupEsForFis());

        for (EducationSubject subject : model.getEducationSubjectDS().getSelectedEntities()) {
            checkAndSaveGroupEsForFis2Es(model, subject);
        }
    }


    @Override
    public void moveAllGroups(Model model)
    {
        if (model.getGroupEsForFis().getGroupName() == null || model.getGroupEsForFis().getEnrollmentDirection() == null) {
            throw new ApplicationException("Заполните необходимые поля");
        }

        saveOrUpdate(model.getGroupEsForFis());

        for (EducationSubject subject : model.getEducationSubjectDS().getEntityList()) {
            checkAndSaveGroupEsForFis2Es(model, subject);
        }
    }

    @Override
    public void update(Model model)
    {
        GroupEsForFis groupEsForFis = model.getGroupEsForFis();
        saveOrUpdate(groupEsForFis);
    }

    private void checkAndSaveGroupEsForFis2Es(Model model, EducationSubject subject)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(GroupEsForFis2Es.class, "gr")
                .where(DQLExpressions.eq(
                        DQLExpressions.property(GroupEsForFis2Es.groupEsForFis().fromAlias("gr")),
                        DQLExpressions.value(model.getGroupEsForFis())
                ))
                .where(DQLExpressions.eq(
                        DQLExpressions.property(GroupEsForFis2Es.educationSubject().fromAlias("gr")),
                        DQLExpressions.value(subject)
                ));
        GroupEsForFis2Es groupEsForFis2Es = builder.createStatement(getSession()).uniqueResult();
        if (groupEsForFis2Es == null) {
            groupEsForFis2Es = new GroupEsForFis2Es();
            groupEsForFis2Es.setEducationSubject(subject);
            groupEsForFis2Es.setGroupEsForFis(model.getGroupEsForFis());
            saveOrUpdate(groupEsForFis2Es);
        }
    }

}
