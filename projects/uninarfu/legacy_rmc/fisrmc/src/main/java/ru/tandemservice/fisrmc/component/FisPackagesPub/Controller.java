package ru.tandemservice.fisrmc.component.FisPackagesPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.fisrmc.entity.FisAnswerTofisPackages;
import ru.tandemservice.fisrmc.entity.FisPackages;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unibase.UniBaseUtils;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent context)
    {
        Model model = getModel(context);
        getDao().prepare(model);

        if (model.getSettings() == null)
            model.setSettings(UniBaseUtils.getDataSettings(context, "FisPackagesPub"));

        prepareDataSource(context);

    }

    protected void prepareDataSource(IBusinessComponent context)
    {
        Model model = getModel(context);
        if (model.getDataSource() != null) return;

        // столбики
        DynamicListDataSource<FisAnswerTofisPackages> dataSourse = UniBaseUtils.createDataSource(context, getDao());

        dataSourse.addColumn(new SimpleColumn("Дата", FisAnswerTofisPackages.P_ANSWER_DATE, DateFormatter.DATE_FORMATTER_WITH_TIME).setClickable(false).setOrderable(true));
        dataSourse.addColumn(new SimpleColumn("Тип ответа", FisAnswerTofisPackages.fisAnswerType().title().s()).setClickable(false).setOrderable(false));
        dataSourse.addColumn(new SimpleColumn("Примечание", FisAnswerTofisPackages.fisPrim().s()).setClickable(false).setOrderable(false));
        dataSourse.addColumn(new IndicatorColumn("Скачать пакет", null, "onClickDownloadAnswer").defaultIndicator(new IndicatorColumn.Item("printer", "Скачать")).setImageHeader(false).setDisableSecondSubmit(false).setOrderable(false));

        dataSourse.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onEditEntity", "Редактировать?"));
        dataSourse.addColumn(new ActionColumn("Удалить ответ", ActionColumn.DELETE, "onDeleteEntity", "Удалить ответ?"));

        model.setDataSource(dataSourse);
        /*
        <property name="answerDate" type="timestamp" title="Дата ответа" required="true"/>
        <many-to-one name="fisPackages" entity-ref="fisPackages" backward-cascade="delete" required="true"/>
        <many-to-one name="fisAnswerType" entity-ref="fisAnswerType"  required="true"/>
        <property name="fisPrim" type="text" title="Примечание из ФИС" required="false" />
        <many-to-one name="xmlAnswer" entity-ref="databaseFile" title="Ответ ФИС"/>
         */
    }

    public void onClickDownloadAnswer(IBusinessComponent context)
    {
        FisAnswerTofisPackages pkg = UniDaoFacade.getCoreDao().get((Long) context.getListenerParameter());
        DatabaseFile dbFile = pkg.getXmlAnswer();

        if (dbFile != null && dbFile.getContent() != null) {
            Integer temporaryId = PrintReportTemporaryStorage.registerTemporaryPrintForm(dbFile.getContent(), "fisAnswer_" + pkg.getId() + ".xml");
            activateInRoot(context, new ComponentActivator(IUniComponents.PRINT_REPORT,
                                                           (new ParametersMap())
                                                                   .add("id", temporaryId)
                                                                   .add("zip", Boolean.FALSE)
                                                                   .add("extension", "xml"))
            );
        }
        else {
            throw new ApplicationException("xml пакет пуст");
        }

    }

    public void onDeleteEntity(IBusinessComponent context)
    {
        Long removeId = context.getListenerParameter();
        getDao().delete(removeId);

    }

    public void onEditEntity(IBusinessComponent context)
    {
        Long EditId = context.getListenerParameter();

        //Model model = getModel(context);
        ParametersMap map = new ParametersMap();
        map.add(PublisherActivator.PUBLISHER_ID_KEY, EditId);
        map.add("inarea", true);
        //map.add("fisPackagesId", model.getPkg().getId());
        String pkgName = ru.tandemservice.fisrmc.component.FisAnswerTofisPackagesAddEdit.Model.class.getPackage().getName();
        context.createChildRegion("FisAnswerTofisPackagesAddEdit", new ComponentActivator
                (
                        pkgName
                        , map));

    }

    public void onClickDownloadPackage(IBusinessComponent context) {
        FisPackages pkg = getModel(context).getPkg();
        DatabaseFile dbFile = pkg.getXmlPackage();
        if (dbFile != null && dbFile.getContent() != null) {
            Integer temporaryId = PrintReportTemporaryStorage.registerTemporaryPrintForm(dbFile.getContent(), "fis.xml");
            activateInRoot(context, new ComponentActivator(IUniComponents.PRINT_REPORT,
                                                           (new ParametersMap())
                                                                   .add("id", temporaryId)
                                                                   .add("zip", Boolean.FALSE)
                                                                   .add("extension", "xml"))
            );
        }
        else {
            throw new ApplicationException("xml пакет пуст");
        }
    }

    public void onClickDownloadErrors(IBusinessComponent context) {
        FisPackages pkg = getModel(context).getPkg();
        DatabaseFile dbFile = pkg.getXmlErrors();
        if (dbFile != null && dbFile.getContent() != null) {
            Integer temporaryId = PrintReportTemporaryStorage.registerTemporaryPrintForm(dbFile.getContent(), "errors.rtf");
            activateInRoot(context, new ComponentActivator(IUniComponents.PRINT_REPORT,
                                                           (new UniMap())
                                                                   .add("id", temporaryId)
                                                                   .add("zip", Boolean.FALSE)
                                                                   .add("extension", "rtf"))
            );
        }
        else {
            throw new ApplicationException("файл с ошибками пуст");
        }
    }

    /**
     * Сохранить изменения, ничего не сохранять остаться на этом-же элементе
     *
     * @param context
     */
    public void onClickApply(IBusinessComponent context)
    {
        getDao().update(getModel(context));
    }

    /**
     * Добавить ответ ФИС
     *
     * @param context
     */
    public void onClickAddAnswer(IBusinessComponent context)
    {
        Model model = getModel(context);

        ParametersMap map = new ParametersMap();
        map.add(PublisherActivator.PUBLISHER_ID_KEY, null);
        map.add("fisPackagesId", model.getPkg().getId());
        map.add("inarea", true);

        String pkgName = ru.tandemservice.fisrmc.component.FisAnswerTofisPackagesAddEdit.Model.class.getPackage().getName();

        context.createChildRegion("FisAnswerTofisPackagesAddEdit", new ComponentActivator
                (
                        pkgName
                        , map));

    }
}
