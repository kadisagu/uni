package ru.tandemservice.fisrmc.dao.answer;

import ru.tandemservice.fisrmc.component.UploadFis2TU.Pub.DefaultParser;

public class AnswerParserOrdersOfAdmission extends DefaultParser
{
    public static final String ROOT = "ImportResultPackage";
    public static final String LOG = "Log";
    public static final String FAILED = "Failed";
    public static final String ORDERS_OfAdmission = "OrdersOfAdmissions";

    public static final String APPLICATION = "Application";
    public static final String APPLICATION_NUMBER = "ApplicationNumber";
    public static final String APPLICATION_DATE = "RegistrationDate";

    public static final String ERROR = "ErrorInfo";
    public static final String ERROR_CODE = "ErrorCode";
    public static final String ERROR_MESS = "Message";

    public AnswerParserOrdersOfAdmission(String xml) {
        super(xml);

        this.elementPath = ROOT + "-" + LOG + "-" + FAILED + "-" + ORDERS_OfAdmission + "-" + APPLICATION;

        this.propertyMap.put(
                this.elementPath + "-" + APPLICATION_NUMBER,
                "number"
        );
        this.propertyMap.put(
                this.elementPath + "-" + APPLICATION_DATE,
                "date"
        );
        this.propertyMap.put(
                this.elementPath + "-" + ERROR + "-" + ERROR_CODE,
                "code"
        );
        this.propertyMap.put(
                this.elementPath + "-" + ERROR + "-" + ERROR_MESS,
                "message"
        );
    }

}
