package ru.tandemservice.fisrmc.dao.packApplications;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.tandemservice.uniec.entity.entrant.Entrant;


public interface IPackEgeDocuments {


    /**
     * Необходимо создать
     * xml елемент <EgeDocuments>
     * в соответствии с описанием на странице 25
     * и возвратить его из данного метода
     *
     * @param doc
     * @param entrant
     *
     * @return
     */
    public Element packEgeDocuments(Document doc, Entrant entrant);

    public void makeCache();

    public void clearCache();

    public String getEgeSertificateId(Entrant entrant, String subjectID, String value);

}
