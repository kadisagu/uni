package ru.tandemservice.fisrmc.dao.dataForDelete;

import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import ru.tandemservice.fisrmc.entity.FisPackages;

import java.util.Date;


public interface IDataForDeleteDAO {

    String BEAN_NAME = IDataForDeleteDAO.class.getName();
    SyncDaemon DAEMON = new SyncDaemon(BEAN_NAME, 1000 * 60 * 24) {
        @Override
        protected void main() {
            if (!DaemonParamsDeletePkg.NEED_RUN)
                return;

            IDataForDeleteDAO dao = (IDataForDeleteDAO) ApplicationRuntime.getBean(BEAN_NAME);

            try {
                dao.setMsgLog("Начали работу");
                FisPackages pkg = dao.process();
                dao.setMsgLog("Упаковка завершена " + new Date());
            }
            catch (Exception ex) {
                dao.setMsgLog("Ошибка упаковки " + ex.getMessage());
                dao.saveDocument(null);
            }

            DaemonParamsDeletePkg.NEED_RUN = false;
        }

    };

    void setParamns(DaemonParamsDeletePkg params);

    String getMsgLog();

    void setMsgLog(String msg);

    @Transactional
    FisPackages process() throws Exception;

    FisPackages saveDocument(byte[] content);
}
