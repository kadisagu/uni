package ru.tandemservice.fisrmc.component.UploadFis2TU.Pub;

import org.tandemframework.core.component.State;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.fisrmc.entity.FisExportFileAction;
import ru.tandemservice.fisrmc.entity.FisExportFiles;

@State(keys = {"publisherId"}, bindings = {"entity.id"})
public class Model {

    private FisExportFiles entity = new FisExportFiles();
    private DynamicListDataSource<FisExportFileAction> dataSource;
    private IDataSettings settings;

    public FisExportFiles getEntity() {
        return entity;
    }

    public void setEntity(FisExportFiles entity) {
        this.entity = entity;
    }

    public DynamicListDataSource<FisExportFileAction> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<FisExportFileAction> dataSource) {
        this.dataSource = dataSource;
    }

    public String getDate() {
        return RussianDateFormatUtils.STANDARD_DATE_TIME_FORMAT.format(getEntity().getDate());
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public IDataSettings getSettings() {
        return settings;
    }


    public void setTestMode(boolean testMode)
    {
        this.getSettings().set("testMode", testMode);
    }

    public boolean getTestMode()
    {
        if (this.getSettings().get("testMode") == null) {
            this.getSettings().set("testMode", Boolean.FALSE);
        }
        return (Boolean) this.getSettings().get("testMode");
    }


}
