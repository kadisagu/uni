package ru.tandemservice.fisrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.fisrmc.entity.FisCheckEntrantRequest;
import ru.tandemservice.fisrmc.entity.catalog.FisAppStatusCode;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проверка заявлений по ответу ФИС
 *
 * Если заявление загружено без ошибок, то обязательно наличие кода fisAppStatusCode=1 'Проверено, ошибок нет'
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FisCheckEntrantRequestGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.fisrmc.entity.FisCheckEntrantRequest";
    public static final String ENTITY_NAME = "fisCheckEntrantRequest";
    public static final int VERSION_HASH = 1384910843;
    private static IEntityMeta ENTITY_META;

    public static final String P_CHECK_DATE = "checkDate";
    public static final String L_ENTRANT_REQUEST = "entrantRequest";
    public static final String P_ERROR_CODE = "errorCode";
    public static final String P_ERROR_MESSAGE = "errorMessage";
    public static final String L_FIS_APP_STATUS_CODE = "fisAppStatusCode";

    private Date _checkDate;     // Дата проверки
    private EntrantRequest _entrantRequest;     // Заявление абитуриента
    private Integer _errorCode;     // код ошибки из фис
    private String _errorMessage;     // Описание ошибки из фис
    private FisAppStatusCode _fisAppStatusCode;     // Статус проверки заявления

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата проверки. Свойство не может быть null.
     */
    @NotNull
    public Date getCheckDate()
    {
        return _checkDate;
    }

    /**
     * @param checkDate Дата проверки. Свойство не может быть null.
     */
    public void setCheckDate(Date checkDate)
    {
        dirty(_checkDate, checkDate);
        _checkDate = checkDate;
    }

    /**
     * @return Заявление абитуриента. Свойство не может быть null.
     */
    @NotNull
    public EntrantRequest getEntrantRequest()
    {
        return _entrantRequest;
    }

    /**
     * @param entrantRequest Заявление абитуриента. Свойство не может быть null.
     */
    public void setEntrantRequest(EntrantRequest entrantRequest)
    {
        dirty(_entrantRequest, entrantRequest);
        _entrantRequest = entrantRequest;
    }

    /**
     * @return код ошибки из фис.
     */
    public Integer getErrorCode()
    {
        return _errorCode;
    }

    /**
     * @param errorCode код ошибки из фис.
     */
    public void setErrorCode(Integer errorCode)
    {
        dirty(_errorCode, errorCode);
        _errorCode = errorCode;
    }

    /**
     * @return Описание ошибки из фис.
     */
    @Length(max=255)
    public String getErrorMessage()
    {
        return _errorMessage;
    }

    /**
     * @param errorMessage Описание ошибки из фис.
     */
    public void setErrorMessage(String errorMessage)
    {
        dirty(_errorMessage, errorMessage);
        _errorMessage = errorMessage;
    }

    /**
     * @return Статус проверки заявления. Свойство не может быть null.
     */
    @NotNull
    public FisAppStatusCode getFisAppStatusCode()
    {
        return _fisAppStatusCode;
    }

    /**
     * @param fisAppStatusCode Статус проверки заявления. Свойство не может быть null.
     */
    public void setFisAppStatusCode(FisAppStatusCode fisAppStatusCode)
    {
        dirty(_fisAppStatusCode, fisAppStatusCode);
        _fisAppStatusCode = fisAppStatusCode;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FisCheckEntrantRequestGen)
        {
            setCheckDate(((FisCheckEntrantRequest)another).getCheckDate());
            setEntrantRequest(((FisCheckEntrantRequest)another).getEntrantRequest());
            setErrorCode(((FisCheckEntrantRequest)another).getErrorCode());
            setErrorMessage(((FisCheckEntrantRequest)another).getErrorMessage());
            setFisAppStatusCode(((FisCheckEntrantRequest)another).getFisAppStatusCode());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FisCheckEntrantRequestGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FisCheckEntrantRequest.class;
        }

        public T newInstance()
        {
            return (T) new FisCheckEntrantRequest();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "checkDate":
                    return obj.getCheckDate();
                case "entrantRequest":
                    return obj.getEntrantRequest();
                case "errorCode":
                    return obj.getErrorCode();
                case "errorMessage":
                    return obj.getErrorMessage();
                case "fisAppStatusCode":
                    return obj.getFisAppStatusCode();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "checkDate":
                    obj.setCheckDate((Date) value);
                    return;
                case "entrantRequest":
                    obj.setEntrantRequest((EntrantRequest) value);
                    return;
                case "errorCode":
                    obj.setErrorCode((Integer) value);
                    return;
                case "errorMessage":
                    obj.setErrorMessage((String) value);
                    return;
                case "fisAppStatusCode":
                    obj.setFisAppStatusCode((FisAppStatusCode) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "checkDate":
                        return true;
                case "entrantRequest":
                        return true;
                case "errorCode":
                        return true;
                case "errorMessage":
                        return true;
                case "fisAppStatusCode":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "checkDate":
                    return true;
                case "entrantRequest":
                    return true;
                case "errorCode":
                    return true;
                case "errorMessage":
                    return true;
                case "fisAppStatusCode":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "checkDate":
                    return Date.class;
                case "entrantRequest":
                    return EntrantRequest.class;
                case "errorCode":
                    return Integer.class;
                case "errorMessage":
                    return String.class;
                case "fisAppStatusCode":
                    return FisAppStatusCode.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FisCheckEntrantRequest> _dslPath = new Path<FisCheckEntrantRequest>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FisCheckEntrantRequest");
    }
            

    /**
     * @return Дата проверки. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisCheckEntrantRequest#getCheckDate()
     */
    public static PropertyPath<Date> checkDate()
    {
        return _dslPath.checkDate();
    }

    /**
     * @return Заявление абитуриента. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisCheckEntrantRequest#getEntrantRequest()
     */
    public static EntrantRequest.Path<EntrantRequest> entrantRequest()
    {
        return _dslPath.entrantRequest();
    }

    /**
     * @return код ошибки из фис.
     * @see ru.tandemservice.fisrmc.entity.FisCheckEntrantRequest#getErrorCode()
     */
    public static PropertyPath<Integer> errorCode()
    {
        return _dslPath.errorCode();
    }

    /**
     * @return Описание ошибки из фис.
     * @see ru.tandemservice.fisrmc.entity.FisCheckEntrantRequest#getErrorMessage()
     */
    public static PropertyPath<String> errorMessage()
    {
        return _dslPath.errorMessage();
    }

    /**
     * @return Статус проверки заявления. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisCheckEntrantRequest#getFisAppStatusCode()
     */
    public static FisAppStatusCode.Path<FisAppStatusCode> fisAppStatusCode()
    {
        return _dslPath.fisAppStatusCode();
    }

    public static class Path<E extends FisCheckEntrantRequest> extends EntityPath<E>
    {
        private PropertyPath<Date> _checkDate;
        private EntrantRequest.Path<EntrantRequest> _entrantRequest;
        private PropertyPath<Integer> _errorCode;
        private PropertyPath<String> _errorMessage;
        private FisAppStatusCode.Path<FisAppStatusCode> _fisAppStatusCode;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата проверки. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisCheckEntrantRequest#getCheckDate()
     */
        public PropertyPath<Date> checkDate()
        {
            if(_checkDate == null )
                _checkDate = new PropertyPath<Date>(FisCheckEntrantRequestGen.P_CHECK_DATE, this);
            return _checkDate;
        }

    /**
     * @return Заявление абитуриента. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisCheckEntrantRequest#getEntrantRequest()
     */
        public EntrantRequest.Path<EntrantRequest> entrantRequest()
        {
            if(_entrantRequest == null )
                _entrantRequest = new EntrantRequest.Path<EntrantRequest>(L_ENTRANT_REQUEST, this);
            return _entrantRequest;
        }

    /**
     * @return код ошибки из фис.
     * @see ru.tandemservice.fisrmc.entity.FisCheckEntrantRequest#getErrorCode()
     */
        public PropertyPath<Integer> errorCode()
        {
            if(_errorCode == null )
                _errorCode = new PropertyPath<Integer>(FisCheckEntrantRequestGen.P_ERROR_CODE, this);
            return _errorCode;
        }

    /**
     * @return Описание ошибки из фис.
     * @see ru.tandemservice.fisrmc.entity.FisCheckEntrantRequest#getErrorMessage()
     */
        public PropertyPath<String> errorMessage()
        {
            if(_errorMessage == null )
                _errorMessage = new PropertyPath<String>(FisCheckEntrantRequestGen.P_ERROR_MESSAGE, this);
            return _errorMessage;
        }

    /**
     * @return Статус проверки заявления. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.FisCheckEntrantRequest#getFisAppStatusCode()
     */
        public FisAppStatusCode.Path<FisAppStatusCode> fisAppStatusCode()
        {
            if(_fisAppStatusCode == null )
                _fisAppStatusCode = new FisAppStatusCode.Path<FisAppStatusCode>(L_FIS_APP_STATUS_CODE, this);
            return _fisAppStatusCode;
        }

        public Class getEntityClass()
        {
            return FisCheckEntrantRequest.class;
        }

        public String getEntityName()
        {
            return "fisCheckEntrantRequest";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
