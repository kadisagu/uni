package ru.tandemservice.fisrmc.component.settings.OlympiadDiploma2DiplomaFis;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.OlympiadDiploma;

import java.util.List;

public class Model {


    private DynamicListDataSource<OlympiadDiploma> dataSource;

    private List settingsValueList;
    private IMultiSelectModel catalogValueList;

    private IDataSettings settings;


    private ISelectModel enrollmentCampaignModel;
    private EnrollmentCampaign enrollmentCampaign;

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public DynamicListDataSource<OlympiadDiploma> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<OlympiadDiploma> dataSource) {
        this.dataSource = dataSource;
    }


    public IMultiSelectModel getCatalogValueList() {
        return catalogValueList;
    }

    public void setCatalogValueList(IMultiSelectModel catalogValueList) {
        this.catalogValueList = catalogValueList;
    }

    public List getSettingsValueList() {
        return settingsValueList;
    }

    public void setSettingsValueList(List settingsValueList) {
        this.settingsValueList = settingsValueList;
    }

    public ISelectModel getEnrollmentCampaignModel() {
        return enrollmentCampaignModel;
    }

    public void setEnrollmentCampaignModel(ISelectModel enrollmentCampaignModel) {
        this.enrollmentCampaignModel = enrollmentCampaignModel;
    }

    public EnrollmentCampaign getEnrollmentCampaign() {
        return enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign) {
        this.enrollmentCampaign = enrollmentCampaign;
    }


}
