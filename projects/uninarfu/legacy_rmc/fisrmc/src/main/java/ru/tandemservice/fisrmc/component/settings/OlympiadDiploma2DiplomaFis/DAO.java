package ru.tandemservice.fisrmc.component.settings.OlympiadDiploma2DiplomaFis;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.fisrmc.entity.OlympiadDiploma2DiplomaFis;
import ru.tandemservice.fisrmc.entity.catalog.Olimpiatfis;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.OlympiadDiploma;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;


public class DAO extends UniDao<Model> implements IDAO {


    @Override
    public void prepare(final Model model)
    {
        model.setEnrollmentCampaignModel(new LazySimpleSelectModel<>(EnrollmentCampaign.class));

        model.setCatalogValueList(new DQLFullCheckSelectModel() {

            @Override
            protected DQLSelectBuilder query(String alias, String filter) {

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Olimpiatfis.class, alias);

                FilterUtils.applySimpleLikeFilter(builder, alias, Olimpiatfis.title(), filter);

                return builder;
            }
        });
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        prepareListData(model);
        DynamicListDataSource<OlympiadDiploma> dataSource = model.getDataSource();

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(OlympiadDiploma.class, "ol")
                .joinEntity("ol", DQLJoinType.left, OlympiadDiploma2DiplomaFis.class, "rel", DQLExpressions.eq(DQLExpressions.property("rel", OlympiadDiploma2DiplomaFis.olympiadDiploma()), DQLExpressions.property("ol")))
                .column("ol");
        // builder.setPredicate(DQLPredicateType.distinct);

        Boolean hasRelation = model.getSettings().get("hasRelation") == null ? false : (Boolean) model.getSettings().get("hasRelation");

        // фильтруем только для позиции - сопоставлено
        if (hasRelation)
            builder.where(DQLExpressions.isNotNull(DQLExpressions.property(OlympiadDiploma2DiplomaFis.id().fromAlias("rel"))));

        FilterUtils.applySelectFilter(builder, "ol", OlympiadDiploma.entrant().enrollmentCampaign(), model.getEnrollmentCampaign());

        // сортировка
        DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(OlympiadDiploma.class, "ol");
        orderRegistry.setOrders("title", new OrderDescription("ol", OlympiadDiploma.P_OLYMPIAD));

        orderRegistry.applyOrder(builder, dataSource.getEntityOrder());

        UniBaseUtils.createPage(dataSource, builder, getSession());

        List<ViewWrapper<OlympiadDiploma>> patchedList = ViewWrapper.getPatchedList(dataSource);

        Map<Long, Olimpiatfis> hasRelated = getSettingsId2ValueMap();

        for (ViewWrapper<OlympiadDiploma> wrapper : patchedList) {
            OlympiadDiploma olimpiad = wrapper.getEntity();


            if (hasRelated.containsKey(olimpiad.getId()))
                wrapper.setViewProperty("hasRelation", true);
            else
                wrapper.setViewProperty("hasRelation", false);

            String title = "";
            title += olimpiad.getOlympiad();

            if (olimpiad.getSubject() != null)
                title += " (" + olimpiad.getSubject() + ") ";

            if (olimpiad.getEntrant() != null)
                title += " Аб: " + olimpiad.getEntrant().getPersonalNumber();

            wrapper.setViewProperty("title", title);
        }
    }

    @SuppressWarnings("unchecked")
    private void prepareListData(Model model)
    {
        ((BlockColumn) model.getDataSource().getColumn("catalogValue")).setValueMap(getSettingsId2ValueMap());
    }

    private Map<Long, Olimpiatfis> getSettingsId2ValueMap()
    {
        DQLSelectBuilder builder1 = new DQLSelectBuilder()
                .fromEntity(OlympiadDiploma2DiplomaFis.class, "rel")
                .column(DQLExpressions.property(OlympiadDiploma2DiplomaFis.olympiadDiploma().id().fromAlias("rel")))
                .column(DQLExpressions.property(OlympiadDiploma2DiplomaFis.olimpiatfis().fromAlias("rel")));

        List<Object[]> data = builder1.createStatement(getSession()).list();

        Map<Long, Olimpiatfis> result = new HashMap<>();
        for (Object[] o : data) {

            Long id = (Long) o[0];
            Olimpiatfis diploma = (Olimpiatfis) o[1];

            if (!result.containsKey(id))
                result.put(id, diploma);
            //((ArrayList)SafeMap.safeGet(result, id, ArrayList.class)).add(diploma);
        }
        return result;
    }


    @SuppressWarnings("unchecked")
    @Override
    public void update(Model model)
    {
        DynamicListDataSource dataSource = model.getDataSource();
        Map<Long, Olimpiatfis> settingsId2ValueMap = ((BlockColumn) dataSource.getColumn("catalogValue")).getValueMap();

        for (Entry<Long, Olimpiatfis> set : settingsId2ValueMap.entrySet()) {
            OlympiadDiploma key = get(OlympiadDiploma.class, set.getKey());

            //Удалим старые настройки
            new DQLDeleteBuilder(OlympiadDiploma2DiplomaFis.class)
                    .where(DQLExpressions.eq(DQLExpressions.property(OlympiadDiploma2DiplomaFis.olympiadDiploma()), DQLExpressions.value(key)))
                    .createStatement(getSession())
                    .execute();

            Olimpiatfis val = set.getValue();

            if (val != null) {
                //Запишем новые
                OlympiadDiploma2DiplomaFis rel = new OlympiadDiploma2DiplomaFis();
                rel.setOlimpiatfis(val);
                rel.setOlympiadDiploma(key);
                saveOrUpdate(rel);
            }
        }

    }

}
