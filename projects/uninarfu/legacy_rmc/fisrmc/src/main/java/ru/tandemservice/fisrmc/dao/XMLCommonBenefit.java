package ru.tandemservice.fisrmc.dao;

import ru.tandemservice.fisrmc.entity.catalog.Competitionkindfis;
import ru.tandemservice.fisrmc.entity.catalog.Olimpiaddiplomatypefis;

import java.util.List;

/**
 * предоставление общей льготы
 * (для олимпиадников)
 *
 * @author vch
 */
public class XMLCommonBenefit {

    /**
     * Типы дипломов участников олимпиады
     */
    private List<Olimpiaddiplomatypefis> olimpicDiplomTypes;

    /**
     * вид льготы
     */
    private Competitionkindfis competitionkindfis;

    /**
     * флаг действия льготы для всех олимпиад
     */
    private boolean isForAllOlympics = true;

    private XMLCompetitiveGroup competitiveGroup = null;

    private String startKeyPart = null;

    public XMLCommonBenefit
            (
                    XMLCompetitiveGroup competitiveGroup
                    , Competitionkindfis competitionkindfis
                    , String startKeyPart

            )
    {
        this.setCompetitionkindfis(competitionkindfis);
        this.setCompetitiveGroup(competitiveGroup);
        this.setStartKeyPart(startKeyPart);

    }

    public String getUid()
    {
        // как по льготе считать uid никто пока не знает
        // посему пока вычисляем
        String key = competitionkindfis.getCode();

        if (startKeyPart != null)
            key += startKeyPart;

        if (olimpicDiplomTypes != null)
            for (Olimpiaddiplomatypefis td : olimpicDiplomTypes) {
                key += td.getCode();
            }

        if (getCompetitiveGroup() != null)
            key += getCompetitiveGroup().getUid();

        return Integer.toString(key.hashCode());
    }

    public void setForAllOlympics(boolean isForAllOlympics) {
        this.isForAllOlympics = isForAllOlympics;
    }


    public boolean isForAllOlympics() {
        return isForAllOlympics;
    }


    public void setCompetitionkindfis(Competitionkindfis competitionkindfis) {
        this.competitionkindfis = competitionkindfis;
    }


    public Competitionkindfis getCompetitionkindfis() {
        return competitionkindfis;
    }


    public void setOlimpicDiplomTypes(List<Olimpiaddiplomatypefis> olimpicDiplomTypes) {
        this.olimpicDiplomTypes = olimpicDiplomTypes;
    }


    public List<Olimpiaddiplomatypefis> getOlimpicDiplomTypes() {
        return olimpicDiplomTypes;
    }

    public void setCompetitiveGroup(XMLCompetitiveGroup competitiveGroup) {
        this.competitiveGroup = competitiveGroup;
    }

    public XMLCompetitiveGroup getCompetitiveGroup() {
        return competitiveGroup;
    }

    public String getStartKeyPart() {
        return startKeyPart;
    }

    public void setStartKeyPart(String startKeyPart) {
        this.startKeyPart = startKeyPart;
    }
}
