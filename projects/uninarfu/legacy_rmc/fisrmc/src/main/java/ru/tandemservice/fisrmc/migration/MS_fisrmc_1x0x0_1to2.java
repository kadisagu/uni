package ru.tandemservice.fisrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_fisrmc_1x0x0_1to2 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.10"),
                        new ScriptDependency("org.tandemframework.shared", "1.1.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.1.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrollmentCampaignStageFIS

        // создана новая сущность
        if (!tool.tableExists("enrollmentcampaignstagefis_t")) {
            // создать таблицу
            DBTable dbt = new DBTable("enrollmentcampaignstagefis_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("enrollmentcampaign_id", DBType.LONG).setNullable(false),
                                      new DBColumn("stage_id", DBType.LONG).setNullable(false),
                                      new DBColumn("dateorder_p", DBType.DATE).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("enrollmentCampaignStageFIS");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность stageFis

        // создана новая сущность
        if (!tool.tableExists("stagefis_t")) {
            // создать таблицу
            DBTable dbt = new DBTable("stagefis_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("title_p", DBType.createVarchar(1200))
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("stageFis");

        }


    }
}