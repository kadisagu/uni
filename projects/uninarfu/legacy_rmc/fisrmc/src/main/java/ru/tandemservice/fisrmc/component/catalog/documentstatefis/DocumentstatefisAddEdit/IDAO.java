package ru.tandemservice.fisrmc.component.catalog.documentstatefis.DocumentstatefisAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.fisrmc.entity.catalog.Documentstatefis;

public interface IDAO extends IDefaultCatalogAddEditDAO<Documentstatefis, Model> {

}
