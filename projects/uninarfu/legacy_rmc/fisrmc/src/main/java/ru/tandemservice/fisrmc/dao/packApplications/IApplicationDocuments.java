package ru.tandemservice.fisrmc.dao.packApplications;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;

public interface IApplicationDocuments {

    /**
     * упаковать документы абитуриента
     * приложенные к заявлению
     *
     * @param doc
     * @param entrantrequest
     *
     * @return
     */
    public Element PackApplicationDocuments
    (
            Document doc
            , EntrantRequest entrantrequest
    );


}
