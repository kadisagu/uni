package ru.tandemservice.fisrmc.component.TestWeb;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.fisrmc.entity.FisPackages;

public class Model {
    private String request;
    private String response;

    //	private boolean admissionInfo=false;
//	private boolean applications=false;
//	private boolean ordersOfAdmission=false;
//
//	private boolean testApplicationMode = true;
//	private boolean suspendErrorDoc = true;
//	private boolean abfuscateData = true;
//
//	private boolean notRfMode = false;
//
//	private boolean suspendCompetitiveGroup = true;
//
//	private int cgCount=1000;
//	private int startAbit = 1;
//	private int abitCount=2000;

    private ISelectModel enrollmentCampaignModel;
    private ISelectModel spravTypeModel;

    private IDataSettings settings;
    private DynamicListDataSource<FisPackages> dataSource;

    public boolean isDeletePackageType() {
        return false;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getRequest() {
        return request;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getResponse() {
        return response;
    }

    public void setCampaignInfo(boolean campaignInfo) {
        this.getSettings().set("campaignInfo", campaignInfo);
    }

    public boolean getCampaignInfo() {
        if (this.getSettings().get("campaignInfo") == null) {
            this.getSettings().set("campaignInfo", Boolean.FALSE);
        }
        return (Boolean) this.getSettings().get("campaignInfo");
    }

    public void setAdmissionInfo(boolean admissionInfo) {
        this.getSettings().set("admissionInfo", admissionInfo);
    }

    public boolean getAdmissionInfo() {
        if (this.getSettings().get("admissionInfo") == null) {
            this.getSettings().set("admissionInfo", Boolean.FALSE);
        }
        return (Boolean) this.getSettings().get("admissionInfo");
    }

    public void setApplications(boolean applications) {
        this.getSettings().set("applications", applications);
    }

    public boolean getApplications() {
        if (this.getSettings().get("applications") == null) {
            this.getSettings().set("applications", Boolean.FALSE);
        }
        return (Boolean) this.getSettings().get("applications");
    }

    public void setOrdersOfAdmission(boolean ordersOfAdmission) {
        this.getSettings().set("ordersOfAdmission", ordersOfAdmission);
    }

    public boolean getOrdersOfAdmission() {
        if (this.getSettings().get("ordersOfAdmission") == null) {
            this.getSettings().set("ordersOfAdmission", Boolean.FALSE);
        }
        return (Boolean) this.getSettings().get("ordersOfAdmission");
    }

    public void setEnrollmentCampaignModel(ISelectModel enrollmentCampaignModel) {
        this.enrollmentCampaignModel = enrollmentCampaignModel;
    }

    public ISelectModel getEnrollmentCampaignModel() {
        return enrollmentCampaignModel;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setTestApplicationMode(boolean testApplicationMode) {
        this.getSettings().set("testApplicationMode", testApplicationMode);
    }

    public boolean getTestApplicationMode() {
        if (this.getSettings().get("testApplicationMode") == null) {
            this.getSettings().set("testApplicationMode", Boolean.TRUE);
        }
        return (Boolean) this.getSettings().get("testApplicationMode");
    }

    public void setSuspendErrorDoc(boolean suspendErrorDoc) {
        this.getSettings().set("suspendErrorDoc", suspendErrorDoc);
    }

    public boolean getSuspendErrorDoc() {
        if (this.getSettings().get("suspendErrorDoc") == null) {
            this.getSettings().set("suspendErrorDoc", Boolean.TRUE);
        }
        return (Boolean) this.getSettings().get("suspendErrorDoc");
    }

    public void setAbfuscateData(boolean abfuscateData) {
        this.getSettings().set("abfuscateData", abfuscateData);
    }

    public boolean getAbfuscateData() {
        if (this.getSettings().get("abfuscateData") == null) {
            this.getSettings().set("abfuscateData", Boolean.TRUE);
        }
        return (Boolean) this.getSettings().get("abfuscateData");
    }

    public void setCgCount(int cgCount) {
        this.getSettings().set("cgCount", cgCount);
    }

    public int getCgCount() {
        if (this.getSettings().get("cgCount") == null) {
            this.getSettings().set("cgCount", 1000);
        }
        return (Integer) this.getSettings().get("cgCount");
    }

    public void setAbitCount(int abitCount) {
        this.getSettings().set("abitCount", abitCount);
    }

    public int getAbitCount() {
        if (this.getSettings().get("abitCount") == null) {
            this.getSettings().set("abitCount", 2000);
        }
        return (Integer) this.getSettings().get("abitCount");
    }

    public void setFisUserName(String fisUserName) {
        getSettings().set("fisUserName", fisUserName);
    }

    public String getFisUserName() {

        if (getSettings().get("fisUserName") == null)
            getSettings().set("fisUserName", "userName");
        return (String) getSettings().get("fisUserName");
    }

    public void setFisPwd(String fisPwd) {
        getSettings().set("fisPwd", fisPwd);
    }

    public String getFisPwd() {
        if (getSettings().get("fisPwd") == null)
            getSettings().set("fisPwd", "pwd");
        return (String) getSettings().get("fisPwd");
    }


    public void setBenefitRequest(boolean benefitRequest) {
        this.getSettings().set("benefitRequest", benefitRequest);
    }

    public boolean getBenefitRequest() {
        if (this.getSettings().get("benefitRequest") == null) {
            this.getSettings().set("benefitRequest", Boolean.FALSE);
        }
        return (Boolean) this.getSettings().get("benefitRequest");
    }
    // benefitRequest

    public void setNotRfMode(boolean notRfMode) {
        this.getSettings().set("notRfMode", notRfMode);
    }

    public boolean getNotRfMode() {
        if (this.getSettings().get("notRfMode") == null) {
            this.getSettings().set("notRfMode", Boolean.FALSE);
        }
        return (Boolean) this.getSettings().get("notRfMode");
    }

    public void setSuspendCompetitiveGroup(boolean suspendCompetitiveGroup) {
        this.getSettings().set("suspendCompetitiveGroup", suspendCompetitiveGroup);
    }

    public boolean getSuspendCompetitiveGroup() {
        if (this.getSettings().get("suspendCompetitiveGroup") == null) {
            this.getSettings().set("suspendCompetitiveGroup", Boolean.TRUE);
        }
        return (Boolean) this.getSettings().get("suspendCompetitiveGroup");
    }

    public void setStartAbit(int startAbit) {
        this.getSettings().set("startAbit", startAbit);
    }

    public int getStartAbit() {
        if (this.getSettings().get("startAbit") == null) {
            this.getSettings().set("startAbit", 1);
        }
        return (Integer) this.getSettings().get("startAbit");
    }

    public DynamicListDataSource<FisPackages> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<FisPackages> dataSource) {
        this.dataSource = dataSource;
    }

    public boolean getEntranceTestResult() {
        if (this.getSettings().get("entranceTestResult") == null) {
            this.getSettings().set("entranceTestResult", Boolean.TRUE);
        }
        return (Boolean) getSettings().get("entranceTestResult");
    }

    public void setEntranceTestResult(boolean entranceTestResult) {
        this.getSettings().set("entranceTestResult", entranceTestResult);
    }

    public void setRegNumber(String regNumber) {
        getSettings().set("regNumber", regNumber);
    }

    public String getRegNumber() {
        if (getSettings().get("regNumber") == null)
            getSettings().set("regNumber", "");
        return (String) getSettings().get("regNumber");
    }

    public void setExcludeRegNumber(String regNumber) {
        getSettings().set("excludeRegNumber", regNumber);
    }

    public String getExcludeRegNumber() {
        if (getSettings().get("excludeRegNumber") == null)
            getSettings().set("excludeRegNumber", "");
        return (String) getSettings().get("excludeRegNumber");
    }

//	public void setAppDateWithoutTime(boolean appDateWithoutTime) {
//	        this.getSettings().set("appDateWithoutTime", appDateWithoutTime);
//	    }

    public boolean getAppDateWithoutTime()
    {
//	        if (this.getSettings().get("appDateWithoutTime") == null) {
//	            this.getSettings().set("appDateWithoutTime", Boolean.FALSE);
//	        }
//	        return (Boolean) this.getSettings().get("appDateWithoutTime");
//	        
        return true;
    }


    public void setWhithoutSendedInFis(boolean whithoutSendedInFis) {
        this.getSettings().set("whithoutSendedInFis", whithoutSendedInFis);
    }


    public boolean getWhithoutSendedInFis() {
        if (this.getSettings().get("whithoutSendedInFis") == null) {
            this.getSettings().set("whithoutSendedInFis", Boolean.FALSE);
        }
        return (Boolean) this.getSettings().get("whithoutSendedInFis");
    }


    public void setWhithoutEntrantInOrder(boolean whithoutEntrantInOrder) {
        this.getSettings().set("whithoutEntrantInOrder", whithoutEntrantInOrder);
    }


    public boolean getWhithoutEntrantInOrder() {
        if (this.getSettings().get("whithoutEntrantInOrder") == null) {
            this.getSettings().set("whithoutEntrantInOrder", Boolean.FALSE);
        }
        return (Boolean) this.getSettings().get("whithoutEntrantInOrder");
    }

    public ISelectModel getSpravTypeModel() {
        return spravTypeModel;
    }

    public void setSpravTypeModel(ISelectModel spravTypeModel) {
        this.spravTypeModel = spravTypeModel;
    }


}
