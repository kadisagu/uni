package ru.tandemservice.fisrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import ru.tandemservice.fisrmc.entity.Olimpiatfis2ecdisciplinefis;

import java.sql.ResultSet;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_fisrmc_1x0x0_23to24 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.5.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.5.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность olimpiatfis2ecdisciplinefis

        // создано обязательное свойство ecolimpiadlevelfis
        {
            // создать колонку
            tool.createColumn("olimpiatfis2ecdisciplinefis_t", new DBColumn("ecolimpiadlevelfis_id", DBType.LONG));


            // для каждой строки нужно вычитать тип олимпиады

            String tableName = EntityRuntime.getMeta(Olimpiatfis2ecdisciplinefis.class).getTableName();
            ResultSet rs = tool.getConnection().createStatement().executeQuery(
                    "select " +
                            " olimpiatfis2ecdisciplinefis_t.id " +
                            " , olimpiatfis_t.ecolimpiadlevelfis_id " +
                            " from " +
                            " olimpiatfis2ecdisciplinefis_t " +
                            " inner join olimpiatfis_t on olimpiatfis2ecdisciplinefis_t.olimpiatfis_id = olimpiatfis_t.id "
            );


            while (rs.next()) {
                Long defaultOlimpiadId = null;
                Long relId = null;

                relId = rs.getLong(1);
                defaultOlimpiadId = rs.getLong(2);

                tool.executeUpdate("update olimpiatfis2ecdisciplinefis_t set ecolimpiadlevelfis_id=? where id=?", defaultOlimpiadId, relId);
            }


            // сделать колонку NOT NULL
            tool.setColumnNullable("olimpiatfis2ecdisciplinefis_t", "ecolimpiadlevelfis_id", false);
        }

    }
}