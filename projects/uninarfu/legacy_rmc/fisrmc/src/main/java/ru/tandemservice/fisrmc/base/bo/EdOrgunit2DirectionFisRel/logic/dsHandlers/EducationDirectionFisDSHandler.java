package ru.tandemservice.fisrmc.base.bo.EdOrgunit2DirectionFisRel.logic.dsHandlers;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.fisrmc.entity.Educationdirectionfis;
import ru.tandemservice.uni.util.FilterUtils;

import java.util.List;

public class EducationDirectionFisDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput> {

    public EducationDirectionFisDSHandler(String ownerId)
    {
        super(ownerId);
    }


    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {


        Object qualificationsFilter = context.get("qualificationsFilter");
        // Object oksoCodeFilter = context.get("oksoCodeFilter");
        String eduProgCodeFilter = context.get("eduProgCodeFilter");
        // Object eduProgCodeSpecFilter = context.get("eduProgCodeSpecFilter");

        Long spravTypeFilterID = context.get("spravTypeFilterID");


        DQLSelectBuilder directionFisBuilder = new DQLSelectBuilder();
        directionFisBuilder.fromEntity(Educationdirectionfis.class, "ed")
                .column("ed");


        if (eduProgCodeFilter != null && eduProgCodeFilter != null) {
            String filter = ((String) eduProgCodeFilter).toUpperCase();

            directionFisBuilder.where(DQLExpressions.or
                    (
                            DQLExpressions.likeUpper(DQLExpressions.property(Educationdirectionfis.Codefis().fromAlias("ed")), DQLExpressions.value(filter))
                            , DQLExpressions.likeUpper(DQLExpressions.property(Educationdirectionfis.NewCode().fromAlias("ed")), DQLExpressions.value(filter))
                    ));
        }

        // тип справочника
        FilterUtils.applySelectFilter(directionFisBuilder, "ed", Educationdirectionfis.spravType().id(), spravTypeFilterID);

        FilterUtils.applySelectFilter(directionFisBuilder, "ed", Educationdirectionfis.QualificationCode(), (String) qualificationsFilter);

        List<Educationdirectionfis> directionFis = directionFisBuilder.createStatement(context.getSession()).list();


        return ListOutputBuilder.get(input, directionFis).pageable(true).build();
    }

}
