package ru.tandemservice.fisrmc.component.dataChecking.FisAnswerAction;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.DateColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.fisrmc.entity.FisAnswerAction;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unibase.UniBaseUtils;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        model.setSettings(UniBaseUtils.getDataSettings(component, "FisAnswerAction.filter"));
        ((IDAO) getDao()).prepare(model);
        prepareDataSource(component);
    }

    protected void prepareDataSource(IBusinessComponent component) {
        final Model model = (Model) getModel(component);
        if (model.getDataSource() != null) return;
        DynamicListDataSource<FisAnswerAction> dataSource = new DynamicListDataSource<>(component, component1 -> {
            ((IDAO) Controller.this.getDao()).prepareListDataSource(model);
        });
        dataSource.addColumn(new DateColumn("Дата обработки", FisAnswerAction.date(), "dd.MM.yyyy HH:mm").setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Тип ответа ФИС", FisAnswerAction.fisAnswerTofisPackages().fisAnswerType().title()).setOrderable(true));

        dataSource.addColumn(new SimpleColumn("Приемные кампании", FisAnswerAction.fisAnswerTofisPackages().fisPackages().enrollmentCampaign()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Примечание выгрузки", FisAnswerAction.fisAnswerTofisPackages().fisPackages().description()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Ответ ФИС (примечание)", FisAnswerAction.fisAnswerTofisPackages().fisPrim()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("№ ФИС пакета", FisAnswerAction.fisAnswerTofisPackages().fisPackages().fisPkgNumber()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("№ ФИС (прим.)", FisAnswerAction.fisAnswerTofisPackages().fisPackages().fisPrim()).setOrderable(false).setClickable(false));


        dataSource.addColumn(new SimpleColumn("Комментарий", FisAnswerAction.comment()).setOrderable(false).setClickable(false));


        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickActionReportPrint", "Скачать отчет обработки"));
        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickErrorReportPrint", "Скачать ошибки"));

        model.setDataSource(dataSource);
    }

    public void onClickActionReportPrint(IBusinessComponent component) {
        FisAnswerAction action = UniDaoFacade.getCoreDao().get(FisAnswerAction.class, (Long) component.getListenerParameter());
        DatabaseFile file = action.getActionReport();
        if (file != null && file.getContent() != null) {
            Integer temporaryId = PrintReportTemporaryStorage.registerTemporaryPrintForm(file.getContent(), "ActionReport.rtf");
            activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT,
                                                             (new ParametersMap())
                                                                     .add("id", temporaryId)
                                                                     .add("zip", Boolean.FALSE)
                                                                     .add("extension", "rtf"))
            );
        }
        else {
            throw new ApplicationException("Отчет обработки пуст");
        }
    }

    public void onClickErrorReportPrint(IBusinessComponent component) {
        FisAnswerAction action = UniDaoFacade.getCoreDao().get(FisAnswerAction.class, (Long) component.getListenerParameter());
        DatabaseFile file = action.getErrorReport();
        if (file != null && file.getContent() != null) {
            Integer temporaryId = PrintReportTemporaryStorage.registerTemporaryPrintForm(file.getContent(), "ErrorReport.rtf");
            activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT,
                                                             (new ParametersMap())
                                                                     .add("id", temporaryId)
                                                                     .add("zip", Boolean.FALSE)
                                                                     .add("extension", "rtf"))
            );
        }
        else {
            throw new ApplicationException("Файл с ошибками пуст");
        }
    }

    public void onClickSearch(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        DataSettingsFacade.saveSettings(model.getSettings());
        model.getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent context)
    {
        Model model = (Model) getModel(context);
        model.getSettings().clear();
        DataSettingsFacade.saveSettings(model.getSettings());
        model.getDataSource().refresh();
    }
}
