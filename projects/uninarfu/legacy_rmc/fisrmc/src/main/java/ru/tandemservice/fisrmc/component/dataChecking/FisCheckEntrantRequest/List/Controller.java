package ru.tandemservice.fisrmc.component.dataChecking.FisCheckEntrantRequest.List;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.DateColumn;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.util.RtfUtil;
import ru.tandemservice.fisrmc.entity.FisCheckEntrantRequest;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        model.setSettings(component.getSettings());
        ((IDAO) getDao()).prepare(model);
        prepareDataSource(component);
    }

    protected void prepareDataSource(IBusinessComponent component) {
        final Model model = (Model) getModel(component);
        if (model.getDataSource() != null) return;
        DynamicListDataSource<FisCheckEntrantRequest> dataSource = new DynamicListDataSource<>(component, component1 -> {
            ((IDAO) Controller.this.getDao()).prepareListDataSource(model);
        });
        dataSource.addColumn(new DateColumn("Дата", FisCheckEntrantRequest.checkDate(), "dd.MM.yyyy HH:mm").setOrderable(true));
        dataSource.addColumn(new SimpleColumn("№ заявления", FisCheckEntrantRequest.entrantRequest().regNumber()).setClickable(false));

        PublisherLinkColumn linkColumn = new PublisherLinkColumn("Абитуриент", FisCheckEntrantRequest.entrantRequest().entrant().person().fullFio()); //.subTab("entrantRequestTab").build();

        linkColumn.setResolver(new IPublisherLinkResolver() {
            @Override
            public String getComponentName(IEntity ientity) {
                return "ru.tandemservice.uniec.component.entrant.EntrantPub";
            }

            @Override
            public Object getParameters(IEntity entity) {
                return ParametersMap.createWith(PublisherActivator.PUBLISHER_ID_KEY, ((FisCheckEntrantRequest) entity).getEntrantRequest().getEntrant().getId());
            }
        });

        dataSource.addColumn(linkColumn);
        dataSource.addColumn(new SimpleColumn("Статус проверки", FisCheckEntrantRequest.fisAppStatusCode().title()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Код ошибки", FisCheckEntrantRequest.errorCode()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Сообщение об ошибке", FisCheckEntrantRequest.errorMessage()).setClickable(false));

        model.setDataSource(dataSource);
    }

    public void onClickSearch(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        component.saveSettings();
        model.getDataSource().refresh();

    }

    public void onClickClear(IBusinessComponent context)
    {
        Model model = (Model) getModel(context);
        model.getSettings().clear();
        ((IDAO) getDao()).prepare(model);
        onClickSearch(context);
    }

    public void onClickPrint(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        RtfDocument document = ((IDAO) getDao()).createDocument(model);
        if (document != null) {
            Integer temporaryId = PrintReportTemporaryStorage.registerTemporaryPrintForm(RtfUtil.toByteArray(document), "CheckEntrantRequest.rtf");
            activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT,
                                                             new ParametersMap()
                                                                     .add("id", temporaryId)
                                                                     .add("zip", Boolean.FALSE)
                                                                     .add("extension", "rtf"))
            );
        }
    }

}
