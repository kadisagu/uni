package ru.tandemservice.fisrmc.component.UploadFis2TU.Pub;

import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import ru.tandemservice.fisrmc.dao.PackUnpackToFis;
import ru.tandemservice.fisrmc.dao.ToolsDao;
import ru.tandemservice.fisrmc.entity.FisExportFileAction;
import ru.tandemservice.fisrmc.entity.FisExportFiles;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;

import java.text.SimpleDateFormat;
import java.util.*;


public class EntrantRequestDateParser extends DefaultParser {
    public static final String ROOT = "InstitutionExport";
    public static final String APPLICATIONS = "Applications";
    public static final String APPLICATION = "Application";
    public static final String APPLICATION_UID = "UID";
    public static final String APPLICATION_NUMBER = "ApplicationNumber";
    public static final String APPLICATION_DATE = "RegistrationDate";

    public EntrantRequestDateParser(String xml) {
        super(xml);

        elementPath = ROOT + "-" + APPLICATIONS + "-" + APPLICATION;

        propertyMap.put(
                elementPath + "-" + APPLICATION_UID,
                "uid"
        );
        propertyMap.put(
                elementPath + "-" + APPLICATION_NUMBER,
                "number"
        );
        propertyMap.put(
                elementPath + "-" + APPLICATION_DATE,
                "date"
        );
    }

    public static void doProcess(FisExportFiles fisFile, boolean testMode)
    {
        IUniBaseDao dao = IUniBaseDao.instance.get();

        //загрузка заявлений из базы
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EntrantRequest.class, "e")
                .column(EntrantRequest.id().fromAlias("e").s())
                .column(EntrantRequest.regNumber().fromAlias("e").s())
                .column(EntrantRequest.regDate().fromAlias("e").s());

        List<Object[]> list = dao.getList(dql);
        Map<Long, RequestInfo> map = new HashMap<Long, RequestInfo>();
        for (Object[] arr : list) {
            Long id = (Long) arr[0];
            Integer number = (Integer) arr[1];
            Date date = (Date) arr[2];
            RequestInfo info = new RequestInfo(id, number, date);

            map.put(id, info);
        }

        try {
            String xml = new String(fisFile.getXmlPackage().getContent());
            List<String> errorList = new ArrayList<String>();
            List<String> infoList = new ArrayList<String>();
            doUpdate(map, xml, errorList, infoList, testMode);
            saveAction(fisFile, errorList, infoList, null, testMode);
        }
        catch (Exception ex) {
            saveAction(fisFile, null, null, ex, testMode);
            ex.printStackTrace();
        }
    }

    protected static void saveAction(
            FisExportFiles fisFile
            , List<String> errorList
            , List<String> infoList
            , Exception ex
            , boolean testMode
    )
    {
        IUniBaseDao dao = IUniBaseDao.instance.get();

        FisExportFileAction action = new FisExportFileAction();
        action.setFisExportFiles(fisFile);
        action.setDate(new Date());
        action.setAction("Сверить даты регистрации заявлений");
        action.setOk(ex == null && errorList.isEmpty());

        if (infoList == null)
            infoList = new ArrayList<String>();
        if (errorList == null)
            errorList = new ArrayList<String>();

        infoList.addAll(errorList);

        String dsk = "";
        if (testMode)
            dsk += "Тестовый режим, без внесения мзменений; ";


        if (!infoList.isEmpty()) {
            TemplateDocument template = dao.getCatalogItem(TemplateDocument.class, PackUnpackToFis.SUSPEND_ERRORS_TEMPLATE);
            RtfDocument resultRtf = new RtfReader().read(template.getContent());
            RtfTableModifier tableModifier = new RtfTableModifier();

            String[][] tableData = new String[infoList.size()][1];
            int idx = 0;
            for (String err : infoList) {
                tableData[idx][0] = err;
                idx++;
            }
            tableModifier.put("T", tableData);
            tableModifier.modify(resultRtf);

            DatabaseFile errorsFile = new DatabaseFile();
            errorsFile.setContent(RtfUtil.toByteArray(resultRtf));
            errorsFile.setFilename("errors");
            dao.saveOrUpdate(errorsFile);

            action.setLog(errorsFile);
        }
        else
            dsk += "Отчет пуст";


        action.setDescription(dsk);

        dao.saveOrUpdate(action);
    }

    @Transactional
    protected static void doUpdate(Map<Long, RequestInfo> map, String xml, List<String> errorList, List<String> infoList, boolean testMode) throws Exception {
        IUniBaseDao dao = IUniBaseDao.instance.get();

        List<Map<String, Object>> xmlList = DefaultParser.parse(new EntrantRequestDateParser(xml));
        for (Map<String, Object> xmlInfo : xmlList) {
            Long id = Long.parseLong((String) xmlInfo.get("uid"));
            Integer number = Integer.parseInt((String) xmlInfo.get("number"));

            if (!map.containsKey(id)) {
                errorList.add("Ошибка. Отсутствует заявление № = " + number + " id = " + id);
                continue;
            }

            Date date = null;
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
                date = sdf.parse((String) xmlInfo.get("date"));
            }
            catch (Exception ex) {
                ex.printStackTrace();
                errorList.add("Ошибка. Неправильная дата заявления № = " + number + "  id = " + id);
                continue;
            }

            EntrantRequestDateParser.RequestInfo info = map.get(id);

            if (!info.getNumber().equals(number)) {
                errorList.add("Ошибка. Не совпадает номер заявления № = " + number + "  id = " + id);
                continue;
            }

            if (
                    !ToolsDao.DateTimeToString(date).equals
                            (
                                    ToolsDao.DateTimeToString(info.getDate())
                            )
                    )
            {
                String msg = " Замена с " + ToolsDao.DateTimeToString(date) + " на " + ToolsDao.DateTimeToString(info.getDate());
                if (!testMode) {
                    EntrantRequest er = dao.getNotNull(id);
                    er.setRegDate(date);
                    dao.saveOrUpdate(er);
                }
                infoList.add("Обновлена дата заявления №=" + number + "  id=" + id + " " + msg);
            }
        }
    }

    public static class RequestInfo {
        private Long id;
        private Integer number;
        private Date date;

        public RequestInfo(Long id, Integer number, Date date) {
            this.id = id;
            this.number = number;
            this.date = date;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public Integer getNumber() {
            return number;
        }

        public void setNumber(Integer number) {
            this.number = number;
        }

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

    }
}
