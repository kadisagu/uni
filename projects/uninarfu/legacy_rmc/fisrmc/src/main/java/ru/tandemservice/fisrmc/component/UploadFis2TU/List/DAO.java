package ru.tandemservice.fisrmc.component.UploadFis2TU.List;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.fisrmc.entity.FisExportFiles;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.Date;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepareListDataSource(Model model) {
        Date dateFrom = model.getSettings().get("dateFrom");
        Date dateTo = model.getSettings().get("dateTo");

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(FisExportFiles.class, "e")
                .column("e");

        if (dateFrom != null)
            dql.where(DQLExpressions.ge(
                    DQLExpressions.property(FisExportFiles.date().fromAlias("e")),
                    DQLExpressions.valueDate(dateFrom)
            ));
        if (dateTo != null)
            dql.where(DQLExpressions.le(
                    DQLExpressions.property(FisExportFiles.date().fromAlias("e")),
                    DQLExpressions.valueDate(dateTo)
            ));


        DQLOrderDescriptionRegistry order = new DQLOrderDescriptionRegistry(FisExportFiles.class, "e");
        order.applyOrder(dql, model.getDataSource().getEntityOrder());

        UniBaseUtils.createPage(model.getDataSource(), dql, getSession());
    }
}
