package ru.tandemservice.fisrmc.component.settings.GroupEsForFis.List;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.fisrmc.entity.GroupEsForFis;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.List;

import static org.tandemframework.hibsupport.dao.CommonDAO.ids;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {

        Model model = (Model) getModel(component);
        prepareDataSource(component);
        model.setSettings(component.getSettings());
        ((IDAO) getDao()).prepare(model);
    }

    private void prepareDataSource(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        if (model.getDataSource() != null) {
            return;
        }
        DynamicListDataSource<GroupEsForFis> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Направление подготовки", GroupEsForFis.enrollmentDirection().title().s()));
        dataSource.addColumn(new SimpleColumn("Название группы", GroupEsForFis.groupName().s()).setClickable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey("editGroupEsForFis"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить группу?").setPermissionKey("deleteGroupEsForFis"));
        model.setDataSource(dataSource);
    }

    public void onClickSearch(IBusinessComponent component) {
        Model model = getModel(component);
        model.getDataSource().showFirstPage();
        model.getDataSource().refresh();
        DataSettingsFacade.saveSettings(model.getSettings());
    }

    public void onClickClear(IBusinessComponent component) {
        Model model = getModel(component);
        model.getSettings().clear();
        onClickSearch(component);
    }

    public void onClickAddGroup(IBusinessComponent component) {
        Model model = getModel(component);
        List<EnrollmentDirection> enrollmentDirectionList = model.getSettings().get("enrollmentDirectionList");
        ParametersMap map = new ParametersMap().add("directionIds", ids(enrollmentDirectionList))
                .add("enrollmentCampaignId", model.getEnrollmentCampaign().getId());
        component.createDefaultChildRegion(new ComponentActivator("ru.tandemservice.fisrmc.component.settings.GroupEsForFis.AddEdit", map));
    }

    public void onClickEditItem(IBusinessComponent component) {

        ParametersMap map = new ParametersMap().add("groupId", component.getListenerParameter());
        component.createDefaultChildRegion(new ComponentActivator("ru.tandemservice.fisrmc.component.settings.GroupEsForFis.AddEdit", map));

    }

    public void onClickDeleteItem(IBusinessComponent component) {
        UniDaoFacade.getCoreDao().delete((Long) component.getListenerParameter());
        getModel(component).getDataSource().refresh();

    }
}