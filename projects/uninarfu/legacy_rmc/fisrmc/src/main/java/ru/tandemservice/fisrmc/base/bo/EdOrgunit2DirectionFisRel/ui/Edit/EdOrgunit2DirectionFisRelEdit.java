package ru.tandemservice.fisrmc.base.bo.EdOrgunit2DirectionFisRel.ui.Edit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.column.PublisherDSColumn;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import ru.tandemservice.fisrmc.base.bo.EdOrgunit2DirectionFisRel.logic.dsHandlers.EducationDirectionFisDSHandler;
import ru.tandemservice.fisrmc.base.bo.EdOrgunit2DirectionFisRel.logic.dsHandlers.EnrollmentCampaignDSHandler;
import ru.tandemservice.fisrmc.base.bo.EdOrgunit2DirectionFisRel.logic.dsHandlers.QualificationsDSHandler;
import ru.tandemservice.fisrmc.entity.Educationdirectionfis;

@Configuration
public class EdOrgunit2DirectionFisRelEdit extends BusinessComponentManager {


    @Bean
    public ColumnListExtPoint edOrgunit2DirectionFisRelDS()
    {

        //TextDSColumn tc = textColumn("codeFIS", Educationdirectionfis.code()).order().create();
        PublisherDSColumn tc = publisherColumn("fisId", Educationdirectionfis.code()).order().create();

        return columnListExtPointBuilder("educationDirectionFisDS")

                .addColumn(tc)
                .addColumn(textColumn("fisCode", Educationdirectionfis.Codefis()).order().create())
                .addColumn(textColumn("fisNewCode", Educationdirectionfis.NewCode()).order().create())


                .addColumn(textColumn("qualificationCodeFIS", Educationdirectionfis.QualificationCode()).order().create())
                .addColumn(textColumn("developPeriodFIS", Educationdirectionfis.Period()).order().create())

                        // коменты к экспорту
                .addColumn(textColumn("exportComment", Educationdirectionfis.exportComment()).order().create())

                .addColumn(textColumn("directionTitleFIS", Educationdirectionfis.title()).order().create())

                .addColumn(actionColumn("relationToFIS", CommonDefines.ICON_EXECUTE, "relationToFIS").disabled(false).create())

                .create();
    }

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS("educationDirectionFisDS", edOrgunit2DirectionFisRelDS()).handler(educationDirectionFisDSHandler()))
                .addDataSource(selectDS("enrollmentCampaignDS", enrollmentCampaignDSHandler()))
                .addDataSource(selectDS("qualificationsDS", qualificationsDSHandler()))
                .create();
    }


    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> educationDirectionFisDSHandler()
    {
        return new EducationDirectionFisDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> enrollmentCampaignDSHandler()
    {
        return new EnrollmentCampaignDSHandler(getName());
    }


    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> qualificationsDSHandler()
    {
        return new QualificationsDSHandler(getName());
    }
}
