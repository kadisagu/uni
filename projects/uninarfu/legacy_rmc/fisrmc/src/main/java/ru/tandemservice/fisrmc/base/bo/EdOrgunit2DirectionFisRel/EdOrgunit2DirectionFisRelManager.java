package ru.tandemservice.fisrmc.base.bo.EdOrgunit2DirectionFisRel;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.fisrmc.base.bo.EdOrgunit2DirectionFisRel.logic.EdOrgunit2DirectionFisRelManagerModifyDAO;
import ru.tandemservice.fisrmc.base.bo.EdOrgunit2DirectionFisRel.logic.IEdOrgunit2DirectionFisRelManagerModifyDAO;

@Configuration
public class EdOrgunit2DirectionFisRelManager extends BusinessObjectManager {

    public static EdOrgunit2DirectionFisRelManager instance()
    {
        return instance(EdOrgunit2DirectionFisRelManager.class);
    }

    @Bean
    public IEdOrgunit2DirectionFisRelManagerModifyDAO modifyDao()
    {
        return new EdOrgunit2DirectionFisRelManagerModifyDAO();
    }
}
