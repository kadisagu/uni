package ru.tandemservice.fisrmc.component.settings.Entrantstate2entrantstateFis;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.fisrmc.entity.Entrantstate2entrantstateFis;
import ru.tandemservice.fisrmc.entity.catalog.Ecrequestfis;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.catalog.EntrantState;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DAO extends UniDao<Model> implements IDAO {


    @Override
    public void prepare(Model model)
    {
        model.setEntrantStateList(getCatalogItemList(EntrantState.class));
        model.setEcrequestfisList(getList(Ecrequestfis.class));
    }

    public void prepareListDataSource(Model model)
    {
        prepareListData(model);

        List states = model.getEntrantStateList();
        DynamicListDataSource dataSource = model.getDataSource();
        dataSource.setCountRow(states.size());
        UniBaseUtils.createPage(dataSource, states);
    }

    @SuppressWarnings("unchecked")
    private void prepareListData(Model model)
    {
        List<Entrantstate2entrantstateFis> states = getList(Entrantstate2entrantstateFis.class, Entrantstate2entrantstateFis.entrantState().title().s());

        model.setStates(states);

        ((BlockColumn) model.getDataSource().getColumn("ecrequestfis")).setValueMap(getId2IdMap(states));
    }

    @SuppressWarnings("unchecked")
    private Map<Long, Ecrequestfis> getId2IdMap(List<Entrantstate2entrantstateFis> disciplines)
    {


        List<Object[]> list = Collections.emptyList();
        if (!disciplines.isEmpty()) {
            Criteria criteria = getSession().createCriteria(Entrantstate2entrantstateFis.class);

            ProjectionList projections = Projections.projectionList();
            projections.add(Projections.property(Entrantstate2entrantstateFis.entrantState().id().s()));
            projections.add(Projections.property(Entrantstate2entrantstateFis.ecrequestfis().s()));

            criteria.setProjection(projections);
            list = criteria.list();
        }
        Map result = new HashMap();
        for (Object[] row : list) {
            result.put((Long) row[0], (Ecrequestfis) row[1]);
        }
        return result;
    }


    public void update(Model model)
    {
        DynamicListDataSource dataSource = model.getDataSource();
        Map Id2Entity = ((BlockColumn) dataSource.getColumn("ecrequestfis")).getValueMap();

        List<EntrantState> entrantStates;
        try {
            entrantStates = dataSource.getEntityList();
        }
        catch (Throwable e) {
            throw new RuntimeException(e);
        }

        Session session = getSession();

        for (EntrantState subject : entrantStates) {

            Ecrequestfis ecrequestfis = (Ecrequestfis) Id2Entity.get(subject.getId());

            Entrantstate2entrantstateFis rel = (Entrantstate2entrantstateFis) get(Entrantstate2entrantstateFis.class, Entrantstate2entrantstateFis.entrantState(), subject);

            if (ecrequestfis != null) {
                if (rel != null)
                    rel.setEcrequestfis(ecrequestfis);
                else {
                    rel = new Entrantstate2entrantstateFis();
                    rel.setEntrantState(subject);
                    rel.setEcrequestfis(ecrequestfis);
                }
                session.saveOrUpdate(rel);
            }
            else if (rel != null) {
                session.delete(rel);
            }
        }
    }


}
