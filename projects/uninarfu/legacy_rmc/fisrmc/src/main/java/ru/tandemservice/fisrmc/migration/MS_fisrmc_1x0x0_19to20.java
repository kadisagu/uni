package ru.tandemservice.fisrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;
import ru.tandemservice.fisrmc.entity.catalog.EducationdirectionSpravType;
import ru.tandemservice.fisrmc.entity.catalog.codes.EducationdirectionSpravTypeCodes;

import java.sql.ResultSet;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_fisrmc_1x0x0_19to20 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.5.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.5.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность educationdirectionfis

        // создано обязательное свойство NewCode
        {
            // создать колонку
            tool.createColumn("educationdirectionfis_t", new DBColumn("newcode_p", DBType.createVarchar(255)));
        }


        {
            // сделать колонку NULL
            tool.setColumnNullable("educationdirectionfis_t", "codefis_p", true);
            tool.setColumnNullable("educationdirectionfis_t", "newcode_p", true);
        }


        // создано обязательное свойство spravType
        {
            // создать колонку
            tool.createColumn("educationdirectionfis_t", new DBColumn("spravtype_id", DBType.LONG));

            // задать значение по умолчанию

            String tableName = EntityRuntime.getMeta(EducationdirectionSpravType.class).getTableName();
            ResultSet rs = tool.getConnection().createStatement().executeQuery("select id from " + tableName + " where code_p='" + EducationdirectionSpravTypeCodes.TITLE_2013 + "'");

            java.lang.Long defaultSpravType = null;
            while (rs.next()) {
                defaultSpravType = rs.getLong(1);
            }

            if (defaultSpravType == null)
                defaultSpravType = makeNewRowEducationdirectionSpravType(tool, EducationdirectionSpravTypeCodes.TITLE_2013);

            tool.executeUpdate("update educationdirectionfis_t set spravtype_id=? where spravtype_id is null", defaultSpravType);

            // сделать колонку NOT NULL
            tool.setColumnNullable("educationdirectionfis_t", "spravtype_id", false);
        }
    }

    /**
     * создать новую запись
     *
     * @param code
     *
     * @return
     *
     * @throws Exception
     */
    private Long makeNewRowEducationdirectionSpravType(DBTool tool, String code) throws Exception
    {
        short discriminator = EntityRuntime.getMeta(EducationdirectionSpravType.class).getEntityCode();
        Long newId = EntityIDGenerator.generateNewId(discriminator);

        String tableName = EntityRuntime.getMeta(EducationdirectionSpravType.class).getTableName();

        tool.executeUpdate("insert into " + tableName + " (id, discriminator, code_p, title_p) values (?,?,?,?)",
                           newId, discriminator, code, "2013");

        return newId;
    }
}