package ru.tandemservice.fisrmc.dao.packEduDocuments;

import org.tandemframework.shared.person.base.entity.PersonEduInstitution;

import java.util.Date;

/**
 * базовый класс для упаковки документов о полученном образовании
 *
 * @author vch
 */
public abstract class XMLBaseEducationDocument {

    private String uid;

    private boolean originalReceived;

    /**
     * необязательное поле
     */
    private Date originalReceivedDate = null;

    private String documentSeries;

    private String documentNumber;

    /**
     * необязательное поле
     */
    private Date documentDate = null;

    /**
     * необязательное поле
     */
    private String documentOrganization = null;

    private PersonEduInstitution personEduInstitution = null;

    public XMLBaseEducationDocument
            (

                    String uid
                    , boolean originalReceived
                    , Date originalReceivedDate
                    , String documentSeries
                    , String documentNumber
                    , Date documentDate
                    , String documentOrganization
                    , PersonEduInstitution personEduInstitution
            )
    {
        setUid(uid);
        setOriginalReceived(originalReceived);
        setOriginalReceivedDate(originalReceivedDate);
        setDocumentSeries(documentSeries);
        setDocumentNumber(documentNumber);
        setDocumentDate(documentDate);
        setDocumentOrganization(documentOrganization);
        setPersonEduInstitution(personEduInstitution);
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUid() {
        return uid;
    }

    public void setOriginalReceived(boolean originalReceived) {
        this.originalReceived = originalReceived;
    }

    public boolean isOriginalReceived() {
        return originalReceived;
    }

    public void setOriginalReceivedDate(Date originalReceivedDate) {
        this.originalReceivedDate = originalReceivedDate;
    }

    public Date getOriginalReceivedDate() {
        return originalReceivedDate;
    }

    public void setDocumentSeries(String documentSeries) {
        this.documentSeries = documentSeries;
    }

    public String getDocumentSeries() {
        return documentSeries;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentDate(Date documentDate) {
        this.documentDate = documentDate;
    }

    public Date getDocumentDate() {
        return documentDate;
    }

    public void setDocumentOrganization(String documentOrganization) {
        this.documentOrganization = documentOrganization;
    }

    public String getDocumentOrganization() {
        return documentOrganization;
    }

    public void setPersonEduInstitution(PersonEduInstitution personEduInstitution) {
        this.personEduInstitution = personEduInstitution;
    }

    public PersonEduInstitution getPersonEduInstitution() {
        return personEduInstitution;
    }

}
