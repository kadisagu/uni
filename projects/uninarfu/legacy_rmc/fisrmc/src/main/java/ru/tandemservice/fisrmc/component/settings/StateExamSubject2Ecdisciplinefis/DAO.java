package ru.tandemservice.fisrmc.component.settings.StateExamSubject2Ecdisciplinefis;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.fisrmc.entity.StateExamSubject2Ecdisciplinefis;
import ru.tandemservice.fisrmc.entity.catalog.Ecdisciplinefis;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DAO extends UniDao<Model> implements IDAO {


    @Override
    public void prepare(Model model)
    {
        model.setEcdisciplinefisList(getCatalogItemList(Ecdisciplinefis.class));
        model.setStateExamSubjectsList(getList(StateExamSubject.class));
    }

    public void prepareListDataSource(Model model)
    {
        prepareListData(model);

        List disciplines = model.getStateExamSubjectsList();
        DynamicListDataSource dataSource = model.getDataSource();
        dataSource.setCountRow(disciplines.size());
        UniBaseUtils.createPage(dataSource, disciplines);
    }

    @SuppressWarnings("unchecked")
    private void prepareListData(Model model)
    {
        List<StateExamSubject2Ecdisciplinefis> disciplines = getList(StateExamSubject2Ecdisciplinefis.class, StateExamSubject2Ecdisciplinefis.stateExamSubject().title().s());

        model.setDisciplines(disciplines);

        ((BlockColumn) model.getDataSource().getColumn("ecdisciplinefis")).setValueMap(getDisciplineId2ExamSubjectMap(disciplines));
    }

    @SuppressWarnings("unchecked")
    private Map<Long, Ecdisciplinefis> getDisciplineId2ExamSubjectMap(List<StateExamSubject2Ecdisciplinefis> disciplines)
    {


        List<Object[]> list = Collections.emptyList();
        if (!disciplines.isEmpty()) {
            Criteria criteria = getSession().createCriteria(StateExamSubject2Ecdisciplinefis.class);

            ProjectionList projections = Projections.projectionList();
            projections.add(Projections.property(StateExamSubject2Ecdisciplinefis.stateExamSubject().id().s()));
            projections.add(Projections.property(StateExamSubject2Ecdisciplinefis.ecdisciplinefis().s()));
            criteria.setProjection(projections);
            list = criteria.list();
        }
        Map result = new HashMap();
        for (Object[] row : list) {
            result.put((Long) row[0], (Ecdisciplinefis) row[1]);
        }
        return result;
    }


    public void update(Model model)
    {
        DynamicListDataSource dataSource = model.getDataSource();
        Map disciplineId2StateExamSubject = ((BlockColumn) dataSource.getColumn("ecdisciplinefis")).getValueMap();
        List<StateExamSubject> stateExamSubjects;
        try {
            stateExamSubjects = dataSource.getEntityList();
        }
        catch (Throwable e) {
            throw new RuntimeException(e);
        }

        Session session = getSession();

        for (StateExamSubject subject : stateExamSubjects) {

            Ecdisciplinefis disEcdisciplinefis = (Ecdisciplinefis) disciplineId2StateExamSubject.get(subject.getId());

            StateExamSubject2Ecdisciplinefis relExamSubject2Ecdisciplinefis = get(StateExamSubject2Ecdisciplinefis.class, StateExamSubject2Ecdisciplinefis.stateExamSubject(), subject);

            if (disEcdisciplinefis != null) {
                if (relExamSubject2Ecdisciplinefis != null) {
                    relExamSubject2Ecdisciplinefis.setEcdisciplinefis(disEcdisciplinefis);
                }
                else {
                    relExamSubject2Ecdisciplinefis = new StateExamSubject2Ecdisciplinefis();
                    relExamSubject2Ecdisciplinefis.setStateExamSubject(subject);
                    relExamSubject2Ecdisciplinefis.setEcdisciplinefis(disEcdisciplinefis);
                }
                session.saveOrUpdate(relExamSubject2Ecdisciplinefis);
            }
            else if (relExamSubject2Ecdisciplinefis != null) {
                session.delete(relExamSubject2Ecdisciplinefis);
            }
        }
    }


}
