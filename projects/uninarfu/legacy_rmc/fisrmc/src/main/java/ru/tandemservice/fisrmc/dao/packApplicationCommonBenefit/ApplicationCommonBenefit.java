package ru.tandemservice.fisrmc.dao.packApplicationCommonBenefit;

import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.tandemservice.fisrmc.dao.*;
import ru.tandemservice.fisrmc.dao.packApplications.Applications;
import ru.tandemservice.fisrmc.dao.packApplications.IApplications;
import ru.tandemservice.fisrmc.entity.catalog.Competitionkindfis;
import ru.tandemservice.fisrmc.entity.catalog.Entrantdocumenttypefis;
import ru.tandemservice.fisrmc.entity.catalog.Olimpiatfis;
import ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.entity.entrant.*;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class ApplicationCommonBenefit
        extends UniBaseDao
        implements IApplicationCommonBenefit
{

    private static IApplicationCommonBenefit _IApplicationCommonBenefit = null;

    public static IApplicationCommonBenefit Instanse()
    {
        if (_IApplicationCommonBenefit == null)
            _IApplicationCommonBenefit = (IApplicationCommonBenefit) ApplicationRuntime.getBean("applicationCommonBenefit");
        return _IApplicationCommonBenefit;
    }


    @Override
    public void makeCache()
    {
        DocumentReason.Instanse().makeCache();
    }

    @Override
    public void clearCache()
    {
        DocumentReason.Instanse().clearCache();
    }


    public Element packApplicationCommonBenefit
            (
                    Document doc
                    , EntrantRequest entrantrequest
                    , List<XMLCompetitiveGroup> lstCompetitiveGroup
                    , List<RequestedEnrollmentDirection> rdBenefit
            )
    {

        // зачтение олимпиады на дисциплине не является общей льготой, если у нас олимпиада
        // то нужен вид конкурса - только вне конкурса
        // если это не олимпиада - то особые условия на заявлении

        Element retVal = doc.createElement("ApplicationCommonBenefits");

        int countBenefit = 0;
        for (RequestedEnrollmentDirection ed : rdBenefit) {
            Element elem = packAppCommonBenefit(doc, entrantrequest, lstCompetitiveGroup, ed);
            if (elem != null) {
                retVal.appendChild(elem);
                countBenefit++;
            }
        }

        if (countBenefit == 0)
            return null;
        else
            return retVal;
    }


    /**
     * Упаковка одной льготы
     *
     * @param doc
     * @param entrantrequest
     * @param lstCompetitiveGroup
     * @param ed
     * @param b
     *
     * @return
     */
    private Element packAppCommonBenefit
    (
            Document doc,
            EntrantRequest entrantrequest,
            List<XMLCompetitiveGroup> lstCompetitiveGroup,
            RequestedEnrollmentDirection red
    )
    {
        OlympiadDiploma dimploma = null;
        Competitionkindfis competitionkindfis = null;
        Entrantdocumenttypefis entrantdocumenttypefis = null;
        List<PersonBenefit> listBenefit = null;


        // только без вступительных испытаний
        if (red.getCompetitionKind().getCode().equals("1")) {
            List<OlympiadDiploma> diplomas = getOlimpiadDiploma(red);//getOlimpiadDiploma(red, null);
            if (diplomas.isEmpty()) {
                // если олимпиады нет - исключительная ситуация
                String msg = "По заявлению на льготных условиях не найдена олимпиада  " + entrantrequest.getStringNumber();
                if (PackUnpackToFis.DAEMON_PARAMS.isSuspendErrorDoc()) {
                    PackUnpackToFis.AddPkgError(msg);
                    return null;
                }
                else
                    throw new ApplicationException(msg);
            }
            // берем первый диплом
            dimploma = diplomas.get(0);
            // вне конкурса однозначно
            competitionkindfis = SpravDao.Instanse().getCompetitionkindfis(red.getCompetitionKind());

            // всеросийская
            if (dimploma.getDiplomaType() != null && dimploma.getDiplomaType().getCode().equals("1"))
                entrantdocumenttypefis = (Entrantdocumenttypefis) SpravDao.Instanse().getEntityByCode(Entrantdocumenttypefis.class, "10");
            else
                entrantdocumenttypefis = (Entrantdocumenttypefis) SpravDao.Instanse().getEntityByCode(Entrantdocumenttypefis.class, "9");

            // упаковать и выйти (если олимпиада, более ничего не пакуем)
            Element retVal = _packBenefit(doc, entrantrequest, red, lstCompetitiveGroup, competitionkindfis, entrantdocumenttypefis, dimploma, listBenefit);
            return retVal;
        }


        // особые права???
        IApplications idaoApp = Applications.Instanse();
        RequestedEnrollmentDirectionExt redExt = idaoApp.getRequestedEnrollmentDirectionExt(red);
        if (redExt != null && redExt.isHaveSpecialRights()) {
            // особые права
            competitionkindfis = (Competitionkindfis) SpravDao.Instanse().getEntityByCode(Competitionkindfis.class, "4");

            // вынимаем все льготы персоны
            listBenefit = getPersonBenefit(entrantrequest);
            // формируем иной документ (пока ничего дополнительно не разбираем)
            if (listBenefit.isEmpty()) {
                String msg = "Для выбранного направления для приема с признаком 'имеет особые условия' не указано ни одной льготы у персоны" + entrantrequest.getStringNumber();
                if (PackUnpackToFis.DAEMON_PARAMS.isSuspendErrorDoc()) {
                    PackUnpackToFis.AddPkgError(msg);
                    return null;
                }
                else
                    throw new ApplicationException(msg);
            }
            else
                // иной документ
                entrantdocumenttypefis = (Entrantdocumenttypefis) SpravDao.Instanse().getEntityByCode(Entrantdocumenttypefis.class, "15");

            // все, если особые права, олимпиаду не засчитываем по предмету (у него свой конкурс)
            Element retVal = _packBenefit(doc, entrantrequest, red, lstCompetitiveGroup, competitionkindfis, entrantdocumenttypefis, dimploma, listBenefit);
            return retVal;
        }


        // vch (2014) проверено на стенде
            /*
			 * Недопустимый тип общей льготы: возможные значения "Без вступ. испытаний", "Вне Конкурса", "Преимущественное право на поступление" (Заявление, UID: 1470159441576222416) (33)
			 */
        // в последную очередь - зачтение олимпиады
			/*
			List<OlympiadDiploma> lst = getOlimpiadDiploma(red);
			if (lst!=null && !lst.isEmpty())
			{
				dimploma = lst.get(0);
				//Приравнивание к лицам, набравшим максимальное количество баллов по ЕГЭ
				competitionkindfis = (Competitionkindfis) SpravDao.Instanse().getEntityByCode(Competitionkindfis.class, "3");
	
				
				// всеросийская
				if (dimploma.getDiplomaType()!=null && dimploma.getDiplomaType().getCode().equals("1"))
					entrantdocumenttypefis = (Entrantdocumenttypefis) SpravDao.Instanse().getEntityByCode(Entrantdocumenttypefis.class, "10");
				else
					entrantdocumenttypefis = (Entrantdocumenttypefis) SpravDao.Instanse().getEntityByCode(Entrantdocumenttypefis.class, "9");
			
				Element retVal = _packBenefit(doc, entrantrequest, red, lstCompetitiveGroup, competitionkindfis,  entrantdocumenttypefis, dimploma, listBenefit );
				return retVal;
			}
			 */
        return null;
    }


    private Element _packBenefit(Document doc,
                                 EntrantRequest entrantrequest,
                                 RequestedEnrollmentDirection red,
                                 List<XMLCompetitiveGroup> lstCompetitiveGroup,
                                 Competitionkindfis competitionkindfis,
                                 Entrantdocumenttypefis entrantdocumenttypefis,
                                 OlympiadDiploma dimploma,
                                 List<PersonBenefit> listBenefit)
    {
        String uid = "acb:" + red.getId();
        Element retVal = doc.createElement("ApplicationCommonBenefit");

        // запакуем uid
        ToolsDao.PackUid(doc, retVal, uid);

        // в какую конкурсную группу попало выбранное направление
        XMLCompetitiveGroup competitiveGroup = _getCompetitiveGroup(red, lstCompetitiveGroup);
        ToolsDao.PackToElement(doc, retVal, "CompetitiveGroupID", competitiveGroup.getUid());

        ToolsDao.PackToElement(doc, retVal, "BenefitKindID", competitionkindfis.getCode());
        ToolsDao.PackToElement(doc, retVal, "DocumentTypeID", entrantdocumenttypefis.getCode());

        // если указан тип документа значит можно паковать и сам документ
        Element documentReason = _packDocumentReason(doc, entrantrequest, red, entrantdocumenttypefis, dimploma, listBenefit);

        if (documentReason != null) {
            Element documentReasonElement = doc.createElement("DocumentReason");
            retVal.appendChild(documentReasonElement);
            documentReasonElement.appendChild(documentReason);
        }

        return retVal;

    }


//	@Override
//	public Element packApplicationCommonBenefitOld2013(
//			Document doc
//			, EntrantRequest entrantrequest
//			, List<EnrollmentDirection> lstEnrollmentDirectionNotUse
//			, List<XMLCompetitiveGroup> lstCompetitiveGroup
//	        , List<RequestedEnrollmentDirection> requestedEnrollmentDirectionBenefitList
//	        , boolean hasPersonBenefitNotUse
//			, boolean hasOlimpiad
//	        )
//	{
//		// есть-ли зачтение олимпиады (одной дисциплины)
//		// если есть, то ищем зачтение олимпиады
//		// всегда обрабатываем только одну льготы
//		RequestedEnrollmentDirection requestedEnrollmentDirection = null;
//		OlympiadDiploma dimploma = null;
//		
//		
//		if (hasOlimpiad)
//		{
//			// в заявлении могут указать направление подготовки, не имеющее отношение к олимпиаде
//			// такое направление нужно отсечь
//			for (RequestedEnrollmentDirection rd : requestedEnrollmentDirectionBenefitList)
//			{
//				if (requestedEnrollmentDirection!=null)
//					continue;
//				
//				// без вступительных испытаний
//				if (rd.getCompetitionKind().getCode().equals("1"))
//				{
//					List<OlympiadDiploma> diplomas = getOlimpiadDiploma(rd);//getOlimpiadDiploma(red, null);
//					if (!diplomas.isEmpty())
//					{
//						requestedEnrollmentDirection = rd;
//						dimploma = diplomas.get(0); 
//					}
//				}
//				else
//				{
//					// <TODO> 2013/04/02 - зачтение диплома не является общей льготой
//					
////						List<ChosenEntranceDiscipline> olimpiadChosenEntranceDiscipline =  getChosenEntranceDisciplineOlimpiad(rd);
////						if (!olimpiadChosenEntranceDiscipline.isEmpty())
////						{
////							// есть зачтение олимпиады
////							List<OlympiadDiploma> diplomas = getOlimpiadDiploma(rd, olimpiadChosenEntranceDiscipline);
////							if (!diplomas.isEmpty())
////							{
////								requestedEnrollmentDirection = rd;
////								dimploma = diplomas.get(0);
////							}
////							else
////							{
////								String msg = "По заявлению " + entrantrequest.getStringNumber() + " на напралении подготовки с приоритетом " + rd.getPriority() + " произведено зачтение олимпиады без олимпиады ";
////								PackUnpackToFis.AddPkgError(msg);
////							}
////						}
//				}
//			}
//		}
//		else // нет олимпиад
//			requestedEnrollmentDirection = requestedEnrollmentDirectionBenefitList.get(0);
//
//		if (requestedEnrollmentDirection==null)
//			requestedEnrollmentDirection = requestedEnrollmentDirectionBenefitList.get(0);
//		
//		String uid = "acb:" + requestedEnrollmentDirection.getId();
//		Element retVal = doc.createElement("ApplicationCommonBenefit");
//		
//		// запакуем uid
//		ToolsDao.PackUid(doc, retVal, uid);
//		
//		// в какую конкурсную группу попало выбранное направление
//		XMLCompetitiveGroup competitiveGroup = _getCompetitiveGroup(requestedEnrollmentDirection, lstCompetitiveGroup); 
//		ToolsDao.PackToElement(doc, retVal, "CompetitiveGroupID", competitiveGroup.getUid());
//		
//		// вид конкурса по справочнику фис
//		Competitionkindfis competitionkindfis = null;
//		
//		CompetitionKind competitionKind = requestedEnrollmentDirection.getCompetitionKind();
//		competitionkindfis = SpravDao.Instanse().getCompetitionkindfis(competitionKind);
//		
//		// документ - основание
//		Entrantdocumenttypefis entrantdocumenttypefis = null;
//		List<PersonBenefit> listBenefit = null;
//
//		// зачтение предмета
//		// <TODO> 2013/04/02 - зачтение диплома не является общей льготой
////		List<ChosenEntranceDiscipline> olimpiadChosenEntranceDiscipline =  getChosenEntranceDisciplineOlimpiad(requestedEnrollmentDirection);
////		if (!olimpiadChosenEntranceDiscipline.isEmpty() )
////		{
////			// есть зачтение предмета
////			// нужно найти олимпиаду (в общем случае - олимпиады)
////			List<OlympiadDiploma> diplomas = getOlimpiadDiploma(requestedEnrollmentDirection, olimpiadChosenEntranceDiscipline);
////			if (!diplomas.isEmpty())
////			{
////				// Приравнивание к лицам, набравшим максимальное количество баллов по ЕГЭ
////				competitionkindfis = (Competitionkindfis) SpravDao.Instanse().getEntityByCode(Competitionkindfis.class, "3");
////				dimploma = diplomas.get(0);
////			}
////			else
////			{
////				// если олимпиады нет - исключительная ситуация
////				String msg = "По заявлению на льготных условиях не найдена олимпиада ()" + entrantrequest.getStringNumber();
////				if (PackUnpackToFis.SUSPENDERRORDOC)
////				{
////					PackUnpackToFis.AddPkgError(msg);
////					return null;
////				}
////				else
////					throw new ApplicationException(msg);
////			}
////			
////		}
//
//			// по виду конкурса
//			// может быть "Без вступительных испытаний"
//			// значит есть олимпиада
//			
//			// "Без вступительных испытаний"
//			if (competitionKind.getCode().equals("1"))
//			{
//				// должна быть супер олимпиада
//				List<OlympiadDiploma> diplomas = getOlimpiadDiploma(requestedEnrollmentDirection);
//				if (!diplomas.isEmpty())
//					// упаковка диплома олимпиады
//					dimploma = diplomas.get(0);
//				else
//				{
//					// если олимпиады нет - исключительная ситуация
//					String msg = "По заявлению на льготных условиях не найдена олимпиада  " + entrantrequest.getStringNumber();
//					if (PackUnpackToFis.DAEMON_PARAMS.isSuspendErrorDoc())
//					{
//						PackUnpackToFis.AddPkgError(msg);
//						return null;
//					}
//					else
//						throw new ApplicationException(msg);
//
//				}
//			}
//			// вне конкурса
//			else if (competitionKind.getCode().equals("3"))
//			{	// вынимаем все льготы персоны
//				listBenefit = getPersonBenefit(entrantrequest);
//				// формируем иной документ (пока ничего дополнительно не разбираем)
//
//				if (listBenefit.isEmpty())
//				{
//					String msg = "Для вида конкурса 'вне конкурса' не указано ни одной льготы " + entrantrequest.getStringNumber();
//					if (PackUnpackToFis.DAEMON_PARAMS.isSuspendErrorDoc())
//					{
//						PackUnpackToFis.AddPkgError(msg);
//						return null;
//					}
//					else
//						throw new ApplicationException(msg);
//				}
//				else
//					// иной документ
//					entrantdocumenttypefis = (Entrantdocumenttypefis) SpravDao.Instanse().getEntityByCode(Entrantdocumenttypefis.class, "15");
//			}
//		
//		
//		if (dimploma!=null)
//		{
//			// обычная олимпиада
//			entrantdocumenttypefis = (Entrantdocumenttypefis) SpravDao.Instanse().getEntityByCode(Entrantdocumenttypefis.class, "9");
//
//			
//			// всеросийская
//			if (dimploma.getDiplomaType()!=null && dimploma.getDiplomaType().getCode().equals("1"))
//				entrantdocumenttypefis = (Entrantdocumenttypefis) SpravDao.Instanse().getEntityByCode(Entrantdocumenttypefis.class, "10");
//		}
//		
//		// вид льготы
//		
//		if (entrantdocumenttypefis!=null && competitionkindfis!=null)
//		{
//			ToolsDao.PackToElement(doc, retVal, "BenefitKindID", competitionkindfis.getCode());
//			ToolsDao.PackToElement(doc, retVal, "DocumentTypeID", entrantdocumenttypefis.getCode());
//			
//			// если указан тип документа значит можно паковать и сам документ
//			Element documentReason = _packDocumentReason(doc, entrantrequest, requestedEnrollmentDirection, entrantdocumenttypefis, dimploma, listBenefit );
//			
//			if (documentReason!=null)
//			{
//				Element documentReasonElement = doc.createElement("DocumentReason");
//				retVal.appendChild(documentReasonElement);
//				documentReasonElement.appendChild(documentReason);
//			}
//		}
//		else
//		{
//			// льгот нет
//			retVal = null;
//		}
//			
//		return retVal;
//	}


    /**
     * Документ - основание оценки
     *
     * @param entrantdocumenttypefis
     * @param olympiadDiploma
     * @param listBenefit
     *
     * @return
     */
    private Element _packDocumentReason(
            Document doc,
            EntrantRequest entrantrequest
            , RequestedEnrollmentDirection requestedEnrollmentDirection
            , Entrantdocumenttypefis entrantdocumenttypefis
            , OlympiadDiploma olympiadDiploma
            , List<PersonBenefit> listBenefit)
    {
        // entrantdocumenttypefis - тип документа
        // 9 - обычная олимпиада
        // 10 - всеросийская олимпиада
        // 15 - иной документ

        IDocumentReason docReason = DocumentReason.Instanse();

        Olimpiatfis olimpiatfis = null;

        if (olympiadDiploma != null) {
            olimpiatfis = SpravDao.Instanse().getOlimpiadFisByOlimpiadTu(olympiadDiploma);
            if (olimpiatfis == null) {
                String msg = "Диплом олимпиады не сопоставлен со справочником ФИС " + entrantrequest.getStringNumber();
                if (PackUnpackToFis.DAEMON_PARAMS.isSuspendErrorDoc()) {
                    PackUnpackToFis.AddPkgError(msg);
                    return null;
                }
                else
                    throw new ApplicationException(msg);
            }
        }

        // обычная олимпиада
        if (entrantdocumenttypefis.getCode().equals("9"))
            return docReason.packOlympicDocument
                    (doc,
                     entrantrequest
                            , requestedEnrollmentDirection.isOriginalDocumentHandedIn()
                     //, entrantdocumenttypefis
                            , olympiadDiploma
                            , olimpiatfis
                    );


        // всеросийская
        if (entrantdocumenttypefis.getCode().equals("10"))
            return docReason.packOlympicTotalDocument
                    (doc,
                     entrantrequest
                            , requestedEnrollmentDirection.isOriginalDocumentHandedIn()
                     //, entrantdocumenttypefis
                            , olympiadDiploma
                            , olimpiatfis
                    );


        // иной документ (по списку льгот)
        if (entrantdocumenttypefis.getCode().equals("15"))
            return docReason.packCustomDocument(doc, entrantrequest, requestedEnrollmentDirection, entrantdocumenttypefis, listBenefit);


        // если нет документа - плохо, запишем в лог
        String msg = " По заявлению, с видом конкурса, требующего ввода основания льготы нет документа " + entrantrequest.getStringNumber();
        PackUnpackToFis.AddPkgError(msg);

        return null;
    }


    private XMLCompetitiveGroup _getCompetitiveGroup
            (
                    RequestedEnrollmentDirection requestedEnrollmentDirection
                    , List<XMLCompetitiveGroup> lstCompetitiveGroup)
    {

        List<XMLCompetitiveGroup> lst = new ArrayList<XMLCompetitiveGroup>();

        Long key = requestedEnrollmentDirection.getEnrollmentDirection().getId();
        for (XMLCompetitiveGroup cg : lstCompetitiveGroup) {
            List<XMLCompetitiveGroupItem> items = cg.getCompetitiveGroupItem();
            for (XMLCompetitiveGroupItem xitem : items) {
                List<EnrollmentDirection> elist = xitem.getEnrollmentDirectionsList();
                for (EnrollmentDirection ed : elist) {
                    if (ed.getId().equals(key)) {
                        if (!lst.contains(cg))
                            lst.add(cg);
                    }
                }
            }
        }


        String msg = "";
        if (lst.isEmpty()) {
            msg = "По заявлению на льготных условиях не найдена конкурсная группа " + requestedEnrollmentDirection.getEntrantRequest().getStringNumber();
            if (PackUnpackToFis.DAEMON_PARAMS.isSuspendErrorDoc())
                PackUnpackToFis.AddPkgError(msg);
            else
                throw new ApplicationException(msg);
        }

        if (!lst.isEmpty() && lst.size() > 1) {
            msg = "По заявлению на льготных условиях более одной конкурсной группы на выбранном направлении подготовки " + requestedEnrollmentDirection.getEntrantRequest().getStringNumber();
            if (PackUnpackToFis.DAEMON_PARAMS.isSuspendErrorDoc())
                PackUnpackToFis.AddPkgError(msg);
            else
                throw new ApplicationException(msg);
        }

        return lst.get(0);
    }


    /**
     * Описание льгот персоны
     *
     * @param entrantrequest
     *
     * @return
     */
    public List<PersonBenefit> getPersonBenefit(EntrantRequest entrantrequest)
    {
        Long personId = entrantrequest.getEntrant().getPerson().getId();

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(PersonBenefit.class, "personbenefit")
                .column("personbenefit")
                .where(DQLExpressions.eq(DQLExpressions.property(
                        PersonBenefit.person().id().fromAlias("personbenefit")), value(personId)))
                .order(property(PersonBenefit.benefit().userCode().fromAlias("personbenefit")), OrderDirection.desc);

        List<PersonBenefit> lstBenefit = dql.createStatement(getSession()).list();

        return lstBenefit;
    }


    //public List<OlympiadDiploma> getOlimpiadDiploma(RequestedEnrollmentDirection requestedEnrollmentDirection, List<ChosenEntranceDiscipline> lst)
    public List<OlympiadDiploma> getOlimpiadDiploma(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {

        List<OlympiadDiploma> retVal = new ArrayList<>();
        // если вид конкурса - без вступительных испытаний
        // то олимпиаду ищем среди незачтенных по предметам, если такой олимпиады нет, то среди зачтенных
        if (requestedEnrollmentDirection.getCompetitionKind().getCode().equals("1")) {
            // нет зачтения
            DQLSelectBuilder dqlNotIn = new DQLSelectBuilder()
                    .fromEntity(Discipline2OlympiadDiplomaRelation.class, "d2oNi")
                    .column(property(Discipline2OlympiadDiplomaRelation.diploma().id().fromAlias("d2oNi")))
                    .where(DQLExpressions.eq(DQLExpressions.property(
                            Discipline2OlympiadDiplomaRelation.diploma().entrant().fromAlias("d2oNi")), value(requestedEnrollmentDirection.getEntrantRequest().getEntrant())));


            DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(OlympiadDiploma.class, "od")
                    .where(DQLExpressions.eq(DQLExpressions.property(
                            OlympiadDiploma.entrant().fromAlias("od")), value(requestedEnrollmentDirection.getEntrantRequest().getEntrant())))
                    .where(DQLExpressions.notIn(DQLExpressions.property(
                            OlympiadDiploma.id().fromAlias("od")), dqlNotIn.getQuery()))
                    .setPredicate(DQLPredicateType.distinct);

            List<OlympiadDiploma> lstDiploma = dql.createStatement(getSession()).list();
            if (lstDiploma != null && !lstDiploma.isEmpty())
                retVal.addAll(lstDiploma);
        }

        if (!retVal.isEmpty())
            return retVal;

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(Discipline2OlympiadDiplomaRelation.class, "d2o");

        // направление подготовки
        dql.where(DQLExpressions.eq(DQLExpressions.property(
                Discipline2OlympiadDiplomaRelation.enrollmentDirection().fromAlias("d2o")), value(requestedEnrollmentDirection.getEnrollmentDirection())))

                // по абитуриенту
                .where(DQLExpressions.eq(DQLExpressions.property(
                        Discipline2OlympiadDiplomaRelation.diploma().entrant().fromAlias("d2o")), value(requestedEnrollmentDirection.getEntrantRequest().getEntrant())))

                .setPredicate(DQLPredicateType.distinct)
                .column(property(Discipline2OlympiadDiplomaRelation.diploma().fromAlias("d2o")));

        List<OlympiadDiploma> lstDiploma = dql.createStatement(getSession()).list();

        return lstDiploma;
    }
	
	/*
	@Override
	public List<ChosenEntranceDiscipline> getChosenEntranceDisciplineOlimpiad(RequestedEnrollmentDirection requestedEnrollmentDirection )
	{
		DQLSelectBuilder dql = new DQLSelectBuilder()
		.fromEntity(ChosenEntranceDiscipline.class, "ced")
		.where(DQLExpressions.eq(
				DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().fromAlias("ced"))
				, value(requestedEnrollmentDirection))
				)
		// зачтение олимпиады
		.where(DQLExpressions.eq(DQLExpressions.property(ChosenEntranceDiscipline.finalMarkSource().fromAlias("ced")),value(1L)));
		
		List<ChosenEntranceDiscipline> lst = dql.createStatement(getSession()).list();
		
		return lst;
	}
	*/

}
