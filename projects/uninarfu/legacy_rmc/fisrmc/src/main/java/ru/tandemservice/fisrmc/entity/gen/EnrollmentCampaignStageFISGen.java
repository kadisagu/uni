package ru.tandemservice.fisrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.fisrmc.entity.EnrollmentCampaignStageFIS;
import ru.tandemservice.fisrmc.entity.catalog.StageFis;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Этапы приемной кампании ФИС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrollmentCampaignStageFISGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.fisrmc.entity.EnrollmentCampaignStageFIS";
    public static final String ENTITY_NAME = "enrollmentCampaignStageFIS";
    public static final int VERSION_HASH = 1671146950;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_STAGE = "stage";
    public static final String P_DATE_ORDER = "dateOrder";

    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private StageFis _stage;     // Этапы
    private Date _dateOrder; 

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Этапы. Свойство не может быть null.
     */
    @NotNull
    public StageFis getStage()
    {
        return _stage;
    }

    /**
     * @param stage Этапы. Свойство не может быть null.
     */
    public void setStage(StageFis stage)
    {
        dirty(_stage, stage);
        _stage = stage;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public Date getDateOrder()
    {
        return _dateOrder;
    }

    /**
     * @param dateOrder  Свойство не может быть null.
     */
    public void setDateOrder(Date dateOrder)
    {
        dirty(_dateOrder, dateOrder);
        _dateOrder = dateOrder;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrollmentCampaignStageFISGen)
        {
            setEnrollmentCampaign(((EnrollmentCampaignStageFIS)another).getEnrollmentCampaign());
            setStage(((EnrollmentCampaignStageFIS)another).getStage());
            setDateOrder(((EnrollmentCampaignStageFIS)another).getDateOrder());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrollmentCampaignStageFISGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrollmentCampaignStageFIS.class;
        }

        public T newInstance()
        {
            return (T) new EnrollmentCampaignStageFIS();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "stage":
                    return obj.getStage();
                case "dateOrder":
                    return obj.getDateOrder();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "stage":
                    obj.setStage((StageFis) value);
                    return;
                case "dateOrder":
                    obj.setDateOrder((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "stage":
                        return true;
                case "dateOrder":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "stage":
                    return true;
                case "dateOrder":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "stage":
                    return StageFis.class;
                case "dateOrder":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrollmentCampaignStageFIS> _dslPath = new Path<EnrollmentCampaignStageFIS>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrollmentCampaignStageFIS");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.EnrollmentCampaignStageFIS#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Этапы. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.EnrollmentCampaignStageFIS#getStage()
     */
    public static StageFis.Path<StageFis> stage()
    {
        return _dslPath.stage();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.EnrollmentCampaignStageFIS#getDateOrder()
     */
    public static PropertyPath<Date> dateOrder()
    {
        return _dslPath.dateOrder();
    }

    public static class Path<E extends EnrollmentCampaignStageFIS> extends EntityPath<E>
    {
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private StageFis.Path<StageFis> _stage;
        private PropertyPath<Date> _dateOrder;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.EnrollmentCampaignStageFIS#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Этапы. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.EnrollmentCampaignStageFIS#getStage()
     */
        public StageFis.Path<StageFis> stage()
        {
            if(_stage == null )
                _stage = new StageFis.Path<StageFis>(L_STAGE, this);
            return _stage;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.EnrollmentCampaignStageFIS#getDateOrder()
     */
        public PropertyPath<Date> dateOrder()
        {
            if(_dateOrder == null )
                _dateOrder = new PropertyPath<Date>(EnrollmentCampaignStageFISGen.P_DATE_ORDER, this);
            return _dateOrder;
        }

        public Class getEntityClass()
        {
            return EnrollmentCampaignStageFIS.class;
        }

        public String getEntityName()
        {
            return "enrollmentCampaignStageFIS";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
