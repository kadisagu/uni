package ru.tandemservice.fisrmc.component.settings.GroupEsForFis.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.fisrmc.entity.GroupEsForFis;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.List;

public class Model implements IEnrollmentCampaignSelectModel {

    private DynamicListDataSource<GroupEsForFis> dataSource;

    private IMultiSelectModel catalogValueList;
    private IDataSettings settings;
    private ISelectModel enrollmentDirectionModel;
    private ISelectModel qualificationModel;
    private ISelectModel formativeOrgUnitModel;
    private ISelectModel territorialOrgUnitModel;
    private List<EnrollmentCampaign> enrollmentCampaignList;

    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return (EnrollmentCampaign) this.settings.get("enrollmentCampaign");
    }

    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        this.settings.set("enrollmentCampaign", enrollmentCampaign);
    }

    public ISelectModel getTerritorialOrgUnitModel() {
        return territorialOrgUnitModel;
    }

    public void setTerritorialOrgUnitModel(ISelectModel territorialOrgUnitModel) {
        this.territorialOrgUnitModel = territorialOrgUnitModel;
    }

    public ISelectModel getFormativeOrgUnitModel() {
        return formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel) {
        this.formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public ISelectModel getQualificationModel() {
        return qualificationModel;
    }

    public void setQualificationModel(ISelectModel qualificationModel) {
        this.qualificationModel = qualificationModel;
    }

    public ISelectModel getEnrollmentDirectionModel() {
        return enrollmentDirectionModel;
    }

    public void setEnrollmentDirectionModel(ISelectModel enrollmentDirectionModel) {
        this.enrollmentDirectionModel = enrollmentDirectionModel;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public DynamicListDataSource<GroupEsForFis> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<GroupEsForFis> dataSource) {
        this.dataSource = dataSource;
    }

    public IMultiSelectModel getCatalogValueList() {
        return catalogValueList;
    }

    public void setCatalogValueList(IMultiSelectModel catalogValueList) {
        this.catalogValueList = catalogValueList;
    }

    public List<EnrollmentCampaign> getEnrollmentCampaignList() {
        return enrollmentCampaignList;
    }

    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList) {
        this.enrollmentCampaignList = enrollmentCampaignList;
    }
}
