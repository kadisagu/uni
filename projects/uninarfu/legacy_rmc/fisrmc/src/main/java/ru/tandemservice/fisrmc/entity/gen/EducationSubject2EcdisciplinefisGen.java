package ru.tandemservice.fisrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.fisrmc.entity.EducationSubject2Ecdisciplinefis;
import ru.tandemservice.fisrmc.entity.catalog.Ecdisciplinefis;
import ru.tandemservice.uniec.entity.catalog.EducationSubject;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность-связь Предмет и Общеобразовательные прдметы (ФИС)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EducationSubject2EcdisciplinefisGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.fisrmc.entity.EducationSubject2Ecdisciplinefis";
    public static final String ENTITY_NAME = "educationSubject2Ecdisciplinefis";
    public static final int VERSION_HASH = -1587565959;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDUCATION_SUBJECT = "educationSubject";
    public static final String L_ECDISCIPLINEFIS = "ecdisciplinefis";

    private EducationSubject _educationSubject;     // Предмет
    private Ecdisciplinefis _ecdisciplinefis;     // Общеобразовательные предметы

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Предмет. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EducationSubject getEducationSubject()
    {
        return _educationSubject;
    }

    /**
     * @param educationSubject Предмет. Свойство не может быть null и должно быть уникальным.
     */
    public void setEducationSubject(EducationSubject educationSubject)
    {
        dirty(_educationSubject, educationSubject);
        _educationSubject = educationSubject;
    }

    /**
     * @return Общеобразовательные предметы. Свойство не может быть null.
     */
    @NotNull
    public Ecdisciplinefis getEcdisciplinefis()
    {
        return _ecdisciplinefis;
    }

    /**
     * @param ecdisciplinefis Общеобразовательные предметы. Свойство не может быть null.
     */
    public void setEcdisciplinefis(Ecdisciplinefis ecdisciplinefis)
    {
        dirty(_ecdisciplinefis, ecdisciplinefis);
        _ecdisciplinefis = ecdisciplinefis;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EducationSubject2EcdisciplinefisGen)
        {
            setEducationSubject(((EducationSubject2Ecdisciplinefis)another).getEducationSubject());
            setEcdisciplinefis(((EducationSubject2Ecdisciplinefis)another).getEcdisciplinefis());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EducationSubject2EcdisciplinefisGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EducationSubject2Ecdisciplinefis.class;
        }

        public T newInstance()
        {
            return (T) new EducationSubject2Ecdisciplinefis();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "educationSubject":
                    return obj.getEducationSubject();
                case "ecdisciplinefis":
                    return obj.getEcdisciplinefis();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "educationSubject":
                    obj.setEducationSubject((EducationSubject) value);
                    return;
                case "ecdisciplinefis":
                    obj.setEcdisciplinefis((Ecdisciplinefis) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "educationSubject":
                        return true;
                case "ecdisciplinefis":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "educationSubject":
                    return true;
                case "ecdisciplinefis":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "educationSubject":
                    return EducationSubject.class;
                case "ecdisciplinefis":
                    return Ecdisciplinefis.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EducationSubject2Ecdisciplinefis> _dslPath = new Path<EducationSubject2Ecdisciplinefis>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EducationSubject2Ecdisciplinefis");
    }
            

    /**
     * @return Предмет. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.fisrmc.entity.EducationSubject2Ecdisciplinefis#getEducationSubject()
     */
    public static EducationSubject.Path<EducationSubject> educationSubject()
    {
        return _dslPath.educationSubject();
    }

    /**
     * @return Общеобразовательные предметы. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.EducationSubject2Ecdisciplinefis#getEcdisciplinefis()
     */
    public static Ecdisciplinefis.Path<Ecdisciplinefis> ecdisciplinefis()
    {
        return _dslPath.ecdisciplinefis();
    }

    public static class Path<E extends EducationSubject2Ecdisciplinefis> extends EntityPath<E>
    {
        private EducationSubject.Path<EducationSubject> _educationSubject;
        private Ecdisciplinefis.Path<Ecdisciplinefis> _ecdisciplinefis;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Предмет. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.fisrmc.entity.EducationSubject2Ecdisciplinefis#getEducationSubject()
     */
        public EducationSubject.Path<EducationSubject> educationSubject()
        {
            if(_educationSubject == null )
                _educationSubject = new EducationSubject.Path<EducationSubject>(L_EDUCATION_SUBJECT, this);
            return _educationSubject;
        }

    /**
     * @return Общеобразовательные предметы. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.EducationSubject2Ecdisciplinefis#getEcdisciplinefis()
     */
        public Ecdisciplinefis.Path<Ecdisciplinefis> ecdisciplinefis()
        {
            if(_ecdisciplinefis == null )
                _ecdisciplinefis = new Ecdisciplinefis.Path<Ecdisciplinefis>(L_ECDISCIPLINEFIS, this);
            return _ecdisciplinefis;
        }

        public Class getEntityClass()
        {
            return EducationSubject2Ecdisciplinefis.class;
        }

        public String getEntityName()
        {
            return "educationSubject2Ecdisciplinefis";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
