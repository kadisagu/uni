package ru.tandemservice.fisrmc.component.catalog.compensationtypefis.CompensationtypefisAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.fisrmc.entity.catalog.Compensationtypefis;

public interface IDAO extends IDefaultCatalogAddEditDAO<Compensationtypefis, Model> {

}
