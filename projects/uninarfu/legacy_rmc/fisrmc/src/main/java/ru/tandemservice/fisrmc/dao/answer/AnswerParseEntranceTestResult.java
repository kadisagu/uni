package ru.tandemservice.fisrmc.dao.answer;

import ru.tandemservice.fisrmc.component.UploadFis2TU.Pub.DefaultParser;

public class AnswerParseEntranceTestResult extends DefaultParser
{
    public static final String ROOT = "ImportResultPackage";
    public static final String LOG = "Log";
    public static final String FAILED = "Failed";
    public static final String ENTRANCETestResults = "EntranceTestResults";

    public static final String ENTRANCETestResult = "EntranceTestResult";
    public static final String APPLICATION_NUMBER = "ApplicationNumber";
    public static final String APPLICATION_DATE = "RegistrationDate";

    public static final String ERROR = "ErrorInfo";
    public static final String ERROR_CODE = "ErrorCode";
    public static final String ERROR_MESS = "Message";

    public static final String SUBJECTNAME = "SubjectName";
    public static final String RESULTVALUE = "ResultValue";


    /*
     *  <SubjectName>Математика</SubjectName>
          <ResultValue>48</ResultValue>
     */
    public AnswerParseEntranceTestResult(String xml) {
        super(xml);

        this.elementPath = ROOT + "-" + LOG + "-" + FAILED + "-" + ENTRANCETestResults + "-" + ENTRANCETestResult;

        this.propertyMap.put(
                this.elementPath + "-" + APPLICATION_NUMBER,
                "number"
        );
        this.propertyMap.put(
                this.elementPath + "-" + APPLICATION_DATE,
                "date"
        );
        this.propertyMap.put(
                this.elementPath + "-" + ERROR + "-" + ERROR_CODE,
                "code"
        );

        this.propertyMap.put(
                this.elementPath + "-" + ERROR + "-" + ERROR_MESS,
                "message"
        );


        this.propertyMap.put(
                this.elementPath + "-" + SUBJECTNAME,
                "subjectName"
        );

        this.propertyMap.put(
                this.elementPath + "-" + RESULTVALUE,
                "resultValue"
        );

    }

}
