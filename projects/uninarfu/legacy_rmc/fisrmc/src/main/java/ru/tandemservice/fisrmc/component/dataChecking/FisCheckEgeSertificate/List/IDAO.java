package ru.tandemservice.fisrmc.component.dataChecking.FisCheckEgeSertificate.List;

import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {
    public RtfDocument createDocument(Model model);
}
