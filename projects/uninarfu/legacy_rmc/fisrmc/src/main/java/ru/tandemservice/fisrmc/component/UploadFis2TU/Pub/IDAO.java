package ru.tandemservice.fisrmc.component.UploadFis2TU.Pub;

import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {

    @Transactional
    public void checkEntrantRequestDate(Model model) throws Exception;
}
