package ru.tandemservice.fisrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import ru.tandemservice.fisrmc.entity.FisPackages;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_fisrmc_1x0x0_8to9 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.10"),
                        new ScriptDependency("org.tandemframework.shared", "1.1.3"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.1.3")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        IEntityMeta meta = EntityRuntime.getMeta(FisPackages.class);
        ////////////////////////////////////////////////////////////////////////////////
        // сущность fisPackages
        if (tool.tableExists(meta.getTableName())) {
            // создано свойство fisPkgNumber
            // создать колонку
            tool.createColumn("fispackages_t", new DBColumn("fispkgnumber_p", DBType.createVarchar(255)));
            // создано свойство fisPrim
            // создать колонку
            tool.createColumn("fispackages_t", new DBColumn("fisprim_p", DBType.TEXT));
            // создано свойство deletePkgType
            // создать колонку
            tool.createColumn("fispackages_t", new DBColumn("deletepkgtype_p", DBType.BOOLEAN));
            // создано свойство deleteApplication
            // создать колонку
            tool.createColumn("fispackages_t", new DBColumn("deleteapplication_p", DBType.BOOLEAN));
            // создано свойство deleteOrdersOfAdmission
            // создать колонку
            tool.createColumn("fispackages_t", new DBColumn("deleteordersofadmission_p", DBType.BOOLEAN));
            // создано свойство deleteCompetitiveGroupItems
            // создать колонку
            tool.createColumn("fispackages_t", new DBColumn("deletecompetitivegroupitems_p", DBType.BOOLEAN));
            // создано свойство deleteEntranceTestResults
            // создать колонку
            tool.createColumn("fispackages_t", new DBColumn("deleteentrancetestresults_p", DBType.BOOLEAN));
            // создано свойство deleteApplicationCommonBenefits
            // создать колонку
            tool.createColumn("fispackages_t", new DBColumn("dltapplctncmmnbnfts_p", DBType.BOOLEAN));
        }


    }
}