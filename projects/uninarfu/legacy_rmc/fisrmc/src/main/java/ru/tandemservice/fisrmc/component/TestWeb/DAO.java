package ru.tandemservice.fisrmc.component.TestWeb;

import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.fisrmc.entity.FisPackages;
import ru.tandemservice.fisrmc.entity.FisPackages2enrollmentCampaign;
import ru.tandemservice.fisrmc.entity.catalog.EducationdirectionSpravType;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.List;

public class DAO
        extends UniDao<Model> implements IDAO
{

    @Override
    public void prepare(Model model)
    {
        model.setEnrollmentCampaignModel(new LazySimpleSelectModel<>(EnrollmentCampaign.class));
        model.setSpravTypeModel(
                new LazySimpleSelectModel<>(EducationdirectionSpravType.class).setSortProperty(EducationdirectionSpravType.title().s()));

    }

    @Override
    public void prepareListDataSource(Model model)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(FisPackages.class, "pkg");
        dql.where(DQLExpressions.eq(DQLExpressions.property(FisPackages.deletePkgType().fromAlias("pkg")), DQLExpressions.value(model.isDeletePackageType())));
        dql.column("pkg");

        List<EnrollmentCampaign> campaigns = model.getSettings().get("enrollmentCampaignList");
        if (campaigns != null && campaigns.size() > 0) {
            DQLSelectBuilder dqlIn = new DQLSelectBuilder();
            dqlIn.fromEntity(FisPackages2enrollmentCampaign.class, "f2c");
            dqlIn.column(FisPackages2enrollmentCampaign.fisPackages().id().fromAlias("f2c").s());
            dqlIn.where(DQLExpressions.in(
                    DQLExpressions.property(FisPackages2enrollmentCampaign.enrollmentCampaign().fromAlias("f2c")),
                    campaigns
            ));
            dqlIn.predicate(DQLPredicateType.distinct);

            dql.where(DQLExpressions.in(
                    DQLExpressions.property(FisPackages.id().fromAlias("pkg")),
                    dqlIn.buildQuery()
            ));
        }


        DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(FisPackages.class, "pkg");
        orderRegistry.applyOrder(dql, model.getDataSource().getEntityOrder());

        UniBaseUtils.createPage(model.getDataSource(), dql, getSession());
    }

    @Override
    public Long getLastAddedPackage(Model model) {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(FisPackages.class, "pkg");
        dql.column(FisPackages.id().fromAlias("pkg").s());
        dql.where(DQLExpressions.isNotNull(FisPackages.xmlPackage().id().fromAlias("pkg")));
        dql.where(DQLExpressions.eq(DQLExpressions.property(FisPackages.deletePkgType().fromAlias("pkg")), DQLExpressions.value(model.isDeletePackageType())));

        dql.order(DQLExpressions.property(FisPackages.formingDate().fromAlias("pkg")), OrderDirection.desc);
        IDQLStatement statement = dql.createStatement(getSession());
        statement.setMaxResults(1);
        List<Long> lst = statement.list();
        if (lst.size() > 0) {
            return lst.get(0);
        }
        return null;
    }
}
