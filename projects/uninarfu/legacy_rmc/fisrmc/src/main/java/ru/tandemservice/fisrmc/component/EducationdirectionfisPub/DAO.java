package ru.tandemservice.fisrmc.component.EducationdirectionfisPub;

import ru.tandemservice.fisrmc.entity.Educationdirectionfis;
import ru.tandemservice.uni.dao.UniDao;

public class DAO extends UniDao<Model> implements IDAO {
    @Override
    public void prepare(Model model)
    {
        Educationdirectionfis entity = getNotNull(Educationdirectionfis.class, model.getEducationdirectionfisId());
        model.setEducationdirectionfis(entity);
    }

    @Override
    public void update(Model model)
    {
        Educationdirectionfis entity = model.getEducationdirectionfis();
        saveOrUpdate(entity);
    }

}
