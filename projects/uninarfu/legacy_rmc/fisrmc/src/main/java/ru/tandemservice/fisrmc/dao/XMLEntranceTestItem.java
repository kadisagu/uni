package ru.tandemservice.fisrmc.dao;

import org.apache.commons.lang.StringUtils;
import ru.tandemservice.fisrmc.entity.catalog.Ecdisciplinefis;
import ru.tandemservice.fisrmc.entity.catalog.Entrancedisciplinetypefis;

import java.util.ArrayList;
import java.util.List;

/**
 * Вступительное испытание конкурсной группы
 * <p/>
 * в документации на сервис упоминаются льготы, но льготы пока не пишу
 *
 * @author vch
 */
public class XMLEntranceTestItem {
    private String uid;

    /**
     * всегда связаны 1 к 1 с дисциплиной
     * соответственно в качестве id можно использовать id дисциплины +
     * id конкурсной группы (id конкурсной группы под вопросом, нужно протестить на импорте)
     */

    /**
     * тип всутипельного испытания
     */
    private Entrancedisciplinetypefis entrancedisciplinetypefis = null;

    /**
     * Минимальный балл
     * не обящательное поле
     */
    private int minScore = 0;


    /**
     * Общеобразовательный предмет
     * пока не понятно, что делать - если у нас творческое испытание, а такого предмета нет
     */
    private Ecdisciplinefis ecdisciplinefis = null;

    private List<XMLCommonBenefit> entranceTestBenefit = null;

    public XMLEntranceTestItem
            (
                    String uid
                    , Entrancedisciplinetypefis entrancedisciplinetypefis
                    , int minScore
                    , Ecdisciplinefis ecdisciplinefis
                    , List<XMLCommonBenefit> entranceTestBenefit
            )
    {
        setEntrancedisciplinetypefis(entrancedisciplinetypefis);
        setMinScore(minScore);
        setEcdisciplinefis(ecdisciplinefis);
        setUid(uid);
        setEntranceTestBenefit(entranceTestBenefit);
    }


    public void setEntrancedisciplinetypefis(Entrancedisciplinetypefis entrancedisciplinetypefis) {
        this.entrancedisciplinetypefis = entrancedisciplinetypefis;
    }


    public Entrancedisciplinetypefis getEntrancedisciplinetypefis() {
        return entrancedisciplinetypefis;
    }


    public String getForm()
    {
        return StringUtils.join(lstForm, ", ");
    }


    public void setEcdisciplinefis(Ecdisciplinefis ecdisciplinefis) {
        this.ecdisciplinefis = ecdisciplinefis;
    }


    public Ecdisciplinefis getEcdisciplinefis() {
        return ecdisciplinefis;
    }


    public void setMinScore(int minScore) {
        this.minScore = minScore;
    }


    public int getMinScore() {
        return minScore;
    }


    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUid() {
        return uid;
    }

    private List<String> lstForm = new ArrayList<String>();

    public void addForm(String form)
    {
        if (!lstForm.contains(form))
            lstForm.add(form);
    }


    public List<XMLCommonBenefit> getEntranceTestBenefit() {
        return entranceTestBenefit;
    }


    public void setEntranceTestBenefit(List<XMLCommonBenefit> entranceTestBenefit) {
        this.entranceTestBenefit = entranceTestBenefit;
    }


}
