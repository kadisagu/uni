package ru.tandemservice.fisrmc.component.settings.DisciplineFisSettings;

import org.tandemframework.core.view.list.source.SimpleListDataSource;

public class Model {

    private SimpleListDataSource dataSource;

    public SimpleListDataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(SimpleListDataSource dataSource) {
        this.dataSource = dataSource;
    }

}
