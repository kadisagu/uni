package ru.tandemservice.fisrmc.dao.enrollmentCampaignExtDao;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.fisrmc.entity.EnrollmentCampaignFisExt;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;

import java.util.HashMap;
import java.util.Map;

public class DaoEc
        extends UniBaseDao implements IDaoEc
{
    private static IDaoEc _IDaoEc = null;

    public static IDaoEc Instanse()
    {
        if (_IDaoEc == null)
            _IDaoEc = (IDaoEc) ApplicationRuntime.getBean("daoEc");
        return _IDaoEc;
    }


    @Override
    public EnrollmentCampaignFisExt getEnrollmentCampaignFisExt(
            EnrollmentCampaign campaign)
    {
        return getEnrollmentCampaignFisExt(campaign, false);
    }

    @Override
    public EnrollmentCampaignFisExt getEnrollmentCampaignFisExt(
            EnrollmentCampaign campaign, boolean createIfNull)
    {
        // нужно найте
        EnrollmentCampaignFisExt ext = get(EnrollmentCampaignFisExt.class, EnrollmentCampaignFisExt.L_ENROLLMENT_CAMPAIGN, campaign);

        if (ext == null && createIfNull) {
            ext = new EnrollmentCampaignFisExt();
            ext.setEnrollmentCampaign(campaign);
        }
        return ext;
    }

    public int getEntrantRequestNewNumber(Entrant entrant, int number)
    {
        return getEntrantRequestNewNumber(entrant, number, false);
    }


    @Override
    public int getEntrantRequestNewNumber(Entrant entrant, int number, boolean systemAction)
    {
        return getEntrantRequestNewNumber(entrant, number, systemAction, true);

    }

    public int getEntrantRequestNewNumber(Entrant entrant, int number, boolean systemAction, boolean checkUnique)
    {
        // номер формируем как

        // yy - год проведения приемки
        // 12 - код из расширенных настроек приемки
        // z - порядковый номер заявления
        // nnnn - личный номер абитуриента без 1-ых 2-х символов (года приема)

        EnrollmentCampaign ec = entrant.getEnrollmentCampaign();

        int year = _getEducationYear(ec, systemAction);

        // 1-ые 2 символа не всегда означают год, для нарфу это перегружено
        int extensionNumber = getExtensionEcNumber(ec, systemAction);

        // порядковый номер заявления (определяем как кол-во +1)
        int exsistRequestCount = getEntrantRequestCount(entrant, systemAction);

        if (number == 0 && checkUnique)
            number = 1;

        int z = exsistRequestCount + number;
        if (z > 9)
            throw new ApplicationException("Порядковый номер абитуриента внутри одной ПК не может быть >9");

        StringBuilder sb = new StringBuilder();

        // 2 числа года
        sb.append(String.format("%02d", new Object[]{
                Integer.valueOf(year % 100)}));

        // код приемки из расширенных настроек
        sb.append(String.format("%02d", new Object[]{
                extensionNumber}));
        // порядковый номер заявления
        sb.append(Integer.toString(z));
        // личный номер без первый 2-х символов
        sb.append(entrant.getPersonalNumber().substring(2));

        int retVal = 0;
        try {
            retVal = Integer.parseInt(sb.toString());
        }
        catch (Exception ex) {
            throw new ApplicationException("Ошибка получения номера заявления из строки " + sb.toString());
        }

        if (checkUnique) {
            // проверка на уникальность
            EntrantRequest exsistNumber = get(EntrantRequest.class, EntrantRequest.P_REG_NUMBER, retVal);
            if (exsistNumber != null) {
                // номер занят, уходим в повтор
                number++;
                return getEntrantRequestNewNumber(entrant, number, systemAction, checkUnique);
            }
        }
        return retVal;
    }


    Map<Long, Integer> mapEducationYear = new HashMap<Long, Integer>();

    private int _getEducationYear(EnrollmentCampaign ec, boolean systemAction)
    {
        Integer retVal = 0;
        if (systemAction) {
            Long key = ec.getId();
            if (mapEducationYear.containsKey(key))
                retVal = mapEducationYear.get(key);
            else {
                retVal = ec.getEducationYear().getIntValue();
                mapEducationYear.put(key, retVal);
            }
        }
        else {
            mapEducationYear = new HashMap<Long, Integer>();
            retVal = ec.getEducationYear().getIntValue();
        }
        return retVal.intValue();
    }

    public int getEntrantRequestCount(Entrant entrant, boolean systemAction)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EntrantRequest.class, "request");
        dql.column(DQLFunctions.count(EntrantRequest.id().fromAlias("request").s()));
        dql.where(DQLExpressions.eq(DQLExpressions.property(EntrantRequest.entrant().id().fromAlias("request")), DQLExpressions.value(entrant.getId())));

        // при системном переименовании используем заявы только с уже правильными номерами
        if (systemAction)
            dql.where(DQLExpressions.gt(DQLExpressions.property(EntrantRequest.regNumber().fromAlias("request")), DQLExpressions.value(10000000)));

        Long count = (Long) dql.createStatement(getSession()).list().get(0);
        return count.intValue();
    }


    Map<Long, Integer> mapExtensionEcNumber = new HashMap<Long, Integer>();

    protected int getExtensionEcNumber(EnrollmentCampaign ec, boolean systemAction)
    {
        if (systemAction) {
            Long key = ec.getId();
            if (mapExtensionEcNumber.containsKey(key))
                return mapExtensionEcNumber.get(key);
            else {
                // найти
                int retVal = _getExtensionEcNumber(ec);
                // разместить
                mapExtensionEcNumber.put(key, retVal);
                return retVal;
            }
        }
        else {
            mapExtensionEcNumber = new HashMap<Long, Integer>();
            int retVal = _getExtensionEcNumber(ec);
            return retVal;

        }
    }

    private int _getExtensionEcNumber(EnrollmentCampaign ec)
    {
        String errorTitle = "Для приемной кампании " + ec.getTitle() + " не указан сокращенный код для формирования номера заявления";
        EnrollmentCampaignFisExt ecExt = getEnrollmentCampaignFisExt(ec, false);
        if (ecExt == null)
            throw new ApplicationException(errorTitle);

        if (ecExt.getRequestPrefix() == null || ecExt.getRequestPrefix() == "")
            throw new ApplicationException(errorTitle);

        int retVal = 0;
        try {
            retVal = Integer.parseInt(ecExt.getRequestPrefix());
        }
        catch (Exception e) {
            errorTitle = "Для приемной кампании " + ec.getTitle() + " код для формирования номера заявления указан с ошибкой ";
            throw new ApplicationException(errorTitle);
        }
        return retVal;
    }


}
