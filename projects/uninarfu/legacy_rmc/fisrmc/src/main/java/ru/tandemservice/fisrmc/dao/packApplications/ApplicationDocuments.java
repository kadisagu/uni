package ru.tandemservice.fisrmc.dao.packApplications;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.tandemservice.fisrmc.dao.ToolsDao;
import ru.tandemservice.fisrmc.dao.packEduDocuments.PackEduDocuments;
import ru.tandemservice.uniecrmc.entity.entrant.Entrant2IndividualAchievements;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;

import java.util.List;

public class ApplicationDocuments
        extends UniBaseDao
        implements IApplicationDocuments
{

    private static IApplicationDocuments _IApplicationDocuments = null;

    public static IApplicationDocuments Instanse()
    {
        if (_IApplicationDocuments == null)
            _IApplicationDocuments = (IApplicationDocuments) ApplicationRuntime.getBean("applicationDocuments");
        return _IApplicationDocuments;
    }

    @Override
    public Element PackApplicationDocuments(Document doc,
                                            EntrantRequest entrantrequest)
    {

        Element applicationDocuments = doc.createElement("ApplicationDocuments");

        // ЕГЭ
        _packEgeDocuments(applicationDocuments, doc, entrantrequest);

        // удостоверения личности
        _packIdentityCards(applicationDocuments, doc, entrantrequest);


        _packIndividualAchievements(applicationDocuments, doc, entrantrequest);


        Element eduDocuments = _packEduDocument(doc, entrantrequest);
        if (eduDocuments != null)
            applicationDocuments.appendChild(eduDocuments);

        return applicationDocuments;
    }

    private void _packIndividualAchievements(
            Element rootDocuments
            , Document doc
            , EntrantRequest entrantrequest)
    {

        Entrant entrant = entrantrequest.getEntrant();

        IApplications idao = Applications.Instanse();

        if (!idao.hasIndividualAchievements(entrant))
            return;

        // упаковка индивидуальных достижений
        // на каждое индивидуальное достижение создаем иной документ
        // иной документ описан на стр. 22
        List<Entrant2IndividualAchievements> lst = idao.getEntrantIndividualAchievements(entrant);

        Element elemDocs = doc.createElement("CustomDocuments");
        rootDocuments.appendChild(elemDocs);

        for (Entrant2IndividualAchievements ia : lst) {
            Element iaElement = _packIADocument(doc, entrantrequest, ia);
            if (iaElement != null)
                elemDocs.appendChild(iaElement);
        }
    }

    /**
     * индивидуальные достижения
     *
     * @param doc
     * @param entrantrequest
     * @param ia
     *
     * @return
     */
    private Element _packIADocument(
            Document doc
            , EntrantRequest entrantrequest
            , Entrant2IndividualAchievements ia)
    {
        // указываем минимальный набор полей, всегда как custom document (стр 22)
        // документ прикладываем к каждому заявлению, поэтому в id участвует заявление
        // сами достижения см. стр 40
        Element elem = doc.createElement("CustomDocument");
        String uid = ia.getUid(entrantrequest);

        ToolsDao.PackUid(doc, elem, uid);

        boolean isOrigin = Applications.Instanse().isOriginalDocuments(entrantrequest);
        ToolsDao.PackOriginalReceived(doc, elem, isOrigin);

        if (isOrigin)
            //Дата предоставления оигинала документов (пока неясно какое значение сюда подставлять)
            ToolsDao.PackToElement(doc, elem, "OriginalReceivedDate", Applications.Instanse().getOriginDocumentDateString(entrantrequest));


        // individualAchievements можно считать типом документа, но справочник
        // пользовательский, хз чего в него накидают
        ToolsDao.PackToElement(doc, elem, "DocumentTypeNameText", "Документ основание ИД");

        String dsk = "";
        if (ia.getIndividualAchievements() != null)
            dsk += ia.getIndividualAchievements().getTitle();

        if (ia.getText() != null && !ia.getText().isEmpty())
            dsk += " (" + ia.getText() + ")";

        if (!dsk.isEmpty())
            ToolsDao.PackToElement(doc, elem, "AdditionalInfo", dsk);

        return elem;
    }

    private Element _packEduDocument
            (
                    Document doc
                    , EntrantRequest entrantrequest
            )
    {
        // тут упакован сам документ
        Element elem = PackEduDocuments.Instanse().PackEduDocumentsToXml(doc, entrantrequest);

        // страница 30 2014 год, теперь документ об образовании вроде
        // не обязателен, упаковываем только один документ

        if (elem != null) {
            Element root = doc.createElement("EduDocuments");
            Element docElem = doc.createElement("EduDocument");

            root.appendChild(docElem);
            docElem.appendChild(elem);

            return root;
        }
        else
            return null;
    }

    private void _packIdentityCards(
            Element root
            , Document doc
            , EntrantRequest entrantrequest
    )
    {
        // поступаем с упрощением, пакуем всегда основное удостоверение личности
        // хотя можно просмотреть все удостоверения на персоне
        IdentityCard ic = entrantrequest.getEntrant().getPerson().getIdentityCard();

        // удостоверение личности есть всегда
        Element elem = PackIdentityDocument.Instanse().packIdentityDocument(doc, entrantrequest, ic);

        root.appendChild(elem);
    }

    private void _packEgeDocuments(
            Element rootDocuments
            , Document doc
            , EntrantRequest entrantrequest)
    {
        Element element = PackEgeDocuments.Instanse().packEgeDocuments(doc, entrantrequest.getEntrant());
        if (element != null)
            rootDocuments.appendChild(element);
    }

}
