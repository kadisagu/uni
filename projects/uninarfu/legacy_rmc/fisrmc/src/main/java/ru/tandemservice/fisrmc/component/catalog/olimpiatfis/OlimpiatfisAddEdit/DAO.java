package ru.tandemservice.fisrmc.component.catalog.olimpiatfis.OlimpiatfisAddEdit;

import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.fisrmc.entity.catalog.Ecolimpiadlevelfis;
import ru.tandemservice.fisrmc.entity.catalog.Olimpiatfis;

public class DAO extends DefaultCatalogAddEditDAO<Olimpiatfis, Model> implements IDAO {


    @Override
    public void prepare(Model model) {
        super.prepare(model);

        model.setEcolimpiadlevelfisModel(new LazySimpleSelectModel<Ecolimpiadlevelfis>(Ecolimpiadlevelfis.class));
    }
}
