package ru.tandemservice.fisrmc.dao.answer;

import ru.tandemservice.fisrmc.component.UploadFis2TU.Pub.DefaultParser;

public class GetEgeDocumentParser extends DefaultParser
{

    public GetEgeDocumentParser(String xml) {
        super(xml);

        this.elementPath = "ImportedAppCheckResultPackage-GetEgeDocuments-GetEgeDocument";

        this.propertyMap.put(
                this.elementPath + "-Application-ApplicationNumber",
                "number"
        );

        this.propertyMap.put(
                this.elementPath + "-Application-RegistrationDate",
                "date"
        );
        this.propertyMap.put(
                this.elementPath + "-Error",
                "error"
        );

        this.propertyMap.put(
                this.elementPath + "-EgeDocuments-EgeDocument-CertificateNumber",
                "docNumber"
        );
        this.listProperty.add("docNumber");
    }


    //////////////////////////////////////////////////////////////////////////////////
    public static class GetEgeDocumentAdditionalParser extends DefaultParser {

        public GetEgeDocumentAdditionalParser(String xml) {
            super(xml);

            this.elementPath = "ImportedAppCheckResultPackage-GetEgeDocuments-GetEgeDocument-EgeDocuments-EgeDocument";

            this.propertyMap.put(
                    this.elementPath + "-CertificateNumber",
                    "docNumber"
            );
            this.propertyMap.put(
                    this.elementPath + "-Year",
                    "docYear"
            );
            this.propertyMap.put(
                    this.elementPath + "-Status",
                    "docStatus"
            );

            this.propertyMap.put(
                    this.elementPath + "-Marks-Mark-SubjectName",
                    "subject"
            );
            this.listProperty.add("subject");

            this.propertyMap.put(
                    this.elementPath + "-Marks-Mark-SubjectMark",
                    "mark"
            );
            this.listProperty.add("mark");

        }

    }
}
