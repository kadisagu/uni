package ru.tandemservice.fisrmc.base.bo.EdOrgunit2DirectionFisRel.logic.dsHandlers;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.fisrmc.entity.EdOrgunit2DirectionFis;
import ru.tandemservice.fisrmc.entity.catalog.EducationdirectionSpravType;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.ArrayList;
import java.util.List;

public class EdOrgunit2DirectionDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput> {

    //public static final String DEVELOP_FORM_FILTER = "developFormFilter";

    public EdOrgunit2DirectionDSHandler(String ownerId)
    {
        super(ownerId);
    }


    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Object enrollmentCampaignFilter = context.get("enrollmentCampaignFilter");
        Object qualificationsFilter = context.get("qualificationsFilter");


        Object eduProgTitleFilter = context.get("eduProgTitleFilter");
        Object eduProgCodeSpecFilter = context.get("eduProgCodeSpecFilter");
        Object eduProgCodeFilter = context.get("eduProgCodeFilter");


        Object directionTitleFilterFis = context.get("directionTitleFilterFis");
        Object exportCommentFis = context.get("exportComment");
        Object spravTypeFilter = context.get("spravTypeFilter");


        Boolean isHasRelaion = (Boolean) (context.get("isSuccessfullyHandOverSession") == null ? false : context.get("isSuccessfullyHandOverSession"));

        DQLSelectBuilder orgUnitBuiilder = new DQLSelectBuilder();
        orgUnitBuiilder.fromEntity(EnrollmentDirection.class, "ed")
                .column(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().fromAlias("ed")))
                .joinEntity("ed", DQLJoinType.left, EdOrgunit2DirectionFis.class, "rel", DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().fromAlias("ed")), DQLExpressions.property(EdOrgunit2DirectionFis.educationOrgUnit().fromAlias("rel"))))
                .column("rel")
                .setPredicate(DQLPredicateType.distinct);

        if (isHasRelaion) {
            // только сопоставленное, на тип справочника не смотрим, сей фильтр см. ниже
            orgUnitBuiilder.where(DQLExpressions.isNotNull(DQLExpressions.property(EdOrgunit2DirectionFis.id().fromAlias("rel"))));
        }
        else {
            // странность какая-то
            // них не понимаю, ни так ни так не работает
            // обойду в цикле <vch> 2014/06/05
            // тут нужен OR
            // orgUnitBuiilder.where(DQLExpressions.isNull(DQLExpressions.property(EdOrgunit2DirectionFis.id().fromAlias("rel"))));
            /*
			orgUnitBuiilder.where(DQLExpressions.or(
					DQLExpressions.isNull(DQLExpressions.property(EdOrgunit2DirectionFis.id().fromAlias("rel")))
					, DQLExpressions.eq(DQLExpressions.property(EdOrgunit2DirectionFis.educationDirectionFis().spravType().fromAlias("rel")), DQLExpressions.value(spravTypeFilter))
					));
			*/

        }

        FilterUtils.applySelectFilter(orgUnitBuiilder, "ed", EnrollmentDirection.enrollmentCampaign(), enrollmentCampaignFilter);
        FilterUtils.applySelectFilter(orgUnitBuiilder, "ed", EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().educationLevel().qualification(), qualificationsFilter);

        if (eduProgCodeFilter != null && eduProgCodeFilter != "")
            FilterUtils.applySimpleLikeFilter(orgUnitBuiilder, "ed", EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().educationLevel().eduProgramSubject().subjectCode(), (String) eduProgCodeFilter);

        if (eduProgCodeSpecFilter != null && eduProgCodeSpecFilter != "")
            FilterUtils.applySimpleLikeFilter(orgUnitBuiilder, "ed", EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().educationLevel().eduProgramSpecialization().programSubject().subjectCode(), (String) eduProgCodeSpecFilter);


        if (eduProgTitleFilter != null && eduProgTitleFilter != null)
            FilterUtils.applySimpleLikeFilter(orgUnitBuiilder, "ed", EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().title(), (String) eduProgTitleFilter);


        // по названию фис
        if (isHasRelaion) {
            if (directionTitleFilterFis != null)
                FilterUtils.applySimpleLikeFilter(orgUnitBuiilder, "rel", EdOrgunit2DirectionFis.educationDirectionFis().title(), (String) directionTitleFilterFis);

            if (exportCommentFis != null)
                FilterUtils.applySimpleLikeFilter(orgUnitBuiilder, "rel", EdOrgunit2DirectionFis.educationDirectionFis().exportComment(), (String) exportCommentFis);

            if (spravTypeFilter != null)
                FilterUtils.applySelectFilter(orgUnitBuiilder, "rel", EdOrgunit2DirectionFis.educationDirectionFis().spravType(), spravTypeFilter);
        }

        List<Object[]> orgUnits = orgUnitBuiilder.createStatement(context.getSession()).list();

        List<DataWrapper> resultList = new ArrayList<DataWrapper>();
        // List<Long> idList = new ArrayList<>();
        List<Long> idListRelated = new ArrayList<>(); // правильно сопоставлено


        // собираем список сопоставленных ID
        for (Object[] data : orgUnits) {
            EducationOrgUnit educationOrgUnit = (EducationOrgUnit) data[0];
            EdOrgunit2DirectionFis edOrgunit2DirectionFis = (EdOrgunit2DirectionFis) data[1];

            // не указан тип справочника - все в жопу
            if (spravTypeFilter == null)
                continue;

            if (edOrgunit2DirectionFis != null && edOrgunit2DirectionFis.getEducationDirectionFis().getSpravType().equals(spravTypeFilter))
                idListRelated.add(educationOrgUnit.getId());

        }


        for (Object[] data : orgUnits) {
            EducationOrgUnit educationOrgUnit = (EducationOrgUnit) data[0];
            EdOrgunit2DirectionFis edOrgunit2DirectionFis = (EdOrgunit2DirectionFis) data[1];

            // не указан тип справочника - все в жопу
            if (spravTypeFilter == null)
                continue;
            EducationdirectionSpravType sType = (EducationdirectionSpravType) spravTypeFilter;

            if (isHasRelaion) {
                if (edOrgunit2DirectionFis != null) {
                    // доп фильтрация по типу справочника
                    if (!edOrgunit2DirectionFis.getEducationDirectionFis().getSpravType().equals(sType))
                        continue;
                }
                else
                    continue;
            }
            else// только не сопоставленное
            {
                if (edOrgunit2DirectionFis != null) {
                    if (idListRelated.contains(educationOrgUnit.getId()))
                        continue;
                    else {
                        // левое значение
                        edOrgunit2DirectionFis = null;
                    }
                }
                else {// точно наше
                }
            }


            DataWrapper w = new DataWrapper(educationOrgUnit);
            w.setProperty("eou", educationOrgUnit);
            w.setProperty("rel", edOrgunit2DirectionFis);
            w.setProperty("hasRelationToFis", edOrgunit2DirectionFis != null ? true : false);
            resultList.add(w);

        }

        return ListOutputBuilder.get(input, resultList).pageable(true).build();
    }

}
