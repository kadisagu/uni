package ru.tandemservice.fisrmc.component.UploadFis2TU.AddEdit;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.core.component.Input;
import ru.tandemservice.fisrmc.entity.FisExportFiles;

@Input(keys = {"publisherId"}, bindings = {"entity.id"})
public class Model {

    private FisExportFiles entity = new FisExportFiles();
    private IUploadFile file;

    public FisExportFiles getEntity() {
        return entity;
    }

    public void setEntity(FisExportFiles entity) {
        this.entity = entity;
    }

    public IUploadFile getFile() {
        return file;
    }

    public void setFile(IUploadFile file) {
        this.file = file;
    }

    public boolean isAddForm()
    {
        if (entity.getId() == null)
            return true;
        else
            return false;
    }

}
