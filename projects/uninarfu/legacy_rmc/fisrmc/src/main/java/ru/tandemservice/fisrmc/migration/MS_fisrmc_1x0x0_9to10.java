package ru.tandemservice.fisrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_fisrmc_1x0x0_9to10 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.10"),
                        new ScriptDependency("org.tandemframework.shared", "1.1.3"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.1.3")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность fisAnswerAction

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("fisansweraction_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("date_p", DBType.TIMESTAMP).setNullable(false),
                                      new DBColumn("fisanswertofispackages_id", DBType.LONG).setNullable(false),
                                      new DBColumn("comment_p", DBType.TEXT),
                                      new DBColumn("actionreport_id", DBType.LONG),
                                      new DBColumn("errorreport_id", DBType.LONG)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("fisAnswerAction");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fisAnswerTofisPackages

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("fisanswertofispackages_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("answerdate_p", DBType.TIMESTAMP).setNullable(false),
                                      new DBColumn("fispackages_id", DBType.LONG).setNullable(false),
                                      new DBColumn("fisanswertype_id", DBType.LONG).setNullable(false),
                                      new DBColumn("fisprim_p", DBType.TEXT)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("fisAnswerTofisPackages");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fisAnswerType

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("fisanswertype_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("title_p", DBType.createVarchar(1200))
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("fisAnswerType");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fisAppStatusCode

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("fisappstatuscode_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("title_p", DBType.createVarchar(1200))
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("fisAppStatusCode");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fisCheckEgeSertificate

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("fischeckegesertificate_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("checkdate_p", DBType.TIMESTAMP).setNullable(false),
                                      new DBColumn("entrantstateexamcertificate_id", DBType.LONG).setNullable(false),
                                      new DBColumn("fisegestatuscode_id", DBType.LONG).setNullable(false),
                                      new DBColumn("errorcodeege_p", DBType.createVarchar(255)),
                                      new DBColumn("errormessageege_p", DBType.createVarchar(255))
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("fisCheckEgeSertificate");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fisCheckEntrantRequest

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("fischeckentrantrequest_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("checkdate_p", DBType.TIMESTAMP).setNullable(false),
                                      new DBColumn("entrantrequest_id", DBType.LONG).setNullable(false),
                                      new DBColumn("errorcode_p", DBType.createVarchar(255)),
                                      new DBColumn("errormessage_p", DBType.createVarchar(255)),
                                      new DBColumn("fisappstatuscode_id", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("fisCheckEntrantRequest");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fisEgeStatusCode

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("fisegestatuscode_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("title_p", DBType.createVarchar(1200))
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("fisEgeStatusCode");

        }


    }
}