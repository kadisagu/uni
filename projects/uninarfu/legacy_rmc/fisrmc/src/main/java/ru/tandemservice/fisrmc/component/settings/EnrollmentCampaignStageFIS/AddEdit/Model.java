package ru.tandemservice.fisrmc.component.settings.EnrollmentCampaignStageFIS.AddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.fisrmc.entity.catalog.StageFis;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.Date;
import java.util.List;

@Input({
        @Bind(key = "enrollmentCampaignId", binding = "enrollmentCampaignId"),
        @Bind(key = "entityId", binding = "entityId")
})

public class Model implements IEnrollmentCampaignSelectModel {

    private List<EnrollmentCampaign> enrollmentCampaignList;
    private EnrollmentCampaign enrollmentCampaign;
    private ISelectModel stageFisModel;
    private StageFis stageFis;
    private Date dateOrder;
    private Long enrollmentCampaignId;
    private Long entityId;

    public Long getEnrollmentCampaignId() {
        return enrollmentCampaignId;
    }

    public void setEnrollmentCampaignId(Long enrollmentCampaignId) {
        this.enrollmentCampaignId = enrollmentCampaignId;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public ISelectModel getStageFisModel() {
        return stageFisModel;
    }

    public void setStageFisModel(ISelectModel stageFisModel) {
        this.stageFisModel = stageFisModel;
    }

    public StageFis getStageFis() {
        return stageFis;
    }

    public void setStageFis(StageFis stageFis) {
        this.stageFis = stageFis;
    }

    public Date getDateOrder() {
        return dateOrder;
    }

    public void setDateOrder(Date dateOrder) {
        this.dateOrder = dateOrder;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign() {
        return enrollmentCampaign;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList() {
        return this.enrollmentCampaignList;
    }

    @Override
    public IDataSettings getSettings() {
        return null;
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentcampaign) {
        this.enrollmentCampaign = enrollmentcampaign;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> list) {
        this.enrollmentCampaignList = list;
    }

}
