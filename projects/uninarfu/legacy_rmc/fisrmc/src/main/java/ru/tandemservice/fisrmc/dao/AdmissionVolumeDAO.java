package ru.tandemservice.fisrmc.dao;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniecrmc.entity.entrant.EnrollmentDirectionExt;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.settings.TargetAdmissionPlanRelation;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

public class AdmissionVolumeDAO
        extends UniBaseDao
        implements IAdmissionVolumeDAO
{
    private static IAdmissionVolumeDAO _IAdmissionVolumeDAO = null;

    public static IAdmissionVolumeDAO Instanse()
    {
        if (_IAdmissionVolumeDAO == null)
            _IAdmissionVolumeDAO = (IAdmissionVolumeDAO) ApplicationRuntime.getBean("admissionVolumeDAO");
        return _IAdmissionVolumeDAO;
    }

    @Override
    public Long getNumberBudgetO(List<EnrollmentDirection> lstEnrolmentDirection) {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EnrollmentDirection.class, "e")
                        //План приема на бюджет
                .column(DQLFunctions.sum(DQLExpressions.property(EnrollmentDirection.ministerialPlan().fromAlias("e"))))
                        //Список направлений
                .where(DQLExpressions.in(DQLExpressions.property(EnrollmentDirection.id().fromAlias("e")), lstEnrolmentDirection))
                        //Очная форма
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().developForm().code().fromAlias("e")),
                                         DQLExpressions.value(DevelopFormCodes.FULL_TIME_FORM)));
        List<Object> list = builder.createStatement(getSession()).list();
        if (list != null && !list.isEmpty()) {
            Long sum = (Long) list.get(0);
            return sum != null ? sum : 0L;
        }
        else
            return 0L;
    }

    @Override
    public Long getNumberBudgetOZ(
            List<EnrollmentDirection> lstEnrolmentDirection)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EnrollmentDirection.class, "e")
                        //План приема на бюджет
                .column(DQLFunctions.sum(DQLExpressions.property(EnrollmentDirection.ministerialPlan().fromAlias("e"))))
                        //Список направлений
                .where(DQLExpressions.in(DQLExpressions.property(EnrollmentDirection.id().fromAlias("e")), lstEnrolmentDirection))
                        //очно-заочная
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().developForm().code().fromAlias("e")),
                                         DQLExpressions.value(DevelopFormCodes.PART_TIME_FORM)));
        List<Object> list = builder.createStatement(getSession()).list();
        if (list != null && !list.isEmpty()) {
            Long sum = (Long) list.get(0);
            return sum != null ? sum : 0L;
        }
        else
            return 0L;
    }

    @Override
    public Long getNumberBudgetZ(List<EnrollmentDirection> lstEnrolmentDirection) {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EnrollmentDirection.class, "e")
                        //План приема на бюджет
                .column(DQLFunctions.sum(DQLExpressions.property(EnrollmentDirection.ministerialPlan().fromAlias("e"))))
                        //Список направлений
                .where(DQLExpressions.in(DQLExpressions.property(EnrollmentDirection.id().fromAlias("e")), lstEnrolmentDirection))
                        // заочная
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().developForm().code().fromAlias("e")),
                                         DQLExpressions.value(DevelopFormCodes.CORESP_FORM)));
        List<Object> list = builder.createStatement(getSession()).list();
        if (list != null && !list.isEmpty()) {
            Long sum = (Long) list.get(0);
            return sum != null ? sum : 0L;
        }
        else
            return 0L;
    }

    @Override
    public Long getNumberPaidO(List<EnrollmentDirection> lstEnrolmentDirection) {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EnrollmentDirection.class, "e")
                        //План приема c оплатой
                .column(DQLFunctions.sum(DQLExpressions.property(EnrollmentDirection.contractPlan().fromAlias("e"))))
                        //Список направлений
                .where(DQLExpressions.in(DQLExpressions.property(EnrollmentDirection.id().fromAlias("e")), lstEnrolmentDirection))
                        //Очная форма
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().developForm().code().fromAlias("e")),
                                         DQLExpressions.value(DevelopFormCodes.FULL_TIME_FORM)));
        List<Object> list = builder.createStatement(getSession()).list();
        if (list != null && !list.isEmpty()) {
            Long sum = (Long) list.get(0);
            return sum != null ? sum : 0L;
        }
        else
            return 0L;
    }

    @Override
    public Long getNumberPaidOZ(List<EnrollmentDirection> lstEnrolmentDirection) {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EnrollmentDirection.class, "e")
                        //План приема c оплатой
                .column(DQLFunctions.sum(DQLExpressions.property(EnrollmentDirection.contractPlan().fromAlias("e"))))
                        //Список направлений
                .where(DQLExpressions.in(DQLExpressions.property(EnrollmentDirection.id().fromAlias("e")), lstEnrolmentDirection))
                        //очно-заочная
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().developForm().code().fromAlias("e")),
                                         DQLExpressions.value(DevelopFormCodes.PART_TIME_FORM)));
        List<Object> list = builder.createStatement(getSession()).list();
        if (list != null && !list.isEmpty()) {
            Long sum = (Long) list.get(0);
            return sum != null ? sum : 0L;
        }
        else
            return 0L;
    }

    @Override
    public Long getNumberPaidZ(List<EnrollmentDirection> lstEnrolmentDirection) {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EnrollmentDirection.class, "e")
                        //План приема c оплатой
                .column(DQLFunctions.sum(DQLExpressions.property(EnrollmentDirection.contractPlan().fromAlias("e"))))
                        //Список направлений
                .where(DQLExpressions.in(DQLExpressions.property(EnrollmentDirection.id().fromAlias("e")), lstEnrolmentDirection))
                        //заочная
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().developForm().code().fromAlias("e")),
                                         DQLExpressions.value(DevelopFormCodes.CORESP_FORM)));
        List<Object> list = builder.createStatement(getSession()).list();
        if (list != null && !list.isEmpty()) {
            Long sum = (Long) list.get(0);
            return sum != null ? sum : 0L;
        }
        else
            return 0L;
    }

    @Override
    public Long getNumberTargetO(List<EnrollmentDirection> lstEnrolmentDirection) {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EnrollmentDirection.class, "e")
                        //План приема по целевому приему по бюджету
                .column(DQLFunctions.sum(DQLExpressions.property(EnrollmentDirection.targetAdmissionPlanBudget().fromAlias("e"))))
                        //План приема по целевому приему по договору
                .column(DQLFunctions.sum(DQLExpressions.property(EnrollmentDirection.targetAdmissionPlanContract().fromAlias("e"))))
                        //Список направлений
                .where(DQLExpressions.in(DQLExpressions.property(EnrollmentDirection.id().fromAlias("e")), lstEnrolmentDirection))
                        //очная
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().developForm().code().fromAlias("e")),
                                         DQLExpressions.value(DevelopFormCodes.FULL_TIME_FORM)));
        List<Object> list = builder.createStatement(getSession()).list();


        if (list != null && !list.isEmpty()) {

            // результат в 2-х столбцах
            Object[] longVal = (Object[]) list.get(0);

            Long sumTargetAdmissionPlanBudget = ((Long) longVal[0]) != null ? ((Long) longVal[0]) : 0L;
            Long sumTargetAdmissionPlanContract = ((Long) longVal[1]) != null ? ((Long) longVal[1]) : 0L;
            Long total = sumTargetAdmissionPlanBudget + sumTargetAdmissionPlanContract;
            return total;
        }
        else
            return 0L;
    }

    @Override
    public Long getNumberTargetOZ(
            List<EnrollmentDirection> lstEnrolmentDirection)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EnrollmentDirection.class, "e")
                        //План приема по целевому приему по бюджету
                .column(DQLFunctions.sum(DQLExpressions.property(EnrollmentDirection.targetAdmissionPlanBudget().fromAlias("e"))))
                        //План приема по целевому приему по договору
                .column(DQLFunctions.sum(DQLExpressions.property(EnrollmentDirection.targetAdmissionPlanContract().fromAlias("e"))))
                        //Список направлений
                .where(DQLExpressions.in(DQLExpressions.property(EnrollmentDirection.id().fromAlias("e")), lstEnrolmentDirection))
                        //очно-заочная
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().developForm().code().fromAlias("e")),
                                         DQLExpressions.value(DevelopFormCodes.PART_TIME_FORM)));
        List<Object> list = builder.createStatement(getSession()).list();

        if (list != null && !list.isEmpty()) {
            // результат в 2-х столбцах
            Object[] longVal = (Object[]) list.get(0);

            Long sumTargetAdmissionPlanBudget = ((Long) longVal[0]) != null ? ((Long) longVal[0]) : 0L;
            Long sumTargetAdmissionPlanContract = ((Long) longVal[1]) != null ? ((Long) longVal[1]) : 0L;
            Long total = sumTargetAdmissionPlanBudget + sumTargetAdmissionPlanContract;
            return total;
        }
        else
            return 0L;
    }

    @Override
    public Long getNumberTargetZ(List<EnrollmentDirection> lstEnrolmentDirection) {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EnrollmentDirection.class, "e")
                        //План приема по целевому приему по бюджету
                .column(DQLFunctions.sum(DQLExpressions.property(EnrollmentDirection.targetAdmissionPlanBudget().fromAlias("e"))))
                        //План приема по целевому приему по договору
                .column(DQLFunctions.sum(DQLExpressions.property(EnrollmentDirection.targetAdmissionPlanContract().fromAlias("e"))))
                        //Список направлений
                .where(DQLExpressions.in(DQLExpressions.property(EnrollmentDirection.id().fromAlias("e")), lstEnrolmentDirection))
                        //заочная
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().developForm().code().fromAlias("e")),
                                         DQLExpressions.value(DevelopFormCodes.CORESP_FORM)));
        List<Object> list = builder.createStatement(getSession()).list();

        if (list != null && !list.isEmpty()) {

            // результат в 2-х столбцах
            Object[] longVal = (Object[]) list.get(0);

            Long sumTargetAdmissionPlanBudget = ((Long) longVal[0]) != null ? ((Long) longVal[0]) : 0L;
            Long sumTargetAdmissionPlanContract = ((Long) longVal[1]) != null ? ((Long) longVal[1]) : 0L;
            Long total = sumTargetAdmissionPlanBudget + sumTargetAdmissionPlanContract;
            return total;
        }
        else
            return 0L;
    }


    private Long _getNumberTargetByCode(TargetAdmissionKind targetadmissionkind
            , String catalogCode
            , List<EnrollmentDirection> edlist)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EnrollmentDirection.class, "e")
                .column("e")
                .where(DQLExpressions.in(DQLExpressions.property(EnrollmentDirection.id().fromAlias("e")), edlist))
                        //очная
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().developForm().code().fromAlias("e")),
                                         DQLExpressions.value(catalogCode)));

        List<EnrollmentDirection> edIn = builder.createStatement(getSession()).list();

        if (edIn == null || edIn.size() == 0) {
            System.out.println("edIn.size()==0");
            return 0L;
        }

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(TargetAdmissionPlanRelation.class, "rel")
                .column(DQLFunctions.sum(DQLExpressions.property(TargetAdmissionPlanRelation.planValue().fromAlias("rel"))))
                .where(DQLExpressions.eq(DQLExpressions.property(TargetAdmissionPlanRelation.targetAdmissionKind().fromAlias("rel")), DQLExpressions.value(targetadmissionkind)))
                .where(DQLExpressions.in(DQLExpressions.property(TargetAdmissionPlanRelation.entranceEducationOrgUnit().id().fromAlias("rel")), edIn));
        List<Object> list = dql.createStatement(getSession()).list();

        // если считаем по ЦП (корневая организация, то нужно вычесть сведения о всех ее потомках)

        Long longAdjustment = 0L;
        if (targetadmissionkind.getParent() == null) {
            // нужно расчитать поправку
            DQLSelectBuilder dqlIn = new DQLSelectBuilder();
            dqlIn.fromEntity(TargetAdmissionKind.class, "tak");
            dqlIn.column("tak");
            dqlIn.where(DQLExpressions.eq(property(TargetAdmissionKind.parent().fromAlias("tak")), value(targetadmissionkind)));


            DQLSelectBuilder dqlAdjustment = new DQLSelectBuilder()
                    .fromEntity(TargetAdmissionPlanRelation.class, "rel")
                    .column(DQLFunctions.sum(DQLExpressions.property(TargetAdmissionPlanRelation.planValue().fromAlias("rel"))))

                    .where(DQLExpressions.in(DQLExpressions.property(TargetAdmissionPlanRelation.targetAdmissionKind().fromAlias("rel")), dqlIn.buildQuery()))
                    .where(DQLExpressions.in(DQLExpressions.property(TargetAdmissionPlanRelation.entranceEducationOrgUnit().id().fromAlias("rel")), edIn));

            List<Object> listAdjustment = dqlAdjustment.createStatement(getSession()).list();
            if (listAdjustment != null && !listAdjustment.isEmpty()) {
                longAdjustment = (Long) listAdjustment.get(0);
                if (longAdjustment == null)
                    longAdjustment = 0L;
            }

        }


        if (list != null && !list.isEmpty()) {
            // результат в 2-х столбцах
            Long longVal = (Long) list.get(0);
            if (longVal == null)
                longVal = 0L;

            longVal = longVal - longAdjustment;
            if (longVal < 0)
                longVal = 0L;

            return longVal;
        }
        else
            return 0L;

    }


    @Override
    public Long getNumberTargetO(TargetAdmissionKind targetadmissionkind,
                                 List<EnrollmentDirection> edlist)
    {
        return _getNumberTargetByCode(targetadmissionkind, DevelopFormCodes.FULL_TIME_FORM, edlist);
    }

    @Override
    public Long getNumberTargetOZ(TargetAdmissionKind targetadmissionkind,
                                  List<EnrollmentDirection> edlist)
    {
        return _getNumberTargetByCode(
                targetadmissionkind
                , DevelopFormCodes.PART_TIME_FORM
                , edlist);

    }

    @Override
    public Long getNumberTargetZ(TargetAdmissionKind targetadmissionkind,
                                 List<EnrollmentDirection> edlist)
    {
        return _getNumberTargetByCode(
                targetadmissionkind
                , DevelopFormCodes.CORESP_FORM
                , edlist);
    }

    @Override
    public Long getNumberQuotaO(List<EnrollmentDirection> lstEnrolmentDirection) {
        return getNumberQuota(lstEnrolmentDirection, DevelopFormCodes.FULL_TIME_FORM);
    }

    private Long getNumberQuota(List<EnrollmentDirection> lstEnrolmentDirection, String developFormCode)
    {

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EnrollmentDirectionExt.class, "e")
                        //План приема на бюджет
                .column(DQLFunctions.sum(DQLExpressions.property(EnrollmentDirectionExt.haveSpecialRightsPlanBudget().fromAlias("e"))))
                        //План приема по контракту
                .column(DQLFunctions.sum(DQLExpressions.property(EnrollmentDirectionExt.haveSpecialRightsPlanContract().fromAlias("e"))))

                        //Список направлений
                .where(DQLExpressions.in(DQLExpressions.property(EnrollmentDirectionExt.enrollmentDirection().id().fromAlias("e")), lstEnrolmentDirection))

                        //очная форма
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirectionExt.enrollmentDirection().educationOrgUnit().developForm().code().fromAlias("e")),
                                         DQLExpressions.value(developFormCode)));
        List<Object[]> list = builder.createStatement(getSession()).list();

        if (list != null && !list.isEmpty()) {
            Long sum_1 = (Long) list.get(0)[0]; // бюджет
            Long sum_2 = (Long) list.get(0)[1]; // договор

            Long sum = 0L;
            if (sum_1 != null)
                sum += sum_1;

            if (sum_2 != null)
                sum += sum_2;
            return sum;
        }
        else
            return 0L;


    }

    @Override
    public Long getNumberQuotaOZ(List<EnrollmentDirection> lstEnrolmentDirection) {
        return getNumberQuota(lstEnrolmentDirection, DevelopFormCodes.PART_TIME_FORM);

    }

    @Override
    public Long getNumberQuotaZ(List<EnrollmentDirection> lstEnrolmentDirection) {
        return getNumberQuota(lstEnrolmentDirection, DevelopFormCodes.CORESP_FORM);
    }
}
