package ru.tandemservice.fisrmc.dao.packApplications;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.tandemservice.uniec.entity.entrant.Entrant;


/**
 * Упаковать данные по абитуриенту
 * страница 13 спецификации сервиса
 *
 * @author vch
 */
public interface IEntrantPack {
    public Element packEntrant
            (
                    Document doc
                    , Entrant entrant
            );
}
