package ru.tandemservice.fisrmc.component.dataChecking.FisCheckEgeSertificate.List;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import ru.tandemservice.fisrmc.entity.FisCheckEgeSertificate;
import ru.tandemservice.fisrmc.entity.catalog.FisEgeStatusCode;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.*;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.setFisEgeStatusCodeModel(new LazySimpleSelectModel<>(FisEgeStatusCode.class).setSortProperty("title"));
        model.setErrorCodeModel(new LazySimpleSelectModel<>(getErrorCodeList()));
    }


    private List<DataWrapper> getErrorCodeList() {
        MQBuilder builder = new MQBuilder(FisCheckEgeSertificate.ENTITY_CLASS, "e")
                .addOrder("e", FisCheckEgeSertificate.errorCodeEge());
        builder.getSelectAliasList().clear();
        builder.addSelect(FisCheckEgeSertificate.errorCodeEge().s());
        builder.setNeedDistinct(true);
        List<Integer> errorList = getList(builder);

        List<DataWrapper> result = new ArrayList<>();
        long id = 1L;

        for (Integer errorCode : errorList) {
            DataWrapper dw = new DataWrapper(id++, Integer.toString(errorCode));
            result.add(dw);
        }

        return result;
    }

    @Override
    public void prepareListDataSource(Model model) {
        DynamicListDataSource dataSource = model.getDataSource();

        DQLSelectBuilder builder = createBuilder(model);

        DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(FisCheckEgeSertificate.class, "fes");
        orderRegistry.applyOrder(builder, model.getDataSource().getEntityOrder());

        UniBaseUtils.createPage(dataSource, builder, getSession());
        List<FisCheckEgeSertificate> l = getList(builder);
        List<Long> list = new ArrayList<>();
        for (FisCheckEgeSertificate el : l) {
            list.add(el.getEntrantStateExamCertificate().getEntrant().getId());
        }

        DQLSelectBuilder selectBuilder = new DQLSelectBuilder().fromEntity(EntrantRequest.class, "er")
                .where(DQLExpressions.in(DQLExpressions.property(EntrantRequest.entrant().id().fromAlias("er")), list));

        List<EntrantRequest> entrantRequests = getList(selectBuilder);

        Map map = new Hashtable();
        for (EntrantRequest request : entrantRequests) {
            List numberList = (List) map.get(request.getEntrant());
            if (numberList == null)
                map.put(request.getEntrant(), numberList = new ArrayList());
            numberList.add(request.getStringNumber());
        }

        for (ViewWrapper view : ViewWrapper.getPatchedList(model.getDataSource())) {
            List numberList = (List) map.get(((FisCheckEgeSertificate) view.getEntity()).getEntrantStateExamCertificate().getEntrant());
            view.setViewProperty("requestNumber", numberList == null ? "" : StringUtils.join(numberList, ", "));
        }

    }

    protected DQLSelectBuilder createBuilder(Model model) {
        IDataSettings settings = model.getSettings();

        Date fromDate = settings.get("fromDate");
        Date toDate = settings.get("toDate");
        EnrollmentCampaign enrollmentCampaign = settings.get("enrollmentCampaign");
        List<FisEgeStatusCode> fisEgeStatusCodeFilter = settings.get("fisEgeStatusCodeFilter");
        List<DataWrapper> errorCodeFilter = settings.get("errorCodeFilter");
        Number requestNumberFilter = settings.get("requestNumberFilter");
        String requestEgeNumberFilter = settings.get("requestEgeNumberFilter");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FisCheckEgeSertificate.class, "fes")
                .where(DQLExpressions.eq(DQLExpressions.property(FisCheckEgeSertificate.entrantStateExamCertificate().entrant().enrollmentCampaign().fromAlias("fes")), DQLExpressions.value(enrollmentCampaign)));
        if (fromDate != null)
            builder.where(DQLExpressions.ge(
                    DQLExpressions.property(FisCheckEgeSertificate.checkDate().fromAlias("fes")),
                    DQLExpressions.valueDate(fromDate)
            ));
        if (toDate != null)
            builder.where(DQLExpressions.le(
                    DQLExpressions.property(FisCheckEgeSertificate.checkDate().fromAlias("fes")),
                    DQLExpressions.valueDate(toDate)
            ));

        FilterUtils.applySelectFilter(builder, "fes", FisCheckEgeSertificate.fisEgeStatusCode().s(), fisEgeStatusCodeFilter);

        if (errorCodeFilter != null && !errorCodeFilter.isEmpty()) {
            List<Integer> errorList = new ArrayList<>();
            for (DataWrapper dataWrapper : errorCodeFilter) {
                if (dataWrapper.getTitle() != null && !dataWrapper.getTitle().isEmpty())
                    errorList.add(Integer.parseInt(dataWrapper.getTitle()));
            }

            FilterUtils.applySelectFilter(builder, "fes", FisCheckEgeSertificate.errorCodeEge().s(), errorList);
        }

        if (requestNumberFilter != null) {
            MQBuilder mqBuilder = new MQBuilder(EntrantRequest.ENTITY_CLASS, "er")
                    .add(MQExpression.eq("er", EntrantRequest.regNumber(), requestNumberFilter.intValue()));
            mqBuilder.getSelectAliasList().clear();
            mqBuilder.addSelect(EntrantRequest.entrant().id().fromAlias("er").s());
            List<Long> list = getList(mqBuilder);
            builder.where(DQLExpressions.in(DQLExpressions.property(FisCheckEgeSertificate.entrantStateExamCertificate().entrant().id().fromAlias("fes")), list));
        }

        FilterUtils.applySimpleLikeFilter(builder, "fes", FisCheckEgeSertificate.entrantStateExamCertificate().number().s(), requestEgeNumberFilter);

        return builder;
    }

    public RtfDocument createDocument(Model model) {
        IPrintFormCreator componentPrint = (IPrintFormCreator) ApplicationRuntime.getBean("fisCheckEgeSertificate");

        TemplateDocument template = getCatalogItem(TemplateDocument.class, "fisCheckEgeSertificate");

        return componentPrint.createPrintForm(template.getContent(), model);
    }


}
