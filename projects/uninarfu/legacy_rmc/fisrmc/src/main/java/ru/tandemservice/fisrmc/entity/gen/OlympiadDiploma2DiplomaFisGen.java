package ru.tandemservice.fisrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.fisrmc.entity.OlympiadDiploma2DiplomaFis;
import ru.tandemservice.fisrmc.entity.catalog.Olimpiatfis;
import ru.tandemservice.uniec.entity.entrant.OlympiadDiploma;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность-связь Олимпиады и Олимпиады ФИС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OlympiadDiploma2DiplomaFisGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.fisrmc.entity.OlympiadDiploma2DiplomaFis";
    public static final String ENTITY_NAME = "olympiadDiploma2DiplomaFis";
    public static final int VERSION_HASH = -1477216929;
    private static IEntityMeta ENTITY_META;

    public static final String L_OLYMPIAD_DIPLOMA = "olympiadDiploma";
    public static final String L_OLIMPIATFIS = "olimpiatfis";

    private OlympiadDiploma _olympiadDiploma;     // Диплом участника олимпиады
    private Olimpiatfis _olimpiatfis;     // Олимпиады

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Диплом участника олимпиады. Свойство не может быть null.
     */
    @NotNull
    public OlympiadDiploma getOlympiadDiploma()
    {
        return _olympiadDiploma;
    }

    /**
     * @param olympiadDiploma Диплом участника олимпиады. Свойство не может быть null.
     */
    public void setOlympiadDiploma(OlympiadDiploma olympiadDiploma)
    {
        dirty(_olympiadDiploma, olympiadDiploma);
        _olympiadDiploma = olympiadDiploma;
    }

    /**
     * @return Олимпиады. Свойство не может быть null.
     */
    @NotNull
    public Olimpiatfis getOlimpiatfis()
    {
        return _olimpiatfis;
    }

    /**
     * @param olimpiatfis Олимпиады. Свойство не может быть null.
     */
    public void setOlimpiatfis(Olimpiatfis olimpiatfis)
    {
        dirty(_olimpiatfis, olimpiatfis);
        _olimpiatfis = olimpiatfis;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OlympiadDiploma2DiplomaFisGen)
        {
            setOlympiadDiploma(((OlympiadDiploma2DiplomaFis)another).getOlympiadDiploma());
            setOlimpiatfis(((OlympiadDiploma2DiplomaFis)another).getOlimpiatfis());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OlympiadDiploma2DiplomaFisGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OlympiadDiploma2DiplomaFis.class;
        }

        public T newInstance()
        {
            return (T) new OlympiadDiploma2DiplomaFis();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "olympiadDiploma":
                    return obj.getOlympiadDiploma();
                case "olimpiatfis":
                    return obj.getOlimpiatfis();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "olympiadDiploma":
                    obj.setOlympiadDiploma((OlympiadDiploma) value);
                    return;
                case "olimpiatfis":
                    obj.setOlimpiatfis((Olimpiatfis) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "olympiadDiploma":
                        return true;
                case "olimpiatfis":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "olympiadDiploma":
                    return true;
                case "olimpiatfis":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "olympiadDiploma":
                    return OlympiadDiploma.class;
                case "olimpiatfis":
                    return Olimpiatfis.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OlympiadDiploma2DiplomaFis> _dslPath = new Path<OlympiadDiploma2DiplomaFis>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OlympiadDiploma2DiplomaFis");
    }
            

    /**
     * @return Диплом участника олимпиады. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.OlympiadDiploma2DiplomaFis#getOlympiadDiploma()
     */
    public static OlympiadDiploma.Path<OlympiadDiploma> olympiadDiploma()
    {
        return _dslPath.olympiadDiploma();
    }

    /**
     * @return Олимпиады. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.OlympiadDiploma2DiplomaFis#getOlimpiatfis()
     */
    public static Olimpiatfis.Path<Olimpiatfis> olimpiatfis()
    {
        return _dslPath.olimpiatfis();
    }

    public static class Path<E extends OlympiadDiploma2DiplomaFis> extends EntityPath<E>
    {
        private OlympiadDiploma.Path<OlympiadDiploma> _olympiadDiploma;
        private Olimpiatfis.Path<Olimpiatfis> _olimpiatfis;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Диплом участника олимпиады. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.OlympiadDiploma2DiplomaFis#getOlympiadDiploma()
     */
        public OlympiadDiploma.Path<OlympiadDiploma> olympiadDiploma()
        {
            if(_olympiadDiploma == null )
                _olympiadDiploma = new OlympiadDiploma.Path<OlympiadDiploma>(L_OLYMPIAD_DIPLOMA, this);
            return _olympiadDiploma;
        }

    /**
     * @return Олимпиады. Свойство не может быть null.
     * @see ru.tandemservice.fisrmc.entity.OlympiadDiploma2DiplomaFis#getOlimpiatfis()
     */
        public Olimpiatfis.Path<Olimpiatfis> olimpiatfis()
        {
            if(_olimpiatfis == null )
                _olimpiatfis = new Olimpiatfis.Path<Olimpiatfis>(L_OLIMPIATFIS, this);
            return _olimpiatfis;
        }

        public Class getEntityClass()
        {
            return OlympiadDiploma2DiplomaFis.class;
        }

        public String getEntityName()
        {
            return "olympiadDiploma2DiplomaFis";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
