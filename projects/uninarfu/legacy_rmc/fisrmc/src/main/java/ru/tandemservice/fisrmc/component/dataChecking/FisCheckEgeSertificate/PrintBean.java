package ru.tandemservice.fisrmc.component.dataChecking.FisCheckEgeSertificate;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.fisrmc.component.dataChecking.FisCheckEgeSertificate.List.Model;
import ru.tandemservice.fisrmc.entity.FisCheckEgeSertificate;

import java.util.ArrayList;
import java.util.List;

public class PrintBean implements IPrintFormCreator<Model> {

    @Override
    public RtfDocument createPrintForm(byte[] template, Model model) {

        RtfDocument document = new RtfReader().read(template);
        createTableModifier(model).modify(document);

        return document;
    }

    protected RtfTableModifier createTableModifier(Model model)
    {
        RtfTableModifier tableModifier = new RtfTableModifier();
        List<ViewWrapper<FisCheckEgeSertificate>> entityList = model.getDataSource().getEntityList();
        List<String[]> list = new ArrayList<String[]>();
        int i = 0;
        for (ViewWrapper<FisCheckEgeSertificate> view : entityList) {

            String[] row = new String[]{
                    String.valueOf(++i),
                    RussianDateFormatUtils.STANDARD_DATE_TIME_FORMAT.format(view.getEntity().getCheckDate()),
                    (String) view.getProperty("requestNumber"),
                    String.valueOf(view.getEntity().getEntrantStateExamCertificate().getNumber()),
                    view.getEntity().getEntrantStateExamCertificate().getEntrant().getPerson().getFullFio(),
                    view.getEntity().getFisEgeStatusCode().getTitle(),
                    Integer.toString(view.getEntity().getErrorCodeEge()),
                    view.getEntity().getErrorMessageEge()
            };
            list.add(row);
        }
        tableModifier.put("T", list.toArray(new String[][]{}));
        return tableModifier;
    }

}
