package ru.tandemservice.unioxrmc.dao;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.unioxrmc.ws.types.PersonIdFioPair;

import java.util.List;

public interface IPersonDAO {
    public List<PersonIdFioPair> getFioDeclinations(List<Long> personIds, GrammaCase grammaCase);
}
