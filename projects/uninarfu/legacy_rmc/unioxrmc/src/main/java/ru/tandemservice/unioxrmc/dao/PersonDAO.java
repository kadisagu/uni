package ru.tandemservice.unioxrmc.dao;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unibasermc.util.DeclinationUtil;
import ru.tandemservice.unioxrmc.ws.types.PersonIdFioPair;
import ru.tandemservice.uni.dao.UniBaseDao;

import java.util.ArrayList;
import java.util.List;

public class PersonDAO extends UniBaseDao implements IPersonDAO {
    private static IPersonDAO _personDAO = null;

    public static IPersonDAO Instance() {
        if (_personDAO == null) {
            _personDAO = (IPersonDAO) ApplicationRuntime.getBean("personDao");
        }
        return _personDAO;
    }

    @Override
    public List<PersonIdFioPair> getFioDeclinations(List<Long> personIds, GrammaCase grammaCase) {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(Person.class, "p");
        dql.addColumn(DQLExpressions.property(Person.id().fromAlias("p").s()))
                .addColumn(DQLExpressions.property(Person.identityCard().fromAlias("p").s()));

        dql.where(DQLExpressions.in(DQLExpressions.property(Person.id().fromAlias("p")), personIds));
        List<Object[]> lines = dql.createStatement(getSession()).list();

        List<PersonIdFioPair> result = new ArrayList<PersonIdFioPair>();
        for (Object[] line : lines) {
            IdentityCard card = (IdentityCard) line[1];
            PersonIdFioPair p = new PersonIdFioPair(
                    (Long) line[0],
                    DeclinationUtil.getDeclinableLastName(grammaCase, card),
                    DeclinationUtil.getDeclinableFirstName(grammaCase, card),
                    DeclinationUtil.getDeclinableMiddleName(grammaCase, card)
            );
            result.add(p);
        }

        return result;
    }
}
