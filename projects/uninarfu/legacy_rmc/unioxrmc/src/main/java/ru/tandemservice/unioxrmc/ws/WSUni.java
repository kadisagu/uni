package ru.tandemservice.unioxrmc.ws;

import org.tandemframework.core.CoreServices;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.sec.runtime.SecurityRuntime;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.person.base.entity.PersonRole;
import org.tandemframework.tapsupport.HttpSessionStorage;
import org.tandemframework.tapsupport.OnlineUser;
import ru.tandemservice.unioxrmc.dao.PersonDAO;
import ru.tandemservice.unioxrmc.ws.types.IdsList;
import ru.tandemservice.unioxrmc.ws.types.PersonIdFioPair;
import ru.tandemservice.unioxrmc.ws.types.PersonList;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;


@WebService(serviceName = "WSUni")
public class WSUni {

    @WebMethod
    public String getTestMessage()
    {
        return "testMessage";
    }

    private UserContext getUserContext(String uniHttpSession)
    {
        List<OnlineUser> activeUsers = HttpSessionStorage.getActiveUsers();
        for (OnlineUser user : activeUsers) {
            if (user.getSessionId().equals(uniHttpSession))
                return user.getUserContext();
        }
        return null;
    }

    @WebMethod
    public Long getPrincipalContextId(@WebParam(name = "uniHttpSession") String uniHttpSession)
    {
        UserContext uc = getUserContext(uniHttpSession);
        if (uc == null)
            return -1L;
        else
            return uc.getPrincipalContext().getId();
    }

    @WebMethod
    public String getLoginName(@WebParam(name = "uniHttpSession") String uniHttpSession)
    {
        UserContext uc = getUserContext(uniHttpSession);
        if (uc == null)
            return "";
        else
            return uc.getPrincipalContext().getPrincipal().getLogin();
    }

    @WebMethod
    public Long getPersonRoleId(@WebParam(name = "uniHttpSession") String uniHttpSession)
    {
        Long uniPersonRoleId = -1L;
        UserContext uc = getUserContext(uniHttpSession);
        if (uc == null)
            return uniPersonRoleId;
        else {
            DQLSelectBuilder builder = new DQLSelectBuilder();
            builder.fromEntity(PersonRole.class, "pr")
                    .column("pr")
                    .where(DQLExpressions.eq(DQLExpressions.property(PersonRole.principal().fromAlias("pr")),
                                             DQLExpressions.value(uc.getPrincipal())));
            List<PersonRole> prList = IUniBaseDao.instance.get().<PersonRole>getList(builder);
            if (prList.size() > 0)
                uniPersonRoleId = prList.get(0).getId();
            return uniPersonRoleId;
        }
    }


    @WebMethod
    public Boolean check(@WebParam(name = "uniHttpSession") String uniHttpSession, @WebParam(name = "permKey") String permKey)
    {
        UserContext uc = getUserContext(uniHttpSession);

        //Подсунули липовую сессию - до свиданья
        if (uc == null)
            return false;

        //Админам все можно
        boolean isAdm = uc.getPrincipalContext().isAdmin();
        if (isAdm)
            return true;

        return CoreServices.securityService().check(SecurityRuntime.getInstance().getCommonSecurityObject(), uc.getPrincipalContext(), permKey);

    }

    @WebMethod
    public Boolean deleteEntity(@WebParam(name = "uniHttpSession") String uniHttpSession, @WebParam(name = "entityId") Long entityId) {
        UserContext uc = getUserContext(uniHttpSession);

        if (uc == null) {
            return false;
        }

        try {
            UniDaoFacade.getCoreDao().delete(entityId);
        }
        catch (Exception e) {
            return false;
        }
        return true;
    }

    @WebMethod
    public PersonList getDeclinationPersonFIO(@WebParam(name = "uniHttpSession") String uniHttpSession
            , @WebParam(name = "grammaCaseName") String grammaCaseName
            , @WebParam(name = "idsList") IdsList idsList)
    {

        PersonList result = new PersonList();
        UserContext uc = getUserContext(uniHttpSession);

        //if(uc != null) {
        List<PersonIdFioPair> lst = PersonDAO.Instance().getFioDeclinations(idsList.getIds(), GrammaCase.valueOf(grammaCaseName));
        result.setPersons(lst);
        //}
        return result;
    }
}
