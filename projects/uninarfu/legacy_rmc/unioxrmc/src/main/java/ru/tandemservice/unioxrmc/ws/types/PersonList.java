package ru.tandemservice.unioxrmc.ws.types;

import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlType(name = "PersonList")
public class PersonList {
    private List<PersonIdFioPair> persons;

    public List<PersonIdFioPair> getPersons() {
        return persons;
    }

    public void setPersons(List<PersonIdFioPair> persons) {
        this.persons = persons;
    }
}
