package ru.tandemservice.unioxrmc.component.ox.SimpleFrame;

import org.tandemframework.core.context.UserContext;
import ru.tandemservice.uni.dao.UniDao;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        UserContext uc = UserContext.getInstance();
        String uniHttpSession = uc.getHttpSessionId();
        String publisherIdPart = model.getPublisherId() == null ? "" : "&soft=TU&" + model.getPublisherIdParameterName() + "=" + model.getPublisherId().toString();
        String searchPropertyPart = model.getSearchProperty() == null ? "" : "&searchProperty=" + model.getSearchProperty();

        model.setJsText("document.writeln('<iframe name=\"oxFrame\" id=\"oxFrame\" src=\"" + model.getOxUrl() + "&locale=ru&uniHttpSession=" + uniHttpSession + publisherIdPart + searchPropertyPart + "\" width=\"100%\" height=\"" + model.getOxHeight() + "\" frameborder=\"0\" > </iframe>');");
        //model.setJsText("document.writeln('<iframe name=\"oxFrame\" id=\"oxFrame\" src=\"../RamecVuzBase/AccessDenied.html\" width=\"100%\" height=\"500\" frameborder=\"0\" > </iframe>');");
        //model.setJsText("document.writeln('<h2>tttttttt</h2>');");
    }

}
