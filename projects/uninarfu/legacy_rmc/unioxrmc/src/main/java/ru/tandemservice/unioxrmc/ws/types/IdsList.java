package ru.tandemservice.unioxrmc.ws.types;

import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlType(name = "IdsList")
public class IdsList {
    private List<Long> ids;

    public List<Long> getIds() {
        return ids;
    }

    public void setIds(List<Long> ids) {
        this.ids = ids;
    }
}
