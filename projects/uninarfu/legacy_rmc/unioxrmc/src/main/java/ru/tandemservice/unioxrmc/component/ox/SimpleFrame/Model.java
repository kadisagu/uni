package ru.tandemservice.unioxrmc.component.ox.SimpleFrame;

import org.tandemframework.core.component.State;


@State(keys = {"publisherId", "publisherIdParameterName"}, bindings = {"publisherId", "publisherIdParameterName"})
public class Model {
    private String oxUrl;
    private String jsText;
    private Long publisherId;

    /**
     * Задание высоты экрана
     */
    private Integer oxHeight = 680;

    /**
     * имя параметра, в котором в URL будет передан id текущей записи
     */
    private String publisherIdParameterName = "recId";

    /**
     * имя параметра в URL, для передачи имени св-ва, по которому осуществлять поиск записи в OX. если null, то ищет по id
     */
    private String searchProperty = null;

    /**
     * Отображать или нет заголовок компонента TU
     */
    private boolean showTitle = true;

    public void setOxUrl(String oxUrl) {
        this.oxUrl = oxUrl;
    }

    public String getOxUrl() {
        return oxUrl;
    }

    public void setJsText(String jsText) {
        this.jsText = jsText;
    }

    public String getJsText() {
        return jsText;
    }

    public void setPublisherId(Long publisherId) {
        this.publisherId = publisherId;
    }

    public Long getPublisherId() {
        return publisherId;
    }

    public String getPublisherIdParameterName() {
        return publisherIdParameterName;
    }

    public void setPublisherIdParameterName(String publisherIdParameterName) {
        this.publisherIdParameterName = publisherIdParameterName;
    }

    public boolean isShowTitle() {
        return showTitle;
    }

    public void setShowTitle(boolean showTitle) {
        this.showTitle = showTitle;
    }

    public String getSearchProperty() {
        return searchProperty;
    }

    public void setSearchProperty(String searchProperty) {
        this.searchProperty = searchProperty;
    }

    public Integer getOxHeight() {
        return oxHeight;
    }

    public void setOxHeight(Integer oxHeight) {
        this.oxHeight = oxHeight;
    }
}
