package ru.tandemservice.unioxrmc.ws.types;

import javax.xml.bind.annotation.XmlType;

@XmlType(name = "PersonIdFioPair")
public class PersonIdFioPair {
    public PersonIdFioPair() {
    }

    public PersonIdFioPair(Long id, String lName, String fName, String mName) {
        this.id = id;
        this.lName = lName;
        this.fName = fName;
        this.mName = mName;
    }

    private Long id;
    private String lName;
    private String fName;
    private String mName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }
}
