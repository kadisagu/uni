package ru.tandemservice.eventssecrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_eventssecrmc_1x0x0_2to3 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.11"),
                        new ScriptDependency("org.tandemframework.shared", "1.2.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.2.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception {
        if (tool.tableExists("enrollmentcampaignsecext_t") && !tool.columnExists("enrollmentcampaignsecext_t", "hidden_p")) {
            tool.createColumn("enrollmentcampaignsecext_t", new DBColumn("hidden_p", DBType.BOOLEAN));
            // задать значение по умолчанию
            java.lang.Boolean defaultHidden = false;
            tool.executeUpdate("update enrollmentcampaignsecext_t set hidden_p=? where hidden_p is null", defaultHidden);

            // сделать колонку NOT NULL
            tool.setColumnNullable("enrollmentcampaignsecext_t", "hidden_p", false);

        }


    }
}