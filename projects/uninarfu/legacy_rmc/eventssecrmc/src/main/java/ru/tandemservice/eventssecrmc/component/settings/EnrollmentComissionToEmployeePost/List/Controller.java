package ru.tandemservice.eventssecrmc.component.settings.EnrollmentComissionToEmployeePost.List;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.eventssecrmc.entity.EnrollmentComissionToEmployeePost;
import ru.tandemservice.unibase.UniBaseUtils;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = getModel(component);
        model.setSettings(component.getSettings());
        getDao().prepare(model);

        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component) {
        Model model = getModel(component);
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<EnrollmentComissionToEmployeePost> dataSource = UniBaseUtils.createDataSource(component, getDao());
        //EnrollmentComissionToEmployeePost aa;aa.getEmployeePost().getTitle()
        dataSource.addColumn(new PublisherLinkColumn("Сотрудник", EnrollmentComissionToEmployeePost.employeePost().person().identityCard().fullFio()).setResolver(new IPublisherLinkResolver() {
            @Override
            public String getComponentName(IEntity ientity) {
                return null;
            }

            @Override
            public Object getParameters(IEntity ientity) {
                return new ParametersMap().add("publisherId", ((EnrollmentComissionToEmployeePost) ientity).getEmployeePost().getId());
            }
        }));

        dataSource.addColumn(new PublisherLinkColumn("Подразделение", EnrollmentComissionToEmployeePost.orgUnit().title()).setResolver(new IPublisherLinkResolver() {
            @Override
            public String getComponentName(IEntity ientity) {
                return null;
            }

            @Override
            public Object getParameters(IEntity ientity) {
                return new ParametersMap().add("publisherId", ((EnrollmentComissionToEmployeePost) ientity).getOrgUnit().getId());
            }
        }));

        dataSource.addColumn(new SimpleColumn("Приемная кампания", EnrollmentComissionToEmployeePost.enrollmentCampaign().title()));

		/*
		dataSource.addColumn(new SimpleColumn("Печатное название должности", EnrollmentComissionToEmployeePost.employeePostPrintTitle()));
		dataSource.addColumn(new SimpleColumn("Печатное название подразделения", EnrollmentComissionToEmployeePost.orgUnitPrintTitle()));
		*/

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit").setPermissionKey("EnrollmentComissionToEmployeePost_edit"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete").setPermissionKey("EnrollmentComissionToEmployeePost_delete"));

        model.setDataSource(dataSource);
    }

    public void onClickClear(IBusinessComponent component) {
        Model model = getModel(component);
        model.getSettings().clear();
        onClickSearch(component);
    }

    public void onClickSearch(IBusinessComponent component) {
        Model model = getModel(component);
        model.getDataSource().refresh();
        component.saveSettings();
    }

    public void onClickEdit(IBusinessComponent component) {
        Long id = component.getListenerParameter();
        activate(component, new ComponentActivator(
                ru.tandemservice.eventssecrmc.component.settings.EnrollmentComissionToEmployeePost.AddEdit.Model.class.getPackage().getName(),
                new ParametersMap().add("id", id)
        ));
    }

    public void onClickAdd(IBusinessComponent component) {
        Model model = getModel(component);
        activate(component, new ComponentActivator(
                ru.tandemservice.eventssecrmc.component.settings.EnrollmentComissionToEmployeePost.AddEdit.Model.class.getPackage().getName(),
                new ParametersMap().add("enrollmentCampaignId", model.getEnrollmentCampaign().getId())
        ));
    }

    public void onClickDelete(IBusinessComponent component) {
        Long id = component.getListenerParameter();
        getDao().delete(id);
    }
}
