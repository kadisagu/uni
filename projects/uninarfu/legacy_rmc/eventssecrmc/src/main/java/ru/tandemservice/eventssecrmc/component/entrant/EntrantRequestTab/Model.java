package ru.tandemservice.eventssecrmc.component.entrant.EntrantRequestTab;

public class Model
        extends ru.tandemservice.uniec.component.entrant.EntrantRequestTab.Model
{
    /**
     * Отборочная комиссия
     */
    private String enrolmentComissionTitle = "";
    private boolean createInOtherComission = false;
    private boolean hasAccessToRequest = true;
    private String accessToRequestTitle = "";

    private boolean disableEditEntrantRequestOrgUnit = true;


    public void setEnrolmentComissionTitle(String enrolmentComissionTitle) {
        this.enrolmentComissionTitle = enrolmentComissionTitle;
    }

    public String getEnrolmentComissionTitle()
    {
        return enrolmentComissionTitle;
    }

    public void setCreateInOtherComission(boolean createInOtherComission) {
        this.createInOtherComission = createInOtherComission;
    }

    public boolean isCreateInOtherComission() {
        return createInOtherComission;
    }

    public void setHasAccessToRequest(boolean hasAccessToRequest) {
        this.hasAccessToRequest = hasAccessToRequest;
    }

    public boolean isHasAccessToRequest() {
        return hasAccessToRequest;
    }

    public void setAccessToRequestTitle(String accessToRequestTitle) {
        this.accessToRequestTitle = accessToRequestTitle;
    }

    public String getAccessToRequestTitle() {
        return accessToRequestTitle;
    }

    public void setDisableEditEntrantRequestOrgUnit(
            boolean disableEditEntrantRequestOrgUnit)
    {
        this.disableEditEntrantRequestOrgUnit = disableEditEntrantRequestOrgUnit;
    }

    public boolean isDisableEditEntrantRequestOrgUnit() {
        return disableEditEntrantRequestOrgUnit;
    }

}
