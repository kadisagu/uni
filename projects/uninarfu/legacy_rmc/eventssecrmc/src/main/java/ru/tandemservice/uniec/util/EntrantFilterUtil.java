package ru.tandemservice.uniec.util;

import org.hibernate.Session;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt;
import ru.tandemservice.eventssecrmc.entity.EnrollmentComissionToEmployeePost;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.CurrentExamGroupLogic;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.List;

public class EntrantFilterUtil {

    private static final int ALL = 1;
    private static final int ALL_WITH_SECURITY = 2;
    private static final int WITHOUT_AUTO_FORMING = 3;
    private static final int WITH_AUTO_FORMING = 4;
    private static final int WITH_AUTO_FORMING_AND_ALGORITHM = 5;

    public static void prepareEnrollmentCampaignFilter(IEnrollmentCampaignSelectModel model, Session session) {
        prepareEnrollmentCampaignFilter(model, session, true);
    }

    public static void prepareEnrollmentCampaignFilter(IEnrollmentCampaignSelectModel model, Session session, boolean doSecFilter) {
        prepareEnrollmentCampaignFilter(model, session, ALL, doSecFilter);
    }

    public static void prepareEnrollmentCampaignFilterWithSecurity(IEnrollmentCampaignSelectModel model, Session session) {
        prepareEnrollmentCampaignFilter(model, session, ALL_WITH_SECURITY, true);
    }

    public static void prepareEnrollmentCampaignFilterWithoutExamGroupAutoForming(IEnrollmentCampaignSelectModel model, Session session) {
        prepareEnrollmentCampaignFilter(model, session, WITHOUT_AUTO_FORMING, true);
    }

    public static void prepareEnrollmentCampaignFilterWithExamGroupAutoForming(IEnrollmentCampaignSelectModel model, Session session) {
        prepareEnrollmentCampaignFilter(model, session, WITH_AUTO_FORMING, true);
    }

    public static void prepareEnrollmentCampaignFilterWithExamGroupAutoFormingAndAlgorithm(IEnrollmentCampaignSelectModel model, Session session) {
        prepareEnrollmentCampaignFilter(model, session, WITH_AUTO_FORMING_AND_ALGORITHM, true);
    }

    private static void prepareEnrollmentCampaignFilter(IEnrollmentCampaignSelectModel model, Session session, int selectType, boolean doSecFilter) {
        //зачем это надо?
        /*
		if (selectType == ALL_WITH_SECURITY) {
			boolean check = CoreServices.securityService().check(new EnrollmentCampaign(), ContextLocal.getUserContext().getPrincipalContext(), "workWithAllEnrollmentCampaigns");
			if (!check) {
				EnrollmentCampaign enrollmentCampaign = new DQLSelectBuilder().fromEntity(EnrollmentCampaign.class, "e")
					.column("e")
					.order(DQLExpressions.property(EnrollmentCampaign.id().fromAlias("e")), OrderDirection.desc)
					.createStatement(session)
					.setMaxResults(1)
					.uniqueResult();

				if (enrollmentCampaign == null) {
					model.setEnrollmentCampaignList(Collections.<EnrollmentCampaign>emptyList());
					model.setEnrollmentCampaign(null);
				}   else {
					model.setEnrollmentCampaignList(Collections.singletonList(enrollmentCampaign));
					model.setEnrollmentCampaign(enrollmentCampaign);
				}

				return;
			}
		}
		*/

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrollmentCampaign.class, "e")
                .column("e")
                .order(EnrollmentCampaign.id().fromAlias("e").s(), OrderDirection.desc);

        switch (selectType) {
            case 1:
            case 2:
                break;
            case 3:
                dql.where(DQLExpressions.eqValue(DQLExpressions.property(EnrollmentCampaign.formingExamGroupAuto().fromAlias("e")), Boolean.FALSE));
                break;
            case 4:
                dql.where(DQLExpressions.eqValue(DQLExpressions.property(EnrollmentCampaign.formingExamGroupAuto().fromAlias("e")), Boolean.TRUE));
                break;
            case 5:
                dql.joinEntity(
                        "e",
                        DQLJoinType.inner,
                        CurrentExamGroupLogic.class,
                        "alg",
                        DQLExpressions.eq(
                                DQLExpressions.property(EnrollmentCampaign.id().fromAlias("e")),
                                DQLExpressions.property(CurrentExamGroupLogic.enrollmentCampaign().id().fromAlias("alg")))
                );
                break;
            default:
                throw new RuntimeException("Unknown selectType: " + selectType);
        }


        if (doSecFilter && !UserContext.getInstance().getPrincipalContext().isAdmin())
            fillSec(dql);

        List<EnrollmentCampaign> enrollmentCampaigns = dql.createStatement(session).list();

        if ((model.getEnrollmentCampaign() == null) && (!enrollmentCampaigns.isEmpty()))
            model.setEnrollmentCampaign((EnrollmentCampaign) enrollmentCampaigns.get(0));

        model.setEnrollmentCampaignList(enrollmentCampaigns);
    }

    private static void fillSec(DQLSelectBuilder dql) {
        dql.joinEntity(
                "e",
                DQLJoinType.left,
                EnrollmentCampaignSecExt.class,
                "ext",
                DQLExpressions.eq(
                        DQLExpressions.property(EnrollmentCampaign.id().fromAlias("e")),
                        DQLExpressions.property(EnrollmentCampaignSecExt.enrollmentCampaign().id().fromAlias("ext")))
        );

        if (UserContext.getInstance().getPrincipalContext() instanceof EmployeePost) {
            EmployeePost ep = (EmployeePost) UserContext.getInstance().getPrincipalContext();
            DQLSelectBuilder subDql = new DQLSelectBuilder().fromEntity(EnrollmentComissionToEmployeePost.class, "rel")
                    .where(DQLExpressions.eqValue(DQLExpressions.property(EnrollmentComissionToEmployeePost.employeePost().fromAlias("rel")), ep))
                    .column(EnrollmentComissionToEmployeePost.enrollmentCampaign().id().fromAlias("rel").s());

            if (IUniBaseDao.instance.get().getCount(subDql) > 0) {
                //сотрудник ОК
                /**
                 * расширенных настроек нет вообще
                 * или
                 * настройки выключены
                 * или
                 * в настройке указано скрывать ПК
                 * или
                 * настройки включены и ПК совпадает с ПК ОК сотрудника
                 */
                dql.where(DQLExpressions.or(
                        DQLExpressions.isNull("ext"),
                        DQLExpressions.eqValue(DQLExpressions.property(EnrollmentCampaignSecExt.useEntrantRequestSecExt().fromAlias("ext")), Boolean.FALSE),
                        DQLExpressions.eqValue(DQLExpressions.property(EnrollmentCampaignSecExt.hidden().fromAlias("ext")), Boolean.FALSE),
                        DQLExpressions.and(
                                DQLExpressions.eqValue(DQLExpressions.property(EnrollmentCampaignSecExt.useEntrantRequestSecExt().fromAlias("ext")), Boolean.TRUE),
                                DQLExpressions.in(DQLExpressions.property(EnrollmentCampaign.id().fromAlias("e")), subDql.buildQuery()))
                ));
            }
            else {
                //не сотрудник ОК
                /**
                 * расширенных настроек нет вообще
                 * или
                 * настройки выключены
                 * или
                 * в настройке указано скрывать ПК
                 * или
                 * настройки включены и ПК не скрыта для сотрудников ПК
                 */
                dql
                        .where(DQLExpressions.or(
                                DQLExpressions.isNull("ext"),
                                DQLExpressions.eqValue(DQLExpressions.property(EnrollmentCampaignSecExt.useEntrantRequestSecExt().fromAlias("ext")), Boolean.FALSE),
                                DQLExpressions.eqValue(DQLExpressions.property(EnrollmentCampaignSecExt.hidden().fromAlias("ext")), Boolean.FALSE),
                                DQLExpressions.and(
                                        DQLExpressions.eqValue(DQLExpressions.property(EnrollmentCampaignSecExt.useEntrantRequestSecExt().fromAlias("ext")), Boolean.TRUE),
                                        DQLExpressions.eqValue(DQLExpressions.property(EnrollmentCampaignSecExt.checkAccess().fromAlias("ext")), Boolean.FALSE))
                        ));
            }
        }
        else {
            /**
             * расширенных настроек нет вообще
             * или
             * настройки выключены
             * или
             * в настройке указано скрывать ПК
             */
            dql.where(DQLExpressions.or(
                    DQLExpressions.isNull("ext"),
                    DQLExpressions.eqValue(DQLExpressions.property(EnrollmentCampaignSecExt.useEntrantRequestSecExt().fromAlias("ext")), Boolean.FALSE),
                    DQLExpressions.eqValue(DQLExpressions.property(EnrollmentCampaignSecExt.hidden().fromAlias("ext")), Boolean.FALSE)
            ));
        }
    }

    public static void resetEnrollmentCampaignFilter(IEnrollmentCampaignSelectModel model) {
        model.getSettings().clear();
        List<EnrollmentCampaign> enrollmentCampaigns = model.getEnrollmentCampaignList();
        if (!enrollmentCampaigns.isEmpty())
            model.getSettings().set("enrollmentCampaign", enrollmentCampaigns.get(0));
    }
}