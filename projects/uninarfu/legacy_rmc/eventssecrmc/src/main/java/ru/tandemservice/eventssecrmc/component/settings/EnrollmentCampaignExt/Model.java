package ru.tandemservice.eventssecrmc.component.settings.EnrollmentCampaignExt;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.List;

public class Model implements IEnrollmentCampaignSelectModel {

    private List<EnrollmentCampaign> enrollmentCampaignList;
    private IDataSettings settings;

    private DynamicListDataSource<EnrollmentCampaignSecExt> dataSource;
    private List<OrgUnitType> orgUnitTypeList;

    private ISelectModel orgUnitList;

    public ISelectModel getOrgUnitList() {
        return orgUnitList;
    }

    public void setOrgUnitList(ISelectModel orgUnitList) {
        this.orgUnitList = orgUnitList;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign() {
        return (EnrollmentCampaign) this.settings.get("enrollmentCampaign");
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList() {
        return enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign) {
        this.settings.set("enrollmentCampaign", enrollmentCampaign);
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList) {
        this.enrollmentCampaignList = enrollmentCampaignList;
    }

    public DynamicListDataSource<EnrollmentCampaignSecExt> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<EnrollmentCampaignSecExt> dataSource) {
        this.dataSource = dataSource;
    }

    public List<OrgUnitType> getOrgUnitTypeList() {
        return orgUnitTypeList;
    }

    public void setOrgUnitTypeList(List<OrgUnitType> orgUnitTypeList) {
        this.orgUnitTypeList = orgUnitTypeList;
    }

}
