package ru.tandemservice.eventssecrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.eventssecrmc.entity.EnrollmentCommission2formativeOrgUnit;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь отборочной комиссии и формирующего подразделения
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrollmentCommission2formativeOrgUnitGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.eventssecrmc.entity.EnrollmentCommission2formativeOrgUnit";
    public static final String ENTITY_NAME = "enrollmentCommission2formativeOrgUnit";
    public static final int VERSION_HASH = -1625078319;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_COMMISSION = "enrollmentCommission";
    public static final String L_FORMATIVE_ORG_UNIT = "formativeOrgUnit";

    private OrgUnit _enrollmentCommission;     // Отборочная комиссия
    private OrgUnit _formativeOrgUnit;     // Формирующее подразделение

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Отборочная комиссия. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public OrgUnit getEnrollmentCommission()
    {
        return _enrollmentCommission;
    }

    /**
     * @param enrollmentCommission Отборочная комиссия. Свойство не может быть null и должно быть уникальным.
     */
    public void setEnrollmentCommission(OrgUnit enrollmentCommission)
    {
        dirty(_enrollmentCommission, enrollmentCommission);
        _enrollmentCommission = enrollmentCommission;
    }

    /**
     * @return Формирующее подразделение.
     */
    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подразделение.
     */
    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrollmentCommission2formativeOrgUnitGen)
        {
            setEnrollmentCommission(((EnrollmentCommission2formativeOrgUnit)another).getEnrollmentCommission());
            setFormativeOrgUnit(((EnrollmentCommission2formativeOrgUnit)another).getFormativeOrgUnit());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrollmentCommission2formativeOrgUnitGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrollmentCommission2formativeOrgUnit.class;
        }

        public T newInstance()
        {
            return (T) new EnrollmentCommission2formativeOrgUnit();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentCommission":
                    return obj.getEnrollmentCommission();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentCommission":
                    obj.setEnrollmentCommission((OrgUnit) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((OrgUnit) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentCommission":
                        return true;
                case "formativeOrgUnit":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentCommission":
                    return true;
                case "formativeOrgUnit":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentCommission":
                    return OrgUnit.class;
                case "formativeOrgUnit":
                    return OrgUnit.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrollmentCommission2formativeOrgUnit> _dslPath = new Path<EnrollmentCommission2formativeOrgUnit>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrollmentCommission2formativeOrgUnit");
    }
            

    /**
     * @return Отборочная комиссия. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentCommission2formativeOrgUnit#getEnrollmentCommission()
     */
    public static OrgUnit.Path<OrgUnit> enrollmentCommission()
    {
        return _dslPath.enrollmentCommission();
    }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentCommission2formativeOrgUnit#getFormativeOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    public static class Path<E extends EnrollmentCommission2formativeOrgUnit> extends EntityPath<E>
    {
        private OrgUnit.Path<OrgUnit> _enrollmentCommission;
        private OrgUnit.Path<OrgUnit> _formativeOrgUnit;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Отборочная комиссия. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentCommission2formativeOrgUnit#getEnrollmentCommission()
     */
        public OrgUnit.Path<OrgUnit> enrollmentCommission()
        {
            if(_enrollmentCommission == null )
                _enrollmentCommission = new OrgUnit.Path<OrgUnit>(L_ENROLLMENT_COMMISSION, this);
            return _enrollmentCommission;
        }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentCommission2formativeOrgUnit#getFormativeOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new OrgUnit.Path<OrgUnit>(L_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

        public Class getEntityClass()
        {
            return EnrollmentCommission2formativeOrgUnit.class;
        }

        public String getEntityName()
        {
            return "enrollmentCommission2formativeOrgUnit";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
