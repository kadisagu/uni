package ru.tandemservice.eventssecrmc.component.settings.EnrollmentComissionToFormativeOrgUnit.List;

import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.eventssecrmc.entity.EnrollmentCommission2formativeOrgUnit;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DAO extends UniDao<Model> implements IDAO {


    @Override
    public void prepareListDataSource(Model model) {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrollmentCommission2formativeOrgUnit.class, "e");
        //.where(DQLExpressions.eqValue( DQLExpressions.property(EnrollmentCommission2formativeOrgUnit.enrollmentCampaign().fromAlias("e")), model.getEnrollmentCampaign() ));
        DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(EnrollmentCommission2formativeOrgUnit.class, "e");
        orderRegistry.applyOrder(dql, model.getDataSource().getEntityOrder());
//		if (model.getDataSource().getEntityOrder() != null)
//			dql.order("e."+model.getDataSource().getEntityOrder().getKeyString(), model.getDataSource().getEntityOrder().getDirection());


        UniBaseUtils.createPage(model.getDataSource(), dql, getSession());
    }

    public void updateEnrollmentCommissions2formativeOrgUnit()
    {

        List<OrgUnit> enrollmentCommissions = getList(OrgUnit.class, OrgUnit.orgUnitType().code(), "30");
        Map<OrgUnit, EnrollmentCommission2formativeOrgUnit> map = new HashMap<>();
        List<EnrollmentCommission2formativeOrgUnit> enrollmentCommission2formativeOrgUnits = getList(EnrollmentCommission2formativeOrgUnit.class);
        for (EnrollmentCommission2formativeOrgUnit ec2fou : enrollmentCommission2formativeOrgUnits) {
            map.put(ec2fou.getEnrollmentCommission(), ec2fou);

        }
        Set<OrgUnit> enrollmentCommissionsExist = map.keySet();
        for (OrgUnit ec : enrollmentCommissions) {
            if (!enrollmentCommissionsExist.contains(ec)) {
                EnrollmentCommission2formativeOrgUnit ec2fou = new EnrollmentCommission2formativeOrgUnit();
                ec2fou.setEnrollmentCommission(ec);
                saveOrUpdate(ec2fou);
            }
        }
        for (OrgUnit ec : enrollmentCommissionsExist) {
            if (!enrollmentCommissions.contains(ec)) {
                delete(map.get(ec));
            }
        }
    }
}
