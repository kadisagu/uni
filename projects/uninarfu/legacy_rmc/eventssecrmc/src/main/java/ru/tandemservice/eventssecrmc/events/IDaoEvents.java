package ru.tandemservice.eventssecrmc.events;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt;
import ru.tandemservice.eventssecrmc.entity.EnrollmentComissionToEmployeePost;
import ru.tandemservice.eventssecrmc.entity.EntrantRequestSecExt;
import ru.tandemservice.eventssecrmc.events.Events.EntityInfo;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;

import java.util.List;
import java.util.Map;

public interface IDaoEvents {
    public static final String BEAN_NAME = "ru.tandemservice.eventssecrmc.events.IDaoEvents";

    boolean needEvent(DSetEvent event);

    public List<EntityInfo> getEntityList(List<EntityInfo> entityInfoList);

    /**
     * свойства, изменения которых можно смело пропускать
     *
     * @return
     */
    public Map<String, List<String>> getPassChangedProperty();

    public boolean passChangedProperty(DSetEvent event);

    public List<CheckSecurity> checkSecurity
            (
                    EmployeePost executorPost
                    , DSetEvent event
                    , List<EntityInfo> entityInfoList
            );

    public void clearCache();

    public void createCache();

    /**
     * есть настройки пользователей, или нет
     * Если настроек нет, ничего не проверяем
     *
     * @return
     */
    public boolean hasEmployeePostSettings();

    /**
     * Результат проверки прав доступа
     * (те сущности, к которым доступа нет)
     *
     * @author vch
     */
    public static class CheckSecurity {
        private IEntity entity = null;
        private String securityMessage = "";

        public void setEntity(IEntity entity) {
            this.entity = entity;
        }

        public IEntity getEntity() {
            return entity;
        }

        public void setSecurityMessage(String securityMessage) {
            this.securityMessage = securityMessage;
        }

        public String getSecurityMessage() {
            return securityMessage;
        }
    }


    public boolean actionFromTrigger(
            EmployeePost employeePost
            , DSetEventType eventType
            , String entityType
            , Long id
            , List<Object> prevSource);

    public EnrollmentComissionToEmployeePost getComission(
            EmployeePost executorPost
            , EnrollmentCampaign enrollmentCampaign
    );


    public EmployeePost getExecutorPost();

    public List<EntrantRequestSecExt> getEntrantRequestSecExt(Entrant entrant);

    String getEntrantComissionName(Entrant entrant);

    /**
     * Для персоны, независимо от приемных камапний
     * возвращается массив всех возможных настроек
     *
     * @param executorPost
     *
     * @return
     */
    public List<EnrollmentComissionToEmployeePost> getEnrollmentComissionToEmployeePostList
    (
            EmployeePost executorPost
    );

    /**
     * список актуальных настроек приемки
     *
     * @param entityInfoList
     *
     * @return
     */
    public List<EnrollmentCampaignSecExt> getEnrollmentCampaignSecExtList
    (
            List<EntityInfo> entityInfoList
    );

    public boolean isUseEntrantState
            (
                    List<EnrollmentCampaignSecExt> enrollmentCampaignSecExtList
            );


    public boolean isUseStudentState
            (
                    List<EnrollmentCampaignSecExt> enrollmentCampaignSecExtList
            );


}
