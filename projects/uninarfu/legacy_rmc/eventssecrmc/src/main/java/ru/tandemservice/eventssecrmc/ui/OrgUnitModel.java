package ru.tandemservice.eventssecrmc.ui;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;

public class OrgUnitModel extends DQLFullCheckSelectModel {
    private IEnrollmentCampaignSelectModel model;

    public OrgUnitModel(IEnrollmentCampaignSelectModel model, String[] labelProperties) {
        super(labelProperties);
        this.model = model;
    }

    @Override
    protected DQLSelectBuilder query(String alias, String filter) {
        EnrollmentCampaignSecExt ext = IUniBaseDao.instance.get().get(EnrollmentCampaignSecExt.class, EnrollmentCampaignSecExt.enrollmentCampaign(), model.getEnrollmentCampaign());
        OrgUnitType orgUnitType = (ext != null) ? ext.getOrgUnitType() : null;

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(OrgUnit.class, alias)
                .where(DQLExpressions.eqValue(DQLExpressions.property(OrgUnit.orgUnitType().fromAlias(alias)), orgUnitType))
                .where(DQLExpressions.eqValue(DQLExpressions.property(OrgUnit.archival().fromAlias(alias)), Boolean.FALSE))
                .order(OrgUnit.title().fromAlias(alias).s());

        if (!StringUtils.isEmpty(filter))
            dql.where(like(OrgUnit.title().fromAlias(alias), filter));

        return dql;
    }

}
