package ru.tandemservice.eventssecrmc.component.settings.EnrollmentComissionToEmployeePost.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.eventssecrmc.entity.EnrollmentComissionToEmployeePost;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.List;

public class Model implements IEnrollmentCampaignSelectModel {

    private DynamicListDataSource<EnrollmentComissionToEmployeePost> dataSource;
    private List<EnrollmentCampaign> enrollmentCampaignList;
    private IDataSettings settings;

    public DynamicListDataSource<EnrollmentComissionToEmployeePost> getDataSource() {
        return dataSource;
    }

    public void setDataSource(
            DynamicListDataSource<EnrollmentComissionToEmployeePost> dataSource)
    {
        this.dataSource = dataSource;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign() {
        return (EnrollmentCampaign) this.settings.get("enrollmentCampaign");
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList() {
        return enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign) {
        this.settings.set("enrollmentCampaign", enrollmentCampaign);
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList) {
        this.enrollmentCampaignList = enrollmentCampaignList;
    }


}
