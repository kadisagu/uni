package ru.tandemservice.eventssecrmc.component.settings.EnrollmentComissionToFormativeOrgUnit.AddEdit;


import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.eventssecrmc.entity.EnrollmentCommission2formativeOrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;

import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(final Model model) {

        model.setRel(getNotNull(EnrollmentCommission2formativeOrgUnit.class, model.getRel().getId()));
        model.setOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING, null));
        if (model.getRel().getFormativeOrgUnit() != null && model.getOldFormativeId() == null)
            model.setOldFormativeId(model.getRel().getFormativeOrgUnit().getId());


    }

    @Override
    public void update(Model model) {
        validate(model, UserContext.getInstance().getErrorCollector());
        getSession().update(model.getRel());
    }

    @Override
    public void validate(Model model, ErrorCollector errorCollector) {

        if (model.getRel().getFormativeOrgUnit() != null) {
            getSession().clear();
            DQLSelectBuilder dqlSelectBuilder = new DQLSelectBuilder().fromEntity(EnrollmentCommission2formativeOrgUnit.class, "e").column(DQLExpressions.property(EnrollmentCommission2formativeOrgUnit.formativeOrgUnit().fromAlias("e")));
            dqlSelectBuilder.where(DQLExpressions.isNotNull(DQLExpressions.property(EnrollmentCommission2formativeOrgUnit.formativeOrgUnit().fromAlias("e"))));
            dqlSelectBuilder.predicate(DQLPredicateType.distinct);

            List<OrgUnit> fou = getList(dqlSelectBuilder);
            Long newId = model.getRel().getFormativeOrgUnit().getId();
            if (fou.contains(model.getRel().getFormativeOrgUnit())) {
                if (model.getOldFormativeId() == null)
                    errorCollector.add(model.getRel().getFormativeOrgUnit().getTitle() + " уже используется. Выберите другое формирующее подразделение");
                else if (!newId.equals(model.getOldFormativeId()))
                    errorCollector.add(model.getRel().getFormativeOrgUnit().getTitle() + " уже используется. Выберите другое формирующее подразделение");
            }
        }
    }


}
