package ru.tandemservice.eventssecrmc.component.settings.EnrollmentComissionToFormativeOrgUnit.AddEdit;

import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.eventssecrmc.entity.EnrollmentCommission2formativeOrgUnit;

@Input({
        @org.tandemframework.core.component.Bind(key = "id", binding = "rel.id")
})
public class Model {

    private EnrollmentCommission2formativeOrgUnit rel = new EnrollmentCommission2formativeOrgUnit();
    private Long oldFormativeId;
    private ISelectModel orgUnitModel;

    public Long getOldFormativeId() {
        return oldFormativeId;
    }

    public void setOldFormativeId(Long oldFormativeId) {
        this.oldFormativeId = oldFormativeId;
    }

    public EnrollmentCommission2formativeOrgUnit getRel() {
        return rel;
    }

    public void setRel(EnrollmentCommission2formativeOrgUnit rel) {
        this.rel = rel;
    }

    public ISelectModel getOrgUnitModel() {
        return orgUnitModel;
    }

    public void setOrgUnitModel(ISelectModel orgUnitModel) {
        this.orgUnitModel = orgUnitModel;
    }


}
