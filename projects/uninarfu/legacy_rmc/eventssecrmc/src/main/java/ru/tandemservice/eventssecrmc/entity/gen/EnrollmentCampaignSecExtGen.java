package ru.tandemservice.eventssecrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Приемка (расширенные настройки)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrollmentCampaignSecExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt";
    public static final String ENTITY_NAME = "enrollmentCampaignSecExt";
    public static final int VERSION_HASH = 1459022744;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_USE_ENTRANT_REQUEST_SEC_EXT = "useEntrantRequestSecExt";
    public static final String P_CHECK_ACCESS = "checkAccess";
    public static final String P_HIDDEN = "hidden";
    public static final String P_DISALLOW_CREATE_REQUEST = "disallowCreateRequest";
    public static final String P_DISALLOW_CHANGE_REQUEST_OWNER = "disallowChangeRequestOwner";
    public static final String P_DISALLOW_EDIT_REQUEST = "disallowEditRequest";
    public static final String P_DISALLOW_DELETE_REQUEST = "disallowDeleteRequest";
    public static final String P_ALLOW_ACTIVE_REQUEST = "allowActiveRequest";
    public static final String P_USE_STUDENT_STATE = "useStudentState";
    public static final String P_USE_ENTRANT_STATE = "useEntrantState";
    public static final String L_ORG_UNIT_TYPE = "orgUnitType";
    public static final String L_ENROLLMENT_COMMISSION = "enrollmentCommission";

    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private boolean _useEntrantRequestSecExt = true;     // Использовать расширенные настройки безопасности
    private boolean _checkAccess = true;     // Скрывать приемную кампанию если нет доступа
    private boolean _hidden = false;     // Скрыть приемную кампанию
    private boolean _disallowCreateRequest = true;     // (не используется) Запретить создавать заявления, если нет разрешения на должности
    private boolean _disallowChangeRequestOwner = true;     // (не используется)Запретить смену владельца заявления
    private boolean _disallowEditRequest = true;     // Запретить редактирование 'чужого заявления'
    private boolean _disallowDeleteRequest = true;     // Запретить удаление 'чужого заявления'
    private int _allowActiveRequest = 1;     // Кол-во активных заявлений
    private boolean _useStudentState = true;     // Учитывать присутствие студентов
    private boolean _useEntrantState = true;     // Только заявления в активном статусе
    private OrgUnitType _orgUnitType;     // Тип позразделения ОК
    private OrgUnit _enrollmentCommission;     // Приемная комиссия

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null и должно быть уникальным.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Использовать расширенные настройки безопасности. Свойство не может быть null.
     */
    @NotNull
    public boolean isUseEntrantRequestSecExt()
    {
        return _useEntrantRequestSecExt;
    }

    /**
     * @param useEntrantRequestSecExt Использовать расширенные настройки безопасности. Свойство не может быть null.
     */
    public void setUseEntrantRequestSecExt(boolean useEntrantRequestSecExt)
    {
        dirty(_useEntrantRequestSecExt, useEntrantRequestSecExt);
        _useEntrantRequestSecExt = useEntrantRequestSecExt;
    }

    /**
     * @return Скрывать приемную кампанию если нет доступа. Свойство не может быть null.
     */
    @NotNull
    public boolean isCheckAccess()
    {
        return _checkAccess;
    }

    /**
     * @param checkAccess Скрывать приемную кампанию если нет доступа. Свойство не может быть null.
     */
    public void setCheckAccess(boolean checkAccess)
    {
        dirty(_checkAccess, checkAccess);
        _checkAccess = checkAccess;
    }

    /**
     * @return Скрыть приемную кампанию. Свойство не может быть null.
     */
    @NotNull
    public boolean isHidden()
    {
        return _hidden;
    }

    /**
     * @param hidden Скрыть приемную кампанию. Свойство не может быть null.
     */
    public void setHidden(boolean hidden)
    {
        dirty(_hidden, hidden);
        _hidden = hidden;
    }

    /**
     * @return (не используется) Запретить создавать заявления, если нет разрешения на должности. Свойство не может быть null.
     */
    @NotNull
    public boolean isDisallowCreateRequest()
    {
        return _disallowCreateRequest;
    }

    /**
     * @param disallowCreateRequest (не используется) Запретить создавать заявления, если нет разрешения на должности. Свойство не может быть null.
     */
    public void setDisallowCreateRequest(boolean disallowCreateRequest)
    {
        dirty(_disallowCreateRequest, disallowCreateRequest);
        _disallowCreateRequest = disallowCreateRequest;
    }

    /**
     * @return (не используется)Запретить смену владельца заявления. Свойство не может быть null.
     */
    @NotNull
    public boolean isDisallowChangeRequestOwner()
    {
        return _disallowChangeRequestOwner;
    }

    /**
     * @param disallowChangeRequestOwner (не используется)Запретить смену владельца заявления. Свойство не может быть null.
     */
    public void setDisallowChangeRequestOwner(boolean disallowChangeRequestOwner)
    {
        dirty(_disallowChangeRequestOwner, disallowChangeRequestOwner);
        _disallowChangeRequestOwner = disallowChangeRequestOwner;
    }

    /**
     * @return Запретить редактирование 'чужого заявления'. Свойство не может быть null.
     */
    @NotNull
    public boolean isDisallowEditRequest()
    {
        return _disallowEditRequest;
    }

    /**
     * @param disallowEditRequest Запретить редактирование 'чужого заявления'. Свойство не может быть null.
     */
    public void setDisallowEditRequest(boolean disallowEditRequest)
    {
        dirty(_disallowEditRequest, disallowEditRequest);
        _disallowEditRequest = disallowEditRequest;
    }

    /**
     * @return Запретить удаление 'чужого заявления'. Свойство не может быть null.
     */
    @NotNull
    public boolean isDisallowDeleteRequest()
    {
        return _disallowDeleteRequest;
    }

    /**
     * @param disallowDeleteRequest Запретить удаление 'чужого заявления'. Свойство не может быть null.
     */
    public void setDisallowDeleteRequest(boolean disallowDeleteRequest)
    {
        dirty(_disallowDeleteRequest, disallowDeleteRequest);
        _disallowDeleteRequest = disallowDeleteRequest;
    }

    /**
     * @return Кол-во активных заявлений. Свойство не может быть null.
     */
    @NotNull
    public int getAllowActiveRequest()
    {
        return _allowActiveRequest;
    }

    /**
     * @param allowActiveRequest Кол-во активных заявлений. Свойство не может быть null.
     */
    public void setAllowActiveRequest(int allowActiveRequest)
    {
        dirty(_allowActiveRequest, allowActiveRequest);
        _allowActiveRequest = allowActiveRequest;
    }

    /**
     * @return Учитывать присутствие студентов. Свойство не может быть null.
     */
    @NotNull
    public boolean isUseStudentState()
    {
        return _useStudentState;
    }

    /**
     * @param useStudentState Учитывать присутствие студентов. Свойство не может быть null.
     */
    public void setUseStudentState(boolean useStudentState)
    {
        dirty(_useStudentState, useStudentState);
        _useStudentState = useStudentState;
    }

    /**
     * @return Только заявления в активном статусе. Свойство не может быть null.
     */
    @NotNull
    public boolean isUseEntrantState()
    {
        return _useEntrantState;
    }

    /**
     * @param useEntrantState Только заявления в активном статусе. Свойство не может быть null.
     */
    public void setUseEntrantState(boolean useEntrantState)
    {
        dirty(_useEntrantState, useEntrantState);
        _useEntrantState = useEntrantState;
    }

    /**
     * @return Тип позразделения ОК.
     */
    public OrgUnitType getOrgUnitType()
    {
        return _orgUnitType;
    }

    /**
     * @param orgUnitType Тип позразделения ОК.
     */
    public void setOrgUnitType(OrgUnitType orgUnitType)
    {
        dirty(_orgUnitType, orgUnitType);
        _orgUnitType = orgUnitType;
    }

    /**
     * @return Приемная комиссия.
     */
    public OrgUnit getEnrollmentCommission()
    {
        return _enrollmentCommission;
    }

    /**
     * @param enrollmentCommission Приемная комиссия.
     */
    public void setEnrollmentCommission(OrgUnit enrollmentCommission)
    {
        dirty(_enrollmentCommission, enrollmentCommission);
        _enrollmentCommission = enrollmentCommission;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrollmentCampaignSecExtGen)
        {
            setEnrollmentCampaign(((EnrollmentCampaignSecExt)another).getEnrollmentCampaign());
            setUseEntrantRequestSecExt(((EnrollmentCampaignSecExt)another).isUseEntrantRequestSecExt());
            setCheckAccess(((EnrollmentCampaignSecExt)another).isCheckAccess());
            setHidden(((EnrollmentCampaignSecExt)another).isHidden());
            setDisallowCreateRequest(((EnrollmentCampaignSecExt)another).isDisallowCreateRequest());
            setDisallowChangeRequestOwner(((EnrollmentCampaignSecExt)another).isDisallowChangeRequestOwner());
            setDisallowEditRequest(((EnrollmentCampaignSecExt)another).isDisallowEditRequest());
            setDisallowDeleteRequest(((EnrollmentCampaignSecExt)another).isDisallowDeleteRequest());
            setAllowActiveRequest(((EnrollmentCampaignSecExt)another).getAllowActiveRequest());
            setUseStudentState(((EnrollmentCampaignSecExt)another).isUseStudentState());
            setUseEntrantState(((EnrollmentCampaignSecExt)another).isUseEntrantState());
            setOrgUnitType(((EnrollmentCampaignSecExt)another).getOrgUnitType());
            setEnrollmentCommission(((EnrollmentCampaignSecExt)another).getEnrollmentCommission());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrollmentCampaignSecExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrollmentCampaignSecExt.class;
        }

        public T newInstance()
        {
            return (T) new EnrollmentCampaignSecExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "useEntrantRequestSecExt":
                    return obj.isUseEntrantRequestSecExt();
                case "checkAccess":
                    return obj.isCheckAccess();
                case "hidden":
                    return obj.isHidden();
                case "disallowCreateRequest":
                    return obj.isDisallowCreateRequest();
                case "disallowChangeRequestOwner":
                    return obj.isDisallowChangeRequestOwner();
                case "disallowEditRequest":
                    return obj.isDisallowEditRequest();
                case "disallowDeleteRequest":
                    return obj.isDisallowDeleteRequest();
                case "allowActiveRequest":
                    return obj.getAllowActiveRequest();
                case "useStudentState":
                    return obj.isUseStudentState();
                case "useEntrantState":
                    return obj.isUseEntrantState();
                case "orgUnitType":
                    return obj.getOrgUnitType();
                case "enrollmentCommission":
                    return obj.getEnrollmentCommission();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "useEntrantRequestSecExt":
                    obj.setUseEntrantRequestSecExt((Boolean) value);
                    return;
                case "checkAccess":
                    obj.setCheckAccess((Boolean) value);
                    return;
                case "hidden":
                    obj.setHidden((Boolean) value);
                    return;
                case "disallowCreateRequest":
                    obj.setDisallowCreateRequest((Boolean) value);
                    return;
                case "disallowChangeRequestOwner":
                    obj.setDisallowChangeRequestOwner((Boolean) value);
                    return;
                case "disallowEditRequest":
                    obj.setDisallowEditRequest((Boolean) value);
                    return;
                case "disallowDeleteRequest":
                    obj.setDisallowDeleteRequest((Boolean) value);
                    return;
                case "allowActiveRequest":
                    obj.setAllowActiveRequest((Integer) value);
                    return;
                case "useStudentState":
                    obj.setUseStudentState((Boolean) value);
                    return;
                case "useEntrantState":
                    obj.setUseEntrantState((Boolean) value);
                    return;
                case "orgUnitType":
                    obj.setOrgUnitType((OrgUnitType) value);
                    return;
                case "enrollmentCommission":
                    obj.setEnrollmentCommission((OrgUnit) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "useEntrantRequestSecExt":
                        return true;
                case "checkAccess":
                        return true;
                case "hidden":
                        return true;
                case "disallowCreateRequest":
                        return true;
                case "disallowChangeRequestOwner":
                        return true;
                case "disallowEditRequest":
                        return true;
                case "disallowDeleteRequest":
                        return true;
                case "allowActiveRequest":
                        return true;
                case "useStudentState":
                        return true;
                case "useEntrantState":
                        return true;
                case "orgUnitType":
                        return true;
                case "enrollmentCommission":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "useEntrantRequestSecExt":
                    return true;
                case "checkAccess":
                    return true;
                case "hidden":
                    return true;
                case "disallowCreateRequest":
                    return true;
                case "disallowChangeRequestOwner":
                    return true;
                case "disallowEditRequest":
                    return true;
                case "disallowDeleteRequest":
                    return true;
                case "allowActiveRequest":
                    return true;
                case "useStudentState":
                    return true;
                case "useEntrantState":
                    return true;
                case "orgUnitType":
                    return true;
                case "enrollmentCommission":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "useEntrantRequestSecExt":
                    return Boolean.class;
                case "checkAccess":
                    return Boolean.class;
                case "hidden":
                    return Boolean.class;
                case "disallowCreateRequest":
                    return Boolean.class;
                case "disallowChangeRequestOwner":
                    return Boolean.class;
                case "disallowEditRequest":
                    return Boolean.class;
                case "disallowDeleteRequest":
                    return Boolean.class;
                case "allowActiveRequest":
                    return Integer.class;
                case "useStudentState":
                    return Boolean.class;
                case "useEntrantState":
                    return Boolean.class;
                case "orgUnitType":
                    return OrgUnitType.class;
                case "enrollmentCommission":
                    return OrgUnit.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrollmentCampaignSecExt> _dslPath = new Path<EnrollmentCampaignSecExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrollmentCampaignSecExt");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Использовать расширенные настройки безопасности. Свойство не может быть null.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt#isUseEntrantRequestSecExt()
     */
    public static PropertyPath<Boolean> useEntrantRequestSecExt()
    {
        return _dslPath.useEntrantRequestSecExt();
    }

    /**
     * @return Скрывать приемную кампанию если нет доступа. Свойство не может быть null.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt#isCheckAccess()
     */
    public static PropertyPath<Boolean> checkAccess()
    {
        return _dslPath.checkAccess();
    }

    /**
     * @return Скрыть приемную кампанию. Свойство не может быть null.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt#isHidden()
     */
    public static PropertyPath<Boolean> hidden()
    {
        return _dslPath.hidden();
    }

    /**
     * @return (не используется) Запретить создавать заявления, если нет разрешения на должности. Свойство не может быть null.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt#isDisallowCreateRequest()
     */
    public static PropertyPath<Boolean> disallowCreateRequest()
    {
        return _dslPath.disallowCreateRequest();
    }

    /**
     * @return (не используется)Запретить смену владельца заявления. Свойство не может быть null.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt#isDisallowChangeRequestOwner()
     */
    public static PropertyPath<Boolean> disallowChangeRequestOwner()
    {
        return _dslPath.disallowChangeRequestOwner();
    }

    /**
     * @return Запретить редактирование 'чужого заявления'. Свойство не может быть null.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt#isDisallowEditRequest()
     */
    public static PropertyPath<Boolean> disallowEditRequest()
    {
        return _dslPath.disallowEditRequest();
    }

    /**
     * @return Запретить удаление 'чужого заявления'. Свойство не может быть null.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt#isDisallowDeleteRequest()
     */
    public static PropertyPath<Boolean> disallowDeleteRequest()
    {
        return _dslPath.disallowDeleteRequest();
    }

    /**
     * @return Кол-во активных заявлений. Свойство не может быть null.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt#getAllowActiveRequest()
     */
    public static PropertyPath<Integer> allowActiveRequest()
    {
        return _dslPath.allowActiveRequest();
    }

    /**
     * @return Учитывать присутствие студентов. Свойство не может быть null.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt#isUseStudentState()
     */
    public static PropertyPath<Boolean> useStudentState()
    {
        return _dslPath.useStudentState();
    }

    /**
     * @return Только заявления в активном статусе. Свойство не может быть null.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt#isUseEntrantState()
     */
    public static PropertyPath<Boolean> useEntrantState()
    {
        return _dslPath.useEntrantState();
    }

    /**
     * @return Тип позразделения ОК.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt#getOrgUnitType()
     */
    public static OrgUnitType.Path<OrgUnitType> orgUnitType()
    {
        return _dslPath.orgUnitType();
    }

    /**
     * @return Приемная комиссия.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt#getEnrollmentCommission()
     */
    public static OrgUnit.Path<OrgUnit> enrollmentCommission()
    {
        return _dslPath.enrollmentCommission();
    }

    public static class Path<E extends EnrollmentCampaignSecExt> extends EntityPath<E>
    {
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Boolean> _useEntrantRequestSecExt;
        private PropertyPath<Boolean> _checkAccess;
        private PropertyPath<Boolean> _hidden;
        private PropertyPath<Boolean> _disallowCreateRequest;
        private PropertyPath<Boolean> _disallowChangeRequestOwner;
        private PropertyPath<Boolean> _disallowEditRequest;
        private PropertyPath<Boolean> _disallowDeleteRequest;
        private PropertyPath<Integer> _allowActiveRequest;
        private PropertyPath<Boolean> _useStudentState;
        private PropertyPath<Boolean> _useEntrantState;
        private OrgUnitType.Path<OrgUnitType> _orgUnitType;
        private OrgUnit.Path<OrgUnit> _enrollmentCommission;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Использовать расширенные настройки безопасности. Свойство не может быть null.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt#isUseEntrantRequestSecExt()
     */
        public PropertyPath<Boolean> useEntrantRequestSecExt()
        {
            if(_useEntrantRequestSecExt == null )
                _useEntrantRequestSecExt = new PropertyPath<Boolean>(EnrollmentCampaignSecExtGen.P_USE_ENTRANT_REQUEST_SEC_EXT, this);
            return _useEntrantRequestSecExt;
        }

    /**
     * @return Скрывать приемную кампанию если нет доступа. Свойство не может быть null.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt#isCheckAccess()
     */
        public PropertyPath<Boolean> checkAccess()
        {
            if(_checkAccess == null )
                _checkAccess = new PropertyPath<Boolean>(EnrollmentCampaignSecExtGen.P_CHECK_ACCESS, this);
            return _checkAccess;
        }

    /**
     * @return Скрыть приемную кампанию. Свойство не может быть null.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt#isHidden()
     */
        public PropertyPath<Boolean> hidden()
        {
            if(_hidden == null )
                _hidden = new PropertyPath<Boolean>(EnrollmentCampaignSecExtGen.P_HIDDEN, this);
            return _hidden;
        }

    /**
     * @return (не используется) Запретить создавать заявления, если нет разрешения на должности. Свойство не может быть null.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt#isDisallowCreateRequest()
     */
        public PropertyPath<Boolean> disallowCreateRequest()
        {
            if(_disallowCreateRequest == null )
                _disallowCreateRequest = new PropertyPath<Boolean>(EnrollmentCampaignSecExtGen.P_DISALLOW_CREATE_REQUEST, this);
            return _disallowCreateRequest;
        }

    /**
     * @return (не используется)Запретить смену владельца заявления. Свойство не может быть null.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt#isDisallowChangeRequestOwner()
     */
        public PropertyPath<Boolean> disallowChangeRequestOwner()
        {
            if(_disallowChangeRequestOwner == null )
                _disallowChangeRequestOwner = new PropertyPath<Boolean>(EnrollmentCampaignSecExtGen.P_DISALLOW_CHANGE_REQUEST_OWNER, this);
            return _disallowChangeRequestOwner;
        }

    /**
     * @return Запретить редактирование 'чужого заявления'. Свойство не может быть null.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt#isDisallowEditRequest()
     */
        public PropertyPath<Boolean> disallowEditRequest()
        {
            if(_disallowEditRequest == null )
                _disallowEditRequest = new PropertyPath<Boolean>(EnrollmentCampaignSecExtGen.P_DISALLOW_EDIT_REQUEST, this);
            return _disallowEditRequest;
        }

    /**
     * @return Запретить удаление 'чужого заявления'. Свойство не может быть null.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt#isDisallowDeleteRequest()
     */
        public PropertyPath<Boolean> disallowDeleteRequest()
        {
            if(_disallowDeleteRequest == null )
                _disallowDeleteRequest = new PropertyPath<Boolean>(EnrollmentCampaignSecExtGen.P_DISALLOW_DELETE_REQUEST, this);
            return _disallowDeleteRequest;
        }

    /**
     * @return Кол-во активных заявлений. Свойство не может быть null.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt#getAllowActiveRequest()
     */
        public PropertyPath<Integer> allowActiveRequest()
        {
            if(_allowActiveRequest == null )
                _allowActiveRequest = new PropertyPath<Integer>(EnrollmentCampaignSecExtGen.P_ALLOW_ACTIVE_REQUEST, this);
            return _allowActiveRequest;
        }

    /**
     * @return Учитывать присутствие студентов. Свойство не может быть null.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt#isUseStudentState()
     */
        public PropertyPath<Boolean> useStudentState()
        {
            if(_useStudentState == null )
                _useStudentState = new PropertyPath<Boolean>(EnrollmentCampaignSecExtGen.P_USE_STUDENT_STATE, this);
            return _useStudentState;
        }

    /**
     * @return Только заявления в активном статусе. Свойство не может быть null.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt#isUseEntrantState()
     */
        public PropertyPath<Boolean> useEntrantState()
        {
            if(_useEntrantState == null )
                _useEntrantState = new PropertyPath<Boolean>(EnrollmentCampaignSecExtGen.P_USE_ENTRANT_STATE, this);
            return _useEntrantState;
        }

    /**
     * @return Тип позразделения ОК.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt#getOrgUnitType()
     */
        public OrgUnitType.Path<OrgUnitType> orgUnitType()
        {
            if(_orgUnitType == null )
                _orgUnitType = new OrgUnitType.Path<OrgUnitType>(L_ORG_UNIT_TYPE, this);
            return _orgUnitType;
        }

    /**
     * @return Приемная комиссия.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt#getEnrollmentCommission()
     */
        public OrgUnit.Path<OrgUnit> enrollmentCommission()
        {
            if(_enrollmentCommission == null )
                _enrollmentCommission = new OrgUnit.Path<OrgUnit>(L_ENROLLMENT_COMMISSION, this);
            return _enrollmentCommission;
        }

        public Class getEntityClass()
        {
            return EnrollmentCampaignSecExt.class;
        }

        public String getEntityName()
        {
            return "enrollmentCampaignSecExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
