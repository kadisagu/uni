package ru.tandemservice.eventssecrmc.component.settings.EnrollmentComissionToFormativeOrgUnit.List;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.eventssecrmc.entity.EnrollmentCommission2formativeOrgUnit;
import ru.tandemservice.unibase.UniBaseUtils;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = getModel(component);
        getDao().prepare(model);
        prepareDataSource(component);
    }

    @Override
    public void onActivateComponent(IBusinessComponent component)
    {
        getDao().updateEnrollmentCommissions2formativeOrgUnit();
    }

    private void prepareDataSource(IBusinessComponent component) {
        Model model = getModel(component);
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<EnrollmentCommission2formativeOrgUnit> dataSource = UniBaseUtils.createDataSource(component, getDao());


        dataSource.addColumn(new SimpleColumn("Отборочная комиссия", EnrollmentCommission2formativeOrgUnit.enrollmentCommission().title()).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Формирующее подразделение", EnrollmentCommission2formativeOrgUnit.formativeOrgUnit().title()));


        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit").setPermissionKey("enrollmentCommissionToFormativeOrgUnit_edit"));

        model.setDataSource(dataSource);
    }


    public void onClickEdit(IBusinessComponent component) {
        Long id = component.getListenerParameter();
        activate(component, new ComponentActivator(
                ru.tandemservice.eventssecrmc.component.settings.EnrollmentComissionToFormativeOrgUnit.AddEdit.Model.class.getPackage().getName(),
                new ParametersMap().add("id", id)
        ));
    }

}
