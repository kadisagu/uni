package ru.tandemservice.eventssecrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.eventssecrmc.entity.EnrollmentComissionToEmployeePost;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Должности сотрудников к отборочным комиссиям
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrollmentComissionToEmployeePostGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.eventssecrmc.entity.EnrollmentComissionToEmployeePost";
    public static final String ENTITY_NAME = "enrollmentComissionToEmployeePost";
    public static final int VERSION_HASH = 1241430255;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_POST = "employeePost";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String P_EMPLOYEE_POST_PRINT_TITLE = "employeePostPrintTitle";
    public static final String P_ORG_UNIT_PRINT_TITLE = "orgUnitPrintTitle";

    private EmployeePost _employeePost;     // Сотрудник
    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private OrgUnit _orgUnit;     // Подразделение
    private String _employeePostPrintTitle;     // Печатное название должности
    private String _orgUnitPrintTitle;     // Печатное название подразделения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сотрудник. Свойство не может быть null.
     */
    @NotNull
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Сотрудник. Свойство не может быть null.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Подразделение.
     */
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Печатное название должности.
     */
    @Length(max=255)
    public String getEmployeePostPrintTitle()
    {
        return _employeePostPrintTitle;
    }

    /**
     * @param employeePostPrintTitle Печатное название должности.
     */
    public void setEmployeePostPrintTitle(String employeePostPrintTitle)
    {
        dirty(_employeePostPrintTitle, employeePostPrintTitle);
        _employeePostPrintTitle = employeePostPrintTitle;
    }

    /**
     * @return Печатное название подразделения.
     */
    @Length(max=255)
    public String getOrgUnitPrintTitle()
    {
        return _orgUnitPrintTitle;
    }

    /**
     * @param orgUnitPrintTitle Печатное название подразделения.
     */
    public void setOrgUnitPrintTitle(String orgUnitPrintTitle)
    {
        dirty(_orgUnitPrintTitle, orgUnitPrintTitle);
        _orgUnitPrintTitle = orgUnitPrintTitle;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrollmentComissionToEmployeePostGen)
        {
            setEmployeePost(((EnrollmentComissionToEmployeePost)another).getEmployeePost());
            setEnrollmentCampaign(((EnrollmentComissionToEmployeePost)another).getEnrollmentCampaign());
            setOrgUnit(((EnrollmentComissionToEmployeePost)another).getOrgUnit());
            setEmployeePostPrintTitle(((EnrollmentComissionToEmployeePost)another).getEmployeePostPrintTitle());
            setOrgUnitPrintTitle(((EnrollmentComissionToEmployeePost)another).getOrgUnitPrintTitle());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrollmentComissionToEmployeePostGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrollmentComissionToEmployeePost.class;
        }

        public T newInstance()
        {
            return (T) new EnrollmentComissionToEmployeePost();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "employeePost":
                    return obj.getEmployeePost();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "employeePostPrintTitle":
                    return obj.getEmployeePostPrintTitle();
                case "orgUnitPrintTitle":
                    return obj.getOrgUnitPrintTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "employeePostPrintTitle":
                    obj.setEmployeePostPrintTitle((String) value);
                    return;
                case "orgUnitPrintTitle":
                    obj.setOrgUnitPrintTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "employeePost":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "orgUnit":
                        return true;
                case "employeePostPrintTitle":
                        return true;
                case "orgUnitPrintTitle":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "employeePost":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "orgUnit":
                    return true;
                case "employeePostPrintTitle":
                    return true;
                case "orgUnitPrintTitle":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "employeePost":
                    return EmployeePost.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "employeePostPrintTitle":
                    return String.class;
                case "orgUnitPrintTitle":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrollmentComissionToEmployeePost> _dslPath = new Path<EnrollmentComissionToEmployeePost>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrollmentComissionToEmployeePost");
    }
            

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentComissionToEmployeePost#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentComissionToEmployeePost#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentComissionToEmployeePost#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Печатное название должности.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentComissionToEmployeePost#getEmployeePostPrintTitle()
     */
    public static PropertyPath<String> employeePostPrintTitle()
    {
        return _dslPath.employeePostPrintTitle();
    }

    /**
     * @return Печатное название подразделения.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentComissionToEmployeePost#getOrgUnitPrintTitle()
     */
    public static PropertyPath<String> orgUnitPrintTitle()
    {
        return _dslPath.orgUnitPrintTitle();
    }

    public static class Path<E extends EnrollmentComissionToEmployeePost> extends EntityPath<E>
    {
        private EmployeePost.Path<EmployeePost> _employeePost;
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PropertyPath<String> _employeePostPrintTitle;
        private PropertyPath<String> _orgUnitPrintTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentComissionToEmployeePost#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentComissionToEmployeePost#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentComissionToEmployeePost#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Печатное название должности.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentComissionToEmployeePost#getEmployeePostPrintTitle()
     */
        public PropertyPath<String> employeePostPrintTitle()
        {
            if(_employeePostPrintTitle == null )
                _employeePostPrintTitle = new PropertyPath<String>(EnrollmentComissionToEmployeePostGen.P_EMPLOYEE_POST_PRINT_TITLE, this);
            return _employeePostPrintTitle;
        }

    /**
     * @return Печатное название подразделения.
     * @see ru.tandemservice.eventssecrmc.entity.EnrollmentComissionToEmployeePost#getOrgUnitPrintTitle()
     */
        public PropertyPath<String> orgUnitPrintTitle()
        {
            if(_orgUnitPrintTitle == null )
                _orgUnitPrintTitle = new PropertyPath<String>(EnrollmentComissionToEmployeePostGen.P_ORG_UNIT_PRINT_TITLE, this);
            return _orgUnitPrintTitle;
        }

        public Class getEntityClass()
        {
            return EnrollmentComissionToEmployeePost.class;
        }

        public String getEntityName()
        {
            return "enrollmentComissionToEmployeePost";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
