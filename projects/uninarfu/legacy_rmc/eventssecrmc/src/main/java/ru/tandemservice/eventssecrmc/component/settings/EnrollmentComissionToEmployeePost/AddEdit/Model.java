package ru.tandemservice.eventssecrmc.component.settings.EnrollmentComissionToEmployeePost.AddEdit;

import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.eventssecrmc.entity.EnrollmentComissionToEmployeePost;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

@Input({
        @org.tandemframework.core.component.Bind(key = "id", binding = "rel.id"),
        @org.tandemframework.core.component.Bind(key = "enrollmentCampaignId", binding = "enrollmentCampaign.id")
})
public class Model {

    private EnrollmentCampaign enrollmentCampaign = new EnrollmentCampaign();
    private EnrollmentComissionToEmployeePost rel = new EnrollmentComissionToEmployeePost();

    private ISelectModel employeePostModel;
    private ISelectModel orgUnitModel;

    public EnrollmentComissionToEmployeePost getRel() {
        return rel;
    }

    public void setRel(EnrollmentComissionToEmployeePost rel) {
        this.rel = rel;
    }

    public EnrollmentCampaign getEnrollmentCampaign() {
        return enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign) {
        this.enrollmentCampaign = enrollmentCampaign;
    }

    public ISelectModel getEmployeePostModel() {
        return employeePostModel;
    }

    public void setEmployeePostModel(ISelectModel employeePostModel) {
        this.employeePostModel = employeePostModel;
    }

    public ISelectModel getOrgUnitModel() {
        return orgUnitModel;
    }

    public void setOrgUnitModel(ISelectModel orgUnitModel) {
        this.orgUnitModel = orgUnitModel;
    }


}
