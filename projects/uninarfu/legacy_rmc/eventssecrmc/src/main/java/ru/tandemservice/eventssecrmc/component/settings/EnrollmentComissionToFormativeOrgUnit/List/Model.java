package ru.tandemservice.eventssecrmc.component.settings.EnrollmentComissionToFormativeOrgUnit.List;


import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.eventssecrmc.entity.EnrollmentCommission2formativeOrgUnit;

public class Model {

    private DynamicListDataSource<EnrollmentCommission2formativeOrgUnit> dataSource;

    public DynamicListDataSource<EnrollmentCommission2formativeOrgUnit> getDataSource() {
        return dataSource;
    }

    public void setDataSource(
            DynamicListDataSource<EnrollmentCommission2formativeOrgUnit> dataSource)
    {
        this.dataSource = dataSource;
    }


}
