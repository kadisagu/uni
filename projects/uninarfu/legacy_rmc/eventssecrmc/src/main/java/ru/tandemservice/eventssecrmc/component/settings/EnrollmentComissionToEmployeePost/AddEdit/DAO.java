package ru.tandemservice.eventssecrmc.component.settings.EnrollmentComissionToEmployeePost.AddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt;
import ru.tandemservice.eventssecrmc.entity.EnrollmentComissionToEmployeePost;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(final Model model) {
        if (model.getEnrollmentCampaign().getId() != null)
            model.setEnrollmentCampaign(getNotNull(EnrollmentCampaign.class, model.getEnrollmentCampaign().getId()));

        if (model.getRel().getId() != null)
            model.setRel(getNotNull(EnrollmentComissionToEmployeePost.class, model.getRel().getId()));
        else
            model.getRel().setEnrollmentCampaign(model.getEnrollmentCampaign());


        model.setEmployeePostModel(new UniQueryFullCheckSelectModel(EmployeePost.fullTitle().s()) {
            @Override
            protected MQBuilder query(String alias, String filter)
            {

                // заранее отбираем сотрудников по принадлежности к комиссиям
                MQBuilder subBuilder = new MQBuilder(EnrollmentCampaignSecExt.ENTITY_CLASS, "red");
                subBuilder.getSelectAliasList().clear();
                subBuilder.addSelect(EnrollmentCampaignSecExt.orgUnitType().id().fromAlias("red").s());
                subBuilder.add(MQExpression.isNotNull("red", EnrollmentCampaignSecExt.orgUnitType().s()));

                if (model.getEnrollmentCampaign().getId() != null)
                    subBuilder.add(MQExpression.eq("red", EnrollmentCampaignSecExt.enrollmentCampaign().id(), model.getEnrollmentCampaign().getId()));

                subBuilder.setNeedDistinct(true);

                List<Long> ids = subBuilder.getResultList(getSession());

                // сотрудник выбран
                if (model.getRel() != null && model.getRel().getEmployeePost() != null && model.getRel().getEmployeePost().getOrgUnit() != null)
                    ids.add(model.getRel().getEmployeePost().getOrgUnit().getOrgUnitType().getId());

                MQBuilder builder = new MQBuilder(EmployeePost.ENTITY_CLASS, alias);
                builder.add(MQExpression.in(alias, EmployeePost.orgUnit().orgUnitType().id(), ids));

				/*
				// если орг юнит выбран, фильтруем сотрудника
				// иначе полный список
				if (model.getRel().getOrgUnit()!=null)
					builder.add(MQExpression.eq(alias, EmployeePost.orgUnit(), model.getRel().getOrgUnit()));
				*/

                if (!StringUtils.isEmpty(filter))
                    builder.add(MQExpression.like(alias, EmployeePost.person().identityCard().lastName(), filter));
                return builder;
            }
        });


        model.setOrgUnitModel(new UniQueryFullCheckSelectModel(OrgUnit.titleWithType().s()) {

            @Override
            protected MQBuilder query(String alias, String filter)
            {
                // подзапрос - подразделения только с сотрудниками
				/*
				MQBuilder subBuilder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "red");
				subBuilder.getSelectAliasList().clear();
				subBuilder.addSelect(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().formativeOrgUnit().id().fromAlias("red").s());
				if (model.getEnrollmentCampaign().getId()!= null)
					subBuilder.add(MQExpression.eq("red", RequestedEnrollmentDirection.enrollmentDirection().enrollmentCampaign().id(), model.getEnrollmentCampaign().getId()));
				subBuilder.setNeedDistinct(true);
					*/

                // по всем расширенным настройкам определим типы отборочных комиссий
                MQBuilder subBuilder = new MQBuilder(EnrollmentCampaignSecExt.ENTITY_CLASS, "red");
                subBuilder.getSelectAliasList().clear();
                subBuilder.addSelect(EnrollmentCampaignSecExt.orgUnitType().id().fromAlias("red").s());
                subBuilder.add(MQExpression.isNotNull("red", EnrollmentCampaignSecExt.orgUnitType().s()));

                if (model.getEnrollmentCampaign().getId() != null)
                    subBuilder.add(MQExpression.eq("red", EnrollmentCampaignSecExt.enrollmentCampaign().id(), model.getEnrollmentCampaign().getId()));
                subBuilder.setNeedDistinct(true);
                List<Long> ids = subBuilder.getResultList(getSession());

                // если уже выбран сотрудник, то следует отфильтроваться и с учетом подразделений сотрудника
                if (model.getRel() != null && model.getRel().getEmployeePost() != null && model.getRel().getEmployeePost().getOrgUnit() != null) {
                    OrgUnitType ouType = model.getRel().getEmployeePost().getOrgUnit().getOrgUnitType();
                    ids.add(ouType.getId());
                }

                MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, alias);
                builder.add(MQExpression.in(alias, OrgUnit.orgUnitType().id(), ids));


                // если уже выбран сотрудник, то следует отфильтроваться и с учетом подразделений сотрудника
                if (model.getRel().getEmployeePost() != null) {
                    OrgUnit ouEmployer = model.getRel().getEmployeePost().getOrgUnit();
                    builder.add(MQExpression.eq(alias, OrgUnit.id(), ouEmployer.getId()));
                }
				
				/*
				else
					// если сотрудник не выбрао - болт
					builder.add(MQExpression.eq(alias, OrgUnit.id(), 1L));
				*/

                if (!StringUtils.isEmpty(filter))
                    builder.add(MQExpression.like(alias, OrgUnit.title(), filter));

                return builder;
            }
        });


    }

    @Override
    public void update(Model model) {
        saveOrUpdate(model.getRel());
    }
}
