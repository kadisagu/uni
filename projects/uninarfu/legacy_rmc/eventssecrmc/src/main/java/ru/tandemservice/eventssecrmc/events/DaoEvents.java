package ru.tandemservice.eventssecrmc.events;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.shared.commonbase.base.entity.DeclinableProperty;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.fias.base.entity.Address;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.*;
import ru.tandemservice.eventssecrmc.dao.DaoFacade;
import ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt;
import ru.tandemservice.eventssecrmc.entity.EnrollmentComissionToEmployeePost;
import ru.tandemservice.eventssecrmc.entity.EntrantRequestSecExt;
import ru.tandemservice.eventssecrmc.events.Events.EntityInfo;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniec.entity.catalog.codes.EntrantStateCodes;
import ru.tandemservice.uniec.entity.entrant.*;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;

public class DaoEvents
        extends UniBaseDao implements IDaoEvents
{
    private List<String> entityTypeList = null;

    private Map<EmployeePost, List<EnrollmentComissionToEmployeePost>> mapComission = null; //new HashMap<EmployeePost, EnrollmentComissionToEmployeePost>();
    private Map<EnrollmentCampaign, EnrollmentCampaignSecExt> mapEnrollmentCampaignSecExt = null;

    /**
     * список нужных нам типов сущностей
     *
     * @return
     */
    public List<String> getEntityTypeList()
    {
        if (entityTypeList == null)
            entityTypeList = initEntityTypeList();

        return entityTypeList;
    }

    public void clearCache()
    {
        mapComission = null;
        mapEnrollmentCampaignSecExt = null;

    }

    public void createCache()
    {
        _createComission();
        _createEnrollmentCompaintSecExt();
    }

    private void _createEnrollmentCompaintSecExt()
    {
        if (mapEnrollmentCampaignSecExt != null)
            return;

        mapEnrollmentCampaignSecExt = new HashMap<EnrollmentCampaign, EnrollmentCampaignSecExt>();
        // создать кеш
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EnrollmentCampaignSecExt.class, "ent")
                .column("ent");

        List<EnrollmentCampaignSecExt> lst = dql.createStatement(getSession()).list();

        for (EnrollmentCampaignSecExt ent : lst) {
            EnrollmentCampaign ec = ent.getEnrollmentCampaign();
            mapEnrollmentCampaignSecExt.put(ec, ent);
        }
    }


    private void _createComission()
    {
        if (mapComission != null)
            return;

        mapComission = new HashMap<EmployeePost, List<EnrollmentComissionToEmployeePost>>();

        // Только приемки с активными настройками
        DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(EnrollmentCampaignSecExt.class, "ein")
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentCampaignSecExt.useEntrantRequestSecExt().fromAlias("ein")), DQLExpressions.value(true)))
                .column(EnrollmentCampaignSecExt.enrollmentCampaign().id().fromAlias("ein").s());


        // создать кеш
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EnrollmentComissionToEmployeePost.class, "ent");
        // только приемки с включенной настройкой
        dql.where(DQLExpressions.in(DQLExpressions.property(EnrollmentComissionToEmployeePost.enrollmentCampaign().id().fromAlias("ent")), dqlIn.getQuery()));
        dql.column("ent");

        List<EnrollmentComissionToEmployeePost> lst = dql.createStatement(getSession()).list();

        for (EnrollmentComissionToEmployeePost ent : lst) {
            EmployeePost ePost = ent.getEmployeePost();
            List<EnrollmentComissionToEmployeePost> settingList = null;

            if (mapComission.containsKey(ePost))
                settingList = mapComission.get(ePost);
            else {
                settingList = new ArrayList<EnrollmentComissionToEmployeePost>();
                mapComission.put(ePost, settingList);
            }

            if (!settingList.contains(settingList))
                settingList.add(ent);
        }
    }

    public boolean hasEmployeePostSettings()
    {
        if (mapComission == null || mapComission.keySet().size() == 0)
            return false;
        else
            return true;
    }

    /**
     * начальная инициализация нужного нам списка сущностей
     *
     * @return
     */
    protected List<String> initEntityTypeList()
    {
        List<String> retVal = new ArrayList<String>();

        retVal.add(Entrant.ENTITY_NAME);
        retVal.add(EntrantRequest.ENTITY_NAME);
        retVal.add(RequestedEnrollmentDirection.ENTITY_NAME);
        retVal.add(Person.ENTITY_NAME);
        retVal.add(IdentityCard.ENTITY_NAME);
        retVal.add(PersonNextOfKin.ENTITY_NAME);
        retVal.add(PersonBenefit.ENTITY_NAME);
        retVal.add(PersonForeignLanguage.ENTITY_NAME);
        retVal.add(PersonSportAchievement.ENTITY_NAME);
        retVal.add(EntrantEnrollmentDocument.ENTITY_NAME);
        retVal.add(EntrantStateExamCertificate.ENTITY_NAME);
        retVal.add(StateExamSubjectMark.ENTITY_NAME);
        retVal.add(OlympiadDiploma.ENTITY_NAME);
        retVal.add(ChosenEntranceDiscipline.ENTITY_NAME);

        retVal.add(EntrantEnrolmentRecommendation.ENTITY_NAME);
        retVal.add(EntrantAccessCourse.ENTITY_NAME);
        retVal.add(EntrantAccessDepartment.ENTITY_NAME);
        retVal.add(EntrantInfoAboutUniversity.ENTITY_NAME);
        retVal.add(DeclinableProperty.ENTITY_NAME);


        // левая сущность - общага от нарфу
        //EntrantEnrolmentRecommendation
        //EntrantAccessCourse
        //EntrantAccessDepartment
        // EntrantInfoAboutUniversity

        return retVal;
    }

    private Map<String, List<String>> _passChangedProperty = null;

    public Map<String, List<String>> getPassChangedProperty()
    {
        if (_passChangedProperty == null)
            _passChangedProperty = initPassChangedProperty();

        return _passChangedProperty;
    }

    protected Map<String, List<String>> initPassChangedProperty()
    {
        Map<String, List<String>> retVal = new HashMap<String, List<String>>();

        List<String> lstRequestEnrollmendDirProp = new ArrayList<String>();
        lstRequestEnrollmendDirProp.add(RequestedEnrollmentDirection.P_PRIORITY_PER_ENTRANT);
        lstRequestEnrollmendDirProp.add(RequestedEnrollmentDirection.P_VERSION);

        retVal.put(RequestedEnrollmentDirection.ENTITY_NAME.toLowerCase(), lstRequestEnrollmendDirProp);

        return retVal;
    }

    public boolean passChangedProperty(DSetEvent event)
    {
        String clsName = event.getMultitude().getEntityMeta().getName().toLowerCase();

        Map<String, List<String>> map = getPassChangedProperty();
        if (map.containsKey(clsName)) {
            // ключ есть, просмотрим все измененные свойства
            if (event.getMultitude().getAffectedProperties().isEmpty())
                return true;
            else {
                // все измененнные свойства должны быть в коллекции
                List<String> lst = map.get(clsName);
                for (String propName : event.getMultitude().getAffectedProperties()) {
                    if (!lst.contains(propName))
                        return false;
                }
                return true;
            }
        }
        else
            return false;

    }


    @Override
    public boolean needEvent(DSetEvent event)
    {
        IEntityMeta entityMeta = event.getMultitude().getEntityMeta();
        String className = entityMeta.getName();

        if (getEntityTypeList().contains(className))
            return true;
        else
            return false;
    }

    @Override
    public List<EntityInfo> getEntityList(List<EntityInfo> list)
    {
        String classname = list.get(0).getClassname();
        Set<Long> idSet = new HashSet<Long>();
        DSetEventType eventType = DSetEventType.afterUpdate;

        for (EntityInfo info : list) {
            idSet.add(info.getId());
            eventType = info.getEventType();

        }
        Map<String, Set<Long>> map = new HashMap<String, Set<Long>>();
        map.put(classname, idSet);

        findReferenseObject(map);

        List<EntityInfo> resultList = new ArrayList<EntityInfo>();
        for (Map.Entry<String, Set<Long>> entry : map.entrySet())
            for (long id : entry.getValue())
                resultList.add(new EntityInfo(id, entry.getKey(), eventType));

        return resultList;
    }

    protected Set<Long> getSaved(Map<String, Set<Long>> map, String classname)
    {
        if (!map.containsKey(classname))
            map.put(classname, new HashSet<Long>());

        return map.get(classname);
    }

    /**
     * Найти ссылки на объекты
     * (по паспорту - заявление, и так далее)
     *
     * @param map
     */
    protected void findReferenseObject(Map<String, Set<Long>> map)
    {
        // по адресу удостоверение личности + персона
        fillByAdress(map);

        // по склонятору - удостоверение личности
        fillByDeclinableProperty(map);


        // по уд. личности - персону
        fillByIdentityCard(map);

        // PersonEduInstitution - персона
        fillByPersonEduInstitution(map);

        // родственники - персона
        fillByPersonnextofkin(map);

        // льготы - персона
        fillByPersonBenefit(map);

        // иностранный язык - персона
        fillByPersonForeignLanguage(map);

        // спортивные достижения - персона
        fillByPersonSportAchievement(map);

        fillByPersonExt(map);

        // по персоне ищем абитуриентов
        fillByPerson(map);

        // документы, здаваемые с заявлением
        fillByEntrantEnrollmentDocument(map);

        // ищем по персоне активного студента
        // если студент найден
        // все настройки можно отключить
        fillStudentByPerson(map);


        // свидетельства ЕГЭ - абитуриент
        fillByEGEEntrantStateExamCertificate(map);
        fillByEGEStateExamSubjectMark(map);

        // олимпиады - абитуриент
        fillOlimpiad(map);

        fillByEntrantEnrolmentRecommendation(map);
        fillByEntrantAccessCourse(map);
        fillByEntrantAccessDepartment(map);
        fillByEntrantInfoAboutUniversity(map);

        // выбранные вст. испытания ChosenEntranceDiscipline - заявление
        fillByChosenEntranceDiscipline(map);

        // по абитуриентам ищем заявления (только не забрал документы)
        fillByEntrant(map);

        // по выбранным направлениям для приема ищем заявления
        fillByRequestEnrollmentDirection(map);

        // FILTRATE ETRANT request by state
        filtrateEntrantByState(map);

        // фильтруем заявления
        filtrateEntrantRequest(map);
    }

    private void filtrateEntrantByState(Map<String, Set<Long>> map)
    {
        Set<Long> findIds = getSaved(map, EntrantRequest.ENTITY_NAME);
        if (findIds.isEmpty())
            return;

        List<Long> ids = new ArrayList<Long>();
        ids.addAll(findIds);
        List<EnrollmentCampaignSecExt> lstEc = getEnrollmentCampaignSecExtListById(ids);


        if (isUseEntrantState(lstEc)) {
            // Если заявление абитуриента связано с абитуриентом в статусе зачислен, в приказе, пред зачислен.
            // то такие нам не нужны
            // как итог, в выборке остались только нужные

            List<Long> list = new DQLSelectBuilder().fromEntity(EntrantRequest.class, "e")
                    .where(DQLExpressions.in(DQLExpressions.property(EntrantRequest.id().fromAlias("e")), findIds))
                    .where(DQLExpressions.notIn(DQLExpressions.property(EntrantRequest.entrant().state().code().fromAlias("e")), getEntrantStatePassCode()))
                    .column(EntrantRequest.id().fromAlias("e").s())
                    .createStatement(getSession())
                    .list();

            getSaved(map, EntrantRequest.ENTITY_NAME).clear();
            getSaved(map, EntrantRequest.ENTITY_NAME).addAll(list);
        }
    }

    private void fillStudentByPerson(Map<String, Set<Long>> map)
    {
        // на основе персоны ищем всех студентов

        Set<Long> personIds = getSaved(map, Person.ENTITY_NAME);
        if (personIds.isEmpty())
            return;

        List<Long> list = new DQLSelectBuilder().fromEntity(Student.class, "e")
                .where(DQLExpressions.in(DQLExpressions.property(Student.person().id().fromAlias("e")), personIds))

                        // исключая закончивших с дипломом и отчисленных
                .where(DQLExpressions.notIn(DQLExpressions.property(Student.status().code().fromAlias("e")), getStudentStatePassCode()))
                .column(Student.id().fromAlias("e").s())
                .createStatement(getSession())
                .list();

        getSaved(map, Student.ENTITY_NAME).addAll(list);

    }

    private void fillByDeclinableProperty(Map<String, Set<Long>> map)
    {
        // DeclinableProperty
        Set<Long> findIds = getSaved(map, DeclinableProperty.ENTITY_NAME);
        if (findIds.isEmpty())
            return;

        DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(DeclinableProperty.class, "ein")
                .where(DQLExpressions.in(DQLExpressions.property(DeclinableProperty.id().fromAlias("ein")), findIds))
                .column(DeclinableProperty.declinable().id().fromAlias("ein").s());


        List<Long> list = new DQLSelectBuilder().fromEntity(IdentityCardDeclinability.class, "e")
                .where(DQLExpressions.in(DQLExpressions.property(IdentityCardDeclinability.id().fromAlias("e")), dqlIn.getQuery()))
                .column(IdentityCardDeclinability.identityCard().id().fromAlias("e").s())
                .setPredicate(DQLPredicateType.distinct)
                .createStatement(getSession())
                .list();

        getSaved(map, IdentityCard.ENTITY_NAME).addAll(list);
    }

    private void fillByEntrantInfoAboutUniversity(Map<String, Set<Long>> map) {
        // EntrantInfoAboutUniversity
        Set<Long> findIds = getSaved(map, EntrantInfoAboutUniversity.ENTITY_NAME);
        if (findIds.isEmpty())
            return;

        DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(EntrantInfoAboutUniversity.class, "ein")
                .where(DQLExpressions.in(DQLExpressions.property(EntrantInfoAboutUniversity.id().fromAlias("ein")), findIds))
                .column(EntrantInfoAboutUniversity.entrant().id().fromAlias("ein").s());

        List<Long> list = dqlIn.createStatement(getSession()).list();
        getSaved(map, Entrant.ENTITY_NAME).addAll(list);

    }

    private void fillByEntrantAccessDepartment(Map<String, Set<Long>> map) {
        // EntrantAccessDepartment
        Set<Long> findIds = getSaved(map, EntrantAccessDepartment.ENTITY_NAME);
        if (findIds.isEmpty())
            return;

        DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(EntrantAccessDepartment.class, "ein")
                .where(DQLExpressions.in(DQLExpressions.property(EntrantAccessDepartment.id().fromAlias("ein")), findIds))
                .column(EntrantAccessDepartment.entrant().id().fromAlias("ein").s());

        List<Long> list = dqlIn.createStatement(getSession()).list();
        getSaved(map, Entrant.ENTITY_NAME).addAll(list);

    }

    private void fillByEntrantAccessCourse(Map<String, Set<Long>> map) {
        // EntrantAccessCourse
        Set<Long> findIds = getSaved(map, EntrantAccessCourse.ENTITY_NAME);
        if (findIds.isEmpty())
            return;

        DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(EntrantAccessCourse.class, "ein")
                .where(DQLExpressions.in(DQLExpressions.property(EntrantAccessCourse.id().fromAlias("ein")), findIds))
                .column(EntrantAccessCourse.entrant().id().fromAlias("ein").s());

        List<Long> list = dqlIn.createStatement(getSession()).list();
        getSaved(map, Entrant.ENTITY_NAME).addAll(list);
    }

    private void fillByEntrantEnrolmentRecommendation(Map<String, Set<Long>> map) {
        // EntrantEnrolmentRecommendation
        Set<Long> findIds = getSaved(map, EntrantEnrolmentRecommendation.ENTITY_NAME);
        if (findIds.isEmpty())
            return;

        DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(EntrantEnrolmentRecommendation.class, "ein")
                .where(DQLExpressions.in(DQLExpressions.property(EntrantEnrolmentRecommendation.id().fromAlias("ein")), findIds))
                .column(EntrantEnrolmentRecommendation.entrant().id().fromAlias("ein").s());

        List<Long> list = dqlIn.createStatement(getSession()).list();
        getSaved(map, Entrant.ENTITY_NAME).addAll(list);
    }

    protected void fillByPersonExt(Map<String, Set<Long>> map)
    {
        // для расширений
    }

    private void fillByPersonSportAchievement(Map<String, Set<Long>> map) {
        // PersonSportAchievement

        Set<Long> findIds = getSaved(map, PersonSportAchievement.ENTITY_NAME);
        if (findIds.isEmpty())
            return;

        DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(PersonSportAchievement.class, "ein")
                .where(DQLExpressions.in(DQLExpressions.property(PersonSportAchievement.id().fromAlias("ein")), findIds))
                .column(PersonSportAchievement.person().id().fromAlias("ein").s());

        List<Long> list = dqlIn.createStatement(getSession()).list();
        getSaved(map, Person.ENTITY_NAME).addAll(list);
    }

    private void fillByPersonForeignLanguage(Map<String, Set<Long>> map) {
        //PersonForeignLanguage
        Set<Long> findIds = getSaved(map, PersonForeignLanguage.ENTITY_NAME);
        if (findIds.isEmpty())
            return;

        DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(PersonForeignLanguage.class, "ein")
                .where(DQLExpressions.in(DQLExpressions.property(PersonForeignLanguage.id().fromAlias("ein")), findIds))
                .column(PersonForeignLanguage.person().id().fromAlias("ein").s());

        List<Long> list = dqlIn.createStatement(getSession()).list();
        getSaved(map, Person.ENTITY_NAME).addAll(list);
    }

    private void fillByPersonBenefit(Map<String, Set<Long>> map) {
        // PersonBenefit
        Set<Long> findIds = getSaved(map, PersonBenefit.ENTITY_NAME);
        if (findIds.isEmpty())
            return;

        DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(PersonBenefit.class, "ein")
                .where(DQLExpressions.in(DQLExpressions.property(PersonBenefit.id().fromAlias("ein")), findIds))
                .column(PersonBenefit.person().id().fromAlias("ein").s());

        List<Long> list = dqlIn.createStatement(getSession()).list();
        getSaved(map, Person.ENTITY_NAME).addAll(list);
    }

    private void fillByPersonnextofkin(Map<String, Set<Long>> map)
    {
        // PersonNextOfKin
        Set<Long> findIds = getSaved(map, PersonNextOfKin.ENTITY_NAME);
        if (findIds.isEmpty())
            return;

        DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(PersonNextOfKin.class, "ein")
                .where(DQLExpressions.in(DQLExpressions.property(PersonNextOfKin.id().fromAlias("ein")), findIds))
                .column(PersonNextOfKin.person().id().fromAlias("ein").s());

        List<Long> list = dqlIn.createStatement(getSession()).list();
        getSaved(map, Person.ENTITY_NAME).addAll(list);
    }

    private void fillByPersonEduInstitution(Map<String, Set<Long>> map)
    {
        // PersonEduInstitution
        Set<Long> findIds = getSaved(map, PersonEduInstitution.ENTITY_NAME);
        if (findIds.isEmpty())
            return;

        DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(PersonEduInstitution.class, "ein")
                .where(DQLExpressions.in(DQLExpressions.property(PersonEduInstitution.id().fromAlias("ein")), findIds))
                .column(PersonEduInstitution.person().id().fromAlias("ein").s());

        List<Long> list = dqlIn.createStatement(getSession()).list();
        getSaved(map, Person.ENTITY_NAME).addAll(list);

    }

    private void filtrateEntrantRequest(Map<String, Set<Long>> map) {

        Set<Long> findIds = getSaved(map, EntrantRequest.ENTITY_NAME);
        if (findIds.isEmpty())
            return;


        int count = 0;
        for (String key : map.keySet()) {
            Set<Long> lst = map.get(key);
            if (lst != null && lst.size() > 0) {
                count += lst.size();
            }
        }

        if (count == 1) {
            // изменяют только заявление
            // никакие цепочки не отработали
            // оставляем как есть, isTakeAway не проверяем
            return;
        }

        List<Long> list = new DQLSelectBuilder().fromEntity(EntrantRequest.class, "e")
                .where(DQLExpressions.in(DQLExpressions.property(EntrantRequest.id().fromAlias("e")), findIds))
                        // документы не забирал
                .where(DQLExpressions.eq(DQLExpressions.property(EntrantRequest.takeAwayDocument().fromAlias("e")), DQLExpressions.value(false)))
                .column(EntrantRequest.id().fromAlias("e").s())
                .createStatement(getSession())
                .list();

        // если заявлений >1
        // то нужно выбрать только одно (свое)
        // если свое заяление не найдено - оставить все
        // vch 2013/06/28
        // если у нас есть контекст (залогинились как пользователь, то нужно отобрать только свое заявление)
        EmployeePost executerPost = getExecutorPost();

        if (list.size() > 1 && executerPost != null) {

            Long myId = null; // мое заявление (под моим executerPost)

            // оставим только заяву из той-же отборочной комиссии
            List<EntrantRequest> listEntrant = new DQLSelectBuilder().fromEntity(EntrantRequest.class, "e")
                    .where(DQLExpressions.in(DQLExpressions.property(EntrantRequest.id().fromAlias("e")), list))
                    .column("e")
                    .createStatement(getSession())
                    .list();

            for (EntrantRequest erequest : listEntrant) {
                EnrollmentCampaign compaign = erequest.getEntrant().getEnrollmentCampaign();
                EnrollmentComissionToEmployeePost comission = getComission(executerPost, compaign);
                if (comission != null && comission.getOrgUnit() != null) {
                    // заданы права
                    EntrantRequestSecExt secExt = getEntrantRequestSecExt(erequest);
                    if (
                            secExt != null
                                    && secExt.getOrgUnit().getId().equals(comission.getOrgUnit().getId())
                            )
                    {
                        // мое заявление
                        myId = erequest.getId();
                    }
                }

                // сафу уточнения к постановке
                if (comission == null) {
                    // пользователь не в комиссии - это заявление можно не учитывать
                    // безопасность отключаем
                    Long removeId = erequest.getId();
                    list.remove(removeId);
                }
            }

            if (myId != null) {
                list = new ArrayList<Long>();
                list.add(myId);
            }
        }
        getSaved(map, EntrantRequest.ENTITY_NAME).clear();
        getSaved(map, EntrantRequest.ENTITY_NAME).addAll(list);
    }

    private void fillByChosenEntranceDiscipline(Map<String, Set<Long>> map)
    {
        // ChosenEntranceDiscipline
        Set<Long> findIds = getSaved(map, ChosenEntranceDiscipline.ENTITY_NAME);
        if (findIds.isEmpty())
            return;

        DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(ChosenEntranceDiscipline.class, "ein")
                .where(DQLExpressions.in(DQLExpressions.property(ChosenEntranceDiscipline.id().fromAlias("ein")), findIds))
                .column(ChosenEntranceDiscipline.chosenEnrollmentDirection().entrantRequest().id().fromAlias("ein").s());

        List<Long> list = dqlIn.createStatement(getSession()).list();
        getSaved(map, EntrantRequest.ENTITY_NAME).addAll(list);

    }

    private void fillOlimpiad(Map<String, Set<Long>> map) {
        // OlympiadDiploma
        Set<Long> findIds = getSaved(map, OlympiadDiploma.ENTITY_NAME);
        if (findIds.isEmpty())
            return;

        DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(OlympiadDiploma.class, "ein")
                .where(DQLExpressions.in(DQLExpressions.property(OlympiadDiploma.id().fromAlias("ein")), findIds))
                .column(OlympiadDiploma.entrant().id().fromAlias("ein").s());

        List<Long> list = dqlIn.createStatement(getSession()).list();
        getSaved(map, Entrant.ENTITY_NAME).addAll(list);

    }


    private void fillByEGEStateExamSubjectMark(Map<String, Set<Long>> map) {
        // StateExamSubjectMark
        Set<Long> findIds = getSaved(map, StateExamSubjectMark.ENTITY_NAME);
        if (findIds.isEmpty())
            return;

        DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(StateExamSubjectMark.class, "ein")
                .where(DQLExpressions.in(DQLExpressions.property(StateExamSubjectMark.id().fromAlias("ein")), findIds))
                .column(StateExamSubjectMark.certificate().entrant().id().fromAlias("ein").s());

        List<Long> list = dqlIn.createStatement(getSession()).list();
        getSaved(map, Entrant.ENTITY_NAME).addAll(list);

    }

    private void fillByEGEEntrantStateExamCertificate(Map<String, Set<Long>> map)
    {
        // EntrantStateExamCertificate
        Set<Long> findIds = getSaved(map, EntrantStateExamCertificate.ENTITY_NAME);
        if (findIds.isEmpty())
            return;

        DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(EntrantStateExamCertificate.class, "ein")
                .where(DQLExpressions.in(DQLExpressions.property(EntrantStateExamCertificate.id().fromAlias("ein")), findIds))
                .column(EntrantStateExamCertificate.entrant().id().fromAlias("ein").s());

        List<Long> list = dqlIn.createStatement(getSession()).list();
        getSaved(map, Entrant.ENTITY_NAME).addAll(list);

    }

    private void fillByEntrantEnrollmentDocument(Map<String, Set<Long>> map)
    {
        Set<Long> findIds = getSaved(map, EntrantEnrollmentDocument.ENTITY_NAME);
        if (findIds.isEmpty())
            return;

        DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(EntrantEnrollmentDocument.class, "ein")
                .where(DQLExpressions.in(DQLExpressions.property(EntrantEnrollmentDocument.id().fromAlias("ein")), findIds))
                .column(EntrantEnrollmentDocument.entrantRequest().id().fromAlias("ein").s());

        // не отозванные заявы
        List<Long> list = new DQLSelectBuilder().fromEntity(EntrantRequest.class, "e")
                .where(DQLExpressions.in(DQLExpressions.property(EntrantRequest.id().fromAlias("e")), dqlIn.getQuery()))
                        // документы не забирал
                .where(DQLExpressions.eq(DQLExpressions.property(EntrantRequest.takeAwayDocument().fromAlias("e")), DQLExpressions.value(false)))
                .column(EntrantRequest.id().fromAlias("e").s())
                .createStatement(getSession())
                .list();

        getSaved(map, EntrantRequest.ENTITY_NAME).addAll(list);
    }

    private void fillByRequestEnrollmentDirection(Map<String, Set<Long>> map)
    {
        Set<Long> redId = getSaved(map, RequestedEnrollmentDirection.ENTITY_NAME);
        if (redId.isEmpty())
            return;

        DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(RequestedEnrollmentDirection.class, "ein")
                .where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirection.id().fromAlias("ein")), redId))
                .column(RequestedEnrollmentDirection.entrantRequest().id().fromAlias("ein").s());

        List<Long> list = new DQLSelectBuilder().fromEntity(EntrantRequest.class, "e")
                .column(EntrantRequest.id().fromAlias("e").s())
                .where(in(DQLExpressions.property(EntrantRequest.id().fromAlias("e")), dqlIn.getQuery()))
                .createStatement(getSession())
                .list();


        getSaved(map, EntrantRequest.ENTITY_NAME).addAll(list);
    }

    private void fillByEntrant(Map<String, Set<Long>> map)
    {
        Set<Long> entrantIds = getSaved(map, Entrant.ENTITY_NAME);
        if (entrantIds.isEmpty())
            return;

        List<Long> list = new DQLSelectBuilder().fromEntity(EntrantRequest.class, "e")
                .where(DQLExpressions.in(DQLExpressions.property(EntrantRequest.entrant().id().fromAlias("e")), entrantIds))
                        // документы не забирал
                .where(DQLExpressions.eq(DQLExpressions.property(EntrantRequest.takeAwayDocument().fromAlias("e")), DQLExpressions.value(false)))

                        // ограничение по статусам
                .where(DQLExpressions.in(DQLExpressions.property(EntrantRequest.takeAwayDocument().fromAlias("e")), DQLExpressions.value(false)))

                .column(EntrantRequest.id().fromAlias("e").s())
                .createStatement(getSession())
                .list();


        getSaved(map, EntrantRequest.ENTITY_NAME).addAll(list);

    }

    /**
     * по персоне ищем абитуриентов
     *
     * @param map
     */
    private void fillByPerson(Map<String, Set<Long>> map)
    {
        Set<Long> personIds = getSaved(map, Person.ENTITY_NAME);
        if (personIds.isEmpty())
            return;

        List<Long> list = new DQLSelectBuilder().fromEntity(Entrant.class, "e")
                .where(DQLExpressions.in(DQLExpressions.property(Entrant.person().id().fromAlias("e")), personIds))
                .column(Entrant.id().fromAlias("e").s())
                .createStatement(getSession())
                .list();

        getSaved(map, Entrant.ENTITY_NAME).addAll(list);
    }

    private void fillByIdentityCard(Map<String, Set<Long>> map)
    {
        Set<Long> cardIds = getSaved(map, IdentityCard.ENTITY_NAME);
        if (cardIds.isEmpty())
            return;

        List<Long> list = new DQLSelectBuilder().fromEntity(Person.class, "e")
                .where(DQLExpressions.in(DQLExpressions.property(Person.identityCard().id().fromAlias("e")), cardIds))
                .column(Person.id().fromAlias("e").s())
                .createStatement(getSession())
                .list();
        getSaved(map, Person.ENTITY_NAME).addAll(list);
    }

    private void fillByAdress(Map<String, Set<Long>> map)
    {

        Set<Long> addressIds = getSaved(map, Address.ENTITY_NAME);
        if (addressIds.isEmpty())
            return;

        //удостоверение личности
        List<Long> list = new DQLSelectBuilder().fromEntity(IdentityCard.class, "e")
                .where(DQLExpressions.in(DQLExpressions.property(IdentityCard.address().id().fromAlias("e")), addressIds))
                .column(IdentityCard.id().fromAlias("e").s())
                .createStatement(getSession())
                .list();
        getSaved(map, IdentityCard.ENTITY_NAME).addAll(list);

        //персона
        list = new DQLSelectBuilder().fromEntity(Person.class, "e")
                .where(DQLExpressions.in(DQLExpressions.property(Person.address().id().fromAlias("e")), addressIds))
                .column(Person.id().fromAlias("e").s())
                .createStatement(getSession())
                .list();
        getSaved(map, Person.ENTITY_NAME).addAll(list);

    }

    @Override
    public List<CheckSecurity> checkSecurity
            (
                    EmployeePost executorPost
                    , DSetEvent event
                    , List<EntityInfo> entityInfoList
            )
    {

        if (
                event.getEventType() == DSetEventType.afterUpdate
                        || event.getEventType() == DSetEventType.beforeDelete
                )
        {
            List<CheckSecurity> retVal = new ArrayList<CheckSecurity>();
            for (EntityInfo entity : entityInfoList) {
                CheckSecurity chSec = checkSecurity(executorPost, event.getEventType(), entity);
                if (chSec != null)
                    retVal.add(chSec);
            }
            return retVal;
        }
        else
            // ничего не проверяем
            return null;
    }

    /**
     * после обновления/удаления
     *
     * @param executorPost
     * @param eventType
     * @param entity
     *
     * @return
     */
    protected CheckSecurity checkSecurity(
            EmployeePost executorPost
            , DSetEventType eventType
            , EntityInfo entity)
    {


        String msg = null;

        if (entity.getClassname().toLowerCase().equals(EntrantRequest.ENTITY_NAME.toLowerCase())) {

            EntrantRequest request = getEntrantRequest(entity.getId());
            if (request == null) {
                System.out.println("Для " + entity.getId() + " нет заявления");
            }

            // получим приемку, зная приемку - настройки прав доступа
            EnrollmentCampaign enrollmentCampaign = request.getEntrant().getEnrollmentCampaign();

            // Получим расширенные настройки
            EnrollmentCampaignSecExt secEcExt = getEnrollmentCampaignSecExt(enrollmentCampaign);

            // не использовать настройки без-ти
            if (!secEcExt.isUseEntrantRequestSecExt())
                return null;

            // получим OrgUnit исполнителя
            EnrollmentComissionToEmployeePost comissionExecutor = getComission(executorPost, enrollmentCampaign);
            // сафу попросило отключать расширенные настройки, если пользователь не указан в расширенных настройках
            if (comissionExecutor == null)
                return null;

            EntrantRequestSecExt secExt = getEntrantRequestSecExt(request);

            if (secExt == null) {
                // нет настроек без-ти
                // значит можно делать что угодно
                return null;
            }
            else {
                // указано расширение
                if (comissionExecutor == null)
                    msg = "Заявление " + request.getTitle() + " запрещено редактировать. Вы не являетесь сотрудником отборочной комиссии";
                else {
                    if (comissionExecutor.getOrgUnit() == null) {
                        // можно делать все что хочется
                        // типа супер пользователь
                        return null;
                    }
                    else {
                        OrgUnit comissionExsist = secExt.getOrgUnit();

                        //comissionExecutor.getOrgUnit()==null - это супер пользователь
                        if (comissionExecutor.getOrgUnit() != null
                                && !comissionExecutor.getOrgUnit().getId().equals(comissionExsist.getId())
                                )
                        {
                            // из разных комиссий
                            if (eventType == DSetEventType.afterUpdate) {
                                if (secEcExt.isDisallowEditRequest())
                                    msg = "Заявление " + request.getTitle() + " создано в другой отборочной комиссии, редактирование запрещено";
                            }

                            if (eventType == DSetEventType.beforeDelete) {
                                if (secEcExt.isDisallowDeleteRequest())
                                    msg = "Заявление " + request.getTitle() + " создано в другой отборочной комиссии, удаление запрещено";
                            }
                        }
                    }
                }
            }

            if (msg != null) {
                CheckSecurity retVal = new CheckSecurity();
                retVal.setEntity(request);
                retVal.setSecurityMessage(msg);
                return retVal;
            }
        }
        return null;
    }


    public EnrollmentComissionToEmployeePost getComission(
            EmployeePost executorPost
            , EnrollmentCampaign enrollmentCampaign
    )
    {
        if (mapComission == null)
            createCache();
        if (mapComission.containsKey(executorPost)) {
            List<EnrollmentComissionToEmployeePost> lst = mapComission.get(executorPost);
            for (EnrollmentComissionToEmployeePost epost : lst) {
                if (epost.getEnrollmentCampaign().getId().equals(enrollmentCampaign.getId()))
                    return epost;
            }
            return null;
        }
        else
            return null;
    }

    /**
     * Расширенные настройки без-ти по приемной компании
     *
     * @param enrollmentCampaign
     *
     * @return
     */
    private EnrollmentCampaignSecExt getEnrollmentCampaignSecExt(
            EnrollmentCampaign enrollmentCampaign)
    {
        if (mapEnrollmentCampaignSecExt == null)
            createCache();

        if (mapEnrollmentCampaignSecExt.containsKey(enrollmentCampaign))
            return mapEnrollmentCampaignSecExt.get(enrollmentCampaign);
        else {
            // создаем умолчание
            EnrollmentCampaignSecExt retVal = new EnrollmentCampaignSecExt();
            retVal.setEnrollmentCampaign(enrollmentCampaign);

            retVal.setAllowActiveRequest(1);
            retVal.setDisallowChangeRequestOwner(false);
            retVal.setDisallowCreateRequest(true);
            retVal.setDisallowDeleteRequest(true);
            retVal.setDisallowEditRequest(true);

            retVal.setUseEntrantRequestSecExt(true);

            retVal.setUseEntrantState(true);
            retVal.setUseStudentState(true);


            return retVal;
        }

    }

    private EntrantRequestSecExt getEntrantRequestSecExt(EntrantRequest request)
    {
        EntrantRequestSecExt item = get(EntrantRequestSecExt.class, EntrantRequestSecExt.L_ENTRANT_REQUEST, request);
        if (item == null) {
            // <TODO>
            return null;
        }
        else
            return item;
    }

    private EntrantRequest getEntrantRequest(Long id)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EntrantRequest.class, "e").column("e").where(DQLExpressions.eq(DQLExpressions.property(EntrantRequest.id().fromAlias("e")), DQLExpressions.value(id)));
        List<EntrantRequest> lst = dql.createStatement(getSession()).list();

        if (!lst.isEmpty())
            return lst.get(0);
        else
            return null;
    }

    @Override
    public boolean actionFromTrigger
            (
                    EmployeePost employeePost
                    , DSetEventType eventType
                    , String entityType
                    , Long id
                    , List<Object> prevSource
            )
    {

        if (EntrantRequest.ENTITY_NAME.toLowerCase().equals(entityType.toLowerCase())) {
            //EntrantRequest request = get(EntrantRequest.class, id);
            EntrantRequest request = getEntrantRequest(id);
            if (request == null) {
                System.out.println("Для " + id + " нет заявления (actionFromTrigger)");
                return false;
            }

            EnrollmentComissionToEmployeePost comission = getComission(employeePost, request.getEntrant().getEnrollmentCampaign());
            // сафу попросило отключать расширенные настройки, если пользователь не указан в расширенных настройках
            if (comission == null)
                return false;

            EnrollmentCampaign ec = request.getEntrant().getEnrollmentCampaign();
            // Получим расширенные настройки
            EnrollmentCampaignSecExt secEcExt = getEnrollmentCampaignSecExt(ec);

            if (!secEcExt.isUseEntrantRequestSecExt())
                return false;

            // проверяем кол-во активных заявлений
            if (eventType == DSetEventType.afterInsert
                    || eventType == DSetEventType.afterUpdate)
            {
                // признак - забрал документы - ставить можно
                if (!request.isTakeAwayDocument()) {
                    // считаем активные заявления
                    List<Long> list = new DQLSelectBuilder().fromEntity(EntrantRequest.class, "e")
                            // только по избранному абитуриенту (а он всегда в рамках одной приемки)
                            .where(DQLExpressions.eq(DQLExpressions.property(EntrantRequest.entrant().id().fromAlias("e")), DQLExpressions.value(request.getEntrant().getId())))
                                    // документы не забирал
                            .where(DQLExpressions.eq(DQLExpressions.property(EntrantRequest.takeAwayDocument().fromAlias("e")), DQLExpressions.value(false)))
                                    // не текущее заявление
                            .where(DQLExpressions.ne(DQLExpressions.property(EntrantRequest.id().fromAlias("e")), DQLExpressions.value(request.getId())))
                            .column(EntrantRequest.id().fromAlias("e").s())
                            .createStatement(getSession())
                            .list();

                    // если есть хоть одна запись - алис капут
                    if (list != null && list.size() > 0)
                        throw new ApplicationException("Активное заявление может быть только одно");
                }
            }

            // после добавления
            if (eventType == DSetEventType.afterInsert) {
                EntrantRequestSecExt requestExt = getEntrantRequestSecExt(request);
                if (requestExt != null) {
                    // это не добавление, это сработал insert по другой сущности и по цепочке вытянуто requestExt
                    CheckSecurity check = checkSecurity(employeePost, DSetEventType.afterUpdate, new EntityInfo(id, EntrantRequest.ENTITY_NAME, DSetEventType.afterUpdate));
                    if (check != null)
                        throw new ApplicationException(check.getSecurityMessage());

                }
                else {
                    // и это может быть следствием связи объектов
                        /*
						if (comission==null && secEcExt.isDisallowCreateRequest())
							throw new ApplicationException("Создать заявление нельзя, Вы не являетесь сотрудником отборочной комиссии.");
						*/

                    if (comission != null && comission.getOrgUnit() != null && requestExt == null) {
                        createEntrantRequestSecExt(request, comission);
                        return true;
                    }
                }
            }

            // после обновления
            if (eventType == DSetEventType.afterUpdate) {
                EntrantRequestSecExt requestExt = getEntrantRequestSecExt(request);

                if (requestExt == null) {
                    // нужно задать? // и протестить
                    if (comission != null && comission.getOrgUnit() != null) {
                        createEntrantRequestSecExt(request, comission);
                        return true;
                    }
                }
            }
        }

        // действия при создании сущности, значения по умолчанию
        return false;
    }

    private void createEntrantRequestSecExt(EntrantRequest request, EnrollmentComissionToEmployeePost comission)
    {
        if (comission != null && comission.getOrgUnit() != null) {
            OrgUnit orgUnit = comission.getOrgUnit();
            EntrantRequestSecExt secExt = new EntrantRequestSecExt();
            secExt.setEntrantRequest(request);
            secExt.setOrgUnit(orgUnit);
            save(secExt);
        }
    }

    public EmployeePost getExecutorPost()
    {
        EmployeePost executorPost = null;
        if (UserContext.getInstance() != null) {
            IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();
            executorPost = DataAccessServices.dao().get(EmployeePost.class, EmployeePost.id(), principalContext.getId());
            if (executorPost != null)
                return executorPost;
            else
                return null;
        }
        else
            return null;

    }

    public List<EntrantRequestSecExt> getEntrantRequestSecExt(Entrant entrant)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EntrantRequestSecExt.class, "e");
        // расширения по указанному абитуриенту
        dql.where(DQLExpressions.eq(DQLExpressions.property(EntrantRequestSecExt.entrantRequest().entrant().fromAlias("e")), DQLExpressions.value(entrant)));
        // только активные (не забрал документы)
        dql.where(DQLExpressions.eq(DQLExpressions.property(EntrantRequestSecExt.entrantRequest().takeAwayDocument().fromAlias("e")), DQLExpressions.value(false)));

        List<EntrantRequestSecExt> lst = dql.createStatement(getSession()).list();

        return lst;

    }

    public String getEntrantComissionName(Entrant entrant)
    {


        String retVal = "";

        List<EntrantRequestSecExt> lst = DaoFacade.getDaoEvents().getEntrantRequestSecExt(entrant);
        if (!lst.isEmpty()) {
            for (EntrantRequestSecExt ext : lst) {
                if (ext.getOrgUnit() != null) {
                    if (ext.getOrgUnit().getShortTitle() != null && ext.getOrgUnit().getShortTitle() != "")
                        retVal += " " + ext.getOrgUnit().getShortTitle();
                    else
                        retVal += " " + ext.getOrgUnit().getTitle();
                }
            }
        }

        if (retVal != "")
            retVal = " ОК: " + retVal;

        return retVal;

    }

    List<String> _passEntrantState = null;

    private List<String> getEntrantStatePassCode()
    {
        if (_passEntrantState == null) {
            _passEntrantState = new ArrayList<String>();

            // зачислен в приказе предзачислен
            _passEntrantState.add(EntrantStateCodes.ENROLED);
            _passEntrantState.add(EntrantStateCodes.PRELIMENARY_ENROLLED);
            _passEntrantState.add(EntrantStateCodes.IN_ORDER);
        }

        return _passEntrantState;
    }

    List<String> _passStudentState = null;

    private List<String> getStudentStatePassCode()
    {
        if (_passStudentState == null) {
            _passStudentState = new ArrayList<String>();
            // зачислен в приказе предзачислен
            _passStudentState.add("3"); // закончил с дипломом
            _passStudentState.add("8"); // отчислен
        }

        return _passStudentState;
    }


    public List<EnrollmentComissionToEmployeePost> getEnrollmentComissionToEmployeePostList
            (
                    EmployeePost executorPost
            )
    {

        if (mapComission.containsKey(executorPost))
            return mapComission.get(executorPost);
        else {
            return null;
        }

    }

    public List<EnrollmentCampaignSecExt> getEnrollmentCampaignSecExtList
            (
                    List<EntityInfo> entityInfoList
            )
    {
        List<Long> ids = getIdList(EntrantRequest.ENTITY_NAME, entityInfoList);
        if (ids.isEmpty())
            return null;

        return getEnrollmentCampaignSecExtListById(ids);
    }

    public List<EnrollmentCampaignSecExt> getEnrollmentCampaignSecExtListById
            (
                    List<Long> entrantRequestIds
            )
    {

        if (entrantRequestIds == null || entrantRequestIds.isEmpty())
            return null;

        // id приемок
        DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(EntrantRequest.class, "ereq")
                .where(DQLExpressions.in(DQLExpressions.property(EntrantRequest.id().fromAlias("ereq")), entrantRequestIds))
                .column(EntrantRequest.entrant().enrollmentCampaign().id().fromAlias("ereq").s());


        List<EnrollmentCampaignSecExt> list = new DQLSelectBuilder().fromEntity(EnrollmentCampaignSecExt.class, "e")

                // только нужные нам приемки
                .where(DQLExpressions.in(DQLExpressions.property(EnrollmentCampaignSecExt.enrollmentCampaign().id().fromAlias("e")), dqlIn.getQuery()))
                        // только активные настройки
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentCampaignSecExt.useEntrantRequestSecExt().fromAlias("e")), DQLExpressions.value(true)))
                .column("e")
                .setPredicate(DQLPredicateType.distinct)
                .createStatement(getSession())
                .list();

        return list;

    }

    private List<Long> getIdList(String entityName,
                                 List<EntityInfo> entityInfoList)
    {
        List<Long> retVal = new ArrayList<Long>();
        for (EntityInfo entity : entityInfoList) {
            if (entity.getClassname().toLowerCase().equals(entityName.toLowerCase()))
                retVal.add(entity.getId());
        }
        return retVal;


    }

    public boolean isUseEntrantState
            (
                    List<EnrollmentCampaignSecExt> enrollmentCampaignSecExtList
            )
    {
        // по умолчанию - не использовать
        boolean retVal = false;
        for (EnrollmentCampaignSecExt ec : enrollmentCampaignSecExtList) {
            if (ec.isUseEntrantState())
                retVal = true;
        }
        return retVal;
    }


    public boolean isUseStudentState
            (
                    List<EnrollmentCampaignSecExt> enrollmentCampaignSecExtList
            )
    {
        // по умолчанию - не использовать
        boolean retVal = false;
        for (EnrollmentCampaignSecExt ec : enrollmentCampaignSecExtList) {
            if (ec.isUseStudentState())
                retVal = true;
        }
        return retVal;

    }
}
