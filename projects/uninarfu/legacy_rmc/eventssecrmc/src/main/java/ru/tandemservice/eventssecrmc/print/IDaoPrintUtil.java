package ru.tandemservice.eventssecrmc.print;

import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;

public interface IDaoPrintUtil {

    public static final String BEAN_NAME = "ru.tandemservice.eventssecrmc.print.IDaoPrintUtil";

    RtfInjectModifier injectMainSecretary(RtfInjectModifier injectModifier, OrgUnit orgUnit);

    RtfInjectModifier injectMainSecretary(RtfInjectModifier injectModifier, EntrantRequest entrantRequest);

    RtfInjectModifier injectEntererData(RtfInjectModifier injectModifier);

    RtfInjectModifier injectSelectionCommissionGenitive(RtfInjectModifier injectModifier, EntrantRequest entrantRequest);

    RtfInjectModifier injectSelectionCommission(RtfInjectModifier injectModifier, EntrantRequest entrantRequest);

    RtfInjectModifier injectSelectionCommissionPhones(RtfInjectModifier injectModifier, EntrantRequest entrantRequest);

    String getDelegateIof(EntrantRequest entrantRequest);

    String getDelegateIof(OrgUnit orgUnit);

    public String getDelegateIofFromContext(EnrollmentCampaign enrollmentCampaign, OrgUnit orgUnit);

    String getDelegatePost(EntrantRequest entrantRequest);

    String getDelegatePost(OrgUnit orgUnit);

    public String getDelegatePostFromContext(EnrollmentCampaign enrollmentCampaign, OrgUnit orgUnit);

    String getSelectionCommission(EntrantRequest entrantRequest);

    boolean isUseExt(EntrantRequest entrantRequest);
}
