package ru.tandemservice.eventssecrmc.component.entrant.EntrantRequestsChangeOwner;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.eventssecrmc.entity.EnrollmentComissionToEmployeePost;
import ru.tandemservice.eventssecrmc.entity.EntrantRequestSecExt;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(final Model model) {
        model.setEntrant(getNotNull(Entrant.class, model.getEntrant().getId()));

        model.setOrgUnitModel(new DQLFullCheckSelectModel() {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder subDql = new DQLSelectBuilder().fromEntity(EnrollmentComissionToEmployeePost.class, "eee")
                        .where(DQLExpressions.eqValue(DQLExpressions.property(EnrollmentComissionToEmployeePost.enrollmentCampaign().fromAlias("eee")), model.getEntrant().getEnrollmentCampaign()))
                        .column(EnrollmentComissionToEmployeePost.orgUnit().id().fromAlias("eee").s());

                DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(OrgUnit.class, alias)
                        .where(DQLExpressions.in(
                                DQLExpressions.property(OrgUnit.id().fromAlias(alias)),
                                subDql.buildQuery()
                        ))
                        .order(OrgUnit.title().fromAlias(alias).s());

                if (!StringUtils.isEmpty(filter))
                    dql.where(DQLExpressions.like(
                            DQLExpressions.property(EnrollmentComissionToEmployeePost.orgUnit().title().fromAlias(alias)),
                            DQLExpressions.value(filter)
                    ));

                return dql;
            }
        });

        if (model.getDataSource() != null)
            return;

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EntrantRequest.class, "er")
                .where(DQLExpressions.eqValue(DQLExpressions.property(EntrantRequest.entrant().fromAlias("er")), model.getEntrant()))
                .joinEntity(
                        "er",
                        DQLJoinType.left,
                        EntrantRequestSecExt.class,
                        "ext",
                        DQLExpressions.eq(
                                DQLExpressions.property(EntrantRequest.id().fromAlias("er")),
                                DQLExpressions.property(EntrantRequestSecExt.entrantRequest().id().fromAlias("ext")))
                )
                .order(EntrantRequest.regNumber().fromAlias("er").s());

        List<Object[]> list = dql.createStatement(getSession()).list();
        List<EntrantRequestSecExt> resultList = new ArrayList<>();
        long counter = -1;
        for (Object[] arr : list) {
            EntrantRequest er = (EntrantRequest) arr[0];
            EntrantRequestSecExt ext = (EntrantRequestSecExt) arr[1];
            if (ext == null) {
                ext = new EntrantRequestSecExt();
                ext.setId(--counter);
                ext.setEntrantRequest(er);
            }
            resultList.add(ext);
        }
        model.setList(resultList);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void prepareListDataSource(Model model) {
        model.getDataSource().setCountRow(model.getList().size());
        UniBaseUtils.createPage(model.getDataSource(), model.getList());

        Map<Long, OrgUnit> map = new HashMap<>();
        for (EntrantRequestSecExt ext : model.getList())
            map.put(ext.getId(), ext.getOrgUnit());
        ((BlockColumn<OrgUnit>) model.getDataSource().getColumn("editOrgUnit")).setValueMap(map);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void update(Model model) {
        Map<Long, OrgUnit> map = ((BlockColumn<OrgUnit>) model.getDataSource().getColumn("editOrgUnit")).getValueMap();

        for (EntrantRequestSecExt ext : model.getList()) {
            OrgUnit ou = map.get(ext.getId());

            if (ou != null) {
                if (ext.getId() < 0)
                    ext.setId(null);

                ext.setOrgUnit(ou);
                getSession().saveOrUpdate(ext);

            }
            else if (ext.getId() > 0)
                getSession().delete(ext);
        }
    }
}
