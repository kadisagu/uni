package ru.tandemservice.eventssecrmc.component.settings.EnrollmentCampaignExt;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.Map;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = getModel(component);
        model.setSettings(component.getSettings());
        getDao().prepare(model);
        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component) {
        Model model = getModel(component);
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<EnrollmentCampaignSecExt> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Приемная кампания", EnrollmentCampaignSecExt.enrollmentCampaign().title()));

        dataSource.addColumn(new ToggleColumn("Скрыть приемную кампанию", EnrollmentCampaignSecExt.hidden())
                                     .setListener("onClickHidden")
                                     .setPermissionKey("EnrollmentCampaignExt_hidden"));
        dataSource.addColumn(new ToggleColumn("Использовать расширенные настройки безопасности", EnrollmentCampaignSecExt.useEntrantRequestSecExt())
                                     .setListener("onClickUseEntrantRequestSecEx")
                                     .setPermissionKey("EnrollmentCampaignExt_use"));
        dataSource.addColumn(new ToggleColumn("Скрывать для пользователей - не сотрудников ОК", EnrollmentCampaignSecExt.checkAccess())
                                     .setListener("onClickCheckAccess")
                                     .setEnabledProperty(EnrollmentCampaignSecExt.useEntrantRequestSecExt())
                                     .setPermissionKey("EnrollmentCampaignExt_use"));


        dataSource.addColumn(new ToggleColumn("Запретить редактирование 'чужого' заявления", EnrollmentCampaignSecExt.disallowEditRequest())
                                     .setListener("onClickDisallowEditRequest")
                                     .setEnabledProperty(EnrollmentCampaignSecExt.useEntrantRequestSecExt())
                                     .setPermissionKey("EnrollmentCampaignExt_edit"));
        dataSource.addColumn(new ToggleColumn("Запретить удаление 'чужого' заявления", EnrollmentCampaignSecExt.disallowDeleteRequest())
                                     .setListener("onClickDisallowDeleteRequest")
                                     .setEnabledProperty(EnrollmentCampaignSecExt.useEntrantRequestSecExt())
                                     .setPermissionKey("EnrollmentCampaignExt_edit"));

		/* более не используется
		dataSource.addColumn( new ToggleColumn("Запретить смену владельца заявления (не используется)", EnrollmentCampaignSecExt.disallowChangeRequestOwner())
			.setListener("onClickDisallowChangeRequestOwner")
			.setPermissionKey("EnrollmentCampaignExt_edit") );
		 */

        dataSource.addColumn(new ToggleColumn("Учитывать присутствие студентов", EnrollmentCampaignSecExt.useStudentState())
                                     .setListener("onClickUseStudentState")
                                     .setEnabledProperty(EnrollmentCampaignSecExt.useEntrantRequestSecExt())
                                     .setPermissionKey("EnrollmentCampaignExt_edit"));

        dataSource.addColumn(new ToggleColumn("Только заявления в активном статусе", EnrollmentCampaignSecExt.useEntrantState())
                                     .setListener("onClickUseEntrantState")
                                     .setEnabledProperty(EnrollmentCampaignSecExt.useEntrantRequestSecExt())
                                     .setPermissionKey("EnrollmentCampaignExt_edit"));

        dataSource.addColumn(new BlockColumn<OrgUnitType>("editOrgUnitType", "Тип подразделения ОК"));
        dataSource.addColumn(new BlockColumn<OrgUnit>("editEnrollmentCommission", "Приемная комиссия"));
		
		/* колонки действуют вместе
		dataSource.addColumn(new BlockColumn<String>("editAllowActiveRequest", "Кол-во активных заявлений"));
		dataSource.addColumn(new ActionColumn("Сохранить", "save", "onClickSave")
			.setPermissionKey("EnrollmentCampaignExt_edit") );
		*/

        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete")
                                     .setPermissionKey("EnrollmentCampaignExt_delete"));

        model.setDataSource(dataSource);
    }

    public void onClickDelete(IBusinessComponent component) {
        Long id = component.getListenerParameter();
        getDao().delete(id);
    }

    @SuppressWarnings("unchecked")
    public void onClickChangeType(IBusinessComponent component) {
        Model model = getModel(component);
        Map<Long, OrgUnitType> map = ((BlockColumn<OrgUnitType>) model.getDataSource().getColumn("editOrgUnitType")).getValueMap();

        for (EnrollmentCampaignSecExt ext : model.getDataSource().getEntityList()) {
            OrgUnitType ouType = map.get(ext.getId());
            boolean changed = false;

            if (ext.getOrgUnitType() == null && ouType != null)
                changed = true;
            else if (ext.getOrgUnitType() != null && !ext.getOrgUnitType().equals(ouType))
                changed = true;

            if (changed) {
                ext.setOrgUnitType(ouType);
                getDao().saveOrUpdate(ext);
            }
        }

    }

    @SuppressWarnings("unchecked")
    public void onClickSaveOrgUnit(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        EnrollmentCampaignSecExt ext = component.getListenerParameter();

//		EnrollmentCampaignSecExt ext = getDao().get(extId);

        if (ext != null) {
            Map<Long, OrgUnit> map = ((BlockColumn<OrgUnit>) model.getDataSource().getColumn("editEnrollmentCommission")).getValueMap();
            ext.setEnrollmentCommission(map.get(ext.getId()));

            getDao().saveOrUpdate(ext);
        }


    }

    @SuppressWarnings("unchecked")
    public void onClickSave(IBusinessComponent component) {
        Model model = getModel(component);
        Long id = component.getListenerParameter();
        EnrollmentCampaignSecExt ext = find(model, id);

        String str = ((BlockColumn<String>) model.getDataSource().getColumn("editAllowActiveRequest")).getValueMap().get(id);
        ext.setAllowActiveRequest(Integer.parseInt(str));
        getDao().saveOrUpdate(ext);
    }

    public void onClickUseEntrantRequestSecEx(IBusinessComponent component) {
        Model model = getModel(component);
        Long id = component.getListenerParameter();
        EnrollmentCampaignSecExt ext = find(model, id);

        ext.setUseEntrantRequestSecExt(!ext.isUseEntrantRequestSecExt());
        getDao().saveOrUpdate(ext);
    }

    public void onClickCheckAccess(IBusinessComponent component) {
        Model model = getModel(component);
        Long id = component.getListenerParameter();
        EnrollmentCampaignSecExt ext = find(model, id);

        ext.setCheckAccess(!ext.isCheckAccess());
        getDao().saveOrUpdate(ext);
    }

    public void onClickHidden(IBusinessComponent component) {
        Model model = getModel(component);
        Long id = component.getListenerParameter();
        EnrollmentCampaignSecExt ext = find(model, id);

        ext.setHidden(!ext.isHidden());
        getDao().saveOrUpdate(ext);
    }

    public void onClickDisallowCreateRequest(IBusinessComponent component) {
        Model model = getModel(component);
        Long id = component.getListenerParameter();
        EnrollmentCampaignSecExt ext = find(model, id);

        ext.setDisallowCreateRequest(!ext.isDisallowCreateRequest());
        getDao().saveOrUpdate(ext);
    }


    public void onClickUseStudentState(IBusinessComponent component) {
        Model model = getModel(component);
        Long id = component.getListenerParameter();
        EnrollmentCampaignSecExt ext = find(model, id);

        ext.setUseStudentState(!ext.isUseStudentState());
        getDao().saveOrUpdate(ext);
    }


    public void onClickUseEntrantState(IBusinessComponent component) {
        Model model = getModel(component);
        Long id = component.getListenerParameter();
        EnrollmentCampaignSecExt ext = find(model, id);

        ext.setUseEntrantState(!ext.isUseEntrantState());
        getDao().saveOrUpdate(ext);
    }


    public void onClickDisallowEditRequest(IBusinessComponent component) {
        Model model = getModel(component);
        Long id = component.getListenerParameter();
        EnrollmentCampaignSecExt ext = find(model, id);

        ext.setDisallowEditRequest(!ext.isDisallowEditRequest());
        getDao().saveOrUpdate(ext);
    }

    public void onClickDisallowDeleteRequest(IBusinessComponent component) {
        Model model = getModel(component);
        Long id = component.getListenerParameter();
        EnrollmentCampaignSecExt ext = find(model, id);

        ext.setDisallowDeleteRequest(!ext.isDisallowDeleteRequest());
        getDao().saveOrUpdate(ext);
    }

    public void onClickDisallowChangeRequestOwner(IBusinessComponent component) {
        Model model = getModel(component);
        Long id = component.getListenerParameter();
        EnrollmentCampaignSecExt ext = find(model, id);

        ext.setDisallowChangeRequestOwner(!ext.isDisallowChangeRequestOwner());
        getDao().saveOrUpdate(ext);
    }

    public void onClickAdd(IBusinessComponent component) {
        Model model = getModel(component);

        EnrollmentCampaignSecExt ext = new EnrollmentCampaignSecExt();
        ext.setEnrollmentCampaign(model.getEnrollmentCampaign());
        getDao().saveOrUpdate(ext);
    }

    public void onSaveSettings(IBusinessComponent component) {
        component.saveSettings();
    }

    private EnrollmentCampaignSecExt find(Model model, Long id) {
        for (EnrollmentCampaignSecExt ext : model.getDataSource().getEntityList())
            if (ext.getId().equals(id)) return ext;

        return null;
    }
}
