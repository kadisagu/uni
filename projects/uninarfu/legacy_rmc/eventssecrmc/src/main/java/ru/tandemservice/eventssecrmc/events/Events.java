package ru.tandemservice.eventssecrmc.events;

import org.hibernate.engine.SessionImplementor;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.eventssecrmc.dao.DaoFacade;
import ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt;
import ru.tandemservice.eventssecrmc.entity.EnrollmentComissionToEmployeePost;
import ru.tandemservice.uni.entity.employee.Student;

import javax.transaction.Synchronization;
import java.util.*;

/**
 * Изменения объектов внешней системе
 *
 * @author vch
 */
public class Events implements IDSetEventListener {

    private static final ThreadLocal<Sync> syncs = new ThreadLocal<Sync>();

    /**
     * в спринге прописан как стартовый метод
     */
    public void init() {
        DSetEventManager.getInstance().registerListener(
                DSetEventType.afterInsert, EntityBase.class, this);
        DSetEventManager.getInstance().registerListener(
                DSetEventType.afterUpdate, EntityBase.class, this);
        DSetEventManager.getInstance().registerListener(
                DSetEventType.beforeDelete, EntityBase.class, this);
    }


    @Override
    public void onEvent(DSetEvent event)
    {
        IEntityMeta entityMeta = event.getMultitude().getEntityMeta();
        String className = entityMeta.getName();

        IDaoEvents dao = DaoFacade.getDaoEvents();
        if (
                className.toLowerCase().equals(EnrollmentComissionToEmployeePost.ENTITY_NAME.toLowerCase())
                        || className.toLowerCase().equals(EnrollmentCampaignSecExt.ENTITY_NAME.toLowerCase())
                )
        {
            dao.clearCache();
            dao.createCache();
        }
        /*
    	OrgUnit ou;
    	TopOrgUnit tou;
    	*/

        // <TODO>
        EmployeePost executorPost = null;
        IPrincipalContext principalContext = UserContext.getInstance() != null ? UserContext.getInstance().getPrincipalContext() : null;
        if (principalContext != null) {
            executorPost = DataAccessServices.dao().get(EmployeePost.class, EmployeePost.id(), principalContext.getId());
            if (executorPost == null)
                // работаем из под админа
                // можно ничего не проверять
                return;
        }
        else {
            // системные действия, тут делать нечего или юзер не залогинен
            return;
        }
    	
        /*
    	if (
    			  className.toLowerCase().equals("EntityForDaemonWS".toLowerCase())
    			&&  (event.getEventType()==DSetEventType.afterUpdate || event.getEventType()==DSetEventType.afterDelete) 
    		)
    				// забыть последнее
    				DaoFacade.getDaoEvents().clearLastEvents();
    	*/

        if (event.getEventType() == DSetEventType.afterUpdate
                && dao.passChangedProperty(event))
            return;

        dao.createCache();

        // если настроек нет - выходим
        if (!dao.hasEmployeePostSettings())
            return;

        boolean isNeed = dao.needEvent(event);
        if (!isNeed)
            return;

        // проверим, а пользователь вообще попал в расширенные настройки ПК
        List<EnrollmentComissionToEmployeePost> secExtList = dao.getEnrollmentComissionToEmployeePostList(executorPost);
        if (secExtList == null || secExtList.isEmpty())
            return;

        // получаем список объектов, с их id
        List<EntityInfo> entityInfoList = getInitialList(event);

        // получаем список взаимосвязанных объектов
        entityInfoList = dao.getEntityList(entityInfoList);

        // настройки приемной кампании (на основе оставшегося списка заявлений)
        List<EnrollmentCampaignSecExt> enrollmentCampaignSecExtList = dao.getEnrollmentCampaignSecExtList(entityInfoList);
        if (enrollmentCampaignSecExtList == null || enrollmentCampaignSecExtList.isEmpty())
            return;

        if (dao.isUseStudentState(enrollmentCampaignSecExtList)) {
            // если среди взаимосвязанных объектов есть студент
            // все - безопасность отключаем
            boolean hasStudent = _findStudent(entityInfoList);
            if (hasStudent)
                return;
        }

        // проверяем права доступа, если для одного из объектов прав нет, вываливаем исключение
        List<IDaoEvents.CheckSecurity> lstSec = dao.checkSecurity(executorPost, event, entityInfoList);

        if (lstSec != null && !lstSec.isEmpty()) {
            // есть объекты с запрещенным доступом
            _throwException(lstSec);
        }

        List<Object> prevSource = null;//new DQLSelectBuilder().fromDataSource(event.getMultitude().getSource(), "s").createStatement(event.getContext()).list();
        Sync sync = getSyncSafe(event.getContext());
        sync.employeePost = executorPost;

        for (EntityInfo info : entityInfoList) {
            if (!sync.entitySet.contains(info)) {
                sync.entitySet.add(info);
                sync.prevSource.put(info.getId(), prevSource);
            }
        }

    }

    private boolean _findStudent(List<EntityInfo> entityInfoList)
    {

        for (EntityInfo entity : entityInfoList) {
            if (entity.getClassname().toLowerCase().equals(Student.ENTITY_NAME.toLowerCase()))
                return true;
        }
        return false;
    }


    private void _throwException(List<IDaoEvents.CheckSecurity> lstSec)
    {
        String msg = "Нет прав на редактирование объектов: ";
        for (IDaoEvents.CheckSecurity sec : lstSec) {
            msg += sec.getSecurityMessage() + "; ";
        }
        throw new ApplicationException(msg);
    }


    /**
     * Начальный список сущностей
     *
     * @param event
     *
     * @return
     */
    private List<EntityInfo> getInitialList(DSetEvent event)
    {
        IEntityMeta entityMeta = event.getMultitude().getEntityMeta();

        List<Long> idList = new ArrayList<Long>();
        if (event.getMultitude().isSingular())
            idList.add(event.getMultitude().getSingularEntity().getId());
        else {
            idList = new DQLSelectBuilder().fromDataSource(event.getMultitude().getSource(), "s")
                    .column("s.id")
                    .createStatement(event.getContext())
                    .<Long>list();
        }
        List<EntityInfo> resultList = new ArrayList<EntityInfo>();
        for (Long id : idList)
            resultList.add(new EntityInfo(id, entityMeta.getName(), event.getEventType()));
        return resultList;
    }


    private Sync getSyncSafe(DQLExecutionContext context)
    {
        Sync sync = syncs.get();
        if (sync == null) {
            syncs.set(sync = new Sync(context.getSessionImplementor()));
            context.getTransaction().registerSynchronization(sync);
        }
        return sync;
    }


    private static class Sync implements Synchronization {
        private SessionImplementor sessionImplementor;

        /**
         * список id сущностей, которые изменились
         */
        private Set<EntityInfo> entitySet = new LinkedHashSet<EntityInfo>();

        //мап значений , которые хранились в бд до совершения операции:
        private Map<Long, List<Object>> prevSource = new HashMap<Long, List<Object>>();

        private EmployeePost employeePost;

        private Sync(SessionImplementor sessionImplementor) {
            this.sessionImplementor = sessionImplementor;
        }

        @Override
        public void beforeCompletion() {
            boolean hasUpdate = false;

            for (EntityInfo info : entitySet) {
                IDaoEvents dao = DaoFacade.getDaoEvents();
                boolean _hasUpdate = dao.actionFromTrigger(employeePost, info.getEventType(), info.getClassname(), info.getId(), prevSource.get(info.getId()));
                if (_hasUpdate)
                    hasUpdate = true;
            }
            if (hasUpdate)
                sessionImplementor.flush();
        }

        @Override
        public void afterCompletion(int status) {
            syncs.remove();
        }
    }

    public static class EntityInfo {
        private Long id;
        private String classname;

        /**
         * тип события
         */
        private DSetEventType eventType;


        public EntityInfo(Long id, String classname, DSetEventType eventType)
        {
            this.id = id;
            this.classname = classname;
            this.eventType = eventType;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getClassname() {
            return classname;
        }

        public void setClassname(String classname)
        {
            this.classname = classname;
        }

        @Override
        public boolean equals(Object obj)
        {
            if (obj != null && obj instanceof EntityInfo) {
                EntityInfo compare = (EntityInfo) obj;
                if (compare.hashCode() == this.hashCode())
                    return true;
                else
                    return false;
            }
            else
                return super.equals(obj);
        }

        @Override
        public int hashCode()
        {
            return this.getId().hashCode() + this.getEventType().hashCode();
        }

        public void setEventType(DSetEventType eventType) {
            this.eventType = eventType;
        }

        public DSetEventType getEventType() {
            return eventType;
        }
    }
}
