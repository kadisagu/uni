package ru.tandemservice.eventssecrmc.dao;

import org.tandemframework.core.runtime.ApplicationRuntime;
import ru.tandemservice.eventssecrmc.events.IDaoEvents;
import ru.tandemservice.eventssecrmc.print.IDaoPrintUtil;


public class DaoFacade {
    public static IDaoEvents _daoEvents;
    public static IDaoPrintUtil _daoPrintUtil;

    public static IDaoEvents getDaoEvents()
    {

        if (_daoEvents == null)
            _daoEvents = (IDaoEvents) ApplicationRuntime.getBean(IDaoEvents.BEAN_NAME);
        return _daoEvents;
    }

    public static IDaoPrintUtil getDaoPrintUtil() {
        if (_daoPrintUtil == null)
            _daoPrintUtil = (IDaoPrintUtil) ApplicationRuntime.getBean(IDaoPrintUtil.BEAN_NAME);
        return _daoPrintUtil;
    }
}
