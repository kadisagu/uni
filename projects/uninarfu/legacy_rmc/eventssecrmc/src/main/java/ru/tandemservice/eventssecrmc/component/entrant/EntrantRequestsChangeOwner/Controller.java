package ru.tandemservice.eventssecrmc.component.entrant.EntrantRequestsChangeOwner;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.eventssecrmc.entity.EntrantRequestSecExt;
import ru.tandemservice.unibase.UniBaseUtils;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = getModel(component);
        getDao().prepare(model);

        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component) {
        Model model = getModel(component);
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<EntrantRequestSecExt> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Заявление", EntrantRequestSecExt.entrantRequest().regNumber()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new BlockColumn<OrgUnit>("editOrgUnit", "Подразделение"));

        model.setDataSource(dataSource);
    }

    public void onClickApply(IBusinessComponent component) {
        Model model = getModel(component);
        getDao().update(model);
        deactivate(component);
    }
}
