package ru.tandemservice.eventssecrmc.component.settings.EnrollmentComissionToEmployeePost.List;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.eventssecrmc.entity.EnrollmentComissionToEmployeePost;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
    }

    @Override
    public void prepareListDataSource(Model model) {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrollmentComissionToEmployeePost.class, "e")
                .where(DQLExpressions.eqValue(DQLExpressions.property(EnrollmentComissionToEmployeePost.enrollmentCampaign().fromAlias("e")), model.getEnrollmentCampaign()));

        if (model.getDataSource().getEntityOrder() != null)
            dql.order("e." + model.getDataSource().getEntityOrder().getKeyString(), model.getDataSource().getEntityOrder().getDirection());

        List<EnrollmentComissionToEmployeePost> list = dql.createStatement(getSession()).list();
        UniBaseUtils.createPage(model.getDataSource(), list);
    }
}
