package ru.tandemservice.eventssecrmc.print;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt;
import ru.tandemservice.eventssecrmc.entity.EnrollmentComissionToEmployeePost;
import ru.tandemservice.eventssecrmc.entity.EntrantRequestSecExt;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;

import java.util.List;

public class DaoPrintUtil extends UniBaseDao implements IDaoPrintUtil {

    public RtfInjectModifier injectMainSecretary(RtfInjectModifier injectModifier, OrgUnit orgUnit) {
        String secretaryIof = "";
        String secretaryPost = "";
        String secretaryOrgUnit = orgUnit.getGenitiveCaseTitle() != null ? orgUnit.getGenitiveCaseTitle() : orgUnit.getShortTitle();

        EmployeePost mainSecretary = (EmployeePost) orgUnit.getHead();
        if (mainSecretary != null) {
            secretaryIof = StringUtils.trimToEmpty(mainSecretary.getEmployee().getPerson().getIdentityCard().getIof());

            String postTitle = mainSecretary.getPostRelation().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle();
            if (StringUtils.isEmpty(postTitle))
                secretaryPost = StringUtils.capitalize(StringUtils.trimToEmpty(mainSecretary.getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle()));
            else secretaryPost = StringUtils.capitalize(postTitle);
        }

        injectModifier.put("delegatePost", secretaryPost + " " + secretaryOrgUnit);
        injectModifier.put("delegateFio", secretaryIof);

        return injectModifier;
    }

    public RtfInjectModifier injectMainSecretary(RtfInjectModifier injectModifier, EntrantRequest entrantRequest) {

        if (injectModifier == null)
            injectModifier = new RtfInjectModifier();

        EntrantRequestSecExt entrantRequestSecExt = get(EntrantRequestSecExt.class, EntrantRequestSecExt.entrantRequest(), entrantRequest);

        if (isUseExt(entrantRequest)) {
            if (entrantRequestSecExt.getOrgUnit() != null) {

                String secretaryIof = "";
                String secretaryPost = "";
                String secretaryOrgUnit = StringUtils.trimToEmpty(entrantRequestSecExt.getOrgUnit().getGenitiveCaseTitle());
                EmployeePost mainSecretary = (EmployeePost) entrantRequestSecExt.getOrgUnit().getHead();

                if (mainSecretary != null) {
                    secretaryIof = StringUtils.trimToEmpty(mainSecretary.getEmployee().getPerson().getIdentityCard().getIof());

                    String postTitle = mainSecretary.getPostRelation().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle();
                    if (StringUtils.isEmpty(postTitle))
                        secretaryPost = StringUtils.capitalize(StringUtils.trimToEmpty(mainSecretary.getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle()));
                    else secretaryPost = StringUtils.capitalize(postTitle);
                }

                injectModifier.put("delegatePost", secretaryPost + " " + secretaryOrgUnit);
                injectModifier.put("delegateFio", secretaryIof);

            }
            else
                throw new ApplicationException("Для абитуриента не указана отборочная комиссия");
        }
        else {
            injectModifier.put("delegatePost", "Ответственный секретарь отборочной комиссии");
            injectModifier.put("delegateFio", "");
        }

        return injectModifier;
    }

    /**
     * Метка "Ввел данные"
     *
     * @param injectModifier
     *
     * @return
     */
    public RtfInjectModifier injectEntererData(RtfInjectModifier injectModifier) {

        Person operator = PersonManager.instance().dao().getPerson(ContextLocal.getUserContext().getPrincipalContext());
        injectModifier.put("enterData", operator != null ? StringUtils.trimToEmpty(operator.getIdentityCard().getIof()) : "");
        return injectModifier;
    }


    /**
     * Метка "Отборочная комиссия" (родительный падеж)
     *
     * @param injectModifier
     * @param entrantRequest
     *
     * @return
     */
    public RtfInjectModifier injectSelectionCommissionGenitive(RtfInjectModifier injectModifier, EntrantRequest entrantRequest) {
        EntrantRequestSecExt entrantRequestSecExt = get(EntrantRequestSecExt.class, EntrantRequestSecExt.entrantRequest(), entrantRequest);

        if (isUseExt(entrantRequest))
            if (entrantRequestSecExt.getOrgUnit() != null)
                injectModifier.put("SelectionCommission_G", StringUtils.trimToEmpty(entrantRequestSecExt.getOrgUnit().getGenitiveCaseTitle()));
            else
                throw new ApplicationException("Для абитуриента не указана отборочная комиссия");
        else injectModifier.put("SelectionCommission_G", "");

        return injectModifier;
    }

    /**
     * Метка "Отборочная комиссия" (именительный падеж)
     *
     * @param injectModifier
     * @param entrantRequest
     *
     * @return
     */
    public RtfInjectModifier injectSelectionCommission(RtfInjectModifier injectModifier, EntrantRequest entrantRequest) {
        EntrantRequestSecExt entrantRequestSecExt = get(EntrantRequestSecExt.class, EntrantRequestSecExt.entrantRequest(), entrantRequest);

        if (isUseExt(entrantRequest))
            if (entrantRequestSecExt.getOrgUnit() != null)
                if (!StringUtils.isEmpty(entrantRequestSecExt.getOrgUnit().getNominativeCaseTitle()))
                    injectModifier.put("SelectionCommission", StringUtils.trimToEmpty(entrantRequestSecExt.getOrgUnit().getNominativeCaseTitle()));
                else injectModifier.put("SelectionCommission", StringUtils.trimToEmpty(entrantRequestSecExt.getOrgUnit().getTitle()));
            else
                throw new ApplicationException("Для абитуриента не указана отборочная комиссия");
        else injectModifier.put("SelectionCommission", "");

        return injectModifier;
    }

    /**
     * Метка "Телефоны отборочной комиссии"
     *
     * @param injectModifier
     * @param entrantRequest
     *
     * @return
     */
    public RtfInjectModifier injectSelectionCommissionPhones(RtfInjectModifier injectModifier, EntrantRequest entrantRequest) {
        EntrantRequestSecExt entrantRequestSecExt = get(EntrantRequestSecExt.class, EntrantRequestSecExt.entrantRequest(), entrantRequest);
        StringBuilder builder = new StringBuilder("");

        if (isUseExt(entrantRequest))
            if (entrantRequestSecExt.getOrgUnit() != null) {
                builder.append(StringUtils.trimToEmpty(entrantRequestSecExt.getOrgUnit().getPhone()));
                if (!StringUtils.isEmpty(entrantRequestSecExt.getOrgUnit().getInternalPhone()))
                    builder.append(" добавочный: " + entrantRequestSecExt.getOrgUnit().getInternalPhone());
            }
            else
                throw new ApplicationException("Для абитуриента не указана отборочная комиссия");

        injectModifier.put("SelectionCommissionPhones", builder.toString());

        return injectModifier;
    }

    public String getDelegateIof(EntrantRequest entrantRequest) {
        EntrantRequestSecExt entrantRequestSecExt = get(EntrantRequestSecExt.class, EntrantRequestSecExt.entrantRequest(), entrantRequest);
        String delegateIof = "";
        if (isUseExt(entrantRequest)) {
            if (entrantRequestSecExt.getOrgUnit() != null) {
                EmployeePost mainSecretary = (EmployeePost) entrantRequestSecExt.getOrgUnit().getHead();
                if (mainSecretary != null)
                    delegateIof = StringUtils.trimToEmpty(mainSecretary.getEmployee().getPerson().getIdentityCard().getIof());
            }
            else
                throw new ApplicationException("Для абитуриента не указана отборочная комиссия");
        }
        return delegateIof;
    }

    public String getDelegatePost(EntrantRequest entrantRequest) {

        EntrantRequestSecExt entrantRequestSecExt = get(EntrantRequestSecExt.class, EntrantRequestSecExt.entrantRequest(), entrantRequest);
        String delegatePost = "";

        if (isUseExt(entrantRequest)) {
            if (entrantRequestSecExt.getOrgUnit() != null) {
                EmployeePost mainSecretary = (EmployeePost) entrantRequestSecExt.getOrgUnit().getHead();
                if (mainSecretary != null)
                    delegatePost = StringUtils.capitalize(StringUtils.trimToEmpty(mainSecretary.getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle()));
            }
            else
                throw new ApplicationException("Для абитуриента не указана отборочная комиссия");
        }

        return delegatePost;
    }

    public String getDelegateIof(OrgUnit orgUnit) {
        String secretaryIof = "";

        if (orgUnit != null) {
            EmployeePost mainSecretary = (EmployeePost) orgUnit.getHead();
            if (mainSecretary != null)
                secretaryIof = StringUtils.trimToEmpty(mainSecretary.getEmployee().getPerson().getIdentityCard().getIof());
        }

        return secretaryIof;
    }

    public String getDelegatePost(OrgUnit orgUnit) {

        String secretaryPost = "";

        if (orgUnit == null)
            return "Ответственный секретарь отборочной комиссии";

        String secretaryOrgUnit = orgUnit.getGenitiveCaseTitle() != null ? orgUnit.getGenitiveCaseTitle() : orgUnit.getShortTitle();

        EmployeePost mainSecretary = (EmployeePost) orgUnit.getHead();
        if (mainSecretary != null) {

            String postTitle = mainSecretary.getPostRelation().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle();
            if (StringUtils.isEmpty(postTitle))
                secretaryPost = StringUtils.capitalize(StringUtils.trimToEmpty(mainSecretary.getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle()));
            else secretaryPost = StringUtils.capitalize(postTitle);
        }
        String delegatePost = secretaryPost + " " + secretaryOrgUnit;

        return delegatePost;
    }

    public String getSelectionCommission(EntrantRequest entrantRequest) {
        EntrantRequestSecExt entrantRequestSecExt = get(EntrantRequestSecExt.class, EntrantRequestSecExt.entrantRequest(), entrantRequest);
        String selectionCommission = "";
        if (isUseExt(entrantRequest))
            if (entrantRequestSecExt.getOrgUnit() != null)
                selectionCommission = StringUtils.trimToEmpty(entrantRequestSecExt.getOrgUnit().getTitle());
            else
                throw new ApplicationException("Для абитуриента не указана отборочная комиссия");
        return selectionCommission;
    }

    public boolean isUseExt(EntrantRequest entrantRequest) {
        EntrantRequestSecExt entrantRequestSecExt = get(EntrantRequestSecExt.class, EntrantRequestSecExt.entrantRequest(), entrantRequest);
        EnrollmentCampaignSecExt enrollmentCampaignSecExt = get(EnrollmentCampaignSecExt.class, EnrollmentCampaignSecExt.enrollmentCampaign(), entrantRequest.getEntrant().getEnrollmentCampaign());
        if (entrantRequestSecExt != null && enrollmentCampaignSecExt != null && enrollmentCampaignSecExt.isUseEntrantRequestSecExt())
            return true;
        return false;
    }

    public String getDelegatePostFromContext(EnrollmentCampaign enrollmentCampaign, OrgUnit orgUnit) {
        Person operator = PersonManager.instance().dao().getPerson(ContextLocal.getUserContext().getPrincipalContext());
        String delegatePost = "";

        if (operator != null) {
            String secretaryPost = "";
            String secretaryOrgUnit = "";
            List<EnrollmentComissionToEmployeePost> enrollmentComissionToEmployeePostList = getEnrollmentComissionToEmployeePost(enrollmentCampaign, operator);
            if (enrollmentComissionToEmployeePostList != null && !enrollmentComissionToEmployeePostList.isEmpty() && enrollmentComissionToEmployeePostList.size() == 1) {
                OrgUnit ou = enrollmentComissionToEmployeePostList.get(0).getOrgUnit();
                if (ou != null) {
                    secretaryOrgUnit = ou.getGenitiveCaseTitle() != null ? ou.getGenitiveCaseTitle() : ou.getShortTitle();
                    EmployeePost mainSecretary = (EmployeePost) ou.getHead();
                    if (mainSecretary != null) {
                        String postTitle = mainSecretary.getPostRelation().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle();
                        if (StringUtils.isEmpty(postTitle))
                            secretaryPost = StringUtils.capitalize(StringUtils.trimToEmpty(mainSecretary.getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle()));
                        else secretaryPost = StringUtils.capitalize(postTitle);
                    }
                    delegatePost = secretaryPost + " " + secretaryOrgUnit;
                }
            }
        }

        if (StringUtils.isEmpty(delegatePost))
            delegatePost = getDelegatePost(orgUnit);

        return delegatePost;
    }


    public String getDelegateIofFromContext(EnrollmentCampaign enrollmentCampaign, OrgUnit orgUnit) {
        Person operator = PersonManager.instance().dao().getPerson(ContextLocal.getUserContext().getPrincipalContext());
        String secretaryIof = "";

        if (operator != null) {
            List<EnrollmentComissionToEmployeePost> enrollmentComissionToEmployeePostList = getEnrollmentComissionToEmployeePost(enrollmentCampaign, operator);
            if (enrollmentComissionToEmployeePostList != null && !enrollmentComissionToEmployeePostList.isEmpty() && enrollmentComissionToEmployeePostList.size() == 1) {
                OrgUnit ou = enrollmentComissionToEmployeePostList.get(0).getOrgUnit();
                if (ou != null) {
                    EmployeePost mainSecretary = (EmployeePost) ou.getHead();
                    if (mainSecretary != null)
                        secretaryIof = StringUtils.trimToEmpty(mainSecretary.getEmployee().getPerson().getIdentityCard().getIof());
                }
            }
        }

        if (StringUtils.isEmpty(secretaryIof))
            secretaryIof = getDelegateIof(orgUnit);

        return secretaryIof;

    }

    private List<EnrollmentComissionToEmployeePost> getEnrollmentComissionToEmployeePost(EnrollmentCampaign enrollmentCampaign, Person person) {

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EnrollmentComissionToEmployeePost.class, "ec2ep")
                .where(DQLExpressions.eq(
                        DQLExpressions.property(EnrollmentComissionToEmployeePost.enrollmentCampaign().id().fromAlias("ec2ep")),
                        DQLExpressions.value(enrollmentCampaign.getId())))
                .where(DQLExpressions.eq(
                        DQLExpressions.property(EnrollmentComissionToEmployeePost.employeePost().person().id().fromAlias("ec2ep")),
                        DQLExpressions.value(person.getId())));
        List<EnrollmentComissionToEmployeePost> enrollmentComissionToEmployeePostList = builder.createStatement(getSession()).list();

        return enrollmentComissionToEmployeePostList;
    }
}
