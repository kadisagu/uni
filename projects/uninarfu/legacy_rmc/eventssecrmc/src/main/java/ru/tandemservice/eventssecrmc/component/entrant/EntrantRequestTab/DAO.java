package ru.tandemservice.eventssecrmc.component.entrant.EntrantRequestTab;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.eventssecrmc.dao.DaoFacade;
import ru.tandemservice.eventssecrmc.entity.EnrollmentComissionToEmployeePost;
import ru.tandemservice.eventssecrmc.entity.EntrantRequestSecExt;
import ru.tandemservice.eventssecrmc.events.IDaoEvents;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.List;


public class DAO
        extends ru.tandemservice.uniecrmc.component.entrant.EntrantRequestTab.DAO
{

    @Override
    public void prepare(ru.tandemservice.uniec.component.entrant.EntrantRequestTab.Model model)
    {
        super.prepare(model);
        Model myModel = (Model) model;

        // разрешить смену ОК
        myModel.setDisableEditEntrantRequestOrgUnit(true);
        EmployeePost executorPost = DaoFacade.getDaoEvents().getExecutorPost();
        if (executorPost == null) {
            // работа под админом
            myModel.setCreateInOtherComission(false);
            myModel.setEnrolmentComissionTitle("");

            myModel.setHasAccessToRequest(false);
            myModel.setAccessToRequestTitle("Вы работаете под учетной записью администратора, отборочные комиссии не назначаются");

            myModel.setDisableEditEntrantRequestOrgUnit(false);
        }
        else {
            List<EntrantRequestSecExt> lst = DaoFacade.getDaoEvents().getEntrantRequestSecExt(myModel.getEntrant());
            OrgUnit ouComission = getOtherComission(executorPost, myModel.getEntrant().getEnrollmentCampaign(), lst);
            if (ouComission != null) {
                myModel.setCreateInOtherComission(true);
                myModel.setEnrolmentComissionTitle("Заявление создано в рамках отборочной комиссии '" + ouComission.getTitle() + "', редактирование запрещено");

                myModel.setHasAccessToRequest(false);
                myModel.setAccessToRequestTitle("Нельзя редактировать заявления, созданные операторами других ОК");

            }
            else {
                EnrollmentComissionToEmployeePost comission = getEnrollmentComissionToEmployeePost(executorPost, myModel.getEntrant().getEnrollmentCampaign());
                if (comission == null) {
                    myModel.setHasAccessToRequest(false);
                    myModel.setAccessToRequestTitle("Вы не являетесь сотрудником ОК");
                }
                else {
                    if (comission.getOrgUnit() == null) {
                        myModel.setHasAccessToRequest(false);
                        myModel.setAccessToRequestTitle("Вы являетесь супер пользователем. Под вашими правами для заявлений не создается признак ОК");
                    }
                    else {
                        myModel.setHasAccessToRequest(true);
                        myModel.setAccessToRequestTitle("");
                    }
                }

                myModel.setCreateInOtherComission(false);
                myModel.setEnrolmentComissionTitle("");

                myModel.setDisableEditEntrantRequestOrgUnit(false);
            }
        }

    }

    private EnrollmentComissionToEmployeePost getEnrollmentComissionToEmployeePost
            (
                    EmployeePost executorPost
                    , EnrollmentCampaign enrollmentCampaign
            )
    {
        IDaoEvents daoEvents = DaoFacade.getDaoEvents();
        EnrollmentComissionToEmployeePost comission = daoEvents.getComission(executorPost, enrollmentCampaign);

        return comission;
    }

    private OrgUnit getOtherComission(
            EmployeePost executorPost
            , EnrollmentCampaign enrollmentCampaign
            , List<EntrantRequestSecExt> lst
    )
    {

        IDaoEvents daoEvents = DaoFacade.getDaoEvents();
        EnrollmentComissionToEmployeePost comission = daoEvents.getComission(executorPost, enrollmentCampaign);

        if (comission == null) {
            // не в комиссии (нет прав)
            return null;
        }
        if (comission.getOrgUnit() == null)
            // можно все
            return null;
        else {
            // в комиссии
            for (EntrantRequestSecExt sec : lst) {
                if (!sec.getOrgUnit().getId().equals(comission.getOrgUnit().getId()))
                    return sec.getOrgUnit();
            }
        }
        return null;

    }


}
