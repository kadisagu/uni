package ru.tandemservice.eventssecrmc.component.settings.EnrollmentCampaignExt;

import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        EnrollmentCampaign c = model.getEnrollmentCampaign();
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession(), false);
        model.setEnrollmentCampaign(c);

        List<OrgUnitType> list = getCatalogItemList(OrgUnitType.class);
        model.setOrgUnitTypeList(list);

        model.setOrgUnitList(new LazySimpleSelectModel<>(OrgUnit.class));
    }

    @SuppressWarnings("unchecked")
    @Override
    public void prepareListDataSource(Model model) {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrollmentCampaignSecExt.class, "e");

        if (model.getEnrollmentCampaign() != null)
            dql.where(DQLExpressions.eqValue(DQLExpressions.property(EnrollmentCampaignSecExt.enrollmentCampaign().fromAlias("e")), model.getEnrollmentCampaign()));

        if (model.getDataSource().getEntityOrder() != null)
            dql.order("e." + model.getDataSource().getEntityOrder().getKeyString(), model.getDataSource().getEntityOrder().getDirection());

        List<EnrollmentCampaignSecExt> list = getList(dql);
        UniBaseUtils.createPage(model.getDataSource(), list);

        Map<Long, OrgUnitType> map = new HashMap<>();
        for (EnrollmentCampaignSecExt ext : list)
            map.put(ext.getId(), ext.getOrgUnitType());
        ((BlockColumn<OrgUnitType>) model.getDataSource().getColumn("editOrgUnitType")).setValueMap(map);

        Map<Long, OrgUnit> ouMap = new HashMap<>();
        for (EnrollmentCampaignSecExt ext : list)
            ouMap.put(ext.getId(), ext.getEnrollmentCommission());
        ((BlockColumn<OrgUnit>) model.getDataSource().getColumn("editEnrollmentCommission")).setValueMap(ouMap);

		/*
		Map<Long, String> map = new HashMap<Long, String>();
		for (EnrollmentCampaignSecExt ext : list)
			map.put(ext.getId(), ""+ext.getAllowActiveRequest());
		((BlockColumn<String>)model.getDataSource().getColumn("editAllowActiveRequest")).setValueMap(map);
		*/

    }
}
