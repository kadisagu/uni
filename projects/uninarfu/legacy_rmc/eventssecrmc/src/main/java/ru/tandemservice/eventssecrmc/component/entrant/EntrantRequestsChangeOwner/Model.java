package ru.tandemservice.eventssecrmc.component.entrant.EntrantRequestsChangeOwner;

import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.eventssecrmc.entity.EntrantRequestSecExt;
import ru.tandemservice.uniec.entity.entrant.Entrant;

import java.util.List;

@State({@org.tandemframework.core.component.Bind(key = "publisherId", binding = "entrant.id")})
public class Model {

    private Entrant entrant = new Entrant();
    private ISelectModel orgUnitModel;

    private List<EntrantRequestSecExt> list;
    private DynamicListDataSource<EntrantRequestSecExt> dataSource;

    public Entrant getEntrant() {
        return entrant;
    }

    public void setEntrant(Entrant entrant) {
        this.entrant = entrant;
    }

    public ISelectModel getOrgUnitModel() {
        return orgUnitModel;
    }

    public void setOrgUnitModel(ISelectModel orgUnitModel) {
        this.orgUnitModel = orgUnitModel;
    }

    public List<EntrantRequestSecExt> getList() {
        return list;
    }

    public void setList(List<EntrantRequestSecExt> list) {
        this.list = list;
    }

    public DynamicListDataSource<EntrantRequestSecExt> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<EntrantRequestSecExt> dataSource) {
        this.dataSource = dataSource;
    }


}
