package ru.tandemservice.eventssecrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_eventssecrmc_1x0x0_3to4 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.12"),
                        new ScriptDependency("org.tandemframework.shared", "1.2.5"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.2.5")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrollmentCampaignSecExt

        // создано обязательное свойство useStudentState
        {
            // создать колонку
            tool.createColumn("enrollmentcampaignsecext_t", new DBColumn("usestudentstate_p", DBType.BOOLEAN));

            // задать значение по умолчанию
            java.lang.Boolean defaultUseStudentState = true;
            tool.executeUpdate("update enrollmentcampaignsecext_t set usestudentstate_p=? where usestudentstate_p is null", defaultUseStudentState);

            // сделать колонку NOT NULL
            tool.setColumnNullable("enrollmentcampaignsecext_t", "usestudentstate_p", false);

        }

        // создано обязательное свойство useEntrantState
        {
            // создать колонку
            tool.createColumn("enrollmentcampaignsecext_t", new DBColumn("useentrantstate_p", DBType.BOOLEAN));
            // задать значение по умолчанию
            java.lang.Boolean defaultUseEntrantState = true;
            tool.executeUpdate("update enrollmentcampaignsecext_t set useentrantstate_p=? where useentrantstate_p is null", defaultUseEntrantState);

            // сделать колонку NOT NULL
            tool.setColumnNullable("enrollmentcampaignsecext_t", "useentrantstate_p", false);

        }


    }
}