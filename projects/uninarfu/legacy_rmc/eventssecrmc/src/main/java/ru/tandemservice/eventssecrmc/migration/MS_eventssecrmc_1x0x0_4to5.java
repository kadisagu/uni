package ru.tandemservice.eventssecrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_eventssecrmc_1x0x0_4to5 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.5.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.5.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        if (!tool.tableExists("enrollmentcampaignsecext_t") || tool.columnExists("enrollmentcampaignsecext_t", "checkaccess_p"))
            return;

        // создать колонку
        tool.createColumn("enrollmentcampaignsecext_t", new DBColumn("checkaccess_p", DBType.BOOLEAN));

        java.lang.Boolean defaultCheckAccess = true;        // TODO: правильно?
        tool.executeUpdate("update enrollmentcampaignsecext_t set checkaccess_p=? where checkaccess_p is null", defaultCheckAccess);

        // сделать колонку NOT NULL
        tool.setColumnNullable("enrollmentcampaignsecext_t", "checkaccess_p", false);
    }
}