package ru.tandemservice.eventssecrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_eventssecrmc_1x0x0_1to2 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.11"),
                        new ScriptDependency("org.tandemframework.shared", "1.2.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.2.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrollmentCampaignSecExt

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("enrollmentcampaignsecext_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("enrollmentcampaign_id", DBType.LONG).setNullable(false),
                                      new DBColumn("useentrantrequestsecext_p", DBType.BOOLEAN).setNullable(false),
                                      new DBColumn("disallowcreaterequest_p", DBType.BOOLEAN).setNullable(false),
                                      new DBColumn("disalloweditrequest_p", DBType.BOOLEAN).setNullable(false),
                                      new DBColumn("disallowdeleterequest_p", DBType.BOOLEAN).setNullable(false),
                                      new DBColumn("disallowchangerequestowner_p", DBType.BOOLEAN).setNullable(false),
                                      new DBColumn("allowactiverequest_p", DBType.INTEGER).setNullable(false)

            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("enrollmentCampaignSecExt");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность entrantRequestSecExt

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("entrantrequestsecext_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("entrantrequest_id", DBType.LONG).setNullable(false),
                                      new DBColumn("orgunit_id", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("entrantRequestSecExt");

        }


    }
}