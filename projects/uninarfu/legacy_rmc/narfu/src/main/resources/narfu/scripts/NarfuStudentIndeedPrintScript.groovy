package narfu.scripts

import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.entity.OrderDirection
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLJoinType
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.SharedRtfUtil
import org.tandemframework.rtf.data.IRtfData
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager
import org.tandemframework.shared.employeebase.base.entity.EmployeePost
import org.tandemframework.shared.fias.base.entity.AddressDetailed
import org.tandemframework.shared.fias.base.entity.AddressRu
import org.tandemframework.shared.organization.base.entity.OrgUnit
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import ru.tandemservice.militaryrmc.dao.StudenOrderInfo
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes
import ru.tandemservice.movestudentrmc.entity.DocOrdRepresent
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase
import ru.tandemservice.movestudentrmc.entity.ListOrdListRepresent
import ru.tandemservice.movestudentrmc.entity.ListRepresent
import ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents
import ru.tandemservice.movestudentrmc.entity.Representation
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentExtractStatesCodes
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentationTypeCodes
import ru.tandemservice.narfu.component.documents.d101.Add.Model
import ru.tandemservice.narfu.military.dao.OrderDAONarfu
import ru.tandemservice.uni.UniDefines
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.uni.entity.employee.Student
import ru.tandemservice.uni.util.rtf.UniRtfUtil

/**
 * "Справка «Действительно является студентом»
 *
 * @author Andrey Andreev
 * @since 18.03.2016
 */
return new NarfuStudentIndeedPrint(
        session: session,
        template: template,
        model: object
).print();


class NarfuStudentIndeedPrint
{
    Session session
    byte[] template
    Model model

    private static final List<String> removedTags = new ArrayList<String>();
    RtfInjectModifier injectModifier = new RtfInjectModifier()


    def print()
    {
        RtfDocument document = createPrintForm(template, model);
        createInjectModifier(model).modify(document);
        createTableModifier(model).modify(document);

        return [document: RtfUtil.toByteArray(document),
                fileName: "Справка «Действительно является студентом» ${model.getStudent().fullFio}.rtf",
                rtf     : document]
    }

    public static RtfDocument createPrintForm(byte[] template, Model model)
    {
        RtfDocument document = new RtfReader().read(template)

        boolean delParagraphAfter = true;
        if (model.getStudent().getEntranceYear() < 2011 && getBranch(model.getStudent().getEducationOrgUnit().getFormativeOrgUnit()) == null)
        {
            IRtfElement parag = UniRtfUtil.findElement(document.getElementList(), "parag");
            delParagraphAfter = false;
            RtfString str = new RtfString();

            str.append("Федеральное государственное автономное образовательное учреждение высшего профессионального образования " +
                       "«Северный (Арктический) федеральный университет» создано распоряжением Правительства Российской Федерации от  " +
                       "02.04.2010 № 502-р путем изменения типа существующего государственного образовательного учреждения высшего профессионального " +
                       "образования «Архангельский государственный технический университет». ")
               .par()
               .append("Приказом Министерства образования и науки Российской Федерации от 02 февраля 2011 г. № 154 федеральное государственное автономное " +
                       "образовательное учреждение высшего профессионального образования «Северный (Арктический) федеральный университет» переименовано " +
                       "в федеральное государственное автономное образовательное учреждение высшего профессионального образования «Северный (Арктический) " +
                       "федеральный университет имени М.В. Ломоносова» и реорганизовано путем присоединения к нему Государственного образовательного учреждения " +
                       "высшего профессионального образования «Поморский государственный университет имени М.В. Ломоносова», Федерального государственного " +
                       "образовательного учреждения среднего профессионального образования «Северодвинский технический колледж» и Федерального государственного " +
                       "образовательного учреждения среднего профессионального образования «Архангельский лесотехнический колледж Императора Петра I».")
               .par()
               .append("Институты бывшего АГТУ созданы на базе ранее существующих факультетов приказом от 16.06.2010 г. № 192.")
               .append("Институты бывшего ПГУ созданы на базе ранее существующих факультетов приказом от 07.06.2011 г. № 324");

            document.getElementList().addAll(document.getElementList().indexOf(parag), str.toList());
        }

        SharedRtfUtil.removeParagraphsWithTagsRecursive(document, removedTags, true, false);

        SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Arrays.asList("parag"), false, delParagraphAfter);

        if (!model.isWithSignatureRector())
            UniRtfUtil.deleteRowsWithLabels(document, Collections.singletonList("FIOrektor"));

        return document;
    }

    protected static RtfInjectModifier createInjectModifier(Model model)
    {

        RtfInjectModifier injectModifier = new RtfInjectModifier();

        //List<StudenOrderInfo> orderList = model.getOrdersList();
        //if (orderList == null) {

        List<StudenOrderInfo> orderList = OrderDAONarfu.getInstance().getOrderInfo(null, model.getStudent());
        model.setOrdersList(orderList);
//    	}

        boolean male = model.getStudent().getPerson().isMale();
        TopOrgUnit academy = TopOrgUnit.getInstance();
        Calendar c = Calendar.getInstance();
        c.setTime(model.getFormingDate());

        OrgUnit branchOU = getBranch(model.getStudent().getEducationOrgUnit().getFormativeOrgUnit());

        if (branchOU != null && !model.isWithSignatureRector())
            injectModifier.put("branchShortTitle", branchOU.getShortTitle());
        else
            removedTags.add("branchShortTitle");

        if (!model.isWithSignatureRector() && getBranch(model.getStudent().getEducationOrgUnit().getFormativeOrgUnit()) == null)
            injectModifier.put("formOrgUnit", model.getStudent().getEducationOrgUnit().getFormativeOrgUnit().getTitle());
        else
            removedTags.add("formOrgUnit");

        OrgUnit orgUnit = model.getStudent().getEducationOrgUnit().getFormativeOrgUnit();

        if (model.isWithSignatureRector())
            orgUnit = academy;
        else if (getBranch(orgUnit) != null)
            orgUnit = getBranch(orgUnit);


        AddressDetailed address = orgUnit.getPostalAddress();
        RtfString addressStr = new RtfString();
        if (address != null)
        {
            AddressRu addressRu = address instanceof AddressRu ? (AddressRu) address : null;

            addressStr.append(addressRu != null && addressRu.getStreet() == null ? "" : addressRu.getStreet().getTitleWithTypeNonPersistent())
                      .append(addressRu != null && addressRu.getHouseNumber() == null ? "" : ", " + addressRu.getHouseNumber() + ", ")
                      .append(addressRu != null && addressRu.getHouseUnitNumber() == null ? "" : addressRu.getHouseUnitNumber() + ", ")
                      .append(IRtfData.PAR)
                      .append(address.getSettlement() == null ? "" : address.getSettlement().getTitleWithType())
                      .append(address.getCountry() == null ? "" : ", " + address.getCountry().getTitle())
                      .append(addressRu != null && addressRu.getInheritedPostCode() == null ? "" : ", " + addressRu.getInheritedPostCode());
        }


        injectModifier.put("faxInst", orgUnit.getFax());
        injectModifier.put("telinst", orgUnit.getPhone())
                      .put("email", orgUnit.getEmail())
                      .put("site", orgUnit.getWebpage())
                      .put("address", addressStr);
        injectModifier.put("DateS", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getFormingDate()));
        injectModifier.put("NumS", String.valueOf(model.getNumber()));
        injectModifier.put("FIO", model.getStudentTitleStr());

        injectModifier.put("kurs", model.getCourse() == null ? "" : model.getCourse().getTitle());

        injectModifier.put("fo", StringUtils.lowerCase(model.getStudent().getEducationOrgUnit().getDevelopForm().getGenCaseTitle()));
        injectModifier.put("formPr", model.getStudent().getEducationOrgUnit().getFormativeOrgUnit().getGenitiveCaseTitle());
        injectModifier.put("telInst", model.getPhone());
        injectModifier.put("FIOisp", model.getExecutor());
        injectModifier.put("FIOdir", model.getManagerFio());
        injectModifier.put("dateF", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getDateEnd()));

        injectModifier.put("SprVidana", model.getDocumentForTitle());

        injectModifier.put("sexTitle", male ? "он" : "она");
        injectModifier.put("studentCase", male ? "студентом" : "студенткой");
        injectModifier.put("studentCaseB", male ? "Обучающийся" : "Обучающаяся");
        injectModifier.put("dateN", "01.09." + String.valueOf(model.getStudent().getEntranceYear()));

        if (model.getStudent().getStatus().getCode().equals(UniDefines.CATALOG_STUDENT_STATUS_EXCLUDED) || model.getStudent().getStatus().getCode().equals(UniDefines.CATALOG_STUDENT_STATUS_GRADUATED_WITH_DIPLOMA))
        {
            injectModifier.put("DO", male ? "являлся" : "являлась");
            injectModifier.put("learning", male ? "Обучался" : "Обучалась");
            injectModifier.put("DateStr", "Дата");
            Date date = getDateExclude(model.getStudent());
            injectModifier.put("dateF", date != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(date) : "");
        } else
        {
            injectModifier.put("DO", "является");
            injectModifier.put("learning", "Обучается");
            injectModifier.put("DateStr", "Предполагаемая дата");
        }

        if (model.getStudent().getCompensationType().getCode().equals("1"))
        {
            injectModifier.put("bud", "за счёт средств федерального бюджета");
        } else
        {
            injectModifier.put("bud", "с полным возмещением затрат на обучение");
        }

        //приказ о зачислении
        String numP = "-";
        String dateP = "-";
        for (StudenOrderInfo info : model.getOrdersList())
            if (info.getExtractType().getTitle().contains("зачислен"))
            {
                numP = info.getOrderNumber();
                dateP = DateFormatter.DEFAULT_DATE_FORMATTER.format(info.getOrderDate());
                break;
            }
        injectModifier.put("numP", numP);
        injectModifier.put("dateP", dateP);

        if (model.isWithSignatureRector())
        {
            EmployeePost post = EmployeeManager.instance().dao().getHead(TopOrgUnit.getInstance());

            injectModifier.put("FIOrektor", post != null ? post.getPerson().getIdentityCard().getIof() : "");
        }


        return injectModifier;
    }

    protected static RtfTableModifier createTableModifier(Model model)
    {
        RtfTableModifier tm = new RtfTableModifier();

        List<StudenOrderInfo> orderList = model.getOrdersList();
        if (orderList == null)
        {
            orderList = OrderDAONarfu.getInstance().getOrderInfo(null, model.getStudent());
            model.setOrdersList(orderList);
        }

        Collections.sort(orderList);

        List<String> orderCodes = Arrays.asList(
                StudentExtractTypeCodes.RESTORATION_VARIANT_1_MODULAR_ORDER, //О восстановлении
                StudentExtractTypeCodes.WEEKEND_OUT_MODULAR_ORDER, //О выходе из академического отпуска
                StudentExtractTypeCodes.EDU_ENROLLMENT_VARIANT_1_MODULAR_ORDER, //О зачислении
                StudentExtractTypeCodes.EDU_ENROLLMENT_AS_TRANSFER_MODULAR_ORDER, //О зачислении в порядке перевода
                StudentExtractTypeCodes.TRANSFER_EXT_MODULAR_ORDER, //О переводе
                StudentExtractTypeCodes.TRANSFER_GROUP_MODULAR_ORDER, //О переводе из группы в группу
                StudentExtractTypeCodes.COMPENSATION_TYPE_TRANSFER_MODULAR_ORDER,//О переводе на другую основу оплаты обучения
                StudentExtractTypeCodes.DEVELOP_FORM_TRANSFER_MODULAR_ORDER, //О переводе на другую форму освоения
                StudentExtractTypeCodes.TRANSFER_EDU_LEVEL_MODULAR_ORDER, //	 О переводе со специальности на специальность (с направления на направление)
                StudentExtractTypeCodes.WEEKEND_VARIANT_1_MODULAR_ORDER, //О предоставлении академического отпуска
                StudentExtractTypeCodes.HOLIDAY_MODULAR_ORDER, //О предоставлении каникул
                StudentExtractTypeCodes.WEEKEND_CHILD_ONE_AND_HALF_MODULAR_ORDER, //О предоставлении отпуска по уходу за ребенком до 1,5 лет
                StudentExtractTypeCodes.PROLONG_WEEKEND_MODULAR_ORDER, //О продлении академического отпуска
                StudentExtractTypeCodes.EXCLUDE_STUDENT_VARIANT_1_MODULAR_ORDER, //Об отчислении
                StudentExtractTypeCodes.EXCLUDE_DUE_TRANSFER_MODULAR_ORDER, //Об отчислении в связи с переводом
                //StudentExtractTypeCodes.COURSE_TRANSFER_LIST_ORDER, //О переводе с курса на курс студентов
                StudentExtractTypeCodes.EXCLUDE_VARIANT_1_LIST_ORDER, //Об отчислении
                //StudentExtractTypeCodes.TRANSFER_NEXT_COURSE_LIST_EXTRACT, //Перевести на следующий курс
                StudentExtractTypeCodes.EXCLUDE_STUDENT_LIST_EXTRACT, //Отчислить

                RepresentationTypeCodes.EXCLUDE,
                RepresentationTypeCodes.WEEKEND,
                RepresentationTypeCodes.TRANSFER,
                RepresentationTypeCodes.EXCLUDE_OUT,
                RepresentationTypeCodes.WEEKEND_OUT,
                RepresentationTypeCodes.WEEKEND_PROLONG,
                RepresentationTypeCodes.DIPLOM_AND_EXCLUDE,
                RepresentationTypeCodes.DISTRIBUTION_TO_BUDGET,
                RepresentationTypeCodes.ENROLLMENT_TRANSFER,
                RepresentationTypeCodes.DIPLOM_AND_EXCLUDE_LIST,
                RepresentationTypeCodes.EXCLUDE_LIST,
                RepresentationTypeCodes.TRANSFER_LIST,
                RepresentationTypeCodes.WEEKEND_LIST
        );

        List<String[]> tableData = new ArrayList<String[]>();
        for (StudenOrderInfo info : orderList)
        {
            if (!orderCodes.contains(info.getExtractType().getCode()))
                continue;

            StringBuilder sb = new StringBuilder("Приказ ").append(info.getExtractType() == null ? "" : StringUtils.uncapitalize(info.getExtractType().getTitle()));
            if (info.getOrderDate() != null)
                sb.append(" от ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(info.getOrderDate()));

            if (!StringUtils.isEmpty(info.getOrderNumber()))
                sb.append(" № ").append(info.getOrderNumber());

            String[] t = new String[1];
            t[0] = sb.toString();
            tableData.add(t);
        }

        tm.put("T", tableData as String [][]);

        return tm;
    }

    private static OrgUnit getBranch(OrgUnit formOU)
    {

        if (isBranch(formOU))
            return formOU;

        OrgUnit ou = formOU;
        while (ou.getParent() != null)
        {
            ou = (OrgUnit) ou.getParent();
            if (isBranch(ou))
                return ou;
        }

        return null;
    }

    private static boolean isBranch(OrgUnit ou)
    {
        return ou.getOrgUnitType().getCode().equals("branch");
    }

    private static Date getDateExclude(Student student)
    {

        IUniBaseDao dao = IUniBaseDao.instance.get();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocRepresentStudentBase.class, "rel")
                                                         .joinEntity("rel", DQLJoinType.inner, DocOrdRepresent.class, "o", DQLExpressions.eq(DQLExpressions.property(DocRepresentStudentBase.representation().id().fromAlias("rel")), DQLExpressions.property(DocOrdRepresent.representation().id().fromAlias("o"))))
                                                         .where(DQLExpressions.eqValue(DQLExpressions.property(DocRepresentStudentBase.student().id().fromAlias("rel")), student.getId()))
                                                         .where(DQLExpressions.eqValue(DQLExpressions.property(DocRepresentStudentBase.representation().state().code().fromAlias("rel")), MovestudentExtractStatesCodes.CODE_6))
                                                         .where(DQLExpressions.in(DQLExpressions.property(DocRepresentStudentBase.representation().type().code().fromAlias("rel")), Arrays.asList(RepresentationTypeCodes.EXCLUDE, RepresentationTypeCodes.DIPLOM_AND_EXCLUDE)))
                                                         .column(DQLExpressions.property(DocRepresentStudentBase.representation().fromAlias("rel")))
                                                         .order(DQLExpressions.property(DocOrdRepresent.order().commitDate().fromAlias("o")), OrderDirection.desc);

        List<Representation> indivOrders = dao.getList(builder);
        Date indivDateExclude = null;
        if (!indivOrders.isEmpty())
            indivDateExclude = indivOrders.get(0).getStartDate();

        DQLSelectBuilder builder2 = new DQLSelectBuilder().fromEntity(RelListRepresentStudents.class, "rel")
                                                          .joinEntity("rel", DQLJoinType.inner, ListOrdListRepresent.class, "o", DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudents.representation().id().fromAlias("rel")), DQLExpressions.property(ListOrdListRepresent.representation().id().fromAlias("o"))))
                                                          .where(DQLExpressions.eqValue(DQLExpressions.property(RelListRepresentStudents.student().id().fromAlias("rel")), student.getId()))
                                                          .where(DQLExpressions.eqValue(DQLExpressions.property(RelListRepresentStudents.representation().state().code().fromAlias("rel")), MovestudentExtractStatesCodes.CODE_6))
                                                          .where(DQLExpressions.in(DQLExpressions.property(RelListRepresentStudents.representation().representationType().code().fromAlias("rel")), Arrays.asList(RepresentationTypeCodes.EXCLUDE_LIST, RepresentationTypeCodes.DIPLOM_AND_EXCLUDE_LIST)))
                                                          .column(DQLExpressions.property(RelListRepresentStudents.representation().fromAlias("rel")))
                                                          .order(DQLExpressions.property(ListOrdListRepresent.order().commitDate().fromAlias("o")), OrderDirection.desc);

        List<ListRepresent> listOrders = dao.getList(builder2);

        Date listDateExclude = null;

        if (!listOrders.isEmpty())
        {
            ListRepresent listRepresent = listOrders.get(0);
            listDateExclude = (Date) listRepresent.getProperty("dateExclude");
        }

        if (indivDateExclude != null && listDateExclude != null)
            if (indivDateExclude.after(listDateExclude))
                return indivDateExclude;
            else
                return listDateExclude;
        if (indivDateExclude != null && listDateExclude == null)
            return indivDateExclude;
        if (indivDateExclude == null && listDateExclude != null)
            return listDateExclude;

        return null;
    }
}