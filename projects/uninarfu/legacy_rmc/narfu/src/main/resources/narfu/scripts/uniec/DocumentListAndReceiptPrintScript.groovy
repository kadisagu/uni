package narfu.scripts.uniec

import org.hibernate.Session
import org.tandemframework.core.runtime.ApplicationRuntime
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.sec.entity.Principal
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import ru.tandemservice.eventssecrmc.dao.DaoFacade
import ru.tandemservice.narfu.entity.EnrCampaignDevelopFormDates
import ru.tandemservice.narfu.utils.NarfuEntrantUtil
import ru.tandemservice.unibasermc.util.DeclinationUtil
import ru.tandemservice.uniec.entity.catalog.EnrollmentDocument
import ru.tandemservice.uniec.entity.entrant.EntrantEnrollmentDocument
import ru.tandemservice.uniec.entity.entrant.EntrantRequest
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection
import ru.tandemservice.uniec.entity.settings.UsedEnrollmentDocument

return new DocumentListAndReceiptPrint(                           // стандартные входные параметры скрипта
        session: session,                                         // сессия
        template: template,                                       // шаблон
        entrantRequest: session.get(EntrantRequest.class, object), // объект печати
        principal: principal
).print()

/**
 * Алгоритм печати описи и расписки абитуриента
 *
 * @author Mike
 * @since 23.02.2012
 */
class DocumentListAndReceiptPrint
{
    Session session
    byte[] template
    EntrantRequest entrantRequest
    Principal principal
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()
    def bean = ApplicationRuntime.getBean("entrantDocumentsInventoryAndReceiptPrintBean")

    def print()
    {
        def person = entrantRequest.entrant.person
        byte[] aspTemplate = bean.getAspirantTemplate(entrantRequest, template)
        if (aspTemplate != null)
            template = aspTemplate;
        im.put('highSchoolTitle', TopOrgUnit.instance.title)
        im.put('inventoryNumber', entrantRequest.stringNumber)
        im.put('FIO', DeclinationUtil.getDeclinableFIO(GrammaCase.NOMINATIVE, person.identityCard))
        im.put('fromFIO', DeclinationUtil.getDeclinableFIO(GrammaCase.GENITIVE, person.identityCard))
        im.put('FIOshort', person.identityCard.iof)
        im.put('receiptNumber', entrantRequest.stringNumber)
        im.put('regDate', entrantRequest.regDate.format('dd.MM.yyyy'))
        im.put('Date', DateFormatter.STRING_MONTHS_AND_QUOTES.format(new Date()))
        im.put('FIOsec', bean.getSecretary(TopOrgUnit.instance))
        im.put('phone', "")
        RequestedEnrollmentDirection firstReqDir = bean.getFirstRequestDirection(entrantRequest)
        if (firstReqDir != null)
        {
            DaoFacade.getDaoPrintUtil().injectSelectionCommissionPhones(im, firstReqDir.entrantRequest)
            DaoFacade.getDaoPrintUtil().injectSelectionCommission(im, firstReqDir.entrantRequest)

        }
        else
        {
            im.put('SelectionCommission', "")
            im.put('SelectionCommissionPhones', "")
            //   im.put('delegatePost', "")
            im.put('delegateFIO', "")
        }

        Date docDate = NarfuEntrantUtil.getLastEnrCampaignDevelopFormDate(entrantRequest);
        if (docDate != null)
        {
            im.put("date_doc", DateFormatter.DEFAULT_DATE_FORMATTER.format(docDate));
        }
        else
        {
            im.put("date_doc", "___________");
        }

        // получаем названия документов согласно настройке 'Используемые документы для подачи в ОУ и их порядок' 
        def titles = getTitles()

        int i = 1
        tm.put('T1', titles.collect { [i++, it] } as String[][])
        i = 1
        tm.put('T2', titles.collect { [(i++) + '.', it] } as String[][])
        bean.extendTable(tm, entrantRequest.entrant);

        //Получаем даты приема оригиналов
        def dates = getDates()
        tm.put('T3', dates.collect { [it.docDate.format('dd.MM.yyyy'), "(" + it.developForm.title + " форма обучения)"] } as String[][])

        i = 1
        def exams = getExams() //bean.getDisciplineList(entrantRequest.entrant)
        if (exams.size() > 0)
        {
            tm.put('T4', exams.collect { [i++, it[0], it[1], it[2]] } as String[][])
            im.put('examComment', "Примечание. Опоздавшие и не явившиеся в срок допускаются к экзамену с разрешения ответственного секретаря приемной комиссии.")
            im.put('doNotNeedExam', "")
            im.put('scheduleAccept', "Расписание составлено верно.")
            DaoFacade.getDaoPrintUtil().injectMainSecretary(im, firstReqDir.entrantRequest)
        }
        else
        {
            tm.remove('T4')
            tm.remove('delegatePost')
            im.put('scheduleAccept', "")
            im.put('examComment', "")
            im.put('doNotNeedExam', "Абитуриенту не нужно сдавать внутренние вступительные испытания")
        }

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(template, im, tm),
                fileName: "Расписка абитуриента ${person.identityCard.fullFio}.rtf"]
    }

    def getTitles()
    {
        def entrant = entrantRequest.entrant          // абитуриент
        def edu = entrant.person.personEduInstitution // основное законченное образовательное учреждение

        // получаем список документов из заявления в порядке приоритета
        def documents = DQL.createStatement(session, /
                from ${EntrantEnrollmentDocument.class.simpleName}
                where ${EntrantEnrollmentDocument.entrantRequest().id()} = ${entrantRequest.id}
                order by ${EntrantEnrollmentDocument.enrollmentDocument().priority()}
        /).<EntrantEnrollmentDocument> list()

        // загружаем настройку 'Используемые документы для подачи в ОУ и их порядок'
        def settingMap = DQL.createStatement(session, /
                from ${UsedEnrollmentDocument.class.simpleName}
                where ${UsedEnrollmentDocument.enrollmentCampaign().id()} = ${entrant.enrollmentCampaign.id}
        /).<UsedEnrollmentDocument> list()
                .<EnrollmentDocument, UsedEnrollmentDocument> collectEntries { [it.enrollmentDocument, it] }

        // для каждого документа, согласно настройке, получаем его название
        def titles = new ArrayList<String>()
        for (def document : documents)
        {
            def title = new StringBuilder(document.enrollmentDocument.title)
            title.append(' ')
            def setting = settingMap.get(document.enrollmentDocument)

            if (setting?.printOriginalityInfo)
                title.append(document.copy ? '(копия) ' : '')

            if (setting?.printEducationDocumentInfo && edu)
            {
                title.append(bean.getDocumentInfo(document))
            }
            titles.add(title.toString())
        }
        def achievements = bean.getAchievements(entrant);
        if (achievements.size() > 0)
            titles.add("Индивидуальные достижения:")
        return titles
    }

    def getDates()
    {
        def comp = entrantRequest.entrant.enrollmentCampaign
        def dataList = DQL.createStatement(session, /
                from ${EnrCampaignDevelopFormDates.class.simpleName}
                where ${EnrCampaignDevelopFormDates.enrollmentCampaign().id()} = ${comp.id}
                order by ${EnrCampaignDevelopFormDates.docDate()}
        /).<EnrCampaignDevelopFormDates> list()

        return dataList
    }

    def getExams()
    {
        def res = new ArrayList<String[]>()
        def exams = bean.getDisciplineList(entrantRequest.entrant)
        for (def e : exams)
        {
            def passDate = bean.getExamPassDate(e)
            if (passDate == null)
                passDate = e.passDate
            def line = new String[3]
            line[0] = e.enrollmentCampaignDiscipline.educationSubject.title
            line[1] = e.subjectPassForm?.title
            line[2] = passDate?.format('dd.MM.yyyy')
            res.add(line)
        }
        return res
    }
}
