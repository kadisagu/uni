package ru.tandemservice.narfu.uniec.component.order.EnrollmentOrderPub.EnrollmentParagraphPersonalInfoDialog;

import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.List;


@Input({
        @org.tandemframework.core.component.Bind(key = "orderId", binding = "orderId")
})

public class Model {

    private Long orderId;

    private IAbstractOrder order;

    private List<IAbstractParagraph<IAbstractOrder>> paragraphsList;

    private ISelectModel paragraphsModel;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public IAbstractOrder getOrder() {
        return order;
    }

    public void setOrder(IAbstractOrder order) {
        this.order = order;
    }

    public List<IAbstractParagraph<IAbstractOrder>> getParagraphsList() {
        return paragraphsList;
    }

    public void setParagraphsList(
            List<IAbstractParagraph<IAbstractOrder>> paragraphsList)
    {
        this.paragraphsList = paragraphsList;
    }

    public ISelectModel getParagraphsModel() {
        return paragraphsModel;
    }

    public void setParagraphsModel(ISelectModel paragraphsModel) {
        this.paragraphsModel = paragraphsModel;
    }


}
