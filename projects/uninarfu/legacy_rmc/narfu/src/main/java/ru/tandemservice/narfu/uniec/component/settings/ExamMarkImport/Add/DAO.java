package ru.tandemservice.narfu.uniec.component.settings.ExamMarkImport.Add;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import ru.tandemservice.narfu.entity.EntrantExamBulletinImportedFile;
import ru.tandemservice.narfu.entity.catalog.ExamBulletinImportedFileResult;
import ru.tandemservice.narfu.entity.catalog.codes.ExamBulletinImportedFileResultCodes;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.ExamPassMarkIstu;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.entrant.ExamPassMark;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void process(Model model) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(model.getUploadFile().getStream()));

        List<String[]> errorList = new ArrayList<>();
        int row = 0;
        while (reader.ready()) {
            row++;
            String line = reader.readLine();
            if (StringUtils.isEmpty(line))
                continue;

            String code = null;
            Double mark = null;
            try {
                String[] arr = line.split(";");
                code = arr[0].trim();
                mark = Double.parseDouble(arr[1].trim());
            }
            catch (Exception ex) {
                errorList.add(new String[]{"Ошибка в строке №" + row + ": " + line});
                continue;
            }

            List<ExamPassMarkIstu> list = new DQLSelectBuilder().fromEntity(ExamPassMarkIstu.class, "e")
                    .where(DQLExpressions.eqValue(DQLExpressions.property(ExamPassMarkIstu.workCipher().fromAlias("e")), code))
                    .createStatement(getSession())
                    .list();

            if (list.isEmpty()) {
                errorList.add(new String[]{"Не найдент шифр работы №" + code});
                continue;
            }
            if (list.size() > 1) {
                errorList.add(new String[]{"Шифр работы не уникален №" + code});
                continue;
            }
            if (mark < 0 || mark > 100) {
                errorList.add(new String[]{"Неправильная оценка для шифра работы №" + code + ": " + mark});
                continue;
            }

            ExamPassMark examPassMark = get(ExamPassMark.class, ExamPassMark.examPassDiscipline(), list.get(0).getExamPassDiscipline());
            if (examPassMark == null) {
                examPassMark = new ExamPassMark();
                examPassMark.setExamPassDiscipline(list.get(0).getExamPassDiscipline());
            }
            examPassMark.setMark(mark);
            examPassMark.setEntrantAbsenceNote(null);
            //examPassMark.setVersion(1);
            getSession().saveOrUpdate(examPassMark);
        }


        EntrantExamBulletinImportedFile log = new EntrantExamBulletinImportedFile();
        log.setImportDate(new Date());
        log.setImportedFileCRC("");

        model.getUploadFile().getStream().reset();
        log.setImportedFile(IOUtils.toByteArray(model.getUploadFile().getStream()));

        RtfDocument errorFile = createErrorFile(errorList);
        if (errorFile != null) {
            log.setResultFile(RtfUtil.toByteArray(errorFile));
            log.setImportResult(getCatalogItem(ExamBulletinImportedFileResult.class, ExamBulletinImportedFileResultCodes.IMPORT_ERROR));
        }
        else
            log.setImportResult(getCatalogItem(ExamBulletinImportedFileResult.class, ExamBulletinImportedFileResultCodes.IMPORT_OK));

        getSession().saveOrUpdate(log);
    }

    private RtfDocument createErrorFile(List<String[]> errorList) {
        if (errorList.isEmpty())
            return null;

        TemplateDocument template = getCatalogItem(TemplateDocument.class, "narfuEntrantExamBulletinImport");
        RtfDocument resultRtf = new RtfReader().read(template.getContent());

        RtfTableModifier tm = new RtfTableModifier();
        tm.put("T", errorList.toArray(new String[][]{}));
        tm.modify(resultRtf);

        return resultRtf;
    }
}
