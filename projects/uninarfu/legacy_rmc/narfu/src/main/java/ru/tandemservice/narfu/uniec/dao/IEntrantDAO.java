package ru.tandemservice.narfu.uniec.dao;

import org.tandemframework.core.CoreCollectionUtils;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;

public interface IEntrantDAO extends ru.tandemservice.uniec.dao.IEntrantDAO {
    /**
     * Метод нужен для обработки приказов о зачислении на 2+ курсы
     *
     * @param student
     *
     * @return
     */
    public CoreCollectionUtils.Pair saveOrUpdatePreliminaryStudentExt(PreliminaryEnrollmentStudent student);
}
