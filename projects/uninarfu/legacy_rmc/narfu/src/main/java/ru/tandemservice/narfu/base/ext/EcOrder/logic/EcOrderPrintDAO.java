package ru.tandemservice.narfu.base.ext.EcOrder.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import ru.tandemservice.narfu.component.enrollmentextract.EnrollmentOrderPrintNARFU;
import ru.tandemservice.narfu.entity.EnrollmentOrderNARFU;
import ru.tandemservice.narfu.utils.PrintUtil;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.catalog.codes.StructureEducationLevelsCodes;
import ru.tandemservice.uni.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EcOrderPrintDAO extends UniBaseDao implements IEcOrderPrintDAO {

    private static IEcOrderPrintDAO dao;

    @Override
    public RtfDocument getDownloadPrintPersonalInfo(Long orderId) {
        UniecScriptItem scriptItem = get(UniecScriptItem.class, UniecScriptItem.CATALOG_ITEM_CODE, "studentPersonalInfoCard");

        RtfDocument document = RtfBean.getElementFactory().createRtfDocument();

        RtfDocument template = null;
        if (scriptItem != null) {
            template = new RtfReader().read(scriptItem.getCurrentTemplate());
            document.setHeader(template.getHeader());
            document.setSettings(template.getSettings());
        }

        EnrollmentOrder order = get(EnrollmentOrder.class, orderId);

        for (IAbstractParagraph paragraph : order.getParagraphList()) {
            List<EnrollmentExtract> enrollmentExtractList = paragraph.getExtractList();
            if (enrollmentExtractList.size() == 0) {
                throw new ApplicationException("Пустой параграф №" + paragraph.getNumber() + ".");
            }

            for (EnrollmentExtract extract : enrollmentExtractList) {

                RtfDocument page = template.getClone();

                PreliminaryEnrollmentStudent preStudent = extract.getEntity();
                EntrantRequest entrantRequest = preStudent.getRequestedEnrollmentDirection().getEntrantRequest();
                IdentityCard card = entrantRequest.getEntrant().getPerson().getIdentityCard();
                RequestedEnrollmentDirection direction = preStudent.getRequestedEnrollmentDirection();
                CompetitionKind compKind = direction.getCompetitionKind();

                StringBuilder met = new StringBuilder("");

                if (preStudent.getCompensationType().getCode().equals(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT))
                    met.append("Д ");

                List<PersonBenefit> benefits = getList(PersonBenefit.class, PersonBenefit.person(), entrantRequest.getEntrant().getPerson());
                if (!benefits.isEmpty()) {
                    for (PersonBenefit benefit : benefits) {
                        String title = benefit.getBenefit().getTitle();
                        if (title.contains("Дети-сироты"))
                            met.append("С ");
                        else if (title.contains("Дети-инвалиды") || title.contains("Инвалид 1 группы") || title.contains("Инвалид 2 группы"))
                            met.append("И ");
                        else
                            met.append("Л ");
                    }
                }

                if (!card.getCitizenship().getTitle().contains("Росс"))
                    met.append("Иг ");

                if (preStudent.getRequestedEnrollmentDirection().isTargetAdmission())
                    met.append("Ц ");

                if (!card.getCitizenship().getTitle().contains("Росс") && compKind.getCode().equals(UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES))
                    met.append("ИгЦ ");

                RtfInjectModifier injectModifier = new RtfInjectModifier()
                        .put("Met", met.toString())
                        .put("formP", extract.getEntity().getEducationOrgUnit().getFormativeOrgUnit().getTitle())
                        .put("fo", extract.getEntity().getEducationOrgUnit().getDevelopForm().getTitle())
                        .put("Bud", extract.getEntity().getRequestedEnrollmentDirection().getCompensationType().getShortTitle())
                        .put("NumA", entrantRequest.getStringNumber())
                        .put("F", card.getLastName())
                        .put("I", card.getFirstName())
                        .put("O", card.getMiddleName() != null ? card.getMiddleName() : "")
                        .put("dateN", entrantRequest.getRegDate() != null ? RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(entrantRequest.getRegDate()) : "");

                String levelTypeCode = extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().getCode();
                if (levelTypeCode.equals(StructureEducationLevelsCodes.HIGH_GOS2_GROUP_MASTER_PROFILE) ||
                        levelTypeCode.equals(StructureEducationLevelsCodes.HIGH_GOS2_GROUP_BACHELOR_PROFILE) ||
                        levelTypeCode.equals(StructureEducationLevelsCodes.ADDITIONAL_GROUP_TRAINING_PROFILE) ||
                        levelTypeCode.equals(StructureEducationLevelsCodes.HIGH_GOS3_GROUP_BACHELOR_PROFILE) ||
                        levelTypeCode.equals(StructureEducationLevelsCodes.HIGH_GOS3_GROUP_MASTER_PROFILE))
                {
                    injectModifier.put("Napr0", extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getParentLevel().getDisplayableTitle());
                    injectModifier.put("Napr", "\"" + extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getTitle() + "\"");
                }
                else {
                    injectModifier.put("Napr0", extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getDisplayableTitle());
                    injectModifier.put("Napr", "-");

                }

                injectModifier.modify(page);

                if (order.getParagraphList().indexOf(paragraph) != (order.getParagraphList().size() - 1))
                    page.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));

                document.getElementList().addAll(page.getElementList());
            }
        }

        return document == null ? RtfBean.getElementFactory().createRtfDocument() : document;
    }

    @Override
    public RtfDocument getDownloadPrintExtracts(Long orderId) {
        /*EnrollmentOrder order = get(EnrollmentOrder.class,orderId);
		
		return getDownloadPrintParagraphExtracts((List<IAbstractParagraph<IAbstractOrder>>)order.getParagraphList());*/

        UniecScriptItem tempHeader = get(UniecScriptItem.class, UniecScriptItem.CATALOG_ITEM_CODE,
                                         "studentPrintExtracts");

        RtfDocument resultDocument = RtfBean.getElementFactory().createRtfDocument();

        RtfDocument template = null;
        if (tempHeader != null) {
            template = new RtfReader().read(tempHeader.getTemplate());
            resultDocument.setHeader(template.getHeader());
            resultDocument.setSettings(template.getSettings());
        }

        EnrollmentOrder order = get(EnrollmentOrder.class, orderId);

        for (IAbstractParagraph paragraph : order.getParagraphList()) {
            List<EnrollmentExtract> enrollmentExtractList = paragraph.getExtractList();
            if (enrollmentExtractList.size() == 0) {
                throw new ApplicationException("Пустой параграф №" + paragraph.getNumber() + ".");
            }

            for (EnrollmentExtract extract : enrollmentExtractList) {
                PreliminaryEnrollmentStudent preStudent = extract.getEntity();
                EntrantRequest entrantRequest = preStudent.getRequestedEnrollmentDirection().getEntrantRequest();
                IdentityCard card = entrantRequest.getEntrant().getPerson().getIdentityCard();
                RequestedEnrollmentDirection direction = preStudent.getRequestedEnrollmentDirection();
                CompetitionKind compKind = direction.getCompetitionKind();
                String developFormTitle = extract.getEntity().getEducationOrgUnit().getDevelopForm().getTitle().toLowerCase();

                //Определяем используемы шаблон параграфа
                UniecScriptItem paragTemplate = new UniecScriptItem();

                if (order.getType().getCode().equals(UniecDefines.ORDER_TYPE_TARGET_BUDGET)) {
                    if (compKind.getCode().equals(UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION) ||
                            compKind.getCode().equals(UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES))
                        paragTemplate.setTemplatePath("narfu/templates/uniec/enrollmetExtracts/enrollmentExtractParagraph_targetBenefitBudget_narfu.rtf");
                    else
                        paragTemplate.setTemplatePath("narfu/templates/uniec/enrollmetExtracts/enrollmentExtractParagraph_targetBudget_narfu.rtf");
                }
                else if (order.getType().getCode().equals(UniecDefines.ORDER_TYPE_STUDENT_BUDGET)) {
                    paragTemplate.setTemplatePath("narfu/templates/uniec/enrollmetExtracts/enrollmentExtractParagraph_studentBudget_narfu.rtf");
                }
                else if (order.getType().getCode().equals(UniecDefines.ORDER_TYPE_STUDENT_CONTRACT)) {
                    if (preStudent.getRequestedEnrollmentDirection().getStudentCategory().getCode().equals(StudentCategoryCodes.STUDENT_CATEGORY_STUDENT) ||
                            preStudent.getRequestedEnrollmentDirection().getStudentCategory().getCode().equals(StudentCategoryCodes.STUDENT_CATEGORY_SECONDARY))
                    {
                        paragTemplate.setTemplatePath("narfu/templates/uniec/enrollmetExtracts/enrollmentExtractParagraph_studentContract_narfu.rtf");
                    }
                    else if (preStudent.getRequestedEnrollmentDirection().getStudentCategory().getCode().equals(StudentCategoryCodes.STUDENT_CATEGORY_LISTENER)) {
                        paragTemplate.setTemplatePath("narfu/templates/uniec/enrollmetExtracts/enrollmentExtractParagraph_listenerContract_narfu.rtf");
                    }
                }

                RtfDocument paragRtfTemp = new RtfReader().read(paragTemplate.getTemplate());
                paragRtfTemp.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));

                RtfDocument page = template.getClone();

                IRtfElement paragraphs = UniRtfUtil.findElement(page.getElementList(), "PARAGRAPHS");

                page.getElementList().addAll(page.getElementList().indexOf(paragraphs), paragRtfTemp.getElementList());
                page.getElementList().remove(paragraphs);

                EnrollmentOrderNARFU orderNARFU = get(EnrollmentOrderNARFU.class, EnrollmentOrderNARFU.enrollmentOrder(), order);
                Date dateP = null;
                String numP = null;
                if (orderNARFU != null) {
                    dateP = orderNARFU.getProtocolDate();
                    numP = orderNARFU.getProtocolNumber();
                }

                RtfTableModifier tableModifier = new RtfTableModifier();
                List<String[]> tableData = new ArrayList<String[]>(enrollmentExtractList.size());
                Double sumMark = UniecDAOFacade.getEntrantDAO().getSumFinalMarks(preStudent.getRequestedEnrollmentDirection());

                String[] row = new String[5];
                row[0] = "1";
                row[1] = card.getFullFio();
                row[2] = entrantRequest.getEntrant().getPersonalNumber();
                row[3] = DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS.format(sumMark);

                if (preStudent.getRequestedEnrollmentDirection().isTargetAdmission()) {
                    TargetAdmissionKind targetAdmissionKind = preStudent.getRequestedEnrollmentDirection().getTargetAdmissionKind();
                    row[4] = "Направление: " + targetAdmissionKind.getTitle();

                }
                else {
                    List<PersonBenefit> benefits = getList(PersonBenefit.class, PersonBenefit.person(), entrantRequest.getEntrant().getPerson());
                    if (!benefits.isEmpty()) {
                        List<String> lgotNames = new ArrayList<String>();
                        for (PersonBenefit benefit : benefits) {
                            lgotNames.add(benefit.getBenefit().getShortTitle());
                        }
                        row[4] = StringUtils.join(lgotNames, ",");
                    }

                }
                tableData.add(row);

                String developPeriodTitle = extract.getEntity().getEducationOrgUnit().getDevelopPeriod().getTitle();
                String developConditionTitle = EnrollmentOrderPrintNARFU.getDevelopConditionTitle(extract.getEntity().getEducationOrgUnit().getDevelopCondition());
                String educationType = PrintUtil.getEducationOrgUnitLabel(extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel(), GrammaCase.NOMINATIVE);

                RtfInjectModifier injectModifier = new RtfInjectModifier()
                        .put("commitDate", order.getCommitDate() != null ? RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(order.getCommitDate()) : "")
                        .put("orderNumber", order.getNumber())
                        .put("dateP", dateP != null ? RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(dateP) : "")
                        .put("numP", numP != null ? numP : "")
                        .put("parNumber", Integer.toString(paragraph.getNumber()))
                        .put("developForm", developFormTitle)
                        .put("developPeriod", developPeriodTitle)
                        .put("developCondition", developConditionTitle)
                        .put("dateZ", order.getEnrollmentDate() != null ? RussianDateFormatUtils.getDateFormattedWithMonthName(order.getEnrollmentDate()) : "")
                        .put("FIL", extract.getEntity().getEducationOrgUnit().getFormativeOrgUnit().getTitle())
                        .put("educationType_D", educationType);

                EnrollmentOrderNARFU enrollmentOrderNARFU = get(EnrollmentOrderNARFU.class, EnrollmentOrderNARFU.enrollmentOrder(), order);
                if (enrollmentOrderNARFU != null) {
                    injectModifier.put("Dolj", enrollmentOrderNARFU.getExecutorPost());
                    injectModifier.put("FIOR", enrollmentOrderNARFU.getExecutorFIO());
                }
                else {
                    injectModifier.put("Dolj", "");
                    injectModifier.put("FIOR", "");
                }

                tableModifier.put("T", tableData.toArray(new String[tableData.size()][]));

                tableModifier.modify(page);
                injectModifier.modify(page);

                resultDocument.getElementList().addAll(page.getElementList());
            }
        }
        return resultDocument;
    }

    @Override
    public RtfDocument getDownloadPrintParagraphExtracts(
            List<IAbstractParagraph<IAbstractOrder>> paragraphList)
    {

        UniecScriptItem tempHeader = get(UniecScriptItem.class, UniecScriptItem.CATALOG_ITEM_CODE,
                                         "studentPrintExtracts");

        RtfDocument resultDocument = RtfBean.getElementFactory().createRtfDocument();

        RtfDocument template = null;
        if (tempHeader != null) {
            template = new RtfReader().read(tempHeader.getTemplate());
            resultDocument.setHeader(template.getHeader());
            resultDocument.setSettings(template.getSettings());
        }

        for (IAbstractParagraph<IAbstractOrder> paragraph : paragraphList) {

            List<EnrollmentExtract> enrollmentExtractList = (List<EnrollmentExtract>) paragraph.getExtractList();
            if (enrollmentExtractList.size() == 0) {
                throw new ApplicationException("Пустой параграф №" + paragraph.getNumber() + ".");
            }
            boolean first = true;
            for (EnrollmentExtract extract : enrollmentExtractList) {

                RtfDocument page = getDownloadPrintExtract(extract);
                if (!first)
                    resultDocument.addElement(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
                else
                    first = false;
                resultDocument.getElementList().addAll(page.getElementList());

            }
        }
        return resultDocument;
    }

    @Override
    public RtfDocument getDownloadPrintParagraphPersonalInfo(
            List<IAbstractParagraph<IAbstractOrder>> paragraphList)
    {

        UniecScriptItem scriptItem = get(UniecScriptItem.class, UniecScriptItem.CATALOG_ITEM_CODE, "studentPersonalInfoCard");

        RtfDocument document = RtfBean.getElementFactory().createRtfDocument();

        RtfDocument template = null;
        if (scriptItem != null) {
            template = new RtfReader().read(scriptItem.getCurrentTemplate());
            document.setHeader(template.getHeader());
            document.setSettings(template.getSettings());
        }

        for (IAbstractParagraph paragraph : paragraphList) {
            EnrollmentOrder order = (EnrollmentOrder) paragraph.getOrder();
            List<EnrollmentExtract> enrollmentExtractList = paragraph.getExtractList();
            if (enrollmentExtractList.size() == 0) {
                throw new ApplicationException("Пустой параграф №" + paragraph.getNumber() + ".");
            }

            for (EnrollmentExtract extract : enrollmentExtractList) {

                RtfDocument page = template.getClone();

                PreliminaryEnrollmentStudent preStudent = extract.getEntity();
                EntrantRequest entrantRequest = preStudent.getRequestedEnrollmentDirection().getEntrantRequest();
                IdentityCard card = entrantRequest.getEntrant().getPerson().getIdentityCard();
                RequestedEnrollmentDirection direction = preStudent.getRequestedEnrollmentDirection();
                CompetitionKind compKind = direction.getCompetitionKind();

                StringBuilder met = new StringBuilder("");

                if (preStudent.getCompensationType().getCode().equals(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT))
                    met.append("Д ");

                List<PersonBenefit> benefits = getList(PersonBenefit.class, PersonBenefit.person(), entrantRequest.getEntrant().getPerson());
                if (!benefits.isEmpty()) {
                    for (PersonBenefit benefit : benefits) {
                        String title = benefit.getBenefit().getTitle();
                        if (title.contains("Дети-сироты"))
                            met.append("С ");
                        else if (title.contains("Дети-инвалиды") || title.contains("Инвалид 1 группы") || title.contains("Инвалид 2 группы"))
                            met.append("И ");
                        else
                            met.append("Л ");
                    }
                }

                if (!card.getCitizenship().getTitle().contains("Росс"))
                    met.append("Иг ");

                if (preStudent.getRequestedEnrollmentDirection().isTargetAdmission())
                    met.append("Ц ");

                if (!card.getCitizenship().getTitle().contains("Росс") && compKind.getCode().equals(UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES))
                    met.append("ИгЦ ");

                RtfInjectModifier injectModifier = new RtfInjectModifier()
                        .put("Met", met.toString())
                        .put("formP", extract.getEntity().getEducationOrgUnit().getFormativeOrgUnit().getTitle())
                        .put("fo", extract.getEntity().getEducationOrgUnit().getDevelopForm().getTitle())
                        .put("Bud", extract.getEntity().getRequestedEnrollmentDirection().getCompensationType().getShortTitle())
                        .put("NumA", entrantRequest.getStringNumber())
                        .put("F", card.getLastName())
                        .put("I", card.getFirstName())
                        .put("O", card.getMiddleName() != null ? card.getMiddleName() : "")
                        .put("dateN", entrantRequest.getRegDate() != null ? RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(entrantRequest.getRegDate()) : "");

                String levelTypeCode = extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().getCode();
                if (levelTypeCode.equals(StructureEducationLevelsCodes.HIGH_GOS2_GROUP_MASTER_PROFILE) ||
                        levelTypeCode.equals(StructureEducationLevelsCodes.HIGH_GOS2_GROUP_BACHELOR_PROFILE) ||
                        levelTypeCode.equals(StructureEducationLevelsCodes.ADDITIONAL_GROUP_TRAINING_PROFILE) ||
                        levelTypeCode.equals(StructureEducationLevelsCodes.HIGH_GOS3_GROUP_BACHELOR_PROFILE) ||
                        levelTypeCode.equals(StructureEducationLevelsCodes.HIGH_GOS3_GROUP_MASTER_PROFILE))
                {
                    injectModifier.put("Napr0", extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getParentLevel().getDisplayableTitle());
                    injectModifier.put("Napr", "\"" + extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getTitle() + "\"");
                }
                else {
                    injectModifier.put("Napr0", extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getDisplayableTitle());
                    injectModifier.put("Napr", "-");

                }

                injectModifier.modify(page);

                if (order.getParagraphList().indexOf(paragraph) != (order.getParagraphList().size() - 1))
                    page.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));

                document.getElementList().addAll(page.getElementList());
            }
        }

        return document == null ? RtfBean.getElementFactory().createRtfDocument() : document;
    }

    @Override
    public RtfDocument getDownloadPrintExtract(EnrollmentExtract extract) {

        UniecScriptItem tempHeader = get(UniecScriptItem.class, UniecScriptItem.CATALOG_ITEM_CODE,
                                         "studentPrintExtracts");
        RtfDocument template = new RtfReader().read(tempHeader.getCurrentTemplate());
        EnrollmentOrder order = extract.getOrder();
        PreliminaryEnrollmentStudent preStudent = extract.getEntity();
        EntrantRequest entrantRequest = preStudent.getRequestedEnrollmentDirection().getEntrantRequest();
        IdentityCard card = entrantRequest.getEntrant().getPerson().getIdentityCard();
        RequestedEnrollmentDirection direction = preStudent.getRequestedEnrollmentDirection();
        CompetitionKind compKind = direction.getCompetitionKind();
        String developFormTitle = extract.getEntity().getEducationOrgUnit().getDevelopForm().getTitle().toLowerCase();

        //Определяем используемы шаблон параграфа
        UniecScriptItem paragTemplate = new UniecScriptItem();

        if (order.getType().getCode().equals(UniecDefines.ORDER_TYPE_TARGET_BUDGET)) {
            if (compKind.getCode().equals(UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION) ||
                    compKind.getCode().equals(UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES))
                paragTemplate.setTemplatePath("narfu/templates/uniec/enrollmetExtracts/enrollmentExtractParagraph_targetBenefitBudget_narfu.rtf");
            else
                paragTemplate.setTemplatePath("narfu/templates/uniec/enrollmetExtracts/enrollmentExtractParagraph_targetBudget_narfu.rtf");
        }
        else if (order.getType().getCode().equals(UniecDefines.ORDER_TYPE_STUDENT_BUDGET)) {
            paragTemplate.setTemplatePath("narfu/templates/uniec/enrollmetExtracts/enrollmentExtractParagraph_studentBudget_narfu.rtf");
        }
        else if (order.getType().getCode().equals(UniecDefines.ORDER_TYPE_STUDENT_CONTRACT)) {
            if (preStudent.getRequestedEnrollmentDirection().getStudentCategory().getCode().equals(StudentCategoryCodes.STUDENT_CATEGORY_STUDENT) ||
                    preStudent.getRequestedEnrollmentDirection().getStudentCategory().getCode().equals(StudentCategoryCodes.STUDENT_CATEGORY_SECONDARY))
            {
                paragTemplate.setTemplatePath("narfu/templates/uniec/enrollmetExtracts/enrollmentExtractParagraph_studentContract_narfu.rtf");
            }
            else if (preStudent.getRequestedEnrollmentDirection().getStudentCategory().getCode().equals(StudentCategoryCodes.STUDENT_CATEGORY_LISTENER)) {
                paragTemplate.setTemplatePath("narfu/templates/uniec/enrollmetExtracts/enrollmentExtractParagraph_listenerContract_narfu.rtf");
            }
        }
        else if (order.getType().getCode().equals(UniecDefines.ORDER_TYPE_LISTENER_CONTRACT)) {
            paragTemplate.setTemplatePath("narfu/templates/uniec/enrollmetExtracts/enrollmentExtractParagraph_listenerContract_narfu.rtf");
            // Переопределенные нами типы приказа "О зачислении на 2 и последующие курсы (бюджет/договор)"
        }
        else if (order.getType().getCode().equals(UniecDefines.ORDER_TYPE_STUDENT_BUDGET_SHORT))
            paragTemplate.setTemplatePath("narfu/templates/uniec/enrollmetExtracts/enrollmentExtractParagraph_afterFirstCourseBudget_narfu.rtf");
        else if (order.getType().getCode().equals(UniecDefines.ORDER_TYPE_STUDENT_CONTRACT_SHORT))
            paragTemplate.setTemplatePath("narfu/templates/uniec/enrollmetExtracts/enrollmentExtractParagraph_afterFirstCourseContract_narfu.rtf");

        RtfDocument paragRtfTemp = new RtfReader().read(paragTemplate.getTemplate());

        RtfDocument page = template.getClone();

        IRtfElement paragraphs = UniRtfUtil.findElement(page.getElementList(), "PARAGRAPHS");

        page.getElementList().addAll(page.getElementList().indexOf(paragraphs), paragRtfTemp.getElementList());
        page.getElementList().remove(paragraphs);

        EnrollmentOrderNARFU orderNARFU = get(EnrollmentOrderNARFU.class, EnrollmentOrderNARFU.enrollmentOrder(), order);
        Date dateP = null;
        String numP = null;
        String courseTitle = "";
        if (orderNARFU != null) {
            dateP = orderNARFU.getProtocolDate();
            numP = orderNARFU.getProtocolNumber();
            if (orderNARFU.getCourse() != null)
                courseTitle = orderNARFU.getCourse().getTitle();
        }

        RtfTableModifier tableModifier = new RtfTableModifier();
        List<String[]> tableData = new ArrayList<String[]>();
        Double sumMark = UniecDAOFacade.getEntrantDAO().getSumFinalMarks(preStudent.getRequestedEnrollmentDirection());

        String[] row = new String[5];
        row[0] = "1";
        row[1] = card.getFullFio();
        row[2] = entrantRequest.getEntrant().getPersonalNumber();
        row[3] = DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS.format(sumMark);

        if (preStudent.getRequestedEnrollmentDirection().isTargetAdmission()) {
            TargetAdmissionKind targetAdmissionKind = preStudent.getRequestedEnrollmentDirection().getTargetAdmissionKind();
            row[4] = "Направление: " + targetAdmissionKind.getTitle();

        }
        else {
            List<PersonBenefit> benefits = getList(PersonBenefit.class, PersonBenefit.person(), entrantRequest.getEntrant().getPerson());
            if (!benefits.isEmpty()) {
                List<String> lgotNames = new ArrayList<String>();
                for (PersonBenefit benefit : benefits) {
                    lgotNames.add(benefit.getBenefit().getShortTitle());
                }
                row[4] = StringUtils.join(lgotNames, ",");
            }

        }
        tableData.add(row);

        String developPeriodTitle = extract.getEntity().getEducationOrgUnit().getDevelopPeriod().getTitle();
        String developConditionTitle = EnrollmentOrderPrintNARFU.getDevelopConditionTitle(extract.getEntity().getEducationOrgUnit().getDevelopCondition());
        String educationType = PrintUtil.getEducationOrgUnitLabel(extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel(), GrammaCase.NOMINATIVE);

        RtfInjectModifier injectModifier = new RtfInjectModifier()
                .put("commitDate", order.getCommitDate() != null ? RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(order.getCommitDate()) : "")
                .put("orderNumber", order.getNumber())
                .put("dateP", dateP != null ? RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(dateP) : "")
                .put("numP", numP != null ? numP : "")
                .put("parNumber", Integer.toString(extract.getParagraph().getNumber()))
                .put("developForm", developFormTitle)
                .put("developPeriod", developPeriodTitle)
                .put("developCondition", developConditionTitle)
                .put("course", courseTitle)
                .put("dateZ", order.getEnrollmentDate() != null ? RussianDateFormatUtils.getDateFormattedWithMonthName(order.getEnrollmentDate()) : "")
                .put("FIL", extract.getEntity().getEducationOrgUnit().getFormativeOrgUnit().getTitle())
                .put("educationType_D", educationType);

        EnrollmentOrderNARFU enrollmentOrderNARFU = get(EnrollmentOrderNARFU.class, EnrollmentOrderNARFU.enrollmentOrder(), order);
        if (enrollmentOrderNARFU != null) {
            injectModifier.put("Dolj", enrollmentOrderNARFU.getExecutorPost());
            injectModifier.put("FIOR", enrollmentOrderNARFU.getExecutorFIO());
        }
        else {
            injectModifier.put("Dolj", "");
            injectModifier.put("FIOR", "");
        }

        tableModifier.put("T", tableData.toArray(new String[tableData.size()][]));

        tableModifier.modify(page);
        injectModifier.modify(page);

        return page;
    }

    public static IEcOrderPrintDAO getInstance() {

        if (dao == null)
            dao = (IEcOrderPrintDAO) ApplicationRuntime.getBean("ecOrderPrintDAO");

        return dao;
    }

}
