package ru.tandemservice.narfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.narfu.entity.EppEduPlanVersionBlockLink;
import ru.tandemservice.narfu.entity.VersionBlockDisciplinesLink;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь дисциплин УП(в)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class VersionBlockDisciplinesLinkGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.narfu.entity.VersionBlockDisciplinesLink";
    public static final String ENTITY_NAME = "versionBlockDisciplinesLink";
    public static final int VERSION_HASH = -396870248;
    private static IEntityMeta ENTITY_META;

    public static final String L_VERSION_BLOCK_LINK = "versionBlockLink";
    public static final String L_CONTROL_ACTION_SRC = "controlActionSrc";
    public static final String L_CONTROL_ACTION_DST = "controlActionDst";

    private EppEduPlanVersionBlockLink _versionBlockLink;     // Связь блоков УП
    private EppRegistryElementPartFControlAction _controlActionSrc;     // Дисциплина
    private EppRegistryElementPartFControlAction _controlActionDst;     // Связана с

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Связь блоков УП. Свойство не может быть null.
     */
    @NotNull
    public EppEduPlanVersionBlockLink getVersionBlockLink()
    {
        return _versionBlockLink;
    }

    /**
     * @param versionBlockLink Связь блоков УП. Свойство не может быть null.
     */
    public void setVersionBlockLink(EppEduPlanVersionBlockLink versionBlockLink)
    {
        dirty(_versionBlockLink, versionBlockLink);
        _versionBlockLink = versionBlockLink;
    }

    /**
     * @return Дисциплина. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElementPartFControlAction getControlActionSrc()
    {
        return _controlActionSrc;
    }

    /**
     * @param controlActionSrc Дисциплина. Свойство не может быть null.
     */
    public void setControlActionSrc(EppRegistryElementPartFControlAction controlActionSrc)
    {
        dirty(_controlActionSrc, controlActionSrc);
        _controlActionSrc = controlActionSrc;
    }

    /**
     * @return Связана с. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElementPartFControlAction getControlActionDst()
    {
        return _controlActionDst;
    }

    /**
     * @param controlActionDst Связана с. Свойство не может быть null.
     */
    public void setControlActionDst(EppRegistryElementPartFControlAction controlActionDst)
    {
        dirty(_controlActionDst, controlActionDst);
        _controlActionDst = controlActionDst;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof VersionBlockDisciplinesLinkGen)
        {
            setVersionBlockLink(((VersionBlockDisciplinesLink)another).getVersionBlockLink());
            setControlActionSrc(((VersionBlockDisciplinesLink)another).getControlActionSrc());
            setControlActionDst(((VersionBlockDisciplinesLink)another).getControlActionDst());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends VersionBlockDisciplinesLinkGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) VersionBlockDisciplinesLink.class;
        }

        public T newInstance()
        {
            return (T) new VersionBlockDisciplinesLink();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "versionBlockLink":
                    return obj.getVersionBlockLink();
                case "controlActionSrc":
                    return obj.getControlActionSrc();
                case "controlActionDst":
                    return obj.getControlActionDst();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "versionBlockLink":
                    obj.setVersionBlockLink((EppEduPlanVersionBlockLink) value);
                    return;
                case "controlActionSrc":
                    obj.setControlActionSrc((EppRegistryElementPartFControlAction) value);
                    return;
                case "controlActionDst":
                    obj.setControlActionDst((EppRegistryElementPartFControlAction) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "versionBlockLink":
                        return true;
                case "controlActionSrc":
                        return true;
                case "controlActionDst":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "versionBlockLink":
                    return true;
                case "controlActionSrc":
                    return true;
                case "controlActionDst":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "versionBlockLink":
                    return EppEduPlanVersionBlockLink.class;
                case "controlActionSrc":
                    return EppRegistryElementPartFControlAction.class;
                case "controlActionDst":
                    return EppRegistryElementPartFControlAction.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<VersionBlockDisciplinesLink> _dslPath = new Path<VersionBlockDisciplinesLink>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "VersionBlockDisciplinesLink");
    }
            

    /**
     * @return Связь блоков УП. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.VersionBlockDisciplinesLink#getVersionBlockLink()
     */
    public static EppEduPlanVersionBlockLink.Path<EppEduPlanVersionBlockLink> versionBlockLink()
    {
        return _dslPath.versionBlockLink();
    }

    /**
     * @return Дисциплина. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.VersionBlockDisciplinesLink#getControlActionSrc()
     */
    public static EppRegistryElementPartFControlAction.Path<EppRegistryElementPartFControlAction> controlActionSrc()
    {
        return _dslPath.controlActionSrc();
    }

    /**
     * @return Связана с. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.VersionBlockDisciplinesLink#getControlActionDst()
     */
    public static EppRegistryElementPartFControlAction.Path<EppRegistryElementPartFControlAction> controlActionDst()
    {
        return _dslPath.controlActionDst();
    }

    public static class Path<E extends VersionBlockDisciplinesLink> extends EntityPath<E>
    {
        private EppEduPlanVersionBlockLink.Path<EppEduPlanVersionBlockLink> _versionBlockLink;
        private EppRegistryElementPartFControlAction.Path<EppRegistryElementPartFControlAction> _controlActionSrc;
        private EppRegistryElementPartFControlAction.Path<EppRegistryElementPartFControlAction> _controlActionDst;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Связь блоков УП. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.VersionBlockDisciplinesLink#getVersionBlockLink()
     */
        public EppEduPlanVersionBlockLink.Path<EppEduPlanVersionBlockLink> versionBlockLink()
        {
            if(_versionBlockLink == null )
                _versionBlockLink = new EppEduPlanVersionBlockLink.Path<EppEduPlanVersionBlockLink>(L_VERSION_BLOCK_LINK, this);
            return _versionBlockLink;
        }

    /**
     * @return Дисциплина. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.VersionBlockDisciplinesLink#getControlActionSrc()
     */
        public EppRegistryElementPartFControlAction.Path<EppRegistryElementPartFControlAction> controlActionSrc()
        {
            if(_controlActionSrc == null )
                _controlActionSrc = new EppRegistryElementPartFControlAction.Path<EppRegistryElementPartFControlAction>(L_CONTROL_ACTION_SRC, this);
            return _controlActionSrc;
        }

    /**
     * @return Связана с. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.VersionBlockDisciplinesLink#getControlActionDst()
     */
        public EppRegistryElementPartFControlAction.Path<EppRegistryElementPartFControlAction> controlActionDst()
        {
            if(_controlActionDst == null )
                _controlActionDst = new EppRegistryElementPartFControlAction.Path<EppRegistryElementPartFControlAction>(L_CONTROL_ACTION_DST, this);
            return _controlActionDst;
        }

        public Class getEntityClass()
        {
            return VersionBlockDisciplinesLink.class;
        }

        public String getEntityName()
        {
            return "versionBlockDisciplinesLink";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
