package ru.tandemservice.narfu.component.reports.StudentbookReport;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.base.bo.EducationYear.EducationYearManager;

import java.util.*;

public class ReportGenerator {

    private List<Student> studentList;
    private Date date;
    private IUniBaseDao dao;

    public ReportGenerator(List<Student> studentList, Date date) {
        this.studentList = studentList;
        this.date = date;
        this.dao = UniDaoFacade.getCoreDao();
    }

    public RtfDocument generate() {
        RtfDocument template = loadTemplate();
        RtfDocument result = template.getClone();
        result.getElementList().clear();

        Map<String, List<Student>> map = new HashMap<>();
        List<String> groupKeyList = new ArrayList<>();
        for (Student student : studentList) {
            String key = getKey(student);

            if (!groupKeyList.contains(key))
                groupKeyList.add(key);
            SafeMap.safeGet(map, key, ArrayList.class).add(student);
        }

        TopOrgUnit top = TopOrgUnit.getInstance();
        int counter = 0;
        for (String key : groupKeyList) {
            RtfDocument page = generatePage(template.getClone(), map.get(key), top, this.date);
            result.getElementList().addAll(page.getElementList());

            if (counter++ < groupKeyList.size())
                result.addElement(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
        }

        return result;
    }

    private RtfDocument generatePage(RtfDocument template, List<Student> studentList, TopOrgUnit top, Date date) {
        EducationOrgUnit eou = studentList.get(0).getEducationOrgUnit();
        int year = EducationYearManager.instance().dao().getCurrent().getIntValue();

        RtfInjectModifier im = new RtfInjectModifier();
        im.put("Uch", eou.getFormativeOrgUnit().getTitle());
        im.put("yr", "" + year + "-" + (year + 1));
        im.put("Form", eou.getDevelopForm().getTitle());
        im.put("Spec", eou.getEducationLevelHighSchool().getEducationLevel().getFullTitleWithRootLevel());
        im.put("DIR", getHead(eou.getFormativeOrgUnit()).getPerson().getIdentityCard().getIof());
        im.modify(template);

        List<String[]> list = new ArrayList<>();
        int counter = 0;
        for (Student st : studentList) {
            String[] arr = new String[4];
            arr[0] = "" + ++counter;
            arr[1] = st.getFio();
            arr[2] = st.getBookNumber();
            arr[3] = date != null ? RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(date) : "";

            list.add(arr);
        }

        RtfTableModifier tm = new RtfTableModifier();
        tm.put("T", list.toArray(new String[][]{}));
        tm.modify(template);

        return template;
    }

    private RtfDocument loadTemplate() {
        TemplateDocument templateDocument = dao.getCatalogItem(TemplateDocument.class, "studentbookReport");
        return new RtfReader().read(templateDocument.getContent());
    }

    private EmployeePost getHead(OrgUnit orgUnit) {
        EmployeePost ep = (EmployeePost) orgUnit.getHead();
        if (ep == null) {
            List<EmployeePost> list = EmployeeManager.instance().dao().getHeaderEmployeePostList(orgUnit);
            if (!list.isEmpty())
                ep = list.get(0);
        }

        return ep;
    }

    private String getKey(Student student) {
        StringBuilder sb = new StringBuilder()
                .append(student.getEducationOrgUnit().getDevelopForm().getTitle())
                .append(student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getFullTitle())
                .append(student.getCourse().getIntValue());
        return sb.toString();
    }
}
