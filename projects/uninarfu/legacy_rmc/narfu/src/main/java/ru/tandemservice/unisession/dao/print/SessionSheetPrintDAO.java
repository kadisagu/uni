package ru.tandemservice.unisession.dao.print;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.narfu.entity.catalog.codes.UnisessionCommonTemplateCodes;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unisession.entity.catalog.UnisessionCommonTemplate;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionSheetDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class SessionSheetPrintDAO extends UniBaseDao implements ru.tandemservice.unisession.print.ISessionSheetPrintDAO
{

    public static RtfString DEVELOP_FORM_INTERNAL;
    public static RtfString DEVELOP_FORM_INTERNAL_CORRESPONDENCE;
    public static RtfString DEVELOP_FORM_CORRESPONDENCE;

    public static RtfString FIRST_TRY;
    public static RtfString SECOND_TRY;
    public static RtfString COMISSION_TRY;


    @Override
    public RtfDocument printSheetList(Collection<Long> collection)
    {
        UnisessionCommonTemplate template = getCatalogItem(UnisessionCommonTemplate.class, UnisessionCommonTemplateCodes.NARFU_STUDENT_EXAM_SHEET);
//		TemplateDocument templateDocument = getCatalogItem(TemplateDocument.class, "narfuStudentExamSheet");

        RtfDocument rtfDocument = new RtfReader().read(template.getContent());

        RtfDocument resultRtf = rtfDocument.getClone();
        resultRtf.getElementList().clear();

        IRtfText pageBreak = RtfBean.getElementFactory().createRtfText("\n\\sect");
        pageBreak.setRaw(true);

        for (Long id : collection)
        {
            RtfDocument rtfPage = rtfDocument.getClone();

            SessionSheetDocument sheet = get(SessionSheetDocument.class, id);
            SessionDocumentSlot slot = get(SessionDocumentSlot.class, SessionDocumentSlot.document().id().s(), id);
            SessionMark mark = get(SessionMark.class, SessionMark.slot().document().id().s(), id);
            OrgUnit orgUnit = sheet.getGroupOu();

            RtfInjectModifier modifier = new RtfInjectModifier();


            //Подчеркивание
            String codeForm = slot.getActualStudent().getEducationOrgUnit().getDevelopForm().getCode();
            Integer tryNumber = slot.getTryNumber();

            prepareFormAndTry(codeForm, tryNumber);

            modifier.put("INTERNAL", DEVELOP_FORM_INTERNAL);
            modifier.put("INTERNAL_CORRESPONDENCE", DEVELOP_FORM_INTERNAL_CORRESPONDENCE);
            modifier.put("CORRESPONDENCE", DEVELOP_FORM_CORRESPONDENCE);

            modifier.put("firstTry", FIRST_TRY);
            modifier.put("secondTry", SECOND_TRY);
            modifier.put("comissionTry", COMISSION_TRY);

            modifier.put("Pod", orgUnit.getShortTitle());
            modifier.put("numZ", slot.getActualStudent().getBookNumber());

            modifier.put("FIO", slot.getActualStudent().getPerson().getFullFio());
            modifier.put("Grup", slot.getActualStudent().getGroup() != null ? slot.getActualStudent().getGroup().getTitle() : "");
            modifier.put("Kurs", slot.getActualStudent().getCourse().getTitle());

            modifier.put("Disc", slot.getStudentWpeCAction().getRegistryElementTitle());

            List<SessionComissionPps> commission = getList(SessionComissionPps.class, SessionComissionPps.L_COMMISSION, slot.getCommission());

            modifier.put("Prep", UniStringUtils.join(commission, SessionComissionPps.pps().person().identityCard().fio().s(), ", "));
            modifier.put("srok", sheet.getDeadlineDate() != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(sheet.getDeadlineDate()) : "");

            modifier.put("ball", mark != null ? mark.getValueItem().getPrintTitle() : "");
            modifier.put("date", mark != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(mark.getPerformDate()) : "");
            modifier.put("dateV", sheet.getIssueDate() != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(sheet.getIssueDate()) : "");
            modifier.put("sheetNumber", sheet.getNumber());

            modifier.put("term", slot.getStudentWpeCAction().getStudentWpe().getTerm().getTitle());
            modifier.put("user", UserContext.getInstance().getPrincipalContext().getFio());
            modifier.put("year", slot.getStudentWpeCAction().getStudentWpe().getYear().getEducationYear().getTitle());

            modifier.modify(rtfPage);

            rtfPage.getElementList().add(pageBreak);

            resultRtf.getElementList().addAll(rtfPage.getElementList());
        }


        RtfUtil.optimizeFontAndColorTable(resultRtf);

        return resultRtf;
    }

    private void prepareFormAndTry(String codeForm, Integer tryNumber)
    {
        //Форма обучения
        if (DevelopFormCodes.FULL_TIME_FORM.equals(codeForm))
            DEVELOP_FORM_INTERNAL = new RtfString().append("\\ulОчная\\ul0", true);
        else
            DEVELOP_FORM_INTERNAL = new RtfString().append("Очная");

        if (DevelopFormCodes.PART_TIME_FORM.equals(codeForm))
            DEVELOP_FORM_INTERNAL_CORRESPONDENCE = new RtfString().append("\\ulочно-заочная (вечерняя)\\ul0", true);
        else
            DEVELOP_FORM_INTERNAL_CORRESPONDENCE = new RtfString().append("очно-заочная (вечерняя)");

        if (DevelopFormCodes.CORESP_FORM.equals(codeForm))
            DEVELOP_FORM_CORRESPONDENCE = new RtfString().append("\\ulзаочная\\ul0", true);
        else
            DEVELOP_FORM_CORRESPONDENCE = new RtfString().append("заочная");

        //Количество попыток
        if (tryNumber.equals(1))
            FIRST_TRY = new RtfString().append("\\ulПервичный\\ul0", true);
        else
            FIRST_TRY = new RtfString().append("Первичный");

        if (tryNumber.equals(2))
            SECOND_TRY = new RtfString().append("\\ulповторный\\ul0", true);
        else
            SECOND_TRY = new RtfString().append("повторный");

        if (tryNumber > 2)
            COMISSION_TRY = new RtfString().append("\\ulкомиссия\\ul0", true);
        else
            COMISSION_TRY = new RtfString().append("комиссия");
    }

    @Override
    public RtfDocument printSheet(Long long1)
    {
        return printSheetList(Collections.singleton(long1));
    }


}
