package ru.tandemservice.narfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.narfu.entity.PersonEduInstitutionNARFU;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Законченное учебное заведение (расширение САФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PersonEduInstitutionNARFUGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.narfu.entity.PersonEduInstitutionNARFU";
    public static final String ENTITY_NAME = "personEduInstitutionNARFU";
    public static final int VERSION_HASH = -783612195;
    private static IEntityMeta ENTITY_META;

    public static final String L_BASE = "base";
    public static final String P_DATE_ISSUE_CERTIFICATE = "dateIssueCertificate";
    public static final String P_EXPECTED_DATE_RETURN = "expectedDateReturn";
    public static final String P_ACTUAL_DATE_RETURN = "actualDateReturn";

    private PersonEduInstitution _base;     // Документ о полученном образовании (базовый)
    private Date _dateIssueCertificate;     // Дата выдачи аттестата из ЛД
    private Date _expectedDateReturn;     // Планируемая дата возврата в ЛД
    private Date _actualDateReturn;     // Планируемая дата возврата в ЛД

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Документ о полученном образовании (базовый). Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public PersonEduInstitution getBase()
    {
        return _base;
    }

    /**
     * @param base Документ о полученном образовании (базовый). Свойство не может быть null и должно быть уникальным.
     */
    public void setBase(PersonEduInstitution base)
    {
        dirty(_base, base);
        _base = base;
    }

    /**
     * @return Дата выдачи аттестата из ЛД.
     */
    public Date getDateIssueCertificate()
    {
        return _dateIssueCertificate;
    }

    /**
     * @param dateIssueCertificate Дата выдачи аттестата из ЛД.
     */
    public void setDateIssueCertificate(Date dateIssueCertificate)
    {
        dirty(_dateIssueCertificate, dateIssueCertificate);
        _dateIssueCertificate = dateIssueCertificate;
    }

    /**
     * @return Планируемая дата возврата в ЛД.
     */
    public Date getExpectedDateReturn()
    {
        return _expectedDateReturn;
    }

    /**
     * @param expectedDateReturn Планируемая дата возврата в ЛД.
     */
    public void setExpectedDateReturn(Date expectedDateReturn)
    {
        dirty(_expectedDateReturn, expectedDateReturn);
        _expectedDateReturn = expectedDateReturn;
    }

    /**
     * @return Планируемая дата возврата в ЛД.
     */
    public Date getActualDateReturn()
    {
        return _actualDateReturn;
    }

    /**
     * @param actualDateReturn Планируемая дата возврата в ЛД.
     */
    public void setActualDateReturn(Date actualDateReturn)
    {
        dirty(_actualDateReturn, actualDateReturn);
        _actualDateReturn = actualDateReturn;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PersonEduInstitutionNARFUGen)
        {
            setBase(((PersonEduInstitutionNARFU)another).getBase());
            setDateIssueCertificate(((PersonEduInstitutionNARFU)another).getDateIssueCertificate());
            setExpectedDateReturn(((PersonEduInstitutionNARFU)another).getExpectedDateReturn());
            setActualDateReturn(((PersonEduInstitutionNARFU)another).getActualDateReturn());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PersonEduInstitutionNARFUGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PersonEduInstitutionNARFU.class;
        }

        public T newInstance()
        {
            return (T) new PersonEduInstitutionNARFU();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "base":
                    return obj.getBase();
                case "dateIssueCertificate":
                    return obj.getDateIssueCertificate();
                case "expectedDateReturn":
                    return obj.getExpectedDateReturn();
                case "actualDateReturn":
                    return obj.getActualDateReturn();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "base":
                    obj.setBase((PersonEduInstitution) value);
                    return;
                case "dateIssueCertificate":
                    obj.setDateIssueCertificate((Date) value);
                    return;
                case "expectedDateReturn":
                    obj.setExpectedDateReturn((Date) value);
                    return;
                case "actualDateReturn":
                    obj.setActualDateReturn((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "base":
                        return true;
                case "dateIssueCertificate":
                        return true;
                case "expectedDateReturn":
                        return true;
                case "actualDateReturn":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "base":
                    return true;
                case "dateIssueCertificate":
                    return true;
                case "expectedDateReturn":
                    return true;
                case "actualDateReturn":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "base":
                    return PersonEduInstitution.class;
                case "dateIssueCertificate":
                    return Date.class;
                case "expectedDateReturn":
                    return Date.class;
                case "actualDateReturn":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PersonEduInstitutionNARFU> _dslPath = new Path<PersonEduInstitutionNARFU>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PersonEduInstitutionNARFU");
    }
            

    /**
     * @return Документ о полученном образовании (базовый). Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.narfu.entity.PersonEduInstitutionNARFU#getBase()
     */
    public static PersonEduInstitution.Path<PersonEduInstitution> base()
    {
        return _dslPath.base();
    }

    /**
     * @return Дата выдачи аттестата из ЛД.
     * @see ru.tandemservice.narfu.entity.PersonEduInstitutionNARFU#getDateIssueCertificate()
     */
    public static PropertyPath<Date> dateIssueCertificate()
    {
        return _dslPath.dateIssueCertificate();
    }

    /**
     * @return Планируемая дата возврата в ЛД.
     * @see ru.tandemservice.narfu.entity.PersonEduInstitutionNARFU#getExpectedDateReturn()
     */
    public static PropertyPath<Date> expectedDateReturn()
    {
        return _dslPath.expectedDateReturn();
    }

    /**
     * @return Планируемая дата возврата в ЛД.
     * @see ru.tandemservice.narfu.entity.PersonEduInstitutionNARFU#getActualDateReturn()
     */
    public static PropertyPath<Date> actualDateReturn()
    {
        return _dslPath.actualDateReturn();
    }

    public static class Path<E extends PersonEduInstitutionNARFU> extends EntityPath<E>
    {
        private PersonEduInstitution.Path<PersonEduInstitution> _base;
        private PropertyPath<Date> _dateIssueCertificate;
        private PropertyPath<Date> _expectedDateReturn;
        private PropertyPath<Date> _actualDateReturn;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Документ о полученном образовании (базовый). Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.narfu.entity.PersonEduInstitutionNARFU#getBase()
     */
        public PersonEduInstitution.Path<PersonEduInstitution> base()
        {
            if(_base == null )
                _base = new PersonEduInstitution.Path<PersonEduInstitution>(L_BASE, this);
            return _base;
        }

    /**
     * @return Дата выдачи аттестата из ЛД.
     * @see ru.tandemservice.narfu.entity.PersonEduInstitutionNARFU#getDateIssueCertificate()
     */
        public PropertyPath<Date> dateIssueCertificate()
        {
            if(_dateIssueCertificate == null )
                _dateIssueCertificate = new PropertyPath<Date>(PersonEduInstitutionNARFUGen.P_DATE_ISSUE_CERTIFICATE, this);
            return _dateIssueCertificate;
        }

    /**
     * @return Планируемая дата возврата в ЛД.
     * @see ru.tandemservice.narfu.entity.PersonEduInstitutionNARFU#getExpectedDateReturn()
     */
        public PropertyPath<Date> expectedDateReturn()
        {
            if(_expectedDateReturn == null )
                _expectedDateReturn = new PropertyPath<Date>(PersonEduInstitutionNARFUGen.P_EXPECTED_DATE_RETURN, this);
            return _expectedDateReturn;
        }

    /**
     * @return Планируемая дата возврата в ЛД.
     * @see ru.tandemservice.narfu.entity.PersonEduInstitutionNARFU#getActualDateReturn()
     */
        public PropertyPath<Date> actualDateReturn()
        {
            if(_actualDateReturn == null )
                _actualDateReturn = new PropertyPath<Date>(PersonEduInstitutionNARFUGen.P_ACTUAL_DATE_RETURN, this);
            return _actualDateReturn;
        }

        public Class getEntityClass()
        {
            return PersonEduInstitutionNARFU.class;
        }

        public String getEntityName()
        {
            return "personEduInstitutionNARFU";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
