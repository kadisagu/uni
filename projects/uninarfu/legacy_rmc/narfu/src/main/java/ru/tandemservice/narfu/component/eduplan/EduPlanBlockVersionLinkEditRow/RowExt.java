package ru.tandemservice.narfu.component.eduplan.EduPlanBlockVersionLinkEditRow;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;

public class RowExt extends ViewWrapper<IEntity> {
    private final RowExt parent;

    @Override
    public RowExt getHierarhyParent() {
        return this.parent;
    }

    public RowExt(final IEntity i, final RowExt parent) {
        super(i);
        this.parent = parent;

    }
}
