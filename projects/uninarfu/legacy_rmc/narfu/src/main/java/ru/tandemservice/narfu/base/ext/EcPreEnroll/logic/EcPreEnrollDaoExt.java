package ru.tandemservice.narfu.base.ext.EcPreEnroll.logic;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.narfu.uniec.dao.IEntrantDAO;
import ru.tandemservice.uniecrmc.base.ext.EcDistribution.ui.util.EcDistributionUtil;
import ru.tandemservice.uniecrmc.base.ext.EcPreEnroll.logic.EcPreEnrollExtDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

public class EcPreEnrollDaoExt extends EcPreEnrollExtDao {

    @Override
    public PreliminaryEnrollmentStudent doChangeOrderType(
            Long requestedEnrollmentDirectionId,
            EducationOrgUnit educationOrgUnit,
            EntrantEnrollmentOrderType orderType)
    {
        PreliminaryEnrollmentStudent preliminaryEnrollmentStudent = new PreliminaryEnrollmentStudent();
        RequestedEnrollmentDirection direction = (RequestedEnrollmentDirection) getNotNull(requestedEnrollmentDirectionId);

        //Если приказы о зачислении на 2+ курс, то измененная логика проверки студента
        if (orderType != null && ((UniecDefines.ORDER_TYPE_STUDENT_BUDGET_SHORT.equals(orderType.getCode())) ||
                (UniecDefines.ORDER_TYPE_STUDENT_CONTRACT_SHORT.equals(orderType.getCode()))))
        {

            preliminaryEnrollmentStudent = get(PreliminaryEnrollmentStudent.class, PreliminaryEnrollmentStudent.requestedEnrollmentDirection(), direction);
            if (preliminaryEnrollmentStudent != null) {
                if (preliminaryEnrollmentStudent.getEntrantEnrollmentOrderType().equals(orderType)) {
                    if (!EcDistributionUtil.isUsePriority(direction.getEnrollmentDirection().getEnrollmentCampaign()))
                        createOrUpdatePreStudentExt(preliminaryEnrollmentStudent, direction);
                    return preliminaryEnrollmentStudent;
                }

                getSession().delete(preliminaryEnrollmentStudent);
            }

            preliminaryEnrollmentStudent = new PreliminaryEnrollmentStudent();
            preliminaryEnrollmentStudent.setRequestedEnrollmentDirection(direction);
            preliminaryEnrollmentStudent.setEducationOrgUnit(educationOrgUnit);
            preliminaryEnrollmentStudent.setEntrantEnrollmentOrderType(orderType);
            preliminaryEnrollmentStudent.setStudiedOPP(direction.isStudiedOPP());
            preliminaryEnrollmentStudent.setCompensationType(orderType.getCompensationType());
            preliminaryEnrollmentStudent.setTargetAdmission((orderType.isTargetAdmission()) || (direction.isTargetAdmission()));
            preliminaryEnrollmentStudent.setStudentCategory(direction.getStudentCategory());

            IEntrantDAO dao = (IEntrantDAO) UniecDAOFacade.getEntrantDAO();
            CoreCollectionUtils.Pair<String, PreliminaryEnrollmentStudent> result = dao.saveOrUpdatePreliminaryStudentExt(preliminaryEnrollmentStudent);
            String error = result.getX();
            if (null != error) {
                throw new ApplicationException(error);
            }

            if (!EcDistributionUtil.isUsePriority(direction.getEnrollmentDirection().getEnrollmentCampaign()))
                createOrUpdatePreStudentExt(preliminaryEnrollmentStudent, direction);

            return result.getY();
        }
        else {

            preliminaryEnrollmentStudent = super.doChangeOrderType(
                    requestedEnrollmentDirectionId,
                    educationOrgUnit,
                    orderType);


            if (preliminaryEnrollmentStudent != null && orderType != null && UniecDefines.ORDER_TYPE_SECOND_HIGH.equals(orderType.getCode())) {
                preliminaryEnrollmentStudent.setCompensationType(direction.getCompensationType());
                preliminaryEnrollmentStudent.setStudentCategory(direction.getStudentCategory());
                preliminaryEnrollmentStudent.setTargetAdmission(direction.isTargetAdmission());

                getSession().saveOrUpdate(preliminaryEnrollmentStudent);
            }


            return preliminaryEnrollmentStudent;
        }


//        if (preliminaryEnrollmentStudent != null)
//            // Переопределенные нами типы приказа "О зачислении на 2 и последующие курсы cтудентов/ слушателей (бюджет/договор)"
//            if ((UniecDefines.ORDER_TYPE_STUDENT_BUDGET_SHORT.equals(orderType.getCode()))
//                    || (UniecDefines.ORDER_TYPE_STUDENT_CONTRACT_SHORT.equals(orderType.getCode()))) {
//                RequestedEnrollmentDirection direction = (RequestedEnrollmentDirection) getNotNull(requestedEnrollmentDirectionId);
//                preliminaryEnrollmentStudent.setStudentCategory(direction.getStudentCategory());
//            }
//
//        return preliminaryEnrollmentStudent;
    }
}
