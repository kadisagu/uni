package ru.tandemservice.narfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.narfu.entity.NarfuReportStudentsByUGS;
import ru.tandemservice.uni.entity.report.StorableReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Отчет «Контингент студентов по укрупненным группам»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class NarfuReportStudentsByUGSGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.narfu.entity.NarfuReportStudentsByUGS";
    public static final String ENTITY_NAME = "narfuReportStudentsByUGS";
    public static final int VERSION_HASH = -1831042567;
    private static IEntityMeta ENTITY_META;

    public static final String P_TITLE = "title";
    public static final String P_NUMBER = "number";
    public static final String P_ACTIVE_STATUS = "activeStatus";
    public static final String P_STUDENT_STATUS = "studentStatus";

    private String _title;     // Название отчета
    private Integer _number;     // Номер отчета
    private String _activeStatus;     // Считать акивными
    private String _studentStatus;     // Статусы студентов

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Название отчета.
     */
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название отчета.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Номер отчета.
     */
    public Integer getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер отчета.
     */
    public void setNumber(Integer number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Считать акивными.
     */
    @Length(max=255)
    public String getActiveStatus()
    {
        return _activeStatus;
    }

    /**
     * @param activeStatus Считать акивными.
     */
    public void setActiveStatus(String activeStatus)
    {
        dirty(_activeStatus, activeStatus);
        _activeStatus = activeStatus;
    }

    /**
     * @return Статусы студентов.
     */
    @Length(max=255)
    public String getStudentStatus()
    {
        return _studentStatus;
    }

    /**
     * @param studentStatus Статусы студентов.
     */
    public void setStudentStatus(String studentStatus)
    {
        dirty(_studentStatus, studentStatus);
        _studentStatus = studentStatus;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof NarfuReportStudentsByUGSGen)
        {
            setTitle(((NarfuReportStudentsByUGS)another).getTitle());
            setNumber(((NarfuReportStudentsByUGS)another).getNumber());
            setActiveStatus(((NarfuReportStudentsByUGS)another).getActiveStatus());
            setStudentStatus(((NarfuReportStudentsByUGS)another).getStudentStatus());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends NarfuReportStudentsByUGSGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) NarfuReportStudentsByUGS.class;
        }

        public T newInstance()
        {
            return (T) new NarfuReportStudentsByUGS();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "title":
                    return obj.getTitle();
                case "number":
                    return obj.getNumber();
                case "activeStatus":
                    return obj.getActiveStatus();
                case "studentStatus":
                    return obj.getStudentStatus();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "activeStatus":
                    obj.setActiveStatus((String) value);
                    return;
                case "studentStatus":
                    obj.setStudentStatus((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "title":
                        return true;
                case "number":
                        return true;
                case "activeStatus":
                        return true;
                case "studentStatus":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "title":
                    return true;
                case "number":
                    return true;
                case "activeStatus":
                    return true;
                case "studentStatus":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "title":
                    return String.class;
                case "number":
                    return Integer.class;
                case "activeStatus":
                    return String.class;
                case "studentStatus":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<NarfuReportStudentsByUGS> _dslPath = new Path<NarfuReportStudentsByUGS>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "NarfuReportStudentsByUGS");
    }
            

    /**
     * @return Название отчета.
     * @see ru.tandemservice.narfu.entity.NarfuReportStudentsByUGS#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Номер отчета.
     * @see ru.tandemservice.narfu.entity.NarfuReportStudentsByUGS#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Считать акивными.
     * @see ru.tandemservice.narfu.entity.NarfuReportStudentsByUGS#getActiveStatus()
     */
    public static PropertyPath<String> activeStatus()
    {
        return _dslPath.activeStatus();
    }

    /**
     * @return Статусы студентов.
     * @see ru.tandemservice.narfu.entity.NarfuReportStudentsByUGS#getStudentStatus()
     */
    public static PropertyPath<String> studentStatus()
    {
        return _dslPath.studentStatus();
    }

    public static class Path<E extends NarfuReportStudentsByUGS> extends StorableReport.Path<E>
    {
        private PropertyPath<String> _title;
        private PropertyPath<Integer> _number;
        private PropertyPath<String> _activeStatus;
        private PropertyPath<String> _studentStatus;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Название отчета.
     * @see ru.tandemservice.narfu.entity.NarfuReportStudentsByUGS#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(NarfuReportStudentsByUGSGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Номер отчета.
     * @see ru.tandemservice.narfu.entity.NarfuReportStudentsByUGS#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(NarfuReportStudentsByUGSGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Считать акивными.
     * @see ru.tandemservice.narfu.entity.NarfuReportStudentsByUGS#getActiveStatus()
     */
        public PropertyPath<String> activeStatus()
        {
            if(_activeStatus == null )
                _activeStatus = new PropertyPath<String>(NarfuReportStudentsByUGSGen.P_ACTIVE_STATUS, this);
            return _activeStatus;
        }

    /**
     * @return Статусы студентов.
     * @see ru.tandemservice.narfu.entity.NarfuReportStudentsByUGS#getStudentStatus()
     */
        public PropertyPath<String> studentStatus()
        {
            if(_studentStatus == null )
                _studentStatus = new PropertyPath<String>(NarfuReportStudentsByUGSGen.P_STUDENT_STATUS, this);
            return _studentStatus;
        }

        public Class getEntityClass()
        {
            return NarfuReportStudentsByUGS.class;
        }

        public String getEntityName()
        {
            return "narfuReportStudentsByUGS";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
