package ru.tandemservice.narfu.base.ext.ReportPersonMilitary.ui.Add.print;


import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.fias.base.entity.Address;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportPrintInfo;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.ReportDQL;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.ReportPrintColumn;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.print.ReportPrintPathColumn;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.militaryrmc.base.bo.ReportPersonMilitary.ui.Add.ReportPersonMilitaryAddUI;
import ru.tandemservice.movestudentrmc.entity.PersonNARFU;

public class PersonMilitaryPrintBlock extends ru.tandemservice.militaryrmc.base.bo.ReportPersonMilitary.ui.Add.print.personMilitary.PersonMilitaryPrintBlock
{
    private static final DateFormatter df = DateFormatter.DEFAULT_DATE_FORMATTER;

    @Override
    public void modifyPersonData(ReportDQL dql, IReportPrintInfo printInfo)
    {
        // домашний телефон
        if (getHomePhoneNumber().isActive())
        {
            dql.leftJoinPath(Person.address());
            int contactIndex = dql.addLastAggregateColumn(Person.contactData().phoneFact());
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("homePhoneNumber", contactIndex, null));
        }
        // мобильный телефон
        if (getMobilePhoneNumber().isActive())
        {
            dql.leftJoinPath(Person.address());
            int contactIndex = dql.addLastAggregateColumn(Person.contactData().phoneMobile());
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("mobilePhoneNumber", contactIndex, null));
        }

        // служебный телефон
        if (getWorkPhoneNumber().isActive())
        {
            dql.leftJoinPath(Person.address());
            int contactIndex = dql.addLastAggregateColumn(Person.contactData().phoneWork());
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("workPhoneNumber", contactIndex, null));
        }

        // адрес регистрации
        if (getRegistrationAddress().isActive())
        {
            dql.leftJoinPath(Person.identityCard().address());
            int contactIndex = dql.addLastAggregateColumn(Person.identityCard().address());
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("registrationAddress", contactIndex, Address.titleWithFlat()));
        }

        modifyPersonNarfuData(dql, printInfo);

        // Адрес временной регистрации
        if (getTempRegistrationAddress().isActive())
        {
            String personAlias = dql.leftJoinEntity(PersonNARFU.class, PersonNARFU.person());
            final int index = dql.addLastAggregateColumn(personAlias);
            //printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("tempRegistrationAddress", index, PersonNARFU.addressTempRegistration().titleWithFlat()));
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintColumn("tempRegistrationAddress")
            {
                @Override
                public Object createColumnValue(Object[] objects)
                {
                    PersonNARFU object = (PersonNARFU) objects[index];
                    if (object == null)
                    {
                        return null;
                    }

                    Address t = object.getAddressTempRegistration();

                    if (t == null)
                    {
                        return "";
                    }
                    if (object.getAddressTempRegistrationStartDate() != null && object.getAddressTempRegistrationEndDate() != null)
                    {
                        return t.getTitleWithFlat() + String.format(" (c %s по %s)", df.format(object.getAddressTempRegistrationStartDate()), df.format(object.getAddressTempRegistrationEndDate()));
                    } else if (object.getAddressTempRegistrationStartDate() != null)
                    {
                        return t.getTitleWithFlat() + String.format(" (c %s)", df.format(object.getAddressTempRegistrationStartDate()));
                    } else if (object.getAddressTempRegistrationEndDate() != null)
                    {
                        return t.getTitleWithFlat() + String.format(" (по %s)", df.format(object.getAddressTempRegistrationEndDate()));
                    }

                    return t.getTitleWithFlat();
                }
            });
        }

        // фактический адрес
        if (getActualAddress().isActive())
        {
            dql.leftJoinPath(Person.address());
            int contactIndex = dql.addLastAggregateColumn(Person.address());
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("actualAddress", contactIndex, Address.titleWithFlat()));
        }

        // Иностранное гражданство (Если гражданство = Россия, то не заполнять)
        if (getCitizenship().isActive())
        {
            final int contactIndex = dql.addLastAggregateColumn(Person.identityCard().citizenship());
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintColumn("citizenship")
            {
                public Object createColumnValue(Object[] data)
                {
                    AddressCountry country = (AddressCountry) data[contactIndex];
                    return (country != null) && (country.getCode() != 0) ? country.getTitle() : "";
                }
            });
        }
    }
}
