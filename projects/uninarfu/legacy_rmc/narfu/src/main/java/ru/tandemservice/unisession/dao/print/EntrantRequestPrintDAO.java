package ru.tandemservice.unisession.dao.print;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import org.tandemframework.shared.person.catalog.entity.EducationLevelStage;
import ru.tandemservice.aspirantrmc.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.eventssecrmc.dao.DaoFacade;
import ru.tandemservice.movestudentrmc.entity.PersonNARFU;
import ru.tandemservice.narfu.base.ext.Person.ui.Tab.PersonNarfuUtil;
import ru.tandemservice.narfu.component.entrant.StateExamUtil;
import ru.tandemservice.narfu.entity.EnrCampaignDevelopFormDates;
import ru.tandemservice.narfu.entity.EntrantStateExamCertificateExt;
import ru.tandemservice.unibasermc.util.DeclinationUtil;
import ru.tandemservice.uniecaspirantrmc.component.entrant.EntrantUtils;
import ru.tandemservice.uniecaspirantrmc.entity.EntrantAspirantData;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.ExamPassDisciplineISTU;
import ru.tandemservice.uniecrmc.entity.entrant.Entrant2IndividualAchievements;
import ru.tandemservice.uniecrmc.entity.entrant.Entrant2SpecialConditions;
import ru.tandemservice.uniecrmc.entity.entrant.EntrantExt;
import ru.tandemservice.unirmc.util.AddressUtil;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.catalog.EnrollmentDocument;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.EntrantDataUtil.DetailLevel;

import java.util.*;

public class EntrantRequestPrintDAO extends UniBaseDao implements
        IEntrantRequestPrintDAO
{

    private static Set<String> ASPIRANT_CODES = Collections.unmodifiableSet(new LinkedHashSet<String>(Arrays.asList(StudentCategoryCodes.STUDENT_CATEGORY_ASPIRANT, StudentCategoryCodes.STUDENT_CATEGORY_APPLICANT, "5", "6")));

    private List<String> removeTags = new ArrayList<String>();

    public static IEntrantRequestPrintDAO instance() {
        return (IEntrantRequestPrintDAO) ApplicationRuntime
                .getBean("entrantRequestPrintDAO");
    }

    @Override
    public RtfDocument printEntrantRequest(Long requestId) {
        EntrantRequest entrantRequest = get(EntrantRequest.class, requestId);

        List<RequestedEnrollmentDirection> listOfEnrollmentDirectios = getList(
                RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.entrantRequest(),
                entrantRequest, new String[]{RequestedEnrollmentDirection.P_PRIORITY});

        for (RequestedEnrollmentDirection enrollmentDirection : listOfEnrollmentDirectios) {
            if (ASPIRANT_CODES.contains(enrollmentDirection.getStudentCategory().getCode())) {
                RtfDocument doc = printEntrantRequestAspirant(entrantRequest, listOfEnrollmentDirectios);
                return doc;
            }
        }

        RequestedEnrollmentDirection requestedEnrollmentDirection = listOfEnrollmentDirectios.get(0);

        boolean isSPO = requestedEnrollmentDirection.getEnrollmentDirection()
                .getEducationOrgUnit().getEducationLevelHighSchool()
                .getEducationLevel().getLevelType().isMiddle();

        TemplateDocument template = null;

        if (isSPO)
            template = getCatalogItem(TemplateDocument.class,
                                      "narfuEntrantRequestSpo");
        else
            template = getCatalogItem(TemplateDocument.class,
                                      "narfuEntrantRequestVpo");

        if (template.getContent() == null) {
            ContextLocal.getErrorCollector().addError("Шаблон не загружен!");
            return null;
        }

        RtfDocument document = new RtfReader().read(template.getContent());

        Entrant entrant = entrantRequest.getEntrant();

        boolean male = entrant.getPerson().isMale();

        if (!removeTags.isEmpty()) removeTags.clear();

        RtfInjectModifier injectModifier = new RtfInjectModifier();

        injectModifier.put("FIO_G", DeclinationUtil.getDeclinableFIO(GrammaCase.GENITIVE, entrant.getPerson().getIdentityCard()));

        injectModifier.put("FIO", entrant.getPerson().getIdentityCard().getFullFio());
        injectModifier.put("registr", male ? "зарегистрированного" : "зарегистрированной");
        injectModifier.put("AdrR", AddressUtil.isSettlement(entrant.getPerson().getAddressRegistration()) ? entrant.getPerson().getAddressRegistration().getTitleWithFlat() : "-");

        injectModifier.put("text", isSPO ?
                "Прошу рассмотреть возможность моего зачисления на следующие направления подготовки/специальности"
                : "Прошу допустить меня к сдаче вступительных испытаний и участию в конкурсе на следующие направления/специальности");

        injectModifier.put("Pass", entrant.getPerson().getIdentityCard().getShortTitle());
        injectModifier.put("VidPass", entrant.getPerson().getIdentityCard().getIssuancePlace() == null ? "-" : entrant.getPerson().getIdentityCard().getIssuancePlace());
        injectModifier.put("datePass", entrant.getPerson().getIdentityCard().getIssuanceDate() != null ? RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(entrant.getPerson().getIdentityCard().getIssuanceDate()) : "-");

        injectModifier.put("Graj", entrant.getPerson().getIdentityCard().getCitizenship().getTitle());
        injectModifier.put("Sex", entrant.getPerson().getIdentityCard().getSex().getTitle());
        injectModifier.put("dateR", entrant.getPerson().getIdentityCard().getBirthDate() != null ? RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT.format(entrant.getPerson().getIdentityCard().getBirthDate()) + " года" : "-");
        injectModifier.put("placeR", entrant.getPerson().getIdentityCard().getBirthPlace() == null ? "-" : entrant.getPerson().getIdentityCard().getBirthPlace());

        injectModifier.put("telD", (entrant.getPerson().getContactData() != null && entrant.getPerson().getContactData().getPhoneFact() != null) ? entrant.getPerson().getContactData().getPhoneFact() : "нет");
        injectModifier.put("telK", (entrant.getPerson().getContactData() != null && entrant.getPerson().getContactData().getPhoneMobile() != null) ? entrant.getPerson().getContactData().getPhoneMobile() : "нет");
        injectModifier.put("mail", (entrant.getPerson().getContactData() != null && entrant.getPerson().getContactData().getEmail() != null) ? entrant.getPerson().getContactData().getEmail() : "нет");
        injectModifier.put("mail", (entrant.getPerson().getContactData() != null && entrant.getPerson().getContactData().getEmail() != null) ? entrant.getPerson().getContactData().getEmail() : "нет");
        List<EntrantEnrolmentRecommendation> recommendations = getList(EntrantEnrolmentRecommendation.class, EntrantEnrolmentRecommendation.entrant().s(), entrant);
        injectModifier.put("superior", CollectionUtils.isEmpty(recommendations) ? "нет" : "да");
        List<Entrant2IndividualAchievements> achievements = getList(Entrant2IndividualAchievements.class, Entrant2IndividualAchievements.entrant().s(), entrant);
        injectModifier.put("individualAchievements", CollectionUtils.isEmpty(achievements) ? "нет" : "да");
        List<Entrant2SpecialConditions> specialConditions = getList(Entrant2SpecialConditions.class, Entrant2SpecialConditions.entrant().s(), entrant);
        injectModifier.put("specialConditions", CollectionUtils.isEmpty(specialConditions) ? "нет" : "да");
        //Контактные лица (ближайшие родственники)
        injectPersonNextOfKinInfo(injectModifier, entrant);

        //Документ об образовании
        try {
            injectModifier.put("urO", entrant.getPerson().getPersonEduInstitution().getEducationLevel().getShortTitle().toLowerCase());
        }
        catch (Exception e) {
            injectModifier.put("urO", "-");
        }

        try {
            injectModifier.put("docO", entrant.getPerson().getPersonEduInstitution().getDocumentType().getTitle().toLowerCase());
        }
        catch (Exception e) {
            injectModifier.put("docO", "-");
        }
        try {
            injectModifier.put("serO", entrant.getPerson().getPersonEduInstitution().getSeria());
        }
        catch (Exception e) {
            injectModifier.put("serO", "-");
        }
        try {
            injectModifier.put("kodO", entrant.getPerson().getPersonEduInstitution().getRegionCode());
        }
        catch (Exception e) {
            injectModifier.put("kodO", "-");
        }

        try {
            injectModifier.put("numO", entrant.getPerson().getPersonEduInstitution().getNumber());
        }
        catch (Exception e) {
            injectModifier.put("numO", "-");
        }
        try {
            injectModifier.put("yearO", String.valueOf(entrant.getPerson().getPersonEduInstitution().getYearEnd()));
        }
        catch (Exception e) {
            injectModifier.put("yearO", "-");
        }
        try {
            injectModifier.put("dateO", RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT.format(entrant.getPerson().getPersonEduInstitution().getIssuanceDate()) + " года");
        }
        catch (Exception e) {
            injectModifier.put("dateO", "-");
        }
        try {
            injectModifier.put("middleMarkO", entrant.getPerson().getPersonEduInstitution().getAverageMark() != null ? DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(entrant.getPerson().getPersonEduInstitution().getAverageMark()) : "-");
        }
        catch (Exception e) {
            injectModifier.put("middleMarkO", "-");
        }

        // Заполняем льготы, если есть
        injectPersonBenefit(injectModifier, entrant);

        // Служба в армии
        injectMilitaryInfo(injectModifier, entrant);

        // Олимпиада
        injectOlympiads(injectModifier, entrant);

        // Общежитие
        injectIsNeedDormitory(injectModifier, entrant);

        injectModifier.put("dateN", RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT.format(entrantRequest.getRegDate()) + " года");

        injectModifier.put("entrNumber", StringUtils.trimToEmpty(entrantRequest.getStringNumber()));

        injectModifier.put("FIOd", PersonManager.instance().declinationDao().getDeclinationFIO(entrant.getPerson().getIdentityCard(), GrammaCase.ACCUSATIVE));

        MQBuilder builder = new MQBuilder(EmployeePost.class.getName(), "ep")
                .add(MQExpression.eq("ep", "postRelation.postBoundedWithQGandQL.title", "Ответственный секретарь ПК"))
                .add(MQExpression.eq("ep", "orgUnit", listOfEnrollmentDirectios.get(0).getEnrollmentDirection().getEducationOrgUnit().getFormativeOrgUnit()));

        List<EmployeePost> resultList = builder.getResultList(getSession());

        String secretar = "";
        if (resultList.size() > 0) {
            secretar = StringUtils.trimToEmpty(resultList.get(0).getEmployee().getPerson().getIdentityCard().getIof());
        }

        OrgUnit formativeOrgUnit = listOfEnrollmentDirectios.get(0).getEnrollmentDirection().getEducationOrgUnit().getFormativeOrgUnit();

        Person person = null;
        try {
            person = PersonManager.instance().dao().getPerson(ContextLocal.getUserContext().getPrincipalContext());
            injectModifier.put("FIOo", person.getIdentityCard().getIof());
        }
        catch (Exception e) {
            injectModifier.put("FIOo", "");
        }

        SharedRtfUtil.removeParagraphsWithTagsRecursive(document, removeTags, false, false);

        // ответственый секретарь и тот, кто ввел данные
        DaoFacade.getDaoPrintUtil().injectEntererData(injectModifier);

        if (DaoFacade.getDaoPrintUtil().isUseExt(entrantRequest)) {
            injectModifier.put("orgUnit", "");
            DaoFacade.getDaoPrintUtil().injectMainSecretary(injectModifier, entrantRequest);
        }
        else {
            injectModifier.put("delegatePost", "Ответственный секретарь отборочной комиссии");
            injectModifier.put("delegateFio", secretar);
            injectModifier.put("orgUnit", formativeOrgUnit.getGenitiveCaseTitle() == null ? "" : formativeOrgUnit.getGenitiveCaseTitle());
        }

        EntrantExt ext = EntrantUtils.getEntrantExt(entrant);
        if (ext.getDocumentReturnMethod() != null) {
            injectModifier.put("documentReturnMethod", ext.getDocumentReturnMethod().getTitle().toLowerCase());
        }
        else {
            injectModifier.put("documentReturnMethod", "лично");
        }
        injectModifier.modify(document);

        RtfTableModifier rtfTableModifier = new RtfTableModifier();
        List<String[]> tableRows = new ArrayList<String[]>();
        int rowNum = 0;
        String[][] tableData;

        //Заполнение таблицы специальности

        List<Discipline2OlympiadDiplomaRelation> diplomaDiscList = getList(Discipline2OlympiadDiplomaRelation.class, Discipline2OlympiadDiplomaRelation.diploma().entrant(), entrant);

        for (RequestedEnrollmentDirection direction : listOfEnrollmentDirectios) {
            rowNum++;

            boolean isOl = false;
            boolean isTargetAdmission = direction.isTargetAdmission();

            for (Discipline2OlympiadDiplomaRelation relation : diplomaDiscList) {

                isOl = relation.getEnrollmentDirection().equals(direction.getEnrollmentDirection())
                        && (relation.getCompensationType() != null && relation.getCompensationType().equals(direction.getCompensationType()));

                if (isOl) break;
            }

            String Napr = direction.getTitle();
            String FO = direction.getEnrollmentDirection().getEducationOrgUnit().getDevelopForm().getTitle();
            String BD = direction.getCompensationType().isBudget() ? "бюджет" : "по договору";
            String LG = direction.getCompetitionKind().getTitle().matches("Вне конкурса") ? "Да" : "-";
            String srok = String.valueOf(direction.getEnrollmentDirection().getEducationOrgUnit().getDevelopPeriod().getTitle());
            tableRows.add(fillTableDisc(rowNum, Napr, FO, BD, srok, LG, isOl ? "Да" : "-", isTargetAdmission ? "Да" : "-"));
        }
        tableData = (String[][]) tableRows.toArray(new String[tableRows.size()][]);
        rtfTableModifier.put("TableDisc", tableData);

        //Заполнение таблицы результаты итоговой государственной аттестации

        tableRows = new ArrayList<String[]>();
        rowNum = 0;

        EntrantDataUtil dataUtil = getEntrantDataUtil(entrantRequest);

        boolean hasChosenEntranceDiscipline = false;

        List<StateExamSubjectMark> entrantEGEDisciplines = new ArrayList<StateExamSubjectMark>();

        Set<ChosenEntranceDiscipline> chosenEntranceDisciplineSet = getChosenEntranceDiscipline(entrantRequest, listOfEnrollmentDirectios);

        Set<Discipline2RealizationWayRelation> olympiadDiplomaRelations = getOlympiads(entrant);

        List<ExamPassDiscipline> examPassDisciplines = getList(ExamPassDiscipline.class, ExamPassDiscipline.entrantExamList().entrant().s(), entrant);
        if (CollectionUtils.isEmpty(examPassDisciplines)) {
            UniRtfUtil.removeTableByName(document, "TableExam", true, false);
        }
        else {
            List<String[]> tableRowsExams = new ArrayList<String[]>();
            int counter = 1;
            for (ExamPassDiscipline examPassDiscipline : examPassDisciplines) {
                String[] row = new String[3];
                row[0] = String.valueOf(counter++);
                row[1] = examPassDiscipline.getEnrollmentCampaignDiscipline().getTitle();
                ExamPassDisciplineISTU disciplineISTU = get(ExamPassDisciplineISTU.class, ExamPassDisciplineISTU.examPassDiscipline(), examPassDiscipline);
                if (disciplineISTU != null && disciplineISTU.getDateSetting() != null && disciplineISTU.getDateSetting().getPassDate() != null) {
                    row[2] = DateFormatter.DEFAULT_DATE_FORMATTER.format(disciplineISTU.getDateSetting().getPassDate());
                }
                else
                    row[2] = "";
                if (!olympiadDiplomaRelations.contains(examPassDiscipline.getEnrollmentCampaignDiscipline()))
                    tableRowsExams.add(row);
            }
            if (!CollectionUtils.isEmpty(tableRowsExams))
                rtfTableModifier.put("TableExam", (String[][]) tableRowsExams.toArray(new String[tableRows.size()][]));
            else
                UniRtfUtil.removeTableByName(document, "TableExam", true, false);
        }

        for (ChosenEntranceDiscipline discipline : chosenEntranceDisciplineSet) {
            if (!entrantEGEDisciplines.contains(dataUtil.getMarkStatistic(discipline).getStateExamMarkPossible()))
                try {
                    hasChosenEntranceDiscipline = true;
                    rowNum++;

                    EntrantStateExamCertificate certificate = dataUtil.getMarkStatistic(discipline).getStateExamMarkPossible().getCertificate();
                    int mark = dataUtil.getMarkStatistic(discipline).getStateExamMarkPossible().getMark();
                    EntrantStateExamCertificateExt certificateExt = StateExamUtil.getEntrantStateExamCertificateExt(certificate);

                    tableRows.add(fillTablePred(
                            rowNum,
                            discipline,
                            String.valueOf(mark), certificateExt.getExamPlace(), certificate.getIssuanceDate()));

                    entrantEGEDisciplines.add(dataUtil.getMarkStatistic(discipline).getStateExamMarkPossible());
                }
                catch (Exception e) {
                    rowNum--;
                }
        }

        if (isSPO || getList(EntrantStateExamCertificate.class, EntrantStateExamCertificate.entrant(), entrant).isEmpty() ||
                !hasChosenEntranceDiscipline || tableRows.isEmpty())
            //удаляем таблицу с шаблона
            UniRtfUtil.removeTableByName(document, "TablePred", true, false);
        else {
            tableData = (String[][]) tableRows.toArray(new String[tableRows.size()][]);
            rtfTableModifier.put("TablePred", tableData);
            rtfTableModifier.modify(document);
        }


        rtfTableModifier.modify(document);

        return document;
    }

    private String[] fillTableDisc(int rowNum, String napr, String fO, String bD, String srok, String lG, String ol, String tA) {
        List<String> result = new ArrayList<String>();

        result.add(String.valueOf(rowNum));
        result.add(napr);
        result.add(fO);
        result.add(bD);
        result.add(srok);
        result.add(lG);
        result.add(ol);
        result.add(tA);

        return (String[]) result.toArray(new String[result.size()]);
    }

    private String[] fillTablePred(int rowNum, ChosenEntranceDiscipline discipline, String mark, String docTitle, Date issuanceDate) {

        String subjectTitle = discipline.getEnrollmentCampaignDiscipline().getEducationSubject().getTitle();
        List<String> result = new ArrayList<String>();

        result.add(String.valueOf(rowNum));
        result.add(subjectTitle);
        result.add(mark);
        result.add(docTitle == null ? "" : docTitle);
        String year = "-";
        if (issuanceDate != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(issuanceDate);
            int yearValue = calendar.get(Calendar.YEAR);
            year = String.valueOf(yearValue);

        }

        result.add(year);
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(Discipline2OlympiadDiplomaRelation.class, "d");
        dql.where(DQLExpressions.eq(DQLExpressions.property(Discipline2OlympiadDiplomaRelation.discipline().fromAlias("d")), DQLExpressions.value(discipline.getEnrollmentCampaignDiscipline())));
        dql.where(DQLExpressions.eq(DQLExpressions.property(Discipline2OlympiadDiplomaRelation.enrollmentDirection().fromAlias("d")), DQLExpressions.value(discipline.getChosenEnrollmentDirection().getEnrollmentDirection())));
        dql.where(DQLExpressions.eq(DQLExpressions.property(Discipline2OlympiadDiplomaRelation.diploma().entrant().fromAlias("d")), DQLExpressions.value(discipline.getChosenEnrollmentDirection().getEntrantRequest().getEntrant())));
        List<Discipline2OlympiadDiplomaRelation> list = getList(dql);
        if (!CollectionUtils.isEmpty(list)) {
            result.add("Да");
        }
        else {
            result.add("");
        }

        return (String[]) result.toArray(new String[result.size()]);
    }


    public RtfDocument printEntrantRequestAspirant(EntrantRequest entrantRequest, List<RequestedEnrollmentDirection> listOfEnrollmentDirectios) {

        TemplateDocument templateDocument = getCatalogItem(TemplateDocument.class, "narfuEntrantRequestAspirant");

        if (templateDocument.getContent() == null) {
            ContextLocal.getErrorCollector().addError("Шаблон не загружен!");
            return null;
        }

        RtfDocument document = new RtfReader().read(templateDocument.getContent());

        Entrant entrant = entrantRequest.getEntrant();

        RtfInjectModifier im = new RtfInjectModifier();

        im.put("FIO", PersonManager.instance().declinationDao().getDeclinationFIO(entrant.getPerson().getIdentityCard(), GrammaCase.GENITIVE));

        Set<String> set = new HashSet<String>();

        if (entrant.getPerson().getWorkPlacePosition() != null)
            set.add(entrant.getPerson().getWorkPlacePosition());
        if (entrant.getPerson().getWorkPlace() != null)
            set.add(entrant.getPerson().getWorkPlace());

        im.put("work", StringUtils.join(set, ", "));

        if (AddressUtil.isSettlement(entrant.getPerson().getAddress()) && AddressUtil.isSettlement(entrant.getPerson().getAddressRegistration())) {
            if (entrant.getPerson().getAddress().getTitleWithFlat().equals(entrant.getPerson().getAddressRegistration().getTitleWithFlat())) {
                im.put("addressreg", entrant.getPerson().getAddressRegistration().getTitleWithFlat());
                deleteRow(document, "addresfact");
            }
            else {
                im.put("addressreg", entrant.getPerson().getAddressRegistration().getTitleWithFlat());
                im.put("addresfact", entrant.getPerson().getAddress().getTitleWithFlat());
            }
        }
        else if (AddressUtil.isSettlement(entrant.getPerson().getAddress()) && !AddressUtil.isSettlement(entrant.getPerson().getAddressRegistration())) {
            im.put("addresfact", entrant.getPerson().getAddress().getTitleWithFlat());
            deleteRow(document, "addressreg");
        }
        else if (!AddressUtil.isSettlement(entrant.getPerson().getAddress()) && AddressUtil.isSettlement(entrant.getPerson().getAddressRegistration())) {
            im.put("addressreg", entrant.getPerson().getAddressRegistration().getTitleWithFlat());
            deleteRow(document, "addresfact");
        }
        else if (!AddressUtil.isSettlement(entrant.getPerson().getAddress()) && !AddressUtil.isSettlement(entrant.getPerson().getAddressRegistration())) {
            im.put("addressreg", "");
            im.put("addresfact", "");
        }

        set.clear();
        if (entrant.getPerson().getContactData() != null) {
            set.add(entrant.getPerson().getContactData().getPhoneFact());
            set.add(entrant.getPerson().getContactData().getPhoneMobile());
            set.add(entrant.getPerson().getContactData().getPhoneWork());
        }
        EntrantAspirantData entrantAspirantData = EntrantUtils.getAspirantData(entrant);
        im.put("sciencePub", String.valueOf(entrantAspirantData.getResearchCount()));
        im.put("scienceInvent", String.valueOf(entrantAspirantData.getInventionCount()));
        im.put("tel", StringUtils.join(set, ", "));
        im.put("email", entrant.getPerson().getEmail() == null ? "" : entrant.getPerson().getEmail());
        EntrantExt entrantExt = EntrantUtils.getEntrantExt(entrant);
        if (entrantExt.getDocumentReturnMethod() != null) {
            im.put("docum", entrantExt.getDocumentReturnMethod().getTitle().toLowerCase());
        }
        else
            im.put("docum", "лично");

        Criteria criteria = getSession().createCriteria(EntrantEnrollmentDocument.class);
        criteria.createAlias(EntrantEnrollmentDocument.L_ENROLLMENT_DOCUMENT, "enrollmentDocument");
        criteria.add(Restrictions.eq(EntrantEnrollmentDocument.L_ENTRANT_REQUEST, entrantRequest));
        criteria.addOrder(Order.asc("enrollmentDocument." + EnrollmentDocument.P_PRIORITY));

        List<EntrantEnrollmentDocument> documentList = criteria.list();

        RtfString col1 = new RtfString();
        RtfString col2 = new RtfString();

        int size = documentList.size();
        if (size % 2 == 0)
            size = size / 2;
        else
            size = Math.round(size / 2);
        if (size == 0 || size == 1)
            size = 2;
        int counter = 1;
        for (EntrantEnrollmentDocument ed : documentList) {
            if (counter <= size) {
                col1.append(String.valueOf(counter)).append(". ").append(ed.getEnrollmentDocument().getTitle()).append(" (").append(ed.isCopy() ? "копия" : "оригинал").append(");").append(IRtfData.PAR);
            }
            else {
                col2.append(String.valueOf(counter)).append(". ").append(ed.getEnrollmentDocument().getTitle()).append(" (").append(ed.isCopy() ? "копия" : "оригинал").append(");").append(IRtfData.PAR);
            }
            counter++;
        }
        im.put("documents", col1);
        im.put("documents2", col2);

        List<Entrant2IndividualAchievements> achievements = getList(Entrant2IndividualAchievements.class, Entrant2IndividualAchievements.entrant().s(), entrant);
        if (CollectionUtils.isEmpty(achievements)) {
            im.put("individualAchievements", "");
        }
        else {
            RtfString string = new RtfString();
            for (Entrant2IndividualAchievements achievement : achievements) {
                string.append(IRtfData.TAB).append("-").append(achievement.getText()).append(";").append(IRtfData.PAR);
            }
            im.put("individualAchievements", string);

        }

        set.clear();
        Set<String> eduLevelSet = new HashSet<String>();
        Set<String> compTypeSet = new HashSet<String>();
        Set<DevelopForm> developFormSet = new HashSet<>();
        Map<String, String> code2Title_A = new HashMap<>();
        code2Title_A.put("1", "очную");
        code2Title_A.put("2", "заочную");
        code2Title_A.put("3", "очно-заочную");
        code2Title_A.put("4", "экстернат");
        code2Title_A.put("5", "самостоятельную");
        for (RequestedEnrollmentDirection enrollmentDirection : listOfEnrollmentDirectios) {
            set.add(code2Title_A.get(enrollmentDirection.getEnrollmentDirection().getEducationOrgUnit().getDevelopForm().getCode()));
            eduLevelSet.add(enrollmentDirection.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getOkso() + " " + enrollmentDirection.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getTitle());
            compTypeSet.add(enrollmentDirection.getCompensationType().getShortTitle());
            developFormSet.add(enrollmentDirection.getEnrollmentDirection().getEducationOrgUnit().getDevelopForm());
        }
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrCampaignDevelopFormDates.class, "e");
        dql.where(DQLExpressions.eq(DQLExpressions.property(EnrCampaignDevelopFormDates.enrollmentCampaign().fromAlias("e")), DQLExpressions.value(entrant.getEnrollmentCampaign())));
        dql.where(DQLExpressions.in(DQLExpressions.property(EnrCampaignDevelopFormDates.developForm().fromAlias("e")), developFormSet));
        dql.where(DQLExpressions.isNotNull(DQLExpressions.property(EnrCampaignDevelopFormDates.docDate().fromAlias("e"))));
        dql.order(DQLExpressions.property(EnrCampaignDevelopFormDates.docDate().fromAlias("e")), OrderDirection.asc);
        List<EnrCampaignDevelopFormDates> dates = IUniBaseDao.instance.get().getList(dql);
        if (!CollectionUtils.isEmpty(dates)) {
            im.put("date_doc", DateFormatter.DEFAULT_DATE_FORMATTER.format(dates.get(0).getDocDate()));
        }
        else {
            im.put("date_doc", "_____________");
        }
        List<Entrant2SpecialConditions> conditions = getList(Entrant2SpecialConditions.class, Entrant2SpecialConditions.entrant().s(), entrant);
        if (!CollectionUtils.isEmpty(conditions)) {
            Set<String> titles = UniBaseUtils.getPropertiesSet(conditions, Entrant2SpecialConditions.specialConditionsForEntrant().title().s());
            im.put("spec_usl", "Да (" + StringUtils.join(titles, ", ") + ")");
        }
        else
            im.put("spec_usl", "Нет");

        im.put("form", StringUtils.join(set, ", "));
        im.put("compensationType", StringUtils.join(compTypeSet, ", "));
        im.put("spec", StringUtils.join(eduLevelSet, ", "));
        IdentityCard card = entrant.getPerson().getIdentityCard();

        if (entrant.getPerson().getPersonEduInstitution() != null) {
            String levelStageStr = "";
            EducationLevelStage stage = entrant.getPerson().getPersonEduInstitution().getEducationLevelStage();
            if (stage != null) {
                while (stage.getParent() != null)
                    stage = stage.getParent();
                levelStageStr = stage.getTitle() + ", ";
            }
            if (entrant.getPerson().getPersonEduInstitution().getEduInstitution() != null) {

                im.put("educationInfo", levelStageStr + entrant.getPerson().getPersonEduInstitution().getEduInstitution().getTitle() + ", " + String.valueOf(entrant.getPerson().getPersonEduInstitution().getYearEnd()));
            }
            else
                im.put("educationInfo", levelStageStr + String.valueOf(entrant.getPerson().getPersonEduInstitution().getYearEnd()));
        }
        else
            im.put("educationInfo", "");

        if (PersonNarfuUtil.getPersonNARFU(entrant.getPerson()).isNeedDormitoryOnReceiptPeriod() || entrant.getPerson().isNeedDormitory())
            im.put("dormitory", "требуется");
        else
            im.put("dormitory", "не требуется");
        if (card.getBirthDate() != null) {
            im.put("birthdate", DateFormatter.STRING_MONTHS_AND_QUOTES.format(entrant.getPerson().getIdentityCard().getBirthDate()));
        }
        else {
            im.put("birthdate", "");
        }
        if (card.getCitizenship() != null) {
            im.put("citizenship", entrant.getPerson().getIdentityCard().getCitizenship().getTitle());
        }
        else {
            im.put("citizenship", "");
        }
        im.put("FIO2", entrant.getPerson().getFullFio());
        Person operator = PersonManager.instance().dao().getPerson(ContextLocal.getUserContext().getPrincipalContext());
        if (operator != null)
            im.put("FIO3", operator.getFio());
        else
            im.put("FIO3", "");
        StringBuilder builder = new StringBuilder();
        builder.append(card.getCardType().getTitle()).append(", ").append(card.getSeria()).append(" ").append(card.getNumber());
        if (card.getIssuanceDate() != null || card.getIssuancePlace() != null) {
            builder.append(",выдан ");

            if (card.getIssuancePlace() != null) {
                builder.append(card.getIssuancePlace()).append(" ");
            }
            if (card.getIssuanceDate() != null) {
                builder.append(DateFormatter.STRING_MONTHS_AND_QUOTES.format(entrant.getPerson().getIdentityCard().getIssuanceDate()));
            }
        }
        im.put("pasportInfo", builder.toString());

        if (entrant.getPerson().getIdentityCard().getCitizenship() != null) {
            im.put("citizenship", entrant.getPerson().getIdentityCard().getCitizenship().getTitle());
        }
        else {
            im.put("citizenship", "");
        }

        im.put("date", DateFormatter.STRING_MONTHS_AND_QUOTES.format(entrantRequest.getRegDate()));
        im.put("num", entrant.getPersonalNumber());


        im.modify(document);

        printDisciplineTable(entrantRequest, listOfEnrollmentDirectios, document);

        return document;
    }

    //заполнение таблицы с канд экзам
    private void printDisciplineTable(EntrantRequest entrantRequest, List<RequestedEnrollmentDirection> listOfEnrollmentDirectios, RtfDocument document) {

        RtfTableModifier rtfTableModifier = new RtfTableModifier();
        List<String[]> tableRows = new ArrayList<String[]>();
        String[][] tableData;

        boolean hasChosenEntranceDiscipline = false;

        //заполнение таблицы Мною сданы кандидатские экзамены

        Set<ChosenEntranceDiscipline> totalSet = getChosenEntranceDiscipline(entrantRequest, listOfEnrollmentDirectios);

        Set<Discipline2RealizationWayRelation> ecdSet = new HashSet<Discipline2RealizationWayRelation>();
        for (ChosenEntranceDiscipline discipline : totalSet) {
            //вступительные испытания есть - смотрим на наличие оценки по предмету и проверяем не выводили раньше данную дисциплину, если есть, то выводим
            if (discipline.getFinalMark() != null && !ecdSet.contains(discipline.getEnrollmentCampaignDiscipline())) {
                tableRows.add(fillTableAspDisc(discipline.getEnrollmentCampaignDiscipline().getTitle(), discipline.getFinalMark()));
                hasChosenEntranceDiscipline = true;
                ecdSet.add(discipline.getEnrollmentCampaignDiscipline());
            }
        }

        if (hasChosenEntranceDiscipline) {
            //если выбраны 	Вступительные испытания	и есть оценки по предметам печатаем таблицу
            tableData = (String[][]) tableRows.toArray(new String[tableRows.size()][]);
            rtfTableModifier.put("disc", tableData);
            rtfTableModifier.modify(document);
        }
        else {
            //иначе удаляем таблицу с шаблона
            UniRtfUtil.removeTableByName(document, "disc", false, true);
        }
    }


    /**
     * Заполняем льготы
     *
     * @param injectModifier
     * @param entrant
     *
     * @return
     */
    private RtfInjectModifier injectPersonBenefit(RtfInjectModifier injectModifier, Entrant entrant) {
        List<PersonBenefit> lgoty = getList(PersonBenefit.class, "person", entrant.getPerson());
        injectModifier.put("docLLabel", "Документ: ");
        injectModifier.put("dateLLabel", "Дата выдачи: ");

        if (!lgoty.isEmpty()) {
            List<String> nameL = new ArrayList<String>();
            List<String> dateL = new ArrayList<String>();
            List<String> docL = new ArrayList<String>();
            for (PersonBenefit benefit : lgoty) {
                nameL.add(benefit.getTitle());
                if (benefit.getDate() != null)
                    dateL.add(RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(benefit.getDate()));
                if (benefit.getDocument() != null)
                    docL.add(benefit.getDocument());
            }

            if (!nameL.isEmpty())
                injectModifier.put("nameL", StringUtils.join(nameL, ", "));
            else
                removeTags.add("nameL");

            if (!docL.isEmpty() || !dateL.isEmpty()) {
                if (!docL.isEmpty())
                    injectModifier.put("docL", StringUtils.join(docL, ", ") + " ");
                else {
                    injectModifier.put("docL", "");
                    injectModifier.put("docLLabel", "");
                }

                if (!dateL.isEmpty())
                    injectModifier.put("dateL", StringUtils.join(dateL, ", ") + " ");
                else {
                    injectModifier.put("dateL", "");
                    injectModifier.put("dateLLabel", "");
                }
            }
            else removeTags.add("docL");
        }
        else {
            removeTags.add("docL");

            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EntrantEnrolmentRecommendation.class, "eer")
                    .column(DQLExpressions.property(EntrantEnrolmentRecommendation.recommendation().title().fromAlias("eer")))
                    .where(DQLExpressions.eqValue(DQLExpressions.property(EntrantEnrolmentRecommendation.entrant().fromAlias("eer")), entrant))
                    .order(DQLExpressions.property(EntrantEnrolmentRecommendation.recommendation().title().fromAlias("eer")));
            List<String> recomendations = getList(builder);

            if (!recomendations.isEmpty()) {
                injectModifier.put("nameL", StringUtils.join(recomendations, ", "));
            }
            else removeTags.add("nameL");
        }

        return injectModifier;
    }

    /**
     * Заполняем контактные лица (ближайшие родственники)
     *
     * @param injectModifier
     * @param entrant
     *
     * @return
     */
    private RtfInjectModifier injectPersonNextOfKinInfo(RtfInjectModifier injectModifier, Entrant entrant) {
        List<PersonNextOfKin> nextOfKinList = getList(PersonNextOfKin.class, "person", entrant.getPerson());
        List<String> familyContactList = new ArrayList<String>();

        for (PersonNextOfKin kin : nextOfKinList) {
            familyContactList.add(kin.getFullFio() + " " + (kin.getPhones() == null ? "" : "(" + kin.getPhones() + ")"));
        }

        if (!familyContactList.isEmpty())
            injectModifier.put("familyContacts", StringUtils.join(familyContactList, ", "));
        else removeTags.add("familyContacts");

        return injectModifier;
    }

    /**
     * Заполняем олимпиады
     *
     * @param injectModifier
     * @param entrant
     *
     * @return
     */
    private RtfInjectModifier injectOlympiads(RtfInjectModifier injectModifier, Entrant entrant) {

        List<OlympiadDiploma> diplomaList = getList(OlympiadDiploma.class, "entrant", entrant);
        injectModifier.put("dateOlLabel", "Дата выдачи: ");
        if (!diplomaList.isEmpty()) {
            List<String> nameOl = new ArrayList<String>();
            List<String> numOl = new ArrayList<String>();
            List<String> dateOl = new ArrayList<String>();
            List<String> stOl = new ArrayList<String>();
            List<String> prOl = new ArrayList<String>();

            for (OlympiadDiploma diploma : diplomaList) {
                nameOl.add(diploma.getOlympiad());
                numOl.add(diploma.getNumber());
                dateOl.add(diploma.getIssuanceDate() != null ? RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(diploma.getIssuanceDate()) : "-");
                stOl.add(diploma.getDegree().getTitle());
                prOl.add(diploma.getSubject());
            }
            injectModifier.put("nameOl", StringUtils.join(nameOl, ", "));
            injectModifier.put("numOl", StringUtils.join(numOl, ", "));

            if (!dateOl.isEmpty())
                injectModifier.put("dateOl", StringUtils.join(dateOl, ", "));
            else {
                injectModifier.put("dateOlLabel", "");
                injectModifier.put("dateOl", "");
            }
            injectModifier.put("stOl", StringUtils.join(stOl, ", "));
            injectModifier.put("prOl", StringUtils.join(prOl, ", "));
        }
        else {
            removeTags.add("numOl");
            removeTags.add("stOl");
            injectModifier.put("nameOl", "нет");
        }

        return injectModifier;
    }

    /**
     * Заполняем служба в армии
     *
     * @param injectModifier
     * @param entrant
     *
     * @return
     */
    private RtfInjectModifier injectMilitaryInfo(RtfInjectModifier injectModifier, Entrant entrant) {

        if (entrant.isPassArmy()) {
            injectModifier.put("slujA", "служил ");
            StringBuilder str = new StringBuilder("");
            if (entrant.getBeginArmy() != null)
                str.append("Дата начала службы: ")
                        .append(RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(entrant.getBeginArmy()))
                        .append(" ");

            if (entrant.getEndArmy() != null)
                str.append("Дата окончания службы: ")
                        .append(RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(entrant.getEndArmy()));
            //				.append(IRtfData.PAR);

            injectModifier.put("dataArmy", str.toString());
            if (entrant.getEndArmyYear() != null)
                injectModifier.put("yearA", "Уволен в запас в " + String.valueOf(entrant.getEndArmyYear()) + " году.");
            else removeTags.add("yearA");
        }
        else {
            removeTags.add("yearA");
            removeTags.add("slujA");
        }

        return injectModifier;
    }

    /**
     * Заполняем информацию о потребности в общежитии
     *
     * @param injectModifier
     * @param entrant
     *
     * @return
     */
    private RtfInjectModifier injectIsNeedDormitory(RtfInjectModifier injectModifier, Entrant entrant) {
        PersonNARFU person_ext = get(PersonNARFU.class, PersonNARFU.L_PERSON, entrant.getPerson());
        if (person_ext != null && (person_ext.isNeedDormitoryOnReceiptPeriod() || entrant.getPerson().isNeedDormitory())) {
            StringBuilder builder = new StringBuilder("Нуждаюсь в предоставлении места в общежитии ");
            if (person_ext.isNeedDormitoryOnReceiptPeriod() && entrant.getPerson().isNeedDormitory())
                builder.append("на период поступления, обучения.");
            else if (entrant.getPerson().isNeedDormitory()) builder.append("на период обучения.");
            else if (person_ext.isNeedDormitoryOnReceiptPeriod()) builder.append("на период поступления.");
            injectModifier.put("neededO", builder.toString());
        }
        else
            injectModifier.put("neededO", "");
        injectModifier.put("examHostel", PersonNarfuUtil.getPersonNARFU(entrant.getPerson()).isNeedDormitoryOnReceiptPeriod() ? "да" : "нет");
        injectModifier.put("eduHostel", entrant.getPerson().isNeedDormitory() ? "да" : "нет");
        return injectModifier;
    }

    private Set<ChosenEntranceDiscipline> getChosenEntranceDiscipline(EntrantRequest entrantRequest, List<RequestedEnrollmentDirection> listOfEnrollmentDirectios) {

        EntrantDataUtil dataUtil = getEntrantDataUtil(entrantRequest);

        Set<ChosenEntranceDiscipline> totalSet = new HashSet<ChosenEntranceDiscipline>();

        for (RequestedEnrollmentDirection direction : listOfEnrollmentDirectios)
            //выбираем Вступительные испытания всех направлений подготовки
            totalSet.addAll(dataUtil.getChosenEntranceDisciplineSet(direction));

        return totalSet;
    }

    private Set<Discipline2RealizationWayRelation> getOlympiads(Entrant entrant) {

        DQLSelectBuilder dqlSelectBuilder = new DQLSelectBuilder().fromEntity(Discipline2OlympiadDiplomaRelation.class, "d");
        dqlSelectBuilder.where(DQLExpressions.eq(DQLExpressions.property(Discipline2OlympiadDiplomaRelation.diploma().entrant().fromAlias("d")), DQLExpressions.value(entrant)));
        List<Discipline2OlympiadDiplomaRelation> list = getList(dqlSelectBuilder);

        Set<Discipline2RealizationWayRelation> totalSet = new HashSet<>();

        for (Discipline2OlympiadDiplomaRelation direction : list)
            //выбираем Вступительные испытания всех направлений подготовки
            totalSet.add(direction.getDiscipline());

        return totalSet;
    }

    private EntrantDataUtil getEntrantDataUtil(EntrantRequest entrantRequest) {

        Entrant entrant = entrantRequest.getEntrant();
        MQBuilder directionBuilder = new MQBuilder("ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection", "r");
        directionBuilder.add(entrantRequest != null ? MQExpression.eq("r", "entrantRequest", entrantRequest) : MQExpression.eq("r", "entrantRequest.entrant", entrant));
        EntrantDataUtil dataUtil = new EntrantDataUtil(getSession(), entrant.getEnrollmentCampaign(), directionBuilder, DetailLevel.EXAM_PASS_WITH_MARK_STATISTICS);

        return dataUtil;
    }

    private String[] fillTableAspDisc(String title, Double mark) {

        List<String> result = new ArrayList<String>();

        result.add(title);
        result.add(String.valueOf(mark.intValue()));

        return (String[]) result.toArray(new String[result.size()]);
    }

    private void deleteRow(RtfDocument document, String name) {
        //удаляем строку из таблицы
        RtfSearchResult search = UniRtfUtil.findRtfTableMark(document, name);
        RtfTable table = (RtfTable) document.getElementList().get(search.getIndex());
        for (RtfRow row : table.getRowList()) {
            if (name.equals(UniRtfUtil.getRowName(row))) {
                table.getRowList().remove(row);
                return;
            }
        }
    }

}
