package ru.tandemservice.narfu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;

/**
 * Очистка identitycard_t без person_id
 *
 * @author Viktor
 */
public class MS_narfu_1x0x0_8to9 extends IndependentMigrationScript {


    @Override
    public void run(DBTool tool) throws Exception
    {
        if (tool.tableExists("person_t") && tool.tableExists("identitycard_t")) {

            /**
             * Удалим персоны содержащие identitycard_t с пустым полем
             */
            tool.executeUpdate("delete from person_t where identitycard_id in(select id from identitycard_t where person_id is null)");

            /**
             * Удалим поля person_id с пустым значением
             */
            tool.executeUpdate("delete from identitycard_t where person_id is null");

        }
    }


}		