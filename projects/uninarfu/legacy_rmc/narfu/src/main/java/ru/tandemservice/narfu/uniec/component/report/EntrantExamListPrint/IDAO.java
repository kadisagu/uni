package ru.tandemservice.narfu.uniec.component.report.EntrantExamListPrint;

import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.uniec.entity.entrant.Entrant;

import java.util.List;


public interface IDAO extends IUniDao<Model> {
    public abstract void createReport(Model model);

    public abstract List<Entrant> getEntrantList(Model model);

}
