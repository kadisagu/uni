package ru.tandemservice.narfu.component.reports.orgUnitStudentGrantByInstitute.Add;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = component.getModel();
        ((IDAO) getDao()).prepare(model);
    }

    public void onClickApply(IBusinessComponent component) {
        Model model = component.getModel();
        ((IDAO) getDao()).createReport(model);
        deactivate(component);
    }
}
