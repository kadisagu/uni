package ru.tandemservice.narfu.component.reports.JournalIssuanceCallCertificate.List;

import jxl.read.biff.BiffException;
import jxl.write.WriteException;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;

import java.io.IOException;

public interface IDAO extends IDefaultCatalogPubDAO {

    void prepareProjectsDataSource(Model model);

    void preparePrintReport(Model model) throws WriteException, IOException, BiffException;

}
