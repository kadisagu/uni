package ru.tandemservice.narfu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_narfu_1x0x0_16to17 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.10"),
                        new ScriptDependency("org.tandemframework.shared", "1.1.3"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.1.3")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность narfuReportStudentArchive

        // удалено свойство educationYear
        {
            // удалить колонку
            tool.dropColumn("narfureportstudentarchive_t", "educationyear_id");

        }

        // удалено свойство executorPost
        {
            // удалить колонку
            tool.dropColumn("narfureportstudentarchive_t", "executorpost_id");

        }

        // удалено свойство visaPost
        {
            // удалить колонку
            tool.dropColumn("narfureportstudentarchive_t", "visapost_id");

        }


    }
}