package ru.tandemservice.narfu.component.eduplan.EduPlanBlockVersionLinkPub;

import org.apache.commons.collections15.Predicate;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.column.IEntityHandler;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.narfu.entity.VersionBlockDisciplinesLink;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.dao.eduplan.EppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.uniepp.ui.EduPlanVersionBlockDataSourceGenerator;

import java.util.*;

public class DAO extends UniDao<Model> implements IDAO {

    public void prepare(final Model model) {
        final EppEduPlanVersionBlock block = model.getEduPlanHolder().refresh(EppEduPlanVersionBlock.class);


        EduPlanVersionBlockDataSourceGenerator dataSourceGenerator = new EduPlanVersionBlockDataSourceGenerator() {
            public boolean isCheckMode() {
                return false;
            }

            public boolean isExtendedView() {
                return false;
            }

            protected boolean isEditable() {
                return false;
            }

            protected String getPermissionKeyEdit() {
                return "editContent_eppEduPlanVersion";
            }

            protected Long getVersionBlockId() {
                return block.getId();
            }

            public void doPrepareBlockDisciplineDataSource(StaticListDataSource<IEppEpvRowWrapper> dataSource, EduPlanVersionBlockDataSourceGenerator.IBlockRenderContext context) {
                super.doPrepareBlockDisciplineDataSource(dataSource, context);


                dataSource.addColumn(getColumn_owner(false).setHeaderAlign("center"));
            }


        };
//        final Predicate blockPredicate = EppEduPlanVersionDataDAO.buildBlockRowPredicate(block);

        dataSourceGenerator.doPrepareBlockDisciplineDataSource(model.getRowDataSource(), new EduPlanVersionBlockDataSourceGenerator.IBlockRenderContext() {

            public IEntityHandler getAdditionalEditDisabler() {
                return null;
            }


            public Collection<IEppEpvRowWrapper> getBlockRows(IEppEpvBlockWrapper versionWrapper) {

                return EppEduPlanVersionDataDAO.filterEduPlanBlockRows(versionWrapper.getRowMap(), buildEppDisciplinesDataSourcePredicate(), true);

            }

        });


    }

    protected Predicate<IEppEpvRowWrapper> buildEppDisciplinesDataSourcePredicate() {
        return object -> (IEppEpvRowWrapper.DISCIPLINES_ROWS.evaluate(object));
    }

    @Override
    public void makeLinks(Model model)
    {
        List<EppRegistryElementPartFControlAction> actionsSrc = getActionList(model.getLinkHolder().getValue().getBlockSrc());
        if (actionsSrc.isEmpty())
            throw new ApplicationException("Для версии УП " + model.getLinkHolder().getValue().getBlockSrc().getTitle() + " не заданы строки реестра");
        List<EppRegistryElementPartFControlAction> actionsDst = getActionList(model.getLinkHolder().getValue().getBlockDst());
        if (actionsDst.isEmpty())
            throw new ApplicationException("Для версии УП " + model.getLinkHolder().getValue().getBlockDst().getTitle() + " не заданы строки реестра");

        List<PairKey<EppRegistryElementPartFControlAction, EppRegistryElementPartFControlAction>> equalElements = new ArrayList<>();
        List<Long> needToUpdate = new ArrayList<>();
        Map<String, List<EppRegistryElementPartFControlAction>> sortedMap = new HashMap<>();
        for (EppRegistryElementPartFControlAction dst : actionsDst) {
            String key = getKey(dst);
            if (!sortedMap.containsKey(key))
                sortedMap.put(key, new ArrayList<>());
            sortedMap.get(key).add(dst);
        }
        for (EppRegistryElementPartFControlAction src : actionsSrc) {

            String key = getKey(src);
            if (!sortedMap.containsKey(key))
                continue;

            List<EppRegistryElementPartFControlAction> actionList = sortedMap.get(key);
            for (EppRegistryElementPartFControlAction action : actionList) {
                if (action.getPart().getNumber() == src.getPart().getNumber() && action.getPart().getRegistryElement().getSize() >= src.getPart().getRegistryElement().getSize()) {
                    equalElements.add(new PairKey<>(src, action));
                    needToUpdate.add(src.getId());
                }
            }


        }
        new DQLDeleteBuilder(VersionBlockDisciplinesLink.class)
                .where(DQLExpressions.in(DQLExpressions.property(VersionBlockDisciplinesLink.controlActionSrc().id()), needToUpdate))
                .where(DQLExpressions.eq(DQLExpressions.property(VersionBlockDisciplinesLink.versionBlockLink()), DQLExpressions.value(model.getLinkHolder().getValue())));
        for (PairKey<EppRegistryElementPartFControlAction, EppRegistryElementPartFControlAction> element : equalElements) {
            VersionBlockDisciplinesLink link = new VersionBlockDisciplinesLink();
            link.setControlActionSrc(element.getFirst());
            link.setControlActionDst(element.getSecond());
            link.setVersionBlockLink(model.getLinkHolder().getValue());
            save(link);
        }


    }

    private String getKey(EppRegistryElementPartFControlAction action)
    {
        return action.getControlAction().getCode() + action.getPart().getRegistryElement().getTitle() + String.valueOf(action.getPart().getRegistryElement().getParts());
    }

    private List<EppRegistryElementPartFControlAction> getActionList(EppEduPlanVersionBlock versionBlock)
    {
        DQLSelectBuilder registryRowsDisciplines = new DQLSelectBuilder().fromEntity(EppEpvRegistryRow.class, "row").column(DQLExpressions.property(EppEpvRegistryRow.registryElement().fromAlias("row")));
        registryRowsDisciplines.where(DQLExpressions.eq(DQLExpressions.property(EppEpvRegistryRow.owner().fromAlias("row")), DQLExpressions.value(versionBlock)));
        List<EppRegistryElement> disciplines = getList(registryRowsDisciplines);
        if (disciplines.isEmpty()) return new ArrayList<>();
        DQLSelectBuilder actions = new DQLSelectBuilder().fromEntity(EppRegistryElementPartFControlAction.class, "act");
        actions.where(DQLExpressions.in(DQLExpressions.property(EppRegistryElementPartFControlAction.part().registryElement().fromAlias("act")), registryRowsDisciplines.buildQuery()));
        return getList(actions);
    }


}

