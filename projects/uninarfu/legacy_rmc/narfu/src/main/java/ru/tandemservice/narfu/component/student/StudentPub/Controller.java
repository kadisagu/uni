package ru.tandemservice.narfu.component.student.StudentPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.narfu.utils.NarfuUtil;
import ru.tandemservice.uni.component.student.StudentPub.IDAO;
import ru.tandemservice.uni.component.student.StudentPub.Model;

public class Controller extends AbstractBusinessController<IDAO, Model>
{

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        ru.tandemservice.uni.component.student.StudentPub.Model baseModel = component.getModel();
        baseModel.setStudentDocumentTypeList(NarfuUtil.getStudentDocumentTypeByCategory(baseModel.getStudent()));
    }

    public void onClickEditStudentBookNumber(IBusinessComponent component)
    {
        Model myModel = component.getModel();
        component.createRegion("studentTabPanel",
                new ComponentActivator("ru.tandemservice.narfu.component.student.StudentBookNumberEdit",
                        new ParametersMap().add("studentId", myModel.getStudent().getId())));
    }

}