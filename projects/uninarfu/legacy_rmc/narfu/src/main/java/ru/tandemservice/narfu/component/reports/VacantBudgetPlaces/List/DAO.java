package ru.tandemservice.narfu.component.reports.VacantBudgetPlaces.List;

import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.narfu.entity.NarfuReportVacantBudgetPlaces;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepareListDataSource(Model model) {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(NarfuReportVacantBudgetPlaces.class, "report");

        DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(NarfuReportVacantBudgetPlaces.class, "report");
        orderRegistry.applyOrder(dql, model.getDataSource().getEntityOrder());

        UniBaseUtils.createPage(model.getDataSource(), dql, getSession());
    }
}
