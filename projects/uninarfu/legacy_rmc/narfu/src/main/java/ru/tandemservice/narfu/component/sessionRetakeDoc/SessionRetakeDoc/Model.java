package ru.tandemservice.narfu.component.sessionRetakeDoc.SessionRetakeDoc;

import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.unisession.entity.document.SessionObject;

import java.util.ArrayList;
import java.util.List;

@Input({
        @org.tandemframework.core.component.Bind(key = "sessionObject", binding = "sessionObjectId"),
        @org.tandemframework.core.component.Bind(key = "publisherId", binding = "orgUnitHolder.id", required = true)})
public class Model {

    private final EntityHolder<OrgUnit> orgUnitHolder;
    private SessionObject sessionObject;
    private Long sessionObjectId;
    private ISelectModel sessionObjectModel;
    private ISelectModel registryElementModel;
    private ISelectModel actionModel;
    private ISelectModel studentModel;
    private List<EppRegistryElementPartFControlAction> actionList = new ArrayList<>();
    private List<Student> studentList = new ArrayList<>();

    private List<EppRegistryElementPart> elementList = new ArrayList<>();

    public Model() {
        this.orgUnitHolder = new EntityHolder<>();
        this.sessionObject = new SessionObject();
    }

    public OrgUnit getOrgUnit() {
        return ((OrgUnit) getOrgUnitHolder().getValue());
    }

    public SessionObject getSessionObject() {
        return sessionObject;
    }

    public void setSessionObject(SessionObject sessionObject) {
        this.sessionObject = sessionObject;
    }

    public EntityHolder<OrgUnit> getOrgUnitHolder() {
        return orgUnitHolder;
    }

    public ISelectModel getSessionObjectModel() {
        return sessionObjectModel;
    }

    public void setSessionObjectModel(ISelectModel sessionObjectModel) {
        this.sessionObjectModel = sessionObjectModel;
    }

    public ISelectModel getRegistryElementModel() {
        return registryElementModel;
    }

    public void setRegistryElementModel(ISelectModel registryElementModel) {
        this.registryElementModel = registryElementModel;
    }

    public Long getSessionObjectId() {
        return sessionObjectId;
    }

    public void setSessionObjectId(Long sessionObjectId) {
        this.sessionObjectId = sessionObjectId;
    }

    public ISelectModel getActionModel() {
        return actionModel;
    }

    public void setActionModel(ISelectModel actionModel) {
        this.actionModel = actionModel;
    }

    public List<EppRegistryElementPartFControlAction> getActionList() {
        return actionList;
    }

    public void setActionList(List<EppRegistryElementPartFControlAction> actionList) {
        this.actionList = actionList;
    }

    public List<EppRegistryElementPart> getElementList() {
        return elementList;
    }

    public void setElementList(List<EppRegistryElementPart> elementList) {
        this.elementList = elementList;
    }

    public ISelectModel getStudentModel() {
        return studentModel;
    }

    public void setStudentModel(ISelectModel studentModel) {
        this.studentModel = studentModel;
    }

    public List<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }
}
