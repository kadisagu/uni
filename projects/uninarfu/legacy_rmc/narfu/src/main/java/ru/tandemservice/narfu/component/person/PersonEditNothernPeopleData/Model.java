/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.narfu.component.person.PersonEditNothernPeopleData;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudentrmc.entity.PersonNARFU;

/**
 * TODO Add description here
 *
 * @author agolubenko
 * @created Jun 12, 2010
 */
@State(@Bind(key = "personId", binding = "person.id"))
public class Model {
    private Person _person = new Person();
    private PersonNARFU _personNarfu = new PersonNARFU();

    private boolean _nothernPeople;

    public Person getPerson()
    {
        return _person;
    }

    public void setPerson(Person person)
    {
        _person = person;
    }

    public PersonNARFU getPersonNarfu() {
        return _personNarfu;
    }

    public void setPersonNarfu(PersonNARFU _personNarfu) {
        this._personNarfu = _personNarfu;
    }

    public boolean isNothernPeople()
    {
        return _nothernPeople;
    }

    public void setNothernPeople(boolean nothernPeople)
    {
        _nothernPeople = nothernPeople;
    }
}
