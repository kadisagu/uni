package ru.tandemservice.narfu.component.menu.examGroup.logic2.MarkDistribution;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.tools.PrivateAccessor;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniec.component.menu.examGroup.abstractMarkDistribution.EntrantDisciplineMarkWrapper;
import ru.tandemservice.uniec.component.menu.examGroup.abstractMarkDistribution.IMarkDistributionDAO;
import ru.tandemservice.uniec.component.menu.examGroup.abstractMarkDistribution.MarkDistributionController;
import ru.tandemservice.uniec.component.menu.examGroup.logic2.MarkDistribution.Model;

import java.util.List;


public class Controller extends ru.tandemservice.uniec.component.menu.examGroup.logic2.MarkDistribution.Controller {


    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        //super.onRefreshComponent(component);
        final Model model = getModel(component);

        model.setPrincipalContext(component.getUserContext().getPrincipalContext());
        model.setCommon(UniDaoFacade.getSettingsManager().getSettings("abstractMarkDistribution"));
        model.setSettings(component.getSettings());
        ((IMarkDistributionDAO<Model>) getDao()).prepare(model);

        PrivateAccessor.invokePrivateMethod(this, MarkDistributionController.class, "prepareListDataSource", new Object[]{component});

        DynamicListDataSource<EntrantDisciplineMarkWrapper> dataSource = model.getDataSource();
        List<AbstractColumn> columns = dataSource.getColumns();

        dataSource = new DynamicListDataSource<>(component, paramIBusinessComponent -> {
            ((IDAO) getDao()).prepareListDataSourceExt(model);

        });

        for (AbstractColumn column : columns) {
            dataSource.addColumn(column);
            if (column.getName().equals("[entrantRequeststringNumber]")) {
                dataSource.addColumn(new SimpleColumn("Дисциплина", "examPassDiscipline.title").setOrderable(false).setClickable(false));
            }
        }

        model.setDataSource(dataSource);

    }
}
