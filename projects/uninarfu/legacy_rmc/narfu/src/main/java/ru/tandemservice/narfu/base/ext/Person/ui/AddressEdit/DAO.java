package ru.tandemservice.narfu.base.ext.Person.ui.AddressEdit;

import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudentrmc.entity.PersonNARFU;
import ru.tandemservice.narfu.base.ext.Person.ui.Tab.PersonNarfuUtil;

public class DAO extends org.tandemframework.shared.person.base.bo.Person.ui.AddressEdit.DAO {
    @Override
    public void prepare(org.tandemframework.shared.person.base.bo.Person.ui.AddressEdit.Model model) {
        super.prepare(model);
        Model myModel = (Model) model;
        PersonNARFU personNARFU = PersonNarfuUtil.getPersonNARFU((Person) model.getPerson());
        myModel.setAddressRegistrationInfo(personNARFU.getAddressRegistrationInfo());
    }

    @Override
    public void update(org.tandemframework.shared.person.base.bo.Person.ui.AddressEdit.Model model) {
        super.update(model);
        Model myModel = (Model) model;
        PersonNARFU personNARFU = PersonNarfuUtil.getPersonNARFU((Person) model.getPerson());
        personNARFU.setAddressRegistrationInfo(myModel.getAddressRegistrationInfo());
        saveOrUpdate(personNARFU);
    }

}
