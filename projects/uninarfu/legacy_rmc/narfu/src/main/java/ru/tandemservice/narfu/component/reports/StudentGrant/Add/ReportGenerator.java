package ru.tandemservice.narfu.component.reports.StudentGrant.Add;

import com.google.common.collect.ImmutableMap;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.*;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.util.PersonSecurityUtil;
import ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab.MonthWrapper;
import ru.tandemservice.movestudentrmc.entity.RelTypeGrantView;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntity;
import ru.tandemservice.movestudentrmc.entity.catalog.StuGrantStatus;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.StuGrantStatusCodes;
import ru.tandemservice.narfu.entity.NarfuReportStudentGrantCharging;
import ru.tandemservice.narfu.utils.StudentSessionHelper;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.lang.Boolean;
import java.util.*;

public class ReportGenerator {

    public static final String TEMPLATE = "studentListGrantCharging";

    //input params
    protected EducationYear year;
    protected YearDistributionPart part;

    protected MonthWrapper grantMonth;
    protected EducationYear grantYear;

    protected Map<String, Map<String, Object>> detalization;

    protected WritableWorkbook workbook;
    protected WritableSheet sheet;
    protected ByteArrayOutputStream workbookStream;

    protected int maxColumn = 0;

    protected Map<Long, OrgUnitData> fouData;
    protected Map<Long, GrantData> grantData;

    protected StudentStatus activeStudentStatus;
    protected StuGrantStatus activeGrantStatus;

    private IUniBaseDao dao;
    private StudentSessionHelper helper;

    public static Long createReport(EducationYear year, YearDistributionPart part, Date month) {
        try {
            ReportGenerator generator = new ReportGenerator(year, part, month);
            NarfuReportStudentGrantCharging report = generator.generateReport();

            UniDaoFacade.getCoreDao().saveOrUpdate(report.getContent());
            UniDaoFacade.getCoreDao().saveOrUpdate(report);

            return report.getId();
        }
        catch (Exception ex) {
            ex.printStackTrace();
            throw new ApplicationException(ex.getMessage(), ex);
        }
    }

    protected ReportGenerator(EducationYear year, YearDistributionPart part, Date month) {
        this.year = year;
        this.part = part;

        MonthWrapper.Data data = MonthWrapper.getInstance(month);
        this.grantYear = data.educationYear;
        this.grantMonth = data.monthWrapper;

        this.dao = UniDaoFacade.getCoreDao();
        this.activeStudentStatus = dao.getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_ACTIVE);
        this.activeGrantStatus = dao.getCatalogItem(StuGrantStatus.class, StuGrantStatusCodes.CODE_1);

        this.helper = new StudentSessionHelper(this.year, this.part);

        loadDetalization();
    }

    protected NarfuReportStudentGrantCharging generateReport() throws Exception {
        NarfuReportStudentGrantCharging report = new NarfuReportStudentGrantCharging();
        report.setFormingDate(new Date());
        report.setEducationYear(this.year.getTitle());
        report.setEducationYearPart(this.part.getTitle());
        report.setMonth(this.grantMonth.getStringValue(this.grantYear));
        report.setExecutor(PersonSecurityUtil.getExecutor());

        //fill excel
        loadTemplate();
        fillReport();
        this.workbook.write();
        this.workbook.close();

        //put excel
        DatabaseFile reportFile = new DatabaseFile();
        reportFile.setContent(this.workbookStream.toByteArray());
        reportFile.setFilename("Начисление стипендии.xls");
        reportFile.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_VND_MS_EXCEL);
        report.setContent(reportFile);

        return report;
    }

    protected void fillReport() throws Exception {
        WritableCell cell = (WritableCell) this.sheet.getCell(0, 0);
        if (cell != null) {
            String title = cell.getContents();

            StringBuilder sb = new StringBuilder()
                    .append(RussianDateFormatUtils.getMonthName(this.grantMonth.getDate(this.grantYear), true))
                    .append(" ")
                    .append(CoreDateUtils.getYear(this.grantMonth.getDate(this.grantYear)))
                    .append(", отчет сформирован ")
                    .append(RussianDateFormatUtils.getDateFormattedWithMonthName(new Date()));
            title += sb.toString();

            ((Label) cell).setString(title);
        }

        loadGrans();
        fillGrantTitles();

        loadFormativeOrgUnits();
        fillFormativeOrgUnits();
    }

    protected void loadDetalization() {
        this.detalization = new LinkedHashMap<String, Map<String, Object>>();

        this.detalization.put("", null);//сам вуз

        this.detalization.put("в т.ч. специалитет",
                              ImmutableMap.of(
                                      Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().high().s(), (Object) Boolean.TRUE,
                                      Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().bachelor().s(), Boolean.FALSE,
                                      Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().master().s(), Boolean.FALSE
                              )
        );
        this.detalization.put("в т.ч. бакалавриат",
                              ImmutableMap.of(
                                      Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().high().s(), (Object) Boolean.TRUE,
                                      Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().bachelor().s(), Boolean.TRUE
                              )
        );
        this.detalization.put("в т.ч. магистратура",
                              ImmutableMap.of(
                                      Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().high().s(), (Object) Boolean.TRUE,
                                      Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().master().s(), Boolean.TRUE
                              )
        );
        this.detalization.put("в т.ч. обучающиеся СПО",
                              ImmutableMap.of(
                                      Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().middle().s(), (Object) Boolean.TRUE
                              )
        );
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    protected void fillFormativeOrgUnits() throws Exception {
        final int columnFirst = 0;
        final int rowFirst = 6;

        WritableCellFormat format = createFormat(false, false);
        WritableCellFormat formatRight = createFormat(false, false);
        formatRight.setAlignment(Alignment.RIGHT);
        WritableCellFormat formatBold = createFormat(true, false);

        int currentRow = rowFirst;
        for (OrgUnitData data : this.fouData.values()) {
            WritableCell cell;

            for (int i = 0; i < this.detalization.size(); i++) {
                //колонка 1 (названия)
                cell = createCell(columnFirst, currentRow, data.rowsName.get(i), i == 0 ? formatBold : formatRight);
                this.sheet.addCell(cell);

                //колонка 2 (Количество обучающихся)
                cell = createCell(columnFirst + 1, currentRow, data.studentsTotal.get(i), format);
                this.sheet.addCell(cell);

                //колонка 3 Количество зачисленных первокурсников
                cell = createCell(columnFirst + 2, currentRow, data.studentsFirst.get(i), format);
                this.sheet.addCell(cell);

                //колонка 4 (Количество студентов, имеющих академическую задолженность)
                cell = createCell(columnFirst + 3, currentRow, data.studentsDebtors.get(i), format);
                this.sheet.addCell(cell);

                //колонка 5  Количество обучающихся, имеющих "3"
                cell = createCell(columnFirst + 4, currentRow, data.studentsThree.get(i), format);
                this.sheet.addCell(cell);

                //колонки 6+ (по стипендиям)
                int column = 5;
                List<Integer> grantList = data.studentGrants.get(i);
                for (Integer count : grantList) {
                    cell = createCell(column++, currentRow, count, format);
                    this.sheet.addCell(cell);
                }

                currentRow++;
            }
        }
    }

    protected void loadFormativeOrgUnits() {
        //грузим список формирующих подр.
        MQBuilder builder = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "eou");
        builder.getSelectAliasList().clear();
        builder.addSelect(EducationOrgUnit.formativeOrgUnit().id().s());
        builder.setNeedDistinct(true);
        List<Long> idList = dao.getList(builder);

        List<OrgUnit> list = dao.getList(OrgUnit.class, idList, new String[]{OrgUnit.title().s()});
        list.add(0, null);// это сам вуз

        helper.calculate();

        //заполняем данные гля каждого FormOU
        this.fouData = new LinkedHashMap<Long, OrgUnitData>();
        for (OrgUnit orgUnit : list) {
            OrgUnitData data = new OrgUnitData();
            data.orgUnit = orgUnit;

            for (Map.Entry<String, Map<String, Object>> entry : this.detalization.entrySet()) {
                //возвращает список стдентов, для форм, и нужной степени детализации

                //колонка 1
                String name = entry.getKey();
                if ("".equals(name)) {
                    name = orgUnit == null ? "В целом по вузу" : orgUnit.getTitle();
                }
                data.rowsName.add(name);

                builder = helper.createStudentsBuilder(orgUnit != null ? Arrays.asList(orgUnit) : null, null);
                builder = helper.applyDetalization(builder, "st", entry.getValue());

                List<Long> studentList = this.dao.getList(builder);

                //колонка 2
                int count = studentList.size();
                data.studentsTotal.add(count);

                //колонка 3 Количество зачисленных первокурсников
                builder = helper.createStudentsBuilder(Arrays.asList(orgUnit), null);
                builder = helper.applyDetalization(builder, "st", entry.getValue());
                builder.add(MQExpression.eq("st", Student.course().intValue(), 1));

                count = dao.getCount(builder);
                data.studentsFirst.add(count);


                //колонка 4 (Количество студентов, имеющих академическую задолженность)
                count = helper.intersection(studentList, helper.getSetDebt()).size();
                data.studentsDebtors.add(count);

                //колонка 5  Количество обучающихся, имеющих "3"
                count = helper.intersection(studentList, helper.getSet3()).size();
                data.studentsThree.add(count);

                //колонки 6+ (по стипендиям)
                MQBuilder studentBuilder = helper.createStudentsBuilder(Arrays.asList(orgUnit), null);
                studentBuilder = helper.applyDetalization(builder, "st", entry.getValue());

                List<Integer> grantList = new ArrayList<Integer>();
                data.studentGrants.add(grantList);
                for (GrantData grantData : this.grantData.values()) {
                    builder = new MQBuilder(StudentGrantEntity.ENTITY_CLASS, "g")
                            .add(MQExpression.eq("g", StudentGrantEntity.view(), grantData.rel.getView()))
                            .add(MQExpression.eq("g", StudentGrantEntity.status().code(), StuGrantStatusCodes.CODE_1))
                            .add(MQExpression.eq("g", StudentGrantEntity.month(), this.grantMonth.getStringValue(this.grantYear)));
                    builder.getSelectAliasList().clear();
                    builder.addSelect(StudentGrantEntity.student().id().fromAlias("g").s());
                    builder.setNeedDistinct(true);

                    List<Long> list2 = this.dao.getList(builder);
                    helper.intersection(list2, studentList);
                    count = helper.intersection(list2, studentList).size();
                    grantList.add(count);
                }
            }

            this.fouData.put(orgUnit == null ? 0L : orgUnit.getId(), data);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////

    protected void fillGrantTitles() throws Exception {
        final int columnFirst = 4;
        final int rowFirst = 2;
        WritableCellFormat formatVert = createFormat(false, true);
        WritableCellFormat format = createFormat(false, false);

        int column = columnFirst;

        String prevType = null;
        int prevTypeColumn = Integer.MAX_VALUE;
        String prevNaim = null;
        int prevNaimColumn = Integer.MAX_VALUE;

        int counter = 1;
        for (GrantData data : this.grantData.values()) {
            //column = columnFirst + data.number;
            column = columnFirst + counter++;
            WritableCell cell;

            //тип стипендии
            if (prevType == null || !prevType.equals(data.rel.getType().getTitle())) {
                cell = createCell(column, rowFirst, data.rel.getType().getTitle(), formatVert);
                this.sheet.addCell(cell);

                if (prevType != null) {
                    this.sheet.mergeCells(prevTypeColumn, rowFirst, column - 1, rowFirst);
                }

                prevType = data.rel.getType().getTitle();
                prevTypeColumn = column;
            }

            //наименование стипендии
            if (prevNaim == null || !prevNaim.equals(data.rel.getGrant().getTitle())) {
                cell = createCell(column, rowFirst + 1, data.rel.getGrant().getTitle(), formatVert);
                this.sheet.addCell(cell);

                if (prevNaim != null) {
                    this.sheet.mergeCells(prevNaimColumn, rowFirst + 1, column - 1, rowFirst + 1);
                }

                prevNaim = data.rel.getGrant().getTitle();
                prevNaimColumn = column;
            }

            //вид стипендии
            cell = createCell(column, rowFirst + 2, data.rel.getView().getTitle(), formatVert);
            this.sheet.addCell(cell);

            //нумерация столбцов
            //cell = createCell(column, rowFirst + 3, 5+data.number, format);
            cell = createCell(column, rowFirst + 3, 5 + counter, format);
            this.sheet.addCell(cell);
        }

        this.maxColumn = column;

        if (this.maxColumn > prevTypeColumn)
            this.sheet.mergeCells(prevTypeColumn, rowFirst, this.maxColumn, rowFirst);
        if (this.maxColumn > prevNaimColumn)
            this.sheet.mergeCells(prevNaimColumn, rowFirst + 1, this.maxColumn, rowFirst + 1);
    }

    protected void loadGrans() {
        MQBuilder subBuilder = new MQBuilder(StudentGrantEntity.ENTITY_CLASS, "sg")
                .add(MQExpression.eq("sg", StudentGrantEntity.status().code(), StuGrantStatusCodes.CODE_1))
                .add(MQExpression.eq("sg", StudentGrantEntity.month(), this.grantMonth.getStringValue(this.grantYear)))
                .add(MQExpression.in("sg", StudentGrantEntity.student().id(), helper.createStudentsBuilder(null, null)));

        subBuilder.getSelectAliasList().clear();
        subBuilder.addSelect(StudentGrantEntity.view().id().fromAlias("sg").s());
        subBuilder.setNeedDistinct(true);

        MQBuilder builder = new MQBuilder(RelTypeGrantView.ENTITY_CLASS, "rel")
                .add(MQExpression.in("rel", RelTypeGrantView.view().id(), subBuilder))
                .addOrder("rel", RelTypeGrantView.type().title().s())
                .addOrder("rel", RelTypeGrantView.view().title().s())
                .addOrder("rel", RelTypeGrantView.grant().title().s());

        List<RelTypeGrantView> list = dao.getList(builder);

        this.grantData = new LinkedHashMap<Long, ReportGenerator.GrantData>();
        int counter = 1;
        for (RelTypeGrantView rel : list) {
            GrantData data = new GrantData();
            data.rel = rel;
            //data.number = counter++;

            this.grantData.put(rel.getView().getId(), data);
        }

    }

    protected void loadTemplate() throws Exception {
        TemplateDocument template = dao.getCatalogItem(TemplateDocument.class, TEMPLATE);
        ByteArrayInputStream in = new ByteArrayInputStream(template.getContent());
        Workbook tempalteWorkbook = Workbook.getWorkbook(in);

        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);

        this.workbookStream = new ByteArrayOutputStream();
        this.workbook = Workbook.createWorkbook(this.workbookStream, tempalteWorkbook, ws);
        this.sheet = this.workbook.getSheet(0);
    }

    ////////////////////////////////////////////////////////////////////////////////////////

    protected static WritableCellFormat createFormat(boolean bold, boolean vertical) throws Exception {
        WritableFont font = new WritableFont(WritableFont.TIMES, 12);
        if (bold)
            font.setBoldStyle(WritableFont.BOLD);

        WritableCellFormat format = new WritableCellFormat(font);
        format.setAlignment(Alignment.CENTRE);
        format.setVerticalAlignment(VerticalAlignment.BOTTOM);
        format.setWrap(true);

        if (vertical)
            format.setOrientation(Orientation.PLUS_90);

        format.setBorder(Border.ALL, BorderLineStyle.THIN);

        return format;
    }

    protected static WritableCell createCell(int col, int row, Object value, WritableCellFormat format) throws Exception {
        if (format == null)
            format = createFormat(false, false);

        if (value instanceof Integer)
            return new jxl.write.Number(col, row, ((Integer) value) + 0.0, format);
        return new Label(col, row, value.toString(), format);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    protected static class OrgUnitData {
        protected OrgUnit orgUnit;

        protected List<String> rowsName = new ArrayList<String>();
        protected List<Integer> studentsTotal = new ArrayList<Integer>();
        protected List<Integer> studentsFirst = new ArrayList<Integer>();
        protected List<Integer> studentsDebtors = new ArrayList<Integer>();
        protected List<Integer> studentsThree = new ArrayList<Integer>();
        protected List<List<Integer>> studentGrants = new ArrayList<List<Integer>>();
    }

    protected static class GrantData {
        protected RelTypeGrantView rel;
        protected int number;
    }

}
 