package ru.tandemservice.narfu.component.sessionBulletin.SessionBulletinEdit;

import ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinEdit.Model;

public class DAO
        extends
        ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinEdit.DAO
{


    /**
     * Нужно убрать логику, запрещающую редактирование ведомости, если введена хотя бы одна оценка,
     * т.е. должна быть возможность редактировать любую незакрытую ведомость. RM#3510
     */
    @Override
    protected void checkBulletinHasMark(Model model) {
    }
}
