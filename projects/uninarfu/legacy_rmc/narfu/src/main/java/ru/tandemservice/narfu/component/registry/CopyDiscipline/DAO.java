package ru.tandemservice.narfu.component.registry.CopyDiscipline;


import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.registry.*;
import ru.tandemservice.uniepp.entity.settings.EppTutorOrgUnit;

import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;

public class DAO extends UniDao<Model>
{

    @Override
    public void prepare(final Model model)
    {
        model.setTutorOuModel(new DQLFullCheckSelectModel()
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(OrgUnit.class, alias);
                DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(EppTutorOrgUnit.class, "t")
                        .column(property(EppTutorOrgUnit.orgUnit().id().fromAlias("t")));
                if (model.getDiscipline().getOwner() != null)
                {
                    builder.where(or(
                            in(property(OrgUnit.id().fromAlias(alias)), subBuilder.buildQuery()),
                            eq(property(OrgUnit.id().fromAlias(alias)), value(model.getDiscipline().getOwner().getId()))));
                } else
                {
                    builder.where(in(property(OrgUnit.id().fromAlias(alias)), subBuilder.buildQuery()));
                }

                if (filter != null)
                    builder.where(DQLExpressions.like(DQLFunctions.upper(property(OrgUnit.title().fromAlias(alias))), value(CoreStringUtils.escapeLike(filter, true))));

                return builder;
            }
        });
    }

    @Override
    public void update(Model model)
    {
        // Клонируем дисциплину
        EppRegistryElement element = model.getDiscipline();

        long oldId = element.getId();
        getSession().evict(element);
        element.setId(null);
        element.setNumber(null);
        element.setNumber(INumberQueueDAO.instance.get().getNextNumber(element));
        save(element);
        DQLSelectBuilder builder;

        // клонируем Часть элемента реестра
        builder = new DQLSelectBuilder().fromEntity(EppRegistryElementPart.class, "e")
                .where(eq(property(EppRegistryElementPart.registryElement().id().fromAlias("e")), value(oldId)));

        List<EppRegistryElementPart> list2 = getList(builder);
        for (EppRegistryElementPart p : list2)
        {
            cloneEppRegistryElementPart(p, element);
        }
        //клонируем Нагрузка элемента реестра (всего)
        builder = new DQLSelectBuilder().fromEntity(EppRegistryElementLoad.class, "l")
                .where(eq(property("l", EppRegistryElementLoad.registryElement().id()), value(oldId)));
        Collection<EppRegistryElementLoad> elementLoadList = getList(builder);
        for (EppRegistryElementLoad elementLoad : elementLoadList)
        {
            cloneEppRegistryElementLoad(elementLoad, element);
        }
    }

    private void cloneEppRegistryElementLoad(EppRegistryElementLoad entity, EppRegistryElement element)
    {
        EppRegistryElementLoad newEntity = new EppRegistryElementLoad();
        newEntity.update(entity);
        newEntity.setId(null);
        newEntity.setRegistryElement(element);
        save(newEntity);
    }

    private void cloneEppRegistryElementPart(EppRegistryElementPart entity, EppRegistryElement element)
    {
        getSession().evict(entity);
        long oldId = entity.getId();
        entity.setId(null);
        entity.setRegistryElement(element);
        save(entity);

        // Копируем EppRegistryElementPartModule
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppRegistryElementPartModule.class, "e")
                .where(
                        eq(
                                property(EppRegistryElementPartModule.part().id().fromAlias("e")), value(oldId)));
        List<EppRegistryElementPartModule> list = getList(builder);

        for (EppRegistryElementPartModule e : list)
        {
            getSession().evict(e);
            e.setId(null);
            e.setPart(entity);
            getSession().save(e);
        }

        // Копируем EppRegistryElementPartFControlAction
        builder = new DQLSelectBuilder().fromEntity(EppRegistryElementPartFControlAction.class, "e")
                .where(
                        eq(
                                property(EppRegistryElementPartFControlAction.part().id().fromAlias("e")), value(oldId)));
        List<EppRegistryElementPartFControlAction> list2 = getList(builder);

        for (EppRegistryElementPartFControlAction e : list2)
        {
            getSession().evict(e);
            e.setId(null);
            e.setPart(entity);
            getSession().save(e);
        }
    }
}
