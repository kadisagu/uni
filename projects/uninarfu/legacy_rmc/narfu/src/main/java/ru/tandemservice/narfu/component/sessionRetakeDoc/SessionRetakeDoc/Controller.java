package ru.tandemservice.narfu.component.sessionRetakeDoc.SessionRetakeDoc;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.uniepp.dao.student.EppStudentSlotDAO;
import ru.tandemservice.unisession.base.bo.SessionMark.daemon.SessionMarkDaemonBean;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    public void onRefreshComponent(IBusinessComponent component) {
        Model model = component.getModel();
        ((IDAO) getDao()).prepare(model);
    }

    public void onClickApply(IBusinessComponent component) {

        Model model = component.getModel();

        if (EppStudentSlotDAO.DAEMON.wakeUpAndWaitDaemon(60) == null)
            throw new ApplicationException("Не удается обновить состояние студентов.");

        if (SessionMarkDaemonBean.DAEMON.wakeUpAndWaitDaemon(60) == null)
            throw new ApplicationException("Не удается обновить оценки студентов.");

        ((IDAO) getDao()).doCreateDocuments(model);

        deactivate(component);
    }

}
