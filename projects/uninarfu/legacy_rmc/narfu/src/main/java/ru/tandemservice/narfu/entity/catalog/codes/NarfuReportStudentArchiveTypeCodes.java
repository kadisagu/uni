package ru.tandemservice.narfu.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип отчета Опись сдачи в архив личных дел отчисленных/выпускников"
 * Имя сущности : narfuReportStudentArchiveType
 * Файл data.xml : catalogs.data.xml
 */
public interface NarfuReportStudentArchiveTypeCodes
{
    /** Константа кода (code) элемента : exclusion (code). Название (title) : Опись сдачи в архив личных дел отчисленных */
    String EXCLUSION = "exclusion";
    /** Константа кода (code) элемента : general (code). Название (title) : Опись сдачи в архив личных дел выпускников */
    String GENERAL = "general";

    Set<String> CODES = ImmutableSet.of(EXCLUSION, GENERAL);
}
