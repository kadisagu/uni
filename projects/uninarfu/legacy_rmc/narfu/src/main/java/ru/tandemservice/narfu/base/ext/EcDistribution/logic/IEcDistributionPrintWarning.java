package ru.tandemservice.narfu.base.ext.EcDistribution.logic;

import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgDistribution;

public interface IEcDistributionPrintWarning {
    public static final SpringBeanCache<IEcDistributionPrintWarning> instance = new SpringBeanCache<IEcDistributionPrintWarning>("EcDistributionPrintWarning");

    public RtfDocument getPrintForm(byte[] template, IEcgDistribution iecgdistribution);
}
