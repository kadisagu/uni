package ru.tandemservice.narfu.component.settings.StateExamSubjectPassScore;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

public class Controller extends ru.tandemservice.uniec.component.settings.StateExamSubjectPassScore.Controller {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        super.onRefreshComponent(component);
        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        DynamicListDataSource<Model.SubjectWrapper> dataSource = model.getDataSource();

        if (dataSource.getColumn("markPrev") != null)
            return;

        dataSource.addColumn(new BlockColumn("markPrev", "Зачетный балл (прошлый год)").setWidth(10), 2);
        dataSource.addColumn(new BlockColumn("markPrev2", "Зачетный балл (позапрошлый год)").setWidth(10), 3);
    }
}
