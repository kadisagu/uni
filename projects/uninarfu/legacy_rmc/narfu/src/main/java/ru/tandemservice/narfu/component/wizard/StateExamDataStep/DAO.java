package ru.tandemservice.narfu.component.wizard.StateExamDataStep;

import ru.tandemservice.narfu.component.entrant.StateExamUtil;
import ru.tandemservice.uniec.component.entrant.EntrantStateExamCertificateAddEdit.Model;

public class DAO extends ru.tandemservice.uniec.component.wizard.StateExamDataStep.DAO {

    @Override
    public void update(Model model) {
        StateExamUtil.update(model, getSession());
    }
}
