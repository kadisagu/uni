package ru.tandemservice.narfu.base.ext.EcDistribution.ui.Pub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.IPresenterExtensionBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.uniec.base.bo.EcDistribution.ui.Pub.EcDistributionPub;

@Configuration
public class EcDistributionPubExt extends BusinessComponentExtensionManager {


    @Autowired
    private EcDistributionPub ecDistributionPub;

    @Bean
    public PresenterExtension presenterExtension()
    {
        IPresenterExtensionBuilder pi = presenterExtensionBuilder(ecDistributionPub.presenterExtPoint());
        pi.addAddon(uiAddon("ui_addon", EcDistributionPubUIExt.class));
        return pi.create();
    }

}
