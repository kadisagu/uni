package ru.tandemservice.narfu.component.documents.d101.Add;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import org.tandemframework.shared.person.base.util.PersonSecurityUtil;
import org.tandemframework.shared.person.catalog.entity.Benefit;
import ru.tandemservice.narfu.entity.StudentDocumentRMC;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseDAO;
import ru.tandemservice.uni.component.documents.DocumentAddBase.IDocumentAddBaseDAO;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.StudentDocumentType;
import ru.tandemservice.uni.entity.catalog.UniScriptItem;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.StudentDocument;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

import java.util.*;

//--------------------------------------------------------------
//--------------------------------------------------------------

@SuppressWarnings("rawtypes")
public class DAO extends DocumentAddBaseDAO<Model> implements IDocumentAddBaseDAO<Model>, IDAO
{
    @Override
    public void prepare(Model model)
    {
//        super.prepare(model);
        model.setStudent((Student) getNotNull(Student.class, model.getStudent().getId()));
        model.setStudentDocumentType((StudentDocumentType) getNotNull(StudentDocumentType.class, model.getStudentDocumentType().getId()));

        if (model.getNumber() == 0) {
            Criteria criteria = getCheckCriteria(new Date(), model.getStudent());
            criteria.setProjection(Projections.max("number"));
            Number number = (Number) criteria.uniqueResult();
            int max = number != null ? number.intValue() : 0;
            model.setNumber(max + 1);
        }
        //льготы
        Criteria criteria = getSession().createCriteria(PersonBenefit.class);
        criteria.createAlias(PersonBenefit.L_BENEFIT, "benefit");
        criteria.add(Restrictions.eq(PersonBenefit.L_PERSON, model.getStudent().getPerson()));
        criteria.addOrder(Order.asc("benefit." + Benefit.P_TITLE));
        criteria.setProjection(Projections.property("benefit." + Benefit.P_TITLE));

        List<String> result = criteria.list();

        model.setBenefits(StringUtils.join(result, ", "));
        //приказ о зачислении
        MQBuilder builder = new MQBuilder("ru.tandemservice.uniec.entity.orders.EnrollmentExtract", "e");
        builder.addJoinFetch("e", EnrollmentExtract.paragraph().order().s(), "o");
        builder.add(MQExpression.like("o", EnrollmentOrder.type().title().s(), "%зачислении%"));
        builder.add(MQExpression.eq("e", EnrollmentExtract.studentNew().id().s(), model.getStudent().getId()));
        List<EnrollmentExtract> list = builder.getResultList(getSession());
        model.setEnrollmentExtractList(list);
        model.setTargetPlace("");

        model.setFormingDate(new Date());
        model.setStudentTitleStr(PersonManager.instance().declinationDao().getDeclinationFIO(model.getStudent().getPerson().getIdentityCard(), GrammaCase.DATIVE));
        model.setCourseList(DevelopGridDAO.getCourseList());
        model.setCourse(model.getStudent().getCourse());
        model.setDepartmentList(Arrays.asList(
                new IdentifiableWrapper(0L, "дневном"),
                new IdentifiableWrapper(1L, "вечернем")
        ));
        model.setDepartment(model.getDepartmentList().get(0));
        model.setDocumentForTitle("Справка выдана для предъявления по месту требования.");

        OrgUnit formativeOrgUnit = model.getStudent().getEducationOrgUnit().getFormativeOrgUnit();
        String managerPostTitle;
        if ((formativeOrgUnit.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.INSTITUTE)) || (formativeOrgUnit.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.BRANCH)))
            managerPostTitle = "Директор ";
        else
            managerPostTitle = "Декан ";
        managerPostTitle = managerPostTitle + (formativeOrgUnit.getGenitiveCaseTitle() == null ? formativeOrgUnit.getTitle() : formativeOrgUnit.getGenitiveCaseTitle());
        model.setManagerPostTitle(managerPostTitle);

        model.setPhone(formativeOrgUnit.getPhone());

        //Фио руководителя
        EmployeePost employeePost = EmployeeManager.instance().dao().getHead(formativeOrgUnit);

        if (employeePost != null)
            model.setManagerFio(employeePost.getPerson().getIdentityCard().getIof());
        else
            model.setManagerFio("");
       /*if (!formativeOrgUnit.getManagerTitle().isEmpty()){
           String manegerFIO = StringUtils.split( model.getStudent().getEducationOrgUnit().getFormativeOrgUnit().getManagerTitle(), ":")[0];
    	   model.setManagerFio(manegerFIO);
       }
       else 
    	   model.setManagerFio("");
       */
        model.setEducationYearModel(new LazySimpleSelectModel<EducationYear>(EducationYear.class));
        model.setEducationYear(get(EducationYear.class, EducationYear.current(), Boolean.TRUE));
        if (!model.getStudent().getStatus().getCode().equals(UniDefines.CATALOG_STUDENT_STATUS_EXCLUDED) || !model.getStudent().getStatus().getCode().equals(UniDefines.CATALOG_STUDENT_STATUS_GRADUATED_WITH_DIPLOMA))
            model.setDateEnd(getEndDate(model));

        //-----------------------------------------------------------------------------------------------------------------        
//        model.setExecutor(PersonSecurityUtil.getExecutor());

        //ФИО исполнителя
        Person currentUser = PersonManager.instance().dao().getPerson(ContextLocal.getUserContext().getPrincipalContext());
        if (currentUser != null)
        {
            model.setExecutor(currentUser.getFullFio());
        }
        else
        {
            model.setExecutor(PersonSecurityUtil.getExecutor());
        }
        //-----------------------------------------------------------------------------------------------------------------        
    }

    @Override
    public void update(Model model) {
        Session session = getSession();
        Criteria criteria = getCheckCriteria(new Date(), model.getStudent());
        criteria.add(Restrictions.eq("number", Integer.valueOf(model.getNumber())));
        List list = criteria.list();
        if (!list.isEmpty()) {
            IdentityCard card = ((StudentDocument) list.get(0)).getStudent().getPerson().getIdentityCard();
            String sexTitle = "1".equals(card.getSex().getCode()) ? "\u0441\u0442\u0443\u0434\u0435\u043D\u0442\u0430" : "\u0441\u0442\u0443\u0434\u0435\u043D\u0442\u043A\u0438";
            ContextLocal.getErrorCollector().add((new StringBuilder()).append("\u041D\u043E\u043C\u0435\u0440 \u0434\u043E\u043A\u0443\u043C\u0435\u043D\u0442\u0430 \u0434\u043E\u043B\u0436\u0435\u043D \u0431\u044B\u0442\u044C \u0443\u043D\u0438\u043A\u0430\u043B\u044C\u043D\u044B\u043C. \u041F\u043E\u0434 \u043D\u043E\u043C\u0435\u0440\u043E\u043C \253").append(model.getNumber()).append("\273 \u0443\u0436\u0435 \u0432\u044B\u0434\u0430\u043D \u0434\u0440\u0443\u0433\u043E\u0439 \u0434\u043E\u043A\u0443\u043C\u0435\u043D\u0442 \u0434\u043B\u044F ").append(sexTitle).append(" \253").append(card.getFullFio()).append("\273.").toString(), new String[]{
                    "number"
            });
            return;
        }
        else {
            final UniScriptItem script =  model.getStudentDocumentType().getScriptItem();
            final Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(script, model);
            RtfDocument document = RtfDocument.fromByteArray((byte[])scriptResult.get("document"));
            DatabaseFile databaseFile = new DatabaseFile();
            databaseFile.setContent(RtfUtil.toByteArray(document));
            session.save(databaseFile);
            StudentDocument studentDocument = new StudentDocument();
            studentDocument.setNumber(model.getNumber());
            studentDocument.setFormingDate(new Date());
            studentDocument.setStudent(model.getStudent());
            studentDocument.setContent(databaseFile);
            studentDocument.setStudentDocumentType(model.getStudentDocumentType());

            saveOrUpdate(studentDocument);

            StudentDocumentRMC documentRMC = new StudentDocumentRMC();
            documentRMC.setDateEndStudy(model.getDateEnd());
            documentRMC.setBase(studentDocument);
            saveOrUpdate(documentRMC);

        }
    }

    private Criteria getCheckCriteria(Date date, Student student)
    {
        int year = CoreDateUtils.getYear(date);
        Date first = CoreDateUtils.getYearFirstTimeMoment(year);
        Date last = CoreDateUtils.getYearLastTimeMoment(year);
        Criteria c = getSession().createCriteria(ru.tandemservice.uni.entity.employee.StudentDocument.class, "d");
        c.createAlias("d.student", "s");
        c.createAlias("s.educationOrgUnit", "e");
        c.add(Restrictions.between("d.formingDate", first, last));
        c.add(Restrictions.eq("e.formativeOrgUnit", student.getEducationOrgUnit().getFormativeOrgUnit()));
        c.add(Restrictions.eq("e.developForm", student.getEducationOrgUnit().getDevelopForm()));
        return c;
    }

    public Date getEndDate(Model model) {

        EppStudent2EduPlanVersion rel = IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionRelation(model.getStudent().getId());

        EppEduPlanVersion version = null;
        if (rel != null)
            version = rel.getEduPlanVersion();
        else
            return null;

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppEduPlanVersionWeekType.class, "rel")
                .where(DQLExpressions.eqValue(DQLExpressions.property(EppEduPlanVersionWeekType.eduPlanVersion().id().fromAlias("rel")), version.getId()))
                .order(DQLExpressions.property(EppEduPlanVersionWeekType.course().intValue().fromAlias("rel")), OrderDirection.desc)
                .order(DQLExpressions.property(EppEduPlanVersionWeekType.week().number().fromAlias("rel")), OrderDirection.desc)
                .column("rel");

        List<EppEduPlanVersionWeekType> lst = getList(builder);

        if (!lst.isEmpty()) {
            EppEduPlanVersionWeekType relWeek = lst.get(0);

            String dateStr = relWeek.getWeek().getTitle();

            String[] s = dateStr.split(" ");
            String[] s1 = dateStr.split(" - ");

            int date = Integer.valueOf(StringUtils.split(s1[1], " ")[0]).intValue();
            int month = s.length == 5 ? relWeek.getWeek().getMonth() : relWeek.getWeek().getMonth() - 1;

//    		int lastCourse = IDevelopGridDAO.instance.get().getDevelopGridDetail(relWeek.getEduPlanVersion().getEduPlan().getDevelopGrid()).keySet().size();
//    		relWeek.getCourse().getIntValue();
            int a = relWeek.getCourse().getIntValue() - model.getStudent().getCourse().getIntValue();

            if (a < 0)
                return null;

            int year = model.getEducationYear().getIntValue() + 1 + a;

            Calendar c = Calendar.getInstance();

            c.set(year, month, date, 0, 0, 0);

            return c.getTime();
        }

        return null;
    }


}
