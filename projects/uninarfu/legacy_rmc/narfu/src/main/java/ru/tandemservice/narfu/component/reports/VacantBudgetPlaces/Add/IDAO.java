package ru.tandemservice.narfu.component.reports.VacantBudgetPlaces.Add;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {
    public void createReport(Model model);
}
