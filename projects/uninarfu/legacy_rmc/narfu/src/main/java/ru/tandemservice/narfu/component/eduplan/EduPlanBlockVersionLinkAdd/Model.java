package ru.tandemservice.narfu.component.eduplan.EduPlanBlockVersionLinkAdd;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.uni.ui.SelectModel;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

import java.util.Arrays;

@org.tandemframework.core.component.State({@Bind(key = "blockId", binding = "blockId")})
public class Model {
    private Long blockId;
    public static final Long BLOCK_SOURCE_ALL = 1L;
    public static final Long BLOCK_SOURCE_SAME_LEVEL = 2L;
    public static final Long BLOCK_SOURCE_SAME_EDUHS = 3L;

    private final SelectModel<IdentifiableWrapper> blockSourceModel = new SelectModel<>(Arrays.asList(new IdentifiableWrapper[]{new IdentifiableWrapper(BLOCK_SOURCE_ALL, "На все направления"), new IdentifiableWrapper(BLOCK_SOURCE_SAME_LEVEL, "На направления текущего уровня"), new IdentifiableWrapper(BLOCK_SOURCE_SAME_EDUHS, "На текущее направление")}));

    private final SelectModel<EppEduPlanVersionBlock> blockModel = new SelectModel<>();

    public Long getBlockId() {
        return blockId;
    }

    public void setBlockId(Long blockId) {
        this.blockId = blockId;
    }

    public SelectModel<IdentifiableWrapper> getBlockSourceModel() {
        return this.blockSourceModel;
    }

    public SelectModel<EppEduPlanVersionBlock> getBlockModel() {
        return this.blockModel;
    }
}
