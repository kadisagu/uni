package ru.tandemservice.narfu.component.reports.ExcludedAspirants.List;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.aspirantrmc.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.movestudentrmc.entity.DocOrdRepresent;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.ListOrdListRepresent;
import ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents;
import ru.tandemservice.narfu.component.reports.ExcludedAspirants.ReportData;
import ru.tandemservice.narfu.uniec.entity.report.ExcludedAspirantsReport;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unibase.UniBaseUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.*;

public class DAO extends UniDao<Model> implements IDAO {
    public static final String TEMPLATE_NAME = "narfuExcludedAspirantsReport";

    public static final String EXCLUDE_REPRESENT = "exclude";
    public static final String EXCLUDE_LIST_REPRESENT = "excludeList";
    public static final String WEEKEND_REPRESENT = "weekend";
    public static final String MEDICAL_REASON = "narfu-1.13";
    public static final String CHILD_REASON = "narfu-1.16";
    public static final String EDUCATION_REASON = "narfu-1.4";

    public static final String PLAN_ROW = "11-01";
    public static final String TARGET_ROW = "11-02";
    public static final String TOTAL_ROW = "11-03";

    public static final List<String> STATUS_CODES_LIST = Arrays.asList("1", "2", "3", "4");


    @Override
    public void prepare(final Model model) {
        if (null != model.getOrgUnitId()) {
            model.setOrgUnit((OrgUnit) getNotNull(model.getOrgUnitId()));
            model.setSecModel(new OrgUnitSecModel(model.getOrgUnit()));
        }
    }

    @Override
    public void prepareListDataSource(Model model) {

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(ExcludedAspirantsReport.class, "report");
//        if(!CollectionUtils.isEmpty(educationLevelsHighSchoolList))
//        {
//            builder.where(DQLExpressions.in(DQLExpressions.property(OrgUnitAspirantReport.educationLevelHighSchool().fromAlias("report")), educationLevelsHighSchoolList));
//        }
//        if(enrollmentCampaign!=null)
//        {
//            builder.where(DQLExpressions.eq(DQLExpressions.property(OrgUnitAspirantReport.enrollmentCampaign().fromAlias("report")), DQLExpressions.value(enrollmentCampaign)));
//        }


        DQLOrderDescriptionRegistry descriptionRegistry = new DQLOrderDescriptionRegistry(ExcludedAspirantsReport.class, "report");
        descriptionRegistry.applyOrder(builder, model.getDataSource().getEntityOrder());

        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }

    @Override
    public ExcludedAspirantsReport createReport(Model model) {

        ReportData reportData = new ReportData();

        model.getReport().setFormingDate(new Date());


        try {
            reportData.setReport(model.getReport());
            reportData.setModel(model);
            loadTemplate(reportData);
            fillReport(reportData, model);

            reportData.getWorkbook().write();
            reportData.getWorkbook().close();

            DatabaseFile reportFile = new DatabaseFile();
            reportFile.setContent(reportData.getWorkbookStream().toByteArray());
            reportFile.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_VND_MS_EXCEL);
            reportFile.setFilename("Report.xls");

            save(reportFile);
            model.getReport().setContent(reportFile);
            save(model.getReport());
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new ApplicationException(e.getMessage(), e);
        }

        return model.getReport();
    }

    protected void fillReport(ReportData reportData, Model model) throws Exception {

        Map<Long, String> studentsToRepresentsRel = getRepresentations(model);
        Map<String, Integer[]> count = sortStudentData(studentsToRepresentsRel);


        //List<Entrant> entrantList = getEntrantList(model);
//        Map<String, Integer[]> sortedMap = sortEntrantsByCitizenship(entrantList);
//        fillPlanCount(sortedMap, model);
        print(reportData, count);

    }

    protected void loadTemplate(ReportData reportData) throws Exception {
        TemplateDocument template = getCatalogItem(TemplateDocument.class, TEMPLATE_NAME);
        ByteArrayInputStream in = new ByteArrayInputStream(template.getContent());

        Workbook templateWBook = Workbook.getWorkbook(in);
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);

        ByteArrayOutputStream wbOS = new ByteArrayOutputStream();
        WritableWorkbook writableWorkbook = Workbook.createWorkbook(wbOS, templateWBook, ws);

        reportData.setWorkbookStream(wbOS);
        reportData.setWorkbook(writableWorkbook);
    }

    private Map<Long, String> getRepresentations(Model model)
    {
        Map<Long, String> retVal = new HashMap<>();
        Map<Long, Date> lastRepresentDate = new HashMap<>();
        findRepresents(model, retVal, lastRepresentDate);
        findListRepresents(model, retVal, lastRepresentDate);
        return retVal;
    }

    private void findRepresents(Model model, Map<Long, String> retVal, Map<Long, Date> lastRepresentDate)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(DocRepresentStudentBase.class, "r")
                .joinEntity("r", DQLJoinType.inner, DocOrdRepresent.class, "o", DQLExpressions.eq(DQLExpressions.property(DocOrdRepresent.representation().fromAlias("o")), DQLExpressions.property(DocRepresentStudentBase.representation().fromAlias("r"))));
        dql.where(DQLExpressions.or(
                DQLExpressions.eq(DQLExpressions.property(DocRepresentStudentBase.representation().type().code().fromAlias("r")), DQLExpressions.value(EXCLUDE_REPRESENT)),
                DQLExpressions.eq(DQLExpressions.property(DocRepresentStudentBase.representation().type().code().fromAlias("r")), DQLExpressions.value(WEEKEND_REPRESENT))
        ));
        dql.where(DQLExpressions.eq(DQLExpressions.property(DocRepresentStudentBase.student().compensationType().code().fromAlias("r")), DQLExpressions.value(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET)));
        dql.where(DQLExpressions.eq(DQLExpressions.property(DocRepresentStudentBase.student().studentCategory().code().fromAlias("r")), DQLExpressions.value(StudentCategoryCodes.STUDENT_CATEGORY_ASPIRANT)));
        dql.where(DQLExpressions.eq(DQLExpressions.property(DocRepresentStudentBase.student().person().identityCard().citizenship().code().fromAlias("r")), DQLExpressions.value("0")));
        dql.where(DQLExpressions.eq(DQLExpressions.property(DocRepresentStudentBase.representation().committed().fromAlias("r")), DQLExpressions.value(true)));

        dql.column(DQLExpressions.property(DocRepresentStudentBase.student().id().fromAlias("r")));
        dql.column(DQLExpressions.property(DocRepresentStudentBase.representation().type().code().fromAlias("r")));
        dql.column(DQLExpressions.property(DocRepresentStudentBase.representation().reason().code().fromAlias("r")));
        dql.column(DQLExpressions.property(DocOrdRepresent.order().commitDate().fromAlias("o")));
        List<Object[]> list = getList(dql);

        for (Object[] item : list) {
            Long studentId = (Long) item[0];
            String repCode = (String) item[1];
            String reasCode = (String) item[2];
            Date commitDate = (Date) item[3];
            if (lastRepresentDate.containsKey(studentId)) {
                if (lastRepresentDate.get(studentId).after(commitDate))
                    continue;
            }
            lastRepresentDate.put(studentId, commitDate);
            retVal.put(studentId, repCode + reasCode);
        }

    }

    private void findListRepresents(Model model, Map<Long, String> retVal, Map<Long, Date> lastRepresentDate)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(RelListRepresentStudents.class, "r")
                .joinEntity("r", DQLJoinType.inner, ListOrdListRepresent.class, "o", DQLExpressions.eq(DQLExpressions.property(ListOrdListRepresent.representation().fromAlias("o")), DQLExpressions.property(DocRepresentStudentBase.representation().fromAlias("r"))));
        dql.where(DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudents.representation().type().code().fromAlias("r")), DQLExpressions.value(EXCLUDE_LIST_REPRESENT)));

        dql.where(DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudents.student().compensationType().code().fromAlias("r")), DQLExpressions.value(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET)));
        dql.where(DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudents.student().studentCategory().code().fromAlias("r")), DQLExpressions.value(StudentCategoryCodes.STUDENT_CATEGORY_ASPIRANT)));
        dql.where(DQLExpressions.eq(DQLExpressions.property(DocRepresentStudentBase.student().person().identityCard().citizenship().code().fromAlias("r")), DQLExpressions.value("0")));
        dql.where(DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudents.representation().committed().fromAlias("r")), DQLExpressions.value(true)));

        dql.column(DQLExpressions.property(RelListRepresentStudents.student().id().fromAlias("r")));
        dql.column(DQLExpressions.property(RelListRepresentStudents.representation().type().code().fromAlias("r")));
        dql.column(DQLExpressions.property(RelListRepresentStudents.representation().representationReason().code().fromAlias("r")));
        dql.column(DQLExpressions.property(ListOrdListRepresent.order().commitDate().fromAlias("o")));

        List<Object[]> list = getList(dql);
        for (Object[] item : list) {
            Long studentId = (Long) item[0];
            String repCode = (String) item[1];
            String reasCode = (String) item[2];
            Date commitDate = (Date) item[3];
            if (lastRepresentDate.containsKey(studentId)) {
                if (lastRepresentDate.get(studentId).after(commitDate))
                    continue;
            }
            lastRepresentDate.put(studentId, commitDate);
            retVal.put(studentId, repCode + reasCode);
        }


    }

    private void fillAspirantCount(Integer[] count)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(Student.class, "s");
        dql.where(DQLExpressions.eq(DQLExpressions.property(Student.person().identityCard().citizenship().code().fromAlias("s")), DQLExpressions.value("0")));
        dql.where(DQLExpressions.eq(DQLExpressions.property(Student.studentCategory().code().fromAlias("s")), DQLExpressions.value(StudentCategoryCodes.STUDENT_CATEGORY_ASPIRANT)));
        dql.where(DQLExpressions.eq(DQLExpressions.property(Student.compensationType().code().fromAlias("s")), DQLExpressions.value(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET)));
        dql.where(DQLExpressions.in(DQLExpressions.property(Student.status().code().fromAlias("s")), STATUS_CODES_LIST));
        dql.column(DQLFunctions.countStar());
        Long total = dql.createStatement(getSession()).uniqueResult();
        count[0] = total.intValue();
    }

    private Map<String, Integer[]> sortStudentData(Map<Long, String> studentToRepresentMap)
    {
        Integer[] count = new Integer[]{0, 0, 0, 0, 0};

        for (Map.Entry<Long, String> entry : studentToRepresentMap.entrySet()) {
            String key = entry.getValue();
            if (key.contains(MEDICAL_REASON)) {
                count[1]++;
                continue;
            }
            if (key.equals(WEEKEND_REPRESENT + CHILD_REASON)) {
                count[2]++;
                continue;
            }
            if (key.equals(EXCLUDE_REPRESENT + EDUCATION_REASON)
                    || key.equals(EXCLUDE_LIST_REPRESENT + EDUCATION_REASON))
            {
                count[3]++;
                continue;
            }
            count[4]++;
        }
        fillAspirantCount(count);

        Map<String, Integer[]> retVal = new HashMap<>();

        retVal.put(PLAN_ROW, count);
        retVal.put(TARGET_ROW, new Integer[]{0, 0, 0, 0, 0});
        retVal.put(TOTAL_ROW, count);
        return retVal;
    }

    private void print(ReportData reportData, Map<String, Integer[]> sortedMap)
    {

        int firstRow = 6;
        for (Map.Entry<String, Integer[]> entry : sortedMap.entrySet()) {
            int firstColumn = 2;
            try {
                appendRow(firstColumn, firstRow, entry, reportData.getWorkbook().getSheet(0));
                firstRow++;
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private void appendRow(int column, int row, Map.Entry<String, Integer[]> rowData, WritableSheet sheet) throws Exception {

        WritableFont font = new WritableFont(WritableFont.TIMES, 9);
        WritableCellFormat format = new WritableCellFormat(font);
        format.setVerticalAlignment(VerticalAlignment.BOTTOM);
        format.setAlignment(Alignment.CENTRE);
        format.setWrap(true);
        format.setBorder(Border.ALL, BorderLineStyle.THIN);
        for (int i = 0; i < rowData.getValue().length; i++) {
            sheet.addCell(new jxl.write.Number(column + i, row, rowData.getValue()[i], format));
        }


    }


}
