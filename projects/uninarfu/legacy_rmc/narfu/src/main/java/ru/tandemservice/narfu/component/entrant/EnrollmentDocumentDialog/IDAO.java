package ru.tandemservice.narfu.component.entrant.EnrollmentDocumentDialog;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {

    public byte[] printEnrollmentDocument(Model model);
}
