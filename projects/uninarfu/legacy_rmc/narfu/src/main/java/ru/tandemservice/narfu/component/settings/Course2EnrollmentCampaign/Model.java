package ru.tandemservice.narfu.component.settings.Course2EnrollmentCampaign;

import org.tandemframework.tapsupport.component.selection.ISelectModel;

import java.util.ArrayList;
import java.util.List;


public class Model {
    private ISelectModel enrollmentCampaignModel;
    private List<Wrapper> course2Campaigns = new ArrayList<>();
    private Wrapper currentCourse;

    public ISelectModel getEnrollmentCampaignModel() {
        return enrollmentCampaignModel;
    }

    public void setEnrollmentCampaignModel(ISelectModel enrollmentCampaignModel) {
        this.enrollmentCampaignModel = enrollmentCampaignModel;
    }

    public List<Wrapper> getCourse2Campaigns() {
        return course2Campaigns;
    }

    public void setCourse2Campaigns(List<Wrapper> course2Campaigns) {
        this.course2Campaigns = course2Campaigns;
    }

    public Wrapper getCurrentCourse() {
        return currentCourse;
    }

    public void setCurrentCourse(Wrapper currentCourse) {
        this.currentCourse = currentCourse;
    }

}
