package ru.tandemservice.narfu.component.studentmassprint.GrantsMassPrint;

import org.tandemframework.sec.runtime.SecurityRuntime;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudentrmc.entity.catalog.Grant;
import ru.tandemservice.narfu.entity.NarfuGrantDocument;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Model extends ru.tandemservice.unirmc.component.studentmassprint.Base.Model
{

    private ISelectModel grantsModel;
    private Grant grant;

    private ISelectModel educationYearModel;
    private EducationYear educationYear;

    private Date dateOfBeginPayment;

    private ISelectModel grantTemplateDocumentModel;

    private Student currentStudent;

    private TemplateDocument grantTemplateDocument = new TemplateDocument();


    private Map<Long, List<NarfuGrantDocument>> studentDocMap = new HashMap<Long, List<NarfuGrantDocument>>();

    private OrgUnitSecModel secModel;

    public Object getSecuredObject() {
        return null == getOrgUnitId() ? SecurityRuntime.getInstance().getCommonSecurityObject() :
                UniDaoFacade.getCoreDao().get(getOrgUnitId());
    }

    public OrgUnitSecModel getSecModel() {
        return secModel;
    }

    public void setSecModel(OrgUnitSecModel secModel) {
        this.secModel = secModel;
    }

    public ISelectModel getGrantsModel() {
        return grantsModel;
    }

    public void setGrantsModel(ISelectModel grantsModel) {
        this.grantsModel = grantsModel;
    }

    public Grant getGrant() {
        return grant;
    }

    public void setGrant(Grant grant) {
        this.grant = grant;
    }

    public ISelectModel getEducationYearModel() {
        return educationYearModel;
    }

    public void setEducationYearModel(ISelectModel educationYearModel) {
        this.educationYearModel = educationYearModel;
    }

    public EducationYear getEducationYear() {
        return educationYear;
    }

    public void setEducationYear(EducationYear educationYear) {
        this.educationYear = educationYear;
    }

    public Date getDateOfBeginPayment() {
        return dateOfBeginPayment;
    }

    public void setDateOfBeginPayment(Date dateOfBeginPayment) {
        this.dateOfBeginPayment = dateOfBeginPayment;
    }

    public ISelectModel getGrantTemplateDocumentModel() {
        return grantTemplateDocumentModel;
    }

    public void setGrantTemplateDocumentModel(ISelectModel grantTemplateDocumentModel) {
        this.grantTemplateDocumentModel = grantTemplateDocumentModel;
    }

    public Student getCurrentStudent() {
        return currentStudent;
    }

    public void setCurrentStudent(Student currentStudent) {
        this.currentStudent = currentStudent;
    }

    public Map<Long, List<NarfuGrantDocument>> getStudentDocMap() {
        return studentDocMap;
    }


    public void setStudentDocMap(Map<Long, List<NarfuGrantDocument>> studentDocMap) {
        this.studentDocMap = studentDocMap;
    }

    public TemplateDocument getGrantTemplateDocument() {
        return grantTemplateDocument;
    }

    public void setGrantTemplateDocument(TemplateDocument grantTemplateDocument) {
        this.grantTemplateDocument = grantTemplateDocument;

    }

}
