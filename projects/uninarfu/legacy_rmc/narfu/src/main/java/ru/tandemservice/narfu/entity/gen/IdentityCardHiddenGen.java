package ru.tandemservice.narfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.narfu.entity.IdentityCardHidden;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Удостоверение личности
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class IdentityCardHiddenGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.narfu.entity.IdentityCardHidden";
    public static final String ENTITY_NAME = "identityCardHidden";
    public static final int VERSION_HASH = -285604423;
    private static IEntityMeta ENTITY_META;

    public static final String L_IDENTITY_CARD = "identityCard";
    public static final String P_SERIA = "seria";
    public static final String P_NUMBER = "number";
    public static final String P_BIRTH_DATE = "birthDate";

    private IdentityCard _identityCard;     // Удостоверение личности
    private String _seria;     // Серия
    private String _number;     // Номер
    private Date _birthDate;     // Дата рождения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Удостоверение личности. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public IdentityCard getIdentityCard()
    {
        return _identityCard;
    }

    /**
     * @param identityCard Удостоверение личности. Свойство не может быть null и должно быть уникальным.
     */
    public void setIdentityCard(IdentityCard identityCard)
    {
        dirty(_identityCard, identityCard);
        _identityCard = identityCard;
    }

    /**
     * @return Серия.
     */
    @Length(max=255)
    public String getSeria()
    {
        return _seria;
    }

    /**
     * @param seria Серия.
     */
    public void setSeria(String seria)
    {
        dirty(_seria, seria);
        _seria = seria;
    }

    /**
     * @return Номер.
     */
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Дата рождения.
     */
    public Date getBirthDate()
    {
        return _birthDate;
    }

    /**
     * @param birthDate Дата рождения.
     */
    public void setBirthDate(Date birthDate)
    {
        dirty(_birthDate, birthDate);
        _birthDate = birthDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof IdentityCardHiddenGen)
        {
            setIdentityCard(((IdentityCardHidden)another).getIdentityCard());
            setSeria(((IdentityCardHidden)another).getSeria());
            setNumber(((IdentityCardHidden)another).getNumber());
            setBirthDate(((IdentityCardHidden)another).getBirthDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends IdentityCardHiddenGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) IdentityCardHidden.class;
        }

        public T newInstance()
        {
            return (T) new IdentityCardHidden();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "identityCard":
                    return obj.getIdentityCard();
                case "seria":
                    return obj.getSeria();
                case "number":
                    return obj.getNumber();
                case "birthDate":
                    return obj.getBirthDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "identityCard":
                    obj.setIdentityCard((IdentityCard) value);
                    return;
                case "seria":
                    obj.setSeria((String) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "birthDate":
                    obj.setBirthDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "identityCard":
                        return true;
                case "seria":
                        return true;
                case "number":
                        return true;
                case "birthDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "identityCard":
                    return true;
                case "seria":
                    return true;
                case "number":
                    return true;
                case "birthDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "identityCard":
                    return IdentityCard.class;
                case "seria":
                    return String.class;
                case "number":
                    return String.class;
                case "birthDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<IdentityCardHidden> _dslPath = new Path<IdentityCardHidden>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "IdentityCardHidden");
    }
            

    /**
     * @return Удостоверение личности. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.narfu.entity.IdentityCardHidden#getIdentityCard()
     */
    public static IdentityCard.Path<IdentityCard> identityCard()
    {
        return _dslPath.identityCard();
    }

    /**
     * @return Серия.
     * @see ru.tandemservice.narfu.entity.IdentityCardHidden#getSeria()
     */
    public static PropertyPath<String> seria()
    {
        return _dslPath.seria();
    }

    /**
     * @return Номер.
     * @see ru.tandemservice.narfu.entity.IdentityCardHidden#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Дата рождения.
     * @see ru.tandemservice.narfu.entity.IdentityCardHidden#getBirthDate()
     */
    public static PropertyPath<Date> birthDate()
    {
        return _dslPath.birthDate();
    }

    public static class Path<E extends IdentityCardHidden> extends EntityPath<E>
    {
        private IdentityCard.Path<IdentityCard> _identityCard;
        private PropertyPath<String> _seria;
        private PropertyPath<String> _number;
        private PropertyPath<Date> _birthDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Удостоверение личности. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.narfu.entity.IdentityCardHidden#getIdentityCard()
     */
        public IdentityCard.Path<IdentityCard> identityCard()
        {
            if(_identityCard == null )
                _identityCard = new IdentityCard.Path<IdentityCard>(L_IDENTITY_CARD, this);
            return _identityCard;
        }

    /**
     * @return Серия.
     * @see ru.tandemservice.narfu.entity.IdentityCardHidden#getSeria()
     */
        public PropertyPath<String> seria()
        {
            if(_seria == null )
                _seria = new PropertyPath<String>(IdentityCardHiddenGen.P_SERIA, this);
            return _seria;
        }

    /**
     * @return Номер.
     * @see ru.tandemservice.narfu.entity.IdentityCardHidden#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(IdentityCardHiddenGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Дата рождения.
     * @see ru.tandemservice.narfu.entity.IdentityCardHidden#getBirthDate()
     */
        public PropertyPath<Date> birthDate()
        {
            if(_birthDate == null )
                _birthDate = new PropertyPath<Date>(IdentityCardHiddenGen.P_BIRTH_DATE, this);
            return _birthDate;
        }

        public Class getEntityClass()
        {
            return IdentityCardHidden.class;
        }

        public String getEntityName()
        {
            return "identityCardHidden";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
