package ru.tandemservice.narfu.uniec.component.report.EnrollmentResultIndexNumbers.EnrollmentResultIndexNumbersAdd;

import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniec.component.report.EnrollmentResultIndexNumbers.EnrollmentResultIndexNumbersAdd.Model;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.util.EntrantDataUtil;

import java.util.*;

public class EnrollmentResultIndexNumbersReportBuilderExt {
    private Model model;
    private Session session;

    public EnrollmentResultIndexNumbersReportBuilderExt(Model model, Session session) {
        this.model = model;
        this.session = session;
    }

    public DatabaseFile getContent() {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, "enrollmentResultIndexNumbers");

        TopOrgUnit academy = TopOrgUnit.getInstance();

        RtfInjectModifier im = new RtfInjectModifier().put("developForm", this.model.getDevelopForm().getTitle()).put("academyTitle", academy.getNominativeCaseTitle() == null ? academy.getTitle() : academy.getNominativeCaseTitle()).put("year", this.model.getReport().getEnrollmentCampaign().getEducationYear().getTitle().split("/")[0]);

        RtfTableModifier tm = new RtfTableModifier().put("T", getTableRows());

        DatabaseFile content = new DatabaseFile();
        content.setContent(RtfUtil.toByteArray(templateDocument.getCurrentTemplate(), im, tm));
        return content;
    }

    public String[][] getTableRows() {
        Map<EducationLevels, Row> rowMap = new HashMap<>();

        MQBuilder directionBuilder = getDirectionBuilder();
        List<RequestedEnrollmentDirection> directionList = directionBuilder.getResultList(this.session);
        for (RequestedEnrollmentDirection direction : directionList) {
            EducationLevels eduLevel = direction.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();

            while ((eduLevel != null) && (!eduLevel.getLevelType().isGroupingLevel()))
                eduLevel = eduLevel.getParentLevel();

            Row row = rowMap.get(eduLevel);
            if (row == null)
                rowMap.put(eduLevel, row = new Row());
            row.entrantIds.add(direction.getEntrantRequest().getEntrant().getId());
        }

        MQBuilder preBuilder = getPreliminaryBuilder();
        List<PreliminaryEnrollmentStudent> preStudentList = preBuilder.getResultList(this.session);
        preBuilder.getSelectAliasList().clear();
        preBuilder.addSelect("p", new Object[]{PreliminaryEnrollmentStudent.requestedEnrollmentDirection()});
        EntrantDataUtil preUtil = new EntrantDataUtil(this.session, this.model.getEnrollmentCampaign(), preBuilder, EntrantDataUtil.DetailLevel.EXAM_PASS);
        for (PreliminaryEnrollmentStudent preStudent : preStudentList) {
            EducationLevels eduLevel = preStudent.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();
            double finalMark = preUtil.getFinalMark(preStudent.getRequestedEnrollmentDirection());

            while ((eduLevel != null) && (!eduLevel.getLevelType().isGroupingLevel()))
                eduLevel = eduLevel.getParentLevel();

            Row row = rowMap.get(eduLevel);
            if (row == null)
                rowMap.put(eduLevel, row = new Row());
            row.enrolled += 1;

            if ((finalMark != 0.0D) && ((row.passingMark == 0.0D) || (row.passingMark > finalMark))) {
                row.passingMark = finalMark;
            }
        }

        MQBuilder builder = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "e");
        builder.addJoin("e", EnrollmentDirection.educationOrgUnit(), "ou");
        builder.add(MQExpression.eq("e", EnrollmentDirection.enrollmentCampaign(), this.model.getEnrollmentCampaign()));
        patchEduOuBuilder(builder);
        List<EnrollmentDirection> directions = builder.getResultList(this.session);
        for (EnrollmentDirection direction : directions) {
            EducationLevels eduLevel = direction.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();

            while ((eduLevel != null) && (!eduLevel.getLevelType().isGroupingLevel()))
                eduLevel = eduLevel.getParentLevel();

            Row row = rowMap.get(eduLevel);
            if (row != null) {
                Integer taPlan;
                Integer plan;
                if (this.model.getCompensationType().isBudget()) {
                    plan = direction.getMinisterialPlan();
                    taPlan = direction.getTargetAdmissionPlanBudget();
                }
                else {
                    plan = direction.getContractPlan();
                    taPlan = direction.getTargetAdmissionPlanContract();
                }
                int planWithoutTaPlan = (plan == null ? 0 : plan) - (taPlan == null ? 0 : taPlan);
                if (planWithoutTaPlan < 0) planWithoutTaPlan = 0;
                row.plan += planWithoutTaPlan;
            }

        }

        Map<EducationLevels, Map<EducationLevels, Row>> group2subMap = new HashMap<>();
        for (Map.Entry<EducationLevels, Row> entry : rowMap.entrySet()) {
            EducationLevels eduLevel = entry.getKey();

            EducationLevels group = eduLevel;
            while ((group.getParentLevel() != null) && (group.getLevelType().getParent() != null)) group = group.getParentLevel();

            Map<EducationLevels, Row> map = group2subMap.get(group);
            if (map == null) {
                group2subMap.put(group, map = new HashMap<>());
            }
            if (null != map.put(eduLevel, entry.getValue())) {
                throw new RuntimeException("Dublication row");
            }
        }
        List<String[]> rowList = new ArrayList<>();
        DoubleFormatter doubleFormatter = DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS;

        List<EducationLevels> keyList = new ArrayList<>(group2subMap.keySet());
        Collections.sort(keyList, (o1, o2) -> {
            int r = o1.getLevelType().getPriority() - o2.getLevelType().getPriority();
            if (r != 0) return r;
            return o1.getTitle().compareTo(o2.getTitle());

        });
        for (EducationLevels eduLevel : keyList) {
            Map<EducationLevels, Row> subMap = group2subMap.get(eduLevel);

            rowList.add(new String[]{eduLevel.getTitle(), eduLevel.getTitleCodePrefix(), null, null, null, null});

            List<EducationLevels> subKeyList = new ArrayList<>(subMap.keySet());
            Collections.sort(subKeyList, ITitled.TITLED_COMPARATOR);

            for (EducationLevels subEduLevel : subKeyList) {
                Row row = subMap.get(subEduLevel);
                rowList.add(new String[]{subEduLevel.getTitle(), subEduLevel.getTitleCodePrefix(), Integer.toString(row.plan), row.plan == 0.0D ? "-" : doubleFormatter.format((double) (row.entrantIds.size() / row.plan)), Integer.toString(row.enrolled), doubleFormatter.format(row.passingMark)});
            }
        }
        return rowList.toArray(new String[rowList.size()][]);
    }

    private MQBuilder getDirectionBuilder() {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "d");
        builder.addJoin("d", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit(), "ou");

        builder.add(MQExpression.eq("d", RequestedEnrollmentDirection.enrollmentDirection().enrollmentCampaign(), this.model.getEnrollmentCampaign()));
        builder.add(UniMQExpression.betweenDate("d", RequestedEnrollmentDirection.entrantRequest().regDate().s(), this.model.getReport().getDateFrom(), this.model.getReport().getDateTo()));
        builder.add(MQExpression.eq("d", RequestedEnrollmentDirection.compensationType(), this.model.getCompensationType()));

        if (this.model.isStudentCategoryActive()) {
            builder.add(MQExpression.in("d", RequestedEnrollmentDirection.studentCategory(), this.model.getStudentCategoryList()));
        }
        patchEduOuBuilder(builder);

        builder.add(MQExpression.eq("d", RequestedEnrollmentDirection.entrantRequest().entrant().archival(), Boolean.FALSE));
        builder.add(MQExpression.eq("d", RequestedEnrollmentDirection.targetAdmission(), Boolean.FALSE));
        builder.add(MQExpression.in("d", RequestedEnrollmentDirection.state().code(), Arrays.asList(new String[]{"4", "5", "6", "3"})));

        return builder;
    }

    private MQBuilder getPreliminaryBuilder() {
        MQBuilder builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "p");
        builder.addJoin("p", PreliminaryEnrollmentStudent.educationOrgUnit(), "ou");

        builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.requestedEnrollmentDirection().enrollmentDirection().enrollmentCampaign(), this.model.getEnrollmentCampaign()));
        builder.add(UniMQExpression.betweenDate("p", PreliminaryEnrollmentStudent.requestedEnrollmentDirection().entrantRequest().regDate().s(), this.model.getReport().getDateFrom(), this.model.getReport().getDateTo()));
        builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.compensationType(), this.model.getCompensationType()));

        if (this.model.isStudentCategoryActive()) {
            builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.studentCategory(), this.model.getStudentCategoryList()));
        }
        patchEduOuBuilder(builder);

        builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.requestedEnrollmentDirection().entrantRequest().entrant().archival(), Boolean.FALSE));
        builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.targetAdmission(), Boolean.FALSE));

        return builder;
    }

    private void patchEduOuBuilder(MQBuilder builder) {
        if (this.model.isQualificationActive()) {
            builder.add(MQExpression.in("ou", EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification().s(), this.model.getQualificationList()));
        }
        if (this.model.isFormativeOrgUnitActive()) {
            builder.add(MQExpression.in("ou", EducationOrgUnit.formativeOrgUnit().s(), this.model.getFormativeOrgUnitList()));
        }
        if (this.model.isTerritorialOrgUnitActive()) {
            builder.add(MQExpression.in("ou", EducationOrgUnit.territorialOrgUnit().s(), this.model.getTerritorialOrgUnitList()));
        }
        if (this.model.isEducationLevelHighSchoolActive()) {
            builder.add(MQExpression.in("ou", EducationOrgUnit.educationLevelHighSchool().s(), this.model.getEducationLevelHighSchoolList()));
        }
        builder.add(MQExpression.eq("ou", EducationOrgUnit.developForm().s(), this.model.getDevelopForm()));

        if (this.model.isDevelopConditionActive()) {
            builder.add(MQExpression.in("ou", EducationOrgUnit.developCondition().s(), this.model.getDevelopConditionList()));
        }
        if (this.model.isDevelopTechActive()) {
            builder.add(MQExpression.in("ou", EducationOrgUnit.developTech().s(), this.model.getDevelopTechList()));
        }
        if (this.model.isDevelopPeriodActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.developPeriod().s(), this.model.getDevelopPeriodList()));
    }

    private static class Row {
        Set<Long> entrantIds;
        int enrolled;
        int plan;
        double passingMark;

        private Row() {
            this.entrantIds = new TreeSet<>();
        }

    }
}
