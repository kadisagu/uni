package ru.tandemservice.narfu.base.ext.Person.ui.Tab;

import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import ru.tandemservice.movestudentrmc.entity.PersonNARFU;
import ru.tandemservice.narfu.entity.PersonBenefitExt;
import ru.tandemservice.narfu.entity.PersonEduInstitutionNARFU;
import ru.tandemservice.narfu.entity.PersonNextOfKinNARFU;
import ru.tandemservice.uni.dao.UniDaoFacade;

import java.util.Date;

public class PersonNarfuUtil
{

    //Используется в OGNL PersonTab_Person.html для вытаскивания свойств PersonNARFU
    //дабы не переопределять CAF компонент
    public static PersonNARFU getPersonNARFU(Person person)
    {
        PersonNARFU personNARFU = UniDaoFacade.getCoreDao().get(PersonNARFU.class, PersonNARFU.L_PERSON, person);
        if (personNARFU != null) {
            return personNARFU;
        }

        personNARFU = new PersonNARFU();
        personNARFU.setPerson(person);
        personNARFU.setNeedDormitoryOnReceiptPeriod(false);
        personNARFU.setAverageFamilyCapital(0);
        personNARFU.setDocumentExpiredDate(new Date());

        if (person.getIdentityCard().getCitizenship().getCode() == 0)// гражданство = Российская Федерация
            personNARFU.setResident(true);
        else
            personNARFU.setResident(false);

        UniDaoFacade.getCoreDao().saveOrUpdate(personNARFU);
        return personNARFU;
    }

    public static PersonNextOfKinNARFU get(PersonNextOfKin base)
    {
        PersonNextOfKinNARFU person = UniDaoFacade.getCoreDao().get(PersonNextOfKinNARFU.class, PersonNextOfKinNARFU.L_BASE, base);

        if (person == null) {
            person = new PersonNextOfKinNARFU();
            person.setBase(base);
        }

        return person;
    }


    public static PersonEduInstitutionNARFU getPersonEduinstitution(PersonEduInstitution base)
    {
        PersonEduInstitutionNARFU personEduInstitution = UniDaoFacade.getCoreDao().get(PersonEduInstitutionNARFU.class, PersonEduInstitutionNARFU.L_BASE, base);

        if (personEduInstitution == null) {
            personEduInstitution = new PersonEduInstitutionNARFU();
            personEduInstitution.setBase(base);
        }

        return personEduInstitution;
    }

    public static PersonBenefitExt getPersonBenefitExt(PersonBenefit personBenefit)
    {
        PersonBenefitExt personBenefitExt = UniDaoFacade.getCoreDao().get(PersonBenefitExt.class, PersonBenefitExt.personBenefit(), personBenefit);

        if (personBenefitExt == null) {
            personBenefitExt = new PersonBenefitExt();
            personBenefitExt.setPersonBenefit(personBenefit);
        }

        return personBenefitExt;
    }

}
