package ru.tandemservice.narfu.uniec.component.report.EntrantRegistration.EntrantRegistrationPub;

import ru.tandemservice.narfu.uniec.entity.report.NarfuEntrantRegistration;
import ru.tandemservice.uni.dao.UniDao;

public class DAO extends UniDao<Model>
        implements IDAO
{

    public DAO()
    {
    }

    public void prepare(Model model)
    {
        model.setReport((NarfuEntrantRegistration) getNotNull(NarfuEntrantRegistration.class, model.getReport().getId()));
    }

}
