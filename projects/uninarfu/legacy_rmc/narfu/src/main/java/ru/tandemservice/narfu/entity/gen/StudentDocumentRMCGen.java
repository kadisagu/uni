package ru.tandemservice.narfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.narfu.entity.StudentDocumentRMC;
import ru.tandemservice.uni.entity.employee.StudentDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выдача справок-вызовов
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentDocumentRMCGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.narfu.entity.StudentDocumentRMC";
    public static final String ENTITY_NAME = "studentDocumentRMC";
    public static final int VERSION_HASH = -1818302155;
    private static IEntityMeta ENTITY_META;

    public static final String L_BASE = "base";
    public static final String P_DATE_START_CERTIFICATION = "dateStartCertification";
    public static final String P_DATE_END_CERTIFICATION = "dateEndCertification";
    public static final String P_DATE_END_STUDY = "dateEndStudy";

    private StudentDocument _base;     // Выданная студенту справка
    private Date _dateStartCertification;     // Дата начала промежуточной аттестации
    private Date _dateEndCertification;     // Дата окончания промежуточной аттестации
    private Date _dateEndStudy;     // Предполагаемая дата окончания

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выданная студенту справка.
     */
    public StudentDocument getBase()
    {
        return _base;
    }

    /**
     * @param base Выданная студенту справка.
     */
    public void setBase(StudentDocument base)
    {
        dirty(_base, base);
        _base = base;
    }

    /**
     * @return Дата начала промежуточной аттестации.
     */
    public Date getDateStartCertification()
    {
        return _dateStartCertification;
    }

    /**
     * @param dateStartCertification Дата начала промежуточной аттестации.
     */
    public void setDateStartCertification(Date dateStartCertification)
    {
        dirty(_dateStartCertification, dateStartCertification);
        _dateStartCertification = dateStartCertification;
    }

    /**
     * @return Дата окончания промежуточной аттестации.
     */
    public Date getDateEndCertification()
    {
        return _dateEndCertification;
    }

    /**
     * @param dateEndCertification Дата окончания промежуточной аттестации.
     */
    public void setDateEndCertification(Date dateEndCertification)
    {
        dirty(_dateEndCertification, dateEndCertification);
        _dateEndCertification = dateEndCertification;
    }

    /**
     * @return Предполагаемая дата окончания.
     */
    public Date getDateEndStudy()
    {
        return _dateEndStudy;
    }

    /**
     * @param dateEndStudy Предполагаемая дата окончания.
     */
    public void setDateEndStudy(Date dateEndStudy)
    {
        dirty(_dateEndStudy, dateEndStudy);
        _dateEndStudy = dateEndStudy;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentDocumentRMCGen)
        {
            setBase(((StudentDocumentRMC)another).getBase());
            setDateStartCertification(((StudentDocumentRMC)another).getDateStartCertification());
            setDateEndCertification(((StudentDocumentRMC)another).getDateEndCertification());
            setDateEndStudy(((StudentDocumentRMC)another).getDateEndStudy());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentDocumentRMCGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentDocumentRMC.class;
        }

        public T newInstance()
        {
            return (T) new StudentDocumentRMC();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "base":
                    return obj.getBase();
                case "dateStartCertification":
                    return obj.getDateStartCertification();
                case "dateEndCertification":
                    return obj.getDateEndCertification();
                case "dateEndStudy":
                    return obj.getDateEndStudy();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "base":
                    obj.setBase((StudentDocument) value);
                    return;
                case "dateStartCertification":
                    obj.setDateStartCertification((Date) value);
                    return;
                case "dateEndCertification":
                    obj.setDateEndCertification((Date) value);
                    return;
                case "dateEndStudy":
                    obj.setDateEndStudy((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "base":
                        return true;
                case "dateStartCertification":
                        return true;
                case "dateEndCertification":
                        return true;
                case "dateEndStudy":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "base":
                    return true;
                case "dateStartCertification":
                    return true;
                case "dateEndCertification":
                    return true;
                case "dateEndStudy":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "base":
                    return StudentDocument.class;
                case "dateStartCertification":
                    return Date.class;
                case "dateEndCertification":
                    return Date.class;
                case "dateEndStudy":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentDocumentRMC> _dslPath = new Path<StudentDocumentRMC>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentDocumentRMC");
    }
            

    /**
     * @return Выданная студенту справка.
     * @see ru.tandemservice.narfu.entity.StudentDocumentRMC#getBase()
     */
    public static StudentDocument.Path<StudentDocument> base()
    {
        return _dslPath.base();
    }

    /**
     * @return Дата начала промежуточной аттестации.
     * @see ru.tandemservice.narfu.entity.StudentDocumentRMC#getDateStartCertification()
     */
    public static PropertyPath<Date> dateStartCertification()
    {
        return _dslPath.dateStartCertification();
    }

    /**
     * @return Дата окончания промежуточной аттестации.
     * @see ru.tandemservice.narfu.entity.StudentDocumentRMC#getDateEndCertification()
     */
    public static PropertyPath<Date> dateEndCertification()
    {
        return _dslPath.dateEndCertification();
    }

    /**
     * @return Предполагаемая дата окончания.
     * @see ru.tandemservice.narfu.entity.StudentDocumentRMC#getDateEndStudy()
     */
    public static PropertyPath<Date> dateEndStudy()
    {
        return _dslPath.dateEndStudy();
    }

    public static class Path<E extends StudentDocumentRMC> extends EntityPath<E>
    {
        private StudentDocument.Path<StudentDocument> _base;
        private PropertyPath<Date> _dateStartCertification;
        private PropertyPath<Date> _dateEndCertification;
        private PropertyPath<Date> _dateEndStudy;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выданная студенту справка.
     * @see ru.tandemservice.narfu.entity.StudentDocumentRMC#getBase()
     */
        public StudentDocument.Path<StudentDocument> base()
        {
            if(_base == null )
                _base = new StudentDocument.Path<StudentDocument>(L_BASE, this);
            return _base;
        }

    /**
     * @return Дата начала промежуточной аттестации.
     * @see ru.tandemservice.narfu.entity.StudentDocumentRMC#getDateStartCertification()
     */
        public PropertyPath<Date> dateStartCertification()
        {
            if(_dateStartCertification == null )
                _dateStartCertification = new PropertyPath<Date>(StudentDocumentRMCGen.P_DATE_START_CERTIFICATION, this);
            return _dateStartCertification;
        }

    /**
     * @return Дата окончания промежуточной аттестации.
     * @see ru.tandemservice.narfu.entity.StudentDocumentRMC#getDateEndCertification()
     */
        public PropertyPath<Date> dateEndCertification()
        {
            if(_dateEndCertification == null )
                _dateEndCertification = new PropertyPath<Date>(StudentDocumentRMCGen.P_DATE_END_CERTIFICATION, this);
            return _dateEndCertification;
        }

    /**
     * @return Предполагаемая дата окончания.
     * @see ru.tandemservice.narfu.entity.StudentDocumentRMC#getDateEndStudy()
     */
        public PropertyPath<Date> dateEndStudy()
        {
            if(_dateEndStudy == null )
                _dateEndStudy = new PropertyPath<Date>(StudentDocumentRMCGen.P_DATE_END_STUDY, this);
            return _dateEndStudy;
        }

        public Class getEntityClass()
        {
            return StudentDocumentRMC.class;
        }

        public String getEntityName()
        {
            return "studentDocumentRMC";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
