package ru.tandemservice.narfu.component.reports.StudentsByUGS.Pub;

import ru.tandemservice.narfu.entity.NarfuReportStudentsByUGS;
import ru.tandemservice.uni.dao.UniDao;

public class DAO extends UniDao<Model>
        implements IDAO
{

    public DAO()
    {
    }

    public void prepare(Model model)
    {
        model.setReport((NarfuReportStudentsByUGS) getNotNull(NarfuReportStudentsByUGS.class, model.getReport().getId()));
    }

}
