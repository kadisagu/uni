package ru.tandemservice.narfu.base.ext.UniStudent;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import ru.tandemservice.narfu.base.ext.UniStudent.ui.logic.ArchivalStudentSearchListExtDSHandler;

@Configuration
public class UniStudentMangerExt extends BusinessObjectExtensionManager
{

    public static UniStudentMangerExt instance() {
        return instance(UniStudentMangerExt.class);
    }


    @Bean
    public IBusinessHandler<DSInput, DSOutput> archivalStudentSearchListExtDSHandler()
    {
        return new ArchivalStudentSearchListExtDSHandler(getName());
    }
}
