package ru.tandemservice.narfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.narfu.entity.NarfuReportStudentArchive;
import ru.tandemservice.narfu.entity.catalog.NarfuReportStudentArchiveType;
import ru.tandemservice.uni.entity.report.StorableReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Опись сдачи в архив личных дел отчисленных/выпускников
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class NarfuReportStudentArchiveGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.narfu.entity.NarfuReportStudentArchive";
    public static final String ENTITY_NAME = "narfuReportStudentArchive";
    public static final int VERSION_HASH = 1547533556;
    private static IEntityMeta ENTITY_META;

    public static final String L_TYPE = "type";
    public static final String P_INDEX = "index";
    public static final String P_NUMBER = "number";

    private NarfuReportStudentArchiveType _type;     // Тип отчета
    private String _index;     // Индекс
    private String _number;     // Номер описи

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип отчета. Свойство не может быть null.
     */
    @NotNull
    public NarfuReportStudentArchiveType getType()
    {
        return _type;
    }

    /**
     * @param type Тип отчета. Свойство не может быть null.
     */
    public void setType(NarfuReportStudentArchiveType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Индекс. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getIndex()
    {
        return _index;
    }

    /**
     * @param index Индекс. Свойство не может быть null.
     */
    public void setIndex(String index)
    {
        dirty(_index, index);
        _index = index;
    }

    /**
     * @return Номер описи. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер описи. Свойство не может быть null.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof NarfuReportStudentArchiveGen)
        {
            setType(((NarfuReportStudentArchive)another).getType());
            setIndex(((NarfuReportStudentArchive)another).getIndex());
            setNumber(((NarfuReportStudentArchive)another).getNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends NarfuReportStudentArchiveGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) NarfuReportStudentArchive.class;
        }

        public T newInstance()
        {
            return (T) new NarfuReportStudentArchive();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "type":
                    return obj.getType();
                case "index":
                    return obj.getIndex();
                case "number":
                    return obj.getNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "type":
                    obj.setType((NarfuReportStudentArchiveType) value);
                    return;
                case "index":
                    obj.setIndex((String) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "type":
                        return true;
                case "index":
                        return true;
                case "number":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "type":
                    return true;
                case "index":
                    return true;
                case "number":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "type":
                    return NarfuReportStudentArchiveType.class;
                case "index":
                    return String.class;
                case "number":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<NarfuReportStudentArchive> _dslPath = new Path<NarfuReportStudentArchive>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "NarfuReportStudentArchive");
    }
            

    /**
     * @return Тип отчета. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.NarfuReportStudentArchive#getType()
     */
    public static NarfuReportStudentArchiveType.Path<NarfuReportStudentArchiveType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Индекс. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.NarfuReportStudentArchive#getIndex()
     */
    public static PropertyPath<String> index()
    {
        return _dslPath.index();
    }

    /**
     * @return Номер описи. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.NarfuReportStudentArchive#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    public static class Path<E extends NarfuReportStudentArchive> extends StorableReport.Path<E>
    {
        private NarfuReportStudentArchiveType.Path<NarfuReportStudentArchiveType> _type;
        private PropertyPath<String> _index;
        private PropertyPath<String> _number;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип отчета. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.NarfuReportStudentArchive#getType()
     */
        public NarfuReportStudentArchiveType.Path<NarfuReportStudentArchiveType> type()
        {
            if(_type == null )
                _type = new NarfuReportStudentArchiveType.Path<NarfuReportStudentArchiveType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Индекс. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.NarfuReportStudentArchive#getIndex()
     */
        public PropertyPath<String> index()
        {
            if(_index == null )
                _index = new PropertyPath<String>(NarfuReportStudentArchiveGen.P_INDEX, this);
            return _index;
        }

    /**
     * @return Номер описи. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.NarfuReportStudentArchive#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(NarfuReportStudentArchiveGen.P_NUMBER, this);
            return _number;
        }

        public Class getEntityClass()
        {
            return NarfuReportStudentArchive.class;
        }

        public String getEntityName()
        {
            return "narfuReportStudentArchive";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
