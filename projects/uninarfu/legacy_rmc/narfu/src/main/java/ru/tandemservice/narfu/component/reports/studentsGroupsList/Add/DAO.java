package ru.tandemservice.narfu.component.reports.studentsGroupsList.Add;


import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uni.entity.catalog.UniScriptItem;
import ru.tandemservice.uni.entity.catalog.codes.UniScriptItemCodes;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DAO extends ru.tandemservice.uni.component.reports.studentsGroupsList.Add.DAO {
    @Override
    public void prepareReportData(ru.tandemservice.uni.component.reports.studentsGroupsList.Add.Model model)
    {
        super.prepareReportData(model);
        Model modelExt = (Model) model;
        int length = model.getColumnSizes().length;
        String[] oldTitles = model.getColumnHeaders();
        int[] oldSizes = model.getColumnSizes();

        for (int i = 0; i < length; i++) {
            if (oldTitles[i].equals("Номер")) {
                oldTitles[i] = "Личный номер";
                oldSizes[i] += 2; //После переименования заголовок не умещается, увеличим на 2% за счет первой колонки
                oldSizes[0] -= 2;
            }
        }
        model.setColumnHeaders(oldTitles);
        model.setColumnSizes(oldSizes);

        if (modelExt.isStudentBookNumberReq()) {


            int[] newSizes = new int[length + 1];
            String[] newTitles = new String[length + 1];
            int finalPerCentWidth = 0;

            for (int i = 0; i < length; i++) {
                newSizes[i] = model.getColumnSizes()[i];
                newTitles[i] = model.getColumnHeaders()[i];
                finalPerCentWidth += newSizes[i];
            }
            newSizes[length] = 100 - 100 * (length + 1) / (length + 2) - 2;
            newTitles[length] = "Номер зач. книжки";
//            List<String> columnHeaders = ;
//                model.getColumnHeaders()columnHeaders.add("Номер зачетной книжки");
//                columnSizes.add(111);
//
//            int i = 0;
//            int completeWidth = 0;
//            int finalPerCentWidth = 0;
//            int[] colPerCentSizes = new int[columnSizes.size()];
//            for(Integer colSize : columnSizes) completeWidth += colSize;
//            for(Integer colSize : columnSizes)
//            {
//                colPerCentSizes[i] = (100 * colSize) / completeWidth;
//                finalPerCentWidth += colPerCentSizes[i++];
//            }

            newSizes[0] += 100 - finalPerCentWidth - newSizes[length];
            model.setColumnHeaders(newTitles);
            model.setColumnSizes(newSizes);

        }
    }


    @Override
    public Integer preparePrintReport(ru.tandemservice.uni.component.reports.studentsGroupsList.Add.Model model) {
        prepareReportData(model);

        List<String> checkList = new ArrayList<>();

        if(model.isSexReq()) checkList.add("isSexReq");
        if(model.isBirthDateReq()) checkList.add("isBirthDateReq");
        if(model.isCompensationTypeReq()) checkList.add("isCompensationTypeReq");
        if(model.isThriftyGroupReq()) checkList.add("isThriftyGroupReq");
        if(model.isStudentStatusReq()) checkList.add("isStudentStatusReq");
        if(model.isPersonalNumberReq()) checkList.add("isPersonalNumberReq");

        if(model.isEduLevelReq()) checkList.add("isEduLevelReq");
        if(model.isDevelopFormReq()) checkList.add("isDevelopFormReq");
        if(model.isDevelopConditionReq()) checkList.add("isDevelopConditionReq");
        if(model.isDevelopTechReq()) checkList.add("isDevelopTechReq");
        if(model.isDevelopPeriodReq()) checkList.add("isDevelopPeriodReq");
        if(model.isFormativeOrgUnitReq()) checkList.add("isFormativeOrgUnitReq");
        if(model.isTerritorialOrgUnitReq()) checkList.add("isTerritorialOrgUnitReq");
        Model modelExt = (Model) model;
        if (modelExt.isStudentBookNumberReq()) checkList.add("isStudentBookNumberReq");

        final UniScriptItem scriptItem = getByCode(UniScriptItem.class, UniScriptItemCodes.STUDENTS_GROUPS_LIST);

        final Map<String, Object> result = CommonManager.instance().scriptDao().getScriptResult(scriptItem,
                "groupMap", model.getStudentsMap(),
                "checkList", checkList,
                "columnSizes", model.getColumnSizes(),
                "columnHeaders", model.getColumnHeaders()
        );

        return PrintReportTemporaryStorage.registerTemporaryPrintForm((byte[]) result.get("document"), (String) result.get("fileName"));
    }
}
