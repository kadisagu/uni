package ru.tandemservice.narfu.base.ext.GlobalReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.GlobalReportManager;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.logic.GlobalReportDefinition;
import ru.tandemservice.narfu.component.reports.VacantBudgetPlaces.List.Model;


/**
 * @author vch
 * @since 21/04/2014
 */
@Configuration
public class GlobalReportExtManager extends BusinessObjectExtensionManager {
    @Autowired
    private GlobalReportManager _globalReportManager;

    @Bean
    public ItemListExtension<GlobalReportDefinition> reportListExtension()
    {
        return itemListExtension(_globalReportManager.reportListExtPoint())

                .add("vacantBudgetPlacesReport", new GlobalReportDefinition("student", "vacantBudgetPlacesReport", Model.class.getPackage().getName()))
                .add("academVacationsJournal", new GlobalReportDefinition("student", "academVacationsJournal", ru.tandemservice.narfu.component.reports.JournalAcademVacations.List.Model.class.getPackage().getName()))
                .add("ugsReport", new GlobalReportDefinition("student", "ugsReport", ru.tandemservice.narfu.component.reports.StudentsByUGS.List.Model.class.getPackage().getName()))
                .create();

    }
}