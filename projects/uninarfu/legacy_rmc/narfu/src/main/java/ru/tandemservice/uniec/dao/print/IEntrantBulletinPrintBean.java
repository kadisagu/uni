package ru.tandemservice.uniec.dao.print;


import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.uniecmarkextrmc.component.menu.MarkDistribution.DisciplineWrapper;
import ru.tandemservice.uniecmarkextrmc.component.menu.MarkDistribution.EntrantDisciplineMarkWrapper;

import java.util.List;

public interface IEntrantBulletinPrintBean {
    public RtfDocument generateEntrantBulletin(List<EntrantDisciplineMarkWrapper> wrappers, DisciplineWrapper discipline);
}
