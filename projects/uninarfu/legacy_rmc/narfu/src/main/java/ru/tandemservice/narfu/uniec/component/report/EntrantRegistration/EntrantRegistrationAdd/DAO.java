package ru.tandemservice.narfu.uniec.component.report.EntrantRegistration.EntrantRegistrationAdd;


import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.narfu.uniec.entity.report.NarfuEntrantRegistration;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.Arrays;
import java.util.Date;

public class DAO extends UniDao<Model>
        implements IDAO
{

    public DAO()
    {
    }

    public void prepare(Model model)
    {
        Session session = getSession();
        model.setParameters(new ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil.Parameters());
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.setEnrollmentCampaignStageList(Arrays.asList(new IdentifiableWrapper[]{
                new IdentifiableWrapper(Long.valueOf(0L), "\u043F\u043E \u0445\u043E\u0434\u0443 \u043F\u0440\u0438\u0435\u043C\u0430 \u0434\u043E\u043A\u0443\u043C\u0435\u043D\u0442\u043E\u0432"), new IdentifiableWrapper(Long.valueOf(1L), "\u043F\u043E \u0440\u0435\u0437\u0443\u043B\u044C\u0442\u0430\u0442\u0430\u043C \u0441\u0434\u0430\u0447\u0438 \u0432\u0441\u0442\u0443\u043F\u0438\u0442\u0435\u043B\u044C\u043D\u044B\u0445 \u0438\u0441\u043F\u044B\u0442\u0430\u043D\u0438\u0439"), new IdentifiableWrapper(Long.valueOf(2L), "\u043F\u043E \u0440\u0435\u0437\u0443\u043B\u044C\u0442\u0430\u0442\u0430\u043C \u0437\u0430\u0447\u0438\u0441\u043B\u0435\u043D\u0438\u044F")
        }));
        model.setFormativeOrgUnitListModel(MultiEnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(model.getPrincipalContext(), (ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil.Model) model));
        model.setEducationLevelHighSchoolListModel(MultiEnrollmentDirectionUtil.createEducationLevelsHighSchoolModel((ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil.Model) model));

        // ставим дату - сегодня
        Date date = new Date();
        model.getReport().setDateFrom(date);

        model.setDevelopFormListModel(MultiEnrollmentDirectionUtil.createDevelopFormModel((ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil.Model) model));
        model.setDevelopConditionListModel(MultiEnrollmentDirectionUtil.createDevelopConditionModel((ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil.Model) model));
        model.setCompensationTypeListModel(new LazySimpleSelectModel(getCatalogItemListOrderByCode(ru.tandemservice.uni.entity.catalog.CompensationType.class), "shortTitle"));
    }

    public void update(Model model)
    {
        Session session = getSession();
        NarfuEntrantRegistration report = model.getReport();
        report.setFormingDate(new Date());
        if (model.isEducationLevelHighSchoolActive())
            report.setEducationLevelHighSchool(StringUtils.join(UniUtils.getPropertiesList(model.getEducationLevelHighSchoolList(), "displayableTitle"), "; "));
        if (model.getCompensationType() != null)
            report.setCompensationTypeTitle(model.getCompensationType().getShortTitle());
        if (model.isFormativeOrgUnitActive())
            report.setFormativeOrgUnitTitle(StringUtils.join(UniUtils.getPropertiesList(model.getFormativeOrgUnitList(), "fullTitle"), "; "));
        if (model.isDevelopFormActive())
            report.setDevelopFormTitle(StringUtils.join(UniUtils.getPropertiesList(model.getDevelopFormList(), "title"), "; "));
        if (model.isDevelopConditionActive())
            report.setDevelopConditionTitle(StringUtils.join(UniUtils.getPropertiesList(model.getDevelopConditionList(), "title"), "; "));
        org.tandemframework.shared.commonbase.base.entity.DatabaseFile databaseFile = (new NarfuEntrantRegistrationReportBuilder(model, getSession())).getContent();
        session.save(databaseFile);
        report.setContent(databaseFile);
        session.save(report);
    }


}
