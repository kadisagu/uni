package ru.tandemservice.narfu.component.studentmassprint.AddGrantDocument;

import org.tandemframework.core.component.Activator;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudentrmc.entity.NarfuDocument;
import ru.tandemservice.movestudentrmc.util.ListenerWrapper;
import ru.tandemservice.narfu.entity.NarfuGrantDocument;
import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unibase.UniBaseUtils;

public class Controller extends AbstractBusinessController<IDAO, Model> implements ListenerWrapper.IListener<NarfuDocument> {


    @Override
    public void onRefreshComponent(IBusinessComponent component) {

        ((IDAO) getDao()).prepare((Model) getModel(component));
        prepareListDataSource(component);
    }

    protected void prepareListDataSource(IBusinessComponent component) {
        Model model = (Model) getModel(component);

        DynamicListDataSource dataSource = UniBaseUtils.createDataSource(component, (IListDataSourceDao) getDao());
        dataSource.addColumn(new SimpleColumn("Наименование", "document.title").setWidth(100));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditDocument"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteDocument"));
        model.setDataSource(dataSource);

    }

    public void onClickAddDocument(IBusinessComponent component) {

        Model model = component.getModel();
        ListenerWrapper<NarfuDocument> listener = new ListenerWrapper<NarfuDocument>(component);
        Activator activator = new ComponentActivator(
                "ru.tandemservice.movestudentrmc.component.represent.SelectStudentDocument",
                new ParametersMap()
                        .add("studentId", model.getStudent().getId())
                        .add("listener", listener)
                        .add("newDoc", new NarfuGrantDocument()));
        ContextLocal.createDesktop("PersonShellDialog", activator);
    }


    public void onClickEditDocument(IBusinessComponent component) {
        Long docId = component.getListenerParameter();
        NarfuGrantDocument document = (NarfuGrantDocument) UniDaoFacade.getCoreDao().getNotNull(docId);
        Model model = component.getModel();
        ListenerWrapper<NarfuDocument> listener = new ListenerWrapper<NarfuDocument>(component);
        Activator activator = new ComponentActivator(
                "ru.tandemservice.movestudentrmc.component.student.DocumentAddEdit",
                new ParametersMap()
                        .add("studentId", model.getStudent().getId())
                        .add("listener", listener)
                        .add("documentId", document.getDocument().getId())
        );
        ContextLocal.createDesktop("PersonShellDialog", activator);
    }

    public void onClickDeleteDocument(IBusinessComponent component) {
        Long id = component.getListenerParameter();
        getDao().delete(id);
    }

    @Override
    public void listen(ListenerWrapper<NarfuDocument> wrapper) {
        if (wrapper.isOk()) {
//			IBusinessComponent component = (IBusinessComponent) wrapper.getListener();
//			Model model = (Model) getModel(component);
            NarfuDocument doc = wrapper.getResult();
            NarfuGrantDocument document = UniDaoFacade.getCoreDao().get(NarfuGrantDocument.class, NarfuGrantDocument.document().id(), doc.getId());
            if (document == null) {
                document = new NarfuGrantDocument();
                document.setDocument(doc);
                UniDaoFacade.getCoreDao().saveOrUpdate(document);
            }
            doc.setTmp(false);
            UniDaoFacade.getCoreDao().saveOrUpdate(doc);
        }
    }

}
