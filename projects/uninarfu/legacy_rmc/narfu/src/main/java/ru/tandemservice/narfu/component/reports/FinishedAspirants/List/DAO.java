package ru.tandemservice.narfu.component.reports.FinishedAspirants.List;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.narfu.uniec.entity.report.FinishedAspirantsReport;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.unibase.UniBaseUtils;

public class DAO extends UniDao<Model> implements IDAO {


    @Override
    public void prepare(final Model model) {
        if (null != model.getOrgUnitId()) {
            model.setOrgUnit((OrgUnit) getNotNull(model.getOrgUnitId()));
            model.setSecModel(new OrgUnitSecModel(model.getOrgUnit()));
        }
        model.setCompensationTypeModel(new LazySimpleSelectModel<CompensationType>(CompensationType.class));


    }

    @Override
    public void prepareListDataSource(Model model) {

        CompensationType compensationType = (CompensationType) model.getSettings().get("compensationType");

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(FinishedAspirantsReport.class, "report");
        if (compensationType != null) {
            builder.where(DQLExpressions.eq(DQLExpressions.property(FinishedAspirantsReport.compensationType().fromAlias("report")), DQLExpressions.value(compensationType)));
        }


        DQLOrderDescriptionRegistry descriptionRegistry = new DQLOrderDescriptionRegistry(FinishedAspirantsReport.class, "report");
        descriptionRegistry.applyOrder(builder, model.getDataSource().getEntityOrder());

        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }
}
