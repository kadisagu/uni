package ru.tandemservice.narfu.utils;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.narfu.entity.DocumentType2StudentCategory;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.StudentDocumentType;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.List;

public class NarfuUtil {

    /**
     * Список типов документов согласно настройке "Типы документов, выдаваемые студентам"
     *
     * @param student
     *
     * @return
     */
    public static List<StudentDocumentType> getStudentDocumentTypeByCategory(Student student) {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(StudentDocumentType.class, "docType")
                .joinEntity("docType", DQLJoinType.inner, DocumentType2StudentCategory.class, "rel",
                            DQLExpressions.eq(
                                    DQLExpressions.property(StudentDocumentType.id().fromAlias("docType")),
                                    DQLExpressions.property(DocumentType2StudentCategory.studentDocumentType().id().fromAlias("rel"))))
                .where(DQLExpressions.eqValue(
                        DQLExpressions.property(DocumentType2StudentCategory.studentCategory().fromAlias("rel")),
                        student.getStudentCategory()))
                .column("docType");

        return IUniBaseDao.instance.get().getList(builder);
    }
}
