package ru.tandemservice.narfu.component.reports.VacantBudgetPlaces;

import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import ru.tandemservice.narfu.entity.NarfuReportVacantBudgetPlaces;

import java.io.ByteArrayOutputStream;

public class ReportData {
    protected WritableWorkbook workbook;
    protected WritableSheet sheet;
    protected ByteArrayOutputStream workbookStream;
    NarfuReportVacantBudgetPlaces report;

    public WritableWorkbook getWorkbook() {
        return workbook;
    }

    public void setWorkbook(WritableWorkbook workbook) {
        this.workbook = workbook;
    }

    public WritableSheet getSheet() {
        return sheet;
    }

    public void setSheet(WritableSheet sheet) {
        this.sheet = sheet;
    }

    public ByteArrayOutputStream getWorkbookStream() {
        return workbookStream;
    }

    public void setWorkbookStream(ByteArrayOutputStream workbookStream) {
        this.workbookStream = workbookStream;
    }

    public NarfuReportVacantBudgetPlaces getReport() {
        return report;
    }

    public void setReport(NarfuReportVacantBudgetPlaces report) {
        this.report = report;
    }
}
