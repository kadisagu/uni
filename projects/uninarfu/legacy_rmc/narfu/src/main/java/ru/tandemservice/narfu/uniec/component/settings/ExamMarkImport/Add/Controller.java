package ru.tandemservice.narfu.uniec.component.settings.ExamMarkImport.Add;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;

import java.io.IOException;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    public void onClickApply(IBusinessComponent component) {
        Model model = getModel(component);
        try {
            ((IDAO) getDao()).process(model);
        }
        catch (IOException ex) {
            throw new ApplicationException("Ошибка обработки файла");
        }
        deactivate(component);
    }
}
