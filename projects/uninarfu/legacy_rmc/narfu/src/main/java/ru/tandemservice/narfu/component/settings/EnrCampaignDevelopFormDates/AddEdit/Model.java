package ru.tandemservice.narfu.component.settings.EnrCampaignDevelopFormDates.AddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.settings.IDataSettings;
import ru.tandemservice.narfu.entity.EnrCampaignDevelopFormDates;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.List;

@Input({
        @Bind(key = "recId", binding = "record.id")
})
public class Model implements IEnrollmentCampaignSelectModel {

    private EnrCampaignDevelopFormDates record = new EnrCampaignDevelopFormDates();
    private List<EnrollmentCampaign> enrollmentCampaignList;
    private List<DevelopForm> developFormList;

    public void setRecord(EnrCampaignDevelopFormDates record) {
        this.record = record;
    }

    public EnrCampaignDevelopFormDates getRecord() {
        return record;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign() {
        return record.getEnrollmentCampaign();
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentcampaign) {
        record.setEnrollmentCampaign(enrollmentcampaign);
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList() {
        return enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList) {
        this.enrollmentCampaignList = enrollmentCampaignList;
    }

    @Override
    public IDataSettings getSettings() {
        return null;
    }

    public void setDevelopFormList(List<DevelopForm> developFormList) {
        this.developFormList = developFormList;
    }

    public List<DevelopForm> getDevelopFormList() {
        return developFormList;
    }

}
