package ru.tandemservice.narfu.component.reports.StudentsByUGS.Add;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.StudentStatus;

import java.util.List;

public class Model {
    ISelectModel studStatusModel;
    List<StudentStatus> studStatusList;
    ISelectModel activeModel;
    List<StudentStatus> activeList;
    private List<OrgUnit> formativeOrgUnitList;
    private IMultiSelectModel formativeOrgUnitListModel;

    public List<OrgUnit> getFormativeOrgUnitList() {
        return formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList) {
        this.formativeOrgUnitList = formativeOrgUnitList;
    }

    public IMultiSelectModel getFormativeOrgUnitListModel() {
        return formativeOrgUnitListModel;
    }

    public void setFormativeOrgUnitListModel(IMultiSelectModel formativeOrgUnitListModel) {
        this.formativeOrgUnitListModel = formativeOrgUnitListModel;
    }

    public ISelectModel getActiveModel() {
        return activeModel;
    }

    public void setActiveModel(ISelectModel activeModel) {
        this.activeModel = activeModel;
    }

    public List<StudentStatus> getActiveList() {
        return activeList;
    }

    public void setActiveList(List<StudentStatus> activeList) {
        this.activeList = activeList;
    }

    public ISelectModel getStudStatusModel() {
        return studStatusModel;
    }

    public void setStudStatusModel(ISelectModel studStatusModel) {
        this.studStatusModel = studStatusModel;
    }

    public List<StudentStatus> getStudStatusList() {
        return studStatusList;
    }

    public void setStudStatusList(List<StudentStatus> studStatusList) {
        this.studStatusList = studStatusList;
    }
}
