package ru.tandemservice.narfu.uniec.dao.print;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.eventssecrmc.dao.DaoFacade;
import ru.tandemservice.narfu.entity.EntrantRequestNARFU;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.dao.print.PrintParameter;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.catalog.codes.EntrantStateCodes;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.ConversionScale;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.ExamPassDisciplineISTU;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class EntrantExamListPrintBean extends UniBaseDao implements IEntrantExamListPrintBean
{

    @Override
    public RtfDocument createPrintForm(byte[] templateByte, PrintParameter prm)
    {

        if (prm.getEntrantId() != null && prm.getEntrantRequestId() != null) {
            // алгоритм одиночной распечатки
            return printSingleStudent(prm);
        }
        else {
            // задано много студентов
            return printMultiStudent(prm);
        }

    }

    private RtfDocument printMultiStudent(PrintParameter prm)
    {
        // IScriptItem templateDocument = (IScriptItem)getCatalogItem(UniecScriptItem.class, "enrollmentExamSheetNarfu_multipage");
        // RtfDocument template = (new RtfReader()).read(templateDocument.getCurrentTemplate());

        RtfDocument pages1 = null;
        RtfDocument pages2 = null;

        // сначала печатаем все первые страницы
        int i = 0;

        for (Long id : prm.getEntrantIds()) {
            i++;

            Entrant entrant = get(Entrant.class, id);
            EntrantRequest request = _getEntrantRequest(entrant);

            if (prm.isPage1Print()) {
                RtfDocument page1 = makePage1(entrant, request);
                if (pages1 == null)
                    pages1 = page1;
                else {
                    pages1.getElementList().addAll(page1.getElementList());
                    // не последний лист - ставим разрыв
                    if (prm.getEntrantIds().indexOf(id) < prm.getEntrantIds().size() - 1 && i == 2) {
                        pages1.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
                        i = 0;
                    }
                }
            }

            if (prm.isPage2Print()) {
                RtfDocument page2 = makePage2(entrant, request);
                if (pages2 == null)
                    pages2 = page2;
                else {
                    pages2.getElementList().addAll(page2.getElementList());

                    // не последний лист - ставим разрыв
                    if (prm.getEntrantIds().indexOf(id) < prm.getEntrantIds().size() - 1 && i == 2) {
                        pages2.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
                        i = 0;
                    }
                }
            }
        }


        // склеим страницы
        RtfDocument result = null;

        if (prm.isPage1Print()) {
            result = pages1;
            if (prm.isPage2Print())
                result.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
        }

        if (prm.isPage2Print()) {
            if (result == null)
                result = pages2;
            else
                result.getElementList().addAll(pages2.getElementList());
        }

        // копируем настройки
        //result.setSettings(template.getSettings());
        //result.setHeader(template.getHeader());
        return result;
    }

    private RtfDocument printSingleStudent(PrintParameter prm)
    {
        // 1 - получим абитуриента
        Entrant entrant = get(Entrant.class, prm.getEntrantId());

        // получим текущее заявление		
        EntrantRequest request = get(EntrantRequest.class, prm.getEntrantRequestId());

        RtfDocument page1 = makePage1(entrant, request);
        RtfDocument page2 = makePage2(entrant, request);

        // склеим страницы
        RtfDocument result = page1;

        RtfUtil.modifySourceList(result.getHeader(), page2.getHeader(), page2.getElementList());

        // новая страница
        result.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));

        result.getElementList().addAll(page2.getElementList());

        return result;
    }

    private RtfDocument makePage2(Entrant entrant, EntrantRequest request) {

        IScriptItem templateDocument = (IScriptItem) getCatalogItem(UniecScriptItem.class, "enrollmentExamSheetNarfu_page2");
        RtfDocument template = (new RtfReader()).read(templateDocument.getCurrentTemplate());
        RtfDocument retVal = template.getClone();

        // 2 - получим мадифаки
        RtfInjectModifier varmod = createInjectModifier();

        DaoFacade.getDaoPrintUtil().injectMainSecretary(varmod, request);

        // считаем итоговый балл
        String finalBall = _getFinalBall(request);
        varmod.put("sum_ball", finalBall);

        varmod.modify(retVal);

        RtfTableModifier tablemod = createTableModifier();

        String[][] _T = _makeTable(entrant);
        tablemod.put("T", _T);

        tablemod.modify(retVal);

        return retVal;
    }


    /**
     * Считаем итоговый балл
     *
     * @param request
     *
     * @return
     */
    private String _getFinalBall(EntrantRequest request)
    {
        return Integer.toString(getFinalBall(request, false));
    }

    @Override
    public Date getExamPassDisciplineDate(ExamPassDiscipline examPassDiscipline)
    {
        EntranceExamPhase examPhase = EntranceExamPhase.getSelectBuilder4ExamPassDiscipline(examPassDiscipline)
                .top(1)
                .createStatement(getSession()).uniqueResult();

        return examPhase == null ? null : examPhase.getPassDate();
    }

    private String[][] _makeTable(Entrant entrant)
    {

        List<ExamPassDiscipline> lst = getDisciplineList(entrant);

        List<String[]> lines = new ArrayList<String[]>();

        int i = 0;
        for (ExamPassDiscipline dis : lst) {
            // строки
            List<String> cells = new ArrayList<String>();
            i++;

            cells.add(Integer.toString(i)); // номер
            cells.add(dis.getEnrollmentCampaignDiscipline().getEducationSubject().getTitle()); // название дисциплины

            String passForm = "";
            if (dis.getSubjectPassForm() != null)
                passForm = dis.getSubjectPassForm().getTitle();
            cells.add(passForm); //форма сдачи

            // дата экзамена
            cells.add(DateFormatter.DEFAULT_DATE_FORMATTER.format(getExamPassDate(dis))); //дата сдачи

            cells.add("");
            cells.add("");
            cells.add("");
            cells.add("");


            lines.add(cells.toArray(new String[]{}));
        }

        return lines.toArray(new String[][]{});
    }

    public Date getExamPassDate(ExamPassDiscipline examPassDiscipline) {
        ExamPassDisciplineISTU disciplineISTU = get(ExamPassDisciplineISTU.class, ExamPassDisciplineISTU.examPassDiscipline(), examPassDiscipline);

        if (disciplineISTU != null && disciplineISTU.getDateSetting() != null && disciplineISTU.getDateSetting().getPassDate() != null) {
            return disciplineISTU.getDateSetting().getPassDate();
        }
        else {
            // возможно указано в справочнике дат сдачи экзаменов
            Date dateSprav = getExamPassDisciplineDate(examPassDiscipline);
            if (dateSprav != null)
                return dateSprav;
        }
        return null;
    }

    private RtfDocument makePage1(Entrant entrant, EntrantRequest request) {

        IScriptItem templateDocument = (IScriptItem) getCatalogItem(UniecScriptItem.class, "enrollmentExamSheetNarfu_page1");
        RtfDocument template = (new RtfReader()).read(templateDocument.getCurrentTemplate());
        RtfDocument retVal = template.getClone();

        // 2 - получим мадифаки
        RtfInjectModifier varmod = createInjectModifier();

        varmod.put("numb_ab", StringUtils.trimToEmpty(request.getStringNumber()));
        varmod.put("FIO_ab", entrant.getPerson().getFullFio());
        // дату регистрации берем из заявления
        varmod.put("date", DateFormatter.DEFAULT_DATE_FORMATTER.format(request.getRegDate()));

        DaoFacade.getDaoPrintUtil().injectMainSecretary(varmod, request);
        DaoFacade.getDaoPrintUtil().injectSelectionCommission(varmod, request);

        varmod.modify(retVal);

        return retVal;
    }

    private EntrantRequest _getEntrantRequest(Entrant entrant)
    {

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EntrantRequest.class, "r")
                .joinEntity("r",
                            DQLJoinType.inner,
                            RequestedEnrollmentDirection.class,
                            "red",
                            DQLExpressions.eq(
                                    DQLExpressions.property(EntrantRequest.id().fromAlias("r")),
                                    DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().id().fromAlias("red"))))
                .where(eq(property(EntrantRequest.entrant().id().fromAlias("r")), value(entrant.getId())))
                .where(DQLExpressions.ne(DQLExpressions.property(RequestedEnrollmentDirection.state().code().fromAlias("red")),
                                         value(EntrantStateCodes.TAKE_DOCUMENTS_AWAY)))
                .column("r")
                .order(property(EntrantRequest.regNumber().fromAlias("r")));

        List<EntrantRequest> lst = dql.createStatement(getSession()).list();

        for (EntrantRequest r : lst) {
            return r;
        }
        return null;
    }


    public String getSecretary(OrgUnit formativOu)
    {
        String retVal = "";

        // основная позиция
        String pozMain = null;
        // запасная позиция
        String pozSecond = null;


        List<EmployeePost> lst = _getPositionList(formativOu);

        for (EmployeePost ep : lst) {
            String s_vrem = ep.getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle();

            if (s_vrem.toLowerCase().startsWith("ответ") && s_vrem.toLowerCase().indexOf("секр") != -1 && s_vrem.toLowerCase().endsWith("пк"))
                pozMain = ep.getPerson().getIdentityCard().getIof();

            if (s_vrem.toLowerCase().startsWith("ответ") && s_vrem.toLowerCase().indexOf("секр") != -1)
                pozSecond = ep.getPerson().getIdentityCard().getIof();

        }

        if (pozMain != null)
            retVal = pozMain;
        else {
            if (pozSecond != null)
                retVal = pozSecond;
        }
        return retVal;

    }

    public RequestedEnrollmentDirection getFirstRequestDirection
            (
                    EntrantRequest request
            )
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(RequestedEnrollmentDirection.class, "rd")
                .column("rd")
                .where(eq(property(RequestedEnrollmentDirection.entrantRequest().id().fromAlias("rd")), value(request.getId())))
                .order(property(RequestedEnrollmentDirection.priority().fromAlias("rd")));

        List<RequestedEnrollmentDirection> lst = dql.createStatement(getSession()).list();

        for (RequestedEnrollmentDirection r : lst) {
            return r;
        }
        return null;
    }


    private List<EmployeePost> _getPositionList(OrgUnit unit)
    {
        MQBuilder builder = new MQBuilder(EmployeePost.ENTITY_CLASS, "ep");
        builder.addJoin("ep", EmployeePost.L_ORG_UNIT, "org");
        builder.addJoin("ep", EmployeePost.L_POST_STATUS, "state");

        builder.add(MQExpression.eq("org", OrgUnit.P_ID, unit.getId()));
        builder.add(MQExpression.eq("state", EmployeePostStatus.P_CODE, "1"));

        List<EmployeePost> empls = builder.getResultList(getSession());
        return empls;
    }

    private RtfInjectModifier createInjectModifier()
    {
        RtfInjectModifier retVal = new RtfInjectModifier();
        return retVal;
    }

    private RtfTableModifier createTableModifier() {
        RtfTableModifier retVal = new RtfTableModifier();
        return retVal;
    }

    public List<ExamPassDiscipline> getDisciplineList(Entrant entrant)
    {
        // для абитуриента тащим все дисциплины для сдачи
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(ExamPassDiscipline.class, "dis")
                .column("dis")
                .where(eq(property(ExamPassDiscipline.entrantExamList().entrant().entrantId().fromAlias("dis")), value(entrant.getId())));
        List<ExamPassDiscipline> lst = dql.createStatement(getSession()).list();

        // сортировка
        Collections.sort(lst, ExamPassDiscipline.TITLED_COMPARATOR);
        return lst;
    }

    @Override
    public EntrantRequestNARFU getEntrantRequestNARFU(EntrantRequest request)
    {
        EntrantRequestNARFU ent = get(EntrantRequestNARFU.class,
                                      EntrantRequestNARFU.L_ENTRANT_REQUEST, request);

        return ent;
    }


    @Override
    public int getFinalBall(EntrantRequest request, boolean byFirstDirection)
    {
// сначала на основе заявы поднимем все выбранные вступ. испытания

        // List<ChosenEntranceDiscipline>

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(ChosenEntranceDiscipline.class, "r")
                .column("r")
                .where(eq(property(ChosenEntranceDiscipline.chosenEnrollmentDirection().entrantRequest().id().fromAlias("r")), value(request.getId())));

        if (byFirstDirection) {
            // включим фильтр на самое приоритетное направление подготовки
            RequestedEnrollmentDirection fdir = getFirstRequestDirection(request);
            dql.where(eq(property(ChosenEntranceDiscipline.chosenEnrollmentDirection().fromAlias("r")), value(fdir)));
        }

        List<ChosenEntranceDiscipline> lst = dql.createStatement(getSession()).list();
        Map<Long, Integer> _mapId = new HashMap<Long, Integer>();

        for (ChosenEntranceDiscipline r : lst) {
            int ball_previos = 0;
            int ball_current = 0;

            // дисциплины должны быть строго уникальны
            // + по дисциплине желательно брать максимум

            Long d_id = r.getEnrollmentCampaignDiscipline().getId();
            if (_mapId.containsKey(d_id))
                ball_previos = _mapId.get(d_id);
            else
                _mapId.put(d_id, ball_current);


            // найдем олимпиаду
            // выделить направление подготовки
            EnrollmentDirection ed = r.getChosenEnrollmentDirection().getEnrollmentDirection();
            CompensationType ct = r.getChosenEnrollmentDirection().getCompensationType();
            Discipline2RealizationWayRelation dis = r.getEnrollmentCampaignDiscipline();


            DQLSelectBuilder dqlOlimp = new DQLSelectBuilder()
                    .fromEntity(Discipline2OlympiadDiplomaRelation.class, "r")
                    .column("r")
                    .where(eq(property(Discipline2OlympiadDiplomaRelation.compensationType().id().fromAlias("r")), value(ct.getId())))
                    .where(eq(property(Discipline2OlympiadDiplomaRelation.discipline().id().fromAlias("r")), value(dis.getId())))
                    .where(eq(property(Discipline2OlympiadDiplomaRelation.enrollmentDirection().id().fromAlias("r")), value(ed.getId())))
                    .where(eq(property(Discipline2OlympiadDiplomaRelation.diploma().entrant().id().fromAlias("r")), value(request.getEntrant().getId())));

            List<Discipline2OlympiadDiplomaRelation> lstOlimp = dqlOlimp.createStatement(getSession()).list();

            if (lstOlimp != null && lstOlimp.size() > 0)
                ball_current += 100;
            else {
                // ищем егэ
                // сначала переведем в представление егэ
                DQLSelectBuilder dqlConv = new DQLSelectBuilder()
                        .fromEntity(ConversionScale.class, "r")
                        .column("r")
                        .where(eq(property(ConversionScale.discipline().fromAlias("r")), value(dis.getId())));
                List<ConversionScale> lstConv = dqlConv.createStatement(getSession()).list();

                StateExamSubject sem = null;

                if (lstConv != null && lstConv.size() > 0) {
                    sem = lstConv.get(0).getSubject();

                    // ну теперь оценку тащим
                    DQLSelectBuilder dqlMark = new DQLSelectBuilder()
                            .fromEntity(StateExamSubjectMark.class, "r")
                            .column("r")
                            .where(eq(property(StateExamSubjectMark.subject().id().fromAlias("r")), value(sem.getId())))
                            .where(eq(property(StateExamSubjectMark.certificate().entrant().id().fromAlias("r")), value(request.getEntrant().getId())));
                    // считаем ЕГЭ в любом состоянии, в том числе незачтенные
                    //.where(eq(property(StateExamSubjectMark.certificate().accepted().fromAlias("r")), value(true)));

                    List<StateExamSubjectMark> lstMark = dqlMark.createStatement(getSession()).list();

                    int max_mark = 0;
                    // по идее у нас может быть несколько сертификатов
                    for (StateExamSubjectMark mark : lstMark) {
                        if (mark.getMark() > max_mark)
                            max_mark = mark.getMark();
                    }

                    ball_current += max_mark;
                }

            }

            // в коллекцию кладем максимум
            if (ball_current > ball_previos)
                _mapId.put(d_id, ball_current);

        }

        int ballTotal = 0;
        for (Long key : _mapId.keySet()) {
            ballTotal += _mapId.get(key);
        }
        return ballTotal;
    }

}
