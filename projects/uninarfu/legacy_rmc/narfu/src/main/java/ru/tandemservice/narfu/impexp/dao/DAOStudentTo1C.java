package ru.tandemservice.narfu.impexp.dao;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressRu;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.tandemservice.movestudentrmc.entity.PersonNARFU;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntity;
import ru.tandemservice.narfu.utils.XMLPack;
import ru.tandemservice.unibasermc.util.rmcsettings.AppProperty;
import ru.tandemservice.unirmc.util.AddressUtil;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * Импорт студентов в 1с
 *
 * @author vch
 */
public class DAOStudentTo1C extends UniBaseDao implements IDAOStudentTo1C
{

    /**
     * регистрируем демона
     */
    public static final SyncDaemon DAEMON = new SyncDaemon(DAOStudentTo1C.class.getName(), 1 /* 1 раз в минуту*/, IDAOStudentTo1C.LOCKER_NAME)
    {
        @Override
        protected void main()
        {
            if (!AppProperty.RUN_DEMON() && !ForseRun)
                return;
            try
            {
                if (ForseRun || needDemonUp())
                {
                    // получим bean
                    IDAOStudentTo1C report = IDAOStudentTo1C.instance.get();
                    report.packStudentsInfoToFile();
                }
            } catch (Exception e)
            {
                this.logger.error(e.getMessage(), e);
                Debug.exception(e.getMessage(), e);
                throw new RuntimeException(e);
            }
        }
    };


    private static Date getStartDemonTime()
    {
        String timeStr = "5:00";
        try
        {
            String s = ApplicationRuntime.getProperty("rundemon.time");
            if (!StringUtils.isEmpty(s))
                timeStr = s;
        } catch (Exception ignored)
        {
        }

        Date d = new Date();
        try
        {
            d = new SimpleDateFormat("HH:mm").parse(timeStr);
        } catch (Exception ignored)
        {
        }

        return d;
    }

    private static boolean needDemonUp()
    {
        Date dNow = new Date();
        int minutesNow = dNow.getHours() * 60 + dNow.getMinutes();

        Date dStart = getStartDemonTime();
        int minutesStart = dStart.getHours() * 60 + dStart.getMinutes();

        // стартовать можно не позднее 10 минут после указанного времени
        return minutesNow > minutesStart && minutesNow < (minutesStart + 10);

    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    private SimpleDateFormat sdfFileName = new SimpleDateFormat("yyyy_MM_dd'T'HH_mm_ss");

    private Map<Long, MarkInfo> mapStudentMarks = new HashMap<>();
    private Map<Long, PersonNARFU> mapPersonNarfu = new HashMap<>();

    public static boolean IS_BEAZY = false;
    public static String LOG_STRING = "";

    /**
     * принудительный запуск
     */
    public static boolean ForseRun = false;

    @Override
    public void packStudentsInfoToFile() throws Exception
    {
        try
        {
            Date dateStart = new Date();

            IS_BEAZY = true;
            Debug.begin("packStudentsInfoToFile");
            Debug.message("Обработка студентов начата");


            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

            Document doc = getStudentInfoXML();
            DOMSource source = new DOMSource(doc);

            String path = ApplicationRuntime.getAppInstallPath();

            // проверим наличие директории
            File dir = new File(path, "impexp");
            if (!dir.exists())
            {
                final boolean isCreate = dir.mkdir();
                if (!isCreate)
                    throw new ApplicationException("Не вожможно сздать директорию \"impexp\"");
            }

            dir = new File(path, "impexp/1c");
            if (!dir.exists())
            {
                final boolean isCreate = dir.mkdir();
                if (!isCreate)
                    throw new ApplicationException("Не вожможно сздать директорию \"impexp/1c\"");
            }


            String fileName = "fromTU_" + sdfFileName.format(new Date()) + ".xml";

            StreamResult result = new StreamResult(new File(path, "impexp/1c/fromTU.xml"));
            StreamResult resultCurrent = new StreamResult(new File(path, "impexp/1c/" + fileName));

            transformer.transform(source, result);
            transformer.transform(source, resultCurrent);

            Date dateEnd = new Date();
            LOG_STRING = "Упаковка начали в " + sdf.format(dateStart) + " закончили в " + sdf.format(dateEnd);
        } catch (Exception e)
        {
            LOG_STRING = "Ошибка упаковки ";
            if (e.getMessage() != null)
                LOG_STRING += " " + e.getMessage();

            throw new RuntimeException(e);
        } finally
        {
            IS_BEAZY = false;
            ForseRun = false;

            Debug.end();
        }
    }


    @Override
    public Document getStudentInfoXML() throws ParserConfigurationException
    {
        Date dateStart = new Date();

        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
        docBuilder = docFactory.newDocumentBuilder();

        // root elements
        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement("root");
        doc.appendChild(rootElement);

        Element rootSystem = doc.createElement("system");
        rootElement.appendChild(rootSystem);

        // 1 - полим список студентов
        Element rootStudents = doc.createElement("students");
        rootElement.appendChild(rootStudents);

        List<Long> studentIds = _getStudentIds();

        List<Long> subArray = new ArrayList<>();
        int _total = studentIds.size();

        // обрабатываем студентов пачками
        // так хоть память освобождается
        int i = 0;
        for (Long id : studentIds)
        {
            i++;
            subArray.add(id);
            if (subArray.size() >= 10)
            {
                // обрабатываем пачку студентов
                List<Student> students = _getStudents(subArray);
                subArray.clear();
                for (Student student : students)
                {
                    _packStudentToXml(doc, rootStudents, student);
                }
                LOG_STRING = " Обработано " + Integer.toString(i) + " из " + Integer.toString(_total);
            }
        }

        // остаток
        List<Student> students = _getStudents(subArray);
        subArray.clear();
        for (Student student : students)
        {
            _packStudentToXml(doc, rootStudents, student);
        }


        // выводим данные по стипендиям
        List<StudentGrantEntity> lstGrants = _getStudentsGrants();

        Element rootGrants = doc.createElement("grants");
        rootElement.appendChild(rootGrants);
        for (StudentGrantEntity grant : lstGrants)
        {
            _packGrantToXml(doc, rootGrants, grant);
        }

        Date dateEnd = new Date();
        _addAttribute("dateStart", sdf.format(dateStart), doc, rootSystem);
        _addAttribute("dateEnd", sdf.format(dateEnd), doc, rootSystem);
        return doc;
    }


    private List<Long> _getStudentIds()
    {
        DQLSelectBuilder dqlStudent = new DQLSelectBuilder()
                .fromEntity(Student.class, "student")
                .column(property(Student.id().fromAlias("student")))
                        // Статус студента не учитываем, учитываем только состояние студента(не архивный)
                .where(eq(property(Student.archival().fromAlias("student")), value(false)));

        return dqlStudent.createStatement(getSession()).list();
    }

    private void fillPersonNARFUMap(List<Long> studentsId)
    {
        mapPersonNarfu = new HashMap<>();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student.class, "s")
                .joinEntity("s", DQLJoinType.inner, PersonNARFU.class, "ext",
                        DQLExpressions.eq(
                                DQLExpressions.property(Student.person().id().fromAlias("s")),
                                DQLExpressions.property(PersonNARFU.person().id().fromAlias("ext"))))
                .where(DQLExpressions.eqValue(DQLExpressions.property(Student.archival().fromAlias("s")), Boolean.FALSE))
                .where(DQLExpressions.in(DQLExpressions.property(Student.id().fromAlias("s")), studentsId))
                .column("s.id")
                .column("ext");

        List<Object[]> lst = getList(builder);

        for (Object[] obj : lst)
        {
            mapPersonNarfu.put((Long) obj[0], (PersonNARFU) obj[1]);
        }
    }


    /**
     * Созжаем кеш оценок по всем студентам
     */
    private void _fillAllMark(List<Long> studentsId)
    {
        mapStudentMarks = new HashMap<>();


        final DQLSelectBuilder markDql = new DQLSelectBuilder();
        markDql.fromEntity(SessionMark.class, "mark");

        markDql.joinPath(DQLJoinType.inner, SessionMark.slot().document().fromAlias("mark"), "document");

        // && !mark.isInSession()
        markDql.where(eq(property(SessionMark.slot().inSession().fromAlias("mark")), value(false)));

        // Статус студента не учитываем, учитываем только состояние студента(не архивный)
        markDql.where(eq(property(SessionMark.slot().studentWpeCAction().studentWpe().student().archival().fromAlias("mark")), value(false)));

        // мероприятия реестра должны быть актуальны
        markDql.where(isNull(SessionMark.slot().studentWpeCAction().removalDate().fromAlias("mark")));

        // берем оценки только из зачетки
        // зачетная книжка SessionStudentGradeBookDocument
        markDql.joinEntity("document", DQLJoinType.inner, SessionStudentGradeBookDocument.class, "gradebook", DQLExpressions.eq
                        (
                                DQLExpressions.property("gradebook.id"),
                                DQLExpressions.property("document.id")
                        )
        );


        // дисциплины реестра
        markDql.joinPath(DQLJoinType.inner, SessionMark.slot().studentWpeCAction().studentWpe().registryElementPart().registryElement().fromAlias("mark"), "regelem");

        // дисциплина реестра должна быть
        // inner join (сам столбец не нужен)
        markDql.joinEntity("regelem", DQLJoinType.inner, EppRegistryDiscipline.class, "regdiscipline", DQLExpressions.eq
                        (
                                DQLExpressions.property("regdiscipline.id"),
                                DQLExpressions.property("regelem.id")
                        )
        );
        // приклеим год и часть
        markDql.joinPath(DQLJoinType.inner, SessionMark.slot().studentWpeCAction().studentWpe().year().educationYear().fromAlias("mark"), "year");
        markDql.joinPath(DQLJoinType.inner, SessionMark.slot().studentWpeCAction().studentWpe().part().fromAlias("mark"), "part");
        markDql.column("mark");
        // id студента
        markDql.column(property(SessionMark.slot().studentWpeCAction().studentWpe().student().id().fromAlias("mark")));
        markDql.column(property(EducationYear.id().fromAlias("year")));
        markDql.column(property(YearDistributionPart.id().fromAlias("part")));
        // id оценки из кеша
        markDql.column(property(SessionMark.cachedMarkValue().id().fromAlias("mark")));
        // фильтр по студентам
        markDql.where(DQLExpressions.in(property(SessionMark.slot().studentWpeCAction().studentWpe().student().id().fromAlias("mark")), studentsId));
        // сортировки
        markDql.order(property(SessionMark.slot().studentWpeCAction().studentWpe().student().id().fromAlias("mark")));
        markDql.order(property(SessionMark.slot().studentWpeCAction().studentWpe().year().educationYear().intValue().fromAlias("mark")));
        markDql.order(property(SessionMark.slot().studentWpeCAction().studentWpe().part().number().fromAlias("mark")));

        List<Object[]> marks = markDql.createStatement(getSession()).list();

        Map<Long, EntityBase> mapTemp = new HashMap<>();

        for (Object[] objs : marks)
        {
            SessionMark mark = (SessionMark) objs[0];
            Long studentId = (Long) objs[1];

            MarkInfo mi = new MarkInfo();
            if (mapStudentMarks.containsKey(studentId))
                mi = mapStudentMarks.get(studentId);
            else
                mapStudentMarks.put(studentId, mi);

            // в кеш кладем
            {
                EducationYear year;
                YearDistributionPart part;

                Long yearId = (Long) objs[2];
                Long partId = (Long) objs[3];

                Long cachedMarkValueId = objs[4] == null ? null : (Long) objs[4];

                if (mapTemp.containsKey(yearId))
                    year = (EducationYear) mapTemp.get(yearId);
                else
                {
                    year = get(yearId);
                    mapTemp.put(yearId, year);
                }

                if (mapTemp.containsKey(partId))
                    part = (YearDistributionPart) mapTemp.get(partId);
                else
                {
                    part = get(partId);
                    mapTemp.put(partId, part);
                }

                SessionMarkCatalogItem markValue = null;
                if (cachedMarkValueId != null)
                {
                    if (mapTemp.containsKey(cachedMarkValueId))
                        markValue = (SessionMarkCatalogItem) mapTemp.get(cachedMarkValueId);
                    else
                    {
                        markValue = get(SessionMarkCatalogItem.class, cachedMarkValueId);
                        mapTemp.put(cachedMarkValueId, markValue);
                    }
                }
                // для дисциплин реестра
                mi.addMark(mark, markValue, year, part);
            }
        }
    }

    private void _packGrantToXml(Document doc, Element elem, StudentGrantEntity grant)
    {

        Element el_grant = doc.createElement("grant");
        elem.appendChild(el_grant);

        _addAttribute("student_id", Long.toString(grant.getStudent().getId()), doc, el_grant);

        ICatalogItem grantView = null;
        if (grant.getView() != null)
            grantView = grant.getView();
        XMLPack.MakeICatalogItem("pay_type", doc, el_grant, grantView);

        ICatalogItem educationYear = null;
        if (grant.getEduYear() != null)
            educationYear = grant.getEduYear();
        XMLPack.MakeICatalogItem("educationYear", doc, el_grant, educationYear);

        _addAttribute("month", grant.getMonth(), doc, el_grant);
        _addAttribute("or_num", grant.getOrderNumber(), doc, el_grant);

        String or_date = "";
        if (grant.getOrderDate() != null)
            or_date = sdf.format(grant.getOrderDate());
        _addAttribute("or_date", or_date, doc, el_grant);

        // статус
        XMLPack.MakeICatalogItem("status", doc, el_grant, grant.getStatus());
        // сумма
        _addAttribute("pay_amount", Double.toString(grant.getSum()), doc, el_grant);

    }

    private List<StudentGrantEntity> _getStudentsGrants()
    {
        Date currentDate = new Date();
        //Добавим условие:  В выгрузке должны учитываться приказы, дата проведения которых  попадает в текущий или предыдущий месяц
        //Первое число предыдущего месяца
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(DateUtils.addMonths(new Date(), -1));
        Date startDate = CoreDateUtils.getMonthFirstTimeMoment(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH));

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(StudentGrantEntity.class, "grants")
                .column("grants")
                        // Статус студента не учитываем, учитываем только состояние студента(не архивный)
                .where(eq(property(StudentGrantEntity.student().archival().fromAlias("grants")), value(false)))
                .where(DQLExpressions.or(
                        DQLExpressions.isNull(DQLExpressions.property(StudentGrantEntity.tmp().fromAlias("grants"))),
                        DQLExpressions.eq(DQLExpressions.property(StudentGrantEntity.tmp().fromAlias("grants")), value(false))
                ))
                .where(DQLExpressions.and(DQLExpressions.ge(DQLExpressions.property("grants", StudentGrantEntity.orderDate()), DQLExpressions.valueDate(startDate)),
                        DQLExpressions.le(DQLExpressions.property("grants", StudentGrantEntity.orderDate()), DQLExpressions.valueDate(currentDate))))
                .fetchPath(DQLJoinType.inner, StudentGrantEntity.student().fromAlias("grants"), "student")
                .order(property(Student.id().fromAlias("student")))
                .order(property(StudentGrantEntity.orderDate().fromAlias("grants")));

        return dql.createStatement(getSession()).list();
    }

    /**
     * упаковать студента в XML
     */
    private void _packStudentToXml(Document doc, Element elem, Student student)
    {

        Element el_student = doc.createElement("studend");
        elem.appendChild(el_student);

        _addAttribute("person_id", Long.toString(student.getPerson().getId()), doc, el_student);

        _addAttribute("id", Long.toString(student.getId()), doc, el_student);

        String _formativeId = "";

        if (student.getEducationOrgUnit() != null && student.getEducationOrgUnit().getFormativeOrgUnit() != null)
            _formativeId = Long.toString(student.getEducationOrgUnit().getFormativeOrgUnit().getId());

        _addAttribute("department_id", _formativeId, doc, el_student);

        _addAttribute("bookNumber", student.getBookNumber(), doc, el_student);
        _addAttribute("personalFileNumber", student.getPersonalFileNumber(), doc, el_student);

        String _personalNumber = student.getPersonalNumber();
        _addAttribute("personalNumber", _personalNumber, doc, el_student);

        int code = 1; // не россия по коду стран
        String digitalCode = "";
        if (student.getPerson().getIdentityCard().getCitizenship() != null && student.getPerson().getIdentityCard().getCitizenship() instanceof AddressCountry)
        {
            AddressCountry addressCountry = (AddressCountry) student.getPerson().getIdentityCard().getCitizenship();
            code = addressCountry.getCode();
            digitalCode = String.valueOf(addressCountry.getDigitalCode());
        }
        String citizenship = "0";
        if (code == 0) // код России
            citizenship = "1";


        _addAttribute("INN", student.getPerson().getInnNumber(), doc, el_student);

        PersonNARFU ext = mapPersonNarfu.get(student.getId());

        if (ext != null && ext.getResident() != null)
        {
            _addAttribute("taxpayerStatus", ext.getResident() ? "1" : "0", doc, el_student);
        } else
            _addAttribute("taxpayerStatus", null, doc, el_student);

        _addAttribute("digitalCode", digitalCode, doc, el_student);

        if (student.getPerson().getIdentityCard().getAddress() != null)
        {
            AddressDetailed addressDetailed = student.getPerson().getIdentityCard().getAddress() instanceof AddressDetailed ? (AddressDetailed) student.getPerson().getIdentityCard().getAddress() : null;
            AddressRu addressRu = student.getPerson().getIdentityCard().getAddress() instanceof AddressRu ? (AddressRu) student.getPerson().getIdentityCard().getAddress() : null;

            if (addressDetailed != null && addressDetailed.getCountry() != null && addressDetailed.getCountry().getCode() == 0)
            {
                Element el_address = doc.createElement("addressRegistration");
                el_student.appendChild(el_address);

                AddressBase address = student.getPerson().getIdentityCard().getAddress();

                _addAttribute("country", (addressDetailed.getCountry() != null) ? addressDetailed.getCountry().getTitle() : null, doc, el_address); //страна
                _addAttribute("index", addressRu != null ? addressRu.getInheritedPostCode() : null, doc, el_address); //Индекс
                _addAttribute("region", AddressUtil.getRegionTitle(address), doc, el_address); //Регион
                _addAttribute("area", AddressUtil.getAreaTitle(address), doc, el_address); //Район
                _addAttribute("city", AddressUtil.getCityTitle(address), doc, el_address); //Город
                _addAttribute("locality", AddressUtil.getLocalityTitle(address), doc, el_address); //Населенный пункт
                _addAttribute("street", (addressRu != null && addressRu.getStreet() != null) ? addressRu.getStreet().getTitleWithType() : null, doc, el_address); //Улица
                _addAttribute("house", addressRu != null ? addressRu.getHouseNumber() : null, doc, el_address); //Дом
                _addAttribute("houseUnit", addressRu != null ? addressRu.getHouseUnitNumber() : null, doc, el_address); //Корпус
                _addAttribute("flat", addressRu != null ? addressRu.getFlatNumber() : null, doc, el_address); //Квартира
            }
        }

        _addAttribute("citizenship", citizenship, doc, el_student);
        _addAttribute("firstname", student.getPerson().getIdentityCard().getFirstName(), doc, el_student);
        _addAttribute("lastname", student.getPerson().getIdentityCard().getLastName(), doc, el_student);
        _addAttribute("middlename", student.getPerson().getIdentityCard().getMiddleName(), doc, el_student);

        String birthdate = "";
        if (student.getPerson().getIdentityCard().getBirthDate() != null)
            birthdate = sdf.format(student.getPerson().getIdentityCard().getBirthDate());

        _addAttribute("birthdate", birthdate, doc, el_student);

        _packIdentityCard(doc, el_student, student.getPerson().getIdentityCard());

        // упакуем пол
        XMLPack.MakeICatalogItem("sex", doc, el_student, student.getPerson().getIdentityCard().getSex());

        // упакуем вид возмещения затрат
        XMLPack.MakeICatalogItem("compensationType", doc, el_student, student.getCompensationType());


        // курс
        ICatalogItem course = null;
        if (student.getCourse() != null)
            course = student.getCourse();
        XMLPack.MakeICatalogItem("course", doc, el_student, course);

        // группа
        ITitled group = null;
        if (student.getGroup() != null)
            group = student.getGroup();
        XMLPack.MakeITitledItem("group", doc, el_student, group);

        // статус студента
        ICatalogItem status = null;
        if (student.getStatus() != null)
            status = student.getStatus();
        XMLPack.MakeICatalogItem("status", doc, el_student, status);

        // упаковка специальности профиля и так далее
        EducationLevels educationLevel = null;

        if (
                student.getEducationOrgUnit() != null &&
                        student.getEducationOrgUnit().getEducationLevelHighSchool() != null &&
                        student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel() != null
                )
            educationLevel = student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();

        _packEducationLevel(educationLevel, doc, el_student);

        // упаковка успеваемости
        _packProgress(student, doc, el_student);
    }

    private void _packIdentityCard(Document doc, Element el_student,
                                   IdentityCard identityCard)
    {
        Element el_identityCard = doc.createElement("identityCard");
        el_student.appendChild(el_identityCard);

        // тип удостоверения личности
        XMLPack.MakeICatalogItem("cardType", doc, el_identityCard, identityCard.getCardType());
        _addAttribute("seria", identityCard.getSeria(), doc, el_identityCard);
        _addAttribute("number", identityCard.getNumber(), doc, el_identityCard);

    }

    /**
     * Упаковка успеваемости
     */
    private void _packProgress(Student student, Document doc, Element el_student)
    {

        Element el_progress = doc.createElement("progressinfos");
        el_student.appendChild(el_progress);
        MarkInfo mi = new MarkInfo();

        if (mapStudentMarks.containsKey(student.getId()))
            mi = mapStudentMarks.get(student.getId());

        // можно выводить оценки (по посл сессии)
        String progress = "-1";
        if (mi.getMapMarkLast() != null)
            progress = Integer.toString(mi.getProgress(mi.getMapMarkLast()));
        _addAttribute("progress", progress, doc, el_progress);

        // все по семестрам
        Map<MarkInfo.YearTerm, Map<MarkInfo.MarkCode, Integer>> mapAll = mi.getMapMarkAll();
        for (MarkInfo.YearTerm yt : mapAll.keySet())
        {
            _packMarkYT(mi, yt, mapAll.get(yt), doc, el_progress);
        }

    }

    private void _packMarkYT(MarkInfo mi, MarkInfo.YearTerm yt, Map<MarkInfo.MarkCode, Integer> map, Document doc, Element el_progress)
    {
        Element el_yt = doc.createElement("progressinfo");
        el_progress.appendChild(el_yt);

        String progress;
        progress = Integer.toString(mi.getProgress(map));

        _addAttribute("progress", progress, doc, el_yt);
        _addAttribute("year", Integer.toString(yt.getYear().getIntValue()), doc, el_yt);
        _addAttribute("partNumber", Integer.toString(yt.getPart().getNumber()), doc, el_yt);
        _addAttribute("partTitle", yt.getPart().getTitle(), doc, el_yt);

        // полная свалка по оценкам
        Element el_marks = doc.createElement("marks");
        el_yt.appendChild(el_marks);

        for (MarkInfo.MarkCode mk : map.keySet())
        {
            Element el_mark = doc.createElement("mark");
            el_marks.appendChild(el_mark);

            _addAttribute("markCode", mk.getCode(), doc, el_mark);
            _addAttribute("markTitle", mk.getTitle(), doc, el_mark);
            _addAttribute("markCount", Integer.toString(map.get(mk)), doc, el_mark);
        }
    }

    private void _packEducationLevel(EducationLevels educationLevel, Document doc, Element el_student)
    {
        Element el_eduLevel = doc.createElement("eduLevel");
        el_student.appendChild(el_eduLevel);

        if (educationLevel != null)
        {
            boolean _isProfile = false;
            // внутри все атрибутами
            StructureEducationLevels levelType = educationLevel.getLevelType();
            if (levelType != null)
                if (levelType.isProfile()
                        || (educationLevel.getEduProgramSpecialization() != null &&
                        educationLevel.getEduProgramSubject().getSubjectIndex().getProgramKind().getCode().equals(EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV)
                ))
                    _isProfile = true;

            String okso = educationLevel.getTitleCodePrefix();
            String title = educationLevel.getTitle();

            if (title == null && educationLevel.getParentLevel() != null)
                title = educationLevel.getParentLevel().getTitle();

            String speciality_name = title;

            String profile_code = "";
            String profile_name = "";

            if (_isProfile)
            {
                // в родителя
                if (educationLevel.getParentLevel() != null)
                {
                    profile_code = educationLevel.getParentLevel().getTitleCodePrefix();
                    profile_name = educationLevel.getParentLevel().getTitle();
                }
            }
             /*
              * в speciality_code и speciality_name для профиля выводим данные родительского НП(С)
			  */
            _addAttribute("speciality_code", _isProfile ? profile_code : okso, doc, el_eduLevel);
            _addAttribute("speciality_name", _isProfile ? profile_name : speciality_name, doc, el_eduLevel);

            _addAttribute("profile_code", _isProfile ? okso : profile_code, doc, el_eduLevel);
            _addAttribute("profile_name", _isProfile ? speciality_name : profile_name, doc, el_eduLevel);

        }

    }

    private void _addAttribute(String name, String val, Document doc, Element element)
    {
        Attr attr = doc.createAttribute(name);
        attr.setValue(val);
        element.setAttributeNode(attr);
    }

    private List<Student> _getStudents(List<Long> ids)
    {
        //Создадим кэш
        _fillAllMark(ids);
        fillPersonNARFUMap(ids);

        DQLSelectBuilder dqlStudent = new DQLSelectBuilder()
                .fromEntity(Student.class, "student")
                .column("student")
                        // пачка студентов
                .where(DQLExpressions.in(property(Student.id().fromAlias("student")), ids))
                        // предварительно вычитываем группы
                .fetchPath(DQLJoinType.inner, Student.person().fromAlias("student"), "person")
                .fetchPath(DQLJoinType.inner, Student.person().identityCard().fromAlias("student"), "identityCard")
                .fetchPath(DQLJoinType.inner, Student.educationOrgUnit().fromAlias("student"), "educationOrgUnit")
                .fetchPath(DQLJoinType.left, Student.group().fromAlias("student"), "groupstudent")
                        // Статус студента не учитываем, учитываем только состояние студента(не архивный)
                .where(eq(property(Student.archival().fromAlias("student")), value(false)));

        return dqlStudent.createStatement(getSession()).list();
    }
}
