package ru.tandemservice.narfu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.BooleanDBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;

import java.sql.PreparedStatement;

public class MS_narfu_1x0x0_1to2 extends IndependentMigrationScript {

    @Override
    public void run(DBTool tool) throws Exception {
        // TODO Auto-generated method stub

        if (tool.tableExists("personnarfu_t")) {
            if (!tool.columnExists("personnarfu_t", "needdormitoryonreceiptperiod_p")) {
                tool.createColumn("personnarfu_t", new BooleanDBColumn("needdormitoryonreceiptperiod_p"));

                PreparedStatement pst = tool.getConnection().prepareStatement("update personnarfu_t SET needdormitoryonreceiptperiod_p=?");
                pst.setBoolean(1, false);
                pst.execute();
            }

            if (!tool.columnExists("personnarfu_t", "needdormitoryonstudingperiod_p")) {
                tool.createColumn("personnarfu_t", new BooleanDBColumn("needdormitoryonstudingperiod_p"));

                PreparedStatement pst = tool.getConnection().prepareStatement("update personnarfu_t SET needdormitoryonstudingperiod_p=?");
                pst.setBoolean(1, false);
                pst.execute();
            }

        }
    }
}		


