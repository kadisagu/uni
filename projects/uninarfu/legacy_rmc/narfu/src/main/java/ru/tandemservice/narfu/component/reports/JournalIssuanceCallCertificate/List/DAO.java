package ru.tandemservice.narfu.component.reports.JournalIssuanceCallCertificate.List;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.read.biff.BiffException;
import jxl.write.*;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.narfu.entity.StudentDocumentRMC;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.OrgUnitKind;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

//import org.tandemframework.core.entity.ViewWrapper;
//import org.tandemframework.core.view.list.source.DynamicListDataSource;
//import org.tandemframework.hibsupport.builder.MQBuilder;
//import org.tandemframework.hibsupport.builder.expression.MQExpression;
//import org.tandemframework.hibsupport.dql.DQLExpressions;
//import ru.tandemservice.uni.entity.employee.StudentDocument;
//import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
//import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
//import java.lang.Boolean;
//------------------------------------------------------------------
//------------------------------------------------------------------

@SuppressWarnings("rawtypes")
public class DAO extends DefaultCatalogPubDAO implements IDAO
{

    private static final String STUDENT_DOCUMENT_RMC_CODE = "rmc.1";

    @Override
    public void prepare(DefaultCatalogPubModel model) {
        Model myModel = (Model) model;
        //----------------------------------------------------------------------------------------------------------
        //myModel.setStudentDocumentRMCMap(getStudentDocumentRMCData());
        
        myModel.setCourseModel(new LazySimpleSelectModel<>(Course.class));
        myModel.setFormativeOrgUnitModel(new DQLFullCheckSelectModel() {
        	@Override
        	protected DQLSelectBuilder query(String alias, String filter) {
        		DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(OrgUnit.class, alias)
        				.joinEntity(alias, DQLJoinType.inner, OrgUnitToKindRelation.class, "rel", eq(property(OrgUnitToKindRelation.orgUnit().id().fromAlias("rel")), property(OrgUnit.id().fromAlias(alias))))
        				.joinEntity("rel", DQLJoinType.inner, OrgUnitKind.class, "kind", eq(property(OrgUnitKind.id().fromAlias("kind")), property(OrgUnitToKindRelation.orgUnitKind().id().fromAlias("rel"))))
        				.where(eq(property(OrgUnitKind.code().fromAlias("kind")), value("2")))
        				.order(property(OrgUnit.title().fromAlias(alias)))
        				;
                FilterUtils.applySimpleLikeFilter(dql, alias, OrgUnit.title().s(), filter);
                return dql;
        	}
        });
        myModel.setDevelopformModel(new LazySimpleSelectModel<>(DevelopForm.class));
        //----------------------------------------------------------------------------------------------------------
    }

    @SuppressWarnings("unchecked")
    @Override
    public void prepareProjectsDataSource(Model model) {
    	
    	//-------------------------------------------------------------------------------------------------------------------------------------------
    	/*
        DynamicListDataSource<?> dataSource = model.getDataSource();
        Map<Long, StudentDocumentRMC> studentDocumentRMCMap = model.getStudentDocumentRMCMap();

        MQBuilder mqBuilder = new MQBuilder(StudentDocument.ENTITY_CLASS, "sd");
        mqBuilder.add(MQExpression.eq("sd", StudentDocument.studentDocumentType().code().s(), STUDENT_DOCUMENT_RMC_CODE));
        mqBuilder.applyOrder(model.getDataSource().getEntityOrder());
        mqBuilder.addOrder("sd", StudentDocument.formingDate().s(), OrderDirection.desc);

        UniBaseUtils.createPage(model.getDataSource(), mqBuilder, getSession());

        List<ViewWrapper<StudentDocument>> result = ViewWrapper.getPatchedList(dataSource);

        for (ViewWrapper<StudentDocument> viewWrapper : result) {
            StudentDocumentRMC studentDocumentRMC = studentDocumentRMCMap.get(viewWrapper.getId());
            viewWrapper.setViewProperty("dateStartCertification", studentDocumentRMC != null ? studentDocumentRMC.getDateStartCertification() : "");
            viewWrapper.setViewProperty("dateEndCertification", studentDocumentRMC != null ? studentDocumentRMC.getDateEndCertification() : "");
        }
        */
    	
    	DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentDocumentRMC.class, "sd")
    			.where(eq(property(StudentDocumentRMC.base().studentDocumentType().code().fromAlias("sd")), value(STUDENT_DOCUMENT_RMC_CODE)));

        FilterUtils.applySelectFilter(builder, "sd", StudentDocumentRMC.base().student().course(), model.getCourseList());
        FilterUtils.applySelectFilter(builder, "sd", StudentDocumentRMC.base().student().educationOrgUnit().formativeOrgUnit(), model.getFormativeOrgUnitList());
        FilterUtils.applySelectFilter(builder, "sd", StudentDocumentRMC.base().student().educationOrgUnit().developForm(), model.getDevelopformList());

        if(model.getStartDate() != null) {
        	builder.where(ge(property(StudentDocumentRMC.base().formingDate().fromAlias("sd")), valueDate(model.getStartDate())));
        }
        if(model.getEndDate() != null) {
        	builder.where(lt(property(StudentDocumentRMC.base().formingDate().fromAlias("sd")), valueDate(new Date(model.getEndDate().getTime() + 24 * 60 * 60 * 1000))));
        }        
    	
        //Сортировка
        DQLOrderDescriptionRegistry descriptionRegistry = new DQLOrderDescriptionRegistry(StudentDocumentRMC.class, "sd");
        if(model.getDataSource().getEntityOrder()!=null) {
        	descriptionRegistry.applyOrder(builder, model.getDataSource().getEntityOrder());
        }
        else {
        	builder.order(property(StudentDocumentRMC.base().formingDate().fromAlias("sd")), OrderDirection.desc);
        }
        
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    	//-------------------------------------------------------------------------------------------------------------------------------------------
    	
    }

    @Override
    public void preparePrintReport(Model model) throws WriteException, IOException, BiffException
    {
    	//---------------------------------------------------------------------------------------------------------
        //Map<Long, StudentDocumentRMC> studentDocumentRMCMap = model.getStudentDocumentRMCMap();
        //Map<DevelopForm, List<StudentDocument>> developForm2DocumentMap = getDevelopForm2StudentDocumentMap();
        
        Map<OrgUnit, List<StudentDocumentRMC>> orgUnitStudentDocumentMap = getOrgUnitStudentDocumentMap(model);
        //---------------------------------------------------------------------------------------------------------

        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, "JournalIssuanceCallCertificate_narfu");
        ByteArrayInputStream in = new ByteArrayInputStream(templateDocument.getCurrentTemplate());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Workbook tempalteWorkbook = Workbook.getWorkbook(in);

        // создаем печатную форму
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        WritableWorkbook workbook = Workbook.createWorkbook(out, tempalteWorkbook, ws);

        // шрифты
        WritableFont times12 = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
        WritableFont times11 = new WritableFont(WritableFont.TIMES, 11);
        
        //-------------------------------------------------------------------------------
        /*
        // формат ячеек
        WritableCellFormat rowFormat = new WritableCellFormat(times11);
        rowFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        rowFormat.setVerticalAlignment(VerticalAlignment.TOP);
        rowFormat.setAlignment(Alignment.JUSTIFY);
        rowFormat.setWrap(true);     
        */
        
        // Формат ячеек с выравниванием по центру
        WritableCellFormat rowCentreFormat = new WritableCellFormat(times11);
        rowCentreFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        rowCentreFormat.setVerticalAlignment(VerticalAlignment.TOP);
        rowCentreFormat.setAlignment(Alignment.CENTRE);
        rowCentreFormat.setWrap(true);      
        
        // Формат ячеек с выравниванием по левому краю
        WritableCellFormat rowLeftFormat = new WritableCellFormat(times11);
        rowLeftFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        rowLeftFormat.setVerticalAlignment(VerticalAlignment.TOP);
        rowLeftFormat.setAlignment(Alignment.LEFT);
        rowLeftFormat.setWrap(true);         
        //-------------------------------------------------------------------------------

        WritableCellFormat rowFirstColFormat = new WritableCellFormat(times12);
        rowFirstColFormat.setVerticalAlignment(VerticalAlignment.TOP);
        rowFirstColFormat.setAlignment(Alignment.CENTRE);
        rowFirstColFormat.setWrap(true);

        WritableCellFormat rowSecondColFormat = new WritableCellFormat(times12);
        rowSecondColFormat.setVerticalAlignment(VerticalAlignment.TOP);
        rowSecondColFormat.setAlignment(Alignment.LEFT);
        rowSecondColFormat.setLocked(true);
        rowSecondColFormat.setShrinkToFit(false);
        rowSecondColFormat.setWrap(false);
      
        WritableSheet sheet = workbook.getSheet(0);

        //-----------------------------------------------------------------------------------------------------------  
        /*
        //текущий учебный год
        EducationYear educationYear = get(EducationYear.class, "current", Boolean.TRUE);

        int rows = 9;
        int counter = 0;

        if (educationYear != null) {
            sheet.addCell(new Label(0, 6, educationYear.getTitle() + " учебный год", rowFirstColFormat));
        }
        else {
            sheet.addCell(new Label(0, 6, " учебный год", rowFirstColFormat));
        }
        */
        
        int rows = 9;
        
        StringBuilder period = new StringBuilder();
        period.append("за период");
        if(model.getStartDate() != null) {
        	period.append(" с ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getStartDate()));
        }
        if(model.getEndDate() != null) {
        	period.append(" по ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getEndDate()));
        }
        sheet.addCell(new Label(0, 6, period.toString(), rowFirstColFormat));
        
        //-----------------------------------------------------------------------------------------------------------
        /*
        for (Map.Entry<DevelopForm, List<StudentDocument>> entry : developForm2DocumentMap.entrySet()) {

            if (counter == 0)
                sheet.addCell(new Label(0, 7, entry.getKey().getTitle() + " форма обучения", rowSecondColFormat));
            else {
                sheet.addCell(new Label(0, rows, entry.getKey().getTitle() + " форма обучения", rowSecondColFormat));
                rows++;
            }

            for (StudentDocument studentDocument : entry.getValue()) {
                StudentDocumentRMC studentDocumentRMC = studentDocumentRMCMap.get(studentDocument.getId());
                counter++;
                sheet.addCell(new Label(0, rows, String.valueOf(counter), rowFormat));
                sheet.addCell(new Label(1, rows, DateFormatter.DEFAULT_DATE_FORMATTER.format(studentDocument.getFormingDate()), rowFormat));
                sheet.addCell(new Label(2, rows, studentDocument.getStudent().getPerson().getFullFio(), rowFormat));
                sheet.addCell(new Label(3, rows, studentDocument.getStudent().getEducationOrgUnit().getFormativeOrgUnit().getTitle(), rowFormat));
                sheet.addCell(new Label(4, rows, studentDocument.getStudent().getEducationOrgUnit().getTitle(), rowFormat));
                sheet.addCell(new Label(5, rows, StringUtils.trimToEmpty(studentDocument.getStudent().getCourse().getTitle()), rowFormat));
                if (studentDocumentRMC != null) {
                    sheet.addCell(new Label(6, rows, DateFormatter.DEFAULT_DATE_FORMATTER.format(studentDocumentRMC.getDateStartCertification()), rowFormat));
                    sheet.addCell(new Label(7, rows, DateFormatter.DEFAULT_DATE_FORMATTER.format(studentDocumentRMC.getDateEndCertification()), rowFormat));
                }
                else {
                    sheet.addCell(new Label(6, rows, "", rowFormat));
                    sheet.addCell(new Label(7, rows, "", rowFormat));
                }
                sheet.addCell(new Label(8, rows, "", rowFormat));

                rows++;
            }

            rows++;

        }
        */
        
        List<OrgUnit> keyList = new ArrayList<>(orgUnitStudentDocumentMap.keySet());
        Collections.sort(keyList, new EntityComparator<>(new EntityOrder(OrgUnit.title(), OrderDirection.asc)));

        for (OrgUnit orgUnit : keyList) {
        	
            sheet.mergeCells(0, rows, 8, rows);
        	sheet.addCell(new Label(0, rows, orgUnit.getTitle(), rowSecondColFormat));
        	rows++;
          
        	for (StudentDocumentRMC studentDocumentRMC : orgUnitStudentDocumentMap.get(orgUnit)) {
        		
                sheet.addCell(new Label(0, rows, String.valueOf(studentDocumentRMC.getBase().getNumber()), rowCentreFormat));
                sheet.addCell(new Label(1, rows, DateFormatter.DEFAULT_DATE_FORMATTER.format(studentDocumentRMC.getBase().getFormingDate()), rowCentreFormat));
                sheet.addCell(new Label(2, rows, studentDocumentRMC.getBase().getStudent().getPerson().getFullFio(), rowLeftFormat));
                sheet.addCell(new Label(3, rows, studentDocumentRMC.getBase().getStudent().getEducationOrgUnit().getDevelopForm().getTitle(), rowCentreFormat));
                sheet.addCell(new Label(4, rows, studentDocumentRMC.getBase().getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle(), rowLeftFormat));
                sheet.addCell(new Label(5, rows, StringUtils.trimToEmpty(studentDocumentRMC.getBase().getStudent().getCourse().getTitle()), rowCentreFormat));
                if (studentDocumentRMC.getDateStartCertification() != null) {
                    sheet.addCell(new Label(6, rows, DateFormatter.DEFAULT_DATE_FORMATTER.format(studentDocumentRMC.getDateStartCertification()), rowCentreFormat));
                }
                else {
                	sheet.addCell(new Label(6, rows, "", rowCentreFormat));
                };
                if(studentDocumentRMC.getDateEndCertification() != null) {
                	sheet.addCell(new Label(7, rows, DateFormatter.DEFAULT_DATE_FORMATTER.format(studentDocumentRMC.getDateEndCertification()), rowCentreFormat));
                }
                else {
                    sheet.addCell(new Label(7, rows, "", rowCentreFormat));
                }
                sheet.addCell(new Label(8, rows, "", rowCentreFormat));

                rows++;
        		
        	}
        	rows++;
        }
           
        //-------------------------------------------------------------------------------------------------------
        workbook.write();
        workbook.close();
        model.setContent(out.toByteArray());
        model.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_VND_MS_EXCEL);
        model.setFileName("JournalIssuanceCallCertificate.xls");

        getSession().clear();

    }

    //-----------------------------------------------------------------------------------------------------------------------------------------------------
    /*
    private Map<Long, StudentDocumentRMC> getStudentDocumentRMCData() {

        Map<Long, StudentDocumentRMC> studentDocumentRMCMap = new HashMap<>();

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(StudentDocumentRMC.class, "s")
                .where(DQLExpressions.in(
                        DQLExpressions.property(StudentDocumentRMC.base().id().fromAlias("s")),
                        new DQLSelectBuilder()
                                .fromEntity(StudentDocument.class, "sd")
                                .column(StudentDocument.id().fromAlias("sd").s()).buildQuery()))
                .column("s");

        List<StudentDocumentRMC> lst = builder.createStatement(getSession()).list();

        for (StudentDocumentRMC studentDocumentRMC : lst) {
        	//-------------------------------------------------------------------------
            //studentDocumentRMCMap.put(studentDocumentRMC.getId(), studentDocumentRMC);
            studentDocumentRMCMap.put(studentDocumentRMC.getBase().getId(), studentDocumentRMC);
            //-------------------------------------------------------------------------
        }

        return studentDocumentRMCMap;
    }

    private Map<DevelopForm, List<StudentDocument>> getDevelopForm2StudentDocumentMap() {
        Map<DevelopForm, List<StudentDocument>> developForm2DocumentMap = new LinkedHashMap<>();

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(StudentDocument.class, "sd")
                .where(DQLExpressions.in(
                        DQLExpressions.property(StudentDocument.student().educationOrgUnit().developForm().fromAlias("sd")),
                        new DQLSelectBuilder().fromEntity(DevelopForm.class, "d").column("d").buildQuery()))
                .order(DQLExpressions.property(StudentDocument.student().educationOrgUnit().educationLevelHighSchool().educationLevel().okso().fromAlias("sd")), OrderDirection.desc)
                .order(DQLExpressions.property(StudentDocument.student().course().title().fromAlias("sd")), OrderDirection.desc)
                .column("sd");

        List<StudentDocument> studentDocumentList = dql.createStatement(getSession()).list();

        for (StudentDocument studentDocument : studentDocumentList) {
            DevelopForm developForm = studentDocument.getStudent().getEducationOrgUnit().getDevelopForm();
            List<StudentDocument> lst = developForm2DocumentMap.get(developForm);
            if (lst == null) {
                lst = new ArrayList<>();
            }
            lst.add(studentDocument);
            developForm2DocumentMap.put(developForm, lst);
        }

        return developForm2DocumentMap;
    }
    */
    
    private Map<OrgUnit, List<StudentDocumentRMC>> getOrgUnitStudentDocumentMap(Model model) {
    	
    	Map<OrgUnit, List<StudentDocumentRMC>> orgUnitStudentDocumentMap = new HashMap<>();
    	
    	DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentDocumentRMC.class, "sd")
    			.where(eq(property(StudentDocumentRMC.base().studentDocumentType().code().fromAlias("sd")), value(STUDENT_DOCUMENT_RMC_CODE)));

        FilterUtils.applySelectFilter(builder, "sd", StudentDocumentRMC.base().student().course(), model.getCourseList());
        FilterUtils.applySelectFilter(builder, "sd", StudentDocumentRMC.base().student().educationOrgUnit().formativeOrgUnit(), model.getFormativeOrgUnitList());
        FilterUtils.applySelectFilter(builder, "sd", StudentDocumentRMC.base().student().educationOrgUnit().developForm(), model.getDevelopformList());

        if(model.getStartDate() != null) {
        	builder.where(ge(property(StudentDocumentRMC.base().formingDate().fromAlias("sd")), valueDate(model.getStartDate())));
        }
        if(model.getEndDate() != null) {
        	builder.where(lt(property(StudentDocumentRMC.base().formingDate().fromAlias("sd")), valueDate(new Date(model.getEndDate().getTime() + 24 * 60 * 60 * 1000))));
        } 

        builder.order(property(StudentDocumentRMC.base().formingDate().fromAlias("sd")), OrderDirection.asc);
        
        List<StudentDocumentRMC> studentDocumentRMCList = builder.createStatement(getSession()).list();
        
        for (StudentDocumentRMC studentDocumentRMC : studentDocumentRMCList) {
            OrgUnit orgUnit = studentDocumentRMC.getBase().getStudent().getEducationOrgUnit().getFormativeOrgUnit();
            List<StudentDocumentRMC> lst = orgUnitStudentDocumentMap.get(orgUnit);
            if (lst == null) {
                lst = new ArrayList<>();
            }
            lst.add(studentDocumentRMC);
            orgUnitStudentDocumentMap.put(orgUnit, lst);
        }  
        
    	return orgUnitStudentDocumentMap;
    }
    //-----------------------------------------------------------------------------------------------------------------------------------------------------

}
