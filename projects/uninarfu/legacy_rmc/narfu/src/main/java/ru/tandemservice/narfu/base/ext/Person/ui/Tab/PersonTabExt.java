package ru.tandemservice.narfu.base.ext.Person.ui.Tab;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.IPresenterExtensionBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.person.base.bo.Person.ui.Tab.PersonTab;

@Configuration
public class PersonTabExt extends BusinessComponentExtensionManager {

    public static final String ADDON = "pExt";

    @Autowired
    private PersonTab personTab;

    @Bean
    public PresenterExtension presenterExtension() {

        IPresenterExtensionBuilder pi = presenterExtensionBuilder(personTab.presenterExtPoint());
        pi.addAddon(uiAddon(ADDON, PersonTabUIExt.class));
        return pi.create();
    }
}
