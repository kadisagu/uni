package ru.tandemservice.narfu.base.ext.EcOrder.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.base.bo.EcOrder.logic.IEnrollmentExtractFactory;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentParagraph;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.Collection;
import java.util.List;

public interface IEcOrderDaoExt extends INeedPersistenceSupport {

    public RtfDocument getDownloadPrintPersonalInfo(Long orderId);

    public RtfDocument getDownloadPrintExtracts(Long orderId);

    public RtfDocument getDownloadPrintParagraphExtracts(List<IAbstractParagraph<IAbstractOrder>> paragraph);

    public RtfDocument getDownloadPrintParagraphPersonalInfo(List<IAbstractParagraph<IAbstractOrder>> paragraph);

    public RtfDocument getDownloadPrintExtract(EnrollmentExtract extract);

    public void saveOrUpdateAspirantParagraphExt(EnrollmentOrder order,
                                                 EnrollmentParagraph paragraph,
                                                 IEnrollmentExtractFactory extractFactory,
                                                 boolean enrollParagraphPerEduOrgUnit,
                                                 Collection<PreliminaryEnrollmentStudent> preStudents,
                                                 PreliminaryEnrollmentStudent manager, OrgUnit formativeOrgUnit,
                                                 EducationLevelsHighSchool educationLevelsHighSchool,
                                                 DevelopForm developForm, Course course, String groupTitle, EducationOrgUnit educationOrgUnit);

}
