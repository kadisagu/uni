package ru.tandemservice.narfu.uniec.component.report.EntrantRegistration.EntrantRegistrationAdd;

import com.ibm.icu.text.SimpleDateFormat;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.eventssecrmc.dao.DaoFacade;
import ru.tandemservice.narfu.entity.EntrantRequestNARFU;
import ru.tandemservice.narfu.entity.FormingOrgUnit2Commission;
import ru.tandemservice.narfu.uniec.dao.print.IEntrantExamListPrintBean;
import ru.tandemservice.uniecrmc.dao.UniecrmcDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.catalog.codes.EntrantStateCodes;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * @author vch
 *         Переделывал быстро, за Ситеком
 */
public class NarfuEntrantRegistrationReportBuilder {

    private IEntrantExamListPrintBean examListBean = (IEntrantExamListPrintBean) ApplicationRuntime.getBean("entrantExamListPrintBean");
    private Model _model;
    protected Date date;
    protected Session _session;

    public NarfuEntrantRegistrationReportBuilder(Model model, Session session) {
        _model = model;
        _session = session;
        if (model != null)
            this.date = model.getReport().getDateFrom();
    }


    public DatabaseFile getContent() {
        byte data[] = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    protected byte[] buildReport() {
        List<OrgUnit> lst = getFormativeOrgUnitList(null, date);

        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, getTemplateDocumentName());

        RtfDocument document = (new RtfReader()).read(templateDocument.getCurrentTemplate().clone());
        document.getElementList().clear();

        int i = 0;
        for (OrgUnit ou : lst) {
            // внутри орг унита по направлениям подготовки
            List<EducationLevelsHighSchool> lsdEduLevels = getEducationLevelsHighSchoolList(null, date, ou);

            //List<EducationLevelsHighSchool> lsdEduLevels = _getEducationLevelsHighSchoolList(null, null, ou);


            for (EducationLevelsHighSchool hs : lsdEduLevels) {
                if (i > 0)
                    // это след. страница - ставим разрыв
                    document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));

                RtfDocument reportPart = buildReport(ou, hs);
                document.getElementList().addAll(reportPart.getElementList());

                i++;
            }

        }

        return RtfUtil.toByteArray(document);
    }

    protected String getTemplateDocumentName() {
        return "narfuEntrantRegistration";
    }

    protected List<EducationLevelsHighSchool> getEducationLevelsHighSchoolList(
            Date startDate
            , Date endDate
            , OrgUnit orgUnit)
    {

        DQLSelectBuilder dql = _getDQLRequestedEnrollmentDirection(startDate, endDate, orgUnit, null, null);
        dql.column(property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().id().fromAlias("rd")));


        DQLSelectBuilder dqlRequest = new DQLSelectBuilder()
                .fromEntity(EducationLevelsHighSchool.class, "r")
                .column("r")
                .where(in(property(EducationLevelsHighSchool.id().fromAlias("r")), dql.buildQuery()));

        List<EducationLevelsHighSchool> lst = dqlRequest.createStatement(_session).list();
        return lst;
    }

    protected List<OrgUnit> getFormativeOrgUnitList(Date startDate, Date endDate)
    {
        DQLSelectBuilder dql = _getDQLRequestedEnrollmentDirection(startDate, endDate, null, null, null);
        dql.column(property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().formativeOrgUnit().id().fromAlias("rd")));

        // список формирующих подразделений
        DQLSelectBuilder dqlRequest = new DQLSelectBuilder()
                .fromEntity(OrgUnit.class, "r")
                .column("r")
                .where(in(property(OrgUnit.id().fromAlias("r")), dql.buildQuery()));

        List<OrgUnit> lst = dqlRequest.createStatement(_session).list();

        return lst;
    }

    protected DQLSelectBuilder _getDQLRequestedEnrollmentDirection(
            Date startDate
            , Date endDate
            , OrgUnit formativOU
            , EducationLevelsHighSchool hs
            , List<EntrantState> states
    )
    {
        Calendar cal = Calendar.getInstance();

        // готовим дату
        Date dStart = null;
        if (startDate != null) {
            dStart = (Date) startDate.clone();

            cal.setTime(dStart);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            dStart = (Date) cal.getTime().clone();

        }

        Date dEnd = null;
        if (endDate != null) {
            dEnd = (Date) endDate.clone();

            cal.setTime(dEnd);
            cal.set(Calendar.HOUR_OF_DAY, 23);
            cal.set(Calendar.MINUTE, 59);
            cal.set(Calendar.SECOND, 59);
            cal.set(Calendar.MILLISECOND, 999);
            dEnd = (Date) cal.getTime().clone();
        }

        // 1 - выбранные направления подготовки
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(RequestedEnrollmentDirection.class, "rd")
                        // не в архиве
                .where(eq(property(RequestedEnrollmentDirection.entrantRequest().entrant().archival().fromAlias("rd")), value(false)));

        // по дате (точное соответствие)
        if (dStart != null)
            dql.where(ge(property(RequestedEnrollmentDirection.entrantRequest().regDate().fromAlias("rd")), valueDate(dStart)));

        if (dEnd != null)
            dql.where(le(property(RequestedEnrollmentDirection.entrantRequest().regDate().fromAlias("rd")), valueDate(dEnd)));


        // по формирующему подразделению
        List<OrgUnit> formOuList = null;
        if (formativOU != null)
            formOuList = Arrays.asList(formativOU);
        else if (_model != null && _model.isFormativeOrgUnitActive())
            formOuList = _model.getFormativeOrgUnitList();
        fillDQLRequestedEnrollmentDirection(dql, formOuList);
            /*
	    	if (formativOU!=null)
	    		fillDQLRequestedEnrollmentDirection(dql, formativOU);
	    	else {
		    	if (_model.isFormativeOrgUnitActive())
		    		dql.where(in(property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().formativeOrgUnit().fromAlias("rd")), value(_model.getFormativeOrgUnitList())));
	    	}
*/
        // по направлению подготовки
        if (hs != null)
            dql.where(eq(property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().fromAlias("rd")), value(hs)));
        else {
            if (_model != null && _model.isEducationLevelHighSchoolActive())
                dql.where(in(property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().fromAlias("rd")), _model.getEducationLevelHighSchoolList()));
        }
        // по форме освоения
        if (_model != null && _model.isDevelopFormActive())
            dql.where(in(property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developForm().fromAlias("rd")), _model.getDevelopFormList()));

        // по условиям освоения
        if (_model != null && _model.isDevelopConditionActive())
            dql.where(in(property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developCondition().fromAlias("rd")), _model.getDevelopConditionList()));

        if (_model != null && _model.isCompensationTypeActive())
            dql.where(in(property(RequestedEnrollmentDirection.compensationType().fromAlias("rd")), value(_model.getCompensationType())));

        // по статусу
        if (states != null)
            dql.where(in(property(RequestedEnrollmentDirection.state().fromAlias("rd")), states));
        return dql;

    }

    protected void fillDQLRequestedEnrollmentDirection(DQLSelectBuilder dql, List<OrgUnit> orgUnitList) {
        if (orgUnitList != null)
            dql.where(in(property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().formativeOrgUnit().fromAlias("rd")), orgUnitList));

        dql.where(eq(property(RequestedEnrollmentDirection.priority().fromAlias("rd")), value(1)));

        // по приемной компании
        dql.where(eq(property(RequestedEnrollmentDirection.entrantRequest().entrant().enrollmentCampaign().fromAlias("rd")), value(_model.getEnrollmentCampaign())));
    }

    private List<EntrantRequest> _getEntrantRequestList(Date startDate, Date endDate, OrgUnit formativeOU, EducationLevelsHighSchool hs, List<EntrantState> states)
    {
        // 1 - выбранные направления подготовки
        DQLSelectBuilder dql = _getDQLRequestedEnrollmentDirection(startDate, endDate, formativeOU, hs, states);
        dql.column(property(RequestedEnrollmentDirection.entrantRequest().id().fromAlias("rd")));

        // сами заявления, в алфавитном порядке
        DQLSelectBuilder dqlRequest = new DQLSelectBuilder()
                .fromEntity(EntrantRequest.class, "r")
                .column("r")
                .order(property(EntrantRequest.entrant().person().identityCard().fullFio().fromAlias("r")))
                .where(in(property(EntrantRequest.id().fromAlias("r")), dql.buildQuery()));

        List<EntrantRequest> lst = dqlRequest.createStatement(_session).list();

        return lst;
    }

    private RtfDocument buildReport(OrgUnit ou, EducationLevelsHighSchool hs)
    {

        List<EntrantState> lstStateTakeAway = new ArrayList<EntrantState>();
        lstStateTakeAway.add(UniDaoFacade.getCoreDao().getCatalogItem(EntrantState.class, EntrantStateCodes.TAKE_DOCUMENTS_AWAY));


        int _requestAll = 0; // всего заяв с начала приемки
        int _takeAwayAll = 0; // всего отозвано заяв
        int _takeAway = 0; // отозвано сегодня
        int _request = 0;  // заявления за сегодня

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String sDateTo = sdf.format(date);


        // список заявлений
        List<EntrantRequest> lst = _getEntrantRequestList(date, date, ou, hs, null);
        String[][] table = new String[lst.size()][6];

        List<CompensationType> lstCompensationType = new ArrayList<CompensationType>();
        List<DevelopCondition> lstDevelopCondition = new ArrayList<DevelopCondition>();
        List<DevelopForm> lstDevelopForm = new ArrayList<DevelopForm>();

        _request = lst.size();


        int row = 0;
        for (EntrantRequest request : lst) {
            String[] arr = getRow(request, lstCompensationType, lstDevelopCondition, lstDevelopForm);
            arr[0] = Integer.toString(row + 1);
            table[row] = arr;

            row++;
        }


        IScriptItem templateDocument = (IScriptItem) UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, getTemplateDocumentName());
        RtfDocument document = (new RtfReader()).read(templateDocument.getCurrentTemplate().clone());


        RtfTableModifier rtfTableModifier = new RtfTableModifier();
        rtfTableModifier.put("TableDisc", table);
        rtfTableModifier.modify(document);


        RtfInjectModifier injectModifier = new RtfInjectModifier();

        String form_depart = !StringUtils.isEmpty(ou.getNominativeCaseTitle()) ? ou.getNominativeCaseTitle() : ou.getTitle();
        injectModifier.put("form_depart", form_depart);


        injectModifier.put("date", DateFormatter.DEFAULT_DATE_FORMATTER.format(date));

        String speciality = hs != null ? hs.getPrintTitle() : "";
        injectModifier.put("speciality", speciality);


        String compens = "";
        compens = UniStringUtils.join(lstCompensationType, "title", ", ");
        injectModifier.put("compens", compens);


        String short_term = UniStringUtils.join(lstDevelopCondition, "title", ", ");
        injectModifier.put("shortTerm", short_term);

        String devForm = UniStringUtils.join(lstDevelopForm, "title", ", ");
        injectModifier.put("devForm", devForm);

        String delegatePost = DaoFacade.getDaoPrintUtil().getDelegatePost(getSelectionComissionOrgUnit(_model.getEnrollmentCampaign(), ou));
        injectModifier.put("delegatePost", delegatePost);

        String delegateFio = DaoFacade.getDaoPrintUtil().getDelegateIof(getSelectionComissionOrgUnit(_model.getEnrollmentCampaign(), ou));
        injectModifier.put("delegateFio", delegateFio);

        injectModifier.put("form_depart", ou.getFullTitle());

        //injectModifier.put("form_depart_G", ou.getGenitiveCaseTitle());

        // статистика
        // всего заявлений
        List<EntrantRequest> lstAll = _getEntrantRequestList(
                null
                , date
                , ou
                , hs
                , null);

        _requestAll = lstAll.size();

        // получим все заявления со статусом - забрал документы
        // анализируем каждое
        List<EntrantRequest> lstTakeAway = _getEntrantRequestList(
                null
                , date
                , ou
                , hs
                , lstStateTakeAway);

        for (EntrantRequest request : lstTakeAway) {
            RequestedEnrollmentDirection direction = UniecrmcDao.Instanse().getFirstRequestDirection(request);

            if (direction.getState().getCode().equals(EntrantStateCodes.TAKE_DOCUMENTS_AWAY)) {
                // забрал документы - смотрим, а есть-ли спец объект для narfu
                EntrantRequestNARFU requestNarfu = examListBean.getEntrantRequestNARFU(request);
                if (requestNarfu != null) {
                    // смотрим только на даты
                    if (requestNarfu.getDateDocumentsTakeAway() != null) {
                        Calendar cal = Calendar.getInstance();

                        // готовим дату
                        Date dStart = (Date) date.clone();

                        cal.setTime(dStart);
                        cal.set(Calendar.HOUR_OF_DAY, 23);
                        cal.set(Calendar.MINUTE, 59);
                        cal.set(Calendar.SECOND, 59);
                        cal.set(Calendar.MILLISECOND, 999);
                        dStart = (Date) cal.getTime().clone();


                        if (requestNarfu.getDateDocumentsTakeAway().getTime() < dStart.getTime())
                            _takeAwayAll++;

                        // если сегодня
                        if (sDateTo.equals(sdf.format(requestNarfu.getDateDocumentsTakeAway())))
                            _takeAway++;
                    }
                }
                else {
                    _takeAway += 1;
                    _takeAwayAll += 1;
                }
            }
        }

        // вставляем итоги
        injectModifier.put("day_order_all", Integer.toString(_request));
        injectModifier.put("day_order_cancel", Integer.toString(_takeAway));

        injectModifier.put("all_order", Integer.toString(_requestAll));
        injectModifier.put("all_order_cancel", Integer.toString(_takeAwayAll));

        injectModifier.modify(document);
        return document;
    }

    // arr[0]: проставить номер нужно потом
    protected String[] getRow(EntrantRequest request, List<CompensationType> lstCompensationType, List<DevelopCondition> lstDevelopCondition, List<DevelopForm> lstDevelopForm) {
        String[] arr = new String[6];

        arr[1] = request.getStringNumber();
        arr[2] = request.getEntrant().getPerson().getFullFio();

        RequestedEnrollmentDirection direction = UniecrmcDao.Instanse().getFirstRequestDirection(request);

        CompensationType ct = direction.getCompensationType();
        if (!lstCompensationType.contains(ct))
            lstCompensationType.add(ct);

        DevelopCondition dc = direction.getEnrollmentDirection().getEducationOrgUnit().getDevelopCondition();
        if (!lstDevelopCondition.contains(dc))
            lstDevelopCondition.add(dc);

        DevelopForm df = direction.getEnrollmentDirection().getEducationOrgUnit().getDevelopForm();
        if (!lstDevelopForm.contains(df))
            lstDevelopForm.add(df);

        String original = "Нет";
        if (direction.isOriginalDocumentHandedIn())
            original = "оригинал";

        arr[4] = original;

        PersonEduInstitution pei = request.getEntrant().getPerson().getPersonEduInstitution();
        if (pei == null)
            arr[3] = "нд";
        else {
            String docInfo = "";
            if (pei.getSeria() != null)
                docInfo = pei.getSeria();

            if (pei.getNumber() != null)
                docInfo += " " + pei.getNumber();

            arr[3] = docInfo;
        }

        // ставим баллы по ЕГЭ или нечто, что просил Аналитик
        arr[5] = Integer.toString(examListBean.getFinalBall(request, true));

        return arr;
    }

    private OrgUnit getSelectionComissionOrgUnit(EnrollmentCampaign enrollmentCampaign, OrgUnit ou) {

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(FormingOrgUnit2Commission.class, "ou2c")
                .where(DQLExpressions.eqValue(
                        DQLExpressions.property(FormingOrgUnit2Commission.enrollmentCampaign().id().fromAlias("ou2c")),
                        enrollmentCampaign.getId()))
                .where(DQLExpressions.eqValue(
                        DQLExpressions.property(FormingOrgUnit2Commission.formingOrgUnit().id().fromAlias("ou2c")),
                        ou.getId()))
                .column(FormingOrgUnit2Commission.commission().fromAlias("ou2c").s());

        OrgUnit selectionOrgUnit = builder.createStatement(_session).uniqueResult();

        return selectionOrgUnit;
    }

    public Model getModel()
    {
        return this._model;
    }
}
