package ru.tandemservice.narfu.base.bo.ReportPersonNarfu.ui.Add.block.personnarfuData;

import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.entity.IdentifiableWrapper;

import java.util.Arrays;

public class PersonnarfuDataBlock {

    // имена источников данных
    public static final String NEED_DORMITORY_ON_RECIPT_PERIOD_DS = "needDormitoryOnReceiptPeriodDS";
    public static final String NEED_DORMITORY_ON_STUDING_PERIOD_DS = "needDormitoryOnStudingPeriodDS";

    // ids
    public static final long NEED_DORMITORY_YES = 0L;
    public static final long NEED_DORMITORY_NOT = 1L;

    public static void onBeforeDataSourceFetch(IUIDataSource dataSource, PersonnarfuDataParam param)
    {
    }

    public static IDefaultComboDataSourceHandler createNeedDormitoryOnReceiptPeriodDS(String name)
    {
        return new SimpleTitledComboDataSourceHandler(name)
                .addAll(Arrays.asList(new IdentifiableWrapper(NEED_DORMITORY_YES, "да"), new IdentifiableWrapper(NEED_DORMITORY_NOT, "нет")))
                .filtered(true);
    }

    public static IDefaultComboDataSourceHandler createNeedDormitoryOnStudingPeriodDS(String name)
    {
        return new SimpleTitledComboDataSourceHandler(name)
                .addAll(Arrays.asList(new IdentifiableWrapper(NEED_DORMITORY_YES, "да"), new IdentifiableWrapper(NEED_DORMITORY_NOT, "нет")))
                .filtered(true);
    }
}
