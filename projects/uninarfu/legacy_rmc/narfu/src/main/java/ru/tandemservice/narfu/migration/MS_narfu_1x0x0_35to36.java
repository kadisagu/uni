package ru.tandemservice.narfu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_narfu_1x0x0_35to36 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность personMilitaryStatusNARFU

        if (!tool.tableExists("personmilitarystatusrmc_t")) {
            if (tool.tableExists("personmilitarystatusnarfu_t")) {
                tool.renameTable("personmilitarystatusnarfu_t", "personmilitarystatusrmc_t");
                tool.executeUpdate("delete from entitycode_s where name_p = 'PersonMilitaryStatusRMC'");  // удаляем вновь созданный entity
                tool.executeUpdate("update entityCode_s set name_p = 'PersonMilitaryStatusRMC' where name_p = 'PersonMilitaryStatusNARFU'");
            }
        }

//    ////////////////////////////////////////////////////////////////////////////////
//    // сущность personMilitaryStatusRMC
//
//    // создана новая сущность
//    {
//
//       // создать таблицу
//       DBTable dbt = new DBTable("personmilitarystatusrmc_t",
//          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
//          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
//          new DBColumn("base_id", DBType.LONG),
//          new DBColumn("militarydata_id", DBType.LONG),
//          new DBColumn("removaldate_p", DBType.DATE),
//          new DBColumn("cardfillingdate_p", DBType.DATE),
//          new DBColumn("verificationdate_p", DBType.DATE),
//          new DBColumn("basisspecial_p", DBType.createVarchar(255)),
//          new DBColumn("basisdate_p", DBType.DATE),
//          new DBColumn("militarycomposition_id", DBType.LONG)
//       );
//       tool.createTable(dbt);
//
//       // гарантировать наличие кода сущности
//       short entityCode = tool.entityCodes().ensure("personMilitaryStatusRMC");
//
//    }

    }
}