package ru.tandemservice.narfu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;

public class MS_narfu_1x0x0_53to54 extends IndependentMigrationScript {

    @Override
    public void run(DBTool tool) throws Exception {

        if (tool.tableExists("epp_c_reg_struct_t")) {
            tool.executeUpdate("UPDATE epp_c_reg_struct_t SET code_p='eppRegistryDiscipline.tutor.1' WHERE code_p='eppRegistryDiscipline.narfu.1'");
            tool.executeUpdate("UPDATE epp_c_reg_struct_t SET code_p='eppRegistryDiscipline.tutor.2' WHERE code_p='eppRegistryDiscipline.narfu.2'");
        }

    }

}
