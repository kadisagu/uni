package ru.tandemservice.narfu.component.reports.StudentsByUGS.Pub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;

import java.text.SimpleDateFormat;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    public Controller()
    {
    }

    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickPrintReport(IBusinessComponent component)
    {

        String fileName = "Report_" + new SimpleDateFormat("dd_MM_yyyy").format(getModel(component).getReport().getFormingDate());
        byte[] content = getModel(component).getReport().getContent().getContent();
        if (content == null)
            throw new ApplicationException("Файл отчета пуст.");
        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(content, fileName);
        activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new UniMap()
                .add("id", id).add("extension", "xls")));
//     activateInRoot(component, new ComponentActivator("ru.tandemservice.uni.component.reports.DownloadStorableReport", (new UniMap()).add("reportId", ((Model)getModel(component)).getReport().getId()).add("extension", "rtf")));
    }
}
