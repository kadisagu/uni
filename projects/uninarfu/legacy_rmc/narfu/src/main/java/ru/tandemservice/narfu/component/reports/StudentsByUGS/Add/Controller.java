package ru.tandemservice.narfu.component.reports.StudentsByUGS.Add;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.narfu.entity.NarfuReportStudentsByUGS;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;

import java.text.SimpleDateFormat;

public class Controller extends AbstractBusinessController<IDAO, Model> {
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = getModel(component);
        getDao().prepare(model);
    }

    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);

        NarfuReportStudentsByUGS report = getDao().createReport(model);

        String fileName = "Report_" + new SimpleDateFormat("dd_MM_yyyy").format(report.getFormingDate());
        byte[] content = report.getContent().getContent();
        if (content == null)
            throw new ApplicationException("Файл отчета пуст.");
        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(content, fileName);

        deactivate(component);
        activateInRoot(component, new PublisherActivator(report));
//        activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new UniMap()
//                .add("id", id).add("extension", "xls")));
//
//        deactivate(component);

    }
}
