package ru.tandemservice.narfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import ru.tandemservice.narfu.entity.PersonNextOfKinNARFU;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение ближайшего родственника
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PersonNextOfKinNARFUGen extends EntityBase
 implements INaturalIdentifiable<PersonNextOfKinNARFUGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.narfu.entity.PersonNextOfKinNARFU";
    public static final String ENTITY_NAME = "personNextOfKinNARFU";
    public static final int VERSION_HASH = -1413437191;
    private static IEntityMeta ENTITY_META;

    public static final String L_BASE = "base";
    public static final String P_I_N_N = "INN";
    public static final String P_PENSION_INSURANCE_NUMBER = "pensionInsuranceNumber";

    private PersonNextOfKin _base;     // Ближайший родственник
    private String _INN;     // ИНН
    private String _pensionInsuranceNumber;     // Свидетельство пенсионного страхования

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Ближайший родственник. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public PersonNextOfKin getBase()
    {
        return _base;
    }

    /**
     * @param base Ближайший родственник. Свойство не может быть null и должно быть уникальным.
     */
    public void setBase(PersonNextOfKin base)
    {
        dirty(_base, base);
        _base = base;
    }

    /**
     * @return ИНН.
     */
    @Length(max=255)
    public String getINN()
    {
        return _INN;
    }

    /**
     * @param INN ИНН.
     */
    public void setINN(String INN)
    {
        dirty(_INN, INN);
        _INN = INN;
    }

    /**
     * @return Свидетельство пенсионного страхования.
     */
    @Length(max=255)
    public String getPensionInsuranceNumber()
    {
        return _pensionInsuranceNumber;
    }

    /**
     * @param pensionInsuranceNumber Свидетельство пенсионного страхования.
     */
    public void setPensionInsuranceNumber(String pensionInsuranceNumber)
    {
        dirty(_pensionInsuranceNumber, pensionInsuranceNumber);
        _pensionInsuranceNumber = pensionInsuranceNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PersonNextOfKinNARFUGen)
        {
            if (withNaturalIdProperties)
            {
                setBase(((PersonNextOfKinNARFU)another).getBase());
            }
            setINN(((PersonNextOfKinNARFU)another).getINN());
            setPensionInsuranceNumber(((PersonNextOfKinNARFU)another).getPensionInsuranceNumber());
        }
    }

    public INaturalId<PersonNextOfKinNARFUGen> getNaturalId()
    {
        return new NaturalId(getBase());
    }

    public static class NaturalId extends NaturalIdBase<PersonNextOfKinNARFUGen>
    {
        private static final String PROXY_NAME = "PersonNextOfKinNARFUNaturalProxy";

        private Long _base;

        public NaturalId()
        {}

        public NaturalId(PersonNextOfKin base)
        {
            _base = ((IEntity) base).getId();
        }

        public Long getBase()
        {
            return _base;
        }

        public void setBase(Long base)
        {
            _base = base;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof PersonNextOfKinNARFUGen.NaturalId) ) return false;

            PersonNextOfKinNARFUGen.NaturalId that = (NaturalId) o;

            if( !equals(getBase(), that.getBase()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getBase());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getBase());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PersonNextOfKinNARFUGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PersonNextOfKinNARFU.class;
        }

        public T newInstance()
        {
            return (T) new PersonNextOfKinNARFU();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "base":
                    return obj.getBase();
                case "INN":
                    return obj.getINN();
                case "pensionInsuranceNumber":
                    return obj.getPensionInsuranceNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "base":
                    obj.setBase((PersonNextOfKin) value);
                    return;
                case "INN":
                    obj.setINN((String) value);
                    return;
                case "pensionInsuranceNumber":
                    obj.setPensionInsuranceNumber((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "base":
                        return true;
                case "INN":
                        return true;
                case "pensionInsuranceNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "base":
                    return true;
                case "INN":
                    return true;
                case "pensionInsuranceNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "base":
                    return PersonNextOfKin.class;
                case "INN":
                    return String.class;
                case "pensionInsuranceNumber":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PersonNextOfKinNARFU> _dslPath = new Path<PersonNextOfKinNARFU>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PersonNextOfKinNARFU");
    }
            

    /**
     * @return Ближайший родственник. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.narfu.entity.PersonNextOfKinNARFU#getBase()
     */
    public static PersonNextOfKin.Path<PersonNextOfKin> base()
    {
        return _dslPath.base();
    }

    /**
     * @return ИНН.
     * @see ru.tandemservice.narfu.entity.PersonNextOfKinNARFU#getINN()
     */
    public static PropertyPath<String> INN()
    {
        return _dslPath.INN();
    }

    /**
     * @return Свидетельство пенсионного страхования.
     * @see ru.tandemservice.narfu.entity.PersonNextOfKinNARFU#getPensionInsuranceNumber()
     */
    public static PropertyPath<String> pensionInsuranceNumber()
    {
        return _dslPath.pensionInsuranceNumber();
    }

    public static class Path<E extends PersonNextOfKinNARFU> extends EntityPath<E>
    {
        private PersonNextOfKin.Path<PersonNextOfKin> _base;
        private PropertyPath<String> _INN;
        private PropertyPath<String> _pensionInsuranceNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Ближайший родственник. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.narfu.entity.PersonNextOfKinNARFU#getBase()
     */
        public PersonNextOfKin.Path<PersonNextOfKin> base()
        {
            if(_base == null )
                _base = new PersonNextOfKin.Path<PersonNextOfKin>(L_BASE, this);
            return _base;
        }

    /**
     * @return ИНН.
     * @see ru.tandemservice.narfu.entity.PersonNextOfKinNARFU#getINN()
     */
        public PropertyPath<String> INN()
        {
            if(_INN == null )
                _INN = new PropertyPath<String>(PersonNextOfKinNARFUGen.P_I_N_N, this);
            return _INN;
        }

    /**
     * @return Свидетельство пенсионного страхования.
     * @see ru.tandemservice.narfu.entity.PersonNextOfKinNARFU#getPensionInsuranceNumber()
     */
        public PropertyPath<String> pensionInsuranceNumber()
        {
            if(_pensionInsuranceNumber == null )
                _pensionInsuranceNumber = new PropertyPath<String>(PersonNextOfKinNARFUGen.P_PENSION_INSURANCE_NUMBER, this);
            return _pensionInsuranceNumber;
        }

        public Class getEntityClass()
        {
            return PersonNextOfKinNARFU.class;
        }

        public String getEntityName()
        {
            return "personNextOfKinNARFU";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
