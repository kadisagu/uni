package ru.tandemservice.narfu.unisession.component.sessionBulletin.SessionBulletinPub;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.unisession.dao.print.INarfuSessionBulletinPrintDAO;
import ru.tandemservice.unisession.dao.print.NarfuSessionBulletinPrintDAO;
import ru.tandemservice.uni.util.ReportRenderer;
import ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPub.Model;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentPrintVersion;

import java.util.Collections;
import java.util.List;

public class DAO extends ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPub.DAO {
    @Override
    public void printBulletin(Model model) {
        SessionBulletinDocument bulletin = model.getBulletin();
        if (null == bulletin.getPerformDate())
            throw new ApplicationException("Для того чтобы распечатать ведомость должна быть заполнена дата сдачи");

        if (getCommissionList(bulletin.getCommission()).isEmpty())
            throw new ApplicationException("Для того чтобы распечатать ведомость должен быть указан преподаватель");


        if (bulletin.isClosed()) {
            SessionDocumentPrintVersion rel = get(SessionDocumentPrintVersion.class, SessionDocumentPrintVersion.doc().id().s(), bulletin.getId());
            if (null != rel) {
                byte content[] = rel.getContent();
                BusinessComponentUtils.downloadDocument(new ReportRenderer("Ведомость.rtf", content), true);
                return;
            }
        }
        NarfuSessionBulletinPrintDAO.ReportFile result =  INarfuSessionBulletinPrintDAO.instance.get().printBulletinListNarfu(Collections.singleton(model.getBulletin().getId()));
        BusinessComponentUtils.downloadDocument(new ReportRenderer("Ведомость.rtf", result.getContent()), true);
    }

    public List<SessionComissionPps> getCommissionList(SessionComission comission) {
        return getList(SessionComissionPps.class, SessionComissionPps.commission(), comission);
    }

    @Override
    public void doCloseBulletin(Model model) {
        super.doCloseBulletin(model);

        SessionDocumentPrintVersion rel = get(SessionDocumentPrintVersion.class, "doc", model.getBulletin());

        if (rel != null) {
            //Пересохраняем новую печ форму
            NarfuSessionBulletinPrintDAO.ReportFile result = INarfuSessionBulletinPrintDAO.instance.get().printBulletinListNarfu(Collections.singleton(model.getBulletin().getId()));
            rel.setContent(result.getContent());
            getSession().saveOrUpdate(rel);
        }

    }
}
