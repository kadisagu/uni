package ru.tandemservice.narfu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;


/**
 * Отключение studentcardrmc
 *
 * @author Ситек начал
 */
public class MS_narfu_1x0x0_6to7 extends IndependentMigrationScript {


    @Override
    public void run(DBTool tool) throws Exception
    {
        _migration(tool);
    }

    private void _migration(DBTool tool) throws Exception
    {

        // 1 если использовались карточки студента из studentcardrmc
        tool.executeUpdate(
                "delete from studentdocument_t where studentdocumenttype_id in (select id from studentdocumenttype_t where code_p =? )"
                , "rmc.2"
        );

        // 2 чистим справочники
        tool.executeUpdate("delete from studentdocumenttype_t where  code_p =?", "rmc.2");
        tool.executeUpdate("delete from templatedocument_t where code_p =? ", "sd.rmc.2");


    }
}		