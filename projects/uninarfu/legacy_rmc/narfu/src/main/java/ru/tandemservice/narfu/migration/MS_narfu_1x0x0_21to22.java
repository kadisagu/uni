package ru.tandemservice.narfu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_narfu_1x0x0_21to22 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.12"),
                        new ScriptDependency("org.tandemframework.shared", "1.2.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.2.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность practiceBase была перенесена в модуль movestudentrmc, задача #4317. Удалять таблицу нет необходимости.

//		// сущность была удалена
//		{
//			// TODO: программист должен подтвердить это действие
//			if( true )
//				throw new UnsupportedOperationException("Confirm me: delete table");
//
//			// удалить таблицу
//			tool.dropTable("practicebase_t", false /* - не удалять, если есть ссылающиеся таблицы */);
//
//			// удалить код сущности
//			tool.entityCodes().delete("practiceBase");
//
//		}


    }
}