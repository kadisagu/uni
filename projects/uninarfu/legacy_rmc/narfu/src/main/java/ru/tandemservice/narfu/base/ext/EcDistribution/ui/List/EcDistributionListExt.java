package ru.tandemservice.narfu.base.ext.EcDistribution.ui.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.IPresenterExtensionBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.uniec.base.bo.EcDistribution.ui.List.EcDistributionList;

@Configuration
public class EcDistributionListExt extends BusinessComponentExtensionManager {

    @Autowired
    private EcDistributionList ecDistributionList;

    @Bean
    public PresenterExtension presenterExtension() {
        IPresenterExtensionBuilder pi = presenterExtensionBuilder(ecDistributionList.presenterExtPoint());
        pi.addAddon(this.uiAddon("ui_addon", EcDistributionPubExtUIExt.class));
        return pi.create();
    }
}
