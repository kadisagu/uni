package ru.tandemservice.narfu.component.reports.orgUnitStudentGrantByInstitute.Add;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.YearDistribution;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.uniedu.base.bo.EducationYear.EducationYearManager;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        model.setEducationYear(EducationYearManager.instance().dao().getCurrent());

        final YearDistribution distr = getCatalogItem(YearDistribution.class, UniDefines.YEAR_DISTRIBUTION_SEMESTERS);
        model.setEducationYearPartModel(new UniQueryFullCheckSelectModel() {
            @Override
            protected MQBuilder query(String alias, String s1) {
                return new MQBuilder(YearDistributionPart.ENTITY_CLASS, alias)
                        .add(MQExpression.eq(alias, YearDistributionPart.yearDistribution(), distr))
                        .addOrder(alias, YearDistributionPart.number());
            }
        });

        model.setDevelopFormModel(new UniQueryFullCheckSelectModel() {
            @Override
            protected MQBuilder query(String alias, String s1) {
                return new MQBuilder(DevelopForm.ENTITY_CLASS, alias)
                        .addOrder(alias, DevelopForm.title());
            }
        });
    }

    @Override
    public void createReport(Model model) {
        ReportGenerator.createReport(model.getEducationYear(), model.getPart(), model.getMonth(), model.getDevelopForm());
    }
}
