package ru.tandemservice.narfu.base.ext.EcOrder.ui.StudentShContractParAddEdit;

import ru.tandemservice.narfu.entity.EnrollmentOrderNARFU;
import ru.tandemservice.uni.dao.UniDaoFacade;

public class EcOrderStudentShContractParAddEditUI extends ru.tandemservice.uniec.base.bo.EcOrder.ui.StudentShContractParAddEdit.EcOrderStudentShContractParAddEditUI {

    @Override
    public void onComponentRefresh() {
        super.onComponentRefresh();
        EnrollmentOrderNARFU orderNarfu = UniDaoFacade.getCoreDao().get(EnrollmentOrderNARFU.class, EnrollmentOrderNARFU.enrollmentOrder().id(), getOrder().getId());
        if (orderNarfu != null)
            setCourse(orderNarfu.getCourse());
    }
}
