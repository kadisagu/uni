package ru.tandemservice.narfu.component.student.StudentBookNumberEdit;

import org.tandemframework.core.component.State;
import ru.tandemservice.uni.entity.employee.Student;

@State({@org.tandemframework.core.component.Bind(key = "publisherId", binding = "student.id"), @org.tandemframework.core.component.Bind(key = "selectedStudentTab", binding = "selectedTab"), @org.tandemframework.core.component.Bind(key = "selectedDataTab", binding = "selectedDataTab")})
public class Model {

    private Student student = new Student();
    private String selectedTab;
    private String selectedDataTab;

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public String getSelectedTab() {
        return selectedTab;
    }

    public void setSelectedTab(String selectedTab) {
        this.selectedTab = selectedTab;
    }

    public String getSelectedDataTab() {
        return selectedDataTab;
    }

    public void setSelectedDataTab(String selectedDataTab) {
        this.selectedDataTab = selectedDataTab;
    }
}
