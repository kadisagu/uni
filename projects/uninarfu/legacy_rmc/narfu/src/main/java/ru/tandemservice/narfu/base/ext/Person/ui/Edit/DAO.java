package ru.tandemservice.narfu.base.ext.Person.ui.Edit;

import ru.tandemservice.narfu.base.ext.Person.ui.Tab.PersonNarfuUtil;

public class DAO extends org.tandemframework.shared.person.base.bo.Person.ui.Edit.DAO {

    @Override
    public void prepare(org.tandemframework.shared.person.base.bo.Person.ui.Edit.Model model) {
        super.prepare(model);
        Model myModel = (Model) model;
        myModel.setPersonNARFU(PersonNarfuUtil.getPersonNARFU(model.getPerson()));

    }

    @Override
    public void update(
            org.tandemframework.shared.person.base.bo.Person.ui.Edit.Model model)
    {
        super.update(model);
        Model myModel = (Model) model;
        getSession().saveOrUpdate(myModel.getPersonNARFU());
    }
}
