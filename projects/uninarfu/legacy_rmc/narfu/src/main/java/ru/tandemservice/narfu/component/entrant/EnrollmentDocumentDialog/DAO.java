package ru.tandemservice.narfu.component.entrant.EnrollmentDocumentDialog;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.eventssecrmc.entity.EnrollmentComissionToEmployeePost;
import ru.tandemservice.movestudentrmc.docord.util.UtilPrintSupport;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;

import java.util.Date;
import java.util.List;


public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        model.setDate(new Date());
    }

    public byte[] printEnrollmentDocument(Model model) {

        TemplateDocument template = UniDaoFacade.getCoreDao().getCatalogItem(TemplateDocument.class, "enrollmentDocument");
        RtfDocument document = new RtfReader().read(template.getContent());

        PreliminaryEnrollmentStudent student = (PreliminaryEnrollmentStudent) get(PreliminaryEnrollmentStudent.class, model.getId());
        Person person = student.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getPerson();
        boolean male = person.isMale();

        RtfInjectModifier im = new RtfInjectModifier();

        StringBuilder dataDocument = new StringBuilder(RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(model.getDate())).append(" года");

        im.put("dataDocument", model.getNumber() != null ? dataDocument.append(" № ").append(model.getNumber()).toString() : dataDocument.toString());
        im.put("FIO", PersonManager.instance().declinationDao().getDeclinationFIO(person.getIdentityCard(), GrammaCase.DATIVE));
        im.put("sexTitle", male ? "он" : "она");
        im.put("enrollment", male ? "зачислен" : "зачислена");
        im.put("studentCategory", getStudentCategoryStr(student.getStudentCategory()));

        im.put("course", "");
        im.put("enrollmentDate", "");
        im.put("dateOrder", "");
        im.put("numberOrder", "");
        im.put("FIOisp", "");
        im.put("telInst", "");

        EnrollmentExtract extract = get(EnrollmentExtract.class, EnrollmentExtract.entity(), student);
        if (extract != null) {
            im.put("course", extract.getCourse().getTitle());
            Date enrollmentDate = ((EnrollmentOrder) extract.getParagraph().getOrder()).getEnrollmentDate();
            im.put("enrollmentDate", enrollmentDate != null ? RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(enrollmentDate) + " года" : "");
            im.put("dateOrder", RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(extract.getParagraph().getOrder().getCommitDate()) + " года");
            im.put("numberOrder", extract.getParagraph().getOrder().getNumber());
        }
        im.put("developForm", student.getEducationOrgUnit().getDevelopForm().getGenCaseTitle());
        im.put("highSchool", UtilPrintSupport.getHighLevelSchoolTypeString(student.getEducationOrgUnit().getEducationLevelHighSchool()));
        im.put("formOrgUnit", student.getEducationOrgUnit().getFormativeOrgUnit().getGenitiveCaseTitle());
        im.put("compType", UtilPrintSupport.getCompensationTypeStr(student.getCompensationType()));
        im.put("developPeriod", student.getEducationOrgUnit().getDevelopPeriod().getTitle());

        if (UserContext.getInstance().getPrincipalContext() instanceof EmployeePost) {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrollmentComissionToEmployeePost.class, "rel")
                    .where(DQLExpressions.eqValue(DQLExpressions.property(EnrollmentComissionToEmployeePost.employeePost().fromAlias("rel")), ((EmployeePost) UserContext.getInstance().getPrincipalContext())))
                    .where(DQLExpressions.eqValue(DQLExpressions.property(EnrollmentComissionToEmployeePost.enrollmentCampaign().fromAlias("rel")), student.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getEnrollmentCampaign()));
            List<EnrollmentComissionToEmployeePost> list = UniDaoFacade.getCoreDao().getList(builder);
            if (!list.isEmpty()) {
                EnrollmentComissionToEmployeePost rel = list.get(0);
                im.put("FIOisp", rel.getEmployeePost().getPerson().getFullFio());
                im.put("telInst", rel.getOrgUnit() != null && rel.getOrgUnit().getPhone() != null ? rel.getOrgUnit().getPhone() : "");
            }
        }

        im.modify(document);

        return RtfUtil.toByteArray(document);
    }

    private String getStudentCategoryStr(StudentCategory category) {

        String code = category.getCode();

        if (code.equals(StudentCategoryCodes.STUDENT_CATEGORY_DPP) || code.equals(StudentCategoryCodes.STUDENT_CATEGORY_LISTENER))
            return "слушателей";
        else
            return "студентов";
    }
}
