package ru.tandemservice.narfu.uniec.component.report.EntrantExamListPrint;

import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DAO
        extends UniDao<Model> implements IDAO
{

    @Override
    public void prepare(final Model model)
    {
        final Session session = getSession();
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.setDateFrom(model.getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : model.getEnrollmentCampaign().getStartDate());
        model.setDateTo(CoreDateUtils.getDayLastTimeMoment(new Date()));

        model.setFormativeOrgUnitListModel(MultiEnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setEducationLevelHighSchoolListModel(MultiEnrollmentDirectionUtil.createEducationLevelsHighSchoolModel(model));

        model.setFormativeOrgUnitList(new ArrayList<OrgUnit>());
        model.setEducationLevelHighSchoolList(new ArrayList<EducationLevelsHighSchool>());

        model.setParameters(new MultiEnrollmentDirectionUtil.Parameters());
        model.getParameters().setRequired(false, MultiEnrollmentDirectionUtil.Parameters.Filter.values());


        // приклеим модель абитуриентов
        model.setEntrantListModel(new FullCheckSelectModel() {
            @Override
            public ListResult<Entrant> findValues(String arg0)
            {
                List<Entrant> entrants = getEntrantList(model);

                return new ListResult<Entrant>(entrants);
            }
        });
    }


    public List<Entrant> getEntrantList(Model model)
    {

        // 1 - нам нужны только те абитуры, которые должны сдавать хоть что-то
        DQLSelectBuilder examEntrant = new DQLSelectBuilder()
                .fromEntity(ExamPassDiscipline.class, "ep")
                .addColumn(property(ExamPassDiscipline.entrantExamList().entrant().entrantId().fromAlias("ep")))
                .where(eq(property(ExamPassDiscipline.enrollmentCampaignDiscipline().enrollmentCampaign().fromAlias("ep")), value(model.getEnrollmentCampaign())));


        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(Entrant.class, "r")
                .addColumn("r")
                .where(in(property(Entrant.id().fromAlias("r")), examEntrant.getQuery()));

        // по датам
        dql.where(gt(property(Entrant.registrationDate().fromAlias("r")), valueDate(model.getDateFrom())));
        dql.where(lt(property(Entrant.registrationDate().fromAlias("r")), valueDate(model.getDateTo())));

        // по форм. подр
        if (model.isFormativeOrgUnitActive() && model.getFormativeOrgUnitList().size() > 0) {
            // все абитуры с этим форм. подр
            DQLSelectBuilder dqlF = new DQLSelectBuilder()
                    .fromEntity(RequestedEnrollmentDirection.class, "rd")
                    .addColumn(property(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("rd")))
                    .where(eq(property(RequestedEnrollmentDirection.entrantRequest().entrant().enrollmentCampaign().fromAlias("rd")), value(model.getEnrollmentCampaign())))
                    .where(in(property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().formativeOrgUnit().fromAlias("rd")), model.getFormativeOrgUnitList()));

            dql.where(in(property(Entrant.id().fromAlias("r")), dqlF.getQuery()));
        }

        // по напр. подготовки
        if (model.isEducationLevelHighSchoolActive() && model.getEducationLevelHighSchoolList().size() > 0) {
            // все абитуры с этим форм. подр
            DQLSelectBuilder dqlHs = new DQLSelectBuilder()
                    .fromEntity(RequestedEnrollmentDirection.class, "rd")
                    .addColumn(property(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("rd")))
                    .where(eq(property(RequestedEnrollmentDirection.entrantRequest().entrant().enrollmentCampaign().fromAlias("rd")), value(model.getEnrollmentCampaign())))
                    .where(in(property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().fromAlias("rd")), model.getEducationLevelHighSchoolList()));

            dql.where(in(property(Entrant.id().fromAlias("r")), dqlHs.getQuery()));
        }
        dql.order(property(Entrant.person().identityCard().lastName().fromAlias("r")));

        List<Entrant> lst = dql.createStatement(getSession()).list();
        return lst;
    }


    @Override
    public void createReport(Model model)
    {
        // TODO Auto-generated method stub

    }

}
