package ru.tandemservice.narfu.component.studentmassprint.documents.mpd1004.Add;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.narfu.component.documents.d1004.Add.ParagraphData;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DAO extends
        ru.tandemservice.unirmc.component.studentmassprint.documents.Base.DAO<Model> implements IDAO
{
    private static final String DIRECTOR = "Директор";
    private static final String ZAM_PROREKTORA = "Заместитель первого проректора по учебной работе";

    @Override
    public void prepare(final Model model)
    {
        Set<OrgUnit> fou = new HashSet<>();
        Set<EppEduPlanVersion> versions = new HashSet<EppEduPlanVersion>();

        StringBuilder mess = new StringBuilder();

        for (Student student : model.getLstStudents()) {
            EppStudent2EduPlanVersion rel = IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionRelation(student.getId());

            if (rel == null && !model.isDisplayWarning()) {
                mess.append("Не у всех выбранных студентов есть УП. ");
                model.setDisplayWarning(true);
            }
            else if (rel != null)
                versions.add(rel.getEduPlanVersion());

            fou.add(student.getEducationOrgUnit().getFormativeOrgUnit());
        }
        if (fou.size() == 1) {
            model.setTelephone(model.getLstStudents().get(0).getEducationOrgUnit().getFormativeOrgUnit().getPhone());
        }

        if (versions.size() != 1 && !versions.isEmpty()) {
            mess.append("У выбранных студентов разные УП.");
            model.setDisplayWarning(true);
        }

        if (model.isDisplayWarning())
            model.setWarningMessage(mess.toString());
        model.setPeriodList(Arrays.asList(ParagraphData.EXAMINATION_SESSION,
                                          ParagraphData.RESULT_QUALIFICATION_WORK,
                                          ParagraphData.STATE_EXAMINATION,
                                          ParagraphData.STATE_FINAL_EXAMINATION,
                                          ParagraphData.FINAL_EXAMINATION));
        model.setPeriod(ParagraphData.EXAMINATION_SESSION);

        model.setFirstStudent(model.getLstStudents().get(0));


        model.setManagerPostList(Arrays.asList(DIRECTOR + " " + model.getFirstStudent().getEducationOrgUnit().getFormativeOrgUnit().getGenitiveCaseTitle(), ZAM_PROREKTORA));
        //	model.setManagerPostTitle(ZAM_PROREKTORA);
        model.setManagerPostTitle(DIRECTOR + " " + model.getFirstStudent().getEducationOrgUnit().getFormativeOrgUnit().getGenitiveCaseTitle());


        model.setManagerFio(getDirectorFIO(null, "137"));


        EppStudent2EduPlanVersion rel = IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionRelation(model.getFirstStudent().getId());

        if (rel != null) {
            final EppEduPlanVersion version = rel.getEduPlanVersion();
            model.setTermModel(new FullCheckSelectModel(DevelopGridTerm.part().title().s()) {
                @Override
                public ListResult<DevelopGridTerm> findValues(String filter) {

                    DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DevelopGridTerm.class, "dgt")
                            .where(DQLExpressions.eqValue(DQLExpressions.property(DevelopGridTerm.course().fromAlias("dgt")), model.getFirstStudent().getCourse()))
                            .where(DQLExpressions.eqValue(DQLExpressions.property(DevelopGridTerm.developGrid().id().fromAlias("dgt")), version.getEduPlan().getDevelopGrid()));

                    List<DevelopGridTerm> lst = getList(builder);

                    return new ListResult<DevelopGridTerm>(lst);
                }
            });
        }

        model.setDateStartCertification(ParagraphData.getDate(model.getFirstStudent(), model.getPeriod(), model.getTerm(), true));
        model.setDateEndCertification(ParagraphData.getDate(model.getFirstStudent(), model.getPeriod(), model.getTerm(), false));

        model.setEmployeePostListModel(new BaseSingleSelectModel("fullTitle") {

            @Override
            public Object getValue(Object primaryKey)
            {
                if (primaryKey == null) return null;
                EmployeePost employeePost = (EmployeePost) DAO.this.get(EmployeePost.class, (Long) primaryKey);
                return employeePost == null ? null : employeePost;
            }

            @Override
            public ListResult<EmployeePost> findValues(String filter) {

                DQLSelectBuilder builder = (DQLSelectBuilder) (new DQLSelectBuilder()).fromEntity(EmployeePost.class, "e");
                builder.column("e");
                if (StringUtils.isNotEmpty(filter)) {
                    IDQLExpression expression = DQLFunctions.concat(new IDQLExpression[]{
                            DQLExpressions.property("e", EmployeePost.employee().person().identityCard().fullFio().s()), DQLFunctions.concat(new IDQLExpression[]{
                            DQLExpressions.property("e", EmployeePost.postRelation().postBoundedWithQGandQL().post().title().s()), DQLFunctions.concat(new IDQLExpression[]{
                            DQLExpressions.property("e", EmployeePost.orgUnit().shortTitle().s()), DQLExpressions.property("e", EmployeePost.orgUnit().orgUnitType().title().s())
                    })
                    })
                    });
                    builder.where(DQLExpressions.like(DQLFunctions.upper(expression), DQLExpressions.value(CoreStringUtils.escapeLike(filter).replace(" ", "%"))));
                }
                builder.where(DQLExpressions.eq(DQLExpressions.property("e", EmployeePost.postStatus().active().s()), DQLExpressions.value(Boolean.TRUE)));
                builder.where(DQLExpressions.eq(DQLExpressions.property("e", EmployeePost.employee().archival().s()), DQLExpressions.value(Boolean.FALSE)));
                builder.order(DQLExpressions.property("e", EmployeePost.employee().person().identityCard().fullFio().s()));
                Number count = (Number) builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();
                IDQLStatement statement = builder.createStatement(new DQLExecutionContext(getSession()));
                statement.setMaxResults(50);
                return new ListResult(statement.list(), count != null ? count.intValue() : 0L);
            }
        });

    }

    public String getPostCode(String managerPost) {
        if (managerPost.contains(DIRECTOR))
            return "115";
        else if (managerPost.contains(ZAM_PROREKTORA))
            return "137";

        return "";
    }

    public String getDirectorFIO(OrgUnit orgUnit, String postCode) {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeePost.class, "post")
                .where(DQLExpressions.eqValue(DQLExpressions.property(EmployeePost.postRelation().postBoundedWithQGandQL().code().fromAlias("post")), postCode));

        if (orgUnit != null)
            builder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeePost.orgUnit().id().fromAlias("post")), orgUnit.getId()));

        List<EmployeePost> list = getList(builder);

        if (!list.isEmpty()) {
            EmployeePost post = list.get(0);
            return post.getPerson().getIdentityCard().getIof();
        }

        return "";
    }

}
