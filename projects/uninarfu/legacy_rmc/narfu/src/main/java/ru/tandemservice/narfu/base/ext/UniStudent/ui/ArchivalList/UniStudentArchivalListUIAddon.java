package ru.tandemservice.narfu.base.ext.UniStudent.ui.ArchivalList;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import ru.tandemservice.uni.base.bo.UniStudent.ui.ArchivalList.UniStudentArchivalList;

public class UniStudentArchivalListUIAddon extends UIAddon
{

    public UniStudentArchivalListUIAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);

    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {

        super.onBeforeDataSourceFetch(dataSource);

        if (dataSource.getName().equals(UniStudentArchivalList.STUDENT_SEARCH_LIST_DS))
        {
            dataSource.put("cardSeria", getPresenter().getSettings().get("cardSeria"));
            dataSource.put("cardNumber", getPresenter().getSettings().get("cardNumber"));
            dataSource.put("cardBirthDateFrom", getPresenter().getSettings().get("cardBirthDateFrom"));
            dataSource.put("cardBirthDateFromTo", getPresenter().getSettings().get("cardBirthDateFromTo"));
        }
    }
}
