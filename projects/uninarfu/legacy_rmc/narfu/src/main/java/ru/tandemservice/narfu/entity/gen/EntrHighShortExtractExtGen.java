package ru.tandemservice.narfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.narfu.entity.EntrHighShortExtractExt;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.orders.EntrHighShortExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение выписки приказа о зачислении в аспирантуру
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrHighShortExtractExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.narfu.entity.EntrHighShortExtractExt";
    public static final String ENTITY_NAME = "entrHighShortExtractExt";
    public static final int VERSION_HASH = 813033057;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXTRACT = "extract";
    public static final String L_EDUCATION_ORG_UNIT = "educationOrgUnit";

    private EntrHighShortExtract _extract;     // О зачислении на второе высшее
    private EducationOrgUnit _educationOrgUnit;     // Направленность

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return О зачислении на второе высшее. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EntrHighShortExtract getExtract()
    {
        return _extract;
    }

    /**
     * @param extract О зачислении на второе высшее. Свойство не может быть null и должно быть уникальным.
     */
    public void setExtract(EntrHighShortExtract extract)
    {
        dirty(_extract, extract);
        _extract = extract;
    }

    /**
     * @return Направленность. Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnit()
    {
        return _educationOrgUnit;
    }

    /**
     * @param educationOrgUnit Направленность. Свойство не может быть null.
     */
    public void setEducationOrgUnit(EducationOrgUnit educationOrgUnit)
    {
        dirty(_educationOrgUnit, educationOrgUnit);
        _educationOrgUnit = educationOrgUnit;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntrHighShortExtractExtGen)
        {
            setExtract(((EntrHighShortExtractExt)another).getExtract());
            setEducationOrgUnit(((EntrHighShortExtractExt)another).getEducationOrgUnit());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrHighShortExtractExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrHighShortExtractExt.class;
        }

        public T newInstance()
        {
            return (T) new EntrHighShortExtractExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "extract":
                    return obj.getExtract();
                case "educationOrgUnit":
                    return obj.getEducationOrgUnit();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "extract":
                    obj.setExtract((EntrHighShortExtract) value);
                    return;
                case "educationOrgUnit":
                    obj.setEducationOrgUnit((EducationOrgUnit) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "extract":
                        return true;
                case "educationOrgUnit":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "extract":
                    return true;
                case "educationOrgUnit":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "extract":
                    return EntrHighShortExtract.class;
                case "educationOrgUnit":
                    return EducationOrgUnit.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrHighShortExtractExt> _dslPath = new Path<EntrHighShortExtractExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrHighShortExtractExt");
    }
            

    /**
     * @return О зачислении на второе высшее. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.narfu.entity.EntrHighShortExtractExt#getExtract()
     */
    public static EntrHighShortExtract.Path<EntrHighShortExtract> extract()
    {
        return _dslPath.extract();
    }

    /**
     * @return Направленность. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.EntrHighShortExtractExt#getEducationOrgUnit()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
    {
        return _dslPath.educationOrgUnit();
    }

    public static class Path<E extends EntrHighShortExtractExt> extends EntityPath<E>
    {
        private EntrHighShortExtract.Path<EntrHighShortExtract> _extract;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnit;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return О зачислении на второе высшее. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.narfu.entity.EntrHighShortExtractExt#getExtract()
     */
        public EntrHighShortExtract.Path<EntrHighShortExtract> extract()
        {
            if(_extract == null )
                _extract = new EntrHighShortExtract.Path<EntrHighShortExtract>(L_EXTRACT, this);
            return _extract;
        }

    /**
     * @return Направленность. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.EntrHighShortExtractExt#getEducationOrgUnit()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
        {
            if(_educationOrgUnit == null )
                _educationOrgUnit = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT, this);
            return _educationOrgUnit;
        }

        public Class getEntityClass()
        {
            return EntrHighShortExtractExt.class;
        }

        public String getEntityName()
        {
            return "entrHighShortExtractExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
