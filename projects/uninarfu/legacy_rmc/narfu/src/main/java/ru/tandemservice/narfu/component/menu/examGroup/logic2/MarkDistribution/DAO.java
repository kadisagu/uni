package ru.tandemservice.narfu.component.menu.examGroup.logic2.MarkDistribution;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniec.component.menu.examGroup.abstractMarkDistribution.EntrantDisciplineMarkWrapper;
import ru.tandemservice.uniec.component.menu.examGroup.logic2.MarkDistribution.Model;
import ru.tandemservice.uniec.dao.examgroup.logic2.ExamGroupLogicSample2;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.ui.EnrollmentDirectionUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DAO extends ru.tandemservice.uniec.component.menu.examGroup.logic2.MarkDistribution.DAO implements IDAO {


    @Override
    public void prepareListDataSourceExt(Model model) {
        DynamicListDataSource dataSource = model.getDataSource();
        List list = new ArrayList();
        List data;


        boolean formingForEntrant = "1".equals(model.getEnrollmentCampaign().getEntrantExamListsFormingFeature().getCode());
        MQBuilder builder = new MQBuilder("ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline", "d");
        builder.addDomain("r", "ru.tandemservice.uniec.entity.entrant.ExamGroupRow");
        builder.add(MQExpression.eqProperty("d", "entrantExamList.entrant.id", "r", "entrantRequest" + ((formingForEntrant) ? ".entrant" : "") + ".id"));
        builder.add(MQExpression.eq("r", "entrantRequest.entrant.archival", Boolean.FALSE));
        ExamGroup examGroup = (ExamGroup) model.getSettings().get("examGroup");

        //Логика по умолчанию
        if (model.getDiscipline() != null) {

            builder.add(MQExpression.eq("d", "enrollmentCampaignDiscipline", model.getDiscipline().getDiscipline()));
            builder.add(MQExpression.eq("d", "subjectPassForm", model.getDiscipline().getSubjectPassForm()));
            builder.add(MQExpression.eq("r", "examGroup", examGroup));

        }
        else {
            //Логика по сафу
            FilterUtils.applySelectFilter(builder, "r", ExamGroupRow.entrantRequest().entrant().enrollmentCampaign().s(), model.getEnrollmentCampaign());
        }

        FilterUtils.applySelectFilter(builder, "r", ExamGroupRow.examGroup().s(), examGroup);

        builder.getSelectAliasList().clear();
        builder.setNeedDistinct(true);
        builder.addSelect("d");
        builder.addSelect("r", new Object[]{"entrantRequest"});

        long totalSize = builder.getResultCount(getSession());

        dataSource.setTotalSize(totalSize);

        int startRow = (int) dataSource.getStartRow();
        int countRow = (int) dataSource.getCountRow();
        data = builder.getResultList(getSession(), startRow, countRow);


        for (Object obj : data) {
            Object[] row = (Object[]) obj;
            ExamPassDiscipline examPassDiscipline = (ExamPassDiscipline) row[0];
            ExamPassMark examPassMark = (ExamPassMark) get(ExamPassMark.class, "examPassDiscipline", examPassDiscipline);

            if (examPassMark != null) getSession().refresh(examPassMark);
            String mark = (examPassMark != null && examPassMark.getEntrantAbsenceNote() != null) ? "н" : (examPassMark == null) ? "" : DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS.format(Double.valueOf(examPassMark.getMark()));
            String paperCode = (examPassMark == null) ? "" : examPassMark.getPaperCode();
            String examiners = (examPassMark == null) ? "" : examPassMark.getExaminers();
            list.add(new EntrantDisciplineMarkWrapper(examPassDiscipline, (EntrantRequest) row[1], mark, paperCode, examiners));
        }

        Collections.sort(list, ITitled.TITLED_COMPARATOR);
        dataSource.createPage(list);
    }

    @Override
    protected void prepareModel(Model model) {
        super.prepareModel(model);

        model.setDevelopFormListModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(DevelopForm.class)));
        model.setDevelopConditionListModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(DevelopCondition.class)));
        model.setDevelopTechListModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(DevelopTech.class)));
        model.setDevelopPeriodListModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(DevelopPeriod.class)));

    }

    @Override
    protected void prepareGroupList(MQBuilder builder, String alias, Model model) {
        CompensationType compensationType = (CompensationType) model.getSettings().get("compensationType");
        EnrollmentDirection enrollmentDirection = EnrollmentDirectionUtil.getEnrollmentDirection(model, getSession());


        if ((compensationType != null) && (enrollmentDirection != null))

        {
            String key = ExamGroupLogicSample2.createKey(enrollmentDirection, compensationType);

            builder.add(MQExpression.eq(alias, "key", key));
        }
    }

}

