package ru.tandemservice.narfu.component.entrant.EcDistributionPrintProtocol;

import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;

import java.util.Arrays;

public class RtfRowIntercepterBaseExt extends RtfRowIntercepterBase {
    private GroupInfo groupInfo;

    public RtfRowIntercepterBaseExt(GroupInfo groupInfo) {
        this.groupInfo = groupInfo;
    }

    @Override
    public void beforeModify(RtfTable table, int currentRowIndex) {
        RtfRow headerRow = (RtfRow) table.getRowList().get(currentRowIndex - 1);
        RtfRow currentRow = (RtfRow) table.getRowList().get(currentRowIndex);

        int[] parts = new int[groupInfo.node.distribution.row.get(0).discipline.row.size()];
        Arrays.fill(parts, 1);
        // Убираем ошибку RuntimeException из RtfUtil.splitRow
        if (parts.length > 0) {
            RtfUtil.splitRow(headerRow, 4, (newCell, i) -> newCell.setElementList(new RtfString().content(groupInfo.node.distribution.row.get(0).discipline.row.get(i).shortTitle).toList()), parts);
            RtfUtil.splitRow(currentRow, 4, null, parts);
        }
    }
}
