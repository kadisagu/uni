package ru.tandemservice.narfu.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Результат импорта оценок из csv файла в ведомости"
 * Имя сущности : examBulletinImportedFileResult
 * Файл data.xml : catalogs.data.xml
 */
public interface ExamBulletinImportedFileResultCodes
{
    /** Константа кода (code) элемента : importOk (code). Название (title) : Без ошибок */
    String IMPORT_OK = "importOk";
    /** Константа кода (code) элемента : importAlreadyExist (code). Название (title) : Ранее обработан */
    String IMPORT_ALREADY_EXIST = "importAlreadyExist";
    /** Константа кода (code) элемента : importError (code). Название (title) : ОШИБКИ */
    String IMPORT_ERROR = "importError";

    Set<String> CODES = ImmutableSet.of(IMPORT_OK, IMPORT_ALREADY_EXIST, IMPORT_ERROR);
}
