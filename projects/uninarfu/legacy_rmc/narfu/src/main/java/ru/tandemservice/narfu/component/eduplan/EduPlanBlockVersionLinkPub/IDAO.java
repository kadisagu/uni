package ru.tandemservice.narfu.component.eduplan.EduPlanBlockVersionLinkPub;

import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {
    @Transactional
    public void makeLinks(Model model);
}
