package ru.tandemservice.narfu.base.ext.SessionTransfer.logic;

import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.unisession.base.bo.SessionTransfer.logic.ISessionTransferDao;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.InsideAddEdit.SessionTransferInOpWrapper;

import java.util.List;

public interface ISessionTransferDaoExt extends ISessionTransferDao {

    public List<SessionTransferInOpWrapper> getAutoFormInnerTransfers(Long studentId, EppEduPlanVersion eduPlanVersion);
}
