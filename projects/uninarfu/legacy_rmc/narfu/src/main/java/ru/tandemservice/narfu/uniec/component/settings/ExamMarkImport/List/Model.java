package ru.tandemservice.narfu.uniec.component.settings.ExamMarkImport.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.narfu.entity.EntrantExamBulletinImportedFile;

public class Model {
    private IDataSettings settings;
    private DynamicListDataSource<EntrantExamBulletinImportedFile> dataSource;
    private ISelectModel resultModel;

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public DynamicListDataSource<EntrantExamBulletinImportedFile> getDataSource() {
        return dataSource;
    }

    public void setDataSource(
            DynamicListDataSource<EntrantExamBulletinImportedFile> dataSource)
    {
        this.dataSource = dataSource;
    }

    public ISelectModel getResultModel() {
        return resultModel;
    }

    public void setResultModel(ISelectModel resultModel) {
        this.resultModel = resultModel;
    }


}
