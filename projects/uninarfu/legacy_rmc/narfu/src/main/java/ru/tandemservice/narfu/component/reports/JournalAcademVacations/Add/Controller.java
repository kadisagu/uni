package ru.tandemservice.narfu.component.reports.JournalAcademVacations.Add;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;

import java.text.SimpleDateFormat;


public class Controller extends AbstractBusinessController<IDAO, Model> {

    public void onRefreshComponent(IBusinessComponent component) {
        Model model = getModel(component);
        getDao().prepare(model);
    }

    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().createReport(model);

        String fileName = "Report_" + new SimpleDateFormat("dd_MM_yyyy").format(model.getReport().getFormingDate());
        byte[] content = model.getReport().getContent().getContent();
        if (content == null)
            throw new ApplicationException("Файл отчета пуст.");
        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(content, fileName);

        activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new UniMap()
                .add("id", id).add("extension", "xls")));

        deactivate(component);

    }
}
