package ru.tandemservice.narfu.component.settings.FormingOrgUnit2Commission.List;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.narfu.entity.FormingOrgUnit2Commission;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DAO extends UniDao<Model> implements IDAO {
    private static final String FORMING_ORGUNIT_CODE = "2";

    @Override
    public void prepare(Model model) {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
    }

    @Override
    public void prepareListDataSource(Model model) {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(OrgUnitToKindRelation.class, "our");
        dql.where(DQLExpressions.eq(DQLExpressions.property(OrgUnitToKindRelation.orgUnitKind().code().fromAlias("our")), DQLExpressions.value(FORMING_ORGUNIT_CODE)));
        dql.column("our.orgUnit");
        List<OrgUnit> formingOrgUnitList = dql.createStatement(getSession()).list();
        Map<OrgUnit, OrgUnit> relMap = new HashMap<>();
        for (FormingOrgUnit2Commission item : getList(FormingOrgUnit2Commission.class, "enrollmentCampaign", model.getEnrollmentCampaign(), new String[0]))
            relMap.put(item.getFormingOrgUnit(), item.getCommission());
        List<Wrapper> wrapperList = new ArrayList<>();
        for (OrgUnit unit : formingOrgUnitList) {
            OrgUnit commission = relMap.get(unit);
            wrapperList.add(new Wrapper(unit, commission));
        }
        DynamicListDataSource<Wrapper> dataSource = model.getDataSource();
        dataSource.setCountRow(wrapperList.size());
        UniUtils.createPage(dataSource, wrapperList);
    }
}
