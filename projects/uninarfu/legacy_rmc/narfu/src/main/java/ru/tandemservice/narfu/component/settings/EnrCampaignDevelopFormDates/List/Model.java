package ru.tandemservice.narfu.component.settings.EnrCampaignDevelopFormDates.List;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.narfu.entity.EnrCampaignDevelopFormDates;

public class Model {
    private DynamicListDataSource<EnrCampaignDevelopFormDates> dataSource;

    public void setDataSource(DynamicListDataSource<EnrCampaignDevelopFormDates> dataSource) {
        this.dataSource = dataSource;
    }

    public DynamicListDataSource<EnrCampaignDevelopFormDates> getDataSource() {
        return dataSource;
    }


}
