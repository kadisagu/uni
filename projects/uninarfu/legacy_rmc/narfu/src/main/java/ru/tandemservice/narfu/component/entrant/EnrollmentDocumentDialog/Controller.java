package ru.tandemservice.narfu.component.entrant.EnrollmentDocumentDialog;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        ((IDAO) getDao()).prepare(model);
    }

    public void onClickPrint(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        byte[] content = ((IDAO) getDao()).printEnrollmentDocument(model);
        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(content, "Справка о зачислении.rtf");
        activateInRoot(component, new ComponentActivator("ru.tandemservice.uni.component.reports.PrintReport", new UniMap().add("id", id)));
        deactivate(component);
    }
}
