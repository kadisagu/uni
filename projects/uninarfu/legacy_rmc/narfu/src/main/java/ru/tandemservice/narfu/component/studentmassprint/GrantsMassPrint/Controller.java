package ru.tandemservice.narfu.component.studentmassprint.GrantsMassPrint;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.component.Activator;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BooleanColumn;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.narfu.component.studentmassprint.printDAO.grants.IStudentGrantReqestGenerator;
import ru.tandemservice.narfu.entity.catalog.GrantTemplateDocument;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class Controller extends ru.tandemservice.unirmc.component.studentmassprint.Base.Controller<IDAO, Model>
{
    @Override
    public String getSettingsKey(final Model model)
    {
        return "MassPrint.filter";
    }

    // массовая печать
    public void onClickPrintAll(final IBusinessComponent component)
    {
        // шаблон militaryTemplateDocument
        Model model = component.getModel();


        GrantTemplateDocument templateDoc = (GrantTemplateDocument) model.getGrantTemplateDocument();

        if (templateDoc == null)
            throw new ApplicationException("Не выбраны шаблон для массовой печати");

        CheckboxColumn ck = (CheckboxColumn) model.getStudentDataSource().getColumn("select");
        Collection<IEntity> lst = ck.getSelectedObjects();

        List<Student> lstStudents = new ArrayList<>();

        if (lst.size() > 0) {
            for (IEntity ent : lst) {
                Student student = null;
                if (ent instanceof DataWrapper)
                    student = ((DataWrapper) ent).getWrapped();

                if (ent instanceof ViewWrapper)
                    student = (Student) ((ViewWrapper) ent).getEntity();

                if (ent instanceof Student)
                    student = (Student) ent;

                if (student != null)
                    lstStudents.add(student);
            }
        }

        if (lstStudents.size() == 0)
            throw new ApplicationException("Не выбраны студенты для массовой печати");


        // можно массово печатать
        _massPrint(templateDoc, lstStudents, model);

    }

    private void _massPrint(GrantTemplateDocument templateDoc, List<Student> lstStudents, Model model)
    {

        String beanName = templateDoc.getCode();
        if (!ApplicationRuntime.containsBean(beanName))
            throw new ApplicationException("Для печатной формы не реализован механизм массовой печати");

        // получим bean
        @SuppressWarnings("unchecked") IStudentGrantReqestGenerator<Model> bean = (IStudentGrantReqestGenerator<Model>) ApplicationRuntime.getBean(templateDoc.getCode());

        byte[] doc = bean.generateReport(lstStudents, templateDoc.getContent(), model);

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName(templateDoc.getTitle() + ".zip").document(doc), false);

    }


    @Override
    protected void addExtColumn(Model model, DynamicListDataSource dataSource)
    {
        dataSource.addColumn(new BooleanColumn("Приложенный документ", "hasDocuments").setClickable(false).setOrderable(true));
        AbstractColumn column = new ActionColumn("Редактировать приложенный документ", ActionColumn.EDIT, "onEditStudentGrantDocuments").setDisplayHeader(true);
        if(model.getSecModel() != null)
            column.setPermissionKey(model.getSecModel().getPermission("rmc_edit_studentgrantmassprint"));
        dataSource.addColumn(column);

    }

    /**
     * Активировать форму добавления документов
     */
    public void onEditStudentGrantDocuments(IBusinessComponent component) {
        Long studentId = component.getListenerParameter();

        Activator activator = new ComponentActivator("ru.tandemservice.narfu.component.studentmassprint.AddGrantDocument",
                                                     new ParametersMap().add("studentId", studentId));

        ContextLocal.createDesktop("PersonShellDialog", activator);
    }
}
