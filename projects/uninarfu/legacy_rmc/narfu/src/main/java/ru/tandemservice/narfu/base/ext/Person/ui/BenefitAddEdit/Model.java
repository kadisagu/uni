package ru.tandemservice.narfu.base.ext.Person.ui.BenefitAddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.narfu.entity.PersonBenefitExt;

import java.util.Arrays;
import java.util.List;

public class Model extends org.tandemframework.shared.person.base.bo.Person.ui.BenefitAddEdit.Model {

    private final List<String> BENEFIT_CODES = Arrays.asList("8", "11", "4");

    private ISelectModel causeDisabilityModel;
    private PersonBenefitExt personBenefitExt;

    public ISelectModel getCauseDisabilityModel() {
        return causeDisabilityModel;
    }

    public void setCauseDisabilityModel(ISelectModel causeDisabilityModel) {
        this.causeDisabilityModel = causeDisabilityModel;
    }

    public PersonBenefitExt getPersonBenefitExt() {
        return personBenefitExt;
    }

    public void setPersonBenefitExt(PersonBenefitExt personBenefitExt) {
        this.personBenefitExt = personBenefitExt;
    }

    public boolean isVisibleCauseDisablility() {
        if (this.getPersonBenefit().getBenefit() == null)
            return false;
        else
            return BENEFIT_CODES.contains(this.getPersonBenefit().getBenefit().getCode());
    }

}
