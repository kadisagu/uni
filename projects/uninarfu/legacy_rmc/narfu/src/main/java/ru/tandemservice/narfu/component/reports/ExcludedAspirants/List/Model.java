package ru.tandemservice.narfu.component.reports.ExcludedAspirants.List;

import org.tandemframework.core.component.State;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.sec.runtime.SecurityRuntime;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.narfu.uniec.entity.report.ExcludedAspirantsReport;

@State(keys = "orgUnitId", bindings = "orgUnitId")
public class Model {

    private ExcludedAspirantsReport report = new ExcludedAspirantsReport();
    private IDataSettings settings;

    private Long orgUnitId;
    private OrgUnit orgUnit;
    private CommonPostfixPermissionModel secModel;

    private DynamicListDataSource<ExcludedAspirantsReport> dataSource;

    public ExcludedAspirantsReport getReport() {
        return report;
    }

    public void setReport(ExcludedAspirantsReport report) {
        this.report = report;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public DynamicListDataSource<ExcludedAspirantsReport> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<ExcludedAspirantsReport> dataSource) {
        this.dataSource = dataSource;
    }


    public CommonPostfixPermissionModel getSecModel() {
        return secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel) {
        this.secModel = secModel;
    }

    public Object getSecuredObject() {
        return null == getOrgUnit() ? SecurityRuntime.getInstance().getCommonSecurityObject() : getOrgUnit();
    }

    public String getViewKey() {

        return getOrgUnit() == null ? "viewGlobalStoredReport" : getSecModel().getPermission("orgUnit_viewExcludedAspirantsReport");
    }

    public String getAddKey()
    {
        return getOrgUnit() == null ? "addGlobalStoredReport" : getSecModel().getPermission("orgUnit_addExcludedAspirantsReport");
    }

    public String getPrintKey()
    {
        return getOrgUnit() == null ? "printGlobalStoredReport" : getSecModel().getPermission("orgUnit_viewExcludedAspirantsReport");
    }

    public String getDeleteKey()
    {
        return getOrgUnit() == null ? "deleteGlobalStoredReport" : getSecModel().getPermission("orgUnit_deleteExcludedAspirantsReport");
    }

    public OrgUnit getOrgUnit() {
        return orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit) {
        this.orgUnit = orgUnit;
    }

    public Long getOrgUnitId() {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId) {
        this.orgUnitId = orgUnitId;
    }

}
