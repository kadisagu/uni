package ru.tandemservice.narfu.component.settings.EnrCampaignDevelopFormDates.List;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.narfu.entity.EnrCampaignDevelopFormDates;
import ru.tandemservice.uni.UniUtils;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        getDao().prepare(model);
        prepareListDateSource(component);
    }

    private void prepareListDateSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null) {
            return;
        }
        else {
            DynamicListDataSource<EnrCampaignDevelopFormDates> dataSource = UniUtils.createDataSource(component, getDao());
            dataSource.addColumn((new SimpleColumn("Приемная кампания", EnrCampaignDevelopFormDates.enrollmentCampaign().title())).setClickable(false).setOrderable(false));
            dataSource.addColumn((new SimpleColumn("Форма освоения", EnrCampaignDevelopFormDates.developForm().title())).setClickable(false).setOrderable(false));
            dataSource.addColumn((new SimpleColumn("Дата сдачи", EnrCampaignDevelopFormDates.P_DOC_DATE, DateFormatter.DEFAULT_DATE_FORMATTER)).setClickable(false).setOrderable(false));
            dataSource.addColumn(new ActionColumn("Редактировать", "edit", "onClickEditRecord"));
            dataSource.addColumn(new ActionColumn("Удалить", "delete", "onClickDeleteRecord", "Удалить запись?"));
            model.setDataSource(dataSource);
            return;
        }
    }

    public void onClickAddRecord(IBusinessComponent component)
    {
        component.createRegion(new ComponentActivator("ru.tandemservice.narfu.component.settings.EnrCampaignDevelopFormDates.AddEdit", (new UniMap()).add("recId", null)));
    }

    public void onClickEditRecord(IBusinessComponent component)
    {
        component.createRegion(new ComponentActivator("ru.tandemservice.narfu.component.settings.EnrCampaignDevelopFormDates.AddEdit", (new UniMap()).add("recId", component.getListenerParameter())));
    }

    public void onClickDeleteRecord(IBusinessComponent component)
    {
        getDao().deleteRow(component);
    }

}
