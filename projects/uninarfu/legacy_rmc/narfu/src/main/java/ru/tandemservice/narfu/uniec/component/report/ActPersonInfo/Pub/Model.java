package ru.tandemservice.narfu.uniec.component.report.ActPersonInfo.Pub;

import org.tandemframework.core.component.State;
import ru.tandemservice.narfu.uniec.entity.report.NarfuActPersonInfo;

@State(keys = {"publisherId"}, bindings = {"report.id"})
public class Model {
    private NarfuActPersonInfo _report = new NarfuActPersonInfo();

    public NarfuActPersonInfo getReport()
    {
        return this._report;
    }

    public void setReport(NarfuActPersonInfo report)
    {
        this._report = report;
    }
}