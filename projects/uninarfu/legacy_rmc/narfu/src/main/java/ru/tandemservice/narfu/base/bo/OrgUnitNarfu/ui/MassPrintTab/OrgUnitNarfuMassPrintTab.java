package ru.tandemservice.narfu.base.bo.OrgUnitNarfu.ui.MassPrintTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;

@Configuration
public class OrgUnitNarfuMassPrintTab extends BusinessComponentManager {

    //Панель вкладок
    public static final String TAB_PANEL = "orgUnitMassPrintTabPanel";

    //Вкладки
    public static final String MASS_PRINT_TAB = "massPrintTab";
    public static final String GRAND_MASS_PRINT_TAB = "grantsMassPrintTab";

    @Bean
    public TabPanelExtPoint tabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(TAB_PANEL)
                .addTab(componentTab(MASS_PRINT_TAB, "ru.tandemservice.unirmc.component.studentmassprint.MassPrint")
                                .parameters("ui:params")
                                .permissionKey("ui:secModel.rmc_view_studentmassprint_tab"))

                .addTab(componentTab(GRAND_MASS_PRINT_TAB, "ru.tandemservice.narfu.component.studentmassprint.GrantsMassPrint")
                                .parameters("ui:params")
                                .permissionKey("ui:secModel.rmc_view_studentgrantmassprint_tab"))
                .create();
    }

}
