package ru.tandemservice.narfu.component.eduplan.EduPlanBlockVersionLinkAdd;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.dsl.IPropertyPath;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import ru.tandemservice.narfu.entity.EppEduPlanVersionBlockLink;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationChild;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationRoot;
import ru.tandemservice.uniepp.entity.plan.*;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DAO extends UniDao<Model> implements IDAO {
    public void prepare(final Model model) {
        final EppEduPlanVersionBlock currentBlock = getNotNull(EppEduPlanVersionBlock.class, model.getBlockId());


        model.getBlockModel().setSource(new DQLFullCheckSelectModel(new IPropertyPath[]{EppEduPlanVersionBlock.fullTitleExtended()}) {
            protected DQLSelectBuilder query(String alias, String filter) {
                IdentifiableWrapper source = model.getBlockSourceModel().getValue();
                if (null == source) return null;

                final String planProfAlias = "eepp";
                final String planVersionAlias = "epv";
                final String rootBlockAlias = "epvrb";
                final String specBlockAlias = "epvsb";

                //Нуно
                final String rootTitle = new EppEduPlanVersionRootBlock().getTitle();
                final DQLSelectBuilder rootBlockTitleBuilder = new DQLSelectBuilder().fromEntity(EppEduPlanVersionRootBlock.class, rootBlockAlias)
                        .column(property(rootBlockAlias, EppEduPlanVersionRootBlock.id()), "id")
                        .column(caseExpr(eq(value(Boolean.TRUE), value(Boolean.TRUE)), value(rootTitle), commonValue(null)), "title");
                //Тоже нуно
                final String specTitle = new EduProgramSpecializationRoot().getDisplayableTitle();
                final DQLSelectBuilder specBlockTitleBuilder = new DQLSelectBuilder().fromEntity(EppEduPlanVersionSpecializationBlock.class, specBlockAlias)
                        .column(property(specBlockAlias, EppEduPlanVersionSpecializationBlock.id()), "id")
                        .column(caseExpr(isNotNull(property("sc", EduProgramSpecializationChild.id())), property("sc", EduProgramSpecializationChild.title()), value(specTitle)), "title")
                        .joinEntity(specBlockAlias, DQLJoinType.left, EduProgramSpecializationChild.class, "sc", eq(property("sc", EduProgramSpecializationChild.id()), property(specBlockAlias, EppEduPlanVersionSpecializationBlock.programSpecialization().id())));

                /////////////////////////////////////////////////////////////////////////////////////////////
                DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppEduPlanVersionBlock.class, alias)
                        .joinPath(DQLJoinType.inner, EppEduPlanVersionBlock.eduPlanVersion().fromAlias(alias), planVersionAlias)
                        .joinEntity(alias,
                                DQLJoinType.inner,
                                EppEduPlanProf.class,
                                planProfAlias,
                                eq(property(planVersionAlias, EppEduPlanVersion.eduPlan().id()), property(planProfAlias, EppEduPlanProf.id())))
                        .joinDataSource(alias,
                                DQLJoinType.left,
                                rootBlockTitleBuilder.buildQuery(),
                                rootBlockAlias,
                                eq(property(rootBlockAlias, "id"), property(alias, EppEduPlanVersionBlock.id())))
                        .joinDataSource(alias,
                                DQLJoinType.left,
                                specBlockTitleBuilder.buildQuery(),
                                specBlockAlias,
                                eq(property(specBlockAlias, "id"), property(alias, EppEduPlanVersionBlock.id())))
                        .where(ne(property(EppEduPlanVersionBlock.id().fromAlias(alias)), value(currentBlock.getId())));

                if (Model.BLOCK_SOURCE_SAME_LEVEL.equals(source.getId())) {
//TODO DEV-6870
//                    dql.where(DQLExpressions.eq(DQLExpressions.property(EppEduPlanVersionBlock.educationLevelHighSchool().educationLevel().levelType().fromAlias(alias)), DQLExpressions.value(currentBlock.getEducationLevelHighSchool().getEducationLevel().getLevelType())));
                }
                else if (Model.BLOCK_SOURCE_SAME_EDUHS.equals(source.getId())) {
//TODO DEV-6870
//                    dql.where(DQLExpressions.eq(DQLExpressions.property(EppEduPlanVersionBlock.educationLevelHighSchool().fromAlias(alias)), DQLExpressions.value(currentBlock.getEducationLevelHighSchool())));
                }

                if (StringUtils.isNotBlank(filter))
                {

                    dql.where(or(
                                    likeUpper(DQLFunctions.concat(
//                                                  №101.1 02.04.20 Лесозаготовка | Общий блок (очн, 4 года (8 сессий), гр.вых.дн., 2013-2015 г.)
//                                                  №<номер УП>.<номер УП(в)> <направление проф. образования> | <блок/направленность> (<форма обучения>, <учебная сетка>, <особенность реализации>, <выпускающее подр.>, <год начала><год окончания> г.)
//                                                  filter by <номер УП>.<номер УП(в)>
                                                    property(EppEduPlanVersionBlock.eduPlanVersion().eduPlan().number().fromAlias(alias)),
                                                    value("."),
                                                    property(EppEduPlanVersionBlock.eduPlanVersion().number().fromAlias(alias))),
                                            value(CoreStringUtils.escapeLike(filter))),
                                    likeUpper(DQLFunctions.concat(
//                                                  filte <направление проф. образования>
//                                                  050407 Педагогика и психология девиантного поведения
//                                                  <полный код направления подготовки (subjectCode)> <название (title)>
                                                    property(EppEduPlanProf.programSubject().subjectCode().fromAlias(planProfAlias)),
                                                    property(EppEduPlanProf.programSubject().title().fromAlias(planProfAlias))),
                                            value(CoreStringUtils.escapeLike(filter))),
                                    likeUpper(
//                                          filter <блок/направленность>
                                            property(specBlockAlias, "title"),
                                            value(CoreStringUtils.escapeLike(filter))),
                                    likeUpper(
//                                          filter <блок/направленность>
                                            property(rootBlockAlias, "title"),
                                            value(CoreStringUtils.escapeLike(filter)))
                            )
                    );
//TODO DEV-6870
//                    dql.where(DQLExpressions.or(new IDQLExpression[]{like(EppEduPlanVersionBlock.educationLevelHighSchool().fullTitle().fromAlias(alias), filter), like(EppEduPlanVersionBlock.eduPlanVersion().eduPlan().number().fromAlias(alias), filter)}));
                }

                dql.order(DQLExpressions.property(EppEduPlanVersionBlock.eduPlanVersion().eduPlan().number().fromAlias(alias)));
                dql.order(DQLExpressions.property(EppEduPlanVersionBlock.eduPlanVersion().number().fromAlias(alias)));
//TODO DEV-6870
//                dql.order(DQLExpressions.property(EppEduPlanVersionBlock.educationLevelHighSchool().educationLevel().code().fromAlias(alias)));
//                dql.order(DQLExpressions.property(EppEduPlanVersionBlock.educationLevelHighSchool().fullTitle().fromAlias(alias)));

                return dql;
            }
        });
        if (null == model.getBlockSourceModel().getValue()) {
            model.getBlockSourceModel().setupFirstValue();
        }

    }

    public void update(Model model) {
        validate(model, ContextLocal.getUserContext().getErrorCollector());
        if (model.getBlockModel().getValue() != null) {
            EppEduPlanVersionBlock currentBlock = getNotNull(EppEduPlanVersionBlock.class, model.getBlockId());
            EppEduPlanVersionBlockLink link = new EppEduPlanVersionBlockLink();
            link.setBlockSrc(currentBlock);
            link.setBlockDst(model.getBlockModel().getValue());
            save(link);

        }
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        if (model.getBlockModel().getValue() != null) {
            EppEduPlanVersionBlock currentBlock = getNotNull(EppEduPlanVersionBlock.class, model.getBlockId());
            DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppEduPlanVersionBlockLink.class, "l");
            dql.where(DQLExpressions.eq(property(EppEduPlanVersionBlockLink.blockSrc().fromAlias("l")), value(currentBlock)));
            dql.where(DQLExpressions.eq(property(EppEduPlanVersionBlockLink.blockDst().fromAlias("l")), value(model.getBlockModel().getValue())));
            List<EppEduPlanVersionBlock> list = getList(dql);
            if (!CollectionUtils.isEmpty(list))
                errors.add("Уже существует такая зависимость");

        }

    }

}
