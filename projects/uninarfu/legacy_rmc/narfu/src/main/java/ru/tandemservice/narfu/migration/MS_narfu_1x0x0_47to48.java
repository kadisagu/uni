package ru.tandemservice.narfu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;

import java.util.Arrays;
import java.util.List;


public class MS_narfu_1x0x0_47to48 extends IndependentMigrationScript {

    @Override
    public void run(DBTool tool) throws Exception
    {

        if (tool.tableExists("DOCUMENTTYPE2STUDENTCATEGORY_T")) {
            // Для Голобурда
            // судя по коду, ты пытаешся переименовать 5 в aspirantbaserms.5
            // но почему сущность в narfu а пытался в общем модуле

            List<String> codes = Arrays.asList("aspirantbaserms.5", "aspirantbaserms.6");

            for (String code : codes) {
                //удалим сущность связи типа документа и категории обучаемого
                // vch - а нафея удалять??? Ну ладно - это не страшно вроде. НО НАФЕЯ!!!
                tool.executeUpdate("delete from DOCUMENTTYPE2STUDENTCATEGORY_T where id in (select dt2sc.id from DOCUMENTTYPE2STUDENTCATEGORY_T dt2sc, STUDENTCATEGORY_T sc where dt2sc.STUDENTCATEGORY_ID = sc.id and sc.code_p = '" + code + "')");


                //удалим "лишнию" категорию обучаемого
                // vch - АФИГЕТЬ!!! А если там есть студенты????
                //tool.executeUpdate("delete from studentcategory_t where code_p='" + code + "'");
            }
            //изменим код у категорий
            tool.executeUpdate("update studentcategory_t set code_p='aspirantbaserms.5' where code_p='5'");
            tool.executeUpdate("update studentcategory_t set code_p='aspirantbaserms.6' where code_p='6'");
        }
    }
}