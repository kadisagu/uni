package ru.tandemservice.narfu.component.settings.YearPartEndDate.List;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.narfu.entity.YearPartStartEndDate;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDaoFacade;

import java.util.Map;

public class Controller extends AbstractBusinessController<IDAO, Model> {
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        prepareListDataSource(component);
        model.setSettings(UniUtils.getDataSettings(component, getSettingsKey(model)));
    }

    public String getSettingsKey(Model model)
    {
        return "YearPartStartEndDate.filter";
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<YearPartStartEndDate> dataSource = UniUtils.createDataSource(component, getDao());

        dataSource.addColumn(new SimpleColumn("Учебный год", YearPartStartEndDate.educationYear().title()).setClickable(true));
        dataSource.addColumn(new SimpleColumn("Часть учебного года", YearPartStartEndDate.part().title()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Начало", YearPartStartEndDate.startDate()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Конец", YearPartStartEndDate.endDate()).setClickable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit").setPermissionKey("ctrrmcYearPartStartEndDateEdit"));

        // эта сортировка сработает 1 раз (при первом создании компонента)
        dataSource.setOrder(0, OrderDirection.desc);
        model.setDataSource(dataSource);

    }

    public void onClickAdd(IBusinessComponent component)
    {
        Model model = getModel(component);

        Long educationYearId = null;
        Long partId = null;
        if (model.getEducationYear() != null)
            educationYearId = model.getEducationYear().getId();

        if (model.getYearDistributionPart() != null)
            partId = model.getYearDistributionPart().getId();

        Map<String, Object> params = new UniMap()
                .add("EducationYear", educationYearId)
                .add("Id", null)
                .add("Part", partId);

        activate(
                component,
                new ComponentActivator("ru.tandemservice.narfu.component.settings.YearPartEndDate.AddEdit", params));
    }

    public void onClickSearch(IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
        model.getDataSource().showFirstPage();
        model.getDataSource().refresh();
        UniDaoFacade.getSettingsManager().saveSettings(model.getSettings());

    }

    public void onClickClear(IBusinessComponent component) {
        Model model = getModel(component);
        model.setEducationYear(null);
        model.setYearDistributionPart(null);
        onClickSearch(component);
    }

    public void onClickEdit(IBusinessComponent component)
    {
        YearPartStartEndDate yearPartStartEndDate = getDao().getNotNull((Long) component.getListenerParameter());

        Map<String, Object> params = new UniMap()
                .add("EducationYear", null)
                .add("Id", yearPartStartEndDate.getId())
                .add("Part", null);

        activate(
                component,
                new ComponentActivator("ru.tandemservice.narfu.component.settings.YearPartEndDate.AddEdit", params)
        );

    }

}
