package ru.tandemservice.narfu.migration;

import org.apache.commons.codec.digest.DigestUtils;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.narfu.entity.IdentityCardHidden;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniec.entity.entrant.Entrant;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_narfu_1x0x0_15to16 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.10"),
                        new ScriptDependency("org.tandemframework.shared", "1.1.3"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.1.3")
                };
    }

    /**
     * Процедура обезличивания УЛ архивных студентов
     * RM#2934
     */
    @Override
    public void run(DBTool tool) throws Exception {
        IEntityMeta hiddenMeta = EntityRuntime.getMeta(IdentityCardHidden.class);
        if (!tool.tableExists(hiddenMeta.getTableName()))
            return;

        //востановим все данные
        restoreOld(tool);

        process(tool);
    }

    protected void process(DBTool tool) throws Exception {
        IEntityMeta hiddenMeta = EntityRuntime.getMeta(IdentityCardHidden.class);
        short code = hiddenMeta.getEntityCode();
        IEntityMeta icMeta = EntityRuntime.getMeta(IdentityCard.class);

        String selectQuery = "select st.id as st_id, ic.id as ic_id, ic.number_p, ic.seria_p, ic.birthdate_p "
                + " from "
                + " student_t st "
                + " inner join personrole_t pr on pr.id=st.id "
                + " inner join identitycard_t ic on ic.person_id=pr.person_id "
                + " where "
                + " st.archival_p=1 ";

        String insertQuery = "insert into " + hiddenMeta.getTableName() + " (id, discriminator, identitycard_id, seria_p, number_p, birthdate_p) values (?,?,?,?,?,?)";
        String updateQuery = "update " + icMeta.getTableName() + " set seria_p=?, number_p=?, birthdate_p=? where id=?";

        ResultSet rs = tool.getConnection().createStatement().executeQuery(selectQuery);
        while (rs.next()) {
            Long stId = rs.getLong(1);
            Long icId = rs.getLong(2);
            String number = rs.getString(3);
            String seria = rs.getString(4);
            Date birth = rs.getDate(5);

            if (hasDependence(tool, icId, stId))
                continue;

            tool.executeUpdate(insertQuery, EntityIDGenerator.generateNewId(code), code, icId, seria, number, birth);

            number = number != null ? DigestUtils.md5Hex(number) : null;
            seria = seria != null ? DigestUtils.md5Hex(seria) : null;

            tool.executeUpdate(updateQuery, seria, number, "01-01-1900", icId);
        }
    }

    protected boolean hasDependence(DBTool tool, Long icId, Long studentId) throws Exception {
        List list = Arrays.asList(ContactorPerson.class, Employee.class, Entrant.class, Student.class);

        for (Object obj : list) {
            Class clazz = (Class) obj;
            IEntityMeta meta = EntityRuntime.getMeta(clazz);

            String selectQuery = "select count(*) from " + meta.getTableName() + " as aaa"
                    + " inner join personrole_t pr on pr.id=aaa.id "
                    + " inner join identitycard_t ic on ic.person_id=pr.person_id "
                    + " where "
                    + " ic.id=? ";

            if (ContactorPerson.class.equals(clazz))
                selectQuery += " and aaa.active_p=1 ";
            else
                selectQuery += " and aaa.archival_p=0 ";

            if (Student.class.equals(clazz))
                selectQuery += " and aaa.id<>?";

            PreparedStatement pst = tool.getConnection().prepareStatement(selectQuery);
            pst.setLong(1, icId);
            if (Student.class.equals(clazz))
                pst.setLong(2, studentId);

            ResultSet rs = pst.executeQuery();
            rs.next();
            int count = rs.getInt(1);

            if (count > 0)
                return true;
        }

        return false;
    }

    protected void restoreOld(DBTool tool) throws Exception {
        IEntityMeta hiddenMeta = EntityRuntime.getMeta(IdentityCardHidden.class);
        IEntityMeta icMeta = EntityRuntime.getMeta(IdentityCard.class);

        String selectQuery = "select ID, IDENTITYCARD_ID, NUMBER_P, SERIA_P, BIRTHDATE_P from " + hiddenMeta.getTableName();
        ResultSet rs = tool.getConnection().createStatement().executeQuery(selectQuery);

        List<Object[]> list = new ArrayList<Object[]>();
        while (rs.next()) {
            Long id = rs.getLong(1);
            long icId = rs.getLong(2);
            String number = rs.getString(3);
            String seria = rs.getString(4);
            Date birth = rs.getDate(5);

            Object[] arr = new Object[5];
            arr[0] = id;
            arr[1] = icId;
            arr[2] = number;
            arr[3] = seria;
            arr[4] = birth;
            list.add(arr);

        }

        String updateQuery = "update " + icMeta.getTableName() + " SET NUMBER_P=?, SERIA_P=?, BIRTHDATE_P=? where ID=?";
        String deleteQuery = "delete from " + hiddenMeta.getTableName() + " where id=?";
        for (Object[] arr : list) {
            tool.executeUpdate(updateQuery, arr[2], arr[3], arr[4], arr[1]);
            tool.executeUpdate(deleteQuery, arr[0]);
        }
    }
}