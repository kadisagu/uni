package ru.tandemservice.narfu.component.reports.StudentAgeSexDistribution.List;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.component.reports.studentsAgeSexDistribution.List.Model;

public class Controller extends ru.tandemservice.uni.component.reports.studentsAgeSexDistribution.List.Controller {
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        Model model = getModel(component);
        DynamicListDataSource dataSource = model.getDataSource();
        dataSource.getColumn(8).setCaption("Категория обучающегося");
    }
}
