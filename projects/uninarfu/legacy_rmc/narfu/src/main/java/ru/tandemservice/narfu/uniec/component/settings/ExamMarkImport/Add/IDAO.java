package ru.tandemservice.narfu.uniec.component.settings.ExamMarkImport.Add;


import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uni.dao.IUniDao;

import java.io.IOException;

public interface IDAO extends IUniDao<Model> {
    @Transactional
    public void process(Model model) throws IOException;
}
