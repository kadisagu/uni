package ru.tandemservice.narfu.base.ext.EcDistribution.logic;

import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.rtf.util.RtfUtil;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgDistribution;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;

public class EcDistributionDaoWarningExt
        extends CommonDAO
        implements IEcDistributionDaoWarningExt
{

    @Override
    public byte[] getRtf(Long distributionId)
    {

        IEcgDistribution distribution = (IEcgDistribution) getNotNull(distributionId);
        getSession().refresh(distribution);
        // получаем бин печати
        IEcDistributionPrintWarning printForm = (IEcDistributionPrintWarning) IEcDistributionPrintWarning.instance.get();
        byte[] template = ((UniecScriptItem) getByCode(UniecScriptItem.class, "ecgDistributionWarningNarfu")).getCurrentTemplate();

        return RtfUtil.toByteArray
                (
                        printForm.getPrintForm(template, distribution)
                );

    }

}
