package ru.tandemservice.narfu.migration;

import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;
import ru.tandemservice.unibasermc.entity.catalog.StudentStatusExt;

import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_narfu_1x0x0_41to42 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность studentStatusNarfu

        if (tool.tableExists("studentstatusext_t") && tool.tableExists("studentstatusnarfu_t")) {
            short code = EntityRuntime.getMeta(StudentStatusExt.class).getEntityCode();
            String insertQuery = "insert into studentstatusext_t (id, discriminator, studentstatus_id, archievable_p) values (?,?,?,?)";
            Statement stmt = tool.getConnection().createStatement();

            stmt.execute("select v.studentstatus_id, v.archievable_p " +
                                 "from studentstatusnarfu_t as v");

            ResultSet rs = stmt.getResultSet();

            while (rs.next()) {
                Long status = rs.getLong("studentstatus_id");
                boolean archivable = rs.getBoolean("archievable_p");


                tool.executeUpdate(insertQuery, EntityIDGenerator.generateNewId(code), code, status, archivable);
            }
        }
        // сущность была удалена
        {
            // TODO: программист должен подтвердить это действие
//			if( true )
//				throw new UnsupportedOperationException("Confirm me: delete table");

            // удалить таблицу
            tool.dropTable("studentstatusnarfu_t", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("studentStatusNarfu");

        }


    }
}