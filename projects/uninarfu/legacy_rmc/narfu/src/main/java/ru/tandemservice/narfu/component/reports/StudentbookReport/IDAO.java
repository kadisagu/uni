package ru.tandemservice.narfu.component.reports.StudentbookReport;

import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {

    public RtfDocument createReport(Model model);
}
