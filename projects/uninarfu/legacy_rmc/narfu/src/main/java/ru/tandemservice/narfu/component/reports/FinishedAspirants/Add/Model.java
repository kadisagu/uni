package ru.tandemservice.narfu.component.reports.FinishedAspirants.Add;

import org.tandemframework.core.component.State;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.narfu.uniec.entity.report.FinishedAspirantsReport;

@State(keys = "orgUnitId", bindings = "orgUnitId")
public class Model {

    private FinishedAspirantsReport report = new FinishedAspirantsReport();

    private ISelectModel compensationTypeModel;

    private Long orgUnitId;
    private OrgUnit orgUnit;

    public FinishedAspirantsReport getReport() {
        return report;
    }

    public void setReport(FinishedAspirantsReport report) {
        this.report = report;
    }

    public Long getOrgUnitId() {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId) {
        this.orgUnitId = orgUnitId;
    }

    public OrgUnit getOrgUnit() {
        return orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit) {
        this.orgUnit = orgUnit;
    }

    public ISelectModel getCompensationTypeModel() {
        return compensationTypeModel;
    }

    public void setCompensationTypeModel(ISelectModel compensationTypeModel) {
        this.compensationTypeModel = compensationTypeModel;
    }
}
