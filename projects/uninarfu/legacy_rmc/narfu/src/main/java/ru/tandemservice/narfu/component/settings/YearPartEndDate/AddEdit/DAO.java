package ru.tandemservice.narfu.component.settings.YearPartEndDate.AddEdit;

import ru.tandemservice.narfu.entity.YearPartStartEndDate;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.ui.EducationYearModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;


public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model)
    {
        YearPartStartEndDate yearPartStartEndDate = null;
        if (model.getEntityId() != null)
            yearPartStartEndDate = this.get(YearPartStartEndDate.class, YearPartStartEndDate.P_ID, model.getEntityId());
        else {
            yearPartStartEndDate = new YearPartStartEndDate();

            if (model.getEducationYearId() != null)
                yearPartStartEndDate.setEducationYear(get(EducationYear.class, model.getEducationYearId()));

            if (model.getYearDistributionPartId() != null)
                yearPartStartEndDate.setPart(get(YearDistributionPart.class, model.getYearDistributionPartId()));
        }


        model.setYearPartStartEndDate(yearPartStartEndDate);
        model.setEducationYearList(new EducationYearModel());
        model.setYearDistributionPartList(this.getList(YearDistributionPart.class, new String[]{EducationYear.P_CODE}));


    }

    @Override
    public void update(Model model)
    {
        this.saveOrUpdate(model.getYearPartStartEndDate());
    }

}
