package ru.tandemservice.narfu.unisession.component.settings.EstimatesExamImportList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.narfu.entity.ExamBulletinImportedFile;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.unibase.UniBaseUtils;

public class Controller extends AbstractBusinessController<IDAO, Model> {
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = (Model) getModel(component);

        //model.setSettings(component.getSettings());

        ((IDAO) getDao()).prepare(model);

        prepareDataSource(component);
        model.setSettings(UniBaseUtils.getDataSettings(component, "EstimatesExamImportList.filter"));
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        Model model = (Model) getModel(component);

        DynamicListDataSource<ExamBulletinImportedFile> dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(new SimpleColumn("Дата импорта", "importDate", DateFormatter.DATE_FORMATTER_WITH_TIME));
        dataSource.addColumn(new IndicatorColumn("Импортированный файл", null, "onClickPrintImportedFile").defaultIndicator(new IndicatorColumn.Item("printer", "Печать")).setImageHeader(false).setDisableSecondSubmit(false).setPermissionKey("printImportedStateExamFile").setOrderable(false));
        dataSource.addColumn(new IndicatorColumn("Результирующий файл", null, "onClickPrintResultFile").defaultIndicator(new IndicatorColumn.Item("printer", "Печать")).setImageHeader(false).setDisableSecondSubmit(false).setPermissionKey("printResultStateExamFile").setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Результат", "importResult.title"));
        dataSource.addColumn(new ActionColumn("Удалить", "delete", "onClickDeleteFiles", "Удалить импортированный и результирующий файлы от «{0}»?", new Object[]{"importDate"}).setPermissionKey("deleteUniecImportedResultFile"));
        dataSource.setOrder("importDate", OrderDirection.desc);
        model.setDataSource(dataSource);
    }

    public void onClickImport(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator("ru.tandemservice.narfu.unisession.component.settings.EstimatesExamImport"));
    }

    public void onClickSearch(IBusinessComponent component)
    {
        component.saveSettings();
        ((Model) getModel(component)).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component)
    {
        component.clearSettings();
        ((IDAO) getDao()).prepare(getModel(component));
        onClickSearch(component);
    }

    public void onClickPrintImportedFile(IBusinessComponent component)
    {
        byte[] content = ((IDAO) getDao()).getPrintImportedFileRequest((Long) component.getListenerParameter());
        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(content, "Импортированный файл оценок.csv");
        activateInRoot(component, new ComponentActivator("ru.tandemservice.uni.component.reports.PrintReport", new ParametersMap().add("id", id).add("extension", "txt")));
    }

    public void onClickPrintResultFile(IBusinessComponent component)
    {
        byte[] content = ((IDAO) getDao()).getPrintResultFileRequest((Long) component.getListenerParameter());
        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(content, "Результирующий файл импорта оценок.rtf");
        activateInRoot(component, new ComponentActivator("ru.tandemservice.uni.component.reports.PrintReport", new ParametersMap().add("id", id).add("extension", "rtf")));
    }

    public void onClickDeleteFiles(IBusinessComponent component)
    {
        ((IDAO) getDao()).deleteRow(component);
    }
}