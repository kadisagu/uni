package ru.tandemservice.narfu.component.reports.orgUnitStudentGrantByInstitute.List;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.narfu.entity.NarfuReportStudentGrantByInstitute;
import ru.tandemservice.unibase.UniBaseUtils;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = component.getModel();
        model.setSettings(component.getSettings());
        ((IDAO) getDao()).prepare(model);

        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component) {
        Model model = component.getModel();
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<NarfuReportStudentGrantByInstitute> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Дата формирования отчета", NarfuReportStudentGrantByInstitute.formingDate().s(), DateFormatter.DATE_FORMATTER_WITH_TIME));
        dataSource.addColumn(new SimpleColumn("Учебный год", NarfuReportStudentGrantByInstitute.educationYear().s()));
        dataSource.addColumn(new SimpleColumn("Часть года", NarfuReportStudentGrantByInstitute.educationYearPart().s()));
        dataSource.addColumn(new SimpleColumn("Форма освоения", NarfuReportStudentGrantByInstitute.developForm().s()));
        dataSource.addColumn(new SimpleColumn("Месяц выплаты", NarfuReportStudentGrantByInstitute.month().s()));
        dataSource.addColumn(new SimpleColumn("Исполнитель", NarfuReportStudentGrantByInstitute.executor().s()));
        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickPrintReport", "Печатать"));
        dataSource.addColumn(
                new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteReport", "Удалить отчет от {0}?", new Object[]{"formingDate"})
                        .setPermissionKey("orgUnit_deleteGrantByInstituteReport")
                        .setOrderable(false)
        );

        model.setDataSource(dataSource);
    }

    public void onClickAddReport(IBusinessComponent component) {
        ContextLocal.createDesktop(
                "PersonShellDialog",
                new ComponentActivator("ru.tandemservice.narfu.component.reports.orgUnitStudentGrantByInstitute.Add")
        );
    }

    public void onClickPrintReport(IBusinessComponent component) throws Exception {
        activateInRoot(
                component,
                new ComponentActivator(
                        "ru.tandemservice.uni.component.reports.DownloadStorableReport",
                        new ParametersMap().add("reportId", component.getListenerParameter()).add("extension", "xls")
                )
        );
    }

    public void onClickDeleteReport(IBusinessComponent component) {
        ((IDAO) getDao()).deleteRow(component);
    }

    public void onClickSearch(IBusinessComponent component) {
        component.saveSettings();
    }

    public void onClickClear(IBusinessComponent component) {
        Model model = component.getModel();
        model.getSettings().clear();
        onClickSearch(component);
    }

}
