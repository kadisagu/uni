package ru.tandemservice.narfu.base.ext.EcDistribution.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniec.base.bo.EcDistribution.EcDistributionManager;
import ru.tandemservice.uniec.base.bo.EcDistribution.logic.EcDistributionListPrint;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgDistribution;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.ws.distribution.EnrollmentDistributionEnvironmentNode;
import ru.tandemservice.uniec.ws.distribution.EnrollmentDistributionEnvironmentNode.BenefitNode.BenefitRow;
import ru.tandemservice.uniec.ws.distribution.EnrollmentDistributionEnvironmentNode.CompetitionKindNode.CompetitionKindRow;
import ru.tandemservice.uniec.ws.distribution.EnrollmentDistributionEnvironmentNode.DistributionNode.DistributionRow;
import ru.tandemservice.uniec.ws.distribution.EnrollmentDistributionEnvironmentNode.Row;
import ru.tandemservice.uniec.ws.distribution.EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow;
import ru.tandemservice.uniec.ws.distribution.IEnrollmentDistributionServiceDao;

import java.util.*;


public class EcDistributionListPrintNARFU extends EcDistributionListPrint implements IEcDistributionListPrintNarfu {


    @Override
    public RtfDocument getPrintFormForDistributionPerCompetitionGroupNarfu(byte[] template, IEcgDistribution distribution) {

        RtfDocument templateDocument = new RtfReader().read(template);

        RtfDocument result = RtfBean.getElementFactory().createRtfDocument();
        result.setHeader(templateDocument.getHeader());
        result.setSettings(templateDocument.getSettings());
        int grayColorIndex = result.getHeader().getColorTable().addColor(217, 217, 217);

        EnrollmentDistributionEnvironmentNode env = ((IEnrollmentDistributionServiceDao) IEnrollmentDistributionServiceDao.INSTANCE.get()).getEnrollmentDistributionEnvironmentNode(Collections.singleton(distribution));
        EnrollmentDistributionEnvironmentNode.DistributionNode.DistributionRow distributionRow = (EnrollmentDistributionEnvironmentNode.DistributionNode.DistributionRow) env.distribution.row.get(0);

        Map targetAdmissionKindMap = new HashMap();
        Map competitionKindMap = new HashMap();
        Map benefitMap = new HashMap();
        Map documentStateMap = new HashMap();
        Map enrollmentStateMap = new HashMap();
        fillTitleMap(env, targetAdmissionKindMap, competitionKindMap, benefitMap, documentStateMap, enrollmentStateMap);

        Map directionMap = new HashMap();
        for (EnrollmentDirection direction : EcDistributionManager.instance().dao().getDistributionDirectionList(distribution.getDistribution())) {
            directionMap.put(Long.toString(direction.getId().longValue()), direction);
        }
        String currentWave = Integer.toString(distributionRow.wave);
        String currentDate = DateFormatter.DEFAULT_DATE_FORMATTER.format(null != distribution.getApprovalDate() ? distribution.getApprovalDate() : new Date());
        String compensationType = distribution.getConfig().getCompensationType().isBudget() ? "на места, финансируемые из федерального бюджета" : "на места с оплатой стоимости обучения";
        String developForm = ((EnrollmentDirection) directionMap.get(((EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow) distributionRow.quota.row.get(0)).id)).getEducationOrgUnit().getDevelopForm().getGenCaseTitle();

        RtfInjectModifier im = new RtfInjectModifier();
        im.put("wave", currentWave);
        im.put("date", currentDate);
        im.put("title", distributionRow.ecgItemTitle);
        im.put("compensationType", compensationType);
        im.put("developForm", developForm);

        List listA = new ArrayList();
        List listB = new ArrayList();
        for (EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow quotaRow : distributionRow.quota.row) {
            EnrollmentDirection direction = (EnrollmentDirection) directionMap.get(quotaRow.id);

            (direction.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isSpecialityPrintTitleMode() ? listA : listB).add(new StringBuilder().append(direction.getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle()).append(" (").append(direction.getEducationOrgUnit().getEducationLevelHighSchool().getShortTitle()).append(")").toString());
        }

        String listAtitle = listA.isEmpty() ? null : new StringBuilder().append(listA.size() == 1 ? "по специальности: " : "по специальностям: ").append(StringUtils.join(listA, ", ")).toString();
        String listBtitle = listB.isEmpty() ? null : new StringBuilder().append(listB.size() == 1 ? "по направлению: " : "по направлениям: ").append(StringUtils.join(listB, ", ")).toString();
        RtfString directionTitle = new RtfString();
        if (listAtitle == null)
            directionTitle.append(listBtitle);
        else if (listBtitle == null)
            directionTitle.append(listAtitle);
        else
            directionTitle.append(listBtitle).par().append(listAtitle);
        im.put("directionTitle", directionTitle);

        Map id2planTitle = new HashMap();
        int totalCountBegin = 0;
        int totalTaQuotaCountBegin = 0;
        Map totalTaKindId2plan = new HashMap();
        for (EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow quotaRow : distributionRow.quota.row) {
            StringBuffer quotaTitle = new StringBuffer();
            quotaTitle.append("Количество мест ").append(distribution.getConfig().getCompensationType().isBudget() ? "на бюджет: " : "по договору: ").append(quotaRow.countBegin);

            totalCountBegin += quotaRow.countBegin;

            int taQuotaCountBegin = 0;
            for (EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow.TaQuotaNode.TaQuotaRow row : quotaRow.taQuota.row) {
                taQuotaCountBegin += row.countBegin;
            }
            totalTaQuotaCountBegin += taQuotaCountBegin;

            if (taQuotaCountBegin > 0) {
                quotaTitle.append(", Целевой прием: ");
                if (quotaRow.defaultTaKind) {
                    quotaTitle.append(taQuotaCountBegin);
                }
                else {
                    List list = new ArrayList();
                    for (EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow.TaQuotaNode.TaQuotaRow row : quotaRow.taQuota.row) {
                        list.add(new StringBuilder().append(((EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow) targetAdmissionKindMap.get(row.id)).shortTitle).append(" – ").append(row.countBegin).toString());

                        Integer plan = (Integer) totalTaKindId2plan.get(row.id);
                        totalTaKindId2plan.put(row.id, Integer.valueOf((plan == null ? 0 : plan.intValue()) + row.countBegin));
                    }
                    quotaTitle.append(StringUtils.join(list, ", "));
                }
            }
            id2planTitle.put(quotaRow.id, quotaTitle.toString());
        }
        StringBuffer quotaTitle = new StringBuffer();
        quotaTitle.append("Количество мест ").append(distribution.getConfig().getCompensationType().isBudget() ? "на бюджет: " : "по договору: ").append(totalCountBegin);

        if (totalTaQuotaCountBegin > 0) {
            quotaTitle.append(", Целевой прием: ");
            if (totalTaKindId2plan.isEmpty()) {
                quotaTitle.append(totalTaQuotaCountBegin);
            }
            else {
                List list = new ArrayList();
                for (EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow.TaQuotaNode.TaQuotaRow row : ((EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow) distributionRow.quota.row.get(0)).taQuota.row) {
                    Integer plan = (Integer) totalTaKindId2plan.get(row.id);
                    list.add(new StringBuilder().append(((EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow) targetAdmissionKindMap.get(row.id)).shortTitle).append(" – ").append(plan == null ? 0 : plan.intValue()).toString());
                }
                quotaTitle.append(StringUtils.join(list, ", "));
            }
        }
        im.put("planTitle", quotaTitle.toString());

        RtfDocument clone = templateDocument.getClone();
        im.modify(clone);

        List tableRow = new ArrayList();
        Set unionRowIndexList = new HashSet();
        fillTableRowListForDistributionPerCompetitionGroupNarfu(distributionRow, null, tableRow, unionRowIndexList, targetAdmissionKindMap, competitionKindMap, benefitMap, documentStateMap, enrollmentStateMap, directionMap);

        fillTableDataNarfu(clone, grayColorIndex, 3, tableRow, unionRowIndexList, distributionRow);

        result.getElementList().addAll(clone.getElementList());
        result.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));

        for (EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow quotaRow : distributionRow.quota.row) {
            im = new RtfInjectModifier();
            im.put("wave", currentWave);
            im.put("date", currentDate);
            im.put("title", distributionRow.ecgItemTitle);
            im.put("compensationType", compensationType);
            im.put("developForm", developForm);

            EnrollmentDirection direction = (EnrollmentDirection) directionMap.get(quotaRow.id);
            EducationLevelsHighSchool hs = direction.getEducationOrgUnit().getEducationLevelHighSchool();
            im.put("directionTitle", new StringBuilder().append(hs.getEducationLevel().getLevelType().isSpecialityPrintTitleMode() ? "по специальности: " : "по направлению: ").append(hs.getPrintTitle()).append(" (").append(hs.getShortTitle()).append(")").toString());
            im.put("planTitle", (String) id2planTitle.get(quotaRow.id));

            clone = templateDocument.getClone();
            im.modify(clone);

            tableRow.clear();
            unionRowIndexList.clear();
            fillTableRowListForDistributionPerCompetitionGroupNarfu(distributionRow, quotaRow.id, tableRow, unionRowIndexList, targetAdmissionKindMap, competitionKindMap, benefitMap, documentStateMap, enrollmentStateMap, directionMap);

            fillTableDataNarfu(clone, grayColorIndex, 3, tableRow, unionRowIndexList, distributionRow);

            result.getElementList().addAll(clone.getElementList());

            result.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));

        }

        return result;

    }


    protected void fillTableRowListForDistributionPerCompetitionGroupNarfu(
            DistributionRow distributionRow, String directionId,
            List<String[]> tableRowList, Set<Integer> unionRowIndexList,
            Map<String, TargetAdmissionKindRow> targetAdmissionKindMap,
            Map<String, CompetitionKindRow> competitionKindMap,
            Map<String, BenefitRow> benefitMap,
            Map<String, Row> documentStateMap,
            Map<String, Row> enrollmentStateMap,
            Map<String, EnrollmentDirection> directionMap)
    {

        boolean defaultTaKind = ((EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow) distributionRow.quota.row.get(0)).defaultTaKind;

        int currentRowKey = -1;

        int rowIndex = 0;

        int number = 1;

        for (EnrollmentDistributionEnvironmentNode.EntrantNode.EntrantRow row : distributionRow.entrant.row) {
            if ((directionId != null) && (!row.priorityIds.contains(Long.valueOf(directionId)))) {
                continue;
            }

            int rowKey = row.targetAdmissionKind == null ? Integer.parseInt(row.competitionKind == null ? "5" : row.competitionKind) : 0;

            if (currentRowKey != rowKey) {
                currentRowKey = rowKey;

                unionRowIndexList.add(Integer.valueOf(rowIndex));

                String title = defaultTaKind ? "Целевой прием" : row.targetAdmissionKind == null ? ((EnrollmentDistributionEnvironmentNode.CompetitionKindNode.CompetitionKindRow) competitionKindMap.get(row.competitionKind == null ? "5" : row.competitionKind)).title : new StringBuilder().append("Целевой прием – ").append(((EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow) targetAdmissionKindMap.get(row.targetAdmissionKind)).shortTitle).toString();

                tableRowList.add(new String[]{null, title});

                rowIndex++;

                number = 1;
            }

            List rowTitleList = new ArrayList();

            rowTitleList.add(Integer.toString(number++));
            rowTitleList.add(row.requestNumber);
            rowTitleList.add(row.fio);
            rowTitleList.add(row.finalMark);

            rowTitleList.add(row.documentState == null ? null : ((EnrollmentDistributionEnvironmentNode.Row) documentStateMap.get(row.documentState)).title);


            List priorityList = new ArrayList();
            for (Long id : row.priorityIds)
                priorityList.add(printOKSO((EnrollmentDirection) directionMap.get(id.toString())));

            rowTitleList.add(StringUtils.join(priorityList, ", "));

            rowTitleList.add(row.enrollmentState == null ? null : (printOKSO((EnrollmentDirection) directionMap.get(row.enrollmentDirection))));

            tableRowList.add((String[]) rowTitleList.toArray(new String[rowTitleList.size()]));

            rowIndex++;

        }

    }

    private void fillTitleMap(EnrollmentDistributionEnvironmentNode env, Map<String, EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow> targetAdmissionKindMap, Map<String, EnrollmentDistributionEnvironmentNode.CompetitionKindNode.CompetitionKindRow> competitionKindMap, Map<String, EnrollmentDistributionEnvironmentNode.BenefitNode.BenefitRow> benefitMap, Map<String, EnrollmentDistributionEnvironmentNode.Row> documentStateMap, Map<String, EnrollmentDistributionEnvironmentNode.Row> enrollmentStateMap)
    {
        for (EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow row : env.targetAdmissionKind.row) {
            targetAdmissionKindMap.put(row.id, row);
        }
        for (EnrollmentDistributionEnvironmentNode.CompetitionKindNode.CompetitionKindRow row : env.competitionKind.row) {
            competitionKindMap.put(row.id, row);
        }
        for (EnrollmentDistributionEnvironmentNode.BenefitNode.BenefitRow row : env.benefit.row) {
            benefitMap.put(row.id, row);
        }
        for (EnrollmentDistributionEnvironmentNode.Row row : env.documentState.row) {
            documentStateMap.put(row.id, row);
        }
        for (EnrollmentDistributionEnvironmentNode.Row row : env.enrollmentState.row)
            enrollmentStateMap.put(row.id, row);
    }

    public String printOKSO(EnrollmentDirection enrollmentDirection) {
        //Код ОКСО
        EducationLevels educationLevel = enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();

        //Два правых символа после точки
        String qualificationCode = enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getQualification().getCode();

        boolean isFullOKSO = "050100.62".equals(educationLevel.getInheritedOksoPrefix()) || "68".equals(qualificationCode);

        String result = isFullOKSO ? educationLevel.getDisplayableTitle() : educationLevel.getInheritedOksoPrefix();

        if (getOKSOprefixes().contains(educationLevel.getInheritedOksoPrefix())) {
            //Сокращенное название форм. подразделения
            String formativeOrgUnitShortTitle = enrollmentDirection.getEducationOrgUnit().getFormativeOrgUnit().getShortTitle();

            new StringBuilder().append(result).append(" (").append(formativeOrgUnitShortTitle).append(")");
        }

        return result;

    }


    protected void fillTableDataNarfu(RtfDocument document, final int grayColorIndex,
                                      final int disciplineColumn, List<String[]> tableData,
                                      final Set<Integer> unionRowIndexList, final DistributionRow distributionRow)
    {

        RtfTableModifier tm = new RtfTableModifier();

        tm.put("T", (String[][]) tableData.toArray(new String[tableData.size()][]));
        tm.put("T", new RtfRowIntercepterBase() {
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                if (unionRowIndexList.contains(Integer.valueOf(rowIndex)))
                    return new RtfString().boldBegin().content(value).boldEnd().toList();
                return null;
            }

            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                for (Integer rowIndex : unionRowIndexList) {
                    RtfRow row = (RtfRow) newRowList.get(rowIndex.intValue() + startIndex);
                    RtfUtil.unitAllCell(row, 1);
                    ((RtfCell) row.getCellList().get(0)).setBackgroundColorIndex(grayColorIndex);
                }
            }
        });
        tm.modify(document);
    }


    public List<String> getOKSOprefixes() {
        return Arrays.asList("050100.62", "230700.62", "010400.62", "151000.62", "050400.62", "030900.62", "080200.62", "080100.62", "190631.51");
    }

}
