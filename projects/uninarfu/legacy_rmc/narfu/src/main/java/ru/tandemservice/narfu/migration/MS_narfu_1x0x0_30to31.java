package ru.tandemservice.narfu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_narfu_1x0x0_30to31 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.13"),
                        new ScriptDependency("org.tandemframework.shared", "1.3.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.3.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность ctrPaymentPromiceNARFU

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("ctr_pr_payment_t_narfu",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("base_id", DBType.LONG).setNullable(false),
                                      new DBColumn("year_id", DBType.LONG),
                                      new DBColumn("part_id", DBType.LONG)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("ctrPaymentPromiceNARFU");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность yearPartStartEndDate

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("yearpartstartenddate_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("educationyear_id", DBType.LONG).setNullable(false),
                                      new DBColumn("part_id", DBType.LONG).setNullable(false),
                                      new DBColumn("startdate_p", DBType.DATE).setNullable(false),
                                      new DBColumn("enddate_p", DBType.DATE).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("yearPartStartEndDate");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eppCtrAgreementTemplateDataMotherCapital

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("epp_ctr_ctmpldt_mcapital_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("course_id", DBType.LONG).setNullable(false),
                                      new DBColumn("eduyear_id", DBType.LONG).setNullable(false),
                                      new DBColumn("requestdate_p", DBType.DATE),
                                      new DBColumn("paymentpart_p", DBType.INTEGER).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("eppCtrAgreementTemplateDataMotherCapital");

        }


    }
}