/*$Id$*/
package ru.tandemservice.narfu.base.ext.Person.ui.NextOfKinAddEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.person.base.bo.Person.ui.NextOfKinAddEdit.PersonNextOfKinAddEditUI;
import ru.tandemservice.narfu.entity.PersonNextOfKinNARFU;

/**
 * @author DMITRY KNYAZEV
 * @since 11.02.2015
 */
public class PersonNextOfKinAddEditUIExt extends UIAddon {

    private PersonNextOfKinNARFU _personNextOfKinNARFU;

    private PersonNextOfKinAddEditUI parentPresenter;

    @Override
    public void onComponentRefresh()
    {
        parentPresenter = getPresenter();
        _personNextOfKinNARFU = DataAccessServices.dao().get(PersonNextOfKinNARFU.class, PersonNextOfKinNARFU.L_BASE, parentPresenter.getNextOfKin());

        if (_personNextOfKinNARFU == null)
        {
            _personNextOfKinNARFU = new PersonNextOfKinNARFU();
            _personNextOfKinNARFU.setBase(parentPresenter.getNextOfKin());
        }
    }

    public PersonNextOfKinAddEditUIExt(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public PersonNextOfKinNARFU getPersonNextOfKinNARFU()
    {
        return _personNextOfKinNARFU;
    }

    public void setPersonNextOfKinNARFU(PersonNextOfKinNARFU personNextOfKinNARFU)
    {
        this._personNextOfKinNARFU = personNextOfKinNARFU;
    }
}
