package ru.tandemservice.narfu.uniec.entity.report;

import ru.tandemservice.narfu.uniec.entity.report.gen.ExcludedAspirantsReportGen;

/**
 * Сведения об аспирантах - гражданах РФ, обучавшихся за счет средств федерального бюджета и выбывших до окончания срока обучения»
 */
public class ExcludedAspirantsReport extends ExcludedAspirantsReportGen
{
}