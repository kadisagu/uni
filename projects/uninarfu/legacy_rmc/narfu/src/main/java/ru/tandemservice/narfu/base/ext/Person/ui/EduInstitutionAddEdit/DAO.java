package ru.tandemservice.narfu.base.ext.Person.ui.EduInstitutionAddEdit;

import ru.tandemservice.narfu.base.ext.Person.ui.Tab.PersonNarfuUtil;

public class DAO extends org.tandemframework.shared.person.base.bo.Person.ui.EduInstitutionAddEdit.DAO {

    @Override
    public void prepare(org.tandemframework.shared.person.base.bo.Person.ui.EduInstitutionAddEdit.Model model) {
        super.prepare(model);
        Model myModel = (Model) model;
        myModel.setPersonEduInstitutionNARFU(PersonNarfuUtil.getPersonEduinstitution(model.getPersonEduInstitution()));
    }

    @Override
    public void update(org.tandemframework.shared.person.base.bo.Person.ui.EduInstitutionAddEdit.Model model) {
        super.update(model);
        Model myModel = (Model) model;
        this.saveOrUpdate(myModel.getPersonEduInstitutionNARFU());
    }
}
