package ru.tandemservice.narfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.narfu.entity.StateExamSubjectPassScoreExt;
import ru.tandemservice.uniec.entity.settings.StateExamSubjectPassScore;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение сущности Зачетный балл по предмету ЕГЭ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StateExamSubjectPassScoreExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.narfu.entity.StateExamSubjectPassScoreExt";
    public static final String ENTITY_NAME = "stateExamSubjectPassScoreExt";
    public static final int VERSION_HASH = 952441846;
    private static IEntityMeta ENTITY_META;

    public static final String L_BASE = "base";
    public static final String P_MARK_PREV = "markPrev";
    public static final String P_MARK_PREV2 = "markPrev2";

    private StateExamSubjectPassScore _base;     // Зачетный балл по предмету ЕГЭ
    private int _markPrev;     // Зачетный балл за прошлый год
    private int _markPrev2;     // Зачетный балл за позапрошлый год

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Зачетный балл по предмету ЕГЭ. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public StateExamSubjectPassScore getBase()
    {
        return _base;
    }

    /**
     * @param base Зачетный балл по предмету ЕГЭ. Свойство не может быть null и должно быть уникальным.
     */
    public void setBase(StateExamSubjectPassScore base)
    {
        dirty(_base, base);
        _base = base;
    }

    /**
     * @return Зачетный балл за прошлый год. Свойство не может быть null.
     */
    @NotNull
    public int getMarkPrev()
    {
        return _markPrev;
    }

    /**
     * @param markPrev Зачетный балл за прошлый год. Свойство не может быть null.
     */
    public void setMarkPrev(int markPrev)
    {
        dirty(_markPrev, markPrev);
        _markPrev = markPrev;
    }

    /**
     * @return Зачетный балл за позапрошлый год. Свойство не может быть null.
     */
    @NotNull
    public int getMarkPrev2()
    {
        return _markPrev2;
    }

    /**
     * @param markPrev2 Зачетный балл за позапрошлый год. Свойство не может быть null.
     */
    public void setMarkPrev2(int markPrev2)
    {
        dirty(_markPrev2, markPrev2);
        _markPrev2 = markPrev2;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StateExamSubjectPassScoreExtGen)
        {
            setBase(((StateExamSubjectPassScoreExt)another).getBase());
            setMarkPrev(((StateExamSubjectPassScoreExt)another).getMarkPrev());
            setMarkPrev2(((StateExamSubjectPassScoreExt)another).getMarkPrev2());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StateExamSubjectPassScoreExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StateExamSubjectPassScoreExt.class;
        }

        public T newInstance()
        {
            return (T) new StateExamSubjectPassScoreExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "base":
                    return obj.getBase();
                case "markPrev":
                    return obj.getMarkPrev();
                case "markPrev2":
                    return obj.getMarkPrev2();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "base":
                    obj.setBase((StateExamSubjectPassScore) value);
                    return;
                case "markPrev":
                    obj.setMarkPrev((Integer) value);
                    return;
                case "markPrev2":
                    obj.setMarkPrev2((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "base":
                        return true;
                case "markPrev":
                        return true;
                case "markPrev2":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "base":
                    return true;
                case "markPrev":
                    return true;
                case "markPrev2":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "base":
                    return StateExamSubjectPassScore.class;
                case "markPrev":
                    return Integer.class;
                case "markPrev2":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StateExamSubjectPassScoreExt> _dslPath = new Path<StateExamSubjectPassScoreExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StateExamSubjectPassScoreExt");
    }
            

    /**
     * @return Зачетный балл по предмету ЕГЭ. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.narfu.entity.StateExamSubjectPassScoreExt#getBase()
     */
    public static StateExamSubjectPassScore.Path<StateExamSubjectPassScore> base()
    {
        return _dslPath.base();
    }

    /**
     * @return Зачетный балл за прошлый год. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.StateExamSubjectPassScoreExt#getMarkPrev()
     */
    public static PropertyPath<Integer> markPrev()
    {
        return _dslPath.markPrev();
    }

    /**
     * @return Зачетный балл за позапрошлый год. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.StateExamSubjectPassScoreExt#getMarkPrev2()
     */
    public static PropertyPath<Integer> markPrev2()
    {
        return _dslPath.markPrev2();
    }

    public static class Path<E extends StateExamSubjectPassScoreExt> extends EntityPath<E>
    {
        private StateExamSubjectPassScore.Path<StateExamSubjectPassScore> _base;
        private PropertyPath<Integer> _markPrev;
        private PropertyPath<Integer> _markPrev2;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Зачетный балл по предмету ЕГЭ. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.narfu.entity.StateExamSubjectPassScoreExt#getBase()
     */
        public StateExamSubjectPassScore.Path<StateExamSubjectPassScore> base()
        {
            if(_base == null )
                _base = new StateExamSubjectPassScore.Path<StateExamSubjectPassScore>(L_BASE, this);
            return _base;
        }

    /**
     * @return Зачетный балл за прошлый год. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.StateExamSubjectPassScoreExt#getMarkPrev()
     */
        public PropertyPath<Integer> markPrev()
        {
            if(_markPrev == null )
                _markPrev = new PropertyPath<Integer>(StateExamSubjectPassScoreExtGen.P_MARK_PREV, this);
            return _markPrev;
        }

    /**
     * @return Зачетный балл за позапрошлый год. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.StateExamSubjectPassScoreExt#getMarkPrev2()
     */
        public PropertyPath<Integer> markPrev2()
        {
            if(_markPrev2 == null )
                _markPrev2 = new PropertyPath<Integer>(StateExamSubjectPassScoreExtGen.P_MARK_PREV2, this);
            return _markPrev2;
        }

        public Class getEntityClass()
        {
            return StateExamSubjectPassScoreExt.class;
        }

        public String getEntityName()
        {
            return "stateExamSubjectPassScoreExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
