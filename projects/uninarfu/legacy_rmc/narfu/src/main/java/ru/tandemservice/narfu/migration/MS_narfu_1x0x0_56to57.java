/* $Id$ */
package ru.tandemservice.narfu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * @author Ekaterina Zvereva
 * @since 06.10.2015
 */
public class MS_narfu_1x0x0_56to57 extends IndependentMigrationScript
{

//    @Override
//    public ScriptDependency[] getBoundaryDependencies()
//    {
//        return new ScriptDependency[]
//                {
//                        new ScriptDependency("org.tandemframework", "1.6.15"),
//                        new ScriptDependency("org.tandemframework.shared", "1.6.0"),
//                        new ScriptDependency("ru.tandemservice.uni.product", "2.6.0"),
//                        new ScriptDependency("ru.tandemservice.uni.project", "2.6.0")
//                };
//    }


    @Override
    public void run(DBTool tool) throws Exception
    {
        //модуль mailrmc

        if (tool.tableExists("mailrmc_t"))
        {
            tool.renameTable("mailrmc_t", "mailmessage_t");
            tool.entityCodes().rename("mailRmc", "mailMessage");
        }

        if (tool.tableExists("maillog_t"))
        {
            tool.renameColumn("maillog_t", "mailrmc_id", "mailmessage_id");
        }

        // модуль tutorrmc
        if (tool.tableExists("optionaldisciplinesubscribe_t"))
        {
            tool.dropTable("optionaldisciplinesubscribe_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);
            tool.entityCodes().delete("optionalDisciplineSubscribe");
        }


        if (tool.tableExists("optionaldiscipline_t"))
        {
            tool.dropTable("optionaldiscipline_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);
            tool.entityCodes().delete("optionalDiscipline");
        }


        //модуль unisakairmc
        //drop tables
        if (tool.tableExists("sakaientitytype_t"))
        {
            tool.dropTable("sakaientitytype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);
            tool.entityCodes().delete("sakaiEntityType");
        }

        if (tool.tableExists("sakaisendmailstate_t"))
        {
            tool.dropTable("sakaisendmailstate_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);
            tool.entityCodes().delete("sakaiSendMailState");
        }

        if (tool.tableExists("sakaimailtype_t"))
        {
            tool.dropTable("sakaimailtype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);
            tool.entityCodes().delete("sakaiMailType");
        }

        //rename tables
        if (tool.tableExists("unisakairmcsettings_t"))
        {
            tool.renameTable("unisakairmcsettings_t", "unisakaisettings_t");
            tool.entityCodes().rename("uniSakaiRmcSettings", "uniSakaiSettings");
            tool.renameColumn("unisakaisettings_t", "unisakairmcsettingsType_id", "unisakaisettingstype_id");
        }

        if (tool.tableExists("unisakairmcsettingstype_t"))
        {
            tool.renameTable("unisakairmcsettingstype_t", "unisakaisettingstype_t");
            tool.entityCodes().rename("uniSakaiRmcSettingsType", "uniSakaiSettingsType");

        }

        if (tool.tableExists("unisakairmcmarkimport_t"))
        {
            tool.renameTable("unisakairmcmarkimport_t", "unisakaimarkimport_t");
            tool.entityCodes().rename("uniSakaiRmcMarkImport", "uniSakaiMarkImport");

        }

        if (tool.tableExists("unisakairmctemplatedocument_t"))
        {
            tool.renameTable("unisakairmctemplatedocument_t", "unisakaitemplatedocument_t");
            tool.entityCodes().rename("uniSakaiRmcTemplateDocument", "uniSakaiTemplateDocument");

        }


        MigrationUtils.removeModuleFromVersion_s(tool, "tutorrmc");
        MigrationUtils.removeModuleFromVersion_s(tool, "unisakairmc");
        MigrationUtils.removeModuleFromVersion_s(tool, "mailrmc");


    }
}