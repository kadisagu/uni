package ru.tandemservice.narfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.narfu.entity.EntrantRequestNARFU;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Доп атрибуты заявления абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrantRequestNARFUGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.narfu.entity.EntrantRequestNARFU";
    public static final String ENTITY_NAME = "entrantRequestNARFU";
    public static final int VERSION_HASH = -661439895;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT_REQUEST = "entrantRequest";
    public static final String P_DATE_DOCUMENTS_TAKE_AWAY = "dateDocumentsTakeAway";

    private EntrantRequest _entrantRequest;     // Заявление абитуриента
    private Date _dateDocumentsTakeAway;     // Дата возврата

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Заявление абитуриента.
     */
    public EntrantRequest getEntrantRequest()
    {
        return _entrantRequest;
    }

    /**
     * @param entrantRequest Заявление абитуриента.
     */
    public void setEntrantRequest(EntrantRequest entrantRequest)
    {
        dirty(_entrantRequest, entrantRequest);
        _entrantRequest = entrantRequest;
    }

    /**
     * @return Дата возврата.
     */
    public Date getDateDocumentsTakeAway()
    {
        return _dateDocumentsTakeAway;
    }

    /**
     * @param dateDocumentsTakeAway Дата возврата.
     */
    public void setDateDocumentsTakeAway(Date dateDocumentsTakeAway)
    {
        dirty(_dateDocumentsTakeAway, dateDocumentsTakeAway);
        _dateDocumentsTakeAway = dateDocumentsTakeAway;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntrantRequestNARFUGen)
        {
            setEntrantRequest(((EntrantRequestNARFU)another).getEntrantRequest());
            setDateDocumentsTakeAway(((EntrantRequestNARFU)another).getDateDocumentsTakeAway());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrantRequestNARFUGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrantRequestNARFU.class;
        }

        public T newInstance()
        {
            return (T) new EntrantRequestNARFU();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrantRequest":
                    return obj.getEntrantRequest();
                case "dateDocumentsTakeAway":
                    return obj.getDateDocumentsTakeAway();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrantRequest":
                    obj.setEntrantRequest((EntrantRequest) value);
                    return;
                case "dateDocumentsTakeAway":
                    obj.setDateDocumentsTakeAway((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrantRequest":
                        return true;
                case "dateDocumentsTakeAway":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrantRequest":
                    return true;
                case "dateDocumentsTakeAway":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrantRequest":
                    return EntrantRequest.class;
                case "dateDocumentsTakeAway":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrantRequestNARFU> _dslPath = new Path<EntrantRequestNARFU>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrantRequestNARFU");
    }
            

    /**
     * @return Заявление абитуриента.
     * @see ru.tandemservice.narfu.entity.EntrantRequestNARFU#getEntrantRequest()
     */
    public static EntrantRequest.Path<EntrantRequest> entrantRequest()
    {
        return _dslPath.entrantRequest();
    }

    /**
     * @return Дата возврата.
     * @see ru.tandemservice.narfu.entity.EntrantRequestNARFU#getDateDocumentsTakeAway()
     */
    public static PropertyPath<Date> dateDocumentsTakeAway()
    {
        return _dslPath.dateDocumentsTakeAway();
    }

    public static class Path<E extends EntrantRequestNARFU> extends EntityPath<E>
    {
        private EntrantRequest.Path<EntrantRequest> _entrantRequest;
        private PropertyPath<Date> _dateDocumentsTakeAway;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Заявление абитуриента.
     * @see ru.tandemservice.narfu.entity.EntrantRequestNARFU#getEntrantRequest()
     */
        public EntrantRequest.Path<EntrantRequest> entrantRequest()
        {
            if(_entrantRequest == null )
                _entrantRequest = new EntrantRequest.Path<EntrantRequest>(L_ENTRANT_REQUEST, this);
            return _entrantRequest;
        }

    /**
     * @return Дата возврата.
     * @see ru.tandemservice.narfu.entity.EntrantRequestNARFU#getDateDocumentsTakeAway()
     */
        public PropertyPath<Date> dateDocumentsTakeAway()
        {
            if(_dateDocumentsTakeAway == null )
                _dateDocumentsTakeAway = new PropertyPath<Date>(EntrantRequestNARFUGen.P_DATE_DOCUMENTS_TAKE_AWAY, this);
            return _dateDocumentsTakeAway;
        }

        public Class getEntityClass()
        {
            return EntrantRequestNARFU.class;
        }

        public String getEntityName()
        {
            return "entrantRequestNARFU";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
