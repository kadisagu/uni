package ru.tandemservice.uniec.dao.print;

import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniec.entity.entrant.*;

import java.util.Date;
import java.util.List;

public interface IEntrantDocumentsInventoryAndReceiptPrintBean {
    public String getSecretary(OrgUnit formativOu);

    public String getTechSecretary();

    public RequestedEnrollmentDirection getFirstRequestDirection(EntrantRequest request);

    public List<ExamPassDiscipline> getDisciplineList(Entrant entrant);

    public String getDocumentInfo(EntrantEnrollmentDocument doc);

    public Date getExamPassDisciplineDate(ExamPassDiscipline exapPassDiscipline);

    public Date getExamPassDate(ExamPassDiscipline examPassDiscipline);

    public List<String> getAchievements(Entrant entrant);

    public void extendTable(RtfTableModifier tm, Entrant entrant);

    public byte[] getAspirantTemplate(EntrantRequest request, byte[] template);
}
