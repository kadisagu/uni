package ru.tandemservice.narfu.component.reports.StudentbookReport;

import org.tandemframework.core.component.State;
import org.tandemframework.sec.runtime.SecurityRuntime;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.Date;
import java.util.List;

@State(keys = {"orgUnitId"}, bindings = {"orgUnitId"})
public class Model {

    private Long orgUnitId;
    private OrgUnit orgUnit;
    private CommonPostfixPermissionModel secModel;

    private boolean developFormActive;
    private ISelectModel developFormModel;
    private List<DevelopForm> developFormList;

    private boolean educationLevelsActive;
    private ISelectModel educationLevelsModel;
    private List<EducationLevels> educationLevelsList;

    private boolean courseActive;
    private ISelectModel courseModel;
    private List<Course> courseList;

    private boolean groupActive;
    private ISelectModel groupModel;
    private List<Group> groupList;

    private boolean studentActive;
    private ISelectModel studentModel;
    private List<Student> studentList;

    private Date date;

    private boolean showDate;

    public String getViewKey() {
        return null == getOrgUnit() ? "viewStudentbookReport" : getSecModel().getPermission("orgUnit_viewStudentbookReport");
    }

    public Object getSecuredObject() {
        return null == getOrgUnit() ? SecurityRuntime.getInstance().getCommonSecurityObject() : getOrgUnit();
    }

    public OrgUnit getOrgUnit() {
        return orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit) {
        this.orgUnit = orgUnit;
    }

    public CommonPostfixPermissionModel getSecModel() {
        return secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel) {
        this.secModel = secModel;
    }

    public boolean isDevelopFormActive() {
        return developFormActive;
    }

    public void setDevelopFormActive(boolean developFormActive) {
        this.developFormActive = developFormActive;
    }

    public ISelectModel getDevelopFormModel() {
        return developFormModel;
    }

    public void setDevelopFormModel(ISelectModel developFormModel) {
        this.developFormModel = developFormModel;
    }

    public List<DevelopForm> getDevelopFormList() {
        return developFormList;
    }

    public void setDevelopFormList(List<DevelopForm> developFormList) {
        this.developFormList = developFormList;
    }

    public boolean isEducationLevelsActive() {
        return educationLevelsActive;
    }

    public void setEducationLevelsActive(boolean educationLevelsActive) {
        this.educationLevelsActive = educationLevelsActive;
    }

    public ISelectModel getEducationLevelsModel() {
        return educationLevelsModel;
    }

    public void setEducationLevelsModel(ISelectModel educationLevelsModel) {
        this.educationLevelsModel = educationLevelsModel;
    }

    public List<EducationLevels> getEducationLevelsList() {
        return educationLevelsList;
    }

    public void setEducationLevelsList(List<EducationLevels> educationLevelsList) {
        this.educationLevelsList = educationLevelsList;
    }

    public boolean isCourseActive() {
        return courseActive;
    }

    public void setCourseActive(boolean courseActive) {
        this.courseActive = courseActive;
    }

    public ISelectModel getCourseModel() {
        return courseModel;
    }

    public void setCourseModel(ISelectModel courseModel) {
        this.courseModel = courseModel;
    }

    public List<Course> getCourseList() {
        return courseList;
    }

    public void setCourseList(List<Course> courseList) {
        this.courseList = courseList;
    }

    public boolean isGroupActive() {
        return groupActive;
    }

    public void setGroupActive(boolean groupActive) {
        this.groupActive = groupActive;
    }

    public ISelectModel getGroupModel() {
        return groupModel;
    }

    public void setGroupModel(ISelectModel groupModel) {
        this.groupModel = groupModel;
    }

    public List<Group> getGroupList() {
        return groupList;
    }

    public void setGroupList(List<Group> groupList) {
        this.groupList = groupList;
    }

    public boolean isStudentActive() {
        return studentActive;
    }

    public void setStudentActive(boolean studentActive) {
        this.studentActive = studentActive;
    }

    public ISelectModel getStudentModel() {
        return studentModel;
    }

    public void setStudentModel(ISelectModel studentModel) {
        this.studentModel = studentModel;
    }

    public List<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isShowDate() {
        return showDate;
    }

    public void setShowDate(boolean showDate) {
        this.showDate = showDate;
    }

    public Long getOrgUnitId() {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId) {
        this.orgUnitId = orgUnitId;
    }

}
