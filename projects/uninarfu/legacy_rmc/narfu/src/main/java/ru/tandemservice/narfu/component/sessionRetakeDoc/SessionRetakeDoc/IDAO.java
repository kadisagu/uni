package ru.tandemservice.narfu.component.sessionRetakeDoc.SessionRetakeDoc;

import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {

    @Transactional
    public void doCreateDocuments(Model model);

}
