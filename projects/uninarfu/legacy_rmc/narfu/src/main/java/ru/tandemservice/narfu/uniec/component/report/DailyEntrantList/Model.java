package ru.tandemservice.narfu.uniec.component.report.DailyEntrantList;

import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;

import java.util.*;

public class Model implements MultiEnrollmentDirectionUtil.Model, IEnrollmentCampaignSelectModel {

    IPrincipalContext principalContext;
    private Date date;
    private byte[] content;
    private String fileName;

    private boolean formativeOrgUnitActive;
    private boolean compensationTypeActive;
    private boolean educationLevelsHighSchoolActive;
    private boolean developFormActive;
    private boolean developConditionActive;
    private boolean developPeriodActive;
    private Map<Long, List<RequestedEnrollmentDirection>> redMap = new HashMap<Long, List<RequestedEnrollmentDirection>>();
    private Map<Long, OrgUnit> formOrgUnit2Commission = new Hashtable<Long, OrgUnit>();

    private IDataSettings settings;
    private MultiEnrollmentDirectionUtil.Parameters parameters;

    private List<EnrollmentCampaign> enrollmentCampaignList;
    private EnrollmentCampaign enrollmentCampaign;

    private ISelectModel formativeOrgUnitListModel;
    private List<OrgUnit> formativeOrgUnitList;

    private ISelectModel compensationTypeModel;
    private CompensationType compensationType;

    private ISelectModel educationLevelsHighSchoolListModel;
    private List<EducationLevelsHighSchool> educationLevelsHighSchoolList;

    private ISelectModel developFormListModel;
    private List<DevelopForm> developFormList;

    private ISelectModel developConditionListModel;
    private List<DevelopCondition> developConditionList;

    private ISelectModel developPeriodListModel;
    private List<DevelopPeriod> developPeriodList;

    public IPrincipalContext getPrincipalContext() {
        return principalContext;
    }

    public void setPrincipalContext(IPrincipalContext principalContext) {
        this.principalContext = principalContext;
    }

    public Map<Long, List<RequestedEnrollmentDirection>> getRedMap() {
        return redMap;
    }

    public void setRedMap(Map<Long, List<RequestedEnrollmentDirection>> redMap) {
        this.redMap = redMap;
    }

    public Map<Long, OrgUnit> getFormOrgUnit2Commission() {
        return formOrgUnit2Commission;
    }

    public void setFormOrgUnit2Commission(Map<Long, OrgUnit> formOrgUnit2Commission) {
        this.formOrgUnit2Commission = formOrgUnit2Commission;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public boolean isFormativeOrgUnitActive() {
        return formativeOrgUnitActive;
    }

    public void setFormativeOrgUnitActive(boolean formativeOrgUnitActive) {
        this.formativeOrgUnitActive = formativeOrgUnitActive;
    }

    public boolean isCompensationTypeActive() {
        return compensationTypeActive;
    }

    public void setCompensationTypeActive(boolean compensationTypeActive) {
        this.compensationTypeActive = compensationTypeActive;
    }

    public boolean isEducationLevelsHighSchoolActive() {
        return educationLevelsHighSchoolActive;
    }

    public void setEducationLevelsHighSchoolActive(
            boolean educationLevelsHighSchoolActive)
    {
        this.educationLevelsHighSchoolActive = educationLevelsHighSchoolActive;
    }

    public boolean isDevelopFormActive() {
        return developFormActive;
    }

    public void setDevelopFormActive(boolean developFormActive) {
        this.developFormActive = developFormActive;
    }

    public boolean isDevelopConditionActive() {
        return developConditionActive;
    }

    public void setDevelopConditionActive(boolean developConditionActive) {
        this.developConditionActive = developConditionActive;
    }

    public boolean isDevelopPeriodActive() {
        return developPeriodActive;
    }

    public void setDevelopPeriodActive(boolean developPeriodActive) {
        this.developPeriodActive = developPeriodActive;
    }

    public ISelectModel getFormativeOrgUnitListModel() {
        return formativeOrgUnitListModel;
    }

    public void setFormativeOrgUnitListModel(ISelectModel formativeOrgUnitListModel) {
        this.formativeOrgUnitListModel = formativeOrgUnitListModel;
    }

    public List<OrgUnit> getFormativeOrgUnitList() {
        return formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList) {
        this.formativeOrgUnitList = formativeOrgUnitList;
    }

    public ISelectModel getCompensationTypeModel() {
        return compensationTypeModel;
    }

    public void setCompensationTypeModel(ISelectModel compensationTypeModel) {
        this.compensationTypeModel = compensationTypeModel;
    }

    public CompensationType getCompensationType() {
        return compensationType;
    }

    public void setCompensationType(CompensationType compensationType) {
        this.compensationType = compensationType;
    }

    public ISelectModel getEducationLevelsHighSchoolListModel() {
        return educationLevelsHighSchoolListModel;
    }

    public void setEducationLevelsHighSchoolListModel(
            ISelectModel educationLevelsHighSchoolListModel)
    {
        this.educationLevelsHighSchoolListModel = educationLevelsHighSchoolListModel;
    }

    public List<EducationLevelsHighSchool> getEducationLevelsHighSchoolList() {
        return educationLevelsHighSchoolList;
    }

    public void setEducationLevelsHighSchoolList(
            List<EducationLevelsHighSchool> educationLevelsHighSchoolList)
    {
        this.educationLevelsHighSchoolList = educationLevelsHighSchoolList;
    }

    public ISelectModel getDevelopFormListModel() {
        return developFormListModel;
    }

    public void setDevelopFormListModel(ISelectModel developFormListModel) {
        this.developFormListModel = developFormListModel;
    }

    public List<DevelopForm> getDevelopFormList() {
        return developFormList;
    }

    public void setDevelopFormList(List<DevelopForm> developFormList) {
        this.developFormList = developFormList;
    }

    public ISelectModel getDevelopConditionListModel() {
        return developConditionListModel;
    }

    public void setDevelopConditionListModel(ISelectModel developConditionListModel) {
        this.developConditionListModel = developConditionListModel;
    }

    public List<DevelopCondition> getDevelopConditionList() {
        return developConditionList;
    }

    public void setDevelopConditionList(List<DevelopCondition> developConditionList) {
        this.developConditionList = developConditionList;
    }

    public ISelectModel getDevelopPeriodListModel() {
        return developPeriodListModel;
    }

    public void setDevelopPeriodListModel(ISelectModel developPeriodListModel) {
        this.developPeriodListModel = developPeriodListModel;
    }

    public List<DevelopPeriod> getDevelopPeriodList() {
        return this.developPeriodList;
    }

    public void setDevelopPeriodList(List<DevelopPeriod> developPeriodList) {
        this.developPeriodList = developPeriodList;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public MultiEnrollmentDirectionUtil.Parameters getParameters() {
        return this.parameters;
    }

    public void setParameters(MultiEnrollmentDirectionUtil.Parameters parameters) {
        this.parameters = parameters;
    }

    @Override
    public List<OrgUnit> getSelectedFormativeOrgUnitList() {
        return isFormativeOrgUnitActive() ? getFormativeOrgUnitList() : null;
    }

    @Override
    public List<EducationLevelsHighSchool> getSelectedEducationLevelHighSchoolList() {
        return isEducationLevelsHighSchoolActive() ? getEducationLevelsHighSchoolList() : null;
    }

    @Override
    public List<DevelopForm> getSelectedDevelopFormList() {
        return isDevelopFormActive() ? getDevelopFormList() : null;
    }

    @Override
    public List<DevelopCondition> getSelectedDevelopConditionList() {
        return isDevelopConditionActive() ? getDevelopConditionList() : null;
    }

    @Override
    public List<DevelopPeriod> getSelectedDevelopPeriodList() {
        return isDevelopPeriodActive() ? getDevelopPeriodList() : null;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList() {
        return this.enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList) {
        this.enrollmentCampaignList = enrollmentCampaignList;
    }

    @Override
    public IDataSettings getSettings() {
        return this.settings;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign() {
        return this.enrollmentCampaign;
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign) {
        this.enrollmentCampaign = enrollmentCampaign;
    }

    @Override
    public List<EnrollmentCampaign> getSelectedEnrollmentCampaignList() {
        return MultiEnrollmentDirectionUtil.getSingletonList(this.enrollmentCampaign);
    }

    @Override
    public List<OrgUnit> getSelectedTerritorialOrgUnitList() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<DevelopTech> getSelectedDevelopTechList() {
        // TODO Auto-generated method stub
        return null;
    }

}
