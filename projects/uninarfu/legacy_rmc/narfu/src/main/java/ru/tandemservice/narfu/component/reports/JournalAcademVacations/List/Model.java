package ru.tandemservice.narfu.component.reports.JournalAcademVacations.List;

import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.sec.runtime.SecurityRuntime;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.narfu.entity.NarfuReportJournalAcademVacations;

@State(keys = "orgUnitId", bindings = "orgUnitId")
public class Model {

    private Long orgUnitId;
    private OrgUnit orgUnit;
    private CommonPostfixPermissionModel secModel;
    private DynamicListDataSource<NarfuReportJournalAcademVacations> dataSource;


    public Long getOrgUnitId() {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId) {
        this.orgUnitId = orgUnitId;
    }

    public OrgUnit getOrgUnit() {
        return orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit) {
        this.orgUnit = orgUnit;
    }

    public DynamicListDataSource<NarfuReportJournalAcademVacations> getDataSource() {
        return dataSource;
    }

    public void setDataSource(
            DynamicListDataSource<NarfuReportJournalAcademVacations> dataSource)
    {
        this.dataSource = dataSource;
    }

    public CommonPostfixPermissionModel getSecModel() {
        return secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel) {
        this.secModel = secModel;
    }

    public Object getSecuredObject() {
        return null == getOrgUnit() ? SecurityRuntime.getInstance().getCommonSecurityObject() : getOrgUnit();
    }

    public String getViewKey() {

        return null != getOrgUnit() ? getSecModel().getPermission("orgUnit_viewReportJournalAcademVacations") : "academVacationsJournal";
    }

    public String getAddKey()
    {
        return null == getOrgUnit() ? "addGlobalStoredReport" : getSecModel().getPermission("addStorableReport");
    }

    public String getPrintKey()
    {
        return null == getOrgUnit() ? "printGlobalStoredReport" : getSecModel().getPermission("printStorableReport");
    }

}
