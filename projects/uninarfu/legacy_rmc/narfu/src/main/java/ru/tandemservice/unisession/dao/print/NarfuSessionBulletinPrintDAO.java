package ru.tandemservice.unisession.dao.print;

import com.google.common.collect.Iterables;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.narfu.entity.catalog.codes.UnisessionCommonTemplateCodes;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeFCACodes;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionType;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionTypeRow;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowed;
import ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowedForBulletin;
import ru.tandemservice.unisession.entity.allowance.gen.SessionStudentNotAllowedForBulletinGen;
import ru.tandemservice.unisession.entity.allowance.gen.SessionStudentNotAllowedGen;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.UnisessionCommonTemplate;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionProjectTheme;
import ru.tandemservice.unisession.entity.document.SessionSlotRatingData;
import ru.tandemservice.unisession.entity.document.gen.SessionBulletinDocumentGen;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionMarkRatingData;

import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class NarfuSessionBulletinPrintDAO extends ru.tandemservice.unisession.print.SessionBulletinPrintDAO implements INarfuSessionBulletinPrintDAO
{

    protected void initEppGroupData_(Map<SessionBulletinDocument, BulletinPrintInfo> bulletinMap)
    {
        final Map<EppRealEduGroup4ActionType, BulletinPrintInfo> groupMap = new HashMap<>();
        for (final Entry<SessionBulletinDocument, BulletinPrintInfo> entry : bulletinMap.entrySet())
        {
            groupMap.put(entry.getKey().getGroup(), entry.getValue());
        }
        for (List<EppRealEduGroup4ActionType> elements : Iterables.partition(groupMap.keySet(), DQL.MAX_VALUES_ROW_NUMBER))
        {
            final DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(EppRealEduGroup4ActionTypeRow.class, "rel")
                    .fetchPath(DQLJoinType.inner, EppRealEduGroup4ActionTypeRow.studentWpePart().fromAlias("rel"), "st")
                    .column(property("rel"))
                    .where(in(property(EppRealEduGroup4ActionTypeRow.group().fromAlias("rel")), elements))
                    .where(isNull(EppRealEduGroup4ActionTypeRow.removalDate().fromAlias("rel")));
            List<EppRealEduGroup4ActionTypeRow> list = getList(dql);
            list.forEach(rel -> {
                BulletinPrintRow row = groupMap.get(rel.getGroup()).getContentMap().get(rel.getStudentWpePart());
                if (null != row)
                    row.setEduGroupRow(rel);
            });
        }
    }

    protected void initContentData_(final Map<SessionBulletinDocument, BulletinPrintInfo> bulletinMap)
    {
        for (List<SessionBulletinDocument> elements : Iterables.partition(bulletinMap.keySet(), DQL.MAX_VALUES_ROW_NUMBER))
        {
            final DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(SessionDocumentSlot.class, "slot")
                    .joinEntity("slot", DQLJoinType.left, SessionMark.class, "mark",
                                eq(property(SessionMark.slot().id().fromAlias("mark")), property("slot.id")))
                    .joinEntity("slot", DQLJoinType.left, SessionProjectTheme.class, "theme",
                                eq(property(SessionProjectTheme.slot().fromAlias("theme")), property("slot")))
                    .joinEntity("slot", DQLJoinType.left, SessionSlotRatingData.class, "slotR",
                                eq(property(SessionSlotRatingData.slot().fromAlias("slotR")), property("slot")))
                    .joinEntity("mark", DQLJoinType.left, SessionMarkRatingData.class, "markR",
                                eq(property(SessionMarkRatingData.mark().fromAlias("markR")), property("mark")))
                    .joinPath(DQLJoinType.inner, SessionDocumentSlot.actualStudent().person().identityCard().fromAlias("slot"), "idc")
                    .column(property("slot"))
                    .column(property("mark"))
                    .column(property("theme"))
                    .column(property("slotR"))
                    .column(property("markR"))
                    .where(in(property(SessionDocumentSlot.document().fromAlias("slot")), elements))
                    .order(property("idc", IdentityCard.lastName()))
                    .order(property("idc", IdentityCard.firstName()))
                    .order(property("idc", IdentityCard.middleName()))
                    .order(property(SessionDocumentSlot.actualStudent().educationOrgUnit().educationLevelHighSchool().title().fromAlias("slot")));

            List<Object[]> list = getList(dql);
            for (final Object[] row : list)
            {
                final SessionDocumentSlot slot = (SessionDocumentSlot) row[0];
                final SessionMark mark = (SessionMark) row[1];
                final SessionProjectTheme theme = (SessionProjectTheme) row[2];
                final SessionSlotRatingData slotR = (SessionSlotRatingData) row[3];
                final SessionMarkRatingData markR = (SessionMarkRatingData) row[4];
                final SessionBulletinDocument bulletin = (SessionBulletinDocument) slot.getDocument();
                bulletinMap.get(bulletin).getContentMap().put(slot.getStudentWpeCAction(), new BulletinPrintRow(slot, mark, theme, slotR, markR));
            }
        }
    }

    protected void initCommissionData_(final Map<SessionBulletinDocument, BulletinPrintInfo> bulletinMap)
    {
        for (List<SessionBulletinDocument> elements : Iterables.partition(bulletinMap.keySet(), DQL.MAX_VALUES_ROW_NUMBER))
        {

            DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(SessionComissionPps.class, "rel")
                    .column(SessionComissionPps.pps().fromAlias("rel").s())
                    .joinPath(DQLJoinType.inner, SessionComissionPps.pps().person().identityCard().fromAlias("rel").s(), "idc")
                    .joinEntity("rel", DQLJoinType.inner, SessionBulletinDocument.class, "bulletin", eq(property(SessionBulletinDocument.commission().id().fromAlias("bulletin")), property(SessionComissionPps.commission().id().fromAlias("rel"))))
                    .column(property("bulletin"))
                    .where(in(property("bulletin"), elements))
                    .order(property("idc", IdentityCard.lastName()))
                    .order(property("idc", IdentityCard.firstName()))
                    .order(property("idc", IdentityCard.middleName()));

            List<Object[]> list = getList(dql);
            list.forEach(row -> {
                SessionBulletinDocument bulletin = (SessionBulletinDocument) row[1];
                PpsEntry ppsEntry = (PpsEntry) row[0];
                bulletinMap.get(bulletin).getCommission().add(ppsEntry);
            });

            dql = new DQLSelectBuilder()
                    .fromEntity(SessionComissionPps.class, "rel")
                    .joinPath(DQLJoinType.inner, SessionComissionPps.pps().person().identityCard().fromAlias("rel").s(), "idc")
                    .joinEntity("rel", DQLJoinType.inner, SessionDocumentSlot.class, "slot", eq(property(SessionDocumentSlot.commission().id().fromAlias("slot")), property(SessionComissionPps.commission().id().fromAlias("rel"))))
                    .where(in(property(SessionDocumentSlot.document().fromAlias("slot")), elements))
                    .column(property(SessionComissionPps.pps().fromAlias("rel")))
                    .column(property(SessionDocumentSlot.document().fromAlias("slot")))
                    .column(property(SessionDocumentSlot.studentWpeCAction().fromAlias("slot")))
                    .order(property("idc", IdentityCard.lastName()))
                    .order(property("idc", IdentityCard.firstName()))
                    .order(property("idc", IdentityCard.middleName()));

            list = getList(dql);
            list.forEach(row -> {
                PpsEntry ppsEntry = (PpsEntry) row[0];
                SessionBulletinDocument bulletin = (SessionBulletinDocument) row[1];
                EppStudentWpeCAction action = (EppStudentWpeCAction) row[2];
                BulletinPrintInfo bulletinInfo = bulletinMap.get(bulletin);
                BulletinPrintRow bulletinRow = bulletinInfo.getContentMap().get(action);
                bulletinRow.getCommission().add(ppsEntry);
            });
        }
    }

    protected Map<SessionBulletinDocument, BulletinPrintInfo> prepareData(Collection<Long> bulletinIds)
    {
        Map<SessionBulletinDocument, BulletinPrintInfo> result = new LinkedHashMap<>();
        getList(SessionBulletinDocument.class, SessionBulletinDocument.id(), bulletinIds, SessionBulletinDocument.P_NUMBER)
                .forEach(bulletin -> result.put(bulletin, new BulletinPrintInfo(bulletin)));

        initEppGroupData_(result);
        initContentData_(result);
        initCommissionData_(result);

        return result;
    }


    @Override
    public ReportFile printBulletinListNarfu(Collection<Long> bulletinIds)
    {
        Map<SessionBulletinDocument, BulletinPrintInfo> bulletinMap = prepareData(bulletinIds);
        if (bulletinMap.isEmpty())
            throw new ApplicationException("Нет ведомостей для печати.");

        if (bulletinMap.keySet().size() > 1 && bulletinMap.keySet().stream().anyMatch(SessionBulletinDocument::isClosed))
            throw new ApplicationException("Массовая печать закрытых ведомостей невозможна. Выберите только открытые ведомости.");

        RtfDocument resultDocument = null;
        for (Entry<SessionBulletinDocument, BulletinPrintInfo> entry : bulletinMap.entrySet())
        {
            BulletinPrintInfo printInfo = entry.getValue();

            RtfDocument doc;
            String caType = printInfo.getBulletin().getGroup().getType().getCode();
            if (caType.equals(EppGroupTypeFCACodes.CONTROL_ACTION_COURSE_PROJECT) || caType.equals(EppGroupTypeFCACodes.CONTROL_ACTION_COURSE_WORK))
                doc = printCourseWorkBulletin(printInfo);
            else
                doc = printBulletin(entry.getValue());

            if (resultDocument == null)
                resultDocument = doc;
            else
                addNewDoc(resultDocument, doc);
        }
        ReportFile result = new ReportFile();
        result.setZip(false);
        result.setName("Ведомости.rtf");

        result.setContent(RtfUtil.toByteArray(resultDocument));

        return result;
    }

    private RtfDocument printBulletin(BulletinPrintInfo printInfo)
    {
        RtfDocument template = new RtfReader().read(getCatalogItem(UnisessionCommonTemplate.class, UnisessionCommonTemplateCodes.NARFU_EXAM_BULLETIN).getContent());

        SessionBulletinDocument bulletin = printInfo.getBulletin();

        List<String> educationDirections = new ArrayList<>();
        List<String> groups = new ArrayList<>();
        List<String> courses = new ArrayList<>();
        List<String> terms = new ArrayList<>();
        printInfo.getContentMap().keySet().forEach(slot -> {
            EducationOrgUnit educationOrgUnit = slot.getStudentWpe().getStudent().getEducationOrgUnit();
            educationDirections.add(educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getTitleCodePrefix()
                                            + " " + educationOrgUnit.getEducationLevelHighSchool().getTitle());
            if (slot.getStudentWpe().getStudent().getGroup() != null)
                groups.add(slot.getStudentWpe().getStudent().getGroup().getTitle());
            courses.add(String.valueOf(slot.getStudentWpe().getCourse().getIntValue()));
            terms.add(String.valueOf(slot.getStudentWpe().getTerm().getIntValue()));
        });

        RtfInjectModifier textModifier = new RtfInjectModifier()
                .put("date", bulletin.getPerformDate() != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(bulletin.getPerformDate()) : "")
                .put("ouTitle", bulletin.getSessionObject().getOrgUnit().getTitle())
                .put("course", courses.stream().sorted().distinct().collect(Collectors.joining(", ")))
                .put("term", terms.stream().sorted().distinct().collect(Collectors.joining(", ")))
                .put("group", groups.stream().sorted().distinct().collect(Collectors.joining(", ")))
                .put("educationOrgUnitTitle", educationDirections.stream().sorted().distinct().collect(Collectors.joining(", ")))
                .put("registryElement", bulletin.getGroup().getActivityPart().getRegistryElement().getEducationElementTitle())
                .put("tutors", UniStringUtils.join(printInfo.getCommission(), PpsEntry.person().identityCard().fio().s(), ", "))
                .put("caType", bulletin.getGroup().getActionType().getTitle())
                .put("eduYear", bulletin.getSessionObject().getEducationYear().getTitle());

        OrgUnit orgUnit = bulletin.getSessionObject().getOrgUnit();
        String ouLeader;
        switch (orgUnit.getOrgUnitType().getCode())
        {
            case OrgUnitTypeCodes.INSTITUTE:
            	//-----------------------------------------------
                //ouLeader = "Директор института";
            	
            	if(orgUnit.getTitle().toLowerCase().contains("школа")) {
            		ouLeader = "Директор высшей школы";              
            	}
            	else {
            		ouLeader = "Директор института"; 
            	}
                //-----------------------------------------------
                break;
            case OrgUnitTypeCodes.BRANCH:
                ouLeader = "Директор филиала";
                break;
            case OrgUnitTypeCodes.COLLEGE:
                ouLeader = "Директор колледжа";
                break;
            case OrgUnitTypeCodes.FACULTY:
                ouLeader = "Декан факультета";
                break;
            default:
                ouLeader = "Директор";
        }
        textModifier.put("leader", ouLeader);

        EmployeePost head = EmployeeManager.instance().dao().getHead(orgUnit);
        textModifier.put("leaderTitle", head == null ? "" : head.getPerson().getFio());

        textModifier.modify(template);

        TreeMap<String, List<BulletinPrintRow>> groupedContentMap = getGroupedAndSortedContent(printInfo);
        final int[] count = {1};
        String[][] table = groupedContentMap.keySet().stream()
                .map(key -> groupedContentMap.get(key).stream()
                             .map(row -> getStudentRow(row.getSlot(), count[0]++))
                             .collect(Collectors.toList())
                )
                .flatMap(Collection::stream)
                .sorted(Comparator.comparing(row -> row[1]))
                .toArray(String[][]::new);

        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T", table).modify(template);

        return template;
    }

    private String[] getStudentRow(SessionDocumentSlot slot, int count)
    {
        SessionStudentNotAllowed sessionAllowance = getByNaturalId(new SessionStudentNotAllowedGen.NaturalId(
                ((SessionBulletinDocumentGen) slot.getDocument()).getSessionObject(), slot.getActualStudent()));
        if (sessionAllowance != null && sessionAllowance.getRemovalDate() != null) //анулировано
            sessionAllowance = null;

        SessionStudentNotAllowedForBulletin bulletinAllowance = getByNaturalId(new SessionStudentNotAllowedForBulletinGen.NaturalId(
                (SessionBulletinDocument) slot.getDocument(), slot.getActualStudent()));
        if (bulletinAllowance != null && bulletinAllowance.getRemovalDate() != null) //анулировано
            bulletinAllowance = null;

        String fio = slot.getActualStudent().getPerson().getFio() + (sessionAllowance != null || bulletinAllowance != null ? " (нд)" : "");

        String[] str = new String[4];
        str[0] = String.valueOf(count);
        str[1] = fio;
        str[2] = slot.getActualStudent().getPersonalNumber();
        str[3] = slot.getActualStudent().getBookNumber() != null ? slot.getActualStudent().getBookNumber() : "";
        return str;
    }

    private RtfDocument printCourseWorkBulletin(BulletinPrintInfo printInfo)
    {
        UnisessionCommonTemplate template = getCatalogItem(UnisessionCommonTemplate.class, UnisessionCommonTemplateCodes.NARFU_COURSE_WORK_BULLETIN);
        RtfDocument doc = new RtfReader().read(template.getContent());

        SessionBulletinDocument bulletin = printInfo.getBulletin();

        List<String> educationDirections = new ArrayList<>();
        List<String> groups = new ArrayList<>();
        List<String> developForms = new ArrayList<>();
        printInfo.getContentMap().keySet().forEach(slot -> {
            // поиск всех направлений подготовки студентов включенных в ведомость
            educationDirections.add(slot.getStudentWpe().getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getFullTitle());
            //	список групп студентов
            if (slot.getStudentWpe().getStudent().getGroup() != null)
                groups.add(slot.getStudentWpe().getStudent().getGroup().getTitle());
            //  список форм обучения
            developForms.add(slot.getStudentWpe().getStudent().getEducationOrgUnit().getDevelopForm().getTitle());
        });

        String position1;
        if (bulletin.getSessionObject().getOrgUnit().getHead() == null)
            position1 = "__________________________";
        else
            position1 = bulletin.getSessionObject().getOrgUnit().getHead().getEmployee().getPerson().getIdentityCard().getFio();
        
        //----------------------------------------------------------------------------------------------------------------------------------------------
        OrgUnit orgUnit = bulletin.getSessionObject().getOrgUnit();
        String ouLeader;
        switch (orgUnit.getOrgUnitType().getCode())
        {
            case OrgUnitTypeCodes.INSTITUTE:
            	if(orgUnit.getTitle().toLowerCase().contains("школа")) {
            		ouLeader = "Директор высшей школы";              
            	}
            	else {
            		ouLeader = "Директор института"; 
            	}
                break;
            case OrgUnitTypeCodes.BRANCH:
                ouLeader = "Директор филиала";
                break;
            case OrgUnitTypeCodes.COLLEGE:
                ouLeader = "Директор колледжа";
                break;
            case OrgUnitTypeCodes.FACULTY:
                ouLeader = "Декан факультета";
                break;
            default:
                ouLeader = "Директор";
        }
        //----------------------------------------------------------------------------------------------------------------------------------------------
        
        // заполнение шапки документа
        RtfInjectModifier im = new RtfInjectModifier();
        im.put("docNumber", bulletin.getNumber())
                .put("formativeOrgUnit", bulletin.getSessionObject().getOrgUnit().getPrintTitle())
                .put("educationDirection", educationDirections.stream().distinct().collect(Collectors.joining(", ", "", "")))
                .put("discipline", bulletin.getGroup().getActivityPart().getRegistryElement().getTitle())
                .put("teachers", UniStringUtils.join(printInfo.getCommission(), PpsEntry.person().identityCard().fio().s(), ", "))
                .put("developForms", developForms.stream().distinct().collect(Collectors.joining(", ", "", "")))
                .put("groups", groups.stream().distinct().collect(Collectors.joining(", ", "", "")))
                .put("documentDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(printInfo.getBulletin().getPerformDate()))
                .put("Position1", position1)
                //------------------------------------------
                .put("leader", ouLeader)
                //------------------------------------------
                .modify(doc);
        
        // заполнение табличной части
        List<String[]> tableRows = new ArrayList<>();
        int rowNum = 0;

        // 	поиск ФИО, оценок и тем курсовых
        Map<SessionMarkCatalogItem, Integer> markMap = new HashMap<>();
        for (Entry<EppStudentWpeCAction, BulletinPrintRow> entry : printInfo.getContentMap().entrySet())
        {
            rowNum++;
            DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(SessionProjectTheme.class, "theme_p")
                    .column("theme_p")
                    .where(eq(property(SessionProjectTheme.slot().studentWpeCAction().studentWpe().student().fromAlias("theme_p")),
                              value(entry.getKey().getStudentWpe().getStudent())))
                    //------------------------------------------------------------------------------------------------------------------------------------
                    //.where(eq(property(SessionProjectTheme.slot().studentWpeCAction().studentWpe().registryElementPart().fromAlias("theme_p")),
                    //          value(printInfo.getBulletin().getGroup().getActivityPart())));
            
                    .where(eq(property(SessionProjectTheme.slot().document().fromAlias("theme_p")), value(printInfo.getBulletin())));
            		//------------------------------------------------------------------------------------------------------------------------------------
            List<SessionProjectTheme> lst = getList(dql);

            String courseWork = !lst.isEmpty() ? lst.get(0).getTheme() : "";
            String fio = entry.getKey().getStudentWpe().getStudent().getPerson().getFullFio();
            //bagfix
            String mark = "";
            if (entry.getValue() == null || entry.getValue().getMark() == null)
            {
                tableRows.add(fillTableDisc(rowNum, fio, courseWork, mark));
                continue;
            }

            markMap.putIfAbsent(entry.getValue().getMark().getValueItem(), 0);
            markMap.put(entry.getValue().getMark().getValueItem(), markMap.get(entry.getValue().getMark().getValueItem()) + 1);

            mark = entry.getValue().getMark().getValueItem().getPrintTitle();
            tableRows.add(fillTableDisc(rowNum, fio, courseWork, mark));
        }

        String[][] tableDataMark = markMap.keySet().stream()
                .sorted(Comparator.comparingInt(SessionMarkCatalogItem::getPriority))
                .map(markItem -> new String[]{markItem.getTitle(), String.valueOf(markMap.get(markItem))})
                .toArray(String[][]::new);

        new RtfTableModifier()
                .put("T", tableRows.toArray(new String[0][]))
                .put("T2", tableDataMark)
                .modify(doc);

        return doc;
    }

    private String[] fillTableDisc(int rowNum, String fio, String coursWork, String mark)
    {
        String[] str = new String[4];
        str[0] = String.valueOf(rowNum);
        str[1] = fio;
        str[2] = coursWork;
        str[3] = mark;
        return str;
    }

    public static class ReportFile
    {
        private boolean zip;
        private String name;
        private byte[] content;

        public boolean isZip()
        {
            return zip;
        }

        public void setZip(boolean zip)
        {
            this.zip = zip;
        }

        public String getName()
        {
            return name;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public byte[] getContent()
        {
            return content;
        }

        public void setContent(byte[] content)
        {
            this.content = content;
        }
    }

    private static void addNewDoc(RtfDocument mainDoc, RtfDocument newDoc)
    {
        addSection(newDoc.getElementList());

        addPaperSize(newDoc.getElementList(),
                     newDoc.getSettings().getPaperWidth(),
                     newDoc.getSettings().getPaperHeight(),
                     newDoc.getSettings().getMarginLeft(),
                     newDoc.getSettings().getMarginRight(),
                     newDoc.getSettings().getMarginTop(),
                     newDoc.getSettings().getMarginBottom());

        mainDoc.getElementList().addAll(newDoc.getElementList());
    }

    /**
     * Добавляет контрол новой секции (\sect) перед сбросом параметров (\sectd)
     *
     * @param input - элементы документа
     */
    private static void addSection(List<IRtfElement> input)
    {
        if (input == null || input.isEmpty()) return;

        IRtfElement first = input.get(0);
        if (first instanceof IRtfGroup)
            addSection(((IRtfGroup) first).getElementList());
        else if (first.equals(RtfBean.getElementFactory().createRtfControl(IRtfData.SECTD)))
            input.add(0, (RtfBean.getElementFactory().createRtfControl(IRtfData.SECT)));
    }

    /**
     * Добавляет параметры листа после сброса параметров (\sectd) если их нет в группе элементов
     *
     * @param input    - элементы документа
     * @param pgwsxn   - ширина листа
     * @param pghsxn   - высота листа
     * @param marglsxn - отступ слева
     * @param margrsxn - отступ справа
     * @param margtsxn - отступ сверху
     * @param margbsxn - отступ снизу
     */
    private static void addPaperSize(List<IRtfElement> input, int pgwsxn, int pghsxn, int marglsxn, int margrsxn, int margtsxn, int margbsxn)
    {
        if (input == null || input.isEmpty()) return;

        int indexSectd = input.indexOf(RtfBean.getElementFactory().createRtfControl(IRtfData.SECTD));
        if (indexSectd >= 0)
        {
            if (input.indexOf(RtfBean.getElementFactory().createRtfControl(IRtfData.MARGTSXN)) == -1)
                input.add(indexSectd + 1, RtfBean.getElementFactory().createRtfControl(IRtfData.MARGTSXN, margtsxn));
            if (input.indexOf(RtfBean.getElementFactory().createRtfControl(IRtfData.MARGBSXN)) == -1)
                input.add(indexSectd + 1, RtfBean.getElementFactory().createRtfControl(IRtfData.MARGBSXN, margbsxn));
            if (input.indexOf(RtfBean.getElementFactory().createRtfControl(IRtfData.MARGLSXN)) == -1)
                input.add(indexSectd + 1, RtfBean.getElementFactory().createRtfControl(IRtfData.MARGLSXN, marglsxn));
            if (input.indexOf(RtfBean.getElementFactory().createRtfControl(IRtfData.MARGRSXN)) == -1)
                input.add(indexSectd + 1, RtfBean.getElementFactory().createRtfControl(IRtfData.MARGRSXN, margrsxn));
            if (input.indexOf(RtfBean.getElementFactory().createRtfControl(IRtfData.PGWSXN)) == -1)
                input.add(indexSectd + 1, RtfBean.getElementFactory().createRtfControl(IRtfData.PGWSXN, pgwsxn));
            if (input.indexOf(RtfBean.getElementFactory().createRtfControl(IRtfData.PGHSXN)) == -1)
                input.add(indexSectd + 1, RtfBean.getElementFactory().createRtfControl(IRtfData.PGHSXN, pghsxn));
        }

        input.forEach(e -> {
            if (e instanceof IRtfGroup)
                addPaperSize(((IRtfGroup) e).getElementList(), pgwsxn, pghsxn, marglsxn, margrsxn, margtsxn, margbsxn);
        });
    }
}
