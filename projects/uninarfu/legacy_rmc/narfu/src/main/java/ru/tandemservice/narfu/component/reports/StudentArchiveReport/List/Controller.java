package ru.tandemservice.narfu.component.reports.StudentArchiveReport.List;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.rtf.RtfDocumentRenderer;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.narfu.entity.NarfuReportStudentArchive;
import ru.tandemservice.unibase.UniBaseUtils;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = component.getModel();

        model.setSettings(component.getSettings());
        getDao().prepare(model);

        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component) {
        Model model = component.getModel();
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<NarfuReportStudentArchive> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new IndicatorColumn("Иконка", null).defaultIndicator(new IndicatorColumn.Item("report", "Отчет")).setOrderable(false).setClickable(false).setRequired(true));
        dataSource.addColumn(new SimpleColumn("Дата формирования отчета", NarfuReportStudentArchive.formingDate().s(), DateFormatter.DATE_FORMATTER_WITH_TIME).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Номер описи", NarfuReportStudentArchive.number().s()).setOrderable(true));
        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickPrintReport", "Печатать").setPermissionKey(model.getPrintKey()));
        dataSource.addColumn(
                new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteReport", "Удалить отчет от {0}?", new Object[]{"formingDate"})
                        .setPermissionKey(model.getDeleteKey())
                        .setOrderable(false)
        );

        model.setDataSource(dataSource);
    }

    public void onClickAddReport(IBusinessComponent component) {
        component.createDefaultChildRegion(new ComponentActivator("ru.tandemservice.narfu.component.reports.StudentArchiveReport.Add"));
    }

    public void onClickPrintReport(IBusinessComponent component) throws Exception {


        NarfuReportStudentArchive report = getDao().get((Long) component.getListenerParameter());

        DatabaseFile contentFile = report.getContent();
        if (contentFile == null || contentFile.getContent() == null)
            throw new ApplicationException("Файл отчета пуст");
        BusinessComponentUtils.downloadDocument(new RtfDocumentRenderer(contentFile.getFilename(), contentFile.getContent()), true);

    }

    public void onClickDeleteReport(IBusinessComponent component) {
        getDao().deleteRow(component);
    }

    public void onClickSearch(IBusinessComponent component) {
        component.saveSettings();
    }

    public void onClickClear(IBusinessComponent component) {
        Model model = component.getModel();
        model.getSettings().clear();
        onClickSearch(component);
    }

}
