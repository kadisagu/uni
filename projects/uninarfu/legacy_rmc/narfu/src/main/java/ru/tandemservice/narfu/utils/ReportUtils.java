package ru.tandemservice.narfu.utils;

import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013;

public class ReportUtils {

    public static EducationLevels getHighParentLevel(EducationLevels edu) {
        EducationLevels parent = edu.getParentLevel();
        if (parent != null) {
            if (edu.getParentLevel().getParentLevel() != null) {
                parent = edu.getParentLevel().getParentLevel();
                if (edu.getParentLevel().getParentLevel().getParentLevel() != null)
                    parent = edu.getParentLevel().getParentLevel().getParentLevel();
            }
            return parent;
        }
        else return edu;
    }

    /**
     * Исправленное получение родителя
     *
     * @param edu
     *
     * @return
     */
    public static WrapperObject getHighParentLevelWrapper(EducationLevels edu) {
        EducationLevels parent = edu.getParentLevel();
        if (edu.getEduProgramSubject() instanceof EduProgramSubject2013) {
            return new WrapperObject(((EduProgramSubject2013) edu.getEduProgramSubject()).getGroup());
        }
        else {
            if (parent != null) {
                if (edu.getParentLevel().getParentLevel() != null) {
                    parent = edu.getParentLevel().getParentLevel();
                    if (edu.getParentLevel().getParentLevel().getParentLevel() != null)
                        parent = edu.getParentLevel().getParentLevel().getParentLevel();
                }
                return new WrapperObject(parent);
            }
            else return new WrapperObject(edu);
        }
    }
}
