package ru.tandemservice.narfu.component.reports.OrgUnitAspirantReport.List;

import org.tandemframework.core.component.State;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.sec.runtime.SecurityRuntime;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.narfu.uniec.entity.report.OrgUnitAspirantReport;

@State(keys = "orgUnitId", bindings = "orgUnitId")
public class Model {

    private IDataSettings settings;

    private ISelectModel enrollmentCampaignModel;
    private ISelectModel educationLevelsHighSchoolModel;

    private Long orgUnitId;
    private OrgUnit orgUnit;
    private CommonPostfixPermissionModel secModel;

    private DynamicListDataSource<OrgUnitAspirantReport> dataSource;

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public DynamicListDataSource<OrgUnitAspirantReport> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<OrgUnitAspirantReport> dataSource) {
        this.dataSource = dataSource;
    }


    public CommonPostfixPermissionModel getSecModel() {
        return secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel) {
        this.secModel = secModel;
    }

    public Object getSecuredObject() {
        return null == getOrgUnit() ? SecurityRuntime.getInstance().getCommonSecurityObject() : getOrgUnit();
    }

    public String getViewKey() {

        return getOrgUnit() == null ? "viewGlobalStoredReport" : getSecModel().getPermission("orgUnit_viewAspirantReport");
    }

    public String getAddKey()
    {
        return getOrgUnit() == null ? "addGlobalStoredReport" : getSecModel().getPermission("orgUnit_addAspirantReport");
    }

    public String getPrintKey()
    {
        return getOrgUnit() == null ? "printGlobalStoredReport" : getSecModel().getPermission("orgUnit_viewAspirantReport");
    }

    public String getDeleteKey()
    {
        return getOrgUnit() == null ? "deleteGlobalStoredReport" : getSecModel().getPermission("orgUnit_deleteAspirantReport");
    }

    public OrgUnit getOrgUnit() {
        return orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit) {
        this.orgUnit = orgUnit;
    }

    public Long getOrgUnitId() {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId) {
        this.orgUnitId = orgUnitId;
    }

    public ISelectModel getEnrollmentCampaignModel() {
        return enrollmentCampaignModel;
    }

    public void setEnrollmentCampaignModel(ISelectModel enrollmentCampaignModel) {
        this.enrollmentCampaignModel = enrollmentCampaignModel;
    }

    public ISelectModel getEducationLevelsHighSchoolModel() {
        return educationLevelsHighSchoolModel;
    }

    public void setEducationLevelsHighSchoolModel(ISelectModel educationLevelsHighSchoolModel) {
        this.educationLevelsHighSchoolModel = educationLevelsHighSchoolModel;
    }
}
