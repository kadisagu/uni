package ru.tandemservice.narfu.component.reports.OrgUnitAspirantReport.List;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.narfu.uniec.entity.report.OrgUnitAspirantReport;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {


    @Override
    public void prepare(final Model model) {
        if (null != model.getOrgUnitId()) {
            model.setOrgUnit(getNotNull(model.getOrgUnitId()));
            model.setSecModel(new OrgUnitSecModel(model.getOrgUnit()));
        }
        model.setEnrollmentCampaignModel(new LazySimpleSelectModel<>(EnrollmentCampaign.class));
        model.setEducationLevelsHighSchoolModel(new DQLFullCheckSelectModel("fullTitle") {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(EducationOrgUnit.class, "e");
                dql.where(DQLExpressions.eq(DQLExpressions.property(EducationOrgUnit.formativeOrgUnit().fromAlias("e")), DQLExpressions.value(model.getOrgUnit())));
                dql.column(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().fromAlias("e")));
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationLevelsHighSchool.class, alias);

                //     dql.order(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().fullTitle().fromAlias("e")));
                builder.where(DQLExpressions.in(DQLExpressions.property(alias), dql.buildQuery()));
                builder.order(DQLExpressions.property(EducationLevelsHighSchool.fullTitle().fromAlias(alias)));
                FilterUtils.applySimpleLikeFilter(builder, alias, EducationLevelsHighSchool.fullTitle().s(), filter);
                return builder;
            }
        });


    }

    @Override
    public void prepareListDataSource(Model model) {

        List<EducationLevelsHighSchool> educationLevelsHighSchoolList = model.getSettings().get("educationLevelsHighSchool");
        EnrollmentCampaign enrollmentCampaign = model.getSettings().get("enrollmentCampaign");

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(OrgUnitAspirantReport.class, "report");
        if (!CollectionUtils.isEmpty(educationLevelsHighSchoolList)) {
            builder.where(DQLExpressions.in(DQLExpressions.property(OrgUnitAspirantReport.educationLevelHighSchool().fromAlias("report")), educationLevelsHighSchoolList));
        }
        if (enrollmentCampaign != null) {
            builder.where(DQLExpressions.eq(DQLExpressions.property(OrgUnitAspirantReport.enrollmentCampaign().fromAlias("report")), DQLExpressions.value(enrollmentCampaign)));
        }


        DQLOrderDescriptionRegistry descriptionRegistry = new DQLOrderDescriptionRegistry(OrgUnitAspirantReport.class, "report");
        descriptionRegistry.applyOrder(builder, model.getDataSource().getEntityOrder());

        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }
}
