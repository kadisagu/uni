package ru.tandemservice.narfu.component.studentmassprint.printDAO.grants.requestDate;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.narfu.component.studentmassprint.GrantsMassPrint.Model;
import ru.tandemservice.narfu.component.studentmassprint.printDAO.grants.AbstractGrantRequestReport;

import java.util.Date;

public class PrintBean extends AbstractGrantRequestReport<Model>
{

    @Override
    protected RtfInjectModifier createInjectModifier(Model model) {
        RtfInjectModifier im = super.createInjectModifier(model);

        Date dateOfEndPayment = (Date) model.getSettings().get("dateOfEndPayment");

        im.put("date_st", RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(model.getDateOfBeginPayment()) + " года");
        im.put("date_fin", RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(dateOfEndPayment) + " года");
        return im;
    }


}
