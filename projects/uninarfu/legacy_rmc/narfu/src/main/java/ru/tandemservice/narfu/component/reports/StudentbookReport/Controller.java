package ru.tandemservice.narfu.component.reports.StudentbookReport;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.rtf.RtfDocumentRenderer;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.util.RtfUtil;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    public void onRefreshComponent(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        ((IDAO) getDao()).prepare(model);
    }

    public void onClickApply(IBusinessComponent component) throws Exception {
        Model model = (Model) getModel(component);
        RtfDocument document = ((IDAO) getDao()).createReport(model);

        BusinessComponentUtils.downloadDocument(new RtfDocumentRenderer("книга регистрации.rtf", RtfUtil.toByteArray(document)), true);
        deactivate(component);
    }
}
