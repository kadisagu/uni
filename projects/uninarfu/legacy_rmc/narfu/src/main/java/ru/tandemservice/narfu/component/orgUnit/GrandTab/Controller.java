package ru.tandemservice.narfu.component.orgUnit.GrandTab;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;

import java.util.ArrayList;

/**
 * User: ramaslov
 * Date: 21.03.13
 * Time: 17:04
 * To change this template use File | Settings | File Templates.
 */
public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        ((IDAO) getDao()).prepare(getModel(component));
        prepareDataSource(component);
    }

    public void onClickCheckAllOrgUnit(IBusinessComponent component) {
        Model model = getModel(component);
        if (model.getCheckAllOrgUnit()) {
            model.setOldOrgUnitList(model.getOrgUnitList());
            model.setOrgUnitList(new ArrayList<OrgUnit>());
        }
        else {
            model.setOrgUnitList(model.getOldOrgUnitList());
        }
    }

    public void onClickCheckAllGrantView(IBusinessComponent component) {
        Model model = getModel(component);
        if (model.getCheckAllGrantView()) {
            model.setOldGrantViewList(model.getGrantViewList());
            model.setGrantViewList(new ArrayList<GrantView>());
        }
        else {
            model.setGrantViewList(model.getOldGrantViewList());
        }
    }

    protected void prepareDataSource(IBusinessComponent component) {

    }

    public void onClickClearAll(IBusinessComponent component)
    {
        getDao().cleanAllGrant(getModel(component));
        component.getUserContext().getInfoCollector().add("Данные успешно изменены");
        //deactivate(component);
    }

}
