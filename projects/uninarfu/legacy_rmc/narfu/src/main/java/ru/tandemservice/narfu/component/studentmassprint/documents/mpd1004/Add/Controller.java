package ru.tandemservice.narfu.component.studentmassprint.documents.mpd1004.Add;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.narfu.component.documents.d1004.Add.ParagraphData;

import java.util.Date;


public class Controller
        extends ru.tandemservice.unirmc.component.studentmassprint.documents.Base.Controller<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        getDao().prepare(getModel(component));
        onChangePost(component);
        onChangeExecutor(component);
    }

    public void getLengthSession(IBusinessComponent component) {
        Model model = component.getModel();
        Date startSession = model.getDateStartCertification();
        Date endSession = model.getDateEndCertification();
        if ((startSession != null) & (endSession != null)) {
            model.setContinuance(CoreDateUtils.getDateDifferenceInDays(endSession, startSession) + 1);
        }
        else
            model.setContinuance(0);
    }

    public void onChangeDate(IBusinessComponent component) {
        Model model = component.getModel();
        model.setDateStartCertification(ParagraphData.getDate(model.getFirstStudent(), model.getPeriod(), model.getTerm(), true));
        model.setDateEndCertification(ParagraphData.getDate(model.getFirstStudent(), model.getPeriod(), model.getTerm(), false));
        getLengthSession(component);
    }

    public void onChangePost(IBusinessComponent component) {
        Model model = component.getModel();

        IDAO dao = (IDAO) getDao();

        String postCode = dao.getPostCode(model.getManagerPostTitle());

        model.setManagerFio(dao.getDirectorFIO(postCode.equals("115") ? model.getFirstStudent().getEducationOrgUnit().getFormativeOrgUnit() : null, postCode));
    }

    public void onChangeExecutor(IBusinessComponent component) {
        Model model = component.getModel();

        IDAO dao = (IDAO) getDao();
        String executant = "";
        if (model.getExecutant() == null) {
            Person currentUser = PersonManager.instance().dao().getPerson(ContextLocal.getUserContext().getPrincipalContext());
            executant = (currentUser == null ? "Администратор " : (currentUser.getIdentityCard().getFullFio() + " "));
        }
        else {
            executant = model.getEmployeePost().getPerson().getFullFio();
        }
        if (executant == "Администратор ") {
            model.setExecutant("");
            model.setTelephone("");
        }
        else {
            ListResult<EmployeePost> listPost = model.getEmployeePostListModel().findValues(executant);
            EmployeePost ep = listPost.getObjects().get(0);
            model.setEmployeePost(ep);
            model.setExecutant(executant);
            model.setTelephone(model.getTelephone() == null ? model.getEmployeePost().getOrgUnit().getPhone() : model.getTelephone());
        }
    }
}
