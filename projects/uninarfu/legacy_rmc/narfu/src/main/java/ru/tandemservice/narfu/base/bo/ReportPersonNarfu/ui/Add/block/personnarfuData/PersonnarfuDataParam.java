package ru.tandemservice.narfu.base.bo.ReportPersonNarfu.ui.Add.block.personnarfuData;

import org.tandemframework.core.entity.IIdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import ru.tandemservice.movestudentrmc.entity.PersonNARFU;
import ru.tandemservice.narfu.base.bo.ReportPersonNarfu.ui.Add.ReportPersonNarfuAddUI;

public class PersonnarfuDataParam
        implements IReportDQLModifier
{

    /**
     * булевы признаки выбора
     */
    private IReportParam<IIdentifiableWrapper> needDormitoryOnReceiptPeriod = new ReportParam<IIdentifiableWrapper>();
    private IReportParam<IIdentifiableWrapper> needDormitoryOnStudingPeriod = new ReportParam<IIdentifiableWrapper>();

    private String alias(ReportDQL dql)
    {
        // соединяем персону НАРФУ
        String personNarfuAlias = dql.innerJoinEntity(PersonNARFU.class, PersonNARFU.person());
        // добавляем сортировку
        // dql.order(entrantAlias, Entrant.enrollmentCampaign().id());
        return personNarfuAlias;
    }


    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {

    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        alias(dql);

        if (needDormitoryOnReceiptPeriod.isActive()) {
            printInfo.addPrintParam(ReportPersonNarfuAddUI.PERSONNARFU_SCHEET, "personnarfuData.needDormitoryOnReceiptPeriod", needDormitoryOnReceiptPeriod.getData().getTitle());
            dql.builder.where(DQLExpressions.eq(DQLExpressions.property(PersonNARFU.needDormitoryOnReceiptPeriod().fromAlias(alias(dql))), DQLExpressions.value(needDormitoryOnReceiptPeriod.getData().getId().equals(PersonnarfuDataBlock.NEED_DORMITORY_YES))));
        }

        if (needDormitoryOnStudingPeriod.isActive()) {
            printInfo.addPrintParam(ReportPersonNarfuAddUI.PERSONNARFU_SCHEET, "personnarfuData.needDormitoryOnStudingPeriod", needDormitoryOnStudingPeriod.getData().getTitle());
            dql.builder.where(DQLExpressions.eq(DQLExpressions.property(PersonNARFU.person().needDormitory().fromAlias(alias(dql))), DQLExpressions.value(needDormitoryOnStudingPeriod.getData().getId().equals(PersonnarfuDataBlock.NEED_DORMITORY_YES))));
        }


    }


    public IReportParam<IIdentifiableWrapper> getNeedDormitoryOnReceiptPeriod() {
        return needDormitoryOnReceiptPeriod;
    }


    public IReportParam<IIdentifiableWrapper> getNeedDormitoryOnStudingPeriod() {
        return needDormitoryOnStudingPeriod;
    }

}
