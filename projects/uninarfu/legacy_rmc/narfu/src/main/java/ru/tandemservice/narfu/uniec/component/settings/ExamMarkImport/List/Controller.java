package ru.tandemservice.narfu.uniec.component.settings.ExamMarkImport.List;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.narfu.entity.EntrantExamBulletinImportedFile;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = getModel(component);
        model.setSettings(component.getSettings());
        ((IDAO) getDao()).prepare(model);

        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component) {
        Model model = getModel(component);
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<EntrantExamBulletinImportedFile> dataSource = UniUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Дата импорта", EntrantExamBulletinImportedFile.importDate(), DateFormatter.DATE_FORMATTER_WITH_TIME));
        dataSource.addColumn(new IndicatorColumn("Импортированный файл", null, "onClickPrintImportedFile").defaultIndicator(new IndicatorColumn.Item("printer", "Печать")).setImageHeader(false).setDisableSecondSubmit(false).setOrderable(false));
        dataSource.addColumn(new IndicatorColumn("Результирующий файл", null, "onClickPrintResultFile").defaultIndicator(new IndicatorColumn.Item("printer", "Печать")).setImageHeader(false).setDisableSecondSubmit(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Результат", EntrantExamBulletinImportedFile.importResult().title()));
        dataSource.addColumn(new ActionColumn("Удалить", "delete", "onClickDeleteFiles", "Удалить импортированный и результирующий файлы от «{0}»?", new Object[]{EntrantExamBulletinImportedFile.importDate().s()}).setPermissionKey("menuEntrantSettingsImportMarksEdit"));
        dataSource.setOrder(EntrantExamBulletinImportedFile.importDate().s(), OrderDirection.desc);

        model.setDataSource(dataSource);
    }

    public void onClickImport(IBusinessComponent component) {
        activate(component, new ComponentActivator(ru.tandemservice.narfu.uniec.component.settings.ExamMarkImport.Add.Model.class.getPackage().getName()));
    }

    public void onClickPrintImportedFile(IBusinessComponent component) {
        Long id = component.getListenerParameter();
        EntrantExamBulletinImportedFile log = find(component, id);

        Integer reportId = PrintReportTemporaryStorage.registerTemporaryPrintForm(log.getImportedFile(), "Импортированный файл оценок.csv");
        activateInRoot(component, new ComponentActivator("ru.tandemservice.uni.component.reports.PrintReport", new ParametersMap().add("id", reportId).add("extension", "txt")));
    }

    public void onClickPrintResultFile(IBusinessComponent component) {
        Long id = component.getListenerParameter();
        EntrantExamBulletinImportedFile log = find(component, id);

        Integer reportId = PrintReportTemporaryStorage.registerTemporaryPrintForm(log.getResultFile(), "Ошибки.rtf");
        activateInRoot(component, new ComponentActivator("ru.tandemservice.uni.component.reports.PrintReport", new ParametersMap().add("id", reportId).add("extension", "rtf")));
    }

    public void onClickDeleteFiles(IBusinessComponent component) {
        Long id = component.getListenerParameter();
        getDao().delete(id);
    }

    public void onClickSearch(IBusinessComponent component) {
        component.saveSettings();
    }

    public void onClickClear(IBusinessComponent component) {
        Model model = getModel(component);
        model.getSettings().clear();
    }

    private EntrantExamBulletinImportedFile find(IBusinessComponent component, Long id) {
        Model model = getModel(component);
        for (EntrantExamBulletinImportedFile log : model.getDataSource().getEntityList())
            if (log.getId().equals(id)) return log;

        return null;
    }
}
