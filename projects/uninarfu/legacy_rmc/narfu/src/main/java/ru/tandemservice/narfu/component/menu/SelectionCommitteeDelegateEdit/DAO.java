package ru.tandemservice.narfu.component.menu.SelectionCommitteeDelegateEdit;

import org.hibernate.Session;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.sec.entity.Principal;
import ru.tandemservice.narfu.entity.Principal2SelectionCommitteeDelegate;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDao;


public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        model.setEntity(getSelectionCommitteeDelegate(getSession()));
    }

    @Override
    public void update(Model model) {
        saveOrUpdate(model.getEntity());
    }

    public static void fillDelegate(RtfInjectModifier im, Principal principal, Session session) {
        Principal2SelectionCommitteeDelegate delegate = getSelectionCommitteeDelegate(principal, session);

        im.put("delegatePost", delegate.getPost() != null ? delegate.getPost() : "");
        im.put("delegateFio", delegate.getFio() != null ? delegate.getFio() : "");
    }

    public static void fillDelegate(RtfDocument document, Session session) {
        Principal2SelectionCommitteeDelegate delegate = getSelectionCommitteeDelegate(session);

        RtfInjectModifier im = new RtfInjectModifier();
        im.put("delegatePost", delegate.getPost() != null ? delegate.getPost() : "");
        im.put("delegateFio", delegate.getFio() != null ? delegate.getFio() : "");

        im.modify(document);
    }

    public static Principal2SelectionCommitteeDelegate getSelectionCommitteeDelegate(Session session) {
        IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();
        Principal principal = (Principal) principalContext.getPrincipal();

        return getSelectionCommitteeDelegate(principal, session);
    }

    public static Principal2SelectionCommitteeDelegate getSelectionCommitteeDelegate(Principal principal, Session session) {
        Principal2SelectionCommitteeDelegate result = IUniBaseDao.instance.get().get(Principal2SelectionCommitteeDelegate.class, Principal2SelectionCommitteeDelegate.principal(), principal);
        if (result == null) {
            result = new Principal2SelectionCommitteeDelegate();

            result.setPrincipal(principal);
            result.setPost("");
            result.setFio("");

            //session.saveOrUpdate(result);
        }

        return result;
    }
}
