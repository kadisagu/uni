package ru.tandemservice.narfu.component.reports.StudentsByUGS.Pub;

import org.tandemframework.core.component.State;
import ru.tandemservice.narfu.entity.NarfuReportStudentsByUGS;

@State(keys = {"publisherId"}, bindings = {"report.id"})
public class Model {
    private NarfuReportStudentsByUGS _report = new NarfuReportStudentsByUGS();

    public NarfuReportStudentsByUGS getReport()
    {
        return this._report;
    }

    public void setReport(NarfuReportStudentsByUGS report)
    {
        this._report = report;
    }
}