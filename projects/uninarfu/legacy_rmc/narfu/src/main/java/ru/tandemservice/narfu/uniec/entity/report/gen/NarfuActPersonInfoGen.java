package ru.tandemservice.narfu.uniec.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.narfu.uniec.entity.report.NarfuActPersonInfo;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Список отчетов «Акт приема-передачи ЛД»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class NarfuActPersonInfoGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.narfu.uniec.entity.report.NarfuActPersonInfo";
    public static final String ENTITY_NAME = "narfuActPersonInfo";
    public static final int VERSION_HASH = -1650059239;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_DATE_CREATE = "dateCreate";
    public static final String P_DOC_NUMBER = "docNumber";
    public static final String P_FORMATIVE_ORG_UNIT_TITLE = "formativeOrgUnitTitle";

    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private Date _dateCreate;     // Дата акта
    private String _docNumber;     // Номер документа
    private String _formativeOrgUnitTitle;     // Формирующее подр.

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Дата акта. Свойство не может быть null.
     */
    @NotNull
    public Date getDateCreate()
    {
        return _dateCreate;
    }

    /**
     * @param dateCreate Дата акта. Свойство не может быть null.
     */
    public void setDateCreate(Date dateCreate)
    {
        dirty(_dateCreate, dateCreate);
        _dateCreate = dateCreate;
    }

    /**
     * @return Номер документа.
     */
    public String getDocNumber()
    {
        return _docNumber;
    }

    /**
     * @param docNumber Номер документа.
     */
    public void setDocNumber(String docNumber)
    {
        dirty(_docNumber, docNumber);
        _docNumber = docNumber;
    }

    /**
     * @return Формирующее подр..
     */
    public String getFormativeOrgUnitTitle()
    {
        return _formativeOrgUnitTitle;
    }

    /**
     * @param formativeOrgUnitTitle Формирующее подр..
     */
    public void setFormativeOrgUnitTitle(String formativeOrgUnitTitle)
    {
        dirty(_formativeOrgUnitTitle, formativeOrgUnitTitle);
        _formativeOrgUnitTitle = formativeOrgUnitTitle;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof NarfuActPersonInfoGen)
        {
            setEnrollmentCampaign(((NarfuActPersonInfo)another).getEnrollmentCampaign());
            setDateCreate(((NarfuActPersonInfo)another).getDateCreate());
            setDocNumber(((NarfuActPersonInfo)another).getDocNumber());
            setFormativeOrgUnitTitle(((NarfuActPersonInfo)another).getFormativeOrgUnitTitle());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends NarfuActPersonInfoGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) NarfuActPersonInfo.class;
        }

        public T newInstance()
        {
            return (T) new NarfuActPersonInfo();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "dateCreate":
                    return obj.getDateCreate();
                case "docNumber":
                    return obj.getDocNumber();
                case "formativeOrgUnitTitle":
                    return obj.getFormativeOrgUnitTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "dateCreate":
                    obj.setDateCreate((Date) value);
                    return;
                case "docNumber":
                    obj.setDocNumber((String) value);
                    return;
                case "formativeOrgUnitTitle":
                    obj.setFormativeOrgUnitTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                        return true;
                case "dateCreate":
                        return true;
                case "docNumber":
                        return true;
                case "formativeOrgUnitTitle":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return true;
                case "dateCreate":
                    return true;
                case "docNumber":
                    return true;
                case "formativeOrgUnitTitle":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "dateCreate":
                    return Date.class;
                case "docNumber":
                    return String.class;
                case "formativeOrgUnitTitle":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<NarfuActPersonInfo> _dslPath = new Path<NarfuActPersonInfo>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "NarfuActPersonInfo");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.narfu.uniec.entity.report.NarfuActPersonInfo#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Дата акта. Свойство не может быть null.
     * @see ru.tandemservice.narfu.uniec.entity.report.NarfuActPersonInfo#getDateCreate()
     */
    public static PropertyPath<Date> dateCreate()
    {
        return _dslPath.dateCreate();
    }

    /**
     * @return Номер документа.
     * @see ru.tandemservice.narfu.uniec.entity.report.NarfuActPersonInfo#getDocNumber()
     */
    public static PropertyPath<String> docNumber()
    {
        return _dslPath.docNumber();
    }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.narfu.uniec.entity.report.NarfuActPersonInfo#getFormativeOrgUnitTitle()
     */
    public static PropertyPath<String> formativeOrgUnitTitle()
    {
        return _dslPath.formativeOrgUnitTitle();
    }

    public static class Path<E extends NarfuActPersonInfo> extends StorableReport.Path<E>
    {
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Date> _dateCreate;
        private PropertyPath<String> _docNumber;
        private PropertyPath<String> _formativeOrgUnitTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.narfu.uniec.entity.report.NarfuActPersonInfo#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Дата акта. Свойство не может быть null.
     * @see ru.tandemservice.narfu.uniec.entity.report.NarfuActPersonInfo#getDateCreate()
     */
        public PropertyPath<Date> dateCreate()
        {
            if(_dateCreate == null )
                _dateCreate = new PropertyPath<Date>(NarfuActPersonInfoGen.P_DATE_CREATE, this);
            return _dateCreate;
        }

    /**
     * @return Номер документа.
     * @see ru.tandemservice.narfu.uniec.entity.report.NarfuActPersonInfo#getDocNumber()
     */
        public PropertyPath<String> docNumber()
        {
            if(_docNumber == null )
                _docNumber = new PropertyPath<String>(NarfuActPersonInfoGen.P_DOC_NUMBER, this);
            return _docNumber;
        }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.narfu.uniec.entity.report.NarfuActPersonInfo#getFormativeOrgUnitTitle()
     */
        public PropertyPath<String> formativeOrgUnitTitle()
        {
            if(_formativeOrgUnitTitle == null )
                _formativeOrgUnitTitle = new PropertyPath<String>(NarfuActPersonInfoGen.P_FORMATIVE_ORG_UNIT_TITLE, this);
            return _formativeOrgUnitTitle;
        }

        public Class getEntityClass()
        {
            return NarfuActPersonInfo.class;
        }

        public String getEntityName()
        {
            return "narfuActPersonInfo";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
