package ru.tandemservice.narfu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_narfu_1x0x0_12to13 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.10"),
                        new ScriptDependency("org.tandemframework.shared", "1.1.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.1.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность narfuReportVacantBudgetPlaces

        // создано свойство campaings
        if (!tool.columnExists("nrfrprtvcntbdgtplcs_t", "campaings_p")) {
            // создать колонку
            tool.createColumn("nrfrprtvcntbdgtplcs_t", new DBColumn("campaings_p", DBType.createVarchar(255)));

        }

        // создано свойство developForm
        if (!tool.columnExists("nrfrprtvcntbdgtplcs_t", "developform_id")) {
            // создать колонку
            tool.createColumn("nrfrprtvcntbdgtplcs_t", new DBColumn("developform_id", DBType.LONG));

        }


    }
}