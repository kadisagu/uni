package ru.tandemservice.narfu.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.narfu.entity.gen.PersonBenefitExtGen;

/**
 * Расширение льготы для персоны
 */
public class PersonBenefitExt extends PersonBenefitExtGen {
    @EntityDSLSupport
    public String getTitleWithCauseDisability() {
        return new StringBuilder()
                .append(this.getPersonBenefit().getTitle())
                .append(" (")
                .append(this.getCauseDisability())
                .append(")")
                .toString();
    }
}