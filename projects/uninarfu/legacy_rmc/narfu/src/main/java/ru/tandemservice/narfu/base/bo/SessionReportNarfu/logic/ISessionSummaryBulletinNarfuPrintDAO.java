package ru.tandemservice.narfu.base.bo.SessionReportNarfu.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.base.bo.SessionReport.util.ISessionReportGroupFilterParams;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.document.SessionObject;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.Collection;

public abstract interface ISessionSummaryBulletinNarfuPrintDAO extends INeedPersistenceSupport {

    public static final SpringBeanCache<ISessionSummaryBulletinNarfuPrintDAO> instance = new SpringBeanCache(ISessionSummaryBulletinNarfuPrintDAO.class.getName());

    public abstract Collection<ISessionSummaryBulletinData> getSessionSummaryBulletinData(ISessionSummaryBulletinParams paramISessionSummaryBulletinParams);

    public abstract byte[] print(ISessionSummaryBulletinParams paramISessionSummaryBulletinParams, Collection<ISessionSummaryBulletinData> paramCollection);

    public abstract void createStoredReport(ISessionSummaryBulletinParams paramISessionSummaryBulletinParams);

    public static abstract interface ISessionSummaryBulletinAction {
        public abstract EppGroupTypeFCA getControlActionType();

        public abstract EppRegistryElementPart getDiscipline();

        public abstract Collection<SessionDocument> getBulletins();
    }

    public static abstract interface ISessionSummaryBulletinStudent {
        public abstract Student getStudent();

        public abstract EducationOrgUnit getEduOu();

        public abstract Course getCourse();

        public abstract Term getTerm();
    }

    public static abstract interface ISessionSummaryBulletinData {
        public abstract String getGroup();

        public abstract Collection<ISessionSummaryBulletinStudent> getStudents();

        public abstract Collection<ISessionSummaryBulletinAction> getActions();

        public abstract EppWorkPlanRowKind getObligation(ISessionSummaryBulletinStudent paramISessionSummaryBulletinStudent, ISessionSummaryBulletinAction paramISessionSummaryBulletinAction);

        public abstract SessionMark getMark(ISessionSummaryBulletinStudent paramISessionSummaryBulletinStudent, ISessionSummaryBulletinAction paramISessionSummaryBulletinAction);
    }

    public static abstract interface ISessionSummaryBulletinParams {
        public abstract SessionObject getSessionObject();

        public abstract boolean isInSessionMarksOnly();

        public abstract Collection<EppWorkPlanRowKind> getObligations();

        public abstract Course getCourse();

        public abstract ISessionReportGroupFilterParams getGroupFilterParams();
    }

}
