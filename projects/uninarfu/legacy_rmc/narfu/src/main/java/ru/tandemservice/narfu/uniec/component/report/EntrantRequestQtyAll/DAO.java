package ru.tandemservice.narfu.uniec.component.report.EntrantRequestQtyAll;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import jxl.write.Number;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.eventssecrmc.dao.DaoFacade;
import ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.lang.Boolean;
import java.util.*;

public class DAO extends UniDao<Model> implements IDAO {
    // private IEntrantExamListPrintBean examListBean = (IEntrantExamListPrintBean) ApplicationRuntime.getBean("entrantExamListPrintBean");

    @Override
    public void prepare(Model model) {
        super.prepare(model);
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.setFormativeOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING, null));
        //model.setDateFrom(model.getEnrollmentCampaign() == null ? DateUtils.getYearFirstTimeMoment() : model.getEnrollmentCampaign().getStartDate());
        model.setRptDate(CoreDateUtils.getDayFirstTimeMoment(new Date()));
    }

    @Override
    public void preparePrintReport(Model model) throws Exception
    {

        IScriptItem templateDocument = (IScriptItem) UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, "entrantRequestQty_narfu");
        ByteArrayInputStream in = new ByteArrayInputStream(templateDocument.getCurrentTemplate());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Workbook tempalteWorkbook = Workbook.getWorkbook(in);

        // создаем печатную форму
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        WritableWorkbook workbook = Workbook.createWorkbook(out, tempalteWorkbook, ws);

        // create fonts
        WritableFont times12 = new WritableFont(WritableFont.TIMES, 12);
        WritableFont times14 = new WritableFont(WritableFont.TIMES, 14);
        WritableFont times12bold = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
        WritableFont times14bold = new WritableFont(WritableFont.TIMES, 14, WritableFont.BOLD);

        // create cell formats
        WritableCellFormat rowFormat = new WritableCellFormat(times14);
        rowFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        rowFormat.setVerticalAlignment(VerticalAlignment.TOP);
        rowFormat.setWrap(true);

        WritableCellFormat rowTotFormat = new WritableCellFormat(times14bold);
        rowTotFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        rowTotFormat.setVerticalAlignment(VerticalAlignment.TOP);
        rowTotFormat.setWrap(true);
        if (model.getFormativeOrgUnit() == null)
            rowTotFormat.setBackground(Colour.GRAY_25);

        WritableCellFormat rowFirstColFormat = new WritableCellFormat(times12);
        rowFirstColFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        rowFirstColFormat.setVerticalAlignment(VerticalAlignment.TOP);
        rowFirstColFormat.setWrap(true);

        WritableCellFormat rowFirstColTotFormat = new WritableCellFormat(times12bold);
        rowFirstColTotFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        rowFirstColTotFormat.setVerticalAlignment(VerticalAlignment.TOP);
        rowFirstColTotFormat.setWrap(true);
        if (model.getFormativeOrgUnit() == null)
            rowFirstColTotFormat.setBackground(Colour.GRAY_25);

        WritableCellFormat signFormat = new WritableCellFormat(times14bold);
        signFormat.setWrap(true);
        signFormat.setVerticalAlignment(VerticalAlignment.TOP);

        WritableCellFormat signFormatRight = new WritableCellFormat(signFormat);
        signFormatRight.setAlignment(Alignment.RIGHT);

        WritableSheet sheet = workbook.getSheet(0);
        Label cell1 = (Label) sheet.getWritableCell(0, 4);
        cell1.setString(model.getFormativeOrgUnit() == null ? "" : model.getFormativeOrgUnit().getTitle());

        Label cell2 = (Label) sheet.getWritableCell(0, 5);
        cell2.setString("Количество поданных заявлений на " + DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getRptDate()) + " года");

        //Выборка направлений из заявлений для данной кампании на заданную дату
        DQLSelectBuilder derBuilder = new DQLSelectBuilder()
                .fromEntity(RequestedEnrollmentDirection.ENTITY_CLASS, "r")
                .where(DQLExpressions.eq(DQLExpressions.property("r." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_ENROLLMENT_CAMPAIGN),
                                         DQLExpressions.value(model.getEnrollmentCampaign())
                ))
                .where(DQLExpressions.lt(DQLExpressions.property("r." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.P_REG_DATE),
                                         DQLExpressions.valueDate(CoreDateUtils.getNextDayFirstTimeMoment(model.getRptDate(), 1))
                ))
                .order("r." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.P_REG_NUMBER)
                .order("r." + RequestedEnrollmentDirection.P_PRIORITY);

		/*
		 * заберем все выбранные НП с флагом "Имеет особые права"
		 */
        DQLSelectBuilder srBuilder = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirectionExt.class, "ext")
                .where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirectionExt.requestedEnrollmentDirection().id().fromAlias("ext")), derBuilder.buildQuery()))
                .where(DQLExpressions.eqValue(DQLExpressions.property(RequestedEnrollmentDirectionExt.haveSpecialRights().fromAlias("ext")), Boolean.TRUE))
                .column(RequestedEnrollmentDirectionExt.requestedEnrollmentDirection().fromAlias("ext").s());

        List<RequestedEnrollmentDirection> srDirections = srBuilder.createStatement(getSession()).<RequestedEnrollmentDirection>list();

        List<RequestedEnrollmentDirection> directions = derBuilder.createStatement(getSession()).<RequestedEnrollmentDirection>list();

        //берем все заявления, группируем по факультетам и по направлениям подготовки
        Map<OrgUnit, Map<EducationLevelsHighSchool, List<RequestedEnrollmentDirection>>> orgUnitsDirectionsMap = new HashMap<>();

        for (RequestedEnrollmentDirection direction : directions) {

            EducationLevelsHighSchool edhs = direction.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool();
            OrgUnit o = direction.getEnrollmentDirection().getEducationOrgUnit().getFormativeOrgUnit();
            //фильтруем по подразделению
            if (model.getFormativeOrgUnit() != null && !model.getFormativeOrgUnit().equals(o))
                continue;
            Map<EducationLevelsHighSchool, List<RequestedEnrollmentDirection>> edm = orgUnitsDirectionsMap.get(o);
            if (edm == null)
                edm = new HashMap<>();

            List<RequestedEnrollmentDirection> dl = edm.get(edhs);
            if (dl == null) dl = new ArrayList<>();
            dl.add(direction);
            edm.put(edhs, dl);
            orgUnitsDirectionsMap.put(o, edm);
        }
        //освободим памяти
        directions.clear();

        int excelRowCount = 16;
        SingleLine tot_line = new SingleLine();
        tot_line.name = "Всего по университету";
        for (OrgUnit o : orgUnitsDirectionsMap.keySet()) {
            SingleLine ou_line = new SingleLine();
            ou_line.name = (model.getFormativeOrgUnit() == null) ? ("Всего по " + o.getTitle()) : "Итого";

            Map<EducationLevelsHighSchool, List<RequestedEnrollmentDirection>> edm = orgUnitsDirectionsMap.get(o);
            for (EducationLevelsHighSchool edhs : edm.keySet()) {
                SingleLine line = new SingleLine();
                line.name = edhs.getDisplayableTitle();

                for (RequestedEnrollmentDirection direction : edm.get(edhs)) {
                    //Разбиваем по формам освоения
                    DevelopForm df = direction.getEnrollmentDirection().getEducationOrgUnit().getDevelopForm();
                    switch (df.getCode())
                    {
                        case DevelopFormCodes.FULL_TIME_FORM:  //очная
                            calcSingleDirection(direction, line.o_batch, srDirections);
                            break;
                        case DevelopFormCodes.PART_TIME_FORM:  //очно-заочная
                            calcSingleDirection(direction, line.oz_batch, srDirections);
                            break;
                        case DevelopFormCodes.CORESP_FORM:  //заочная
                            calcSingleDirection(direction, line.z_batch, srDirections);
                            break;
                    }
                }
                ou_line.Accum(line);
                printSingleLine(sheet, excelRowCount++, line, rowFirstColFormat, rowFormat);
            }
            tot_line.Accum(ou_line);
            //Итого по факультету
            printSingleLine(sheet, excelRowCount++, ou_line, rowFirstColTotFormat, rowTotFormat);
        }
        //Общий итог
        if (model.getFormativeOrgUnit() == null)
            printSingleLine(sheet, excelRowCount++, tot_line, rowFirstColTotFormat, rowTotFormat);

        excelRowCount += 2;

        sheet.mergeCells(0, excelRowCount, 10, excelRowCount + 2);
        sheet.mergeCells(11, excelRowCount, 18, excelRowCount + 2);

        sheet.addCell(new Label(0, excelRowCount, DaoFacade.getDaoPrintUtil().getDelegatePostFromContext(model.getEnrollmentCampaign(), model.getFormativeOrgUnit()), signFormat));
        sheet.addCell(new Label(11, excelRowCount, DaoFacade.getDaoPrintUtil().getDelegateIofFromContext(model.getEnrollmentCampaign(), model.getFormativeOrgUnit()), signFormatRight));

        workbook.write();
        workbook.close();
        model.setContent(out.toByteArray());
        model.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_VND_MS_EXCEL);
        model.setFileName("EntrantDump.xls");

        getSession().clear();
    }

    private void printSingleLine(WritableSheet sheet, int row, SingleLine line, WritableCellFormat firstColFormat, WritableCellFormat rowFormat) throws Exception {
        sheet.addCell(new Label(0, row, line.name, firstColFormat));
        Integer col = 1;
        for (int i = 0; i < 6; i++)
            sheet.addCell(new Number(col++, row, line.o_batch[i], rowFormat));

        for (int i = 0; i < 6; i++)
            sheet.addCell(new Number(col++, row, line.oz_batch[i], rowFormat));

        for (int i = 0; i < 6; i++)
            sheet.addCell(new Number(col++, row, line.z_batch[i], rowFormat));
    }

    private void calcSingleDirection(RequestedEnrollmentDirection direction, Integer[] arr, List<RequestedEnrollmentDirection> srDirections) {
        //Бюджет
        if (direction.getCompensationType().getCode().equals(UniDefines.COMPENSATION_TYPE_BUDGET)) {
            arr[0]++;
            if (direction.getCompetitionKind().getCode().equals(UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES))
                arr[2]++;  // без ВИ
            else if (srDirections.contains(direction))
                arr[3]++; //квота (имееет особые права)
            else if (direction.isTargetAdmission())
                arr[4]++; // Целевики
            else
                arr[1]++;  // все остальные
        }
        else { // договор
            arr[5]++;
        }
    }

    private class SingleLine {
        public String name;
        public Integer[] o_batch = new Integer[]{0, 0, 0, 0, 0, 0};
        public Integer[] oz_batch = new Integer[]{0, 0, 0, 0, 0, 0};
        public Integer[] z_batch = new Integer[]{0, 0, 0, 0, 0, 0};

        public void Accum(SingleLine line) {
            for (int i = 0; i < 6; i++)
                this.o_batch[i] += line.o_batch[i];
            for (int i = 0; i < 6; i++)
                this.oz_batch[i] += line.oz_batch[i];
            for (int i = 0; i < 6; i++)
                this.z_batch[i] += line.z_batch[i];
        }

        @Override
        public String toString() {
            StringBuilder str = new StringBuilder();
            str.append("{[");
            for (int i = 0; i < 6; i++)
                str.append(o_batch[i].toString()).append(i < 5 ? "," : "");
            str.append("] ");
            str.append("[");
            for (int i = 0; i < 6; i++)
                str.append(oz_batch[i].toString()).append(i < 5 ? "," : "");
            str.append("] ");
            str.append("[");
            for (int i = 0; i < 6; i++)
                str.append(z_batch[i].toString()).append(i < 5 ? "," : "");
            str.append("]}");

            return str.toString();
        }

    }

}