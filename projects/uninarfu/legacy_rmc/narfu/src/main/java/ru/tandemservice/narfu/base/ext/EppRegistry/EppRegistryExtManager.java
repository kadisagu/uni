/* $Id:$ */
package ru.tandemservice.narfu.base.ext.EppRegistry;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.narfu.base.ext.EppRegistry.ui.Base.NarfuRegistryListAddon;
import ru.tandemservice.uni.base.ui.OrgUnitUIPresenter;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;

import static org.tandemframework.hibsupport.dql.DQLExpressions.exists;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Irina Ugfeld
 * @since 10.03.2016
 */
@Configuration
public class EppRegistryExtManager extends BusinessObjectExtensionManager {
    public static EppRegistryExtManager instance() {
        return instance(EppRegistryExtManager.class);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> getEppWorkPlanHandler() {
        return new EntityComboDataSourceHandler(getName(), EppWorkPlan.class)
                .customize((alias, dql, context, filter) ->{
                    Long orgUnitId = context.get(OrgUnitUIPresenter.INPUT_PARAM_ORG_UNIT_ID);
                    if (orgUnitId != null) {
                        dql.where(exists(EppWorkPlanRegistryElementRow.class,
                                EppWorkPlanRegistryElementRow.workPlan().s(), property(alias),
                                EppWorkPlanRegistryElementRow.registryElementPart().registryElement().owner().s(), orgUnitId
                        ));
                    }
                    return dql;
                })
                .where(EppWorkPlan.parent().eduPlanVersion(), NarfuRegistryListAddon.SETTING_EDU_PLAN_VERSION, true)
                .filter(EppWorkPlan.number())
                .order(EppWorkPlan.number())
                .order(EppWorkPlan.term().intValue());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> getEduPlanVersionHandler() {
        return new EntityComboDataSourceHandler(getName(), EppEduPlanVersion.class)
                .customize((alias, dql, context, filter) ->{
                    Long orgUnitId = context.get(OrgUnitUIPresenter.INPUT_PARAM_ORG_UNIT_ID);
                    if (orgUnitId != null) {
                        dql.where(exists(EppEpvRegistryRow.class,
                                EppEpvRegistryRow.owner().eduPlanVersion().s(), property(alias),
                                EppEpvRegistryRow.registryElementOwner().id().s(), orgUnitId
                        ));
                    }
                    return dql;
                })
                .filter(EppEduPlanVersion.eduPlan().number())
                .order(EppEduPlanVersion.eduPlan().number())
                .order(EppEduPlanVersion.number());

    }
}
