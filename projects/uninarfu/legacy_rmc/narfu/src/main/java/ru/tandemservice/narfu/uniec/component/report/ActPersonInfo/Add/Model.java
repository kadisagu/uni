package ru.tandemservice.narfu.uniec.component.report.ActPersonInfo.Add;

import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.narfu.uniec.entity.report.NarfuActPersonInfo;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil.Parameters;

import java.util.List;

public class Model
        implements ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil.Model, IEnrollmentCampaignSelectModel
{

    private NarfuActPersonInfo _report;
    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil.Parameters _parameters;
    private boolean _formativeOrgUnitActive;
    private ISelectModel _formativeOrgUnitListModel;
    private List<OrgUnit> _formativeOrgUnitList;
    private IPrincipalContext _principalContext;

    public Model()
    {
        _report = new NarfuActPersonInfo();

    }

    public IPrincipalContext getPrincipalContext()
    {
        return _principalContext;
    }

    public void setPrincipalContext(IPrincipalContext principalContext)
    {
        _principalContext = principalContext;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign() {
        return _report.getEnrollmentCampaign();

    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList() {
        return _enrollmentCampaignList;
    }

    @Override
    public IDataSettings getSettings()
    {
        return null;
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign) {
        _report.setEnrollmentCampaign(enrollmentCampaign);

    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList) {
        _enrollmentCampaignList = enrollmentCampaignList;

    }

    @Override
    public Parameters getParameters() {
        return _parameters;
    }

    public void setParameters(ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil.Parameters parameters)
    {
        _parameters = parameters;
    }


    @Override
    public List<EnrollmentCampaign> getSelectedEnrollmentCampaignList() {
        return MultiEnrollmentDirectionUtil.getSingletonList(getReport().getEnrollmentCampaign());
    }

    public NarfuActPersonInfo getReport() {
        return _report;
    }

    @Override
    public List<OrgUnit> getSelectedFormativeOrgUnitList() {
        return isFormativeOrgUnitActive() ? getFormativeOrgUnitList() : null;
    }

    @Override
    public List<OrgUnit> getSelectedTerritorialOrgUnitList() {
        return null;
    }

    @Override
    public List<EducationLevelsHighSchool> getSelectedEducationLevelHighSchoolList() {
        return null;
    }

    @Override
    public List<DevelopForm> getSelectedDevelopFormList() {
        return null;
    }

    @Override
    public List<DevelopCondition> getSelectedDevelopConditionList() {
        return null;
    }

    @Override
    public List<DevelopTech> getSelectedDevelopTechList() {
        return null;
    }

    @Override
    public List<DevelopPeriod> getSelectedDevelopPeriodList() {
        return null;
    }


    public boolean isFormativeOrgUnitActive()
    {
        return _formativeOrgUnitActive;
    }

    public void setFormativeOrgUnitActive(boolean formativeOrgUnitActive)
    {
        _formativeOrgUnitActive = formativeOrgUnitActive;
    }


    public ISelectModel getFormativeOrgUnitListModel()
    {
        return _formativeOrgUnitListModel;
    }

    public void setFormativeOrgUnitListModel(ISelectModel formativeOrgUnitListModel)
    {
        _formativeOrgUnitListModel = formativeOrgUnitListModel;
    }

    public List<OrgUnit> getFormativeOrgUnitList()
    {
        return _formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList)
    {
        _formativeOrgUnitList = formativeOrgUnitList;
    }

}
