package ru.tandemservice.narfu.migration;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_narfu_1x0x0_9to10 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.10"),
                        new ScriptDependency("org.tandemframework.shared", "1.1.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.1.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // модуль aspirantrmc отключен - удаляем все его сущности

        // убедиться, что модуль и в самом деле удален
        if (ApplicationRuntime.hasModule("aspirantrmc"))
            throw new RuntimeException("Module 'aspirantrmc' is not deleted");

        MigrationUtils.removeModuleFromVersion_s(tool, "aspirantrmc");

        // удалить сущность outerParticipant
        {
            // удалить записи из базовых таблиц
            tool.deleteRowsFromParentTables("outerparticipant_t", "personrole_t");

            // удалить таблицу
            tool.dropTable("outerparticipant_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("outerParticipant");

        }

        // удалить сущность educationLevelScientific
        {
            // таблицы у сущности нет
            // удалить код сущности
            tool.entityCodes().delete("educationLevelScientific");

        }

        // удалить сущность typePublication
        {
            // удалить таблицу
            tool.dropTable("typepublication_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("typePublication");

        }

        // удалить сущность typeParticipation
        {
            // удалить таблицу
            tool.dropTable("typeparticipation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("typeParticipation");

        }

        // удалить сущность typeInvention
        {
            // удалить таблицу
            tool.dropTable("typeinvention_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("typeInvention");

        }

        // удалить сущность typeAction
        {
            // удалить таблицу
            tool.dropTable("typeaction_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("typeAction");

        }

        // удалить сущность statusMission
        {
            // удалить таблицу
            tool.dropTable("statusmission_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("statusMission");

        }

        // удалить сущность statusAction
        {
            // удалить таблицу
            tool.dropTable("statusaction_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("statusAction");

        }

        // удалить сущность staffDissertationSoviet
        {
            // удалить таблицу
            tool.dropTable("staffdissertationsoviet_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("staffDissertationSoviet");

        }

        // удалить сущность scientificResearchParticipantType
        {
            // удалить таблицу
            tool.dropTable("scntfcrsrchprtcpnttyp_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("scientificResearchParticipantType");

        }

        // удалить сущность scientificResearch
        {
            // удалить таблицу
            tool.dropTable("scientificresearch_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("scientificResearch");

        }

        // удалить сущность scientificPublication
        {
            // удалить таблицу
            tool.dropTable("scientificpublication_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("scientificPublication");

        }

        // удалить сущность scientificPatent
        {
            // удалить таблицу
            tool.dropTable("scientificpatent_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("scientificPatent");

        }

        // удалить сущность scienceParticipant
        {
            // удалить таблицу
            tool.dropTable("scienceparticipant_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("scienceParticipant");

        }

        // удалить сущность relEducationLevelScientificDissertationSoviet
        {
            // удалить таблицу
            tool.dropTable("rledctnlvlscntfcdssrttnsvt_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("relEducationLevelScientificDissertationSoviet");

        }

        // удалить сущность relDissertationSovietStaffPost
        {
            // удалить таблицу
            tool.dropTable("rldssrttnsvtstffpst_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("relDissertationSovietStaffPost");

        }

        // удалить сущность publishingHouse
        {
            // удалить таблицу
            tool.dropTable("publishinghouse_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("publishingHouse");

        }

        // удалить сущность practiceWork
        {
            // удалить таблицу
            tool.dropTable("practicework_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("practiceWork");

        }

        // удалить сущность postDissertationSoviet
        {
            // удалить таблицу
            tool.dropTable("postdissertationsoviet_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("postDissertationSoviet");

        }

        // удалить сущность leadOrganization
        {
            // удалить таблицу
            tool.dropTable("leadorganization_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("leadOrganization");

        }

        // удалить сущность financing
        {
            // удалить таблицу
            tool.dropTable("financing_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("financing");

        }

        // удалить сущность dissertationSoviet
        {
            // удалить таблицу
            tool.dropTable("dissertationsoviet_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dissertationSoviet");

        }


    }
}