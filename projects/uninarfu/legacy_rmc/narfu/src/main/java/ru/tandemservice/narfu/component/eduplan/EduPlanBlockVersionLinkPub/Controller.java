package ru.tandemservice.narfu.component.eduplan.EduPlanBlockVersionLinkPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BooleanColumn;
import org.tandemframework.core.view.list.column.IEntityHandler;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.narfu.entity.EppEduPlanVersionBlockLink;
import ru.tandemservice.narfu.entity.VersionBlockDisciplinesLink;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;

import java.util.List;

public class Controller extends AbstractBusinessController<IDAO, Model> {
    final IEntityHandler editRemove = entity -> {
        IEppEpvRowWrapper row = (IEppEpvRowWrapper) entity;
        return !(row.getRow() instanceof EppEpvRegistryRow);

    };
    final IEntityHandler editDisable = entity -> {
        IEppEpvRowWrapper row = (IEppEpvRowWrapper) entity;
        return !(row.getRow() instanceof EppEpvRegistryRow) || ((EppEpvRegistryRow) row.getRow()).getRegistryElement() == null;

    };

    public void onRefreshComponent(IBusinessComponent component) {
        getDao().prepare(getModel(component));
        prepareDataSource(component);
    }

    public void onClickApply(IBusinessComponent component) {
        getDao().update(getModel(component));
        deactivate(component);
    }

    private void prepareDataSource(IBusinessComponent component) {
        Model model = component.getModel();

        StaticListDataSource<IEppEpvRowWrapper> dataSource = model.getRowDataSource();


        dataSource.addColumn(getBooleanColumn(model.getLinkHolder().getValue()));
        dataSource.addColumn(getEditColumn("editRow", "Редактировать", "onClickEdit"));
        model.setRowDataSource(dataSource);
    }


    public void onClickEdit(IBusinessComponent component) {
        Long id = component.getListenerParameter();
        IEntity entity = getDao().get(id);
        UniMap map = new UniMap();

        if (entity instanceof EppEpvRegistryRow) {
            EppEpvRegistryRow discipline = (EppEpvRegistryRow) entity;
            map.add("elementId", discipline.getRegistryElement().getId()).add("linkId", getModel(component).getLinkHolder().getId());
            ContextLocal.createDesktop("PersonShellDialog",
                                       new ComponentActivator("ru.tandemservice.narfu.component.eduplan.EduPlanBlockVersionLinkEditRow", map));

        }


    }

    public void onClickAutomatic(IBusinessComponent component) {
        getDao().makeLinks(getModel(component));
        component.refresh();
    }


    public IndicatorColumn getEditColumn(final String permissionkey, final String actionTitle, final String listener) {

        return (IndicatorColumn) new IndicatorColumn("", "action." + listener, listener) {
            @Override
            public IndicatorColumn.Item getContent(final IEntity entity) {
                if (editRemove.handle(entity)) {
                    return null;
                }
                return new IndicatorColumn.Item(ActionColumn.EDIT, actionTitle);
            }
        }.setImageHeader(false).setDisableHandler(editRemove).setOrderable(false).setRequired(true).setDisableHandler(editDisable).setClickable(true).setWidth(1);
    }

    protected BooleanColumn getBooleanColumn(final EppEduPlanVersionBlockLink link) {
        return new BooleanColumn("Сопоставление", "edit") {
            @Override
            public Item getContent(IEntity ientity) {
                if (editRemove.handle(ientity)) {
                    return null;
                }
                IEppEpvRowWrapper row = (IEppEpvRowWrapper) ientity;


                DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(VersionBlockDisciplinesLink.class, "bl")
                        .where(DQLExpressions.eq(DQLExpressions.property(VersionBlockDisciplinesLink.versionBlockLink().fromAlias("bl")), DQLExpressions.value(link)))
                        .where(DQLExpressions.eq(DQLExpressions.property(VersionBlockDisciplinesLink.controlActionSrc().part().registryElement().fromAlias("bl")), DQLExpressions.value(((EppEpvRegistryRow) row.getRow()).getRegistryElement())));
                List<VersionBlockDisciplinesLink> list = IUniBaseDao.instance.get().getList(dql);
                if (list.size() > 0)
                    return getTrueItem();
                else return getFalseItem();

            }
        };
    }


}
