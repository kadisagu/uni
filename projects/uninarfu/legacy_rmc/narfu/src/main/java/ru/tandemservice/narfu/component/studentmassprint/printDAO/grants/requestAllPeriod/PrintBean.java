package ru.tandemservice.narfu.component.studentmassprint.printDAO.grants.requestAllPeriod;

import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import ru.tandemservice.narfu.component.studentmassprint.GrantsMassPrint.Model;
import ru.tandemservice.narfu.component.studentmassprint.printDAO.grants.AbstractGrantRequestReport;


public class PrintBean extends AbstractGrantRequestReport<Model>
{

    @Override
    protected RtfInjectModifier createInjectModifier(Model model) {
        RtfInjectModifier im = super.createInjectModifier(model);


        return im;
    }


    @Override
    protected RtfTableModifier createTableModifier(Model model) {

        RtfTableModifier tm = super.createTableModifier(model);


        return tm;
    }


}
