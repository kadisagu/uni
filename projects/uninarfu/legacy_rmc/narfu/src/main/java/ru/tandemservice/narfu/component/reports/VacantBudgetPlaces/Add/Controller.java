package ru.tandemservice.narfu.component.reports.VacantBudgetPlaces.Add;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController<IDAO, Model> {
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = getModel(component);
        getDao().prepare(model);
    }

    public void onClickApply(IBusinessComponent component)
    {
        Model model = (Model) getModel(component);
        ((IDAO) getDao()).createReport(model);

        deactivate(component);
    }
}
