package ru.tandemservice.narfu.uniec.component.report.EntrantRegistration.EntrantRegistrationAddExt;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.narfu.uniec.entity.report.NarfuEntrantRegistration;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.List;

public class Model implements IEnrollmentCampaignSelectModel {

    private EnrollmentCampaign enrollmentCampaign;
    private List<EnrollmentCampaign> enrollmentCampaignList;

    private NarfuEntrantRegistration report;

    private boolean orgUnitActive;
    private ISelectModel orgUnitModel;
    private List<OrgUnit> orgUnitList;

    @Override
    public EnrollmentCampaign getEnrollmentCampaign() {
        return enrollmentCampaign;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList() {
        return enrollmentCampaignList;
    }

    @Override
    public IDataSettings getSettings() {
        return null;
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentcampaign) {
        this.enrollmentCampaign = enrollmentcampaign;

    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> list) {
        this.enrollmentCampaignList = list;
    }

    public ISelectModel getOrgUnitModel() {
        return orgUnitModel;
    }

    public void setOrgUnitModel(ISelectModel orgUnitModel) {
        this.orgUnitModel = orgUnitModel;
    }

    public List<OrgUnit> getOrgUnitList() {
        return orgUnitList;
    }

    public void setOrgUnitList(List<OrgUnit> orgUnitList) {
        this.orgUnitList = orgUnitList;
    }

    public NarfuEntrantRegistration getReport() {
        return report;
    }

    public void setReport(NarfuEntrantRegistration report) {
        this.report = report;
    }

    public boolean isOrgUnitActive() {
        return orgUnitActive;
    }

    public void setOrgUnitActive(boolean orgUnitActive) {
        this.orgUnitActive = orgUnitActive;
    }

}
