package ru.tandemservice.narfu.component.entrant.EntrantPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {

        ru.tandemservice.uniec.component.entrant.EntrantPub.Model model = component.getModel();

        DynamicListDataSource<PreliminaryEnrollmentStudent> dataSource = model.getPreliminaryEnrollmentDataSource();
        if (dataSource != null) {
            if (dataSource.getColumn("printEnrllDoc") == null) {
                dataSource.addColumn(getPrintColumn("narfuEntrantPub:onClickPrintEnrollmentDocument", "Печать справки о зачислении").setPermissionKey("printEntrantEnrollmentDocument"));
                model.setPreliminaryEnrollmentDataSource(dataSource);
            }
        }
        prepareEntrantStateExamCertificatesDataSourceExt(component);

    }

    public IndicatorColumn getPrintColumn(String listener, String title)
    {
        IndicatorColumn column = new IndicatorColumn("printEnrllDoc", title, null, listener, (String) null);
        column.setOrderable(false);
        column.setImageHeader(false);
        column.defaultIndicator(new org.tandemframework.core.view.list.column.IndicatorColumn.Item("printer", title));
        column.setDisableSecondSubmit(false);
        return column;
    }

    public void prepareEntrantStateExamCertificatesDataSourceExt(IBusinessComponent component)
    {

        ru.tandemservice.uniec.component.entrant.EntrantPub.Model model = component.getModel();
        DynamicListDataSource<EntrantStateExamCertificate> dataSource = model.getEntrantStateExamCertificatesDataSource();
        if (dataSource != null) {
            if (dataSource.getColumn("examPlace") == null) {
                dataSource.addColumn(new BlockColumn("examPlace", "Место сдачи"), 1);
                model.setEntrantStateExamCertificatesDataSource(dataSource);
            }
        }

    }

    public void onClickPrintEnrollmentDocument(IBusinessComponent component) {
        ContextLocal.createDesktop("PersonShellDialog",
                                   new ComponentActivator("ru.tandemservice.narfu.component.entrant.EnrollmentDocumentDialog",
                                                          new UniMap().add("id", (Long) component.getListenerParameter())));
    }
}
