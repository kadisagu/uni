package ru.tandemservice.narfu.component.reports.OrgUnitAspirantReport.Add;

import ru.tandemservice.narfu.uniec.entity.report.OrgUnitAspirantReport;
import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {

    public OrgUnitAspirantReport createReport(Model model);
}
