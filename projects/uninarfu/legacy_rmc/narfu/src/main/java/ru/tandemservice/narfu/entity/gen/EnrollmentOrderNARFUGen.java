package ru.tandemservice.narfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.narfu.entity.EnrollmentOrderNARFU;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение ПРИКАЗА О ЗАЧИСЛЕНИИ АБИТУРИЕНТОВ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrollmentOrderNARFUGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.narfu.entity.EnrollmentOrderNARFU";
    public static final String ENTITY_NAME = "enrollmentOrderNARFU";
    public static final int VERSION_HASH = -390718482;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_ORDER = "enrollmentOrder";
    public static final String P_EXECUTOR_POST = "executorPost";
    public static final String P_EXECUTOR_F_I_O = "executorFIO";
    public static final String P_PROTOCOL_NUMBER = "protocolNumber";
    public static final String P_PROTOCOL_DATE = "protocolDate";
    public static final String L_COURSE = "course";

    private EnrollmentOrder _enrollmentOrder;     // Приказ о зачислении абитуриентов
    private String _executorPost;     // Должномть
    private String _executorFIO;     // ФИО должностного лица
    private String _protocolNumber;     // Номер протокола
    private Date _protocolDate;     // Дата протокола
    private Course _course;     // Курс

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приказ о зачислении абитуриентов.
     */
    public EnrollmentOrder getEnrollmentOrder()
    {
        return _enrollmentOrder;
    }

    /**
     * @param enrollmentOrder Приказ о зачислении абитуриентов.
     */
    public void setEnrollmentOrder(EnrollmentOrder enrollmentOrder)
    {
        dirty(_enrollmentOrder, enrollmentOrder);
        _enrollmentOrder = enrollmentOrder;
    }

    /**
     * @return Должномть.
     */
    @Length(max=255)
    public String getExecutorPost()
    {
        return _executorPost;
    }

    /**
     * @param executorPost Должномть.
     */
    public void setExecutorPost(String executorPost)
    {
        dirty(_executorPost, executorPost);
        _executorPost = executorPost;
    }

    /**
     * @return ФИО должностного лица.
     */
    @Length(max=255)
    public String getExecutorFIO()
    {
        return _executorFIO;
    }

    /**
     * @param executorFIO ФИО должностного лица.
     */
    public void setExecutorFIO(String executorFIO)
    {
        dirty(_executorFIO, executorFIO);
        _executorFIO = executorFIO;
    }

    /**
     * @return Номер протокола.
     */
    @Length(max=255)
    public String getProtocolNumber()
    {
        return _protocolNumber;
    }

    /**
     * @param protocolNumber Номер протокола.
     */
    public void setProtocolNumber(String protocolNumber)
    {
        dirty(_protocolNumber, protocolNumber);
        _protocolNumber = protocolNumber;
    }

    /**
     * @return Дата протокола.
     */
    public Date getProtocolDate()
    {
        return _protocolDate;
    }

    /**
     * @param protocolDate Дата протокола.
     */
    public void setProtocolDate(Date protocolDate)
    {
        dirty(_protocolDate, protocolDate);
        _protocolDate = protocolDate;
    }

    /**
     * @return Курс.
     */
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrollmentOrderNARFUGen)
        {
            setEnrollmentOrder(((EnrollmentOrderNARFU)another).getEnrollmentOrder());
            setExecutorPost(((EnrollmentOrderNARFU)another).getExecutorPost());
            setExecutorFIO(((EnrollmentOrderNARFU)another).getExecutorFIO());
            setProtocolNumber(((EnrollmentOrderNARFU)another).getProtocolNumber());
            setProtocolDate(((EnrollmentOrderNARFU)another).getProtocolDate());
            setCourse(((EnrollmentOrderNARFU)another).getCourse());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrollmentOrderNARFUGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrollmentOrderNARFU.class;
        }

        public T newInstance()
        {
            return (T) new EnrollmentOrderNARFU();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentOrder":
                    return obj.getEnrollmentOrder();
                case "executorPost":
                    return obj.getExecutorPost();
                case "executorFIO":
                    return obj.getExecutorFIO();
                case "protocolNumber":
                    return obj.getProtocolNumber();
                case "protocolDate":
                    return obj.getProtocolDate();
                case "course":
                    return obj.getCourse();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentOrder":
                    obj.setEnrollmentOrder((EnrollmentOrder) value);
                    return;
                case "executorPost":
                    obj.setExecutorPost((String) value);
                    return;
                case "executorFIO":
                    obj.setExecutorFIO((String) value);
                    return;
                case "protocolNumber":
                    obj.setProtocolNumber((String) value);
                    return;
                case "protocolDate":
                    obj.setProtocolDate((Date) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentOrder":
                        return true;
                case "executorPost":
                        return true;
                case "executorFIO":
                        return true;
                case "protocolNumber":
                        return true;
                case "protocolDate":
                        return true;
                case "course":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentOrder":
                    return true;
                case "executorPost":
                    return true;
                case "executorFIO":
                    return true;
                case "protocolNumber":
                    return true;
                case "protocolDate":
                    return true;
                case "course":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentOrder":
                    return EnrollmentOrder.class;
                case "executorPost":
                    return String.class;
                case "executorFIO":
                    return String.class;
                case "protocolNumber":
                    return String.class;
                case "protocolDate":
                    return Date.class;
                case "course":
                    return Course.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrollmentOrderNARFU> _dslPath = new Path<EnrollmentOrderNARFU>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrollmentOrderNARFU");
    }
            

    /**
     * @return Приказ о зачислении абитуриентов.
     * @see ru.tandemservice.narfu.entity.EnrollmentOrderNARFU#getEnrollmentOrder()
     */
    public static EnrollmentOrder.Path<EnrollmentOrder> enrollmentOrder()
    {
        return _dslPath.enrollmentOrder();
    }

    /**
     * @return Должномть.
     * @see ru.tandemservice.narfu.entity.EnrollmentOrderNARFU#getExecutorPost()
     */
    public static PropertyPath<String> executorPost()
    {
        return _dslPath.executorPost();
    }

    /**
     * @return ФИО должностного лица.
     * @see ru.tandemservice.narfu.entity.EnrollmentOrderNARFU#getExecutorFIO()
     */
    public static PropertyPath<String> executorFIO()
    {
        return _dslPath.executorFIO();
    }

    /**
     * @return Номер протокола.
     * @see ru.tandemservice.narfu.entity.EnrollmentOrderNARFU#getProtocolNumber()
     */
    public static PropertyPath<String> protocolNumber()
    {
        return _dslPath.protocolNumber();
    }

    /**
     * @return Дата протокола.
     * @see ru.tandemservice.narfu.entity.EnrollmentOrderNARFU#getProtocolDate()
     */
    public static PropertyPath<Date> protocolDate()
    {
        return _dslPath.protocolDate();
    }

    /**
     * @return Курс.
     * @see ru.tandemservice.narfu.entity.EnrollmentOrderNARFU#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    public static class Path<E extends EnrollmentOrderNARFU> extends EntityPath<E>
    {
        private EnrollmentOrder.Path<EnrollmentOrder> _enrollmentOrder;
        private PropertyPath<String> _executorPost;
        private PropertyPath<String> _executorFIO;
        private PropertyPath<String> _protocolNumber;
        private PropertyPath<Date> _protocolDate;
        private Course.Path<Course> _course;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приказ о зачислении абитуриентов.
     * @see ru.tandemservice.narfu.entity.EnrollmentOrderNARFU#getEnrollmentOrder()
     */
        public EnrollmentOrder.Path<EnrollmentOrder> enrollmentOrder()
        {
            if(_enrollmentOrder == null )
                _enrollmentOrder = new EnrollmentOrder.Path<EnrollmentOrder>(L_ENROLLMENT_ORDER, this);
            return _enrollmentOrder;
        }

    /**
     * @return Должномть.
     * @see ru.tandemservice.narfu.entity.EnrollmentOrderNARFU#getExecutorPost()
     */
        public PropertyPath<String> executorPost()
        {
            if(_executorPost == null )
                _executorPost = new PropertyPath<String>(EnrollmentOrderNARFUGen.P_EXECUTOR_POST, this);
            return _executorPost;
        }

    /**
     * @return ФИО должностного лица.
     * @see ru.tandemservice.narfu.entity.EnrollmentOrderNARFU#getExecutorFIO()
     */
        public PropertyPath<String> executorFIO()
        {
            if(_executorFIO == null )
                _executorFIO = new PropertyPath<String>(EnrollmentOrderNARFUGen.P_EXECUTOR_F_I_O, this);
            return _executorFIO;
        }

    /**
     * @return Номер протокола.
     * @see ru.tandemservice.narfu.entity.EnrollmentOrderNARFU#getProtocolNumber()
     */
        public PropertyPath<String> protocolNumber()
        {
            if(_protocolNumber == null )
                _protocolNumber = new PropertyPath<String>(EnrollmentOrderNARFUGen.P_PROTOCOL_NUMBER, this);
            return _protocolNumber;
        }

    /**
     * @return Дата протокола.
     * @see ru.tandemservice.narfu.entity.EnrollmentOrderNARFU#getProtocolDate()
     */
        public PropertyPath<Date> protocolDate()
        {
            if(_protocolDate == null )
                _protocolDate = new PropertyPath<Date>(EnrollmentOrderNARFUGen.P_PROTOCOL_DATE, this);
            return _protocolDate;
        }

    /**
     * @return Курс.
     * @see ru.tandemservice.narfu.entity.EnrollmentOrderNARFU#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

        public Class getEntityClass()
        {
            return EnrollmentOrderNARFU.class;
        }

        public String getEntityName()
        {
            return "enrollmentOrderNARFU";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
