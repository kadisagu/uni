package ru.tandemservice.narfu.migration;

import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.unitutor.entity.EppEduPlanVersionExt;

import java.sql.ResultSet;
import java.sql.Statement;

/**
 * @author Ivan Anishchenko <ivan.anishchenko@citeck.ru>
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_narfu_1x0x0_49to50 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies() {
        return new ScriptDependency[]{
                new ScriptDependency("org.tandemframework", "1.6.14"),
                new ScriptDependency("org.tandemframework.shared", "1.5.2"),
                new ScriptDependency("ru.tandemservice.uni.product", "2.5.2"),
                new ScriptDependency("ru.tandemservice.uni.project", "2.5.2")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception {

        if (tool.tableExists("course_t") && tool.tableExists("qualifications_t") && tool.tableExists("epp_eduplan_t") && tool.tableExists("educationlevelshighschool_t")
                && tool.tableExists("educationlevels_t") && tool.tableExists("epp_eduplan_ver_t") && tool.tableExists("eppeduplanversionext_t"))
        {

            // Подготовим необходимые для дальнейшего заполнения значения из справочника "Курсы годов обучения"
            // Бакалавр
            Statement bakalavrStmt = tool.getConnection().createStatement();
            bakalavrStmt.execute("SELECT id FROM course_t WHERE code_p = '3'");
            ResultSet bakalavrRS = bakalavrStmt.getResultSet();

            Long bakalavrCourseId = null;
            Long specialistCourseId = null;

            if (bakalavrRS.next()) {
                bakalavrCourseId = bakalavrRS.getLong(1);
            }

            // Специалист
            Statement specialistStmt = tool.getConnection().createStatement();
            specialistStmt.execute("SELECT id FROM course_t WHERE code_p = '2'");
            ResultSet specialistRS = specialistStmt.getResultSet();

            if (specialistRS.next()) {
                specialistCourseId = specialistRS.getLong(1);
            }

            // Версии учебных планов и код квалификации
            String selectQuery = "SELECT eduPlanVersion.id, q.code_p FROM epp_eduplan_ver_t AS eduPlanVersion " +
                    "INNER JOIN epp_eduplan_t eduPlan ON eduPlanVersion.eduplan_id = eduPlan.id " +
                    "INNER JOIN educationlevelshighschool_t elhs ON eduPlan.educationlevelhighschool_id = elhs.id " +
                    "INNER JOIN educationlevels_t el ON elhs.educationlevel_id = el.id " +
                    "INNER JOIN qualifications_t q ON el.qualification_id = q.id";

            Statement stmt = tool.getConnection().createStatement();
            stmt.execute(selectQuery);
            ResultSet resultSet = stmt.getResultSet();

            short code = EntityRuntime.getMeta(EppEduPlanVersionExt.class).getEntityCode(); // дискриминатор

            while (resultSet.next()) {
                Long eduPlanVersionId = resultSet.getLong(1);
                String qualificationCode = resultSet.getString(2);

                String insertQuery = "insert into eppeduplanversionext_t (id, discriminator, eduplanversion_id, course_id) values (?, ?, ?, ?)";

                // Бакалавр
                if (isBakalavr(qualificationCode)) {
                    tool.executeUpdate(insertQuery, EntityIDGenerator.generateNewId(code), code, eduPlanVersionId, bakalavrCourseId);
                }

                // Специалист
                if (isSpecialist(qualificationCode)) {
                    tool.executeUpdate(insertQuery, EntityIDGenerator.generateNewId(code), code, eduPlanVersionId, specialistCourseId);
                }

            }
        }
    }

    private boolean isBakalavr(String qualificationCode) {
        return QualificationsCodes.BAKALAVR.equals(qualificationCode);
    }

    private boolean isSpecialist(String qualificationCode) {
        return QualificationsCodes.SPETSIALIST.equals(qualificationCode);
    }
}