package ru.tandemservice.narfu.base.ext.ReportPersonMilitary.ui.Add;

import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportPrintBlock;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportPrintInfo;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.ReportDQL;
import ru.tandemservice.narfu.base.ext.ReportPersonMilitary.ui.Add.print.PersonMilitaryPrintBlock;

public class ReportPersonMilitaryAddUI extends ru.tandemservice.militaryrmc.base.bo.ReportPersonMilitary.ui.Add.ReportPersonMilitaryAddUI
{

    private IReportPrintBlock personMilitaryPrintBlock = new PersonMilitaryPrintBlock();
    private String selectedTab;

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (this.isPersonMilitaryScheet())
        {
            printInfo.addSheet(PERSON_MILITARY_SCHEET);
            // фильтры, модифицирующие запросы согласно установленным фильтрам
            this.getPersonMilitaryData().modify(dql, printInfo);
            // печатные блоки, модифицирующие запросы. Создают печатные колонки
            getPersonMilitaryPrintBlock().modify(dql, printInfo);
        }
    }

    public IReportPrintBlock getPersonMilitaryPrintBlock()
    {
        return personMilitaryPrintBlock;
    }

    public void setPersonMilitaryPrintBlock(IReportPrintBlock personMilitaryPrintBlock)
    {
        this.personMilitaryPrintBlock = personMilitaryPrintBlock;
    }

    public String getSelectedTab()
    {
        return selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        this.selectedTab = selectedTab;
    }
}
