package ru.tandemservice.narfu.component.settings.YearPartEndDate.AddEdit;

import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.narfu.entity.YearPartStartEndDate;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;

import java.util.List;


@Input({
        @org.tandemframework.core.component.Bind(key = "EducationYear", binding = "educationYearId"),
        @org.tandemframework.core.component.Bind(key = "Part", binding = "yearDistributionPartId"),
        @org.tandemframework.core.component.Bind(key = "Id", binding = "entityId")
})
public class Model {

    private ISelectModel _educationYearList;
    private List<YearDistributionPart> _yearDistributionPartList;
    private Long _entityId;

    private Long yearDistributionPartId;
    private Long educationYearId;


    private YearPartStartEndDate yearPartStartEndDate = null;

    public ISelectModel getEducationYearList() {
        return _educationYearList;
    }

    public void setEducationYearList(ISelectModel _educationYearList) {
        this._educationYearList = _educationYearList;
    }

    public List<YearDistributionPart> getYearDistributionPartList() {
        return _yearDistributionPartList;
    }

    public void setYearDistributionPartList(List<YearDistributionPart> _yearDistributionPart) {
        this._yearDistributionPartList = _yearDistributionPart;
    }

    public Long getEntityId() {
        return _entityId;
    }

    public void setEntityId(Long _entityId) {
        this._entityId = _entityId;
    }

    public void setYearPartStartEndDate(YearPartStartEndDate yearPartStartEndDate) {
        this.yearPartStartEndDate = yearPartStartEndDate;
    }

    public YearPartStartEndDate getYearPartStartEndDate() {
        return yearPartStartEndDate;
    }

    public void setYearDistributionPartId(Long yearDistributionPartId) {
        this.yearDistributionPartId = yearDistributionPartId;
    }

    public Long getYearDistributionPartId() {
        return yearDistributionPartId;
    }

    public void setEducationYearId(Long educationYearId) {
        this.educationYearId = educationYearId;
    }

    public Long getEducationYearId() {
        return educationYearId;
    }

    public boolean isEditForm()
    {
        if (_entityId == null)
            return false;
        else
            return true;
    }
}
