package ru.tandemservice.narfu.component.documents.d1004.Add;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.narfu.entity.StudentDocumentRMC;
import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseDAO;
import ru.tandemservice.uni.component.documents.DocumentAddBase.IDocumentAddBaseDAO;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.UniScriptItem;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.StudentDocument;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

@SuppressWarnings("rawtypes")
public class DAO extends DocumentAddBaseDAO<Model> implements IDocumentAddBaseDAO<Model>, IDAO {

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        setEduEnrData(model);
        model.setFromPersonalCard(true);

        model.setEmployer(model.getStudent().getPerson().getWorkPlace());

        model.setDocumentEmployer("");
        model.setTargetPlace("");

        model.setReasonList(Arrays.asList(
                new IdentifiableWrapper(0L, "промежуточной аттестации"),
                new IdentifiableWrapper(1L, "Подготовки и защиты выпускной квалификационной работы и сдачи итоговых государственных экзаменов")
        ));

        model.setReason(model.getReasonList().get(0));


        model.setFormingDate(new Date());
        model.setStudentTitleStr(PersonManager.instance().declinationDao().getDeclinationFIO(model.getStudent().getPerson().getIdentityCard(), GrammaCase.DATIVE).toUpperCase());

        model.setStudentTitleStrIminit(PersonManager.instance().declinationDao().getDeclinationFIO(model.getStudent().getPerson().getIdentityCard(), GrammaCase.NOMINATIVE).toUpperCase());


        model.setCourseList(DevelopGridDAO.getCourseList());
        model.setCourse(model.getStudent().getCourse());

        Student stud = getNotNull(Student.class, model.getStudent().getId());
        EducationOrgUnit eduOrgUnit = getNotNull(EducationOrgUnit.class, stud.getEducationOrgUnit().getId());
        OrgUnit formativeOrgUnit = model.getStudent().getEducationOrgUnit().getFormativeOrgUnit();
        //getNotNull(OrgUnit.class, eduOrgUnit.getFormativeOrgUnit().getId());
       /* String managerPostTitle;
   
       if (model.getManagerPostTitle() == null){
   
    	   managerPostTitle = "Заместитель первого проректора  по учебной работе";
        
    	   model.setManagerPostTitle(managerPostTitle);
        }
       if (model.getManagerFio() == null){
	        EmployeePost manager = EmployeeManager.instance().dao().getHead(formativeOrgUnit);
                if (manager != null)
            model.setManagerFio(manager.getPerson().getIdentityCard().getIof());
        
    	        else {
    		        String manFio = "Н.И. Дундин";
    		        model.setManagerFio(manFio);
    	        }
            }
           */
        String managerPostTitle = "";
        String managerFio = "";
        String phone = "";
        String executant = "";

        Person currentUser = PersonManager.instance().dao().getPerson(ContextLocal.getUserContext().getPrincipalContext());
        executant = (currentUser == null ? "Администратор" : currentUser.getIdentityCard().getIof());

        EmployeePost manager = EmployeeManager.instance().dao().getHead(formativeOrgUnit);

        phone = formativeOrgUnit.getPhone();
        managerPostTitle = manager.getPostRelation().getPostBoundedWithQGandQL().getTitle();
        managerPostTitle = managerPostTitle.substring(0, 1).toUpperCase() + managerPostTitle.substring(1);
        managerFio = manager.getPerson().getIdentityCard().getIof();
        if (managerPostTitle != null && managerPostTitle != "") {
        	//----------------------------------------------------------------------------------------------------
            //managerPostTitle += (" " + manager.getOrgUnit().getOrgUnitType().getGenitive());
        	if(manager.getOrgUnit().getTitle().toLowerCase().contains("школа")) {
        		managerPostTitle += " высшей школы";
        	}
        	else if(manager.getOrgUnit().getTitle().toLowerCase().contains("колледж")) {
        		managerPostTitle += " колледжа";
        	}
        	else if(manager.getOrgUnit().getTitle().toLowerCase().contains("институт")) {
        		managerPostTitle += " института";
        	}
        	else if(manager.getOrgUnit().getTitle().toLowerCase().contains("филиал")) {
        		managerPostTitle += " филиала";
        	}
        	else if(manager.getOrgUnit().getTitle().toLowerCase().contains("кафедра")) {
        		managerPostTitle += " кафедры";
        	}
        	else {
        		managerPostTitle += (" " + manager.getOrgUnit().getOrgUnitType().getGenitive());
        	}
            //----------------------------------------------------------------------------------------------------
        }


        model.setTelephone(phone != null ? phone : "");
        model.setManagerPostTitle(managerPostTitle != null ? managerPostTitle : "");
        model.setManagerFio(managerFio != null ? managerFio : "");
        model.setExecutant(executant);

        model.setPeriodList(Arrays.asList(ParagraphData.EXAMINATION_SESSION,
                                          ParagraphData.RESULT_QUALIFICATION_WORK,
                                          ParagraphData.STATE_EXAMINATION,
                                          ParagraphData.STATE_FINAL_EXAMINATION,
                                          ParagraphData.FINAL_EXAMINATION));

        model.setPeriod(ParagraphData.EXAMINATION_SESSION);

        EppStudent2EduPlanVersion rel = IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionRelation(model.getStudent().getId());

        if (rel != null) {
            final EppEduPlanVersion version = rel.getEduPlanVersion();
            model.setTermModel(new FullCheckSelectModel(DevelopGridTerm.part().title().s()) {
                @Override
                public ListResult<DevelopGridTerm> findValues(String filter) {

                    DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DevelopGridTerm.class, "dgt")
                            .where(DQLExpressions.eqValue(DQLExpressions.property(DevelopGridTerm.course().fromAlias("dgt")), model.getStudent().getCourse()))
                            .where(DQLExpressions.eqValue(DQLExpressions.property(DevelopGridTerm.developGrid().id().fromAlias("dgt")), version.getEduPlan().getDevelopGrid()));

                    List<DevelopGridTerm> lst = getList(builder);

                    return new ListResult<DevelopGridTerm>(lst);
                }
            });
        }

        model.setDateStartCertification(ParagraphData.getDate(model.getStudent(), model.getPeriod(), model.getTerm(), true));
        model.setDateEndCertification(ParagraphData.getDate(model.getStudent(), model.getPeriod(), model.getTerm(), false));


    }

    private void setEduEnrData(Model model)
    {
        OrderData data = get(OrderData.class, OrderData.L_STUDENT, model.getStudent());
        if (data == null) return;
        if (data.getRestorationOrderDate() == null && data.getEduEnrollmentOrderDate() == null) return;
        if (data.getRestorationOrderDate() == null || (data.getEduEnrollmentOrderDate() != null && data.getEduEnrollmentOrderDate().after(data.getRestorationOrderDate()))) {
            model.setOrderNumber(data.getEduEnrollmentOrderNumber());
        }
        else {
            model.setOrderNumber(data.getRestorationOrderNumber());
        }
    }
    
    /*
    public EppWeek getWeek (Model model, boolean isStartDate){
    	
    	EppStudent2EduPlanVersion rel = IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionRelation(model.getStudent().getId());
    	
    	EppEduPlanVersion version = null;
    	if (rel != null)
    		version = rel.getEduPlanVersion();
    	else
    		return null;
    	
    	if (model.getTerm() == null)
    		return null;
    	
    	String weekCode = "";
    	
    	switch (model.getPeriod()) {
		case ParagraphData.EXAMINATION_SESSION:
			weekCode = EppWeekTypeCodes.EXAMINATION_SESSION;
			break;
		case ParagraphData.STATE_EXAMINATION:
			weekCode = EppWeekTypeCodes.STATE_EXAMINATION;
			break;
		default:
			return null;
		}
    	
    	DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppEduPlanVersionWeekType.class, "rel")
    			.where(DQLExpressions.eqValue(DQLExpressions.property(EppEduPlanVersionWeekType.eduPlanVersion().id().fromAlias("rel")), version.getId()))
    			.where(DQLExpressions.eqValue(DQLExpressions.property(EppEduPlanVersionWeekType.course().id().fromAlias("rel")), model.getStudent().getCourse().getId()))
    			.where(DQLExpressions.eqValue(DQLExpressions.property(EppEduPlanVersionWeekType.term().id().fromAlias("rel")), model.getTerm().getTerm().getId()))
    			.where(DQLExpressions.eqValue(DQLExpressions.property(EppEduPlanVersionWeekType.weekType().code().fromAlias("rel")), weekCode))
    			.order(DQLExpressions.property(EppEduPlanVersionWeekType.week().number().fromAlias("rel")))
    			.column("rel");
    	
    	
    	List<EppEduPlanVersionWeekType> lst = getList(builder);
    	
    	if (!lst.isEmpty()){
    		int index = isStartDate ? 0 : lst.size() - 1;
    		EppEduPlanVersionWeekType relWeek = lst.get(index);
    		return relWeek.getWeek();
    	}
    	
    	return null;
    }
    
    public Date getDate(Model model, boolean isStartDate){
    	
    	EppWeek week = getWeek(model, isStartDate);
    	
    	if (week != null){
    		EducationYear educationYear = IUniBaseDao.instance.get().get(EducationYear.class, EducationYear.current(), Boolean.TRUE);
    		
    		String dateStr = week.getTitle();
    		
    		String[] s = dateStr.split(" ");
    		String[] s1 = dateStr.split(" - ");
    		
    		int date, month, year;
    		
    		if (isStartDate){
    			date = Integer.valueOf(s[0]).intValue();
    			month = week.getMonth()-1;
    			year = week.getNumber() <= 18 ? educationYear.getIntValue() : educationYear.getIntValue() + 1;
    		} else {
    			date = Integer.valueOf(StringUtils.split(s1[1], " ")[0]).intValue();
    			month = s.length == 5 ? week.getMonth() : week.getMonth()-1;
    			year = week.getNumber() <= 17 ? educationYear.getIntValue() : educationYear.getIntValue() + 1;
    		}
    		
    		Calendar c = Calendar.getInstance();
    		c.set(year, month, date, 0, 0, 0);
    		
    		return c.getTime();
    	}
    	
    	return null;
    }
    */


    @Override
    public void update(Model model) {
        Session session = getSession();
        Criteria criteria = getCheckCriteria(new Date(), model.getStudent().getEducationOrgUnit().getFormativeOrgUnit());
        criteria.add(Restrictions.eq("number", Integer.valueOf(model.getNumber())));
        List list = criteria.list();
        if (!list.isEmpty()) {
            IdentityCard card = ((StudentDocument) list.get(0)).getStudent().getPerson().getIdentityCard();
            String sexTitle = "1".equals(card.getSex().getCode()) ? "\u0441\u0442\u0443\u0434\u0435\u043D\u0442\u0430" : "\u0441\u0442\u0443\u0434\u0435\u043D\u0442\u043A\u0438";
            ContextLocal.getErrorCollector().add((new StringBuilder()).append("\u041D\u043E\u043C\u0435\u0440 \u0434\u043E\u043A\u0443\u043C\u0435\u043D\u0442\u0430 \u0434\u043E\u043B\u0436\u0435\u043D \u0431\u044B\u0442\u044C \u0443\u043D\u0438\u043A\u0430\u043B\u044C\u043D\u044B\u043C. \u041F\u043E\u0434 \u043D\u043E\u043C\u0435\u0440\u043E\u043C \253").append(model.getNumber()).append("\273 \u0443\u0436\u0435 \u0432\u044B\u0434\u0430\u043D \u0434\u0440\u0443\u0433\u043E\u0439 \u0434\u043E\u043A\u0443\u043C\u0435\u043D\u0442 \u0434\u043B\u044F ").append(sexTitle).append(" \253").append(card.getFullFio()).append("\273.").toString(), new String[]{
                    "number"
            });
            return;
        }
        else {

            UniScriptItem script = model.getStudentDocumentType().getScriptItem();
            final Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(script, model);
            RtfDocument document = RtfDocument.fromByteArray((byte[])scriptResult.get("document"));
            DatabaseFile databaseFile = new DatabaseFile();
            databaseFile.setContent(RtfUtil.toByteArray(document));
            session.save(databaseFile);
            StudentDocument studentDocument = new StudentDocument();
            studentDocument.setNumber(model.getNumber());
            studentDocument.setFormingDate(new Date());
            studentDocument.setStudent(model.getStudent());
            studentDocument.setContent(databaseFile);
            studentDocument.setStudentDocumentType(model.getStudentDocumentType());

            StudentDocumentRMC documentRMC = new StudentDocumentRMC();
            documentRMC.setBase(studentDocument);
            documentRMC.setDateStartCertification(model.getDateStartCertification());
            documentRMC.setDateEndCertification(model.getDateEndCertification());

            getSession().saveOrUpdate(studentDocument);
            saveOrUpdate(documentRMC);
            return;
        }
    }

    private Criteria getCheckCriteria(Date date, OrgUnit formativeOrgUnit)
    {
        int year = CoreDateUtils.getYear(date);
        Date first = CoreDateUtils.getYearFirstTimeMoment(year);
        Date last = CoreDateUtils.getYearLastTimeMoment(year);
        Criteria c = getSession().createCriteria(StudentDocument.class, "d");
        c.createAlias("d.student", "s");
        c.createAlias("s.educationOrgUnit", "e");
        c.add(Restrictions.between("d.formingDate", first, last));
        c.add(Restrictions.eq("e.formativeOrgUnit", formativeOrgUnit));
        return c;
    }
}
