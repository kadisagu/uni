package ru.tandemservice.narfu.component.settings.YearPartEndDate.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.narfu.entity.YearPartStartEndDate;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.Date;
import java.util.List;


public class Model {
    private boolean _addForm;

    private List<EducationYear> _educationYearList;
    private List<YearDistributionPart> _yearDistributionPartList;
    private Date _startDate;
    private Date _endDate;

    private EducationYear _educationYear;
    private YearDistributionPart _yearDistributionPart;


    private DynamicListDataSource<YearPartStartEndDate> _dataSource;

    private IDataSettings _settings;

    public List<EducationYear> getEducationYearList() {
        return _educationYearList;
    }

    public void setEducationYearList(List<EducationYear> _educationYearList) {
        this._educationYearList = _educationYearList;
    }

    public List<YearDistributionPart> getYearDistributionPartList() {
        return _yearDistributionPartList;
    }

    public void setYearDistributionPartList(List<YearDistributionPart> _yearDistributionPart) {
        this._yearDistributionPartList = _yearDistributionPart;
    }

    public Date getStartDate() {
        return _startDate;
    }

    public void setStartDate(Date _startDate) {
        this._startDate = _startDate;
    }

    public Date getEndDate() {
        return _endDate;
    }

    public void setEndDate(Date _endDate) {
        this._endDate = _endDate;
    }

    public boolean isAddForm()
    {
        return _addForm;
    }

    public void setAddForm(boolean addForm) {
        this._addForm = addForm;
    }

    public void setEditForm(boolean editForm) {
        this._addForm = editForm;
    }

    public IDataSettings getSettings() {
        return _settings;
    }

    public void setSettings(IDataSettings _settings) {
        this._settings = _settings;
    }

    public DynamicListDataSource<YearPartStartEndDate> getDataSource() {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<YearPartStartEndDate> _dataSource) {
        this._dataSource = _dataSource;
    }


    public EducationYear getEducationYear() {
        return _educationYear;
    }

    public void setEducationYear(EducationYear _educationYear) {
        this._educationYear = _educationYear;
    }

    public YearDistributionPart getYearDistributionPart() {
        return _yearDistributionPart;
    }

    public void setYearDistributionPart(YearDistributionPart _yearDistributionPart) {
        this._yearDistributionPart = _yearDistributionPart;
    }
}
