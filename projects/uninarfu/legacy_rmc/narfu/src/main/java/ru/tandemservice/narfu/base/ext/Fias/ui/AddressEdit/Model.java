package ru.tandemservice.narfu.base.ext.Fias.ui.AddressEdit;

public class Model extends org.tandemframework.shared.fias.base.bo.Fias.ui.AddressEdit.Model {
    private String addressInfo;

    public String getAddressInfo() {
        return addressInfo;
    }

    public void setAddressInfo(String addressInfo) {
        this.addressInfo = addressInfo;
    }
}
