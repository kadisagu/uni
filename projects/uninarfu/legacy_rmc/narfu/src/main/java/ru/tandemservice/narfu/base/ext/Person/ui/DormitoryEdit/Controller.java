package ru.tandemservice.narfu.base.ext.Person.ui.DormitoryEdit;

import org.tandemframework.core.component.IBusinessComponent;

public class Controller extends org.tandemframework.shared.person.base.bo.Person.ui.DormitoryEdit.Controller
{

    @Override
    public void onClickApply(IBusinessComponent component)
    {
        super.onClickApply(component);

        getDao().update(getModel(component));
    }
}
