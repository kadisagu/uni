package ru.tandemservice.narfu.uniec.component.report.EntrantRequestQtyAll;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {
    void preparePrintReport(Model model) throws Exception;
}

