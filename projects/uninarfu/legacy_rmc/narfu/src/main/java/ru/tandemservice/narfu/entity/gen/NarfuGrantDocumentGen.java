package ru.tandemservice.narfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudentrmc.entity.NarfuDocument;
import ru.tandemservice.narfu.entity.NarfuGrantDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Документы для массовой печати заявлений на получение стипендии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class NarfuGrantDocumentGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.narfu.entity.NarfuGrantDocument";
    public static final String ENTITY_NAME = "narfuGrantDocument";
    public static final int VERSION_HASH = 190591311;
    private static IEntityMeta ENTITY_META;

    public static final String L_DOCUMENT = "document";

    private NarfuDocument _document;     // Документ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Документ. Свойство не может быть null.
     */
    @NotNull
    public NarfuDocument getDocument()
    {
        return _document;
    }

    /**
     * @param document Документ. Свойство не может быть null.
     */
    public void setDocument(NarfuDocument document)
    {
        dirty(_document, document);
        _document = document;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof NarfuGrantDocumentGen)
        {
            setDocument(((NarfuGrantDocument)another).getDocument());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends NarfuGrantDocumentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) NarfuGrantDocument.class;
        }

        public T newInstance()
        {
            return (T) new NarfuGrantDocument();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "document":
                    return obj.getDocument();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "document":
                    obj.setDocument((NarfuDocument) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "document":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "document":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "document":
                    return NarfuDocument.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<NarfuGrantDocument> _dslPath = new Path<NarfuGrantDocument>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "NarfuGrantDocument");
    }
            

    /**
     * @return Документ. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.NarfuGrantDocument#getDocument()
     */
    public static NarfuDocument.Path<NarfuDocument> document()
    {
        return _dslPath.document();
    }

    public static class Path<E extends NarfuGrantDocument> extends EntityPath<E>
    {
        private NarfuDocument.Path<NarfuDocument> _document;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Документ. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.NarfuGrantDocument#getDocument()
     */
        public NarfuDocument.Path<NarfuDocument> document()
        {
            if(_document == null )
                _document = new NarfuDocument.Path<NarfuDocument>(L_DOCUMENT, this);
            return _document;
        }

        public Class getEntityClass()
        {
            return NarfuGrantDocument.class;
        }

        public String getEntityName()
        {
            return "narfuGrantDocument";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
