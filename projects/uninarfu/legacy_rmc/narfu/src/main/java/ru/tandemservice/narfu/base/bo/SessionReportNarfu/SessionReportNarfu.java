package ru.tandemservice.narfu.base.bo.SessionReportNarfu;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

@Configuration
public class SessionReportNarfu extends BusinessObjectManager {

    public static SessionReportNarfu instance() {
        return instance(SessionReportNarfu.class);
    }

}
