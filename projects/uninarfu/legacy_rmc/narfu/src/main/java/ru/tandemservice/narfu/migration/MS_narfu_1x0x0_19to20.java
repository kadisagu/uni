package ru.tandemservice.narfu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;
import ru.tandemservice.unibasermc.entity.catalog.EducationLevelStageExt;

import java.sql.ResultSet;


/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_narfu_1x0x0_19to20 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.10"),
                        new ScriptDependency("org.tandemframework.shared", "1.1.4"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.1.4")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        IEntityMeta meta = EntityRuntime.getMeta(EducationLevelStageExt.class);
        if (!tool.tableExists(meta.getTableName())) {
            // создать таблицу
            DBTable dbt = new DBTable("educationlevelstageext_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("base_id", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("educationLevelStageExt");

            //проставим записи из парента
            ResultSet rs = tool.getConnection().createStatement().executeQuery("select id, title_p from educationlevelstage_t");
            String insertQuery = "insert into " + meta.getTableName() + "(id,discriminator,title_p,base_id) values(?,?,?,?)";
            short discriminator = meta.getEntityCode().shortValue();

            while (rs.next()) {
                Long baseId = rs.getLong(1);
                String title = rs.getString(2);

                tool.executeUpdate(insertQuery, EntityIDGenerator.generateNewId(discriminator), discriminator, title, baseId);
            }
            rs.close();
        }


    }
}