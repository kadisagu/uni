package ru.tandemservice.narfu.component.studentmassprint.printDAO.grants;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.narfu.entity.NarfuGrantDocument;
import ru.tandemservice.narfu.utils.RtfToZipSupport;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.employee.Student;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractGrantRequestReport<Model extends ru.tandemservice.narfu.component.studentmassprint.GrantsMassPrint.Model> implements IPrintFormCreator<Model>, IStudentGrantReqestGenerator<Model> {

    private int studentsPerPage = 50;

    private String paramPage;

    @Override
    public int getStudentPerPage() {
        return studentsPerPage;
    }

    @Override
    public byte[] generateReport(List<Student> students, byte[] template, Model model) {

        List<File> files = new ArrayList<File>();

        RtfDocument rtfFile = RtfBean.getElementFactory().createRtfDocument();
        rtfFile.setSettings(new RtfReader().read(template).getSettings());
        int studentNumber = 0;

        for (Student student : students) {
            model.setCurrentStudent(student);

            RtfDocument createPrintForm = createPrintForm(template, model);

            rtfFile.getElementList().addAll(createPrintForm.getElementList());

            if (students.indexOf(student) < (students.size() - 1))
                rtfFile.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));

            studentNumber++;

            if (studentNumber >= getStudentPerPage()) {
                //Запишем в файл колбасу из n студентов
                files.add(RtfToZipSupport.writeToFile(rtfFile));
                rtfFile = RtfBean.getElementFactory().createRtfDocument();
                studentNumber = 0;
            }
        }

        if (files.isEmpty())
            files.add(RtfToZipSupport.writeToFile(rtfFile));

        return RtfToZipSupport.toZip(files);
    }


    @Override
    public RtfDocument createPrintForm(byte[] template, Model model) {
        RtfDocument document = createTemplateDocument(template, model);

        createInjectModifier(model).modify(document);

        createTableModifier(model).modify(document);

        return document;
    }

    protected RtfDocument createTemplateDocument(byte[] template, Model model) {
        return new RtfReader().read(template);
    }

    protected RtfInjectModifier createInjectModifier(Model model) {
        RtfInjectModifier im = new RtfInjectModifier();

        Student student = model.getCurrentStudent();
        Person person = student.getPerson();
        boolean male = person.isMale();

        im.put("genderBasedDevelop", male ? "Студента" : "Студентки");
        im.put("stud", male ? "обучающегося" : "обучающейся");
        im.put("course", String.valueOf(student.getCourse().getIntValue()));
        im.put("developForm", getDevelopFormStr_G(student.getEducationOrgUnit().getDevelopForm()));
        im.put("inst", student.getEducationOrgUnit().getFormativeOrgUnit().getGenitiveCaseTitle());
        EducationLevelsHighSchool level = student.getEducationOrgUnit().getEducationLevelHighSchool();

        im.put("specHighSchool", level.isSpeciality() || level.isSpecialization() ? "специальности" : "направлению подготовки");

        im.put("codeHighSchool", level.getEducationLevel().getInheritedOkso());
        im.put("highSchool", level.getDisplayableShortTitle());
        im.put("compensationType", getCompensationTypeStr(student.getCompensationType()));
        im.put("F", PersonManager.instance().declinationDao().getDeclinationLastName(person.getIdentityCard().getLastName(), GrammaCase.GENITIVE, male));
        im.put("I", PersonManager.instance().declinationDao().getDeclinationFirstName(person.getIdentityCard().getFirstName(), GrammaCase.GENITIVE, male));
        String middleName = person.getIdentityCard().getMiddleName();
        if (middleName != null)
            im.put("O", PersonManager.instance().declinationDao().getDeclinationMiddleName(middleName, GrammaCase.GENITIVE, male));
        else
            im.put("O", "");
        im.put("stip_vin", model.getGrant() != null ? model.getGrant().getAccusative() : "");
        im.put("date_st", RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(model.getDateOfBeginPayment()) + " года");

        return im;
    }

    public String getDevelopFormStr_G(DevelopForm developForm) {
        String code = developForm.getCode();
        if (DevelopFormCodes.FULL_TIME_FORM.equals(code))
            return "очной";
        else if (DevelopFormCodes.CORESP_FORM.equals(code))
            return "заочной";
        else
            return "очно-заочной";
    }

    protected String getCompensationTypeStr(CompensationType type) {
        String code = type.getCode();
        if (code.equals(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET))
            return "за счет средств федерального бюджета";
        else if (code.equals(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT))
            return "с полным возмещением затрат на обучение";
        else return "";

    }

    protected RtfTableModifier createTableModifier(Model model) {
        RtfTableModifier tm = new RtfTableModifier();

        List<NarfuGrantDocument> grantDocuments = model.getStudentDocMap().get(model.getCurrentStudent().getId());

        List<String[]> rows = new ArrayList<String[]>();

        if (grantDocuments != null)
            for (NarfuGrantDocument doc : grantDocuments) {
                rows.add(new String[]{grantDocuments.indexOf(doc) + 1 + ".", doc.getDocument().getTitle() + " года"});
            }

        tm.put("T", rows.toArray(new String[][]{}));

        return tm;
    }

    public String getParamPage() {
        return paramPage;
    }

    public void setParamPage(String paramPage) {
        this.paramPage = paramPage;
    }


}
