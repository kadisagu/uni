package ru.tandemservice.narfu.component.entrant.EcDistributionPrintProtocol;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = getModel(component);
        ((IDAO) getDao()).prepare(model);
    }

    public void onClickApply(IBusinessComponent component) {
        Model model = getModel(component);
        RtfDocument document = ((IDAO) getDao()).createReport(model);

        deactivate(component);

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("протокол.rtf").document(document), true);
    }
}
