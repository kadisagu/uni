package ru.tandemservice.narfu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_narfu_1x0x0_54to55 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.15"),
                        new ScriptDependency("org.tandemframework.shared", "1.6.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.6.0"),
                        new ScriptDependency("ru.tandemservice.uni.project", "2.6.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность eppCtrAgreementTemplateDataTransfer

        // сущность была удалена
        {
            // удалить таблицу
            tool.dropTable("epp_ctr_ctmpldt_transfer_t", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("eppCtrAgreementTemplateDataTransfer");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eppCtrAgreementTemplateDataMotherCapital

        // сущность была удалена
        {
            // удалить таблицу
            tool.dropTable("epp_ctr_ctmpldt_mcapital_t", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("eppCtrAgreementTemplateDataMotherCapital");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eppCtrAgreementTemplateDataDividePayment

        // сущность была удалена
        {
            // удалить таблицу
            tool.dropTable("epp_ctr_ctmpldt_divpay_t", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("eppCtrAgreementTemplateDataDividePayment");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eppCtrAgreementTemplateDataChangeName

        // сущность была удалена
        {
            // удалить таблицу
            tool.dropTable("epp_ctr_ctmpldt_chname_t", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("eppCtrAgreementTemplateDataChangeName");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность ctrPaymentPromiceNARFU

        // сущность была удалена
        {
            // удалить таблицу
            tool.dropTable("ctr_pr_payment_t_narfu", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("ctrPaymentPromiceNARFU");

        }


    }
}