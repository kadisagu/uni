package ru.tandemservice.narfu.component.settings.Course2EnrollmentCampaign;

import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.narfu.entity.Course2EnrollmentCampaign;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.*;
import java.util.stream.Collectors;

public class DAO extends UniDao<Model> implements IDAO {
    @Override
    public void prepare(Model model)
    {
        model.setCourse2Campaigns(new ArrayList<>());
        List<Course> courseList = getCourses(6);
        List<Course2EnrollmentCampaign> list = getList(Course2EnrollmentCampaign.class);
        Map<Course, List<EnrollmentCampaign>> map = new HashMap<>();
        for (Course2EnrollmentCampaign item : list) {
            if (!map.containsKey(item.getCourse())) {
                map.put(item.getCourse(), new ArrayList<>());
            }
            map.get(item.getCourse()).add(item.getEnrollmentCampaign());
        }
        Collections.sort(courseList, new EntityComparator<>(new EntityOrder(Course.title().s())));
        model.setEnrollmentCampaignModel(new LazySimpleSelectModel<>(EnrollmentCampaign.class));
        for (Course item : courseList) {
            Wrapper wrp = new Wrapper();
            wrp.setCourse(item);
            wrp.setCampaigns(map.get(item));
            model.getCourse2Campaigns().add(wrp);
        }
    }

    @Override
    public void update(Model model)
    {
        List<Course2EnrollmentCampaign> list = getList(Course2EnrollmentCampaign.class);
        for (Course2EnrollmentCampaign item : list) {
            delete(item);
        }
        for (Wrapper course : model.getCourse2Campaigns()) {
            for (EnrollmentCampaign campaign : course.getCampaigns()) {
                Course2EnrollmentCampaign entity = new Course2EnrollmentCampaign();
                entity.setCourse(course.getCourse());
                entity.setEnrollmentCampaign(campaign);
                saveOrUpdate(entity);
            }
        }

    }

    private List<Course> getCourses(int max)
    {
        return DevelopGridDAO.getCourseMap().values().stream()
                .filter(c -> c.getIntValue() <= max)
                .collect(Collectors.toList());
    }
}
