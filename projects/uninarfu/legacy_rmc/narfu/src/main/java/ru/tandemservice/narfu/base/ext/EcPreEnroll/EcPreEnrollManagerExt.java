package ru.tandemservice.narfu.base.ext.EcPreEnroll;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.narfu.base.ext.EcPreEnroll.logic.EcPreEnrollDaoExt;
import ru.tandemservice.uniec.base.bo.EcPreEnroll.logic.IEcPreEnrollDao;

@Configuration
public class EcPreEnrollManagerExt extends BusinessObjectExtensionManager {

    @Bean
    @BeanOverride
    public IEcPreEnrollDao dao() {
        return new EcPreEnrollDaoExt();
    }
}
