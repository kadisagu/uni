package ru.tandemservice.unisession.dao.print;

import org.tandemframework.core.util.cache.SpringBeanCache;

import java.util.Collection;

public interface INarfuSessionBulletinPrintDAO /*extends ru.tandemservice.unisession.print.ISessionBulletinPrintDAO */{

    public static final SpringBeanCache<INarfuSessionBulletinPrintDAO> instance = new SpringBeanCache<>(INarfuSessionBulletinPrintDAO.class.getName());
    public NarfuSessionBulletinPrintDAO.ReportFile printBulletinListNarfu(Collection<Long> ids);

}
