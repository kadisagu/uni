package ru.tandemservice.narfu.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import ru.tandemservice.narfu.entity.catalog.GrantTemplateDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Печатные шаблоны модуля Стипендии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class GrantTemplateDocumentGen extends TemplateDocument
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.narfu.entity.catalog.GrantTemplateDocument";
    public static final String ENTITY_NAME = "grantTemplateDocument";
    public static final int VERSION_HASH = -1106883988;
    private static IEntityMeta ENTITY_META;

    public static final String P_USE_MASS_PRINT = "useMassPrint";

    private boolean _useMassPrint; 

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public boolean isUseMassPrint()
    {
        return _useMassPrint;
    }

    /**
     * @param useMassPrint  Свойство не может быть null.
     */
    public void setUseMassPrint(boolean useMassPrint)
    {
        dirty(_useMassPrint, useMassPrint);
        _useMassPrint = useMassPrint;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof GrantTemplateDocumentGen)
        {
            setUseMassPrint(((GrantTemplateDocument)another).isUseMassPrint());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends GrantTemplateDocumentGen> extends TemplateDocument.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) GrantTemplateDocument.class;
        }

        public T newInstance()
        {
            return (T) new GrantTemplateDocument();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "useMassPrint":
                    return obj.isUseMassPrint();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "useMassPrint":
                    obj.setUseMassPrint((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "useMassPrint":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "useMassPrint":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "useMassPrint":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<GrantTemplateDocument> _dslPath = new Path<GrantTemplateDocument>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "GrantTemplateDocument");
    }
            

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.catalog.GrantTemplateDocument#isUseMassPrint()
     */
    public static PropertyPath<Boolean> useMassPrint()
    {
        return _dslPath.useMassPrint();
    }

    public static class Path<E extends GrantTemplateDocument> extends TemplateDocument.Path<E>
    {
        private PropertyPath<Boolean> _useMassPrint;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.catalog.GrantTemplateDocument#isUseMassPrint()
     */
        public PropertyPath<Boolean> useMassPrint()
        {
            if(_useMassPrint == null )
                _useMassPrint = new PropertyPath<Boolean>(GrantTemplateDocumentGen.P_USE_MASS_PRINT, this);
            return _useMassPrint;
        }

        public Class getEntityClass()
        {
            return GrantTemplateDocument.class;
        }

        public String getEntityName()
        {
            return "grantTemplateDocument";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
