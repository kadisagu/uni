package ru.tandemservice.narfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.narfu.entity.EppEduPlanVersionBlockLink;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь блоков УП(в)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppEduPlanVersionBlockLinkGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.narfu.entity.EppEduPlanVersionBlockLink";
    public static final String ENTITY_NAME = "eppEduPlanVersionBlockLink";
    public static final int VERSION_HASH = 17635219;
    private static IEntityMeta ENTITY_META;

    public static final String L_BLOCK_SRC = "blockSrc";
    public static final String L_BLOCK_DST = "blockDst";

    private EppEduPlanVersionBlock _blockSrc;     // Блок УП
    private EppEduPlanVersionBlock _blockDst;     // Связан с

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Блок УП. Свойство не может быть null.
     */
    @NotNull
    public EppEduPlanVersionBlock getBlockSrc()
    {
        return _blockSrc;
    }

    /**
     * @param blockSrc Блок УП. Свойство не может быть null.
     */
    public void setBlockSrc(EppEduPlanVersionBlock blockSrc)
    {
        dirty(_blockSrc, blockSrc);
        _blockSrc = blockSrc;
    }

    /**
     * @return Связан с. Свойство не может быть null.
     */
    @NotNull
    public EppEduPlanVersionBlock getBlockDst()
    {
        return _blockDst;
    }

    /**
     * @param blockDst Связан с. Свойство не может быть null.
     */
    public void setBlockDst(EppEduPlanVersionBlock blockDst)
    {
        dirty(_blockDst, blockDst);
        _blockDst = blockDst;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppEduPlanVersionBlockLinkGen)
        {
            setBlockSrc(((EppEduPlanVersionBlockLink)another).getBlockSrc());
            setBlockDst(((EppEduPlanVersionBlockLink)another).getBlockDst());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppEduPlanVersionBlockLinkGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppEduPlanVersionBlockLink.class;
        }

        public T newInstance()
        {
            return (T) new EppEduPlanVersionBlockLink();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "blockSrc":
                    return obj.getBlockSrc();
                case "blockDst":
                    return obj.getBlockDst();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "blockSrc":
                    obj.setBlockSrc((EppEduPlanVersionBlock) value);
                    return;
                case "blockDst":
                    obj.setBlockDst((EppEduPlanVersionBlock) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "blockSrc":
                        return true;
                case "blockDst":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "blockSrc":
                    return true;
                case "blockDst":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "blockSrc":
                    return EppEduPlanVersionBlock.class;
                case "blockDst":
                    return EppEduPlanVersionBlock.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppEduPlanVersionBlockLink> _dslPath = new Path<EppEduPlanVersionBlockLink>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppEduPlanVersionBlockLink");
    }
            

    /**
     * @return Блок УП. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.EppEduPlanVersionBlockLink#getBlockSrc()
     */
    public static EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> blockSrc()
    {
        return _dslPath.blockSrc();
    }

    /**
     * @return Связан с. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.EppEduPlanVersionBlockLink#getBlockDst()
     */
    public static EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> blockDst()
    {
        return _dslPath.blockDst();
    }

    public static class Path<E extends EppEduPlanVersionBlockLink> extends EntityPath<E>
    {
        private EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> _blockSrc;
        private EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> _blockDst;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Блок УП. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.EppEduPlanVersionBlockLink#getBlockSrc()
     */
        public EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> blockSrc()
        {
            if(_blockSrc == null )
                _blockSrc = new EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock>(L_BLOCK_SRC, this);
            return _blockSrc;
        }

    /**
     * @return Связан с. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.EppEduPlanVersionBlockLink#getBlockDst()
     */
        public EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> blockDst()
        {
            if(_blockDst == null )
                _blockDst = new EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock>(L_BLOCK_DST, this);
            return _blockDst;
        }

        public Class getEntityClass()
        {
            return EppEduPlanVersionBlockLink.class;
        }

        public String getEntityName()
        {
            return "eppEduPlanVersionBlockLink";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
