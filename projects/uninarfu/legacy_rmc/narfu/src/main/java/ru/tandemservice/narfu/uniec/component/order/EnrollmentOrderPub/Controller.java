package ru.tandemservice.narfu.uniec.component.order.EnrollmentOrderPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.shared.commonbase.base.util.UniMap;

public class Controller extends ru.tandemservice.uniec.component.order.EnrollmentOrderPub.Controller {

    public void onClickPrintPersonalInfo(IBusinessComponent component) {
        ContextLocal.createDesktop("PersonShellDialog",
                                   new ComponentActivator("ru.tandemservice.narfu.uniec.component.order.EnrollmentOrderPub.EnrollmentParagraphPersonalInfoDialog",
                                                          new UniMap().add("orderId", getModel(component).getOrder().getId())));
    }

    public void onClickPrintExtracts(IBusinessComponent component) {

        ContextLocal.createDesktop("PersonShellDialog",
                                   new ComponentActivator("ru.tandemservice.narfu.uniec.component.order.EnrollmentOrderPub.EnrollmentParagraphExtractsDialog",
                                                          new UniMap().add("orderId", getModel(component).getOrder().getId())));

    }


}
