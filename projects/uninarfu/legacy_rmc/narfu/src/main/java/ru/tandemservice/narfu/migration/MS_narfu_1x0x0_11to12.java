package ru.tandemservice.narfu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;

@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_narfu_1x0x0_11to12 extends IndependentMigrationScript {
    //из модуля aspirantrmc удалили не все сущности, доудаляем

    @Override
    public void run(DBTool tool) throws Exception
    {
        // удалить сущность practiceWork
        if (tool.tableExists("practicework_t")) {
            // удалить таблицу
            tool.dropTable("practicework_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            if (tool.entityCodes().get("practiceWork") != null) {
                tool.entityCodes().delete("practiceWork");
            }

        }

        // удалить сущность leadOrganization
        if (tool.tableExists("leadorganization_t")) {
            // удалить таблицу
            tool.dropTable("leadorganization_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            if (tool.entityCodes().get("leadOrganization") != null) {
                tool.entityCodes().delete("leadOrganization");
            }

        }
    }
}
