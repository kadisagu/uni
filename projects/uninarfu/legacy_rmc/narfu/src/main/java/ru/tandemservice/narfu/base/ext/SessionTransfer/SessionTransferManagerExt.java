package ru.tandemservice.narfu.base.ext.SessionTransfer;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.narfu.base.ext.SessionTransfer.logic.SessionTransferDaoExt;
import ru.tandemservice.unisession.base.bo.SessionTransfer.logic.ISessionTransferDao;

@Configuration
public class SessionTransferManagerExt extends BusinessObjectExtensionManager {
    @Bean
    @BeanOverride
    public ISessionTransferDao dao()
    {
        return new SessionTransferDaoExt();
    }
}
