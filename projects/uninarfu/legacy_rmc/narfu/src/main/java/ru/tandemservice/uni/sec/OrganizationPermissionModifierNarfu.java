package ru.tandemservice.uni.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.*;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.shared.organization.catalog.entity.codes.OrgUnitTypeCodes;

import java.util.List;
import java.util.Map;

public class OrganizationPermissionModifierNarfu implements ISecurityConfigMetaMapModifier {

    @Override
    public void modify(Map<String, SecurityConfigMeta> securityConfigMetaMap) {

        SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("uni");
        config.setName("narfu-organization-sec-config");
        config.setTitle("");

        ModuleGlobalGroupMeta moduleGlobalGroupMeta = PermissionMetaUtil.createModuleGlobalGroup(config, "orgstructModuleGlobalGroup", "Модуль «Орг. структура»");
        ModuleLocalGroupMeta moduleLocalGroupMeta = PermissionMetaUtil.createModuleLocalGroup(config, "orgstructLocalGroup", "Модуль «Орг. структура»");

        List<OrgUnitType> orgUnitTypeList = DataAccessServices.dao().getList(OrgUnitType.class, OrgUnitType.P_TITLE);
        for (OrgUnitType description : orgUnitTypeList) {
            String code = description.getCode();

            ClassGroupMeta globalClassGroup = PermissionMetaUtil.createClassGroup(moduleGlobalGroupMeta, code + "PermissionClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());
            ClassGroupMeta localClassGroup = PermissionMetaUtil.createClassGroup(moduleLocalGroupMeta, code + "LocalClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());

            // Вкладка "Массовая печать" на подразделении
            PermissionGroupMeta permissionGroupMassPrintTab = PermissionMetaUtil.createPermissionGroup(config, code + "MassPrintPermissionGroup", "Вкладка «Массовая печать»");
            PermissionMetaUtil.createPermission(permissionGroupMassPrintTab, "rmc_viewOrgUnitMassPrintTab_" + code, "Просмотр");
            addTabPermission(config, globalClassGroup, localClassGroup, permissionGroupMassPrintTab);

            // Вкладка "Документы обучающихся" на табе "Массовая печать"
            PermissionGroupMeta permissionGroupMassPrint = PermissionMetaUtil.createPermissionGroup(permissionGroupMassPrintTab, code + "MassPrintDocumentsTabPermissionGroup", "Вкладка «Документы обучающихся»");
            PermissionMetaUtil.createPermission(permissionGroupMassPrint, "rmc_view_studentmassprint_tab_" + code, "Просмотр");

            // Вкладка "Заявления на предоставление стипендий" на табе "Массовая печать"
            PermissionGroupMeta permissionGroupGrantMassPrint = PermissionMetaUtil.createPermissionGroup(permissionGroupMassPrintTab, code + "MassPrintGrantsTabPermissionGroup", "Вкладка «Заявления на предоставление стипендий»");
            PermissionMetaUtil.createPermission(permissionGroupGrantMassPrint, "rmc_view_studentgrantmassprint_tab_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(permissionGroupGrantMassPrint, "rmc_edit_studentgrantmassprint_" + code, "Редактирование приложенных документов");

            PermissionGroupMeta permissionGroupStudentsTab = PermissionMetaUtil.createPermissionGroup(config, code + "StudentsPermissionGroup", "Вкладка «Студенты»");
            addTabPermission(config, globalClassGroup, localClassGroup, permissionGroupStudentsTab);

            PermissionGroupMeta permissionGroupReports = PermissionMetaUtil.createPermissionGroup(config, code + "ReportsTabPermissionGroup", "Вкладка «Отчеты»");
            PermissionMetaUtil.createPermission(permissionGroupReports, "addStorableReport_" + code, "Добавление отчета");
            PermissionMetaUtil.createPermission(permissionGroupReports, "printStorableReport_" + code, "Печать отчета");
            PermissionMetaUtil.createPermission(permissionGroupReports, "deleteStorableReport_" + code, "Удаление отчета");
            addTabPermission(config, globalClassGroup, localClassGroup, permissionGroupReports);

            PermissionGroupMeta pgStudentReports = PermissionMetaUtil.createPermissionGroup(permissionGroupReports, code + "StudentReportPermissionGroup", "Отчеты модуля «Контингент студентов»");
            PermissionMetaUtil.createPermission(pgStudentReports, "orgUnit_viewReportJournalAcademVacations_" + code, "Просмотр отчета «Журнал учета Академических отпусков»");
            PermissionMetaUtil.createPermission(pgStudentReports, "orgUnit_viewStudentArchiveReport_" + code, "Просмотр отчета «Опись сдачи в архив личных дел отчисленных/выпускников»");
            PermissionMetaUtil.createPermission(pgStudentReports, "orgUnit_viewStudentbookReport_" + code, "Просмотр отчета «Книга учета студ. Билетов и зачетных книжек»");

            PermissionGroupMeta scienceWorkPG = PermissionMetaUtil.createPermissionGroup(permissionGroupReports, code + "ScienceWorkReportPG", "Отчеты модуля «Научные работы»");
            PermissionMetaUtil.createPermission(scienceWorkPG, "orgUnit_viewAspirantReport_" + code, "Просмотр и печать отчета «Сведения о приеме в аспирантуру за счет средств федерального бюджета»");
            PermissionMetaUtil.createPermission(scienceWorkPG, "orgUnit_addAspirantReport_" + code, "Добавление отчета «Сведения о приеме в аспирантуру за счет средств федерального бюджета»");
            PermissionMetaUtil.createPermission(scienceWorkPG, "orgUnit_deleteAspirantReport_" + code, "Удаление отчета «Сведения о приеме в аспирантуру за счет средств федерального бюджета»");
            PermissionMetaUtil.createPermission(scienceWorkPG, "orgUnit_viewExcludedAspirantsReport_" + code, "Просмотр и печать отчета «Сведения об аспирантах - гражданах РФ, обучавшихся за счет средств федерального бюджета и выбывших до окончания срока обучения»");
            PermissionMetaUtil.createPermission(scienceWorkPG, "orgUnit_addExcludedAspirantsReport_" + code, "Добавление отчета «Сведения об аспирантах - гражданах РФ, обучавшихся за счет средств федерального бюджета и выбывших до окончания срока обучения»");
            PermissionMetaUtil.createPermission(scienceWorkPG, "orgUnit_deleteExcludedAspirantsReport_" + code, "Удаление отчета «Сведения об аспирантах - гражданах РФ, обучавшихся за счет средств федерального бюджета и выбывших до окончания срока обучения»");
            PermissionMetaUtil.createPermission(scienceWorkPG, "orgUnit_viewFinishedAspirantsReport_" + code, "Просмотр и печать отчета «Выпуск аспирантов - граждан РФ, обучавшихся по прямым договорам с оплатой стоимости обучения физическими или юридическими лицами, и защиты диссертаций на соискание ученой степени кандидата наук лицами этой категории, окончившими аспирантуру данного вуза без защиты диссертации, по отраслям науки и специальностям»");
            PermissionMetaUtil.createPermission(scienceWorkPG, "orgUnit_addFinishedAspirantsReport_" + code, "Добавление отчета «Выпуск аспирантов - граждан РФ, обучавшихся по прямым договорам с оплатой стоимости обучения физическими или юридическими лицами, и защиты диссертаций на соискание ученой степени кандидата наук лицами этой категории, окончившими аспирантуру данного вуза без защиты диссертации, по отраслям науки и специальностям»");
            PermissionMetaUtil.createPermission(scienceWorkPG, "orgUnit_deleteFinishedAspirantsReport_" + code, "Удаление отчета «Выпуск аспирантов - граждан РФ, обучавшихся по прямым договорам с оплатой стоимости обучения физическими или юридическими лицами, и защиты диссертаций на соискание ученой степени кандидата наук лицами этой категории, окончившими аспирантуру данного вуза без защиты диссертации, по отраслям науки и специальностям»");

            PermissionGroupMeta pgEppTab = PermissionMetaUtil.createPermissionGroup(config, code + "EppPG", "Вкладка «Учебный процесс»");
            PermissionGroupMeta pgOptionalDisciplineTab = PermissionMetaUtil.createPermissionGroup(pgEppTab, code + "TutorExtMakeWPVersionPG", "Вкладка «Версии РП»");
            PermissionMetaUtil.createPermission(pgOptionalDisciplineTab, "orgUnit_narfuTutorExtMakeWPVersion_" + code, "Создание версии РУП");
        }
        securityConfigMetaMap.put(config.getName(), config);
    }

    private void addTabPermission(SecurityConfigMeta config, ClassGroupMeta globalClassGroup, ClassGroupMeta localClassGroup, PermissionGroupMeta tab)
    {
        PermissionMetaUtil.createGroupRelation(config, tab.getName(), globalClassGroup.getName());
        PermissionMetaUtil.createGroupRelation(config, tab.getName(), localClassGroup.getName());
    }
}
