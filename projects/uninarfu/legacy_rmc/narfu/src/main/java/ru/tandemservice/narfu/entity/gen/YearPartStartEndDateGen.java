package ru.tandemservice.narfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.narfu.entity.YearPartStartEndDate;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Даты начала/окончания части учебного года
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class YearPartStartEndDateGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.narfu.entity.YearPartStartEndDate";
    public static final String ENTITY_NAME = "yearPartStartEndDate";
    public static final int VERSION_HASH = -1574525255;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDUCATION_YEAR = "educationYear";
    public static final String L_PART = "part";
    public static final String P_START_DATE = "startDate";
    public static final String P_END_DATE = "endDate";

    private EducationYear _educationYear;     // Учебный год
    private YearDistributionPart _part;     // Часть года
    private Date _startDate;     // Начальная дата
    private Date _endDate;     // Конечная дата

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    /**
     * @param educationYear Учебный год. Свойство не может быть null.
     */
    public void setEducationYear(EducationYear educationYear)
    {
        dirty(_educationYear, educationYear);
        _educationYear = educationYear;
    }

    /**
     * @return Часть года. Свойство не может быть null.
     */
    @NotNull
    public YearDistributionPart getPart()
    {
        return _part;
    }

    /**
     * @param part Часть года. Свойство не может быть null.
     */
    public void setPart(YearDistributionPart part)
    {
        dirty(_part, part);
        _part = part;
    }

    /**
     * @return Начальная дата. Свойство не может быть null.
     */
    @NotNull
    public Date getStartDate()
    {
        return _startDate;
    }

    /**
     * @param startDate Начальная дата. Свойство не может быть null.
     */
    public void setStartDate(Date startDate)
    {
        dirty(_startDate, startDate);
        _startDate = startDate;
    }

    /**
     * @return Конечная дата. Свойство не может быть null.
     */
    @NotNull
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Конечная дата. Свойство не может быть null.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof YearPartStartEndDateGen)
        {
            setEducationYear(((YearPartStartEndDate)another).getEducationYear());
            setPart(((YearPartStartEndDate)another).getPart());
            setStartDate(((YearPartStartEndDate)another).getStartDate());
            setEndDate(((YearPartStartEndDate)another).getEndDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends YearPartStartEndDateGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) YearPartStartEndDate.class;
        }

        public T newInstance()
        {
            return (T) new YearPartStartEndDate();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "educationYear":
                    return obj.getEducationYear();
                case "part":
                    return obj.getPart();
                case "startDate":
                    return obj.getStartDate();
                case "endDate":
                    return obj.getEndDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "educationYear":
                    obj.setEducationYear((EducationYear) value);
                    return;
                case "part":
                    obj.setPart((YearDistributionPart) value);
                    return;
                case "startDate":
                    obj.setStartDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "educationYear":
                        return true;
                case "part":
                        return true;
                case "startDate":
                        return true;
                case "endDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "educationYear":
                    return true;
                case "part":
                    return true;
                case "startDate":
                    return true;
                case "endDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "educationYear":
                    return EducationYear.class;
                case "part":
                    return YearDistributionPart.class;
                case "startDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<YearPartStartEndDate> _dslPath = new Path<YearPartStartEndDate>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "YearPartStartEndDate");
    }
            

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.YearPartStartEndDate#getEducationYear()
     */
    public static EducationYear.Path<EducationYear> educationYear()
    {
        return _dslPath.educationYear();
    }

    /**
     * @return Часть года. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.YearPartStartEndDate#getPart()
     */
    public static YearDistributionPart.Path<YearDistributionPart> part()
    {
        return _dslPath.part();
    }

    /**
     * @return Начальная дата. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.YearPartStartEndDate#getStartDate()
     */
    public static PropertyPath<Date> startDate()
    {
        return _dslPath.startDate();
    }

    /**
     * @return Конечная дата. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.YearPartStartEndDate#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    public static class Path<E extends YearPartStartEndDate> extends EntityPath<E>
    {
        private EducationYear.Path<EducationYear> _educationYear;
        private YearDistributionPart.Path<YearDistributionPart> _part;
        private PropertyPath<Date> _startDate;
        private PropertyPath<Date> _endDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.YearPartStartEndDate#getEducationYear()
     */
        public EducationYear.Path<EducationYear> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new EducationYear.Path<EducationYear>(L_EDUCATION_YEAR, this);
            return _educationYear;
        }

    /**
     * @return Часть года. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.YearPartStartEndDate#getPart()
     */
        public YearDistributionPart.Path<YearDistributionPart> part()
        {
            if(_part == null )
                _part = new YearDistributionPart.Path<YearDistributionPart>(L_PART, this);
            return _part;
        }

    /**
     * @return Начальная дата. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.YearPartStartEndDate#getStartDate()
     */
        public PropertyPath<Date> startDate()
        {
            if(_startDate == null )
                _startDate = new PropertyPath<Date>(YearPartStartEndDateGen.P_START_DATE, this);
            return _startDate;
        }

    /**
     * @return Конечная дата. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.YearPartStartEndDate#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(YearPartStartEndDateGen.P_END_DATE, this);
            return _endDate;
        }

        public Class getEntityClass()
        {
            return YearPartStartEndDate.class;
        }

        public String getEntityName()
        {
            return "yearPartStartEndDate";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
