package ru.tandemservice.narfu.component.student.EntrantInfoStudentTab;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.narfu.utils.NarfuEntrantUtil;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.component.entrant.EntrantMarkTab.MarkStatisticCollector;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.util.EntrantDataUtil;

import java.util.*;


public class DAO extends UniDao<Model> implements IDAO {

    private static final OrderDescriptionRegistry _entrantStateExamCertificatesOrderSettings = new OrderDescriptionRegistry("certificate");


    @Override
    public void delete(IEntity entity) {
        super.delete(entity);
    }

    @Override
    public void delete(Long entityId) {
        super.delete(entityId);
    }

    public void prepare(Model model) {
        RequestedEnrollmentDirection requestedEnrollmentDirection = NarfuEntrantUtil.getRequestedEnrollmentDirectionByStudent(model.getStudent());
        if (requestedEnrollmentDirection != null)
            model.setHasEntrantData(true);
        else model.setHasEntrantData(false);

        if (model.isHasEntrantData()) {
            model.setRequestedEnrollmentDirection(requestedEnrollmentDirection);
            model.setEntrant(requestedEnrollmentDirection.getEntrantRequest().getEntrant());
        }
    }


//---------------------------Сертификаты ЕГЭ

    @Override
    public void prepareEntrantStateExamCertificatesDataSource(Model model) {
        DynamicListDataSource<EntrantStateExamCertificate> dataSource = model.getEntrantStateExamCertificatesDataSource();
        dataSource.setCountRow(3L);

        MQBuilder builder = new MQBuilder(EntrantStateExamCertificate.ENTITY_CLASS, "certificate");
        builder.add(MQExpression.eq("certificate", EntrantStateExamCertificate.L_ENTRANT, model.getEntrant()));
        _entrantStateExamCertificatesOrderSettings.applyOrder(builder, dataSource.getEntityOrder());
        UniUtils.createPage(dataSource, builder, getSession());

        model.setCertificate2SubjectsMarks(prepareCertificatesSubjectsMarks(model.getEntrantStateExamCertificatesDataSource().getEntityList()));

    }

    public Map<EntrantStateExamCertificate, List<StateExamSubjectMark>> prepareCertificatesSubjectsMarks(List<EntrantStateExamCertificate> certificates) {
        Map<EntrantStateExamCertificate, List<StateExamSubjectMark>> result = new HashMap<>();
        if (!certificates.isEmpty()) {
            for (EntrantStateExamCertificate certificate : certificates) {
                result.put(certificate, new ArrayList<StateExamSubjectMark>());
            }

            Criteria criteria = getSession().createCriteria(StateExamSubjectMark.class);
            criteria.add(Restrictions.in(StateExamSubjectMark.L_CERTIFICATE, certificates));
            criteria.createAlias(StateExamSubjectMark.L_SUBJECT, "subject");
            criteria.addOrder(Order.asc("subject.title"));
            List<StateExamSubjectMark> list = (List<StateExamSubjectMark>) criteria.list();

            for (StateExamSubjectMark mark : list) {
                result.get(mark.getCertificate()).add(mark);
            }
        }
        return result;
    }

    @Override
    public void changeCertificateOriginal(Long id) {
        EntrantStateExamCertificate certificate = getNotNull(EntrantStateExamCertificate.class, id);
        certificate.setOriginal(!certificate.isOriginal());
        update(certificate);
    }

    @Override
    public void changeCertificateChecked(Long id) {
        EntrantStateExamCertificate certificate = getNotNull(EntrantStateExamCertificate.class, id);
        certificate.setAccepted(!certificate.isAccepted());
        update(certificate);
    }

    @Override
    public void changeCertificateSent(Long id) {
        EntrantStateExamCertificate certificate = getNotNull(EntrantStateExamCertificate.class, id);
        certificate.setSent(!certificate.isSent());
        update(certificate);
    }

    //------------------------Дипломы олимпиад

    protected List<OlympiadDiploma> getDiplomaList(Entrant entrant) {
        return getList(OlympiadDiploma.class, OlympiadDiploma.L_ENTRANT, entrant);
    }

    public void prepareOlympiadDiplomaDataSource(Model model) {
        List<OlympiadDiploma> diplomaList = getDiplomaList(model.getEntrant());

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(Discipline2OlympiadDiplomaRelation.class, "e")
                .column("e")
                .where(DQLExpressions.eq(DQLExpressions.property(Discipline2OlympiadDiplomaRelation.diploma().entrant().fromAlias("e")), DQLExpressions.value(model.getEntrant())));
        List<Discipline2OlympiadDiplomaRelation> relationList = getList(dql);

        Map<OlympiadDiploma, List<Discipline2OlympiadDiplomaRelation>> map = new Hashtable<>();
        for (Discipline2OlympiadDiplomaRelation relation : relationList) {
            List<Discipline2OlympiadDiplomaRelation> list = map.get(relation.getDiploma());
            if (list == null)
                map.put(relation.getDiploma(), list = new ArrayList<>());
            list.add(relation);
        }

        List<Model.OlympiadDiplomaWrapper> resultList = new ArrayList<>();

        for (OlympiadDiploma diploma : diplomaList) {
            List<Discipline2OlympiadDiplomaRelation> list = map.get(diploma);

            if ((list == null) || (list.isEmpty())) {
                resultList.add(new Model.OlympiadDiplomaWrapper(diploma, null, null));
            }
            else {
                List<EnrollmentDirection> enrollmentDirectionList = new ArrayList<>();
                List<CompensationType> compensationTypeList = new ArrayList<>();
                List<String> titleList = new ArrayList<>();

                for (Discipline2OlympiadDiplomaRelation relation : list) {
                    enrollmentDirectionList.add(relation.getEnrollmentDirection());
                    compensationTypeList.add(relation.getCompensationType());
                    titleList.add(relation.getDiscipline().getTitle());
                }

                Collections.sort(titleList);

                EnrollmentDirection enrollmentDirection = enrollmentDirectionList.iterator().next();
                CompensationType compensationType = compensationTypeList.iterator().next();

                String enrollmentDirectionTitle = enrollmentDirection.getTitle() + " (" + compensationType.getShortTitle() + ")";

                String disciplineListTitle = StringUtils.join(titleList, ", ");

                resultList.add(new Model.OlympiadDiplomaWrapper(diploma, enrollmentDirectionTitle, disciplineListTitle));
            }
        }

        if (resultList.isEmpty())
            model.getOlympiadDiplomaDataSource().setCountRow(2L);
        else {
            model.getOlympiadDiplomaDataSource().setCountRow(resultList.size());
        }
        UniUtils.createPage(model.getOlympiadDiplomaDataSource(), resultList);
    }

    //---------------------Оценки

    public void prepareMarkResultDataSource(Model model) {
        Session session = getSession();
        EnrollmentCampaign enrollmentCampaign = model.getEntrant().getEnrollmentCampaign();
        boolean compensationTypeDiff = enrollmentCampaign.isEnrollmentPerCompTypeDiff();
        Set<MultiKey> realizationFormSet = UniecDAOFacade.getEntrantDAO().getRealizationFormSet(enrollmentCampaign);
        CompensationType compensationTypeBudget = getCatalogItem(CompensationType.class, "1");
        CompensationType compensationTypeContract = getCatalogItem(CompensationType.class, "2");

        MQBuilder directionBuilder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r");
        directionBuilder.add(MQExpression.eq("r", RequestedEnrollmentDirection.P_ID, model.getRequestedEnrollmentDirection().getId()));

        EntrantDataUtil dataUtil = new EntrantDataUtil(session, enrollmentCampaign, directionBuilder, EntrantDataUtil.DetailLevel.EXAM_PASS_WITH_MARK_STATISTICS);

        MarkStatisticCollector.CollectScope collectScope = MarkStatisticCollector.CollectScope.ENTRANT_REQUEST;
        MarkStatisticCollector collector = new MarkStatisticCollector(collectScope, compensationTypeDiff, dataUtil);

        List<Model.DisciplineMarkWrapper> result = new ArrayList<>();
        for (Discipline2RealizationWayRelation discipline : collector.getDisciplineSet()) {
            String stateExamMark = null;
            String stateExamScaledMark = null;
            String olympiadMark = null;

            String stateExamInternalMark = null;
            String stateExamInternalMarkAppeal = null;
            String examMark = null;
            String examMarkAppeal = null;
            String testMark = null;
            String testMarkAppeal = null;
            String interviewMark = null;
            String interviewMarkAppeal = null;

            Double[] finalMark = {0.0D, 0.0D};

            if (collector.getStateExamMark(discipline) != null) {
                stateExamMark = Integer.toString(collector.getStateExamMark(discipline).getMark());
                if (compensationTypeDiff) {
                    String budgetMark = "-";
                    String contractMark = "-";
                    Double budget = collector.getScaledStateExamMarkBudget(discipline);
                    Double contract = collector.getScaledStateExamMarkContract(discipline);
                    if (budget != null) {
                        finalMark[0] = budget;
                        budgetMark = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(finalMark[0]);
                    }
                    if (contract != null) {
                        finalMark[1] = contract;
                        contractMark = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(finalMark[1]);
                    }
                    stateExamScaledMark = budgetMark + "/" + contractMark;
                }
                else {
                    finalMark[0] = collector.getScaledStateExamMark(discipline);
                    stateExamScaledMark = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(finalMark[0]);
                }

            }

            if (collector.getOlympiad(discipline) != null) {
                olympiadMark = Integer.toString(collector.getOlympiad(discipline));
                finalMark[0] = (double) discipline.getMaxMark();
                finalMark[1] = (double) discipline.getMaxMark();
            }

            for (Map.Entry<SubjectPassForm, PairKey<ExamPassMark, ExamPassMarkAppeal>> entryKey : collector.getInternalMarkMap(discipline).entrySet()) {
                SubjectPassForm passForm = entryKey.getKey();
                String[] marks = NarfuEntrantUtil.getMarkForForm(finalMark, entryKey.getValue(), passForm, realizationFormSet, compensationTypeDiff, discipline, compensationTypeBudget, compensationTypeContract, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS);

                if ("5".equals(passForm.getCode())) {
                    stateExamInternalMark = marks[0];
                    stateExamInternalMarkAppeal = marks[1];
                }
                else if ("3".equals(passForm.getCode())) {
                    examMark = marks[0];
                    examMarkAppeal = marks[1];
                }
                else if ("2".equals(passForm.getCode())) {
                    testMark = marks[0];
                    testMarkAppeal = marks[1];
                }
                else if ("4".equals(passForm.getCode())) {
                    interviewMark = marks[0];
                    interviewMarkAppeal = marks[1];
                }
            }
            String finalMarkValue;
            if (compensationTypeDiff)
                finalMarkValue = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(finalMark[0]) + "/" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(finalMark[1]);
            else {
                finalMarkValue = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(finalMark[0]);
            }

            Model.DisciplineMarkWrapper wrapper = new Model.DisciplineMarkWrapper(discipline, stateExamMark, stateExamScaledMark, olympiadMark, finalMarkValue);

            wrapper.setStateExamInternalMark(stateExamInternalMark);
            wrapper.setStateExamInternalMarkAppeal(stateExamInternalMarkAppeal);
            wrapper.setExamMark(examMark);
            wrapper.setExamMarkAppeal(examMarkAppeal);
            wrapper.setTestMark(testMark);
            wrapper.setTestMarkAppeal(testMarkAppeal);
            wrapper.setInterviewMark(interviewMark);
            wrapper.setInterviewMarkAppeal(interviewMarkAppeal);
            result.add(wrapper);
        }

        Collections.sort(result, ITitled.TITLED_COMPARATOR);
        DynamicListDataSource<Model.DisciplineMarkWrapper> dataSource = model.getMarkResultDataSource();
        dataSource.setCountRow(result.size());
        UniUtils.createPage(dataSource, result);
    }


}
