package ru.tandemservice.narfu.base.ext.Person.ui.SocialPaymentEdit;

import org.tandemframework.common.dao.IComponentBaseDAO;

public interface IDAO extends IComponentBaseDAO {

    void prepare(Model model);

    void update(Model model);

}
