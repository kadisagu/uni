package ru.tandemservice.narfu.uniec.component.report.ActPersonInfo.Add;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.narfu.uniec.entity.report.NarfuActPersonInfo;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


public class MakeReport {

    private Model _model = null;
    private Session _session = null;
    private NarfuActPersonInfo _report = null;

    public MakeReport(NarfuActPersonInfo report, Model model, Session session)
    {
        _report = report;
        _model = model;
        _session = session;
    }

    public DatabaseFile getContent()
    {
        byte data[] = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private byte[] buildReport()
    {
        // просматриваем по формирующим подразделениям
        IScriptItem templateDocument
                = (IScriptItem) UniDaoFacade.getCoreDao().getCatalogItem
                (
                        UniecScriptItem.class, "narfuActPersonInfo"
                );
        RtfDocument document = (new RtfReader()).read(templateDocument.getCurrentTemplate().clone());

        RtfDocument outDoc = document.getClone();
        outDoc.getElementList().clear();

        for (OrgUnit ou : _model.getFormativeOrgUnitList()) {
            RtfDocument doc = _getOuReport(ou, document);
            if (doc != null)
                outDoc.getElementList().addAll(doc.getElementList());
        }
        return RtfUtil.toByteArray(outDoc);
    }

    private RtfDocument _getOuReport(OrgUnit ou, RtfDocument template)
    {
        Map<EducationOrgUnit, Map<DevelopForm, Map<CompensationType, List<PreliminaryEnrollmentStudent>>>> map = new HashMap<EducationOrgUnit, Map<DevelopForm, Map<CompensationType, List<PreliminaryEnrollmentStudent>>>>();

        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EnrollmentExtract.class, "ee");

        // отсекаем приемку
        dql.where(eq(property(EnrollmentExtract.entity().requestedEnrollmentDirection().enrollmentDirection().enrollmentCampaign().fromAlias("ee")), value(_model.getEnrollmentCampaign())));

        // отсекаем формирующие подразделения
        dql.where(eq(property(EnrollmentExtract.entity().educationOrgUnit().formativeOrgUnit().fromAlias("ee")), value(ou)));

        // выводим студентов предв зачисления
        dql.joinPath(DQLJoinType.inner, EnrollmentExtract.entity().fromAlias("ee"), "prestud");
        dql.addColumn("prestud");

        // сортировка
        dql.order(property(EnrollmentExtract.entity().requestedEnrollmentDirection().entrantRequest().entrant().person().identityCard().fullFio().fromAlias("ee")));

        List<PreliminaryEnrollmentStudent> prestudents = dql.createStatement(_session).list();

        for (PreliminaryEnrollmentStudent preStud : prestudents) {

            EducationOrgUnit educationOU = preStud.getEducationOrgUnit();

            Map<DevelopForm, Map<CompensationType, List<PreliminaryEnrollmentStudent>>> m_eduou;
            if (map.containsKey(educationOU))
                m_eduou = map.get(educationOU);
            else {
                m_eduou = new HashMap<DevelopForm, Map<CompensationType, List<PreliminaryEnrollmentStudent>>>();
                map.put(educationOU, m_eduou);
            }

            DevelopForm df = educationOU.getDevelopForm();

            Map<CompensationType, List<PreliminaryEnrollmentStudent>> m_df;
            if (m_eduou.containsKey(df))
                m_df = m_eduou.get(df);
            else {
                m_df = new HashMap<CompensationType, List<PreliminaryEnrollmentStudent>>();
                m_eduou.put(df, m_df);
            }

            CompensationType ct = preStud.getCompensationType();

            List<PreliminaryEnrollmentStudent> m_ct;
            if (m_df.containsKey(ct))
                m_ct = m_df.get(ct);
            else {
                m_ct = new ArrayList<PreliminaryEnrollmentStudent>();
                m_df.put(ct, m_ct);
            }
            m_ct.add(preStud);

            String fio = preStud.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getPerson().getFio();
            System.out.println(fio);
        }

        RtfDocument outPage = template.getClone();
        outPage.getElementList().clear();

        int i = 0;

        // теперь формируем листы отчета
        for (EducationOrgUnit eou : map.keySet()) {
            Map<DevelopForm, Map<CompensationType, List<PreliminaryEnrollmentStudent>>> m_eou = map.get(eou);
            for (DevelopForm df : m_eou.keySet()) {
                Map<CompensationType, List<PreliminaryEnrollmentStudent>> m_df = m_eou.get(df);
                for (CompensationType ct : m_df.keySet()) {
                    i++;
                    List<PreliminaryEnrollmentStudent> lst = m_df.get(ct);
                    RtfDocument page = _makePage(template, ou, eou, df, ct, lst);
                    outPage.getElementList().addAll(page.getElementList());
                }
            }
        }

        if (i > 0)
            return outPage;
        else
            return null;

    }

    private RtfDocument _makePage(RtfDocument template, OrgUnit ou, EducationOrgUnit eou,
                                  DevelopForm df, CompensationType ct, List<PreliminaryEnrollmentStudent> lst)
    {

        RtfDocument outDoc = template.getClone();

        RtfInjectModifier im = new RtfInjectModifier();

        String FormPr = ou.getGenitiveCaseTitle();
        if (FormPr == null)
            FormPr = ou.getTitle();
        im.put("FormPr", FormPr);

        String FormP = ou.getTitle();
        im.put("FormP", FormP);

        String dateA = RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(_model.getReport().getDateCreate());
        im.put("dateA", dateA);

        String numA = _model.getReport().getDocNumber();
        im.put("numA", numA);

        String fo = df.getTitle();
        im.put("fo", fo);

        String Napr = eou.getTitle();
        im.put("Napr", Napr);

        String Bud = ct.getTitle();
        if (ct.getCode().equals("1"))
            Bud = "за счет средств федерального бюджета";

        if (ct.getCode().equals("2"))
            Bud = "с полным возмещением затрат на обучение";

        im.put("Bud", Bud);

        im.modify(outDoc);

        // создаем таблицу
        List<String[]> lines = new ArrayList<String[]>();
        int i = 0;
        for (PreliminaryEnrollmentStudent ps : lst) {
            i++;

            Entrant entrant = ps.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant();
            Person person = ps.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getPerson();

            PersonEduInstitution pedu = person.getPersonEduInstitution();

            List<String> line = new ArrayList<String>();

            // номер строки
            line.add(Integer.toString(i));

            // фио
            String fio = ps.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getPerson().getFullFio();
            line.add(fio);


            String educationInfo = "";
            // документ о полученном образовании
            if (pedu != null) {
                if (pedu.getDocumentType() != null)
                    educationInfo += pedu.getDocumentType().getTitle();
                if (pedu.getSeria() != null)
                    educationInfo += " серия: " + pedu.getSeria();

                if (pedu.getNumber() != null)
                    educationInfo += " номер: " + pedu.getNumber();

            }
            line.add(educationInfo);

            // ЕГЭ
            DQLSelectBuilder builder = new DQLSelectBuilder()
                    .fromEntity(EntrantStateExamCertificate.class, "es")
                    .addColumn("es")
                    .where(DQLExpressions.eq(DQLExpressions.property(EntrantStateExamCertificate.entrant().fromAlias("es")),
                                             DQLExpressions.value(entrant)));
            List<EntrantStateExamCertificate> esList = builder.createStatement(_session).<EntrantStateExamCertificate>list();

            StringBuffer ege = new StringBuffer();
            for (EntrantStateExamCertificate es : esList) {
                if (ege.length() > 0)
                    ege.append(", ");
                if (es.getNumber() != null)
                    ege.append("№ " + es.getNumber());
                if (es.getIssuanceDate() != null)
                    ege.append(" от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(es.getIssuanceDate()));
            }
            line.add(ege.toString());

            // приказ о зачислении
            DQLSelectBuilder dql = new DQLSelectBuilder();
            dql.fromEntity(EnrollmentExtract.class, "ee");
            // по студенту
            dql.where(eq(property(EnrollmentExtract.entity().fromAlias("ee")), value(ps)));
            dql.addColumn("ee");
            List<EnrollmentExtract> lstEe = dql.createStatement(_session).list();

            StringBuffer order = new StringBuffer();
            for (EnrollmentExtract ee : lstEe) {
                String number = ee.getParagraph().getOrder().getNumber();
                String orderDate = "";

                if (ee.getParagraph().getOrder().getCommitDate() != null)
                    orderDate = DateFormatter.DEFAULT_DATE_FORMATTER.format(ee.getParagraph().getOrder().getCommitDate());

                if (order.length() > 0)
                    order.append(", ");

                order.append("№" + number + " от " + orderDate + " пар: " + ee.getParagraph().getNumber());
            }
            line.add(order.toString());

            // примечания по студентам
            String prim = "";
            if (ps.getStudentCategory().getCode().endsWith(StudentCategoryCodes.STUDENT_CATEGORY_LISTENER))
                prim = "слушатель";

            if (ps.isTargetAdmission()) {
                TargetAdmissionKind targetAdmissionKind = ps.getRequestedEnrollmentDirection().getTargetAdmissionKind();

                if (targetAdmissionKind != null)
                    prim += targetAdmissionKind.getTitle();
                else
                    prim += " ЦП ";
            }

            if (ps.getRequestedEnrollmentDirection().getCompetitionKind().getCode().equals("3")) {
                // вне конкурса - выводим льготы
                DQLSelectBuilder dqlPB = new DQLSelectBuilder();
                dqlPB.fromEntity(PersonBenefit.class, "pb");
                dqlPB.where(eq(property(PersonBenefit.person().fromAlias("pb")), value(person)));
                dqlPB.addColumn("pb");
                List<PersonBenefit> benefits = dqlPB.createStatement(_session).list();

                if (!benefits.isEmpty()) {
                    List<String> lgotNames = new ArrayList<String>();
                    for (PersonBenefit benefit : benefits) {
                        lgotNames.add(benefit.getBenefit().getShortTitle());
                    }
                    prim += StringUtils.join(lgotNames, ",");
                }
            }
            line.add(prim);

            lines.add(line.toArray(new String[]{}));

        }

        RtfTableModifier tm = new RtfTableModifier();
        tm.put("T", lines.toArray(new String[][]{}));
        tm.modify(outDoc);


        return outDoc;
    }

}
