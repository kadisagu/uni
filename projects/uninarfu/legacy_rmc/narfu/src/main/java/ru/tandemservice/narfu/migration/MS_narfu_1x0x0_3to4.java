package ru.tandemservice.narfu.migration;

import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudentrmc.entity.PersonNARFU;

public class MS_narfu_1x0x0_3to4 extends IndependentMigrationScript {

    @Override
    public void run(DBTool tool) throws Exception {

        IEntityMeta meta = EntityRuntime.getMeta(Person.class);
        IEntityMeta meta2 = EntityRuntime.getMeta(PersonNARFU.class);

        String personTable = meta.getTableName();
        String personNarfuTable = meta2.getTableName();

        tool.executeUpdate("update " + personTable + " set " + personTable + ".innnumber_p = " +
                                   personNarfuTable + ".inn_p from " + personTable + ", " + personNarfuTable +
                                   " where " + personTable + ".id = " + personNarfuTable + ".person_id and " +
                                   personNarfuTable + ".inn_p is not null");

        tool.executeUpdate("update " + personTable + " set " + personTable + ".SNILSNUMBER_P = " +
                                   personNarfuTable + ".pensioninsurancenumber_p from " + personTable + ", " + personNarfuTable +
                                   " where " + personTable + ".id = " + personNarfuTable + ".person_id and " +
                                   personNarfuTable + ".pensioninsurancenumber_p is not null");
    }
}		