package ru.tandemservice.narfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.narfu.entity.DocumentType2StudentCategory;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.StudentDocumentType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь между типом документа и категорией обучающегося
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DocumentType2StudentCategoryGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.narfu.entity.DocumentType2StudentCategory";
    public static final String ENTITY_NAME = "documentType2StudentCategory";
    public static final int VERSION_HASH = -998817413;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT_DOCUMENT_TYPE = "studentDocumentType";
    public static final String L_STUDENT_CATEGORY = "studentCategory";

    private StudentDocumentType _studentDocumentType;     // Тип документа, выдаваемого студенту
    private StudentCategory _studentCategory;     // Категория обучаемого

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип документа, выдаваемого студенту. Свойство не может быть null.
     */
    @NotNull
    public StudentDocumentType getStudentDocumentType()
    {
        return _studentDocumentType;
    }

    /**
     * @param studentDocumentType Тип документа, выдаваемого студенту. Свойство не может быть null.
     */
    public void setStudentDocumentType(StudentDocumentType studentDocumentType)
    {
        dirty(_studentDocumentType, studentDocumentType);
        _studentDocumentType = studentDocumentType;
    }

    /**
     * @return Категория обучаемого. Свойство не может быть null.
     */
    @NotNull
    public StudentCategory getStudentCategory()
    {
        return _studentCategory;
    }

    /**
     * @param studentCategory Категория обучаемого. Свойство не может быть null.
     */
    public void setStudentCategory(StudentCategory studentCategory)
    {
        dirty(_studentCategory, studentCategory);
        _studentCategory = studentCategory;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DocumentType2StudentCategoryGen)
        {
            setStudentDocumentType(((DocumentType2StudentCategory)another).getStudentDocumentType());
            setStudentCategory(((DocumentType2StudentCategory)another).getStudentCategory());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DocumentType2StudentCategoryGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DocumentType2StudentCategory.class;
        }

        public T newInstance()
        {
            return (T) new DocumentType2StudentCategory();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "studentDocumentType":
                    return obj.getStudentDocumentType();
                case "studentCategory":
                    return obj.getStudentCategory();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "studentDocumentType":
                    obj.setStudentDocumentType((StudentDocumentType) value);
                    return;
                case "studentCategory":
                    obj.setStudentCategory((StudentCategory) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "studentDocumentType":
                        return true;
                case "studentCategory":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "studentDocumentType":
                    return true;
                case "studentCategory":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "studentDocumentType":
                    return StudentDocumentType.class;
                case "studentCategory":
                    return StudentCategory.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DocumentType2StudentCategory> _dslPath = new Path<DocumentType2StudentCategory>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DocumentType2StudentCategory");
    }
            

    /**
     * @return Тип документа, выдаваемого студенту. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.DocumentType2StudentCategory#getStudentDocumentType()
     */
    public static StudentDocumentType.Path<StudentDocumentType> studentDocumentType()
    {
        return _dslPath.studentDocumentType();
    }

    /**
     * @return Категория обучаемого. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.DocumentType2StudentCategory#getStudentCategory()
     */
    public static StudentCategory.Path<StudentCategory> studentCategory()
    {
        return _dslPath.studentCategory();
    }

    public static class Path<E extends DocumentType2StudentCategory> extends EntityPath<E>
    {
        private StudentDocumentType.Path<StudentDocumentType> _studentDocumentType;
        private StudentCategory.Path<StudentCategory> _studentCategory;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип документа, выдаваемого студенту. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.DocumentType2StudentCategory#getStudentDocumentType()
     */
        public StudentDocumentType.Path<StudentDocumentType> studentDocumentType()
        {
            if(_studentDocumentType == null )
                _studentDocumentType = new StudentDocumentType.Path<StudentDocumentType>(L_STUDENT_DOCUMENT_TYPE, this);
            return _studentDocumentType;
        }

    /**
     * @return Категория обучаемого. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.DocumentType2StudentCategory#getStudentCategory()
     */
        public StudentCategory.Path<StudentCategory> studentCategory()
        {
            if(_studentCategory == null )
                _studentCategory = new StudentCategory.Path<StudentCategory>(L_STUDENT_CATEGORY, this);
            return _studentCategory;
        }

        public Class getEntityClass()
        {
            return DocumentType2StudentCategory.class;
        }

        public String getEntityName()
        {
            return "documentType2StudentCategory";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
