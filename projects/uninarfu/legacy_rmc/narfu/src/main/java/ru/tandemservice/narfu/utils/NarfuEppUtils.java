package ru.tandemservice.narfu.utils;

import org.apache.tapestry.form.IFormComponent;
import org.apache.tapestry.form.ValidationMessages;
import org.apache.tapestry.form.translator.Translator;
import org.apache.tapestry.valid.ValidationConstraint;
import org.apache.tapestry.valid.ValidatorException;
import org.tandemframework.tapsupport.translator.StrongNumberTranslator;
import ru.tandemservice.uniepp.UniEppUtils;

public class NarfuEppUtils {

    public static final Translator LOAD_TRANSLATOR = new StrongNumberTranslator("pattern=0.##") {
        @Override
        protected Object parseText(IFormComponent field, ValidationMessages messages, String text) throws ValidatorException {
            Number result = (Number) super.parseText(field, messages, text);
            if (null == result) {
                return null;
            }

            // число должно быть кратно 0.25
            double value = result.doubleValue() * 100.0;
            if (UniEppUtils.eq(value, Math.round(value))) {
                return result;
            }

            // выводим сообщение об ошибке
            throw new ValidatorException(field.getDisplayName() + " не пропроционально 0.01", ValidationConstraint.NUMBER_FORMAT);
        }
    };
}
