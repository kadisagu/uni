package ru.tandemservice.narfu.component.reports.orgUnitStudentGrantByInstitute.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.narfu.entity.NarfuReportStudentGrantByInstitute;

public class Model extends ru.tandemservice.narfu.component.reports.orgUnitStudentGrantByInstitute.Add.Model
{

    private IDataSettings settings;

    private DynamicListDataSource<NarfuReportStudentGrantByInstitute> dataSource;

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public DynamicListDataSource<NarfuReportStudentGrantByInstitute> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<NarfuReportStudentGrantByInstitute> dataSource) {
        this.dataSource = dataSource;
    }

}
