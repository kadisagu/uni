package ru.tandemservice.narfu.uniec.component.order.EnrollmentOrderPub;

import ru.tandemservice.uniec.UniecDefines;

public class Model extends ru.tandemservice.uniec.component.order.EnrollmentOrderPub.Model {

    public boolean isOrderTypeAfterFirstCourse() {
        if (getOrder() == null || getOrder().getType() == null)
            return false;
        else {
            // Переопределенные нами типы приказа "О зачислении на 2 и последующие курсы (бюджет/договор)"
            return (getOrder().getType().getCode().equals(UniecDefines.ORDER_TYPE_STUDENT_BUDGET_SHORT)
                    || getOrder().getType().getCode().equals(UniecDefines.ORDER_TYPE_STUDENT_CONTRACT_SHORT));
        }

    }
}
