package ru.tandemservice.narfu.base.ext.Person.ui.SocialPaymentEdit;

import org.tandemframework.common.dao.ComponentBaseDAO;
import ru.tandemservice.movestudentrmc.entity.PersonNARFU;


public class DAO extends ComponentBaseDAO implements IDAO {


    @Override
    public void prepare(Model model) {
        PersonNARFU personNARFU = get(PersonNARFU.class, PersonNARFU.L_PERSON, model.getPerson());
        if (personNARFU == null) {
            personNARFU = new PersonNARFU();
            personNARFU.setPerson(model.getPerson());
        }
        model.setPersonNARFU(personNARFU);
    }

    @Override
    public void update(Model model) {
        model.getPersonNARFU().setNeedSocialPayment(model.getNeedSocialPayment().isTrue());
        saveOrUpdate(model.getPersonNARFU());
    }
}
