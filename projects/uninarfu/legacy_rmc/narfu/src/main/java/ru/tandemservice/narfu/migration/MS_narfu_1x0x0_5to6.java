package ru.tandemservice.narfu.migration;

import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;

import java.sql.ResultSet;
import java.util.Arrays;
import java.util.List;

/**
 * Ненужные сущности
 * 25    EduplanVersion
 * 38    UniDSBulletin
 * 129   UniDSBulletinType
 * 136   WorkPlanAdditAction
 * 264   EduPlanDiscEduLoad
 * 362   UniDSBulletinDoubleToStudent
 * 400   UniDSAttestationSlot
 * 484   PracticeGroup
 * 493   PracticeGroupToWeekType
 * 499   StateEduStdDevPerRel
 * 576   AttestationDiscipline
 * 596   UniDSOuterMarkTransfer
 * 620   EduProcSchedSpecLoad
 * 665   UniDSGradeValue
 * 734   EduPlanAdditActionToCathedra
 * 747   DisciplineType
 * 758   EduLoadType
 * 760   UniDSELCDocument
 * 761   UniDSTemplate
 * 787   UniDSMarkTransferDocument
 * 820   EduPlanTheorWeeksAmntByTerms
 * 919   UniDSBulletinDouble
 * 992   ControlAction2BulletinType
 * 1006  DisciplineToCathedraRel
 * 1042  EduplanState
 * 1069  ComponentDiscipline
 * 1225  UniDSSessionResultRep
 * 1239  UniDSMarkTransfer
 * 1296  EducationalWeeks
 * 1297  StateEduStandIdxCompDisc
 * 1341  UniPlanEduLoadType
 * 1371  Practice
 * 1411  EduPlanAdditAction
 * 1419  WorkplanToGroup
 * 1473  UniDSCardCreationReasons
 * 1477  UniDSMarkTransferDocument2Reason
 * 1490  ControlAction2GradeScale
 * 1491  ControlAction
 * 1498  Workplan
 * 1500  WorkTypeWithEP
 * 1532  UniDSMarkTransferReason
 * 1533  LoadingPeriod
 * 1562  EduPlanDiscExclusion
 * 1563  WorkPlanCtrlAction
 * 1643  EduplanVersionToGroup
 * 1685  Commission2EmployeePost
 * 1712  Attestation
 * 1754  EduplanVerSpecRel
 * 1827  EduPlanTermsCourse
 * 1833  EduplanVersionToStudent
 *
 * @author Viktor
 */

public class MS_narfu_1x0x0_5to6 extends IndependentMigrationScript {

    public static final List<String> dropTables = Arrays.asList(
            "stateedustddevperrel_t", "eduplanverspecrel_t", "unidsinnermarktransfer_t", "eduplandisciplinetype_t",
            "stateedustandardindexdisc_t", "stateedustandidxcompdisc_t", "eduloadtype_t", "1111",
            "disciplineindex_t", "unidsdocument_t", "uniplandisciplinebase_t", "eduplandisc_t",
            "eduplangroupdisc_t", "extraeduloadtype_t", "eduprocschedulespeciality_t", "eduplanindexcomponentdisc_t",
            "eduplandisctermtocact_t", "workplanelement_t", "eduplanversiontotloadsrel_t", "unidsmarkvaluebase_t",
            "commission2employeepost_t", "dplnvrsnttlwksamntbytrms_t", "temporarydataforcopy_t", "examallowancepolicy_t",
            "unidscard_t", "workplandisc_t", "unidsorgunitsessionresultrep_t", "stateedustdnestedcatalogdisc_t",
            "nocrsoncourse_t", "educationalweeks_t", "eduplanadditactiontocathedra_t", "stateedustandarddisc_t",
            "unidspracticeslot_t", "showruptoorgunitkindrel_t", "unidstemplate_t", "disciplinetype_t",
            "eduprocessschedule_t", "controlaction2bulletintype_t", "practice_t", "attestationgroup_t",
            "eduleveltoworktypesrel_t", "eduplanversiontogroup_t", "unidsslot_t", "eduplanorgunitsrel_t",
            "unidsbulletin_t", "controlaction_t", "unidsoutermarktransfer_t", "workplanadditaction_t",
            "studentstatus2bulletintype_t", "workplanadditactiondisc_t", "practicediscipline_t", "worktypewithep_t",
            "stateedustandardgroupdisc_t", "eduplandiscextraeduload_t", "loadingperiod_t", "workplanctrlaction_t",
            "eduplannestedcatalogdisc_t", "eduplanstate_t", "eduplanversiontostudent_t", "worktypewithgos_t",
            "workplandisceduload_t", "unidselcdocument_t", "controlaction2gradescale_t", "disciplinetocathedrarel_t",
            "ndsmrktrnsfrdcmnt2rsn_t", "unidscommission_t", "unidscardcreationreasons_t", "eduplandisceduload_t",
            "unidstesttable_t", "unidsgradevalue_t", "unidsmarktransferreason_t", "practicegroup_t",
            "stateedustandardchoicedisc_t", "componentdiscipline_t", "unidsgradescale_t", "unidsdisciplineslot_t",
            "unidsmarktransfer_t", "workplantogroup_t", "stateedustandard_t", "eduplanchoicedisc_t",
            "eduplantermscourse_t", "unidsmarktransferdocument_t", "attestationdiscipline_t", "stateedustandardverspecrel_t",
            "developformtoloadingperiods_t", "discipline_t", "uniplanadditactionbase_t", "stateedustandardversion_t",
            "unidsattestationslot_t", "eduplancatalogdisc_t", "eduplandiscexclusiontype_t", "eduplanadditaction_t",
            "eduprocschedspecload_t", "eduplanversionweektypesrel_t", "attestation_t", "unidsbulletintype_t",
            "unidssessionresultrep_t", "eduplanversion_t", "unidsbulletindoubletostudent_t", "uniplaneduloadtype_t",
            "practicegrouptoweektype_t", "eduplantheorweeksamntbyterms_t", "controltoadditaction_t", "loadingdestination_t",
            "eduplanindexdisc_t", "unidsbulletindouble_t", "eduplandiscexclusion_t", "workplandiscweekeduload_t",
            "stateedustandardcatalogdisc_t", "ndssssnrsltbydscplnsrp_t", "workplantostudent_t", "eduplan_t",
            "extendeddevelopform_t", "eduweektypes_t", "eduloadtyperelation_t", "workplan_t");

    public static final List<String> disabledModules = Arrays.asList(
            "unids_001x002x004", "unids_002x000x006", "unidsrmc_001x000x000",
            "uniplan_002x000x000", "uniplan_002x000x006");

    @Override
    public void run(DBTool tool) throws Exception {

        synchronizeCodes(tool);

        // cleanVersion(tool);
        // doDropTable(tool);
    }


    /**
     * Удаляет версии миграций
     * отключенных модулей
     *
     * @param tool
     *
     * @throws Exception
     */
    private void cleanVersion(DBTool tool) throws Exception {
        for (String module : disabledModules) {
            tool.executeUpdate("delete from version_s where MODULE=? ", module);
        }
    }

    /**
     * Определяем ненужные сущности
     * и удаляем их
     *
     * @param tool
     *
     * @throws Exception
     */
    private void synchronizeCodes(DBTool tool) throws Exception {
        //Почистим ClassCode
        ResultSet localList = tool.getStatement().executeQuery("select c.id,  c.classname_p from classcode_t as  c");
        while (localList.next()) {
            String id = localList.getString(1);
            String className = localList.getString(2);
            if (className.startsWith("*entity*")) {
                className = className.substring("*entity*".length());
                if (!EntityRuntime.isEntity(className)) {
                    tool.executeUpdate("delete from  classcode_t where id=?", id);
                }
            }
        }
        //Почистим ENTITYCODE_S
        tool.executeUpdate("delete from ENTITYCODE_S where state_p='2'");
    }

    /**
     * Удалим неиспользуемые таблицы
     *
     * @param tool
     *
     * @throws Exception
     */
    private void doDropTable(DBTool tool) throws Exception {

        for (String tableName : dropTables) {
            if (tool.tableExists(tableName)) {

                tool.dropTable(tableName);
            }
        }


    }

}		