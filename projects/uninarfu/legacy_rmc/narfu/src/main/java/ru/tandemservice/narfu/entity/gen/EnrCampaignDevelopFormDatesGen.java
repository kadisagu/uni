package ru.tandemservice.narfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.narfu.entity.EnrCampaignDevelopFormDates;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Даты окончания подачи оригиналов документов от формы освоения для приемной кампании
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrCampaignDevelopFormDatesGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.narfu.entity.EnrCampaignDevelopFormDates";
    public static final String ENTITY_NAME = "enrCampaignDevelopFormDates";
    public static final int VERSION_HASH = -1481889665;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_DEVELOP_FORM = "developForm";
    public static final String P_DOC_DATE = "docDate";

    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private DevelopForm _developForm;     // Форма освоения
    private Date _docDate;     // Дата сдачи

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения. Свойство не может быть null.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Дата сдачи. Свойство не может быть null.
     */
    @NotNull
    public Date getDocDate()
    {
        return _docDate;
    }

    /**
     * @param docDate Дата сдачи. Свойство не может быть null.
     */
    public void setDocDate(Date docDate)
    {
        dirty(_docDate, docDate);
        _docDate = docDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrCampaignDevelopFormDatesGen)
        {
            setEnrollmentCampaign(((EnrCampaignDevelopFormDates)another).getEnrollmentCampaign());
            setDevelopForm(((EnrCampaignDevelopFormDates)another).getDevelopForm());
            setDocDate(((EnrCampaignDevelopFormDates)another).getDocDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrCampaignDevelopFormDatesGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrCampaignDevelopFormDates.class;
        }

        public T newInstance()
        {
            return (T) new EnrCampaignDevelopFormDates();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "developForm":
                    return obj.getDevelopForm();
                case "docDate":
                    return obj.getDocDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
                case "docDate":
                    obj.setDocDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "developForm":
                        return true;
                case "docDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "developForm":
                    return true;
                case "docDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "developForm":
                    return DevelopForm.class;
                case "docDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrCampaignDevelopFormDates> _dslPath = new Path<EnrCampaignDevelopFormDates>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrCampaignDevelopFormDates");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.EnrCampaignDevelopFormDates#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.EnrCampaignDevelopFormDates#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Дата сдачи. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.EnrCampaignDevelopFormDates#getDocDate()
     */
    public static PropertyPath<Date> docDate()
    {
        return _dslPath.docDate();
    }

    public static class Path<E extends EnrCampaignDevelopFormDates> extends EntityPath<E>
    {
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private DevelopForm.Path<DevelopForm> _developForm;
        private PropertyPath<Date> _docDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.EnrCampaignDevelopFormDates#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.EnrCampaignDevelopFormDates#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Дата сдачи. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.EnrCampaignDevelopFormDates#getDocDate()
     */
        public PropertyPath<Date> docDate()
        {
            if(_docDate == null )
                _docDate = new PropertyPath<Date>(EnrCampaignDevelopFormDatesGen.P_DOC_DATE, this);
            return _docDate;
        }

        public Class getEntityClass()
        {
            return EnrCampaignDevelopFormDates.class;
        }

        public String getEntityName()
        {
            return "enrCampaignDevelopFormDates";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
