package ru.tandemservice.narfu.component.settings.FormingOrgUnit2Commission.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.List;

public class Model implements IEnrollmentCampaignSelectModel {

    private DynamicListDataSource<Wrapper> _dataSource;
    private IDataSettings _settings;
    private List<EnrollmentCampaign> _enrollmentCampaignList;

    public DynamicListDataSource<Wrapper> getDataSource() {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<Wrapper> dataSource) {
        this._dataSource = dataSource;
    }


    public EnrollmentCampaign getEnrollmentCampaign() {
        return (EnrollmentCampaign) getSettings().get("enrollmentCampaign");
    }

    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign) {
        getSettings().set("enrollmentCampaign", enrollmentCampaign);
    }

    public List<EnrollmentCampaign> getEnrollmentCampaignList() {
        return this._enrollmentCampaignList;
    }

    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList) {
        this._enrollmentCampaignList = enrollmentCampaignList;
    }

    public IDataSettings getSettings() {
        return this._settings;
    }

    public void setSettings(IDataSettings settings) {
        this._settings = settings;
    }

}
