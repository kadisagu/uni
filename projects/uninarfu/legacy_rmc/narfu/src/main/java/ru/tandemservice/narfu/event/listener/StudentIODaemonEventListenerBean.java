package ru.tandemservice.narfu.event.listener;

import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import ru.tandemservice.narfu.event.dao.IStudentIODaemonBean;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Set;

public class StudentIODaemonEventListenerBean implements IDSetEventListener
{
    public void init()
    {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, Student.class, this);

    }

    @Override
    public void onEvent(DSetEvent event)
    {
        Set affectedProperties = event.getMultitude().getAffectedProperties();

        if (affectedProperties.contains("archival"))
        {
            IStudentIODaemonBean.instance.get().updateArchivalIdentityCard(event);
        }
    }

}
