package ru.tandemservice.narfu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_narfu_1x0x0_36to37 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {

        // переезд сущностей в модуль militaryrmc

//		////////////////////////////////////////////////////////////////////////////////
//		// сущность militaryPrintReport
//
//		// сущность была удалена
//		{
//			
//			// удалить таблицу
//			tool.dropTable("militaryprintreport_t", false /* - не удалять, если есть ссылающиеся таблицы */);
//
//			// удалить код сущности
//			tool.entityCodes().delete("militaryPrintReport");
//
//		}
//
//		////////////////////////////////////////////////////////////////////////////////
//		// сущность personMilitaryStatusRMC
//
//		// сущность была удалена
//		{
//			
//			// удалить таблицу
//			tool.dropTable("personmilitarystatusrmc_t", false /* - не удалять, если есть ссылающиеся таблицы */);
//
//			// удалить код сущности
//			tool.entityCodes().delete("personMilitaryStatusRMC");
//
//		}
//
//		////////////////////////////////////////////////////////////////////////////////
//		// сущность militaryTemplateDocument
//
//		// сущность была удалена
//		{
//			
//			// удалить таблицу
//			tool.dropTable("militarytemplatedocument_t", false /* - не удалять, если есть ссылающиеся таблицы */);
//
//			// удалить код сущности
//			tool.entityCodes().delete("militaryTemplateDocument");
//
//		}
//
//		////////////////////////////////////////////////////////////////////////////////
//		// сущность militarySpecialRegistrationSettings
//
//		// сущность была удалена
//		{
//			
//			// удалить таблицу
//			tool.dropTable("mltryspclrgstrtnsttngs_t", false /* - не удалять, если есть ссылающиеся таблицы */);
//
//			// удалить код сущности
//			tool.entityCodes().delete("militarySpecialRegistrationSettings");
//
//		}
//
//		////////////////////////////////////////////////////////////////////////////////
//		// сущность militaryRegDataSettings
//
//		// сущность была удалена
//		{
//			
//			// удалить таблицу
//			tool.dropTable("militaryregdatasettings_t", false /* - не удалять, если есть ссылающиеся таблицы */);
//
//			// удалить код сущности
//			tool.entityCodes().delete("militaryRegDataSettings");
//
//		}
//
//		////////////////////////////////////////////////////////////////////////////////
//		// сущность militaryDataSettings
//
//		// сущность была удалена
//		{
//			
//			// удалить таблицу
//			tool.dropTable("militarydatasettings_t", false /* - не удалять, если есть ссылающиеся таблицы */);
//
//			// удалить код сущности
//			tool.entityCodes().delete("militaryDataSettings");
//
//		}
//
//		////////////////////////////////////////////////////////////////////////////////
//		// сущность militaryCompositionSettings
//
//		// сущность была удалена
//		{
//			
//			// удалить таблицу
//			tool.dropTable("militarycompositionsettings_t", false /* - не удалять, если есть ссылающиеся таблицы */);
//
//			// удалить код сущности
//			tool.entityCodes().delete("militaryCompositionSettings");
//
//		}
//
//		////////////////////////////////////////////////////////////////////////////////
//		// сущность militaryComposition
//
//		// сущность была удалена
//		{
//			
//			// удалить таблицу
//			tool.dropTable("militarycomposition_t", false /* - не удалять, если есть ссылающиеся таблицы */);
//
//			// удалить код сущности
//			tool.entityCodes().delete("militaryComposition");
//
//		}
//
//		////////////////////////////////////////////////////////////////////////////////
//		// сущность militaryAbilityStatusSettings
//
//		// сущность была удалена
//		{
//			
//			// удалить таблицу
//			tool.dropTable("mltryabltysttssttngs_t", false /* - не удалять, если есть ссылающиеся таблицы */);
//
//			// удалить код сущности
//			tool.entityCodes().delete("militaryAbilityStatusSettings");
//
//		}


    }
}