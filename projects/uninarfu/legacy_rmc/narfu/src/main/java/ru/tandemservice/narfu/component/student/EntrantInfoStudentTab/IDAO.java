package ru.tandemservice.narfu.component.student.EntrantInfoStudentTab;

import ru.tandemservice.uni.dao.IUniDao;


public interface IDAO extends IUniDao<Model> {
    public void prepareEntrantStateExamCertificatesDataSource(Model model);

    public void prepareOlympiadDiplomaDataSource(Model model);

    public void prepareMarkResultDataSource(Model model);

    public void changeCertificateOriginal(Long id);

    public void changeCertificateChecked(Long id);

    public void changeCertificateSent(Long id);
}
