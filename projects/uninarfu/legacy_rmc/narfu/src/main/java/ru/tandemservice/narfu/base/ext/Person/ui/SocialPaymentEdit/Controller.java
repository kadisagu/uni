package ru.tandemservice.narfu.base.ext.Person.ui.SocialPaymentEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController {

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = component.getModel();
        ((IDAO) getDao()).prepare(model);

    }

    public void onClickApply(IBusinessComponent component)
    {
        Model model = component.getModel();
        ((IDAO) getDao()).update(model);
        deactivate(component);
    }
}
