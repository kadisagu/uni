package ru.tandemservice.narfu.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Код квалификации"
 * Имя сущности : narfuEducationLevelNumber
 * Файл data.xml : catalogs.data.xml
 */
public interface NarfuEducationLevelNumberCodes
{
    /** Константа кода (code) элемента : bachelor (code). Название (title) : Бакалавр */
    String BACHELOR = "bachelor";
    /** Константа кода (code) элемента : speciality (code). Название (title) : Специалитет */
    String SPECIALITY = "speciality";
    /** Константа кода (code) элемента : master (code). Название (title) : Магистратура */
    String MASTER = "master";

    Set<String> CODES = ImmutableSet.of(BACHELOR, SPECIALITY, MASTER);
}
