package ru.tandemservice.narfu.base.ext.EcDistribution.logic;

import org.tandemframework.rtf.util.RtfUtil;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgDistribution;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistribution;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

public class EcDistributionDaoExt extends UniBaseDao implements IEcDistributionDaoExt {

    @Override
    public byte[] getRtfPrintNarfu(Long distributionId) {
        IEcgDistribution distribution = (IEcgDistribution) getNotNull(distributionId);

        getSession().refresh(distribution);

        IEcDistributionListPrintNarfu printForm = (IEcDistributionListPrintNarfu) IEcDistributionListPrintNarfu.instance.get();

        return RtfUtil.toByteArray((distribution.getConfig().getEcgItem() instanceof EnrollmentDirection) ?
                                           printForm.getPrintFormForDistributionPerDirection(((UniecScriptItem) getByCode(UniecScriptItem.class, "ecgDistributionPerDirection")).getCurrentTemplate(), (EcgDistribution) distribution) :
                                           printForm.getPrintFormForDistributionPerCompetitionGroupNarfu(((UniecScriptItem) getByCode(UniecScriptItem.class, "ecgDistributionPerCompetitionGroupNarfu")).getCurrentTemplate(), distribution));

    }


}
