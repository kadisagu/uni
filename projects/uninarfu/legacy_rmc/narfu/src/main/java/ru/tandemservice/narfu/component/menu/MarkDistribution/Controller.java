package ru.tandemservice.narfu.component.menu.MarkDistribution;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.uniec.dao.print.IEntrantBulletinPrintBean;
import ru.tandemservice.uniec.dao.print.IEntrantExamBlankPrintBean;
import ru.tandemservice.uniecmarkextrmc.component.menu.MarkDistribution.DisciplineWrapper;
import ru.tandemservice.uniecmarkextrmc.component.menu.MarkDistribution.EntrantDisciplineMarkWrapper;
import ru.tandemservice.uni.util.ReportRenderer;
import ru.tandemservice.uniecmarkextrmc.component.menu.MarkDistribution.Model;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Controller extends ru.tandemservice.uniecmarkextrmc.component.menu.MarkDistribution.Controller
{
    public void onClickPrintBulletin(IBusinessComponent component) {
        Model model = component.getModel();
        List<EntrantDisciplineMarkWrapper> wrp = model.getDataSourceExt().getEntityList();
        DisciplineWrapper dsc = model.getDisciplineExt();
        IEntrantBulletinPrintBean bean = (IEntrantBulletinPrintBean) ApplicationRuntime.getBean("entrantBulletinPrintBean");
        RtfDocument report = bean.generateEntrantBulletin(wrp, dsc);
        BusinessComponentUtils.downloadDocument(new ReportRenderer("entrantBulletin.rtf", report), true);
    }

    public void onClickGenerateWorkCodes(IBusinessComponent component) {
        Model model = component.getModel();
        AtomicInteger beginCode = ((IDAO) getDao()).getMaxCode();
        generateWorkCodes(beginCode, model);

        super.onClickApply(component);
        super.onRefreshComponent(component);
    }

    public void onClickPrintBlanks(IBusinessComponent component)
    {
        super.onClickApply(component);
        Model model = component.getModel();
        byte[] report;
        IEntrantExamBlankPrintBean bean = (IEntrantExamBlankPrintBean) ApplicationRuntime.getBean("entrantExamBlankPrintBean");
        List<EntrantDisciplineMarkWrapper> notValid = getEmptyWorkCodes(model);
        String docTitle;
        if (notValid.size() > 0)
            docTitle = "ErrorReport.rtf";
        else
            docTitle = "entrantExamBlanks.rtf";
        report = bean.generateEntrantExamBlank(model);
        BusinessComponentUtils.downloadDocument(new ReportRenderer(docTitle, report), true);
    }


    public void generateWorkCodes(AtomicInteger beginCode, Model model) {
        List<EntrantDisciplineMarkWrapper> wrps = model.getDataSourceExt().getEntityList();
        for (EntrantDisciplineMarkWrapper item : wrps) {
            String code = item.getExamPassMarkIstu().getWorkCipher();
            if (code != null)
                continue;

            code = generateCode(beginCode.incrementAndGet());
            item.getExamPassMarkIstu().setWorkCipher(code);
        }
    }

    private String generateCode(Integer code) {
        String base = "00000000";
        String retVal = base + String.valueOf(code);
        return retVal.substring(retVal.length() - 8, retVal.length());
    }

    List<EntrantDisciplineMarkWrapper> getEmptyWorkCodes(Model model)
    {
        List<EntrantDisciplineMarkWrapper> wrps = model.getDataSourceExt().getEntityList();
        List<EntrantDisciplineMarkWrapper> result = new ArrayList<>();
        for (EntrantDisciplineMarkWrapper item : wrps)
            if (item.getExamPassMarkIstu() == null || item.getExamPassMarkIstu().getWorkCipher() == null)
                result.add(item);
        return result;

    }
}
