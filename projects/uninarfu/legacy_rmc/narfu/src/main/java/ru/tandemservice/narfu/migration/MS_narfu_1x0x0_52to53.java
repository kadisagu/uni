package ru.tandemservice.narfu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.Statement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_narfu_1x0x0_52to53 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.5.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.5.2"),
                        new ScriptDependency("ru.tandemservice.uni.project", "2.5.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность choiceOrientationSettings
        if (tool.tableExists("choiceorientationsettings_t"))
        // создано обязательное свойство educationYear
        {
            // создать колонку
            if (!tool.columnExists("choiceorientationsettings_t", "educationyear_id"))
                tool.createColumn("choiceorientationsettings_t", new DBColumn("educationyear_id", DBType.LONG));

            // задать значение по умолчанию
            Statement stmt = tool.getConnection().createStatement();
            stmt.execute("select id from educationyear_t where current_p = 1");
            java.lang.Long defaultEducationYear = null;
            if (stmt.getResultSet().next())
                defaultEducationYear = stmt.getResultSet().getLong(1);
            tool.executeUpdate("update choiceorientationsettings_t set educationyear_id=? where educationyear_id is null", defaultEducationYear);

            // сделать колонку NOT NULL
            tool.setColumnNullable("choiceorientationsettings_t", "educationyear_id", false);

        }


    }
}