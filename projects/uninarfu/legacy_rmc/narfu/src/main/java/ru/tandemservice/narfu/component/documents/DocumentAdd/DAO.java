package ru.tandemservice.narfu.component.documents.DocumentAdd;

import ru.tandemservice.narfu.utils.NarfuUtil;
import ru.tandemservice.uni.entity.employee.Student;

public class DAO extends ru.tandemservice.uni.component.documents.DocumentAdd.DAO {

    @Override
    public void prepare(ru.tandemservice.uni.component.documents.DocumentAdd.Model model) {
        Student student = getNotNull(Student.class, model.getStudentId());
        model.setStudentDocumentTypeList(NarfuUtil.getStudentDocumentTypeByCategory(student));
    }
}
