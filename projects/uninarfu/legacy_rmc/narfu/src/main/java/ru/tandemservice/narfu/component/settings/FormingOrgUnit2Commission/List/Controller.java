package ru.tandemservice.narfu.component.settings.FormingOrgUnit2Commission.List;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.uni.UniUtils;


public class Controller extends AbstractBusinessController<IDAO, Model> {
    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = getModel(component);
        model.setSettings(component.getSettings());
        prepareListDateSource(component);
        getDao().prepare(model);


    }

    private void prepareListDateSource(IBusinessComponent component) {
        Model model = getModel(component);
        if (model.getDataSource() != null) {
            return;
        }
        else {
            DynamicListDataSource<Wrapper> dataSource = UniUtils.createDataSource(component, getDao());
            dataSource.addColumn((new SimpleColumn("Формирующее подразделение", new String[]{"formingOrgUnit", "title"})).setClickable(false).setOrderable(false));
            dataSource.addColumn((new SimpleColumn("Отборочная коммиссия", new String[]{"commission", "title"})).setClickable(false).setOrderable(false));
            dataSource.addColumn((new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditRelation").setPermissionKey("menuFormingOrgUnit2CommissionEdit")));
            model.setDataSource(dataSource);
        }
    }

    public void onRefresh(IBusinessComponent component) {
        component.saveSettings();
    }

    public void onClickEditRelation(IBusinessComponent component) {

        UniMap map = new UniMap();
        map.add("enrollmentCampaignId", getModel(component).getEnrollmentCampaign().getId());
        map.add("formingOrgUnitId", component.getListenerParameter());
        ContextLocal.createDesktop("PersonShellDialog", new ComponentActivator("ru.tandemservice.narfu.component.settings.FormingOrgUnit2Commission.Edit", map));

    }


}
