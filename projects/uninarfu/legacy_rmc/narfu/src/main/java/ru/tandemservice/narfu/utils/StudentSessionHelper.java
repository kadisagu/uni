package ru.tandemservice.narfu.utils;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkGradeValueCatalogItemCodes;
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkStateCatalogItemCodes;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.*;

public class StudentSessionHelper {
    private EducationYear year;
    private YearDistributionPart part;

    private DevelopForm paramDevelopForm;
    private CompensationType paramCompensationType;

    private IUniBaseDao dao = UniDaoFacade.getCoreDao();

    private Set<Long> setDebt;
    private Set<Long> set3;
    private Set<Long> set4;
    private Set<Long> set45;
    private Set<Long> set5;

    public StudentSessionHelper(EducationYear year, YearDistributionPart part) {
        this.year = year;
        this.part = part;
    }

    public void calculate() {
        MQBuilder studentBuilder = createStudentsBuilder(null, null);
        MQBuilder markBuilder = createMarksBuilder();
        List<Long> listEmpty = getEmptyStudents(markBuilder, studentBuilder);

        MQBuilder builder = createMarksBuilder()
                .add(MQExpression.eq("m", SessionMark.slot().inSession(), Boolean.TRUE))
                .add(MQExpression.or(
                        MQExpression.eq("m", SessionMark.cachedMarkValue().code(), SessionMarkStateCatalogItemCodes.NO_ADMISSION), //не должен здавать
                        MQExpression.eq("m", SessionMark.cachedMarkValue().code(), SessionMarkStateCatalogItemCodes.EXPELLED), //выбыл
                        MQExpression.eq("m", SessionMark.cachedMarkValue().code(), SessionMarkGradeValueCatalogItemCodes.OTLICHNO)
                ));
        List<Long> list5 = this.dao.getList(builder);

        builder = createMarksBuilder()
                .add(MQExpression.eq("m", SessionMark.slot().inSession(), Boolean.TRUE))
                .add(MQExpression.eq("m", SessionMark.cachedMarkValue().code(), SessionMarkGradeValueCatalogItemCodes.HOROSHO));
        List<Long> list4 = this.dao.getList(builder);

        builder = createMarksBuilder()
                .add(MQExpression.eq("m", SessionMark.slot().inSession(), Boolean.TRUE))
                .add(MQExpression.eq("m", SessionMark.cachedMarkValue().code(), SessionMarkGradeValueCatalogItemCodes.UDOVLETVORITELNO));
        List<Long> list3 = this.dao.getList(builder);

        builder = createMarksBuilder()
                .add(MQExpression.eq("m", SessionMark.slot().inSession(), Boolean.TRUE))
                .add(MQExpression.eq("m", SessionMark.cachedMarkPositiveStatus(), Boolean.FALSE)); //хреновая оценка
        List<Long> list2 = this.dao.getList(builder);

        builder = createMarksBuilder()
                .add(MQExpression.eq("m", SessionMark.slot().inSession(), Boolean.FALSE))
                .add(MQExpression.eq("m", SessionMark.cachedMarkPositiveStatus(), Boolean.FALSE)); //хреновая оценка
        List<Long> listDebt = this.dao.getList(builder);


        //должники = есть плохие оценки(итог) + нет оценок вообще
        this.setDebt = new HashSet<>(listEmpty);
        this.setDebt.addAll(listDebt);

        //3 = <есть 3> - <должники> - <есть 2>
        this.set3 = new HashSet<>(list3);
        this.set3.removeAll(this.setDebt);
        this.set3.removeAll(list2);

        //4 = <есть 4> - <есть 5> - <есть 3> - <должники> - <есть 2>
        this.set4 = new HashSet<>(list4);
        this.set4.removeAll(list5);
        this.set4.removeAll(list3);
        this.set4.removeAll(this.setDebt);
        this.set4.removeAll(list2);

        //45 = (<есть 4> and <есть 5>) - <есть 3> - <должники> - <4> - <есть 2>
        this.set45 = new HashSet<>(list5);
        this.set45.retainAll(list4);
        this.set45.removeAll(list3);
        this.set45.removeAll(this.setDebt);
        this.set45.removeAll(list2);

        //5 = <есть 5> - <есть 4> - <есть 3> - <должники> - <4> - <есть 2>
        this.set5 = new HashSet<>(list5);
        this.set5.removeAll(list4);
        this.set5.removeAll(list3);
        this.set5.removeAll(this.setDebt);
        this.set5.removeAll(list2);
    }

    public Set<Long> getSetDebt() {
        return this.setDebt;
    }

    public Set<Long> getSet3() {
        return this.set3;
    }

    public Set<Long> getSet4() {
        return this.set4;
    }

    public Set<Long> getSet45() {
        return this.set45;
    }

    public Set<Long> getSet5() {
        return this.set5;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public MQBuilder createMarksBuilder() {
        MQBuilder builder = new MQBuilder(SessionMark.ENTITY_CLASS, "m")
                .add(MQExpression.eq("m", SessionMark.slot().studentWpeCAction().studentWpe().year().educationYear(), this.year))
                .add(MQExpression.eq("m", SessionMark.slot().studentWpeCAction().studentWpe().part(), this.part))
                .add(MQExpression.isNull("m", SessionMark.slot().studentWpeCAction().removalDate()))
                .addJoin("m", SessionMark.slot().studentWpeCAction().studentWpe(), "d")
                .add(MQExpression.isNull("d", EppStudentWorkPlanElement.studentEduPlanVersion().removalDate()));

        builder.getSelectAliasList().clear();
        builder.addSelect(EppStudentWorkPlanElement.student().id().fromAlias("d").s());
        builder.setNeedDistinct(true);

        return builder;
    }

    //select student.id ... where formOrgUnit=orgUnitList and studentStatusCode=studentStatusCodeList and params..
    public MQBuilder createStudentsBuilder(List<OrgUnit> orgUnitList, List<String> studentStatusCodeList) {
        if (orgUnitList == null || (orgUnitList.size() == 1 && orgUnitList.get(0) == null))
            orgUnitList = Collections.emptyList();
        if (studentStatusCodeList == null)
            studentStatusCodeList = Collections.emptyList();

        MQBuilder builder = new MQBuilder(Student.ENTITY_CLASS, "st")
                .add(MQExpression.eq("st", Student.archival(), Boolean.FALSE)); //не архивный

        if (this.paramDevelopForm != null)
            builder.add(MQExpression.eq("st", Student.educationOrgUnit().developForm(), this.paramDevelopForm));

        if (this.paramCompensationType != null)
            builder.add(MQExpression.eq("st", Student.compensationType(), this.paramCompensationType));

        if (studentStatusCodeList.isEmpty())
            builder.add(MQExpression.notEq("st", Student.status().code(), UniDefines.CATALOG_STUDENT_STATUS_EXCLUDED));
        else
            builder.add(MQExpression.in("st", Student.status().code(), studentStatusCodeList));

        if (!orgUnitList.isEmpty())
            builder.add(MQExpression.in("st", Student.educationOrgUnit().formativeOrgUnit(), orgUnitList));

        builder.getSelectAliasList().clear();
        builder.addSelect(Student.id().fromAlias("st").s());
        builder.setNeedDistinct(true);

        return builder;
    }

    public MQBuilder applyDetalization(MQBuilder builder, String alias, Map<String, Object> detalization) {
        if (detalization == null)
            detalization = new HashMap<>();

        for (Map.Entry<String, Object> entry : detalization.entrySet()) {
            String field = entry.getKey();
            Object value = entry.getValue();

            if (value instanceof MQBuilder)
                builder.add(MQExpression.in(alias, field, (MQBuilder) value));
            else if (value instanceof Collection)
                builder.add(MQExpression.in(alias, field, (Collection) value));
            else
                builder.add(MQExpression.eq(alias, field, value));
        }

        return builder;
    }

    //список студентов у которых нет оценок(статусов)
    public List<Long> getEmptyStudents(MQBuilder marksBuilder, MQBuilder studentBuilder) {
        MQBuilder subBuilder = new MQBuilder(marksBuilder);
        subBuilder.getSelectAliasList().clear();
        subBuilder.addSelect(SessionMark.slot().studentWpeCAction().id().fromAlias("m").s());
        subBuilder.setNeedDistinct(true);

        MQBuilder builder = new MQBuilder(EppStudentWpeCAction.ENTITY_CLASS, "slot")
                .add(MQExpression.isNull("slot", EppStudentWpeCAction.removalDate()))
                .add(MQExpression.eq("slot", EppStudentWpeCAction.studentWpe().year().educationYear(), this.year))
                .add(MQExpression.eq("slot", EppStudentWpeCAction.studentWpe().part(), this.part))
                .add(MQExpression.in("slot", EppStudentWpeCAction.studentWpe().student().id(), studentBuilder))
                .add(MQExpression.notIn("slot", EppStudentWpeCAction.id(), subBuilder));

        builder.getSelectAliasList().clear();
        builder.addSelect(EppStudentWpeCAction.studentWpe().student().id().fromAlias("slot").s());
        builder.setNeedDistinct(true);

        return this.dao.getList(builder);
    }

    public List<OrgUnit> getChildren(OrgUnit ou) {
        List<OrgUnit> resultList = new ArrayList<>();
        resultList.add(ou);

        List<OrgUnit> list = resultList;
        do {
            MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "o")
                    .add(MQExpression.in("o", OrgUnit.parent(), list))
                    .add(MQExpression.notEq("o", OrgUnit.orgUnitType().code(), OrgUnitTypeCodes.BRANCH))
                    .add(MQExpression.eq("o", OrgUnit.archival(), Boolean.FALSE));

            list = this.dao.getList(builder);
            resultList.addAll(list);
        } while (!list.isEmpty());

        //отбираем ток те, которые есть в EducationOrgUnit
        MQBuilder builder = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "eou")
                .add(MQExpression.in("eou", EducationOrgUnit.formativeOrgUnit(), resultList));
        if (this.paramDevelopForm != null)
            builder.add(MQExpression.eq("eou", EducationOrgUnit.developForm(), this.paramDevelopForm));

        builder.getSelectAliasList().clear();
        builder.addSelect(EducationOrgUnit.formativeOrgUnit().id().fromAlias("eou").s());
        builder.setNeedDistinct(true);
        List<Long> ids = dao.getList(builder);

        resultList = dao.getList(OrgUnit.class, ids, OrgUnit.title().s());
        return resultList;
    }

    @SuppressWarnings("unchecked")
    public Collection<Long> intersection(Collection<Long> set1, Collection<Long> set2) {
        return CollectionUtils.intersection(set1, set2);
    }
    ///////////////////////////////////////////////////////////////////////////////////////

    public DevelopForm getParamDevelopForm() {
        return paramDevelopForm;
    }

    public void setParamDevelopForm(DevelopForm paramDevelopForm) {
        this.paramDevelopForm = paramDevelopForm;
    }

    public CompensationType getParamCompensationType() {
        return paramCompensationType;
    }

    public void setParamCompensationType(CompensationType paramCompensationType) {
        this.paramCompensationType = paramCompensationType;
    }


}
