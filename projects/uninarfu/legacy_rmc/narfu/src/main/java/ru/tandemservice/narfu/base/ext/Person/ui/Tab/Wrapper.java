package ru.tandemservice.narfu.base.ext.Person.ui.Tab;

import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;

import java.util.Date;

public class Wrapper extends EntityBase {

    private PersonEduInstitution base;

    private Date dateIssueCertificate;     // Дата выдачи аттестата из ЛД
    private Date expectedDateReturn;     // Планируемая дата возврата в ЛД
    private Date actualDateReturn;     // Планируемая дата возврата в ЛД

    @Override
    public Long getId() {
        return base.getId();
    }

    public PersonEduInstitution getBase() {
        return base;
    }

    public void setBase(PersonEduInstitution base) {
        this.base = base;
    }

    public Date getDateIssueCertificate() {
        return dateIssueCertificate;
    }

    public void setDateIssueCertificate(Date dateIssueCertificate) {
        this.dateIssueCertificate = dateIssueCertificate;
    }

    public Date getExpectedDateReturn() {
        return expectedDateReturn;
    }

    public void setExpectedDateReturn(Date expectedDateReturn) {
        this.expectedDateReturn = expectedDateReturn;
    }

    public Date getActualDateReturn() {
        return actualDateReturn;
    }

    public void setActualDateReturn(Date actualDateReturn) {
        this.actualDateReturn = actualDateReturn;
    }
}
