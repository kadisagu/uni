package ru.tandemservice.narfu.component.reports.orgUnitStudentGrantByInstitute.Add;

import com.google.common.collect.ImmutableMap;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.*;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import org.tandemframework.shared.person.base.util.PersonSecurityUtil;
import ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab.MonthWrapper;
import ru.tandemservice.movestudentrmc.entity.RelTypeGrantView;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntity;
import ru.tandemservice.movestudentrmc.entity.catalog.StuGrantStatus;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.StuGrantStatusCodes;
import ru.tandemservice.narfu.entity.NarfuReportStudentGrantByInstitute;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkGradeValueCatalogItemCodes;
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkStateCatalogItemCodes;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.lang.Boolean;
import java.util.*;

public class ReportGenerator {

    public static final String TEMPLATE = "orgUnitGrantByInstitute";

    //input params
    protected EducationYear year;
    protected YearDistributionPart part;
    protected DevelopForm developForm;
    protected MonthWrapper grantMonth;
    protected EducationYear grantYear;

    protected WritableWorkbook workbook;
    protected WritableSheet sheet;
    protected ByteArrayOutputStream workbookStream;

    protected List<DetailRow> sheetRows = new ArrayList<>();

    protected StuGrantStatus activeGrantStatus;

    private IUniBaseDao dao;

    public static Long createReport(EducationYear year, YearDistributionPart part, Date month, DevelopForm developForm) {
        try {
            ReportGenerator generator = new ReportGenerator(year, part, month, developForm);
            NarfuReportStudentGrantByInstitute report = generator.generateReport();

            UniDaoFacade.getCoreDao().saveOrUpdate(report.getContent());
            UniDaoFacade.getCoreDao().saveOrUpdate(report);

            return report.getId();
        }
        catch (Exception ex) {
            ex.printStackTrace();
            throw new ApplicationException(ex.getMessage(), ex);
        }
    }

    protected ReportGenerator(EducationYear year, YearDistributionPart part, Date month, DevelopForm developForm) {
        this.dao = UniDaoFacade.getCoreDao();

        this.year = year;
        this.part = part;
        this.developForm = developForm;

        MonthWrapper.Data data = MonthWrapper.getInstance(month);
        this.grantYear = data.educationYear;
        this.grantMonth = data.monthWrapper;

        this.activeGrantStatus = dao.getCatalogItem(StuGrantStatus.class, StuGrantStatusCodes.CODE_1);
    }

    protected NarfuReportStudentGrantByInstitute generateReport() throws Exception {
        NarfuReportStudentGrantByInstitute report = new NarfuReportStudentGrantByInstitute();
        report.setFormingDate(new Date());
        report.setEducationYear(this.year.getTitle());
        report.setEducationYearPart(this.part.getTitle());
        report.setMonth(this.grantMonth.getStringValue(this.grantYear));
        report.setDevelopForm(this.developForm.getTitle());
        report.setExecutor(PersonSecurityUtil.getExecutor());

        //fill excel
        loadTemplate();
        fillReport();
        this.workbook.write();
        this.workbook.close();

        //put excel
        DatabaseFile reportFile = new DatabaseFile();
        reportFile.setContent(this.workbookStream.toByteArray());
        reportFile.setFilename("Начисление стипендии.xls");
        reportFile.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_VND_MS_EXCEL);
        report.setContent(reportFile);

        return report;
    }

    protected void fillReport() throws Exception {
        WritableCell cell = (WritableCell) this.sheet.getCell(0, 0);
        if (cell != null) {
            String title = cell.getContents();
            title = title
                    .replace("developForm", this.developForm.getTitle().toLowerCase() + " форма")
                    .replace("month", RussianDateFormatUtils.getMonthName(this.grantMonth.getMonthNumber() + 1, true))
                    .replace("year", this.grantYear.getTitle());
            ((Label) cell).setString(title);
        }

        loadData();
        loadStatistic();
        writeRows();
    }

    protected void writeRows() throws Exception {
        int columnStart = 0;
        int rowStart = 4;

        WritableCellFormat format = createFormat(false, false);
        WritableCellFormat formatBold = createFormat(true, false);
        WritableCellFormat formatRight = createFormat(false, false);
        WritableCellFormat formatRightBold = createFormat(true, false);
        formatRight.setAlignment(Alignment.RIGHT);

        WritableCellFormat formatVert = createFormat(false, true);
        formatVert.setVerticalAlignment(VerticalAlignment.CENTRE);

        int currentRow = rowStart;
        int prevRow = rowStart;
        String prevRowTitle = null;
        for (DetailRow row : this.sheetRows) {
            if (!row.hasData())
                continue;

            WritableCell cell;

            //1
            String title1 = row.parent.branch.isTop() ? "Головной вуз" : row.parent.branch.getTitle();
            if (row.parent.vpo)
                title1 += " (ВПО)";
            else
                title1 += " (СПО)";

            if (prevRowTitle == null || !prevRowTitle.equals(title1)) {
                cell = createCell(columnStart, currentRow, title1, formatVert);
                this.sheet.addCell(cell);

                if (prevRowTitle != null)
                    this.sheet.mergeCells(0, prevRow, 0, currentRow - 1);

                prevRowTitle = title1;
                prevRow = currentRow;
            }

            //2
            cell = createCell(columnStart + 1, currentRow, row.title, row.bold ? formatBold : format);
            this.sheet.addCell(cell);

            //3..15
            for (int i = 0; i < 13; i++) {
                cell = createCell(columnStart + 2 + i, currentRow, row.values[i], (row.bold || i == 12) ? formatRightBold : formatRight);
                this.sheet.addCell(cell);
            }

            currentRow++;
        }

        if (prevRow < currentRow - 1)
            this.sheet.mergeCells(0, prevRow, 0, currentRow - 1);

    }

    protected void loadStatistic() {
        //списки студентов по оценкам
        List<Long> listEmpty = getEmptyStudents();

        MQBuilder builder = getMarksBuilder()
                .add(MQExpression.eq("m", SessionMark.slot().inSession(), Boolean.TRUE))
                .add(MQExpression.or(
                        MQExpression.eq("m", SessionMark.cachedMarkValue().code(), SessionMarkStateCatalogItemCodes.NO_ADMISSION), //не должен здавать
                        MQExpression.eq("m", SessionMark.cachedMarkValue().code(), SessionMarkStateCatalogItemCodes.EXPELLED), //выбыл
                        MQExpression.eq("m", SessionMark.cachedMarkValue().code(), SessionMarkGradeValueCatalogItemCodes.OTLICHNO)
                ));
        List<Long> list5 = this.dao.getList(builder);

        builder = getMarksBuilder()
                .add(MQExpression.eq("m", SessionMark.slot().inSession(), Boolean.TRUE))
                .add(MQExpression.eq("m", SessionMark.cachedMarkValue().code(), SessionMarkGradeValueCatalogItemCodes.HOROSHO));
        List<Long> list4 = this.dao.getList(builder);

        builder = getMarksBuilder()
                .add(MQExpression.eq("m", SessionMark.slot().inSession(), Boolean.TRUE))
                .add(MQExpression.eq("m", SessionMark.cachedMarkValue().code(), SessionMarkGradeValueCatalogItemCodes.UDOVLETVORITELNO));
        List<Long> list3 = this.dao.getList(builder);

        builder = getMarksBuilder()
                .add(MQExpression.eq("m", SessionMark.slot().inSession(), Boolean.TRUE))
                .add(MQExpression.eq("m", SessionMark.cachedMarkPositiveStatus(), Boolean.FALSE)); //хреновая оценка
        List<Long> list2 = this.dao.getList(builder);

        builder = getMarksBuilder()
                .add(MQExpression.eq("m", SessionMark.slot().inSession(), Boolean.FALSE))
                .add(MQExpression.eq("m", SessionMark.cachedMarkPositiveStatus(), Boolean.FALSE)); //хреновая оценка
        List<Long> listDebt = this.dao.getList(builder);


        //должники = есть плохие оценки(итог) + нет оценок вообще
        Set<Long> collectDebt = new HashSet<>(listEmpty);
        collectDebt.addAll(listDebt);

        //3 = <есть 3> - <должники> - <есть 2>
        Set<Long> collect3 = new HashSet<>(list3);
        collect3.removeAll(collectDebt);
        collect3.removeAll(list2);

        //4 = <есть 4> - <есть 5> - <есть 3> - <должники> - <есть 2>
        Set<Long> collect4 = new HashSet<>(list4);
        collect4.removeAll(list5);
        collect4.removeAll(list3);
        collect4.removeAll(collectDebt);
        collect4.removeAll(list2);

        //45 = (<есть 4> and <есть 5>) - <есть 3> - <должники> - <4> - <есть 2>
        Set<Long> collect45 = new HashSet<>(list5);
        collect45.retainAll(list4);
        collect45.removeAll(list3);
        collect45.removeAll(collectDebt);
        collect45.removeAll(list2);

        //5 = <есть 5> - <есть 4> - <есть 3> - <должники> - <4> - <есть 2>
        Set<Long> collect5 = new HashSet<>(list5);
        collect5.removeAll(list4);
        collect5.removeAll(list3);
        collect5.removeAll(collectDebt);
        collect5.removeAll(list2);

        for (DetailRow row : this.sheetRows) {
            //15 - Итого (бюджет)
            builder = getStudentsBuilder(row);
            List<Long> studentList = this.dao.getList(builder);
            row.values[12] = studentList.size();

            //нет смысла считать остальные столбцы если студентов нет
            if (row.values[12] == 0)
                continue;

            //7 кол-во на 5
            row.values[4] = CollectionUtils.intersection(collect5, studentList).size();
            //8 кол-во на 4 и 5
            row.values[5] = CollectionUtils.intersection(collect45, studentList).size();
            //9 кол-во на 4
            row.values[6] = CollectionUtils.intersection(collect4, studentList).size();
            //10 кол-во с 3
            row.values[7] = CollectionUtils.intersection(collect3, studentList).size();
            //11 кол-во должников
            row.values[8] = CollectionUtils.intersection(collectDebt, studentList).size();

            //стипендии
            List<Long> grant1 = this.dao.getList(getGrantBuilder(row, "grant2"));//Повышенная_1 стипендия
            List<Long> grant2 = this.dao.getList(getGrantBuilder(row, "grant3"));//Повышенная_2 стипендия

            //3 Повыш.стип. I ст
            row.values[0] = grant1.size();

            //4 Повыш.стип. II ст кол-во на 5
            row.values[1] = CollectionUtils.intersection(grant2, CollectionUtils.intersection(collect5, studentList)).size();

            //5 Повыш.стип. II ст кол-во на 4 и 5
            row.values[2] = CollectionUtils.intersection(grant2, CollectionUtils.intersection(collect45, studentList)).size();
            //6 Повыш.стип. II ст кол-во на 4
            row.values[3] = CollectionUtils.intersection(grant2, CollectionUtils.intersection(collect4, studentList)).size();

            //12 кол-во зач.первокурсников
            builder = getStudentsBuilder(row)
                    .add(MQExpression.eq("st", Student.course().intValue(), 1));
            row.values[9] = dao.getCount(builder);

            //13 кол-во иностр. студентов по международным соглашениям
            builder = getStudentsBuilder(row)
                    .add(MQExpression.notEq("st", Student.person().identityCard().citizenship().code(), 0));//не Россия
            row.values[10] = dao.getCount(builder);

            //14 кол-во в академическом отпуске
            builder = getStudentsBuilder(row)
                    .add(MQExpression.eq("st", Student.status().code(), UniDefines.CATALOG_STUDENT_STATUS_ACADEM));
            row.values[11] = this.dao.getCount(builder);
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    protected void loadData() {
        List<OrgUnit> branchList = new ArrayList<>();
        branchList.add(TopOrgUnit.getInstance());

        MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "o")
                .add(MQExpression.eq("o", OrgUnit.orgUnitType().code(), OrgUnitTypeCodes.BRANCH))
                .addOrder("o", OrgUnit.title());
        List<OrgUnit> list = dao.getList(builder);
        branchList.addAll(list);

        for (OrgUnit branch : branchList) {
            List<OrgUnit> children = getChildren(branch);

            GOSData vpo = createVPO(branch, children);
            this.sheetRows.addAll(vpo.rows);

            GOSData spo = createSPO(branch, children);
            this.sheetRows.addAll(spo.rows);
        }
    }

    protected GOSData createVPO(OrgUnit branch, List<OrgUnit> children) {
        GOSData gosData = new GOSData();
        gosData.branch = branch;
        gosData.children = children;
        gosData.vpo = true;

        //сироты
        MQBuilder subBuilder = new MQBuilder(PersonBenefit.ENTITY_CLASS, "pb")
                .add(MQExpression.eq("pb", PersonBenefit.benefit().code(), "5"));//сирота
        subBuilder.getSelectAliasList().clear();
        subBuilder.addSelect(PersonBenefit.person().id().fromAlias("pb").s());

        MQBuilder builder = new MQBuilder(Student.ENTITY_CLASS, "st")
                .add(MQExpression.in("st", Student.person().id(), subBuilder));
        builder.getSelectAliasList().clear();
        builder.addSelect(Student.id().fromAlias("st").s());

        //описание детализаций
        Map<String, Map<String, Object>> detalization = new LinkedHashMap<>();

        Map<String, Object> params = ImmutableMap.of(
                Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().high().s(), (Object) Boolean.TRUE,
                Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().bachelor().s(), Boolean.TRUE
        );
        detalization.put("бакалавриат", params);

        params = ImmutableMap.of(
                Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().high().s(), (Object) Boolean.TRUE,
                Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().bachelor().s(), Boolean.FALSE,
                Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().master().s(), Boolean.FALSE
        );
        detalization.put("специалитет", params);

        params = ImmutableMap.of(
                Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().high().s(), (Object) Boolean.TRUE,
                Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().master().s(), Boolean.TRUE
        );
        detalization.put("магистратура", params);

        params = ImmutableMap.of(
                Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().high().s(), (Object) Boolean.TRUE,
                Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().master().s(), Boolean.FALSE
        );
        detalization.put("Всего (бакалавры, специалисты)", params);

        params = ImmutableMap.of(
                Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().high().s(), (Object) Boolean.TRUE,
                Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().master().s(), Boolean.TRUE
        );
        detalization.put("Всего (магистры)", params);

        params = ImmutableMap.of(
                Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().high().s(), (Object) Boolean.TRUE
        );
        detalization.put("Всего (магистры, бакалавры, специалисты)", params);

        params = ImmutableMap.of(
                Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().high().s(), (Object) Boolean.TRUE,
                Student.id().s(), builder
        );
        detalization.put("В том числе сирот", params);

        for (Map.Entry<String, Map<String, Object>> entry : detalization.entrySet()) {
            String rowTitle = entry.getKey();
            params = entry.getValue();


            DetailRow row = new DetailRow();
            row.parent = gosData;
            row.title = rowTitle;
            row.sqlParam = params;
            row.bold = true;

            if (rowTitle.contains("Всего") || rowTitle.contains("В том числе")) {
                row.bold = rowTitle.contains("Всего");
                gosData.rows.add(row);
            }
            else {
                gosData.rows.add(row);

                for (OrgUnit orgUnit : gosData.children) {
                    row = new DetailRow();
                    row.parent = gosData;
                    row.fou = orgUnit;
                    row.title = orgUnit.getTitle();
                    row.bold = false;

                    row.sqlParam = params;

                    gosData.rows.add(row);
                }
            }
        }

        return gosData;
    }

    protected GOSData createSPO(OrgUnit branch, List<OrgUnit> children) {
        GOSData gosData = new GOSData();
        gosData.branch = branch;
        gosData.children = children;
        gosData.vpo = false;


        Map<String, Object> params = ImmutableMap.of(
                Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().middle().s(), (Object) Boolean.TRUE
        );
        for (OrgUnit orgUnit : gosData.children) {
            DetailRow row = new DetailRow();
            row.parent = gosData;
            row.fou = orgUnit;
            row.bold = false;
            row.title = orgUnit.getTitle();

            row.sqlParam = params;

            gosData.rows.add(row);
        }

        //сироты
        MQBuilder subBuilder = new MQBuilder(PersonBenefit.ENTITY_CLASS, "pb")
                .add(MQExpression.eq("pb", PersonBenefit.benefit().code(), "5"));//сирота
        subBuilder.getSelectAliasList().clear();
        subBuilder.addSelect(PersonBenefit.person().id().fromAlias("pb").s());

        MQBuilder builder = new MQBuilder(Student.ENTITY_CLASS, "st")
                .add(MQExpression.in("st", Student.person().id(), subBuilder));
        builder.getSelectAliasList().clear();
        builder.addSelect(Student.id().fromAlias("st").s());

        //описание детализаций
        Map<String, Map<String, Object>> detalization = new LinkedHashMap<>();

        params = ImmutableMap.of(
                Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().middle().s(), (Object) Boolean.TRUE
        );
        detalization.put("Всего", params);

        params = ImmutableMap.of(
                Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().middle().s(), (Object) Boolean.TRUE,
                Student.id().s(), builder
        );
        detalization.put("В том числе сирот", params);

        for (Map.Entry<String, Map<String, Object>> entry : detalization.entrySet()) {
            String rowTitle = entry.getKey();
            params = entry.getValue();

            DetailRow row = new DetailRow();
            row.parent = gosData;
            row.bold = rowTitle.contains("Всего");
            row.title = rowTitle;
            row.sqlParam = params;

            gosData.rows.add(row);
        }

        return gosData;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    //список студентов у которых нет оценок(статусов)
    protected List<Long> getEmptyStudents() {
        MQBuilder subBuilder = getMarksBuilder();
        subBuilder.getSelectAliasList().clear();
        subBuilder.addSelect(SessionMark.slot().studentWpeCAction().id().fromAlias("m").s());

        MQBuilder builder = new MQBuilder(EppStudentWpeCAction.ENTITY_CLASS, "slot")
                .add(MQExpression.isNull("slot", EppStudentWpeCAction.removalDate()))
                .add(MQExpression.eq("slot", EppStudentWpeCAction.studentWpe().year().educationYear(), this.year))
                .add(MQExpression.eq("slot", EppStudentWpeCAction.studentWpe().part(), this.part))
                .add(MQExpression.in("slot", EppStudentWpeCAction.studentWpe().student().id(), getStudentsBuilder(null)))
                .add(MQExpression.in("slot", EppStudentWpeCAction.studentWpe().student().id(), getStudentsBuilder(null)))
                .add(MQExpression.notIn("slot", EppStudentWpeCAction.id(), subBuilder));

        builder.getSelectAliasList().clear();
        builder.addSelect(EppStudentWpeCAction.studentWpe().student().id().fromAlias("slot").s());
        builder.setNeedDistinct(true);

        return this.dao.getList(builder);
    }

    protected MQBuilder getMarksBuilder() {
        MQBuilder builder = new MQBuilder(SessionMark.ENTITY_CLASS, "m")
                .add(MQExpression.eq("m", SessionMark.slot().studentWpeCAction().studentWpe().year().educationYear(), this.year))
                .add(MQExpression.eq("m", SessionMark.slot().studentWpeCAction().studentWpe().part(), this.part))
                .add(MQExpression.isNull("m", SessionMark.slot().studentWpeCAction().removalDate()))
                .addJoin("m", SessionMark.slot().studentWpeCAction().studentWpe(), "d")
                .add(MQExpression.isNull("d", EppStudentWorkPlanElement.studentEduPlanVersion().removalDate()))
                .add(MQExpression.in("m", SessionMark.slot().studentWpeCAction().studentWpe().student().id(), getStudentsBuilder(null)));

        builder.getSelectAliasList().clear();
        builder.addSelect(EppStudentWorkPlanElement.studentEduPlanVersion().student().id().fromAlias("d").s());
        builder.setNeedDistinct(true);

        return builder;
    }

    // select student.id from student where ....
    protected MQBuilder getStudentsBuilder(DetailRow row) {
        if (row == null)
            row = new DetailRow();

        MQBuilder builder = new MQBuilder(Student.ENTITY_CLASS, "st")
                .add(MQExpression.eq("st", Student.educationOrgUnit().developForm(), this.developForm))
                .add(MQExpression.eq("st", Student.archival(), Boolean.FALSE)) //не архивный
                .add(MQExpression.notEq("st", Student.status().code(), UniDefines.CATALOG_STUDENT_STATUS_EXCLUDED)) //не отчислен
                .add(MQExpression.eq("st", Student.compensationType().code(), UniDefines.COMPENSATION_TYPE_BUDGET)); //бюджет

        if (row.fou != null)
            builder.add(MQExpression.eq("st", Student.educationOrgUnit().formativeOrgUnit(), row.fou));
        else if (row.parent != null)
            builder.add(MQExpression.in("st", Student.educationOrgUnit().formativeOrgUnit(), row.parent.children));

        for (Map.Entry<String, Object> entry : row.sqlParam.entrySet()) {
            String field = entry.getKey();
            Object value = entry.getValue();

            if (value instanceof MQBuilder)
                builder.add(MQExpression.in("st", field, (MQBuilder) value));
            else if (value instanceof Collection)
                builder.add(MQExpression.in("st", field, (Collection) value));
            else
                builder.add(MQExpression.eq("st", field, value));
        }

        builder.getSelectAliasList().clear();
        builder.addSelect(Student.id().fromAlias("st").s());
        builder.setNeedDistinct(true);
        return builder;
    }

    protected MQBuilder getGrantBuilder(DetailRow row, String code) {
        MQBuilder subBuilder = new MQBuilder(RelTypeGrantView.ENTITY_CLASS, "r")
                .add(MQExpression.eq("r", RelTypeGrantView.grant().code(), code));
        subBuilder.getSelectAliasList().clear();
        subBuilder.addSelect(RelTypeGrantView.view().fromAlias("r").s());

        MQBuilder builder = new MQBuilder(StudentGrantEntity.ENTITY_CLASS, "sg")
                .add(MQExpression.eq("sg", StudentGrantEntity.status().code(), StuGrantStatusCodes.CODE_1))
                .add(MQExpression.eq("sg", StudentGrantEntity.month(), this.grantMonth.getStringValue(this.grantYear)))
                .add(MQExpression.in("sg", StudentGrantEntity.student().id(), getStudentsBuilder(row)))
                .add(MQExpression.in("sg", StudentGrantEntity.view(), subBuilder));

        builder.getSelectAliasList().clear();
        builder.addSelect(StudentGrantEntity.student().id().fromAlias("sg").s());
        builder.setNeedDistinct(true);

        return builder;
    }

    private List<OrgUnit> getChildren(OrgUnit ou) {
        List<OrgUnit> resultList = new ArrayList<>();
        resultList.add(ou);

        List<OrgUnit> list = resultList;
        do {
            MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "o")
                    .add(MQExpression.in("o", OrgUnit.parent(), list))
                    .add(MQExpression.notEq("o", OrgUnit.orgUnitType().code(), OrgUnitTypeCodes.BRANCH))
                    .add(MQExpression.eq("o", OrgUnit.archival(), Boolean.FALSE));

            list = this.dao.getList(builder);
            resultList.addAll(list);
        } while (!list.isEmpty());

        //отбираем ток те, которые есть в EducationOrgUnit
        MQBuilder builder = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "eou")
                .add(MQExpression.in("eou", EducationOrgUnit.formativeOrgUnit(), resultList));
        builder.getSelectAliasList().clear();
        builder.addSelect(EducationOrgUnit.formativeOrgUnit().id().fromAlias("eou").s());
        builder.setNeedDistinct(true);
        List<Long> ids = dao.getList(builder);

        resultList = dao.getList(OrgUnit.class, ids, OrgUnit.title().s());
        return resultList;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////

    protected void loadTemplate() throws Exception {
        TemplateDocument template = dao.getCatalogItem(TemplateDocument.class, TEMPLATE);
        ByteArrayInputStream in = new ByteArrayInputStream(template.getContent());
        Workbook tempalteWorkbook = Workbook.getWorkbook(in);

        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);

        this.workbookStream = new ByteArrayOutputStream();
        this.workbook = Workbook.createWorkbook(this.workbookStream, tempalteWorkbook, ws);
        this.sheet = this.workbook.getSheet(0);
    }

    ////////////////////////////////////////////////////////////////////////////////////////

    protected static WritableCellFormat createFormat(boolean bold, boolean vertical) throws Exception {
        WritableFont font = new WritableFont(WritableFont.TIMES, 12);
        if (bold)
            font.setBoldStyle(WritableFont.BOLD);

        WritableCellFormat format = new WritableCellFormat(font);
        format.setVerticalAlignment(VerticalAlignment.BOTTOM);
        format.setWrap(true);

        if (vertical)
            format.setOrientation(Orientation.PLUS_90);

        format.setBorder(Border.ALL, BorderLineStyle.THIN);

        return format;
    }

    protected static WritableCell createCell(int col, int row, Object value, WritableCellFormat format) throws Exception {
        if (format == null)
            format = createFormat(false, false);

        if (value instanceof Integer)
            return new jxl.write.Number(col, row, ((Integer) value) + 0.0, format);
        return new Label(col, row, value.toString(), format);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    protected enum MarkResult {
        MARK5, MARK45, MARK4, MARK3, MARK2
    }

    protected static class GOSData {
        protected OrgUnit branch;
        protected boolean vpo = true;
        protected List<OrgUnit> children;

        protected List<DetailRow> rows = new ArrayList<>();

        protected String getTitle() {
            StringBuilder sb = new StringBuilder()
                    .append(branch.isTop() ? "Головной вуз" : branch.getTerritorialTitle())
                    .append(" (")
                    .append(vpo ? "ВПО" : "СПО")
                    .append(")");
            return sb.toString();
        }
    }

    protected static class DetailRow {
        protected GOSData parent;
        protected OrgUnit fou;

        protected String title;
        protected boolean bold;
        protected boolean totalRow = false;

        protected Map<String, Object> sqlParam = new HashMap<>();

        protected int[] values = new int[13];

        protected boolean hasData() {
            return (fou == null) || (values[12] > 0);
        }
    }
}
 