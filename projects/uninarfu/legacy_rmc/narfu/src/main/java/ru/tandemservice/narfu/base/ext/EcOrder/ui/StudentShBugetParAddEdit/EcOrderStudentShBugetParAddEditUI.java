package ru.tandemservice.narfu.base.ext.EcOrder.ui.StudentShBugetParAddEdit;

import ru.tandemservice.narfu.entity.EnrollmentOrderNARFU;
import ru.tandemservice.uni.dao.UniDaoFacade;

public class EcOrderStudentShBugetParAddEditUI extends ru.tandemservice.uniec.base.bo.EcOrder.ui.StudentShBugetParAddEdit.EcOrderStudentShBugetParAddEditUI {

    @Override
    public void onComponentRefresh() {
        super.onComponentRefresh();
        EnrollmentOrderNARFU orderNarfu = UniDaoFacade.getCoreDao().get(EnrollmentOrderNARFU.class, EnrollmentOrderNARFU.enrollmentOrder().id(), getOrder().getId());
        if (orderNarfu != null)
            setCourse(orderNarfu.getCourse());
    }
}
