package ru.tandemservice.narfu.base.bo.ReportPersonNarfu.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportDQLModifierOwner;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportPrintBlock;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportPrintInfo;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.ReportDQL;
import ru.tandemservice.narfu.base.bo.ReportPersonNarfu.ui.Add.block.personnarfuData.PersonnarfuDataBlock;
import ru.tandemservice.narfu.base.bo.ReportPersonNarfu.ui.Add.block.personnarfuData.PersonnarfuDataParam;
import ru.tandemservice.narfu.base.bo.ReportPersonNarfu.ui.Add.print.personnarfu.PersonnarfuPrintBlock;

public class ReportPersonNarfuAddUI
        extends UIPresenter implements IReportDQLModifierOwner
{

    public static final String PERSONNARFU_SCHEET = "personnarfuScheet";

    // tab flag
    private boolean personnarfuScheet;

    private String _selectedTab;

    private PersonnarfuDataParam personnarfuData = new PersonnarfuDataParam();

    // print blocks
    private IReportPrintBlock personnarfuPrintBlock = new PersonnarfuPrintBlock();

    @Override
    public void onComponentRefresh()
    {

        // ставим значения по умолчанию
        /*
        _entrantData.getEnrollmentCampaign().setActive(true);
        _entrantData.getEnrollmentCampaign().setData(UniecDAOFacade.getEntrantDAO().getLastEnrollmentCampaign());

        // является абитуриентом
        _entrantData.getEntrant().setActive(true);
        _entrantData.getEntrant().setData(new IdentifiableWrapper(EntrantDataBlock.ENTRANT_YES, "да"));

        // выбираем по умолчанию неархивных абитуриентов
        _entrantData.getEntrantArchival().setActive(true);
        _entrantData.getEntrantArchival().setData(new IdentifiableWrapper(EntrantDataBlock.ENTRANT_ARCHIVAL_NOT, "нет"));
        */

    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (personnarfuScheet) {
            PersonnarfuDataBlock.onBeforeDataSourceFetch(dataSource, personnarfuData);
        }
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {

    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (personnarfuScheet) {
            printInfo.addSheet(PERSONNARFU_SCHEET);
            // фильтры модифицируют запросы
            personnarfuData.modify(dql, printInfo);
            // печатные блоки модифицируют запросы и создают печатные колонки
            personnarfuPrintBlock.modify(dql, printInfo);
        }
    }

    // Getters
    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }

    public void setPersonnarfuScheet(boolean personnarfuScheet) {
        this.personnarfuScheet = personnarfuScheet;
    }

    public boolean isPersonnarfuScheet() {
        return personnarfuScheet;
    }

    public void setPersonnarfuData(PersonnarfuDataParam personnarfuData) {
        this.personnarfuData = personnarfuData;
    }

    public PersonnarfuDataParam getPersonnarfuData() {
        return personnarfuData;
    }

    public void setPersonnarfuPrintBlock(IReportPrintBlock personnarfuPrintBlock) {
        this.personnarfuPrintBlock = personnarfuPrintBlock;
    }

    public IReportPrintBlock getPersonnarfuPrintBlock() {
        return personnarfuPrintBlock;
    }

}
