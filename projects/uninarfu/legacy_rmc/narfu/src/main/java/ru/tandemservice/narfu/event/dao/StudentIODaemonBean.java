package ru.tandemservice.narfu.event.dao;

import org.apache.commons.codec.digest.DigestUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.narfu.entity.IdentityCardHidden;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniec.entity.entrant.Entrant;

import java.util.Arrays;
import java.util.List;

public class StudentIODaemonBean extends CommonDAO implements IStudentIODaemonBean
{

    @Override
    public void updateArchivalIdentityCard(DSetEvent event)
    {

        IEntity entity = event.getMultitude().getSingularEntity();

        if (!(entity instanceof Student))
            return;

        IEventServiceLock eventLock;

        Session session = getSession();
        eventLock = CoreServices.eventService().lock();

        Student st = (Student) entity;

        List<IdentityCard> icList = getList(IdentityCard.class, IdentityCard.person(), st.getPerson());
        for (IdentityCard card : icList)
        {
            if (st.isArchival() && !hasDependence(card.getId(), st.getId()))
            {
                String number = card.getNumber();
                String seria = card.getSeria();

                IdentityCardHidden cardHidden = get(IdentityCardHidden.class, IdentityCardHidden.identityCard(), card);
                if (cardHidden == null)
                {
                    cardHidden = new IdentityCardHidden();
                    cardHidden.setIdentityCard(card);
                }

                cardHidden.setNumber(number);
                cardHidden.setSeria(seria);
                cardHidden.setBirthDate(card.getBirthDate());
                session.saveOrUpdate(cardHidden);

                if (number != null)
                    card.setNumber(DigestUtils.md5Hex(number));
                if (seria != null)
                {
                    card.setSeria(DigestUtils.md5Hex(seria));
                }
                card.setBirthDate(CoreDateUtils.getYearFirstTimeMoment(1900));

                session.saveOrUpdate(card);

            } else if (!st.isArchival())
            {
                //Студент из архива - восстановим данные из IdentityCardHidden
                IdentityCardHidden cardHidden = get(IdentityCardHidden.class, IdentityCardHidden.identityCard(), card);
                if (cardHidden == null)
                {
                    continue;
                }

                card.setNumber(cardHidden.getNumber());
                card.setSeria(cardHidden.getSeria());
                card.setBirthDate(cardHidden.getBirthDate());

                session.saveOrUpdate(card);
                session.delete(cardHidden);
            }

        }
        //Студент в архив - данные в IdentityCardHidden
        eventLock.release();
    }

    protected boolean hasDependence(Long icId, Long studentId)
    {
        List list = Arrays.asList(ContactorPerson.class, Employee.class, Entrant.class, Student.class);

        for (Object obj : list)
        {
            Class clazz = (Class) obj;

            DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(clazz, "e")
                    .joinEntity(
                            "e",
                            DQLJoinType.inner,
                            IdentityCard.class,
                            "ic",
                            DQLExpressions.eq(
                                    DQLExpressions.property("e.person"),
                                    DQLExpressions.property(IdentityCard.person().fromAlias("ic")))
                    )
                    .where(DQLExpressions.eq(
                            DQLExpressions.property(IdentityCard.id().fromAlias("ic")),
                            DQLExpressions.value(icId)
                    ));

            //У ContactorPerson archival == !active
            if (ContactorPerson.class.equals(clazz))
                dql.where(DQLExpressions.eq(
                        DQLExpressions.property("e.active"),
                        DQLExpressions.value(Boolean.TRUE)
                ));
            else
                dql.where(DQLExpressions.eq(
                        DQLExpressions.property("e.archival"),
                        DQLExpressions.value(Boolean.FALSE)
                ));

            if (Student.class.equals(clazz))
                dql.where(DQLExpressions.ne(
                        DQLExpressions.property(Student.id().fromAlias("e")),
                        DQLExpressions.value(studentId)
                ));

            int count = getCount(dql);
            if (count > 0)
                return true;

        }
        return false;
    }
}
