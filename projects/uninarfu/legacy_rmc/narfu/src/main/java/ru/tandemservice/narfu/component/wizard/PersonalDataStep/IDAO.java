package ru.tandemservice.narfu.component.wizard.PersonalDataStep;


public interface IDAO extends ru.tandemservice.uniec.component.wizard.PersonalDataStep.IDAO {
    public abstract void prepareSelectAchievements(Model paramModel);

    public abstract void deleteAchievement(Model paramModel, Long id);
}
