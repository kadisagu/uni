package ru.tandemservice.narfu.component.studentmassprint.GrantsMassPrint;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.movestudentrmc.entity.catalog.Grant;
import ru.tandemservice.narfu.entity.NarfuGrantDocument;
import ru.tandemservice.narfu.entity.catalog.GrantTemplateDocument;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;

import java.util.*;

public class DAO
        extends ru.tandemservice.unirmc.component.studentmassprint.Base.DAO<Model>
        implements ru.tandemservice.unirmc.component.studentmassprint.Base.IDAO<Model>
{

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        if (model.getGrantTemplateDocumentModel() == null) {
            // только то, что поддерживает множественную печать
            List<GrantTemplateDocument> _templateDocs = _makeTemplateDocsList();
            model.setGrantTemplateDocumentModel(new LazySimpleSelectModel<>(_templateDocs));
        }

        model.setGrantsModel(new LazySimpleSelectModel<>(Grant.class));
        model.setEducationYearModel(new EducationYearModel(CoreDateUtils.getYear(new Date())));

    }


    /**
     * Только те шаблоны, по которым есть соотв. bean с поддержкой множественной печати
     */
    private List<GrantTemplateDocument> _makeTemplateDocsList()
    {
        // для code справочника должен быть bean
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(GrantTemplateDocument.class, "t")
                .where(DQLExpressions.eqValue(DQLExpressions.property(GrantTemplateDocument.useMassPrint().fromAlias("t")), Boolean.TRUE))
                .order("t.title");
        return getList(builder);
    }


    @Override
    protected void addAdditionalRestrictions(Model model, DQLSelectBuilder builder, String alias)
    {
    }

    @Override
    protected void createDataSourcePage(Model model, DynamicListDataSource<Student> dataSource, DQLSelectBuilder builderWithNoColumns) {
        super.createDataSourcePage(model, dataSource, builderWithNoColumns);

        List<ViewWrapper<Student>> list = ViewWrapper.getPatchedList(dataSource);

        Map<Long, List<NarfuGrantDocument>> map = _makeMap(list);

        model.setStudentDocMap(map);
        wrapPatchedListExt(list, model);

    }

    public void wrapPatchedListExt(List<ViewWrapper<Student>> lst, Model model) {
        Map<Long, List<NarfuGrantDocument>> map = model.getStudentDocMap();

        for (ViewWrapper<Student> viewWrapper : lst) {
            Student student = viewWrapper.getEntity();
            Long id = student.getId();
            viewWrapper.setViewProperty("hasDocuments", (!(map.get(id) == null || map.get(id).isEmpty())));
        }
    }

    //Создадим мапу Студент - Список документов  для заявлений на получение стипендий
    private Map<Long, List<NarfuGrantDocument>> _makeMap(List<ViewWrapper<Student>> lst)
    {
        // id Student
        List<Long> lstPerson = new ArrayList<>();
        for (ViewWrapper<Student> vrap : lst) {
            lstPerson.add(vrap.getEntity().getId());
        }

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(NarfuGrantDocument.class, "doc")
                .column("doc")
                        //.column(DQLExpressions.property(NarfuGrantDocument.student().id().fromAlias("doc")))
                .where(DQLExpressions.in(DQLExpressions.property(NarfuGrantDocument.document().student().id().fromAlias("doc")), lstPerson));

        List<NarfuGrantDocument> docs = dql.createStatement(getComponentSession()).list();

        Map<Long, List<NarfuGrantDocument>> retVal = new HashMap<>();

        for (NarfuGrantDocument doc : docs) {
            Long studId = doc.getDocument().getStudent().getId();

            //Сгруппируем
            SafeMap.safeGet(retVal, studId, ArrayList.class).add(doc);
        }

        return retVal;
    }

    @Override
    /**
     * без архивных студентов
     */
    protected boolean whithArchivalStudents()
    {
        return false;
    }


    @Override
    protected void wrapPatchedList(List<ViewWrapper<Student>> lst) {

    }

}
