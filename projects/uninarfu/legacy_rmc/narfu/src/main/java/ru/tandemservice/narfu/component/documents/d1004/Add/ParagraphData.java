package ru.tandemservice.narfu.component.documents.d1004.Add;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.entity.catalog.EppWeek;
import ru.tandemservice.uniepp.entity.catalog.codes.EppWeekTypeCodes;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRow2EduPlan;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRowWeek;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationWeek;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ParagraphData {
    public static final String[] SUBTITLE_SPO = {"дающая право на предоставление по месту работы дополнительного отпуска,", "связанного с обучением по программам СПО в высшем учебном заведении,", "которое имеет государственную аккредитацию"};
    public static final String[] SUBTITLE_VPO = {"дающая право на предоставление по месту работы дополнительного отпуска,", "связанного с обучением в высшем учебном заведении,", "которое имеет государственную аккредитацию"};

    public static final String INTRODUCTION_VPO = "В соответствии со статьей 173 Трудового кодекса Российской Федерации";
    public static final String INTRODUCTION_SPO = "В соответствии со статьей 174 Трудового кодекса Российской Федерации";
    /*   public static final String[] ORDEREXTRACT_SPO = {"Статья 174. Гарантии и компенсации работникам, обучающимся в образовательных учреждениях среднего профессионального образования, и работникам, поступающим в указанные образовательные учреждения.",
               "Работникам, направленным на обучение работодателем или поступившим самостоятельно в имеющие государственную аккредитацию образовательные учреждения среднего профессионального образования независимо от их организационно-правовых форм по заочной и очно-заочной (вечерней) формам обучения, успешно обучающимся в указанных учреждениях, работодатель предоставляет дополнительные отпуска с сохранением среднего заработка для:",
               "прохождения промежуточной аттестации на первом и втором курсах - по 30 календарных дней, на каждом из последующих курсов - по 40 календарных дней;",
               "подготовки и защиты выпускной квалификационной работы и сдачи итоговых государственных экзаменов - два месяца;",
               "сдачи итоговых государственных экзаменов - один месяц."};*/
    public static final String[] ORDEREXTRACT_SPO = {"Статья 174 (в ред. Федерального закона от 02.07.2013 № 185-ФЗ). Гарантии и компенсации работникам, совмещающим работу с получением среднего профессионального образования, и работникам, поступающим на обучение по образовательным программам среднего профессионального образования.",
            "Работникам, успешно осваивающим имеющие государственную аккредитацию образовательные программы среднего профессионального образования по заочной и очно-заочной формам обучения, работодатель предоставляет дополнительные отпуска с сохранением среднего заработка для:",
            "прохождения промежуточной аттестации на первом и втором курсах - по 30 календарных дней, на каждом из последующих курсов - по 40 календарных дней;",
            "прохождения государственной итоговой аттестации - до двух месяцев в соответствии с учебным планом осваиваемой работником образовательной программы среднего профессионального образования."};
    /*    public static final String[] ORDEREXTRACT_VPO = {"Статья 173. Гарантии и компенсации работникам, совмещающим работу с обучением в образовательных учреждениях высшего профессионального образования, и работникам, поступающим в указанные образовательные учреждения.",
                "Работникам, направленным на обучение работодателем или поступившим самостоятельно в имеющие государственную аккредитацию образовательные учреждения высшего профессионального образования независимо от их организационно-правовых форм по заочной и очно-заочной (вечерней) формам обучения, успешно обучающимся в этих учреждениях, работодатель предоставляет дополнительные отпуска с сохранением среднего заработка для:",
                "прохождения промежуточной аттестации на первом и втором курсах, соответственно – по 40 календарных дней, на каждом из последующих курсов соответственно – по 50 календарных дней (при освоении основных образовательных программ высшего профессионального образования в сокращенные сроки на втором курсе – 50 календарных дней);",
                "подготовки и защиты выпускной квалификационной работы и сдачи итоговых государственных экзаменов –четыре месяца;",
                "сдачи итоговых государственных экзаменов – один месяц."};*/
    public static final String[] ORDEREXTRACT_VPO = {"Статья 173 (в ред. Федерального закона от 02.07.2013 № 185-ФЗ). Гарантии и компенсации работникам, совмещающим работу с получением высшего образования по программам бакалавриата, программам специалитета или программам магистратуры, и работникам, поступающим на обучение по указанным образовательным программам.",
            "Работникам, направленным на обучение работодателем или поступившим самостоятельно на обучение по имеющим государственную аккредитацию программам бакалавриата, программам специалитета или программам магистратуры по заочной и очно-заочной формам обучения и успешно осваивающим эти программы, работодатель предоставляет дополнительные отпуска с сохранением среднего заработка для:",
            "прохождения промежуточной аттестации на первом и втором курсах соответственно – по 40 календарных дней, на каждом из последующих курсов соответственно – по 50 календарных дней (при освоении образовательных программ высшего образования в сокращенные сроки на втором курсе – 50 календарных дней);",
            "прохождения государственной итоговой аттестации – до четырех месяцев в соответствии с учебным планом осваиваемой работником образовательной программы высшего образования."};

    public static final String EDU_PROG_VPO = "высшего";
    public static final String EDU_PROG_SPO = "среднего профессионального";

    public static final String EXAMINATION_SESSION = "для прохождения промежуточной аттестации";
    public static final String RESULT_QUALIFICATION_WORK = "для подготовки и защиты выпускной квалификационной работы и сдачи итоговых государственных экзаменов";
    public static final String STATE_EXAMINATION = "для сдачи итоговых государственных экзаменов";
    public static final String STATE_FINAL_EXAMINATION = "для прохождения государственной итоговой аттестации";
    public static final String FINAL_EXAMINATION = "для прохождения итоговой аттестации";

    public static EppWeek getWeek(Student student, String period, DevelopGridTerm term, boolean isStartDate) {

        EppStudent2EduPlanVersion rel = IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionRelation(student.getId());

        EppEduPlanVersion version = null;
        if (rel != null)
            version = rel.getEduPlanVersion();
        else
            return null;

        if (term == null)
            return null;

        String weekCode = "";

        switch (period) {
            case ParagraphData.EXAMINATION_SESSION:
                weekCode = EppWeekTypeCodes.EXAMINATION_SESSION;
                break;
            case ParagraphData.STATE_EXAMINATION:
                weekCode = EppWeekTypeCodes.STATE_EXAMINATION;
                break;
            default:
                return null;
        }

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppEduPlanVersionWeekType.class, "rel")
                .where(DQLExpressions.eqValue(DQLExpressions.property(EppEduPlanVersionWeekType.eduPlanVersion().id().fromAlias("rel")), version.getId()))
                .where(DQLExpressions.eqValue(DQLExpressions.property(EppEduPlanVersionWeekType.course().id().fromAlias("rel")), student.getCourse().getId()))
                .where(DQLExpressions.eqValue(DQLExpressions.property(EppEduPlanVersionWeekType.term().id().fromAlias("rel")), term.getTerm().getId()))
                .where(DQLExpressions.eqValue(DQLExpressions.property(EppEduPlanVersionWeekType.weekType().code().fromAlias("rel")), weekCode))
                .order(DQLExpressions.property(EppEduPlanVersionWeekType.week().number().fromAlias("rel")))
                .column("rel");


        List<EppEduPlanVersionWeekType> lst = IUniBaseDao.instance.get().getList(builder);

        if (!lst.isEmpty()) {
            int index = isStartDate ? 0 : lst.size() - 1;
            EppEduPlanVersionWeekType relWeek = lst.get(index);
            return relWeek.getWeek();
        }

        return null;
    }


    public static EppWorkGraphRowWeek getWeekGraph(Student student, String period, DevelopGridTerm term, boolean isStartDate) {

        EppStudent2EduPlanVersion rel = IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionRelation(student.getId());

        EppEduPlanVersion version = null;
        if (rel != null)
            version = rel.getEduPlanVersion();
        else
            return null;

        if (term == null)
            return null;

        String weekCode = "";

        switch (period) {
            case ParagraphData.EXAMINATION_SESSION:
                weekCode = EppWeekTypeCodes.EXAMINATION_SESSION;
                break;
            case ParagraphData.STATE_EXAMINATION:
                weekCode = EppWeekTypeCodes.STATE_EXAMINATION;
                break;
            default:
                return null;
        }

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppWorkGraphRowWeek.class, "week")
                .where(DQLExpressions.eqValue(DQLExpressions.property(EppWorkGraphRowWeek.row().graph().developForm().id().fromAlias("week")), student.getEducationOrgUnit().getDevelopForm().getId()))
                .where(DQLExpressions.eqValue(DQLExpressions.property(EppWorkGraphRowWeek.row().graph().developCondition().id().fromAlias("week")), student.getEducationOrgUnit().getDevelopCondition().getId()))
                .where(DQLExpressions.eqValue(DQLExpressions.property(EppWorkGraphRowWeek.row().graph().developTech().id().fromAlias("week")), student.getEducationOrgUnit().getDevelopTech().getId()))
                .where(DQLExpressions.eqValue(DQLExpressions.property(EppWorkGraphRowWeek.row().graph().developGrid().developPeriod().id().fromAlias("week")), student.getEducationOrgUnit().getDevelopPeriod().getId()))
                .where(DQLExpressions.eqValue(DQLExpressions.property(EppWorkGraphRowWeek.row().graph().year().educationYear().current().fromAlias("week")), Boolean.TRUE))
                .joinEntity("week", DQLJoinType.inner, EppWorkGraphRow2EduPlan.class, "epp",
                            DQLExpressions.eq(DQLExpressions.property(EppWorkGraphRowWeek.row().id().fromAlias("week")), DQLExpressions.property(EppWorkGraphRow2EduPlan.row().id().fromAlias("epp"))))
                .where(DQLExpressions.eqValue(DQLExpressions.property(EppWorkGraphRow2EduPlan.eduPlanVersion().id().fromAlias("epp")), version.getId()))
                .where(DQLExpressions.eqValue(DQLExpressions.property(EppWorkGraphRowWeek.row().course().id().fromAlias("week")), student.getCourse().getId()))
                .where(DQLExpressions.eqValue(DQLExpressions.property(EppWorkGraphRowWeek.type().code().fromAlias("week")), weekCode))
                .where(DQLExpressions.eqValue(DQLExpressions.property(EppWorkGraphRowWeek.term().id().fromAlias("week")), term.getTerm().getId()))
                .order(DQLExpressions.property(EppWorkGraphRowWeek.week().fromAlias("week")))
                .column("week");

        List<EppWorkGraphRowWeek> lst = IUniBaseDao.instance.get().getList(builder);

        if (!lst.isEmpty()) {
            int index = isStartDate ? 0 : lst.size() - 1;
            EppWorkGraphRowWeek relWeek = lst.get(index);
            return relWeek;
        }

        return null;
    }

    public static Date getStartWeekDate(Integer number) {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppYearEducationWeek.class, "week")
                .where(DQLExpressions.eqValue(DQLExpressions.property(EppYearEducationWeek.year().educationYear().current().fromAlias("week")), Boolean.TRUE))
                .where(DQLExpressions.eqValue(DQLExpressions.property(EppYearEducationWeek.number().fromAlias("week")), number))
                .column(DQLExpressions.property(EppYearEducationWeek.date().fromAlias("week")));

        List<Date> lst = IUniBaseDao.instance.get().getList(builder);

        if (!lst.isEmpty())
            return lst.get(0);
        else
            return null;
    }

    public static Date getDate(Student student, String period, DevelopGridTerm term, boolean isStartDate) {

        EppWorkGraphRowWeek week = getWeekGraph(student, period, term, isStartDate);

        if (week != null) {
            Integer weekNumber = week.getWeek();

            Date startDate = getStartWeekDate(weekNumber);

            if (startDate != null)
                if (isStartDate)
                    return startDate;
                else {
                    Calendar c = Calendar.getInstance();
                    c.setTime(startDate);
                    c.add(Calendar.DAY_OF_MONTH, 6);
                    return c.getTime();
                }
        }

    	/*
    	EppWeek week = getWeek(student, period, term, isStartDate);
    	
    	if (week != null){
    		EducationYear educationYear = IUniBaseDao.instance.get().get(EducationYear.class, EducationYear.current(), Boolean.TRUE);
    		
    		String dateStr = week.getTitle();
    		
    		String[] s = dateStr.split(" ");
    		String[] s1 = dateStr.split(" - ");
    		
    		int date, month, year;
    		
    		if (isStartDate){
    			date = Integer.valueOf(s[0]).intValue();
    			month = week.getMonth()-1;
    			year = week.getNumber() <= 18 ? educationYear.getIntValue() : educationYear.getIntValue() + 1;
    		} else {
    			date = Integer.valueOf(StringUtils.split(s1[1], " ")[0]).intValue();
    			month = s.length == 5 ? week.getMonth() : week.getMonth()-1;
    			year = week.getNumber() <= 17 ? educationYear.getIntValue() : educationYear.getIntValue() + 1;
    		}
    		
    		Calendar c = Calendar.getInstance();
    		c.set(year, month, date, 0, 0, 0);
    		
    		return c.getTime();
    	}
    	*/


        return null;
    }
}
