package ru.tandemservice.narfu.unisession.component.settings.EstimatesExamImportList;

import com.ibm.icu.util.Calendar;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.narfu.entity.ExamBulletinImportedFile;
import ru.tandemservice.narfu.entity.catalog.ExamBulletinImportedFileResult;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDao;

import java.util.Date;

public class DAO extends UniDao<Model> implements IDAO {
    @Override
    public void prepare(Model model) {

        model.setImportResultList(getCatalogItemListOrderByCode(ExamBulletinImportedFileResult.class));
        // EntrantFilterUtil.prepareEnrollmentCampaignFilter(model,
        // getSession());

        //String filter = "ИМПОРТИРОВАНО";

        //model.setImportResultList(new LazySimpleSelectModel(getList(ExamBulletinImportedFile.class, "importResult")));

		
/*	        model.setImportResultList(new UniFullCheckSelectModel() {
				
				@Override
				public ListResult findValues(String filter) {
					   MQBuilder builder = new MQBuilder("ru.tandemservice.narfu.entity.ExamBulletinImportedFile", "importResults");
				        if (StringUtils.isNotEmpty(filter))
				        {
				            builder.add(MQExpression.like("importResult", ExamBulletinImportedFile.importResult().s(), 
				            		CoreStringUtils.escapeLike(filter)));
				            builder.addOrder("importResult", ExamBulletinImportedFile.importResult().s());
				          }	
				        return new ListResult(builder.getResultList(getSession()));   
				}
			});*/

    }

    @Override
    public void prepareListDataSource(Model model) {

        Date importedDate = (Date) model.getSettings().get("importedDate");
        ExamBulletinImportedFileResult resultType = (ExamBulletinImportedFileResult) model.getSettings().get("importResultFilter");

        MQBuilder builder = new MQBuilder(
                "ru.tandemservice.narfu.entity.ExamBulletinImportedFile",
                "importedFiles");

        if (resultType != null)
            builder.add(MQExpression.eq("importedFiles", "importResult", resultType));

        if (importedDate != null) {
            Calendar c = Calendar.getInstance();
            c.setTime(importedDate);
            c.add(Calendar.HOUR, 24);
            builder.add(MQExpression.between("importedFiles", "importDate", importedDate, c.getTime()));
        }
        new OrderDescriptionRegistry("importedFiles").applyOrder(builder, model
                .getDataSource().getEntityOrder());
        UniUtils.createPage(model.getDataSource(), builder, getSession());

    }

    public byte[] getPrintImportedFileRequest(long id) {
        return ((ExamBulletinImportedFile) get(ExamBulletinImportedFile.class,
                                               Long.valueOf(id))).getImportedFile();
    }

    public byte[] getPrintResultFileRequest(long id) {
        return ((ExamBulletinImportedFile) get(ExamBulletinImportedFile.class,
                                               Long.valueOf(id))).getResultFile();
    }
}