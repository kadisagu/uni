package ru.tandemservice.narfu.dao;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unitutor.entity.Student2Orientation;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eqValue;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

public class OrientationDAO extends ru.tandemservice.movestudentrmc.dao.OrientationDAO
{

    @Override
    public DQLSelectBuilder getOrientationsBuilder() {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student2Orientation.class, "st2o")
                .where(eqValue(property(Student2Orientation.actual().fromAlias("st2o")), Boolean.TRUE))
                .column(property(Student2Orientation.orientation().id().fromAlias("st2o")))
                .distinct();

        return builder;
    }

    @Override
    public DQLSelectBuilder getStudentBuilder(
            EducationOrgUnit educationOrgUnit, EducationYear educationYear)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student2Orientation.class, "st2o")
                .where(eqValue(property(Student2Orientation.actual().fromAlias("st2o")), Boolean.TRUE))
                .column(property(Student2Orientation.student().id().fromAlias("st2o")));

        FilterUtils.applySelectFilter(builder, "st2o", Student2Orientation.orientation(), educationOrgUnit);
        FilterUtils.applySelectFilter(builder, "st2o", Student2Orientation.educationYear(), educationYear);

        return builder;
    }

}
