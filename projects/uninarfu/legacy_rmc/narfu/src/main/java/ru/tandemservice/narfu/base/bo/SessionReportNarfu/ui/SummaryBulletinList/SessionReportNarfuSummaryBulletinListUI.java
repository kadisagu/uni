package ru.tandemservice.narfu.base.bo.SessionReportNarfu.ui.SummaryBulletinList;


import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.State;
import ru.tandemservice.narfu.base.bo.SessionReportNarfu.logic.SessionSummaryNarfuBulletinDSHandler;
import ru.tandemservice.narfu.base.bo.SessionReportNarfu.ui.SummaryBulletinAdd.SessionReportNarfuSummaryBulletinAdd;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.SummaryBulletinList.SessionReportSummaryBulletinListUI;


@State({@org.tandemframework.core.component.Bind(key = "orgUnitId", binding = "ouHolder.id", required = true)})
public class SessionReportNarfuSummaryBulletinListUI extends SessionReportSummaryBulletinListUI
{

    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);

        Object groups = getSettings().get("groups");
        if (groups != null)
            dataSource.put(SessionSummaryNarfuBulletinDSHandler.PARAM_GROUPS_OBJECT, groups);
    }

    public void onClickAddReport()
    {
        getActivationBuilder().asRegionDialog(SessionReportNarfuSummaryBulletinAdd.class)
                .parameter(SessionReportManager.BIND_ORG_UNIT, getOrgUnit().getId()).activate();
    }
}
