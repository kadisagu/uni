package ru.tandemservice.narfu.base.ext.SessionTransfer.logic;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.narfu.entity.EppEduPlanVersionBlockLink;
import ru.tandemservice.narfu.entity.VersionBlockDisciplinesLink;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionTransfer.logic.SessionTransferDao;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.InsideAddEdit.SessionTransferInOpWrapper;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.document.SessionTransferInsideDocument;
import ru.tandemservice.unisession.entity.document.SessionTransferInsideOperation;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionSlotRegularMark;
import ru.tandemservice.unisession.util.selectModel.SessionTermModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class SessionTransferDaoExt extends SessionTransferDao implements ISessionTransferDaoExt
{
    public List<SessionTransferInOpWrapper> getAutoFormInnerTransfers(Long studentId)
    {
        return new ArrayList<>();
    }

    private String getKeyId(EppGroupTypeFCA actionType, EppRegistryElementPart part)
    {
        return String.valueOf(actionType.getCode()) + String.valueOf(part.getId());
    }

    private String getKey(EppStudentWpeCAction slot)
    {
        return getKey(slot.getType(), slot.getStudentWpe().getRegistryElementPart());
    }

    /**
     * определяем ключ, по которому будем различать дисциплины
     * столкнулся с ситуацией, что дисциплина блока УПв <> дисциплине РУП
     * общее только название и номер части
     * пока ключ формируем по типу контроля, названию дисциплины и части дисциплины
     *
     * @param actionType
     * @param part
     * @return
     */
    private String getKey(EppGroupTypeFCA actionType, EppRegistryElementPart part)
    {
        return String.valueOf(actionType.getCode()) + String.valueOf(part.getRegistryElement().getTitle().trim()) + String.valueOf(part.getNumber());
    }

    @Override
    public List<SessionTransferInOpWrapper> getAutoFormInnerTransfers(Long studentId, EppEduPlanVersion eduPlanVersion)
    {
        //сопоставления дисциплин
        List<VersionBlockDisciplinesLink> linkList = getLinkList(studentId, eduPlanVersion);

        if (linkList.isEmpty())
            throw new ApplicationException("Автоматическое создание перезачтения не возможно, т.к. не найдены связанные дисциплины УП(в)");


        //текущие МСРП, формиурем мап: ключ - МСРП
        Map<String, EppStudentWpeCAction> actionMap = new HashMap<>();
        Map<String, EppStudentWpeCAction> actionIdMap = new HashMap<>();
        List<EppStudentWpeCAction> newActions = getActions(studentId, false);
        newActions.forEach(slot -> {
            actionMap.put(getKey(slot), slot);
            actionIdMap.put(getKeyId(slot.getType(), slot.getStudentWpe().getRegistryElementPart()), slot);
        });

        if (actionMap.isEmpty())
            throw new ApplicationException("Автоматическое создание перезачтения не возможно, т.к. в новом УПв не найдены мероприятие студента по итоговой форме контроля");

        //МСРП по старому УПв, формиурем мап по старым МСРП: ключ - МСРП
        Map<String, EppStudentWpeCAction> actionOldMap = new HashMap<>();
        Map<String, EppStudentWpeCAction> actionOldIdMap = new HashMap<>();
        List<EppStudentWpeCAction> oldActions = getActions(studentId, true);
        oldActions.forEach(slot -> {
            actionOldMap.put(getKey(slot), slot);
            actionOldIdMap.put(getKeyId(slot.getType(), slot.getStudentWpe().getRegistryElementPart()), slot);
        });

        if (actionOldMap.isEmpty())
            throw new ApplicationException("Автоматическое создание перезачтения не возможно, т.к. в старом УПв не найдены мероприятие студента по итоговой форме контроля");

        //формиурем мап: откуда брать отметку - куда ставить, на основе сопоставленых дисциплин
        /*
         * Упустили из виду регулярные манипуляции в САФУ с учебными планами, а именно:
			До появления кнопки "Изменить дисциплины реестра на основе строк учебного плана" они каждый раз создавали новые дисциплины.
			Итог: сопоставление есть с одной дисциплиной реестра, а МСРП студента, переведенного на другой УП, ссылается на другую дисциплину.
			Решили:
			действуем по следующей логике:
			1. Ищем по сопоставлению, если все штатно и дисциплины реестра совпадают - по постановке (в комментариях на перезачтении напротив этой дисциплины ставим "сопоставлено из настройки".
			2. Если МСРП с оценкой и дисциплина в настройке сопоставлений не совпадают, то берем название дисциплины из сопоставления и ищем по МСРП аналогичное название, часть дисциплины и форму контроля.
				Находим, ставим перезачтение и в комментариях пишем "Сопоставлено по названию и части дисциплины, а так же по форме контроля"

		в mapId - совпадение части дисциплины из блока УП и из РУП (по id) (100% нужная нам дисциплина)
		в map - совпадение части дисциплины из блока УП и из РУП (по названию, части и форме контроля)
         */
        Map<EppStudentWpeCAction, EppStudentWpeCAction> mapLinkByName = new HashMap<>();
        Map<EppStudentWpeCAction, EppStudentWpeCAction> mapLinkById = new HashMap<>();
        for (VersionBlockDisciplinesLink link : linkList)
        {
            // откуда будем брать оценку
            String keyDst;
            EppStudentWpeCAction cActionDst;
            //куда будем ставить оценку
            String keySrc;
            EppStudentWpeCAction cActionSrc;

            keyDst = getKeyId(link.getControlActionDst().getControlAction().getEppGroupType(), link.getControlActionDst().getPart());
            cActionDst = actionOldIdMap.get(keyDst);
            keySrc = getKeyId(link.getControlActionSrc().getControlAction().getEppGroupType(), link.getControlActionSrc().getPart());
            cActionSrc = actionIdMap.get(keySrc);
            if (cActionDst != null && cActionSrc != null)
                mapLinkById.put(cActionDst, cActionSrc);

            keyDst = getKey(link.getControlActionDst().getControlAction().getEppGroupType(), link.getControlActionDst().getPart());
            cActionDst = actionOldMap.get(keyDst);
            keySrc = getKey(link.getControlActionSrc().getControlAction().getEppGroupType(), link.getControlActionSrc().getPart());
            cActionSrc = actionOldMap.get(keySrc);
            if (cActionDst != null && cActionSrc != null)
                mapLinkByName.put(cActionDst, cActionSrc);
        }

        if (mapLinkByName.isEmpty() && mapLinkById.isEmpty())
            throw new ApplicationException("Автоматическое создание перезачтения не возможно, т.к. не найдены сопоставленные дисциплины учебных планов");

        //формиуруем список перезачтений
        return getAutoFormInnerTransfers(mapLinkById, mapLinkByName);
    }

    private List<EppStudentWpeCAction> getActions(Long studentId, boolean old)
    {
        String alias = "ss";
        DQLSelectBuilder actionBuilder = new DQLSelectBuilder()
                .fromEntity(EppStudentWpeCAction.class, alias)
                .column(alias)
                .where(eq(property(alias, EppStudentWpeCAction.studentWpe().student().id()), value(studentId)));

        if (old)
            actionBuilder.where(isNotNull(property(alias, EppStudentWpeCAction.studentWpe().removalDate())));
        else
            actionBuilder.where(isNull(property(alias, EppStudentWpeCAction.studentWpe().removalDate())));

        return getList(actionBuilder);
    }

    private List<VersionBlockDisciplinesLink> getLinkList(Long studentId, EppEduPlanVersion eduPlanVersion)
    {
        String alias = "bLink";
        DQLSelectBuilder vLinkBuilder = new DQLSelectBuilder()
                .fromEntity(EppEduPlanVersionBlockLink.class, alias)
                .column(property(alias))

                .where(eq(property(alias, EppEduPlanVersionBlockLink.blockSrc().id()),
                          value(IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionBlock(studentId))))

                .where(existsByExpr(EppEduPlanVersionBlock.class, "dst", and(
                        eq(property("dst"), property(alias, EppEduPlanVersionBlockLink.blockDst())),
                        eq(property("dst", EppEduPlanVersionBlock.eduPlanVersion()), value(eduPlanVersion))
                )));

        return getList(
                new DQLSelectBuilder()
                        .fromEntity(VersionBlockDisciplinesLink.class, "dLink")
                        .column(property("dLink"))
                        .where(in(property("dLink", VersionBlockDisciplinesLink.versionBlockLink()), vLinkBuilder.buildQuery()))
        );
    }

    private List<SessionTransferInOpWrapper> getAutoFormInnerTransfers(Map<EppStudentWpeCAction, EppStudentWpeCAction> mapLinkById,
                                                                       Map<EppStudentWpeCAction, EppStudentWpeCAction> mapLinkByName)
    {
        //оценки по старым МСРП
        String markAlias = "mark";
        DQLSelectBuilder oldMarksDql = new DQLSelectBuilder()
                .fromEntity(SessionSlotRegularMark.class, markAlias)
                .column(property(markAlias))
                .where(or(in(property(markAlias, SessionMark.slot().studentWpeCAction()), mapLinkById.keySet()),
                          in(property(markAlias, SessionMark.slot().studentWpeCAction()), mapLinkByName.keySet())))
                //оценка выставлена перезачтением
                .joinEntity(markAlias, DQLJoinType.left, SessionTransferInsideOperation.class, "op",
                            eq(property(markAlias), property("op", SessionTransferInsideOperation.sourceMark())))
                .column("op");


        List<Object[]> oldMarks = oldMarksDql.createStatement(getSession()).list();

        List<SessionTransferInOpWrapper> result = new ArrayList<>();
        for (Object[] row : oldMarks)
        {
            SessionSlotRegularMark mark = (SessionSlotRegularMark) row[0];
            EppStudentWpeCAction oldSlot = mark.getSlot().getStudentWpeCAction();
            EppStudentWpeCAction newSlot;
            String comment;

            newSlot = mapLinkById.get(oldSlot);
            if (newSlot != null)// ищем совпадение по части дисциплины (id)
                comment = "По правилам настройки сопоставления с другими УПв";
            else                //ищем совподение по составному ключу (названию)
            {
                newSlot = mapLinkByName.get(oldSlot);
                comment = "Сопоставлено из неактуальных МСРП по названию и части дисциплины, форме контроля и общего кол-ва часов.";
            }

            if (newSlot != null)
            {
                Class sessionDocumentClass = EntityRuntime.getMeta(mark.getSlot().getDocument().getId()).getEntityClass();
                if ((!SessionTransferInsideDocument.class.isAssignableFrom(sessionDocumentClass))
                        && (!SessionStudentGradeBookDocument.class.isAssignableFrom(sessionDocumentClass)))
                {
                    SessionTransferInOpWrapper wrapper = new SessionTransferInOpWrapper();
                    wrapper.setSourceTerm(new SessionTermModel.TermWrapper(mark.getSlot()));
                    wrapper.setSourceEppSlot(oldSlot);
                    wrapper.setSourceMark(mark);
                    wrapper.setTerm(new SessionTermModel.TermWrapper(newSlot));
                    wrapper.setEppSlot(newSlot);
                    //оценку ставим только для тех дисциплин, у которых совпадает форма контроля
                    if (oldSlot.getType().equals(newSlot.getType()))
                        wrapper.setMark(mark.getValueItem());
                    if (row[1] != null)
                        comment = "<div style='white-space:nowrap;color:red;'>По дисциплине уже есть перезачтение. </div>" + comment;
                    wrapper.setComment(comment);
                    result.add(wrapper);
                }
            }
        }

        return result;
    }
}
