package ru.tandemservice.narfu.component.student.EntrantInfoStudentPub;

import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Map;


public class Model {

    private Student student;
    public Map parameters;

    public Model() {
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Map getParameters() {
        if (parameters == null) {
            parameters = new UniMap();
            ((UniMap) parameters).add("studentId", getStudent().getId());

        }
        return parameters;
    }


}
