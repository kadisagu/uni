package ru.tandemservice.narfu.component.entrant.EntrantStateExamCertificateAddEdit;

import ru.tandemservice.narfu.entity.EntrantStateExamCertificateExt;

public class Model extends ru.tandemservice.uniec.component.entrant.EntrantStateExamCertificateAddEdit.Model {
    private EntrantStateExamCertificateExt entrantStateExamCertificateExt;

    public EntrantStateExamCertificateExt getEntrantStateExamCertificateExt() {
        return entrantStateExamCertificateExt;
    }

    public void setEntrantStateExamCertificateExt(EntrantStateExamCertificateExt entrantStateExamCertificateExt) {
        this.entrantStateExamCertificateExt = entrantStateExamCertificateExt;
    }
}
