package ru.tandemservice.narfu.base.ext.Person.ui.NextOfKinView;

import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.narfu.entity.PersonNextOfKinNARFU;

public class DAO extends org.tandemframework.shared.person.base.bo.Person.ui.NextOfKinView.DAO {

    @Override
    public void prepare(org.tandemframework.shared.person.base.bo.Person.ui.NextOfKinView.Model model) {
        super.prepare(model);

        Model myModel = (Model) model;
        myModel.setPersonNextOfKinNARFU(DataAccessServices.dao().get(PersonNextOfKinNARFU.class, PersonNextOfKinNARFU.base(), model.getNextOfKin()));
    }
}
