/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.narfu.component.person.PersonEditNothernPeopleData;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudentrmc.entity.PersonNARFU;

/**
 * TODO Add description here
 *
 * @author agolubenko
 * @created Jun 12, 2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onActivateComponent(IBusinessComponent component) {
        Model model = getModel(component);
        Person person = getDao().get(Person.class, model.getPerson().getId());
        PersonNARFU person_ext = getDao().get(PersonNARFU.class, PersonNARFU.L_PERSON, person);
        if (person_ext == null) {
            person_ext = new PersonNARFU();
        }
        model.setNothernPeople(person_ext.isNeedSocialPayment());

        ((Model) model).setPersonNarfu(person_ext);
    }

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = getModel(component);
        Person person = getDao().get(Person.class, model.getPerson().getId());
        PersonNARFU person_ext = getDao().get(PersonNARFU.class, PersonNARFU.L_PERSON, person);
        if (person_ext == null) {
            person_ext = new PersonNARFU();
        }
        model.setNothernPeople(person_ext.isNeedSocialPayment());

        ((Model) model).setPersonNarfu(person_ext);
    }

    public void onChangeNothernPeople(IBusinessComponent component)
    {
    }

    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);
        Person person = getDao().get(Person.class, model.getPerson().getId());
        model.setPerson(person);
        PersonNARFU person_ext = getDao().get(PersonNARFU.class, PersonNARFU.L_PERSON, person);
        if (person_ext == null) {
            person_ext = new PersonNARFU();
            person_ext.setPerson(person);
        }

        person_ext.setNeedSocialPayment(model.isNothernPeople());
        ((Model) model).setPersonNarfu(person_ext);
        getDao().saveOrUpdate(model.getPersonNarfu());
        deactivate(component);
    }
}
