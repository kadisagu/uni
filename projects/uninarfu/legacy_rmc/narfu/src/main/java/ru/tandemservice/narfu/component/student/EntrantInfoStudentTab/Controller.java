package ru.tandemservice.narfu.component.student.EntrantInfoStudentTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate;
import ru.tandemservice.uniec.entity.entrant.OlympiadDiploma;
import ru.tandemservice.uniec.ui.EntrantStateExamCertificateNumberFormatter;


public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {

        final Student student = ((ru.tandemservice.uni.component.student.StudentPub.Model) component.getParentComponent().getModel()).getStudent();
        Model model = getModel(component);
        model.setStudent(student);
        getDao().prepare(model);
        if (model.isHasEntrantData()) {
            prepareEntrantStateExamCertificates(component);
            prepareOlympiadDiplomaDataSource(component);
            prepareMarkResultDataSource(component);
        }
    }


    //DataSources
    private void prepareEntrantStateExamCertificates(IBusinessComponent component) {
        final Model model = getModel(component);
        if (model.getEntrantStateExamCertificatesDataSource() != null) return;

        IEntityHandler disabled = entity -> {
            return true;//(!model.isAccessible()) || (((EntrantStateExamCertificate) entity).isSent());
        };

        DynamicListDataSource<EntrantStateExamCertificate> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareEntrantStateExamCertificatesDataSource(model);
        });
        dataSource.addColumn(new SimpleColumn("Свидетельство", "number", new EntrantStateExamCertificateNumberFormatter()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата выдачи", "issuanceDate", DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Статус экзамена", EntrantStateExamCertificate.stateExamType().title().s()).setClickable(false));
        dataSource.addColumn(new ToggleColumn("Оригинал", "original").setListener("onChangeCertificateOriginal").setPermissionKey("editEntrantStateExamCertificate_entrant").setDisableHandler(disabled));
        dataSource.addColumn(new BlockColumn("subjectsMarks", "Баллы по предметам"));
        dataSource.addColumn(new ToggleColumn("Зачтено", "accepted").setListener("onChangeCertificateChecked").setPermissionKey("editCheckedEntrantStateExamCertificate_entrant").setDisableHandler(disabled));
        dataSource.addColumn(new ToggleColumn("Проверено", "sent").setListener("onChangeCertificateSent").setPermissionKey("editSentEntrantStateExamCertificate_entrant").setDisableHandler(disabled));


        // dataSource.addColumn(new ActionColumn("Редактировать", "edit", "onClickEditStateExamCertificate", null).setPermissionKey("editEntrantStateExamCertificate_entrant").setDisableHandler(entityHandler));
        // dataSource.addColumn(new ActionColumn("Удалить", "delete", "onClickDelete", "Удалить свидетельство о результатах ЕГЭ № {0}?", new Object[]{"title"}).setPermissionKey("deleteEntrantStateExamCertificate_entrant").setDisableHandler(entityHandler));

        model.setEntrantStateExamCertificatesDataSource(dataSource);
    }


    private void prepareOlympiadDiplomaDataSource(IBusinessComponent component) {
        final Model model = getModel(component);
        if (model.getOlympiadDiplomaDataSource() != null) return;

        DynamicListDataSource<Model.OlympiadDiplomaWrapper> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareOlympiadDiplomaDataSource(model);
        });
        dataSource.addColumn(new SimpleColumn("Тип олимпиады", "diploma.diplomaType.title").setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Степень отличия", "diploma.degree.title").setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Название олимпиады", "diploma.olympiad").setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Предмет", "diploma.subject").setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Серия и номер", "diploma." + OlympiadDiploma.DIPLOM_KEY).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата выдачи", "diploma.issuanceDate", DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Населенный пункт", "diploma.settlement.title").setOrderable(false).setClickable(false));
        if (model.getEntrant().getEnrollmentCampaign().isOlympiadDiplomaForDirection())
            dataSource.addColumn(new MultiValuesColumn("Зачитывать по направлению", "enrollmentDirectionTitle").setOrderable(false).setClickable(false));
        dataSource.addColumn(new MultiValuesColumn("Засчитывать по дисциплине", "disciplineListTitle").setOrderable(false).setClickable(false));
        //  dataSource.addColumn(new ActionColumn("Редактировать", "edit", "onClickEditOlympiadDiploma").setPermissionKey("editEntrantOlympiadDiploma").setDisableHandler(model.getDefaultEntityHandler()));
        //  dataSource.addColumn(new ActionColumn("Удалить", "delete", "onClickDelete", "Удалить диплом участника олимпиады «{0}»?", new Object[]{"diploma.olympiad"}).setPermissionKey("deleteEntrantOlympiadDiploma").setDisableHandler(model.getDefaultEntityHandler()));

        model.setOlympiadDiplomaDataSource(dataSource);
    }

    private void prepareMarkResultDataSource(IBusinessComponent component) {
        final Model model = getModel(component);
        if (model.getMarkResultDataSource() != null) {
            return;
        }

        DynamicListDataSource<Model.DisciplineMarkWrapper> dataSource = new DynamicListDataSource<>(component, component1 -> {
            Controller.this.getDao().prepareMarkResultDataSource(model);
        });
        AbstractColumn column = new SimpleColumn("", new String[]{"title"}).setClickable(false).setOrderable(false);
        column.setStyle("width:30%");
        dataSource.addColumn(column);
        model.setMarkResultDataSource(dataSource);

        dataSource.addColumn(new SimpleColumn("ЕГЭ", "stateExamMark").setClickable(false).setOrderable(false));
        AbstractColumn simpleColumn = new SimpleColumn("ЕГЭ по\nшкале вуза", "stateExamScaledMark").setClickable(false).setOrderable(false);
        simpleColumn.setHeaderStyle("white-space:nowrap;");
        dataSource.addColumn(simpleColumn);
        dataSource.addColumn(new SimpleColumn("Олимпиада", "olympiadMark").setClickable(false).setOrderable(false));

        dataSource.addColumn(new BlockColumn("columnStateExamInternal", "ЕГЭ (вуз)").setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn("columnStateExamInternalAppeal", "Апелляция").setClickable(false).setOrderable(false));

        dataSource.addColumn(new BlockColumn("columnExam", "Экзамен").setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn("columnExamAppeal", "Апелляция").setClickable(false).setOrderable(false));

        dataSource.addColumn(new BlockColumn("columnTest", "Тестирование").setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn("columnTestAppeal", "Апелляция").setClickable(false).setOrderable(false));

        dataSource.addColumn(new BlockColumn("columnInterview", "Собеседование").setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn("columnInterviewAppeal", "Апелляция").setClickable(false).setOrderable(false));

        dataSource.addColumn(new SimpleColumn("Итог", "finalMark").setClickable(false).setOrderable(false));
    }


    //Listeners
    @Override
    public void onDeactivateComponent(IBusinessComponent component) {
        super.onDeactivateComponent(component);
        getModel(component).setStudent(null);
    }


    public void onClickAddStateExamCertificate(IBusinessComponent component) {
        activateInRoot(component, new ComponentActivator("ru.tandemservice.uniec.component.entrant.EntrantStateExamCertificateAddEdit",
                                                         ParametersMap.createWith("entrantId", getModel(component).getEntrant().getId())));
    }


    public void onClickEditStateExamCertificate(IBusinessComponent component) {
        activateInRoot(component, new ComponentActivator("ru.tandemservice.uniec.component.entrant.EntrantStateExamCertificateAddEdit",
                                                         ParametersMap.createWith("certificateId", component.getListenerParameter())));
    }

    public void onChangeCertificateOriginal(IBusinessComponent component) {
        ((IDAO) getDao()).changeCertificateOriginal((Long) component.getListenerParameter());
    }

    public void onChangeCertificateChecked(IBusinessComponent component) {
        try {
            ((IDAO) getDao()).changeCertificateChecked((Long) component.getListenerParameter());
        }
        finally {
            ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
        }
    }

    public void onChangeCertificateSent(IBusinessComponent component) {
        try {
            ((IDAO) getDao()).changeCertificateSent((Long) component.getListenerParameter());
        }
        finally {
            ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
        }
    }

    public void onClickDelete(IBusinessComponent context) {
        ((IDAO) getDao()).delete((Long) context.getListenerParameter());
    }

    public void onClickAddOlympiadDiploma(IBusinessComponent component) {
        activateInRoot(component, new ComponentActivator("ru.tandemservice.uniec.component.entrant.OlympiadDiplomaAddEdit",
                                                         ParametersMap.createWith("entrantId", getModel(component).getEntrant().getId())));
    }

    public void onClickEditOlympiadDiploma(IBusinessComponent component) {
        activateInRoot(component, new ComponentActivator("ru.tandemservice.uniec.component.entrant.OlympiadDiplomaAddEdit",
                                                         ParametersMap.createWith("diplomaId", component.getListenerParameter())));
    }

}
