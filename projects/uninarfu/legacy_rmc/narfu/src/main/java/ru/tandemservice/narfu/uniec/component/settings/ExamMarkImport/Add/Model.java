package ru.tandemservice.narfu.uniec.component.settings.ExamMarkImport.Add;

import org.apache.tapestry.request.IUploadFile;

public class Model {
    private IUploadFile _uploadFile;

    public IUploadFile getUploadFile() {
        return this._uploadFile;
    }

    public void setUploadFile(IUploadFile uploadFile) {
        this._uploadFile = uploadFile;
    }

}
