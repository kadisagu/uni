package ru.tandemservice.narfu.base.ext.Person.ui.EduInstitutionView;

import ru.tandemservice.narfu.entity.PersonEduInstitutionNARFU;

public class Model extends org.tandemframework.shared.person.base.bo.Person.ui.EduInstitutionView.Model {
    private PersonEduInstitutionNARFU personEduInstitutionNARFU;

    public PersonEduInstitutionNARFU getPersonEduInstitutionNARFU() {
        return personEduInstitutionNARFU;
    }

    public void setPersonEduInstitutionNARFU(PersonEduInstitutionNARFU personEduInstitutionNARFU) {
        this.personEduInstitutionNARFU = personEduInstitutionNARFU;
    }
}
