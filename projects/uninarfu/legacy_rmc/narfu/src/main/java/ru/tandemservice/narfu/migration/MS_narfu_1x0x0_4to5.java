package ru.tandemservice.narfu.migration;

import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.ddl.schema.columns.LongDBColumn;
import org.tandemframework.dbsupport.ddl.schema.columns.ReferenceDBColumn;
import org.tandemframework.dbsupport.ddl.schema.columns.ShortDBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.accreditationrmc.entity.AccreditatedGroup;
import ru.tandemservice.accreditationrmc.entity.catalog.AccreditateType;
import ru.tandemservice.uni.entity.catalog.EducationLevels;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class MS_narfu_1x0x0_4to5 extends IndependentMigrationScript {

    private static final String TABLE_OLD = "ACCREDITATEDGROUPNARFU_T";

    @Override
    public void run(DBTool tool) throws Exception {
        tool.executeUpdate("delete from ENTITYCODE_S where NAME_P='AccreditatedGroupNARFU'", new Object[0]);

        if (!tool.tableExists(TABLE_OLD))
            return;

        IEntityMeta meta = EntityRuntime.getMeta(AccreditatedGroup.class);

        createTable(tool, meta);
        copyData(tool, meta);

        tool.table(TABLE_OLD).drop();
    }

    private void createTable(DBTool tool, IEntityMeta meta) throws Exception {
        if (tool.tableExists(meta.getTableName()))
            return;


        List<DBColumn> columnList = new ArrayList<DBColumn>();

		/*
		columnList.add( new LongDBColumn("ID", true, true) );
		columnList.add( new ShortDBColumn("DISCRIMINATOR").setNullable(false) );
		columnList.add( new LongDBColumn("BASE_ID", true, false) );
		columnList.add( new LongDBColumn("FORMATIVEORGUNIT_ID", false, false) );
		columnList.add( new LongDBColumn("BRANCH_ID", true, false) );
		columnList.add( new LongDBColumn("ACCREDITATETYPE_ID", true, false) );
		*/

        columnList.add(new LongDBColumn("ID", true, true));
        columnList.add(new ShortDBColumn("DISCRIMINATOR").setNullable(false));

        IEntityMeta metaEduLevels = EntityRuntime.getMeta(EducationLevels.class);
        columnList.add(new ReferenceDBColumn("BASE_ID", true, "fk_base_accreditatedgroup", metaEduLevels.getTableName(), "ID"));

        IEntityMeta metaOrgUnit = EntityRuntime.getMeta(OrgUnit.class);

        ReferenceDBColumn refColumn = new ReferenceDBColumn("FORMATIVEORGUNIT_ID", false, "fk_formativeorgunit_cc1cf252", metaOrgUnit.getTableName(), "ID");
        // внимательно смотрим на реализацию new ReferenceDBColumn с указанной перегрузкой, и видим, что такая перегрузка не делает
        // setNullable(true); <TODO> vch
        refColumn.setNullable(true);
        columnList.add(refColumn);

        columnList.add(new ReferenceDBColumn("BRANCH_ID", true, "fk_branch_accreditatedgroup", metaOrgUnit.getTableName(), "ID"));

        IEntityMeta metaType = EntityRuntime.getMeta(AccreditateType.class);
        columnList.add(new ReferenceDBColumn("ACCREDITATETYPE_ID", true, "fk_accreditatetype_cc1cf252", metaType.getTableName(), "ID"));

        DBTable table = new DBTable(meta.getTableName(), columnList);

        tool.createTable(table);
    }

    private void copyData(DBTool tool, IEntityMeta meta) throws Exception {
        Statement st = tool.getConnection().createStatement();
        ResultSet rs = st.executeQuery("select BASE_ID, FORMATIVEORGUNIT_ID, BRANCH_ID, ACCREDITATETYPE_ID from " + TABLE_OLD);

        PreparedStatement pst = tool.getConnection().prepareStatement("insert into " + meta.getTableName() + " (ID, DISCRIMINATOR, BASE_ID, FORMATIVEORGUNIT_ID, BRANCH_ID, ACCREDITATETYPE_ID) values (?, ?, ?, ?, ?, ?)");
        short discriminator = meta.getEntityCode().shortValue();
        while (rs.next()) {
            pst.setLong(1, EntityIDGenerator.generateNewId(discriminator));
            pst.setShort(2, discriminator);
            pst.setLong(3, rs.getLong(1));
            // Саша! rs.getLong(2) возвращает примитивный long (с маленькой буквы), какой там нахрен null???
            // rs.setLong - устанавливает примитив
            // <TODO> vch
            pst.setObject(4, rs.getObject(2));
            pst.setLong(5, rs.getLong(3));
            pst.setLong(6, rs.getLong(4));

            pst.executeUpdate();
        }

        rs.close();
        pst.close();
    }
}		