package ru.tandemservice.narfu.component.entrant.EcDistributionPrintProtocol;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.catalog.codes.DevelopConditionCodes;
import ru.tandemservice.uniec.base.bo.EcDistribution.EcDistributionManager;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgDistribution;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.ws.distribution.EnrollmentDistributionEnvironmentNode;
import ru.tandemservice.uniec.ws.distribution.EnrollmentDistributionEnvironmentNode.EntrantNode.EntrantRow;
import ru.tandemservice.uniec.ws.distribution.IEnrollmentDistributionServiceDao;

import java.util.*;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        model.setCourse(DevelopGridDAO.getCourseMap().get(1));
        List<Course> courseList = DevelopGridDAO.getCourseList();
        model.setCourseList(courseList);

        model.setCompensationType(getNotNull(CompensationType.class, model.getCompensationType().getId()));

        Calendar c = Calendar.getInstance();
        c.set(Calendar.MONTH, Calendar.SEPTEMBER);
        c.set(Calendar.DAY_OF_MONTH, 1);
        model.setEntranceDate(c.getTime());
    }

    @Override
    public RtfDocument createReport(Model model) {
        boolean normal = model.getConkurs().getId() == 2;
        List<GroupInfo> dataList = getData(model);

        TemplateDocument templateDocument = getCatalogItem(TemplateDocument.class, "narfuEcgDistributionProtocol");
        RtfDocument document = new RtfReader().read(templateDocument.getContent());

        templateDocument = getCatalogItem(TemplateDocument.class, "narfuEcgDistributionProtocol_agenda");
        RtfDocument templateDevForm = new RtfReader().read(templateDocument.getContent());

        templateDocument = getCatalogItem(TemplateDocument.class, "narfuEcgDistributionProtocol_fou");
        RtfDocument templateFou = new RtfReader().read(templateDocument.getContent());

        templateDocument = getCatalogItem(TemplateDocument.class, "narfuEcgDistributionProtocol_spec");
        RtfDocument templateSpec = new RtfReader().read(templateDocument.getContent());

        templateDocument = getCatalogItem(TemplateDocument.class, "narfuEcgDistributionProtocol_par");
        RtfDocument templatePar = new RtfReader().read(templateDocument.getContent());
        RtfInjectModifier im;


        String formOU = "";
        String devForm = "";
        List<IRtfElement> rtfList = new ArrayList<IRtfElement>();
        for (GroupInfo groupInfo : dataList) {
            if (groupInfo.getList(normal).isEmpty())
                continue;

            if (!groupInfo.eou.getDevelopForm().getTitle().equals(devForm)) {
                RtfDocument doc = templateDevForm.getClone();

                im = new RtfInjectModifier();
                im.put("Course", "" + model.getCourse().getIntValue());
                im.put("DevForm", groupInfo.eou.getDevelopForm().getGenCaseTitle().toLowerCase());

                if (CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(model.getCompensationType().getCode()))
                    im.put("Budget", "на места, финансируемые из средств федерального бюджета");
                else
                    im.put("Budget", "на места с полным возмещением затрат");

                im.modify(doc);

                rtfList.addAll(doc.getElementList());
                devForm = groupInfo.eou.getDevelopForm().getTitle();
            }

            if (!groupInfo.eou.getFormativeOrgUnit().getTitle().equals(formOU)) {
                RtfDocument doc = templateFou.getClone();

                OrgUnit fou = groupInfo.eou.getFormativeOrgUnit();
                im = new RtfInjectModifier();
                im.put("fouTitle", !StringUtils.isEmpty(fou.getGenitiveCaseTitle()) ? fou.getGenitiveCaseTitle() : fou.getTitle());
                im.modify(doc);

                rtfList.addAll(doc.getElementList());
                formOU = groupInfo.eou.getFormativeOrgUnit().getTitle();
            }


            rtfList.addAll(getGroupList(model, groupInfo, templateSpec, templatePar));
        }

        im = new RtfInjectModifier();
        im.put("comissionTitle", model.getComissionTitle());
        im.put("protocolDate", RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(model.getProtocolDate()));
        im.put("protocolNumber", model.getProtocolNumber());
        im.put("body", rtfList);
        im.modify(document);

        return document;
    }

    private List<IRtfElement> getGroupList(Model model, GroupInfo groupInfo, RtfDocument templateSpec, RtfDocument templatePar) {
        boolean normal = model.getConkurs().getId() == 2;
        List<IRtfElement> rtfList = new ArrayList<>();

        List<EntrantRow> rowList = groupInfo.getList(normal);

        String specialityKey = "";
        Long specCounter = 1L;

        if (!specialityKey.equals(groupInfo.getKey())) {
            specialityKey = groupInfo.getKey();
            RtfDocument doc = templateSpec.getClone();

            RtfInjectModifier im = new RtfInjectModifier();

            Long formativeOrgUnitId = groupInfo.eou.getFormativeOrgUnit().getId();

            // счетчик параграфов для каждого формирующего
            if (model.getParagraphCounter().containsKey(formativeOrgUnitId)) {
                specCounter = model.getParagraphCounter().get(formativeOrgUnitId);
                im.put("specNumber", "" + specCounter);
                ++specCounter;
                model.getParagraphCounter().put(formativeOrgUnitId, specCounter);
            }
            else {
                im.put("specNumber", "" + specCounter);
                ++specCounter;
                model.getParagraphCounter().put(formativeOrgUnitId, specCounter);
            }

            im.put("specTitle", groupInfo.eou.getEducationLevelHighSchool().getDisplayableShortTitle());
            im.put("devPeriod", groupInfo.eou.getDevelopPeriod().getTitle().toLowerCase());

            if (DevelopConditionCodes.FULL_PERIOD.equals(groupInfo.eou.getDevelopCondition().getCode()))
                im.put("devCondition", "");
            else
                im.put("devCondition", ", " + groupInfo.eou.getDevelopCondition().getTitle());

            im.modify(doc);

            rtfList.addAll(doc.getElementList());
        }

        Map<String, List<String[]>> tableMap = new HashMap<String, List<String[]>>();
        for (EntrantRow row : rowList) {

            String kind = GroupInfo.getKoncursType(row);
            List<String[]> list = tableMap.get(kind);
            if (list == null)
                tableMap.put(kind, list = new ArrayList<String[]>());

            String[] arr = groupInfo.getArr(row);
            // Проверяем что не пустой
            if (arr != null)
                list.add(arr);
        }

        int tableNumber = 1;
        for (String konkurs : GroupInfo.PAR_LIST) {
            List<String[]> list = tableMap.get(konkurs);
            if (list == null || list.isEmpty())
                continue;

            RtfDocument doc = getTable(model, groupInfo, konkurs, list, templatePar, tableNumber++);
            rtfList.addAll(doc.getElementList());
        }

        return rtfList;
    }

    private RtfDocument getTable(Model model, final GroupInfo groupInfo, String konkurs, List<String[]> tableData, RtfDocument template, int blockCounter) {
        RtfDocument result = template.getClone();

        RtfInjectModifier im = new RtfInjectModifier();
        im.put("Punkt", "" + blockCounter);
        im.put("entranceDate", RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(model.getEntranceDate()));
        //im.put("Category", groupInfo.category.getTitle().toLowerCase());
        //im.put("Course", ""+model.getCourse().getTitle());
        //im.put("devForm", groupInfo.eou.getDevelopForm().getGenCaseTitle());
        /*
		if (CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(model.getCompensationType().getCode()))
			im.put("Budget", "на места, финансируемые из средств федерального бюджета");
		else
			im.put("Budget", "на места с полным возмещением затрат");
		
		*/
        im.put("vidConk", konkurs);
        im.modify(result);

        int counter = 1;
        for (String[] arr : tableData)
            arr[0] = "" + counter++;

        RtfTableModifier tm = new RtfTableModifier();
        tm.put("T", tableData.toArray(new String[][]{}));
        tm.put("T", new RtfRowIntercepterBaseExt(groupInfo));
        tm.modify(result);

        return result;
    }

    private List<GroupInfo> getData(Model model) {
        boolean secondEdu = model.getCategoryId() == EcDistributionManager.DISTRIBUTION_CATEGORY_SECOND_HIGH_ADMISSION.getId();
        boolean isBudget = CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(model.getCompensationType().getCode());

        Map<String, GroupInfo> dataMap = new HashMap<String, GroupInfo>();

        for (Long id : model.getIds()) {
            IEcgDistribution distribution = get(id);
            if (distribution.getConfig().isSecondHighAdmission() != secondEdu)
                continue;
            if (distribution.getConfig().getCompensationType().isBudget() != isBudget)
                continue;

            EnrollmentDistributionEnvironmentNode node = IEnrollmentDistributionServiceDao.INSTANCE.get().getEnrollmentDistributionEnvironmentNode(Collections.singleton(distribution));

            EnrollmentDirection enrollmentDirection = null;
            if (distribution.getConfig().getEcgItem() instanceof EnrollmentDirection) {
                enrollmentDirection = (EnrollmentDirection) distribution.getConfig().getEcgItem();
            }
            else {
                enrollmentDirection = get(EnrollmentDirection.class, Long.parseLong(node.distribution.row.get(0).quota.row.get(0).id));
            }

            GroupInfo group = new GroupInfo(enrollmentDirection.getEducationOrgUnit());
            if (!dataMap.containsKey(group.getKey())) {
                group.node = node;
                group.compensationType = model.getCompensationType();
                group.category = secondEdu ? EcDistributionManager.DISTRIBUTION_CATEGORY_SECOND_HIGH_ADMISSION : EcDistributionManager.DISTRIBUTION_CATEGORY_STUDENTS_AND_LISTENERS;
                dataMap.put(group.getKey(), group);
            }
            group = dataMap.get(group.getKey());

            for (EntrantRow eRow : node.distribution.row.get(0).entrant.row)
                group.list.add(eRow);

        }

        List<GroupInfo> result = new ArrayList<GroupInfo>(dataMap.values());
        Collections.sort(result);
        return result;
    }
}
