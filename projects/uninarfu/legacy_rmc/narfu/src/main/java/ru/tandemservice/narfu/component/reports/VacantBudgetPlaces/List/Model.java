package ru.tandemservice.narfu.component.reports.VacantBudgetPlaces.List;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.narfu.entity.NarfuReportVacantBudgetPlaces;

public class Model {
    private DynamicListDataSource<NarfuReportVacantBudgetPlaces> dataSource;

    public DynamicListDataSource<NarfuReportVacantBudgetPlaces> getDataSource() {
        return dataSource;
    }

    public void setDataSource(
            DynamicListDataSource<NarfuReportVacantBudgetPlaces> dataSource)
    {
        this.dataSource = dataSource;
    }
}
