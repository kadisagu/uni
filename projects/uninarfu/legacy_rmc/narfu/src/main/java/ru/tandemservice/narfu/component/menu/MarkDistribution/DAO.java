package ru.tandemservice.narfu.component.menu.MarkDistribution;


import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.ExamPassMarkIstu;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class DAO extends ru.tandemservice.uniecmarkextrmc.component.menu.MarkDistribution.DAO implements IDAO {
    private static AtomicInteger COUNTER;

    @Override
    public synchronized AtomicInteger getMaxCode() {
        if (COUNTER != null)
            return COUNTER;

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(ExamPassMarkIstu.class, "em")
                .column(DQLFunctions.max(DQLFunctions.cast(
                        DQLExpressions.property(ExamPassMarkIstu.workCipher().fromAlias("em")), PropertyType.INTEGER
                )));
        List<Integer> list = dql.createStatement(getSession()).list();

        Integer result = (list == null || list.isEmpty() || list.get(0) == null) ? 0 : list.get(0);
        COUNTER = new AtomicInteger(result);

        return COUNTER;
    }
}
