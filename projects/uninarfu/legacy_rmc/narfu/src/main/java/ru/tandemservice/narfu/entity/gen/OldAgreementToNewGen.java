package ru.tandemservice.narfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import ru.tandemservice.narfu.entity.OldAgreementToNew;
import ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Перенос старых договоров в новые
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OldAgreementToNewGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.narfu.entity.OldAgreementToNew";
    public static final String ENTITY_NAME = "oldAgreementToNew";
    public static final int VERSION_HASH = 2025863028;
    private static IEntityMeta ENTITY_META;

    public static final String L_OLD_AGREEMENT = "oldAgreement";
    public static final String L_NEW_AGREEMENT = "newAgreement";
    public static final String P_DATE = "date";
    public static final String P_SUCCESS = "success";
    public static final String P_ERROR_MESSAGE = "errorMessage";
    public static final String P_SUCCESS_MESSAGE = "successMessage";
    public static final String P_CONTRACT_VERSION_ID = "contractVersionId";

    private UniscEduAgreement2Student _oldAgreement;     // Старый договор
    private CtrContractObject _newAgreement;     // Новый договор
    private Date _date;     // Дата переноса
    private boolean _success;     // Перенесено без ошибок
    private String _errorMessage;     // Сообщение об ощибке
    private String _successMessage;     // Примечание к копированию
    private Long _contractVersionId;     // Ссылка на версию

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Старый договор. Свойство должно быть уникальным.
     */
    public UniscEduAgreement2Student getOldAgreement()
    {
        return _oldAgreement;
    }

    /**
     * @param oldAgreement Старый договор. Свойство должно быть уникальным.
     */
    public void setOldAgreement(UniscEduAgreement2Student oldAgreement)
    {
        dirty(_oldAgreement, oldAgreement);
        _oldAgreement = oldAgreement;
    }

    /**
     * @return Новый договор.
     */
    public CtrContractObject getNewAgreement()
    {
        return _newAgreement;
    }

    /**
     * @param newAgreement Новый договор.
     */
    public void setNewAgreement(CtrContractObject newAgreement)
    {
        dirty(_newAgreement, newAgreement);
        _newAgreement = newAgreement;
    }

    /**
     * @return Дата переноса. Свойство не может быть null.
     */
    @NotNull
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Дата переноса. Свойство не может быть null.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Перенесено без ошибок. Свойство не может быть null.
     */
    @NotNull
    public boolean isSuccess()
    {
        return _success;
    }

    /**
     * @param success Перенесено без ошибок. Свойство не может быть null.
     */
    public void setSuccess(boolean success)
    {
        dirty(_success, success);
        _success = success;
    }

    /**
     * @return Сообщение об ощибке.
     */
    @Length(max=1200)
    public String getErrorMessage()
    {
        return _errorMessage;
    }

    /**
     * @param errorMessage Сообщение об ощибке.
     */
    public void setErrorMessage(String errorMessage)
    {
        dirty(_errorMessage, errorMessage);
        _errorMessage = errorMessage;
    }

    /**
     * @return Примечание к копированию.
     */
    @Length(max=1200)
    public String getSuccessMessage()
    {
        return _successMessage;
    }

    /**
     * @param successMessage Примечание к копированию.
     */
    public void setSuccessMessage(String successMessage)
    {
        dirty(_successMessage, successMessage);
        _successMessage = successMessage;
    }

    /**
     * @return Ссылка на версию.
     */
    public Long getContractVersionId()
    {
        return _contractVersionId;
    }

    /**
     * @param contractVersionId Ссылка на версию.
     */
    public void setContractVersionId(Long contractVersionId)
    {
        dirty(_contractVersionId, contractVersionId);
        _contractVersionId = contractVersionId;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OldAgreementToNewGen)
        {
            setOldAgreement(((OldAgreementToNew)another).getOldAgreement());
            setNewAgreement(((OldAgreementToNew)another).getNewAgreement());
            setDate(((OldAgreementToNew)another).getDate());
            setSuccess(((OldAgreementToNew)another).isSuccess());
            setErrorMessage(((OldAgreementToNew)another).getErrorMessage());
            setSuccessMessage(((OldAgreementToNew)another).getSuccessMessage());
            setContractVersionId(((OldAgreementToNew)another).getContractVersionId());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OldAgreementToNewGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OldAgreementToNew.class;
        }

        public T newInstance()
        {
            return (T) new OldAgreementToNew();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "oldAgreement":
                    return obj.getOldAgreement();
                case "newAgreement":
                    return obj.getNewAgreement();
                case "date":
                    return obj.getDate();
                case "success":
                    return obj.isSuccess();
                case "errorMessage":
                    return obj.getErrorMessage();
                case "successMessage":
                    return obj.getSuccessMessage();
                case "contractVersionId":
                    return obj.getContractVersionId();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "oldAgreement":
                    obj.setOldAgreement((UniscEduAgreement2Student) value);
                    return;
                case "newAgreement":
                    obj.setNewAgreement((CtrContractObject) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "success":
                    obj.setSuccess((Boolean) value);
                    return;
                case "errorMessage":
                    obj.setErrorMessage((String) value);
                    return;
                case "successMessage":
                    obj.setSuccessMessage((String) value);
                    return;
                case "contractVersionId":
                    obj.setContractVersionId((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "oldAgreement":
                        return true;
                case "newAgreement":
                        return true;
                case "date":
                        return true;
                case "success":
                        return true;
                case "errorMessage":
                        return true;
                case "successMessage":
                        return true;
                case "contractVersionId":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "oldAgreement":
                    return true;
                case "newAgreement":
                    return true;
                case "date":
                    return true;
                case "success":
                    return true;
                case "errorMessage":
                    return true;
                case "successMessage":
                    return true;
                case "contractVersionId":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "oldAgreement":
                    return UniscEduAgreement2Student.class;
                case "newAgreement":
                    return CtrContractObject.class;
                case "date":
                    return Date.class;
                case "success":
                    return Boolean.class;
                case "errorMessage":
                    return String.class;
                case "successMessage":
                    return String.class;
                case "contractVersionId":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OldAgreementToNew> _dslPath = new Path<OldAgreementToNew>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OldAgreementToNew");
    }
            

    /**
     * @return Старый договор. Свойство должно быть уникальным.
     * @see ru.tandemservice.narfu.entity.OldAgreementToNew#getOldAgreement()
     */
    public static UniscEduAgreement2Student.Path<UniscEduAgreement2Student> oldAgreement()
    {
        return _dslPath.oldAgreement();
    }

    /**
     * @return Новый договор.
     * @see ru.tandemservice.narfu.entity.OldAgreementToNew#getNewAgreement()
     */
    public static CtrContractObject.Path<CtrContractObject> newAgreement()
    {
        return _dslPath.newAgreement();
    }

    /**
     * @return Дата переноса. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.OldAgreementToNew#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Перенесено без ошибок. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.OldAgreementToNew#isSuccess()
     */
    public static PropertyPath<Boolean> success()
    {
        return _dslPath.success();
    }

    /**
     * @return Сообщение об ощибке.
     * @see ru.tandemservice.narfu.entity.OldAgreementToNew#getErrorMessage()
     */
    public static PropertyPath<String> errorMessage()
    {
        return _dslPath.errorMessage();
    }

    /**
     * @return Примечание к копированию.
     * @see ru.tandemservice.narfu.entity.OldAgreementToNew#getSuccessMessage()
     */
    public static PropertyPath<String> successMessage()
    {
        return _dslPath.successMessage();
    }

    /**
     * @return Ссылка на версию.
     * @see ru.tandemservice.narfu.entity.OldAgreementToNew#getContractVersionId()
     */
    public static PropertyPath<Long> contractVersionId()
    {
        return _dslPath.contractVersionId();
    }

    public static class Path<E extends OldAgreementToNew> extends EntityPath<E>
    {
        private UniscEduAgreement2Student.Path<UniscEduAgreement2Student> _oldAgreement;
        private CtrContractObject.Path<CtrContractObject> _newAgreement;
        private PropertyPath<Date> _date;
        private PropertyPath<Boolean> _success;
        private PropertyPath<String> _errorMessage;
        private PropertyPath<String> _successMessage;
        private PropertyPath<Long> _contractVersionId;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Старый договор. Свойство должно быть уникальным.
     * @see ru.tandemservice.narfu.entity.OldAgreementToNew#getOldAgreement()
     */
        public UniscEduAgreement2Student.Path<UniscEduAgreement2Student> oldAgreement()
        {
            if(_oldAgreement == null )
                _oldAgreement = new UniscEduAgreement2Student.Path<UniscEduAgreement2Student>(L_OLD_AGREEMENT, this);
            return _oldAgreement;
        }

    /**
     * @return Новый договор.
     * @see ru.tandemservice.narfu.entity.OldAgreementToNew#getNewAgreement()
     */
        public CtrContractObject.Path<CtrContractObject> newAgreement()
        {
            if(_newAgreement == null )
                _newAgreement = new CtrContractObject.Path<CtrContractObject>(L_NEW_AGREEMENT, this);
            return _newAgreement;
        }

    /**
     * @return Дата переноса. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.OldAgreementToNew#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(OldAgreementToNewGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Перенесено без ошибок. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.OldAgreementToNew#isSuccess()
     */
        public PropertyPath<Boolean> success()
        {
            if(_success == null )
                _success = new PropertyPath<Boolean>(OldAgreementToNewGen.P_SUCCESS, this);
            return _success;
        }

    /**
     * @return Сообщение об ощибке.
     * @see ru.tandemservice.narfu.entity.OldAgreementToNew#getErrorMessage()
     */
        public PropertyPath<String> errorMessage()
        {
            if(_errorMessage == null )
                _errorMessage = new PropertyPath<String>(OldAgreementToNewGen.P_ERROR_MESSAGE, this);
            return _errorMessage;
        }

    /**
     * @return Примечание к копированию.
     * @see ru.tandemservice.narfu.entity.OldAgreementToNew#getSuccessMessage()
     */
        public PropertyPath<String> successMessage()
        {
            if(_successMessage == null )
                _successMessage = new PropertyPath<String>(OldAgreementToNewGen.P_SUCCESS_MESSAGE, this);
            return _successMessage;
        }

    /**
     * @return Ссылка на версию.
     * @see ru.tandemservice.narfu.entity.OldAgreementToNew#getContractVersionId()
     */
        public PropertyPath<Long> contractVersionId()
        {
            if(_contractVersionId == null )
                _contractVersionId = new PropertyPath<Long>(OldAgreementToNewGen.P_CONTRACT_VERSION_ID, this);
            return _contractVersionId;
        }

        public Class getEntityClass()
        {
            return OldAgreementToNew.class;
        }

        public String getEntityName()
        {
            return "oldAgreementToNew";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
