/*$Id$*/
package ru.tandemservice.narfu.base.ext.Person.ui.NextOfKinAddEdit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.person.base.bo.Person.ui.NextOfKinAddEdit.PersonNextOfKinAddEdit;


/**
 * @author DMITRY KNYAZEV
 * @since 11.02.2015
 */
@Configuration
public class PersonNextOfKinAddEditExt extends BusinessComponentExtensionManager {

    public static String ADDONE_NAME = "nextOfKinAddExt";

    @Autowired
    private PersonNextOfKinAddEdit nextOfKinAddEdit;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(nextOfKinAddEdit.presenterExtPoint())
                .addAction(new PersonNextOfKindClickApply("onClickApply"))
                .addAddon(uiAddon(ADDONE_NAME, PersonNextOfKinAddEditUIExt.class))
                .create();
    }

}
