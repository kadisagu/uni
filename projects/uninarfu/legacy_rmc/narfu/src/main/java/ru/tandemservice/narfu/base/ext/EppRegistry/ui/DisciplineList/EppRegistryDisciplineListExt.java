/* $Id$ */
package ru.tandemservice.narfu.base.ext.EppRegistry.ui.DisciplineList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.button.ButtonListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.narfu.base.ext.EppRegistry.EppRegistryExtManager;
import ru.tandemservice.narfu.base.ext.EppRegistry.logic.NarfuRegistryBaseDSHandler;
import ru.tandemservice.narfu.base.ext.EppRegistry.ui.Base.INarfuRegistryListConfig;
import ru.tandemservice.narfu.base.ext.EppRegistry.ui.Base.NarfuRegistryListAddon;
import ru.tandemservice.uniepp.base.bo.EppRegistry.ui.AbstractList.EppRegistryAbstractList;

/**
 * @author Irina Ugfeld
 * @since 10.03.2016
 */
@Configuration
public class EppRegistryDisciplineListExt extends BusinessComponentExtensionManager implements INarfuRegistryListConfig
{

    @Autowired
    private ru.tandemservice.uniepp.base.bo.EppRegistry.ui.DisciplineList.EppRegistryDisciplineList eppRegistryDisciplineList;

    @Bean
    public PresenterExtension presenterExtension() {
        EppRegistryExtManager eppRegistryExtManager = EppRegistryExtManager.instance();
        return presenterExtensionBuilder(eppRegistryDisciplineList.presenterExtPoint())
                .replaceDataSource(searchListDS(EppRegistryAbstractList.ELEMENT_DS, eppRegistryDisciplineList.getColumns(), getDisciplineListDSHandler()))
                .addDataSource(selectDS(EDU_PLAN_VERSION_DS, eppRegistryExtManager.getEduPlanVersionHandler()))
                .addDataSource(selectDS(EPP_WORK_PLAN_DS, eppRegistryExtManager.getEppWorkPlanHandler()))
                .addAddon(uiAddon(NarfuRegistryListAddon.NAME, NarfuRegistryListAddon.class))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> getDisciplineListDSHandler() {
        return new NarfuRegistryBaseDSHandler(getName());
    }

    @Bean
    public ButtonListExtension blockButtonListExtension() {
        return buttonListExtensionBuilder(eppRegistryDisciplineList.blockActionButtonListExtPoint())
                .addButton(submitButton(EDIT_OWNER_BUTTON).listener(NarfuRegistryListAddon.NAME + ":onClickEditOwner").permissionKey("ui:sec.editOwner"))
                .addButton(submitButton(COPY_BUTTON).listener(NarfuRegistryListAddon.NAME + ":onClickCopy"))
                .create();
    }

}