package ru.tandemservice.narfu.component.reports.JournalIssuanceCallCertificate.List;

import jxl.read.biff.BiffException;
import jxl.write.WriteException;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.narfu.entity.StudentDocumentRMC;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;

import java.io.IOException;

//import org.tandemframework.core.view.list.column.IndicatorColumn;
//import ru.tandemservice.uni.entity.employee.StudentDocument;
//-------------------------------------------------------------
//-------------------------------------------------------------

@SuppressWarnings("rawtypes")
public class Controller extends DefaultCatalogPubController
{
    protected DynamicListDataSource<?> createListDataSource(IBusinessComponent context) {
        final Model model = context.getModel();

        DynamicListDataSource<?> dataSource = new DynamicListDataSource<>(context, component -> {
            ((IDAO) getDao()).prepareProjectsDataSource(model);

        });
        /*
        dataSource.addColumn(IndicatorColumn.createIconColumn("student", "Студент"));
        dataSource.addColumn(new SimpleColumn("ФИО", StudentDocument.student().person().fullFio()).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Формирующее подразделение", StudentDocument.student().educationOrgUnit().formativeOrgUnit().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Форма обучения", StudentDocument.student().educationOrgUnit().developForm().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Курс", StudentDocument.student().course().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Направление подготовки/специальность", StudentDocument.student().educationOrgUnit().educationLevelHighSchool().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата начала промежуточной аттестации", "dateStartCertification").setClickable(false).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Дата окончания промежуточной аттестации", "dateEndCertification").setClickable(false).setOrderable(true));
        */
        //------------------------------------------------------------------------------------------------------------------------------------------------
        dataSource.addColumn(new SimpleColumn("Номер", StudentDocumentRMC.base().number()).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Дата выдачи", StudentDocumentRMC.base().formingDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("ФИО", StudentDocumentRMC.base().student().person().fullFio()).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Формирующее подразделение", StudentDocumentRMC.base().student().educationOrgUnit().formativeOrgUnit().title().s()).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Форма обучения", StudentDocumentRMC.base().student().educationOrgUnit().developForm().title().s()).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Курс", StudentDocumentRMC.base().student().course().title().s()).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Направление подготовки/специальность", StudentDocumentRMC.base().student().educationOrgUnit().title()).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Дата начала промежуточной аттестации", StudentDocumentRMC.dateStartCertification()).setFormatter(DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Дата окончания промежуточной аттестации", StudentDocumentRMC.dateEndCertification()).setFormatter(DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(true));

        //------------------------------------------------------------------------------------------------------------------------------------------------
        return dataSource;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onRefreshComponent(IBusinessComponent context) {
        Model model = context.getModel();
        ((IDAO) getDao()).prepare(model);
        model.setDataSource(createListDataSource(context));
    }

    public void onClickPrintJournal(IBusinessComponent context) throws BiffException, IOException, WriteException
    {
        final Model model = context.getModel();

        ((IDAO) getDao()).preparePrintReport(model);

        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(model.getContent(), model.getFileName());

        activateInRoot(context, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap()
                .add("id", id).add("extension", "xls")));
    }
    
    //-------------------------------------------------------------------------------------------------------------------------------------
    @Override
    public void onClickSearch(IBusinessComponent context) {
    	
        onRefreshComponent(context);
    }
    
    @Override
    public void onClickClear(IBusinessComponent context) {
        Model model = context.getModel();
        model.getCourseList().clear();
        model.setFormativeOrgUnitList(null);
        model.getDevelopformList().clear();
        model.setStartDate(null);
        model.setEndDate(null);
        
        onClickSearch(context);
    }
    //-------------------------------------------------------------------------------------------------------------------------------------

}
