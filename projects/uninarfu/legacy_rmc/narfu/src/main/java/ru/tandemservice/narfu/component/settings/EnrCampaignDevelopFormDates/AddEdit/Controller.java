package ru.tandemservice.narfu.component.settings.EnrCampaignDevelopFormDates.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController<IDAO, Model> {
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);
    }

    public void onClickApply(IBusinessComponent component)
    {
        getDao().update(getModel(component));

        deactivate(component);
    }
}
