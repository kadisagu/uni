package ru.tandemservice.narfu.component.documents.d101.Add;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.militaryrmc.dao.StudenOrderInfo;
import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.Date;
import java.util.List;

@SuppressWarnings("rawtypes")
public class Model extends DocumentAddBaseModel {

    private String _benefits;
    private List<EnrollmentExtract> _EnrollmentExtractList;
    private Date _dateEnd;
    private String _executor;


    private Date _formingDate;
    private String _studentTitleStr;
    private List<Course> _courseList;
    private Course _course;
    private List<IdentifiableWrapper> _departmentList;
    private IdentifiableWrapper _department;
    private String _documentForTitle;
    private String _managerPostTitle;
    private String _managerFio;
    private String _secretarTitle;
    private Integer _term;
    private String _phone;

    private String _targetPlace = "";


    private List<StudenOrderInfo> ordersList;
    private ISelectModel educationYearModel;
    private EducationYear educationYear;
    private boolean withSignatureRector;


    // Getters & Setters

    public void setTargetPlace(String _targetPlace) {
        this._targetPlace = _targetPlace;
    }

    public boolean isWithSignatureRector() {
        return withSignatureRector;
    }

    public void setWithSignatureRector(boolean withSignatureRector) {
        this.withSignatureRector = withSignatureRector;
    }

    public ISelectModel getEducationYearModel() {
        return educationYearModel;
    }

    public void setEducationYearModel(ISelectModel educationYearModel) {
        this.educationYearModel = educationYearModel;
    }

    public EducationYear getEducationYear() {
        return educationYear;
    }

    public void setEducationYear(EducationYear educationYear) {
        this.educationYear = educationYear;
    }

    public String getTargetPlace() {
        return _targetPlace;
    }

    public Date getFormingDate()
    {
        return _formingDate;
    }

    public void setFormingDate(Date formingDate)
    {
        _formingDate = formingDate;
    }

    public String getStudentTitleStr()
    {
        return _studentTitleStr;
    }

    public void setStudentTitleStr(String studentTitleStr)
    {
        _studentTitleStr = studentTitleStr;
    }

    public List<Course> getCourseList()
    {
        return _courseList;
    }

    public void setCourseList(List<Course> courseList)
    {
        _courseList = courseList;
    }

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public List<IdentifiableWrapper> getDepartmentList()
    {
        return _departmentList;
    }

    public void setDepartmentList(List<IdentifiableWrapper> departmentList)
    {
        _departmentList = departmentList;
    }

    public IdentifiableWrapper getDepartment()
    {
        return _department;
    }

    public void setDepartment(IdentifiableWrapper department)
    {
        _department = department;
    }

    public String getDocumentForTitle()
    {
        return _documentForTitle;
    }

    public void setDocumentForTitle(String documentForTitle)
    {
        _documentForTitle = documentForTitle;
    }

    public String getManagerPostTitle()
    {
        return _managerPostTitle;
    }

    public void setManagerPostTitle(String managerPostTitle)
    {
        _managerPostTitle = managerPostTitle;
    }

    public String getManagerFio()
    {
        return _managerFio;
    }

    public void setManagerFio(String managerFio)
    {
        _managerFio = managerFio;
    }

    public String getSecretarTitle()
    {
        return _secretarTitle;
    }

    public void setSecretarTitle(String secretarTitle)
    {
        _secretarTitle = secretarTitle;
    }

    public Integer getTerm()
    {
        return _term;
    }

    public void setTerm(Integer term)
    {
        _term = term;
    }

    public String getPhone()
    {
        return _phone;
    }

    public void setPhone(String phone)
    {
        _phone = phone;
    }

    public Date getDateEnd() {
        return _dateEnd;
    }

    public void setDateEnd(Date _dateEnd) {
        this._dateEnd = _dateEnd;
    }

    public String getExecutor() {
        return _executor;
    }

    public void setExecutor(String _executor) {
        this._executor = _executor;
    }

    public List<EnrollmentExtract> getEnrollmentExtractList() {
        return _EnrollmentExtractList;
    }

    public void setEnrollmentExtractList(List<EnrollmentExtract> _EnrollmentExtractList) {
        this._EnrollmentExtractList = _EnrollmentExtractList;
    }

    public String getBenefits() {
        return _benefits;
    }

    public void setBenefits(String _benefits) {
        this._benefits = _benefits;
    }

    public List<StudenOrderInfo> getOrdersList() {
        return ordersList;
    }

    public void setOrdersList(List<StudenOrderInfo> ordersList) {
        this.ordersList = ordersList;
    }


}