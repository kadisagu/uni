package ru.tandemservice.narfu.component.reports.FinishedAspirants.Add;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import ru.tandemservice.aspirantrmc.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.aspirantrmc.entity.DefenseDissertationWork;
import ru.tandemservice.movestudentrmc.entity.DocOrdRepresent;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.ListOrdListRepresent;
import ru.tandemservice.movestudentrmc.entity.RelListRepresentStudentsOldData;
import ru.tandemservice.narfu.component.reports.FinishedAspirants.ReportBlock;
import ru.tandemservice.narfu.component.reports.FinishedAspirants.ReportData;
import ru.tandemservice.narfu.component.reports.FinishedAspirants.ReportRow;
import ru.tandemservice.narfu.uniec.entity.report.FinishedAspirantsReport;
import ru.tandemservice.narfu.utils.ReportUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.*;

public class DAO extends UniDao<Model> implements IDAO {
    public static final String TEMPLATE_NAME = "narfuFinishedAspirantsReport";


    @Override
    public void prepare(final Model model) {

        OrgUnit orgUnit = model.getOrgUnitId() != null ? getNotNull(OrgUnit.class, model.getOrgUnitId()) : null;
        model.setOrgUnit(orgUnit);
        model.setCompensationTypeModel(new LazySimpleSelectModel<>(CompensationType.class, "shortTitle"));


    }

    @Override
    public FinishedAspirantsReport createReport(Model model) {
        ReportData reportData = new ReportData();
        model.getReport().setFormingDate(new Date());
        //«Отчет № 1 от 09.11.2012
//        String title = new StringBuilder()
//                .append("Отчет № ")
//                .append(String.valueOf(report.getNumber()))
//                .append(" от ")
//                .append(DateFormattingUtil.STANDARD_DATE_FORMAT.format(report.getFormingDate()))
//                .toString();
//        report.setTitle(title);

        try {
            reportData.setReport(model.getReport());
            reportData.setModel(model);
            loadTemplate(reportData);
            fillReport(reportData, model);

            reportData.getWorkbook().write();
            reportData.getWorkbook().close();

            DatabaseFile reportFile = new DatabaseFile();
            reportFile.setContent(reportData.getWorkbookStream().toByteArray());
            reportFile.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_VND_MS_EXCEL);
            reportFile.setFilename("Report.xls");

            save(reportFile);
            model.getReport().setContent(reportFile);

            save(model.getReport());
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new ApplicationException(e.getMessage(), e);
        }

        return model.getReport();
    }

    protected void fillReport(ReportData reportData, Model model) throws Exception {
        Integer firstRowIndex = 7;
        Integer firstColumnIndex = 2;
        List<ReportBlock> report = makeReport(model);

        WritableFont font = new WritableFont(WritableFont.TIMES, 9);
        WritableFont totalRowFont = new WritableFont(WritableFont.TIMES, 9, WritableFont.BOLD);

        WritableCellFormat format = new WritableCellFormat(font);
        format.setVerticalAlignment(VerticalAlignment.BOTTOM);
        format.setAlignment(Alignment.LEFT);
        format.setWrap(true);
        format.setBorder(Border.ALL, BorderLineStyle.THIN);

        WritableCellFormat totalFormat = new WritableCellFormat(totalRowFont);
        totalFormat.setVerticalAlignment(VerticalAlignment.BOTTOM);
        totalFormat.setAlignment(Alignment.LEFT);
        totalFormat.setWrap(true);
        totalFormat.setBorder(Border.ALL, BorderLineStyle.THIN);

        WritableCell cell = (WritableCell) reportData.getWorkbook().getSheet(0).getCell(0, 0);
        if (cell != null) {
            String title = cell.getContents();
            if (model.getReport().getCompensationType().isBudget())
                title = title.replace("BUD\\DOG", "обучавшихся за счет средств федерального бюджета;");
            else
                title = title.replace("BUD\\DOG", "обучавшихся по прямым договорам с оплатой стоимости обучения физическими или юридическими лицами, и");
            ((Label) cell).setString(title);
        }

        for (ReportBlock block : report) {
            drawReportBlock(block, firstColumnIndex, firstRowIndex, reportData.getWorkbook().getSheet(0), format, totalFormat);
            firstRowIndex += block.getLevelsData().size() + 1;
        }
    }

    List<ReportBlock> makeReport(Model model) {
        GregorianCalendar c = new GregorianCalendar();
        c.setTime(new Date());
        int year = c.get(Calendar.YEAR);
        Date beginYear = CoreDateUtils.getYearFirstTimeMoment(year);
        Date endYear = CoreDateUtils.getYearLastTimeMoment(year);
        List<ReportBlock> retVal = new ArrayList<>();

        Map<EducationLevelsHighSchool, ReportRow> rowMap = new HashMap<>();
        Set<Student> studentSet = new HashSet<>();


        //Список аспирантов, окончивших обучение в отчетном году

        //Поиск по представлениям
        DQLSelectBuilder represents = new DQLSelectBuilder().fromEntity(DocOrdRepresent.class, "o").joinEntity("o", DQLJoinType.inner, DocRepresentStudentBase.class, "s",
                                                                                                               DQLExpressions.eq(DQLExpressions.property(DocOrdRepresent.representation().fromAlias("o")), DQLExpressions.property(DocRepresentStudentBase.representation().fromAlias("s"))));

        represents.where(DQLExpressions.eq(DQLExpressions.property(DocOrdRepresent.representation().committed().fromAlias("o")), DQLExpressions.value(true)));
        represents.where(DQLExpressions.eq(DQLExpressions.property(DocOrdRepresent.representation().type().code().fromAlias("o")), DQLExpressions.value("diplomAndExclude")));
        represents.where(DQLExpressions.eq(DQLExpressions.property(DocRepresentStudentBase.student().person().identityCard().citizenship().code().fromAlias("s")), DQLExpressions.value(0)));
        represents.where(DQLExpressions.eq(DQLExpressions.property(DocRepresentStudentBase.student().studentCategory().code().fromAlias("s")), DQLExpressions.value(StudentCategoryCodes.STUDENT_CATEGORY_ASPIRANT)));
        represents.where(DQLExpressions.eq(DQLExpressions.property(DocRepresentStudentBase.student().compensationType().fromAlias("s")), DQLExpressions.value(model.getReport().getCompensationType())));
        represents.where(DQLExpressions.between(DQLExpressions.property(DocOrdRepresent.order().commitDateSystem().fromAlias("o")), DQLExpressions.valueDate(beginYear), DQLExpressions.valueDate(endYear)));
        represents.column(DQLExpressions.property(DocRepresentStudentBase.student().fromAlias("s")));

        //Поиск по списочным представлениям
        DQLSelectBuilder listRepresents = new DQLSelectBuilder().fromEntity(ListOrdListRepresent.class, "o").joinEntity("o", DQLJoinType.inner, RelListRepresentStudentsOldData.class, "s",
                                                                                                                        DQLExpressions.eq(DQLExpressions.property(ListOrdListRepresent.representation().fromAlias("o")), DQLExpressions.property(RelListRepresentStudentsOldData.representation().fromAlias("s"))));
        listRepresents.where(DQLExpressions.eq(DQLExpressions.property(ListOrdListRepresent.representation().committed().fromAlias("o")), DQLExpressions.value(true)));
        listRepresents.where(DQLExpressions.eq(DQLExpressions.property(ListOrdListRepresent.representation().type().code().fromAlias("o")), DQLExpressions.value("diplomAndExcludeList")));
        listRepresents.where(DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudentsOldData.student().person().identityCard().citizenship().code().fromAlias("s")), DQLExpressions.value(0)));
        listRepresents.where(DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudentsOldData.student().studentCategory().code().fromAlias("s")), DQLExpressions.value(StudentCategoryCodes.STUDENT_CATEGORY_ASPIRANT)));
        listRepresents.where(DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudentsOldData.student().compensationType().fromAlias("s")), DQLExpressions.value(model.getReport().getCompensationType())));
        listRepresents.where(DQLExpressions.between(DQLExpressions.property(ListOrdListRepresent.order().commitDateSystem().fromAlias("o")), DQLExpressions.valueDate(beginYear), DQLExpressions.valueDate(endYear)));
        listRepresents.column(DQLExpressions.property(RelListRepresentStudentsOldData.student().fromAlias("s")));

        List<Student> studentList = getList(represents);
        List<Student> listReps = getList(listRepresents);


        studentList.addAll(listReps);

        for (Student student : studentList) {
            if (studentSet.contains(student))
                continue;
            studentSet.add(student);
            EducationLevelsHighSchool level = student.getEducationOrgUnit().getEducationLevelHighSchool();
            if (!rowMap.containsKey(level))
                rowMap.put(level, new ReportRow(level));
            rowMap.get(level).getRowData()[0]++;
        }

        //Найдем студентов, защитивших диссертации
        DQLSelectBuilder dissertationsDql = new DQLSelectBuilder().fromEntity(DefenseDissertationWork.class, "d");
        dissertationsDql.where(DQLExpressions.in(DQLExpressions.property(DefenseDissertationWork.aspirant().fromAlias("d")), studentList));
        List<DefenseDissertationWork> dissertationList = getList(dissertationsDql);
        for (DefenseDissertationWork defenseDissertationWork : dissertationList) {
            EducationLevelsHighSchool level = defenseDissertationWork.getAspirant().getEducationOrgUnit().getEducationLevelHighSchool();
            if (defenseDissertationWork.getMark() != null && defenseDissertationWork.getTitle() != null) {

                rowMap.get(level).getRowData()[1]++;
                if (defenseDissertationWork.getAspirant().getEducationOrgUnit().getDevelopForm().getCode().equals(DevelopFormCodes.FULL_TIME_FORM))
                    rowMap.get(level).getRowData()[2]++;
                if (!defenseDissertationWork.getAspirant().getPerson().isMale())
                    rowMap.get(level).getRowData()[3]++;
            }
            if (defenseDissertationWork.getTitle() != null) {
                rowMap.get(level).getRowData()[4]++;
            }
        }

        //Получим список соискателей
        DQLSelectBuilder degrees = new DQLSelectBuilder().fromEntity(PersonAcademicDegree.class, "d");
        FilterUtils.applyLikeFilter(degrees, "кандидат", PersonAcademicDegree.academicDegree().title().fromAlias("d"));
        degrees.where(DQLExpressions.between(DQLExpressions.property(PersonAcademicDegree.date().fromAlias("d")), DQLExpressions.valueDate(beginYear), DQLExpressions.valueDate(endYear)));
        degrees.column(DQLExpressions.property(PersonAcademicDegree.person().fromAlias("d")));
        DQLSelectBuilder challengers = new DQLSelectBuilder().fromEntity(Student.class, "st");
        challengers.where(DQLExpressions.in(DQLExpressions.property(Student.person().fromAlias("st")), degrees.buildQuery()));
        challengers.where(DQLExpressions.eq(DQLExpressions.property(Student.studentCategory().code().fromAlias("st")), DQLExpressions.value(StudentCategoryCodes.STUDENT_CATEGORY_APPLICANT)));
        challengers.where(DQLExpressions.eq(DQLExpressions.property(Student.compensationType().fromAlias("st")), DQLExpressions.value(model.getReport().getCompensationType())));
        List<Student> challengerList = getList(challengers);

        for (Student student : challengerList) {
            if (student.getPerson().getPersonEduInstitution().getEduInstitution().getTitle().contains("САФУ")
                    || student.getPerson().getPersonEduInstitution().getEduInstitution().getTitle().contains("Северный(Арктический)"))
            {
                EducationLevelsHighSchool level = student.getEducationOrgUnit().getEducationLevelHighSchool();
                if (!rowMap.containsKey(level))
                    rowMap.put(level, new ReportRow(level));
                rowMap.get(level).getRowData()[9]++;
                if (!student.getPerson().isMale())
                    rowMap.get(level).getRowData()[10]++;
                int difference = year - student.getPerson().getPersonEduInstitution().getYearEnd();
                if (difference <= 3 && difference >= 0) {
                    rowMap.get(level).getRowData()[5 + difference]++;
                }
            }
        }

        //Отсортируем все строки по УГС
        Map<EducationLevels, List<ReportRow>> ugsMap = new TreeMap<>(new EntityComparator<EducationLevels>(new EntityOrder(EducationLevels.inheritedOkso().s())));

        for (Map.Entry<EducationLevelsHighSchool, ReportRow> entry : rowMap.entrySet()) {
            EducationLevels highParent = ReportUtils.getHighParentLevel(entry.getKey().getEducationLevel());
            if (!ugsMap.containsKey(highParent))
                ugsMap.put(highParent, new ArrayList<ReportRow>());
            ugsMap.get(highParent).add(entry.getValue());
        }

        for (Map.Entry<EducationLevels, List<ReportRow>> entry : ugsMap.entrySet()) {
            Collections.sort(entry.getValue(), new Comparator<ReportRow>() {
                @Override
                public int compare(ReportRow o1, ReportRow o2) {
                    return o1.getCode().compareTo(o2.getCode());
                }
            });
            ReportBlock block = new ReportBlock(new ArrayList<>(entry.getValue()));
            block.getTotalRow().setTitle("Всего " + entry.getKey().getTitle().toLowerCase());
            block.getTotalRow().setCode(entry.getKey().getInheritedOksoPrefix());
            retVal.add(block);
        }

        return retVal;
    }


    private void drawReportBlock(ReportBlock block, Integer left, Integer top, WritableSheet sheet, WritableCellFormat font, WritableCellFormat totalFont) throws Exception {

        for (ReportRow row : block.getLevelsData()) {
            appendRow(left, top, row, sheet, font);
            top++;
        }
        appendRow(left, top++, block.getTotalRow(), sheet, totalFont);

    }

    private void appendRow(int column, int row, ReportRow rowData, WritableSheet sheet, WritableCellFormat format) throws Exception {


        sheet.addCell(new Label(column - 2, row, rowData.getTitle(), format));
        sheet.addCell(new Label(column - 1, row, rowData.getCode(), format));
        for (int i = 0; i < rowData.getRowData().length; i++) {

            sheet.addCell(new jxl.write.Number(column + i, row, rowData.getRowData()[i], format));
        }


    }


    protected void loadTemplate(ReportData reportData) throws Exception {
        TemplateDocument template = getCatalogItem(TemplateDocument.class, TEMPLATE_NAME);
        ByteArrayInputStream in = new ByteArrayInputStream(template.getContent());

        Workbook templateWBook = Workbook.getWorkbook(in);
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);

        ByteArrayOutputStream wbOS = new ByteArrayOutputStream();
        WritableWorkbook writableWorkbook = Workbook.createWorkbook(wbOS, templateWBook, ws);

        reportData.setWorkbookStream(wbOS);
        reportData.setWorkbook(writableWorkbook);
    }

}
