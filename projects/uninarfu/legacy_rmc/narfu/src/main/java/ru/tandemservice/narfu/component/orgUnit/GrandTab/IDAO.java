package ru.tandemservice.narfu.component.orgUnit.GrandTab;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * User: ramaslov
 * Date: 21.03.13
 * Time: 17:05
 * To change this template use File | Settings | File Templates.
 */
public interface IDAO extends IUniDao<Model> {

    public void cleanAllGrant(final Model model);

}
