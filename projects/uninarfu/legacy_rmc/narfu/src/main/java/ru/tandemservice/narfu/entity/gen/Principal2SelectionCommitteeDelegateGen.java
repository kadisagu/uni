package ru.tandemservice.narfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.sec.entity.Principal;
import ru.tandemservice.narfu.entity.Principal2SelectionCommitteeDelegate;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройка задать представителя отборочной комиссии для пользователя
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class Principal2SelectionCommitteeDelegateGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.narfu.entity.Principal2SelectionCommitteeDelegate";
    public static final String ENTITY_NAME = "principal2SelectionCommitteeDelegate";
    public static final int VERSION_HASH = 859278292;
    private static IEntityMeta ENTITY_META;

    public static final String L_PRINCIPAL = "principal";
    public static final String P_POST = "post";
    public static final String P_FIO = "fio";

    private Principal _principal;     // Пользователь
    private String _post;     // Должность
    private String _fio;     // ФИО

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Пользователь. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public Principal getPrincipal()
    {
        return _principal;
    }

    /**
     * @param principal Пользователь. Свойство не может быть null и должно быть уникальным.
     */
    public void setPrincipal(Principal principal)
    {
        dirty(_principal, principal);
        _principal = principal;
    }

    /**
     * @return Должность.
     */
    @Length(max=255)
    public String getPost()
    {
        return _post;
    }

    /**
     * @param post Должность.
     */
    public void setPost(String post)
    {
        dirty(_post, post);
        _post = post;
    }

    /**
     * @return ФИО.
     */
    @Length(max=255)
    public String getFio()
    {
        return _fio;
    }

    /**
     * @param fio ФИО.
     */
    public void setFio(String fio)
    {
        dirty(_fio, fio);
        _fio = fio;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof Principal2SelectionCommitteeDelegateGen)
        {
            setPrincipal(((Principal2SelectionCommitteeDelegate)another).getPrincipal());
            setPost(((Principal2SelectionCommitteeDelegate)another).getPost());
            setFio(((Principal2SelectionCommitteeDelegate)another).getFio());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends Principal2SelectionCommitteeDelegateGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Principal2SelectionCommitteeDelegate.class;
        }

        public T newInstance()
        {
            return (T) new Principal2SelectionCommitteeDelegate();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "principal":
                    return obj.getPrincipal();
                case "post":
                    return obj.getPost();
                case "fio":
                    return obj.getFio();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "principal":
                    obj.setPrincipal((Principal) value);
                    return;
                case "post":
                    obj.setPost((String) value);
                    return;
                case "fio":
                    obj.setFio((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "principal":
                        return true;
                case "post":
                        return true;
                case "fio":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "principal":
                    return true;
                case "post":
                    return true;
                case "fio":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "principal":
                    return Principal.class;
                case "post":
                    return String.class;
                case "fio":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Principal2SelectionCommitteeDelegate> _dslPath = new Path<Principal2SelectionCommitteeDelegate>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Principal2SelectionCommitteeDelegate");
    }
            

    /**
     * @return Пользователь. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.narfu.entity.Principal2SelectionCommitteeDelegate#getPrincipal()
     */
    public static Principal.Path<Principal> principal()
    {
        return _dslPath.principal();
    }

    /**
     * @return Должность.
     * @see ru.tandemservice.narfu.entity.Principal2SelectionCommitteeDelegate#getPost()
     */
    public static PropertyPath<String> post()
    {
        return _dslPath.post();
    }

    /**
     * @return ФИО.
     * @see ru.tandemservice.narfu.entity.Principal2SelectionCommitteeDelegate#getFio()
     */
    public static PropertyPath<String> fio()
    {
        return _dslPath.fio();
    }

    public static class Path<E extends Principal2SelectionCommitteeDelegate> extends EntityPath<E>
    {
        private Principal.Path<Principal> _principal;
        private PropertyPath<String> _post;
        private PropertyPath<String> _fio;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Пользователь. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.narfu.entity.Principal2SelectionCommitteeDelegate#getPrincipal()
     */
        public Principal.Path<Principal> principal()
        {
            if(_principal == null )
                _principal = new Principal.Path<Principal>(L_PRINCIPAL, this);
            return _principal;
        }

    /**
     * @return Должность.
     * @see ru.tandemservice.narfu.entity.Principal2SelectionCommitteeDelegate#getPost()
     */
        public PropertyPath<String> post()
        {
            if(_post == null )
                _post = new PropertyPath<String>(Principal2SelectionCommitteeDelegateGen.P_POST, this);
            return _post;
        }

    /**
     * @return ФИО.
     * @see ru.tandemservice.narfu.entity.Principal2SelectionCommitteeDelegate#getFio()
     */
        public PropertyPath<String> fio()
        {
            if(_fio == null )
                _fio = new PropertyPath<String>(Principal2SelectionCommitteeDelegateGen.P_FIO, this);
            return _fio;
        }

        public Class getEntityClass()
        {
            return Principal2SelectionCommitteeDelegate.class;
        }

        public String getEntityName()
        {
            return "principal2SelectionCommitteeDelegate";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
