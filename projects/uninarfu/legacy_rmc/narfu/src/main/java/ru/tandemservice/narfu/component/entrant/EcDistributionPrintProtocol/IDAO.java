package ru.tandemservice.narfu.component.entrant.EcDistributionPrintProtocol;

import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {

    RtfDocument createReport(Model model);
}
