package ru.tandemservice.narfu.component.entrant.EntrantStateExamCertificateAddEdit;

import ru.tandemservice.narfu.component.entrant.StateExamUtil;

public class DAO extends ru.tandemservice.uniec.component.entrant.EntrantStateExamCertificateAddEdit.DAO {


    @Override
    public void prepare(ru.tandemservice.uniec.component.entrant.EntrantStateExamCertificateAddEdit.Model model) {
        super.prepare(model);
        Model modelExt = (Model) model;
        modelExt.setEntrantStateExamCertificateExt(StateExamUtil.getEntrantStateExamCertificateExt(model.getCertificate()));
    }

    @Override
    public void update(ru.tandemservice.uniec.component.entrant.EntrantStateExamCertificateAddEdit.Model model) {
        StateExamUtil.update(model, getSession());
        saveOrUpdate(((Model) model).getEntrantStateExamCertificateExt());
    }

}
