package ru.tandemservice.narfu.component.enrollmentextract;

import org.hibernate.Session;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.aspirantrmc.entity.ScienceParticipant;
import ru.tandemservice.aspirantrmc.entity.ScientificResearch;
import ru.tandemservice.narfu.entity.EntrHighShortExtractExt;
import ru.tandemservice.uniecaspirantrmc.entity.EntrantScienceParticipant;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.component.enrollmentextract.CommonEnrollmentExtractDao;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;

import java.util.List;

public class CommonEnrollmentExtractDaoNARFU extends CommonEnrollmentExtractDao {

    @Override
    public OrderData doCommit(EnrollmentExtract extract) {
        OrderData orderData = super.doCommit(extract);

        if (extract.getStudentNew() != null && extract.getOrder().getType().getCode().equals(UniecDefines.ORDER_TYPE_SECOND_HIGH)) {

            Session session = getSession();

            EntrHighShortExtractExt extractExt = get(EntrHighShortExtractExt.class, EntrHighShortExtractExt.extract().id(), extract.getId());

            if (extractExt != null) {
                extract.getStudentNew().setEducationOrgUnit(extractExt.getEducationOrgUnit());
                session.update(extract.getStudentNew());
            }
            //заберем научных руководителей аспиранта
            List<EntrantScienceParticipant> scienceParticipantList = new DQLSelectBuilder().fromEntity(EntrantScienceParticipant.class, "e")
                    .where(DQLExpressions.eqValue(DQLExpressions.property(EntrantScienceParticipant.entrantAspirantData().entrant().id().fromAlias("e")), extract.getEntity().getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getId()))
                    .createStatement(session).list();

            if (!scienceParticipantList.isEmpty()) {
                //создадим научное исследование, т.к. без него не можем назначить аспиранту научных руководителей
                ScientificResearch scientificResearch = new ScientificResearch();
                scientificResearch.setStudent(extract.getStudentNew());
                session.save(scientificResearch);

                for (EntrantScienceParticipant entrantScienceParticipant : scienceParticipantList) {
                    ScienceParticipant participant = new ScienceParticipant();
                    participant.setResearch(scientificResearch);
                    participant.setPerson(entrantScienceParticipant.getPerson());
                    participant.setParticipantType(entrantScienceParticipant.getParticipantType());
                    participant.setIsInner(entrantScienceParticipant.getIsInner());
                    participant.setIsMajor(entrantScienceParticipant.getIsMajor());

                    session.save(participant);
                }

            }

        }

        return orderData;
    }
}
