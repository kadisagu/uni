package ru.tandemservice.narfu.base.bo.SessionReportNarfu.logic;

import com.google.common.collect.Collections2;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfBorder;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.util.PersonSecurityUtil;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionTypeRow;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.group.gen.EppRealEduGroup4ActionTypeRowGen;
import ru.tandemservice.uniepp.entity.student.group.gen.EppRealEduGroupRowGen;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionReport.util.ISessionReportGroupFilterParams;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem;
import ru.tandemservice.unisession.entity.catalog.UnisessionTemplate;
import ru.tandemservice.unisession.entity.document.*;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionSlotLinkMark;
import ru.tandemservice.unisession.entity.mark.SessionSlotRegularMark;
import ru.tandemservice.unisession.entity.report.UnisessionSummaryBulletinReport;

import java.util.*;

public class SessionSummaryBulletinNarfuPrintDAO extends UniBaseDao implements ISessionSummaryBulletinNarfuPrintDAO {

    private static final String RETAKE = "retake";
    private SessionObject sessionObject;

    @Override
    public byte[] print(ISessionSummaryBulletinParams params, Collection<ISessionSummaryBulletinData> data)
    {
        UnisessionTemplate templateItem = getCatalogItem(UnisessionTemplate.class, "summaryBulletinNarfu");
        if (templateItem == null)
            throw new RuntimeException("Печатный шаблон есть, но не импортирован в справочник шаблонов.");
        RtfDocument template = new RtfReader().read(templateItem.getContent());

        RtfDocument result = null;

        for (ISessionSummaryBulletinData bulletin : data) {
            RtfDocument document = printBulletin(params, bulletin, template.getClone());
            if (null == result) {
                result = document;
            }
            else if (null != document) {
                RtfUtil.modifySourceList(result.getHeader(), document.getHeader(), document.getElementList());
                result.getElementList().addAll(document.getElementList());
                // нет признака печати с новой страницы <TODO>
            }
        }

        if (null != result) {
            return RtfUtil.toByteArray(result);
        }
        throw new ApplicationException("Нет данных для построения отчета.");
    }

    public void createStoredReport(ISessionSummaryBulletinParams params)
    {
        sessionObject = params.getSessionObject();
        Collection<ISessionSummaryBulletinData> data = getSessionSummaryBulletinData(params);

        UnisessionSummaryBulletinReport report = new UnisessionSummaryBulletinReport();

        DatabaseFile content = new DatabaseFile();
        content.setContent(print(params, data));
        save(content);
        report.setContent(content);

        report.setFormingDate(new Date());
        report.setExecutor(PersonSecurityUtil.getExecutor());
        report.setSessionObject(sessionObject);
        report.setInSession(params.isInSessionMarksOnly() ? "по результатам в сессию" : "по итоговым результатам");
        report.setDiscKinds(UniStringUtils.join(params.getObligations(), EppWorkPlanRowKind.title().s(), "; "));
        if (null != params.getCourse())
            report.setCourse(params.getCourse().getTitle());
        if (null != params.getGroupFilterParams())
            report.setGroups(UniStringUtils.joinWithSeparator("; ", new String[]{StringUtils.join(params.getGroupFilterParams().getGroups(), "; "), params.getGroupFilterParams().isIncludeStudentsWithoutGroups() ? "вне групп" : null}));
        save(report);
    }

    public Collection<ISessionSummaryBulletinData> getSessionSummaryBulletinData(final ISessionSummaryBulletinParams params)
    {
        final HashMap<EppStudentWpeCAction, Set<SessionDocument>> bulletinMap = new HashMap<>();
        final Map<EppStudentWpeCAction, EppRealEduGroupRow> eduGroupDataMap = new HashMap<>();

        if ((null != params.getGroupFilterParams()) && (!CollectionUtils.isEmpty(params.getGroupFilterParams().getGroups()))) {
            BatchUtils.execute(params.getGroupFilterParams().getGroups(), 256, new BatchUtils.Action<String>() {
                public void execute(Collection<String> elements)
                {
                    DQLSelectBuilder wpcaDQL = getWpSlotDQL(params, elements);
                    for (Object[] row : wpcaDQL.createStatement(getSession()).<Object[]>list()) {
                        EppStudentWpeCAction wpca = (EppStudentWpeCAction) row[0];
                        SafeMap.safeGet(bulletinMap, wpca, HashSet.class).add((SessionDocument) row[1]);
                        eduGroupDataMap.put(wpca, (EppRealEduGroupRow) row[2]);
                    }
                }
            });
        }

        final Map<EppStudentWpeCAction, SessionMark> markMap = new HashMap<>();
        BatchUtils.execute(bulletinMap.keySet(), DQL.MAX_VALUES_ROW_NUMBER, elements -> {
            DQLSelectBuilder markDQL = new DQLSelectBuilder().fromEntity(SessionMark.class, "m")
                    .fromEntity(SessionStudentGradeBookDocument.class, "d")
                    .where(DQLExpressions.eq(DQLExpressions.property(SessionMark.slot().document().fromAlias("m")), DQLExpressions.property("d")))
                    .where(DQLExpressions.in(DQLExpressions.property(SessionMark.slot().studentWpeCAction().fromAlias("m")), elements))
                    .where(DQLExpressions.eq(DQLExpressions.property(SessionMark.slot().inSession().fromAlias("m")), DQLExpressions.value(params.isInSessionMarksOnly())))
                    .fetchPath(DQLJoinType.inner, SessionMark.slot().document().fromAlias("m"))
                    .column(DQLExpressions.property(SessionMark.slot().studentWpeCAction().fromAlias("m")))
                    .column("m");

            for (Object[] row : markDQL.createStatement(getSession()).<Object[]>list())
                markMap.put((EppStudentWpeCAction) row[0], (SessionMark) row[1]);
        });
        Map<String, ISessionSummaryBulletinData> dataMap = new HashMap<>();

        for (EppStudentWpeCAction slot : bulletinMap.keySet()) {
            Set<SessionDocument> bulletins = bulletinMap.get(slot);
            EppRealEduGroupRow row = eduGroupDataMap.get(slot);
            SessionMark mark = markMap.get(slot);

            if (slot.getStudentWpe().getSourceRow() == null) {
                continue;
            }
            String group = row.getStudentGroupTitle();

            SessionSummaryBulletinData bulletinData = (SessionSummaryBulletinData) dataMap.get(group);
            if (null == bulletinData) {
                dataMap.put(group, bulletinData = new SessionSummaryBulletinData(group));
            }

            SessionSummaryBulletinStudent student = new SessionSummaryBulletinStudent(row);
            bulletinData.students.add(student);

            MultiKey actionKey = SessionSummaryBulletinAction.key(slot.getType(), slot.getStudentWpe().getRegistryElementPart());
            SessionSummaryBulletinAction action = (SessionSummaryBulletinAction) bulletinData.actions.get(actionKey);
            if (null == action) {
                bulletinData.actions.put(actionKey, action = new SessionSummaryBulletinAction(slot.getType(), slot.getStudentWpe().getRegistryElementPart()));
            }
            action.bulletins.addAll(bulletins);

            MultiKey key = bulletinData.key(student, action);
            bulletinData.obligationMap.put(key, slot.getStudentWpe().getSourceRow().getKind());
            bulletinData.markMap.put(key, mark);

        }

        return dataMap.values();
    }

    private DQLSelectBuilder getWpSlotDQL(ISessionSummaryBulletinParams params, Collection<String> groups)
    {
        DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(SessionStudentGradeBookDocument.class, "b")
                .column("b.id");

        DQLSelectBuilder wpcaDQL = new DQLSelectBuilder().fromEntity(EppStudentWpeCAction.class, "wpca")
                .column("wpca")
//				.fromEntity(SessionBulletinDocument.class, "b")
//				.column("b")
//                .fromEntity(SessionSheetDocument.class, "s")
                .fromEntity(SessionDocumentSlot.class, "slot")
                .fromEntity(EppRealEduGroupRow.class, "g_row")
                .column(DQLExpressions.property(SessionDocumentSlot.document().fromAlias("slot")))
                .column("g_row")
                .fetchPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().sourceRow().fromAlias("wpca"))
                .where(DQLExpressions.notIn(DQLExpressions.property(SessionDocumentSlot.document().fromAlias("slot")), subBuilder.buildQuery()))
//				.where(DQLExpressions.or(
//                        DQLExpressions.eq(DQLExpressions.property(SessionDocumentSlot.document().fromAlias("slot")), DQLExpressions.property("b")),
//                        DQLExpressions.eq(DQLExpressions.property(SessionDocumentSlot.document().fromAlias("slot")), DQLExpressions.property("s"))))
                .where(DQLExpressions.eq(DQLExpressions.property(SessionDocumentSlot.studentWpeCAction().fromAlias("slot")), DQLExpressions.property("wpca")))
                .where(DQLExpressions.eq(DQLExpressions.property(EppRealEduGroupRowGen.studentWpePart().fromAlias("g_row")), DQLExpressions.property("wpca")))
                .where(DQLExpressions.eq(DQLExpressions.property(SessionDocumentSlot.studentWpeCAction().studentWpe().part().id().fromAlias("slot")), DQLExpressions.value(sessionObject.getYearDistributionPart().getId())))
                .where(DQLExpressions.eq(DQLExpressions.property(SessionDocumentSlot.studentWpeCAction().studentWpe().year().educationYear().id().fromAlias("slot")), DQLExpressions.value(sessionObject.getEducationYear().getId())));

        if (null != params.getCourse()) {
            wpcaDQL.where(DQLExpressions.eq(DQLExpressions.property(EppStudentWpeCAction.studentWpe().course().fromAlias("wpca")), DQLExpressions.value(params.getCourse())));
        }
        if (!CollectionUtils.isEmpty(params.getObligations())) {
            wpcaDQL.where(DQLExpressions.in(DQLExpressions.property(EppStudentWpeCAction.studentWpe().sourceRow().kind().fromAlias("wpca")), params.getObligations()));
        }

        ISessionReportGroupFilterParams groupFilterParams = params.getGroupFilterParams();
        if (groupFilterParams != null) {
            DQLSelectBuilder dqlX = new DQLSelectBuilder().fromEntity(EppRealEduGroup4ActionTypeRow.class, "rel")
                    .column(DQLExpressions.property(EppRealEduGroup4ActionTypeRowGen.group().id().fromAlias("rel")));

            if (!CollectionUtils.isEmpty(groups)) {
                if (groupFilterParams.isIncludeStudentsWithoutGroups()) {
                    dqlX.where(DQLExpressions.or(new IDQLExpression[]{
                                                         DQLExpressions.in(DQLExpressions.property(EppRealEduGroup4ActionTypeRowGen.studentGroupTitle().fromAlias("rel")), groups),
                                                         DQLExpressions.isNull(DQLExpressions.property(EppRealEduGroup4ActionTypeRowGen.studentGroupTitle().fromAlias("rel")))
                                                 }
                    ));
                }
                else {
                    dqlX.where(DQLExpressions.in(DQLExpressions.property(EppRealEduGroup4ActionTypeRowGen.studentGroupTitle().fromAlias("rel")), groups));
                }
            }
            else if (groupFilterParams.isIncludeStudentsWithoutGroups()) {
                dqlX.where(DQLExpressions.isNull(DQLExpressions.property(EppRealEduGroup4ActionTypeRowGen.studentGroupTitle().fromAlias("rel"))));
            }
            wpcaDQL.where(DQLExpressions.in(DQLExpressions.property(EppRealEduGroupRowGen.group().id().fromAlias("g_row")), dqlX.buildQuery()));
        }


        return wpcaDQL;
    }

    protected RtfDocument printBulletin(ISessionSummaryBulletinParams params, ISessionSummaryBulletinData data, RtfDocument document)
    {
        if (CollectionUtils.isEmpty(data.getActions())) {
            return null;
        }
        printAdditionalData(params, data, document);

        RtfInjectModifier modifier = new RtfInjectModifier();
        prepareHeaderInjectModifier(params, data, modifier);
        modifier.modify(document);
        document.getHeader().getColorTable().addColor(219, 229, 241);

        final BulletinTableData tableData = new BulletinTableData(data);

        RtfTableModifier tableModifier = new RtfTableModifier().put("T", tableData.getTableData());
        final List<int[]> cellIndex = new ArrayList<>();
        tableModifier.put("T", new RtfRowIntercepterBase() {
            public void beforeModify(RtfTable table, int currentRowIndex)
            {
                RtfUtil.splitRow((RtfRow) table.getRowList().get(currentRowIndex - 4), 4, (newCell, index) -> newCell.getElementList().addAll(new RtfString().boldBegin().append(tableData.getCATitle(index)).boldEnd().toList())
                        , tableData.getControlActionTypeScales());


                RtfUtil.splitRow((RtfRow) table.getRowList().get(currentRowIndex - 3), 4, (newCell, index) -> {
                    newCell.getElementList().addAll(new RtfString().boldBegin().append(tableData.getDiscTitle(index)).boldEnd().toList());
                    newCell.setTextDirection(285);
                }
                        , tableData.getScales());
                /*
				//Преподователи
				RtfUtil.splitRow((RtfRow)table.getRowList().get(currentRowIndex - 2), 4, new IRtfRowSplitInterceptor()
				{
					public void intercept(RtfCell newCell, int index)
					{
						List<SessionComissionPps> ppsList = UniUtils.getPropertiesList(getList(SessionComissionPps.class, 
								SessionComissionPps.commission().s(), tableData.getBulletins(index).iterator().next().getCommission(), new String[0]), SessionComissionPps.pps().s());

						newCell.getElementList().addAll(new RtfString().boldBegin().append(UniStringUtils.join(ppsList, PpsEntryByEmployeePost.title().s(), ",")).boldEnd().toList());
						//newCell.getElementList().addAll(new RtfString().boldBegin().append(tableData.getDiscTitle(index)).boldEnd().toList());
						
						newCell.setTextDirection(285);
					}
				}
				, tableData.getScales());
				*/
                RtfUtil.splitRow((RtfRow) table.getRowList().get(currentRowIndex - 2), 4, (newCell, index) -> newCell.getElementList().addAll(new RtfString().append(tableData.getPerformDate(index)).toList())
                        , tableData.getScales());

                RtfUtil.splitRow((RtfRow) table.getRowList().get(currentRowIndex - 1), 4, (newCell, index) -> newCell.getElementList().addAll(new RtfString().append(tableData.getBulletinNumber(index)).toList())
                        , tableData.getScales());

                RtfUtil.splitRow((RtfRow) table.getRowList().get(currentRowIndex), 4, (newCell, index) -> {
                }
                        , tableData.getScales());
            }

            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                if (tableData.getTableData()[rowIndex].length == 1)
                    return new RtfString().append(1288).boldBegin().append(value).toList();
                if ((colIndex == 1) && (tableData.getGroupInactiveRows().contains(Integer.valueOf(rowIndex + 1))))
                    return new RtfString().append(656).append(value).toList();

                if (!StringUtils.isEmpty(value)) {
                    String[] str = StringUtils.split(value, ",");
                    if (str[0].equals(RETAKE)) {
                        cellIndex.add(new int[]{rowIndex, colIndex});
                        return new RtfString().append(IRtfData.I).boldBegin().append(str[1]).boldEnd().append(IRtfData.I, 0).toList();
                    }
                }
                return null;
            }

            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                for (int rowIndex = 0; rowIndex < tableData.getTableData().length; rowIndex++) {
                    String[] rowData = tableData.getTableData()[rowIndex];

                    if (rowData.length != 1)
                        continue;
                    RtfUtil.unitAllCell((RtfRow) newRowList.get(startIndex + rowIndex), 0);
                }
                for (int[] index : cellIndex) {
                    RtfCell cell = newRowList.get(index[0] + 5).getCellList().get(index[1]);
                    if (cell != null) {
                        cell.setBackgroundColorIndex(18);

                        RtfBorder oldBorder = cell.getCellBorder().getBottom();

                        RtfBorder border = RtfBorder.getInstance(40, oldBorder.getColorIndex(), oldBorder.getStyle());

                        cell.getCellBorder().setBottom(border);
                        cell.getCellBorder().setTop(border);
                        cell.getCellBorder().setLeft(border);
                        cell.getCellBorder().setRight(border);
                    }
                }

            }
        });
        tableModifier.modify(document);

        return document;
    }

    protected void prepareHeaderInjectModifier(ISessionSummaryBulletinParams params, ISessionSummaryBulletinData data, RtfInjectModifier modifier)
    {
        modifier.put("currentDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
//		OrgUnit academy = TopOrgUnit.getInstance();
        OrgUnit ou = sessionObject.getOrgUnit();
		/*	    modifier.put("vuzTitle", academy.getPrintTitle()); */
        modifier.put("ouTitle", ou.getPrintTitle());
	    /*
	    modifier.put("eduLevel", UniStringUtils.joinUnique(Collections2.transform(data.getStudents(), new Function()
	    {
	      public Student apply(Object input)
	      {
	        return ((ISessionSummaryBulletinStudent)input).getStudent();
	      }
	    }), Student.educationOrgUnit().educationLevelHighSchool().displayableTitle().s(), ", "));*/
        modifier.put("eduLevel", data.getStudents().iterator().next().getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle());
        modifier.put("formativeOrgUnit", ou.getTitle());
        modifier.put("eduYear", sessionObject.getEducationYear().getTitle());
        modifier.put("part", sessionObject.getYearDistributionPart().getTitle());
        modifier.put("course", params.getCourse().getTitle());
        modifier.put("groupTitle", StringUtils.isEmpty(data.getGroup()) ? "вне групп" : data.getGroup());

        List<String> terms = new ArrayList<>(new HashSet<>(Collections2.transform(data.getStudents(), (Object input) -> ((ISessionSummaryBulletinStudent) input).getTerm().getTitle())));
        Collections.sort(terms);
        modifier.put("term", StringUtils.join(terms, ", "));

		/*	    String ouLeader;
	    if ((ou.getOrgUnitType().getCode().equals("institute")) || (ou.getOrgUnitType().getCode().equals("branch")) 
	    		|| (ou.getOrgUnitType().getCode().equals("department")))
	      ouLeader = "Директор ";
	    else
	      ouLeader = "Декан факультета ";
	    ouLeader = ouLeader + ou.getShortTitle();
	    modifier.put("ouleader", ouLeader);

	    EmployeePost head = (EmployeePost)ou.getHead();

	    if (null == head)
	    {
	      List probablyHeaderPosts = EmployeeManager.instance().dao().getHeaderEmployeePostList(ou);
	      if (probablyHeaderPosts.size() == 1) head = (EmployeePost)probablyHeaderPosts.get(0);
	    }*/

		/*	    modifier.put("FIOouleader", head == null ? "" : head.getPerson().getFio());*/
        EmployeePost head = EmployeeManager.instance().dao().getHead(ou);

        modifier.put("headIOF", head != null ? head.getPerson().getIdentityCard().getIof() : "");
        modifier.put("operator", PersonSecurityUtil.getExecutor());
    }

    protected String[] printStudentData(int studentNumber, ISessionSummaryBulletinStudent student)
    {
        String number = Integer.toString(studentNumber);
        String fio = student.getStudent().getPerson().getFullFio();
        String compType = student.getStudent().getCompensationType().getShortTitle();
        String bookNumber = student.getStudent().getBookNumber();
        return new String[]{number, fio, bookNumber, compType};
    }

    protected void printAdditionalData(ISessionSummaryBulletinParams params, ISessionSummaryBulletinData data, RtfDocument document)
    {
    }

    protected boolean printTotals()
    {
        return false;
    }

    protected String formatBulletinsNumber(Collection<SessionDocument> bulletins)
    {
        return UniStringUtils.join(bulletins, SessionDocument.number().s(), ", ");
    }

    private class BulletinTableData {
        private List<ISessionSummaryBulletinAction> actions;
        private List<EppGroupTypeFCA> caList;
        private List<EppRegistryElementPart> discList;
        private String[][] _tableData;
        private Set<Integer> _groupInactiveRows;
        private int[] scales;
        private int[] controlActionTypeScales;

        public BulletinTableData(ISessionSummaryBulletinData data)
        {
            init(data);
        }

        public Set<Integer> getGroupInactiveRows()
        {
            return this._groupInactiveRows;
        }

        public String[][] getTableData()
        {
            return this._tableData;
        }

        public int[] getScales()
        {
            return this.scales;
        }

        public int[] getControlActionTypeScales()
        {
            return this.controlActionTypeScales;
        }

        public String getCATitle(int index)
        {
            return (this.caList.get(index)).getTitle();
        }

        public String getDiscTitle(int index)
        {
            return this.discList.get(index).getTitleWithLabor();
        }

        public Collection<SessionDocument> getBulletins(int index)
        {
            return ((ISessionSummaryBulletinAction) this.actions.get(index)).getBulletins();
        }

        private String getBulletinNumber(int index)
        {
            return formatBulletinsNumber(getBulletins(index));
        }

        private String getPerformDate(int index)
        {
            List<String> list = new ArrayList<>();
            for (SessionDocument item : getBulletins(index)) {
                if (item instanceof SessionBulletinDocument) {
                    if (null != ((SessionBulletinDocument) item).getPerformDate())
                        list.add(DateFormatter.DEFAULT_DATE_FORMATTER.format(((SessionBulletinDocument) item).getPerformDate()));
                }
                else {
                    if (null != item.getCloseDate())
                        list.add(DateFormatter.DEFAULT_DATE_FORMATTER.format(item.getCloseDate()));
                }
            }
            return StringUtils.join(list, ", ");
        }

        private void init(ISessionSummaryBulletinData data)
        {
            this.actions = new ArrayList<>(data.getActions());

            Collections.sort(this.actions, (o1, o2) -> {
                int i = o1.getControlActionType().getTitle().compareTo(o2.getControlActionType().getTitle());
                if (i == 0)
                    i = o1.getDiscipline().getRegistryElement().getTitle().compareTo(o2.getDiscipline().getRegistryElement().getTitle());
                return i;
            });
            LinkedHashMap<EppGroupTypeFCA, Collection<EppRegistryElementPart>> headerMap = new LinkedHashMap<>();

            for (ISessionSummaryBulletinAction action : this.actions) {
                SafeMap.safeGet(headerMap, action.getControlActionType(), ArrayList.class).add(action.getDiscipline());
            }

            this.caList = new ArrayList<>(headerMap.keySet());
            this.discList = new ArrayList<>();
            for (EppGroupTypeFCA ca : headerMap.keySet()) {
                this.discList.addAll(headerMap.get(ca));
            }

            this.controlActionTypeScales = new int[headerMap.keySet().size()];
            int iter = 0;
            int c = 0;
            for (EppGroupTypeFCA ca : headerMap.keySet()) {
                this.controlActionTypeScales[(iter++)] = ((List<EppRegistryElementPart>) headerMap.get(ca)).size();
                c += ((List<EppRegistryElementPart>) headerMap.get(ca)).size();
            }

            this.scales = new int[c];
            Arrays.fill(this.scales, 1);

            Set<EducationLevelsHighSchool> eduLevels = new HashSet<>();
            for (ISessionSummaryBulletinStudent student : data.getStudents())
                eduLevels.add(student.getEduOu().getEducationLevelHighSchool());

            List<EducationLevelsHighSchool> eduLevelList = new ArrayList<>(eduLevels);
            Collections.sort(eduLevelList, new EntityComparator<>(new EntityOrder("displayableTitle", OrderDirection.asc)));

            List<ISessionSummaryBulletinStudent> students = new ArrayList<>(data.getStudents());
            Collections.sort(students, (so1, so2) -> {
                ISessionSummaryBulletinStudent s1 = (ISessionSummaryBulletinStudent) so1;
                ISessionSummaryBulletinStudent s2 = (ISessionSummaryBulletinStudent) so2;

                int i = 0;
                if (i == 0)
                    i = s1.getStudent().getPerson().getFullFio().compareTo(s2.getStudent().getPerson().getFullFio());
                if (i == 0) i = s1.getStudent().getId().compareTo(s2.getStudent().getId());
                return i;
            });
            int studentNumber = 1;
            int rowNumber = 1;
            List<String[]> tableData = new ArrayList<>();
            this._groupInactiveRows = new HashSet<>();
            Map<ISessionSummaryBulletinAction, MutableInt> positiveMarksTotalbyCA = SafeMap.get(MutableInt.class);
            int[] negativeMark = new int[this.actions.size()];
            for (EducationLevelsHighSchool eduLevel : eduLevelList) {
                if (eduLevelList.size() > 1) {
                    tableData.add(new String[]{eduLevel.getTitle()});
                    rowNumber++;
                }

                for (ISessionSummaryBulletinStudent student : students) {
                    if (!eduLevel.getId().equals(student.getEduOu().getEducationLevelHighSchool().getId()))
                        continue;
                    if (!student.getStudent().getStatus().isActive())
                        this._groupInactiveRows.add(rowNumber);
                    rowNumber++;

                    List<String> row = new ArrayList<>();

                    row.addAll(Arrays.asList(printStudentData(studentNumber++, student)));

//					int positiveMarksTotal = 0;

                    for (ISessionSummaryBulletinAction action : this.actions) {
                        EppWorkPlanRowKind obligation = data.getObligation(student, action);

                        if (null == obligation) {
                            row.add("-");
                            continue;
                        }

                        //getList(SessionRetakeDocument.class,SessionRetakeDocument.sessionObject().s)
                        student.getStudent();

                        SessionMark sessionMark = data.getMark(student, action);
                        if (sessionMark == null) {
                            row.add("");
                            continue;
                        }

                        SessionSlotRegularMark regMark = null;
                        if (sessionMark instanceof SessionSlotLinkMark)
                            regMark = ((SessionSlotLinkMark) sessionMark).getTarget();


                        if (regMark != null) {
                            if (regMark.getSlot().getDocument() instanceof SessionRetakeDocument || !regMark.isInSession())
                                row.add(RETAKE + "," + sessionMark.getValueShortTitle() + " " + regMark.getSlot().getDocument().getNumber());
                            else if (regMark.getSlot().getDocument() instanceof SessionListDocument)
                                row.add(RETAKE + "," + sessionMark.getValueShortTitle());
                            else
                                row.add(sessionMark.getValueShortTitle());
                        }
                        else {
                            row.add(sessionMark.getValueShortTitle());
                        }


                        if ((sessionMark.getValueItem() instanceof SessionMarkGradeValueCatalogItem)) {
                            SessionMarkGradeValueCatalogItem markValue = (SessionMarkGradeValueCatalogItem) sessionMark.getValueItem();
                            if (markValue.isPositive()) {
//								positiveMarksTotal++;
                                ((MutableInt) positiveMarksTotalbyCA.get(action)).increment();
                            }
                            else
                                negativeMark[this.actions.indexOf(action)]++;

                        }
                        if ((sessionMark.getValueItem() instanceof SessionMarkStateCatalogItem)) {
                            SessionMarkStateCatalogItem markValue = (SessionMarkStateCatalogItem) sessionMark.getValueItem();
                            if (!markValue.isCachedPositiveStatus())
                                negativeMark[this.actions.indexOf(action)]++;
                        }
                    }

/*					if (printTotals()) {
						row.add(String.valueOf(positiveMarksTotal));
					}*/
                    tableData.add(row.toArray(new String[row.size()]));
                }
				
				
/*				if (printTotals())
				{
					List row = new ArrayList();
					row.addAll(Arrays.asList(new String[] { "", "", "" }));
					for (ISessionSummaryBulletinAction action : this.actions)
						row.add(String.valueOf(((MutableInt)positiveMarksTotalbyCA.get(action)).intValue()));
					tableData.add(row.toArray(new String[row.size()]));
				}*/
            }

            List<String> totalNegativeRow = new ArrayList<>();
            totalNegativeRow.add("");
            totalNegativeRow.add("Не сдали всего");
            totalNegativeRow.add("");
            totalNegativeRow.add("");

            for (int mark : negativeMark)
                totalNegativeRow.add(mark == 0 ? "" : "" + mark);
            tableData.add(totalNegativeRow.toArray(new String[totalNegativeRow.size()]));

            this._tableData = tableData.toArray(new String[tableData.size()][]);
        }
    }

    private static class SessionSummaryBulletinAction extends UniBaseUtils.KeyBase<MultiKey>
            implements ISessionSummaryBulletinAction
    {
        private EppGroupTypeFCA controlActionType;
        private EppRegistryElementPart discipline;
        private Set<SessionDocument> bulletins = new HashSet<>();

        private SessionSummaryBulletinAction(EppGroupTypeFCA controlActionType, EppRegistryElementPart discipline)
        {
            this.controlActionType = controlActionType;
            this.discipline = discipline;
        }

        private static MultiKey key(EppGroupTypeFCA controlActionType, EppRegistryElementPart discipline)
        {
            return new MultiKey(controlActionType, discipline);
        }

        public EppGroupTypeFCA getControlActionType()
        {
            return this.controlActionType;
        }

        public EppRegistryElementPart getDiscipline()
        {
            return this.discipline;
        }

        public Set<SessionDocument> getBulletins()
        {
            return this.bulletins;
        }

        public MultiKey key()
        {
            return key(this.controlActionType, this.discipline);
        }
    }

    private static class SessionSummaryBulletinStudent extends UniBaseUtils.KeyBase<Student>
            implements ISessionSummaryBulletinStudent
    {
        private EppRealEduGroupRow row;

        private SessionSummaryBulletinStudent(EppRealEduGroupRow row)
        {
            this.row = row;
        }

        public Student getStudent()
        {
            return this.row.getStudentWpePart().getStudentWpe().getStudent();
        }

        public EducationOrgUnit getEduOu()
        {
            return this.row.getStudentEducationOrgUnit();
        }

        public Course getCourse()
        {
            return this.row.getStudentWpePart().getStudentWpe().getCourse();
        }

        public Term getTerm()
        {
            return this.row.getStudentWpePart().getStudentWpe().getTerm();
        }

        public Student key()
        {
            return getStudent();
        }
    }

    private static class SessionSummaryBulletinData
            implements ISessionSummaryBulletinData
    {
        private String group;
        protected Map<MultiKey, EppWorkPlanRowKind> obligationMap = new HashMap<>();
        protected Map<MultiKey, SessionMark> markMap = new HashMap<>();
        protected Set<ISessionSummaryBulletinStudent> students = new HashSet<>();
        protected Map<MultiKey, ISessionSummaryBulletinAction> actions = new HashMap<>();

        private SessionSummaryBulletinData(String group)
        {
            this.group = group;
        }

        public String getGroup()
        {
            return this.group;
        }

        public Set<ISessionSummaryBulletinStudent> getStudents()
        {
            return this.students;
        }

        public Collection<ISessionSummaryBulletinAction> getActions()
        {
            return this.actions.values();
        }

        public SessionMark getMark(ISessionSummaryBulletinStudent student, ISessionSummaryBulletinAction action)
        {
            return this.markMap.get(key(student, action));
        }

        public EppWorkPlanRowKind getObligation(ISessionSummaryBulletinStudent student, ISessionSummaryBulletinAction action)
        {
            return obligationMap.get(key(student, action));
        }

        protected MultiKey key(ISessionSummaryBulletinStudent student, ISessionSummaryBulletinAction action)
        {
            return new MultiKey(student.getStudent().getId(), action.getControlActionType().getId(), action.getDiscipline().getId());
        }


    }
}
