package ru.tandemservice.narfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.narfu.entity.NarfuReportJournalAcademVacations;
import ru.tandemservice.uni.entity.report.StorableReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Отчет «Журнал учета Академических отпусков»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class NarfuReportJournalAcademVacationsGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.narfu.entity.NarfuReportJournalAcademVacations";
    public static final String ENTITY_NAME = "narfuReportJournalAcademVacations";
    public static final int VERSION_HASH = 760407345;
    private static IEntityMeta ENTITY_META;

    public static final String P_TITLE = "title";
    public static final String P_NUMBER = "number";
    public static final String L_ORG_UNIT = "orgUnit";

    private String _title;     // Название отчета
    private Integer _number;     // Номер отчета
    private OrgUnit _orgUnit;     // Формирующее подразделение

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Название отчета.
     */
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название отчета.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Номер отчета.
     */
    public Integer getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер отчета.
     */
    public void setNumber(Integer number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Формирующее подразделение.
     */
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Формирующее подразделение.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof NarfuReportJournalAcademVacationsGen)
        {
            setTitle(((NarfuReportJournalAcademVacations)another).getTitle());
            setNumber(((NarfuReportJournalAcademVacations)another).getNumber());
            setOrgUnit(((NarfuReportJournalAcademVacations)another).getOrgUnit());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends NarfuReportJournalAcademVacationsGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) NarfuReportJournalAcademVacations.class;
        }

        public T newInstance()
        {
            return (T) new NarfuReportJournalAcademVacations();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "title":
                    return obj.getTitle();
                case "number":
                    return obj.getNumber();
                case "orgUnit":
                    return obj.getOrgUnit();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "title":
                        return true;
                case "number":
                        return true;
                case "orgUnit":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "title":
                    return true;
                case "number":
                    return true;
                case "orgUnit":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "title":
                    return String.class;
                case "number":
                    return Integer.class;
                case "orgUnit":
                    return OrgUnit.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<NarfuReportJournalAcademVacations> _dslPath = new Path<NarfuReportJournalAcademVacations>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "NarfuReportJournalAcademVacations");
    }
            

    /**
     * @return Название отчета.
     * @see ru.tandemservice.narfu.entity.NarfuReportJournalAcademVacations#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Номер отчета.
     * @see ru.tandemservice.narfu.entity.NarfuReportJournalAcademVacations#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.narfu.entity.NarfuReportJournalAcademVacations#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    public static class Path<E extends NarfuReportJournalAcademVacations> extends StorableReport.Path<E>
    {
        private PropertyPath<String> _title;
        private PropertyPath<Integer> _number;
        private OrgUnit.Path<OrgUnit> _orgUnit;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Название отчета.
     * @see ru.tandemservice.narfu.entity.NarfuReportJournalAcademVacations#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(NarfuReportJournalAcademVacationsGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Номер отчета.
     * @see ru.tandemservice.narfu.entity.NarfuReportJournalAcademVacations#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(NarfuReportJournalAcademVacationsGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.narfu.entity.NarfuReportJournalAcademVacations#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

        public Class getEntityClass()
        {
            return NarfuReportJournalAcademVacations.class;
        }

        public String getEntityName()
        {
            return "narfuReportJournalAcademVacations";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
