package ru.tandemservice.narfu.eventssecrmc.events;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudentrmc.entity.PersonNARFU;
import ru.tandemservice.narfu.entity.PersonEduInstitutionNARFU;
import ru.tandemservice.narfu.entity.PersonNextOfKinNARFU;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class DaoEvents
        extends ru.tandemservice.eventssecrmc.events.DaoEvents
{

    @Override
    protected List<String> initEntityTypeList()
    {
        List<String> retVal = super.initEntityTypeList();

        retVal.add(PersonNARFU.ENTITY_NAME);
        retVal.add(PersonNextOfKinNARFU.ENTITY_NAME);
        retVal.add(PersonEduInstitutionNARFU.ENTITY_NAME);

        return retVal;
    }

    @Override
    protected void fillByPersonExt(Map<String, Set<Long>> map)
    {
        fillByPersonNARFU(map);

        fillByPersonNextOfKinNARFU(map);

        fillByPersonEduInstitutionNARFU(map);

    }

    private void fillByPersonEduInstitutionNARFU(Map<String, Set<Long>> map) {
        //PersonEduInstitutionNARFU
        Set<Long> findIds = getSaved(map, PersonEduInstitutionNARFU.ENTITY_NAME);
        if (findIds.isEmpty())
            return;

        DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(PersonEduInstitutionNARFU.class, "ein")
                .where(DQLExpressions.in(DQLExpressions.property(PersonEduInstitutionNARFU.id().fromAlias("ein")), findIds))
                .column(PersonEduInstitutionNARFU.base().person().id().fromAlias("ein").s());

        List<Long> list = dqlIn.createStatement(getSession()).list();
        getSaved(map, Person.ENTITY_NAME).addAll(list);
    }

    private void fillByPersonNextOfKinNARFU(Map<String, Set<Long>> map) {
        // PersonNextOfKinNARFU
        Set<Long> findIds = getSaved(map, PersonNextOfKinNARFU.ENTITY_NAME);
        if (findIds.isEmpty())
            return;

        DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(PersonNextOfKinNARFU.class, "ein")
                .where(DQLExpressions.in(DQLExpressions.property(PersonNextOfKinNARFU.id().fromAlias("ein")), findIds))
                .column(PersonNextOfKinNARFU.base().person().id().fromAlias("ein").s());

        List<Long> list = dqlIn.createStatement(getSession()).list();
        getSaved(map, Person.ENTITY_NAME).addAll(list);
    }

    private void fillByPersonNARFU(Map<String, Set<Long>> map) {
        // PersonNARFU
        Set<Long> findIds = getSaved(map, PersonNARFU.ENTITY_NAME);
        if (findIds.isEmpty())
            return;

        DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(PersonNARFU.class, "ein")
                .where(DQLExpressions.in(DQLExpressions.property(PersonNARFU.id().fromAlias("ein")), findIds))
                .column(PersonNARFU.person().id().fromAlias("ein").s());

        List<Long> list = dqlIn.createStatement(getSession()).list();
        getSaved(map, Person.ENTITY_NAME).addAll(list);

    }
}
