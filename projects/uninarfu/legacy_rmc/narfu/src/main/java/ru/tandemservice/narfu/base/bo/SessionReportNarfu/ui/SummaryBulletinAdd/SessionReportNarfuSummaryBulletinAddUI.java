package ru.tandemservice.narfu.base.bo.SessionReportNarfu.ui.SummaryBulletinAdd;

import com.google.common.collect.ImmutableList;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.narfu.base.bo.SessionReportNarfu.logic.ISessionSummaryBulletinNarfuPrintDAO;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionTypeRow;
import ru.tandemservice.uniepp.entity.student.group.gen.EppRealEduGroup4ActionTypeRowGen;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.util.ISessionReportGroupFilterParams;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

@Input({@org.tandemframework.core.component.Bind(key = "orgUnitId", binding = "ouHolder.id", required = true)})
public class SessionReportNarfuSummaryBulletinAddUI extends UIPresenter
{
    private static IdentifiableWrapper noGroup = new IdentifiableWrapper(-9223372036854775808L, "вне групп");

    private OrgUnitHolder ouHolder = new OrgUnitHolder();
    private ISelectModel groupModel;
    private EducationYear year;
    private YearDistributionPart part;
    private DataWrapper inSession;
    private List<EppWorkPlanRowKind> discObligations;
    private Course course;
    private List<IdentifiableWrapper> groups;

    public void onComponentActivate()
    {
        getOuHolder().refresh();

        setDiscObligations(ImmutableList.of(IUniBaseDao.instance.get().getCatalogItem(EppWorkPlanRowKind.class, "1")));
        setInSession(SessionReportManager.instance().getResultDefaultOption());
    }

    public void onComponentRefresh()
    {
        getOuHolder().refresh();

        if (null == getYear()) {
            setYear(IUniBaseDao.instance.get().get(EducationYear.class, "current", Boolean.TRUE));
        }
        List<IdentifiableWrapper> groups = getWrappedGroupList(getGroupList());
        setGroupModel(new LazySimpleSelectModel<>(groups));
    }

    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put("orgUnit", getOrgUnit());
        dataSource.put("eduYear", getYear());
    }

    private List<String> getGroupList()
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(SessionBulletinDocument.class, "b")
                .where(eq(property(SessionBulletinDocument.sessionObject().orgUnit().fromAlias("b")), value(getOrgUnit())))
                .where(eq(property(SessionBulletinDocument.sessionObject().educationYear().fromAlias("b")), value(getYear())))
                .where(eq(property(SessionBulletinDocument.sessionObject().yearDistributionPart().fromAlias("b")), value(getPart())));

        dql.joinEntity("b", DQLJoinType.left, SessionDocumentSlot.class, "s", eq(property(SessionDocumentSlot.document().fromAlias("s")), property("b")));

        if (getCourse() != null)
        {
            dql.where(eq(property(SessionDocumentSlot.studentWpeCAction().studentWpe().course().fromAlias("s")), value(getCourse())));
        }

        dql.joinEntity("s", DQLJoinType.inner, EppRealEduGroup4ActionTypeRow.class, "rel", and(
                isNull(property(EppRealEduGroup4ActionTypeRow.removalDate().fromAlias("rel"))),
                eq(property(EppRealEduGroup4ActionTypeRow.studentWpePart().fromAlias("rel")), property(SessionDocumentSlot.studentWpeCAction().id().fromAlias("s")))
        ));

        dql.predicate(DQLPredicateType.distinct);
        dql.column(property(EppRealEduGroup4ActionTypeRowGen.studentGroupTitle().fromAlias("rel")));
        dql.order(property(EppRealEduGroup4ActionTypeRowGen.studentGroupTitle().fromAlias("rel")));
        dql.where(isNull(property(EppRealEduGroup4ActionTypeRowGen.removalDate().fromAlias("rel"))));
        return dql.createStatement(this._uiSupport.getSession()).list();
    }

    private List<IdentifiableWrapper> getWrappedGroupList(List<String> groups)
    {
        final List<IdentifiableWrapper> result = new ArrayList<>();
        if (groups != null)
        {
            final long[] index = {0};
            result.addAll(groups.stream()
                                  .map(group -> {
                                      if (group == null || group.equals(""))
                                          return noGroup;

                                      return new IdentifiableWrapper(index[0]++, group);
                                  })
                                  .collect(Collectors.toList()));
        }
        return result;
    }

    public void onClickApply()
    {
        //ISessionSummaryBulletinPrintDAO dao = (ISessionSummaryBulletinPrintDAO)ISessionSummaryBulletinPrintDAO.instance.get();
        ISessionSummaryBulletinNarfuPrintDAO dao = ISessionSummaryBulletinNarfuPrintDAO.instance.get();
        dao.createStoredReport(formReportParams());
        deactivate();
    }

    public void doNothing()
    {
    }


    protected ISessionSummaryBulletinNarfuPrintDAO.ISessionSummaryBulletinParams formReportParams()
    {
        return new ISessionSummaryBulletinNarfuPrintDAO.ISessionSummaryBulletinParams()
        {
            public SessionObject getSessionObject()
            {
                DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(SessionObject.class, "obj")
                        .column("obj")
                        .fetchPath(DQLJoinType.inner, SessionObject.orgUnit().fromAlias("obj"), "ou")
                        .fetchPath(DQLJoinType.inner, SessionObject.educationYear().fromAlias("obj"), "ey")
                        .fetchPath(DQLJoinType.inner, SessionObject.yearDistributionPart().fromAlias("obj"), "p")
                        .where(eq(property(SessionObject.orgUnit().fromAlias("obj")), value(SessionReportNarfuSummaryBulletinAddUI.this.getOrgUnit())))
                        .where(eq(property(SessionObject.yearDistributionPart().fromAlias("obj")), value(SessionReportNarfuSummaryBulletinAddUI.this.part)))
                        .where(eq(property(SessionObject.educationYear().fromAlias("obj")), value(SessionReportNarfuSummaryBulletinAddUI.this.year)))
                        .order(property(SessionObject.id().fromAlias("obj")));

                List<SessionObject> list = dql.createStatement(SessionReportNarfuSummaryBulletinAddUI.this._uiSupport.getSession()).list();
                if (list.isEmpty())
                    return null;
                if (list.size() != 1)
                    throw new IllegalStateException();
                return list.get(0);
            }

            public boolean isInSessionMarksOnly()
            {
                return 0L == SessionReportNarfuSummaryBulletinAddUI.this.getInSession().getId();
            }

            public Collection<EppWorkPlanRowKind> getObligations()
            {
                return SessionReportNarfuSummaryBulletinAddUI.this.getDiscObligations();
            }

            public Course getCourse()
            {
                return SessionReportNarfuSummaryBulletinAddUI.this.getCourse();
            }

            public ISessionReportGroupFilterParams getGroupFilterParams()
            {
                final List<IdentifiableWrapper> choiceGroups = new ArrayList<>(SessionReportNarfuSummaryBulletinAddUI.this.getGroups());
                if(choiceGroups.isEmpty())
                    choiceGroups.addAll(getWrappedGroupList(getGroupList()));
                return new ISessionReportGroupFilterParams()
                {
                    public Collection<String> getGroups()
                    {
                        return choiceGroups.stream()
                                .filter(group -> !SessionReportNarfuSummaryBulletinAddUI.noGroup.equals(group))
                                .map(IdentifiableWrapper::getTitle)
                                .collect(Collectors.toList());
                    }

                    public boolean isIncludeStudentsWithoutGroups()
                    {
                        return choiceGroups.contains(SessionReportNarfuSummaryBulletinAddUI.noGroup);
                    }
                };
            }
        };
    }

    public List<EppWorkPlanRowKind> getDiscObligations()
    {
        return this.discObligations;
    }

    public void setDiscObligations(List<EppWorkPlanRowKind> discObligations)
    {
        this.discObligations = discObligations;
    }

    public ISelectModel getGroupModel()
    {
        return this.groupModel;
    }

    public void setGroupModel(ISelectModel groupModel)
    {
        this.groupModel = groupModel;
    }

    public EducationYear getYear()
    {
        return this.year;
    }

    public void setYear(EducationYear year)
    {
        this.year = year;
    }

    public YearDistributionPart getPart()
    {
        return this.part;
    }

    public void setPart(YearDistributionPart part)
    {
        this.part = part;
    }

    public DataWrapper getInSession()
    {
        return this.inSession;
    }

    public void setInSession(DataWrapper inSession)
    {
        this.inSession = inSession;
    }

    public Course getCourse()
    {
        return this.course;
    }

    public void setCourse(Course course)
    {
        this.course = course;
    }

    public List<IdentifiableWrapper> getGroups()
    {
        return this.groups;
    }

    public void setGroups(List<IdentifiableWrapper> groups)
    {
        this.groups = groups;
    }

    public OrgUnitHolder getOuHolder()
    {
        return this.ouHolder;
    }

    public OrgUnit getOrgUnit()
    {
        return getOuHolder().getValue();
    }
}