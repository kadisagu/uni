package ru.tandemservice.narfu.component.person.AddressTempRegistrationEdit;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.movestudentrmc.entity.PersonNARFU;


public class DAO extends org.tandemframework.shared.fias.base.bo.Fias.ui.AddressEdit.DAO implements IDAO {

    @Override
    public void prepare(org.tandemframework.shared.fias.base.bo.Fias.ui.AddressEdit.Model model) {
        super.prepare(model);
        Model myModel = (Model) model;

        PersonNARFU personNARFU = (PersonNARFU) model.getEntity();

        myModel.setAddressTempRegistrationStartDate(personNARFU.getAddressTempRegistrationStartDate());
        myModel.setAddressTempRegistrationEndDate(personNARFU.getAddressTempRegistrationEndDate());
        myModel.setAddressTempRegistrationInfo(personNARFU.getAddressTempRegistrationInfo());


    }

    @Override
    public void update(
            org.tandemframework.shared.fias.base.bo.Fias.ui.AddressEdit.Model model)
    {
        Model myModel = (Model) model;

        validate(myModel);

        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();
        if (errorCollector.hasErrors()) {
            return;
        }

        super.update(model);

        PersonNARFU personNARFU = (PersonNARFU) model.getEntity();
        personNARFU.setAddressTempRegistrationStartDate(myModel.getAddressTempRegistrationStartDate());
        personNARFU.setAddressTempRegistrationEndDate(myModel.getAddressTempRegistrationEndDate());
        personNARFU.setAddressTempRegistrationInfo(myModel.getAddressTempRegistrationInfo());

        update(model.getEntity());
    }

    private void validate(Model model)
    {
        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();

        if ((model.getAddressTempRegistrationStartDate() != null) && (model.getAddressTempRegistrationEndDate() != null) &&
                (model.getAddressTempRegistrationStartDate().getTime() > model.getAddressTempRegistrationEndDate().getTime()))
        {
            errorCollector.add("Дата начала регистрации должна быть меньше даты окончания.", new String[]{"addressTempRegistrationStartDate", "addressTempRegistrationEndDate"});
        }
        if ((model.getAddressTempRegistrationStartDate() != null) && (model.getAddressTempRegistrationEndDate() == null)) {
            errorCollector.add("Если указана дата окончания регистрации, то должна быть указана дата начала регистрации.", new String[]{"addressTempRegistrationStartDate", "addressTempRegistrationEndDate"});
        }
    }


}
