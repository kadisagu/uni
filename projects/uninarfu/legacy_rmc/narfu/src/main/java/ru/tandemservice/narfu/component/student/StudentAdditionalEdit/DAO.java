package ru.tandemservice.narfu.component.student.StudentAdditionalEdit;

import ru.tandemservice.movestudentrmc.entity.StudentVKRTheme;

public class DAO extends ru.tandemservice.unibasermc.component.student.StudentAdditionalEdit.DAO
{
    @Override
    public void prepare(ru.tandemservice.uni.component.student.StudentAdditionalEdit.Model model) {
        super.prepare(model);
        Model myModel = (Model) model;
        StudentVKRTheme theme = get(StudentVKRTheme.class, StudentVKRTheme.student(), model.getStudent());
        if (theme == null) {
            theme = new StudentVKRTheme();
            theme.setStudent(model.getStudent());
        }
        myModel.setTheme(theme);
    }

    @Override
    public void update(ru.tandemservice.uni.component.student.StudentAdditionalEdit.Model model) {
        Model myModel = (Model) model;
        StudentVKRTheme entity = myModel.getTheme();
        model.getStudent().setFinalQualifyingWorkTheme(entity.getTheme());
        super.update(model);
        getSession().saveOrUpdate(entity);

    }
}
