package ru.tandemservice.narfu.component.catalog.DefaultPrintCatalogAddEdit;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultPrintCatalogAddEdit.DefaultPrintCatalogAddEditModel;


public class DefaultPrintCatalogAddEditDAO<T extends ICatalogItem & ITemplateDocument, Model extends DefaultPrintCatalogAddEditModel<T>>
        extends org.tandemframework.shared.commonbase.component.catalog.base.DefaultPrintCatalogAddEdit.DefaultPrintCatalogAddEditDAO<T, Model>
{

    @Override
    public void validate(Model model, ErrorCollector errors) {
        //Нужно достать метод из класса-"дедушки"
        DefaultCatalogAddEditDAO dao = new DefaultCatalogAddEditDAO<>();
        dao.validate(model, errors);

        if (model.getUploadFile() != null) {
            final T template = model.getCatalogItem();
            final String path = template.getPath().toLowerCase();

            if (model.getUploadFile().getSize() > (1024 * 1024)) {
                throw new ApplicationException("Размер загружаемого файла не должен превышать 1024Kb.");
            }

            if (path.endsWith(".rtf")) {
                if (!model.getUploadFile().getFileName().endsWith(".rtf")) {
                    errors.add("Файл шаблона должен быть в формате .rtf", "template");
                }
            }
            else if (path.endsWith(".xls")) {
                if (!model.getUploadFile().getFileName().endsWith(".xls")) {
                    errors.add("Файл шаблона должен быть в формате .xls", "template");
                }
            }
        }
    }
}