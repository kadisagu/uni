package ru.tandemservice.narfu.component.edugroup.list.GroupList;

import ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel;

public class Model extends
        ru.tandemservice.uniepp.component.edugroup.list.GroupList.Model
{
    @Override
    public EppRealEduGroupCompleteLevel getCurLevel() {
        // TODO Auto-generated method stub
        EppRealEduGroupCompleteLevel compLevel = super.getCurLevel();
        if ("Деканат".equalsIgnoreCase(compLevel.getTitle())) {
            compLevel.setTitle("Директорат");
            compLevel.setShortTitle("Дир.");
        }
        return super.getCurLevel();
    }
}
