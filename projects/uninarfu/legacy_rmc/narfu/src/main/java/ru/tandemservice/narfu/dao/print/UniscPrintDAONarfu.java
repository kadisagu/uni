package ru.tandemservice.narfu.dao.print;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.NumberSpellingUtil;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes;
import ru.tandemservice.narfu.entity.PersonNextOfKinNARFU;
import ru.tandemservice.uni.dao.grid.IDevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;
import ru.tandemservice.unisc.dao.print.UniscPrintDAO;
import ru.tandemservice.unisc.entity.agreements.*;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlanRow;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class UniscPrintDAONarfu extends UniscPrintDAO {

    private static final String SEVERODVINK = "г. Северодвинске";
    private static final String KORIAZHMA = "г. Коряжме";

    private static final Map<String, Object> POSTFIX = new UniMap()
            .add(SEVERODVINK, "_svrdvinks")
            .add(KORIAZHMA, "_koriazhma");

    @Override
    public RtfInjectModifier getRtfInjectorModifier(UniscEduAgreementBase agreementBase, Person person) {
        if (agreementBase instanceof UniscEduAdditAgreement)
            return super.getRtfInjectorModifier(agreementBase, person);

        UniscEduMainAgreement mainAgreement = (UniscEduMainAgreement) agreementBase;
        RtfInjectModifier im = new RtfInjectModifier();

        im.put("date", RussianDateFormatUtils.getDateFormattedWithMonthName(new Date()));
        im.put("date1", RussianDateFormatUtils.getDateFormattedWithMonthName(new Date()));

        String str = getPostfix(mainAgreement.getConfig().getEducationOrgUnit().getFormativeOrgUnit());
        if (str.isEmpty())
            str = getPostfix(mainAgreement.getConfig().getEducationOrgUnit().getFormativeOrgUnit().getParent());
        if (str.equals("_svrdvinks"))
            str = "г. Северодвинск";
        else if (str.equals("_koriazhma"))
            str = "г. Коряжма";
        else
            str = "г. Архангельск";

        im.put("TO", str);

        Object customerObject = getCustomer(mainAgreement);
        patchCustomer(mainAgreement, person, customerObject, im);

        if (mainAgreement.getConfig().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().getTitle().contains("профиль")) {
            im.put("Napr0", mainAgreement.getConfig().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getParentLevel().getTitle() + ", ");
        }
        else
            im.put("Napr0", "");
        im.put("Napr", mainAgreement.getConfig().getEducationOrgUnit().getEducationLevelHighSchool().getFullTitle());

        im.put("FO", PersonManager.instance().declinationDao().getDeclinationLastName(mainAgreement.getConfig().getEducationOrgUnit().getDevelopForm().getTitle(), GrammaCase.DATIVE, false));
        im.put("SO", mainAgreement.getConfig().getEducationOrgUnit().getDevelopPeriod().getTitle());

        //семестры
        List<DevelopGrid> list = getList(DevelopGrid.class, DevelopGrid.developPeriod().s(), mainAgreement.getConfig().getEducationOrgUnit().getDevelopPeriod());
        int semCount = mainAgreement.getConfig().getEducationOrgUnit().getDevelopPeriod().getLastCourse() * 2;
        if (list != null && !list.isEmpty()) {
            semCount = 0;
            Map<Course, Integer[]> semestrMap = IDevelopGridDAO.instance.get().getDevelopGridDetail(list.get(0));
            for (Integer[] arr : semestrMap.values())
                for (int i = 0; i < arr.length; i++)
                    if (arr[i] != null && arr[i] > semCount) semCount = arr[i];
            //semCount += arr.length;
        }
        str = "" + semCount + " семестр";
        if (semCount == 1)
            str += "";
        else if (semCount < 5)
            str += "а";
        else
            str += "ов";
        im.put("Sem", str);

        //блок студента
        im.put("FIO", person.getFullFio());
        im.put("FIOr", PersonManager.instance().declinationDao().getDeclinationFIO(person.getIdentityCard(), GrammaCase.ACCUSATIVE));

        im.put("AdrR", person.getIdentityCard().getAddress() == null ? "" : person.getIdentityCard().getAddress().getTitleWithFlat());
        im.put("Pass", person.getIdentityCard().getFullNumber());
        im.put("podrCode", "");
        if (person.getIdentityCard().getIssuanceCode() != null)
            im.put("podrCode", "код подразделения " + StringUtils.trimToEmpty(person.getIdentityCard().getIssuanceCode()));
        im.put("VidPass", person.getIdentityCard().getIssuancePlace() == null ? "" : person.getIdentityCard().getIssuancePlace());
        im.put("datePass", person.getIdentityCard().getIssuanceDate() == null ? "" : RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(person.getIdentityCard().getIssuanceDate()));
        im.put("placeR", person.getWorkPlace() == null ? "" : person.getWorkPlace());


        im.put("INN", person.getInnNumber() == null ? "" : person.getInnNumber());
        im.put("PensS", person.getSnilsNumber() == null ? "" : person.getSnilsNumber());

        if (customerObject instanceof UniscEduAgreementNaturalPerson &&
                person.getFullFio().equals(((UniscEduAgreementNaturalPerson) customerObject).getFullFio()))
        {

            im.put("PensSz", "Св-во пенс. страхования: " + im.getStringValue("PensS"));
        }

        if (customerObject instanceof UniscEduAgreementNaturalPerson &&
                person.getFullFio().equals(((UniscEduAgreementNaturalPerson) customerObject).getFullFio()))
        {
            im.put("AdrRz", person.getIdentityCard().getAddress() == null ? "" : person.getIdentityCard().getAddress().getTitleWithFlat());
        }

        im.put("FIOk", person.getIdentityCard().getIof());

        List<UniscEduAgreementPayPlanRow> rows = ((IUniscEduAgreementDAO) IUniscEduAgreementDAO.INSTANCE.get()).getAgreementPayPlanRows(mainAgreement, false);
        if (rows != null && !rows.isEmpty()) {
            im.put("Sum", rows.get(0).getCostAsString());

            int rub = new Double(rows.get(0).getCostAsDouble()).intValue();
            int kop = new Double((rows.get(0).getCostAsDouble() - rub) * 100.0).intValue();

            StringBuilder sb = new StringBuilder()
                    .append(new Double(rows.get(0).getCostAsDouble()).intValue())
                    .append(" (")
                    .append(StringUtils.capitalize(NumberSpellingUtil.spellNumberMasculineGender(rub)))
                    .append(") рублей");
            im.put("Sum", sb.toString());
        }
        else
            im.put("Sum", "");

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(UniscEduOrgUnitPayPlanRow.class, "e");
        dql.where(DQLExpressions.eq(DQLExpressions.property(UniscEduOrgUnitPayPlanRow.plan().uniscEduOrgUnit().educationYear().fromAlias("e")), DQLExpressions.value(mainAgreement.getConfig().getEducationYear())));
        dql.where(DQLExpressions.eq(DQLExpressions.property(UniscEduOrgUnitPayPlanRow.plan().uniscEduOrgUnit().educationOrgUnit().fromAlias("e")), DQLExpressions.value(mainAgreement.getConfig().getEducationOrgUnit())));
        dql.where(DQLExpressions.eq(DQLExpressions.property(UniscEduOrgUnitPayPlanRow.plan().uniscPayFreq().code().fromAlias("e")), DQLExpressions.value("0")));

        List<UniscEduOrgUnitPayPlanRow> plans = getList(dql);
        if (CollectionUtils.isEmpty(plans)) {
            im.put("SumTotal", "___________");
        }
        else {
            int rub = new Double(plans.get(0).getCostAsDouble()).intValue();
            int kop = new Double((plans.get(0).getCostAsDouble() - rub) * 100.0).intValue();

            StringBuilder sb = new StringBuilder()
                    .append(new Double(plans.get(0).getCostAsDouble()).intValue())
                    .append(" (")
                    .append(StringUtils.capitalize(NumberSpellingUtil.spellNumberMasculineGender(rub)))
                    .append(") рублей");
            im.put("SumTotal", sb.toString());

        }
        EducationLevelsHighSchool educationLevelsHighSchool = mainAgreement.getConfig().getEducationOrgUnit().getEducationLevelHighSchool();
        {
            String title = educationLevelsHighSchool.getDisplayableTitle();
            String shortTitle = educationLevelsHighSchool.getTitle();
            int index = title.indexOf(shortTitle);
            if (index != -1)
                title = title.substring(index);
            im.put("eduTitle", title);
            if (educationLevelsHighSchool.getEducationLevel().isProfileOrSpecialization()) {
                EducationLevels educationLevels = educationLevelsHighSchool.getEducationLevel();
                String profile = " профиль ";
                if (educationLevels.isSpecialization())
                    profile = " специализация ";
                title = educationLevels.getParentLevel().getDisplayableTitle();
                shortTitle = educationLevels.getParentLevel().getTitle();
                if (index != -1)
                    title = title.substring(index);
                title += profile + educationLevelsHighSchool.getTitle();
                im.put("eduTitle", title);
            }
        }
        EducationLevels educationLevels = educationLevelsHighSchool.getEducationLevel();

        im.put("eduLevel", "");

        if (educationLevels.getLevelType().isBachelor())
            im.put("eduLevel", "бакалавриат");
        if (educationLevels.getLevelType().isSpecialty())
            im.put("eduLevel", "специалитет");
        if (educationLevels.getLevelType().isMaster())
            im.put("eduLevel", "магистратура");
        if (educationLevels.getLevelType().isMiddle())
            im.put("eduLevel", "СПО");

        im.put("eduCode", "");
        if (educationLevels.getEduProgramSubject() != null)
            im.put("eduCode", "(" + educationLevels.getEduProgramSubject().getSubjectCode() + ")");

        im.put("nameDir", mainAgreement.getConfig().getEducationOrgUnit().getFormativeOrgUnit().getHead() == null ?
                       "" :
                       ((EmployeePost) mainAgreement.getConfig().getEducationOrgUnit().getFormativeOrgUnit().getHead()).getPerson().getIdentityCard().getIof()
        );

        return im;
    }

    private void patchCustomer(UniscEduMainAgreement mainAgreement, Person person, Object customerObject, RtfInjectModifier im) {
        im.put("PhoneZ", "");
        if (customerObject instanceof UniscEduAgreementNaturalPerson) {
            UniscEduAgreementNaturalPerson natCustomer = (UniscEduAgreementNaturalPerson) customerObject;

            Boolean sex = isContractorMale(natCustomer, person);
            if (sex == null)
                im.put("FIOzR", natCustomer.getFullFio());
            else
                im.put("FIOzR", PersonManager.instance().declinationDao().getDeclinationFIO(
                        natCustomer.getLastName(),
                        natCustomer.getFirstName(),
                        natCustomer.getMiddleName(),
                        GrammaCase.NOMINATIVE,
                        sex
                ));

            im.put("FIOz", natCustomer.getFullFio());

            if (!natCustomer.getFullFio().equals(person.getFullFio()))
                im.put("AdrRz", natCustomer.getAddress().getTitleWithFlat());

            //Паспортные данные заказчика
            StringBuilder sb = new StringBuilder()
                    .append("Паспортные данные: ")
                    .append(natCustomer.getPassportSeria() == null ? "" : natCustomer.getPassportSeria() + " ")
                    .append(natCustomer.getPassportNumber() == null ? "" : natCustomer.getPassportNumber());
            if (natCustomer.getPassportIssuancePlace() != null)
                sb.append(" выдан ").append(natCustomer.getPassportIssuancePlace()).append(" ");
            if (natCustomer.getPassportIssuanceDate() != null)
                sb.append(" от ").append(RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(natCustomer.getPassportIssuanceDate()));
            im.put("Passz", sb.toString());

            sb = new StringBuilder()
                    .append(natCustomer.getFirstName().charAt(0)).append(". ")
                    .append(natCustomer.getMiddleName() == null ? "" : (natCustomer.getMiddleName().charAt(0) + ". "))
                    .append(natCustomer.getLastName());
            im.put("FIOzk", sb.toString());

            im.put("PhoneZ", StringUtils.trimToEmpty(natCustomer.getMobilePhoneNumber()));
            im.put("Phone", StringUtils.trimToEmpty(person.getContactData().getPhoneMobile()));

            //ищем в родственниках по ФИО для вывода пенс свидетельства
            MQBuilder builder = new MQBuilder(PersonNextOfKinNARFU.ENTITY_CLASS, "p")
                    .add(MQExpression.eq("p", PersonNextOfKinNARFU.base().person(), person))
                    .add(MQExpression.eq("p", PersonNextOfKinNARFU.base().lastName(), natCustomer.getLastName()))
                    .add(MQExpression.eq("p", PersonNextOfKinNARFU.base().firstName(), natCustomer.getFirstName()))
                    .add(MQExpression.eq("p", PersonNextOfKinNARFU.base().middleName(), natCustomer.getMiddleName()));
            List<PersonNextOfKinNARFU> persons = builder.getResultList(getSession());

            if (!persons.isEmpty()) {
                im.put("PensSz", "Св-во пенс. страхования: " + (persons.get(0).getPensionInsuranceNumber() == null ? "" : persons.get(0).getPensionInsuranceNumber()));
                im.put("INNz", "ИНН: " + (persons.get(0).getINN() == null ? "" : persons.get(0).getINN()));
            }
            else {
                im.put("PensSz", "Св-во пенс. страхования: ");
                // Если заказчиком является сам абитуриент, то расширение для него может в принципе не существовать
                if (natCustomer.getRelationDegree().getCode().equals(RelationDegreeCodes.ANOTHER) && natCustomer.getFullFio().equals(person.getFullFio()))
                    im.put("INNz", "ИНН: " + (person.getInnNumber() == null ? "" : person.getInnNumber()));
                else im.put("INNz", "ИНН: ");
            }

            im.put("FIOzL", "");

        }
        else if (mainAgreement.getCustomerType().hasJuridicalPart()) {
            StringBuilder sb = new StringBuilder()
                    .append(mainAgreement.getJuridicalPerson().getContractor().getLegalForm().getShortTitle() == null ?
                                    mainAgreement.getJuridicalPerson().getContractor().getLegalForm().getTitle()
                                    : mainAgreement.getJuridicalPerson().getContractor().getLegalForm().getShortTitle())
                    .append(" " + mainAgreement.getJuridicalPerson().getContractor().getTitle());
            if (mainAgreement.getJuridicalPerson().getFullFio() != null && !mainAgreement.getJuridicalPerson().getFullFio().isEmpty())
                sb.append(" в лице ").append(mainAgreement.getJuridicalPerson().getFullFio());
            im.put("FIOz", sb.toString());

            im.put("FIOzL", mainAgreement.getJuridicalPerson().getFullFio() == null ? "" : mainAgreement.getJuridicalPerson().getFullFio());
            im.put("PhoneZ", StringUtils.trimToEmpty(mainAgreement.getJuridicalPerson().getContractor().getPhone()));
            sb = new StringBuilder()
                    .append(mainAgreement.getJuridicalPerson().getContractor().getLegalForm().getShortTitle() == null ?
                                    mainAgreement.getJuridicalPerson().getContractor().getLegalForm().getTitle()
                                    : mainAgreement.getJuridicalPerson().getContractor().getLegalForm().getShortTitle())
                    .append(" " + mainAgreement.getJuridicalPerson().getContractor().getTitle());
            im.put("FIOzR", sb.toString());

            if (mainAgreement.getJuridicalPerson().getContractor().getLegalAddress() != null)
                im.put("AdrRz", mainAgreement.getJuridicalPerson().getContractor().getLegalAddress().getTitle());
            else
                im.put("AdrRz", "");

            im.put("FIOzk", getShortFio(mainAgreement));

            sb = new StringBuilder()
                    .append("ИНН ")
                    .append(mainAgreement.getJuridicalPerson().getContractor().getInn() == null ? "" : mainAgreement.getJuridicalPerson().getContractor().getInn())
                    .append(" / ")
                    .append("КПП ")
                    .append(mainAgreement.getJuridicalPerson().getContractor().getKpp() == null ? "" : mainAgreement.getJuridicalPerson().getContractor().getKpp());
            im.put("Passz", sb.toString());

            if (mainAgreement.getJuridicalPerson().getContractor().getCurAccount() != null) {
                RtfString rtf = new RtfString()
                        .append("р/с ")
                        .append(mainAgreement.getJuridicalPerson().getContractor().getCurAccount()).append(IRtfData.PAR)
                        .append(" в БИК ")
                        .append(mainAgreement.getJuridicalPerson().getContractor().getBic() == null ? "" : mainAgreement.getJuridicalPerson().getContractor().getBic());
                im.put("PensSz", rtf);
            }
            else
                im.put("PensSz", "");

            im.put("INNz", "");

        }

    }

    private String getShortFio(UniscEduMainAgreement agreement)
    {
        StringBuilder sb = new StringBuilder();
        sb.append(agreement.getJuridicalPerson().getFirstName().substring(0, 1) + ".");
        if (agreement.getJuridicalPerson().getMiddleName() != null && !agreement.getJuridicalPerson().getMiddleName().isEmpty())
            sb.append(agreement.getJuridicalPerson().getMiddleName().substring(0, 1) + ".");
        sb.append(" " + agreement.getJuridicalPerson().getLastName());
        return sb.toString();

    }

    private Boolean isContractorMale(UniscEduAgreementNaturalPerson contractor, Person person) {
        if (person.getFullFio().equals(contractor.getFullFio()))
            return Boolean.valueOf(person.getIdentityCard().getSex().isMale());

        @SuppressWarnings("unchecked")
        List<String> female = Arrays.asList(new String[]{
                "2", //Жена
                "mother",
                "6", //Дочь
                "8", //Бабушка
                "10", //Внучка
                "sister",
                "14", //Мачеха
                "16", //Падчерица
                "18", //Теща
                "20", //Свекровь
                "22" //Невестка (сноха)
        });
        if (female.contains(contractor.getRelationDegree().getCode()))
            return Boolean.FALSE;
        else if (Arrays.asList(new String[]{"23", "24"}).contains(contractor.getRelationDegree().getCode()))
            return null;
        else
            return Boolean.TRUE;
    }

    private Object getCustomer(UniscEduMainAgreement mainAgreement) {
        if (mainAgreement.getCustomerType().hasJuridicalPart()) {
            return mainAgreement.getJuridicalPerson();
        }
        else if (mainAgreement.getCustomerType().hasNaturalPart()) {
            List<UniscEduAgreementNaturalPerson> persons = ((IUniscEduAgreementDAO) IUniscEduAgreementDAO.INSTANCE.get()).getNaturalPersons(mainAgreement);
            if (persons.isEmpty())
                return null;

            UniscEduAgreementNaturalPerson person = persons.get(0);
            for (UniscEduAgreementNaturalPerson np : persons)
                if (np.isSignInAgreement()) {
                    person = np;
                    break;
                }

            return person;
        }


        return null;
    }

    @Override
    protected String getDocumentTemplateName4MainAgreement(UniscEduMainAgreement agreementBase) {
        String postfix = getPostfix(agreementBase.getAgreement().getConfig().getEducationOrgUnit().getFormativeOrgUnit());

        if (postfix.equals(""))
            postfix = getPostfix(agreementBase.getAgreement().getConfig().getEducationOrgUnit().getFormativeOrgUnit().getParent());

        return "studentContract" + postfix;
    }

    private String getPostfix(OrgUnit orgUnit) {
        String postfix = "";
        if (orgUnit == null)
            return "";

        String title = orgUnit.getTerritorialTitle();
        for (String townStr : POSTFIX.keySet())
            if (title.contains(townStr)) {
                postfix = POSTFIX.get(townStr).toString();
                break;
            }

        return postfix;
    }

}
