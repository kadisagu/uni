package ru.tandemservice.narfu.uniec.component.report.EntrantRegistration.EntrantRegistrationAdd;

//Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
//Jad home page: http://www.kpdus.com/jad.html
//Decompiler options: packimports(3) radix(10) lradix(10) 
//Source File Name:   Model.java


import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.narfu.uniec.entity.report.NarfuEntrantRegistration;
import ru.tandemservice.uni.component.reports.YesNoUIObject;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;

import java.util.Date;
import java.util.List;

public class Model
        implements ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil.Model, IEnrollmentCampaignSelectModel
{

    public Model()
    {
        _report = new NarfuEntrantRegistration();
    }

    public List getSelectedEnrollmentCampaignList()
    {
        return MultiEnrollmentDirectionUtil.getSingletonList(getReport().getEnrollmentCampaign());
    }

    public List getSelectedFormativeOrgUnitList()
    {
        return isFormativeOrgUnitActive() ? getFormativeOrgUnitList() : null;
    }

    public List getSelectedTerritorialOrgUnitList()
    {
        return isTerritorialOrgUnitActive() ? getTerritorialOrgUnitList() : null;
    }

    public List getSelectedEducationLevelHighSchoolList()
    {
        return isEducationLevelHighSchoolActive() ? getEducationLevelHighSchoolList() : null;
    }

    public List getSelectedDevelopFormList()
    {
        return isDevelopFormActive() ? getDevelopFormList() : null;
    }

    public List getSelectedDevelopConditionList()
    {
        return isDevelopConditionActive() ? getDevelopConditionList() : null;
    }

    public List getSelectedDevelopTechList()
    {
        return isDevelopTechActive() ? getDevelopTechList() : null;
    }

    public List getSelectedDevelopPeriodList()
    {
        return isDevelopPeriodActive() ? getDevelopPeriodList() : null;
    }

    public IDataSettings getSettings()
    {
        return null;
    }

    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _report.getEnrollmentCampaign();
    }

    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _report.setEnrollmentCampaign(enrollmentCampaign);
    }

    public NarfuEntrantRegistration getReport()
    {
        return _report;
    }

    public void setReport(NarfuEntrantRegistration report)
    {
        _report = report;
    }

    public ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil.Parameters getParameters()
    {
        return _parameters;
    }

    public void setParameters(ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil.Parameters parameters)
    {
        _parameters = parameters;
    }

    public IPrincipalContext getPrincipalContext()
    {
        return _principalContext;
    }

    public void setPrincipalContext(IPrincipalContext principalContext)
    {
        _principalContext = principalContext;
    }

    public boolean isStudentCategoryActive()
    {
        return _studentCategoryActive;
    }

    public void setStudentCategoryActive(boolean studentCategoryActive)
    {
        _studentCategoryActive = studentCategoryActive;
    }

    public boolean isQualificationActive()
    {
        return _qualificationActive;
    }

    public void setQualificationActive(boolean qualificationActive)
    {
        _qualificationActive = qualificationActive;
    }

    public boolean isCompensationTypeActive()
    {
        return _compensationTypeActive;
    }

    public void setCompensationTypeActive(boolean compensationTypeActive)
    {
        _compensationTypeActive = compensationTypeActive;
    }

    public boolean isFormativeOrgUnitActive()
    {
        return _formativeOrgUnitActive;
    }

    public void setFormativeOrgUnitActive(boolean formativeOrgUnitActive)
    {
        _formativeOrgUnitActive = formativeOrgUnitActive;
    }

    public boolean isTerritorialOrgUnitActive()
    {
        return _territorialOrgUnitActive;
    }

    public void setTerritorialOrgUnitActive(boolean territorialOrgUnitActive)
    {
        _territorialOrgUnitActive = territorialOrgUnitActive;
    }

    public boolean isEducationLevelHighSchoolActive()
    {
        return _educationLevelHighSchoolActive;
    }

    public void setEducationLevelHighSchoolActive(boolean educationLevelHighSchoolActive)
    {
        _educationLevelHighSchoolActive = educationLevelHighSchoolActive;
    }

    public boolean isDevelopFormActive()
    {
        return _developFormActive;
    }

    public void setDevelopFormActive(boolean developFormActive)
    {
        _developFormActive = developFormActive;
    }

    public boolean isDevelopConditionActive()
    {
        return _developConditionActive;
    }

    public void setDevelopConditionActive(boolean developConditionActive)
    {
        _developConditionActive = developConditionActive;
    }

    public boolean isDevelopTechActive()
    {
        return _developTechActive;
    }

    public void setDevelopTechActive(boolean developTechActive)
    {
        _developTechActive = developTechActive;
    }

    public boolean isDevelopPeriodActive()
    {
        return _developPeriodActive;
    }

    public void setDevelopPeriodActive(boolean developPeriodActive)
    {
        _developPeriodActive = developPeriodActive;
    }

    public boolean isParallelActive()
    {
        return _parallelActive;
    }

    public void setParallelActive(boolean parallelActive)
    {
        _parallelActive = parallelActive;
    }

    public List getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    public void setEnrollmentCampaignList(List enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public List getEnrollmentCampaignStageList()
    {
        return _enrollmentCampaignStageList;
    }

    public void setEnrollmentCampaignStageList(List enrollmentCampaignStageList)
    {
        _enrollmentCampaignStageList = enrollmentCampaignStageList;
    }

    public ISelectModel getStudentCategoryListModel()
    {
        return _studentCategoryListModel;
    }

    public void setStudentCategoryListModel(ISelectModel studentCategoryListModel)
    {
        _studentCategoryListModel = studentCategoryListModel;
    }

    public ISelectModel getQualificationListModel()
    {
        return _qualificationListModel;
    }

    public void setQualificationListModel(ISelectModel qualificationListModel)
    {
        _qualificationListModel = qualificationListModel;
    }

    public ISelectModel getCompensationTypeListModel()
    {
        return _compensationTypeListModel;
    }

    public void setCompensationTypeListModel(ISelectModel compensationTypeListModel)
    {
        _compensationTypeListModel = compensationTypeListModel;
    }

    public ISelectModel getFormativeOrgUnitListModel()
    {
        return _formativeOrgUnitListModel;
    }

    public void setFormativeOrgUnitListModel(ISelectModel formativeOrgUnitListModel)
    {
        _formativeOrgUnitListModel = formativeOrgUnitListModel;
    }

    public ISelectModel getTerritorialOrgUnitListModel()
    {
        return _territorialOrgUnitListModel;
    }

    public void setTerritorialOrgUnitListModel(ISelectModel territorialOrgUnitListModel)
    {
        _territorialOrgUnitListModel = territorialOrgUnitListModel;
    }

    public ISelectModel getEducationLevelHighSchoolListModel()
    {
        return _educationLevelHighSchoolListModel;
    }

    public void setEducationLevelHighSchoolListModel(ISelectModel educationLevelHighSchoolListModel)
    {
        _educationLevelHighSchoolListModel = educationLevelHighSchoolListModel;
    }

    public ISelectModel getDevelopFormListModel()
    {
        return _developFormListModel;
    }

    public void setDevelopFormListModel(ISelectModel developFormListModel)
    {
        _developFormListModel = developFormListModel;
    }

    public ISelectModel getDevelopConditionListModel()
    {
        return _developConditionListModel;
    }

    public void setDevelopConditionListModel(ISelectModel developConditionListModel)
    {
        _developConditionListModel = developConditionListModel;
    }

    public ISelectModel getDevelopTechListModel()
    {
        return _developTechListModel;
    }

    public void setDevelopTechListModel(ISelectModel developTechListModel)
    {
        _developTechListModel = developTechListModel;
    }

    public ISelectModel getDevelopPeriodListModel()
    {
        return _developPeriodListModel;
    }

    public void setDevelopPeriodListModel(ISelectModel developPeriodListModel)
    {
        _developPeriodListModel = developPeriodListModel;
    }

    public List getParallelList()
    {
        return _parallelList;
    }

    public void setParallelList(List parallelList)
    {
        _parallelList = parallelList;
    }

    public IdentifiableWrapper getEnrollmentCampaignStage()
    {
        return _enrollmentCampaignStage;
    }

    public void setEnrollmentCampaignStage(IdentifiableWrapper enrollmentCampaignStage)
    {
        _enrollmentCampaignStage = enrollmentCampaignStage;
    }

    public List getStudentCategoryList()
    {
        return _studentCategoryList;
    }

    public void setStudentCategoryList(List studentCategoryList)
    {
        _studentCategoryList = studentCategoryList;
    }

    public List getQualificationList()
    {
        return _qualificationList;
    }

    public void setQualificationList(List qualificationList)
    {
        _qualificationList = qualificationList;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public List<OrgUnit> getFormativeOrgUnitList()
    {
        return _formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList)
    {
        _formativeOrgUnitList = formativeOrgUnitList;
    }

    public List getTerritorialOrgUnitList()
    {
        return _territorialOrgUnitList;
    }

    public void setTerritorialOrgUnitList(List territorialOrgUnitList)
    {
        _territorialOrgUnitList = territorialOrgUnitList;
    }

    public List getEducationLevelHighSchoolList()
    {
        return _educationLevelHighSchoolList;
    }

    public void setEducationLevelHighSchoolList(List educationLevelHighSchoolList)
    {
        _educationLevelHighSchoolList = educationLevelHighSchoolList;
    }

    public List getDevelopFormList()
    {
        return _developFormList;
    }

    public void setDevelopFormList(List developFormList)
    {
        _developFormList = developFormList;
    }

    public List getDevelopConditionList()
    {
        return _developConditionList;
    }

    public void setDevelopConditionList(List developConditionList)
    {
        _developConditionList = developConditionList;
    }

    public List getDevelopTechList()
    {
        return _developTechList;
    }

    public void setDevelopTechList(List developTechList)
    {
        _developTechList = developTechList;
    }

    public List getDevelopPeriodList()
    {
        return _developPeriodList;
    }

    public void setDevelopPeriodList(List developPeriodList)
    {
        _developPeriodList = developPeriodList;
    }

    public YesNoUIObject getParallel()
    {
        return _parallel;
    }

    public void setParallel(YesNoUIObject parallel)
    {
        _parallel = parallel;
    }

    public String getEnroll3007title()
    {
        return _enroll3007title;
    }

    public void setEnroll3007title(String enroll3007title)
    {
        _enroll3007title = enroll3007title;
    }

    public Date getEnroll3007from()
    {
        return _enroll3007from;
    }

    public void setEnroll3007from(Date enroll3007from)
    {
        _enroll3007from = enroll3007from;
    }

    public Date getEnroll3007to()
    {
        return _enroll3007to;
    }

    public void setEnroll3007to(Date enroll3007to)
    {
        _enroll3007to = enroll3007to;
    }

    public String getEnroll0508title()
    {
        return _enroll0508title;
    }

    public void setEnroll0508title(String enroll0508title)
    {
        _enroll0508title = enroll0508title;
    }

    public Date getEnroll0508from()
    {
        return _enroll0508from;
    }

    public void setEnroll0508from(Date enroll0508from)
    {
        _enroll0508from = enroll0508from;
    }

    public Date getEnroll0508to()
    {
        return _enroll0508to;
    }

    public void setEnroll0508to(Date enroll0508to)
    {
        _enroll0508to = enroll0508to;
    }

    public String getEnroll3108title()
    {
        return _enroll3108title;
    }

    public void setEnroll3108title(String enroll3108title)
    {
        _enroll3108title = enroll3108title;
    }

    public Date getEnroll3108from()
    {
        return _enroll3108from;
    }

    public void setEnroll3108from(Date enroll3108from)
    {
        _enroll3108from = enroll3108from;
    }

    public Date getEnroll3108to()
    {
        return _enroll3108to;
    }

    public void setEnroll3108to(Date enroll3108to)
    {
        _enroll3108to = enroll3108to;
    }

    static final int ENROLLMENT_CAMP_STAGE_DOCUMENTS = 0;
    static final int ENROLLMENT_CAMP_STAGE_EXAMS = 1;
    static final int ENROLLMENT_CAMP_STAGE_ENROLLMENT = 2;
    private NarfuEntrantRegistration _report;
    private ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil.Parameters _parameters;
    private IPrincipalContext _principalContext;
    private boolean _studentCategoryActive;
    private boolean _qualificationActive;
    private boolean _compensationTypeActive;
    private boolean _formativeOrgUnitActive;
    private boolean _territorialOrgUnitActive;
    private boolean _educationLevelHighSchoolActive;
    private boolean _developFormActive;
    private boolean _developConditionActive;
    private boolean _developTechActive;
    private boolean _developPeriodActive;
    private boolean _parallelActive;
    private List _enrollmentCampaignList;
    private List _enrollmentCampaignStageList;
    private ISelectModel _studentCategoryListModel;
    private ISelectModel _qualificationListModel;
    private ISelectModel _compensationTypeListModel;
    private ISelectModel _formativeOrgUnitListModel;
    private ISelectModel _territorialOrgUnitListModel;
    private ISelectModel _educationLevelHighSchoolListModel;
    private ISelectModel _developFormListModel;
    private ISelectModel _developConditionListModel;
    private ISelectModel _developTechListModel;
    private ISelectModel _developPeriodListModel;
    private List _parallelList;
    private IdentifiableWrapper _enrollmentCampaignStage;
    private List _studentCategoryList;
    private List _qualificationList;
    private CompensationType _compensationType;
    private List<OrgUnit> _formativeOrgUnitList;
    private List _territorialOrgUnitList;
    private List _educationLevelHighSchoolList;
    private List _developFormList;
    private List _developConditionList;
    private List _developTechList;
    private List _developPeriodList;
    private YesNoUIObject _parallel;
    private String _enroll3007title;
    private Date _enroll3007from;
    private Date _enroll3007to;
    private String _enroll0508title;
    private Date _enroll0508from;
    private Date _enroll0508to;
    private String _enroll3108title;
    private Date _enroll3108from;
    private Date _enroll3108to;
}
