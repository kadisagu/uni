package ru.tandemservice.narfu.uniec.dao.print;

import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.narfu.entity.EntrantRequestNARFU;
import ru.tandemservice.uniec.dao.print.PrintParameter;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline;

import java.util.Date;
import java.util.List;

public interface IEntrantExamListPrintBean extends IPrintFormCreator<PrintParameter> {

    public String getSecretary(OrgUnit formativOu);

    public List<ExamPassDiscipline> getDisciplineList(Entrant entrant);

    /**
     * Дата сдачи дисциплины - если не указано явно - дерем из справочника дат сдач для
     * дисциплин
     *
     * @param exapPassDiscipline
     *
     * @return
     */
    public Date getExamPassDisciplineDate(ExamPassDiscipline exapPassDiscipline);

    public Date getExamPassDate(ExamPassDiscipline examPassDiscipline);

    public EntrantRequestNARFU getEntrantRequestNARFU(EntrantRequest request);

    public int getFinalBall(EntrantRequest request, boolean byFirstDirection);
}
