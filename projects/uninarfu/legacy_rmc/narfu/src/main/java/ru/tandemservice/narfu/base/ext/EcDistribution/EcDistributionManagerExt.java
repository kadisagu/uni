package ru.tandemservice.narfu.base.ext.EcDistribution;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import ru.tandemservice.narfu.base.ext.EcDistribution.logic.EcDistributionDaoExt;
import ru.tandemservice.narfu.base.ext.EcDistribution.logic.EcDistributionDaoWarningExt;
import ru.tandemservice.narfu.base.ext.EcDistribution.logic.IEcDistributionDaoExt;
import ru.tandemservice.narfu.base.ext.EcDistribution.logic.IEcDistributionDaoWarningExt;

@Configuration
public class EcDistributionManagerExt extends BusinessObjectExtensionManager {


    public static EcDistributionManagerExt instance() {
        return instance(EcDistributionManagerExt.class);
    }


    @Bean
    public IEcDistributionDaoExt dao() {
        return new EcDistributionDaoExt();
    }

    @Bean
    public IEcDistributionDaoWarningExt daoWarningDistribution() {
        return new EcDistributionDaoWarningExt();
    }


}
