package ru.tandemservice.narfu.component.settings.EnrCampaignDevelopFormDates.AddEdit;

import ru.tandemservice.narfu.entity.EnrCampaignDevelopFormDates;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.GregorianCalendar;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.setDevelopFormList(getList(DevelopForm.class));
        GregorianCalendar today = new GregorianCalendar();

        if (model.getRecord().getId() != null)
            model.setRecord(getNotNull(EnrCampaignDevelopFormDates.class, model.getRecord().getId()));
        else
            model.getRecord().setDocDate(new GregorianCalendar(today.get(GregorianCalendar.YEAR), 7, 31).getTime());
    }

    @Override
    public void update(Model model)
    {
        getSession().saveOrUpdate(model.getRecord());
    }
}
