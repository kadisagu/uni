package ru.tandemservice.narfu.component.eduplan.EduPlanBlockVersionLink;

import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.narfu.entity.EppEduPlanVersionBlockLink;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

import java.util.Collections;
import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {
    @Override
    public void prepare(Model model)
    {
        if (model.getBlockId() != null)
            model.setBlock(get(EppEduPlanVersionBlock.class, model.getBlockId()));
        else
            throw new ApplicationException("Не верный блок версии");

    }

    @Override
    public void prepareListDataSource(Model model) {

        MQBuilder builder = new MQBuilder(EppEduPlanVersionBlockLink.ENTITY_CLASS, "eou")
                .add(MQExpression.eq("eou", EppEduPlanVersionBlockLink.blockSrc().id(), model.getBlockId()));
        List<EppEduPlanVersionBlockLink> resultList = builder.getResultList(getSession());


        if (model.getDataSource().getEntityOrder() != null)
            Collections.sort(resultList, new EntityComparator<>(model.getDataSource().getEntityOrder()));

        UniBaseUtils.createPage(model.getDataSource(), resultList);

    }
}
