package ru.tandemservice.narfu.component.orgUnit.StudentToProtocolSAC;

import ru.tandemservice.diplomarmc.component.orgunit.StudentToProtocolSAC.Model;
import ru.tandemservice.diplomarmc.entity.RelProtocolSACToStudent;
import ru.tandemservice.movestudentrmc.entity.StudentVKRTheme;
import ru.tandemservice.movestudentrmc.util.RepresentationUtil;
import ru.tandemservice.uni.entity.employee.Student;

public class DAO extends ru.tandemservice.diplomarmc.component.orgunit.StudentToProtocolSAC.DAO
{
    @Override
    public void update(Model model) {
        saveOrUpdate(model.getProtocolSAC());
        if (model.getProtocolSACId() == null) {
            for (Long id : model.getStudentIds()) {

                Student student = getNotNull(id);
                StudentVKRTheme theme = RepresentationUtil.getStudentVkrThemeLong(student);
                RelProtocolSACToStudent rel = new RelProtocolSACToStudent();
                rel.setProtocolSAC(model.getProtocolSAC());
                rel.setStudent(student);
                rel.setThemaVKR(theme.getTheme());
                rel.setHons(false);
                saveOrUpdate(rel);
            }
        }
    }
}
