package ru.tandemservice.narfu.component.studentmassprint.printDAO.grants.requestMonth;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import ru.tandemservice.narfu.component.studentmassprint.GrantsMassPrint.Model;
import ru.tandemservice.narfu.component.studentmassprint.printDAO.grants.AbstractGrantRequestReport;

import java.util.Date;

public class PrintBean extends AbstractGrantRequestReport<Model>
{

    @Override
    protected RtfInjectModifier createInjectModifier(Model model) {
        RtfInjectModifier im = super.createInjectModifier(model);

        Date beginPayment = model.getDateOfBeginPayment();
        String month = CommonBaseDateUtil.getMonthNameDeclined(beginPayment, GrammaCase.PREPOSITIONAL);
        im.put("month", month);

        im.put("year", String.valueOf(CoreDateUtils.getYear(beginPayment)));

        return im;
    }

}
