package ru.tandemservice.narfu.component.reports.JournalAcademVacations.List;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.narfu.entity.NarfuReportJournalAcademVacations;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        if (TopOrgUnit.getInstance().getId().equals(model.getOrgUnitId()))
            model.setOrgUnitId(null);
        if (null != model.getOrgUnitId()) {
            model.setOrgUnit(getNotNull(model.getOrgUnitId()));
            model.setSecModel(new OrgUnitSecModel(model.getOrgUnit()));
        }
    }

    @Override
    public void prepareListDataSource(Model model) {
        MQBuilder builder = new MQBuilder(NarfuReportJournalAcademVacations.ENTITY_CLASS, "report");
        if (model.getOrgUnitId() != null)
            builder.add(MQExpression.eq("report", NarfuReportJournalAcademVacations.L_ORG_UNIT + ".id", model.getOrgUnitId()));
        new OrderDescriptionRegistry("report").applyOrder(builder, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }
}
