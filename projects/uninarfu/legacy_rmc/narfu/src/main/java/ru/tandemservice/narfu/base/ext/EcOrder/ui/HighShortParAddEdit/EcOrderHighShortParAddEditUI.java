package ru.tandemservice.narfu.base.ext.EcOrder.ui.HighShortParAddEdit;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.narfu.entity.EntrHighShortExtractExt;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.base.bo.EcOrder.ui.BlockParAddEdit.EcOrderBlockParAddEditUI;


public class EcOrderHighShortParAddEditUI extends ru.tandemservice.uniec.base.bo.EcOrder.ui.HighShortParAddEdit.EcOrderHighShortParAddEditUI {

    private ISelectModel eduLevSpecHSModel;
    private EducationOrgUnit eduLevSpecHS;

    @Override
    public void onComponentRefresh() {
        super.onComponentRefresh();

        final EcOrderBlockParAddEditUI child = (EcOrderBlockParAddEditUI) this._uiSupport.getChildUI("ecOrderBlockParAddEditRegion");

        setEduLevSpecHSModel(new DQLFullCheckSelectModel(EducationOrgUnit.educationLevelHighSchool().displayableTitle().s()) {

            @Override
            protected DQLSelectBuilder query(String alias, String filter) {

                if (child.getFormativeOrgUnit() == null)
                    return null;
                if (child.getEducationLevelsHighSchool() == null)
                    return null;
                if (child.getDevelopForm() == null)
                    return null;

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, alias)
                        .where(DQLExpressions.eqValue(DQLExpressions.property(EducationOrgUnit.formativeOrgUnit().fromAlias(alias)), child.getFormativeOrgUnit()))
                        .where(DQLExpressions.eqValue(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().educationLevel().parentLevel().fromAlias(alias)), child.getEducationLevelsHighSchool().getEducationLevel()))
                        .where(DQLExpressions.eqValue(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSubject().fromAlias(alias)), child.getEducationLevelsHighSchool().getEducationLevel().getEduProgramSubject()))
//						.where(DQLExpressions.instanceOf(EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSpecialization().fromAlias(alias).s(), EduProgramSpecializationChild.class))
                        .where(DQLExpressions.eqValue(DQLExpressions.property(EducationOrgUnit.developForm().fromAlias(alias)), child.getDevelopForm()));
                if (child.isEnrollParagraphPerEduOrgUnit()) {
                    if (child.getDevelopCondition() == null)
                        return null;
                    if (child.getDevelopPeriod() == null)
                        return null;
                    if (child.getDevelopTech() == null)
                        return null;

                    builder
                            .where(DQLExpressions.eqValue(DQLExpressions.property(EducationOrgUnit.developCondition().fromAlias(alias)), child.getDevelopCondition()))
                            .where(DQLExpressions.eqValue(DQLExpressions.property(EducationOrgUnit.developPeriod().fromAlias(alias)), child.getDevelopPeriod()))
                            .where(DQLExpressions.eqValue(DQLExpressions.property(EducationOrgUnit.developTech().fromAlias(alias)), child.getDevelopTech()));
                }

                if (filter != null)
                    builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().title().fromAlias(alias))), DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));

                return builder;
            }
        });

        if (isEditForm() && getParagraph() != null) {
            EntrHighShortExtractExt extractExt = DataAccessServices.dao().get(EntrHighShortExtractExt.class, EntrHighShortExtractExt.extract().id(), getParagraph().getFirstExtract().getId());

            if (extractExt != null)
                setEduLevSpecHS(extractExt.getEducationOrgUnit());
        }

    }

    public ISelectModel getEduLevSpecHSModel() {
        return eduLevSpecHSModel;
    }

    public void setEduLevSpecHSModel(ISelectModel eduLevSpecHSModel) {
        this.eduLevSpecHSModel = eduLevSpecHSModel;
    }

    public EducationOrgUnit getEduLevSpecHS() {
        return eduLevSpecHS;
    }

    public void setEduLevSpecHS(EducationOrgUnit eduLevSpecHS) {
        this.eduLevSpecHS = eduLevSpecHS;
    }

}
