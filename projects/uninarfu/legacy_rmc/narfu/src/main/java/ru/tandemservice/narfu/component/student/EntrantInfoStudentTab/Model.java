package ru.tandemservice.narfu.component.student.EntrantInfoStudentTab;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.IEntityHandler;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

import java.util.List;
import java.util.Map;

public class Model {


    public Map parameters;
    private Entrant entrant;
    private Student _student = new Student();
    private RequestedEnrollmentDirection requestedEnrollmentDirection;
    private boolean hasEntrantData;

    private DynamicListDataSource<EntrantStateExamCertificate> entrantStateExamCertificatesDataSource;
    private Map<EntrantStateExamCertificate, List<StateExamSubjectMark>> certificate2SubjectsMarks;
    private StateExamSubjectMark currentStateExamSubjectMark;
    private IEntityHandler defaultEntityHandler;

    private DynamicListDataSource<OlympiadDiplomaWrapper> _olympiadDiplomaDataSource;

    private DynamicListDataSource<DisciplineMarkWrapper> markResultDataSource;

    public Model() {
        this.entrant = new Entrant();
        this.defaultEntityHandler = entity -> !Model.this.isAccessible();
    }

    public Student getStudent() {
        return _student;
    }

    public void setStudent(Student student) {
        _student = student;
    }

    public boolean isHasEntrantData() {
        return hasEntrantData;
    }

    public void setHasEntrantData(boolean hasEntrantData) {
        this.hasEntrantData = hasEntrantData;
    }

    public RequestedEnrollmentDirection getRequestedEnrollmentDirection() {
        return requestedEnrollmentDirection;
    }

    public void setRequestedEnrollmentDirection(RequestedEnrollmentDirection requestedEnrollmentDirection) {
        this.requestedEnrollmentDirection = requestedEnrollmentDirection;
    }

    public DynamicListDataSource<DisciplineMarkWrapper> getMarkResultDataSource() {
        return markResultDataSource;
    }

    public void setMarkResultDataSource(DynamicListDataSource<DisciplineMarkWrapper> markResultDataSource) {
        this.markResultDataSource = markResultDataSource;
    }

    public DynamicListDataSource<OlympiadDiplomaWrapper> getOlympiadDiplomaDataSource() {
        return _olympiadDiplomaDataSource;
    }

    public void setOlympiadDiplomaDataSource(DynamicListDataSource<OlympiadDiplomaWrapper> olympiadDiplomaDataSource) {
        this._olympiadDiplomaDataSource = olympiadDiplomaDataSource;
    }


    public Map getParameters() {
        if (parameters == null) {
            parameters = ParametersMap.createWith("studentId", getStudent().getId());
        }
        return parameters;
    }


    public IEntityHandler getDefaultEntityHandler() {
        return defaultEntityHandler;
    }

    public Entrant getEntrant() {
        return entrant;
    }

    public void setEntrant(Entrant entrant) {
        this.entrant = entrant;
    }

    public DynamicListDataSource<EntrantStateExamCertificate> getEntrantStateExamCertificatesDataSource() {
        return entrantStateExamCertificatesDataSource;
    }

    public void setEntrantStateExamCertificatesDataSource(DynamicListDataSource<EntrantStateExamCertificate> entrantStateExamCertificatesDataSource) {
        this.entrantStateExamCertificatesDataSource = entrantStateExamCertificatesDataSource;
    }

    public Map<EntrantStateExamCertificate, List<StateExamSubjectMark>> getCertificate2SubjectsMarks() {
        return certificate2SubjectsMarks;
    }

    public void setCertificate2SubjectsMarks(Map<EntrantStateExamCertificate, List<StateExamSubjectMark>> certificate2SubjectsMarks) {
        this.certificate2SubjectsMarks = certificate2SubjectsMarks;
    }

    public StateExamSubjectMark getCurrentStateExamSubjectMark() {
        return currentStateExamSubjectMark;
    }

    public void setCurrentStateExamSubjectMark(StateExamSubjectMark currentStateExamSubjectMark) {
        this.currentStateExamSubjectMark = currentStateExamSubjectMark;
    }

    public boolean isAccessible() {
        return !this.entrant.isArchival();
    }


    static class OlympiadDiplomaWrapper extends IdentifiableWrapper<OlympiadDiploma> {
        public static final String P_DIPLOMA = "diploma";
        private OlympiadDiploma _diploma;
        private String _enrollmentDirectionTitle;
        private String _disciplineListTitle;
        public static final String ENROLLMENT_DIRECTION_TITLE = "enrollmentDirectionTitle";
        public static final String DISCIPLINE_LIST_TITLE = "disciplineListTitle";

        public OlympiadDiplomaWrapper(OlympiadDiploma diploma, String enrollmentDirectionTitle, String disciplineListTitle)
                throws ClassCastException
        {
            super(diploma.getId(), diploma.getSubject());
            this._diploma = diploma;
            this._enrollmentDirectionTitle = enrollmentDirectionTitle;
            this._disciplineListTitle = disciplineListTitle;
        }

        public OlympiadDiploma getDiploma() {
            return this._diploma;
        }

        public String getEnrollmentDirectionTitle() {
            return this._enrollmentDirectionTitle;
        }

        public String getDisciplineListTitle() {
            return this._disciplineListTitle;
        }
    }

    static class DisciplineMarkWrapper extends IdentifiableWrapper<Discipline2RealizationWayRelation> {
        public static final String COLUMN_STATE_EXAM_INTERNAL = "columnStateExamInternal";
        public static final String COLUMN_STATE_EXAM_INTERNAL_APPEAL = "columnStateExamInternalAppeal";
        public static final String COLUMN_EXAM = "columnExam";
        public static final String COLUMN_EXAM_APPEAL = "columnExamAppeal";
        public static final String COLUMN_TEST = "columnTest";
        public static final String COLUMN_TEST_APPEAL = "columnTestAppeal";
        public static final String COLUMN_INTERVIEW = "columnInterview";
        public static final String COLUMN_INTERVIEW_APPEAL = "columnInterviewAppeal";
        public static final String P_TITLE = "title";
        public static final String P_STATE_EXAM = "stateExamMark";
        public static final String P_STATE_EXAM_VALUE = "stateExamScaledMark";
        public static final String P_OLYMPIAD = "olympiadMark";
        public static final String P_FINAL_MARK = "finalMark";
        private String _stateExamMark;
        private String _stateExamScaledMark;
        private String _olympiadMark;
        private String _stateExamInternalMark;
        private String _stateExamInternalMarkAppeal;
        private String _examMark;
        private String _examMarkAppeal;
        private String _testMark;
        private String _testMarkAppeal;
        private String _interviewMark;
        private String _interviewMarkAppeal;
        private String _finalMark;
        private int _minMark;
        private int _maxMark;

        public DisciplineMarkWrapper(Discipline2RealizationWayRelation discipline, String stateExamMark, String stateExamScaledMark, String olympiadMark, String finalMark) {
            super(discipline);
            this._stateExamMark = stateExamMark;
            this._stateExamScaledMark = stateExamScaledMark;
            this._olympiadMark = olympiadMark;
            this._finalMark = finalMark;
            this._minMark = discipline.getMinMark();
            this._maxMark = discipline.getMaxMark();
        }

        public String getStateExamMark() {
            return this._stateExamMark;
        }

        public void setStateExamMark(String stateExamMark) {
            this._stateExamMark = stateExamMark;
        }

        public String getStateExamScaledMark() {
            return this._stateExamScaledMark;
        }

        public void setStateExamScaledMark(String stateExamScaledMark) {
            this._stateExamScaledMark = stateExamScaledMark;
        }

        public String getOlympiadMark() {
            return this._olympiadMark;
        }

        public void setOlympiadMark(String olympiadMark) {
            this._olympiadMark = olympiadMark;
        }

        public String getStateExamInternalMark() {
            return this._stateExamInternalMark;
        }

        public void setStateExamInternalMark(String stateExamInternalMark) {
            this._stateExamInternalMark = stateExamInternalMark;
        }

        public String getStateExamInternalMarkAppeal() {
            return this._stateExamInternalMarkAppeal;
        }

        public void setStateExamInternalMarkAppeal(String stateExamInternalMarkAppeal) {
            this._stateExamInternalMarkAppeal = stateExamInternalMarkAppeal;
        }

        public String getExamMark() {
            return this._examMark;
        }

        public void setExamMark(String examMark) {
            this._examMark = examMark;
        }

        public String getExamMarkAppeal() {
            return this._examMarkAppeal;
        }

        public void setExamMarkAppeal(String examMarkAppeal) {
            this._examMarkAppeal = examMarkAppeal;
        }

        public String getTestMark() {
            return this._testMark;
        }

        public void setTestMark(String testMark) {
            this._testMark = testMark;
        }

        public String getTestMarkAppeal() {
            return this._testMarkAppeal;
        }

        public void setTestMarkAppeal(String testMarkAppeal) {
            this._testMarkAppeal = testMarkAppeal;
        }

        public String getInterviewMark() {
            return this._interviewMark;
        }

        public void setInterviewMark(String interviewMark) {
            this._interviewMark = interviewMark;
        }

        public String getInterviewMarkAppeal() {
            return this._interviewMarkAppeal;
        }

        public void setInterviewMarkAppeal(String interviewMarkAppeal) {
            this._interviewMarkAppeal = interviewMarkAppeal;
        }

        public String getFinalMark() {
            return this._finalMark;
        }

        public void setFinalMark(String finalMark) {
            this._finalMark = finalMark;
        }

        public int getMinMark() {
            return this._minMark;
        }

        public void setMinMark(int minMark) {
            this._minMark = minMark;
        }

        public int getMaxMark() {
            return this._maxMark;
        }

        public void setMaxMark(int maxMark) {
            this._maxMark = maxMark;
        }
    }

}
