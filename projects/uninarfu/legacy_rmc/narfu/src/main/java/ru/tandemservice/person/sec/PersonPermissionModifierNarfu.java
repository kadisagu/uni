package ru.tandemservice.person.sec;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.PermissionGroupMeta;
import org.tandemframework.sec.meta.SecurityConfigMeta;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.person.sec.IPersonRolePermissionMeta;

import java.util.*;
import java.util.stream.Collectors;


public class PersonPermissionModifierNarfu implements ISecurityConfigMetaMapModifier
{

    @Override
    public void modify(Map<String, SecurityConfigMeta> stringSecurityConfigMetaMap)
    {

        SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("person");
        config.setName("narfu-person-sec-config");
        config.setTitle("");

        Map<String, PermissionGroupMeta> permissionGroupMetaMap = stringSecurityConfigMetaMap.values().stream()
                .map(SecurityConfigMeta::getPermissionGroupList)
                .flatMap(Collection::stream)
                .collect(Collectors.toMap(PermissionGroupMeta::getName, meta -> meta, (meta1, meta2) -> meta1));

        for (IPersonRolePermissionMeta meta : (List<IPersonRolePermissionMeta>) ApplicationRuntime.getBean(IPersonRolePermissionMeta.LIST_BEAN_NAME))
        {
            String name = meta.getName();
            String title = meta.getTitle();

            String pgName = name + "PG";
            PermissionGroupMeta personRolePG = permissionGroupMetaMap.get(pgName);
            if (personRolePG == null)
                PermissionMetaUtil.createPermissionGroup(config, pgName, "Объект  «" + title + "»");

            String tabPassportGN = "tabPassport_" + pgName;
            PermissionGroupMeta tabPersonDataPG = findPermissionGroupMeta(personRolePG, tabPassportGN)
                    .orElse(PermissionMetaUtil.createPermissionGroup(personRolePG, tabPassportGN, "Вкладка «Личные данные»"));

            String tabContractGN = "tabContacts_" + pgName;
            PermissionGroupMeta tabContactsPG = findPermissionGroupMeta(tabPersonDataPG, tabContractGN)
                    .orElse(PermissionMetaUtil.createPermissionGroup(tabPersonDataPG, tabContractGN, "Вкладка «Контакты»"));
            PermissionMetaUtil.createPermission(tabContactsPG, "editTempAddress_" + name, "Редактирование адреса временной регистрации");

            String tabPersonGN = "tabPerson_" + pgName;
            PermissionGroupMeta tabPersonPG = findPermissionGroupMeta(tabPersonDataPG, tabPersonGN)
                    .orElse(PermissionMetaUtil.createPermissionGroup(tabPersonDataPG, tabPersonGN, "Вкладка «Персона»"));

            String tabSocPayGN = "tabPerson_socialPayments_" + pgName;
            PermissionGroupMeta socialBlockPG = findPermissionGroupMeta(tabPersonPG, tabSocPayGN)
                    .orElse(PermissionMetaUtil.createPermissionGroup(tabPersonPG, tabSocPayGN, "Блок «Данные для социальной выплаты»"));
            PermissionMetaUtil.createPermission(socialBlockPG, "viewSocialPaymentsData_" + name, "Просмотр блока «Данные для социальной выплаты»");
            PermissionMetaUtil.createPermission(socialBlockPG, "editSocialPaymentsData_" + name, "Редактирование данных для социальной выплаты");
        }

        stringSecurityConfigMetaMap.put(config.getName(), config);
    }

    private Optional<PermissionGroupMeta> findPermissionGroupMeta(PermissionGroupMeta parentPG, String groupeMetaName)
    {
        if (parentPG == null) return Optional.empty();

        return parentPG.getPermissionGroupList().stream().filter(m -> m.getName().equals(groupeMetaName)).findFirst();
    }
}
