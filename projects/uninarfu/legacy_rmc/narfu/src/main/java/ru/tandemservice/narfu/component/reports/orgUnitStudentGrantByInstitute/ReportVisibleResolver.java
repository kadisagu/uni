package ru.tandemservice.narfu.component.reports.orgUnitStudentGrantByInstitute;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitReportList.IOrgUnitReportVisibleResolver;

public class ReportVisibleResolver implements IOrgUnitReportVisibleResolver {

    public ReportVisibleResolver() {
    }

    @Override
    public boolean isVisible(OrgUnit orgunit) {
        return orgunit.isTop();
    }

}
