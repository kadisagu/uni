package ru.tandemservice.narfu.component.catalog.DefaultScriptCatalogAddEdit;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultScriptCatalogAddEdit.DefaultScriptCatalogAddEditModel;

public class DefaultScriptCatalogAddEditDAO<T extends ICatalogItem & IScriptItem, Model extends DefaultScriptCatalogAddEditModel<T>>
        extends org.tandemframework.shared.commonbase.component.catalog.base.DefaultScriptCatalogAddEdit.DefaultScriptCatalogAddEditDAO<T, Model>
{

    @Override
    public void validate(Model model, ErrorCollector errors) {
        //Нужно достать метод из класса-"дедушки"
        DefaultCatalogAddEditDAO dao = new DefaultCatalogAddEditDAO<>();
        dao.validate(model, errors);

        if (model.getUserTemplateFile() != null) {
            T template = model.getCatalogItem();
            String path = template.getTemplatePath().toLowerCase();

            if (model.getUserTemplateFile().getSize() > 1024 * 1024) {
                throw new ApplicationException("Размер загружаемого файла не должен превышать 1024Kb.");
            }

            if (path.endsWith(".rtf")) {
                if (!model.getUserTemplateFile().getFileName().endsWith(".rtf")) {
                    errors.add("Файл шаблона должен быть в формате .rtf", "template");
                }
            }
            else if (path.endsWith(".xls")) {
                if (!model.getUserTemplateFile().getFileName().endsWith(".xls")) {
                    errors.add("Файл шаблона должен быть в формате .xls", "template");
                }
            }
        }
    }
}