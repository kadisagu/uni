package ru.tandemservice.narfu.component.student.StudentBookNumberEdit;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        model.setStudent(getNotNull(Student.class, model.getStudent().getId()));
    }

    @Override
    public void update(Model model) {
        saveOrUpdate(model.getStudent());
    }
}
