package ru.tandemservice.narfu.component.reports.StudentArchiveReport.List;

import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.narfu.entity.NarfuReportStudentArchive;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

public class DAO extends UniDao<Model> implements IDAO {


    @Override
    public void prepare(Model model) {
        model.setEducationYearModel(new LazySimpleSelectModel<>(EducationYear.class));

        if (null != model.getOrgUnitId()) {
            model.setOrgUnit(getNotNull(model.getOrgUnitId()));
            model.setSecModel(new OrgUnitSecModel(model.getOrgUnit()));
        }
    }

    @Override
    public void prepareListDataSource(Model model) {

        String index = model.getSettings().get("index");
        String number = model.getSettings().get("number");

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(NarfuReportStudentArchive.class, "report");

        FilterUtils.applySimpleLikeFilter(builder, "report", NarfuReportStudentArchive.index(), index);
        FilterUtils.applySimpleLikeFilter(builder, "report", NarfuReportStudentArchive.number(), number);

        DQLOrderDescriptionRegistry descriptionRegistry = new DQLOrderDescriptionRegistry(NarfuReportStudentArchive.class, "report");
        descriptionRegistry.applyOrder(builder, model.getDataSource().getEntityOrder());

        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }
}
