package ru.tandemservice.narfu.utils;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.person.base.entity.Person;

public class PersonUtil {

    /**
     * Проверка ИНН. Для отображения ошибок используется ErrorCollector
     *
     * @param person
     */
    public static void validateInnNumber(Person person) {
        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();

        if (person.getInnNumber() == null)
            return;
        String inn = person.getInnNumber();

        if ((inn.length() != 10) && (inn.length() != 12)) {
            errorCollector.add("Номер ИНН введен неверно.", new String[]{"inn"});
        }

        if (inn.length() == 10) {
            Integer[] controlNumber = {Integer.valueOf(2), Integer.valueOf(4),
                    Integer.valueOf(10), Integer.valueOf(3),
                    Integer.valueOf(5), Integer.valueOf(9), Integer.valueOf(4),
                    Integer.valueOf(6), Integer.valueOf(8), Integer.valueOf(0)};

            Double controlSumm = Double.valueOf(0.0D);
            for (int i = 0; i < 10; ++i) {
                controlSumm = Double.valueOf(controlSumm.doubleValue()
                                                     + Double.parseDouble(String.valueOf(inn
                                                                                                 .toCharArray()[i])) * controlNumber[i].intValue());
            }
            controlSumm = Double.valueOf(controlSumm.doubleValue() % 11.0D);
            controlSumm = Double
                    .valueOf((controlSumm.doubleValue() > 9.0D) ? controlSumm
                            .doubleValue() % 10.0D : controlSumm.doubleValue());

            if (!(controlSumm.equals(Double.valueOf(Double.parseDouble(String.valueOf(inn.toCharArray()[9]))))))
                errorCollector.add("Номер ИНН введен неверно (не сошлось контрольное число).", new String[]{"inn"});
        }
        else {
            if (inn.length() != 12)
                return;
            Integer[] controlNumber11 = {Integer.valueOf(7),
                    Integer.valueOf(2), Integer.valueOf(4),
                    Integer.valueOf(10), Integer.valueOf(3),
                    Integer.valueOf(5), Integer.valueOf(9), Integer.valueOf(4),
                    Integer.valueOf(6), Integer.valueOf(8), Integer.valueOf(0)};
            Integer[] controlNumber12 = {Integer.valueOf(3),
                    Integer.valueOf(7), Integer.valueOf(2), Integer.valueOf(4),
                    Integer.valueOf(10), Integer.valueOf(3),
                    Integer.valueOf(5), Integer.valueOf(9), Integer.valueOf(4),
                    Integer.valueOf(6), Integer.valueOf(8), Integer.valueOf(0)};

            Double controlSumm11 = Double.valueOf(0.0D);
            Double controlSumm12 = Double.valueOf(0.0D);
            for (int i = 0; i < 11; ++i) {
                controlSumm11 = Double.valueOf(controlSumm11.doubleValue() + Double.parseDouble(String.valueOf(inn.toCharArray()[i])) * controlNumber11[i].intValue());
            }
            for (int i = 0; i < 12; ++i) {
                controlSumm12 = Double.valueOf(controlSumm12.doubleValue() + Double.parseDouble(String.valueOf(inn.toCharArray()[i])) * controlNumber12[i].intValue());
            }
            controlSumm11 = Double.valueOf(controlSumm11.doubleValue() % 11.0D);
            controlSumm11 = Double.valueOf((controlSumm11.doubleValue() > 9.0D) ?
                                                   controlSumm11.doubleValue() % 10.0D :
                                                   controlSumm11.doubleValue());

            controlSumm12 = Double.valueOf(controlSumm12.doubleValue() % 11.0D);
            controlSumm12 = Double.valueOf((controlSumm12.doubleValue() > 9.0D) ?
                                                   controlSumm12.doubleValue() % 10.0D :
                                                   controlSumm12.doubleValue());

            if ((!(controlSumm11.equals(Double.valueOf(Double.parseDouble(String.valueOf(inn.toCharArray()[10]))))))
                    || (!(controlSumm12.equals(Double.valueOf(Double.parseDouble(String.valueOf(inn.toCharArray()[11])))))))
                errorCollector.add("Номер ИНН введен неверно (не сошлось контрольное число).", new String[]{"inn"});
        }

    }
}
