package ru.tandemservice.narfu.unisession.component.settings.EstimatesExamImport;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController<IDAO, Model> {
    public void onRefreshComponent(IBusinessComponent component)
    {
        ((IDAO) getDao()).prepare(getModel(component));
    }

    public void onClickApply(IBusinessComponent component)
    {
        Model model = (Model) getModel(component);

        ((IDAO) getDao()).update(model);

        deactivate(component);
    }
}