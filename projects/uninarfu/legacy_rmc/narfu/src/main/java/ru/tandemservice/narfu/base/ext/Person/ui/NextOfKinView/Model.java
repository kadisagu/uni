package ru.tandemservice.narfu.base.ext.Person.ui.NextOfKinView;

import ru.tandemservice.narfu.entity.PersonNextOfKinNARFU;

public class Model extends org.tandemframework.shared.person.base.bo.Person.ui.NextOfKinView.Model {

    private PersonNextOfKinNARFU _personNextOfKinNARFU;

    public PersonNextOfKinNARFU getPersonNextOfKinNARFU() {
        return _personNextOfKinNARFU;
    }

    public void setPersonNextOfKinNARFU(PersonNextOfKinNARFU personNextOfKinNARFU) {
        this._personNextOfKinNARFU = personNextOfKinNARFU;
    }

}
