package ru.tandemservice.narfu.impexp.dao;

import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.HashMap;
import java.util.Map;

public class MarkInfo {
    public class MarkCode {
        private String code = "";
        private String title = "";
        private boolean positive;

        public MarkCode(SessionMark totMark, SessionMarkCatalogItem markValue)
        {
            SessionMarkCatalogItem cacheMark = null;
            boolean isPozitive;

            if (markValue != null)
                cacheMark = markValue;
            else
                cacheMark = totMark.getCachedMarkValue();

            if (cacheMark != null) {
                code = cacheMark.getCode();
                title = cacheMark.getTitle();

            }
            else {
                code = totMark.getValueItem().getCode();
                title = totMark.getValueItem().getTitle();
            }

            if (totMark.getCachedMarkPositiveStatus() != null)
                isPozitive = totMark.getCachedMarkPositiveStatus();
            else
                isPozitive = totMark.getValueItem().isCachedPositiveStatus();

            setPositive(isPozitive);

        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getTitle() {
            return title;
        }

        public void setPositive(boolean positive) {
            this.positive = positive;
        }

        public boolean isPositive() {
            return positive;
        }

        public String ObjectCode()
        {
            return Boolean.toString(positive) + getCode() + getTitle();
        }

        @Override
        public boolean equals(Object obj)
        {
            if (obj instanceof MarkCode) {
                MarkCode compare = (MarkCode) obj;

                return this.ObjectCode().equals(compare.ObjectCode());
            }
            return super.equals(obj);
        }

        @Override
        public int hashCode() {

            return ObjectCode().hashCode();
        }
    }

    public class YearTerm {
        private EducationYear year;
        private YearDistributionPart part;

        public YearTerm(EducationYear year, YearDistributionPart part)
        {
            this.part = part;
            this.year = year;
        }

        public void setYear(EducationYear year) {
            this.year = year;
        }

        public EducationYear getYear() {
            return year;
        }

        public void setPart(YearDistributionPart part) {
            this.part = part;
        }

        public YearDistributionPart getPart() {
            return part;
        }

        @Override
        public boolean equals(Object obj)
        {
            if (obj instanceof YearTerm) {
                YearTerm compare = (YearTerm) obj;

                return this.Code().equals(compare.Code());
            }
            return super.equals(obj);
        }

        public String Code()
        {
            return Integer.toString(year.getIntValue()) + Integer.toString(part.getNumber());
        }

        @Override
        public int hashCode() {

            return Code().hashCode();
        }

    }

    private Map<YearTerm, Map<MarkCode, Integer>> mapMarkAll = new HashMap<MarkInfo.YearTerm, Map<MarkCode, Integer>>();
    private Map<MarkCode, Integer> mapMarkLast = null;


    public void addMark(
            SessionMark totMark
            , SessionMarkCatalogItem markValue
            , EducationYear year
            , YearDistributionPart part)
    {
        YearTerm yt = new YearTerm(year, part);
        Map<MarkCode, Integer> map = null;

        if (getMapMarkAll().containsKey(yt))
            map = getMapMarkAll().get(yt);
        else {
            map = new HashMap<MarkInfo.MarkCode, Integer>();
            getMapMarkAll().put(yt, map);
            setMapMarkLast(map);
        }

        MarkCode mk = new MarkCode(totMark, markValue);

        Integer count = 0;
        if (map.containsKey(mk))
            count = map.get(mk);

        count++;

        map.put(mk, count);
    }

    /**
     * Успеваемость студента (в понимании САФУ)
     *
     * @return
     */
    public int getProgress(Map<MarkCode, Integer> map)
    {
        int retVal = -1;

        // определяем наличие задолжности
        for (MarkCode mk : map.keySet()) {
            if (!mk.isPositive())
                return 0;
        }

        for (MarkCode mk : map.keySet()) {
            if (!mk.isPositive())
                return 0;
        }

        for (MarkCode mk : map.keySet()) {
            if (mk.getCode().indexOf("5.3") != -1)
                return 1;
        }

        for (MarkCode mk : map.keySet()) {
            if (mk.getCode().indexOf("5.4") != -1)
                return 2;
        }

        return retVal;
    }

    public Map<YearTerm, Map<MarkCode, Integer>> getMapMarkAll() {
        return mapMarkAll;
    }

    public void setMapMarkLast(Map<MarkCode, Integer> mapMarkLast) {
        this.mapMarkLast = mapMarkLast;
    }

    public Map<MarkCode, Integer> getMapMarkLast() {
        return mapMarkLast;
    }
}
