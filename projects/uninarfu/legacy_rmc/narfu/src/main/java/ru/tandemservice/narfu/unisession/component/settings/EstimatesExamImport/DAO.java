package ru.tandemservice.narfu.unisession.component.settings.EstimatesExamImport;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import ru.tandemservice.narfu.entity.ExamBulletinImportedFile;
import ru.tandemservice.narfu.entity.catalog.ExamBulletinImportedFileResult;
import ru.tandemservice.narfu.entity.catalog.codes.ExamBulletinImportedFileResultCodes;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unisession.base.bo.SessionMark.SessionMarkManager;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.ISessionMarkDAO.MarkData;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem;
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkGradeValueCatalogItemCodes;
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkStateCatalogItemCodes;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.Map.Entry;

public class DAO extends UniDao<Model> implements IDAO {

    private static final String REPLY_BULLETIN_NOT_FOUND = "Ведомость не найдена";

    @SuppressWarnings("unchecked")
    @Override
    public void update(Model model) {

        RtfDocument resultRtf = RtfBean.getElementFactory().createRtfDocument();
        ExamBulletinImportedFileResult importStatus = null;
        List tableRows = new ArrayList();
        RtfTableModifier tableModifier = new RtfTableModifier();

        byte[] importedContent;
        try {

            TemplateDocument template = getCatalogItem(TemplateDocument.class,
                                                       "narfuExamBulletinImport");
            resultRtf = new RtfReader().read(template.getContent());
            importedContent = IOUtils.toByteArray(model.getUploadFile()
                                                          .getStream());

            List<ExamBulletinImportedFile> importFileExist = importFileExist(importedContent);
            if (importFileExist.size() > 0) {
                ExamBulletinImportedFileResult result =
                        getCatalogItem(ExamBulletinImportedFileResult.class, ExamBulletinImportedFileResultCodes.IMPORT_ALREADY_EXIST);

                for (ExamBulletinImportedFile file : importFileExist) {
                    file.setImportResult(result);
                    getSession().update(file);
                }
                return;
            }

        }
        catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }

        Map<Long, Map> bulletinStudentMap = new HashMap<Long, Map>();

        if (importedContent != null && resultRtf != null) {

            String string = "";
            try {

                string = new String(importedContent, "windows-1251");
            }
            catch (UnsupportedEncodingException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            String[] rows = string.split("\r\n");

            int rowNumb = 0;
            for (String row : rows) {
                rowNumb++;
                try {
                    String[] values = row.split(";");
                    if (bulletinStudentMap.containsKey(Long.valueOf(values[0]))) {
                        bulletinStudentMap.get(Long.valueOf(values[0])).put(values[1], values[2]);
                    }
                    else {
                        Map<String, String> m = new HashMap<String, String>();

                        m.put(values[1], values[2]);
                        bulletinStudentMap.put(Long.valueOf(values[0]), m);
                    }
                }
                catch (Exception e) {
                    tableRows.add(fillTableRow("Строка № " + rowNumb, "неверный формат данных", e.getLocalizedMessage()));
                }
            }

            for (Long bulletinId : bulletinStudentMap.keySet()) {

                Map<String, String> students = (Map<String, String>) bulletinStudentMap.get(bulletinId);

                Date performDate = null;
                SessionBulletinDocument sessionBulletinDocument = null;
                Collection<SessionDocumentSlot> sessionSlots = null;
                try {
                    sessionBulletinDocument = get(
                            SessionBulletinDocument.class, bulletinId);
                    performDate = sessionBulletinDocument.getPerformDate();
                    sessionSlots = initSessionSlots(Collections
                                                            .singletonList(sessionBulletinDocument));
                }
                catch (Exception e) {
                    tableRows.add(fillTableRow(String.valueOf(bulletinId), "", REPLY_BULLETIN_NOT_FOUND));
                    importStatus = getCatalogItem(ExamBulletinImportedFileResult.class, ExamBulletinImportedFileResultCodes.IMPORT_ERROR);
                    break;
                }


                for (Entry<String, String> student : students.entrySet()) {

                    String csvStudentId = student.getKey();
                    String stringMark = student.getValue();


                    MarkData mark = null;

                    try {
                        mark = getMark(Integer.valueOf(stringMark), performDate);
                    }
                    catch (Exception e) {
                        mark = getStatemark(toStateMakrCode(stringMark), performDate);
                    }

                    SessionDocumentSlot slot = findSlotByStudentNumber(sessionSlots, csvStudentId);

                    if (slot != null && mark != null) {
                        SessionMarkManager.instance().regularMarkDao().saveOrUpdateMark(slot, mark);
                        tableRows.add(fillTableRow(bulletinId, slot, "Оценка (" + mark.getMarkValue().getTitle() + ") выставлена"));
                    }
                    else {
                        if (slot == null)
                            tableRows.add(fillTableRow(String.valueOf(bulletinId), "Студент с номером " + csvStudentId + " не найден!",
                                                       "Для оценки (" + mark.getMarkValue().getTitle() + ") не найдено подходящего документа"));
                        else if (mark == null)
                            tableRows.add(fillTableRow(String.valueOf(bulletinId), "Неверная оценка для судента №" + csvStudentId, stringMark));

                        importStatus = getCatalogItem(ExamBulletinImportedFileResult.class, ExamBulletinImportedFileResultCodes.IMPORT_ERROR);
                    }

                }


            }
            String[][] tableData = (String[][]) tableRows.toArray(new String[tableRows.size()][3]);
            tableModifier.put("T", tableData);
            tableModifier.modify(resultRtf);
            if (importStatus == null) {
                importStatus = getCatalogItem(ExamBulletinImportedFileResult.class, ExamBulletinImportedFileResultCodes.IMPORT_OK);
            }
        }

        updateExamBulletinImport(importedContent, resultRtf, importStatus);

    }

    private String toStateMakrCode(String stringMark) {
        if ("н".equals(stringMark))
            return SessionMarkStateCatalogItemCodes.NOT_APPEAR_UNKNOWN;

        return "";
    }

    private MarkData getStatemark(final String stateCode, final Date performDate) {
        if (!"".equals(stateCode))
            return new MarkData() {
                @Override
                public Double getPoints() {
                    return null;
                }

                @Override
                public Date getPerformDate() {
                    return performDate;
                }

                @Override
                public SessionMarkCatalogItem getMarkValue() {
                    return getByCode(SessionMarkStateCatalogItem.class, stateCode);
                }

                @Override
                public String getComment() {

                    return null;
                }
            };
        return null;

    }

    private SessionDocumentSlot findSlotByStudentNumber(
            Collection<SessionDocumentSlot> sessionSlots, String csvStudentId)
    {

        for (SessionDocumentSlot slot : sessionSlots) {
            if (slot.getActualStudent().getPersonalNumber().equals(csvStudentId))
                return slot;
        }

        return null;
    }

    private List<ExamBulletinImportedFile> importFileExist(byte[] importedContent) {

        String md5ImpContent = DigestUtils.md5Hex(importedContent);

        MQBuilder osspBasementBuilder = (new MQBuilder(ExamBulletinImportedFile.ENTITY_CLASS, "iFile"));

        osspBasementBuilder.add(MQExpression.eq("iFile", ExamBulletinImportedFile.importedFileCRC(), md5ImpContent));

        return osspBasementBuilder.getResultList(getSession());
    }

    private String[] fillTableRow(Long bulletindId, SessionDocumentSlot slot, String report)
    {
        List<String> result = new ArrayList<String>();
        result.add(Long.toString(bulletindId));
        result.add(slot.getActualStudent().getPerson().getFullFio());
        result.add(report);

        return (String[]) result.toArray(new String[result.size()]);
    }

    private String[] fillTableRow(String bulletindId, String slot, String report)
    {
        List<String> result = new ArrayList<String>();

        result.add(bulletindId);
        result.add(slot);
        result.add(report);

        return (String[]) result.toArray(new String[result.size()]);
    }

    private MarkData getMark(int mark, Date pdate) {
        final SessionMarkCatalogItem mi = getMarkItemByNumber(mark);
        if (mi == null)
            return null;
        final Date pd = pdate;

        return new MarkData() {
            @Override
            public Double getPoints() {
                return null;
            }

            @Override
            public Date getPerformDate() {
                return pd;
            }

            @Override
            public SessionMarkCatalogItem getMarkValue() {
                return mi;
            }

            @Override
            public String getComment() {
                return null;
            }
        };
    }

    private SessionMarkCatalogItem getMarkItemByNumber(int mark) {

        switch (mark) {
            case 1:
                return getByCode(SessionMarkCatalogItem.class, SessionMarkGradeValueCatalogItemCodes.NE_ZACHTENO);
            case 2:
                return getByCode(SessionMarkCatalogItem.class,
                                 SessionMarkGradeValueCatalogItemCodes.NEUDOVLETVORITELNO);
            case 3:
                return getByCode(SessionMarkCatalogItem.class,
                                 SessionMarkGradeValueCatalogItemCodes.UDOVLETVORITELNO);
            case 4:
                return getByCode(SessionMarkCatalogItem.class,
                                 SessionMarkGradeValueCatalogItemCodes.HOROSHO);
            case 5:
                return getByCode(SessionMarkCatalogItem.class,
                                 SessionMarkGradeValueCatalogItemCodes.OTLICHNO);
            case 6:
                return getByCode(SessionMarkCatalogItem.class, SessionMarkGradeValueCatalogItemCodes.ZACHTENO);
            default:
                return null;
        }

    }

    private void updateExamBulletinImport(byte[] importedContent, RtfDocument resultRtf, ExamBulletinImportedFileResult status) {
        ExamBulletinImportedFile bulletinImportedFile = new ExamBulletinImportedFile();
        bulletinImportedFile.setImportDate(new Date());
        bulletinImportedFile.setImportedFile(importedContent);
        bulletinImportedFile.setResultFile(RtfUtil.toByteArray(resultRtf));
        bulletinImportedFile.setImportResult(status);
        bulletinImportedFile.setImportedFileCRC(DigestUtils.md5Hex(importedContent));
        getSession().saveOrUpdate(bulletinImportedFile);
        getSession().flush();
    }

    private List<SessionDocumentSlot> initSessionSlots(final List bulletins) {

        final List<SessionDocumentSlot> resultSlots = new ArrayList<SessionDocumentSlot>();

        BatchUtils.execute(bulletins, 50,
                           new BatchUtils.Action<SessionBulletinDocument>() {

                               public void execute(
                                       Collection<SessionBulletinDocument> elements)
                               {
                                   DQLSelectBuilder dql = ((DQLSelectBuilder) ((DQLSelectBuilder) ((DQLSelectBuilder) ((DQLSelectBuilder) ((DQLSelectBuilder) new DQLSelectBuilder()
                                           .fromEntity(SessionDocumentSlot.class, "slot"))
                                           .joinEntity(
                                                   "slot",
                                                   DQLJoinType.left,
                                                   SessionMark.class,
                                                   "mark",
                                                   DQLExpressions.eq(
                                                           DQLExpressions
                                                                   .property(SessionMark
                                                                                     .slot()
                                                                                     .id()
                                                                                     .fromAlias(
                                                                                             "mark")),
                                                           DQLExpressions
                                                                   .property("slot.id"))))
                                           .addColumn("slot")).addColumn("mark"))
                                           .where(DQLExpressions.in(DQLExpressions
                                                                            .property(SessionDocumentSlot
                                                                                              .document().fromAlias("slot")),
                                                                    bulletins))).order(DQLExpressions
                                                                                               .property(SessionDocumentSlot.actualStudent()
                                                                                                                 .person().identityCard().fullFio()
                                                                                                                 .fromAlias("slot")));

                                   for (Object row : dql.createStatement(getSession())
                                           .list()) {
                                       Object[] r = (Object[]) row;
                                       SessionDocumentSlot slot = (SessionDocumentSlot) r[0];
                                       resultSlots.add(slot);

                                   }
                               }
                           });
        return resultSlots;
    }
}
