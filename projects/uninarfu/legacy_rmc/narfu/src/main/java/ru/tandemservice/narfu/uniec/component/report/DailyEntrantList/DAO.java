package ru.tandemservice.narfu.uniec.component.report.DailyEntrantList;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.eventssecrmc.dao.DaoFacade;
import ru.tandemservice.narfu.entity.FormingOrgUnit2Commission;
import ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EducationSubject;
import ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantFilterUtil;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;

import java.util.*;

public class DAO extends UniDao<Model> implements IDAO {

    private final static Map<RequestedEnrollmentDirection, Set<ChosenEntranceDiscipline>> direction2chosen = new Hashtable<RequestedEnrollmentDirection, Set<ChosenEntranceDiscipline>>();

    public void prepare(Model model) {
        Session session = getSession();
        EntrantFilterUtil.prepareEnrollmentCampaignFilterWithSecurity(model, session);
        model.setFormativeOrgUnitListModel(MultiEnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(null, model));
        model.setCompensationTypeModel(new LazySimpleSelectModel<CompensationType>(getCatalogItemListOrderByCode(CompensationType.class), "shortTitle"));
        model.setParameters(new MultiEnrollmentDirectionUtil.Parameters());

        model.getParameters().setRequired(false, MultiEnrollmentDirectionUtil.Parameters.Filter.values());

        model.setEducationLevelsHighSchoolListModel(MultiEnrollmentDirectionUtil.createEducationLevelsHighSchoolModel(model));
        model.setDevelopFormListModel(MultiEnrollmentDirectionUtil.createDevelopFormModel(model));
        model.setDevelopConditionListModel(MultiEnrollmentDirectionUtil.createDevelopConditionModel(model));
        model.setDevelopPeriodListModel(MultiEnrollmentDirectionUtil.createDevelopPeriodModel(model));

        model.setDate(new Date());
    }

    public void createReport(Model model) {
        Map<String, DirectionGroup> processedData = processData(model);

        TemplateDocument templateDocument = getCatalogItem(TemplateDocument.class, "narfuDailyEntrantList");
        RtfDocument template = new RtfReader().read(templateDocument.getContent());
        RtfDocument resultDocument = template.getClone();
        resultDocument.getElementList().clear();

        List<String> groupKeys = new ArrayList<String>(processedData.keySet());
        Collections.sort(groupKeys);
        int i = 0;
        for (String groupKey : groupKeys) {
            DirectionGroup group = processedData.get(groupKey);

            RtfDocument document = createEnrollmentDirectionReport(model, group, template.getClone());

            resultDocument.getElementList().add(document);
            if (++i < groupKeys.size())
                resultDocument.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
        }

        model.setContent(RtfUtil.toByteArray(resultDocument));
    }

    protected RtfDocument createEnrollmentDirectionReport(Model model, DirectionGroup group, RtfDocument template) {
        if (group.requests.size() < 1)
            return null;

        RequestWrapper firstWrapper = group.requests.get(0);
        RtfDocument resultDocument = template.getClone();
        EnrollmentDirection direction = firstWrapper.direction.getEnrollmentDirection();
        EducationOrgUnit educationOrgUnit = direction.getEducationOrgUnit();

        RtfInjectModifier varmod = new RtfInjectModifier();
        varmod.put("INST", educationOrgUnit.getFormativeOrgUnit().getTitle());
        varmod.put("reportTitle", getReportTitle(firstWrapper.direction.getCompensationType()));
        varmod.put("dateN", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(model.getDate()));
        varmod.put("FO", educationOrgUnit.getDevelopForm().getTitle());
        varmod.put("developCondition", StringUtils.trimToEmpty(educationOrgUnit.getDevelopCondition().getTitle()));
        varmod.put("developPeriod", StringUtils.trimToEmpty(educationOrgUnit.getDevelopPeriod().getTitle()));
        varmod.put("Napr", direction.getTitle());
        varmod.put("Vs", "" + group.requests.size());
        varmod.put("instR", educationOrgUnit.getFormativeOrgUnit().getGenitiveCaseTitle());

        if (model.getFormOrgUnit2Commission().get(educationOrgUnit.getFormativeOrgUnit().getId()) != null)
            DaoFacade.getDaoPrintUtil().injectMainSecretary(varmod, model.getFormOrgUnit2Commission().get(educationOrgUnit.getFormativeOrgUnit().getId()));
        else {
            varmod.put("delegatePost", "Ответственный секретарь отборочной комиссии");
            varmod.put("delegateFio", "");
        }

        //таблица
        RtfTableModifier tabmod = new RtfTableModifier();
        List<String[]> tableRows = new ArrayList<String[]>();

        RtfString dechipherSubjects = new RtfString();
        // Расшифровка предметов из справочника
        List<String> strings = new ArrayList<String>();
        Set<Long> dechipherSet = new HashSet<Long>();

        for (int i = 0; i < group.requests.size(); i++) {
            RequestWrapper wrapper = group.requests.get(i);

            String[] arr = new String[9];
            tableRows.add(arr);
            arr[0] = "" + (i + 1);
            arr[1] = wrapper.direction.getEntrantRequest().getStringNumber();
            arr[2] = wrapper.direction.getEntrantRequest().getEntrant().getPerson().getIdentityCard().getFio();

            //документ (оригинал/ копия)
            String documentText = wrapper.direction.isOriginalDocumentHandedIn() ? "оригинал" : "копия";

            // Средний балл аттестата
            arr[5] = wrapper.getAverageMarkString();

            arr[6] = documentText;

            // Направления подготовки
            List<RequestedEnrollmentDirection> redList = model.getRedMap().get(wrapper.direction.getEntrantRequest().getId());
            Set<String> okso = new HashSet<String>();
            for (RequestedEnrollmentDirection red : redList) {
                if (red.equals(wrapper.direction))
                    continue;

                EduProgramSubject currentEduProgramSubject = red.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject();
                EduProgramSubject wrpEduProgramSubject = wrapper.direction.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject();
                String currentOkso = red.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getInheritedOkso();
                String wrapperOkso = wrapper.direction.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getInheritedOkso();
                if (currentEduProgramSubject != null && wrpEduProgramSubject != null) {
                    if (!currentEduProgramSubject.getSubjectCode().equals(wrpEduProgramSubject.getSubjectCode())) {
                        okso.add(currentEduProgramSubject.getSubjectCode());
                        continue;
                    }

                }
                if (currentOkso != null && wrapperOkso != null) {
                    if (!currentOkso.equals(wrapperOkso)) {
                        okso.add(currentOkso);
                    }

                }
//                if(red.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject()!=null)
//				if (!red.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject().getSubjectCode().equals(
//						wrapper.direction.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject().getSubjectCode()))
//					okso.add(red.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject().getSubjectCode());
            }

            arr[7] = StringUtils.join(okso, ", ");

            //примечание
            if (wrapper.hasSpecialRights) {
                arr[8] = "Особые права";
            }
            else if (wrapper.direction.isTargetAdmission())
                arr[8] = "Целевой прием";
            else
                arr[8] = wrapper.direction.getCompetitionKind().getTitle();

            if (wrapper.getCompetitionKindNumber() == 3) {
                arr[3] = "";
                arr[4] = "";
                continue;
            }

            //собираем оценки и формируем расшифровку дисциплин в условных примечаниях
            strings.clear();
            for (ChosenEntranceDiscipline chosenEntranceDiscipline : wrapper.chosenEntranceDisciplineSet) {
                strings.add(wrapper.getDisciplineString(chosenEntranceDiscipline));

                EducationSubject educationSubject = chosenEntranceDiscipline.getEnrollmentCampaignDiscipline().getEducationSubject();

                if (!dechipherSet.contains(educationSubject.getId())) {
                    dechipherSubjects.append(wrapper.getDechipherString(educationSubject)).append(";").append(IRtfData.PAR);
                    dechipherSet.add(educationSubject.getId());
                }
            }
            arr[3] = StringUtils.join(strings, " ");

            //сумма баллов
            if (wrapper.totalSum > 0.0001)
                arr[4] = "" + (int) wrapper.totalSum;
            else
                arr[4] = "";
        }

        tabmod.put("n", tableRows.toArray(new String[][]{}));
        tabmod.modify(resultDocument);

        varmod.put("dechipher", dechipherSubjects);
        varmod.modify(resultDocument);

        return resultDocument;
    }

    protected Map<String, DirectionGroup> processData(Model model) {

        Calendar currDate = Calendar.getInstance();
        currDate.setTime(model.getDate());
        currDate.set(Calendar.HOUR, 23);
        currDate.set(Calendar.MINUTE, 59);
        currDate.set(Calendar.SECOND, 59);

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(RequestedEnrollmentDirection.class, "red")
                .where(DQLExpressions.eq(
                        DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().enrollmentCampaign().id().fromAlias("red")),
                        DQLExpressions.value(model.getEnrollmentCampaign().getId())))
                .where(DQLExpressions.le(
                        DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().regDate().fromAlias("red")),
                        DQLExpressions.valueDate(currDate.getTime())))
                .where(DQLExpressions.eq(
                        DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().takeAwayDocument().fromAlias("red")),
                        DQLExpressions.value(Boolean.FALSE)));

        // Вид возмещения затрат
        if (model.getCompensationType() != null)
            builder.where(DQLExpressions.eq(
                    DQLExpressions.property(RequestedEnrollmentDirection.compensationType().id().fromAlias("red")),
                    DQLExpressions.value(model.getCompensationType().getId())));

        // Формирующее подразделение
        if (model.getSelectedFormativeOrgUnitList() != null && !model.getSelectedFormativeOrgUnitList().isEmpty())
            builder.where(DQLExpressions.in(
                    DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().formativeOrgUnit().fromAlias("red")),
                    model.getSelectedFormativeOrgUnitList()));

        // Направление подготовки/специальность
        if (model.getEducationLevelsHighSchoolList() != null && !model.getEducationLevelsHighSchoolList().isEmpty())
            builder.where(DQLExpressions.in(
                    DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().fromAlias("red")),
                    model.getEducationLevelsHighSchoolList()));

        // Форма освоения
        if (model.getDevelopFormList() != null && !model.getDevelopFormList().isEmpty())
            builder.where(DQLExpressions.in(
                    DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developForm().fromAlias("red")),
                    model.getDevelopFormList()));

        // Условия освоения
        if (model.getDevelopConditionList() != null && !model.getDevelopConditionList().isEmpty())
            builder.where(DQLExpressions.in(
                    DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developCondition().fromAlias("red")),
                    model.getDevelopConditionList()));

        // Срок освоения
        if (model.getDevelopPeriodList() != null && !model.getDevelopPeriodList().isEmpty())
            builder.where(DQLExpressions.in(
                    DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developPeriod().fromAlias("red")),
                    model.getDevelopPeriodList()));

        builder.column("red");

        List<RequestedEnrollmentDirection> enrollmentDirectionList = getList(builder);

        //загружаем вступительные испытания
        DQLSelectBuilder chosenBuilder = new DQLSelectBuilder().fromEntity(ChosenEntranceDiscipline.class, "c")
                .where(DQLExpressions.in(DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().fromAlias("c")), builder.buildQuery()));
        List<ChosenEntranceDiscipline> chosenLst = getList(chosenBuilder);
        for (ChosenEntranceDiscipline chosen : chosenLst) {
            Set<ChosenEntranceDiscipline> set = direction2chosen.get(chosen.getChosenEnrollmentDirection());
            if (set == null)
                direction2chosen.put(chosen.getChosenEnrollmentDirection(), set = new HashSet<ChosenEntranceDiscipline>());
            set.add(chosen);
        }

        //загружаем отборочные коммисии
        DQLSelectBuilder orgUnitBuilder = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "e")
                .where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirection.id().fromAlias("e")), builder.buildQuery()))
                .column(DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().formativeOrgUnit().id().fromAlias("e")))
                .predicate(DQLPredicateType.distinct);

        DQLSelectBuilder commissiomBuilder = new DQLSelectBuilder().fromEntity(FormingOrgUnit2Commission.class, "c")
                .where(DQLExpressions.eqValue(DQLExpressions.property(FormingOrgUnit2Commission.enrollmentCampaign().fromAlias("c")), model.getEnrollmentCampaign()))
                .where(DQLExpressions.in(DQLExpressions.property(FormingOrgUnit2Commission.formingOrgUnit().id().fromAlias("c")), orgUnitBuilder.buildQuery()))
                .column("c");

        List<FormingOrgUnit2Commission> commissLst = getList(commissiomBuilder);

        for (FormingOrgUnit2Commission rel : commissLst)
            model.getFormOrgUnit2Commission().put(rel.getFormingOrgUnit().getId(), rel.getCommission());

        Map<String, DirectionGroup> processedGroups = new HashMap<String, DirectionGroup>();

        DQLSelectBuilder dirExt = new DQLSelectBuilder()
                .fromEntity(RequestedEnrollmentDirectionExt.class, "rede").column(DQLExpressions.property(RequestedEnrollmentDirectionExt.requestedEnrollmentDirection().fromAlias("rede")));
        dirExt.where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirectionExt.requestedEnrollmentDirection().fromAlias("rede")), builder.buildQuery()));
        dirExt.where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirectionExt.haveSpecialRights().fromAlias("rede")), DQLExpressions.value(true)));

        List<RequestedEnrollmentDirection> specialList = getList(dirExt);

        // группируем
        for (RequestedEnrollmentDirection requestedEnrollmentDirection : enrollmentDirectionList) {

            boolean hasSpecialRights = specialList.contains(requestedEnrollmentDirection);

            processRequest(processedGroups, requestedEnrollmentDirection, hasSpecialRights);

            List<RequestedEnrollmentDirection> redList = model.getRedMap().get(requestedEnrollmentDirection.getEntrantRequest().getId());

            if (redList == null)
                redList = new ArrayList<RequestedEnrollmentDirection>();

            redList.add(requestedEnrollmentDirection);
            model.getRedMap().put(requestedEnrollmentDirection.getEntrantRequest().getId(), redList);

        }

        for (DirectionGroup group : processedGroups.values())
            group.sortRequests();

        return processedGroups;
    }

    protected void processRequest(Map<String, DirectionGroup> result, RequestedEnrollmentDirection direction, Boolean hasSpecialRights) {
        String key = DirectionGroup.getSortString(direction);
        DirectionGroup group = result.get(key);
        if (group == null) {
            group = new DirectionGroup(direction);
            result.put(key, group);
        }
        group.add(direction, hasSpecialRights);
    }

    protected String getReportTitle(CompensationType compensationType) {
        String reportTitle = "";

        if (compensationType.isBudget())
            reportTitle = "Список абитуриентов, претендующих на места, финансируемые из средств федерального бюджета";
        else reportTitle = "Список абитуриентов, претендующих на места с полным возмещением затрат";

        return reportTitle;
    }

    static class DirectionGroup implements Comparable<DirectionGroup> {

        public RequestedEnrollmentDirection enrollmentDirection;
        List<RequestWrapper> requests = new ArrayList<RequestWrapper>();

        public DirectionGroup(RequestedEnrollmentDirection direction) {
            this.enrollmentDirection = direction;
        }

        public void add(RequestedEnrollmentDirection direction, Boolean hasSpecialRights) {
            RequestWrapper wrapper = new RequestWrapper(direction, hasSpecialRights);

            requests.add(wrapper);
        }

        public void sortRequests() {
            Collections.sort(requests);
        }

        public static String getSortString(RequestedEnrollmentDirection direction) {
            StringBuilder sortCriteria = new StringBuilder()
                    .append(direction.getEnrollmentDirection().getEducationOrgUnit().getFormativeOrgUnit().getTitle().toLowerCase())
                    .append(direction.getCompensationType().getShortTitle().toLowerCase())
                    .append(direction.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getTitle().toLowerCase())
                    .append(direction.getEnrollmentDirection().getEducationOrgUnit().getDevelopForm().getTitle().toLowerCase())
                    .append(direction.getEnrollmentDirection().getEducationOrgUnit().getDevelopCondition().getTitle().toLowerCase())
                    .append(direction.getEnrollmentDirection().getEducationOrgUnit().getDevelopPeriod().getTitle().toLowerCase());

            return sortCriteria.toString();
        }

        @Override
        public int compareTo(DirectionGroup o) {
            return getSortString(this.enrollmentDirection).compareTo(getSortString(o.enrollmentDirection));
        }

    }

    static class RequestWrapper implements Comparable<RequestWrapper> {
        RequestedEnrollmentDirection direction;
        Boolean hasSpecialRights;

        Set<ChosenEntranceDiscipline> chosenEntranceDisciplineSet;
        double totalSum = 0.0;
        double profExamMark = 0.0;
        double averageMark = 0.0;

        public RequestWrapper(RequestedEnrollmentDirection direction, Boolean hasSpecialRights) {
            this.direction = direction;
            this.chosenEntranceDisciplineSet = direction2chosen.get(direction);
            if (this.chosenEntranceDisciplineSet == null)
                this.chosenEntranceDisciplineSet = Collections.emptySet();

            for (ChosenEntranceDiscipline chosenEntranceDiscipline : chosenEntranceDisciplineSet)
                this.totalSum += chosenEntranceDiscipline.getFinalMark() != null ? chosenEntranceDiscipline.getFinalMark() : 0.0;

            if (direction.getProfileChosenEntranceDiscipline() != null)
                this.profExamMark = direction.getProfileChosenEntranceDiscipline().getFinalMark() != null ?
                        direction.getProfileChosenEntranceDiscipline().getFinalMark() : 0;

            this.averageMark = getAverageEduInstMark(direction.getEntrantRequest().getEntrant());
            this.hasSpecialRights = hasSpecialRights;
        }

        public int getCompetitionKindNumber() {
            if (direction.isTargetAdmission())
                return 1;
            if (hasSpecialRights)
                return 2;
            if ("1".equals(direction.getCompetitionKind().getCode())) //Без вступительных испытаний
                return 3;
            if ("3".equals(direction.getCompetitionKind().getCode())) //Вне конкурса
                return 4;

            return 9;
        }

        public String getDisciplineString(ChosenEntranceDiscipline chosenEntranceDiscipline) {

            if (chosenEntranceDiscipline.getFinalMark() != null) {
                String examType = getExamTypeString(chosenEntranceDiscipline.getFinalMarkSource());
                String examCode = chosenEntranceDiscipline.getEnrollmentCampaignDiscipline().getEducationSubject().getShortTitle();

                Double mark = chosenEntranceDiscipline.getFinalMark();
                return examCode + "-" + mark.intValue() + "(" + examType + ")";
            }
            else
                return "";
        }

        /**
         * Средний балл
         *
         * @param entrant
         *
         * @return
         */
        private Double getAverageEduInstMark(Entrant entrant) {
            PersonEduInstitution eduInstitution = entrant.getPerson().getPersonEduInstitution();

            if (eduInstitution != null)
                return eduInstitution.getAverageMark() == null ? 0d : eduInstitution.getAverageMark();

            return 0d;
        }

        public String getDechipherString(EducationSubject educationSubject) {
            String subjectDechipher = educationSubject.getShortTitle() + " - "
                    + StringUtils.trimToEmpty(educationSubject.getTitle());
            return subjectDechipher;
        }

        private static String getExamTypeString(Integer source) {

            switch (source) {
                case UniecDefines.FINAL_MARK_OLYMPIAD:
                    return "О";
                case UniecDefines.FINAL_MARK_STATE_EXAM_WAVE1:
                    return "Е";
                case UniecDefines.FINAL_MARK_STATE_EXAM_WAVE2:
                    return "Е";
                default:
                    return "В";
            }
        }

        private String getMarkString(int mark) {
            String result = "" + mark;
            if (result.length() < 4)
                result += StringUtils.repeat("0", 4 - result.length());

            return result;
        }

        public String getAverageMarkString() {
            if (this.averageMark > 0)
                return String.valueOf(round(this.averageMark, 2));
            else return "";
        }

        private double round(double number, int scale) {
            int pow = 10;
            for (int i = 1; i < scale; i++)
                pow *= 10;
            double tmp = number * pow;
            return (double) (int) ((tmp - (int) tmp) >= 0.5d ? tmp + 1 : tmp) / pow;
        }

        private String getSortString() {
            StringBuilder sb = new StringBuilder();

            sb.append(getCompetitionKindNumber());
            sb.append(getMarkString(9999 - (int) this.totalSum));
            sb.append(getMarkString(9999 - (int) this.profExamMark));
            sb.append(getMarkString(9999 - (int) this.averageMark));

            return sb.toString();
        }

        @Override
        public int compareTo(RequestWrapper o) {
            return getSortString().compareTo(o.getSortString());
        }
    }
}
