package ru.tandemservice.narfu.component.entrant.EntrantRequestTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;


@State({@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrant.id")})
public class Model extends ru.tandemservice.eventssecrmc.component.entrant.EntrantRequestTab.Model
{

    private boolean hasEnrollmentComission = false;
    private String enrollmentComissionStr;

    public boolean isHasEnrollmentComission() {
        return hasEnrollmentComission;
    }

    public void setHasEnrollmentComission(boolean hasEnrollmentComission) {
        this.hasEnrollmentComission = hasEnrollmentComission;
    }

    public String getEnrollmentComissionStr() {
        return enrollmentComissionStr;
    }

    public void setEnrollmentComissionStr(String enrollmentComissionStr) {
        this.enrollmentComissionStr = enrollmentComissionStr;
    }

}
