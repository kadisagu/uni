package ru.tandemservice.narfu.uniec.component.report.ActPersonInfo.Add;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.narfu.uniec.entity.report.NarfuActPersonInfo;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.Date;

public class DAO extends UniDao<Model>
        implements IDAO
{

    public DAO()
    {
    }

    public void prepare(Model model)
    {
        Session session = getSession();
        model.setParameters(new ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil.Parameters());
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.setFormativeOrgUnitListModel(MultiEnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(model.getPrincipalContext(), (ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil.Model) model));

        // ставим дату - сегодня
        Date date = new Date();
        model.getReport().setDateCreate(date);

    }

    public void update(Model model)
    {
        Session session = getSession();
        NarfuActPersonInfo report = model.getReport();

        report.setFormingDate(new Date());

        if (model.isFormativeOrgUnitActive())
            report.setFormativeOrgUnitTitle(StringUtils.join(UniUtils.getPropertiesList(model.getFormativeOrgUnitList(), "fullTitle"), "; "));

        DatabaseFile databaseFile = (new MakeReport(report, model, getSession())).getContent();

        session.save(databaseFile);
        report.setContent(databaseFile);
        session.save(report);
    }


}
