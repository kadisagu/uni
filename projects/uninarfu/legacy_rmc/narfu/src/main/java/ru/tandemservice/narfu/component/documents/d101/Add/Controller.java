package ru.tandemservice.narfu.component.documents.d101.Add;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseController;

import java.util.Date;


public class Controller extends DocumentAddBaseController<IDAO, Model> {

    public void onChangeDate(IBusinessComponent component) {
        Model model = (Model) component.getModel();
        Date date = ((IDAO) getDao()).getEndDate(model);
        model.setDateEnd(date);
    }
}

