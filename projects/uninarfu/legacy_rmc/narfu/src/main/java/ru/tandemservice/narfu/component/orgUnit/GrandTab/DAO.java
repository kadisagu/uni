package ru.tandemservice.narfu.component.orgUnit.GrandTab;

import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab.MonthWrapper;
import ru.tandemservice.movestudentrmc.entity.GrantEntity;
import ru.tandemservice.movestudentrmc.entity.RelGrantViewOrgUnit;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ramaslov
 * Date: 21.03.13
 * Time: 17:09
 * To change this template use File | Settings | File Templates.
 */
public class DAO extends UniDao<Model> implements IDAO {

    private FullCheckSelectModel getFullCheckSelectModelForOrgUnit() {
        return new FullCheckSelectModel() {
            @Override
            public ListResult findValues(String s) {

                String alias = "r";
                DQLSelectBuilder builder = new DQLSelectBuilder();
                builder.fromEntity(OrgUnit.class, alias);

                FilterUtils.applySimpleLikeFilter(builder, alias, OrgUnit.title(), s);

                List<OrgUnit> grantViews = builder.createStatement(getSession()).list();

                return new ListResult<>(grantViews);
            }
        };
    }

    private FullCheckSelectModel getFullCheckSelectModelForGrantView() {
        return new FullCheckSelectModel() {
            @Override
            public ListResult findValues(String s) {

                String alias = "r";
                DQLSelectBuilder builder = new DQLSelectBuilder();
                builder.fromEntity(GrantView.class, alias);

                FilterUtils.applySimpleLikeFilter(builder, alias, GrantView.title(), s);

                List<GrantView> grantViews = builder.createStatement(getSession()).list();

                return new ListResult<>(grantViews);
            }
        };
    }

    @Override
    public void prepare(final Model model) {
        model.setEduYearList(getCatalogItemList(EducationYear.class));
        if (model.getEduYear() == null)
            model.setEduYear(get(EducationYear.class, EducationYear.current().s(), Boolean.TRUE));

        Calendar c = Calendar.getInstance();
        c.get(Calendar.MONTH);
        List<MonthWrapper> months = new ArrayList<>();
        for (int i = 0; i < 12; i++) {
            MonthWrapper wrapper = new MonthWrapper(i);
            if (model.getMonthBegin() == null && wrapper.getMonthNumber() == c.get(Calendar.MONTH)) {
                model.setMonthBegin(wrapper);
                model.setMonthEnd(wrapper);
            }

            months.add(wrapper);
        }

        model.setMonthBeginModel(months);
        model.setMonthEndModel(months);

        model.setGrantViewModel(getFullCheckSelectModelForGrantView());

        model.setOrgUnitModel(getFullCheckSelectModelForOrgUnit());
    }

    private List<GrantView> makeGrantViews() {
        String alias = "o";
        DQLSelectBuilder builderAllOrgUnit = new DQLSelectBuilder();
        builderAllOrgUnit.fromEntity(GrantView.class, alias);
        return builderAllOrgUnit.createStatement(getSession()).list();
    }

    private List<OrgUnit> makeOrgUnits() {
        String aliasAllOU = "o";
        DQLSelectBuilder builderAllOrgUnit = new DQLSelectBuilder();
        builderAllOrgUnit.fromEntity(OrgUnit.class, aliasAllOU);
        return builderAllOrgUnit.createStatement(getSession()).list();
    }

    private List<GrantEntity> findGrantEntity(OrgUnit orgUnit, GrantView grantView, EducationYear educationYear, String month) {
        String alias = "o";
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(GrantEntity.class, alias);
        builder.where(
                DQLExpressions.and(
                        DQLExpressions.eq(
                                DQLExpressions.property(GrantEntity.orgUnit().fromAlias(alias)),
                                DQLExpressions.value(orgUnit)),
                        DQLExpressions.eq(
                                DQLExpressions.property(GrantEntity.view().fromAlias(alias)),
                                DQLExpressions.value(grantView)),
                        DQLExpressions.eq(
                                DQLExpressions.property(GrantEntity.eduYear().fromAlias(alias)),
                                DQLExpressions.value(educationYear)),
                        DQLExpressions.eq(
                                DQLExpressions.property(GrantEntity.month().fromAlias(alias)),
                                DQLExpressions.value(month))
                )
        );

        return builder.createStatement(getSession()).list();
    }

    private void createGrantEntity(OrgUnit orgUnit, GrantView grantView, EducationYear educationYear, String month, double summ) {
        GrantEntity grantEntity = new GrantEntity();

        grantEntity.setSum(summ > 0 ? summ : 0);
        grantEntity.setOrgUnit(orgUnit);
        grantEntity.setView(grantView);
        grantEntity.setEduYear(educationYear);
        grantEntity.setMonth(month);

        saveOrUpdate(grantEntity);
    }

    @Override
    @Transactional(readOnly = false)
    public void cleanAllGrant(Model model) {

        if (model.getMonthBegin().getMonth() > model.getMonthEnd().getMonth()) {
            throw new ApplicationException("Дата начала выплаты не может быть позже даты окончания выплаты");
        }

        List<OrgUnit> orgUnitList = model.getCheckAllOrgUnit() ? makeOrgUnits() : model.getOrgUnitList();
        List<GrantView> grantViewList = model.getCheckAllGrantView() ? makeGrantViews() : model.getGrantViewList();

        for (int i = model.getMonthBegin().getMonth(); i <= model.getMonthEnd().getMonth(); i++) {
            MonthWrapper monthWrapper = new MonthWrapper(i);

            for (OrgUnit orgUnit : orgUnitList) {
                List<RelGrantViewOrgUnit> relList = getList(RelGrantViewOrgUnit.class, RelGrantViewOrgUnit.second(), orgUnit);

                for (RelGrantViewOrgUnit rel : relList) {

                    for (GrantView grantView : grantViewList) {
                        if (!grantView.getId().equals(rel.getFirst().getId()))
                            continue;

                        String monthForSearch = monthWrapper.getStringValue(model.getEduYear());
                        List<GrantEntity> grantEntities = findGrantEntity(orgUnit, grantView, model.getEduYear(), monthForSearch);

                        if (grantEntities == null || grantEntities.isEmpty() || grantEntities.size() == 0) {
                            createGrantEntity(
                                    orgUnit,
                                    grantView,
                                    model.getEduYear(),
                                    monthWrapper.getStringValue(model.getEduYear()),
                                    model.getSum()
                            );

                        }
                        else {
                            for (GrantEntity item : grantEntities) {
                                item.setSum(model.getSum() > 0 ? model.getSum() : 0);
                                update(item);
                            }
                        }
                    }
                }
            }
        }

    }
}
