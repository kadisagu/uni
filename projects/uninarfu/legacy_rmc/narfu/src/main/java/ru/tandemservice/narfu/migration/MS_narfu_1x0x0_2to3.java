package ru.tandemservice.narfu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;


/*
 * Сущность и логика сильно изменилась, пусть таблица создастся заново
 */
public class MS_narfu_1x0x0_2to3 extends IndependentMigrationScript {

    private static final String TABLE_OLD = "ACCREDITATEDGROUPNARFU_T";

    @Override
    public void run(DBTool tool) throws Exception {
        if (!tool.tableExists(TABLE_OLD))
            return;

        tool.dropTable(TABLE_OLD);
    }
}		


