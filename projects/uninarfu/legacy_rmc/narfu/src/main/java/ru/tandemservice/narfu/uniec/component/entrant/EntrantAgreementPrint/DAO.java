package ru.tandemservice.narfu.uniec.component.entrant.EntrantAgreementPrint;

import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.uni.util.ReportRenderer;
import ru.tandemservice.uniecc.component.entrant.EntrantAgreementPrint.Model;
import ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant;
import ru.tandemservice.unisc.dao.print.IUniscPrintDAO;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPrintVersion;

public class DAO extends ru.tandemservice.uniecc.component.entrant.EntrantAgreementPrint.DAO {

    @Override
    public IDocumentRenderer createDocument(Model model) throws Exception {
        UniscEduAgreement2Entrant a2s = (UniscEduAgreement2Entrant) get(UniscEduAgreement2Entrant.class, model.getId());
        UniscEduAgreementBase agreementBase = a2s.getAgreement();

        UniscEduAgreementPrintVersion printVersion = ((IUniscPrintDAO) IUniscPrintDAO.INSTANCE.get()).getLastPrintVersion(agreementBase);
        if (null != printVersion) return printVersion.getRenderer();

        RtfDocument document = ((IUniscPrintDAO) IUniscPrintDAO.INSTANCE.get()).getDocumentTemplate(agreementBase);
        ((IUniscPrintDAO) IUniscPrintDAO.INSTANCE.get()).getRtfInjectorModifier(agreementBase, a2s.getEntrant().getPerson()).modify(document);

        return new ReportRenderer("StudentContract.rtf", document);
    }
}
