package ru.tandemservice.narfu.component.menu.SelectionCommitteeDelegateEdit;

import ru.tandemservice.narfu.entity.Principal2SelectionCommitteeDelegate;

public class Model {
    Principal2SelectionCommitteeDelegate entity;

    public Principal2SelectionCommitteeDelegate getEntity() {
        return entity;
    }

    public void setEntity(Principal2SelectionCommitteeDelegate entity) {
        this.entity = entity;
    }
}
