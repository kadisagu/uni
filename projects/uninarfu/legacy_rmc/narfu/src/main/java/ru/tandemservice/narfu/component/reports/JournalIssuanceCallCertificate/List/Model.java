package ru.tandemservice.narfu.component.reports.JournalIssuanceCallCertificate.List;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.narfu.entity.StudentDocumentRMC;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;

import java.util.Date;
import java.util.List;
import java.util.Map;

//-----------------------------------------------------------
//-----------------------------------------------------------

@SuppressWarnings("rawtypes")
public class Model extends DefaultCatalogPubModel {

    private Map<Long, StudentDocumentRMC> studentDocumentRMCMap;

    private String contentType;
    private String fileName;
    private byte[] content;
    //--------------------------------------------------------------
    private Date startDate;
    private Date endDate;
    private List<OrgUnit> formativeOrgUnitList;
    private ISelectModel formativeOrgUnitModel;
    private List<Course> courseList;
    private ISelectModel courseModel;
    private List<DevelopForm> developformList;
    private ISelectModel developformModel;

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public List<OrgUnit> getFormativeOrgUnitList() {
		return formativeOrgUnitList;
	}

	public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList) {
		this.formativeOrgUnitList = formativeOrgUnitList;
	} 
	
    public ISelectModel getFormativeOrgUnitModel() {
		return formativeOrgUnitModel;
	}

	public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel) {
		this.formativeOrgUnitModel = formativeOrgUnitModel;
	}	
	
	public List<Course> getCourseList() {
		return courseList;
	}

	public void setCourseList(List<Course> courseList) {
		this.courseList = courseList;
	}
	
	public ISelectModel getCourseModel() {
		return courseModel;
	}

	public void setCourseModel(ISelectModel courseModel) {
		this.courseModel = courseModel;
	}

	public List<DevelopForm> getDevelopformList() {
		return developformList;
	}

	public void setDevelopformList(List<DevelopForm> developformList) {
		this.developformList = developformList;
	}

	public ISelectModel getDevelopformModel() {
		return developformModel;
	}

	public void setDevelopformModel(ISelectModel developformModel) {
		this.developformModel = developformModel;
	}
    //--------------------------------------------------------------

	public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public Map<Long, StudentDocumentRMC> getStudentDocumentRMCMap() {
        return studentDocumentRMCMap;
    }

    public void setStudentDocumentRMCMap(
            Map<Long, StudentDocumentRMC> studentDocumentRMCMap)
    {
        this.studentDocumentRMCMap = studentDocumentRMCMap;
    }
}
