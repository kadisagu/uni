package ru.tandemservice.narfu.component.reports.StudentsByUGS;


import java.util.HashMap;
import java.util.Map;

public class UGSReportData {
    private String title;
    //мапы по курсам:
    private Map<Integer, Integer> actBudStudCount = new HashMap<Integer, Integer>();
    private Map<Integer, Integer> actCtrStudCount = new HashMap<Integer, Integer>();
    private Map<Integer, Integer> vacCtrStudCount = new HashMap<Integer, Integer>();
    private Map<Integer, Integer> vacBudStudCount = new HashMap<Integer, Integer>();

    private Integer lastCourseVal = 0;

    public UGSReportData() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Map<Integer, Integer> getActBudStudCount() {
        return actBudStudCount;
    }

    public void setActBudStudCount(Map<Integer, Integer> actBudStudCount) {
        this.actBudStudCount = actBudStudCount;
    }

    public Map<Integer, Integer> getActCtrStudCount() {
        return actCtrStudCount;
    }

    public void setActCtrStudCount(Map<Integer, Integer> actCtrStudCount) {
        this.actCtrStudCount = actCtrStudCount;
    }

    public Map<Integer, Integer> getVacCtrStudCount() {
        return vacCtrStudCount;
    }

    public void setVacCtrStudCount(Map<Integer, Integer> vacCtrStudCount) {
        this.vacCtrStudCount = vacCtrStudCount;
    }

    public Map<Integer, Integer> getVacBudStudCount() {
        return vacBudStudCount;
    }

    public void setVacBudStudCount(Map<Integer, Integer> vacBudStudCount) {
        this.vacBudStudCount = vacBudStudCount;
    }

    public Integer getLastCourseVal() {
        return lastCourseVal;
    }

    public void setLastCourseVal(Integer lastCourseVal) {
        this.lastCourseVal = lastCourseVal;
    }
}
