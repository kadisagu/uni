package ru.tandemservice.narfu.component.student.OrderListPub;


public class DAO extends ru.tandemservice.movestudentbasermc.component.student.OrderListPub.DAO
{

    @Override
    public void prepare(ru.tandemservice.movestudentbasermc.component.student.OrderListPub.Model model) {
        super.prepare(model);
        Model myModel = (Model) model;
        if (model.getStudent().getStudentCategory().getCode().equals("5") || model.getStudent().getStudentCategory().getCode().equals("6")) {
            myModel.setAspirant(true);
        }
        else {
            myModel.setAspirant(false);
        }

    }

}
