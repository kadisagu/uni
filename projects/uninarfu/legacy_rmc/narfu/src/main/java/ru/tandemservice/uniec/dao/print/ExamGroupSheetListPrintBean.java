package ru.tandemservice.uniec.dao.print;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.dao.examgroup.IExamGroupSetDao;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.catalog.codes.EntrantStateCodes;
import ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.ExamGroup;
import ru.tandemservice.uniec.entity.entrant.ExamGroupRow;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.util.EntrantDataUtil;

import java.util.*;

public class ExamGroupSheetListPrintBean extends UniBaseDao implements IExamGroupSheetListPrintBean {
    @Override
    public RtfDocument generateExamGroupSheetReport(List<Long> groupIds) {
        if (groupIds == null || groupIds.isEmpty())
            throw new ApplicationException("Нет данных для печати");

        IScriptItem script = getCatalogItem(UniecScriptItem.class, "narfuEnrollmentPassDisciplineSheet_code2");
        RtfDocument template = new RtfReader().read(script.getTemplate());

        RtfDocument document = template.getClone();
        document.getElementList().clear();

        boolean canAddPage = false;
        for (Long groupId : groupIds) {
            ExamGroup examGroup = get(ExamGroup.class, groupId);
            if (examGroup == null)
                continue;

            MQBuilder builder = new MQBuilder(ExamGroupRow.ENTITY_CLASS, "g")
                    .add(MQExpression.eq("g", ExamGroupRow.examGroup(), examGroup))
                    .addOrder("g", ExamGroupRow.entrantRequest().entrant().person().identityCard().lastName())
                    .addOrder("g", ExamGroupRow.entrantRequest().entrant().person().identityCard().firstName())
                    .addOrder("g", ExamGroupRow.entrantRequest().entrant().person().identityCard().middleName());

            List<ExamGroupRow> rows = builder.getResultList(getSession());
            if (rows == null || rows.isEmpty())
                return null;

            Map<PairKey<Discipline2RealizationWayRelation, SubjectPassForm>, Set<RequestedEnrollmentDirection>> groupData =
                    IExamGroupSetDao.instance.get().getExamPassDisciplineList(examGroup);
            Map<Long, SubjectPassForm> passFormMap = new HashMap<Long, SubjectPassForm>();
            for (PairKey<Discipline2RealizationWayRelation, SubjectPassForm> pairKey : groupData.keySet())
                passFormMap.put(pairKey.getFirst().getId(), pairKey.getSecond());

            Map<String, List<ChosenEntranceDiscipline>> data = new HashMap<String, List<ChosenEntranceDiscipline>>();
            RequestedEnrollmentDirection mainDirection = null;
            for (ExamGroupRow row : rows) {
                if (row.getEntrantRequest().getEntrant().isArchival())
                    continue;

                builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "red")
                        .add(MQExpression.eq("red", RequestedEnrollmentDirection.entrantRequest(), row.getEntrantRequest()))
                        .add(MQExpression.notEq("red", RequestedEnrollmentDirection.state().code(), EntrantStateCodes.TAKE_DOCUMENTS_AWAY))
                        .add(MQExpression.notEq("red", RequestedEnrollmentDirection.state().code(), EntrantStateCodes.OUT_OF_COMPETITION));

                EntrantDataUtil dataUtil = new EntrantDataUtil(
                        getSession(),
                        row.getEntrantRequest().getEntrant().getEnrollmentCampaign(),
                        builder,
                        EntrantDataUtil.DetailLevel.EXAM_PASS_WITH_MARK_STATISTICS);

                if (mainDirection == null) {
                    builder.addOrder("red", RequestedEnrollmentDirection.priority());
                    mainDirection = (RequestedEnrollmentDirection) builder.getResultList(getSession()).get(0);
                }

                Map<Long, ChosenEntranceDiscipline> disciplinesMap = new HashMap<Long, ChosenEntranceDiscipline>();
                for (RequestedEnrollmentDirection direction : dataUtil.getDirectionSet()) {

                    for (ChosenEntranceDiscipline discipline : dataUtil.getChosenEntranceDisciplineSet(direction))
                        if (!disciplinesMap.containsKey(discipline.getEnrollmentCampaignDiscipline().getId()))
                            disciplinesMap.put(discipline.getEnrollmentCampaignDiscipline().getId(), discipline);
                }

                for (ChosenEntranceDiscipline discipline : disciplinesMap.values()) {
                    Double mark = discipline.getFinalMark();
                    if (mark != null && mark > 0.00001)
                        continue;

                    SubjectPassForm passForm = passFormMap.get(discipline.getEnrollmentCampaignDiscipline().getId());
                    String subjectTitle = discipline.getTitle();
                    if (passForm != null && passForm.getTitle() != null)
                        subjectTitle += " (" + passForm.getTitle() + ")";

                    List<ChosenEntranceDiscipline> subjectList = data.get(subjectTitle);
                    if (subjectList == null) {
                        subjectList = new ArrayList<ChosenEntranceDiscipline>();
                        data.put(subjectTitle, subjectList);
                    }
                    subjectList.add(discipline);
                }
            }

            for (String subjectTitle : data.keySet()) {
                List<ChosenEntranceDiscipline> disciplines = data.get(subjectTitle);

                if (canAddPage)
                    document.addElement(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));

                RtfDocument groupReport = generateGroupReport(examGroup, mainDirection, subjectTitle, disciplines, template);
                document.getElementList().addAll(groupReport.getElementList());
                canAddPage = true;
            }
        }

        return document;
    }

    protected RtfDocument generateGroupReport(ExamGroup examGroup, RequestedEnrollmentDirection mainDirection, String subjectTitle, List<ChosenEntranceDiscipline> disciplines, RtfDocument template) {
        RtfDocument document = template.getClone();

        RtfInjectModifier im = new RtfInjectModifier();

        im.put("Disc", subjectTitle);
        im.put("grupp", examGroup.getShortTitle());
        im.put("bud", mainDirection.getCompensationType().getTitle());
        im.put("fo", mainDirection.getEnrollmentDirection().getEducationOrgUnit().getDevelopForm().getTitle());
        im.put("Napr", mainDirection.getEnrollmentDirection().getTitle());
        im.put("uo", mainDirection.getEnrollmentDirection().getEducationOrgUnit().getDevelopCondition().getTitle());
        im.put("so", mainDirection.getEnrollmentDirection().getEducationOrgUnit().getDevelopPeriod().getTitle());
        im.put("formP", mainDirection.getEnrollmentDirection().getEducationOrgUnit().getFormativeOrgUnit().getTitle());
        im.modify(document);

        List<String[]> tableData = new ArrayList<String[]>();
        int counter = 1;
        for (ChosenEntranceDiscipline discipline : disciplines) {
            String[] arr = new String[7];
            arr[0] = "" + counter++;
            arr[1] = discipline.getChosenEnrollmentDirection().getEntrantRequest().getStringNumber();
            arr[2] = discipline.getChosenEnrollmentDirection().getEntrantRequest().getEntrant().getPerson().getFullFio();
            arr[3] = "";
            arr[4] = "";
            arr[5] = "";
            arr[6] = "";
            tableData.add(arr);
        }

        RtfTableModifier tm = new RtfTableModifier();
        tm.put("tt", tableData.toArray(new String[][]{}));
        tm.modify(document);

        return document;
    }

}
