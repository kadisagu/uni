package ru.tandemservice.narfu.component.reports.StudentsByUGS.List;

import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.narfu.entity.NarfuReportStudentsByUGS;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;

public class DAO extends UniDao<Model> implements IDAO {
    @Override
    public void prepareListDataSource(Model model) {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(NarfuReportStudentsByUGS.class, "report");

        DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(NarfuReportStudentsByUGS.class, "report");
        orderRegistry.applyOrder(dql, model.getDataSource().getEntityOrder());

        UniBaseUtils.createPage(model.getDataSource(), dql, getSession());
    }

}
