package ru.tandemservice.narfu.component.settings.YearPartEndDate.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);
    }

    public synchronized void onClickApply(IBusinessComponent component) {
        Model model = getModel(component);
        this.getDao().update(model);
        this.deactivate(component);
    }

    public synchronized void onClickDeactivate(IBusinessComponent component) {
        this.deactivate(component);
    }

}
