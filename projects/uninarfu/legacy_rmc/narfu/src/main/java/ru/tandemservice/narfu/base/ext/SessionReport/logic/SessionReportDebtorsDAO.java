package ru.tandemservice.narfu.base.ext.SessionReport.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.addon.IUIAddon;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.person.base.util.PersonSecurityUtil;
import ru.tandemservice.narfu.base.ext.SessionReport.ui.DebtorsAdd.SessionReportDebtorsAddUIExt;
import ru.tandemservice.narfu.entity.catalog.codes.UnisessionCommonTemplateCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.DebtorsAdd.SessionReportDebtorsAddUI;
import ru.tandemservice.unisession.base.bo.SessionReport.util.SessionReportUtil;
import ru.tandemservice.unisession.base.bo.SessionReport.util.UniSessionFilterAddon;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem;
import ru.tandemservice.unisession.entity.catalog.UnisessionTemplate;
import ru.tandemservice.unisession.entity.document.SessionObject;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class SessionReportDebtorsDAO extends ru.tandemservice.unisession.base.bo.SessionReport.ui.DebtorsAdd.SessionReportDebtorsDAO
{

    @Override
    public UnisessionDebtorsReport createStoredReport(SessionReportDebtorsAddUI model)
    {
        UnisessionDebtorsReport report = new UnisessionDebtorsReport();

        final UniSessionFilterAddon filterAddon = model.getSessionFilterAddon();

        report.setFormingDate(new Date());
        report.setExecutor(PersonSecurityUtil.getExecutor());

        final SessionObject sessionObject = model.getSessionObject();
        report.setSessionObject(sessionObject);
        report.setFormativeOrgUnit(sessionObject.getOrgUnit().getTitle());

        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_COURSE, UnisessionDebtorsReport.P_COURSE, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_GROUP_WITH_NOGROUP, UnisessionDebtorsReport.P_GROUPS, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_STUDENT_STATUS, UnisessionDebtorsReport.P_STUDENT_STATUS, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_TARGET_ADMISSION, UnisessionDebtorsReport.P_TARGET_ADMISSION, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_COMPENSATION_TYPE, UnisessionDebtorsReport.P_COMPENSATION_TYPE, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_DEVELOP_FORM, UnisessionDebtorsReport.P_DEVELOP_FORM, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_DEVELOP_CONDITION, UnisessionDebtorsReport.P_DEVELOP_CONDITION, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_DEVELOP_PERIOD, UnisessionDebtorsReport.P_DEVELOP_PERIOD, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_DEVELOP_TECH, UnisessionDebtorsReport.P_DEVELOP_TECH, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_EDUCATION_LEVELS, UnisessionDebtorsReport.P_EDUCATION_LEVEL_HIGH_SCHOOL, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_REGISTRY_STRUCTURE, UnisessionDebtorsReport.P_REGISTRY_STRUCTURE, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_CONTROL_FORM, UnisessionDebtorsReport.P_CONTROL_ACTION_TYPES, "title");

        DatabaseFile content = new DatabaseFile();
        content.setContent(print(model, report, sessionObject));
        save(content);
        report.setContent(content);

        save(report);

        return report;
    }

    protected byte[] print(SessionReportDebtorsAddUI model, UnisessionDebtorsReport report, SessionObject sessionObject)
    {
        if (null == sessionObject)
            throw new IllegalStateException("SessionObject is required in report parameters.");
        UnisessionTemplate templateItem = getCatalogItem(UnisessionTemplate.class, UnisessionCommonTemplateCodes.DEBTORS_REPORT_NARFU);
//        UnisessionTemplate templateItem = getCatalogItem(UnisessionTemplate.class, "debtorsReportNarfu");
        if (templateItem == null)
            throw new RuntimeException("Печатный шаблон есть, но не импортирован в справочник шаблонов.");

        RtfDocument rtf = new RtfReader().read(templateItem.getContent());

        prepareHeader(rtf, report, sessionObject);

        DQLSelectBuilder dql = createQuery(model, sessionObject);

        Map<Long, TableData> tableRowMap = new LinkedHashMap<>();
        for (Object[] row : dql.createStatement(getSession()).<Object[]>list())
        {
            Long studentId = (Long) row[0];
            TableData tableData = tableRowMap.get(studentId);
            Integer gridTerm = (Integer) row[5];

            if (tableData == null)
            {
                tableRowMap.put(studentId, tableData = new TableData());
                tableData.row = new String[]{
                        (String) row[4],                            //1 group
                        "",                                         //2 number
                        (String) row[1],                            //3 fio
                        "1".equals(row[2]) ? "бюджет" : "контракт", //4
                        "" + gridTerm,                              //5 semestr number
                        "",                                         //6
                        "",                                         //7
                        ""                                          //8
                };
            }

            if ((sessionObject.getEducationYear().getId().equals(row[6])) && (sessionObject.getYearDistributionPart().getId().equals(row[7])))
                tableData.currentTerm = gridTerm;
            tableData.addDate(gridTerm, (SessionMarkCatalogItem) row[10], (EppRegistryElementPart) row[9], (EppGroupTypeFCA) row[8]);
        }

        int studentCount = 1;
        List<String[]> studentTableData = new ArrayList<>();
        for (TableData tableData : tableRowMap.values())
        {
            tableData.fillRow(tableData.currentTerm);
            tableData.row[1] = String.valueOf(studentCount++);

            studentTableData.add(tableData.row);
        }

        new RtfTableModifier().put("T", studentTableData.toArray(new String[studentTableData.size()][])).put("T", new RtfRowIntercepterBase()
        {
            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                List<IRtfElement> list = new ArrayList<>();
                IRtfText text = RtfBean.getElementFactory().createRtfText(value);
                text.setRaw(true);
                list.add(text);
                return list;
            }
        }).modify(rtf);

        return RtfUtil.toByteArray(rtf);
    }

    //WTF
    private DQLSelectBuilder createQuery(SessionReportDebtorsAddUI model, SessionObject sessionObject)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .column(property(Student.id().fromAlias("s")))//1
                .column(property(Student.person().identityCard().fullFio().fromAlias("s")))//2
                .column(property(Student.compensationType().code().fromAlias("s")))//3
                .column(property(Student.course().title().fromAlias("s")))//4
                .column(property(Group.title().fromAlias("g")))//5
                .column(property(SessionMark.slot().studentWpeCAction().studentWpe().term().intValue().fromAlias("m")))//6
                .column(property(SessionMark.slot().studentWpeCAction().studentWpe().year().educationYear().id().fromAlias("m")))//7
                .column(property(SessionMark.slot().studentWpeCAction().studentWpe().part().id().fromAlias("m")))//8
                .column(property(SessionMark.slot().studentWpeCAction().type().fromAlias("m")))//9
                .column(property(SessionMark.slot().studentWpeCAction().studentWpe().registryElementPart().fromAlias("m")))//10
                .column(property(SessionMark.cachedMarkValue().fromAlias("m")));//11

        dql.fromEntity(SessionMark.class, "m");
        dql.where(eq(
                property(SessionMark.cachedMarkPositiveStatus().fromAlias("m")),
                value(Boolean.FALSE)
        ));
        dql.joinPath(DQLJoinType.inner, SessionMark.slot().studentWpeCAction().fromAlias("m"), "wpca");
        dql.where(eq(
                property(SessionMark.slot().inSession().fromAlias("m")),
                value(Boolean.FALSE)
        ));
        dql.fromEntity(Student.class, "s");
        dql.fromEntity(SessionStudentGradeBookDocument.class, "doc");
        dql.where(eq(
                property(SessionStudentGradeBookDocument.student().fromAlias("doc")),
                property("s")
        ));
        dql.where(eq(
                property(SessionMark.slot().document().fromAlias("m")),
                property("doc")
        ));
//NOTE DEV-DEV-8908
//                dql.where(isNull(
//                        property(SessionMark.slot().studentWpeCAction().studentWpe().studentEduPlanVersion().removalDate().fromAlias("m"))
//                ));
        dql.where(isNull(
                property(SessionMark.slot().studentWpeCAction().removalDate().fromAlias("m"))
        ));
        dql.where(eq(
                property(Student.archival().fromAlias("s")),
                value(Boolean.FALSE)
        ));
        dql.where(eq(
                property(Student.status().active().fromAlias("s")),
                value(Boolean.TRUE)
        ));
        dql.joinEntity("s", DQLJoinType.left, Group.class, "g", eq(property(Student.group().fromAlias("s")), property("g")));
        dql.where(eq(
                property(Student.educationOrgUnit().groupOrgUnit().fromAlias("s")),
                value(sessionObject.getOrgUnit())
        ));
        dql.where(eq(
                property(SessionMark.slot().studentWpeCAction().studentWpe().year().educationYear().fromAlias("m")),
                value(sessionObject.getEducationYear())
        ));
        dql.where(eq(
                property(SessionMark.slot().studentWpeCAction().studentWpe().part().fromAlias("m")),
                value((sessionObject.getYearDistributionPart()))
        ));

        model.getSessionFilterAddon().applyFilters(dql, "wpca");

        final IUIAddon uiAddon = model.getConfig().getAddon("ui_addon");

        if (uiAddon != null)
        {

            final SessionReportDebtorsAddUIExt presenter = (SessionReportDebtorsAddUIExt) uiAddon;
            final boolean byMark = presenter.isByMark();
            final boolean byStatus = presenter.isByStatus();

            if (byMark && byStatus)//always true 'byMark' & 'byStatus' do not used
            {
                //do nothing
            } else if (byMark)
            {
                dql.fromEntity(SessionMarkGradeValueCatalogItem.class, "grade");
                dql.where(eq(
                        property(SessionMark.cachedMarkValue().id().fromAlias("m")),
                        property(SessionMarkGradeValueCatalogItem.id().fromAlias("grade"))
                ));
            } else if (byStatus)
            {
                dql.fromEntity(SessionMarkStateCatalogItem.class, "grade");
                dql.where(eq(
                        property(SessionMark.cachedMarkValue().id().fromAlias("m")),
                        property(SessionMarkStateCatalogItem.id().fromAlias("grade"))
                ));
            } else
            {
                dql.where(isNull(property(SessionMark.cachedMarkValue().id().fromAlias("m"))));
            }
        }

        dql.order(property(Group.title().fromAlias("g")));
        dql.order(property(Student.person().identityCard().fullFio().fromAlias("s")));
        dql.order(property(SessionMark.slot().studentWpeCAction().type().priority().fromAlias("m")));
        dql.order(property(SessionMark.slot().studentWpeCAction().studentWpe().registryElementPart().registryElement().title().fromAlias("m")));

        return dql;
    }

    private void prepareHeader(RtfDocument rtf, UnisessionDebtorsReport report, SessionObject sessionObject)
    {
        RtfInjectModifier im = new RtfInjectModifier();

        im.put("formativeOu", report.getFormativeOrgUnit());
        im.put("educationYear", sessionObject.getEducationYear().getTitle());
        im.put("educationYearPart", sessionObject.getYearDistributionPart().getTitle());

        if (report.getCompensationType() != null)
            im.put("compensationType", report.getCompensationType());
        else
            im.put("compensationType", "все");

        if (report.getDevelopForm() != null)
            im.put("developForm", report.getDevelopForm());
        else
            im.put("developForm", "все");

        if (report.getCourse() != null)
            im.put("course", report.getCourse());
        else
            im.put("course", "все");

        if (report.getGroups() != null)
            im.put("groups", report.getGroups());
        else
            im.put("groups", "все");

        im.modify(rtf);
    }


    private static class TableData
    {
        private String[] row;
        private Map<Integer, List<MarkData>> debtMap = new HashMap<>();
        private Integer currentTerm;

        private void addDate(Integer termNumber, SessionMarkCatalogItem value, EppRegistryElementPart part, EppGroupTypeFCA controlType)
        {
            List<MarkData> list = debtMap.get(termNumber);
            if (list == null)
            {
                list = new ArrayList<>();
                debtMap.put(termNumber, list);
            }
            list.add(new MarkData(value, part, controlType));
        }

        private void fillRow(Integer termNumber)
        {
            List<MarkData> list = debtMap.get(termNumber);
            if (list == null)
                return;

            List<String> disciplineList = new ArrayList<>();
            List<String> markList = new ArrayList<>();
            for (MarkData markData : list)
            {
                disciplineList.add(markData.part.getRegistryElement().getTitleWithNumber());
                markList.add(markData.controlType.getShortTitle() + " (" + markData.value.getShortTitle() + ")");
            }

            row[5] = StringUtils.join(disciplineList, "\\par ");
            row[6] = StringUtils.join(markList, "\\par ");
            row[7] = "" + disciplineList.size();
        }
    }

    protected static class MarkData
    {
        SessionMarkCatalogItem value;
        EppRegistryElementPart part;
        EppGroupTypeFCA controlType;

        MarkData(SessionMarkCatalogItem value, EppRegistryElementPart part, EppGroupTypeFCA controlType)
        {
            this.value = value;
            this.part = part;
            this.controlType = controlType;
        }
    }

}
