package ru.tandemservice.narfu.component.entrant.EcDistributionPrintProtocol;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.ws.distribution.EnrollmentDistributionEnvironmentNode;
import ru.tandemservice.uniec.ws.distribution.EnrollmentDistributionEnvironmentNode.EntrantNode.EntrantRow;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GroupInfo implements Comparable<GroupInfo> {
    public static final String TARGET_ADMISSION = " поступающих на места, выделенные для целевого приема";
    public static final String NORMAL = "";
    public static final String BEZ_EXAM = " поступающих без вступительных испытаний";
    public static final String VNE_KONKURS = " поступающих вне конкурса";
    public static final List<String> PAR_LIST = Arrays.asList(NORMAL, BEZ_EXAM, VNE_KONKURS, TARGET_ADMISSION);

    public EnrollmentDistributionEnvironmentNode node;
    public DataWrapper category;

    public EducationOrgUnit eou;
    public CompensationType compensationType;

    public List<EntrantRow> list = new ArrayList<EntrantRow>();

    public GroupInfo(EducationOrgUnit eou) {
        this.eou = eou;
    }

    private String _key;

    public String getKey() {
        if (_key == null) {
            _key = new StringBuilder()
                    .append(eou.getDevelopForm().getTitle())
                    .append(eou.getFormativeOrgUnit().getTitle())
                    .append(eou.getEducationLevelHighSchool().getFullTitle())
                    .append(eou.getId())
                    .toString();
        }

        return _key;
    }

    public List<EntrantRow> getList(boolean normal) {
        List<EntrantRow> result = new ArrayList<EntrantRow>();

        for (EntrantRow row : list) {
            if (NORMAL.equals(getKoncursType(row)) == normal)
                result.add(row);
            /*
			boolean hasTA = !StringUtils.isEmpty(row.targetAdmissionKind);
			boolean hasBenefit = !UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION.equals(row.competitionKind);
			if ((hasTA || hasBenefit) == normal)
				continue;

			result.add(row);
			*/
        }

        return result;
    }

    public String[] getArr(EntrantRow row) {

        // Временное решение
        // Почему-то приходит частично заполненный EntrantRow
        if (row.marks == null) {
            return null;
        }
        int markSize = row.marks.size() > 0 ? row.marks.size() : 1;

        String[] arr = new String[8 + markSize];
        arr[1] = row.regNumber;
        arr[2] = row.fio;
        arr[3] = row.finalMark;

        //marks
        for (int i = 0; i < row.marks.size(); i++)
            arr[4 + i] = row.marks.get(i);
        if (row.marks.size() == 0)
            arr[4] = "";

        arr[4 + markSize] = row.averageEduInstitutionMark;
        arr[5 + markSize] = getDocumentString(row.documentState);

        //примечание
        if (isTargetAdmission(row))
            arr[6 + markSize] = getTargetString(row.targetAdmissionKind);
        else if (row.benefitList != null && !row.benefitList.isEmpty()) {
            List<String> list = new ArrayList<>();
            for (String code : row.benefitList)
                list.add(getBenefitString(code));
            arr[6 + markSize] = StringUtils.join(list, ", ");
        }

        arr[7 + markSize] = getEnrollmentString(row.enrollmentState);//решение

        return arr;
    }

    private String getDocumentString(String state) {
        List<EnrollmentDistributionEnvironmentNode.Row> rowList = this.node.documentState.row;
        for (EnrollmentDistributionEnvironmentNode.Row row : rowList)
            if (row.id.equals(state)) return row.title.toLowerCase();
        return null;
    }

    private String getTargetString(String code) {
        List<EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow> rowList = this.node.targetAdmissionKind.row;
        for (EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow row : rowList)
            if (row.id.equals(code)) return row.title;
        return null;
    }

    private String getBenefitString(String code) {
        List<EnrollmentDistributionEnvironmentNode.BenefitNode.BenefitRow> rowList = this.node.benefit.row;
        for (EnrollmentDistributionEnvironmentNode.BenefitNode.BenefitRow row : rowList)
            if (row.id.equals(code)) return row.shortTitle;
        return null;
    }

    private String getEnrollmentString(String code) {
		/* from EnrollmentDistributionServiceDao
	    env.enrollmentState.row.add(new EnrollmentDistributionEnvironmentNode.Row("Рекомендован", "1"));
	    env.enrollmentState.row.add(new EnrollmentDistributionEnvironmentNode.Row("Выбыл", "2"));
	    env.enrollmentState.row.add(new EnrollmentDistributionEnvironmentNode.Row("Зачислен", "3"));
	    */
        if ("1".equals(code) || "3".equals(code))
            return "зачислить";
        else
            return "отклонить";
    }

    public static String getKoncursType(EntrantRow row) {
        if (!StringUtils.isEmpty(row.targetAdmissionKind))
            return TARGET_ADMISSION;

        if (row.competitionKind != null)
            switch (row.competitionKind) {
                case UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES:
                    return BEZ_EXAM;
                case UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION:
                    return VNE_KONKURS;
            }

        return NORMAL;
    }

    public static boolean isTargetAdmission(EntrantRow row) {
        return TARGET_ADMISSION.equals(getKoncursType(row));
    }

    @Override
    public int compareTo(GroupInfo o) {
        return this.getKey().compareTo(o.getKey());
    }
}
