package ru.tandemservice.narfu.component.eduplan.EduPlanBlockVersionLink;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.narfu.entity.EppEduPlanVersionBlockLink;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

@org.tandemframework.core.component.State({@Bind(key = "blockId", binding = "blockId")})
public class Model {
    private Long blockId;
    private DynamicListDataSource<EppEduPlanVersionBlockLink> dataSource;
    private EppEduPlanVersionBlock block;

    public EppEduPlanVersionBlock getBlock() {
        return block;
    }

    public void setBlock(EppEduPlanVersionBlock block) {
        this.block = block;
    }

    public DynamicListDataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Long getBlockId() {
        return blockId;
    }

    public void setBlockId(Long blockId) {
        this.blockId = blockId;
    }
}
