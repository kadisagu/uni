package ru.tandemservice.narfu.base.ext.EcDistribution.ui.List;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.tools.PrivateAccessor;
import ru.tandemservice.narfu.component.entrant.EcDistributionPrintProtocol.Model;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.base.bo.EcDistribution.ui.List.EcDistributionListUI;

import java.util.Set;

public class EcDistributionPubExtUIExt extends UIAddon {
    private EcDistributionListUI parent;

    public EcDistributionPubExtUIExt(IUIPresenter presenter, String name, String componentId) {
        super(presenter, name, componentId);
        this.parent = (EcDistributionListUI) presenter;
    }

    public void onClickPrintProtocol() {
        Set<Long> selectedIds = (Set<Long>) PrivateAccessor.invokePrivateMethod(parent, "getSelectedDistributionIds", new Object[]{});

        if (selectedIds.isEmpty())
            throw new ApplicationException("Не выбрано ни одно распределение");

        DataWrapper category = parent.getSettings().get("distributionCategory");
        CompensationType compensationType = parent.getSettings().get("compensationType");

        this.getActivationBuilder().asCurrent(Model.class.getPackage().getName())
                .parameter("ids", selectedIds)
                .parameter("categoryId", category.getId())
                .parameter("compensationTypeId", compensationType.getId())
                .activate();
    }

}
