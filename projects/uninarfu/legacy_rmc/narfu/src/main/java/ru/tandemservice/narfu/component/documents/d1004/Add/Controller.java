package ru.tandemservice.narfu.component.documents.d1004.Add;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseController;

import java.util.Date;

public class Controller extends DocumentAddBaseController<IDAO, Model> {
    @Override
    public void onClickApply(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        ErrorCollector errors = component.getUserContext().getErrorCollector();
        ((IDAO) getDao()).validate(model, errors);
        if (errors.hasErrors()) {
            return;
        }
        else {
            ((IDAO) getDao()).update(model);
            deactivate(component);
            return;
        }
    }

    public void onChangeEmployeer(IBusinessComponent component) {
        Model model = (Model) component.getModel();
        if (model.isFromPersonalCard())
            model.setEmployer(model.getStudent().getPerson().getWorkPlace());
        else
            model.setEmployer(null);
    }

    public void getLengthSession(IBusinessComponent component) {
        Model model = component.getModel();
        Date startSession = model.getDateStartCertification();
        Date endSession = model.getDateEndCertification();
        if ((startSession != null) & (endSession != null)) {
            model.setContinuance(CoreDateUtils.getDateDifferenceInDays(endSession, startSession) + 1);
        }
        else
            model.setContinuance(0);
    }

    public void onChangeDate(IBusinessComponent component) {
        Model model = component.getModel();
        model.setDateStartCertification(ParagraphData.getDate(model.getStudent(), model.getPeriod(), model.getTerm(), true));
        model.setDateEndCertification(ParagraphData.getDate(model.getStudent(), model.getPeriod(), model.getTerm(), false));
        getLengthSession(component);
    }
}
