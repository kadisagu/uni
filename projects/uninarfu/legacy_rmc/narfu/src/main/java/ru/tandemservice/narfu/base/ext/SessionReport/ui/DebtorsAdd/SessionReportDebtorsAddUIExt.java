package ru.tandemservice.narfu.base.ext.SessionReport.ui.DebtorsAdd;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.DebtorsAdd.SessionReportDebtorsAddUI;

public class SessionReportDebtorsAddUIExt extends UIAddon
{

    private boolean byMark = true;
    private boolean byStatus = true;

    private SessionReportDebtorsAddUI presenter;

    public SessionReportDebtorsAddUIExt(IUIPresenter presenterBase, String name, String componentId)
    {
        super(presenterBase, name, componentId);
        this.presenter = (SessionReportDebtorsAddUI) presenterBase;

    }

    public boolean isByMark()
    {
        return byMark;
    }

    public void setByMark(boolean byMark)
    {
        this.byMark = byMark;
    }

    public boolean isByStatus()
    {
        return byStatus;
    }

    public void setByStatus(boolean byStatus)
    {
        this.byStatus = byStatus;
    }
}
