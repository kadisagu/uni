package ru.tandemservice.narfu.base.ext.Fias.ui.AddressEdit;

import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudentrmc.entity.PersonNARFU;
import ru.tandemservice.narfu.base.ext.Person.ui.Tab.PersonNarfuUtil;

public class DAO extends org.tandemframework.shared.fias.base.bo.Fias.ui.AddressEdit.DAO {
    @Override
    public void prepare(org.tandemframework.shared.fias.base.bo.Fias.ui.AddressEdit.Model model) {
        super.prepare(model);
        if (model.getEntity() instanceof Person) {
            Model myModel = (Model) model;
            PersonNARFU personNARFU = PersonNarfuUtil.getPersonNARFU((Person) model.getEntity());
            myModel.setAddressInfo(personNARFU.getAddressInfo());
        }

    }

    @Override
    public void update(org.tandemframework.shared.fias.base.bo.Fias.ui.AddressEdit.Model model) {
        super.update(model);
        if (model.getEntity() instanceof Person) {
            Model myModel = (Model) model;
            PersonNARFU personNARFU = PersonNarfuUtil.getPersonNARFU((Person) model.getEntity());
            personNARFU.setAddressInfo(myModel.getAddressInfo());
            saveOrUpdate(personNARFU);
        }
    }
}
