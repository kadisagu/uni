package ru.tandemservice.narfu.uniec.component.report.DailyEntrantList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    public void onRefreshComponent(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        model.setPrincipalContext(component.getUserContext().getPrincipalContext());
        ((IDAO) getDao()).prepare(model);
    }

    public void onClickApply(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        ((IDAO) getDao()).createReport(model);

        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(model.getContent(), "Ежедневный список абитуриентов.rtf");
        activateInRoot(component, new ComponentActivator("ru.tandemservice.uni.component.reports.PrintReport", new UniMap().add("id", id)));
    }
}
