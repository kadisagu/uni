package ru.tandemservice.narfu.base.ext.ReportPerson.ui.Add;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.person.base.bo.ReportPerson.ui.Add.ReportPersonAdd;
import ru.tandemservice.narfu.base.bo.ReportPersonNarfu.ui.Add.ReportPersonNarfuAdd;

@Configuration
public class ReportPersonAddExt extends BusinessComponentExtensionManager {

    public static final String PERSONEXT_TAB = "personExtTab";

    @Autowired
    private ReportPersonAdd _reportPersonAdd;

    @Bean
    public TabPanelExtension tabPanelExtension() {
        return tabPanelExtensionBuilder(_reportPersonAdd.tabPanelExtPoint())
                .addTab(componentTab(PERSONEXT_TAB, ReportPersonNarfuAdd.class))
                .create();
    }

}
