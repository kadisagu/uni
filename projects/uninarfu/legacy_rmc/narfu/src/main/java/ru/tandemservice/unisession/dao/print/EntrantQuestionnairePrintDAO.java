package ru.tandemservice.unisession.dao.print;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.RtfParagraph;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import ru.tandemservice.narfu.entity.PersonNextOfKinNARFU;
import ru.tandemservice.unibasermc.util.DeclinationUtil;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.PriorityProfileEduOu;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.ArrayList;
import java.util.List;

public class EntrantQuestionnairePrintDAO extends UniBaseDao implements IEntrantQuestionnairePrintDAO {

    public static IEntrantQuestionnairePrintDAO instance() {
        return (IEntrantQuestionnairePrintDAO) ApplicationRuntime
                .getBean("entrantQuestionnairePrintDAO");
    }

    @Override
    public RtfDocument printEntrantQuestionnaire(Long requestId) {
        RtfDocument rtfDocument = null;
        TemplateDocument template = null;

        try {
            template = getCatalogItem(TemplateDocument.class, "entrantQuestionnaireTemplate");
            rtfDocument = new RtfReader().read(template.getContent());
        }
        catch (Exception e) {
            ContextLocal.getErrorCollector().addError("Шаблон для анкеты абитуриента не найден");
            return null;
        }

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        RtfTableModifier tableModifier = new RtfTableModifier();

        List<String[]> tableRows = new ArrayList<String[]>();
        String[][] tableData = null;
        int rowNumb = 1;

        EntrantRequest entrantRequest = get(EntrantRequest.class, requestId);

        List<RequestedEnrollmentDirection> listofDiections = getList(RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.L_ENTRANT_REQUEST, entrantRequest, new String[]{RequestedEnrollmentDirection.P_PRIORITY});

        Entrant entrant = entrantRequest.getEntrant();

        int numberOfRepeatParagraf = 4;

        int numberOfTableParagraf = 6;


        List<IRtfElement> tableProfileList = new ArrayList<IRtfElement>();
        tableProfileList.add(rtfDocument.getClone().getElementList().get(numberOfTableParagraf));
        tableProfileList.add(rtfDocument.getClone().getElementList().get(numberOfTableParagraf + 1));


        List<IRtfElement> paragraphs = new ArrayList<IRtfElement>();

        //Повтрорение строки "Направление подготовки/специальность..."
        for (RequestedEnrollmentDirection direction : listofDiections) {

            RtfParagraph rtfParagraph = (RtfParagraph) rtfDocument.getClone().getElementList().get(numberOfRepeatParagraf);

            injectModifier.put("speciality", direction.getEnrollmentDirection().getShortTitle());
            injectModifier.put("faculty", direction.getEnrollmentDirection().getEducationOrgUnit().getFormativeOrgUnit().getTitle());
            injectModifier.put("terDivision", direction.getEnrollmentDirection().getEducationOrgUnit().getTerritorialOrgUnit().getTitle());
            injectModifier.put("eduForm", direction.getEnrollmentDirection().getEducationOrgUnit().getDevelopForm().getTitle());
            injectModifier.put("compensationType", direction.getCompensationType().getShortTitle());

            //Таблица "Профиль"
            List<PriorityProfileEduOu> list = getList(PriorityProfileEduOu.class, PriorityProfileEduOu.requestedEnrollmentDirection(), direction, new String[]{PriorityProfileEduOu.P_PRIORITY});
            List<IRtfElement> tableProfileListCopy = null;
            if (list.size() > 0) {
                tableProfileListCopy = new ArrayList<IRtfElement>();
                for (IRtfElement element : tableProfileList) {
                    tableProfileListCopy.add(element.getClone());
                }
                //tableProfileListCopy = tableProfileList;
                tableRows.clear();
                for (PriorityProfileEduOu eduOu : list) {
                    tableRows.add(fillProfileTable(rowNumb++, eduOu));
                }

                tableData = (String[][]) tableRows.toArray(new String[tableRows.size()][]);
                tableModifier.put("profileTable", tableData);

                tableModifier.modify(tableProfileListCopy);
            }

            injectModifier.modify(rtfParagraph.getElementList());
            injectModifier = new RtfInjectModifier();
            paragraphs.add(rtfParagraph);

            if (list.size() > 0)
                paragraphs.addAll(tableProfileListCopy);

        }
        //Удалим параграф  №1
        rtfDocument.getElementList().remove(numberOfRepeatParagraf);
        rtfDocument.getElementList().remove(numberOfRepeatParagraf);
        rtfDocument.getElementList().remove(numberOfRepeatParagraf);

        //Удалим таблицу "Профиль"
        //rtfDocument.getElementList().remove(numberOfTableParagraf);


        rtfDocument.getElementList().addAll(numberOfRepeatParagraf, paragraphs);


        //Таблица "Склонение моих фамилий"
        tableRows.clear();
        tableRows.add(fillDeclinationTable("Именительный", entrant.getPerson(), GrammaCase.NOMINATIVE));
        tableRows.add(fillDeclinationTable("Родительный", entrant.getPerson(), GrammaCase.GENITIVE));
        tableRows.add(fillDeclinationTable("Дательный", entrant.getPerson(), GrammaCase.DATIVE));
        tableRows.add(fillDeclinationTable("Винительный", entrant.getPerson(), GrammaCase.ACCUSATIVE));
        tableRows.add(fillDeclinationTable("Творительный", entrant.getPerson(), GrammaCase.INSTRUMENTAL));
        tableRows.add(fillDeclinationTable("Предложный", entrant.getPerson(), GrammaCase.PREPOSITIONAL));

        tableData = (String[][]) tableRows.toArray(new String[tableRows.size()][]);
        tableModifier.put("declinationTable", tableData);

        injectModifier.put("regNum", StringUtils.trimToEmpty(entrantRequest.getStringNumber()));
        injectModifier.put("fioG", DeclinationUtil.getDeclinableFIO(GrammaCase.GENITIVE, entrant.getPerson().getIdentityCard()));
        injectModifier.put("regDate", RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(entrantRequest.getRegDate()) + " года");


        //Родственник(и)
        List<PersonNextOfKin> nextOfKinList = getList(PersonNextOfKin.class, PersonNextOfKin.L_PERSON, entrant.getPerson());
        if (!nextOfKinList.isEmpty()) {
            PersonNextOfKin personNextOfKin = nextOfKinList.get(0);
            injectModifier.put("relDegree", personNextOfKin.getRelationDegree().getTitle());
            injectModifier.put("relFIO", personNextOfKin.getFullFio());
            try {
                injectModifier.put("adress", personNextOfKin.getAddress().getTitleWithFlat());
            }
            catch (Exception e) {
                injectModifier.put("adress", "");
            }
            injectModifier.put("docType", personNextOfKin.getPerson().getIdentityCard().getCardType().getTitle());
            try {
                injectModifier.put("docNum", (personNextOfKin.getPassportSeria() == null ? "" : personNextOfKin.getPassportSeria()) + "№" + (personNextOfKin.getPassportNumber() == null ? "" : personNextOfKin.getPassportNumber()));
            }
            catch (Exception e) {
                injectModifier.put("docNum", "");
            }
            injectModifier.put("docPlace", personNextOfKin.getPassportIssuancePlace() == null ? "" : personNextOfKin.getPassportIssuancePlace());

            if (personNextOfKin.getPassportIssuanceDate() != null)
                injectModifier.put("issueDate", RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(personNextOfKin.getPassportIssuanceDate()));
            else
                injectModifier.put("issueDate", "");

            injectModifier.put("contPhone", StringUtils.trimToEmpty(personNextOfKin.getPhones()));

            PersonNextOfKinNARFU kinNARFU = get(PersonNextOfKinNARFU.class, PersonNextOfKinNARFU.base(), personNextOfKin);

            if (kinNARFU != null) {
                injectModifier.put("kindINN", StringUtils.trimToEmpty(kinNARFU.getINN()));
                injectModifier.put("kindNStrach", StringUtils.trimToEmpty(kinNARFU.getPensionInsuranceNumber()));
            }
            else {
                injectModifier.put("kindINN", "");
                injectModifier.put("kindNStrach", "");
            }

        }
        else {
            injectModifier.put("relDegree", "");
            injectModifier.put("relFIO", "");
            injectModifier.put("adress", "");
            injectModifier.put("docType", "");
            injectModifier.put("docNum", "");
            injectModifier.put("docPlace", "");
            injectModifier.put("issueDate", "");
            injectModifier.put("contPhone", "");
            injectModifier.put("kindINN", "");
            injectModifier.put("kindNStrach", "");
        }

        tableModifier.modify(rtfDocument);
        injectModifier.modify(rtfDocument);

        return rtfDocument;
    }

    private String[] fillProfileTable(int rowNumb, PriorityProfileEduOu profileEduOu) {
        List<String> result = new ArrayList<String>();

        result.add(String.valueOf(rowNumb));
        result.add(profileEduOu.getProfileEducationOrgUnit().getEducationOrgUnit().getTitle());
        result.add(String.valueOf(profileEduOu.getPriority()));

        return (String[]) result.toArray(new String[result.size()]);
    }

    private String[] fillDeclinationTable(String grammarTitle, Person person, GrammaCase grammaCase) {
        List<String> result = new ArrayList<String>();

        result.add(grammarTitle);
        result.add(DeclinationUtil.getDeclinableLastName(grammaCase, person.getIdentityCard()));
        result.add(DeclinationUtil.getDeclinableFirstName(grammaCase, person.getIdentityCard()));
        result.add(DeclinationUtil.getDeclinableMiddleName(grammaCase, person.getIdentityCard()));
        result.add("верно");

        return (String[]) result.toArray(new String[result.size()]);
    }
}
