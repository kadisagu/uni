package ru.tandemservice.narfu.component.settings.StateExamSubjectPassScore;

import ru.tandemservice.uniec.entity.catalog.StateExamSubject;

public class Model extends ru.tandemservice.uniec.component.settings.StateExamSubjectPassScore.Model {

    public static class SubjectWrapperExt extends SubjectWrapper {
        private static final long serialVersionUID = -5885171355546391292L;

        private Integer markPrev;
        private Integer markPrev2;

        public SubjectWrapperExt(StateExamSubject i, Integer mark, Integer markPrev, Integer markPrev2) throws ClassCastException {
            super(i, mark);
            this.markPrev = markPrev;
            this.markPrev2 = markPrev2;
        }

        public boolean isEmpty() {
            if (getMark() != null && getMark() > 0)
                return false;
            if (getMarkPrev() != null && getMarkPrev() > 0)
                return false;
            if (getMarkPrev2() != null && getMarkPrev2() > 0)
                return false;
            return true;
        }

        public Integer getMarkPrev() {
            return markPrev;
        }

        public void setMarkPrev(Integer markPrev) {
            this.markPrev = markPrev;
        }

        public Integer getMarkPrev2() {
            return markPrev2;
        }

        public void setMarkPrev2(Integer markPrev2) {
            this.markPrev2 = markPrev2;
        }
    }
}
