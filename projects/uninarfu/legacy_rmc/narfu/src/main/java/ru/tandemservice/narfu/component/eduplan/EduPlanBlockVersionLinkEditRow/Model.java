package ru.tandemservice.narfu.component.eduplan.EduPlanBlockVersionLinkEditRow;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.narfu.entity.EppEduPlanVersionBlockLink;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

@Input({@Bind(key = "elementId", binding = "element.id"),
        @Bind(key = "linkId", binding = "link.id")})

public class Model {

    private EntityHolder<EppRegistryElement> element = new EntityHolder<>();
    private EntityHolder<EppEduPlanVersionBlockLink> link = new EntityHolder<>();
    private StaticListDataSource<RowExt> dataSource;
    private ISelectModel disciplineList;

    public ISelectModel getDisciplineList() {
        return disciplineList;
    }

    public void setDisciplineList(ISelectModel disciplineList) {
        this.disciplineList = disciplineList;
    }

    public EntityHolder<EppRegistryElement> getElement() {
        return element;
    }

    public EntityHolder<EppEduPlanVersionBlockLink> getLink() {
        return link;
    }

    public void setLink(EntityHolder<EppEduPlanVersionBlockLink> link) {
        this.link = link;
    }

    public void setElement(EntityHolder<EppRegistryElement> element) {
        this.element = element;
    }

    public StaticListDataSource<RowExt> getDataSource() {
        return dataSource;
    }

    public void setDataSource(StaticListDataSource<RowExt> dataSource) {
        this.dataSource = dataSource;
    }


}
