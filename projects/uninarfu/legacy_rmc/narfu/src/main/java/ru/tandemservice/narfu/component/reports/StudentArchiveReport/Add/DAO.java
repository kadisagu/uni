package ru.tandemservice.narfu.component.reports.StudentArchiveReport.Add;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.sec.entity.Principal;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.base.util.NumberSpellingUtil;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.narfu.entity.NarfuReportStudentArchive;
import ru.tandemservice.narfu.entity.catalog.NarfuReportStudentArchiveType;
import ru.tandemservice.narfu.entity.catalog.codes.NarfuReportStudentArchiveTypeCodes;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        model.setTypeModel(new LazySimpleSelectModel<NarfuReportStudentArchiveType>(NarfuReportStudentArchiveType.class));

        OrgUnit orgUnit = model.getOrgUnitId() != null ? getNotNull(OrgUnit.class, model.getOrgUnitId()) : null;
        model.setOrgUnit(orgUnit);

        IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();
        EmployeePost employeePost = get(EmployeePost.class, EmployeePost.principal(), (Principal) principalContext.getPrincipal());
        if (employeePost != null) {
            model.setDocumentsHandedPost(employeePost.getPostRelation().getPostBoundedWithQGandQL().getTitle());
            model.setDocumentsHandedFIO(employeePost.getEmployee().getPerson().getIdentityCard().getIof());
        }

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(OrgUnit.class, "ou")
                .where(DQLExpressions.eq(DQLExpressions.property(OrgUnit.title().fromAlias("ou")), DQLExpressions.value("Архивный отдел")));
        if (!getList(builder).isEmpty()) {
            OrgUnit archiveOrgUnit = (OrgUnit) getList(builder).get(0);
            EmployeePost post = (EmployeePost) archiveOrgUnit.getHead();
            if (post != null) {
                model.setDocumentsReceivedPost(post.getPostRelation().getPostBoundedWithQGandQL().getTitle());
                model.setDocumentsReceivedFIO(post.getEmployee().getPerson().getIdentityCard().getIof());
            }
            else {
                //если у подразделения нет руководителя, то ищем сотрудника с руководящей должностью и выводим его
                DQLSelectBuilder selectBuilder = new DQLSelectBuilder().fromEntity(EmployeePost.class, "ep")
                        .where(DQLExpressions.eq(DQLExpressions.property(EmployeePost.orgUnit().fromAlias("ep")), DQLExpressions.value(archiveOrgUnit)))
                        .where(DQLExpressions.eq(DQLExpressions.property(EmployeePost.postRelation().headerPost().fromAlias("ep")), DQLExpressions.value(Boolean.TRUE)));
                if (!getList(selectBuilder).isEmpty()) {
                    EmployeePost empPost = (EmployeePost) getList(selectBuilder).get(0);
                    model.setDocumentsReceivedPost(empPost.getPostRelation().getPostBoundedWithQGandQL().getTitle());
                    model.setDocumentsReceivedFIO(empPost.getEmployee().getPerson().getIdentityCard().getIof());
                }
            }
        }
    }

    @Override
    public Long createReport(Model model) {

        if (model.getEducationYear() == null) {
            throw new ApplicationException("Значение поля «За год» должно быть не меньше, чем 1990 и не больше, чем " + String.valueOf(CoreDateUtils.getYear(new Date())));
        }

        if (model.getEducationYear() < 1990)
            throw new ApplicationException("Значение поля «За год» должно быть не меньше, чем 1990");
        if (model.getEducationYear() > CoreDateUtils.getYear(new Date()))
            throw new ApplicationException("Значение поля «За год» должно быть не больше, чем " + String.valueOf(CoreDateUtils.getYear(new Date())));

        NarfuReportStudentArchive report = model.getReport();

        DatabaseFile databaseFile = new DatabaseFile();

        databaseFile.setContent(print(report, model));

        String var = report.getType().getCode().equals(NarfuReportStudentArchiveTypeCodes.EXCLUSION) ? "отчисл." : "выпуск.";

        databaseFile.setFilename(report.getNumber() + "_" + StringUtils.trimToEmpty(var) + ".rtf");
        saveOrUpdate(databaseFile);

        report.setContent(databaseFile);
        report.setFormingDate(new Date());

        saveOrUpdate(report);
        return report.getId();
    }

    public byte[] print(NarfuReportStudentArchive report, Model model) {
        RtfDocument document = loadTemplate();
        RtfInjectModifier im = new RtfInjectModifier();
        RtfTableModifier tm = new RtfTableModifier();

        im.put("number", report.getNumber());
        int yearIntValue = model.getEducationYear();
        im.put("YR_1", String.valueOf(yearIntValue));
        im.put("YR", String.valueOf(CoreDateUtils.getYear(new Date())));

        im.put("stat", report.getType().getCode().equals(NarfuReportStudentArchiveTypeCodes.EXCLUSION) ? "отчисленных" : "выпускников");

        im.put("Master_post", model.getVisaPost());
        im.put("Master", model.getVisaFIO());

        im.put("Arch_post", model.getDocumentsReceivedPost());
        im.put("arch", model.getDocumentsReceivedFIO());

        im.put("div", TopOrgUnit.getInstance().getShortTitle());

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(Student.class, "s")
                .where(DQLExpressions.eqValue(DQLExpressions.property(Student.archival().fromAlias("s")), true));

        FilterUtils.applySelectFilter(builder, "s", Student.educationOrgUnit().groupOrgUnit(), model.getOrgUnit());

        if (report.getType().getCode().equals(NarfuReportStudentArchiveTypeCodes.EXCLUSION))
            FilterUtils.applySelectFilter(builder, "s", Student.status().code(), "8");

        else if (report.getType().getCode().equals(NarfuReportStudentArchiveTypeCodes.GENERAL))
            FilterUtils.applySelectFilter(builder, "s", Student.status().code(), "3");

        Calendar cal = Calendar.getInstance();
        cal.set(yearIntValue, Calendar.JANUARY, 1, 00, 00, 00);
        Date dateFrom = cal.getTime();

        cal.set(yearIntValue, Calendar.DECEMBER, 31, 23, 59, 59);
        Date dateTo = cal.getTime();

        FilterUtils.applyBetweenFilter(builder, "s", Student.archivingDate().s(), dateFrom, dateTo);

        List<Student> students = getList(builder);

        if (students.isEmpty())
            throw new ApplicationException("В отчет не полал ни один студент.");

        List<String[]> studentsRows = new ArrayList<String[]>();

        for (Student student : students) {
            List<String> row = new ArrayList<String>();

            row.add(String.valueOf(students.indexOf(student) + 1));
            row.add(report.getIndex());
            row.add(student.getFio());
            row.add(String.valueOf(student.getEntranceYear()));
            row.add(String.valueOf(student.getFinishYear()));
            row.add("1");

            studentsRows.add(row.toArray(new String[row.size()]));
        }

        im.put("num", "1");
        im.put("num_1", String.valueOf(students.size()));
        im.put("num_1_write", NumberSpellingUtil.spellNumberMasculineGender(students.size()));

        im.put("User", model.getDocumentsHandedFIO());
        im.put("User_post", model.getDocumentsHandedPost());

        IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();
        EmployeePost employeePost = get(EmployeePost.class, EmployeePost.principal(), (Principal) principalContext.getPrincipal());

        im.put("Uch", employeePost != null ? employeePost.getOrgUnit().getTitle() : "");

        tm.put("T", studentsRows.toArray(new String[studentsRows.size()][]));

        im.modify(document);
        tm.modify(document);
        return RtfUtil.toByteArray(document);
    }

    public RtfDocument loadTemplate() {
        TemplateDocument templateDocument = getCatalogItem(TemplateDocument.class, "studentArchiveReport");
        return new RtfReader().read(templateDocument.getContent());
    }
}
