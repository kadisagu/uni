package ru.tandemservice.narfu.component.reports.StudentsByUGS.List;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.narfu.entity.NarfuReportStudentsByUGS;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.unibase.UniBaseUtils;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        ((IDAO) getDao()).prepare(model);
        prepareDataSource(component);
    }

    protected void prepareDataSource(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        if (model.getDataSource() != null) return;
        DynamicListDataSource<NarfuReportStudentsByUGS> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new IndicatorColumn("Иконка", null).defaultIndicator(new IndicatorColumn.Item("report", "Отчет")).setOrderable(false).setClickable(false).setRequired(true));
        dataSource.addColumn(new SimpleColumn("Дата формирования", NarfuReportStudentsByUGS.P_FORMING_DATE, DateFormatter.DATE_FORMATTER_WITH_TIME));
        dataSource.addColumn(new SimpleColumn("Номер", NarfuReportStudentsByUGS.P_TITLE));
        dataSource.addColumn(new IndicatorColumn("Печать", null, "onClickPrintReport")
                                     .defaultIndicator(new IndicatorColumn.Item("printer", "Печать"))
                                     .setImageHeader(false).setDisableSecondSubmit(false).setOrderable(false).setPermissionKey("printGlobalStoredReport"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteReport",
                                              "Удалить отчет?").setPermissionKey("deleteGlobalStoredReport"));
        model.setDataSource(dataSource);
    }

    public void onClickAddReport(IBusinessComponent component) {
        component.createRegion(new ComponentActivator("ru.tandemservice.narfu.component.reports.StudentsByUGS.Add"));
    }

    public void onClickPrintReport(IBusinessComponent component) throws Exception {
        activateInRoot(
                component,
                new ComponentActivator(
                        IUniComponents.DOWNLOAD_STORABLE_REPORT, new UniMap()
                        .add("reportId", component.getListenerParameter())
                        .add("extension", "xls")
                )
        );

    }

    public void onClickDeleteReport(IBusinessComponent component)
    {
        getDao().deleteRow(component);
    }
}
