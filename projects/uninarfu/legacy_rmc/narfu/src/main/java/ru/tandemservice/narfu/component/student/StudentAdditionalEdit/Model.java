package ru.tandemservice.narfu.component.student.StudentAdditionalEdit;

import ru.tandemservice.movestudentrmc.entity.StudentVKRTheme;

public class Model extends ru.tandemservice.unibasermc.component.student.StudentAdditionalEdit.Model
{
    private StudentVKRTheme theme;

    public StudentVKRTheme getTheme() {
        return theme;
    }

    public void setTheme(StudentVKRTheme theme) {
        this.theme = theme;
    }
}
