/* $Id$ */
package ru.tandemservice.narfu.base.ext.EppRegistry.ui.Base;

/**
 * @author Irina Ugfeld
 * @since 11.03.2016
 */
public interface INarfuRegistryListConfig {
    String EDU_PLAN_VERSION_DS = "eduPlanVersionDS";
    String EPP_WORK_PLAN_DS = "eppWorkPlanDS";

    String EDIT_OWNER_BUTTON = "regElements.editOwner";
    String COPY_BUTTON = "regElements.copy";
}