package ru.tandemservice.narfu.migration;

import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.shared.person.catalog.entity.MilitaryOffice;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class MS_narfu_1x0x0_26to27 extends IndependentMigrationScript {

    /**
     * #4106 перенос данных из самописного справочника (EnlistmentOffice) в тандемовский (MilitaryOffice)
     * по умолчанию населенный пункт для военкомата "0 км" с кодом 8900800004900
     */
    @Override
    public void run(DBTool tool) throws Exception {
        //наибольший код в тандемовском справочнике
        IEntityMeta meta = EntityRuntime.getMeta(MilitaryOffice.class);
        String sql = "select max(convert(int,code_p)) from " + meta.getTableName();
        ResultSet rs = tool.getConnection().createStatement().executeQuery(sql);
        int max = 0;
        if (rs.next())
            max = rs.getInt(1);
        rs.close();

        //id для населенного пункта
        PreparedStatement pst = tool.prepareStatement("select id from addressitem_t where code_p =?");
        pst.setString(1, "8900800004900");
        rs = pst.executeQuery();
        Long city = 0L;
        if (rs.next())
            city = rs.getLong(1);
        rs.close();

        if (tool.tableExists("enlistmentoffice_t")) {
            Statement stmt = tool.getConnection().createStatement();
            stmt.execute("select * from enlistmentoffice_t");
            rs = stmt.getResultSet();
            while (rs.next()) {
                long id = EntityIDGenerator.generateNewId(meta.getEntityCode());
                short discriminator = meta.getEntityCode();
                String code = String.valueOf(max + 1);
                String title = rs.getString(4);
                tool.executeUpdate("insert into " + meta.getTableName() + " (id, discriminator, code_p, settlement_id, title_p, USERCODE_P, disableddate_p) values (?,?,?,?,?,?,?)",
                                   id, discriminator, code, city, title, code, null
                );
                max++;
            }
            rs.close();

            tool.dropTable("enlistmentoffice_t", false /* - не удалять, если есть ссылающиеся таблицы */);
            // удалить код сущности
            tool.entityCodes().delete("enlistmentOffice");
        }
    }
}