package ru.tandemservice.narfu.component.enrollmentextract;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.*;
import ru.tandemservice.narfu.entity.EnrollmentOrderNARFU;
import ru.tandemservice.narfu.entity.EntrHighShortExtractExt;
import ru.tandemservice.narfu.utils.PrintUtil;
import ru.tandemservice.uniecaspirantrmc.entity.EntrantScienceParticipant;
import ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.codes.DevelopConditionCodes;
import ru.tandemservice.uni.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;
import ru.tandemservice.uniec.component.enrollmentextract.EnrollmentOrderPrint;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentParagraph;
import ru.tandemservice.uniec.entity.orders.EntrHighShortExtract;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.*;

public class EnrollmentOrderPrintNARFU extends EnrollmentOrderPrint {

    private IUniBaseDao iDao = IUniBaseDao.instance.get();

    @Override
    public RtfDocument createPrintForm(byte[] orderTemplate,
                                       EnrollmentOrder order)
    {
        RtfDocument createPrintForm = super.createPrintForm(orderTemplate, order);

        RtfInjectModifier injectModifier = new RtfInjectModifier();

        injectModifier.put("dateZ", order.getEnrollmentDate() != null ? RussianDateFormatUtils.getDateFormattedWithMonthName(order.getEnrollmentDate()) : "");
        injectFormOrgUnit(order, injectModifier);
        EnrollmentOrderNARFU enrollmentOrderNARFU = iDao.get(EnrollmentOrderNARFU.class, EnrollmentOrderNARFU.enrollmentOrder(), order);
        if (enrollmentOrderNARFU != null) {
            injectModifier.put("Dolj", enrollmentOrderNARFU.getExecutorPost());
            injectModifier.put("FIOR", enrollmentOrderNARFU.getExecutorFIO());
        }
        else {
            injectModifier.put("Dolj", "");
            injectModifier.put("FIOR", "");
        }

        injectModifier.modify(createPrintForm);
        return createPrintForm;
    }

    @Override
    protected void injectParagraphs(RtfDocument document, EnrollmentOrder order) {

        if (order.getType().getCode().equals(UniecDefines.ORDER_TYPE_TARGET_BUDGET))
            injectParagraphsTargetBudget(document, order);
        else if (order.getType().getCode().equals(UniecDefines.ORDER_TYPE_STUDENT_BUDGET)
                || order.getType().getCode().equals(UniecDefines.ORDER_TYPE_STUDENT_BUDGET_SHORT))
            injectParagraphsStudentBudget(document, order);
        else if (order.getType().getCode().equals(UniecDefines.ORDER_TYPE_STUDENT_CONTRACT)
                || order.getType().getCode().equals(UniecDefines.ORDER_TYPE_STUDENT_CONTRACT_SHORT))
            injectParagraphsStudentContract(document, order);
        else if (order.getType().getCode().equals(UniecDefines.ORDER_TYPE_LISTENER_CONTRACT))
            injectParagraphsStudentContract(document, order);
        else if (order.getType().getCode().equals(UniecDefines.ORDER_TYPE_SECOND_HIGH))
            injectParagraphsAspirant(document, order);

        super.injectParagraphs(document, order);
    }

    private void injectParagraphsStudentContract(RtfDocument document, EnrollmentOrder order)
    {
        RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, "PARAGRAPHS");

        if (rtfSearchResult.isFound()) {
            byte[] paragraphTemplate = EcOrderManager.instance().dao().getParagraphTemplate(order.getType());
            RtfDocument parargraphDocument = new RtfReader().read(paragraphTemplate);

            List<IRtfGroup> parList = new ArrayList<IRtfGroup>();

            for (IAbstractParagraph paragraph : order.getParagraphList()) {
                List<EnrollmentExtract> enrollmentExtractList = paragraph.getExtractList();
                if (enrollmentExtractList.size() == 0) {
                    throw new ApplicationException("Пустой параграф №" + paragraph.getNumber() + ".");
                }

                EnrollmentExtract extract = (EnrollmentExtract) enrollmentExtractList.get(0);

                String developFormTitle = extract.getEntity().getEducationOrgUnit().getDevelopForm().getTitle().toLowerCase();

                String developPeriodTitle = extract.getEntity().getEducationOrgUnit().getDevelopPeriod().getTitle();
                String developConditionTitle = getDevelopConditionTitle(extract.getEntity().getEducationOrgUnit().getDevelopCondition());

                EnrollmentOrderNARFU orderNARFU = iDao.get(EnrollmentOrderNARFU.class, EnrollmentOrderNARFU.enrollmentOrder(), order);

                Date dateP = null;
                String numP = null;
                String course = "";
                String category = getStudentCategoryTitle(extract.getEntity().getRequestedEnrollmentDirection().getStudentCategory());
                String educationType = PrintUtil.getEducationOrgUnitLabel(extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel(), GrammaCase.NOMINATIVE);

                if (orderNARFU != null) {
                    dateP = orderNARFU.getProtocolDate();
                    numP = orderNARFU.getProtocolNumber();
                    if (orderNARFU.getCourse() != null)
                        course = orderNARFU.getCourse().getTitle();
                }

                RtfInjectModifier injectModifier = new RtfInjectModifier()
                        .put("parNumber", Integer.toString(paragraph.getNumber()))
                        .put("developForm", developFormTitle)
                        .put("developPeriod", developPeriodTitle)
                        .put("developCondition", developConditionTitle)
                        .put("FIL", extract.getEntity().getEducationOrgUnit().getFormativeOrgUnit().getTitle())
                        .put("educationType_D", StringUtils.capitalize(educationType))
                        .put("dateP", dateP != null ? RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(dateP) : "")
                        .put("numP", numP != null ? numP : "")
                        .put("category", category)
                        .put("course", course);


                RtfTableModifier tableModifier = new RtfTableModifier();

                List<String[]> tableData_student = new ArrayList<String[]>(enrollmentExtractList.size());
                List<String[]> tableData_listener = new ArrayList<String[]>(enrollmentExtractList.size());

                int studentCounter = 1;
                int listenerCounter = 1;

                for (EnrollmentExtract enrollmentExtract : enrollmentExtractList) {
                    PreliminaryEnrollmentStudent preStudent = enrollmentExtract.getEntity();
                    Entrant entrant = preStudent.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant();
                    IdentityCard card = entrant.getPerson().getIdentityCard();

                    Double sumMark = UniecDAOFacade.getEntrantDAO().getSumFinalMarks(enrollmentExtract.getEntity().getRequestedEnrollmentDirection());

                    String[] row = new String[4];
                    row[1] = card.getFullFio();
                    row[2] = String.valueOf(preStudent.getRequestedEnrollmentDirection().getEntrantRequest().getRegNumber());
                    row[3] = DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS.format(sumMark);


                    if (enrollmentExtract.getEntity().getRequestedEnrollmentDirection().getStudentCategory().getCode().equals(StudentCategoryCodes.STUDENT_CATEGORY_STUDENT) ||
                            enrollmentExtract.getEntity().getRequestedEnrollmentDirection().getStudentCategory().getCode().equals(StudentCategoryCodes.STUDENT_CATEGORY_SECONDARY))
                    {
                        tableData_student.add(row);
                        row[0] = String.valueOf(studentCounter++);
                    }
                    else if (enrollmentExtract.getEntity().getRequestedEnrollmentDirection().getStudentCategory().getCode().equals(StudentCategoryCodes.STUDENT_CATEGORY_LISTENER)) {
                        tableData_listener.add(row);
                        row[0] = String.valueOf(listenerCounter++);
                    }

                }


                RtfDocument paragraphPart = parargraphDocument.getClone();

                String itemNumberT = "";
                String itemNumberM = "";

                if (!tableData_student.isEmpty() && !tableData_listener.isEmpty()) {
                    itemNumberT = "1.";
                    itemNumberM = "2.";
                }

                injectModifier.put("itemNumberT", itemNumberT);
                injectModifier.put("itemNumberM", itemNumberM);

                if (!tableData_student.isEmpty())
                    tableModifier.put("T", tableData_student.toArray(new String[tableData_student.size()][]));

                if (!tableData_listener.isEmpty())
                    tableModifier.put("M", tableData_listener.toArray(new String[tableData_listener.size()][]));


                tableModifier.modify(paragraphPart);
                injectModifier.modify(paragraphPart);

                UniRtfUtil.removeTableByName(paragraphPart, "T", true, true);
                UniRtfUtil.removeTableByName(paragraphPart, "M", true, true);

                if (order.getParagraphList().indexOf(paragraph) != (order.getParagraphList().size() - 1))
                    paragraphPart.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));

                RtfUtil.modifySourceList(document.getHeader(), paragraphPart.getHeader(), paragraphPart.getElementList());

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPart.getElementList());
                parList.add(rtfGroup);
            }

            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);

        }

    }

    private void injectFormOrgUnit(EnrollmentOrder order, RtfInjectModifier injectModifier) {
        List<EnrollmentParagraph> parList = (List<EnrollmentParagraph>) order.getParagraphList();

        boolean branch = true;
        boolean college = true;

        Set<String> branchs = new HashSet<String>();
        Set<String> colleges = new HashSet<String>();
        Set<String> developForms = new HashSet<String>();
        Set<String> compTypes = new HashSet<String>();

        for (EnrollmentParagraph paragraph : parList) {
            EducationOrgUnit eduOrgUnit = paragraph.getFirstExtract().getEntity().getEducationOrgUnit();

            if (getBranch(eduOrgUnit.getFormativeOrgUnit()) == null)
                branch = false;
            else
                branchs.add(getBranch(eduOrgUnit.getFormativeOrgUnit()).getGenitiveCaseTitle());

            if (!isCollege(eduOrgUnit.getFormativeOrgUnit()))
                college = false;
            else
                colleges.add(eduOrgUnit.getFormativeOrgUnit().getGenitiveCaseTitle());

            developForms.add(eduOrgUnit.getDevelopForm().getGenCaseTitle());
            if (paragraph.getFirstExtract().getEntity().getCompensationType().isBudget())
                compTypes.add("на места, финансируемые из федерального бюджета");
            else
                compTypes.add("на места по договорам об оказании платных образовательных услуг");
        }

        injectModifier
                .put("developForm", new StringBuilder().append(StringUtils.join(developForms, ", ")).append(" ").append(developForms.size() > 1 ? "форм" : "формы").toString())
                .put("college", !college ? "" : StringUtils.join(colleges, ", "))
                .put("branch", !branch ? "" : new StringBuilder().append(college ? " " : "").append(StringUtils.join(branchs, ", ")).append(" ").toString())
                .put("compType", StringUtils.join(compTypes, ", "));

    }

    public static OrgUnit getBranch(OrgUnit formOU) {

        if (isBranch(formOU))
            return formOU;

        OrgUnit ou = formOU;
        while (ou.getParent() != null) {
            ou = (OrgUnit) ou.getParent();
            if (isBranch(ou))
                return ou;
        }

        return null;
    }

    public static boolean isBranch(OrgUnit ou) {
        return ou.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.BRANCH);
    }

    public static boolean isCollege(OrgUnit ou) {
        return ou.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.COLLEGE);
    }

    private void injectParagraphsStudentBudget(RtfDocument document, EnrollmentOrder order) {
        RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, "PARAGRAPHS");

        if (rtfSearchResult.isFound()) {
            byte[] paragraphTemplate = EcOrderManager.instance().dao().getParagraphTemplate(order.getType());
            RtfDocument parargraphDocument = new RtfReader().read(paragraphTemplate);

            List<IRtfGroup> parList = new ArrayList<IRtfGroup>();

            for (IAbstractParagraph paragraph : order.getParagraphList()) {
                List<EnrollmentExtract> enrollmentExtractList = (List<EnrollmentExtract>) paragraph.getExtractList();
                if (enrollmentExtractList.size() == 0) {
                    throw new ApplicationException("Пустой параграф №" + paragraph.getNumber() + ".");
                }

                EnrollmentExtract extract = (EnrollmentExtract) enrollmentExtractList.get(0);

                String developFormTitle = extract.getEntity().getEducationOrgUnit().getDevelopForm().getTitle().toLowerCase();

                String developPeriodTitle = extract.getEntity().getEducationOrgUnit().getDevelopPeriod().getTitle();
                String developConditionTitle = getDevelopConditionTitle(extract.getEntity().getEducationOrgUnit().getDevelopCondition());

                EnrollmentOrderNARFU orderNARFU = iDao.get(EnrollmentOrderNARFU.class, EnrollmentOrderNARFU.enrollmentOrder(), order);
                Date dateP = null;
                String numP = null;
                String course = "";
                String category = getStudentCategoryTitle(extract.getEntity().getRequestedEnrollmentDirection().getStudentCategory());
                String educationType = PrintUtil.getEducationOrgUnitLabel(extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel(), GrammaCase.NOMINATIVE);

                if (orderNARFU != null) {
                    dateP = orderNARFU.getProtocolDate();
                    numP = orderNARFU.getProtocolNumber();
                    if (orderNARFU.getCourse() != null)
                        course = orderNARFU.getCourse().getTitle();
                }

                RtfInjectModifier injectModifier = new RtfInjectModifier()
                        .put("parNumber", Integer.toString(paragraph.getNumber()))
                        .put("developForm", developFormTitle)
                        .put("developPeriod", developPeriodTitle)
                        .put("developCondition", developConditionTitle)
                        .put("FIL", extract.getEntity().getEducationOrgUnit().getFormativeOrgUnit().getTitle())
                        .put("educationType_D", StringUtils.capitalize(educationType))
                        .put("dateP", dateP != null ? RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(dateP) : "")
                        .put("numP", numP != null ? numP : "")
                        .put("category", category)
                        .put("course", course);

                RtfTableModifier tableModifier = new RtfTableModifier();

                List<String[]> tableData = new ArrayList<String[]>(enrollmentExtractList.size());


                for (EnrollmentExtract enrollmentExtract : enrollmentExtractList) {
                    PreliminaryEnrollmentStudent preStudent = enrollmentExtract.getEntity();
                    Entrant entrant = preStudent.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant();
                    IdentityCard card = entrant.getPerson().getIdentityCard();

                    Double sumMark = UniecDAOFacade.getEntrantDAO().getSumFinalMarks(enrollmentExtract.getEntity().getRequestedEnrollmentDirection());

                    String[] row = new String[4];

                    row[0] = String.valueOf(enrollmentExtractList.indexOf(enrollmentExtract) + 1);
                    row[1] = card.getFullFio();
                    row[2] = String.valueOf(preStudent.getRequestedEnrollmentDirection().getEntrantRequest().getRegNumber());
                    row[3] = DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS.format(sumMark);

                    tableData.add(row);

                }

                RtfDocument paragraphPart = parargraphDocument.getClone();
                tableModifier.put("T", tableData.toArray(new String[tableData.size()][]));

                tableModifier.modify(paragraphPart);
                injectModifier.modify(paragraphPart);

                RtfUtil.modifySourceList(document.getHeader(), paragraphPart.getHeader(), paragraphPart.getElementList());

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPart.getElementList());
                parList.add(rtfGroup);
            }

            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    protected void injectParagraphsAspirant(RtfDocument document, EnrollmentOrder order) {

        RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, "PARAGRAPHS");

        if (rtfSearchResult.isFound()) {
            byte[] paragraphTemplate = EcOrderManager.instance().dao().getParagraphTemplate(order.getType());
            RtfDocument parargraphDocument = new RtfReader().read(paragraphTemplate);

            List<IRtfGroup> parList = new ArrayList<IRtfGroup>();

            for (IAbstractParagraph paragraph : order.getParagraphList()) {
                List<EnrollmentExtract> enrollmentExtractList = paragraph.getExtractList();
                if (enrollmentExtractList.size() == 0) {
                    throw new ApplicationException("Пустой параграф №" + paragraph.getNumber() + ".");
                }

                List<EntrHighShortExtractExt> extractExtList = iDao.getList(new DQLSelectBuilder().fromEntity(EntrHighShortExtractExt.class, "e")
                                                                                    .where(DQLExpressions.in(DQLExpressions.property(EntrHighShortExtractExt.extract().fromAlias("e")), enrollmentExtractList)));

                Map<EntrHighShortExtract, EntrHighShortExtractExt> extractExtMap = new HashMap<EntrHighShortExtract, EntrHighShortExtractExt>();

                for (EntrHighShortExtractExt extractExt : extractExtList) {
                    extractExtMap.put(extractExt.getExtract(), extractExt);
                }
                List<Long> entrantIds = new ArrayList<Long>();
                for (EnrollmentExtract enrollmentExtract : enrollmentExtractList) {
                    Long entrantId = enrollmentExtract.getEntity().getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getId();
                    entrantIds.add(entrantId);
                }

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EntrantScienceParticipant.class, "e")
                        .where(DQLExpressions.in(DQLExpressions.property(EntrantScienceParticipant.entrantAspirantData().entrant().id().fromAlias("e")), entrantIds))
                        .column(EntrantScienceParticipant.entrantAspirantData().entrant().id().fromAlias("e").s())
                        .column(EntrantScienceParticipant.person().fromAlias("e").s());

                List<Object[]> objects = iDao.getList(builder);

                Map<Long, List<Person>> participantMap = new HashMap<Long, List<Person>>();
                List<Long> personIds = new ArrayList<Long>();
                for (Object[] obj : objects) {
                    Long entrantId = (Long) obj[0];
                    Person participant = (Person) obj[1];
                    personIds.add(participant.getId());

                    List<Person> list = participantMap.get(entrantId);
                    if (list == null)
                        list = new ArrayList<Person>();
                    list.add(participant);
                    participantMap.put(entrantId, list);
                }

                DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(PersonAcademicStatus.class, "status")
                        .where(DQLExpressions.in(
                                DQLExpressions.property(PersonAcademicStatus.person().id().fromAlias("status")),
                                personIds
                        ));
                dql.order(DQLExpressions.property(PersonAcademicStatus.academicStatus().title().fromAlias("status")));

                List<PersonAcademicStatus> statuses = iDao.getList(dql);

                Map<Long, List<String>> statusMap = new HashMap<Long, List<String>>();

                for (PersonAcademicStatus status : statuses) {
                    Long personId = status.getPerson().getId();
                    String st = status.getAcademicStatus().getTitle();

                    List<String> lst = statusMap.get(personId);

                    if (lst == null)
                        lst = new ArrayList<String>();

                    lst.add(st);

                    statusMap.put(personId, lst);
                }

                dql = new DQLSelectBuilder().fromEntity(PersonAcademicDegree.class, "degree")
                        .where(DQLExpressions.in(
                                DQLExpressions.property(PersonAcademicDegree.person().id().fromAlias("degree")),
                                personIds
                        ));
                dql.order(DQLExpressions.property(PersonAcademicDegree.academicDegree().title().fromAlias("degree")));

                List<PersonAcademicDegree> degrees = iDao.getList(dql);

                Map<Long, List<String>> degreeMap = new HashMap<Long, List<String>>();

                for (PersonAcademicDegree degree : degrees) {
                    Long personId = degree.getPerson().getId();
                    String d = degree.getAcademicDegree().getShortTitle();

                    List<String> lst = degreeMap.get(personId);

                    if (lst == null)
                        lst = new ArrayList<String>();

                    lst.add(d);

                    degreeMap.put(personId, lst);
                }


                EnrollmentExtract extract = (EnrollmentExtract) enrollmentExtractList.get(0);
                EntrHighShortExtractExt extractExt = extractExtMap.get(extract);

                String developPeriodTitle = extract.getEntity().getEducationOrgUnit().getDevelopPeriod().getTitle();

                EnrollmentOrderNARFU orderNARFU = iDao.get(EnrollmentOrderNARFU.class, EnrollmentOrderNARFU.enrollmentOrder(), order);
                Date dateP = null;
                String numP = null;
                if (orderNARFU != null) {
                    dateP = orderNARFU.getProtocolDate();
                    numP = orderNARFU.getProtocolNumber();
                }

                RtfInjectModifier injectModifier = new RtfInjectModifier()
                        .put("parNumber", Integer.toString(paragraph.getNumber()))
                        .put("okso", extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject().getSubjectCode())
                        .put("eduProgramSubject", extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject().getTitle())
                        .put("specialization", extractExt == null ? "" : extractExt.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSpecialization().getTitle())
                        .put("developPeriod", developPeriodTitle)
                        .put("dateP", dateP != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(dateP) : "")
                        .put("numP", numP != null ? numP : "");

                RtfTableModifier tableModifier = new RtfTableModifier();

                List<String[]> tableData = new ArrayList<String[]>(enrollmentExtractList.size());

                int counter = 1;

                for (EnrollmentExtract enrollmentExtract : enrollmentExtractList) {
                    PreliminaryEnrollmentStudent preStudent = enrollmentExtract.getEntity();
                    Entrant entrant = preStudent.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant();
                    IdentityCard card = entrant.getPerson().getIdentityCard();
                    extractExt = extractExtMap.get(extract);
                    Double sumMark = UniecDAOFacade.getEntrantDAO().getSumFinalMarks(enrollmentExtract.getEntity().getRequestedEnrollmentDirection());
                    List<Person> list = participantMap.get(entrant.getId());
                    List<String> col6 = new ArrayList<String>();
                    for (Person person : list) {
                        StringBuilder str = new StringBuilder();
                        str.append(person.getIdentityCard().getFullFio());

                        List<String> st = statusMap.get(person.getId());
                        if (st != null && !st.isEmpty())
                            str.append(", ")
                                    .append(StringUtils.join(st, ", "));

                        List<String> d = degreeMap.get(person.getId());
                        if (d != null && !d.isEmpty())
                            str.append(", ")
                                    .append(StringUtils.join(d, ", "));
                        col6.add(str.toString());
                    }

                    String[] row = new String[6];

                    row[0] = String.valueOf(counter++);
                    row[1] = card.getFullFio();
                    row[2] = String.valueOf(preStudent.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getPersonalNumber());
                    row[3] = DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS.format(sumMark);
                    row[4] = extractExt.getEducationOrgUnit().getEducationLevelHighSchool().getOrgUnit().getTitle();
                    row[5] = StringUtils.join(col6, "; ");

                    tableData.add(row);
                }

                RtfDocument paragraphPart = parargraphDocument.getClone();

                if (!tableData.isEmpty())
                    tableModifier.put("T", tableData.toArray(new String[tableData.size()][]));

                tableModifier.modify(paragraphPart);
                injectModifier.modify(paragraphPart);

                RtfUtil.modifySourceList(document.getHeader(), paragraphPart.getHeader(), paragraphPart.getElementList());

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPart.getElementList());
                parList.add(rtfGroup);
            }

            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    protected void injectParagraphsTargetBudget(RtfDocument document, EnrollmentOrder order) {

        RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, "PARAGRAPHS");

        if (rtfSearchResult.isFound()) {
            byte[] paragraphTemplate = EcOrderManager.instance().dao().getParagraphTemplate(order.getType());
            RtfDocument parargraphDocument = new RtfReader().read(paragraphTemplate);

            List<IRtfGroup> parList = new ArrayList<IRtfGroup>();

            for (IAbstractParagraph paragraph : order.getParagraphList()) {
                List<EnrollmentExtract> enrollmentExtractList = paragraph.getExtractList();
                if (enrollmentExtractList.size() == 0) {
                    throw new ApplicationException("Пустой параграф №" + paragraph.getNumber() + ".");
                }

                EnrollmentExtract extract = (EnrollmentExtract) enrollmentExtractList.get(0);

                String developFormTitle = extract.getEntity().getEducationOrgUnit().getDevelopForm().getTitle().toLowerCase();

                String developPeriodTitle = extract.getEntity().getEducationOrgUnit().getDevelopPeriod().getTitle();
                String developConditionTitle = getDevelopConditionTitle(extract.getEntity().getEducationOrgUnit().getDevelopCondition());
                String educationType = PrintUtil.getEducationOrgUnitLabel(extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel(), GrammaCase.NOMINATIVE);

                EnrollmentOrderNARFU orderNARFU = iDao.get(EnrollmentOrderNARFU.class, EnrollmentOrderNARFU.enrollmentOrder(), order);
                Date dateP = null;
                String numP = null;
                if (orderNARFU != null) {
                    dateP = orderNARFU.getProtocolDate();
                    numP = orderNARFU.getProtocolNumber();
                }

                RtfInjectModifier injectModifier = new RtfInjectModifier()
                        .put("parNumber", Integer.toString(paragraph.getNumber()))
                        .put("developForm", developFormTitle)
                        .put("developPeriod", developPeriodTitle)
                        .put("developCondition", developConditionTitle)
                        .put("FIL", extract.getEntity().getEducationOrgUnit().getFormativeOrgUnit().getTitle())
                        .put("educationType_D", educationType)
                        .put("dateP", dateP != null ? RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(dateP) : "")
                        .put("numP", numP != null ? numP : "");

                RtfTableModifier tableModifier = new RtfTableModifier();

                List<String[]> tableData_BEYOND_COMPETITION = new ArrayList<String[]>(enrollmentExtractList.size());
                List<String[]> tableData_TARGET_ADMISSION = new ArrayList<String[]>(enrollmentExtractList.size());

                int beyondCompCounter = 1;
                int targetCompCounter = 1;

                for (EnrollmentExtract enrollmentExtract : enrollmentExtractList) {
                    PreliminaryEnrollmentStudent preStudent = enrollmentExtract.getEntity();
                    Entrant entrant = preStudent.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant();
                    IdentityCard card = entrant.getPerson().getIdentityCard();
                    RequestedEnrollmentDirection direction = preStudent.getRequestedEnrollmentDirection();

                    CompetitionKind compKind = direction.getCompetitionKind();

                    Double sumMark = UniecDAOFacade.getEntrantDAO().getSumFinalMarks(enrollmentExtract.getEntity().getRequestedEnrollmentDirection());

                    String[] row = new String[5];


                    if (direction.isTargetAdmission()) {
                        TargetAdmissionKind targetAdmissionKind = preStudent.getRequestedEnrollmentDirection().getTargetAdmissionKind();
                        row[4] = targetAdmissionKind.getTitle();
                        row[0] = String.valueOf(targetCompCounter++);

                    }
                    else {
                        IUniBaseDao iCoreDao = IUniBaseDao.instance.get();

                        List<String> comment = new ArrayList<String>();

                        if (isSpecialRights(direction)) {
                            List<PersonBenefit> benefits = iCoreDao.getList(PersonBenefit.class, PersonBenefit.person(), entrant.getPerson());
                            if (!benefits.isEmpty())
                                for (PersonBenefit benefit : benefits)
                                    comment.add(benefit.getBenefit().getShortTitle());
                        }
                        else if (compKind.getCode().equals(UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES)) {
                            List<OlympiadDiploma> diplomas = getOlympiadDiplomas(entrant);
                            for (OlympiadDiploma olympiadDiploma : diplomas) {
                                comment.add(olympiadDiploma.getOlympiad());
                            }

                            List<PersonSportAchievement> sports = iCoreDao.getList(PersonSportAchievement.class, PersonSportAchievement.person(), entrant.getPerson());
                            for (PersonSportAchievement sport : sports) {
                                comment.add(sport.getSportKind().getTitle());
                            }
                        }

                        row[4] = StringUtils.join(comment, ", ");
                        row[0] = String.valueOf(beyondCompCounter++);

                    }

                    //row[0] = String.valueOf(enrollmentExtractList.indexOf(enrollmentExtract)+1);
                    row[1] = card.getFullFio();
                    row[2] = String.valueOf(preStudent.getRequestedEnrollmentDirection().getEntrantRequest().getRegNumber());
                    row[3] = DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS.format(sumMark);


                    //цп
                    if (direction.isTargetAdmission())
                        tableData_TARGET_ADMISSION.add(row);
                    else
                        tableData_BEYOND_COMPETITION.add(row);

                }


                RtfDocument paragraphPart = parargraphDocument.getClone();

                String itemNumberT = "";
                String itemNumberM = "";

                if (!tableData_BEYOND_COMPETITION.isEmpty() && !tableData_TARGET_ADMISSION.isEmpty()) {
                    itemNumberT = "1.";
                    itemNumberM = "2.";
                }

                injectModifier.put("itemNumberT", itemNumberT);
                injectModifier.put("itemNumberM", itemNumberM);

                if (!tableData_BEYOND_COMPETITION.isEmpty())
                    tableModifier.put("T", tableData_BEYOND_COMPETITION.toArray(new String[tableData_BEYOND_COMPETITION.size()][]));

                if (!tableData_TARGET_ADMISSION.isEmpty())
                    tableModifier.put("M", tableData_TARGET_ADMISSION.toArray(new String[tableData_TARGET_ADMISSION.size()][]));

                tableModifier.modify(paragraphPart);
                injectModifier.modify(paragraphPart);

                UniRtfUtil.removeTableByName(paragraphPart, "T", true, false);
                UniRtfUtil.removeTableByName(paragraphPart, "M", true, true);

                RtfUtil.modifySourceList(document.getHeader(), paragraphPart.getHeader(), paragraphPart.getElementList());

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPart.getElementList());
                parList.add(rtfGroup);
            }

            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    public static List<OlympiadDiploma> getOlympiadDiplomas(Entrant entrant) {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(OlympiadDiploma.class, "o")
                .column("o")
                .joinEntity("o", DQLJoinType.left, Discipline2OlympiadDiplomaRelation.class, "rel",
                            DQLExpressions.eq(
                                    DQLExpressions.property(OlympiadDiploma.id().fromAlias("o")),
                                    DQLExpressions.property(Discipline2OlympiadDiplomaRelation.diploma().id().fromAlias("rel"))))
                .where(DQLExpressions.eqValue(DQLExpressions.property(OlympiadDiploma.entrant().id().fromAlias("o")), entrant.getId()))
                .where(DQLExpressions.isNull("rel"));

        return IUniBaseDao.instance.get().getList(builder);
    }

    public static boolean isSpecialRights(RequestedEnrollmentDirection direction) {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirectionExt.class, "ext")
                .where(DQLExpressions.eqValue(DQLExpressions.property(RequestedEnrollmentDirectionExt.requestedEnrollmentDirection().id().fromAlias("ext")), direction.getId()))
                .where(DQLExpressions.eqValue(DQLExpressions.property(RequestedEnrollmentDirectionExt.haveSpecialRights().fromAlias("ext")), Boolean.TRUE));

        return IUniBaseDao.instance.get().getCount(builder) > 0;
    }

    public static String getDevelopConditionTitle(DevelopCondition developCondition) {

        if (developCondition.getCode().equals(DevelopConditionCodes.FULL_PERIOD))
            return "";

        return new String(" (" + developCondition.getTitle().toLowerCase() + ")");
    }

    private String getStudentCategoryTitle(StudentCategory studentCategory) {

        String studentCategoryTitle = "";

        if (studentCategory.getCode().equals(StudentCategoryCodes.STUDENT_CATEGORY_STUDENT))
            studentCategoryTitle = "студентов";
        if (studentCategory.getCode().equals(StudentCategoryCodes.STUDENT_CATEGORY_LISTENER))
            studentCategoryTitle = "слушателей";

        return studentCategoryTitle;
    }

}
