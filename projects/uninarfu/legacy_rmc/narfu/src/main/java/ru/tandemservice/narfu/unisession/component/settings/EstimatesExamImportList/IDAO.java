package ru.tandemservice.narfu.unisession.component.settings.EstimatesExamImportList;

import ru.tandemservice.uni.dao.IUniDao;

public abstract interface IDAO extends IUniDao<Model> {
    public abstract byte[] getPrintImportedFileRequest(long paramLong);

    public abstract byte[] getPrintResultFileRequest(long paramLong);
}