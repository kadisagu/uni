package ru.tandemservice.narfu.component.reports.studentsGroupsList.Add;

public class Model extends ru.tandemservice.uni.component.reports.studentsGroupsList.Add.Model {
    private boolean studentBookNumberReq;

    public boolean isStudentBookNumberReq() {
        return studentBookNumberReq;
    }

    public void setStudentBookNumberReq(boolean studentBookNumberReq) {
        this.studentBookNumberReq = studentBookNumberReq;
    }
}
