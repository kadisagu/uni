package ru.tandemservice.narfu.component.studentmassprint.MassPrint;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.unibase.UniBaseUtils;

public class Controller extends ru.tandemservice.unirmc.component.studentmassprint.MassPrint.Controller
{

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = component.getModel();
        model.setSettings(UniBaseUtils.getDataSettings(component, getSettingsKey(model)));
        super.onRefreshComponent(component);
    }

    @Override
    public void onSearchParamsChange(final IBusinessComponent component) {
        ((IDAO) getDao()).prepare((Model) component.getModel());
    }
}
