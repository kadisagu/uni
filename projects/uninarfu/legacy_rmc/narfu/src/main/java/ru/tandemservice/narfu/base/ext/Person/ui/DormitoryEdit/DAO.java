package ru.tandemservice.narfu.base.ext.Person.ui.DormitoryEdit;

import ru.tandemservice.movestudentrmc.entity.PersonNARFU;


public class DAO extends org.tandemframework.shared.person.base.bo.Person.ui.DormitoryEdit.DAO
{

    @Override
    public void prepare(org.tandemframework.shared.person.base.bo.Person.ui.DormitoryEdit.Model model)
    {
        super.prepare(model);

        Model myModel = (Model) model;

        PersonNARFU personNARFU = get(PersonNARFU.class, PersonNARFU.L_PERSON, model.getPerson());
        if (personNARFU != null) {
            myModel.setNeedDormitoryOnReceiptPeriod(personNARFU.isNeedDormitoryOnReceiptPeriod());
        }
    }


    @Override
    public void update(org.tandemframework.shared.person.base.bo.Person.ui.DormitoryEdit.Model model)
    {
        super.update(model);

        Model myModel = (Model) model;

        PersonNARFU personNARFU = get(PersonNARFU.class, PersonNARFU.L_PERSON, model.getPerson());
        if (personNARFU == null) {
            personNARFU = new PersonNARFU();
            personNARFU.setPerson(model.getPerson());
        }

        personNARFU.setNeedDormitoryOnReceiptPeriod(myModel.isNeedDormitoryOnReceiptPeriod());

        getSession().saveOrUpdate(personNARFU);
    }

}
