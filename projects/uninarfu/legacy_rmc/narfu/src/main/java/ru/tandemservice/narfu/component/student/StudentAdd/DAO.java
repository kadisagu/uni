package ru.tandemservice.narfu.component.student.StudentAdd;

import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudentrmc.entity.PersonNARFU;
import ru.tandemservice.uni.component.student.StudentAdd.Model;

public class DAO extends ru.tandemservice.uni.component.student.StudentAdd.DAO {
    @Override
    public void create(Model model)
    {
        super.create(model);
        Person person = get(Person.class, model.getPerson().getId());
        PersonNARFU personExt = new PersonNARFU();
        personExt.setNeedSocialPayment(false);
        personExt.setPerson(person);
        saveOrUpdate(personExt);
    }
}
