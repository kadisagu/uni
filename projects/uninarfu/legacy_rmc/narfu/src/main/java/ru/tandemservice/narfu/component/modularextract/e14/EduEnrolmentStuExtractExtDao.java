package ru.tandemservice.narfu.component.modularextract.e14;

import ru.tandemservice.movestudent.entity.EduEnrolmentStuExtractExt;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Map;

public class EduEnrolmentStuExtractExtDao extends ru.tandemservice.movestudent.component.modularextract.e14.EduEnrolmentStuExtractExtDao {

    @Override
    public void doCommit(EduEnrolmentStuExtractExt extract, Map parameters) {
        //сохраняем дефолтную логику:
        super.doCommit(extract, parameters);

        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder()) {
            return;
        }

        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData) {
            //что-то не то, в базовом классе нужно было его создать. Ничего не делаем
            return;
        }

        //а теперь ставим нужную дату зачисления и сохраняем :
        orderData.setEduEnrollmentOrderEnrDate(extract.getEntryDate());
        getSession().saveOrUpdate(orderData);

    }
}
