package ru.tandemservice.narfu.component.studentmassprint.printDAO.grants;

import ru.tandemservice.uni.entity.employee.Student;

import java.util.List;

public interface IStudentGrantReqestGenerator<Model> {
    public abstract byte[] generateReport(List<Student> students, byte[] template, Model model);

    public abstract int getStudentPerPage();

    public abstract String getParamPage();
}
