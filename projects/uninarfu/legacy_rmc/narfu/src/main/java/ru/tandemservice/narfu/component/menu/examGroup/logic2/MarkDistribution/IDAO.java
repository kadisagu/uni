package ru.tandemservice.narfu.component.menu.examGroup.logic2.MarkDistribution;

import ru.tandemservice.uniec.component.menu.examGroup.logic2.MarkDistribution.Model;

public interface IDAO
        extends
        ru.tandemservice.uniec.component.menu.examGroup.logic2.MarkDistribution.IDAO
{

    public void prepareListDataSourceExt(Model model);

}
