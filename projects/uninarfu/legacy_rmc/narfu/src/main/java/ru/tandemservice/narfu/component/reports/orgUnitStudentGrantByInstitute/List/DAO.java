package ru.tandemservice.narfu.component.reports.orgUnitStudentGrantByInstitute.List;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab.MonthWrapper;
import ru.tandemservice.narfu.entity.NarfuReportStudentGrantByInstitute;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.Date;
import java.util.List;

public class DAO extends ru.tandemservice.narfu.component.reports.orgUnitStudentGrantByInstitute.Add.DAO implements IDAO {

    @Override
    public void prepare(ru.tandemservice.narfu.component.reports.orgUnitStudentGrantByInstitute.Add.Model model) {
        super.prepare(model);
    }

    @Override
    public void prepareListDataSource(ru.tandemservice.narfu.component.reports.orgUnitStudentGrantByInstitute.Add.Model baseModel) {
        Model model = (Model) baseModel;
        EducationYear year = model.getSettings().get("educationYear");
        YearDistributionPart part = model.getSettings().get("part");
        DevelopForm developForm = model.getSettings().get("developForm");

        Date month = model.getSettings().get("month");
        MonthWrapper.Data monthData = month != null ? MonthWrapper.getInstance(month) : null;

        MQBuilder builder = new MQBuilder(NarfuReportStudentGrantByInstitute.ENTITY_CLASS, "r")
                .add(MQExpression.eq("r", NarfuReportStudentGrantByInstitute.educationYear(), year == null ? null : year.getTitle()))
                .add(MQExpression.eq("r", NarfuReportStudentGrantByInstitute.educationYearPart(), part == null ? null : part.getTitle()))
                .add(MQExpression.eq("r", NarfuReportStudentGrantByInstitute.month(), monthData == null ? null : monthData.monthWrapper.getStringValue(monthData.educationYear)));

        if (developForm != null)
            builder.add(MQExpression.eq("r", NarfuReportStudentGrantByInstitute.developForm(), developForm.getTitle()));

        if (model.getDataSource().getEntityOrder() != null)
            builder.addOrder("r", model.getDataSource().getEntityOrder().getKeyString(), model.getDataSource().getEntityOrder().getDirection());

        List<NarfuReportStudentGrantByInstitute> list = builder.getResultList(getSession());
        UniBaseUtils.createPage(model.getDataSource(), list);
    }
}
