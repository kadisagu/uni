package ru.tandemservice.narfu.base.ext.EcOrder;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.narfu.base.ext.EcOrder.logic.EcOrderDaoExt;
import ru.tandemservice.narfu.base.ext.EcOrder.logic.EcOrderExtDao;
import ru.tandemservice.narfu.base.ext.EcOrder.logic.IEcOrderDaoExt;
import ru.tandemservice.uniec.base.bo.EcOrder.logic.IEcOrderDao;

@Configuration
public class EcOrderManagerExt extends BusinessObjectExtensionManager {


    public static EcOrderManagerExt instance() {
        return instance(EcOrderManagerExt.class);
    }

    @Bean
    @BeanOverride
    public IEcOrderDao dao()
    {
        return new EcOrderExtDao();
    }

    @Bean
    public IEcOrderDaoExt daoExt()
    {
        return new EcOrderDaoExt();
    }

}
