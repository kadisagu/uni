package ru.tandemservice.narfu.unisession.component.orgunit.SessionBulletinListTab;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.rtf.RtfDocumentRenderer;
import ru.tandemservice.unisession.dao.print.INarfuSessionBulletinPrintDAO;
import ru.tandemservice.unisession.dao.print.NarfuSessionBulletinPrintDAO;
import ru.tandemservice.uni.util.ReportRenderer;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentPrintVersion;

import java.util.*;

public class DAO extends ru.tandemservice.unisession.component.orgunit.SessionBulletinListTab.DAO implements IDAO
{

    @Override
    public void doPrintBulletin(Long id) {

        SessionBulletinDocument bulletin = get(SessionBulletinDocument.class, id);
        if (bulletin.isClosed()) {
            SessionDocumentPrintVersion rel = get(SessionDocumentPrintVersion.class, SessionDocumentPrintVersion.doc().id().s(), id);
            if (null != rel) {
                byte[] content = rel.getContent();
                BusinessComponentUtils.downloadDocument(new ReportRenderer("Ведомость.rtf", content), true);
                return;
            }
        }

        NarfuSessionBulletinPrintDAO.ReportFile result = INarfuSessionBulletinPrintDAO.instance.get().printBulletinListNarfu(Collections.singleton(id));
        BusinessComponentUtils.downloadDocument(new RtfDocumentRenderer(result.getName(), result.getContent()), true);
    }

    @Override
    public boolean isNullProperty(IEntity document, String property) {

        return document.getProperty(property) == null;
    }

    @Override
    public Map<String, List<IEntity>> getNullPropertyDocs(List<IEntity> docs, String... properies) {

        Map<String, List<IEntity>> result = new HashMap<>();

        for (IEntity doc : docs) {
            for (String property : properies) {
                if (isNullProperty(doc, property)) {
                    SafeMap.safeGet(result, property, ArrayList.class).add(doc);
                }
            }
        }

        return result;
    }


}
