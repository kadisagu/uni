package ru.tandemservice.unisession.dao.print;

import org.tandemframework.rtf.document.RtfDocument;

public interface IEntrantQuestionnairePrintDAO {

    public RtfDocument printEntrantQuestionnaire(Long listenerParameter);
}
