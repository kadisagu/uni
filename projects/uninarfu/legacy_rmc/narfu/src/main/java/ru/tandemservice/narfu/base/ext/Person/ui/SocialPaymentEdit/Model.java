package ru.tandemservice.narfu.base.ext.Person.ui.SocialPaymentEdit;

import org.tandemframework.core.component.Input;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudentrmc.entity.PersonNARFU;
import ru.tandemservice.uni.component.reports.YesNoUIObject;

import java.util.List;

@Input(keys = {"person"}, bindings = {"person"})
public class Model {

    private Person person;

    private PersonNARFU personNARFU;

    private YesNoUIObject needSocialPayment;

    public Model()
    {
        _yesNoList = YesNoUIObject.createYesNoList();
    }

    private List _yesNoList;

    public List getYesNoList()
    {
        return _yesNoList;
    }

    public void setYesNoList(List yesNoList)
    {
        _yesNoList = yesNoList;
    }

    public PersonNARFU getPersonNARFU() {
        return personNARFU;
    }

    public void setPersonNARFU(PersonNARFU personNARFU) {
        this.personNARFU = personNARFU;
    }

    public YesNoUIObject getNeedSocialPayment() {
        return needSocialPayment;
    }

    public void setNeedSocialPayment(YesNoUIObject needSocialPayment) {
        this.needSocialPayment = needSocialPayment;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
