package ru.tandemservice.narfu.component.entrant.EntrantRequestTab;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.RtfDocumentRenderer;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.sec.entity.Principal;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.narfu.uniec.dao.IEntrantDAO;
import ru.tandemservice.uniec.dao.print.PrintParameter;
import ru.tandemservice.unisession.dao.print.EntrantQuestionnairePrintDAO;
import ru.tandemservice.unisession.dao.print.EntrantRequestPrintDAO;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uniec.entity.catalog.EnrollmentDocument;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantEnrollmentDocument;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.ArrayList;
import java.util.List;

public class Controller extends ru.tandemservice.eventssecrmc.component.entrant.EntrantRequestTab.Controller
{

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        super.onRefreshComponent(component);
        Model model = (Model) getModel(component);

        ((IDAO) getDao()).prepare(model);

        ((IDAO) getDao()).prepareEntrantRequestList(model);

        for (EntrantRequest entrantRequest : model.getEntrantRequestList()) {
            prepareEntrantRequestDataSource(component, entrantRequest);
        }


    }

    @Override
    public void onClickPrintListAndReceipt(IBusinessComponent component) {
        IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();
        Principal principal = (Principal) principalContext.getPrincipal();

        CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(
                (IScriptItem) DataAccessServices.dao().getByCode(UniecScriptItem.class, "documentListAndReceipt"),
                "object", component.getListenerParameter(),
                "principal", principal
        );
    }

    private void prepareEntrantRequestDataSource(IBusinessComponent component, final EntrantRequest entrantRequest) {
        final Model model = (Model) getModel(component);
        DynamicListDataSource<RequestedEnrollmentDirection> dataSource = model.getRequestedEnrollmentDirectionDataSourceByRequest().get(entrantRequest);

        // если нет источника данных, или колонка создана (но нахрена такие условия)
        if (dataSource == null || dataSource.getColumn("Дата возврата") != null)
            return;

        //вставить перед примечапнием
        AbstractColumn col = dataSource.getColumn("comment");
        int index = col != null ? col.getNumber() : 999;
        dataSource.addColumn(new SimpleColumn("Дата возврата", "requestNARFU.dateDocumentsTakeAway", DateFormatter.DATE_FORMATTER_WITH_TIME).setClickable(false).setOrderable(false).setRequired(true), index);
    }


    @SuppressWarnings("unchecked")
    @Override
    public void onClickPrintEnrollmentExamSheet(IBusinessComponent component)
    {
        Long id = getModel(component).getEntrant().getId();
        Long entrantRequestId = (Long) component.getListenerParameter();

        PrintParameter prm = new PrintParameter(id, entrantRequestId, null, true, true);

        final IPrintFormCreator<PrintParameter> formCreator = (IPrintFormCreator<PrintParameter>) ApplicationRuntime.getBean("entrantExamListPrintBean");
        byte[] content = RtfUtil.toByteArray(formCreator.createPrintForm(null, prm));
        Integer temporaryId = PrintReportTemporaryStorage.registerTemporaryPrintForm(content, "экзам. лист.rtf");
        activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new UniMap().add("id", temporaryId).add("zip", Boolean.FALSE)));

    }

    @Override
    public void onClickPrintEntrantRequest(IBusinessComponent component)
            throws Exception
    {

        RtfDocument document = EntrantRequestPrintDAO.instance()
                .printEntrantRequest((Long) component.getListenerParameter());

        if (document == null) {
            ContextLocal.getErrorCollector().addError("Ошибка печати заявления абитуриента.");
            return;
        }

        BusinessComponentUtils.downloadDocument(new RtfDocumentRenderer(
                "Заявление.rtf", document), true);
    }


    public void onClickPrintEntrantQuestionnaire(IBusinessComponent component) throws Exception {

        RtfDocument document = EntrantQuestionnairePrintDAO.instance().printEntrantQuestionnaire((Long) component.getListenerParameter());

        if (document == null) {
            ContextLocal.getErrorCollector().addError("Ошибка печати анкеты абитуриента.");
            return;
        }

        BusinessComponentUtils.downloadDocument(new RtfDocumentRenderer("Анкета абитуриента.rtf", document), true);
    }

    public void onClickPrintList(IBusinessComponent component) {
        Session session = DataAccessServices.dao().getComponentSession();
        EntrantRequest er = (EntrantRequest) session.get(EntrantRequest.class, (Long) component.getListenerParameter());
        Entrant entrant = getModel(component).getEntrant();
        UniecScriptItem template = (UniecScriptItem) ((IDAO) getDao()).getCatalogItem(ru.tandemservice.uniec.entity.catalog.UniecScriptItem.class, "documentsListNarfu");

        if (template == null) {
            return;
        }
        else {

            RtfDocument document = new RtfReader().read(template.getCurrentTemplate());
            RtfInjectModifier im = new RtfInjectModifier();
            RtfTableModifier tm = new RtfTableModifier();
            Person person = entrant.getPerson();

            im.put("numb_ab", er.getStringNumber());

            im.modify(document);

            Criteria criteria = session.createCriteria(EntrantEnrollmentDocument.class);

            criteria.createAlias(EntrantEnrollmentDocument.L_ENROLLMENT_DOCUMENT, "enrollmentDocument");
            criteria.add(Restrictions.eq(EntrantEnrollmentDocument.L_ENTRANT_REQUEST, er));
            criteria.addOrder(Order.asc("enrollmentDocument." + EnrollmentDocument.P_PRIORITY));

            List<String[]> enrollmentDocuments = ((IDAO) getDao()).getEnrollmentDocuments(criteria.list(), person.getPersonEduInstitution(), entrant);

            tm.put("T1", getInventoryTable(enrollmentDocuments));
            tm.modify(document);

            BusinessComponentUtils.downloadDocument(new RtfDocumentRenderer("Опись абитуриента " + person.getIdentityCard().getFullFio().toString() + ".rtf", document), true);

        }
    }

    private String[][] getInventoryTable(List<String[]> enrollmentDocuments) {
        int size = enrollmentDocuments.size();
        List<String[]> result = new ArrayList<String[]>(size);

        for (int i = 0; i < size; i++) {
            result.add(new String[]{Integer.toString(i + 1), "", enrollmentDocuments.get(i)[0], enrollmentDocuments.get(i)[1]});
        }

        return result.toArray(new String[][]{});
    }

    public void onClickDocumentsReturn(IBusinessComponent component)
    {
        super.onClickDocumentsReturn(component);
        IEntrantDAO dao = (IEntrantDAO) ApplicationRuntime.getBean("ru.tandemservice.uniec.dao.IEntrantDAO");
    }

}
