package ru.tandemservice.narfu.uniec.component.report.EntrantExamListPrint;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.uniec.dao.print.PrintParameter;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uniec.entity.entrant.Entrant;

import java.util.ArrayList;
import java.util.List;

public class Controller
        extends AbstractBusinessController<IDAO, Model>
{
    public Controller()
    {
    }

    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = (Model) getModel(component);
        ((IDAO) getDao()).prepare(model);
    }

    @SuppressWarnings("unchecked")
    public void onClickApply(IBusinessComponent component)
    {
        Model model = (Model) getModel(component);
        List<Entrant> entrants = null;
        if (model.isEntrantListActive())
            entrants = model.getEntrantList();
        else
            entrants = ((IDAO) getDao()).getEntrantList(model);

        List<Long> ids = new ArrayList<Long>();

        if (entrants != null && entrants.size() > 0) {
            for (Entrant e : entrants) {
                ids.add(e.getId());
            }
        }

        if (ids == null || ids.size() == 0)
            return;


        PrintParameter prm = new PrintParameter(null, null, ids, model.isPage1Print(), model.isPage2Print());
        final IPrintFormCreator<PrintParameter> formCreator = (IPrintFormCreator<PrintParameter>) ApplicationRuntime.getBean("entrantExamListPrintBean");
        byte[] content = RtfUtil.toByteArray(formCreator.createPrintForm(null, prm));
        Integer temporaryId = PrintReportTemporaryStorage.registerTemporaryPrintForm(content, "экзам. лист.rtf");
        activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new UniMap().add("id", temporaryId).add("zip", Boolean.FALSE)));

    }
}
