package ru.tandemservice.narfu.component.eduplan.EduPlanBlockVersionLinkPub;

import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.narfu.entity.EppEduPlanVersionBlockLink;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

@Input({@org.tandemframework.core.component.Bind(key = "publisherId", binding = "eduPlanHolder.id"), @org.tandemframework.core.component.Bind(key = "linkId", binding = "linkHolder.id")})
public class Model {

    private final EntityHolder<EppEduPlanVersionBlock> eduPlanHolder = new EntityHolder<>();
    private final EntityHolder<EppEduPlanVersionBlockLink> linkHolder = new EntityHolder<>();

    private StaticListDataSource<IEppEpvRowWrapper> rowDataSource = new StaticListDataSource<>();

    public EntityHolder<EppEduPlanVersionBlock> getEduPlanHolder()
    {
        return this.eduPlanHolder;
    }

    public EntityHolder<EppEduPlanVersionBlockLink> getLinkHolder()
    {
        return this.linkHolder;
    }

    public Long getId() {
        return getEduPlanHolder().getId();
    }

    public StaticListDataSource<IEppEpvRowWrapper> getRowDataSource() {
        return this.rowDataSource;
    }

    public void setRowDataSource(StaticListDataSource<IEppEpvRowWrapper> rowDataSource)
    {
        this.rowDataSource = rowDataSource;
    }

    public String getTitle() {
        return UniEppUtils.getEppWorkPlanRowFormTitle(getId());
    }

}
