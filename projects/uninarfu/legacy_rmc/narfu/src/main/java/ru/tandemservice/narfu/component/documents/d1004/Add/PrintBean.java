package ru.tandemservice.narfu.component.documents.d1004.Add;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressRu;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.tools.DateCalc;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.component.documents.DocumentPrintBean;
import ru.tandemservice.uni.entity.catalog.EducationLevelMiddle;
import ru.tandemservice.uni.entity.orgstruct.AcademyData;

import java.util.Arrays;

public class PrintBean extends DocumentPrintBean<Model> {
    @Override
    public RtfDocument createPrintForm(byte[] template, Model model) {
        RtfDocument document = super.createPrintForm(template, model);

        SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Arrays.asList("branchShortTitle"), true, false);

        return document;
    }

    @Override
    protected RtfInjectModifier createInjectModifier(Model model)
    {
        RtfInjectModifier injectModifier = new RtfInjectModifier();

        OrgUnit academy = TopOrgUnit.getInstance();

        AcademyData academyData = AcademyData.getInstance();

        OrgUnit fou = model.getStudent().getEducationOrgUnit().getFormativeOrgUnit();

        int _deyDiff = 1 + DateCalc.getDayDiff(model.getDateStartCertification(), model.getDateEndCertification());

        boolean spo = model.getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel() instanceof EducationLevelMiddle;
        boolean isBranch = false;

        RtfString subtitle = new RtfString();
        RtfString orderExtract = new RtfString();
        RtfString address = new RtfString();
        AddressDetailed orgUnitAddress = academy.getPostalAddress();

        if (fou.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.BRANCH)) {
            isBranch = true;
            academy = fou;

            orgUnitAddress = fou.getPostalAddress();
        }
        else if (fou.getParent() != null && fou.getParent().getOrgUnitType().getCode().equals(OrgUnitTypeCodes.BRANCH)) {
            isBranch = true;
            academy = fou.getParent();
            orgUnitAddress = fou.getParent().getPostalAddress();
        }

        // String fouP="";
        // String fouP=fou.getPrepositionalCaseTitle() + " " + fou.getParent().getTitle();
        String fouP = fou.getPrepositionalCaseTitle() + " " + fou.getParent().getGenitiveCaseTitle();
        if (isBranch) {

            if (fou.getTitle().contains("Коряжм")) {
                fouP = fou.getPrepositionalCaseTitle();
            }
            else if (fou.getParent().getTitle().contains("Северодвинск")) {
                fouP = fou.getPrepositionalCaseTitle() + " " + fou.getParent().getGenitiveCaseTitle();
            }
            // 	else{
            //		fouP = fou.getPrepositionalCaseTitle() + " " + fou.getParent().getTitle();
            //}
        }

        /*if(isBranch)
        {
            String settlementTitle = "";
            if(orgUnitAddress!=null && orgUnitAddress.getSettlement()!=null)
                settlementTitle = orgUnitAddress.getSettlement().getTitle();
            if (fou.getTitle().contains("Коряжме") || settlementTitle.contains("Коряжма"))
                address.boldBegin().append("филиал в городе Коряжме").boldEnd().append(IRtfData.PAR);
            if (fou.getTitle().contains("Северодвинске") || settlementTitle.contains("Северодвинск"))
                address.boldBegin().append("филиал в городе Северодвинске").boldEnd().append(IRtfData.PAR);
        }
        else
        {
            orgUnitAddress = TopOrgUnit.getInstance().getPostalAddress();
        }
        */
        if (!isBranch)
            orgUnitAddress = TopOrgUnit.getInstance().getPostalAddress();

        if (orgUnitAddress != null && orgUnitAddress instanceof AddressRu) {
            AddressRu addressRu = (AddressRu) orgUnitAddress;
            address.append(addressRu.getStreet() == null ? "" : addressRu.getStreet().getTitleWithTypeNonPersistent())
                    .append(addressRu.getHouseNumber() == null ? "" : ", " + addressRu.getHouseNumber() + ", ")
                    .append(addressRu.getHouseUnitNumber() == null ? "" : addressRu.getHouseUnitNumber() + ", ")
                    .append(IRtfData.PAR)
                    .append(orgUnitAddress.getSettlement() == null ? "" : orgUnitAddress.getSettlement().getTitleWithType())
                    .append(orgUnitAddress.getCountry() == null ? "" : ", " + orgUnitAddress.getCountry().getTitle())
                    .append(addressRu.getInheritedPostCode() == null ? "" : ", " + addressRu.getInheritedPostCode());
        }
        if (spo) {
            for (int i = 0; i < ParagraphData.SUBTITLE_SPO.length; i++) {
                subtitle.append(ParagraphData.SUBTITLE_SPO[i]);
                if (i != ParagraphData.SUBTITLE_SPO.length - 1)
                    subtitle.append(IRtfData.PAR);
            }
            for (int i = 0; i < ParagraphData.ORDEREXTRACT_SPO.length; i++) {
                orderExtract.append(ParagraphData.ORDEREXTRACT_SPO[i]);
                if (i != ParagraphData.ORDEREXTRACT_SPO.length - 1)
                    orderExtract.append(IRtfData.PAR);
            }
        }
        else {
            for (int i = 0; i < ParagraphData.SUBTITLE_VPO.length; i++) {
                subtitle.append(ParagraphData.SUBTITLE_VPO[i]);
                if (i != ParagraphData.SUBTITLE_VPO.length - 1)
                    subtitle.append(IRtfData.PAR);
            }
            for (int i = 0; i < ParagraphData.ORDEREXTRACT_VPO.length; i++) {
                orderExtract.append(ParagraphData.ORDEREXTRACT_VPO[i]);
                if (i != ParagraphData.ORDEREXTRACT_VPO.length - 1)
                    orderExtract.append(IRtfData.PAR);
            }

        }


        String eduSpec = model.getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().getDativeCaseShortTitle();
        eduSpec += " ";
        eduSpec += model.getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getProgramSubjectTitleWithCode();

        OrgUnit branchOU = getBranch(model.getStudent().getEducationOrgUnit().getFormativeOrgUnit());

        if (branchOU != null)
            injectModifier.put("branchShortTitle", branchOU.getShortTitle());

        injectModifier

                .put("faxInst", academy.getFax())
                .put("telinst", academy.getPhone())
                .put("DateS", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(model.getFormingDate()))
                .put("NumS", String.valueOf(model.getNumber()))
                .put("Rabot", model.getEmployer() == null ? model.getStudent().getPerson().getWorkPlace() : model.getEmployer())
                .put("fo", StringUtils.lowerCase(model.getStudent().getEducationOrgUnit().getDevelopForm().getGenCaseTitle()))
                .put("kurs", model.getCourse().getTitle())
//                .put("formPr", model.getStudent().getEducationOrgUnit().getFormativeOrgUnit().getGenitiveCaseTitle())
                .put("eduForm", (spo ? ParagraphData.EDU_PROG_SPO : ParagraphData.EDU_PROG_VPO))
                .put("eduSpec", eduSpec)
                .put("fouP", fouP)
                .put("FIO", model.getStudentTitleStr())
                .put("learned_D", (model.getStudent().getPerson().getIdentityCard().getSex().getCode().equals(SexCodes.MALE) ? "обучающемуся" : "обучающейся"))
                .put("dateN", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(model.getDateStartCertification()))
                .put("dateF", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(model.getDateEndCertification()))
                .put("sert_date", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(academyData.getCertificateDate()))
                .put("certSer", academyData.getCertificateSeria() != null ? academyData.getCertificateSeria() : "-")
                .put("certNum", academyData.getCertificateNumber() != null ? academyData.getCertificateNumber() : "-")
                .put("certregNum", academyData.getCertificateRegNumber() != null ? academyData.getCertificateRegNumber() : "-")
                .put("certDate", academyData.getCertificateDate() != null ? DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(academyData.getCertificateDate()) : "-")
                .put("certAg", academyData.getCertificationAgency() != null ? academyData.getCertificationAgency().replace("Федеральная", "Федеральной").replace("служба", "службой") : "-")
                .put("certExtDate", academyData.getCertificateExpiriationDate() != null ? DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(academyData.getCertificateExpiriationDate()) : "-")
                .put("Dolj", model.getManagerPostTitle())
                .put("FIOruk", model.getManagerFio())
                .put("days", model.getContinuance() > 0 ? String.valueOf(model.getContinuance()) : Integer.toString(_deyDiff))
                .put("daysStr", getDaysStr(model.getContinuance() > 0 ? model.getContinuance() : _deyDiff))
                .put("FIOi", model.getStudentTitleStrIminit())
                .put("FIOisp", model.getExecutant())
                .put("telIsp", model.getTelephone())
                .put("address", address)
                .put("site", academy.getWebpage())
                .put("email", academy.getEmail())
                .put("subtitle", subtitle)
                .put("introduction", spo ? ParagraphData.INTRODUCTION_SPO : ParagraphData.INTRODUCTION_VPO)
                .put("orderExtract", orderExtract)
                .put("period", model.getPeriod())
                .put("placed", (model.getStudent().getPerson().getIdentityCard().getSex().getCode().equals(SexCodes.MALE) ? "находился" : "находилась"));


        return injectModifier;
    }

    private String getDaysStr(int days) {

        String str = String.valueOf(days);
        char lastChar = str.charAt(str.length() - 1);
        int last = Integer.valueOf(String.valueOf(lastChar)).intValue();

        if (days == 1 || (last == 1 && days > 20))
            return "календарный день";

        if ((days >= 2 && days <= 4) || (last >= 2 && last <= 4 && days > 20))
            return "календарных дня";

        if ((days >= 5 && days <= 20) || (last >= 5 && last <= 9) || (last == 0))
            return "календарных дней";

        return "";
    }

    private OrgUnit getBranch(OrgUnit formOU) {

        if (isBranch(formOU))
            return formOU;

        OrgUnit ou = formOU;
        while (ou.getParent() != null) {
            ou = (OrgUnit) ou.getParent();
            if (isBranch(ou))
                return ou;
        }

        return null;
    }

    private boolean isBranch(OrgUnit ou) {
        return ou.getOrgUnitType().getCode().equals("branch");
    }
}
