package ru.tandemservice.narfu.component.registry.base.TutorOrgunitEdit;

import org.hibernate.Session;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModule;
import ru.tandemservice.uniepp.entity.settings.EppTutorOrgUnit;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

public class DAO extends UniDao<Model>
{

    @Override
    public void prepare(Model model)
    {
        model.setTutorOuModel(new DQLFullCheckSelectModel()
        {

            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(OrgUnit.class, alias);
                DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(EppTutorOrgUnit.class, "t")
                        .column(DQLExpressions.property(EppTutorOrgUnit.orgUnit().id().fromAlias("t")));
                builder.where(DQLExpressions.in(DQLExpressions.property(OrgUnit.id().fromAlias(alias)), subBuilder.buildQuery()));

                if (filter != null)
                    builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(OrgUnit.title().fromAlias(alias))), DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));

                return builder;
            }
        });
    }

    @Override
    public void update(Model model)
    {
        List<EppRegistryElement> elements = getList(EppRegistryElement.class, model.getRegistryIds());
        Session session = getSession();
        for (EppRegistryElement element : elements) {
            element.setOwner(model.getTutorOU());
            session.saveOrUpdate(element);
            for (EppRegistryModule module : getRegistryModules(element)) {
                module.setOwner(model.getTutorOU());
                session.saveOrUpdate(module);
            }
        }
    }

    List<EppRegistryModule> getRegistryModules(EppRegistryElement element)
    {
        return new DQLSelectBuilder()
                .fromEntity(EppRegistryElementPartModule.class, "a")
                .where(eq(property("a", EppRegistryElementPartModule.part().registryElement().id()), value(element.getId())))
                .column(property("a", EppRegistryElementPartModule.module()))
                .createStatement(getSession()).<EppRegistryModule>list();
    }
}
