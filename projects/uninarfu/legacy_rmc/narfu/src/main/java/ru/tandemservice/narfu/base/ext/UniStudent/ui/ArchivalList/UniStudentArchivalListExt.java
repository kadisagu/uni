package ru.tandemservice.narfu.base.ext.UniStudent.ui.ArchivalList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.IPresenterExtensionBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.narfu.base.ext.UniStudent.UniStudentMangerExt;
import ru.tandemservice.uni.base.bo.UniStudent.ui.ArchivalList.UniStudentArchivalList;

@Configuration
public class UniStudentArchivalListExt extends BusinessComponentExtensionManager
{

    @Autowired
    private UniStudentArchivalList parent;

    @Bean
    public PresenterExtension presenterExtension()
    {
        IPresenterExtensionBuilder pi = presenterExtensionBuilder(parent.presenterExtPoint());
        pi.addAddon(uiAddon("ui_addon", UniStudentArchivalListUIAddon.class));
        pi.replaceDataSource(searchListDS(UniStudentArchivalList.STUDENT_SEARCH_LIST_DS, parent.studentSearchListDSColumnExtPoint()).handler(UniStudentMangerExt.instance().archivalStudentSearchListExtDSHandler()));
        return pi.create();
    }
}
