package ru.tandemservice.narfu.base.ext.EcDistribution.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

public interface IEcDistributionDaoWarningExt extends INeedPersistenceSupport {
    public byte[] getRtf(Long idDistribution);
}
