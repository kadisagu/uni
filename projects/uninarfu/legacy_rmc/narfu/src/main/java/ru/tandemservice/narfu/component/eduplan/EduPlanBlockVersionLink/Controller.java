package ru.tandemservice.narfu.component.eduplan.EduPlanBlockVersionLink;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.narfu.entity.EppEduPlanVersionBlockLink;
import ru.tandemservice.unibase.UniBaseUtils;

public class Controller extends AbstractBusinessController<IDAO, Model> {
    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = component.getModel();

        getDao().prepare(model);
        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component) {
        Model model = component.getModel();
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<EppEduPlanVersionBlockLink> dataSource = UniBaseUtils.createDataSource(component, getDao());
        PublisherLinkColumn linkColumn = new PublisherLinkColumn("УП", EppEduPlanVersionBlockLink.blockDst().eduPlanVersion().eduPlan().title());

        linkColumn.setResolver(new IPublisherLinkResolver() {
            @Override
            public String getComponentName(IEntity ientity) {
                return "ru.tandemservice.narfu.component.eduplan.EduPlanBlockVersionLinkPub";
            }

            @Override
            public Object getParameters(IEntity entity) {
                ParametersMap params = new ParametersMap();
                params.put(PublisherActivator.PUBLISHER_ID_KEY, ((EppEduPlanVersionBlockLink) entity).getBlockSrc().getId());
                params.put("linkId", entity.getId());
                return params;
            }
        });

        dataSource.addColumn(linkColumn);
        dataSource.addColumn(new SimpleColumn("Блок версии УП", EppEduPlanVersionBlockLink.blockDst().fullTitle()).setOrderable(true));

        dataSource.addColumn(
                new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить?")
                        .setOrderable(false)
        );

        model.setDataSource(dataSource);
    }

    public void onClickDelete(IBusinessComponent component)
    {
        getDao().deleteRow(component);
    }

    public void onClickAddVersionBlock(IBusinessComponent component)
    {
        Model model = getModel(component);
        ContextLocal.createDesktop("PersonShellDialog",
                                   new ComponentActivator(ru.tandemservice.narfu.component.eduplan.EduPlanBlockVersionLinkAdd.Model.class.getPackage().getName(), new UniMap().add("blockId", model.getBlock().getId())));

    }


}
