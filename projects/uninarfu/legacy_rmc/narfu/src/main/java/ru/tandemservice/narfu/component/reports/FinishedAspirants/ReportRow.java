package ru.tandemservice.narfu.component.reports.FinishedAspirants;

import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;

import java.util.Arrays;

public class ReportRow {
    private String title;
    private String code;
    private Integer[] rowData;

    public ReportRow()
    {
        this.title = "";
        this.code = "";
        this.rowData = new Integer[11];
        Arrays.fill(rowData, 0);
    }

    public ReportRow(EducationLevelsHighSchool level)
    {
        this.title = level.getTitle();
        this.code = level.getEducationLevel().getInheritedOksoPrefix();
        this.rowData = new Integer[11];
        Arrays.fill(rowData, 0);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer[] getRowData() {
        return rowData;
    }

    public void setRowData(Integer[] rowData) {
        this.rowData = rowData;
    }
}