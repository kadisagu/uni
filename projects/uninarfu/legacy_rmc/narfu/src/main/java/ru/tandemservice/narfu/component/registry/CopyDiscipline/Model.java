package ru.tandemservice.narfu.component.registry.CopyDiscipline;


import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

@Input({@org.tandemframework.core.component.Bind(key = "discipline", binding = "discipline")})
public class Model
{

    private ISelectModel tutorOuModel;
    private EppRegistryElement discipline;

    public ISelectModel getTutorOuModel()
    {
        return tutorOuModel;
    }

    public void setTutorOuModel(ISelectModel tutorOuModel)
    {
        this.tutorOuModel = tutorOuModel;
    }

    public EppRegistryElement getDiscipline()
    {
        return discipline;
    }

    public void setDiscipline(EppRegistryElement discipline)
    {
        this.discipline = discipline;
    }
}
