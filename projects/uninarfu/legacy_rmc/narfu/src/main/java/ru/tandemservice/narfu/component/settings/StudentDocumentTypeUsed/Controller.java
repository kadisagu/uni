package ru.tandemservice.narfu.component.settings.StudentDocumentTypeUsed;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.StudentDocumentType;

public class Controller extends ru.tandemservice.uni.component.settings.StudentDocumentTypeUsed.Controller {

    public void onRefreshComponent(IBusinessComponent component) {
        super.onRefreshComponent(component);
        ru.tandemservice.uni.component.settings.StudentDocumentTypeUsed.Model model = getModel(component);
        ((IDAO) getDao()).prepare(model);
        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component) {
        Model model = component.getModel();

        DynamicListDataSource<StudentDocumentType> dataSource = model.getDataSource();

        int columnNumber = 0;

        // колонка "Использовать" теперь не нужна
        dataSource.getColumn("active").setVisible(Boolean.FALSE);

        for (final StudentCategory studentCategory : model.getStudentCategoryList()) {
            String columnViewKey = ((IDAO) getDao()).getViewPropertyKey(studentCategory);
            ToggleColumn column = (ToggleColumn) new ToggleColumn(studentCategory.getTitle(), columnViewKey).setListener("onClickToggle");

            column.setParametersResolver((entity, valueEntity) -> new Object[]{valueEntity.getId(), studentCategory.getCode()});

            // чтобы  не создавались постоянно новые колонки при refresh, если колонки уже созданы
            if (dataSource.getColumn(columnViewKey) != null) {
                columnNumber++;
                continue;
            }
            dataSource.addColumn(column, dataSource.getColumns().size() + columnNumber);
            columnNumber++;
        }

        model.setDataSource(dataSource);
    }

    public void onClickToggle(IBusinessComponent component) {
        Model model = component.getModel();
        ((IDAO) getDao()).updateRelation(model, (Object[]) component.getListenerParameter());
    }

}
