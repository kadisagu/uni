package ru.tandemservice.narfu.component.reports.StudentsAgeSexDistribution2.List;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

public class Controller extends ru.tandemservice.uni.component.reports.studentsAgeSexDistribution.List.Controller {
    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        super.onRefreshComponent(component);
        Model model = (Model) getModel(component);
        DynamicListDataSource dataSource = model.getDataSource();
        dataSource.getColumn(8).setCaption("Категория обучающегося");
    }

    @Override
    public void onClickAddReport(IBusinessComponent component) {
        component.createRegion(new ComponentActivator("ru.tandemservice.narfu.component.reports.StudentsAgeSexDistribution2.Add"));
    }


}
