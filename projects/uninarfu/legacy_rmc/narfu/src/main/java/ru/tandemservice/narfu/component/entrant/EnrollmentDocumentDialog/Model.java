package ru.tandemservice.narfu.component.entrant.EnrollmentDocumentDialog;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;

import java.util.Date;

@State({@Bind(key = "id", binding = "id")})
public class Model {

    private Long id;
    private Date date;
    private String number;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

}
