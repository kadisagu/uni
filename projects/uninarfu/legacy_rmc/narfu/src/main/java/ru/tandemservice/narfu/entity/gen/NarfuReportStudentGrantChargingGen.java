package ru.tandemservice.narfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.narfu.entity.NarfuReportStudentGrantCharging;
import ru.tandemservice.uni.entity.report.StorableReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Список отчетов «Начисление стипендии»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class NarfuReportStudentGrantChargingGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.narfu.entity.NarfuReportStudentGrantCharging";
    public static final String ENTITY_NAME = "narfuReportStudentGrantCharging";
    public static final int VERSION_HASH = -2035668457;
    private static IEntityMeta ENTITY_META;

    public static final String P_EDUCATION_YEAR = "educationYear";
    public static final String P_EDUCATION_YEAR_PART = "educationYearPart";
    public static final String P_MONTH = "month";
    public static final String P_EXECUTOR = "executor";

    private String _educationYear;     // Учебный год
    private String _educationYearPart;     // Часть года
    private String _month;     // Месяц
    private String _executor;     // Исполнитель

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Учебный год.
     */
    @Length(max=255)
    public String getEducationYear()
    {
        return _educationYear;
    }

    /**
     * @param educationYear Учебный год.
     */
    public void setEducationYear(String educationYear)
    {
        dirty(_educationYear, educationYear);
        _educationYear = educationYear;
    }

    /**
     * @return Часть года.
     */
    @Length(max=255)
    public String getEducationYearPart()
    {
        return _educationYearPart;
    }

    /**
     * @param educationYearPart Часть года.
     */
    public void setEducationYearPart(String educationYearPart)
    {
        dirty(_educationYearPart, educationYearPart);
        _educationYearPart = educationYearPart;
    }

    /**
     * @return Месяц.
     */
    @Length(max=255)
    public String getMonth()
    {
        return _month;
    }

    /**
     * @param month Месяц.
     */
    public void setMonth(String month)
    {
        dirty(_month, month);
        _month = month;
    }

    /**
     * @return Исполнитель.
     */
    @Length(max=255)
    public String getExecutor()
    {
        return _executor;
    }

    /**
     * @param executor Исполнитель.
     */
    public void setExecutor(String executor)
    {
        dirty(_executor, executor);
        _executor = executor;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof NarfuReportStudentGrantChargingGen)
        {
            setEducationYear(((NarfuReportStudentGrantCharging)another).getEducationYear());
            setEducationYearPart(((NarfuReportStudentGrantCharging)another).getEducationYearPart());
            setMonth(((NarfuReportStudentGrantCharging)another).getMonth());
            setExecutor(((NarfuReportStudentGrantCharging)another).getExecutor());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends NarfuReportStudentGrantChargingGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) NarfuReportStudentGrantCharging.class;
        }

        public T newInstance()
        {
            return (T) new NarfuReportStudentGrantCharging();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                    return obj.getEducationYear();
                case "educationYearPart":
                    return obj.getEducationYearPart();
                case "month":
                    return obj.getMonth();
                case "executor":
                    return obj.getExecutor();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "educationYear":
                    obj.setEducationYear((String) value);
                    return;
                case "educationYearPart":
                    obj.setEducationYearPart((String) value);
                    return;
                case "month":
                    obj.setMonth((String) value);
                    return;
                case "executor":
                    obj.setExecutor((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                        return true;
                case "educationYearPart":
                        return true;
                case "month":
                        return true;
                case "executor":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                    return true;
                case "educationYearPart":
                    return true;
                case "month":
                    return true;
                case "executor":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                    return String.class;
                case "educationYearPart":
                    return String.class;
                case "month":
                    return String.class;
                case "executor":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<NarfuReportStudentGrantCharging> _dslPath = new Path<NarfuReportStudentGrantCharging>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "NarfuReportStudentGrantCharging");
    }
            

    /**
     * @return Учебный год.
     * @see ru.tandemservice.narfu.entity.NarfuReportStudentGrantCharging#getEducationYear()
     */
    public static PropertyPath<String> educationYear()
    {
        return _dslPath.educationYear();
    }

    /**
     * @return Часть года.
     * @see ru.tandemservice.narfu.entity.NarfuReportStudentGrantCharging#getEducationYearPart()
     */
    public static PropertyPath<String> educationYearPart()
    {
        return _dslPath.educationYearPart();
    }

    /**
     * @return Месяц.
     * @see ru.tandemservice.narfu.entity.NarfuReportStudentGrantCharging#getMonth()
     */
    public static PropertyPath<String> month()
    {
        return _dslPath.month();
    }

    /**
     * @return Исполнитель.
     * @see ru.tandemservice.narfu.entity.NarfuReportStudentGrantCharging#getExecutor()
     */
    public static PropertyPath<String> executor()
    {
        return _dslPath.executor();
    }

    public static class Path<E extends NarfuReportStudentGrantCharging> extends StorableReport.Path<E>
    {
        private PropertyPath<String> _educationYear;
        private PropertyPath<String> _educationYearPart;
        private PropertyPath<String> _month;
        private PropertyPath<String> _executor;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Учебный год.
     * @see ru.tandemservice.narfu.entity.NarfuReportStudentGrantCharging#getEducationYear()
     */
        public PropertyPath<String> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new PropertyPath<String>(NarfuReportStudentGrantChargingGen.P_EDUCATION_YEAR, this);
            return _educationYear;
        }

    /**
     * @return Часть года.
     * @see ru.tandemservice.narfu.entity.NarfuReportStudentGrantCharging#getEducationYearPart()
     */
        public PropertyPath<String> educationYearPart()
        {
            if(_educationYearPart == null )
                _educationYearPart = new PropertyPath<String>(NarfuReportStudentGrantChargingGen.P_EDUCATION_YEAR_PART, this);
            return _educationYearPart;
        }

    /**
     * @return Месяц.
     * @see ru.tandemservice.narfu.entity.NarfuReportStudentGrantCharging#getMonth()
     */
        public PropertyPath<String> month()
        {
            if(_month == null )
                _month = new PropertyPath<String>(NarfuReportStudentGrantChargingGen.P_MONTH, this);
            return _month;
        }

    /**
     * @return Исполнитель.
     * @see ru.tandemservice.narfu.entity.NarfuReportStudentGrantCharging#getExecutor()
     */
        public PropertyPath<String> executor()
        {
            if(_executor == null )
                _executor = new PropertyPath<String>(NarfuReportStudentGrantChargingGen.P_EXECUTOR, this);
            return _executor;
        }

        public Class getEntityClass()
        {
            return NarfuReportStudentGrantCharging.class;
        }

        public String getEntityName()
        {
            return "narfuReportStudentGrantCharging";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
