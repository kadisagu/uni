package ru.tandemservice.narfu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_narfu_1x0x0_55to56 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				new ScriptDependency("org.tandemframework", "1.6.15"),
				new ScriptDependency("org.tandemframework.shared", "1.6.0"),
				new ScriptDependency("ru.tandemservice.uni.product", "2.6.0"),
				new ScriptDependency("ru.tandemservice.uni.project", "2.6.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность personEduInstitutionNARFU

		//  свойство base стало обязательным
		{
			tool.executeUpdate("delete from personeduinstitutionnarfu_t where base_id is null");
			// сделать колонку NOT NULL
			tool.setColumnNullable("personeduinstitutionnarfu_t", "base_id", false);

		}


    }
}