package ru.tandemservice.narfu.unisession.component.settings.EstimatesExamImportList;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.narfu.entity.ExamBulletinImportedFile;
import ru.tandemservice.narfu.entity.catalog.ExamBulletinImportedFileResult;

import java.util.List;


public class Model

{
    private DynamicListDataSource<ExamBulletinImportedFile> _dataSource;
    private ExamBulletinImportedFile examBulletinImportedFile;
    private IDataSettings settings;

    private List<ExamBulletinImportedFileResult> importResultList;


    public DynamicListDataSource<ExamBulletinImportedFile> getDataSource() {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<ExamBulletinImportedFile> _dataSource) {
        this._dataSource = _dataSource;
    }

    public List<ExamBulletinImportedFileResult> getImportResultList() {
        return importResultList;
    }

    public void setImportResultList(List<ExamBulletinImportedFileResult> importResultList) {
        this.importResultList = importResultList;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public ExamBulletinImportedFile getExamBulletinImportedFile() {
        return examBulletinImportedFile;
    }

    public void setExamBulletinImportedFile(ExamBulletinImportedFile examBulletinImportedFile) {
        this.examBulletinImportedFile = examBulletinImportedFile;
    }


}