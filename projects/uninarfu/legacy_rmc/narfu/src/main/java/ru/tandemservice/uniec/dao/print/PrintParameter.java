package ru.tandemservice.uniec.dao.print;

import java.util.List;

public class PrintParameter {

    private Long entrantId;
    private List<Long> entrantIds;
    private boolean page1Print;
    private boolean page2Print;
    // нужна для печати экзаменационного листа
    private Long entrantRequestId;

    /**
     * со слов Маши - у нарфушного абитуриента может быть только одно заявление
     * посему эта переменная лишная
     */

    public PrintParameter
    (
            Long entrantId
            , Long entrantRequestId
            , List<Long> entrantIds
            , boolean page1Print
            , boolean page2Print

    )
    {
        this.setEntrantId(entrantId);
        this.setEntrantRequestId(entrantRequestId);
        this.setEntrantIds(entrantIds);
        this.page1Print = page1Print;
        this.page2Print = page2Print;

    }

    public void setEntrantId(Long entrantId) {
        this.entrantId = entrantId;
    }

    public Long getEntrantId() {
        return entrantId;
    }

    public void setEntrantIds(List<Long> entrantIds) {
        this.entrantIds = entrantIds;
    }

    public List<Long> getEntrantIds() {
        return entrantIds;
    }

    public void setPage1Print(boolean page1Print) {
        this.page1Print = page1Print;
    }

    public boolean isPage1Print() {
        return page1Print;
    }

    public void setPage2Print(boolean page2Print) {
        this.page2Print = page2Print;
    }

    public boolean isPage2Print() {
        return page2Print;
    }

    public Long getEntrantRequestId() {
        return entrantRequestId;
    }

    public void setEntrantRequestId(Long entrantRequestId) {
        this.entrantRequestId = entrantRequestId;
    }

}
