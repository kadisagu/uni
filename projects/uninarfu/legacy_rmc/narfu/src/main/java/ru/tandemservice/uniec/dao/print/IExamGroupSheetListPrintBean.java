package ru.tandemservice.uniec.dao.print;

import org.tandemframework.rtf.document.RtfDocument;

import java.util.List;

public interface IExamGroupSheetListPrintBean {
    public RtfDocument generateExamGroupSheetReport(List<Long> groupIds);
}
