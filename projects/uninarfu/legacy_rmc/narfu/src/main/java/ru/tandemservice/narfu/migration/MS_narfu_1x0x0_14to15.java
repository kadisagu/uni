package ru.tandemservice.narfu.migration;

import org.apache.commons.codec.digest.DigestUtils;
import org.tandemframework.core.common.DBType;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;
import ru.tandemservice.narfu.entity.IdentityCardHidden;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_narfu_1x0x0_14to15 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.10"),
                        new ScriptDependency("org.tandemframework.shared", "1.1.3"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.1.3")
                };
    }

    /**
     * Процедура обезличивания УЛ архивных студентов
     * RM#2934
     */
    @Override
    public void run(DBTool tool) throws Exception
    {
        //миграция неправильная
        if (1 == 1) return;
        ////////////////////////////////////////////////////////////////////////////////
        // сущность identityCardHidden
        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("identitycardhidden_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("identitycard_id", DBType.LONG).setNullable(false),
                                      new DBColumn("seria_p", DBType.createVarchar(255)),
                                      new DBColumn("number_p", DBType.createVarchar(255)),
                                      new DBColumn("birthdate_p", DBType.DATE)
            );

            if (!tool.tableExists("identitycardhidden_t"))
                tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("identityCardHidden");

            short code = EntityRuntime.getMeta(IdentityCardHidden.class).getEntityCode();

            Statement stmt = tool.getConnection().createStatement();

            /**
             * выбираем УЛ у архивных студентов
             */
            String selectQuery =
                    "select DISTINCT ic.id,ic.seria_p, ic.number_p, ic.birthdate_p from student_t  s " +
                            "inner join personrole_t pr  on s.id = pr.id " +
                            "inner join person_t pt on pt.id = pr.person_id " +
                            "inner join identitycard_t ic on ic.id = pt.identitycard_id " +
                            "where s.archival_p = ?";

            PreparedStatement pst = tool.getConnection().prepareStatement(selectQuery);
            pst.setBoolean(1, true);
            pst.execute();

            ResultSet rs = pst.getResultSet();

            /**
             * Перенос данных в IdentityCardHidden
             */
            while (rs.next()) {
                String seria_p = rs.getString("seria_p");
                String number_p = rs.getString("number_p");
                java.sql.Date birthdate_p = rs.getDate("birthdate_p");

                tool.executeUpdate(
                        "insert into identitycardhidden_t (id, discriminator, identitycard_id, seria_p, number_p, birthdate_p) values (?,?,?,?,?,?)",
                        EntityIDGenerator.generateNewId(code), code, rs.getLong("id"), seria_p, number_p, birthdate_p);

                //необходимо записать в identitycard_t поля ic.seria_p, ic.number_p хеш значения, ic.birthdate_p = 01-01-1900
                tool.executeUpdate("update identitycard_t set seria_p=?, number_p=?, birthdate_p=?  where id=?", (seria_p != null ? DigestUtils.md5Hex(seria_p) : seria_p), (number_p != null ? DigestUtils.md5Hex(number_p) : number_p), "01-01-1900", rs.getLong("id"));

            }

        }


    }
}