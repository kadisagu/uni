package ru.tandemservice.narfu.component.wizard.PersonalDataStep;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniecrmc.entity.entrant.Entrant2IndividualAchievements;
import ru.tandemservice.unibase.UniBaseUtils;

public class Controller extends ru.tandemservice.uniec.component.wizard.PersonalDataStep.Controller {
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        prepareAchievementsDataSource(component);
    }


    private void prepareAchievementsDataSource(IBusinessComponent component) {
        final Model model = (Model) getModel(component);
        if (model.getAchievementsDataSource() != null) return;

        DynamicListDataSource<Entrant2IndividualAchievements> dataSource = new DynamicListDataSource<>(component, component1 -> {
            model.getAchievementsDataSource().setCountRow(model.getSelectedAchievementsList().size());
            UniBaseUtils.createPage(model.getAchievementsDataSource(), model.getSelectedAchievementsList());
        });
        dataSource.addColumn(new SimpleColumn("Вид достижения", Entrant2IndividualAchievements.individualAchievements().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Описание", Entrant2IndividualAchievements.text().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Удалить", "delete", "onClickDeleteAchievement"));
        model.setAchievementsDataSource(dataSource);
    }

    public void onClickSelectAchievement(IBusinessComponent component)
    {
        ErrorCollector errors = component.getUserContext().getErrorCollector();
        Model model = (Model) getModel(component);

        if (model.getEntrant2IndividualAchievements().getIndividualAchievements() == null) {
            errors.add("Поле \"Вид достижения\" обязательно для заполнения", new String[]{"achievement"});
        }

        if (errors.hasErrors()) return;

        ((IDAO) getDao()).prepareSelectAchievements((Model) getModel(component));
    }

    public void onClickDeleteAchievement(IBusinessComponent component)
    {
        ((IDAO) getDao()).deleteAchievement((Model) getModel(component), (Long) component.getListenerParameter());
    }
}
