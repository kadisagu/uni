package ru.tandemservice.narfu.component.reports.VacantBudgetPlaces.Add;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.narfu.component.settings.Course2EnrollmentCampaign.Wrapper;
import ru.tandemservice.narfu.entity.NarfuReportVacantBudgetPlaces;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.ArrayList;
import java.util.List;

public class Model {
    private List<Wrapper> courseToCampaigns = new ArrayList<>();
    private Wrapper course;
    private ISelectModel enrollmentCampaignModel;
    private ISelectModel studentStatusModel;
    private ISelectModel developFormModel;
    private List<StudentStatus> studentStatusList;
    private List<EnrollmentCampaign> enrollmentCampaignList;
    private DevelopForm developForm;
    private List<OrgUnit> formativeOrgUnitList;
    private IMultiSelectModel formativeOrgUnitListModel;

    public List<OrgUnit> getFormativeOrgUnitList() {
        return formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList) {
        this.formativeOrgUnitList = formativeOrgUnitList;
    }

    public IMultiSelectModel getFormativeOrgUnitListModel() {
        return formativeOrgUnitListModel;
    }

    public void setFormativeOrgUnitListModel(IMultiSelectModel formativeOrgUnitListModel) {
        this.formativeOrgUnitListModel = formativeOrgUnitListModel;
    }

    NarfuReportVacantBudgetPlaces report = new NarfuReportVacantBudgetPlaces();

    public List<Wrapper> getCourseToCampaigns() {
        return courseToCampaigns;
    }

    public void setCourseToCampaigns(List<Wrapper> courseToCampaigns) {
        this.courseToCampaigns = courseToCampaigns;
    }

    public Wrapper getCourse() {
        return course;
    }

    public void setCourse(Wrapper course) {
        this.course = course;
    }

    public NarfuReportVacantBudgetPlaces getReport() {
        return report;
    }

    public void setReport(NarfuReportVacantBudgetPlaces report) {
        this.report = report;
    }

    public ISelectModel getEnrollmentCampaignModel() {
        return enrollmentCampaignModel;
    }

    public void setEnrollmentCampaignModel(ISelectModel enrollmentCampaignModel) {
        this.enrollmentCampaignModel = enrollmentCampaignModel;
    }

    public List<EnrollmentCampaign> getEnrollmentCampaignList() {
        return enrollmentCampaignList;
    }

    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList) {
        this.enrollmentCampaignList = enrollmentCampaignList;
    }

    public ISelectModel getStudentStatusModel() {
        return studentStatusModel;
    }

    public void setStudentStatusModel(ISelectModel studentStatusModel) {
        this.studentStatusModel = studentStatusModel;
    }

    public List<StudentStatus> getStudentStatusList() {
        return studentStatusList;
    }

    public void setStudentStatusList(List<StudentStatus> studentStatusList) {
        this.studentStatusList = studentStatusList;
    }

    public ISelectModel getDevelopFormModel() {
        return developFormModel;
    }

    public void setDevelopFormModel(ISelectModel developFormModel) {
        this.developFormModel = developFormModel;
    }

    public DevelopForm getDevelopForm() {
        return developForm;
    }

    public void setDevelopForm(DevelopForm developForm) {
        this.developForm = developForm;
    }
}