package ru.tandemservice.unisession.dao.print;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.tools.PrivateAccessor;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA;
import ru.tandemservice.uniepp.entity.registry.EppRegistryAttestation;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryPractice;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.entity.document.SessionRetakeDocument;
import ru.tandemservice.unisession.print.ISessionRetakeDocPrintDAO;

import java.util.*;

public class SessionRetakeDocPrintDAO extends
        ru.tandemservice.unisession.print.SessionRetakeDocPrintDAO
        implements ISessionRetakeDocPrintDAO
{


    @Override
    protected void printAdditionalInfo(BulletinPrintInfo printData) {
        super.printAdditionalInfo(printData);
    }

    @SuppressWarnings("unchecked")
    public RtfDocument printDocList(Collection<Long> ids) {
        Map<SessionRetakeDocument, BulletinPrintInfo> bulletinMap = (Map<SessionRetakeDocument, BulletinPrintInfo>) PrivateAccessor.invokePrivateMethod(this, ru.tandemservice.unisession.print.SessionRetakeDocPrintDAO.class, "prepareData", new Object[]{ids});
        if (bulletinMap.isEmpty()) {
            throw new ApplicationException("Не выбраны ведомости для печати.");
        }

        RtfDocument result = null;

        for (Map.Entry<SessionRetakeDocument, BulletinPrintInfo> entry : bulletinMap.entrySet()) {
            SessionRetakeDocument bulletin = entry.getKey();

            if ((bulletin.isClosed()) && (bulletinMap.size() > 1)) {
                throw new ApplicationException("Массовая печать закрытых ведомостей невозможна. Выберите только открытые ведомости.");
            }
            RtfDocument document = printBulletin(entry.getValue());
            if (null == result) {
                result = document;
            }
            else {
                RtfUtil.modifySourceList(result.getHeader(), document.getHeader(), document.getElementList());
                result.getElementList().addAll(document.getElementList());
            }
        }
        if (null == result) {
            throw new ApplicationException("Не выбраны ведомости для печати.");
        }
        return result;
    }

    private RtfDocument printBulletin(BulletinPrintInfo printData) {
        SessionRetakeDocument bulletin = printData.getBulletin();
        Map<EppStudentWpeCAction, SessionRetakeDocPrintDAO.BulletinPrintRow> contentMap = printData.getContentMap();

        if (null == printData.getDocument())
            throw new ApplicationException("Для формы контроля и настроек рейтинга ведомости " + bulletin.getTitle() + " не задан печатный шаблон ведомости.");

        RtfDocument document = printData.getDocument();

        OrgUnit orgUnit = bulletin.getSessionObject().getOrgUnit();
        TreeSet<String> eduTitles = new TreeSet<>();

        TreeSet<String> groupWithDevelopFormTitles = new TreeSet<>();

        TreeSet<String> courseTitles = new TreeSet<>();
        Set<String> caTypes = new HashSet<>();

        for (BulletinPrintRow row : contentMap.values()) {

            eduTitles.add(row.getSlot().getActualStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getProgramSubjectTitleWithCode());

            StringBuilder sb = new StringBuilder("");
            sb.append(row.getSlot().getActualStudent().getGroup() == null ? ""
                              : row.getSlot().getActualStudent().getGroup().getTitle())
                    .append(" (")
                    .append(row.getSlot().getActualStudent().getEducationOrgUnit().getDevelopForm().getTitle().toLowerCase())
                    .append(")");
            groupWithDevelopFormTitles.add(StringUtils.trimToEmpty(sb.toString()));

            courseTitles.add(String.valueOf(row.getSlot().getStudentWpeCAction().getStudentWpe().getCourse().getIntValue()));
            caTypes.add(row.getSlot().getStudentWpeCAction().getType().getTitle());
        }

        EppRegistryElement registryElement = bulletin.getRegistryElementPart().getRegistryElement();

        List<PpsEntry> commission = printData.getCommission();
        if (commission == null) {
            commission = Collections.emptyList();
        }

        RtfInjectModifier modifier = new RtfInjectModifier();

        String orgUnitTitle = StringUtils.isEmpty(orgUnit.getNominativeCaseTitle()) ?
                StringUtils.capitalize(orgUnit.getNominativeCaseTitle()) :
                StringUtils.capitalize(orgUnit.getTitle());

        EmployeePost employeePost = EmployeeManager.instance().dao().getHead(orgUnit);
        modifier.put("orgUnitTitle", orgUnitTitle);
        modifier.put("orgUnitHead", employeePost != null ? employeePost.getPerson().getFullFio() : "");

        modifier.put("documentNumber", bulletin.getNumber());
        modifier.put("educationOrgUnitTitle", StringUtils.join(eduTitles.iterator(), ", "));
        modifier.put("eduYear", bulletin.getSessionObject().getEducationYear().getTitle());
        modifier.put("course", StringUtils.join(courseTitles.iterator(), ", "));
        modifier.put("group", StringUtils.join(groupWithDevelopFormTitles.iterator(), ", "));
        modifier.put("term", bulletin.getSessionObject().getYearDistributionPart().getTitle());
        if (registryElement instanceof EppRegistryAttestation)
            modifier.put("regType", "Мероприятие ГИА");
        else if (registryElement instanceof EppRegistryPractice)
            modifier.put("regType", "Практика");
        else {
            modifier.put("regType", "Дисциплина");
        }
        modifier.put("registryElement", registryElement.getTitle());
        modifier.put("cathedra", registryElement.getOwner().getPrintTitle());
        modifier.put("caType", StringUtils.join(caTypes, ", "));
        if (caTypes.size() == 1) {
            EppGroupTypeFCA caType = contentMap.values().iterator().next().getSlot().getStudentWpeCAction().getType();
            if ((caType.getCode().equals(EppControlActionType.FULL_CODE_CONTROL_ACTION_EXAM))
                    || (caType.getCode().equals(EppControlActionType.FULL_CODE_CONTROL_ACTION_EXAM_ACCUM)))
                modifier.put("documentType", "Экзаменационная ведомость");
            else
                modifier.put("documentType", "Зачетная ведомость");
        }
        else {
            modifier.put("documentType", "Ведомость пересдач");
        }
        modifier.put("tutors", UniStringUtils.join(commission, PpsEntry.person().identityCard().fio().s(), ", "));
        modifier.put("formingDate", (bulletin.getFormingDate() == null) ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(bulletin.getFormingDate()));
        modifier.put("date", (bulletin.getFormingDate() == null) ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(bulletin.getFormingDate()));

        SessionReportManager.addOuLeaderData(modifier, orgUnit, "leader", "leaderTitle");

        customizeSimpleTags(modifier, printData);

        modifier.modify(document);

        final List<ColumnType> columns = prepareColumnList(printData);

        final List<ColumnType> templateColumns = Arrays.asList(ColumnType.NUMBER, ColumnType.FIO, ColumnType.BOOK_NUMBER, ColumnType.MARK, ColumnType.DATE);

        final int fioColumnIndex = templateColumns.indexOf(ColumnType.FIO);
        if (fioColumnIndex == -1) {
            throw new IllegalStateException();
        }

        TreeMap<String, List<BulletinPrintRow>> groupedContentMap = getGroupedAndSortedContent(printData);
        final boolean useGrouping = groupedContentMap.size() > 1;

        List<String[]> tableRows = new ArrayList<>();
        int rowNumber = 1;

        for (Map.Entry<String, List<BulletinPrintRow>> entry : groupedContentMap.entrySet()) {
            if (useGrouping) {
                tableRows.add(new String[]{entry.getKey()});
            }
            for (BulletinPrintRow row : entry.getValue()) {
                tableRows.add(fillTableRow(row, columns, rowNumber++));
            }
        }

        final String[][] tableData = tableRows.toArray(new String[tableRows.size()][]);
        RtfTableModifier tableModifier = new RtfTableModifier().put("T", tableData);
        tableModifier.put("T", new RtfRowIntercepterBase() {
            public void beforeModify(RtfTable table, int currentRowIndex) {
                for (int i = templateColumns.size() - 1; i >= 0; --i) {
                    if (columns.contains(templateColumns.get(i)))
                        continue;
                    deleteCellFioIncrease(table.getRowList().get(currentRowIndex), i, fioColumnIndex);
                    deleteCellFioIncrease(table.getRowList().get(currentRowIndex - 1), i, fioColumnIndex);
                }
            }

            private void deleteCellFioIncrease(RtfRow row, int cellIndex, int fioCellIndex) {
                RtfCell cell = row.getCellList().get(cellIndex);
                RtfCell destCell = row.getCellList().get(fioCellIndex);
                destCell.setWidth(destCell.getWidth() + cell.getWidth());
                row.getCellList().remove(cellIndex);
            }

            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value) {
                if ((useGrouping) && (tableData[rowIndex].length == 1)) {
                    return new RtfString().boldBegin().append(value).boldEnd().toList();
                }

                return null;
            }

            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex) {
                if (useGrouping)
                    for (int i = 0; i < tableData.length; ++i) {
                        if (tableData[i].length != 1)
                            continue;
                        RtfRow row = newRowList.get(startIndex + i);
                        RtfUtil.unitAllCell(row, 0);
                    }
            }
        });
        tableModifier.modify(document);

        printAdditionalInfo(printData);

        return document;
    }

}
