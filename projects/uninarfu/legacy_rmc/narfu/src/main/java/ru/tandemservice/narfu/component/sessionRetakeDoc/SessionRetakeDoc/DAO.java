package ru.tandemservice.narfu.component.sessionRetakeDoc.SessionRetakeDoc;

import org.apache.cxf.common.util.StringUtils;
import org.hibernate.Session;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.document.*;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(final Model model) {
        model.getOrgUnitHolder().refresh();

        if (model.getSessionObjectId() != null)
            model.setSessionObject(get(SessionObject.class, model.getSessionObjectId()));

        model.setSessionObjectModel(getPreparedSessionObjectSelectableModel(model));
        model.setRegistryElementModel(getPreparedRegistryElementSelectableModel(model));
        model.setActionModel(getActionTypeSelectableModel(model));
        model.setStudentModel(getStudentSelectableModel(model));
    }

    @Override
    public void doCreateDocuments(Model model) {

        Session session = lock(model.getSessionObject().getOrgUnit().getId());

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(SessionDocumentSlot.class, "b_slot")
                .where(DQLExpressions.in(
                        DQLExpressions.property(SessionDocumentSlot.studentWpeCAction().id().fromAlias("b_slot")),
                        getWPCASubDQL(model).buildQuery()))
                .where(DQLExpressions.notIn(
                        DQLExpressions.property(SessionDocumentSlot.studentWpeCAction().id().fromAlias("b_slot")),
                        new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, "rt_slot")
                                .column(DQLExpressions.property(SessionDocumentSlot.studentWpeCAction().id().fromAlias("rt_slot")))
                                .joinEntity("rt_slot", DQLJoinType.left, SessionMark.class, "rt_mark",
                                            DQLExpressions.eq(
                                                    DQLExpressions.property("rt_slot"),
                                                    DQLExpressions.property(SessionMark.slot().fromAlias("rt_mark"))))
                                .where(DQLExpressions.isNull(DQLExpressions.property("rt_mark.id")))
                                .joinEntity("rt_slot", DQLJoinType.inner, SessionRetakeDocument.class, "rt",
                                            DQLExpressions.eq(
                                                    DQLExpressions.property("rt"),
                                                    DQLExpressions.property(SessionDocumentSlot.document().fromAlias("rt_slot"))))
                                .where(DQLExpressions.eq(
                                        DQLExpressions.property(SessionRetakeDocument.sessionObject().fromAlias("rt")),
                                        DQLExpressions.value(model.getSessionObject()))).buildQuery()))
                .joinEntity("b_slot", DQLJoinType.inner, SessionBulletinDocument.class, "b",
                            DQLExpressions.eq(
                                    DQLExpressions.property(SessionDocumentSlot.document().fromAlias("b_slot")),
                                    DQLExpressions.property("b")))
                .where(DQLExpressions.eq(
                        DQLExpressions.property(SessionBulletinDocument.sessionObject().fromAlias("b")),
                        DQLExpressions.value(model.getSessionObject())))
                        // часть дисциплины
                .where(DQLExpressions.in(
                        DQLExpressions.property(SessionDocumentSlot.studentWpeCAction().studentWpe().registryElementPart().fromAlias("b_slot")),
                        model.getElementList()))
                        // студент
                .where(DQLExpressions.in(
                        DQLExpressions.property(SessionDocumentSlot.studentWpeCAction().studentWpe().student().fromAlias("b_slot")),
                        model.getStudentList()));

        DQLSelectColumnNumerator n = new DQLSelectColumnNumerator(dql);

        int c_regel = n.column(property(SessionBulletinDocument.group().activityPart().id().fromAlias("b")));
        int c_wpca = n.column(property(SessionDocumentSlot.studentWpeCAction().id().fromAlias("b_slot")));

        List<Object[]> result = getList(dql);

        boolean isRetakeDocCreated = false;
        SessionRetakeDocument doc = null;

        for (Object[] item : result) {

            if (!isRetakeDocCreated) {
                SessionComission docCommission = new SessionComission();
                session.save(docCommission);

                EppRegistryElementPart regElement = (EppRegistryElementPart) session.load(EppRegistryElementPart.class, (Serializable) item[c_regel]);

                doc = new SessionRetakeDocument();
                doc.setSessionObject(model.getSessionObject());
                doc.setNumber(INumberQueueDAO.instance.get().getNextNumber(doc));
                doc.setFormingDate(new Date());
                doc.setRegistryElementPart(regElement);
                doc.setCommission(docCommission);
                session.save(doc);
                isRetakeDocCreated = true;
            }

            SessionComission slotCommission = new SessionComission();
            session.save(slotCommission);

            EppStudentWpeCAction wpca = (EppStudentWpeCAction) session.load(EppStudentWpeCAction.class, (Serializable) item[c_wpca]);
            SessionDocumentSlot newSlot = new SessionDocumentSlot();
            newSlot.setDocument(doc);
            newSlot.setStudentWpeCAction(wpca);
            newSlot.setCommission(slotCommission);
            session.save(newSlot);
        }

        session.flush();
        session.clear();

    }

    protected Session lock(Long orgUnitId) {
        Session session = getSession();
        NamedSyncInTransactionCheckLocker.register(session, "session-retake-" + String.valueOf(orgUnitId));
        return session;
    }

    private DQLSelectBuilder getWPCASubDQL(Model model) {

        return new DQLSelectBuilder()
                .fromEntity(SessionMark.class, "gb_mark")
                .joinPath(DQLJoinType.inner, SessionMark.slot().fromAlias("gb_mark"), "gb_slot")
                .column(DQLExpressions.property(SessionDocumentSlot.studentWpeCAction().id().fromAlias("gb_slot")))
                .where(DQLExpressions.eq(
                        DQLExpressions.property(SessionMark.cachedMarkPositiveStatus().fromAlias("gb_mark")),
                        DQLExpressions.value(Boolean.FALSE)))
                .where(DQLExpressions.isNull(
                        DQLExpressions.property(SessionDocumentSlot.studentWpeCAction().removalDate().fromAlias("gb_slot"))))
                .where(DQLExpressions.eq(
                        DQLExpressions.property(SessionDocumentSlot.inSession().fromAlias("gb_slot")),
                        DQLExpressions.value(Boolean.FALSE)))
                .joinEntity("gb_slot", DQLJoinType.inner, SessionStudentGradeBookDocument.class, "gb",
                            DQLExpressions.eq(
                                    DQLExpressions.property("gb"),
                                    DQLExpressions.property(SessionDocumentSlot.document().fromAlias("gb_slot"))))
                .where(DQLExpressions.isNull(
                        DQLExpressions.property(SessionMark.slot().studentWpeCAction().studentWpe().studentEduPlanVersion().removalDate().fromAlias("gb_mark"))));
    }

    private DQLSelectBuilder getRegistryElementPartDQL(Model model, String alias) {

        return new DQLSelectBuilder()
                .fromEntity(EppStudentWorkPlanElement.class, alias)
                .where(DQLExpressions.eq(
                        DQLExpressions.property(EppStudentWorkPlanElement.year().educationYear().id().fromAlias(alias)),
                        DQLExpressions.value(model.getSessionObject().getEducationYear().getId())))
                .where(DQLExpressions.eq(
                        DQLExpressions.property(EppStudentWorkPlanElement.part().id().fromAlias(alias)),
                        DQLExpressions.value(model.getSessionObject().getYearDistributionPart().getId())))
                .where(DQLExpressions.isNull(DQLExpressions.property(EppStudentWorkPlanElement.removalDate().fromAlias(alias))))
                .where(DQLExpressions.eq(
                        DQLExpressions.property(EppStudentWorkPlanElement.student().educationOrgUnit().groupOrgUnit().fromAlias(alias)),
                        DQLExpressions.value(model.getSessionObject().getGroupOu())))
                .where(DQLExpressions.isNull(DQLExpressions.property(EppStudentWorkPlanElement.studentEduPlanVersion().removalDate().fromAlias(alias))));
    }

    private DQLSelectBuilder getStudentSubDQLBuilder(Model model) {
        DQLSelectBuilder subDQL = new DQLSelectBuilder()
                .fromEntity(SessionMark.class, "gb_mark")
                .joinPath(DQLJoinType.inner, SessionMark.slot().fromAlias("gb_mark"), "gb_slot")
                .column(DQLExpressions.property(SessionDocumentSlot.studentWpeCAction().studentWpe().student().id().fromAlias("gb_slot")))
                .where(DQLExpressions.eq(
                        DQLExpressions.property(SessionMark.cachedMarkPositiveStatus().fromAlias("gb_mark")),
                        DQLExpressions.value(Boolean.FALSE)))
                .where(DQLExpressions.isNull(
                        DQLExpressions.property(SessionDocumentSlot.studentWpeCAction().removalDate().fromAlias("gb_slot"))))
                .where(DQLExpressions.eq(
                        DQLExpressions.property(SessionDocumentSlot.inSession().fromAlias("gb_slot")),
                        DQLExpressions.value(Boolean.FALSE)))
                .joinEntity("gb_slot", DQLJoinType.inner, SessionStudentGradeBookDocument.class, "gb",
                            DQLExpressions.eq(
                                    DQLExpressions.property("gb"),
                                    DQLExpressions.property(SessionDocumentSlot.document().fromAlias("gb_slot"))))
                .where(DQLExpressions.isNull(
                        DQLExpressions.property(SessionMark.slot().studentWpeCAction().studentWpe().studentEduPlanVersion().removalDate().fromAlias("gb_mark"))));

        if (!model.getElementList().isEmpty())
            subDQL.where(DQLExpressions.in(
                    DQLExpressions.property(SessionMark.slot().studentWpeCAction().studentWpe().registryElementPart().fromAlias("gb_mark")),
                    model.getElementList()));

        if (!model.getActionList().isEmpty()) {
            List<EppFControlActionType> controlActionTypes = new ArrayList<>();
            for (EppRegistryElementPartFControlAction registryElementPartFControlAction : model.getActionList())
                controlActionTypes.add(registryElementPartFControlAction.getControlAction());

            subDQL.where(DQLExpressions.in(
                    DQLExpressions.property(SessionMark.slot().studentWpeCAction().type().fromAlias("gb_mark")),
                    controlActionTypes));
        }

        return subDQL;
    }

    private DQLFullCheckSelectModel getPreparedSessionObjectSelectableModel(final Model model) {

        return new DQLFullCheckSelectModel() {

            @Override
            protected DQLSelectBuilder query(String alias, String filter) {

                DQLSelectBuilder dql = new DQLSelectBuilder()
                        .fromEntity(SessionObject.class, alias)
                        .where(DQLExpressions.eq(
                                DQLExpressions.property(SessionObject.orgUnit().fromAlias(alias)),
                                DQLExpressions.value(model.getOrgUnit())))
                        .order(DQLExpressions.property(SessionObject.startupDate().fromAlias(alias)));

                FilterUtils.applyLikeFilter(dql, filter,
                                            SessionObject.educationYear().title().fromAlias(alias),
                                            SessionObject.yearDistributionPart().title().fromAlias(alias));

                return dql;
            }
        };
    }

    private DQLFullCheckSelectModel getPreparedRegistryElementSelectableModel(final Model model) {

        return new DQLFullCheckSelectModel("registryElement.number", "registryElement.educationElementTitle")
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {

                DQLSelectBuilder subDQL = getRegistryElementPartDQL(model, "slot")
                        .column(EppStudentWorkPlanElement.registryElementPart().id().fromAlias("slot").s());

                DQLSelectBuilder dql = new DQLSelectBuilder()
                        .fromEntity(EppRegistryElementPart.class, alias)
                        .where(DQLExpressions.in(
                                DQLExpressions.property(EppRegistryElementPart.id().fromAlias(alias)),
                                subDQL.buildQuery()))
                        .order(DQLExpressions.property(EppRegistryElementPart.registryElement().title().fromAlias(alias)))
                        .order(DQLExpressions.property(EppRegistryElementPart.registryElement().owner().title().fromAlias(alias)));

                FilterUtils.applyLikeFilter(dql, filter, EppRegistryElementPart.registryElement().number().fromAlias(alias), EppRegistryElementPart.registryElement().title().fromAlias(alias));

                return dql;
            }
        };
    }

    private DQLFullCheckSelectModel getActionTypeSelectableModel(final Model model) {

        return new DQLFullCheckSelectModel("title") {
            protected DQLSelectBuilder query(String alias, String filter) {

                DQLSelectBuilder dql = new DQLSelectBuilder();

                if (!model.getElementList().isEmpty()) {

                    dql.fromEntity(EppRegistryElementPartFControlAction.class, alias);
                    dql.where(DQLExpressions.in(
                            DQLExpressions.property(EppRegistryElementPartFControlAction.part().id().fromAlias(alias)),
                            CommonDAO.ids(model.getElementList())));
                    if (!StringUtils.isEmpty(filter))
                        dql.where(like(EppRegistryElementPartFControlAction.controlAction().title().fromAlias(alias), filter));

                    dql.order(DQLExpressions.property(EppRegistryElementPartFControlAction.controlAction().title().fromAlias(alias)));

                }
                else {
                    DQLSelectBuilder subDQL = getRegistryElementPartDQL(model, "slot")
                            .column(EppStudentWorkPlanElement.registryElementPart().id().fromAlias("slot").s());

                    dql.fromEntity(EppRegistryElementPartFControlAction.class, alias)
                            .where(DQLExpressions.in(
                                    DQLExpressions.property(EppRegistryElementPartFControlAction.part().id().fromAlias(alias)),
                                    subDQL.getQuery()));

                    if (!StringUtils.isEmpty(filter))
                        dql.where(like(EppRegistryElementPartFControlAction.controlAction().title().fromAlias(alias), filter));

                    dql.order(DQLExpressions.property(EppRegistryElementPartFControlAction.controlAction().title().fromAlias(alias)));
                }

                return dql;
            }
        };
    }

    private DQLFullCheckSelectModel getStudentSelectableModel(final Model model) {

        return new DQLFullCheckSelectModel(Student.titleWithFio().s()) {

            @Override
            protected DQLSelectBuilder query(String alias, String filter) {

                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(Student.class, alias)
                        .where(DQLExpressions.in(
                                DQLExpressions.property(Student.id().fromAlias(alias)),
                                getStudentSubDQLBuilder(model).buildQuery()))
                        .where(DQLExpressions.eq(
                                DQLExpressions.property(Student.educationOrgUnit().formativeOrgUnit().fromAlias(alias)),
                                DQLExpressions.value(model.getOrgUnit())))
                        .order(DQLExpressions.property(Student.person().identityCard().fullFio().fromAlias(alias)));

                FilterUtils.applySimpleLikeFilter(builder, alias, Student.person().identityCard().fullFio(), filter);

                return builder;
            }
        };
    }
}
