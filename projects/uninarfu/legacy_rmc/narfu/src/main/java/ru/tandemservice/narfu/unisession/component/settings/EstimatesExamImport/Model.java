package ru.tandemservice.narfu.unisession.component.settings.EstimatesExamImport;

import org.apache.tapestry.request.IUploadFile;

public class Model

{

    private IUploadFile _uploadFile;


    public IUploadFile getUploadFile()
    {
        return this._uploadFile;
    }

    public void setUploadFile(IUploadFile uploadFile)
    {
        this._uploadFile = uploadFile;
    }


}