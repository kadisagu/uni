package ru.tandemservice.narfu.base.ext.EcDistribution.logic;

import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.uniec.base.bo.EcDistribution.logic.IEcDistributionListPrint;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgDistribution;

public interface IEcDistributionListPrintNarfu extends IEcDistributionListPrint {

    public static final SpringBeanCache<IEcDistributionListPrintNarfu> instance = new SpringBeanCache<IEcDistributionListPrintNarfu>(IEcDistributionListPrint.class.getName());

    public RtfDocument getPrintFormForDistributionPerCompetitionGroupNarfu(byte[] abyte0, IEcgDistribution iecgdistribution);
}
