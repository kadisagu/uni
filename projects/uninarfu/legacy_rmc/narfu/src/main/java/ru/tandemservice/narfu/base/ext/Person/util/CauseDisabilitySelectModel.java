package ru.tandemservice.narfu.base.ext.Person.util;

import org.hibernate.Session;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.component.selection.ListResult;
import org.tandemframework.tapsupport.component.selection.SingleSelectTextModel;
import ru.tandemservice.narfu.entity.PersonBenefitExt;

import java.util.List;

public class CauseDisabilitySelectModel extends SingleSelectTextModel {

    private Session session;

    public CauseDisabilitySelectModel(Session session) {
        this.session = session;
    }

    @Override
    public ListResult<String> findValues(String filter) {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PersonBenefitExt.class, "ext")
                .column(DQLExpressions.property(PersonBenefitExt.causeDisability().fromAlias("ext")))
                .setPredicate(DQLPredicateType.distinct)
                .where(DQLExpressions.likeUpper(DQLExpressions.property(PersonBenefitExt.causeDisability().fromAlias("ext")), DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));

//		List<String> list = IUniBaseDao.instance.get().getList(builder);
//		Number number = IUniBaseDao.instance.get().getCount(builder);
        List<String> list = builder.createStatement(session).setMaxResults(50).list();
        Number number = (Number) builder.createCountStatement(new DQLExecutionContext(session)).uniqueResult();
        return new ListResult<String>(list, number != null ? number.intValue() : 0L);

    }

}
