package ru.tandemservice.narfu.uniec.component.report.ActPersonInfo.List;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.narfu.uniec.entity.report.NarfuActPersonInfo;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

public class DAO
        extends UniDao<Model>
        implements IDAO
{

    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
    }

    public void prepareListDataSource(Model model)
    {
        EnrollmentCampaign enrollmentCampaign = (EnrollmentCampaign) model.getSettings().get("enrollmentCampaign");
        MQBuilder builder = new MQBuilder(NarfuActPersonInfo.ENTITY_CLASS, "r");
        builder.add(MQExpression.eq("r", NarfuActPersonInfo.enrollmentCampaign().s(), enrollmentCampaign));
        (new OrderDescriptionRegistry("r")).applyOrder(builder, model.getDataSource().getEntityOrder());

        DynamicListDataSource dataSource = model.getDataSource();
        dataSource.setCountRow(builder.getResultCount(getSession()));
        UniUtils.createPage(model.getDataSource(), builder, getSession());
    }

    public void deleteRow(IBusinessComponent component)
    {
        Long id = (Long) component.getListenerParameter();
        NarfuActPersonInfo report = (NarfuActPersonInfo) getNotNull(NarfuActPersonInfo.class, id);
        delete(report);
        delete(report.getContent());
    }


}
