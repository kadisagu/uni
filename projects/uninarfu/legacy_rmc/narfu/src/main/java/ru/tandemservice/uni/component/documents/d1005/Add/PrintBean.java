package ru.tandemservice.uni.component.documents.d1005.Add;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.uni.component.documents.DocumentPrintBean;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.util.NumberConvertingUtil;


public class PrintBean extends DocumentPrintBean<Model> {
    @Override
    protected RtfInjectModifier createInjectModifier(Model model)
    {
        RtfInjectModifier injectModifier = super.createInjectModifier(model);

        injectModifier
                .put("date", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getFormingDate()))
                .put("num", Integer.toString(model.getNumber()))
                .put("fio", model.getStudentTitleStr())
                .put("course", NumberConvertingUtil.getSpelledNumeric(model.getStudent().getCourse().getIntValue(), false, NumberConvertingUtil.GENETIVE_CASE))
                .put("spec", model.getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getOkso() + " " + model.getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getTitle())
                .put("date_begin", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getDateFrom()))
                .put("date_end", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getDateTo()))
                .put("topic", "«" + model.getScientificWorkTheme() + "»");

        // формируем надпись по форме обучения студента
        String code = model.getStudent().getEducationOrgUnit().getDevelopForm().getCode();
        if (DevelopFormCodes.FULL_TIME_FORM.equals(code))
            injectModifier.put("form", "очной формы");
        else if (DevelopFormCodes.CORESP_FORM.equals(code))
            injectModifier.put("form", "заочной формы");
        else
            injectModifier.put("form", "очно-заочной формы");
        return injectModifier;
    }
}
