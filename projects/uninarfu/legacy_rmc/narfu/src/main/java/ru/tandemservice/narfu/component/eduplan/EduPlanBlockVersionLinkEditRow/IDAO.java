package ru.tandemservice.narfu.component.eduplan.EduPlanBlockVersionLinkEditRow;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {
    public void saveLink(Model model);
}
