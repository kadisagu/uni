package ru.tandemservice.narfu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_narfu_1x0x0_44to45 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность studentDocumentRMC

        //  свойство base стало обязательным
        {
            // удаляем некорректные записи
            tool.executeUpdate("delete from studentdocumentrmc_t where base_id is null");

            // сделать колонку NOT NULL
            tool.setColumnNullable("studentdocumentrmc_t", "base_id", false);

        }


    }
}