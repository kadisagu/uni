package ru.tandemservice.narfu.uniec.component.report.EntrantRequestQtyAll;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;

public class Controller extends AbstractBusinessController<IDAO, Model> {
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);
    }

    public void onClickApply(IBusinessComponent component) throws Exception
    {
        final Model model = getModel(component);

        // создаем отчет
        getDao().preparePrintReport(model);

        // регистрируем его в универсальном компоненте рендера
        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(model.getContent(), model.getFileName());

        // переходим на универсальный компонент рендера отчета
        activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new UniMap()
                .add("id", id).add("extension", "xls")
        ));
    }
}