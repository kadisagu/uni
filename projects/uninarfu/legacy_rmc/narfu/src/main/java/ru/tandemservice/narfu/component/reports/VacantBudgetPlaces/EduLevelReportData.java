package ru.tandemservice.narfu.component.reports.VacantBudgetPlaces;

import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013;

import java.util.*;

public class EduLevelReportData {
    private String name;
    private String npNum;
    private String formativeOrgUnit;
    //общее кол-во студентов
    private Map<Course, Integer> totalCount = new HashMap<Course, Integer>();
    //кол-во студентов-договорников с первого по COURSES_COUNT курс
    private Map<Course, Integer> budgetCount = new HashMap<Course, Integer>();
    private Map<Course, Integer> planCount = new HashMap<>();
    private Map<Course, Integer> vacantCount = new HashMap<>();
    private DevelopPeriod developPeriod;
    private DevelopCondition developCondition;
    private EducationLevels eduLevel;
    private EduProgramSubject2013 eduProgramSubject2013;
    private Set<String> structureEducationLevelsSet = new HashSet<>();
    private Map<Course, Map<String, Integer>> totalStructureCount = new HashMap<>();
    private Map<Course, Map<String, Integer>> budgetStructureCount = new HashMap<>();
    private Map<Course, Map<String, Integer>> planStructureCount = new HashMap<>();
    private Map<Course, Map<String, Integer>> vacantStructureCount = new HashMap<>();
    private Map<Course, Map<String, Integer>> courseTotalStructureCount = new HashMap<Course, Map<String, Integer>>();
    private Map<Course, Map<String, Integer>> courseBudgetStructureCount = new HashMap<Course, Map<String, Integer>>();


    public EduLevelReportData() {

    }

    public DevelopCondition getDevelopCondition() {
        return developCondition;
    }

    public void setDevelopCondition(DevelopCondition developCondition) {
        this.developCondition = developCondition;
    }

    public EduLevelReportData(List<Course> courses) {
        for (Course course : courses) {
            totalCount.put(course, 0);
            budgetCount.put(course, 0);
            planCount.put(course, 0);
            vacantCount.put(course, 0);
        }
    }

    public DevelopPeriod getDevelopPeriod() {
        return developPeriod;
    }

    public void setDevelopPeriod(DevelopPeriod developPeriod) {
        this.developPeriod = developPeriod;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNpNum() {
        return npNum;
    }

    public void setNpNum(String npNum) {
        this.npNum = npNum;
    }

    public Map<Course, Integer> getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Map<Course, Integer> totalCount) {
        this.totalCount = totalCount;
    }

    public Map<Course, Integer> getBudgetCount() {
        return budgetCount;
    }

    public void setBudgetCount(Map<Course, Integer> budgetCount) {
        this.budgetCount = budgetCount;
    }

    public Map<Course, Integer> getPlanCount() {
        return planCount;
    }

    public void setPlanCount(Map<Course, Integer> planCount) {
        this.planCount = planCount;
    }

    public Map<Course, Integer> getVacantCount() {
        return vacantCount;
    }

    public void setVacantCount(Map<Course, Integer> vacantCount) {
        this.vacantCount = vacantCount;
    }

    public EducationLevels getEduLevel() {
        return eduLevel;
    }

    public void setEduLevel(EducationLevels eduLevel) {
        this.eduLevel = eduLevel;
    }

    public Set<String> getStructureEducationLevelsSet() {
        return structureEducationLevelsSet;
    }

    public void setStructureEducationLevelsSet(Set<String> structureEducationLevelsSet) {
        this.structureEducationLevelsSet = structureEducationLevelsSet;
    }

    public Map<Course, Map<String, Integer>> getTotalStructureCount() {
        return totalStructureCount;
    }

    public void setTotalStructureCount(Map<Course, Map<String, Integer>> totalStructureCount) {
        this.totalStructureCount = totalStructureCount;
    }

    public Map<Course, Map<String, Integer>> getBudgetStructureCount() {
        return budgetStructureCount;
    }

    public void setBudgetStructureCount(Map<Course, Map<String, Integer>> budgetStructureCount) {
        this.budgetStructureCount = budgetStructureCount;
    }

    public Map<Course, Map<String, Integer>> getPlanStructureCount() {
        return planStructureCount;
    }

    public void setPlanStructureCount(Map<Course, Map<String, Integer>> planStructureCount) {
        this.planStructureCount = planStructureCount;
    }

    public Map<Course, Map<String, Integer>> getVacantStructureCount() {
        return vacantStructureCount;
    }

    public void setVacantStructureCount(Map<Course, Map<String, Integer>> vacantStructureCount) {
        this.vacantStructureCount = vacantStructureCount;
    }

    public String getFormativeOrgUnit() {
        return formativeOrgUnit;
    }

    public void setFormativeOrgUnit(String formativeOrgUnit) {
        this.formativeOrgUnit = formativeOrgUnit;
    }

    public Map<Course, Map<String, Integer>> getCourseTotalStructureCount() {
        return courseTotalStructureCount;
    }

    public void setCourseTotalStructureCount(Map<Course, Map<String, Integer>> courseTotalStructureCount) {
        this.courseTotalStructureCount = courseTotalStructureCount;
    }

    public Map<Course, Map<String, Integer>> getCourseBudgetStructureCount() {
        return courseBudgetStructureCount;
    }

    public void setCourseBudgetStructureCount(Map<Course, Map<String, Integer>> courseBudgetStructureCount) {
        this.courseBudgetStructureCount = courseBudgetStructureCount;
    }

    public EduProgramSubject2013 getEduProgramSubject2013() {
        return eduProgramSubject2013;
    }

    public void setEduProgramSubject2013(EduProgramSubject2013 eduProgramSubject2013) {
        this.eduProgramSubject2013 = eduProgramSubject2013;
    }
}
