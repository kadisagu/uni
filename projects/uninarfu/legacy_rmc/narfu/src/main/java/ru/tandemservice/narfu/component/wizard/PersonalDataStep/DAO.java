package ru.tandemservice.narfu.component.wizard.PersonalDataStep;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.person.catalog.entity.MilitaryOffice;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudentrmc.entity.PersonNARFU;
import ru.tandemservice.uniecrmc.entity.catalog.IndividualAchievements;
import ru.tandemservice.uniecrmc.entity.entrant.Entrant2IndividualAchievements;
import ru.tandemservice.uniec.entity.catalog.EnrollmentRecommendation;
import ru.tandemservice.uniec.entity.entrant.EntrantEnrolmentRecommendation;

import java.util.Iterator;
import java.util.List;


public class DAO extends
        ru.tandemservice.uniecrmc.component.wizard.PersonalDataStep.DAO implements IDAO
{

    @Override
    public void prepare(ru.tandemservice.uniec.component.wizard.PersonalDataStep.Model model) {
        // TODO Auto-generated method stub
        super.prepare(model);

        Model myModel = (Model) model;

        if (myModel.getPersonNARFU() == null) {
            PersonNARFU personNARFU = get(PersonNARFU.class, PersonNARFU.L_PERSON, model.getEntrant().getPerson());
            if (personNARFU == null) {
                personNARFU = new PersonNARFU();
                personNARFU.setPerson(model.getEntrant().getPerson());
            }
            myModel.setPersonNARFU(personNARFU);
        }
        Model modelExt = (Model) model;
        modelExt.getEntrant2IndividualAchievements().setEntrant(model.getEntrant());
        modelExt.setIndividualAchievementsModel(new LazySimpleSelectModel<>(IndividualAchievements.class));
        myModel.setEnlistmentOfficesModel(new FullCheckSelectModel() {
            @Override
            public ListResult<MilitaryOffice> findValues(String filter) {
                List<MilitaryOffice> list;
                if (StringUtils.isEmpty(filter))
                    list = getList(MilitaryOffice.class, new String[]{MilitaryOffice.title().s()});
                else {
                    MQBuilder builder = new MQBuilder(MilitaryOffice.ENTITY_CLASS, "mo")
                            .add(MQExpression.like("mo", MilitaryOffice.title(), "%" + filter + "%"))
                            .addOrder("mo", MilitaryOffice.title().s());
                    list = builder.getResultList(getSession());
                }

                return new ListResult<MilitaryOffice>(list);
            }
        });

    }


    @Override
    public void update(
            ru.tandemservice.uniec.component.wizard.PersonalDataStep.Model model)
    {
        // TODO Auto-generated method stub
        Model myModel = (Model) model;

        if (myModel.getAttachedEnlistmentOffice() != null)
            model.getMilitaryStatus().setAttachedIssuancePlace(myModel.getAttachedEnlistmentOffice().getTitle());
        else
            model.getMilitaryStatus().setAttachedIssuancePlace(null);

        if (myModel.getMilitaryEnlistmentOffice() != null)
            model.getMilitaryStatus().setStayIssuancePlace(myModel.getMilitaryEnlistmentOffice().getTitle());
        else
            model.getMilitaryStatus().setStayIssuancePlace(null);

        super.update(model);

        // сохранить можно, только если нет ошибок
        ErrorCollector errors = ContextLocal.getErrorCollector();
        if (!errors.hasErrors()) {
            saveOrUpdate(myModel.getPersonNARFU());
        }
        getSession().saveOrUpdate(model.getEntrant());

        myModel = (Model) model;

        if (myModel.isHasIndividualAchievement()) {
            EnrollmentRecommendation recommendation = get(EnrollmentRecommendation.class, EnrollmentRecommendation.code(), "uniecrmc.1");//Индивидуальные достижения

            EntrantEnrolmentRecommendation entrantEnrolmentRecommendation = new EntrantEnrolmentRecommendation();
            entrantEnrolmentRecommendation.setEntrant(model.getEntrant());
            entrantEnrolmentRecommendation.setRecommendation(recommendation);

            getSession().saveOrUpdate(entrantEnrolmentRecommendation);
            for (Entrant2IndividualAchievements achievements : ((Model) model).getSelectedAchievementsList()) {
                if (achievements.getId() < 0L) {
                    achievements.setEntrant(model.getEntrant());
                    getSession().save(achievements);
                }
            }
        }
    }

    @Override
    public void prepareSelectAchievements(Model model) {
        Long min = 0L;
        Entrant2IndividualAchievements item;
        for (Iterator i$ = model.getSelectedAchievementsList().iterator(); i$.hasNext(); min = Math.min(min.longValue(), item.getId().longValue()))
            item = (Entrant2IndividualAchievements) i$.next();
        model.getEntrant2IndividualAchievements().setId(Long.valueOf(min - 1L));
        model.getEntrant2IndividualAchievements().setEntrant(model.getEntrant());
        model.getSelectedAchievementsList().add(model.getEntrant2IndividualAchievements());
        model.setEntrant2IndividualAchievements(new Entrant2IndividualAchievements());
        model.getEntrant2IndividualAchievements().setEntrant(model.getEntrant());
    }


    @Override
    public void deleteAchievement(Model model, Long id) {
        List<Entrant2IndividualAchievements> list = model.getSelectedAchievementsList();
        int i = 0;
        while ((i < list.size()) && (!(list.get(i)).getId().equals(id))) i++;
        if (i == list.size()) return;
        list.remove(i);
        if (id > 0L) {
            delete(id);
        }
    }

}
