package ru.tandemservice.narfu.component.settings.Course2EnrollmentCampaign;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController<IDAO, Model> {
    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        ((IDAO) getDao()).prepare(getModel(component));
    }

    public void onClickApply(IBusinessComponent component) {
        getDao().update(getModel(component));
        deactivate(component);
    }
}
