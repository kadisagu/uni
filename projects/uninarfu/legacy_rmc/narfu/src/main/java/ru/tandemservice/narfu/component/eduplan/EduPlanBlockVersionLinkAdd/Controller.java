package ru.tandemservice.narfu.component.eduplan.EduPlanBlockVersionLinkAdd;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController<IDAO, Model> {
    public void onRefreshComponent(IBusinessComponent component) {
        getDao().prepare(getModel(component));
    }

    public void onClickApply(IBusinessComponent component) {
        getDao().update(getModel(component));
        deactivate(component);
    }
}
