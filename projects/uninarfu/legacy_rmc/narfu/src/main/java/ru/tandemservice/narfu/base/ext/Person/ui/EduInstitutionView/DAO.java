package ru.tandemservice.narfu.base.ext.Person.ui.EduInstitutionView;

import ru.tandemservice.narfu.base.ext.Person.ui.Tab.PersonNarfuUtil;

public class DAO extends org.tandemframework.shared.person.base.bo.Person.ui.EduInstitutionView.DAO {

    @Override
    public void prepare(org.tandemframework.shared.person.base.bo.Person.ui.EduInstitutionView.Model model)
    {
        super.prepare(model);

        Model myModel = (Model) model;
        myModel.setPersonEduInstitutionNARFU(PersonNarfuUtil.getPersonEduinstitution(model.getPersonEduInstitution()));
    }
}
