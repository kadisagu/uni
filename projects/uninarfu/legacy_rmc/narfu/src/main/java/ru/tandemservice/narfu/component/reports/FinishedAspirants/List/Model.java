package ru.tandemservice.narfu.component.reports.FinishedAspirants.List;

import org.tandemframework.core.component.State;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.sec.runtime.SecurityRuntime;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.narfu.uniec.entity.report.FinishedAspirantsReport;

@State(keys = "orgUnitId", bindings = "orgUnitId")
public class Model {

    private IDataSettings settings;

    private ISelectModel compensationTypeModel;

    private Long orgUnitId;
    private OrgUnit orgUnit;
    private CommonPostfixPermissionModel secModel;

    private DynamicListDataSource<FinishedAspirantsReport> dataSource;

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public DynamicListDataSource<FinishedAspirantsReport> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<FinishedAspirantsReport> dataSource) {
        this.dataSource = dataSource;
    }


    public CommonPostfixPermissionModel getSecModel() {
        return secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel) {
        this.secModel = secModel;
    }

    public Object getSecuredObject() {
        return null == getOrgUnit() ? SecurityRuntime.getInstance().getCommonSecurityObject() : getOrgUnit();
    }

    public String getViewKey() {

        return getOrgUnit() == null ? "viewGlobalStoredReport" : getSecModel().getPermission("orgUnit_viewFinishedAspirantsReport");
    }

    public String getAddKey()
    {
        return getOrgUnit() == null ? "addGlobalStoredReport" : getSecModel().getPermission("orgUnit_addFinishedAspirantsReport");
    }

    public String getPrintKey()
    {
        return getOrgUnit() == null ? "printGlobalStoredReport" : getSecModel().getPermission("orgUnit_viewFinishedAspirantsReport");
    }

    public String getDeleteKey()
    {
        return getOrgUnit() == null ? "deleteGlobalStoredReport" : getSecModel().getPermission("orgUnit_deleteFinishedAspirantsReport");
    }

    public OrgUnit getOrgUnit() {
        return orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit) {
        this.orgUnit = orgUnit;
    }

    public Long getOrgUnitId() {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId) {
        this.orgUnitId = orgUnitId;
    }

    public ISelectModel getCompensationTypeModel() {
        return compensationTypeModel;
    }

    public void setCompensationTypeModel(ISelectModel compensationTypeModel) {
        this.compensationTypeModel = compensationTypeModel;
    }
}
