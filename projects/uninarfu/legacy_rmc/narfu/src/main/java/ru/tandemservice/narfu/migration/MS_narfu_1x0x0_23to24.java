package ru.tandemservice.narfu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_narfu_1x0x0_23to24 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.12"),
                        new ScriptDependency("org.tandemframework.shared", "1.2.5"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.2.5")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность narfuReportStudentsByUGS

        // создано свойство activeStatus
        {
            // создать колонку
            tool.createColumn("narfureportstudentsbyugs_t", new DBColumn("activestatus_p", DBType.createVarchar(255)));

        }

        // создано свойство studentStatus
        {
            // создать колонку
            tool.createColumn("narfureportstudentsbyugs_t", new DBColumn("studentstatus_p", DBType.createVarchar(255)));

        }


    }
}