package ru.tandemservice.narfu.uniec.component.report.EntrantRegistration.EntrantRegistrationAddExt;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.eventssecrmc.entity.EntrantRequestSecExt;
import ru.tandemservice.narfu.uniec.component.report.EntrantRegistration.EntrantRegistrationAdd.NarfuEntrantRegistrationReportBuilder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class NarfuEntrantRegistrationReportBuilderExt extends NarfuEntrantRegistrationReportBuilder {

    private Model model;

    public NarfuEntrantRegistrationReportBuilderExt(Model model, Session session) {
        super(new ru.tandemservice.narfu.uniec.component.report.EntrantRegistration.EntrantRegistrationAdd.Model(), session);
        this.model = model;
        this.date = model.getReport().getDateFrom();
        super.getModel().setEnrollmentCampaign(model.getEnrollmentCampaign());
    }

    @Override
    protected String getTemplateDocumentName() {
        return "narfuEntrantRegistration_ok";
    }

    @Override
    protected List<OrgUnit> getFormativeOrgUnitList(Date startDate, Date endDate) {
        List<OrgUnit> list = model.getOrgUnitList();

        if (list == null || list.isEmpty() || !model.isOrgUnitActive()) {
            DQLSelectBuilder subDql = new DQLSelectBuilder().fromEntity(EntrantRequestSecExt.class, "ext")
                    .where(DQLExpressions.eqValue(DQLExpressions.property(EntrantRequestSecExt.entrantRequest().entrant().enrollmentCampaign().fromAlias("ext")), model.getEnrollmentCampaign()))
                    .column(EntrantRequestSecExt.orgUnit().id().fromAlias("ext").s());

            DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(OrgUnit.class, "e")
                    .where(DQLExpressions.in(DQLExpressions.property(OrgUnit.id().fromAlias("e")), subDql.getQuery()))
                    .order(OrgUnit.shortTitle().fromAlias("e").s());
            list = IUniBaseDao.instance.get().getList(dql);
        }
        else
            Collections.sort(list, OrgUnit.TITLED_COMPARATOR);

        return list;
    }

    @Override
    protected String[] getRow(EntrantRequest request, List<CompensationType> lstCompensationType, List<DevelopCondition> lstDevelopCondition, List<DevelopForm> lstDevelopForm) {
        String[] arr = super.getRow(request, lstCompensationType, lstDevelopCondition, lstDevelopForm);

        arr[1] = request.getStringNumber();
        arr[2] = request.getEntrant().getPerson().getFullFio();

        List<RequestedEnrollmentDirection> redList = IUniBaseDao.instance.get().getList(
                RequestedEnrollmentDirection.class,
                RequestedEnrollmentDirection.entrantRequest(),
                request,
                new String[]{RequestedEnrollmentDirection.priority().s()}
        );

        String original = "Нет";
        if (redList.get(0).isOriginalDocumentHandedIn())
            original = "оригинал";

        arr[4] = original;

        PersonEduInstitution pei = request.getEntrant().getPerson().getPersonEduInstitution();
        if (pei == null)
            arr[3] = "нд";
        else {
            String docInfo = "";
            if (pei.getSeria() != null)
                docInfo = pei.getSeria();
            if (pei.getNumber() != null)
                docInfo += " " + pei.getNumber();

            arr[3] = docInfo;
        }

        List<String> directionList = new ArrayList<String>();
        for (int i = 0; i < redList.size(); i++) {
            RequestedEnrollmentDirection red = redList.get(i);

            StringBuilder sb = new StringBuilder()
                    .append(red.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getInheritedOksoPrefix())
                    .append(" ")
                    .append(red.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getTitle())
                    .append(" (")
                    .append(red.getEnrollmentDirection().getEducationOrgUnit().getDevelopForm().getGenCaseTitle())
                    .append(", ")
                    .append(red.getEnrollmentDirection().getEducationOrgUnit().getDevelopCondition().getTitle().toLowerCase())
                    .append(", ")
                    .append(red.getCompensationType().getTitle().toLowerCase())
                    .append(")");
            directionList.add(sb.toString());
        }
        arr[5] = StringUtils.join(directionList, "; ");


        return arr;
    }

    @Override
    protected List<EducationLevelsHighSchool> getEducationLevelsHighSchoolList(Date startDate, Date endDate, OrgUnit orgUnit) {
        return Arrays.asList((EducationLevelsHighSchool) null);
    }

    @Override
    protected void fillDQLRequestedEnrollmentDirection(DQLSelectBuilder dql, List<OrgUnit> orgUnitList) {
        dql.joinEntity(
                "rd",
                DQLJoinType.inner,
                EntrantRequestSecExt.class,
                "ext",
                DQLExpressions.eq(
                        DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().fromAlias("rd")),
                        DQLExpressions.property(EntrantRequestSecExt.entrantRequest().fromAlias("ext")))
        )
                .where(DQLExpressions.in(DQLExpressions.property(EntrantRequestSecExt.orgUnit().fromAlias("ext")), orgUnitList));

        dql.where(eq(property(RequestedEnrollmentDirection.priority().fromAlias("rd")), value(1)));

        // по приемной компании
        dql.where(eq(property(RequestedEnrollmentDirection.entrantRequest().entrant().enrollmentCampaign().fromAlias("rd")), value(model.getEnrollmentCampaign())));
    }
}
