package ru.tandemservice.narfu.component.settings.YearPartEndDate.List;


import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.narfu.entity.YearPartStartEndDate;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.List;


public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model)
    {

        // указываем сортировку
        model.setEducationYearList(this.getList(EducationYear.class, new String[]{EducationYear.P_INT_VALUE}));

        // указываем сортировку более длинным путем
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(YearDistributionPart.class, "ent")
                .order(DQLExpressions.property(YearDistributionPart.code().fromAlias("ent")), OrderDirection.asc);
        List<YearDistributionPart> lst = this.getList(dql);

        model.setYearDistributionPartList(lst);
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(YearPartStartEndDate.ENTITY_CLASS, "ypsed");
        if (model.getEducationYear() != null)
            builder.add(MQExpression.eq("ypsed", YearPartStartEndDate.educationYear(), model.getEducationYear()));

        if (model.getYearDistributionPart() != null)
            builder.add(MQExpression.eq("ypsed", YearPartStartEndDate.part(), model.getEducationYear()));

        new OrderDescriptionRegistry("ypsed").applyOrder(builder, model.getDataSource().getEntityOrder());
        UniUtils.createPage(model.getDataSource(), builder, getSession());
    }
}
