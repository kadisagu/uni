package ru.tandemservice.narfu.component.reports.FinishedAspirants.Add;

import ru.tandemservice.narfu.uniec.entity.report.FinishedAspirantsReport;
import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {

    public FinishedAspirantsReport createReport(Model model);
}
