package ru.tandemservice.narfu.uniec.component.order.EnrollmentOrderPub.EnrollmentParagraphExtractsDialog;

import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentParagraph;

public class DAO extends UniDao<Model> implements IDAO {


    @Override
    public void prepare(Model model) {

        final EnrollmentOrder enrollmentOrder = get(EnrollmentOrder.class, model.getOrderId());

        model.setParagraphsModel(new FullCheckSelectModel() {
            @Override
            public ListResult findValues(String arg0) {
                return new ListResult(getList(EnrollmentParagraph.class, EnrollmentParagraph.order(), enrollmentOrder));
            }
        });

    }
}
