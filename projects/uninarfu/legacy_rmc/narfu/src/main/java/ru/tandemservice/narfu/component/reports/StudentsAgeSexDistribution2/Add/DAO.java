package ru.tandemservice.narfu.component.reports.StudentsAgeSexDistribution2.Add;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.uni.component.reports.studentsAgeSexDistribution.Add.Model;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.report.StudentsAgeSexDistributionReport;
import ru.tandemservice.uni.util.FilterUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DAO extends ru.tandemservice.uni.component.reports.studentsAgeSexDistribution.Add.DAO {
    private final static int TOTAL_ROWINDEX = 0;
    private final static int FEMALE_ROWINDEX = 1;
    private final static int FULLTIME_ROWINDEX = 2;
    private final static int MALE_ROWINDEX = 3;

    @Override
    public void update(Model model) {
        IFormatter<Object> formatter = CollectionFormatter.COLLECTION_FORMATTER;

        StudentsAgeSexDistributionReport report = model.getReport();
        report.setFormingDate(new Date());
        report.setFormativeOrgUnit(StringUtils.trimToNull(formatter.format(model.getFormativeOrgUnitList())));
        report.setTerritorialOrgUnit((model.isTerritorialOrgUnitActive()) && (model.getTerritorialOrgUnitList().isEmpty()) ? "не указано" : StringUtils.trimToNull(formatter.format(model.getTerritorialOrgUnitList())));
        report.setDevelopForm(StringUtils.trimToNull(formatter.format(model.getDevelopFormList())));
        report.setDevelopConditition(StringUtils.trimToNull(formatter.format(model.getDevelopConditionList())));
        report.setDevelopTech(StringUtils.trimToNull(formatter.format(model.getDevelopTechList())));
        report.setQualification(StringUtils.trimToNull(formatter.format(model.getQualificationList())));
        report.setStudentCategory(StringUtils.trimToNull(formatter.format(model.getStudentCategoryList())));
        report.setStudentStatus(StringUtils.trimToNull(formatter.format(model.getStudentStatusList())));

        DatabaseFile databaseFile = new DatabaseFile();
        databaseFile.setContent(RtfUtil.toByteArray(getDocument(model)));
        getSession().save(databaseFile);

        report.setContent(databaseFile);
        getSession().save(report);
    }

    private RtfDocument getDocument(Model model) {
        ITemplateDocument templateDocument = getCatalogItem(TemplateDocument.class, "studentsAgeSexDistributionReport2");
        RtfDocument template = new RtfReader().read(templateDocument.getContent());
        RtfDocument document = template.getClone();

        StudentsAgeSexDistributionReport report = model.getReport();

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        //в печатной форме нет, но, возможно, пригодится
        // injectModifier.put("formingDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(report.getFormingDate())).modify(document);

        new RtfTableModifier().put("P", getParameters(report)).modify(document);

        int[][] table = getTable(model);
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[i].length; j++) {
                injectModifier.put("T" + i + j, Integer.toString(table[i][j]));
            }
        }
        injectModifier.modify(document);

        return document;
    }

    //Параметры отчета в заголовке
    private String[][] getParameters(StudentsAgeSexDistributionReport report) {
        List<String[]> parameters = new ArrayList<>();
        addParameterRow(parameters, "Формирующее подр.", report.getFormativeOrgUnit());
        addParameterRow(parameters, "Территориальное подр.", report.getTerritorialOrgUnit());
        addParameterRow(parameters, "Форма освоения", report.getDevelopForm());
        addParameterRow(parameters, "Условие освоения", report.getDevelopConditition());
        addParameterRow(parameters, "Технология освоения", report.getDevelopTech());
        addParameterRow(parameters, "Квалификация", report.getQualification());
        addParameterRow(parameters, "Категория обучаемого", report.getStudentCategory());
        addParameterRow(parameters, "Состояние", report.getStudentStatus());
        return parameters.toArray(new String[0][]);
    }

    private void addParameterRow(List<String[]> parameters, String title, String value) {
        if (!StringUtils.isEmpty(value)) {
            parameters.add(new String[]{title, value});
        }
    }

    //Содержимое таблицы
    private int[][] getTable(Model model) {
        int[][] result = new int[4][5];
        int column = 0;

        for (Student student : getReportData(model)) {
            IdentityCard identityCard = student.getPerson().getIdentityCard();
            boolean female = identityCard.getSex().getCode().equals(SexCodes.FEMALE);
            boolean fulltime = student.getEducationOrgUnit().getDevelopForm().getCode().equals(DevelopFormCodes.FULL_TIME_FORM);

            Date birthDate = identityCard.getBirthDate();

            if (birthDate != null) {
                int age = CoreDateUtils.getFullYearsAge(birthDate);
                if (age <= 28)
                    column = 1;
                if (age > 28 && age <= 35)
                    column = 2;
                if (age > 35 && age <= 40)
                    column = 3;
                if (age > 40)
                    column = 4;

                result[TOTAL_ROWINDEX][column] += 1;
                result[TOTAL_ROWINDEX][0] += 1;

                if (fulltime) {
                    result[FULLTIME_ROWINDEX][0] += 1;
                    result[FULLTIME_ROWINDEX][column] += 1;
                    if (!female) {
                        result[MALE_ROWINDEX][0] += 1;
                        result[MALE_ROWINDEX][column] += 1;
                    }
                }
                if (female) {
                    result[FEMALE_ROWINDEX][0] += 1;
                    result[FEMALE_ROWINDEX][column] += 1;
                }

            }
        }
        return result;
    }

    //Данные для запроса
    private List<Student> getReportData(Model model) {

        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(Student.class, "s");
        dql.where(DQLExpressions.eq(
                DQLExpressions.property(Student.archival().fromAlias("s")),
                DQLExpressions.value(false)
        ));
        FilterUtils.applySelectFilter(dql, "s", Student.educationOrgUnit().formativeOrgUnit(), model.getFormativeOrgUnitList());
        FilterUtils.applySelectFilter(dql, "s", Student.educationOrgUnit().territorialOrgUnit(), model.getTerritorialOrgUnitList());
        FilterUtils.applySelectFilter(dql, "s", Student.educationOrgUnit().developForm(), model.getDevelopFormList());
        FilterUtils.applySelectFilter(dql, "s", Student.educationOrgUnit().developTech(), model.getDevelopTechList());
        FilterUtils.applySelectFilter(dql, "s", Student.educationOrgUnit().developCondition(), model.getDevelopConditionList());
        FilterUtils.applySelectFilter(dql, "s", Student.educationOrgUnit().educationLevelHighSchool().educationLevel().qualification(), model.getQualificationList());
        FilterUtils.applySelectFilter(dql, "s", Student.studentCategory(), model.getStudentCategoryList());
        FilterUtils.applySelectFilter(dql, "s", Student.status(), model.getStudentStatusList());
        return dql.createStatement(getSession()).list();
    }

}
