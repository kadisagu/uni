package ru.tandemservice.narfu.utils;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.eventssecrmc.entity.EnrollmentComissionToEmployeePost;
import ru.tandemservice.narfu.entity.EnrCampaignDevelopFormDates;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

public class NarfuEntrantUtil {

    private static IUniBaseDao dao = UniDaoFacade.getCoreDao();

    //Не понимаю, зачем были добавлены группы в регулярном выражении?
    //private static final Pattern INT_PATTERN = Pattern.compile("[1-9]+[0-9]*");

    //Viktor
    private static final Pattern INT_PATTERN = Pattern.compile("\\d*");

    public static String getEntrantUniqueNumber(EntrantRequest entrantRequest) {

        return getEntrantUniqueNumber(dao.getList(RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.L_ENTRANT_REQUEST, entrantRequest, new String[]{RequestedEnrollmentDirection.P_PRIORITY}));
    }

    public static String getEntrantUniqueNumber(List<RequestedEnrollmentDirection> selectedList) {

        if (selectedList.size() > 0) {

            RequestedEnrollmentDirection requestedEnrollmentDirection = selectedList.get(0);

            EducationLevels educationLevel = requestedEnrollmentDirection.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();
            boolean isSPO = educationLevel.getLevelType().isMiddle();

            EnrollmentComissionToEmployeePost rel = null;

            if (UserContext.getInstance().getPrincipalContext() instanceof EmployeePost) {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrollmentComissionToEmployeePost.class, "rel")
                        .where(DQLExpressions.eqValue(DQLExpressions.property(EnrollmentComissionToEmployeePost.employeePost().fromAlias("rel")), ((EmployeePost) UserContext.getInstance().getPrincipalContext())))
                        .where(DQLExpressions.eqValue(DQLExpressions.property(EnrollmentComissionToEmployeePost.enrollmentCampaign().fromAlias("rel")), requestedEnrollmentDirection.getEntrantRequest().getEntrant().getEnrollmentCampaign()));
                List<EnrollmentComissionToEmployeePost> list = UniDaoFacade.getCoreDao().getList(builder);
                if (!list.isEmpty())
                    rel = list.get(0);
            }

            String divisionCode = "";

            if (rel != null && rel.getOrgUnit() != null) {
                divisionCode = rel.getOrgUnit().getDivisionCode();
            }
            else {
                //Внутренний код структурного подразделения
                divisionCode = requestedEnrollmentDirection.getEnrollmentDirection().getEducationOrgUnit().getFormativeOrgUnit().getDivisionCode();
            }

            if (divisionCode == null)
                throw new ApplicationException("Невозможно сгенерировать личный номер абитуриента. Не установлен внутренний код подразделения отборочной комиссии");

            //проверим чтобы код структурного подразделения был более 1-го символа
            // vch - добавил проверку на числовой формат - не работает INT_PATTERN.matcher "[1-9]+[0-9]*"
            if (divisionCode.length() > 1 && INT_PATTERN.matcher(divisionCode).matches())
                divisionCode = divisionCode.substring(0, 2);
            else {
                ContextLocal.getErrorCollector().addError("Невозможно сгенерировать личный номер абитуриента. Внутренний код структурного подразделенимя должен состоять из 2-х цифр");
                return null;
            }

            //Вид формы обучения
            DevelopForm developForm = requestedEnrollmentDirection.getEnrollmentDirection().getEducationOrgUnit().getDevelopForm();
            String codeForm = developForm.getCode();
            String formaO = null;
            if (DevelopFormCodes.FULL_TIME_FORM.equals(codeForm)) {
                formaO = "0";
            }
            else if (DevelopFormCodes.PART_TIME_FORM.equals(codeForm)) {
                formaO = "1";
            }
            else if (DevelopFormCodes.CORESP_FORM.equals(codeForm)) {
                formaO = "2";
            }
            if (formaO == null) {
                ContextLocal.getErrorCollector().addError("Невозможно сгенерировать личный номер абитуриента. Вид формы обучения '" + developForm.getTitle() + "' не совпадает со значением в справочнике");
                return null;
            }

            //Вид условия обучения (освоения) для СПО или квалификация для ВПО
            String codeCond = null;
            DevelopCondition developCondition = null;
            if (isSPO) {
                if (QualificationsCodes.BAZOVYY_UROVEN_S_P_O.equals(educationLevel.getQualification().getCode()))
                    codeCond = "9";
                else if (QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O.equals(educationLevel.getQualification().getCode()))
                    codeCond = "2";
            }
            else {

                developCondition = requestedEnrollmentDirection.getEnrollmentDirection().getEducationOrgUnit().getDevelopCondition();
                String codeDevelopCondition = developCondition.getCode();
                if (UniDefines.DEVELOP_CONDITION_FULL_TIME.equals(codeDevelopCondition)) {
                    codeCond = "0";
                }
                else if (UniDefines.DEVELOP_CONDITION_SHORT.equals(codeDevelopCondition)) {
                    codeCond = "1";
                }
            }

            if (codeCond == null) {
                ContextLocal.getErrorCollector().addError("Невозможно сгенерировать личный номер абитуриента. Вид условия обучения (освоения) '" + developCondition.getTitle() + "' не совпадает со значением в справочнике");
                return null;
            }

            String entrantUniqueNumber = getEntrantUniqueNumber(divisionCode, formaO, codeCond);

            return entrantUniqueNumber;
        }
        else
            return null;
    }

    private static String getEntrantUniqueNumber(String divisionCode, String developFormCode, String condition) {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Entrant.class, "e")
                .where(DQLExpressions.like(DQLExpressions.property(Entrant.personalNumber().fromAlias("e")), DQLExpressions.value((new StringBuilder()).append(divisionCode + developFormCode + condition).append("%").toString())))
                .order(DQLFunctions.cast(DQLExpressions.property(Entrant.personalNumber().fromAlias("e")), PropertyType.INTEGER), OrderDirection.desc);

        List<Entrant> list = UniDaoFacade.getCoreDao().getList(builder);

        String max = null;
        if (!list.isEmpty())
            max = list.get(0).getPersonalNumber();

        String retVal = null;
        if (max == null)
            retVal = (new StringBuilder()).append(divisionCode + developFormCode + condition).append("0001").toString();
        else {
            int uniqueNumber = Integer.valueOf(max.substring(4, max.length()));

            if (uniqueNumber == 9999)
                uniqueNumber = 1;
            else
                uniqueNumber++;

            retVal = (new StringBuilder()).append(divisionCode + developFormCode + condition).append(StringUtils.leftPad(String.valueOf(uniqueNumber), 4, "0")).toString();

        }
        // проверка уникальности номера
        while (_hasNumber(retVal)) {
            int _curVal = Integer.valueOf(retVal).intValue() + 1;
            retVal = String.format("%08d", new Object[]{_curVal});
        }

        return retVal;
    }

    /**
     * vch задача 1639 - ситуация, если попали в сущ. номер заявления
     *
     * @param number
     *
     * @return
     */
    private static boolean _hasNumber(String number)
    {

        boolean retVal = false;

        // проверка на номер абитуриента
        MQBuilder bbb = new MQBuilder(Entrant.class.getName(), "entr")
                .add(MQExpression.eq("entr", "personalNumber", number));
        List<Entrant> list = UniDaoFacade.getCoreDao().getList(bbb);
        if (list.size() > 0)
            retVal = true;


        // проверка на номер заявления
        int _reqNumber = Integer.valueOf(number).intValue();

        MQBuilder mqer = new MQBuilder(EntrantRequest.class.getName(), "er")
                .add(MQExpression.eq("er", "regNumber", _reqNumber));
        List<EntrantRequest> listER = UniDaoFacade.getCoreDao().getList(mqer);
        if (listER.size() > 0)
            retVal = true;

        return retVal;
    }

    public static RequestedEnrollmentDirection getRequestedEnrollmentDirectionByStudent(Student student) {
        DQLSelectBuilder eooBuilder = new DQLSelectBuilder()
                .fromEntity(EnrollmentExtract.class, "ee")
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentExtract.studentNew().id().fromAlias("ee")), DQLExpressions.value(student.getId())))
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentExtract.committed().fromAlias("ee")), DQLExpressions.value(true)))
                .order(DQLExpressions.property(EnrollmentExtract.createDate().fromAlias("ee")));
        List<EnrollmentExtract> extractList = IUniBaseDao.instance.get().getList(eooBuilder);
        if (CollectionUtils.isEmpty(extractList))
            return null;
        return extractList.get(0).getEntity().getRequestedEnrollmentDirection();

    }

    public static String[] getMarkForForm(Double[] finalMark, PairKey<ExamPassMark, ExamPassMarkAppeal> common, SubjectPassForm passForm, Set<MultiKey> realizationFormSet, boolean compensationTypeDiff, Discipline2RealizationWayRelation discipline, CompensationType budget, CompensationType contract, DoubleFormatter markFormatter) {
        String mark = "";
        String markAppeal = null;

        if (common != null) {
            ExamPassMark passMark = common.getFirst();

            if (passMark.getEntrantAbsenceNote() != null) {
                mark = passMark.getEntrantAbsenceNote().getShortTitle();
            }
            else {
                markAppeal = "";
                double finalMarkValue = passMark.getMark();

                mark = markFormatter.format(finalMarkValue);
                ExamPassMarkAppeal appealMark = common.getSecond();
                if (appealMark != null) {
                    finalMarkValue = appealMark.getMark();
                    markAppeal = markFormatter.format(finalMarkValue);
                }
                boolean forBudget = (!compensationTypeDiff) || (realizationFormSet.contains(new MultiKey(discipline, passForm, budget)));
                boolean forContract = (!compensationTypeDiff) || (realizationFormSet.contains(new MultiKey(discipline, passForm, contract)));
                if (forBudget)
                    finalMark[0] = Math.max(finalMark[0], finalMarkValue);
                if (forContract)
                    finalMark[1] = Math.max(finalMark[1], finalMarkValue);
            }
        }
        return new String[]{mark, markAppeal};
    }

    public static Date getLastEnrCampaignDevelopFormDate(EntrantRequest entrantRequest) {
        List<RequestedEnrollmentDirection> directions = IUniBaseDao.instance.get().getList(RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.entrantRequest().s(), entrantRequest);
        if (directions.isEmpty())
            return null;
        Set<DevelopForm> developFormSet = new HashSet<>();
        for (RequestedEnrollmentDirection direction : directions) {
            developFormSet.add(direction.getEnrollmentDirection().getEducationOrgUnit().getDevelopForm());
        }
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrCampaignDevelopFormDates.class, "e");
        dql.where(DQLExpressions.eq(DQLExpressions.property(EnrCampaignDevelopFormDates.enrollmentCampaign().fromAlias("e")), DQLExpressions.value(entrantRequest.getEntrant().getEnrollmentCampaign())));
        dql.where(DQLExpressions.in(DQLExpressions.property(EnrCampaignDevelopFormDates.developForm().fromAlias("e")), developFormSet));
        dql.where(DQLExpressions.isNotNull(DQLExpressions.property(EnrCampaignDevelopFormDates.docDate().fromAlias("e"))));
        dql.order(DQLExpressions.property(EnrCampaignDevelopFormDates.docDate().fromAlias("e")), OrderDirection.asc);
        List<EnrCampaignDevelopFormDates> list = IUniBaseDao.instance.get().getList(dql);
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }
        return list.get(0).getDocDate();
    }


}
