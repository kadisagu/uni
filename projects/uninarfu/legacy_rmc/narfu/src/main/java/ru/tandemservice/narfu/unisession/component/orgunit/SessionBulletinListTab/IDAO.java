package ru.tandemservice.narfu.unisession.component.orgunit.SessionBulletinListTab;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.PropertyNotFoundException;

import java.util.List;
import java.util.Map;

public interface IDAO
        extends
        ru.tandemservice.unisession.component.orgunit.SessionBulletinListTab.IDAO
{

    public boolean isNullProperty(IEntity document, String property) throws PropertyNotFoundException;

    public Map<String, List<IEntity>> getNullPropertyDocs(List<IEntity> docs, String... properties);

}
