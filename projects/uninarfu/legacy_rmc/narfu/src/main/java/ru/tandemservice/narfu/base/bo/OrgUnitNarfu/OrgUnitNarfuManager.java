package ru.tandemservice.narfu.base.bo.OrgUnitNarfu;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

@Configuration
public class OrgUnitNarfuManager extends BusinessObjectManager {
    public static OrgUnitNarfuManager instance()
    {
        return instance(OrgUnitNarfuManager.class);
    }
}
