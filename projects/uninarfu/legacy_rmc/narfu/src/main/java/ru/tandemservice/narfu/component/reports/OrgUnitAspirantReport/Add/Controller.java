package ru.tandemservice.narfu.component.reports.OrgUnitAspirantReport.Add;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.rtf.RtfDocumentRenderer;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.narfu.uniec.entity.report.OrgUnitAspirantReport;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = component.getModel();
        getDao().prepare(model);
    }

    public void onClickApply(IBusinessComponent component) {
        Model model = component.getModel();
        OrgUnitAspirantReport report = getDao().createReport(model);


//		NarfuReportStudentArchive report = getDao().get(reportId);

        DatabaseFile contentFile = report.getContent();
        if (contentFile == null || contentFile.getContent() == null)
            throw new ApplicationException("Файл отчета пуст");
        BusinessComponentUtils.downloadDocument(new RtfDocumentRenderer(contentFile.getFilename(), contentFile.getContent()), true);

        deactivate(component);
    }
}
