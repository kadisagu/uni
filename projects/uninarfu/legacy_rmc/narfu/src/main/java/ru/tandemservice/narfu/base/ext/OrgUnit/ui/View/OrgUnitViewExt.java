package ru.tandemservice.narfu.base.ext.OrgUnit.ui.View;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitView;
import ru.tandemservice.narfu.base.bo.OrgUnitNarfu.ui.MassPrintTab.OrgUnitNarfuMassPrintTab;


@Configuration
public class OrgUnitViewExt extends BusinessComponentExtensionManager {

    @Autowired
    public OrgUnitView orgUnitView;

    @Bean
    public TabPanelExtension tabPanelExtension() {
        return tabPanelExtensionBuilder(this.orgUnitView.orgUnitTabPanelExtPoint())
                .addTab(componentTab("orgUnitMassPrintTab", OrgUnitNarfuMassPrintTab.class)
                                //.before(org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitView.ORG_UNIT_TAB_PANEL)
                                .parameters("mvel:['orgUnitId':presenter.orgUnit.id]")
                                .permissionKey("ui:secModel.rmc_viewOrgUnitMassPrintTab")
                ).create();
    }
}
