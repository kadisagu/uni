package ru.tandemservice.narfu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.ResultSet;
import java.sql.Statement;

public class MS_narfu_1x0x0_25to26 extends IndependentMigrationScript {

    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.12"),
                        new ScriptDependency("org.tandemframework.shared", "1.2.5"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.2.5")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception {

        if (tool.tableExists("epp_eduplan_t") && tool.tableExists("epp_eduplan_ver_t")) {

            Statement stmt = tool.getConnection().createStatement();

            stmt.execute("select v.id, e.edustartyear_p, v.titlepostfix_p " +
                                 "from epp_eduplan_ver_t as v, epp_eduplan_t as e " +
                                 "where e.id = v.eduplan_id");

            ResultSet rs = stmt.getResultSet();

            while (rs.next()) {
                int edustartyear = rs.getInt(2);
                String titlepostfix = rs.getString(3);

                String newTitlePostfix = new StringBuilder()
                        .append(edustartyear)
                        .append(titlepostfix == null ? "" : " " + titlepostfix).toString();

                tool.executeUpdate("update epp_eduplan_ver_t set titlepostfix_p=? where id=?",
                                   newTitlePostfix, rs.getLong(1));
            }

        }
    }

}
