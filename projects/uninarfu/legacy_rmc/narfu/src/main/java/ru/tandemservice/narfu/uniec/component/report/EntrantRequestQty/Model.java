package ru.tandemservice.narfu.uniec.component.report.EntrantRequestQty;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.Date;
import java.util.List;

public class Model implements IEnrollmentCampaignSelectModel {
    private List<EnrollmentCampaign> enrollmentCampaignList;
    private EnrollmentCampaign enrollmentCampaign;     // Приемная кампания
    private Date rptDate;
    private OrgUnit formativeOrgUnit;
    private ISelectModel formativeOrgUnitModel;


    public Date getRptDate() {
        return rptDate;
    }

    public void setRptDate(Date rptDate) {
        this.rptDate = rptDate;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    private byte[] content;
    private String contentType;
    private String fileName;

    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList) {
        this.enrollmentCampaignList = enrollmentCampaignList;
    }

    public List<EnrollmentCampaign> getEnrollmentCampaignList() {
        return enrollmentCampaignList;
    }

    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign) {
        this.enrollmentCampaign = enrollmentCampaign;
    }

    public EnrollmentCampaign getEnrollmentCampaign() {
        return enrollmentCampaign;
    }

    @Override
    public IDataSettings getSettings() {
        return null;
    }

    public ISelectModel getFormativeOrgUnitModel() {
        return formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel) {
        this.formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit) {
        this.formativeOrgUnit = formativeOrgUnit;
    }

    public OrgUnit getFormativeOrgUnit() {
        return formativeOrgUnit;
    }

}
