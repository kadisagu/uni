package ru.tandemservice.narfu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_narfu_1x0x0_32to33 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.13"),
                        new ScriptDependency("org.tandemframework.shared", "1.3.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.3.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность ctrPaymentPromiceNARFU

        //  свойство template стало необязательным
        {
            // сделать колонку NULL
            tool.setColumnNullable("ctr_pr_payment_t_narfu", "template_id", true);

        }


    }
}