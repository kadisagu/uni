package ru.tandemservice.narfu.bi.ws;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.entrant.*;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Выдает наборы данных для заполнения DataWarehause БД
 *
 * @author Mike
 */

@WebService(serviceName = "BiWServices")
public class BiWServices {
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    private IUniBaseDao getDao() {
        return IUniBaseDao.instance.get();
    }

    @WebMethod
    public String getTestMessage() {
        return "testMessage";
    }

    @WebMethod
    public String getEnrollmentCampaigns() {
        try {
            Document dom = createXmlDocument();
            Element root = dom.createElement("root");
            dom.appendChild(root);

            List<EnrollmentCampaign> campaigns = getDao().getList(EnrollmentCampaign.class);

            for (EnrollmentCampaign c : campaigns) {
                Element item = dom.createElement("enrollmentCampaign");

                Attr idAttr = dom.createAttribute("id");
                idAttr.setNodeValue(c.getId().toString());
                item.setAttributeNode(idAttr);

                Attr titleAttr = dom.createAttribute("title");
                titleAttr.setNodeValue(c.getTitle());
                item.setAttributeNode(titleAttr);

                root.appendChild(item);
            }

            return xmlToString(dom);
        }
        catch (ParserConfigurationException e) {
            return "<error>" + e.getMessage() + "</error>";
        }
    }

    @WebMethod
    public String getEntrantRequests() {
        try {
            Document dom = createXmlDocument();
            Element root = dom.createElement("root");
            dom.appendChild(root);

            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EntrantRequest.class, "r")
                    .column("r");

            List<EntrantRequest> requests = getDao().<EntrantRequest>getList(builder);

            for (EntrantRequest request : requests) {

                DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "rd")
                        .column("rd")
                        .where(eq(property(RequestedEnrollmentDirection.entrantRequest().fromAlias("rd")), value(request.getId())))
                        .order("rd." + RequestedEnrollmentDirection.P_PRIORITY)
                        .top(1);

                List<RequestedEnrollmentDirection> dirs = getDao().<RequestedEnrollmentDirection>getList(subBuilder);
                if (dirs.isEmpty()) continue;
                RequestedEnrollmentDirection dir = dirs.get(0);

                Element requestElem = dom.createElement("request");
                Attr idAttr = dom.createAttribute("id");
                idAttr.setNodeValue(request.getId().toString());
                requestElem.setAttributeNode(idAttr);

                Attr regdateAttr = dom.createAttribute("regdate");
                regdateAttr.setNodeValue(sdf.format(request.getRegDate())); //String.format("%1$tFT%1$tT", request.getRegDate())
                requestElem.setAttributeNode(regdateAttr);

                Attr regnumberAttr = dom.createAttribute("regnumber");
                regnumberAttr.setNodeValue(request.getStringNumber());
                requestElem.setAttributeNode(regnumberAttr);

                Attr entrantAttr = dom.createAttribute("entrant_id");
                entrantAttr.setNodeValue(Long.toString(request.getEntrantId()));
                requestElem.setAttributeNode(entrantAttr);

                Attr origdocAttr = dom.createAttribute("originaldocumenthandedin");
                origdocAttr.setNodeValue(Boolean.toString(dir.isOriginalDocumentHandedIn()));
                requestElem.setAttributeNode(origdocAttr);

                Attr budgetAttr = dom.createAttribute("budget");
                budgetAttr.setNodeValue(Boolean.toString(dir.getCompensationType().getCode().equals(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET)));
                requestElem.setAttributeNode(budgetAttr);

                Attr withbenefitAttr = dom.createAttribute("withbenefit");
                withbenefitAttr.setNodeValue(Boolean.toString(dir.getCompetitionKind().getCode().equals(UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION)));
                requestElem.setAttributeNode(withbenefitAttr);

                Attr enrdirAttr = dom.createAttribute("enrollmentdirection_id");
                enrdirAttr.setNodeValue(dir.getEnrollmentDirection().getId().toString());
                requestElem.setAttributeNode(enrdirAttr);

                root.appendChild(requestElem);

            }
            return xmlToString(dom);
        }
        catch (ParserConfigurationException e) {
            return "<error>" + e.getMessage() + "</error>";
        }
    }

    @WebMethod
    public String getEnrollmentDirections() {
        try {
            Document dom = createXmlDocument();
            Element root = dom.createElement("root");
            dom.appendChild(root);

            List<EnrollmentDirection> edirs = getDao().getList(EnrollmentDirection.class);

            for (EnrollmentDirection d : edirs) {
                Element item = dom.createElement("enrollmentDirection");

                Attr idAttr = dom.createAttribute("id");
                idAttr.setNodeValue(d.getId().toString());
                item.setAttributeNode(idAttr);

                Attr titleAttr = dom.createAttribute("title");
                titleAttr.setNodeValue(d.getTitle());
                item.setAttributeNode(titleAttr);

                if (d.getMinisterialPlan() != null) {
                    Attr mplanAttr = dom.createAttribute("ministerialplan");
                    mplanAttr.setNodeValue(d.getMinisterialPlan().toString());
                    item.setAttributeNode(mplanAttr);
                }

                if (d.getContractPlan() != null) {
                    Attr cplanAttr = dom.createAttribute("contractplan");
                    cplanAttr.setNodeValue(d.getContractPlan().toString());
                    item.setAttributeNode(cplanAttr);
                }

                Attr ecompAttr = dom.createAttribute("enrollmentcampaign_id");
                ecompAttr.setNodeValue(d.getEnrollmentCampaign().getId().toString());
                item.setAttributeNode(ecompAttr);

                Attr orgunitAttr = dom.createAttribute("orgunit_id");
                orgunitAttr.setNodeValue(d.getEducationOrgUnit().getFormativeOrgUnit().getId().toString());
                item.setAttributeNode(orgunitAttr);

                Attr elevelAttr = dom.createAttribute("eduleveltype");
                elevelAttr.setNodeValue(d.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().getRoot().getShortTitle());
                item.setAttributeNode(elevelAttr);

                root.appendChild(item);
            }

            return xmlToString(dom);
        }
        catch (ParserConfigurationException e) {
            return "<error>" + e.getMessage() + "</error>";
        }
    }

    @WebMethod
    public String getEntrants() {
        try {
            Document dom = createXmlDocument();
            Element root = dom.createElement("root");
            dom.appendChild(root);

            List<Entrant> entrants = getDao().getList(Entrant.class);

            for (Entrant e : entrants) {
                Element item = dom.createElement("entrant");

                Attr idAttr = dom.createAttribute("id");
                idAttr.setNodeValue(e.getId().toString());
                item.setAttributeNode(idAttr);

                Attr fioAttr = dom.createAttribute("fio");
                fioAttr.setNodeValue(e.getPerson().getFio());
                item.setAttributeNode(fioAttr);

                Attr genderAttr = dom.createAttribute("gender");
                genderAttr.setNodeValue(e.getPerson().getIdentityCard().getSex().getCode().equals(SexCodes.MALE) ? "1" : "0");
                item.setAttributeNode(genderAttr);

                Date today = new Date();
                Date bDate = e.getPerson().getIdentityCard().getBirthDate();
                final double YEAR = 1000.0 * 3600.0 * 24.0 * 365.0; //ms in year
                if (bDate != null) {
                    Attr birthAttr = dom.createAttribute("birthDate");
                    birthAttr.setNodeValue(sdf.format(bDate));
                    item.setAttributeNode(birthAttr);

                    Double age = Math.floor((today.getTime() - bDate.getTime()) / YEAR);
                    Attr ageAttr = dom.createAttribute("age");
                    ageAttr.setNodeValue(age.toString());
                    item.setAttributeNode(ageAttr);
                }

                AddressDetailed addr = e.getPerson().getAddressRegistration() instanceof AddressDetailed ? (AddressDetailed) e.getPerson().getAddressRegistration() : null;

                if (addr != null && addr.getSettlement() != null) {
                    Attr villagerAttr = dom.createAttribute("villager");
                    villagerAttr.setNodeValue(addr.getSettlement().getAddressType().getTitle().equalsIgnoreCase("город") ? "0" : "1");
                    item.setAttributeNode(villagerAttr);
                }

                DQLSelectBuilder sub = new DQLSelectBuilder()
                        .fromEntity(PersonBenefit.class, "pb")
                        .where(eq(property(PersonBenefit.person().fromAlias("pb")), value(e.getPerson())))
                        .top(1);

                List<PersonBenefit> benefits = getDao().<PersonBenefit>getList(sub);
                if (!benefits.isEmpty()) {
                    Attr benefitAttr = dom.createAttribute("benefit");
                    benefitAttr.setNodeValue(benefits.get(0).getTitle());
                    item.setAttributeNode(benefitAttr);
                }

                root.appendChild(item);
            }

            return xmlToString(dom);
        }
        catch (ParserConfigurationException e) {
            return "<error>" + e.getMessage() + "</error>";
        }
    }

    @WebMethod
    public String getOrgUnits() {
        try {
            DQLSelectBuilder builder = new DQLSelectBuilder();
            builder.fromEntity(OrgUnitToKindRelation.class, "ok")
                    .column(property(OrgUnitToKindRelation.orgUnit().fromAlias("ok")))
                    .where(eq(property(OrgUnitToKindRelation.orgUnitKind().code().fromAlias("ok")),
                              value(UniDefines.CATALOG_ORGUNIT_KIND_FORMING)));
            List<OrgUnit> orgUnits = getDao().<OrgUnit>getList(builder);

            Document dom = createXmlDocument();
            Element root = dom.createElement("root");
            dom.appendChild(root);
            for (OrgUnit ou : orgUnits) {
                Element item = dom.createElement("orgUnit");
                Attr idAttr = dom.createAttribute("id");
                idAttr.setNodeValue(ou.getId().toString());
                item.setAttributeNode(idAttr);

                Attr titleAttr = dom.createAttribute("title");
                titleAttr.setNodeValue(ou.getTitle());
                item.setAttributeNode(titleAttr);

                root.appendChild(item);
            }
            return xmlToString(dom);
        }
        catch (ParserConfigurationException e) {
            return "<error>" + e.getMessage() + "</error>";
        }
    }

    private Document createXmlDocument() throws ParserConfigurationException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
        docBuilder = docFactory.newDocumentBuilder();
        return docBuilder.newDocument();
    }

    /**
     * XML документ - в строку
     *
     * @param dom
     *
     * @return
     */
    private String xmlToString(Document dom) {
        String output;
        try {
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer;

            transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            StringWriter writer = new StringWriter();

            transformer.transform(new DOMSource(dom), new StreamResult(writer));
            output = writer.getBuffer().toString();
        }
        catch (Exception ex) {
            output = "<error>" + ex.getMessage() + "</error>";
        }
        return output;
    }

}
