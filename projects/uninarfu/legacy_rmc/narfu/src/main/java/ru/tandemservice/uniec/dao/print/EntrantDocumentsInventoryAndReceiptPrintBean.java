package ru.tandemservice.uniec.dao.print;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.aspirantrmc.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.narfu.uniec.dao.print.IEntrantExamListPrintBean;
import ru.tandemservice.uniecrmc.dao.UniecrmcDao;
import ru.tandemservice.uniecrmc.entity.entrant.Entrant2IndividualAchievements;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;

import java.util.*;

public class EntrantDocumentsInventoryAndReceiptPrintBean extends UniBaseDao implements IEntrantDocumentsInventoryAndReceiptPrintBean {
    private IEntrantExamListPrintBean examListBean = (IEntrantExamListPrintBean) ApplicationRuntime.getBean("entrantExamListPrintBean");

    private static Set<String> ASPIRANT_CODES = Collections.unmodifiableSet(new LinkedHashSet<String>(Arrays.asList(StudentCategoryCodes.STUDENT_CATEGORY_ASPIRANT, StudentCategoryCodes.STUDENT_CATEGORY_APPLICANT, "5", "6")));

    @Override
    public String getSecretary(OrgUnit formativOu) {
        //IEntrantExamListPrintBean examListBean = (IEntrantExamListPrintBean) ApplicationRuntime.getBean("entrantExamListPrintBean");
        return examListBean.getSecretary(formativOu);
    }

    @Override
    public RequestedEnrollmentDirection getFirstRequestDirection(EntrantRequest request) {
        return UniecrmcDao.Instanse().getFirstRequestDirection(request);
    }

    @Override
    public String getTechSecretary() {
        return UserContext.getInstance().getAuditor().getPrincipalContext().getTitle();
    }

    @Override
    public List<ExamPassDiscipline> getDisciplineList(Entrant entrant) {
        //IEntrantExamListPrintBean examListBean = (IEntrantExamListPrintBean) ApplicationRuntime.getBean("entrantExamListPrintBean");
        return examListBean.getDisciplineList(entrant);
    }

    @Override
    public String getDocumentInfo(EntrantEnrollmentDocument doc) {
        StringBuffer res = new StringBuffer();
        String docTitle = doc.getEnrollmentDocument().getTitle();
        EntrantRequest request = doc.getEntrantRequest();
        Entrant entrant = request.getEntrant();
        PersonEduInstitution edu = entrant.getPerson().getPersonEduInstitution();

        String docNum = "";
        Date docDate = null;

        if ((docTitle.toLowerCase().indexOf("заявление") > -1) ||
                (docTitle.toLowerCase().indexOf("трудов") > -1) ||
                (docTitle.toLowerCase().indexOf("направлен") > -1 && docTitle.toLowerCase().indexOf("обучен") > -1))
        {
            docNum = request.getStringNumber();
            docDate = request.getRegDate();
        }
        else if ((docTitle.toLowerCase().indexOf("паспорт") > -1) ||
                (docTitle.toLowerCase().indexOf("удостоверение") > -1))
        {
            IdentityCard iCard = entrant.getPerson().getIdentityCard();
            if (iCard.getSeria() != null)
                docNum = iCard.getSeria();
            if (iCard.getNumber() != null)
                docNum += " " + iCard.getNumber();
            docDate = entrant.getPerson().getIdentityCard().getIssuanceDate();
        }
        else if (docTitle.toLowerCase().indexOf("егэ") > -1) {
            DQLSelectBuilder builder = new DQLSelectBuilder()
                    .fromEntity(EntrantStateExamCertificate.class, "es")
                    .addColumn("es")
                    .where(DQLExpressions.eq(DQLExpressions.property(EntrantStateExamCertificate.entrant().fromAlias("es")),
                                             DQLExpressions.value(entrant)));
            List<EntrantStateExamCertificate> esList = builder.createStatement(getSession()).<EntrantStateExamCertificate>list();
            for (EntrantStateExamCertificate es : esList) {
                if (res.length() > 0)
                    res.append(", ");
                if (es.getNumber() != null)
                    res.append(es.getNumber());
                if (es.getIssuanceDate() != null)
                    res.append(" от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(es.getIssuanceDate()));
            }
        }
        else if (docTitle.toLowerCase().indexOf("олимпиад") > -1) {
            DQLSelectBuilder builder = new DQLSelectBuilder()
                    .fromEntity(OlympiadDiploma.class, "od")
                    .addColumn("od")
                    .where(DQLExpressions.eq(DQLExpressions.property(OlympiadDiploma.entrant().fromAlias("od")),
                                             DQLExpressions.value(entrant)));
            List<OlympiadDiploma> odList = builder.createStatement(getSession()).<OlympiadDiploma>list();
            for (OlympiadDiploma od : odList) {
                if (res.length() > 0)
                    res.append(", ");
                if (od.getNumber() != null)
                    res.append(od.getNumber());
                if (od.getIssuanceDate() != null)
                    res.append(" от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(od.getIssuanceDate()));
            }
        }
        else if ((docTitle.toLowerCase().indexOf("аттестат") > -1) ||
                (docTitle.toLowerCase().indexOf("диплом") > -1) ||
                (docTitle.toLowerCase().indexOf("академ") > -1 && docTitle.toLowerCase().indexOf("справка") > -1))
        {
            if (edu != null) {
                if (edu.getSeria() != null)
                    docNum = edu.getSeria();
                if (edu.getNumber() != null)
                    docNum += " " + edu.getNumber();
                docDate = edu.getIssuanceDate();
            }
        }

        res.append(docNum);
        if (docDate != null)
            res.append(" от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(docDate));
        return res.toString();
    }


    public Date getExamPassDisciplineDate(ExamPassDiscipline exapPassDiscipline) {
        return examListBean.getExamPassDisciplineDate(exapPassDiscipline);
    }

    @Override
    public Date getExamPassDate(ExamPassDiscipline examPassDiscipline) {
        return examListBean.getExamPassDate(examPassDiscipline);
    }

    public List<String> getAchievements(Entrant entrant) {
        List<String> retVal = new ArrayList<>();
        List<Entrant2IndividualAchievements> entrant2IndividualAchievements = getList(Entrant2IndividualAchievements.class, Entrant2IndividualAchievements.entrant().s(), entrant);
        for (Entrant2IndividualAchievements achievements : entrant2IndividualAchievements) {
            retVal.add(achievements.getText());
        }
        //  tm.put
        return retVal;//examListBean.getExamPassDisciplineDate(exapPassDiscipline);
    }

    public void extendTable(RtfTableModifier tm, Entrant entrant) {
        final List<String> achievements = getAchievements(entrant);
        tm.put("T2", new RtfRowIntercepterBase() {
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex) {
                if (CollectionUtils.isEmpty(achievements))
                    return;
                RtfRow row = newRowList.get(newRowList.size() - 1);
                List<RtfCell> cellList = row.getCellList();
                RtfCell cell = cellList.get(1);
                for (String title : achievements) {
                    RtfString rtfString = new RtfString().append(IRtfData.PAR).append(IRtfData.TAB).append("- " + title + ";");
                    cell.getElementList().addAll(rtfString.toList());
                }

            }

        });

    }

    @Override
    public byte[] getAspirantTemplate(EntrantRequest request, byte[] template)
    {
        List<RequestedEnrollmentDirection> listOfEnrollmentDirectios = getList(
                RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.entrantRequest(),
                request, new String[]{RequestedEnrollmentDirection.P_PRIORITY});

        for (RequestedEnrollmentDirection enrollmentDirection : listOfEnrollmentDirectios) {
            if (ASPIRANT_CODES.contains(enrollmentDirection.getStudentCategory().getCode())) {
                IScriptItem templateDocument
                        = (IScriptItem) UniDaoFacade.getCoreDao().getCatalogItem
                        (
                                UniecScriptItem.class, "documentListAndReceiptAspirant"
                        );
                RtfDocument document = (new RtfReader()).read(templateDocument.getCurrentTemplate().clone());
                return RtfUtil.toByteArray(document);
            }
        }
        return null;

    }


}
