package ru.tandemservice.narfu.component.modularextract.e4;

import ru.tandemservice.movestudent.entity.EduEnrolmentStuExtract;
import ru.tandemservice.uni.entity.employee.OrderData;

public class EduEnrolmentStuExtractDao extends ru.tandemservice.movestudent.component.modularextract.e4.EduEnrolmentStuExtractDao {
    @Override
    protected OrderData commit(EduEnrolmentStuExtract extract) {
        OrderData result = super.commit(extract);
        result.setEduEnrollmentOrderEnrDate(extract.getEntryDate());
        getSession().saveOrUpdate(result);

        return result;
    }
}
