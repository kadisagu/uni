package ru.tandemservice.narfu.uniec.component.settings.ExamMarkImport.List;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.narfu.entity.EntrantExamBulletinImportedFile;
import ru.tandemservice.narfu.entity.catalog.ExamBulletinImportedFileResult;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.util.FilterUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DAO extends UniDao<Model> {

    @Override
    public void prepare(Model model) {
        model.setResultModel(new LazySimpleSelectModel<ExamBulletinImportedFileResult>(ExamBulletinImportedFileResult.class));
    }

    @Override
    public void prepareListDataSource(Model model) {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EntrantExamBulletinImportedFile.class, "e");

        Date date = model.getSettings().get("importedDate");
        if (date != null) {
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            c.set(Calendar.HOUR_OF_DAY, 23);
            c.set(Calendar.MINUTE, 59);
            c.set(Calendar.SECOND, 59);
            FilterUtils.applyBetweenFilter(dql, "e", EntrantExamBulletinImportedFile.importDate().s(), date, c.getTime());
        }

        ExamBulletinImportedFileResult result = model.getSettings().get("result");
        FilterUtils.applySelectFilter(dql, EntrantExamBulletinImportedFile.importResult().fromAlias("e"), result);

        if (model.getDataSource().getEntityOrder() != null)
            dql.order("e." + model.getDataSource().getEntityOrder().getKey(), model.getDataSource().getEntityOrder().getDirection());

        List<EntrantExamBulletinImportedFile> list = getList(dql);
        UniUtils.createPage(model.getDataSource(), list);
    }
}
