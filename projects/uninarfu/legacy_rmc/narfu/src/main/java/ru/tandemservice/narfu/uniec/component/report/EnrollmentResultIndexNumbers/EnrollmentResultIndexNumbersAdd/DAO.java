package ru.tandemservice.narfu.uniec.component.report.EnrollmentResultIndexNumbers.EnrollmentResultIndexNumbersAdd;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.report.EnrollmentResultIndexNumbersReport;

import java.util.Date;

public class DAO extends ru.tandemservice.uniec.component.report.EnrollmentResultIndexNumbers.EnrollmentResultIndexNumbersAdd.DAO {
    @Override
    public void update(ru.tandemservice.uniec.component.report.EnrollmentResultIndexNumbers.EnrollmentResultIndexNumbersAdd.Model model) {
        Session session = getSession();

        EnrollmentResultIndexNumbersReport report = model.getReport();
        report.setFormingDate(new Date());

        DatabaseFile databaseFile = new EnrollmentResultIndexNumbersReportBuilderExt(model, session).getContent();
        session.save(databaseFile);
        report.setContent(databaseFile);

        report.setCompensationType(model.getCompensationType().getShortTitle());
        if (model.isStudentCategoryActive())
            report.setStudentCategory(UniStringUtils.join(model.getStudentCategoryList(), "title", "; "));
        if (model.isQualificationActive())
            report.setQualification(UniStringUtils.join(model.getQualificationList(), "title", "; "));
        if (model.isFormativeOrgUnitActive())
            report.setFormativeOrgUnit(UniStringUtils.join(model.getFormativeOrgUnitList(), "fullTitle", "; "));
        if (model.isTerritorialOrgUnitActive())
            report.setTerritorialOrgUnit(StringUtils.join(UniBaseUtils.getPropertiesList(model.getTerritorialOrgUnitList(), "territorialFullTitle"), "; "));
        if (model.isEducationLevelHighSchoolActive())
            report.setEducationLevelHighSchool(UniStringUtils.join(model.getEducationLevelHighSchoolList(), "displayableTitle", "; "));
        report.setDevelopForm(model.getDevelopForm().getTitle());
        if (model.isDevelopConditionActive())
            report.setDevelopCondition(UniStringUtils.join(model.getDevelopConditionList(), "title", "; "));
        if (model.isDevelopTechActive())
            report.setDevelopTech(UniStringUtils.join(model.getDevelopTechList(), "title", "; "));
        if (model.isDevelopPeriodActive())
            report.setDevelopPeriod(UniStringUtils.join(model.getDevelopPeriodList(), "title", "; "));
        if (model.isParallelActive()) {
            report.setExcludeParallel(model.getParallel().isTrue());
        }
        session.save(report);


    }
}
