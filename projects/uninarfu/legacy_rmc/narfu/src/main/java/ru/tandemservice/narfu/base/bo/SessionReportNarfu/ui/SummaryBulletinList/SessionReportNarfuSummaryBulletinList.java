package ru.tandemservice.narfu.base.bo.SessionReportNarfu.ui.SummaryBulletinList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.narfu.base.bo.SessionReportNarfu.logic.SessionSummaryNarfuBulletinDSHandler;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionTypeRow;
import ru.tandemservice.uniepp.entity.student.group.gen.EppRealEduGroup4ActionTypeRowGen;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.SummaryBulletinList.SessionReportSummaryBulletinList;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionObject;
import ru.tandemservice.unisession.entity.report.UnisessionSummaryBulletinReport;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

@Configuration
public class SessionReportNarfuSummaryBulletinList extends BusinessComponentManager
{
    public static final String GROUP_DS = "groupDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(SessionReportManager.instance().eduYearDSConfig())
                .addDataSource(SessionReportManager.instance().yearPartDSConfig())
                .addDataSource(selectDS(GROUP_DS, groupsDSHandler()).addColumn(Group.title().s()))
                .addDataSource(searchListDS(SessionReportSummaryBulletinList.DS_REPORTS, sessionSummaryBulletinDS(), sessionSummaryBulletinDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint sessionSummaryBulletinDS()
    {
        return columnListExtPointBuilder(SessionReportSummaryBulletinList.DS_REPORTS)
                .addColumn(indicatorColumn("icon").defaultIndicatorItem(new IndicatorColumn.Item("report", "Отчет")).create())
                .addColumn(publisherColumn("date", UnisessionSummaryBulletinReport.formingDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).order().create())
                .addColumn(textColumn("year", UnisessionSummaryBulletinReport.sessionObject().educationYear().title().s()).create())
                .addColumn(textColumn("part", UnisessionSummaryBulletinReport.sessionObject().yearDistributionPart().title().s()).create())
                .addColumn(textColumn("groups", UnisessionSummaryBulletinReport.groups().s()).create())
                .addColumn(textColumn("exec", UnisessionSummaryBulletinReport.executor().s()).create())
                .addColumn(textColumn("discipl", UnisessionSummaryBulletinReport.discKinds().s()).create())
                .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onClickPrint"))
                .addColumn(actionColumn("delete", CommonDefines.ICON_DELETE, "onDeleteEntityFromList").alert(FormattedMessage.with().template("sessionReportSummaryBulletinDS.delete.alert").parameter(UnisessionSummaryBulletinReport.formingDateStr().s()).create()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> sessionSummaryBulletinDSHandler()
    {
        return new SessionSummaryNarfuBulletinDSHandler(this.getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> groupsDSHandler()
    {
        EntityComboDataSourceHandler.IQueryCustomizer customizer = (alias, dql, context, filter) -> {
            SessionObject sessionObject = context.get(SessionSummaryNarfuBulletinDSHandler.PARAM_SESSION_OBJECT);
            if (sessionObject != null)
            {
                String docAls = "sub";
                DQLSelectBuilder sub = new DQLSelectBuilder().fromEntity(SessionBulletinDocument.class, docAls)
                        .where(eq(property(docAls, SessionBulletinDocument.sessionObject()), value(sessionObject)));

                String slotAls = "slot";
                sub.joinEntity(docAls, DQLJoinType.left, SessionDocumentSlot.class, slotAls,
                               eq(property(slotAls, SessionDocumentSlot.document()), property(docAls)));

                String relAls = "rel";
                sub.joinEntity(slotAls, DQLJoinType.inner, EppRealEduGroup4ActionTypeRow.class, relAls,
                               and(
                                       isNull(property(relAls, EppRealEduGroup4ActionTypeRow.removalDate())),
                                       eq(property(relAls, EppRealEduGroup4ActionTypeRow.studentWpePart()),
                                          property(slotAls, SessionDocumentSlot.studentWpeCAction().id()))
                               ));

                sub.order(property(relAls, EppRealEduGroup4ActionTypeRowGen.studentGroupTitle()))
                        .where(isNull(property(relAls, EppRealEduGroup4ActionTypeRowGen.removalDate())))
                        .where(eq(property(relAls, EppRealEduGroup4ActionTypeRowGen.studentGroupTitle()), property(alias, Group.title())));

                dql.where(exists(sub.buildQuery()));
            }
            return dql;
        };

        return Group.defaultSelectDSHandler(getName()).customize(customizer);
    }
}
