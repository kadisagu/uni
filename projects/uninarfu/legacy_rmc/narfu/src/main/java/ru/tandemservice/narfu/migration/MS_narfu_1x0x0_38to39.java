package ru.tandemservice.narfu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_narfu_1x0x0_38to39 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {

        // перенесено в другой модуль

//		////////////////////////////////////////////////////////////////////////////////
//		// сущность militarySpecialRegistrationSettings2MilitarySpecialRegistration
//
//		// сущность была удалена
//		{
//			// удалить таблицу
//			tool.dropTable("mltryspclrgstrtnsttngs2mltry_t", false /* - не удалять, если есть ссылающиеся таблицы */);
//			// удалить код сущности
//			tool.entityCodes().delete("militarySpecialRegistrationSettings2MilitarySpecialRegistration");
//		}
//
//		////////////////////////////////////////////////////////////////////////////////
//		// сущность militaryRegDataSettings2MilitaryRegData
//
//		// сущность была удалена
//		{
//			// удалить таблицу
//			tool.dropTable("mltryrgdtsttngs2mltryrgdt_t", false /* - не удалять, если есть ссылающиеся таблицы */);
//			// удалить код сущности
//			tool.entityCodes().delete("militaryRegDataSettings2MilitaryRegData");
//		}
//
//		////////////////////////////////////////////////////////////////////////////////
//		// сущность militaryDataSettings2MilitaryData
//
//		// сущность была удалена
//		{
//			// удалить таблицу
//			tool.dropTable("mltrydtsttngs2mltrydt_t", false /* - не удалять, если есть ссылающиеся таблицы */);
//			// удалить код сущности
//			tool.entityCodes().delete("militaryDataSettings2MilitaryData");
//		}
//
//		////////////////////////////////////////////////////////////////////////////////
//		// сущность militaryCompositionSettings2MilitaryComposition
//
//		// сущность была удалена
//		{
//			// удалить таблицу
//			tool.dropTable("mltrycmpstnsttngs2mltrycmpst_t", false /* - не удалять, если есть ссылающиеся таблицы */);
//			// удалить код сущности
//			tool.entityCodes().delete("militaryCompositionSettings2MilitaryComposition");
//		}
//
//		////////////////////////////////////////////////////////////////////////////////
//		// сущность militaryAbilityStatusSettings2MilitaryAbilityStatus
//
//		// сущность была удалена
//		{
//			// удалить таблицу
//			tool.dropTable("mltryabltysttssttngs2mltryab_t", false /* - не удалять, если есть ссылающиеся таблицы */);
//			// удалить код сущности
//			tool.entityCodes().delete("militaryAbilityStatusSettings2MilitaryAbilityStatus");
//		}

    }
}