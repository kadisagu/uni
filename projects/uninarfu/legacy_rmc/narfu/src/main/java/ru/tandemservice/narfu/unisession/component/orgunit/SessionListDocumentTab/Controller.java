package ru.tandemservice.narfu.unisession.component.orgunit.SessionListDocumentTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.util.RtfUtil;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.unisession.dao.print.ISessionListDocumentPrintDAO;

public class Controller extends ru.tandemservice.unisession.component.orgunit.SessionListDocumentTab.Controller {

    @Override
    public void onClickPrint(IBusinessComponent component) {
        final RtfDocument document = ISessionListDocumentPrintDAO.instance.get().printSessionListDocument(component.<Long>getListenerParameter());
        byte[] content = RtfUtil.toByteArray(document);
        final Integer temporaryId = PrintReportTemporaryStorage.registerTemporaryPrintForm(content, "ExamCard.rtf");
        this.activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap().add("id", temporaryId).add("zip", Boolean.FALSE)));
    }
}
