package ru.tandemservice.narfu.base.ext.EcEntrant;

import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.narfu.utils.NarfuEntrantUtil;
import ru.tandemservice.uniecrmc.entity.entrant.RequestedEnrollmentDirectionExt;
import ru.tandemservice.uniec.entity.catalog.ProfileKnowledge;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.ProfileExaminationMark;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.List;
import java.util.Map;

public class EcEntrantDaoExt extends ru.tandemservice.uniecrmc.base.ext.EcEntrant.logic.EcEntrantDaoExt {


    @Override
    public void saveOrUpdateEntrantRequest(
            List<RequestedEnrollmentDirection> selectedList,
            List<RequestedEnrollmentDirection> forDeleteList, boolean addForm,
            EntrantRequest entrantRequest, Integer entrantRequestNumber,
            Integer entrantDirectionNumber,
            Map<Long, List<ProfileKnowledge>> profileKnowledgeMap,
            Map<Long, List<ProfileExaminationMark>> profileMarkMap)
    {

        if (selectedList.isEmpty()) {
            throw new ApplicationException("Не выбраны напрваления подготовки (специальности)");
        }

        if (addForm) {
            String entrantUniqueNumber = NarfuEntrantUtil.getEntrantUniqueNumber(selectedList);
            entrantRequest.getEntrant().setPersonalNumber(entrantUniqueNumber);
            entrantRequestNumber = Integer.parseInt(entrantUniqueNumber);
        }

		/*
		super.saveOrUpdateEntrantRequest(selectedList, forDeleteList, addForm,
				entrantRequest, entrantRequestNumber, entrantDirectionNumber,
				profileKnowledgeMap, profileMarkMap);
		
		EntrantRequest entrantRequest2 = get(EntrantRequest.class, EntrantRequest.L_ENTRANT,entrantRequest.getEntrant());
		entrantRequest2.setRegNumber(Integer.valueOf(entrantUniqueNumber));
		update(entrantRequest2);
		*/

        super.saveOrUpdateEntrantRequest(selectedList, forDeleteList, addForm,
                                         entrantRequest, entrantRequestNumber, entrantDirectionNumber,
                                         profileKnowledgeMap, profileMarkMap);


    }

    @Override
    public void saveOrUpdateEntrantRequest(
            List<RequestedEnrollmentDirection> selectedList,
            List<RequestedEnrollmentDirection> forDeleteList, boolean addForm,
            EntrantRequest entrantRequest, Integer entrantRequestNumber,
            Integer entrantDirectionNumber,
            Map<Long, List<ProfileKnowledge>> profileKnowledgeMap,
            Map<Long, List<ProfileExaminationMark>> profileMarkMap,
            Map<Long, RequestedEnrollmentDirectionExt> directionExtMap)
    {

        if (selectedList.isEmpty()) {
            throw new ApplicationException("Не выбраны напрваления подготовки (специальности)");
        }

        if (addForm) {
            String entrantUniqueNumber = NarfuEntrantUtil.getEntrantUniqueNumber(selectedList);
            entrantRequest.getEntrant().setPersonalNumber(entrantUniqueNumber);
            entrantRequestNumber = Integer.parseInt(entrantUniqueNumber);
        }

        super.saveOrUpdateEntrantRequest(selectedList, forDeleteList, addForm,
                                         entrantRequest, entrantRequestNumber, entrantDirectionNumber,
                                         profileKnowledgeMap, profileMarkMap, directionExtMap);
    }


}
