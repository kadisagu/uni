package ru.tandemservice.narfu.component.studentmassprint.AddGrantDocument;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.narfu.entity.NarfuGrantDocument;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.ArrayList;
import java.util.List;

@Input({@Bind(key = "studentId", binding = "student.id", required = true)})
public class Model {

    private Student student = new Student();
    private String fio;
    private List<NarfuGrantDocument> narfuGrantDocuments = new ArrayList<NarfuGrantDocument>();
    private DynamicListDataSource dataSource;

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public List<NarfuGrantDocument> getNarfuGrantDocuments() {
        return narfuGrantDocuments;
    }

    public void setNarfuGrantDocuments(
            List<NarfuGrantDocument> narfuGrantDocuments)
    {
        this.narfuGrantDocuments = narfuGrantDocuments;
    }

    public DynamicListDataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource dataSource) {
        this.dataSource = dataSource;
    }
}
