package ru.tandemservice.narfu.base.ext.UniStudent.ui.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.narfu.entity.IdentityCardHidden;
import ru.tandemservice.uni.base.bo.UniStudent.logic.archivalList.ArchivalStudentSearchListDSHandler;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;

import java.util.Date;

public class ArchivalStudentSearchListExtDSHandler extends ArchivalStudentSearchListDSHandler
{
    public ArchivalStudentSearchListExtDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected void addAdditionalRestrictions(DQLSelectBuilder builder, String alias, DSInput input, ExecutionContext context)
    {
        super.addAdditionalRestrictions(builder, alias, input, context);

        String icHiddenAlias = "icH";
        builder.joinEntity(alias, DQLJoinType.left, IdentityCardHidden.class, icHiddenAlias, DQLExpressions.eq(DQLExpressions.property(icHiddenAlias, IdentityCardHidden.identityCard()), DQLExpressions.property(alias, Student.person().identityCard())));

        String cardSeria = context.get("cardSeria");
        String cardNumber = context.get("cardNumber");
        Date cardBirthDateFrom = context.get("cardBirthDateFrom");
        Date cardBirthDateFromTo = context.get("cardBirthDateFromTo");

        FilterUtils.applySimpleLikeFilter(builder, icHiddenAlias, IdentityCardHidden.seria(), cardSeria);
        FilterUtils.applySimpleLikeFilter(builder, icHiddenAlias, IdentityCardHidden.number(), cardNumber);

        if (cardBirthDateFrom != null)
        {
            builder.where(DQLExpressions.ge(DQLExpressions.property("icH", IdentityCardHidden.birthDate()), DQLExpressions.value(cardBirthDateFrom, PropertyType.DATE)));
        }

        if (cardBirthDateFromTo != null)
        {
            builder.where(DQLExpressions.le(DQLExpressions.property("icH", IdentityCardHidden.birthDate()), DQLExpressions.value(cardBirthDateFromTo, PropertyType.DATE)));
        }
    }

}
