package ru.tandemservice.narfu.component.reports.FinishedAspirants;

import java.util.List;

public class ReportBlock {
    List<ReportRow> levelsData;
    ReportRow totalRow = new ReportRow();

    public List<ReportRow> getLevelsData() {
        return levelsData;
    }

    public ReportBlock(List<ReportRow> rows)
    {
        this.levelsData = rows;
        calculateTotalRow(rows);
    }

    public void setLevelsData(List<ReportRow> levelsData) {
        this.levelsData = levelsData;
    }

    public ReportRow getTotalRow() {
        return totalRow;
    }

    public void setTotalRow(ReportRow totalRow) {
        this.totalRow = totalRow;
    }

    void calculateTotalRow(List<ReportRow> rowList)
    {
        for (int i = 0; i < totalRow.getRowData().length; i++)
            calculateTotalRow(rowList, i);

    }

    void calculateTotalRow(List<ReportRow> rowList, int i)
    {
        for (ReportRow row : rowList)
            totalRow.getRowData()[i] += row.getRowData()[i];
    }
}
