package ru.tandemservice.narfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.narfu.entity.Course2EnrollmentCampaign;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Приемные кампании для отчетов по умолчанию
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class Course2EnrollmentCampaignGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.narfu.entity.Course2EnrollmentCampaign";
    public static final String ENTITY_NAME = "course2EnrollmentCampaign";
    public static final int VERSION_HASH = -2131123224;
    private static IEntityMeta ENTITY_META;

    public static final String L_COURSE = "course";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";

    private Course _course;     // Курс
    private EnrollmentCampaign _enrollmentCampaign;     // Приемная компания

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Приемная компания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная компания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof Course2EnrollmentCampaignGen)
        {
            setCourse(((Course2EnrollmentCampaign)another).getCourse());
            setEnrollmentCampaign(((Course2EnrollmentCampaign)another).getEnrollmentCampaign());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends Course2EnrollmentCampaignGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Course2EnrollmentCampaign.class;
        }

        public T newInstance()
        {
            return (T) new Course2EnrollmentCampaign();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "course":
                    return obj.getCourse();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "course":
                        return true;
                case "enrollmentCampaign":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "course":
                    return true;
                case "enrollmentCampaign":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "course":
                    return Course.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Course2EnrollmentCampaign> _dslPath = new Path<Course2EnrollmentCampaign>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Course2EnrollmentCampaign");
    }
            

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.Course2EnrollmentCampaign#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Приемная компания. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.Course2EnrollmentCampaign#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    public static class Path<E extends Course2EnrollmentCampaign> extends EntityPath<E>
    {
        private Course.Path<Course> _course;
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.Course2EnrollmentCampaign#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Приемная компания. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.Course2EnrollmentCampaign#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

        public Class getEntityClass()
        {
            return Course2EnrollmentCampaign.class;
        }

        public String getEntityName()
        {
            return "course2EnrollmentCampaign";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
