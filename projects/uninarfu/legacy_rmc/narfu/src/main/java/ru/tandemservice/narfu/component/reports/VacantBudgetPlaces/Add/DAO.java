package ru.tandemservice.narfu.component.reports.VacantBudgetPlaces.Add;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.bean.FastPropertyComparator;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLStatement;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import ru.tandemservice.narfu.component.reports.VacantBudgetPlaces.EduLevelReportData;
import ru.tandemservice.narfu.component.reports.VacantBudgetPlaces.ReportData;
import ru.tandemservice.narfu.component.settings.Course2EnrollmentCampaign.Wrapper;
import ru.tandemservice.narfu.entity.Course2EnrollmentCampaign;
import ru.tandemservice.narfu.entity.NarfuReportVacantBudgetPlaces;
import ru.tandemservice.narfu.utils.ReportUtils;
import ru.tandemservice.narfu.utils.WrapperObject;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.lang.Boolean;
import java.util.*;
import java.util.stream.Collectors;


public class DAO extends UniDao<Model> implements IDAO {
    private static final String TEMPLATE_NAME = "narfuVacantBudgetPlaces";


    @Override
    public void prepare(Model model) {
        model.setCourseToCampaigns(new ArrayList<>());
        model.setEnrollmentCampaignModel(new DQLFullCheckSelectModel() {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(EnrollmentCampaign.class, alias)
                        .order(EnrollmentCampaign.title().fromAlias(alias).s());
                if (!StringUtils.isEmpty(filter))
                    FilterUtils.applySimpleLikeFilter(builder, alias, EnrollmentCampaign.title(), filter);
                return builder;
            }
        });
        model.setStudentStatusModel(new DQLFullCheckSelectModel() {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(StudentStatus.class, alias)
                        .order(StudentStatus.title().fromAlias(alias).s());
                if (!StringUtils.isEmpty(filter))
                    FilterUtils.applySimpleLikeFilter(builder, alias, StudentStatus.title(), filter);
                return builder;
            }
        });
        model.setStudentStatusList(createActiveStudentStatusList());
        model.setDevelopFormModel(new LazySimpleSelectModel<>(DevelopForm.class));
        prepareEnrollmentCampaigns(model);
        model.setFormativeOrgUnitListModel(new OrgUnitKindAutocompleteModel("2", null));
    }

    @Override
    public void createReport(Model model) {
        List<EnrollmentCampaign> chosenCampaigns = new ArrayList<>();
        for (Wrapper item : model.getCourseToCampaigns()) {
            chosenCampaigns.addAll(item.getCampaigns());
        }
        if (chosenCampaigns.isEmpty())
            throw new ApplicationException("Не выбрано ни одной приемной кампании");
        ReportData reportData = new ReportData();
        reportData.setReport(new NarfuReportVacantBudgetPlaces());
        reportData.getReport().setFormingDate(new Date());

        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(NarfuReportVacantBudgetPlaces.class, "report");
        dql.column(NarfuReportVacantBudgetPlaces.number().fromAlias("report").s());
        dql.order(DQLExpressions.property(NarfuReportVacantBudgetPlaces.number().fromAlias("report")), OrderDirection.desc);

        IDQLStatement dqlStatement = dql.createStatement(getSession());
        dqlStatement.setMaxResults(1);
        List<Integer> numbersList = dqlStatement.list();
        if (numbersList.size() > 0 && numbersList.get(0) != null) {
            reportData.getReport().setNumber(numbersList.get(0) + 1);
        }
        else {
            reportData.getReport().setNumber(1);
        }

        String title = new StringBuilder()
                //название отчета должно быть такое(примерно) :Отчет № 1 от 09.11.2012
                .append("Отчет № ")
                .append(String.valueOf(reportData.getReport().getNumber()))
                .append(" от ")
                .append(RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(reportData.getReport().getFormingDate()))
                .toString();
        reportData.getReport().setTitle(title);

        try {
            makeExcel(model, reportData);

            DatabaseFile reportFile = new DatabaseFile();
            reportFile.setContent(reportData.getWorkbookStream().toByteArray());
            reportFile.setFilename(title);
            reportFile.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_VND_MS_EXCEL);

            save(reportFile);
            reportData.getReport().setContent(reportFile);
            save(reportData.getReport());

            model.setReport(reportData.getReport());

        }
        catch (Exception e) {
            e.printStackTrace();
            throw new ApplicationException(e.getMessage(), e);
        }
    }

    protected void makeExcel(Model model, ReportData reportData) throws Exception {
        loadTemplate(reportData);
        fillReport(reportData, model);

        reportData.getWorkbook().write();
        reportData.getWorkbook().close();
    }

    protected void loadTemplate(ReportData reportData) throws Exception {
        TemplateDocument template = getCatalogItem(TemplateDocument.class, TEMPLATE_NAME);
        ByteArrayInputStream in = new ByteArrayInputStream(template.getContent());

        Workbook templateWBook = Workbook.getWorkbook(in);
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);

        ByteArrayOutputStream wbOS = new ByteArrayOutputStream();
        WritableWorkbook writableWorkbook = Workbook.createWorkbook(wbOS, templateWBook, ws);

        reportData.setWorkbookStream(wbOS);
        reportData.setWorkbook(writableWorkbook);
        reportData.setSheet(writableWorkbook.getSheet(0));
    }

    protected void fillReport(ReportData reportData, Model model) throws Exception {

        List<Course> filledCourses = new ArrayList<>();
        for (Wrapper item : model.getCourseToCampaigns()) {
            if (item.getCampaigns().size() > 0)
                filledCourses.add(item.getCourse());
        }
        Set<EnrollmentCampaign> cList = new HashSet<>();
        for (Wrapper item : model.getCourseToCampaigns()) {
            cList.addAll(item.getCampaigns());
        }

        List<Long> eduIds = getEducationOrgUnits(cList, model.getDevelopForm());
        Map<String, EduLevelReportData> levelsMap = getEmptyMap(model, model.getDevelopForm(), new ArrayList<>(cList));
        for (Wrapper item : model.getCourseToCampaigns()) {
            fillEnrollmentData(levelsMap, item.getCampaigns(), item.getCourse(), model.getDevelopForm());
        }

        List<Course> courses = getCourses();
        for (Course course : courses)
            fillStudentDataByCourse(model, levelsMap, course, eduIds);

        WritableFont times10 = new WritableFont(WritableFont.TIMES, 10);
        WritableSheet sheet = reportData.getSheet();
        WritableCellFormat ugRowFormat = new WritableCellFormat(times10);
        ugRowFormat.setBorder(Border.ALL, BorderLineStyle.MEDIUM, Colour.BLACK);
        ugRowFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        ugRowFormat.setWrap(true);
        WritableCellFormat ugCourseRowFormat = new WritableCellFormat(times10);
        ugCourseRowFormat.setBorder(jxl.format.Border.TOP, BorderLineStyle.MEDIUM, Colour.BLACK);
        ugCourseRowFormat.setBorder(Border.RIGHT, BorderLineStyle.THIN, Colour.BLACK);
        ugCourseRowFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        ugCourseRowFormat.setWrap(true);
        WritableCellFormat qualificationRowFormat = new WritableCellFormat(times10);
        qualificationRowFormat.setBackground(Colour.GREY_25_PERCENT);
        qualificationRowFormat.setBorder(Border.ALL, BorderLineStyle.MEDIUM, Colour.BLACK);
        qualificationRowFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        qualificationRowFormat.setWrap(true);

        WritableCell cell = (WritableCell) sheet.getCell(0, 1);
        if (cell != null) {
            String title = cell.getContents();
            title = title
                    .replace("devForm", model.getDevelopForm().getTitle().toLowerCase())
                    .replace("date", RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(reportData.getReport().getFormingDate()));
            ((Label) cell).setString(title);
        }

        int rowNum = 6;//первое положение "курсора" в excel-таблице

        EduLevelReportData totalData = new EduLevelReportData(courses);//информация в строке ИТОГО
        Map<WrapperObject, List<EduLevelReportData>> parentMap = getParentsMap(levelsMap);
        Set<WrapperObject> keySet = parentMap.keySet();

        List<WrapperObject> keyList = Arrays.asList(keySet.toArray(new WrapperObject[keySet.size()]));
        final Comparator<IEntity> comparator1 = new FastPropertyComparator<>(EducationLevels.P_OKSO);
        final Comparator<IEntity> comparator2 = CommonBaseUtil.CODE_SIMPLE_SAFE_COMPARATOR;
        Collections.sort(keyList, (o1, o2) -> {
            if (o1.getLevels() != null && o2.getLevels() != null) {
                return comparator1.compare(o1.getLevels(), o2.getLevels());
            }
            else if (o1.getEduProgramSubject2013Group() != null && o2.getEduProgramSubject2013Group() != null) {
                return comparator2.compare(o1.getEduProgramSubject2013Group(), o2.getEduProgramSubject2013Group());
            }
            else if (o1.getLevels() != null) {
                return -1;
            }

            return 0;
        });
        for (WrapperObject ug : keyList) {
            EduLevelReportData ugReportData = new EduLevelReportData(courses);//информация в строке с УГ
            EduLevelReportData bachReportData = new EduLevelReportData(courses);
            bachReportData.setName("бакалавриат");//информация о НП(С) уровня бакалавров
            EduLevelReportData mastReportData = new EduLevelReportData(courses);
            mastReportData.setName("магистратура");//информация о НП(С) уровня магистров
            EduLevelReportData specReportData = new EduLevelReportData(courses);
            specReportData.setName("специалитет");//информация о НП(С) уровня специалитета

            Collections.sort(parentMap.get(ug), (rd1, rd2) -> {
                int r = 0;
                if (rd1.getEduLevel().getInheritedOkso() != null && rd2.getEduLevel().getInheritedOkso() != null) {
                    r = rd1.getEduLevel().getInheritedOkso().compareTo(rd2.getEduLevel().getInheritedOkso());
                }
                if (r == 0)
                    r = rd1.getEduLevel().getQualification().getCode().compareTo(rd2.getEduLevel().getQualification().getCode());
                return r;
            });
            for (EduLevelReportData eduData : parentMap.get(ug)) {
                ugReportData.setDevelopPeriod(eduData.getDevelopPeriod());
                for (Course course : courses) {
                    appendCount(course, eduData.getPlanCount(), ugReportData.getPlanCount());
                    appendCount(course, eduData.getVacantCount(), ugReportData.getVacantCount());
                    appendCount(course, eduData.getTotalCount(), ugReportData.getTotalCount());
                    appendCount(course, eduData.getBudgetCount(), ugReportData.getBudgetCount());
                    if (eduData.getEduLevel().getLevelType().isSpecialty()) {
                        appendCount(course, eduData.getPlanCount(), specReportData.getPlanCount());
                        appendCount(course, eduData.getVacantCount(), specReportData.getVacantCount());
                        appendCount(course, eduData.getTotalCount(), specReportData.getTotalCount());
                        appendCount(course, eduData.getBudgetCount(), specReportData.getBudgetCount());
                    }
                    else if (eduData.getEduLevel().getLevelType().isBachelor()) {
                        appendCount(course, eduData.getPlanCount(), bachReportData.getPlanCount());
                        appendCount(course, eduData.getVacantCount(), bachReportData.getVacantCount());
                        appendCount(course, eduData.getTotalCount(), bachReportData.getTotalCount());
                        appendCount(course, eduData.getBudgetCount(), bachReportData.getBudgetCount());
                    }
                    else if (eduData.getEduLevel().getLevelType().isMaster()) {
                        appendCount(course, eduData.getPlanCount(), mastReportData.getPlanCount());
                        appendCount(course, eduData.getVacantCount(), mastReportData.getVacantCount());
                        appendCount(course, eduData.getTotalCount(), mastReportData.getTotalCount());
                        appendCount(course, eduData.getBudgetCount(), mastReportData.getBudgetCount());
                    }
                }
            }

            //а теперь добавим значеня в строку с ИТОГО:
            for (Course course : courses) {
                Integer planCnt = totalData.getPlanCount().get(course) + ugReportData.getPlanCount().get(course);
                totalData.getPlanCount().put(course, planCnt);
                Integer vacantCnt = totalData.getVacantCount().get(course) + ugReportData.getVacantCount().get(course);
                totalData.getVacantCount().put(course, vacantCnt);
                Integer totalCnt = totalData.getTotalCount().get(course) + ugReportData.getTotalCount().get(course);
                totalData.getTotalCount().put(course, totalCnt);
                Integer contractCnt = totalData.getBudgetCount().get(course) + ugReportData.getBudgetCount().get(course);
                totalData.getBudgetCount().put(course, contractCnt);
            }

            //все нужные суммы посчитаны, выводим в excel:
            appendInfoToSheet(sheet, rowNum, ug, ugReportData, ugRowFormat, ugCourseRowFormat, courses, filledCourses, false, true);
            rowNum++;
            for (EduLevelReportData key : parentMap.get(ug)) {
                appendInfoToSheet(sheet, rowNum, null, key, ugRowFormat, ugCourseRowFormat, courses, filledCourses, true, false);
                rowNum++;

            }
            appendInfoToSheet(sheet, rowNum, null, bachReportData, qualificationRowFormat, qualificationRowFormat, courses, filledCourses, false, false);
            rowNum++;
            appendInfoToSheet(sheet, rowNum, null, specReportData, qualificationRowFormat, qualificationRowFormat, courses, filledCourses, false, false);
            rowNum++;
            appendInfoToSheet(sheet, rowNum, null, mastReportData, qualificationRowFormat, qualificationRowFormat, courses, filledCourses, false, false);
            rowNum++;

        }

    }

    private Map<WrapperObject, List<EduLevelReportData>> getParentsMap(Map<String, EduLevelReportData> levelsMap)
    {
        // Если объявлять с WrapperObject - неправильно будет считаться hashCode и работать containsKey!!!
        Map<Object, List<EduLevelReportData>> result = new HashMap<>();
        for (Map.Entry<String, EduLevelReportData> entry : levelsMap.entrySet()) {
            WrapperObject ug = ReportUtils.getHighParentLevelWrapper(entry.getValue().getEduLevel());
            if (!result.containsKey(ug))
                result.put(ug, new ArrayList<>());
            result.get(ug).add(entry.getValue());
        }

        return (Map) result;

    }

    private List<Long> getEducationOrgUnits(Set<EnrollmentCampaign> cList, DevelopForm dForm)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EnrollmentDirection.class, "ed")
                .column(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().fromAlias("ed")))
//                .addColumn(EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().educationLevel().fromAlias("ed").s())
                .where(DQLExpressions.in(
                               DQLExpressions.property(EnrollmentDirection.enrollmentCampaign().fromAlias("ed")),
                               cList)
                )
                .where(DQLExpressions.eq(
                               DQLExpressions.property(EnrollmentDirection.educationOrgUnit().developForm().fromAlias("ed")),
                               DQLExpressions.value(dForm))
                );
        List<EducationOrgUnit> educationOrgUnits = getList(dql);
        List<EducationLevels> levels = new ArrayList<>();
        for (EducationOrgUnit item : educationOrgUnits) {
            EducationLevels level = item.getEducationLevelHighSchool().getEducationLevel();
            if (level.isProfileOrSpecialization())
                level = level.getParentLevel();
            if (level.isProfileOrSpecialization())
                level = level.getParentLevel();
            levels.add(level);
        }
        DQLSelectBuilder additional = new DQLSelectBuilder()
                .fromEntity(EducationOrgUnit.class, "eo")
                .column(DQLExpressions.property("eo"))
                .where(DQLExpressions.in(
                               DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().educationLevel().parentLevel().fromAlias("eo")),
                               levels)
                )
                .where(DQLExpressions.eq(
                               DQLExpressions.property(EducationOrgUnit.developForm().fromAlias("eo")),
                               DQLExpressions.value(dForm))
                );
        List<EducationOrgUnit> additionalOrgUnits = getList(additional);
        educationOrgUnits.addAll(additionalOrgUnits);
        return (List<Long>) ids(educationOrgUnits);
    }


    /**
     * Вывод в XLS.
     */
    protected void appendInfoToSheet(WritableSheet sheet,
                                     int rowNum, WrapperObject wo,
                                     EduLevelReportData eduData,
                                     WritableCellFormat format,
                                     WritableCellFormat courseFormat,
                                     List<Course> courses,
                                     List<Course> filledCourses,
                                     boolean extensionFlag,
                                     boolean isUG
    ) throws Exception
    {
        Boolean qualificationRow = wo == null;
        EducationLevels elvl = !qualificationRow ? wo.getLevels() : null;

        String code = "";
        String title = "";
        // Это не укрёпнённая группа
        if (!isUG) {
            EduProgramSubject2013 eduProgramSubject2013 = eduData.getEduProgramSubject2013();
            if (eduProgramSubject2013 != null) {
                code = eduProgramSubject2013.getSubjectCode();
            }
            else {
                EducationLevels educationLevels = eduData.getEduLevel();
                // educationLevels - это уже parent
                if (educationLevels != null) {
                    if (educationLevels.getQualification() != null) {
                        code = educationLevels.getInheritedOkso() + "." + educationLevels.getQualification().getCode();
                    }
                    else {
                        code = educationLevels.getOkso();
                    }
                }
            }
        }
        else {
            // Укрупнённая группа 2013
            if (wo.getEduProgramSubject2013Group() != null) {
                code = wo.getEduProgramSubject2013Group().getCode();
            }
            else { // Укрупнённая группа прошлых версий
                if (elvl.getParentLevel() == null) {
                    code = elvl.getOkso();
                }
                else if (elvl.getQualification() != null) {
                    code = elvl.getInheritedOkso() + "." + elvl.getQualification().getCode();
                }
                else {
                    code = elvl.getOkso();
                }
            }
        }

        if (isUG) {
            if (qualificationRow)
                title = eduData.getName();
            else {
                title = wo.getTitle();
            }
        }
        else if (eduData.getEduLevel() != null) {
            title = eduData.getEduLevel().getTitle();
        }
        if (!isUG && !extensionFlag)
            title = eduData.getName();

        String fomativeOrgUnit = "";
        String developPeriod = "";
        String developCondition = "";
        if (!isUG) {
            fomativeOrgUnit = eduData.getFormativeOrgUnit() == null ? "" : eduData.getFormativeOrgUnit();
            developPeriod = eduData.getDevelopPeriod() == null ? "" : eduData.getDevelopPeriod().getTitle();
            developCondition = eduData.getDevelopCondition() == null ? "" : eduData.getDevelopCondition().getTitle();
        }


        Integer colNum = 0;
        sheet.addCell(new Label(colNum++, rowNum, title, format));
        sheet.addCell(new Label(colNum++, rowNum, code, format));
        sheet.addCell(new Label(colNum++, rowNum, fomativeOrgUnit, format));
        sheet.addCell(new Label(colNum++, rowNum, developPeriod, format));
        sheet.addCell(new Label(colNum++, rowNum, developCondition, format));

        Integer sumPlan = 0;
        Integer sumVacant = 0;
        Integer sumContract = 0;
        Integer sumTotal = 0;
        for (Course course : courses) {
            sumPlan += eduData.getPlanCount().get(course);
            sumVacant += eduData.getVacantCount().get(course);
            sumContract += eduData.getBudgetCount().get(course);
            sumTotal += eduData.getTotalCount().get(course);
        }


        //заполняем инф. о плановых местах
        sheet.addCell(new jxl.write.Number(colNum++, rowNum, sumPlan, format));
        for (Course course1 : courses) {
            if (filledCourses.contains(course1))
                sheet.addCell(new jxl.write.Number(colNum++, rowNum, eduData.getPlanCount().get(course1), courseFormat));
            else
                sheet.addCell(new jxl.write.Label(colNum++, rowNum, "", courseFormat));
        }
        //заполняем инф. о вакантных местах
        sheet.addCell(new jxl.write.Number(colNum++, rowNum, sumVacant, format));
        for (Course course1 : courses) {
            if (filledCourses.contains(course1))
                sheet.addCell(new jxl.write.Number(colNum++, rowNum, eduData.getVacantCount().get(course1), courseFormat));
            else
                sheet.addCell(new jxl.write.Label(colNum++, rowNum, "", courseFormat));
        }
        //заполняем инф. о бюджетных местах
        sheet.addCell(new jxl.write.Number(colNum++, rowNum, sumContract, format));
        for (Course course1 : courses) {
            sheet.addCell(new jxl.write.Number(colNum++, rowNum, eduData.getBudgetCount().get(course1), courseFormat));
        }
        //заполняем инф. о количестве студентов
        sheet.addCell(new jxl.write.Number(colNum++, rowNum, sumTotal, format));
        for (Course course1 : courses) {

            sheet.addCell(new jxl.write.Number(colNum++, rowNum, eduData.getTotalCount().get(course1), courseFormat));
        }

        if (extensionFlag) {
            sheet.addCell(new jxl.write.Label(colNum + 1, rowNum, wo == null ? "" : elvl.getQualification().getCode(), format));
        }
    }

    private void appendCount(Course c, Map<Course, Integer> subMap, Map<Course, Integer> totalMap) {
        totalMap.put(c, totalMap.get(c) + subMap.get(c));
    }


    private static List<Course> getCourses() {
        //нам нужна только информация о 1-6 курсах :
        return DevelopGridDAO.getCourseMap().values().stream()
                .filter(c -> c.getIntValue() <= 6)
                .collect(Collectors.toList());
    }

    /**
     * @return Возвращает мап с ключами для НП(С)
     */
    protected Map<String, EduLevelReportData> getEmptyMap(Model model, DevelopForm df, List<EnrollmentCampaign> cList) {
        Map<String, EduLevelReportData> levelsMap = new HashMap<>();
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EnrollmentDirection.class, "ed")
                .column("ed")
                .where(
                        DQLExpressions.in(
                                DQLExpressions.property(EnrollmentDirection.enrollmentCampaign().fromAlias("ed")),
                                cList)
                )
                .where(DQLExpressions.eq(
                        DQLExpressions.property(EnrollmentDirection.educationOrgUnit().developForm().fromAlias("ed")),
                        DQLExpressions.value(df)
                ));
        if (model.getFormativeOrgUnitList() != null && !model.getFormativeOrgUnitList().isEmpty())
            dql.where(DQLExpressions.in(
                    DQLExpressions.property(EnrollmentDirection.educationOrgUnit().formativeOrgUnit().fromAlias("ed")),
                    model.getFormativeOrgUnitList()
            ));

        List<EnrollmentDirection> lines = getList(dql);
        List<Course> courseList = getCourses();
        for (EnrollmentDirection line : lines) {

            String key = makeKey(line.getEducationOrgUnit());
            if (levelsMap.containsKey(key))
                continue;


            EduLevelReportData data = new EduLevelReportData(courseList);
            EducationOrgUnit eou = line.getEducationOrgUnit();
            EducationLevels edu = eou.getEducationLevelHighSchool().getEducationLevel();
            EduProgramSubject2013 eduProgramSubject = null;
            if (edu.getEduProgramSubject() instanceof EduProgramSubject2013) {
                eduProgramSubject = (EduProgramSubject2013) edu.getEduProgramSubject();
            }
            if (edu.isProfileOrSpecialization())
                edu = edu.getParentLevel();
            data.setName(edu.getFullTitle());
            data.setDevelopPeriod(eou.getDevelopPeriod());
            data.setFormativeOrgUnit(eou.getFormativeOrgUnit().getShortTitle());
            data.setDevelopCondition(eou.getDevelopCondition());
            data.setDevelopPeriod(eou.getDevelopPeriod());
            data.setEduLevel(edu);
            data.setEduProgramSubject2013(eduProgramSubject);

            levelsMap.put(key, data);

        }

        return levelsMap;
    }

    /**
     * Заполняет levelsMap данными из приемной кампании campaign(если передали null, то ищем по всем доступным кампаниям):
     */
    protected void fillEnrollmentData(Map<String, EduLevelReportData> levelsMap, List<EnrollmentCampaign> campaigns, Course course, DevelopForm devForm) {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EnrollmentDirection.class, "ed");
        dql.column("ed");
        dql.where(DQLExpressions.eq(
                DQLExpressions.property(EnrollmentDirection.educationOrgUnit().developForm().fromAlias("ed")),
                DQLExpressions.value(devForm)
        ));
        dql.where(DQLExpressions.in(
                          DQLExpressions.property(EnrollmentDirection.enrollmentCampaign().fromAlias("ed")),
                          campaigns)
        );

        List<EnrollmentDirection> enrDirs = getList(dql);
        for (EnrollmentDirection enrData : enrDirs) {
            String key = makeKey(enrData.getEducationOrgUnit());
            EduLevelReportData repData = levelsMap.get(key);
            int plan = 0;
            if (enrData.getMinisterialPlan() != null) {
                plan = enrData.getMinisterialPlan();
            }
            if (repData != null) {
                Integer planCount = repData.getPlanCount().get(course);
                planCount += plan;
                repData.getPlanCount().put(course, planCount);
            }
        }
    }

    private String makeKey(EducationOrgUnit eou)
    {
        EducationLevels lev = eou.getEducationLevelHighSchool().getEducationLevel();
        if (eou.getEducationLevelHighSchool().getEducationLevel().isProfileOrSpecialization()) {
            lev = eou.getEducationLevelHighSchool().getEducationLevel().getParentLevel();
        }
        if (lev.isProfileOrSpecialization()) {
            lev = lev.getParentLevel();
        }
        String retVal = lev.getFullTitle()
                + eou.getFormativeOrgUnit().getShortTitle()
                + eou.getDevelopForm().getTitle()
                + eou.getDevelopCondition().getTitle()
                + eou.getDevelopPeriod().getTitle();
        return retVal;
    }

    /**
     * В EduLevelReportData заполняет общее кол-во студентов и кол-во платников:
     */
    protected void fillStudentDataByCourse(Model model, Map<String, EduLevelReportData> levelsMap, Course course, List<Long> eduIds) {


        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(Student.class, "student");

        dql.where(DQLExpressions.in(
                DQLExpressions.property(Student.educationOrgUnit().id().fromAlias("student")),
                eduIds
        ));
        dql.where(DQLExpressions.eq(
                DQLExpressions.property(Student.course().fromAlias("student")),
                DQLExpressions.value(course)
        ));
        dql.where(DQLExpressions.in(
                DQLExpressions.property(Student.status().fromAlias("student")),
                model.getStudentStatusList()
        ));


        dql.addColumn(Student.educationOrgUnit().id().fromAlias("student").s());
        dql.addColumn(Student.compensationType().code().fromAlias("student").s());
        dql.addColumn(DQLFunctions.count("student"));

        dql.group(DQLExpressions.property(Student.educationOrgUnit().id().fromAlias("student")));
        dql.group(DQLExpressions.property(Student.compensationType().code().fromAlias("student")));


        List<Object[]> list = getList(dql);
        for (Object[] line : list) {
            if (line.length >= 3) {
                /*
                 0 - ид НП(С)
                 1 - способ возмещения затрат
                 2 - кол-во студентов
                 */

                EducationOrgUnit eou = get(EducationOrgUnit.class, (Long) line[0]);
                String key = makeKey(eou);
                EduLevelReportData data = levelsMap.get(key);
                if (data != null) {

                    Integer totalCnt = data.getTotalCount().get(course);
                    if (totalCnt == null) {
                        data.getTotalCount().put(course, ((Long) line[2]).intValue());
                    }
                    else {
                        data.getTotalCount().put(course, totalCnt + ((Long) line[2]).intValue());
                    }

                    String compType = (String) line[1];
                    if (compType.equals(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET)) {
                        Integer budgetCount = data.getBudgetCount().get(course);
                        if (budgetCount == null) {
                            data.getBudgetCount().put(course, ((Long) line[2]).intValue());
                        }
                        else {
                            data.getBudgetCount().put(course, budgetCount + ((Long) line[2]).intValue());
                        }
                    }
                }
            }
        }
        for (Map.Entry<String, EduLevelReportData> entry : levelsMap.entrySet()) {
            int total = entry.getValue().getTotalCount().get(course);
            int budget = entry.getValue().getBudgetCount().get(course);
            int plan = entry.getValue().getPlanCount().get(course);
            entry.getValue().getVacantCount().put(course, plan - budget);
        }

    }

    /**
     * Заполняем фильтр значениями по умолчанию - актвиные статусы студентов.
     *
     * @return
     */
    private List<StudentStatus> createActiveStudentStatusList() {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(StudentStatus.class, "ss")
                .where(DQLExpressions.eq(
                        DQLExpressions.property(StudentStatus.active().fromAlias("ss")),
                        DQLExpressions.value(true)
                ));
        return getList(builder);
    }

    private void prepareEnrollmentCampaigns(Model model)
    {
        List<Course> courseList = getCourses();
        List<Course2EnrollmentCampaign> list = getList(Course2EnrollmentCampaign.class);
        Map<Course, List<EnrollmentCampaign>> map = new HashMap<>();
        for (Course2EnrollmentCampaign item : list) {
            if (!map.containsKey(item.getCourse())) {
                map.put(item.getCourse(), new ArrayList<>());
            }
            map.get(item.getCourse()).add(item.getEnrollmentCampaign());
        }
        Collections.sort(courseList, new EntityComparator<>(new EntityOrder(Course.title().s())));
        for (Course item : courseList) {
            Wrapper wrp = new Wrapper();
            wrp.setCourse(item);
            wrp.setCampaigns(map.get(item));
            model.getCourseToCampaigns().add(wrp);
        }
    }
}
