package ru.tandemservice.narfu.base.ext.EcOrder.logic;

import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.List;

public interface IEcOrderPrintDAO extends IUniBaseDao {

    public RtfDocument getDownloadPrintPersonalInfo(Long orderId);

    public RtfDocument getDownloadPrintExtracts(Long orderId);

    public RtfDocument getDownloadPrintParagraphExtracts(List<IAbstractParagraph<IAbstractOrder>> paragraphList);

    public RtfDocument getDownloadPrintParagraphPersonalInfo(List<IAbstractParagraph<IAbstractOrder>> paragraphList);

    public RtfDocument getDownloadPrintExtract(EnrollmentExtract extract);
}
