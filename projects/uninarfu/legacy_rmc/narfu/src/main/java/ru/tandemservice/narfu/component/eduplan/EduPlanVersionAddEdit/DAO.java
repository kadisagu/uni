package ru.tandemservice.narfu.component.eduplan.EduPlanVersionAddEdit;

import ru.tandemservice.uniepp.component.eduplan.EduPlanVersionAddEdit.Model;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

public class DAO extends ru.tandemservice.uniepp.component.eduplan.EduPlanVersionAddEdit.DAO {

    @Override
    public void prepare(Model model) {
        super.prepare(model);
        if (null == ((EppEduPlanVersion) model.getElement()).getTitlePostfix()) {
            ((EppEduPlanVersion) model.getElement()).setTitlePostfix("" + model.getElement().getEduPlan().getEduStartYear());
        }
    }
}
