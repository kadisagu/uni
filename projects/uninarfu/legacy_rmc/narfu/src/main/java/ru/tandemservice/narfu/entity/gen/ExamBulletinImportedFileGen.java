package ru.tandemservice.narfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.narfu.entity.ExamBulletinImportedFile;
import ru.tandemservice.narfu.entity.catalog.ExamBulletinImportedFileResult;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Импорт оценок из csv файла в ведомости
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ExamBulletinImportedFileGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.narfu.entity.ExamBulletinImportedFile";
    public static final String ENTITY_NAME = "examBulletinImportedFile";
    public static final int VERSION_HASH = 1024422862;
    private static IEntityMeta ENTITY_META;

    public static final String P_IMPORT_DATE = "importDate";
    public static final String P_IMPORTED_FILE = "importedFile";
    public static final String P_IMPORTED_FILE_C_R_C = "importedFileCRC";
    public static final String P_RESULT_FILE = "resultFile";
    public static final String L_IMPORT_RESULT = "importResult";

    private Date _importDate;     // Дата импорта
    private byte[] _importedFile;     // Импортированный файл
    private String _importedFileCRC;     // CRC импортированного файла
    private byte[] _resultFile;     // Результирующий файл
    private ExamBulletinImportedFileResult _importResult;     // Результат импорта оценок из csv файла в ведомости

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата импорта. Свойство не может быть null.
     */
    @NotNull
    public Date getImportDate()
    {
        return _importDate;
    }

    /**
     * @param importDate Дата импорта. Свойство не может быть null.
     */
    public void setImportDate(Date importDate)
    {
        dirty(_importDate, importDate);
        _importDate = importDate;
    }

    /**
     * @return Импортированный файл. Свойство не может быть null.
     */
    @NotNull
    public byte[] getImportedFile()
    {
        return _importedFile;
    }

    /**
     * @param importedFile Импортированный файл. Свойство не может быть null.
     */
    public void setImportedFile(byte[] importedFile)
    {
        dirty(_importedFile, importedFile);
        _importedFile = importedFile;
    }

    /**
     * @return CRC импортированного файла. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getImportedFileCRC()
    {
        return _importedFileCRC;
    }

    /**
     * @param importedFileCRC CRC импортированного файла. Свойство не может быть null.
     */
    public void setImportedFileCRC(String importedFileCRC)
    {
        dirty(_importedFileCRC, importedFileCRC);
        _importedFileCRC = importedFileCRC;
    }

    /**
     * @return Результирующий файл. Свойство не может быть null.
     */
    @NotNull
    public byte[] getResultFile()
    {
        return _resultFile;
    }

    /**
     * @param resultFile Результирующий файл. Свойство не может быть null.
     */
    public void setResultFile(byte[] resultFile)
    {
        dirty(_resultFile, resultFile);
        _resultFile = resultFile;
    }

    /**
     * @return Результат импорта оценок из csv файла в ведомости.
     */
    public ExamBulletinImportedFileResult getImportResult()
    {
        return _importResult;
    }

    /**
     * @param importResult Результат импорта оценок из csv файла в ведомости.
     */
    public void setImportResult(ExamBulletinImportedFileResult importResult)
    {
        dirty(_importResult, importResult);
        _importResult = importResult;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ExamBulletinImportedFileGen)
        {
            setImportDate(((ExamBulletinImportedFile)another).getImportDate());
            setImportedFile(((ExamBulletinImportedFile)another).getImportedFile());
            setImportedFileCRC(((ExamBulletinImportedFile)another).getImportedFileCRC());
            setResultFile(((ExamBulletinImportedFile)another).getResultFile());
            setImportResult(((ExamBulletinImportedFile)another).getImportResult());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ExamBulletinImportedFileGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ExamBulletinImportedFile.class;
        }

        public T newInstance()
        {
            return (T) new ExamBulletinImportedFile();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "importDate":
                    return obj.getImportDate();
                case "importedFile":
                    return obj.getImportedFile();
                case "importedFileCRC":
                    return obj.getImportedFileCRC();
                case "resultFile":
                    return obj.getResultFile();
                case "importResult":
                    return obj.getImportResult();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "importDate":
                    obj.setImportDate((Date) value);
                    return;
                case "importedFile":
                    obj.setImportedFile((byte[]) value);
                    return;
                case "importedFileCRC":
                    obj.setImportedFileCRC((String) value);
                    return;
                case "resultFile":
                    obj.setResultFile((byte[]) value);
                    return;
                case "importResult":
                    obj.setImportResult((ExamBulletinImportedFileResult) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "importDate":
                        return true;
                case "importedFile":
                        return true;
                case "importedFileCRC":
                        return true;
                case "resultFile":
                        return true;
                case "importResult":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "importDate":
                    return true;
                case "importedFile":
                    return true;
                case "importedFileCRC":
                    return true;
                case "resultFile":
                    return true;
                case "importResult":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "importDate":
                    return Date.class;
                case "importedFile":
                    return byte[].class;
                case "importedFileCRC":
                    return String.class;
                case "resultFile":
                    return byte[].class;
                case "importResult":
                    return ExamBulletinImportedFileResult.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ExamBulletinImportedFile> _dslPath = new Path<ExamBulletinImportedFile>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ExamBulletinImportedFile");
    }
            

    /**
     * @return Дата импорта. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.ExamBulletinImportedFile#getImportDate()
     */
    public static PropertyPath<Date> importDate()
    {
        return _dslPath.importDate();
    }

    /**
     * @return Импортированный файл. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.ExamBulletinImportedFile#getImportedFile()
     */
    public static PropertyPath<byte[]> importedFile()
    {
        return _dslPath.importedFile();
    }

    /**
     * @return CRC импортированного файла. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.ExamBulletinImportedFile#getImportedFileCRC()
     */
    public static PropertyPath<String> importedFileCRC()
    {
        return _dslPath.importedFileCRC();
    }

    /**
     * @return Результирующий файл. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.ExamBulletinImportedFile#getResultFile()
     */
    public static PropertyPath<byte[]> resultFile()
    {
        return _dslPath.resultFile();
    }

    /**
     * @return Результат импорта оценок из csv файла в ведомости.
     * @see ru.tandemservice.narfu.entity.ExamBulletinImportedFile#getImportResult()
     */
    public static ExamBulletinImportedFileResult.Path<ExamBulletinImportedFileResult> importResult()
    {
        return _dslPath.importResult();
    }

    public static class Path<E extends ExamBulletinImportedFile> extends EntityPath<E>
    {
        private PropertyPath<Date> _importDate;
        private PropertyPath<byte[]> _importedFile;
        private PropertyPath<String> _importedFileCRC;
        private PropertyPath<byte[]> _resultFile;
        private ExamBulletinImportedFileResult.Path<ExamBulletinImportedFileResult> _importResult;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата импорта. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.ExamBulletinImportedFile#getImportDate()
     */
        public PropertyPath<Date> importDate()
        {
            if(_importDate == null )
                _importDate = new PropertyPath<Date>(ExamBulletinImportedFileGen.P_IMPORT_DATE, this);
            return _importDate;
        }

    /**
     * @return Импортированный файл. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.ExamBulletinImportedFile#getImportedFile()
     */
        public PropertyPath<byte[]> importedFile()
        {
            if(_importedFile == null )
                _importedFile = new PropertyPath<byte[]>(ExamBulletinImportedFileGen.P_IMPORTED_FILE, this);
            return _importedFile;
        }

    /**
     * @return CRC импортированного файла. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.ExamBulletinImportedFile#getImportedFileCRC()
     */
        public PropertyPath<String> importedFileCRC()
        {
            if(_importedFileCRC == null )
                _importedFileCRC = new PropertyPath<String>(ExamBulletinImportedFileGen.P_IMPORTED_FILE_C_R_C, this);
            return _importedFileCRC;
        }

    /**
     * @return Результирующий файл. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.ExamBulletinImportedFile#getResultFile()
     */
        public PropertyPath<byte[]> resultFile()
        {
            if(_resultFile == null )
                _resultFile = new PropertyPath<byte[]>(ExamBulletinImportedFileGen.P_RESULT_FILE, this);
            return _resultFile;
        }

    /**
     * @return Результат импорта оценок из csv файла в ведомости.
     * @see ru.tandemservice.narfu.entity.ExamBulletinImportedFile#getImportResult()
     */
        public ExamBulletinImportedFileResult.Path<ExamBulletinImportedFileResult> importResult()
        {
            if(_importResult == null )
                _importResult = new ExamBulletinImportedFileResult.Path<ExamBulletinImportedFileResult>(L_IMPORT_RESULT, this);
            return _importResult;
        }

        public Class getEntityClass()
        {
            return ExamBulletinImportedFile.class;
        }

        public String getEntityName()
        {
            return "examBulletinImportedFile";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
