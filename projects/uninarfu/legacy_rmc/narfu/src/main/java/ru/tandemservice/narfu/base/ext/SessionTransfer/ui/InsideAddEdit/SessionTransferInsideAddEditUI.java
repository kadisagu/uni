package ru.tandemservice.narfu.base.ext.SessionTransfer.ui.InsideAddEdit;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.narfu.base.ext.SessionTransfer.logic.ISessionTransferDaoExt;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.base.bo.EducationYear.EducationYearManager;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.unisession.base.bo.SessionTransfer.SessionTransferManager;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.InsideAddEdit.SessionTransferInOpWrapper;

import java.util.ArrayList;
import java.util.Date;

public class SessionTransferInsideAddEditUI extends ru.tandemservice.unisession.base.bo.SessionTransfer.ui.InsideAddEdit.SessionTransferInsideAddEditUI {

    private ISelectModel eduPlanVersionModel;
    private EppEduPlanVersion eduPlanVersion;

    @Override
    public void onComponentRefresh() {
        super.onComponentRefresh();

        if (isAutoForm() && isAddForm()) {
            setMarkDate(new Date());
            getDocument().setNumber("0");
            getDocument().setEducationYear(EducationYearManager.instance().dao().getCurrent());
        }

        setEduPlanVersionModel(new DQLFullCheckSelectModel(EppEduPlanVersion.title().s()) {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(EppStudent2EduPlanVersion.class, "r")
                        .where(DQLExpressions.eqValue(DQLExpressions.property(EppStudent2EduPlanVersion.student().id().fromAlias("r")), getStudentId()))
                        .where(DQLExpressions.isNotNull(DQLExpressions.property(EppStudent2EduPlanVersion.removalDate().fromAlias("r"))))
                        .order(EppStudent2EduPlanVersion.removalDate().fromAlias("r").s())
                        .column(EppStudent2EduPlanVersion.eduPlanVersion().fromAlias("r").s());
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppEduPlanVersion.class, alias)
                        .where(DQLExpressions.in(DQLExpressions.property(alias), subBuilder.buildQuery()));
                FilterUtils.applySimpleLikeFilter(builder, alias, EppEduPlanVersion.eduPlan().number(), filter);
                return builder;
            }
        });
    }

    @Override
    public void onClickApply() {
        for (SessionTransferInOpWrapper item : getRowList()) {
            if (item.getComment() != null && item.getComment().contains("</div>"))
                item.setComment(item.getComment().split("</div>")[1].trim());
        }
        super.onClickApply();
    }

    public void refreshRowList() {
        if (getEduPlanVersion() == null)
            setRowList(new ArrayList<SessionTransferInOpWrapper>());
        else
            setRowList(((ISessionTransferDaoExt) SessionTransferManager.instance().dao()).getAutoFormInnerTransfers(getStudentId(), getEduPlanVersion()));
    }

    private boolean isAddForm() {
        return getDocumentId() == null;
    }

    public boolean isEduPlanComboVisible() {
        return isAddForm() && isAutoForm();
    }

    public ISelectModel getEduPlanVersionModel() {
        return eduPlanVersionModel;
    }

    public void setEduPlanVersionModel(ISelectModel eduPlanVersionModel) {
        this.eduPlanVersionModel = eduPlanVersionModel;
    }

    public EppEduPlanVersion getEduPlanVersion() {
        return eduPlanVersion;
    }

    public void setEduPlanVersion(EppEduPlanVersion eduPlanVersion) {
        this.eduPlanVersion = eduPlanVersion;
    }

}
