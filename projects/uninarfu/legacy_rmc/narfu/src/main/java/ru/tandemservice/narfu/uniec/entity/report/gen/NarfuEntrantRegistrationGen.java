package ru.tandemservice.narfu.uniec.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.narfu.uniec.entity.report.NarfuEntrantRegistration;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Список отчетов «Журнал регистрации абитуриентов»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class NarfuEntrantRegistrationGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.narfu.uniec.entity.report.NarfuEntrantRegistration";
    public static final String ENTITY_NAME = "narfuEntrantRegistration";
    public static final int VERSION_HASH = 140918217;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_EDUCATION_LEVEL_HIGH_SCHOOL = "educationLevelHighSchool";
    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_COMPENSATION_TYPE_TITLE = "compensationTypeTitle";
    public static final String P_FORMATIVE_ORG_UNIT_TITLE = "formativeOrgUnitTitle";
    public static final String P_DEVELOP_FORM_TITLE = "developFormTitle";
    public static final String P_DEVELOP_CONDITION_TITLE = "developConditionTitle";
    public static final String P_KIND = "kind";

    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private String _educationLevelHighSchool;     // Направления подготовки (специальности)
    private Date _dateFrom;     // Заявления с
    private String _compensationTypeTitle;     // Вид возмещения затрат
    private String _formativeOrgUnitTitle;     // Формирующее подр.
    private String _developFormTitle;     // Форма освоения
    private String _developConditionTitle;     // Условие освоения
    private String _kind;     // Тип отчета

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Направления подготовки (специальности).
     */
    public String getEducationLevelHighSchool()
    {
        return _educationLevelHighSchool;
    }

    /**
     * @param educationLevelHighSchool Направления подготовки (специальности).
     */
    public void setEducationLevelHighSchool(String educationLevelHighSchool)
    {
        dirty(_educationLevelHighSchool, educationLevelHighSchool);
        _educationLevelHighSchool = educationLevelHighSchool;
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     */
    @NotNull
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Заявления с. Свойство не может быть null.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Вид возмещения затрат.
     */
    @Length(max=255)
    public String getCompensationTypeTitle()
    {
        return _compensationTypeTitle;
    }

    /**
     * @param compensationTypeTitle Вид возмещения затрат.
     */
    public void setCompensationTypeTitle(String compensationTypeTitle)
    {
        dirty(_compensationTypeTitle, compensationTypeTitle);
        _compensationTypeTitle = compensationTypeTitle;
    }

    /**
     * @return Формирующее подр..
     */
    public String getFormativeOrgUnitTitle()
    {
        return _formativeOrgUnitTitle;
    }

    /**
     * @param formativeOrgUnitTitle Формирующее подр..
     */
    public void setFormativeOrgUnitTitle(String formativeOrgUnitTitle)
    {
        dirty(_formativeOrgUnitTitle, formativeOrgUnitTitle);
        _formativeOrgUnitTitle = formativeOrgUnitTitle;
    }

    /**
     * @return Форма освоения.
     */
    @Length(max=255)
    public String getDevelopFormTitle()
    {
        return _developFormTitle;
    }

    /**
     * @param developFormTitle Форма освоения.
     */
    public void setDevelopFormTitle(String developFormTitle)
    {
        dirty(_developFormTitle, developFormTitle);
        _developFormTitle = developFormTitle;
    }

    /**
     * @return Условие освоения.
     */
    @Length(max=255)
    public String getDevelopConditionTitle()
    {
        return _developConditionTitle;
    }

    /**
     * @param developConditionTitle Условие освоения.
     */
    public void setDevelopConditionTitle(String developConditionTitle)
    {
        dirty(_developConditionTitle, developConditionTitle);
        _developConditionTitle = developConditionTitle;
    }

    /**
     * @return Тип отчета.
     */
    @Length(max=255)
    public String getKind()
    {
        return _kind;
    }

    /**
     * @param kind Тип отчета.
     */
    public void setKind(String kind)
    {
        dirty(_kind, kind);
        _kind = kind;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof NarfuEntrantRegistrationGen)
        {
            setEnrollmentCampaign(((NarfuEntrantRegistration)another).getEnrollmentCampaign());
            setEducationLevelHighSchool(((NarfuEntrantRegistration)another).getEducationLevelHighSchool());
            setDateFrom(((NarfuEntrantRegistration)another).getDateFrom());
            setCompensationTypeTitle(((NarfuEntrantRegistration)another).getCompensationTypeTitle());
            setFormativeOrgUnitTitle(((NarfuEntrantRegistration)another).getFormativeOrgUnitTitle());
            setDevelopFormTitle(((NarfuEntrantRegistration)another).getDevelopFormTitle());
            setDevelopConditionTitle(((NarfuEntrantRegistration)another).getDevelopConditionTitle());
            setKind(((NarfuEntrantRegistration)another).getKind());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends NarfuEntrantRegistrationGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) NarfuEntrantRegistration.class;
        }

        public T newInstance()
        {
            return (T) new NarfuEntrantRegistration();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "educationLevelHighSchool":
                    return obj.getEducationLevelHighSchool();
                case "dateFrom":
                    return obj.getDateFrom();
                case "compensationTypeTitle":
                    return obj.getCompensationTypeTitle();
                case "formativeOrgUnitTitle":
                    return obj.getFormativeOrgUnitTitle();
                case "developFormTitle":
                    return obj.getDevelopFormTitle();
                case "developConditionTitle":
                    return obj.getDevelopConditionTitle();
                case "kind":
                    return obj.getKind();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "educationLevelHighSchool":
                    obj.setEducationLevelHighSchool((String) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "compensationTypeTitle":
                    obj.setCompensationTypeTitle((String) value);
                    return;
                case "formativeOrgUnitTitle":
                    obj.setFormativeOrgUnitTitle((String) value);
                    return;
                case "developFormTitle":
                    obj.setDevelopFormTitle((String) value);
                    return;
                case "developConditionTitle":
                    obj.setDevelopConditionTitle((String) value);
                    return;
                case "kind":
                    obj.setKind((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                        return true;
                case "educationLevelHighSchool":
                        return true;
                case "dateFrom":
                        return true;
                case "compensationTypeTitle":
                        return true;
                case "formativeOrgUnitTitle":
                        return true;
                case "developFormTitle":
                        return true;
                case "developConditionTitle":
                        return true;
                case "kind":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return true;
                case "educationLevelHighSchool":
                    return true;
                case "dateFrom":
                    return true;
                case "compensationTypeTitle":
                    return true;
                case "formativeOrgUnitTitle":
                    return true;
                case "developFormTitle":
                    return true;
                case "developConditionTitle":
                    return true;
                case "kind":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "educationLevelHighSchool":
                    return String.class;
                case "dateFrom":
                    return Date.class;
                case "compensationTypeTitle":
                    return String.class;
                case "formativeOrgUnitTitle":
                    return String.class;
                case "developFormTitle":
                    return String.class;
                case "developConditionTitle":
                    return String.class;
                case "kind":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<NarfuEntrantRegistration> _dslPath = new Path<NarfuEntrantRegistration>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "NarfuEntrantRegistration");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.narfu.uniec.entity.report.NarfuEntrantRegistration#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Направления подготовки (специальности).
     * @see ru.tandemservice.narfu.uniec.entity.report.NarfuEntrantRegistration#getEducationLevelHighSchool()
     */
    public static PropertyPath<String> educationLevelHighSchool()
    {
        return _dslPath.educationLevelHighSchool();
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.narfu.uniec.entity.report.NarfuEntrantRegistration#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.narfu.uniec.entity.report.NarfuEntrantRegistration#getCompensationTypeTitle()
     */
    public static PropertyPath<String> compensationTypeTitle()
    {
        return _dslPath.compensationTypeTitle();
    }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.narfu.uniec.entity.report.NarfuEntrantRegistration#getFormativeOrgUnitTitle()
     */
    public static PropertyPath<String> formativeOrgUnitTitle()
    {
        return _dslPath.formativeOrgUnitTitle();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.narfu.uniec.entity.report.NarfuEntrantRegistration#getDevelopFormTitle()
     */
    public static PropertyPath<String> developFormTitle()
    {
        return _dslPath.developFormTitle();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.narfu.uniec.entity.report.NarfuEntrantRegistration#getDevelopConditionTitle()
     */
    public static PropertyPath<String> developConditionTitle()
    {
        return _dslPath.developConditionTitle();
    }

    /**
     * @return Тип отчета.
     * @see ru.tandemservice.narfu.uniec.entity.report.NarfuEntrantRegistration#getKind()
     */
    public static PropertyPath<String> kind()
    {
        return _dslPath.kind();
    }

    public static class Path<E extends NarfuEntrantRegistration> extends StorableReport.Path<E>
    {
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<String> _educationLevelHighSchool;
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<String> _compensationTypeTitle;
        private PropertyPath<String> _formativeOrgUnitTitle;
        private PropertyPath<String> _developFormTitle;
        private PropertyPath<String> _developConditionTitle;
        private PropertyPath<String> _kind;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.narfu.uniec.entity.report.NarfuEntrantRegistration#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Направления подготовки (специальности).
     * @see ru.tandemservice.narfu.uniec.entity.report.NarfuEntrantRegistration#getEducationLevelHighSchool()
     */
        public PropertyPath<String> educationLevelHighSchool()
        {
            if(_educationLevelHighSchool == null )
                _educationLevelHighSchool = new PropertyPath<String>(NarfuEntrantRegistrationGen.P_EDUCATION_LEVEL_HIGH_SCHOOL, this);
            return _educationLevelHighSchool;
        }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.narfu.uniec.entity.report.NarfuEntrantRegistration#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(NarfuEntrantRegistrationGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.narfu.uniec.entity.report.NarfuEntrantRegistration#getCompensationTypeTitle()
     */
        public PropertyPath<String> compensationTypeTitle()
        {
            if(_compensationTypeTitle == null )
                _compensationTypeTitle = new PropertyPath<String>(NarfuEntrantRegistrationGen.P_COMPENSATION_TYPE_TITLE, this);
            return _compensationTypeTitle;
        }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.narfu.uniec.entity.report.NarfuEntrantRegistration#getFormativeOrgUnitTitle()
     */
        public PropertyPath<String> formativeOrgUnitTitle()
        {
            if(_formativeOrgUnitTitle == null )
                _formativeOrgUnitTitle = new PropertyPath<String>(NarfuEntrantRegistrationGen.P_FORMATIVE_ORG_UNIT_TITLE, this);
            return _formativeOrgUnitTitle;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.narfu.uniec.entity.report.NarfuEntrantRegistration#getDevelopFormTitle()
     */
        public PropertyPath<String> developFormTitle()
        {
            if(_developFormTitle == null )
                _developFormTitle = new PropertyPath<String>(NarfuEntrantRegistrationGen.P_DEVELOP_FORM_TITLE, this);
            return _developFormTitle;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.narfu.uniec.entity.report.NarfuEntrantRegistration#getDevelopConditionTitle()
     */
        public PropertyPath<String> developConditionTitle()
        {
            if(_developConditionTitle == null )
                _developConditionTitle = new PropertyPath<String>(NarfuEntrantRegistrationGen.P_DEVELOP_CONDITION_TITLE, this);
            return _developConditionTitle;
        }

    /**
     * @return Тип отчета.
     * @see ru.tandemservice.narfu.uniec.entity.report.NarfuEntrantRegistration#getKind()
     */
        public PropertyPath<String> kind()
        {
            if(_kind == null )
                _kind = new PropertyPath<String>(NarfuEntrantRegistrationGen.P_KIND, this);
            return _kind;
        }

        public Class getEntityClass()
        {
            return NarfuEntrantRegistration.class;
        }

        public String getEntityName()
        {
            return "narfuEntrantRegistration";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
