package ru.tandemservice.unisession.dao.print;

import org.tandemframework.rtf.document.RtfDocument;

public interface IEntrantRequestPrintDAO {

    public RtfDocument printEntrantRequest(Long requetId);

}
