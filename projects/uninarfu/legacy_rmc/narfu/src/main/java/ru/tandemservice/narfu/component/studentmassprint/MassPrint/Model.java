package ru.tandemservice.narfu.component.studentmassprint.MassPrint;

import org.tandemframework.tapsupport.component.selection.ISelectModel;

public class Model extends ru.tandemservice.unirmc.component.studentmassprint.MassPrint.Model
{

    private ISelectModel studentCategorySelectModel;

    public ISelectModel getStudentCategorySelectModel() {
        return studentCategorySelectModel;
    }

    public void setStudentCategorySelectModel(
            ISelectModel studentCategorySelectModel)
    {
        this.studentCategorySelectModel = studentCategorySelectModel;
    }
}
