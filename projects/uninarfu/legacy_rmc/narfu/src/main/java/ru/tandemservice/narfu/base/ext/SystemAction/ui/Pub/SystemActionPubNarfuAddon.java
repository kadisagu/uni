package ru.tandemservice.narfu.base.ext.SystemAction.ui.Pub;

import org.apache.commons.io.FileUtils;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.ui.Pub.SystemActionPubUI;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.narfu.dao.movestudent.OrderCopyDAO;
import ru.tandemservice.narfu.impexp.dao.DAOStudentTo1C;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;

import java.io.File;
import java.io.IOException;

public class SystemActionPubNarfuAddon extends UIAddon
{

    public SystemActionPubNarfuAddon(IUIPresenter presenter, String name,
                                     String componentId)
    {
        super(presenter, name, componentId);

    }

    SystemActionPubUI parent = getPresenter();

    public void onRunClearGrant()
    {

        this.getActivationBuilder().asRegionDialog("ru.tandemservice.narfu.component.orgUnit.GrandTab").activate();

    }


    public void onCopyOldAgreementToNew()
    {
        //this.getActivationBuilder().asRegionDialog("ru.tandemservice.narfu.component.systemaction.ctr.OldAgreementToNew").activate();

        // целиком в окне
        IBusinessComponent component = parent.getUserContext().getCurrentComponent();
        component.getController().activateInRoot(component, new ComponentActivator("ru.tandemservice.narfu.component.systemaction.ctr.OldAgreementToNew"));

    }

    public void onDemonStudentsTo1CState()
    {
        ErrorCollector errors = parent.getUserContext().getErrorCollector();
        if (DAOStudentTo1C.IS_BEAZY)
            errors.add("Служба уже исполняется " + DAOStudentTo1C.LOG_STRING);
        else
            errors.add("Служба НЕ ЗАПУЩЕНА" + DAOStudentTo1C.LOG_STRING);
    }

    public void onRunDemonStudentsTo1C()
    {

        ErrorCollector errors = parent.getUserContext().getErrorCollector();
        if (DAOStudentTo1C.IS_BEAZY)
        {
            errors.add("Служба уже исполняется " + DAOStudentTo1C.LOG_STRING);
        } else
        {
            // стартуем
            DAOStudentTo1C.ForseRun = true;
            DAOStudentTo1C.DAEMON.wakeUpDaemon();
        }
    }

    /**
     * импортировать студентов в 1с
     */
    public void onImportStudentsTo1C()
    {

        String path = ApplicationRuntime.getAppInstallPath();

        File file = new File(path, "impexp/1c/fromTU.xml");
        if (file.exists())
        {
            // можно прочитать содержимое
            try
            {
                byte[] bts = FileUtils.readFileToByteArray(file);
                Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(bts, "studentTo1C.xml");
                IBusinessComponent component = parent.getUserContext().getCurrentComponent();
                component.getController().activateInRoot(component, new ComponentActivator("ru.tandemservice.uni.component.reports.PrintReport", (new UniMap()).add("id", id).add("zip", Boolean.FALSE).add("extension", "xml")));

            } catch (IOException e)
            {
                throw CoreExceptionUtils.getRuntimeException(e);
            }
        } else
        {
            ErrorCollector errors = parent.getUserContext().getErrorCollector();
            errors.add("Файл с результатами импорта еще не создан");
        }
    }

    public void onClickCopyOrders()
    {
        OrderCopyDAO.getInstance().doCopyOrders();
    }
}
