package ru.tandemservice.narfu.dao.uniplancheckrmc;

import ru.tandemservice.uniplancheckrmc.dao.InspectionExamInYear;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionTypeCodes;

import java.util.Arrays;
import java.util.List;

public class InspectionSetoffOnlyInYear extends InspectionExamInYear {

    @Override
    protected List<String> getActionTypeList() {
        return Arrays.asList(
                EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF,
                EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF_DIFF);
    }
}
