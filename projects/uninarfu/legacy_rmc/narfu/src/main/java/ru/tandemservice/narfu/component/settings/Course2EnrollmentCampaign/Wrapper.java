package ru.tandemservice.narfu.component.settings.Course2EnrollmentCampaign;


import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.ArrayList;
import java.util.List;

public class Wrapper {

    private Course course;
    private List<EnrollmentCampaign> campaigns = new ArrayList<EnrollmentCampaign>();

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public List<EnrollmentCampaign> getCampaigns() {
        return campaigns;
    }

    public void setCampaigns(List<EnrollmentCampaign> campaigns) {
        this.campaigns = campaigns;
    }
}
