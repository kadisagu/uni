package ru.tandemservice.narfu.component.entrant;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import ru.tandemservice.narfu.entity.EntrantStateExamCertificateExt;
import ru.tandemservice.narfu.entity.StateExamSubjectPassScoreExt;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniec.component.entrant.EntrantStateExamCertificateAddEdit.Model;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate;
import ru.tandemservice.uniec.entity.entrant.StateExamSubjectMark;
import ru.tandemservice.uniec.entity.settings.StateExamSubjectPassScore;

import java.util.*;

public class StateExamUtil {

    @SuppressWarnings("unchecked")
    public static void update(Model model, Session session) {
        IUniBaseDao dao = IUniBaseDao.instance.get();

        Map<Long, Integer> markMap = ((BlockColumn) model.getDataSource().getColumn(1)).getValueMap();

        StateExamUtil.validate(
                model.getCertificate(),
                model.getTerritoryCode(),
                model.getNumber(),
                model.getYear(),
                markMap
        );

        ErrorCollector errors = ContextLocal.getErrorCollector();
        if (errors.hasErrors())
            return;

        session.saveOrUpdate(model.getCertificate());

        for (Map.Entry<Long, Integer> entry : markMap.entrySet()) {
            Integer mark = entry.getValue();
            StateExamSubject stateExamSubject = dao.getNotNull(StateExamSubject.class, entry.getKey());

            Criteria criteria = session.createCriteria(StateExamSubjectMark.class);
            criteria.add(Restrictions.eq("subject", stateExamSubject));
            criteria.add(Restrictions.eq("certificate", model.getCertificate()));
            StateExamSubjectMark stateExamSubjectMark = (StateExamSubjectMark) criteria.uniqueResult();

            if (mark == null) {
                if (stateExamSubjectMark != null)
                    session.delete(stateExamSubjectMark);
            }
            else {
                if (stateExamSubjectMark == null)
                    stateExamSubjectMark = new StateExamSubjectMark();

                stateExamSubjectMark.setCertificate(model.getCertificate());
                stateExamSubjectMark.setSubject(stateExamSubject);
                stateExamSubjectMark.setMark(mark);

                session.saveOrUpdate(stateExamSubjectMark);
            }
        }
    }

    /**
     * Валидация формы ввода ЕГЭ
     *
     * @param certificate       сертификат
     * @param certTerritoryCode терр код сертификата
     * @param certNumber        сам номер
     * @param certYear          последние 2 цифры номера сертивиката
     * @param markMap           мапа (ИД предмета на оценку)
     */
    private static void validate(
            EntrantStateExamCertificate certificate,
            String certTerritoryCode,
            String certNumber,
            String certYear,
            Map<Long, Integer> markMap
    )
    {
        IUniBaseDao dao = IUniBaseDao.instance.get();
        ErrorCollector errors = ContextLocal.getErrorCollector();

        String stateExamTypeCode = certificate.getStateExamType().getCode();
        boolean numberRequired = certificate.getEntrant().getEnrollmentCampaign().isStateExamWave1NumberRequired();
        boolean numberExists = (StringUtils.isNotEmpty(certNumber)) && (!"000000000".equals(certNumber));

        if (("1".equals(stateExamTypeCode)) && (!numberExists) && (numberRequired))
            errors.add("Для свидетельства ЕГЭ первой волны номер обязателен.", "number");
        if ((numberExists) && (certNumber.length() != 9))
            errors.add("Номер свидетельства ЕГЭ должен содержать 9 цифр.", "number");
        if ((certificate.getRegistrationDate() != null) && (certificate.getRegistrationDate().after(new Date())))
            errors.add("Дата добавления должна быть не позже сегодняшней.", "registrationDate");
        if ((certificate.getIssuanceDate() != null) && (certificate.getIssuanceDate().after(new Date())))
            errors.add("Дата выдачи должна быть не позже сегодняшней", "issuanceDate");

        //проверка номера сертификата
        String certificateNumber;
        if (numberExists) {
            certificateNumber = certTerritoryCode + certNumber + certYear;

            MQBuilder builder = new MQBuilder(EntrantStateExamCertificate.ENTITY_CLASS, "c")
                    .add(MQExpression.eq("c", EntrantStateExamCertificate.entrant().enrollmentCampaign(), certificate.getEntrant().getEnrollmentCampaign()))
                    .add(MQExpression.eq("c", EntrantStateExamCertificate.number(), certificateNumber));
            if (certificate.getId() != null)
                builder.add(MQExpression.notEq("c", EntrantStateExamCertificate.id(), certificate.getId()));

            if (dao.getCount(builder) > 0)
                errors.add("Номер свидетельства ЕГЭ должен быть уникальным в рамках приемной кампании.", "territoryCode", "number", "year");

        }
        else
            certificateNumber = certTerritoryCode + "000000000" + certYear;

        certificate.setNumber(certificateNumber);

        //проверка чтоб стояла оценка
        boolean noSubjects = true;
        for (Map.Entry<Long, Integer> entry : markMap.entrySet())
            if (entry.getValue() != null) {
                noSubjects = false;
                break;
            }
        if (noSubjects)
            errors.add("В свидетельстве ЕГЭ необходимо указать баллы хотя бы для одного из предметов.");

        //проверка чтоб оценка была зачтенной
        Map<StateExamSubject, StateExamSubjectPassScoreExt> scoreMap = getScoreMap(certificate.getEntrant().getEnrollmentCampaign());
        List<StateExamSubject> subjectList = dao.getCatalogItemList(StateExamSubject.class);
        Collections.sort(subjectList, CommonCatalogUtil.createCodeComparator("subjectCTMOCode"));

        for (int i = 0; i < subjectList.size(); i++) {
            StateExamSubject subject = subjectList.get(i);
            StateExamSubjectPassScoreExt score = scoreMap.get(subject);
            if (score != null) {
                Integer mark = markMap.get(subject.getId());
                Integer goodMark = (Integer) score.getProperty(getMarkProperty(certificate.getEntrant().getEnrollmentCampaign(), certYear));
                if (mark != null && mark < goodMark) {
                    String field_name = "stateExamResults_" + String.valueOf(i) + "_0";
                    errors.add("Балл по предмету не может быть меньше установленного зачетного балла (" + goodMark + ").", field_name);
                }
            }
        }
    }


    private static Map<StateExamSubject, StateExamSubjectPassScoreExt> getScoreMap(EnrollmentCampaign campaign) {
        Map<StateExamSubject, StateExamSubjectPassScoreExt> resultMap = new HashMap<>();

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(StateExamSubjectPassScore.class, "e")
                .joinEntity(
                        "e",
                        DQLJoinType.left,
                        StateExamSubjectPassScoreExt.class,
                        "ext",
                        DQLExpressions.eq(
                                DQLExpressions.property(StateExamSubjectPassScore.id().fromAlias("e")),
                                DQLExpressions.property(StateExamSubjectPassScoreExt.base().id().fromAlias("ext")))
                )
                .where(DQLExpressions.eqValue(DQLExpressions.property(StateExamSubjectPassScore.enrollmentCampaign().fromAlias("e")), campaign))
                .column("e")
                .column("ext");
        List<Object[]> list = IUniBaseDao.instance.get().getList(dql);

        for (Object[] arr : list) {
            StateExamSubjectPassScore score = (StateExamSubjectPassScore) arr[0];
            StateExamSubjectPassScoreExt scoreExt = (StateExamSubjectPassScoreExt) arr[1];
            if (scoreExt == null) {
                scoreExt = new StateExamSubjectPassScoreExt();
                scoreExt.setBase(score);
            }

            resultMap.put(score.getStateExamSubject(), scoreExt);
        }

        return resultMap;
    }

    private static String getMarkProperty(EnrollmentCampaign campaign, String certificateYear) {
        int certYear = 2000 + Integer.parseInt(certificateYear);
        int campYear = campaign.getEducationYear().getIntValue();
        int diff = campYear - certYear;

        if (diff == 0)
            return StateExamSubjectPassScoreExt.base().mark().s();
        else if (diff == 1)
            return StateExamSubjectPassScoreExt.markPrev().s();
        else if (diff == 2)
            return StateExamSubjectPassScoreExt.markPrev2().s();

        return StateExamSubjectPassScoreExt.base().mark().s();//TODO: считаем что год текущий
    }

    public static EntrantStateExamCertificateExt getEntrantStateExamCertificateExt(EntrantStateExamCertificate certificate)
    {
        EntrantStateExamCertificateExt examCertificateExt = IUniBaseDao.instance.get().get(EntrantStateExamCertificateExt.class, EntrantStateExamCertificateExt.entrantStateExamCertificate().s(), certificate);
        if (examCertificateExt == null) {
            examCertificateExt = new EntrantStateExamCertificateExt();
            examCertificateExt.setEntrantStateExamCertificate(certificate);
        }
        return examCertificateExt;
    }
}
