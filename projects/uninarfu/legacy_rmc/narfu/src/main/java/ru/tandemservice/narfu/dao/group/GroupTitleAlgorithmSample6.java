/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.narfu.dao.group;


import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.group.IAbstractGroupTitleDAO;
import ru.tandemservice.uni.entity.orgstruct.Group;

/**
 * TODO Add description here
 *
 * @author vip_delete
 */
public class GroupTitleAlgorithmSample6 extends UniBaseDao implements IAbstractGroupTitleDAO {
    /*
    * Формат - AAБВГГ
    * Пример - 700401
    * Описание:
    *   АА - индекс учебного структурного подразделения (Индекс учебного структурного подразделения соответствует внутреннему коду подразделения).
    *   B - Третья цифра – «1», «2», или «3» соответствует виду формы обучения:
	*       1 – для групп очной формы обучения;
	*       2 – для очно-заочной (вечерней) формы обучения;
	*       3 – для заочной формы обучения. 
    *   C - Четвертая цифра соответствует последней цифре года поступления в вуз;
    *   DD - Пятая и шестая цифры соответствуют  порядковому номеру группы на курсе и форме обучения.
    */
    @Override
    public String getTitle(Group group, int number)
    {
        String AA = String.valueOf(group.getEducationOrgUnit().getFormativeOrgUnit().getDivisionCode());
        if (AA.trim().isEmpty() || !AA.matches("\\A\\d\\d\\Z")) {
            AA = "00";
        }


        String B = group.getEducationOrgUnit().getDevelopForm().getCode();
        String C = String.valueOf(group.getStartEducationYear().getIntValue());
        C = C.substring(C.length() - 1);
        String DD = String.valueOf(number);
        if (number < 10) {
            DD = "0" + DD;
        }
        else {
            DD = DD.substring(DD.length() - 2);
        }
        StringBuffer buffer = new StringBuffer();
        buffer.append(AA);
        buffer.append(B);
        buffer.append(C);
        buffer.append(DD);
        return buffer.toString();
    }

    @Override
    public int getMaxNumber()
    {
        return 10;
    }
}
