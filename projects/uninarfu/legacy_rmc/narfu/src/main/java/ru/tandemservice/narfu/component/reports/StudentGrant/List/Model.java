package ru.tandemservice.narfu.component.reports.StudentGrant.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.narfu.entity.NarfuReportStudentGrantCharging;

public class Model extends ru.tandemservice.narfu.component.reports.StudentGrant.Add.Model
{

    private IDataSettings settings;

    private DynamicListDataSource<NarfuReportStudentGrantCharging> dataSource;

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public DynamicListDataSource<NarfuReportStudentGrantCharging> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<NarfuReportStudentGrantCharging> dataSource) {
        this.dataSource = dataSource;
    }

}
