package ru.tandemservice.narfu.component.reports.JournalAcademVacations.Add;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.movestudentrmc.entity.DocOrdRepresent;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.RelRepresentationReasonBasic;
import ru.tandemservice.movestudentrmc.entity.RepresentWeekend;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentationTypeCodes;
import ru.tandemservice.narfu.component.reports.JournalAcademVacations.ReportGenerator;
import ru.tandemservice.narfu.entity.NarfuReportJournalAcademVacations;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;

import java.util.HashMap;
import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(final Model model) {
        final OrgUnit orgUnit = model.getOrgUnitId() != null ? getNotNull(OrgUnit.class, model.getOrgUnitId()) : null;
        model.setOrgUnit(orgUnit);

        if (orgUnit != null)
            model.setSecModel(new OrgUnitSecModel(orgUnit));
        else
            model.setOrgUnitModel(new UniQueryFullCheckSelectModel(new String[]{OrgUnit.title().s()}) {
                @Override
                protected MQBuilder query(String alias, String filter) {
                    MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, alias)
                            .add(MQExpression.eq(alias, OrgUnit.orgUnitType().code(), "institute"))
                            .addOrder(alias, OrgUnit.title());

                    if (!StringUtils.isEmpty(filter))
                        builder.add(MQExpression.like(alias, OrgUnit.title(), filter));

                    return builder;
                }
            });
        model.setDevelopFormModel(new UniQueryFullCheckSelectModel(new String[]{DevelopForm.title().s()}) {
            @Override
            protected MQBuilder query(String alias, String filter) {
                MQBuilder builder = new MQBuilder(DevelopForm.ENTITY_CLASS, alias)
                        .addOrder(alias, DevelopForm.title());

                if (!StringUtils.isEmpty(filter))
                    builder.add(MQExpression.like(alias, DevelopForm.title(), filter));

                return builder;
            }
        });

        model.setEducationLevelsModel(new UniQueryFullCheckSelectModel(new String[]{EducationLevels.fullTitleWithRootLevel().s()}) {
            @Override
            protected MQBuilder query(String alias, String filter) {
                MQBuilder subBuilder = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "eou");
                if (model.isOrgUnitActive() && model.getOrgUnitList() != null)
                    subBuilder.add(MQExpression.in("eou", EducationOrgUnit.formativeOrgUnit(), model.getOrgUnitList()));
                else if (orgUnit != null)
                    subBuilder.add(MQExpression.eq("eou", EducationOrgUnit.formativeOrgUnit(), orgUnit));
                subBuilder.getSelectAliasList().clear();
                subBuilder.addSelect(EducationOrgUnit.educationLevelHighSchool().educationLevel().id().fromAlias("eou").s());

                MQBuilder builder = new MQBuilder(EducationLevels.ENTITY_CLASS, alias)
                        .add(MQExpression.in(alias, EducationLevels.id(), subBuilder))
                        .addOrder(alias, EducationLevels.title());

                if (!StringUtils.isEmpty(filter))
                    builder.add(MQExpression.or(
                            MQExpression.like(alias, EducationLevels.title(), filter),
                            MQExpression.like(alias, EducationLevels.inheritedOkso(), filter)
                    ));

                return builder;
            }
        });

        model.setCourseModel(new UniQueryFullCheckSelectModel(new String[]{Course.title().s()}) {
            @Override
            protected MQBuilder query(String alias, String filter) {
                return new MQBuilder(Course.ENTITY_CLASS, alias)
                        .addOrder(alias, Course.intValue());
            }
        });

        model.setCompensationTypeModel(new UniQueryFullCheckSelectModel(new String[]{CompensationType.shortTitle().s()}) {
            @Override
            protected MQBuilder query(String alias, String filter) {
                return new MQBuilder(CompensationType.ENTITY_CLASS, alias)
                        .addOrder(alias, CompensationType.shortTitle());
            }
        });

        model.setReasonModel(new UniQueryFullCheckSelectModel(new String[]{RepresentationReason.title().s()}) {
            @Override
            protected MQBuilder query(String alias, String filter) {
                MQBuilder subBuilder = new MQBuilder(RelRepresentationReasonBasic.ENTITY_CLASS, "rrb")
                        .add(MQExpression.eq("rrb", RelRepresentationReasonBasic.type().code(), RepresentationTypeCodes.WEEKEND));
                subBuilder.getSelectAliasList().clear();
                subBuilder.addSelect(RelRepresentationReasonBasic.reason().id().fromAlias("rrb").s());
                MQBuilder builder = new MQBuilder(RepresentationReason.ENTITY_CLASS, alias)
                        .add(MQExpression.in(alias, RepresentationReason.id(), subBuilder))
                        .addOrder(alias, RepresentationReason.title());

                if (!StringUtils.isEmpty(filter))
                    builder.add(MQExpression.like(alias, RepresentationReason.title(), filter));
                return builder;
            }
        });

        model.setDevelopPeriodModel(new UniQueryFullCheckSelectModel(new String[]{DevelopPeriod.title().s()}) {
            @Override
            protected MQBuilder query(String alias, String filter) {
                return new MQBuilder(DevelopPeriod.ENTITY_CLASS, alias)
                        .addOrder(alias, DevelopPeriod.title());
            }
        });

    }

    @Override
    public void createReport(Model model) {
        String shortTitleOrgUnit = "";
        //формируем имя отчета
        if (model.getOrgUnit() != null)
            shortTitleOrgUnit = model.getOrgUnit().getShortTitle();
        else if (model.getOrgUnitList() != null) {
            for (OrgUnit orgUnit : model.getOrgUnitList()) {
                shortTitleOrgUnit += ", " + orgUnit.getShortTitle();
            }
            shortTitleOrgUnit.replaceFirst(", ", "");
        }
        else shortTitleOrgUnit = TopOrgUnit.getInstance().getShortTitle();
        model.setReport(get(NarfuReportJournalAcademVacations.class, NarfuReportJournalAcademVacations.id(), ReportGenerator.createReport((model.getOrgUnit() != null) ? model.getOrgUnit() : null, shortTitleOrgUnit, getStudentMap(model))));
    }

    public HashMap<Student, DocOrdRepresent> getStudentMap(Model model) {
        MQBuilder builder = new MQBuilder(Student.ENTITY_CLASS, "s")
                .add(MQExpression.in("s", Student.status().code(), "5", "6", "7", "20", "21")) //выбираем студентов со статусами "отп.акад.б.посещ", "отп.б.посещ", "отп.по.бер.и.род", "отп.по.ух.за.реб.", "отп.с.посещ"
                .add(MQExpression.eq("s", Student.archival(), Boolean.FALSE))
                .addOrder("s", Student.person().identityCard().fullFio());

        if (model.getOrgUnit() != null)
            builder.add(MQExpression.eq("s", Student.educationOrgUnit().formativeOrgUnit(), model.getOrgUnit()));
        if (model.isOrgUnitActive() && model.getOrgUnitList() != null)
            builder.add(MQExpression.in("s", Student.educationOrgUnit().formativeOrgUnit(), model.getOrgUnitList()));
        if (model.isDevelopFormActive() && model.getDevelopFormList() != null)
            builder.add(MQExpression.in("s", Student.educationOrgUnit().developForm(), model.getDevelopFormList()));
        if (model.isEducationLevelsActive() && model.getEducationLevelsList() != null)
            builder.add(MQExpression.in("s", Student.educationOrgUnit().educationLevelHighSchool().educationLevel(), model.getEducationLevelsList()));
        if (model.isCourseActive() && model.getCourseList() != null)
            builder.add(MQExpression.in("s", Student.course(), model.getCourseList()));
        if (model.isDevelopPeriodActive() && model.getDevelopPeriodList() != null)
            builder.add(MQExpression.in("s", Student.educationOrgUnit().developPeriod(), model.getDevelopPeriodList()));

        if ((model.isReasonActive() && model.getReasonList() != null)) {
            //выбираем студентов, удовлетворяющих фильтру Ппичина АО
            MQBuilder subBuilderRepresentation = new MQBuilder(DocOrdRepresent.ENTITY_CLASS, "dor")
                    .add(MQExpression.eq("dor", DocOrdRepresent.order().state().code(), "5"))//проведенный приказ
                    .add(MQExpression.eq("dor", DocOrdRepresent.representation().type().code(), RepresentationTypeCodes.WEEKEND))//приказ о предоставлении АО
                    .add(MQExpression.in("dor", DocOrdRepresent.representation().reason(), model.getReasonList()));
            subBuilderRepresentation.getSelectAliasList().clear();
            subBuilderRepresentation.addSelect(DocOrdRepresent.representation().id().fromAlias("dor").s());

            MQBuilder build = new MQBuilder(DocRepresentStudentBase.ENTITY_CLASS, "drs")
                    .add(MQExpression.in("drs", DocRepresentStudentBase.representation().id(), subBuilderRepresentation));
            build.getSelectAliasList().clear();
            build.addSelect(DocRepresentStudentBase.student().id().fromAlias("drs").s());

            builder.add(MQExpression.in("s", Student.id(), build));
        }

        HashMap<Student, DocOrdRepresent> map = new HashMap<Student, DocOrdRepresent>();
        List<Student> studentList = getList(builder);
        for (Student student : studentList) {
            boolean addid = true;
            //берем у студента сущнотсь связь приказ-представление
            DQLSelectBuilder selectBuilder = new DQLSelectBuilder()
                    .fromEntity(DocOrdRepresent.class, "dor")
                    .addColumn("dor")
                    .joinEntity("dor", DQLJoinType.left, DocRepresentStudentBase.class, "drs", DQLExpressions.eq(DQLExpressions.property(DocRepresentStudentBase.representation().fromAlias("drs")), DQLExpressions.property(DocOrdRepresent.representation().fromAlias("dor"))))
                    .where(DQLExpressions.eq(DQLExpressions.property(DocRepresentStudentBase.student().fromAlias("drs")), DQLExpressions.value(student)))
                    .where(DQLExpressions.eq(DQLExpressions.property(DocOrdRepresent.order().state().code().fromAlias("dor")), DQLExpressions.value("5")))
                    .where(DQLExpressions.eq(DQLExpressions.property(DocOrdRepresent.representation().type().code().fromAlias("dor")), DQLExpressions.value(RepresentationTypeCodes.WEEKEND)))
                    .order(DQLExpressions.property(DocOrdRepresent.order().commitDate().fromAlias("dor")), OrderDirection.desc);

            List<DocOrdRepresent> list = getList(selectBuilder);
            DocOrdRepresent orderRepresent;
            if (!list.isEmpty()) {
                orderRepresent = list.get(0);//берем самый поздний по дате приказ
                if (model.isStartActive() && model.getStartDateFrom() != null) {
                    if (((RepresentWeekend) orderRepresent.getRepresentation()).getStartDate().getTime() < model.getStartDateFrom().getTime())
                        addid = false;
                }
                if (model.isStartActive() && model.getStartDateTo() != null) {
                    if (((RepresentWeekend) orderRepresent.getRepresentation()).getStartDate().getTime() > model.getStartDateTo().getTime())
                        addid = false;
                }
                if (model.isEndActive() && model.getEndDateFrom() != null) {
                    if (((RepresentWeekend) orderRepresent.getRepresentation()).getEndDate().getTime() < model.getEndDateFrom().getTime())
                        addid = false;
                }
                if (model.isEndActive() && model.getEndDateTo() != null) {
                    if (((RepresentWeekend) orderRepresent.getRepresentation()).getEndDate().getTime() > model.getEndDateTo().getTime())
                        addid = false;
                }
                if (addid)
                    map.put(student, orderRepresent);
            }
            else {
                orderRepresent = null;
                if ((model.isStartActive() && model.getStartDateFrom() == null && model.getStartDateTo() == null) ||
                        ((!model.isStartActive()) && (!model.isEndActive())) ||
                        (model.isEndActive() && model.getEndDateFrom() == null && model.getEndDateTo() == null)
                        )
                    map.put(student, orderRepresent);
            }
        }

        return map;
    }


}
