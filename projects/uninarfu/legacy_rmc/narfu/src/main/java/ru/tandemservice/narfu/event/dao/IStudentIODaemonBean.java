package ru.tandemservice.narfu.event.dao;

import groovy.transform.Synchronized;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.transaction.sync.RuntimeTimeoutException;

public interface IStudentIODaemonBean
{
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, noRollbackFor = {RuntimeTimeoutException.class})
    @Synchronized
    void updateArchivalIdentityCard(DSetEvent event);

    SpringBeanCache<IStudentIODaemonBean> instance = new SpringBeanCache<>(IStudentIODaemonBean.class.getName());
}
