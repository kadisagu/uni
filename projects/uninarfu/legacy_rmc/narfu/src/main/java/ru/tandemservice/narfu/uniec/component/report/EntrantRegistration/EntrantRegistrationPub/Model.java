package ru.tandemservice.narfu.uniec.component.report.EntrantRegistration.EntrantRegistrationPub;

import org.tandemframework.core.component.State;
import ru.tandemservice.narfu.uniec.entity.report.NarfuEntrantRegistration;

@State(keys = {"publisherId"}, bindings = {"report.id"})
public class Model {
    private NarfuEntrantRegistration _report = new NarfuEntrantRegistration();

    public NarfuEntrantRegistration getReport()
    {
        return this._report;
    }

    public void setReport(NarfuEntrantRegistration report)
    {
        this._report = report;
    }
}