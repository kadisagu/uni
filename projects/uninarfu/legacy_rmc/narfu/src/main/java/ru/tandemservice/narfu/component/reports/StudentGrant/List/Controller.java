package ru.tandemservice.narfu.component.reports.StudentGrant.List;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.narfu.entity.NarfuReportStudentGrantCharging;
import ru.tandemservice.unibase.UniBaseUtils;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = component.getModel();

        model.setSettings(component.getSettings());
        ((IDAO) getDao()).prepare(model);

        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component) {
        Model model = component.getModel();
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<NarfuReportStudentGrantCharging> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Дата формирования отчета", NarfuReportStudentGrantCharging.formingDate().s(), DateFormatter.DATE_FORMATTER_WITH_TIME));
        dataSource.addColumn(new SimpleColumn("Учебный год", NarfuReportStudentGrantCharging.educationYear().s()));
        dataSource.addColumn(new SimpleColumn("Часть года", NarfuReportStudentGrantCharging.educationYearPart().s()));
        dataSource.addColumn(new SimpleColumn("Месяц выплаты", NarfuReportStudentGrantCharging.month().s()));
        dataSource.addColumn(new SimpleColumn("Исполнитель", NarfuReportStudentGrantCharging.executor().s()));
        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickPrintReport", "Печатать"));
        dataSource.addColumn(
                new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteReport", "Удалить отчет от {0}?", new Object[]{"formingDate"})
                        .setPermissionKey("deleteStudentGrantCharging")
                        .setOrderable(false)
        );

        model.setDataSource(dataSource);
    }

    public void onClickAddReport(IBusinessComponent component) {
        ContextLocal.createDesktop(
                "PersonShellDialog",
                new ComponentActivator("ru.tandemservice.narfu.component.reports.StudentGrant.Add")
        );
    }

    public void onClickPrintReport(IBusinessComponent component) throws Exception {
        activateInRoot(
                component,
                new ComponentActivator(
                        "ru.tandemservice.uni.component.reports.DownloadStorableReport",
                        new ParametersMap().add("reportId", component.getListenerParameter()).add("extension", "xls")
                )
        );
    }

    public void onClickDeleteReport(IBusinessComponent component) {
        getDao().deleteRow(component);
    }

    public void onClickSearch(IBusinessComponent component) {
        component.saveSettings();
    }

    public void onClickClear(IBusinessComponent component) {
        Model model = component.getModel();
        model.getSettings().clear();
        onClickSearch(component);
    }

}
