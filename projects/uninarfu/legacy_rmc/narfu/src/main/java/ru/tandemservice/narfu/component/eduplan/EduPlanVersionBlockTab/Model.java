package ru.tandemservice.narfu.component.eduplan.EduPlanVersionBlockTab;

import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

import java.util.Map;

@State({@org.tandemframework.core.component.Bind(key = "publisherId", binding = "blockHolder.id", required = true)})
public class Model {
    private final EntityHolder<EppEduPlanVersionBlock> blockHolder = new EntityHolder<>();
    private String selectedTab;
    private Map<String, Object> parameters;

    public EppEduPlanVersion getEduplanVersion()
    {
        return (EppEduPlanVersion) getBlockHolder().getValue().getEduPlanVersion();
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, Object> parameters) {
        this.parameters = parameters;
    }

    public String getSelectedTab() {
        return selectedTab;
    }


    public void setSelectedTab(String selectedTab) {
        this.selectedTab = selectedTab;
    }

    public EntityHolder<EppEduPlanVersionBlock> getBlockHolder() {
        return this.blockHolder;
    }

}
