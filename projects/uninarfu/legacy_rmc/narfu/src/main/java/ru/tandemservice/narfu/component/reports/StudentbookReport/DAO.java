package ru.tandemservice.narfu.component.reports.StudentbookReport;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;

import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    public void prepare(final Model model) {
        final OrgUnit orgUnit = model.getOrgUnitId() != null ? getNotNull(OrgUnit.class, model.getOrgUnitId()) : null;
        model.setOrgUnit(orgUnit);

        if (orgUnit != null)
            model.setSecModel(new OrgUnitSecModel(orgUnit));

        model.setCourseModel(new UniQueryFullCheckSelectModel(new String[]{Course.title().s()}) {
            @Override
            protected MQBuilder query(String alias, String filter) {
                return new MQBuilder(Course.ENTITY_CLASS, alias)
                        .addOrder(alias, Course.intValue());
            }
        });

        model.setGroupModel(new UniQueryFullCheckSelectModel(new String[]{"fullTitle"}) {
            @Override
            protected MQBuilder query(String alias, String filter) {
                MQBuilder builder = new MQBuilder(Group.ENTITY_CLASS, alias)
                        .add(MQExpression.eq(alias, Group.educationOrgUnit().formativeOrgUnit(), orgUnit))
                        .addOrder(alias, Group.title());

                if (!StringUtils.isEmpty(filter))
                    builder.add(MQExpression.like(alias, Group.title(), filter));

                return builder;
            }
        });

        model.setDevelopFormModel(new UniQueryFullCheckSelectModel(new String[]{DevelopForm.title().s()}) {
            @Override
            protected MQBuilder query(String alias, String filter) {
                MQBuilder builder = new MQBuilder(DevelopForm.ENTITY_CLASS, alias)
                        .addOrder(alias, DevelopForm.title());

                if (!StringUtils.isEmpty(filter))
                    builder.add(MQExpression.like(alias, DevelopForm.title(), filter));

                return builder;
            }
        });

        model.setEducationLevelsModel(new UniQueryFullCheckSelectModel(new String[]{EducationLevels.fullTitleWithRootLevel().s()}) {
            @Override
            protected MQBuilder query(String alias, String filter) {
                MQBuilder subBuilder = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "eou");
                if (orgUnit != null)
                    subBuilder.add(MQExpression.eq("eou", EducationOrgUnit.formativeOrgUnit(), orgUnit));
                subBuilder.getSelectAliasList().clear();
                subBuilder.addSelect(EducationOrgUnit.educationLevelHighSchool().educationLevel().id().fromAlias("eou").s());

                MQBuilder builder = new MQBuilder(EducationLevels.ENTITY_CLASS, alias)
                        .add(MQExpression.in(alias, EducationLevels.id(), subBuilder))
                        .addOrder(alias, EducationLevels.title());

                if (!StringUtils.isEmpty(filter))
                    builder.add(MQExpression.or(
                            MQExpression.like(alias, EducationLevels.title(), filter),
                            MQExpression.like(alias, EducationLevels.inheritedOkso(), filter)
                    ));

                return builder;
            }
        });

        model.setStudentModel(new UniQueryFullCheckSelectModel(new String[]{Student.person().fullFio().s()}) {
            @Override
            protected MQBuilder query(String alias, String filter) {
                this.setPageSize(0);

                MQBuilder builder = new MQBuilder(Student.ENTITY_CLASS, alias)
                        .addJoinFetch(alias, Student.person().identityCard(), "ic")
                        .addJoinFetch(alias, Student.educationOrgUnit().developForm(), "df")
                        .addJoinFetch(alias, Student.educationOrgUnit().educationLevelHighSchool().educationLevel(), "el")
                        .addJoinFetch(alias, Student.course(), "co")
                        .add(MQExpression.eq(alias, Student.archival(), Boolean.FALSE))
                        .addOrder(alias, Student.person().identityCard().fullFio());

                if (orgUnit != null)
                    builder.add(MQExpression.eq(alias, Student.educationOrgUnit().formativeOrgUnit(), orgUnit));

                applyFilters(builder, alias, model);

                if (!StringUtils.isEmpty(filter))
                    builder.add(MQExpression.like(alias, Student.person().identityCard().fullFio(), filter));
                return builder;
            }
        });
    }

    private void applyFilters(MQBuilder builder, String alias, Model model) {
        if (model.isDevelopFormActive() && model.getDevelopFormList() != null)
            builder.add(MQExpression.in(alias, Student.educationOrgUnit().developForm(), model.getDevelopFormList()));
        if (model.isCourseActive() && model.getCourseList() != null)
            builder.add(MQExpression.in(alias, Student.course(), model.getCourseList()));
        if (model.isGroupActive() && model.getGroupList() != null)
            builder.add(MQExpression.in(alias, Student.group(), model.getGroupList()));
        if (model.isEducationLevelsActive() && model.getEducationLevelsList() != null)
            builder.add(MQExpression.in(alias, Student.educationOrgUnit().educationLevelHighSchool().educationLevel(), model.getEducationLevelsList()));
    }

    @Override
    public RtfDocument createReport(Model model) {
        List<Student> studentList = null;
        if (model.isStudentActive() && model.getStudentList() != null)
            studentList = model.getStudentList();
        else
            studentList = model.getStudentModel().findValues("").getObjects();

        return new ReportGenerator(studentList, model.isShowDate() ? model.getDate() : null).generate();
    }

}
