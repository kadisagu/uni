package ru.tandemservice.narfu.base.ext.Person.ui.BenefitAddEdit;

import org.apache.commons.lang.StringUtils;
import ru.tandemservice.narfu.base.ext.Person.ui.Tab.PersonNarfuUtil;
import ru.tandemservice.narfu.base.ext.Person.util.CauseDisabilitySelectModel;

public class DAO extends org.tandemframework.shared.person.base.bo.Person.ui.BenefitAddEdit.DAO {
    @Override
    public void prepare(org.tandemframework.shared.person.base.bo.Person.ui.BenefitAddEdit.Model model) {
        super.prepare(model);

        Model myModel = (Model) model;

        myModel.setCauseDisabilityModel(new CauseDisabilitySelectModel(getSession()));
        myModel.setPersonBenefitExt(PersonNarfuUtil.getPersonBenefitExt(model.getPersonBenefit()));
    }

    @Override
    public void update(
            org.tandemframework.shared.person.base.bo.Person.ui.BenefitAddEdit.Model model)
    {
        super.update(model);

        Model myModel = (Model) model;

        if (myModel.isVisibleCauseDisablility() && !StringUtils.isEmpty(myModel.getPersonBenefitExt().getCauseDisability())) {
            getSession().saveOrUpdate(myModel.getPersonBenefitExt());
        }
    }

	/*@Override
	public void update(Model model) 
	{
		//<TODO> полностью убрать расширение этого компонента при переходе на новую весрию
		// 2.0.4 uni r2 (VCH)
		
		Long personId = null;

		// затычка вместо биндинга параметров
		if (model.getPersonId()!=null)
			personId = model.getPersonId();
		if (personId==null)
			personId = model.getPersonBenefit().getPerson().getId();
		
			
			
		  MQBuilder builder = new MQBuilder("org.tandemframework.shared.person.base.entity.PersonBenefit", "e");
	        builder.add(MQExpression.eq("e", "benefit", model.getPersonBenefit().getBenefit()));
	        builder.add(MQExpression.eq("e", "person", get(Person.class, personId)));
	        if(model.getPersonBenefit().getId() != null)
	            builder.add(MQExpression.notEq("e", "id", model.getPersonBenefit().getId()));
	        if(builder.getResultCount(getSession()) > 0L)
	        {
	            ContextLocal.getErrorCollector().add("\u0422\u0430\u043A\u0430\u044F \u043B\u044C\u0433\u043E\u0442\u0430 \u0443\u0436\u0435 \u0434\u043E\u0431\u0430\u0432\u043B\u0435\u043D\u0430.", new String[] {
	                "personBenefit"
	            });
	            return;
	        }
	        PersonBenefit personBenefit = model.getPersonBenefit();
	        if(personBenefit.getId() == null)
	            personBenefit.setPerson((Person)get(Person.class, model.getPersonId()));
	        getSession().saveOrUpdate(personBenefit);
	        
	}*/
}
