package ru.tandemservice.narfu.base.ext.EcDistribution.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uniec.base.bo.EcDistribution.EcDistributionManager;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgDistribution;
import ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.ws.distribution.EnrollmentDistributionEnvironmentNode;
import ru.tandemservice.uniec.ws.distribution.EnrollmentDistributionEnvironmentNode.DistributionNode.DistributionRow;
import ru.tandemservice.uniec.ws.distribution.IEnrollmentDistributionServiceDao;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class EcDistributionPrintWarning
        extends CommonDAO
        implements IEcDistributionPrintWarning
{


    @Override
    public RtfDocument getPrintForm(byte[] template,
                                    IEcgDistribution distribution)
    {

        List<String[]> tableRowList = new ArrayList<String[]>();

        EnrollmentDistributionEnvironmentNode env = ((IEnrollmentDistributionServiceDao) IEnrollmentDistributionServiceDao.INSTANCE.get()).getEnrollmentDistributionEnvironmentNode(Collections.singleton(distribution));
        EnrollmentDistributionEnvironmentNode.DistributionNode.DistributionRow distributionRow = (EnrollmentDistributionEnvironmentNode.DistributionNode.DistributionRow) env.distribution.row.get(0);

        Map targetAdmissionKindMap = new HashMap();
        Map competitionKindMap = new HashMap();
        Map benefitMap = new HashMap();
        Map documentStateMap = new HashMap();
        Map enrollmentStateMap = new HashMap();

        Map directionMap = new HashMap();
        for (EnrollmentDirection direction : EcDistributionManager.instance().dao().getDistributionDirectionList(distribution.getDistribution())) {
            directionMap.put(Long.toString(direction.getId().longValue()), direction);
        }

        fillTitleMap(env, targetAdmissionKindMap, competitionKindMap, benefitMap, documentStateMap, enrollmentStateMap);
        Set unionRowIndexList = new HashSet();

        try {
            _fillTable(distributionRow, tableRowList, unionRowIndexList, targetAdmissionKindMap, competitionKindMap, benefitMap, documentStateMap, enrollmentStateMap, directionMap);
        }
        catch (Exception e) {
            tableRowList = new ArrayList<String[]>();
        }

        RtfDocument document = (new RtfReader()).read(template).getClone();
        int grayColorIndex = document.getHeader().getColorTable().addColor(217, 217, 217);

        RtfInjectModifier im = new RtfInjectModifier();
        im.modify(document);

        fillTableData(document, grayColorIndex, 3, tableRowList, unionRowIndexList, distributionRow);

        return document;
    }

    protected void fillTableData(RtfDocument document, final int grayColorIndex,
                                 final int disciplineColumn, List<String[]> tableData,
                                 final Set<Integer> unionRowIndexList, final DistributionRow distributionRow)
    {

        RtfTableModifier tm = new RtfTableModifier();

        tm.put("T", (String[][]) tableData.toArray(new String[tableData.size()][]));
        tm.put("T", new RtfRowIntercepterBase() {
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                if (unionRowIndexList.contains(Integer.valueOf(rowIndex)))
                    return new RtfString().boldBegin().content(value).boldEnd().toList();
                return null;
            }

            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                for (Integer rowIndex : unionRowIndexList) {
                    RtfRow row = (RtfRow) newRowList.get(rowIndex.intValue() + startIndex);
                    RtfUtil.unitAllCell(row, 1);
                    ((RtfCell) row.getCellList().get(0)).setBackgroundColorIndex(grayColorIndex);
                }
            }
        });
        tm.modify(document);
    }


    private void fillTitleMap(EnrollmentDistributionEnvironmentNode env, Map<String, EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow> targetAdmissionKindMap, Map<String, EnrollmentDistributionEnvironmentNode.CompetitionKindNode.CompetitionKindRow> competitionKindMap, Map<String, EnrollmentDistributionEnvironmentNode.BenefitNode.BenefitRow> benefitMap, Map<String, EnrollmentDistributionEnvironmentNode.Row> documentStateMap, Map<String, EnrollmentDistributionEnvironmentNode.Row> enrollmentStateMap)
    {
        for (EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow row : env.targetAdmissionKind.row) {
            targetAdmissionKindMap.put(row.id, row);
        }
        for (EnrollmentDistributionEnvironmentNode.CompetitionKindNode.CompetitionKindRow row : env.competitionKind.row) {
            competitionKindMap.put(row.id, row);
        }
        for (EnrollmentDistributionEnvironmentNode.BenefitNode.BenefitRow row : env.benefit.row) {
            benefitMap.put(row.id, row);
        }
        for (EnrollmentDistributionEnvironmentNode.Row row : env.documentState.row) {
            documentStateMap.put(row.id, row);
        }
        for (EnrollmentDistributionEnvironmentNode.Row row : env.enrollmentState.row)
            enrollmentStateMap.put(row.id, row);
    }


    private List<RequestedEnrollmentDirection> getRequestDirections
            (
                    String number
            ) throws Exception
    {

        List<RequestedEnrollmentDirection> retVal = new ArrayList<RequestedEnrollmentDirection>();
        try {

            DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(RequestedEnrollmentDirection.class, "rd")
                    .addColumn("rd")
                    .where(eq(property(RequestedEnrollmentDirection.entrantRequest().regNumber().fromAlias("rd")), value(number)))

                            // возмещение затрат - только бюджет
                    .where(eq(property(RequestedEnrollmentDirection.compensationType().code().fromAlias("rd")), value(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET)))

                    .order(property(RequestedEnrollmentDirection.priority().fromAlias("rd")));
            List<RequestedEnrollmentDirection> lst = dql.createStatement(getSession()).list();

            for (RequestedEnrollmentDirection r : lst) {
                retVal.add(r);
            }
        }
        catch (Exception e) {
            throw e;
        }
        return retVal;
    }


    private double getFinalMark(RequestedEnrollmentDirection rd) throws Exception
    {
        double retVal = 0D;

        try {
            DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(ChosenEntranceDiscipline.class, "r")
                    .addColumn("r")
                    .where(eq(property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("r")), value(rd.getId())));
            List<ChosenEntranceDiscipline> lst = dql.createStatement(getSession()).list();

            for (ChosenEntranceDiscipline ced : lst) {
                if (ced.getFinalMark() != null)
                    retVal += ced.getFinalMark();
            }
        }
        catch (Exception e) {
            throw e;
        }
        return retVal;
    }

    private void _fillTable(
            DistributionRow distributionRow
            , List<String[]> tableRowList
            , Set<Integer> unionRowIndexList
            , Map targetAdmissionKindMap
            , Map competitionKindMap
            , Map benefitMap
            , Map documentStateMap
            , Map enrollmentStateMap
            , Map directionMap) throws Exception
    {


        int rowIndex = 0;
        int number = 1;
        int currentRowKey = -1;

        boolean defaultTaKind = ((EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow) distributionRow.quota.row.get(0)).defaultTaKind;

        for (EnrollmentDistributionEnvironmentNode.EntrantNode.EntrantRow row : distributionRow.entrant.row) {

            // с таким абитуриентов все впорядке
            if (row.priorityIds.size() <= 1)
                continue;

            boolean hasSd = false;
            if (row.enrollmentDirection != null && row.enrollmentState != null)
                hasSd = true;


            int rowKey = row.targetAdmissionKind == null ? Integer.parseInt(row.competitionKind == null ? "5" : row.competitionKind) : 0;

            if (currentRowKey != rowKey) {
                currentRowKey = rowKey;

                unionRowIndexList.add(Integer.valueOf(rowIndex));

                String title = defaultTaKind ? "Целевой прием" : row.targetAdmissionKind == null ? ((EnrollmentDistributionEnvironmentNode.CompetitionKindNode.CompetitionKindRow) competitionKindMap.get(row.competitionKind == null ? "5" : row.competitionKind)).title : new StringBuilder().append("Целевой прием – ").append(((EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow) targetAdmissionKindMap.get(row.targetAdmissionKind)).shortTitle).toString();

                tableRowList.add(new String[]{null, title});

                rowIndex++;

                number = 1;
            }


            String prim = "";

            // смотрим выбранное направление подготовки
            // проверяем баллы
            List<RequestedEnrollmentDirection> lst = getRequestDirections(row.regNumber);

            double mark_1 = 0D; // балл по приоритетному направлению подготовки
            double mark_max = -100D; // максимальный балл
            double mark_min = 10000D; //минимальный балл

            int i = 0;

            Map<Long, Double> mapMark = new HashMap<Long, Double>();
            for (RequestedEnrollmentDirection rd : lst) {
                Long idEd = rd.getEnrollmentDirection().getId();
                double fm = getFinalMark(rd);
                mapMark.put(idEd, fm);

                if (i == 0)
                    mark_1 = fm;
                i++;

                if (fm > mark_max)
                    mark_max = fm;

                if (fm < mark_min)
                    mark_min = fm;

            }

            if (mark_1 == mark_max && mark_1 == mark_min)
                // все баллы одинаковы
                continue;


            if (mark_1 < mark_max)
                prim = " R<max ";

            if (mark_1 > mark_min) {
                prim += " R>min ";

                // Маша просила
                if (!hasSd)
                    continue;
            }

            String finalMark = row.finalMark;

            List<String> rowTitleList = new ArrayList<String>();
            rowTitleList.add(Integer.toString(number++));
            rowTitleList.add(row.requestNumber);
            rowTitleList.add(row.fio);
            rowTitleList.add(row.documentState == null ? null : ((EnrollmentDistributionEnvironmentNode.Row) documentStateMap.get(row.documentState)).title);

            List priorityList = new ArrayList();
            for (Long id : row.priorityIds) {
                String ed_info = printOKSO((EnrollmentDirection) directionMap.get(id.toString()));

                if (mapMark.containsKey(id))
                    ed_info += "(" + mapMark.get(id) + ")";

                priorityList.add(ed_info);
            }

            if (row.enrollmentDirection != null && row.enrollmentState != null) {
                // проверяем, что при зачислении нет колизии
                double markSD = mapMark.get(Long.parseLong(row.enrollmentDirection));

                // зачислен по баллам приоритетного направления
                if (markSD == mark_1)
                    continue;

                // если абитуриент зачислен на направление подготовки с баллом
                if (markSD < mark_1)
                    finalMark += " (1-ый " + mark_1 + ")";


                if (markSD == mark_min)
                    prim += " by min ";

                if (markSD == mark_max)
                    prim += " BY MAX ";

                if (markSD > mark_1) {
                    prim += " SD>R ";
                    // маша просила
                    continue;
                }

                if (markSD < mark_1)
                    prim += " SD<R ";
            }

            rowTitleList.add(finalMark);

			/*
			// цикл по оценкам
			if (row.marks != null)
			{
				int markIndex = 0;
				for (String mark : row.marks) 
				{
					markIndex++;
					System.out.println(markIndex + " " + mark );
				}
			}
			*/


            rowTitleList.add(StringUtils.join(priorityList, ", "));
            rowTitleList.add(row.enrollmentState == null ? null : (printOKSO((EnrollmentDirection) directionMap.get(row.enrollmentDirection))));

            rowTitleList.add(prim);

            tableRowList.add((String[]) rowTitleList.toArray(new String[rowTitleList.size()]));


            rowIndex++;

        }

    }

    public String printOKSO(EnrollmentDirection enrollmentDirection) {
        //Код ОКСО
        EducationLevels educationLevel = enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();

        //Два правых символа после точки
        String qualificationCode = enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getQualification().getCode();

        boolean isFullOKSO = "050100.62".equals(educationLevel.getInheritedOksoPrefix()) || "68".equals(qualificationCode);

        String result = isFullOKSO ? educationLevel.getDisplayableTitle() : educationLevel.getInheritedOksoPrefix();

        if (getOKSOprefixes().contains(educationLevel.getInheritedOksoPrefix())) {
            //Сокращенное название форм. подразделения
            String formativeOrgUnitShortTitle = enrollmentDirection.getEducationOrgUnit().getFormativeOrgUnit().getShortTitle();

            new StringBuilder().append(result).append(" (").append(formativeOrgUnitShortTitle).append(")");
        }

        return result;

    }

    public List<String> getOKSOprefixes() {
        return Arrays.asList("050100.62", "230700.62", "010400.62", "151000.62", "050400.62", "030900.62", "080200.62", "080100.62", "190631.51");
    }

}
