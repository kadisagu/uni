package ru.tandemservice.narfu.utils;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.util.RtfUtil;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class RtfToZipSupport {

    public static File writeToFile(RtfDocument document) {
        File file = null;

        try {
            file = File.createTempFile("" + (new Date().getTime()), null);
            FileOutputStream fout = new FileOutputStream(file);
            fout.write(RtfUtil.toByteArray(document));
            fout.close();
        }
        catch (Exception ex) {
            throw new ApplicationException(ex.getMessage(), ex);
        }

        return file;
    }


    /*
     * собрать архив из файлов
     */
    public static byte[] toZip(List<File> files) {
        return toZip(files, "rtf");
    }

    public static byte[] toZip(List<File> files, String ext) {
        byte[] data = null;
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ZipOutputStream zipOut = new ZipOutputStream(out);

            int counter = 0;
            for (File file : files) {
                byte[] fileData = new byte[(int) file.length()];
                FileInputStream fin = new FileInputStream(file);
                fin.read(fileData);
                fin.close();

                zipOut.putNextEntry(new ZipEntry("" + (++counter) + "." + ext));
                zipOut.write(fileData);
            }

            zipOut.close();
            data = out.toByteArray();
            out.close();
        }
        catch (Exception ex) {
            throw new ApplicationException(ex.getMessage(), ex);
        }

        return data;
    }
}
