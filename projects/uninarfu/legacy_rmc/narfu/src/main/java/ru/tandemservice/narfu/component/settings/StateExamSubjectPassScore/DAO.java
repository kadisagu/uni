package ru.tandemservice.narfu.component.settings.StateExamSubjectPassScore;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.narfu.entity.StateExamSubjectPassScoreExt;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uniec.component.settings.StateExamSubjectPassScore.Model.SubjectWrapper;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.settings.StateExamSubjectPassScore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DAO extends ru.tandemservice.uniec.component.settings.StateExamSubjectPassScore.DAO {

    @Override
    public void prepareListDataSource(ru.tandemservice.uniec.component.settings.StateExamSubjectPassScore.Model model) {
        super.prepareListDataSource(model);

        List<Model.SubjectWrapperExt> list = new ArrayList<Model.SubjectWrapperExt>();
        for (SubjectWrapper wrapper : model.getDataSource().getEntityList()) {
            StateExamSubjectPassScoreExt ext = getExt(model, wrapper.getSubject());
            Model.SubjectWrapperExt extWrapper = new Model.SubjectWrapperExt(
                    wrapper.getSubject(),
                    wrapper.getMark(),
                    ext != null ? ext.getMarkPrev() : 0,
                    ext != null ? ext.getMarkPrev2() : 0
            );

            list.add(extWrapper);
        }

        UniUtils.createPage(model.getDataSource(), (List) list);
    }

    @Override
    public void update(ru.tandemservice.uniec.component.settings.StateExamSubjectPassScore.Model model) {
        Map<StateExamSubject, StateExamSubjectPassScore> scoreMap = new HashMap<StateExamSubject, StateExamSubjectPassScore>();
        for (StateExamSubjectPassScore score : getList(StateExamSubjectPassScore.class, "enrollmentCampaign", model.getEnrollmentCampaign(), new String[0]))
            scoreMap.put(score.getStateExamSubject(), score);

        for (Model.SubjectWrapper wrapper : model.getDataSource().getEntityList()) {
            Model.SubjectWrapperExt wrapperExt = (Model.SubjectWrapperExt) wrapper;
            StateExamSubjectPassScore existingScore = scoreMap.get(wrapper.getSubject());

            if (wrapperExt.isEmpty() && existingScore != null)
                getSession().delete(existingScore);

            if (wrapperExt.isEmpty())
                continue;

            //сохраним базовый
            if (existingScore == null) {
                existingScore = new StateExamSubjectPassScore();
                existingScore.setEnrollmentCampaign(model.getEnrollmentCampaign());
                existingScore.setStateExamSubject(wrapperExt.getSubject());
            }
            existingScore.setMark(wrapperExt.getMark() != null ? wrapperExt.getMark() : 0);
            getSession().saveOrUpdate(existingScore);

            //сохраним расширение
            StateExamSubjectPassScoreExt ext = getExt(model, wrapperExt.getSubject());
            if (ext == null) {
                ext = new StateExamSubjectPassScoreExt();
                ext.setBase(existingScore);
            }
            ext.setMarkPrev(wrapperExt.getMarkPrev() != null ? wrapperExt.getMarkPrev() : 0);
            ext.setMarkPrev2(wrapperExt.getMarkPrev2() != null ? wrapperExt.getMarkPrev2() : 0);
            getSession().saveOrUpdate(ext);
        }
    }

    protected StateExamSubjectPassScoreExt getExt(ru.tandemservice.uniec.component.settings.StateExamSubjectPassScore.Model model, StateExamSubject subject) {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(StateExamSubjectPassScoreExt.class, "ext")
                .where(DQLExpressions.eqValue(DQLExpressions.property(StateExamSubjectPassScoreExt.base().enrollmentCampaign().fromAlias("ext")), model.getEnrollmentCampaign()))
                .where(DQLExpressions.eqValue(DQLExpressions.property(StateExamSubjectPassScoreExt.base().stateExamSubject().fromAlias("ext")), subject));
        List<StateExamSubjectPassScoreExt> list = getList(dql);

        return !list.isEmpty() ? list.get(0) : null;
    }
}
