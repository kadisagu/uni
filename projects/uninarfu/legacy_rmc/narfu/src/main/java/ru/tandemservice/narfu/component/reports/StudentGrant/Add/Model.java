package ru.tandemservice.narfu.component.reports.StudentGrant.Add;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab.MonthWrapper;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.ui.EducationYearModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Model {

    private ISelectModel educationYearModel = new EducationYearModel();
    private EducationYear educationYear;

    private ISelectModel educationYearPartModel;
    private YearDistributionPart part;

    private Date month;

    public ISelectModel getEducationYearModel() {
        return educationYearModel;
    }

    public void setEducationYearModel(ISelectModel educationYearModel) {
        this.educationYearModel = educationYearModel;
    }

    public List<MonthWrapper> getMonthList() {
        List<MonthWrapper> list = new ArrayList<MonthWrapper>();
        for (int i = 0; i < 12; i++) {
            MonthWrapper monthWrapper = new MonthWrapper(i);
            list.add(monthWrapper);
        }
        return list;
    }

    public EducationYear getEducationYear() {
        return educationYear;
    }

    public void setEducationYear(EducationYear educationYear) {
        this.educationYear = educationYear;
    }

    public Date getMonth() {
        return month;
    }

    public void setMonth(Date month) {
        this.month = month;
    }

    public ISelectModel getEducationYearPartModel() {
        return educationYearPartModel;
    }

    public void setEducationYearPartModel(ISelectModel educationYearPartModel) {
        this.educationYearPartModel = educationYearPartModel;
    }

    public YearDistributionPart getPart() {
        return part;
    }

    public void setPart(YearDistributionPart part) {
        this.part = part;
    }


}
