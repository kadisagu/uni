/* $Id$ */
package ru.tandemservice.narfu.base.ext.EppRegistry.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.narfu.base.ext.EppRegistry.ui.Base.NarfuRegistryListAddon;
import ru.tandemservice.uniepp.base.bo.EppRegistry.logic.RegistryBaseDSHandler;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.exists;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Irina Ugfeld
 * @since 11.03.2016
 */
public class NarfuRegistryBaseDSHandler extends RegistryBaseDSHandler {

    public NarfuRegistryBaseDSHandler(String ownerId) {
        super(ownerId);
    }

    @Override
    protected void applyWhereConditions(final DQLSelectBuilder builder, final String alias, final DSInput input, final Class elementClass, final ExecutionContext context) {

        List eduPlanVersionList = context.get(NarfuRegistryListAddon.SETTING_EDU_PLAN_VERSION);
        if (eduPlanVersionList != null && !eduPlanVersionList.isEmpty()) {
            builder.where(exists(EppEpvRegistryRow.class,
                    EppEpvRegistryRow.registryElement().s(), property(alias),
                    EppEpvRegistryRow.owner().eduPlanVersion().s(), eduPlanVersionList));
        }

        List eppWorkPlanList = context.get(NarfuRegistryListAddon.SETTING_EPP_WORK_PLAN);
        if (eppWorkPlanList != null && !eppWorkPlanList.isEmpty()) {
            builder.where(exists(EppWorkPlanRegistryElementRow.class,
                    EppWorkPlanRegistryElementRow.registryElementPart().registryElement().s(), property(alias),
                    EppWorkPlanRegistryElementRow.workPlan().s(), eppWorkPlanList));
        }
    }
}