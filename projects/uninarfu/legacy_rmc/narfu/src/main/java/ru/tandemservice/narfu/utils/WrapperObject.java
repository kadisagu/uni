package ru.tandemservice.narfu.utils;

import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013Group;

/**
 * Created by dalopatin on 03.10.2014.
 */
public class WrapperObject {
    private EducationLevels levels;
    private EduProgramSubject2013Group eduProgramSubject2013Group;


    public WrapperObject(EducationLevels levels) {
        this.levels = levels;
    }

    public WrapperObject(EduProgramSubject2013Group eduProgramSubject2013Group) {
        this.eduProgramSubject2013Group = eduProgramSubject2013Group;
    }


    public EducationLevels getLevels() {
        return levels;
    }

    public void setLevels(EducationLevels levels) {
        this.levels = levels;
    }

    public EduProgramSubject2013Group getEduProgramSubject2013Group() {
        return eduProgramSubject2013Group;
    }

    public void setEduProgramSubject2013Group(EduProgramSubject2013Group eduProgramSubject2013Group) {
        this.eduProgramSubject2013Group = eduProgramSubject2013Group;
    }

    @Override
    public boolean equals(Object obj) {
        return hashCode() == obj.hashCode();
    }

    @Override
    public int hashCode() {
        if (levels != null) {
            return levels.hashCode();
        }

        return eduProgramSubject2013Group.hashCode();
    }


    public String getTitle() {
        if (levels != null) {
            return levels.getTitle();
        }
        return eduProgramSubject2013Group.getTitle();
    }

}
