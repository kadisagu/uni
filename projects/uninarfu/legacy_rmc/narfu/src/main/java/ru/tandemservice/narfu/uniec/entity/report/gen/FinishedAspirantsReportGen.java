package ru.tandemservice.narfu.uniec.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.narfu.uniec.entity.report.FinishedAspirantsReport;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.report.StorableReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выпуск аспирантов - граждан РФ, обучавшихся по прямым договорам с оплатой стоимости обучения физическими или юридическими лицами,  и защиты диссертаций на соискание ученой степени кандидата наук лицами этой категории, окончившими аспирантуру данного вуза без защиты диссертации, по отраслям науки и специальностямВыпуск аспирантов - граждан РФ, обучавшихся по прямым договорам с оплатой стоимости обучения физическими или юридическими лицами,  и защиты диссертаций на соискание ученой степени кандидата наук лицами этой категории, окончившими аспирантуру данного вуза без защиты диссертации, по отраслям науки и специальностям
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FinishedAspirantsReportGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.narfu.uniec.entity.report.FinishedAspirantsReport";
    public static final String ENTITY_NAME = "finishedAspirantsReport";
    public static final int VERSION_HASH = 764453383;
    private static IEntityMeta ENTITY_META;

    public static final String L_COMPENSATION_TYPE = "compensationType";

    private CompensationType _compensationType;     // Вид возмещения затрат

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FinishedAspirantsReportGen)
        {
            setCompensationType(((FinishedAspirantsReport)another).getCompensationType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FinishedAspirantsReportGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FinishedAspirantsReport.class;
        }

        public T newInstance()
        {
            return (T) new FinishedAspirantsReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "compensationType":
                    return obj.getCompensationType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "compensationType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "compensationType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "compensationType":
                    return CompensationType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FinishedAspirantsReport> _dslPath = new Path<FinishedAspirantsReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FinishedAspirantsReport");
    }
            

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.narfu.uniec.entity.report.FinishedAspirantsReport#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    public static class Path<E extends FinishedAspirantsReport> extends StorableReport.Path<E>
    {
        private CompensationType.Path<CompensationType> _compensationType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.narfu.uniec.entity.report.FinishedAspirantsReport#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

        public Class getEntityClass()
        {
            return FinishedAspirantsReport.class;
        }

        public String getEntityName()
        {
            return "finishedAspirantsReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
