package ru.tandemservice.narfu.uniec.component.report.ActPersonInfo.Pub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.shared.commonbase.base.util.UniMap;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    public Controller()
    {
    }

    public void onRefreshComponent(IBusinessComponent component)
    {
        ((IDAO) getDao()).prepare(getModel(component));
    }

    public void onClickPrintReport(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator("ru.tandemservice.uni.component.reports.DownloadStorableReport", (new UniMap()).add("reportId", ((Model) getModel(component)).getReport().getId()).add("extension", "rtf")));
    }
}
