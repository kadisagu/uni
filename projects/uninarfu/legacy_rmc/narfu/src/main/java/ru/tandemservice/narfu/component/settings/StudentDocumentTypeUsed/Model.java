package ru.tandemservice.narfu.component.settings.StudentDocumentTypeUsed;

import ru.tandemservice.uni.entity.catalog.StudentCategory;

import java.util.ArrayList;
import java.util.List;

public class Model extends ru.tandemservice.uni.component.settings.StudentDocumentTypeUsed.Model {

    private List<StudentCategory> studentCategoryList = new ArrayList<>();

    public List<StudentCategory> getStudentCategoryList() {
        return studentCategoryList;
    }

    public void setStudentCategoryList(List<StudentCategory> studentCategoryList) {
        this.studentCategoryList = studentCategoryList;
    }

}
