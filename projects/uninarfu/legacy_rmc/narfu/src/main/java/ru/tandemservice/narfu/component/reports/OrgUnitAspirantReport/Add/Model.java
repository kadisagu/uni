package ru.tandemservice.narfu.component.reports.OrgUnitAspirantReport.Add;

import org.tandemframework.core.component.State;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.narfu.uniec.entity.report.OrgUnitAspirantReport;

@State(keys = "orgUnitId", bindings = "orgUnitId")
public class Model {

    private OrgUnitAspirantReport report = new OrgUnitAspirantReport();

    private ISelectModel enrollmentCampaignModel;
    private ISelectModel educationLevelsHighSchoolModel;


    private Long orgUnitId;
    private OrgUnit orgUnit;


    public OrgUnitAspirantReport getReport() {
        return report;
    }

    public void setReport(OrgUnitAspirantReport report) {
        this.report = report;
    }

    public Long getOrgUnitId() {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId) {
        this.orgUnitId = orgUnitId;
    }

    public OrgUnit getOrgUnit() {
        return orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit) {
        this.orgUnit = orgUnit;
    }

    public ISelectModel getEnrollmentCampaignModel() {
        return enrollmentCampaignModel;
    }

    public void setEnrollmentCampaignModel(ISelectModel enrollmentCampaignModel) {
        this.enrollmentCampaignModel = enrollmentCampaignModel;
    }

    public ISelectModel getEducationLevelsHighSchoolModel() {
        return educationLevelsHighSchoolModel;
    }

    public void setEducationLevelsHighSchoolModel(ISelectModel educationLevelsHighSchoolModel) {
        this.educationLevelsHighSchoolModel = educationLevelsHighSchoolModel;
    }
}
