package ru.tandemservice.narfu.component.settings.FormingOrgUnit2Commission.Edit;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.eventssecrmc.entity.EnrollmentCampaignSecExt;
import ru.tandemservice.narfu.entity.FormingOrgUnit2Commission;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(final Model model) {
        FormingOrgUnit2Commission rel = getFormingOrgUnitRel(model.getEnrollmentCampaignId(), model.getFormingOrgUnitId());
        model.setFormingOrgUnit2Commission(rel);
        model.setCommissionSelectModel(new DQLFullCheckSelectModel() {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                EnrollmentCampaignSecExt ecExt = get(EnrollmentCampaignSecExt.class, EnrollmentCampaignSecExt.enrollmentCampaign().id(), model.getEnrollmentCampaignId());
                DQLSelectBuilder builder = new DQLSelectBuilder();
                builder.fromEntity(OrgUnit.class, alias);
                if (ecExt != null && ecExt.getOrgUnitType() != null) {
                    builder.where(DQLExpressions.eq(
                            DQLExpressions.property(OrgUnit.orgUnitType().fromAlias(alias)),
                            DQLExpressions.value(ecExt.getOrgUnitType())
                    ));
                }
                FilterUtils.applySimpleLikeFilter(builder, alias, OrgUnit.title(), filter);
                return builder;

            }
        });
    }

    @Override
    public void update(Model model) {
        saveOrUpdate(model.getFormingOrgUnit2Commission());
    }


    private FormingOrgUnit2Commission getFormingOrgUnitRel(Long enrollmentCampaignId, Long formingOrgUnitId) {
        EnrollmentCampaign campaign = get(EnrollmentCampaign.class, enrollmentCampaignId);
        OrgUnit unit = get(OrgUnit.class, formingOrgUnitId);
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(FormingOrgUnit2Commission.class, "f2c");
        dql.where(DQLExpressions.and(
                DQLExpressions.eq(
                        DQLExpressions.property(FormingOrgUnit2Commission.enrollmentCampaign().id().fromAlias("f2c"))
                        , DQLExpressions.value(enrollmentCampaignId)
                ),
                DQLExpressions.eq(
                        DQLExpressions.property(FormingOrgUnit2Commission.formingOrgUnit().id().fromAlias("f2c"))
                        , DQLExpressions.value(formingOrgUnitId)
                )
        ));
        List<FormingOrgUnit2Commission> rel = dql.createStatement(getSession()).list();
        if (rel == null || rel.size() == 0) {
            FormingOrgUnit2Commission newRel = new FormingOrgUnit2Commission();
            newRel.setEnrollmentCampaign(campaign);
            newRel.setFormingOrgUnit(unit);
            return newRel;
        }
        else
            return rel.get(0);
    }
}
