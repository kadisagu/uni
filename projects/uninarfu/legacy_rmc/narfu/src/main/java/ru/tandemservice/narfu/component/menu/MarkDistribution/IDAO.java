package ru.tandemservice.narfu.component.menu.MarkDistribution;

import java.util.concurrent.atomic.AtomicInteger;


public interface IDAO extends ru.tandemservice.uniecmarkextrmc.component.menu.MarkDistribution.IDAO
{
    public AtomicInteger getMaxCode();
}
