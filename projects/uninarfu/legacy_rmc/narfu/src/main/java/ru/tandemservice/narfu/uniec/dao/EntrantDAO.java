package ru.tandemservice.narfu.uniec.dao;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

//public class EntrantDAO extends ru.tandemservice.uniec.dao.EntrantDAO implements IEntrantDAO {
public class EntrantDAO extends ru.tandemservice.uniecrmc.dao.EntrantDAOExt implements IEntrantDAO {

    /**
     * Метод нужен для обработки приказов о зачислении на 2+ курсы
     * Не нужна проверка на наличие студента в других приказах, как это делается в паренте
     *
     * @param student
     *
     * @return
     */
    public synchronized CoreCollectionUtils.Pair<String, PreliminaryEnrollmentStudent> saveOrUpdatePreliminaryStudentExt(PreliminaryEnrollmentStudent student) {
        Session session = getSession();

        RequestedEnrollmentDirection direction = student.getRequestedEnrollmentDirection();
        session.lock(direction, LockMode.UPGRADE);
        session.refresh(direction);

        Entrant entrant = direction.getEntrantRequest().getEntrant();
        String fio = entrant.getPerson().getFullFio();

        if (entrant.getEnrollmentCampaign().isNeedOriginDocForPreliminary()) {
            if ((student.getStudentCategory().getCode().equals(UniDefines.STUDENT_CATEGORY_STUDENT)) && (!direction.isOriginalDocumentHandedIn())) {
                return new CoreCollectionUtils.Pair("Абитуриент «" + fio + "» не сдал оригиналы документов, поэтому нельзя провести предварительное зачисление абитуриента в число студентов.", null);
            }
            Criteria criteria = getSession().createCriteria(RequestedEnrollmentDirection.class);
            criteria.createAlias("entrantRequest", "entrantRequest");
            criteria.add(Restrictions.eq("entrantRequest.entrant", entrant));
            criteria.add(Restrictions.eq("originalDocumentHandedIn", Boolean.TRUE));
            criteria.setLockMode(LockMode.UPGRADE);

            if (criteria.list().isEmpty()) {
                return new CoreCollectionUtils.Pair("Абитуриент «" + fio + "» не сдал оригиналы документов, поэтому нельзя провести предварительное зачисление абитуриента в число студентов.", null);
            }
        }
//          Не проверяем другие направления подготовки
//        if (student.getStudentCategory().getCode().equals(UniDefines.STUDENT_CATEGORY_STUDENT)) {
//            MQBuilder builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "p");
//            builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.requestedEnrollmentDirection().entrantRequest().entrant().s(), entrant));
//            builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.studentCategory().code().s(), UniDefines.STUDENT_CATEGORY_STUDENT));
//            if (student.getId() != null)
//                builder.add(MQExpression.notEq("p", PreliminaryEnrollmentStudent.id().s(), student.getId()));
//            PreliminaryEnrollmentStudent otherStudent = (PreliminaryEnrollmentStudent) builder.uniqueResult(session);
//            if (otherStudent != null) {
//                String directionTitle = otherStudent.getEducationOrgUnit().getEducationLevelHighSchool().getDisplayableTitle();
//                return new CoreCollectionUtils.Pair("Абитуриент «" + fio + "» уже предварительно зачислен в число студентов по направлению «" + directionTitle + "».", null);
//            }
//
//        }

        if (student.getCompensationType().getCode().equals(UniDefines.COMPENSATION_TYPE_BUDGET)) {
            MQBuilder builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "p");
            builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.requestedEnrollmentDirection().entrantRequest().entrant().s(), entrant));
            builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.compensationType().code().s(), UniDefines.COMPENSATION_TYPE_BUDGET));
            if (student.getId() != null)
                builder.add(MQExpression.notEq("p", PreliminaryEnrollmentStudent.id().s(), student.getId()));
            PreliminaryEnrollmentStudent otherStudent = (PreliminaryEnrollmentStudent) builder.uniqueResult(session);
            if (otherStudent != null) {
                String directionTitle = otherStudent.getEducationOrgUnit().getEducationLevelHighSchool().getDisplayableTitle();
                return new CoreCollectionUtils.Pair("Абитуриент «" + fio + "» уже предварительно зачислен на бюджет по направлению «" + directionTitle + "».", null);
            }

        }

        MQBuilder builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "p");
        builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.requestedEnrollmentDirection().entrantRequest().entrant().s(), entrant));
        builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.educationOrgUnit().s(), student.getEducationOrgUnit()));
        if (student.getId() != null)
            builder.add(MQExpression.notEq("p", PreliminaryEnrollmentStudent.id().s(), student.getId()));
        if (builder.getResultCount(session) != 0L) {
            return new CoreCollectionUtils.Pair("Абитуриент «" + fio + "» уже предварительно зачислен в число студентов по данному направлению.", null);
        }
        PreliminaryEnrollmentStudent newStudent = new PreliminaryEnrollmentStudent();
        newStudent.update(student);
        if (student.getId() != null) {
            session.delete(student);
            session.flush();
        }
        session.saveOrUpdate(newStudent);

        return new CoreCollectionUtils.Pair(null, newStudent);
    }
}
