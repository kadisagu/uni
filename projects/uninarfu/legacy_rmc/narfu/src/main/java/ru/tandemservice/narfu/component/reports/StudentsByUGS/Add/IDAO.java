package ru.tandemservice.narfu.component.reports.StudentsByUGS.Add;

import ru.tandemservice.narfu.entity.NarfuReportStudentsByUGS;
import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {
    public NarfuReportStudentsByUGS createReport(Model model);

}
