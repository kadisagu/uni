package ru.tandemservice.narfu.component.reports.StudentsByUGS.Add;

import jxl.Cell;
import jxl.Range;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.*;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import org.tandemframework.tapsupport.component.selection.BaseMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.narfu.component.reports.StudentsByUGS.ReportData;
import ru.tandemservice.narfu.component.reports.StudentsByUGS.UGSReportData;
import ru.tandemservice.narfu.entity.NarfuReportStudentsByUGS;
import ru.tandemservice.narfu.utils.ReportUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.util.FilterUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.lang.Boolean;
import java.util.*;

public class DAO extends UniDao<Model> implements IDAO {
    public static final String TEMPLATE_NAME = "narfuStudentsByUGSReport";
    public static final String BACHELOR_CODE = "62";
    public static final String SPECIALTY_CODE = "65";
    public static final String MASTER_CODE = "68";

    protected static final Integer HEADER_START_ROW = 1;
    protected static final Integer HEADER_END_ROW = 2;
    protected static final Integer HEADER_START_COL = 0;
    protected static final Integer HEADER_END_COL = 31;

    protected List<StudentStatus> getDefaultStudentStatusList() {
        StudentStatus status = getCatalogItem(StudentStatus.class, "5");
        List<StudentStatus> statuses = new ArrayList<StudentStatus>();
        statuses.add(status);
        return statuses;
    }

    protected List<StudentStatus> getDefaultActiveList() {
        return getCatalogItemList(StudentStatus.class, StudentStatus.active().s(), true);
    }

    @Override
    public void prepare(Model model) {
        model.setStudStatusList(getDefaultStudentStatusList());
        model.setActiveList(getDefaultActiveList());
        model.setActiveModel(new DQLFullCheckSelectModel() {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(StudentStatus.class, alias)
                        .order(StudentStatus.title().fromAlias(alias).s());
                if (!StringUtils.isEmpty(filter))
                    FilterUtils.applySimpleLikeFilter(builder, alias, StudentStatus.title(), filter);
                return builder;
            }
        });
        model.setStudStatusModel(new BaseMultiSelectModel() {
            @Override
            public List getValues(Set primaryKeys) {
                DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(StudentStatus.class, "status");
                dql.addColumn("status");
                dql.where(DQLExpressions.eq(
                        DQLExpressions.property(StudentStatus.active().fromAlias("status")),
                        DQLExpressions.value(Boolean.FALSE)
                ));
                dql.where(DQLExpressions.in(
                        DQLExpressions.property(StudentStatus.id().fromAlias("status")),
                        primaryKeys
                ));
                return dql.createStatement(getSession()).list();
            }

            @Override
            public ListResult findValues(String filter) {
                DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(StudentStatus.class, "status");
                dql.addColumn("status");
                dql.where(DQLExpressions.eq(
                        DQLExpressions.property(StudentStatus.active().fromAlias("status")),
                        DQLExpressions.value(Boolean.FALSE)
                ));
                if (filter != null && filter.length() > 0) {
                    FilterUtils.applyLikeFilter(dql, filter, StudentStatus.title().fromAlias("status"));
                }
                List result = dql.createStatement(getSession()).list();
                return new ListResult(result, result.size());
            }
        });
        model.setFormativeOrgUnitListModel(new OrgUnitKindAutocompleteModel("2", null));
    }

    @Override
    public NarfuReportStudentsByUGS createReport(Model model) {
        NarfuReportStudentsByUGS report = new NarfuReportStudentsByUGS();
        ReportData reportData = new ReportData();

        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(NarfuReportStudentsByUGS.class, "report");
        dql.addColumn(DQLFunctions.count(NarfuReportStudentsByUGS.id().fromAlias("report").s()));
        Long num = (Long) dql.createStatement(getSession()).list().get(0);
        if (model.getStudStatusList().size() > 0) {
            String studentStatus = "";
            for (int i = 0; i < model.getStudStatusList().size(); i++) {
                studentStatus += model.getStudStatusList().get(i).getTitle();
                if (i != model.getStudStatusList().size() - 1)
                    studentStatus += ", ";
            }
            report.setStudentStatus(studentStatus);
        }
        if (model.getActiveList().size() > 0) {
            String active = "";
            for (int i = 0; i < model.getActiveList().size(); i++) {
                active += model.getActiveList().get(i).getTitle();
                if (i != model.getActiveList().size() - 1)
                    active += ", ";
            }
            report.setActiveStatus(active);
        }

        report.setNumber(num.intValue() + 1);
        report.setFormingDate(new Date());
        //«Отчет № 1 от 09.11.2012
        String title = new StringBuilder()
                .append("Отчет № ")
                .append(String.valueOf(report.getNumber()))
                .append(" от ")
                .append(RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(report.getFormingDate()))
                .toString();
        report.setTitle(title);

        try {
            reportData.setReport(report);
            reportData.setModel(model);
            loadTemplate(reportData);
            fillReport(reportData, model);

            reportData.getWorkbook().write();
            reportData.getWorkbook().close();

            DatabaseFile reportFile = new DatabaseFile();
            reportFile.setContent(reportData.getWorkbookStream().toByteArray());
            reportFile.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_VND_MS_EXCEL);

            save(reportFile);
            report.setContent(reportFile);
            save(report);
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new ApplicationException(e.getMessage(), e);
        }

        return report;
    }

    protected void loadTemplate(ReportData reportData) throws Exception {
        TemplateDocument template = getCatalogItem(TemplateDocument.class, TEMPLATE_NAME);
        ByteArrayInputStream in = new ByteArrayInputStream(template.getContent());

        Workbook templateWBook = Workbook.getWorkbook(in);
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);

        ByteArrayOutputStream wbOS = new ByteArrayOutputStream();
        WritableWorkbook writableWorkbook = Workbook.createWorkbook(wbOS, templateWBook, ws);

        reportData.setWorkbookStream(wbOS);
        reportData.setWorkbook(writableWorkbook);
    }

    protected void copyHeaderCellsToSheet(WritableWorkbook workbook, int sheetIdxFrom, int sheetIdxTo, int startRow, int endRow, int startCol, int endCol) throws Exception {
        WritableSheet sheetFrom = workbook.getSheet(sheetIdxFrom);
        WritableSheet sheetTo = workbook.getSheet(sheetIdxTo);
        for (int row = startRow; row <= endRow; row++) {
            Cell[] cells = sheetFrom.getRow(row);
            for (Cell cell : cells) {
                Label lbl = new Label(cell.getColumn(), cell.getRow(), cell.getContents(), cell.getCellFormat());
                sheetTo.addCell(lbl);
            }
        }

        Range[] mergedCells = sheetFrom.getMergedCells();
        for (Range mc : mergedCells) {
            sheetTo.mergeCells(mc.getTopLeft().getColumn(), mc.getTopLeft().getRow(), mc.getBottomRight().getColumn(), mc.getBottomRight().getRow());
        }

        for (int col = startCol; col < endCol; col++) {
            sheetTo.setColumnView(col, sheetFrom.getColumnView(col));
        }
    }

    protected void fillReport(ReportData reportData, Model model) throws Exception {
        List<DevelopForm> developForms = getList(DevelopForm.class);

        int index = 1;
        for (DevelopForm developForm : developForms) {
            //стандартная функция из jxl для копирования листа имеет изветстный баг, поэтому придется вручную копировать:
            WritableSheet sheet = reportData.getWorkbook().createSheet("УГС " + developForm.getShortTitle(), index);
            copyHeaderCellsToSheet(reportData.getWorkbook(), 0, index, HEADER_START_ROW, HEADER_END_ROW, HEADER_START_COL, HEADER_END_COL);
            fillDevelopForm(reportData, developForm, sheet, model);
            index++;
        }
        reportData.getWorkbook().removeSheet(0);
    }

    DQLSelectBuilder getStudentCountDql(ReportData reportData, DevelopForm developForm, List<Qualifications> qualifList, Model model, boolean isParentParent) {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(Student.class, "student");

        if (isParentParent) {
            dql.addColumn(Student.educationOrgUnit().educationLevelHighSchool().educationLevel().parentLevel().parentLevel().parentLevel().id().fromAlias("student").s());
            dql.addColumn(Student.educationOrgUnit().educationLevelHighSchool().educationLevel().parentLevel().parentLevel().parentLevel().inheritedOkso().fromAlias("student").s());

            dql.group(Student.educationOrgUnit().educationLevelHighSchool().educationLevel().parentLevel().parentLevel().parentLevel().id().fromAlias("student").s());
            dql.group(Student.educationOrgUnit().educationLevelHighSchool().educationLevel().parentLevel().parentLevel().parentLevel().inheritedOkso().fromAlias("student").s());
        }
        else {
            dql.addColumn(Student.educationOrgUnit().educationLevelHighSchool().educationLevel().parentLevel().parentLevel().id().fromAlias("student").s());
            dql.addColumn(Student.educationOrgUnit().educationLevelHighSchool().educationLevel().parentLevel().parentLevel().inheritedOkso().fromAlias("student").s());

            dql.group(Student.educationOrgUnit().educationLevelHighSchool().educationLevel().parentLevel().parentLevel().id().fromAlias("student").s());
            dql.group(Student.educationOrgUnit().educationLevelHighSchool().educationLevel().parentLevel().parentLevel().inheritedOkso().fromAlias("student").s());
        }
        if (model.getFormativeOrgUnitList() != null && !model.getFormativeOrgUnitList().isEmpty())
            dql.where(DQLExpressions.in(
                    DQLExpressions.property(Student.educationOrgUnit().formativeOrgUnit().fromAlias("student")),
                    model.getFormativeOrgUnitList()
            ));

        dql.addColumn(Student.educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().code().fromAlias("student").s());
        dql.addColumn(Student.course().intValue().fromAlias("student").s());
        dql.addColumn(Student.status().code().fromAlias("student").s());
        dql.addColumn(Student.compensationType().code().fromAlias("student").s());
        dql.addColumn(Student.educationOrgUnit().developPeriod().lastCourse().fromAlias("student").s());
        dql.addColumn(DQLFunctions.count(Student.id().fromAlias("student").s()));

        dql.group(DQLExpressions.property(Student.educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().code().fromAlias("student")));
        dql.group(DQLExpressions.property(Student.course().intValue().fromAlias("student")));
        dql.group(DQLExpressions.property(Student.status().code().fromAlias("student")));
        dql.group(DQLExpressions.property(Student.compensationType().code().fromAlias("student")));
        dql.group(DQLExpressions.property(Student.educationOrgUnit().developPeriod().lastCourse().fromAlias("student")));

        dql.where(DQLExpressions.eq(
                DQLExpressions.property(Student.educationOrgUnit().developForm().fromAlias("student")),
                DQLExpressions.value(developForm)
        ));
        dql.where(DQLExpressions.in(
                DQLExpressions.property(Student.educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().fromAlias("student")),
                qualifList
        ));
        dql.where(DQLExpressions.eq(
                DQLExpressions.property(Student.archival().fromAlias("student")),
                DQLExpressions.value(Boolean.FALSE)
        ));

        dql.where(DQLExpressions.or(
                DQLExpressions.in(
                        DQLExpressions.property(Student.status().fromAlias("student")),
                        reportData.getModel().getStudStatusList()
                ),
                DQLExpressions.in(
                        DQLExpressions.property(Student.status().fromAlias("student")),
                        reportData.getModel().getActiveList()
                )
        ));

        return dql;
    }

    List<Long> getUGListIds(Model model) {
        if (model.getFormativeOrgUnitList() != null && !model.getFormativeOrgUnitList().isEmpty()) {
            DQLSelectBuilder dqlOU = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "eou");
            dqlOU.where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.formativeOrgUnit().fromAlias("eou")), model.getFormativeOrgUnitList()));
            dqlOU.column(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().educationLevel().fromAlias("eou")));
            List<EducationLevels> levels = getList(dqlOU);
            Set<EducationLevels> ugs = new HashSet<>();
            for (EducationLevels lvl : levels) {
                ugs.add(ReportUtils.getHighParentLevel(lvl));
            }
            return new ArrayList<>(ids(ugs));
        }
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EducationLevels.class, "e");
        dql.addColumn(EducationLevels.id().fromAlias("e").s());
        dql.where(DQLExpressions.isNull(
                DQLExpressions.property(EducationLevels.parentLevel().id().fromAlias("e"))
        ));

        return getList(dql);
    }


    protected WritableCellFormat getCellFormat(boolean isBold, jxl.format.Colour colour, boolean isCenter) throws Exception {
        WritableFont font = new WritableFont(WritableFont.TIMES, 10);
        if (isBold) {
            font.setBoldStyle(WritableFont.BOLD);
        }

        WritableCellFormat result = new WritableCellFormat(font);
        result.setBackground(colour);
        result.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        result.setVerticalAlignment(VerticalAlignment.CENTRE);
        if (isCenter) {
            result.setAlignment(Alignment.CENTRE);
        }
        else {
            result.setAlignment(Alignment.LEFT);
        }

        result.setWrap(true);

        return result;
    }

    protected void fillDevelopForm(ReportData reportData, DevelopForm developForm, WritableSheet sheet, Model model) throws Exception {
        List<Qualifications> qual = getUsableQualifications(reportData);
        List<Long> ugList = getUGListIds(model);

        DQLSelectBuilder dql1 = getStudentCountDql(reportData, developForm, qual, model, false);
        DQLSelectBuilder dql2 = getStudentCountDql(reportData, developForm, qual, model, true);

        List<Object[]> result1 = getList(dql1);
        List<Object[]> result2 = getList(dql2);

        Map<String, UGSReportData> bachelorDataMap = new TreeMap<String, UGSReportData>();
        Map<String, UGSReportData> specDataMap = new TreeMap<String, UGSReportData>();
        Map<String, UGSReportData> masterDataMap = new TreeMap<String, UGSReportData>();
        List<StudentStatus> activeStatusList = model.getActiveList();
        List<String> activeStatusListCodes = new ArrayList<>();
        for (StudentStatus item : activeStatusList) {
            activeStatusListCodes.add(item.getCode());
        }
        List<StudentStatus> vacationStatusList = model.getStudStatusList();
        List<String> vacationStatusListCodes = new ArrayList<>();
        for (StudentStatus item : vacationStatusList) {
            vacationStatusListCodes.add(item.getCode());
        }

        for (Object[] line : result1) {
            processDqlLine(line, bachelorDataMap, specDataMap, masterDataMap, ugList, activeStatusListCodes, vacationStatusListCodes);
        }
        for (Object[] line : result2) {
            processDqlLine(line, bachelorDataMap, specDataMap, masterDataMap, ugList, activeStatusListCodes, vacationStatusListCodes);
        }

        UGSReportData totalData = new UGSReportData();
        totalData.setTitle("ВСЕГО");
        int rowNum = 3;//нумерация в jxl идет с нуля
        rowNum = addDataFromMap(rowNum, totalData, masterDataMap, sheet, reportData, "Всего по магистратуре");
        rowNum = addDataFromMap(rowNum, totalData, bachelorDataMap, sheet, reportData, "Всего по бакалавриату");
        rowNum = addDataFromMap(rowNum, totalData, specDataMap, sheet, reportData, "Всего по специалитету");

        addRow(rowNum, totalData, sheet, reportData, getCellFormat(true, Colour.ICE_BLUE, false), getCellFormat(true, Colour.ICE_BLUE, false));

        String dt = RussianDateFormatUtils.getDateFormattedWithMonthName(reportData.getReport().getFormingDate());
        String devFormGen = null;
        try {
            devFormGen = developForm.getGenCaseTitle();
        }
        catch (Exception e) {
            devFormGen = developForm.getTitle();
        }
        sheet.addCell(new Label(0, 0, "КОНТИНГЕНТ студентов " + devFormGen + " формы обучения по УГС на " + dt + " года", getCellFormat(true, Colour.WHITE, true)));
    }

    protected int addDataFromMap(int rowNum, UGSReportData totalData, Map<String, UGSReportData> reportDataMap, WritableSheet sheet, ReportData reportData, String titleString) throws Exception {
        int startRowNum = rowNum;
        rowNum++;
        UGSReportData totalReportData = new UGSReportData();
        totalReportData.setTitle(titleString);

        WritableCellFormat titleRowFormat = getCellFormat(false, Colour.LAVENDER, false);
        WritableCellFormat numRowFormat = getCellFormat(true, Colour.WHITE, false);

        for (String oksoUG : reportDataMap.keySet()) {
            UGSReportData ugReportData = reportDataMap.get(oksoUG);
            addRow(rowNum, ugReportData, sheet, reportData, titleRowFormat, numRowFormat);

            for (int course = 1; course <= 6; course++) {
                addValueToMap(course, totalReportData.getActBudStudCount(), ugReportData.getActBudStudCount().get(course));
                addValueToMap(course, totalReportData.getActCtrStudCount(), ugReportData.getActCtrStudCount().get(course));
                addValueToMap(course, totalReportData.getVacBudStudCount(), ugReportData.getVacBudStudCount().get(course));
                addValueToMap(course, totalReportData.getVacCtrStudCount(), ugReportData.getVacCtrStudCount().get(course));

                addValueToMap(course, totalData.getActBudStudCount(), ugReportData.getActBudStudCount().get(course));
                addValueToMap(course, totalData.getActCtrStudCount(), ugReportData.getActCtrStudCount().get(course));
                addValueToMap(course, totalData.getVacBudStudCount(), ugReportData.getVacBudStudCount().get(course));
                addValueToMap(course, totalData.getVacCtrStudCount(), ugReportData.getVacCtrStudCount().get(course));
            }
            rowNum++;
            totalReportData.setLastCourseVal(totalReportData.getLastCourseVal() + ugReportData.getLastCourseVal());
            totalData.setLastCourseVal(totalData.getLastCourseVal() + ugReportData.getLastCourseVal());
        }

        WritableCellFormat totalCellFormat = getCellFormat(true, Colour.VERY_LIGHT_YELLOW, false);
        addRow(startRowNum, totalReportData, sheet, reportData, totalCellFormat, totalCellFormat);
        return rowNum;
    }

    protected void addRow(int rowNum, UGSReportData ugReportData, WritableSheet sheet, ReportData reportData, WritableCellFormat titleRowFormat, WritableCellFormat numberRowFormat) throws Exception {
        sheet.addCell(new Label(0, rowNum, ugReportData.getTitle(), titleRowFormat));

        int colNum = 1;
        Integer totalActBugCount = 0;
        Integer totalActCtrCount = 0;
        Integer totalVacBugCount = 0;
        Integer totalVacCtrCount = 0;

        for (int course = 1; course <= 6; course++) {    //TODO last course
            Integer actBud = getValueFromMap(course, ugReportData.getActBudStudCount());
            Integer actCtr = getValueFromMap(course, ugReportData.getActCtrStudCount());
            Integer vacBud = getValueFromMap(course, ugReportData.getVacBudStudCount());
            Integer vacCtr = getValueFromMap(course, ugReportData.getVacCtrStudCount());
            Integer totalByCourseCount = actBud + actCtr;

            sheet.addCell(new jxl.write.Number(colNum, rowNum, totalByCourseCount, numberRowFormat));
            colNum++;
            sheet.addCell(new jxl.write.Number(colNum, rowNum, actCtr, numberRowFormat));
            colNum++;
            sheet.addCell(new jxl.write.Number(colNum, rowNum, vacBud, numberRowFormat));
            colNum++;
            sheet.addCell(new jxl.write.Number(colNum, rowNum, vacCtr, numberRowFormat));
            colNum++;

            totalActBugCount += actBud;
            totalActCtrCount += actCtr;
            totalVacBugCount += vacBud;
            totalVacCtrCount += vacCtr;
        }
        Integer totalCount = totalActBugCount + totalActCtrCount;

        sheet.addCell(new jxl.write.Number(colNum, rowNum, totalCount, numberRowFormat));
        colNum++;
        sheet.addCell(new jxl.write.Number(colNum, rowNum, totalActBugCount, numberRowFormat));
        colNum++;
        sheet.addCell(new jxl.write.Number(colNum, rowNum, totalActCtrCount, numberRowFormat));
        colNum++;

        sheet.addCell(new jxl.write.Number(colNum, rowNum, totalVacBugCount, numberRowFormat));
        colNum++;
        sheet.addCell(new jxl.write.Number(colNum, rowNum, totalVacCtrCount, numberRowFormat));
        colNum++;
        sheet.addCell(new jxl.write.Number(colNum, rowNum, ugReportData.getLastCourseVal(), numberRowFormat));
        colNum++;

        Integer totalCtrCount = totalVacCtrCount + totalActCtrCount;
        sheet.addCell(new jxl.write.Number(colNum, rowNum, totalCtrCount, numberRowFormat));
        colNum++;
    }

    private void processDqlLine(Object[] line,
                                Map<String, UGSReportData> bachelorDataMap,
                                Map<String, UGSReportData> specDataMap,
                                Map<String, UGSReportData> masterDataMap,
                                List<Long> ugList,
                                List<String> activeStatusListCodes,
                                List<String> vacationStatusListCodes)
    {
        UGSReportData rep = null;

        if (line.length <= 7 || Arrays.asList(line).contains(null)) {
            return;
        }

        Long ugId = (Long) line[0];
        String oksoUG = (String) line[1];

        if (!ugList.contains(ugId)) {
            return;
        }

        String qualifCode = (String) line[2];

        if (qualifCode.equals(BACHELOR_CODE)) {
            rep = getDataFromMap(oksoUG, bachelorDataMap);
        }
        else if (qualifCode.equals(SPECIALTY_CODE)) {
            rep = getDataFromMap(oksoUG, specDataMap);
        }
        else if (qualifCode.equals(MASTER_CODE)) {
            rep = getDataFromMap(oksoUG, masterDataMap);
        }
        if (rep == null) return;

        if (rep.getTitle() == null) {
            rep.setTitle("Группа специальностей " + oksoUG);
        }

        Integer course = (Integer) line[3];
        Integer lastCourse = (Integer) line[6];

        Integer count = ((Long) line[7]).intValue();
        String status = (String) line[4];

        Boolean isBudget = line[5].toString().equals(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET);

        if (activeStatusListCodes.contains(status) || vacationStatusListCodes.contains(status)) {
            if (isBudget) {
                addValueToMap(course, rep.getActBudStudCount(), count);
            }
            else {
                addValueToMap(course, rep.getActCtrStudCount(), count);
            }
        }
        if (vacationStatusListCodes.contains(status)) {
            if (isBudget) {
                addValueToMap(course, rep.getVacBudStudCount(), count);
            }
            else {
                addValueToMap(course, rep.getVacCtrStudCount(), count);
            }
        }

        //прибавляем значения к выпускному курсу:
        if (course.equals(lastCourse) && activeStatusListCodes.contains(status)) {
            rep.setLastCourseVal(rep.getLastCourseVal() + count);
        }
    }

    private UGSReportData getDataFromMap(String key, Map<String, UGSReportData> map) {
        if (map.containsKey(key)) {
            return map.get(key);
        }
        else {
            UGSReportData reportData = new UGSReportData();
            map.put(key, reportData);
            return reportData;
        }
    }

    private Integer getValueFromMap(Integer key, Map<Integer, Integer> map) {
        if (map.containsKey(key)) {
            return map.get(key);
        }
        return 0;
    }

    private void addValueToMap(Integer key, Map<Integer, Integer> map, Integer addVal) {
        if (addVal != null) {
            Integer val = 0;
            if (map.containsKey(key)) {
                val = map.get(key);
            }
            map.put(key, val + addVal);
        }
    }

    protected List<Qualifications> getUsableQualifications(ReportData reportData) {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(Qualifications.class, "q");
        dql.addColumn("q");
        dql.where(DQLExpressions.or(
                DQLExpressions.eq(
                        DQLExpressions.property(Qualifications.code().fromAlias("q")),
                        DQLExpressions.value(BACHELOR_CODE)
                ),
                DQLExpressions.eq(
                        DQLExpressions.property(Qualifications.code().fromAlias("q")),
                        DQLExpressions.value(SPECIALTY_CODE)
                ),
                DQLExpressions.eq(
                        DQLExpressions.property(Qualifications.code().fromAlias("q")),
                        DQLExpressions.value(MASTER_CODE)
                )
        ));
        return getList(dql);
    }


}
