package ru.tandemservice.narfu.component.reports.StudentsByUGS.List;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.narfu.entity.NarfuReportStudentsByUGS;

public class Model {
    private DynamicListDataSource<NarfuReportStudentsByUGS> dataSource;

    public DynamicListDataSource<NarfuReportStudentsByUGS> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<NarfuReportStudentsByUGS> dataSource) {
        this.dataSource = dataSource;
    }
}
