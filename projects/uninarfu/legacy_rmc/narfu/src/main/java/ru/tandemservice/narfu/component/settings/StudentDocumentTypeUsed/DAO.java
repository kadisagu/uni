package ru.tandemservice.narfu.component.settings.StudentDocumentTypeUsed;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.narfu.entity.DocumentType2StudentCategory;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.StudentDocumentType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DAO extends ru.tandemservice.uni.component.settings.StudentDocumentTypeUsed.DAO implements IDAO {

    @Override
    public void prepare(ru.tandemservice.uni.component.settings.StudentDocumentTypeUsed.Model model) {
        super.prepare(model);
        Model myModel = (Model) model;
        myModel.setStudentCategoryList(getCatalogItemList(StudentCategory.class));
    }

    @Override
    public void prepareListDataSource(ru.tandemservice.uni.component.settings.StudentDocumentTypeUsed.Model model) {
        super.prepareListDataSource(model);
        Model myModel = (Model) model;

        DynamicListDataSource<StudentDocumentType> dataSource = model.getDataSource();
        List<DocumentType2StudentCategory> relationList = getList(new DQLSelectBuilder().fromEntity(DocumentType2StudentCategory.class, "rel"));

        Map<MultiKey, Boolean> relationMap = new HashMap<>();

        for (DocumentType2StudentCategory rel : relationList) {
            relationMap.put(new MultiKey(rel.getStudentDocumentType(), rel.getStudentCategory()), Boolean.TRUE);
        }

        List<ViewWrapper<StudentDocumentType>> patchedList = ViewWrapper.getPatchedList(dataSource);

        for (ViewWrapper<StudentDocumentType> wrapper : patchedList) {
            for (StudentCategory studentCategory : myModel.getStudentCategoryList()) {
                MultiKey key = new MultiKey(wrapper.getEntity(), studentCategory);
                String viewProperty = getViewPropertyKey(studentCategory);
                if (!relationMap.containsKey(key)) {
                    wrapper.setViewProperty(viewProperty, Boolean.FALSE);
                }
                else {
                    wrapper.setViewProperty(viewProperty, relationMap.get(key).booleanValue());
                }
            }
        }
    }

    public String getViewPropertyKey(StudentCategory studentCategory) {
        return new StringBuilder().append(StudentCategory.class.getSimpleName()).append(DELIMETER).append(studentCategory.getCode()).toString();
    }

    @SuppressWarnings("unchecked")
    @Override
    public void updateRelation(Model model, Object[] parameters) {

        ViewWrapper<StudentDocumentType> wrapper = ((ViewWrapper<StudentDocumentType>) model.getDataSource().getRecordById((Long) parameters[0]));
        StudentDocumentType studentDocumentType = wrapper.getEntity();  // тип документа
        StudentCategory studentCategory = getCatalogItem(StudentCategory.class, (String) parameters[1]);

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(DocumentType2StudentCategory.class, "rel")
                .where(DQLExpressions.eqValue(
                        DQLExpressions.property(DocumentType2StudentCategory.studentDocumentType().fromAlias("rel")), studentDocumentType))
                .where(DQLExpressions.eqValue(
                        DQLExpressions.property(DocumentType2StudentCategory.studentCategory().fromAlias("rel")), studentCategory));

        DocumentType2StudentCategory documentType2StudentCategory = dql.createStatement(getSession()).uniqueResult();

        if (documentType2StudentCategory == null) {
            documentType2StudentCategory = new DocumentType2StudentCategory();
            documentType2StudentCategory.setStudentCategory(studentCategory);
            documentType2StudentCategory.setStudentDocumentType(studentDocumentType);
            getSession().save(documentType2StudentCategory);
        }
        else {
            getSession().delete(documentType2StudentCategory);
        }
    }


}
