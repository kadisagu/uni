package ru.tandemservice.narfu.base.ext.SessionReport.ui.DebtorsList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.IPresenterExtensionBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.DebtorsList.SessionReportDebtorsList;
import ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport;

@Configuration
public class SessionReportDebtorsListExt extends BusinessComponentExtensionManager {

    @Autowired
    public SessionReportDebtorsList parent;

    @Autowired
    @Qualifier("SessionReportDebtorsList.sessionReportDebtorsDS")
    public ColumnListExtPoint columnList;

    @Bean
    public PresenterExtension presenterExtension() {
        IPresenterExtensionBuilder pi = presenterExtensionBuilder(parent.presenterExtPoint());

        columnList.addExtensionPointItemCallback(
                textColumn("compensationType", UnisessionDebtorsReport.compensationType().s()).before("print").create());
        columnList.addExtensionPointItemCallback(
                textColumn("developForm", UnisessionDebtorsReport.developForm().s()).after("compensationType").create());
        return pi.create();
    }

}
