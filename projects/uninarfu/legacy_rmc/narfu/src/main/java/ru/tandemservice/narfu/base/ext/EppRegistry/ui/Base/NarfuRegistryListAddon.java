/* $Id$ */
package ru.tandemservice.narfu.base.ext.EppRegistry.ui.Base;

import com.google.common.collect.ImmutableMap;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.base.bo.EppRegistry.ui.AbstractList.EppRegistryAbstractList;
import ru.tandemservice.uniepp.base.bo.EppRegistry.ui.CloneElement.EppRegistryCloneElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import java.util.Collection;
import java.util.List;

/**
 * @author Irina Ugfeld
 * @since 11.03.2016
 */
public class NarfuRegistryListAddon extends UIAddon {
    public static final String NAME = "narfuRegistryListAddon";

    public static final String SETTING_EDU_PLAN_VERSION = "eduPlanVersion";
    public static final String SETTING_EPP_WORK_PLAN = "eppWorkPlan";

    public NarfuRegistryListAddon(IUIPresenter presenter, String name, String componentId) {
        super(presenter, name, componentId);
    }

   public void onClickCopy(){
       Collection<IEntity> checkedItems = getSelectedRowsFromRegElementsDS();

       if (checkedItems.size() != 1)
       {
           throw new ApplicationException("Необходимо выделить одну дисциплину.");
       }
       EppRegistryElement element = ((DataWrapper)checkedItems.iterator().next()).getWrapped();
       ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME,
               new ComponentActivator(
                       EppRegistryCloneElement.class.getSimpleName(),
                       new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, element.getId())
               )
       );
   }

    public void onClickEditOwner() {
        Collection<IEntity> checkedItems = getSelectedRowsFromRegElementsDS();

        if (checkedItems.isEmpty())
            throw new ApplicationException("Не выбраны записи реестра.");

        List<Long> ids = UniBaseUtils.getIdList(checkedItems);

        ContextLocal.createDesktop(
                "PersonShellDialog",
                new ComponentActivator(
                        "ru.tandemservice.narfu.component.registry.base.TutorOrgunitEdit",
                        ImmutableMap.of("registryIds", ids))
        );
    }

    private Collection<IEntity> getSelectedRowsFromRegElementsDS(){
        return ((BaseSearchListDataSource) getPresenterConfig().getDataSource(EppRegistryAbstractList.ELEMENT_DS))
                .getOptionColumnSelectedObjects(EppRegistryAbstractList.COLUMN_CHECKBOX);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        dataSource.put(SETTING_EDU_PLAN_VERSION, _presenter.getSettings().get(SETTING_EDU_PLAN_VERSION));
        dataSource.put(SETTING_EPP_WORK_PLAN, _presenter.getSettings().get(SETTING_EPP_WORK_PLAN));
    }

}