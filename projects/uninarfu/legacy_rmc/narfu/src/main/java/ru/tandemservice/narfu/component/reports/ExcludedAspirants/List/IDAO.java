package ru.tandemservice.narfu.component.reports.ExcludedAspirants.List;

import ru.tandemservice.narfu.uniec.entity.report.ExcludedAspirantsReport;
import ru.tandemservice.uni.dao.IUniDao;


public interface IDAO extends IUniDao<Model> {

    public ExcludedAspirantsReport createReport(Model model);

}
