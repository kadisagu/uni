/* $Id$ */
package ru.tandemservice.narfu.base.ext.Person.ui.NextOfKinAddEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.caf.ui.addon.IUIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.person.base.bo.Person.ui.NextOfKinAddEdit.PersonNextOfKinAddEditUI;

/**
 * @author Ekaterina Zvereva
 * @since 28.06.2015
 */
public class PersonNextOfKindClickApply extends NamedUIAction
{
    protected PersonNextOfKindClickApply(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        if(presenter instanceof PersonNextOfKinAddEditUI)
        {
            PersonNextOfKinAddEditUI pr = (PersonNextOfKinAddEditUI) presenter;
            pr.onClickApply();
            IUIAddon uiAddon = presenter.getConfig().getAddon(PersonNextOfKinAddEditExt.ADDONE_NAME);
            if (null != uiAddon && uiAddon instanceof PersonNextOfKinAddEditUIExt)
            {
                PersonNextOfKinAddEditUIExt addon = (PersonNextOfKinAddEditUIExt) uiAddon;
                addon.getSession().clear();
                DataAccessServices.dao().saveOrUpdate(addon.getPersonNextOfKinNARFU());
            }
            pr.deactivate();
        }
    }
}
