package ru.tandemservice.narfu.component.reports.StudentArchiveReport.Add;

import org.tandemframework.core.component.State;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.narfu.entity.NarfuReportStudentArchive;

@State(keys = "orgUnitId", bindings = "orgUnitId")
public class Model {

    private NarfuReportStudentArchive report = new NarfuReportStudentArchive();

    private Integer educationYear;

    private String documentsReceivedPost;
    private String documentsReceivedFIO;

    private String documentsHandedPost;
    private String documentsHandedFIO;

    private String visaPost;
    private String visaFIO;

    private ISelectModel typeModel;

    private Long orgUnitId;
    private OrgUnit orgUnit;

    public ISelectModel getTypeModel() {
        return typeModel;
    }

    public Integer getEducationYear() {
        return educationYear;
    }

    public void setEducationYear(Integer educationYear) {
        this.educationYear = educationYear;
    }

    public String getDocumentsReceivedPost() {
        return documentsReceivedPost;
    }

    public void setDocumentsReceivedPost(String documentsReceivedPost) {
        this.documentsReceivedPost = documentsReceivedPost;
    }

    public String getDocumentsReceivedFIO() {
        return documentsReceivedFIO;
    }

    public void setDocumentsReceivedFIO(String documentsReceivedFIO) {
        this.documentsReceivedFIO = documentsReceivedFIO;
    }

    public String getDocumentsHandedPost() {
        return documentsHandedPost;
    }

    public void setDocumentsHandedPost(String documentsHandedPost) {
        this.documentsHandedPost = documentsHandedPost;
    }

    public String getDocumentsHandedFIO() {
        return documentsHandedFIO;
    }

    public void setDocumentsHandedFIO(String documentsHandedFIO) {
        this.documentsHandedFIO = documentsHandedFIO;
    }

    public String getVisaPost() {
        return visaPost;
    }

    public void setVisaPost(String visaPost) {
        this.visaPost = visaPost;
    }

    public String getVisaFIO() {
        return visaFIO;
    }

    public void setVisaFIO(String visaFIO) {
        this.visaFIO = visaFIO;
    }

    public void setTypeModel(ISelectModel typeModel) {
        this.typeModel = typeModel;
    }

    public NarfuReportStudentArchive getReport() {
        return report;
    }

    public void setReport(NarfuReportStudentArchive report) {
        this.report = report;
    }

    public Long getOrgUnitId() {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId) {
        this.orgUnitId = orgUnitId;
    }

    public OrgUnit getOrgUnit() {
        return orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit) {
        this.orgUnit = orgUnit;
    }

}
