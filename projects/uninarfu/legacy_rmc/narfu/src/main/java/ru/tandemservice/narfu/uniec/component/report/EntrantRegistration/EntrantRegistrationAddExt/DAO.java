package ru.tandemservice.narfu.uniec.component.report.EntrantRegistration.EntrantRegistrationAddExt;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.eventssecrmc.ui.OrgUnitModel;
import ru.tandemservice.narfu.uniec.entity.report.NarfuEntrantRegistration;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.Date;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());

        NarfuEntrantRegistration report = new NarfuEntrantRegistration();
        report.setDateFrom(new Date());
        report.setKind("Отб.ком.");
        model.setReport(report);

        model.setOrgUnitModel(new OrgUnitModel(model, new String[]{OrgUnit.titleWithType().s()}));
    }

    @Override
    public void update(Model model) {
        NarfuEntrantRegistration report = model.getReport();
        report.setFormingDate(new Date());
        report.setEnrollmentCampaign(model.getEnrollmentCampaign());

        if (model.isOrgUnitActive())
            report.setFormativeOrgUnitTitle(StringUtils.join(UniUtils.getPropertiesList(model.getOrgUnitList(), "fullTitle"), "; "));

        DatabaseFile databaseFile = new NarfuEntrantRegistrationReportBuilderExt(model, getSession()).getContent();
        getSession().save(databaseFile);
        report.setContent(databaseFile);

        getSession().save(report);
    }


}
