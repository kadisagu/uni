package ru.tandemservice.narfu.component.wizard.PersonalDataStep;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.catalog.entity.MilitaryOffice;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudentrmc.entity.PersonNARFU;
import ru.tandemservice.uniecrmc.entity.entrant.Entrant2IndividualAchievements;

import java.util.ArrayList;
import java.util.List;


public class Model extends
        ru.tandemservice.uniecrmc.component.wizard.PersonalDataStep.Model
{
    private boolean hasIndividualAchievement;
    private Entrant2IndividualAchievements entrant2IndividualAchievements = new Entrant2IndividualAchievements();
    private List<Entrant2IndividualAchievements> selectedAchievementsList = new ArrayList<>();
    private ISelectModel individualAchievementsModel;
    private DynamicListDataSource<Entrant2IndividualAchievements> achievementsDataSource;

    private ISelectModel enlistmentOfficesModel;
    private MilitaryOffice attachedEnlistmentOffice;
    private MilitaryOffice militaryEnlistmentOffice;

    public MilitaryOffice getMilitaryEnlistmentOffice() {
        return militaryEnlistmentOffice;
    }

    public void setMilitaryEnlistmentOffice(MilitaryOffice militaryEnlistmentOffice) {
        this.militaryEnlistmentOffice = militaryEnlistmentOffice;
    }

    public MilitaryOffice getAttachedEnlistmentOffice() {
        return attachedEnlistmentOffice;
    }

    public void setAttachedEnlistmentOffice(MilitaryOffice attachedEnlistmentOffice) {
        this.attachedEnlistmentOffice = attachedEnlistmentOffice;
    }

    public ISelectModel getEnlistmentOfficesModel() {
        return enlistmentOfficesModel;
    }

    public void setEnlistmentOfficesModel(ISelectModel enlistmentOfficesModel) {
        this.enlistmentOfficesModel = enlistmentOfficesModel;
    }

    public boolean isHasIndividualAchievement() {
        return hasIndividualAchievement;
    }

    public void setHasIndividualAchievement(boolean hasIndividualAchievement) {
        this.hasIndividualAchievement = hasIndividualAchievement;
    }

    public Entrant2IndividualAchievements getEntrant2IndividualAchievements() {
        return entrant2IndividualAchievements;
    }

    public void setEntrant2IndividualAchievements(Entrant2IndividualAchievements entrant2IndividualAchievements) {
        this.entrant2IndividualAchievements = entrant2IndividualAchievements;
    }

    public List<Entrant2IndividualAchievements> getSelectedAchievementsList() {
        return selectedAchievementsList;
    }

    public void setSelectedAchievementsList(List<Entrant2IndividualAchievements> selectedAchievementsList) {
        this.selectedAchievementsList = selectedAchievementsList;
    }

    public ISelectModel getIndividualAchievementsModel() {
        return individualAchievementsModel;
    }

    public void setIndividualAchievementsModel(ISelectModel individualAchievementsModel) {
        this.individualAchievementsModel = individualAchievementsModel;
    }

    public DynamicListDataSource<Entrant2IndividualAchievements> getAchievementsDataSource() {
        return achievementsDataSource;
    }

    public void setAchievementsDataSource(DynamicListDataSource<Entrant2IndividualAchievements> achievementsDataSource) {
        this.achievementsDataSource = achievementsDataSource;
    }

    private PersonNARFU personNARFU;

    public PersonNARFU getPersonNARFU() {
        return personNARFU;
    }

    public void setPersonNARFU(PersonNARFU personNARFU) {
        this.personNARFU = personNARFU;
    }

}
