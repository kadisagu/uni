package ru.tandemservice.narfu.component.entrant.EcDistributionPrintProtocol;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;

import java.util.*;

@Input(keys = {"ids", "categoryId", "compensationTypeId"}, bindings = {"ids", "categoryId", "compensationType.id"})
public class Model {
    private Set<Long> ids;
    private Long categoryId;
    private CompensationType compensationType = new CompensationType();

    private IdentifiableWrapper conkurs;
    private List<IdentifiableWrapper> conkursList = Arrays.asList(
            new IdentifiableWrapper(1L, "вне конкурса и целевой прием"),
            new IdentifiableWrapper(2L, "на общих основаниях")
    );
    private String comissionTitle = "приемной комиссии";
    private Date protocolDate = new Date();
    private String protocolNumber;
    private Course course;
    private List<Course> courseList;
    private Date entranceDate;

    Map<Long, Long> paragraphCounter = new HashMap<Long, Long>();

    public Set<Long> getIds() {
        return ids;
    }

    public void setIds(Set<Long> ids) {
        this.ids = ids;
    }

    public CompensationType getCompensationType() {
        return compensationType;
    }

    public void setCompensationType(CompensationType compensationType) {
        this.compensationType = compensationType;
    }

    public IdentifiableWrapper getConkurs() {
        return conkurs;
    }

    public void setConkurs(IdentifiableWrapper conkurs) {
        this.conkurs = conkurs;
    }

    public List<IdentifiableWrapper> getConkursList() {
        return conkursList;
    }

    public void setConkursList(List<IdentifiableWrapper> conkursList) {
        this.conkursList = conkursList;
    }

    public String getComissionTitle() {
        return comissionTitle;
    }

    public void setComissionTitle(String comissionTitle) {
        this.comissionTitle = comissionTitle;
    }

    public Date getProtocolDate() {
        return protocolDate;
    }

    public void setProtocolDate(Date protocolDate) {
        this.protocolDate = protocolDate;
    }

    public String getProtocolNumber() {
        return protocolNumber;
    }

    public void setProtocolNumber(String protocolNumber) {
        this.protocolNumber = protocolNumber;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public List<Course> getCourseList() {
        return courseList;
    }

    public void setCourseList(List<Course> courseList) {
        this.courseList = courseList;
    }

    public Date getEntranceDate() {
        return entranceDate;
    }

    public void setEntranceDate(Date entranceDate) {
        this.entranceDate = entranceDate;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Map<Long, Long> getParagraphCounter() {
        return paragraphCounter;
    }

    public void setParagraphCounter(Map<Long, Long> paragraphCounter) {
        this.paragraphCounter = paragraphCounter;
    }

}
