package ru.tandemservice.narfu.dao.movestudent;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentbasermc.entity.OrderList;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentExtractStatesCodes;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;

import java.text.SimpleDateFormat;
import java.util.*;

public class OrderCopyDAO extends UniBaseDao implements IOrderCopyDAO {

    private static IOrderCopyDAO instance;

    public static IOrderCopyDAO getInstance() {

        if (instance == null)
            instance = (IOrderCopyDAO) ApplicationRuntime.getBean(IOrderCopyDAO.class.getName());
        return instance;
    }

    @Override
    public void doCopyOrders() {

        List<OrderList> orders = getOrdersToCopy();

        if (!orders.isEmpty()) {
            for (OrderList order : orders) {
                getSession().saveOrUpdate(order);
            }
        }
    }

    private List<OrderList> getOrdersToCopy() {
        Map<Long, List<String>> uniqueKeyCache = getOrdersUniqueKeyCache();

        List<OrderList> orders = getOrders();

        List<OrderList> ordersToCopy = new ArrayList<>();

        if (!orders.isEmpty()) {
            for (OrderList order : orders) {
                Long sid = order.getStudent().getId();
                boolean isExist = false;
                if (uniqueKeyCache.containsKey(sid)) {
                    String uniqueKey = getUniqueOrderKey(order);
                    for (String key : uniqueKeyCache.get(sid)) {
                        if (uniqueKey.equals(key)) {
                            isExist = true;
                            break;
                        }
                    }
                }

                if (!isExist)
                    ordersToCopy.add(order);
            }
        }
        return ordersToCopy;
    }

    private List<OrderList> getOrders() {
        List<AbstractStudentExtract> extracts = getExtracts();
        List<OrderList> orders = new ArrayList<>();

        for (AbstractStudentExtract extract : extracts) {
            OrderList order = new OrderList();

            Student student = extract.getEntity();
            String orderNumber = "";
            Date orderDate = null;
            Date startDate = extract.getBeginDate();
            Date stopDate = extract.getEndDate();
            String comment = "";
            StudentExtractType studentExtractType = null;
            String text = "";

            if (extract.getParagraph() != null) {
                orderNumber = extract.getParagraph().getOrder().getNumber();
                orderDate = extract.getParagraph().getOrder().getCommitDate();
                text = extract.getParagraph().getOrder().getTextParagraph();

                if (extract instanceof ModularStudentExtract) {
                    studentExtractType = extract.getType();
                    comment = ((ModularStudentExtract) extract).getReasonFullTitle();
                }
                else if (extract instanceof ListStudentExtract) {
                    studentExtractType = ((StudentListOrder) extract.getParagraph().getOrder()).getType();
                    comment = ((StudentListOrder) extract.getParagraph().getOrder()).getReasonFullTitle();
                }
                else if (extract instanceof OtherStudentExtract) {
                    studentExtractType = extract.getType();
                    comment = "";
                }
            }

            order.setStudent(student);
            order.setOrderNumber(orderNumber);
            order.setType(studentExtractType);
            order.setOrderDate(orderDate);
            order.setOrderDateStart(startDate);
            order.setOrderDateStop(stopDate);
            order.setOrderDsk(comment);
            order.setOrderText(text);

            orders.add(order);
        }
        return orders;
    }

    private List<AbstractStudentExtract> getExtracts() {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(AbstractStudentExtract.class, "e")
                .where(DQLExpressions.eqValue(DQLExpressions.property(AbstractStudentExtract.state().code().fromAlias("e")), MovestudentExtractStatesCodes.CODE_6))
                .where(DQLExpressions.in(DQLExpressions.property(AbstractStudentExtract.type().code().fromAlias("e")), movestudentTransferOrderCodes))
                .where(DQLExpressions.eqValue(DQLExpressions.property(AbstractStudentExtract.entity().archival().fromAlias("e")), Boolean.FALSE))
                .column("e");

        return getList(dql);
    }

    private Map<Long, List<String>> getOrdersUniqueKeyCache() {
        Map<Long, List<String>> cacheMap = new HashMap<>();

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(OrderList.class, "ol")
                .where(DQLExpressions.in(
                        DQLExpressions.property(OrderList.type().code().fromAlias("ol")), movestudentTransferOrderCodes))
                .where(DQLExpressions.eqValue(DQLExpressions.property(OrderList.student().archival().fromAlias("ol")), Boolean.FALSE))
                .column("ol");

        List<OrderList> orders = getList(builder);

        for (OrderList order : orders) {
            Long sid = order.getStudent().getId();
            String uniqueKey = getUniqueOrderKey(order);
            if (cacheMap.containsKey(sid)) {
                cacheMap.get(sid).add(uniqueKey);
            }
            else {
                List<String> lst = new ArrayList<>();
                lst.add(uniqueKey);
                cacheMap.put(sid, lst);
            }
        }
        return cacheMap;
    }

    /**
     * уникальный ключ для сравнения с приказами, которые будем добавлять, таким
     * образом исключаем возможность добавить один и тот же приказ второй раз
     *
     * @param order
     *
     * @return
     */
    private String getUniqueOrderKey(OrderList order) {
        return new StringBuilder()
                .append(order.getStudent().getId())
                .append(order.getType().getCode())
                .append(order.getOrderNumber())
                .append(new SimpleDateFormat("dd-MM-yyyy").format(order
                                                                          .getOrderDate())).toString();
    }
}
