package ru.tandemservice.narfu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;

import java.util.Arrays;
import java.util.List;

/**
 * Остатки модуля unids
 *
 * @author vch
 */
public class MS_narfu_1x0x0_7to8 extends IndependentMigrationScript {


    // 1225, 1939, 2597
    private List<String> dropEntitys = Arrays.asList("UniDSSessionResultRep", "UniDSSessionResultByDisciplinesRep", "UniDSOrgUnitSessionResultRep");
    private List<String> dropDiscriminator = Arrays.asList("1225", "1939", "2597");
    private List<String> dropTable = Arrays.asList("UNIDSORGUNITSESSIONRESULTREP_T", "UniDSSessionResultRep_t", "UniDSSessionResultByDisciplinesRep_t", "NDSSSSNRSLTBYDSCPLNSRP_t");

    @Override
    public void run(DBTool tool) throws Exception
    {
        _migration(tool);

    }

    private void _migration(DBTool tool) throws Exception
    {
        for (String entity : dropEntitys) {
            tool.executeUpdate("delete from ENTITYCODE_S where NAME_P=? ", entity);
        }

        // из хранимых отчетов выкидываем все отчеты по старой сессии
        for (String table : dropTable) {
            if (tool.tableExists(table)) {

                String tbl_name = table.toLowerCase();
                tool.dropTable(tbl_name);
            }
        }

        // из хранимых отчетов выкидываем все отчеты по старой сессии
        for (String discrim : dropDiscriminator) {
            tool.executeUpdate("delete from storablereport_t where discriminator=? ", discrim);
        }

    }
}		