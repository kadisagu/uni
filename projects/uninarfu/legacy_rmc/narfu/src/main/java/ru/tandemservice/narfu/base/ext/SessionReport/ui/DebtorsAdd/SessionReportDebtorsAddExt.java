package ru.tandemservice.narfu.base.ext.SessionReport.ui.DebtorsAdd;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.IPresenterExtensionBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.DebtorsAdd.SessionReportDebtorsAdd;

@Configuration
public class SessionReportDebtorsAddExt extends BusinessComponentExtensionManager
{

    @Autowired
    public SessionReportDebtorsAdd sessionReportDebtorsAdd;

    @Bean
    public PresenterExtension presenterExtension()
    {
        IPresenterExtensionBuilder pi = presenterExtensionBuilder(sessionReportDebtorsAdd.presenterExtPoint());
        pi.addAddon(uiAddon("ui_addon", SessionReportDebtorsAddUIExt.class));
        return pi.create();
    }
}
