package ru.tandemservice.narfu.component.documents.d101.Add;

import ru.tandemservice.uni.component.documents.DocumentAddBase.IDocumentAddBaseDAO;

import java.util.Date;

public interface IDAO extends IDocumentAddBaseDAO<Model> {
    public Date getEndDate(Model model);
}

