package ru.tandemservice.narfu.base.ext.Person.ui.AddressEdit;

public class Model extends org.tandemframework.shared.person.base.bo.Person.ui.AddressEdit.Model {
    private String addressRegistrationInfo;

    public String getAddressRegistrationInfo() {
        return addressRegistrationInfo;
    }

    public void setAddressRegistrationInfo(String addressInfo) {
        this.addressRegistrationInfo = addressInfo;
    }
}
