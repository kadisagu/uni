package ru.tandemservice.narfu.component.person.AddressTempRegistrationEdit;

import java.util.Date;

public class Model extends org.tandemframework.shared.fias.base.bo.Fias.ui.AddressEdit.Model {

    private Date addressTempRegistrationStartDate;
    private Date addressTempRegistrationEndDate;
    private String addressTempRegistrationInfo;

    public String getAddressTempRegistrationInfo() {
        return addressTempRegistrationInfo;
    }

    public void setAddressTempRegistrationInfo(String addressTempRegistrationInfo) {
        this.addressTempRegistrationInfo = addressTempRegistrationInfo;
    }

    public Date getAddressTempRegistrationStartDate() {
        return addressTempRegistrationStartDate;
    }

    public void setAddressTempRegistrationStartDate(
            Date addressTempRegistrationStartDate)
    {
        this.addressTempRegistrationStartDate = addressTempRegistrationStartDate;
    }

    public Date getAddressTempRegistrationEndDate() {
        return addressTempRegistrationEndDate;
    }

    public void setAddressTempRegistrationEndDate(
            Date addressTempRegistrationEndDate)
    {
        this.addressTempRegistrationEndDate = addressTempRegistrationEndDate;
    }


}
