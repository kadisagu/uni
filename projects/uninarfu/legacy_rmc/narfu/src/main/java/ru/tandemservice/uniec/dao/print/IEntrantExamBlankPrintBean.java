package ru.tandemservice.uniec.dao.print;

import ru.tandemservice.uniecmarkextrmc.component.menu.MarkDistribution.Model;

public interface IEntrantExamBlankPrintBean {
    public byte[] generateEntrantExamBlank(Model model);

}
