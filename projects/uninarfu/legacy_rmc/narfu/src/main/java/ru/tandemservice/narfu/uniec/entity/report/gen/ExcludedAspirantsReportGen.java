package ru.tandemservice.narfu.uniec.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.narfu.uniec.entity.report.ExcludedAspirantsReport;
import ru.tandemservice.uni.entity.report.StorableReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сведения об аспирантах - гражданах РФ, обучавшихся за счет средств федерального бюджета и выбывших до окончания срока обучения
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ExcludedAspirantsReportGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.narfu.uniec.entity.report.ExcludedAspirantsReport";
    public static final String ENTITY_NAME = "excludedAspirantsReport";
    public static final int VERSION_HASH = 1026973567;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ExcludedAspirantsReportGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ExcludedAspirantsReportGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ExcludedAspirantsReport.class;
        }

        public T newInstance()
        {
            return (T) new ExcludedAspirantsReport();
        }
    }
    private static final Path<ExcludedAspirantsReport> _dslPath = new Path<ExcludedAspirantsReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ExcludedAspirantsReport");
    }
            

    public static class Path<E extends ExcludedAspirantsReport> extends StorableReport.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return ExcludedAspirantsReport.class;
        }

        public String getEntityName()
        {
            return "excludedAspirantsReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
