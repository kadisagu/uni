package ru.tandemservice.narfu.unisession.component.orgunit.SessionBulletinListTab;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.node.IRtfElement;
import ru.tandemservice.unisession.dao.print.INarfuSessionBulletinPrintDAO;
import ru.tandemservice.unisession.dao.print.NarfuSessionBulletinPrintDAO;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uni.util.ReportRenderer;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisession.component.orgunit.SessionBulletinListTab.Model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Controller
        extends
        ru.tandemservice.unisession.component.orgunit.SessionBulletinListTab.Controller
{

    Map<Object, String> propertyTitleMap = new HashMap<Object, String>() {
        {
            put("comission", "Преподаватель");
            put("performDate", "Дата сдачи");
        }
    };


    @Override
    public void onClickPrintSelected(IBusinessComponent component) {
        Model model = getModel(component);
        List<Long> selectedIds = UniBaseUtils.getIdList(model.getDataSource().getSelectedEntities());

        if (selectedIds.isEmpty())
            return;

        List selectedEntities = model.getDataSource().getSelectedEntities();

        Map<String, List<IEntity>> docNumbers = ((IDAO) getDao()).getNullPropertyDocs(selectedEntities, "comission", "performDate");


        if (!docNumbers.isEmpty()) {
            printErrorLog(docNumbers);
            return;
        }

        NarfuSessionBulletinPrintDAO.ReportFile result = INarfuSessionBulletinPrintDAO.instance.get().printBulletinListNarfu(selectedIds);

        Integer temporaryId = PrintReportTemporaryStorage.registerTemporaryPrintForm(result.getContent(), result.getName());
        activateInRoot(component, new ComponentActivator(
                "ru.tandemservice.uni.component.reports.PrintReport",
                new ParametersMap().add("id", temporaryId).add("zip", Boolean.FALSE).add("extension", result.isZip() ? "zip" : "rtf")));

    }

    private void printErrorLog(Map<String, List<IEntity>> docNumbers) {
        String title = "Номера документов с незаполненным полем: ";

        RtfDocument printFailPerformDateDocs = RtfBean.getElementFactory().createRtfDocument();

        List<IRtfElement> elementList = printFailPerformDateDocs.getElementList();

        for (Entry<String, List<IEntity>> entry : docNumbers.entrySet()) {

            String property = entry.getKey();
            elementList.add(RtfBean.getElementFactory().createRtfText(title + propertyTitleMap.get(property)));

            elementList.add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            for (IEntity doc : entry.getValue()) {
                elementList.add(RtfBean.getElementFactory().createRtfText(String.valueOf(doc.getProperty("number"))));
                elementList.add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));

            }
            elementList.add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
        }

        BusinessComponentUtils.downloadDocument(new ReportRenderer("Ошибки.rtf", printFailPerformDateDocs), true);
    }


    @Override
    public void onClickPrint(IBusinessComponent component) {
        Long bulletinId = component.getListenerParameter();

        for (IEntity iEntity : getModel(component).getDataSource().getEntityList()) {
            if (iEntity.getId().equals(bulletinId)) {

                if (((IDAO) getDao()).isNullProperty(iEntity, "performDate"))
                    throw new ApplicationException("Для того чтобы распечатать ведомость должна быть заполнена дата сдачи");

                if (((IDAO) getDao()).isNullProperty(iEntity, "comission"))
                    throw new ApplicationException("Для того чтобы распечатать ведомость должен быть указан преподаватель");
            }
        }

        super.onClickPrint(component);
    }
}
