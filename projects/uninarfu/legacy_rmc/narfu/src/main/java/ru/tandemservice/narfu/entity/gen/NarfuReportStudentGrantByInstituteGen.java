package ru.tandemservice.narfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.narfu.entity.NarfuReportStudentGrantByInstitute;
import ru.tandemservice.uni.entity.report.StorableReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Список отчетов «Контингент студентов по институтам (академическая стипендия)»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class NarfuReportStudentGrantByInstituteGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.narfu.entity.NarfuReportStudentGrantByInstitute";
    public static final String ENTITY_NAME = "narfuReportStudentGrantByInstitute";
    public static final int VERSION_HASH = -2124510893;
    private static IEntityMeta ENTITY_META;

    public static final String P_EDUCATION_YEAR = "educationYear";
    public static final String P_EDUCATION_YEAR_PART = "educationYearPart";
    public static final String P_MONTH = "month";
    public static final String P_DEVELOP_FORM = "developForm";
    public static final String P_EXECUTOR = "executor";

    private String _educationYear;     // Учебный год
    private String _educationYearPart;     // Часть года
    private String _month;     // Месяц
    private String _developForm;     // Форма освоения
    private String _executor;     // Исполнитель

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Учебный год.
     */
    @Length(max=255)
    public String getEducationYear()
    {
        return _educationYear;
    }

    /**
     * @param educationYear Учебный год.
     */
    public void setEducationYear(String educationYear)
    {
        dirty(_educationYear, educationYear);
        _educationYear = educationYear;
    }

    /**
     * @return Часть года.
     */
    @Length(max=255)
    public String getEducationYearPart()
    {
        return _educationYearPart;
    }

    /**
     * @param educationYearPart Часть года.
     */
    public void setEducationYearPart(String educationYearPart)
    {
        dirty(_educationYearPart, educationYearPart);
        _educationYearPart = educationYearPart;
    }

    /**
     * @return Месяц.
     */
    @Length(max=255)
    public String getMonth()
    {
        return _month;
    }

    /**
     * @param month Месяц.
     */
    public void setMonth(String month)
    {
        dirty(_month, month);
        _month = month;
    }

    /**
     * @return Форма освоения.
     */
    @Length(max=255)
    public String getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения.
     */
    public void setDevelopForm(String developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Исполнитель.
     */
    @Length(max=255)
    public String getExecutor()
    {
        return _executor;
    }

    /**
     * @param executor Исполнитель.
     */
    public void setExecutor(String executor)
    {
        dirty(_executor, executor);
        _executor = executor;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof NarfuReportStudentGrantByInstituteGen)
        {
            setEducationYear(((NarfuReportStudentGrantByInstitute)another).getEducationYear());
            setEducationYearPart(((NarfuReportStudentGrantByInstitute)another).getEducationYearPart());
            setMonth(((NarfuReportStudentGrantByInstitute)another).getMonth());
            setDevelopForm(((NarfuReportStudentGrantByInstitute)another).getDevelopForm());
            setExecutor(((NarfuReportStudentGrantByInstitute)another).getExecutor());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends NarfuReportStudentGrantByInstituteGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) NarfuReportStudentGrantByInstitute.class;
        }

        public T newInstance()
        {
            return (T) new NarfuReportStudentGrantByInstitute();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                    return obj.getEducationYear();
                case "educationYearPart":
                    return obj.getEducationYearPart();
                case "month":
                    return obj.getMonth();
                case "developForm":
                    return obj.getDevelopForm();
                case "executor":
                    return obj.getExecutor();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "educationYear":
                    obj.setEducationYear((String) value);
                    return;
                case "educationYearPart":
                    obj.setEducationYearPart((String) value);
                    return;
                case "month":
                    obj.setMonth((String) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((String) value);
                    return;
                case "executor":
                    obj.setExecutor((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                        return true;
                case "educationYearPart":
                        return true;
                case "month":
                        return true;
                case "developForm":
                        return true;
                case "executor":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                    return true;
                case "educationYearPart":
                    return true;
                case "month":
                    return true;
                case "developForm":
                    return true;
                case "executor":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                    return String.class;
                case "educationYearPart":
                    return String.class;
                case "month":
                    return String.class;
                case "developForm":
                    return String.class;
                case "executor":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<NarfuReportStudentGrantByInstitute> _dslPath = new Path<NarfuReportStudentGrantByInstitute>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "NarfuReportStudentGrantByInstitute");
    }
            

    /**
     * @return Учебный год.
     * @see ru.tandemservice.narfu.entity.NarfuReportStudentGrantByInstitute#getEducationYear()
     */
    public static PropertyPath<String> educationYear()
    {
        return _dslPath.educationYear();
    }

    /**
     * @return Часть года.
     * @see ru.tandemservice.narfu.entity.NarfuReportStudentGrantByInstitute#getEducationYearPart()
     */
    public static PropertyPath<String> educationYearPart()
    {
        return _dslPath.educationYearPart();
    }

    /**
     * @return Месяц.
     * @see ru.tandemservice.narfu.entity.NarfuReportStudentGrantByInstitute#getMonth()
     */
    public static PropertyPath<String> month()
    {
        return _dslPath.month();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.narfu.entity.NarfuReportStudentGrantByInstitute#getDevelopForm()
     */
    public static PropertyPath<String> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Исполнитель.
     * @see ru.tandemservice.narfu.entity.NarfuReportStudentGrantByInstitute#getExecutor()
     */
    public static PropertyPath<String> executor()
    {
        return _dslPath.executor();
    }

    public static class Path<E extends NarfuReportStudentGrantByInstitute> extends StorableReport.Path<E>
    {
        private PropertyPath<String> _educationYear;
        private PropertyPath<String> _educationYearPart;
        private PropertyPath<String> _month;
        private PropertyPath<String> _developForm;
        private PropertyPath<String> _executor;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Учебный год.
     * @see ru.tandemservice.narfu.entity.NarfuReportStudentGrantByInstitute#getEducationYear()
     */
        public PropertyPath<String> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new PropertyPath<String>(NarfuReportStudentGrantByInstituteGen.P_EDUCATION_YEAR, this);
            return _educationYear;
        }

    /**
     * @return Часть года.
     * @see ru.tandemservice.narfu.entity.NarfuReportStudentGrantByInstitute#getEducationYearPart()
     */
        public PropertyPath<String> educationYearPart()
        {
            if(_educationYearPart == null )
                _educationYearPart = new PropertyPath<String>(NarfuReportStudentGrantByInstituteGen.P_EDUCATION_YEAR_PART, this);
            return _educationYearPart;
        }

    /**
     * @return Месяц.
     * @see ru.tandemservice.narfu.entity.NarfuReportStudentGrantByInstitute#getMonth()
     */
        public PropertyPath<String> month()
        {
            if(_month == null )
                _month = new PropertyPath<String>(NarfuReportStudentGrantByInstituteGen.P_MONTH, this);
            return _month;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.narfu.entity.NarfuReportStudentGrantByInstitute#getDevelopForm()
     */
        public PropertyPath<String> developForm()
        {
            if(_developForm == null )
                _developForm = new PropertyPath<String>(NarfuReportStudentGrantByInstituteGen.P_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Исполнитель.
     * @see ru.tandemservice.narfu.entity.NarfuReportStudentGrantByInstitute#getExecutor()
     */
        public PropertyPath<String> executor()
        {
            if(_executor == null )
                _executor = new PropertyPath<String>(NarfuReportStudentGrantByInstituteGen.P_EXECUTOR, this);
            return _executor;
        }

        public Class getEntityClass()
        {
            return NarfuReportStudentGrantByInstitute.class;
        }

        public String getEntityName()
        {
            return "narfuReportStudentGrantByInstitute";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
