package ru.tandemservice.narfu.uniec.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.narfu.uniec.entity.report.OrgUnitAspirantReport;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сведения о приеме в аспирантуру за счет средств федерального бюджета»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OrgUnitAspirantReportGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.narfu.uniec.entity.report.OrgUnitAspirantReport";
    public static final String ENTITY_NAME = "orgUnitAspirantReport";
    public static final int VERSION_HASH = 545479454;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_EDUCATION_LEVEL_HIGH_SCHOOL = "educationLevelHighSchool";

    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private EducationLevelsHighSchool _educationLevelHighSchool;     // Параметры выпуска студентов по направлению подготовки (НПв)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Параметры выпуска студентов по направлению подготовки (НПв).
     */
    public EducationLevelsHighSchool getEducationLevelHighSchool()
    {
        return _educationLevelHighSchool;
    }

    /**
     * @param educationLevelHighSchool Параметры выпуска студентов по направлению подготовки (НПв).
     */
    public void setEducationLevelHighSchool(EducationLevelsHighSchool educationLevelHighSchool)
    {
        dirty(_educationLevelHighSchool, educationLevelHighSchool);
        _educationLevelHighSchool = educationLevelHighSchool;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof OrgUnitAspirantReportGen)
        {
            setEnrollmentCampaign(((OrgUnitAspirantReport)another).getEnrollmentCampaign());
            setEducationLevelHighSchool(((OrgUnitAspirantReport)another).getEducationLevelHighSchool());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OrgUnitAspirantReportGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OrgUnitAspirantReport.class;
        }

        public T newInstance()
        {
            return (T) new OrgUnitAspirantReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "educationLevelHighSchool":
                    return obj.getEducationLevelHighSchool();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "educationLevelHighSchool":
                    obj.setEducationLevelHighSchool((EducationLevelsHighSchool) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                        return true;
                case "educationLevelHighSchool":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return true;
                case "educationLevelHighSchool":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "educationLevelHighSchool":
                    return EducationLevelsHighSchool.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OrgUnitAspirantReport> _dslPath = new Path<OrgUnitAspirantReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OrgUnitAspirantReport");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.narfu.uniec.entity.report.OrgUnitAspirantReport#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Параметры выпуска студентов по направлению подготовки (НПв).
     * @see ru.tandemservice.narfu.uniec.entity.report.OrgUnitAspirantReport#getEducationLevelHighSchool()
     */
    public static EducationLevelsHighSchool.Path<EducationLevelsHighSchool> educationLevelHighSchool()
    {
        return _dslPath.educationLevelHighSchool();
    }

    public static class Path<E extends OrgUnitAspirantReport> extends StorableReport.Path<E>
    {
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private EducationLevelsHighSchool.Path<EducationLevelsHighSchool> _educationLevelHighSchool;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.narfu.uniec.entity.report.OrgUnitAspirantReport#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Параметры выпуска студентов по направлению подготовки (НПв).
     * @see ru.tandemservice.narfu.uniec.entity.report.OrgUnitAspirantReport#getEducationLevelHighSchool()
     */
        public EducationLevelsHighSchool.Path<EducationLevelsHighSchool> educationLevelHighSchool()
        {
            if(_educationLevelHighSchool == null )
                _educationLevelHighSchool = new EducationLevelsHighSchool.Path<EducationLevelsHighSchool>(L_EDUCATION_LEVEL_HIGH_SCHOOL, this);
            return _educationLevelHighSchool;
        }

        public Class getEntityClass()
        {
            return OrgUnitAspirantReport.class;
        }

        public String getEntityName()
        {
            return "orgUnitAspirantReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
