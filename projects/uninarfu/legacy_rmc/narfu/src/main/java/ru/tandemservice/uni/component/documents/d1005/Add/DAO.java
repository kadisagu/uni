package ru.tandemservice.uni.component.documents.d1005.Add;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.aspirantrmc.entity.ScientificResearch;
import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseDAO;
import ru.tandemservice.uni.component.documents.DocumentAddBase.IDocumentAddBaseDAO;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Date;
import java.util.List;


@SuppressWarnings("rawtypes")
public class DAO extends DocumentAddBaseDAO<Model> implements IDocumentAddBaseDAO<Model> {

    @Override
    public void prepare(Model model)
    {

        super.prepare(model);


        model.setFormingDate(new Date());
        model.setStudentTitleStr(PersonManager.instance().declinationDao().getDeclinationFIO(model.getStudent().getPerson().getIdentityCard(), GrammaCase.DATIVE));
        model.setStudentTitleStrIminit(PersonManager.instance().declinationDao().getDeclinationFIO(model.getStudent().getPerson().getIdentityCard(), GrammaCase.NOMINATIVE));
        model.setScientificWorkTheme(getSciTheme(model.getStudent()));

    }

    private String getSciTheme(Student student)
    {
        //В описании сущности поле "студент" не уникально, используем обычный DQL запрос для получения списка тем научных работ
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(ScientificResearch.class, "sr");
        dql.where(DQLExpressions.eq(
                DQLExpressions.property(ScientificResearch.student().fromAlias("sr")),
                DQLExpressions.value(student)
        ));
        dql.column("sr.title");
        List<String> lst = dql.createStatement(getSession()).list();
        if (lst != null && lst.size() > 0)
            return lst.get(0);
        else
            return "";
    }

}
