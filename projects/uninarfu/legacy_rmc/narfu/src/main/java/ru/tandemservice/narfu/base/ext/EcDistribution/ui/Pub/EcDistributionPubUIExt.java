package ru.tandemservice.narfu.base.ext.EcDistribution.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.narfu.base.ext.EcDistribution.EcDistributionManagerExt;
import ru.tandemservice.uniec.base.bo.EcDistribution.EcDistributionManager;
import ru.tandemservice.uniec.base.bo.EcDistribution.ui.Pub.EcDistributionPubUI;

public class EcDistributionPubUIExt extends UIAddon {

    private EcDistributionPubUI parentPresenter = null;


    public EcDistributionPubUIExt(IUIPresenter presenter, String name, String componentId) {
        super(presenter, name, componentId);

        parentPresenter = (EcDistributionPubUI) presenter;
    }


    public void onClickRtfPrintNarfu()
    {
        //refresh
        parentPresenter.getUserContext().getCurrentComponent().refresh();
        try {
            byte[] data = EcDistributionManagerExt.instance().dao().getRtfPrintNarfu(parentPresenter.getDistributionId());

            String fileName = EcDistributionManager.instance().dao().getDistributionFileName(parentPresenter.getDistribution());

            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName(fileName + ".rtf").document(data), false);
        }
        finally {
            onComponentRefresh();
        }
    }


    /**
     * предупреждения о возможном неправильном распределении
     */
    public void onClickRtfPrintNarfuWarningDistribution()
    {
        //refresh
        parentPresenter.getUserContext().getCurrentComponent().refresh();
        try {
            byte[] data = EcDistributionManagerExt.instance().daoWarningDistribution().getRtf(parentPresenter.getDistributionId());

            String fileName = EcDistributionManager.instance().dao().getDistributionFileName(parentPresenter.getDistribution()) + "_warning";

            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName(fileName + ".rtf").document(data), false);
        }
        finally {
            onComponentRefresh();
        }
    }


}
