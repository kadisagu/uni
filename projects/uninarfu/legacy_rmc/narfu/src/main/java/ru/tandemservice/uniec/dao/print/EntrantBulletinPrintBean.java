package ru.tandemservice.uniec.dao.print;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.base.util.NumberSpellingUtil;
import ru.tandemservice.uniecmarkextrmc.component.menu.MarkDistribution.DisciplineWrapper;
import ru.tandemservice.uniecmarkextrmc.component.menu.MarkDistribution.EntrantDisciplineMarkWrapper;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.DisciplineDateSettingISTU;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EntrantBulletinPrintBean extends UniBaseDao implements IEntrantBulletinPrintBean {
    @Override
    public RtfDocument generateEntrantBulletin(List<EntrantDisciplineMarkWrapper> wrappers, DisciplineWrapper discipline) {

        IScriptItem templateDocument = getCatalogItem(UniecScriptItem.class, "entrantBulletin");
        RtfDocument template = (new RtfReader()).read(templateDocument.getCurrentTemplate());
        RtfDocument result = RtfBean.getElementFactory().createRtfDocument();
        result.setHeader(template.getHeader());
        result.setSettings(template.getSettings());
        Map<DisciplineDateSettingISTU, List<EntrantDisciplineMarkWrapper>> map = new HashMap<>();


        for (EntrantDisciplineMarkWrapper item : wrappers) {
            if (item.getExamPassDisciplineISTU() == null || item.getExamPassDisciplineISTU().getDateSetting() == null) {
                if (!map.containsKey(null))
                    map.put(null, new ArrayList<EntrantDisciplineMarkWrapper>());
                map.get(null).add(item);
                continue;
            }
            DisciplineDateSettingISTU ds = item.getExamPassDisciplineISTU().getDateSetting();
            if (!map.containsKey(ds)) {
                map.put(ds, new ArrayList<EntrantDisciplineMarkWrapper>());
            }
            map.get(ds).add(item);
        }

        for (Map.Entry<DisciplineDateSettingISTU, List<EntrantDisciplineMarkWrapper>> entry : map.entrySet()) {
            List<String[]> lines = new ArrayList<>();
            RtfDocument retVal = template.getClone();
            List<BulletinData> list = new ArrayList<>();

            for (EntrantDisciplineMarkWrapper item : entry.getValue()) {
                String strMark;
                if (item.getMark() != null && !item.getMark().equals("н"))
                    strMark = NumberSpellingUtil.spellNumberMasculineGender(Long.parseLong(item.getMark()));
                else
                    strMark = "";

                list.add(new BulletinData(
                        item.getExamPassDiscipline().getEntrantExamList().getNumber()
                        , item.getEntrantFIO()
                        , item.getExamPassMarkIstu().getWorkCipher()
                        , item.getMark() == null ? "" : item.getMark().equals("н") ? "" : item.getMark()
                        , strMark));
            }

            int counter = 1;
            for (BulletinData item : list) {
                List<String> cells = new ArrayList<String>();
                cells.add("" + counter);
                cells.add(item.getNumber());
                cells.add(item.getFio());
                String wc = "";
                if (item.getWorkChipher() != null)
                    wc = item.getWorkChipher();
                cells.add(wc);
                String cm = "";
                if (item.getCipherMark() != null)
                    cm = item.getCipherMark();
                cells.add(cm);
                String sm = "";
                if (item.getStringMark() != null)
                    sm = item.getStringMark();
                cells.add(sm);
                lines.add(cells.toArray(new String[]{}));
                counter++;
            }
            RtfTableModifier tablemod = new RtfTableModifier();
            tablemod.put("tt", lines.toArray(new String[][]{}));
            tablemod.modify(retVal);

            RtfInjectModifier injectModifier = new RtfInjectModifier();
            injectModifier.put("Disc", discipline.getDiscipline().getTitle());
            if (entry.getKey() == null) {
                injectModifier.put("date", "\"___\" ___________ 20__г.");
                injectModifier.put("place", "_____________________________");
            }
            else {
                injectModifier.put("date", RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_ONLY.format(entry.getKey().getPassDate()));
                if (entry.getKey().getClassroom() == null)
                    injectModifier.put("place", "_____________________________");
                else
                    injectModifier.put("place", entry.getKey().getClassroom());

            }
            injectModifier.modify(retVal);
            result.getElementList().addAll(retVal.getElementList());
            result.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
        }

        return result;
    }

    private class BulletinData {
        private String number;
        private String fio;
        private String workChipher;
        private String cipherMark;
        private String stringMark;

        BulletinData(String number, String fio, String workChipher, String cipherMark, String stringMark) {
            this.number = number;
            this.fio = fio;
            this.workChipher = workChipher;
            this.cipherMark = cipherMark;
            this.stringMark = stringMark;
        }

        public String getNumber() {
            return number;
        }

        public String getFio() {
            return fio;
        }

        public String getWorkChipher() {
            return workChipher;
        }

        public String getCipherMark() {
            return cipherMark;
        }

        public String getStringMark() {
            return stringMark;
        }
    }

}


