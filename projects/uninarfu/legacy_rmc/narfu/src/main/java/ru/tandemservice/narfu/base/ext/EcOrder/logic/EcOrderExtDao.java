package ru.tandemservice.narfu.base.ext.EcOrder.logic;

import org.hibernate.Session;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.narfu.base.ext.EcOrder.ui.HighShortParAddEdit.EcOrderHighShortParAddEditUI;
import ru.tandemservice.narfu.entity.EntrHighShortExtractExt;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;
import ru.tandemservice.uniec.base.bo.EcOrder.logic.EcOrderDao;
import ru.tandemservice.uniec.base.bo.EcOrder.logic.IEnrollmentExtractFactory;
import ru.tandemservice.uniec.dao.IPrintableEnrollmentExtract;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentParagraph;
import ru.tandemservice.uniec.entity.orders.EntrHighShortExtract;

import java.util.*;

public class EcOrderExtDao extends EcOrderDao {

    @Override
    public void saveOrUpdateEnrollmentParagraph(EnrollmentOrder order,
                                                EnrollmentParagraph paragraph,
                                                IEnrollmentExtractFactory extractFactory,
                                                boolean enrollParagraphPerEduOrgUnit,
                                                Collection<PreliminaryEnrollmentStudent> preStudents,
                                                PreliminaryEnrollmentStudent manager, OrgUnit formativeOrgUnit,
                                                EducationLevelsHighSchool educationLevelsHighSchool,
                                                DevelopForm developForm, Course course, String groupTitle)
    {

        boolean isAddPar = paragraph == null;

        super.saveOrUpdateEnrollmentParagraph(order, paragraph, extractFactory,
                                              enrollParagraphPerEduOrgUnit, preStudents, manager, formativeOrgUnit,
                                              educationLevelsHighSchool, developForm, course, groupTitle);

        if (isAddPar && Arrays.asList(UniecDefines.ORDER_TYPE_STUDENT_BUDGET, UniecDefines.ORDER_TYPE_STUDENT_BUDGET_SHORT, UniecDefines.ORDER_TYPE_STUDENT_CONTRACT, UniecDefines.ORDER_TYPE_STUDENT_CONTRACT_SHORT).contains(order.getType().getCode()))
            sortParagraphList(order);

        if (order.getType().getCode().equals(UniecDefines.ORDER_TYPE_SECOND_HIGH)) {
            saveExtractExt(paragraph, ((EcOrderHighShortParAddEditUI) extractFactory).getEduLevSpecHS());
        }
    }

    public void saveExtractExt(EnrollmentParagraph paragraph, EducationOrgUnit educationOrgUnit) {

        Session session = getSession();

        List<EntrHighShortExtract> extractList = getList(EntrHighShortExtract.class, EntrHighShortExtract.paragraph(), paragraph, new String[0]);
        //удалим сохраненные ранее расширение
        new DQLDeleteBuilder(EntrHighShortExtractExt.class)
                .where(DQLExpressions.in(DQLExpressions.property(EntrHighShortExtractExt.extract()), extractList));

        for (EntrHighShortExtract extract : extractList) {
            EntrHighShortExtractExt extractExt = new EntrHighShortExtractExt();
            extractExt.setExtract(extract);
            extractExt.setEducationOrgUnit(educationOrgUnit);
            session.save(extractExt);
        }

        session.flush();
    }

    public void sortParagraphList(
            EnrollmentOrder order)
    {

        Session session = getSession();
        session.refresh(order);

        List<EnrollmentParagraph> parList = (List<EnrollmentParagraph>) order.getParagraphList();

        /**
         * Параграфы сортировать по коду квалификации, коду направления, названию направления, сроку освоения
         */
        Collections.sort(parList, new Comparator<EnrollmentParagraph>() {
            @Override
            public int compare(EnrollmentParagraph o1, EnrollmentParagraph o2) {

                EducationOrgUnit e1 = o1.getFirstExtract().getEntity().getEducationOrgUnit();
                EducationOrgUnit e2 = o2.getFirstExtract().getEntity().getEducationOrgUnit();

                int result = UniBaseUtils.compare(e1.getEducationLevelHighSchool().getEducationLevel().getQualification().getCode(), e2.getEducationLevelHighSchool().getEducationLevel().getQualification().getCode(), true);
                if (result != 0)
                    return result;

                result = UniBaseUtils.compare(e1.getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject().getSubjectCode(), e2.getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject().getSubjectCode(), true);
                if (result != 0)
                    return result;

                result = UniBaseUtils.compare(e1.getEducationLevelHighSchool().getEducationLevel().getTitle(), e2.getEducationLevelHighSchool().getEducationLevel().getTitle(), true);
                if (result != 0)
                    return result;

                result = UniBaseUtils.compare(e1.getDevelopPeriod().getTitle(), e2.getDevelopPeriod().getTitle(), true);
                if (result != 0)
                    return result;

                return -UniBaseUtils.compare(o1.getNumber(), o2.getNumber(), true);
            }
        });

        //поменяем номера параграфов с учетом сортировки
        /*
		 * для смены номера используем тандемовский метод  EcOrderManager.instance().dao().updatePriority(),
		 * т.к. если меняем в цикле у всех парагравоф номера, то срабатывает какой-то листенер, который возвращает номера обратно
		 */
        for (EnrollmentParagraph par : parList) {
            int newNumber = parList.indexOf(par) + 1;
            if (newNumber != par.getNumber())
                EcOrderManager.instance().dao().updatePriority(par.getId(), (newNumber - par.getNumber()));
        }

    }

    @Override
    public RtfDocument getExtractDocument(IPrintableEnrollmentExtract extract)
    {
        if (extract instanceof EnrollmentExtract) {
            return EcOrderPrintDAO.getInstance().getDownloadPrintExtract((EnrollmentExtract) extract);
        }
       return super.getExtractDocument(extract);
    }

}
