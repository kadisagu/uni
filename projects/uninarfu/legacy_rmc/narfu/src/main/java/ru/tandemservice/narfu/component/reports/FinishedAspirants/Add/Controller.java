package ru.tandemservice.narfu.component.reports.FinishedAspirants.Add;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.rtf.RtfDocumentRenderer;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.narfu.uniec.entity.report.FinishedAspirantsReport;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = component.getModel();
        getDao().prepare(model);
    }

    public void onClickApply(IBusinessComponent component) {
        Model model = component.getModel();
        FinishedAspirantsReport report = getDao().createReport(model);

        DatabaseFile content = report.getContent();
        if (content == null || content.getContent() == null)
            throw new ApplicationException("Файл отчета пуст");
        BusinessComponentUtils.downloadDocument(new RtfDocumentRenderer(content.getFilename(), content.getContent()), true);

        deactivate(component);
    }
}
