/*
Чук Рамэк 10-2010
справка вызов для Мами
*/

package ru.tandemservice.uni.component.documents.d1005.Add;

import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseModel;

import java.util.Date;

@SuppressWarnings("rawtypes")
public class Model extends DocumentAddBaseModel {
    private Date _formingDate;

    private String _studentTitleStr;

    private String _studentTitleStrIminit;

    private Date _dateFrom;
    private Date _dateTo;

    private String scientificWorkTheme;

    // Getters & Setters

    public String getScientificWorkTheme() {
        return scientificWorkTheme;
    }

    public void setScientificWorkTheme(String scientificWorkTheme) {
        this.scientificWorkTheme = scientificWorkTheme;
    }

    public Date getFormingDate()
    {
        return _formingDate;
    }

    public void setFormingDate(Date formingDate)
    {
        _formingDate = formingDate;
    }

    public String getStudentTitleStr()
    {
        return _studentTitleStr;
    }

    public void setStudentTitleStr(String studentTitleStr)
    {
        _studentTitleStr = studentTitleStr;
    }


    public String getStudentTitleStrIminit()
    {
        return _studentTitleStrIminit;
    }

    public void setStudentTitleStrIminit(String studentTitleStr)
    {
        _studentTitleStrIminit = studentTitleStr;
    }

    public Date getDateFrom()
    {
        return _dateFrom;
    }

    public void setDateFrom(Date eduFrom)
    {
        _dateFrom = eduFrom;
    }

    public Date getDateTo()
    {
        return _dateTo;
    }

    public void setDateTo(Date eduTo)
    {
        _dateTo = eduTo;
    }


}
