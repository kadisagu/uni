package ru.tandemservice.narfu.utils;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.common.ITitled;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XMLPack {

    public static void MakeICatalogItem
            (
                    String elementName
                    , Document doc
                    , Element parentElement
                    , ICatalogItem item
            )
    {
        Element el_item = doc.createElement(elementName);
        parentElement.appendChild(el_item);

        String code = "";
        String title = "";

        if (item != null) {
            code = item.getCode();
            title = item.getTitle();
        }

        Attr attr_code = doc.createAttribute("code");
        attr_code.setValue(code);
        el_item.setAttributeNode(attr_code);

        Attr attr_title = doc.createAttribute("title");
        attr_title.setValue(title);
        el_item.setAttributeNode(attr_title);

    }


    public static void MakeITitledItem
            (
                    String elementName
                    , Document doc
                    , Element parentElement
                    , ITitled item
            )
    {
        Element el_item = doc.createElement(elementName);
        parentElement.appendChild(el_item);

        String title = "";

        if (item != null)
            title = item.getTitle();

        Attr attr_title = doc.createAttribute("title");
        attr_title.setValue(title);
        el_item.setAttributeNode(attr_title);

    }


}
