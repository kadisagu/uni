package ru.tandemservice.narfu.uniec.component.order.EnrollmentOrderPub.EnrollmentParagraphPersonalInfoDialog;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.rtf.RtfDocumentRenderer;
import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.narfu.base.ext.EcOrder.EcOrderManagerExt;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.List;


public class Controller extends AbstractBusinessController<IDAO, Model> {


    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        getDao().prepare(getModel(component));
    }

    public void onClickApply(IBusinessComponent component) {

        Model model = getModel(component);

        List<IAbstractParagraph<IAbstractOrder>> paragraphsList = model.getParagraphsList();

        if (!paragraphsList.isEmpty()) {
            RtfDocument downloadPrintPersonalInfo = EcOrderManagerExt.instance().daoExt().getDownloadPrintParagraphPersonalInfo(paragraphsList);
            BusinessComponentUtils.downloadDocument(new RtfDocumentRenderer(
                    "Обложки ЛД.rtf", downloadPrintPersonalInfo), false);
        }
        deactivate(component);
    }

}
