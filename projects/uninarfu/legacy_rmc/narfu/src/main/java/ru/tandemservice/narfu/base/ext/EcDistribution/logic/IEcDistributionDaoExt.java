package ru.tandemservice.narfu.base.ext.EcDistribution.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;


public interface IEcDistributionDaoExt extends INeedPersistenceSupport {


    public byte[] getRtfPrintNarfu(Long long1);
}
