package ru.tandemservice.narfu.base.ext.Person.ui.Edit;

import ru.tandemservice.movestudentrmc.entity.PersonNARFU;

public class Model extends org.tandemframework.shared.person.base.bo.Person.ui.Edit.Model {

    private PersonNARFU personNARFU;

    public PersonNARFU getPersonNARFU() {
        return personNARFU;
    }

    public void setPersonNARFU(PersonNARFU personNARFU) {
        this.personNARFU = personNARFU;
    }

}
