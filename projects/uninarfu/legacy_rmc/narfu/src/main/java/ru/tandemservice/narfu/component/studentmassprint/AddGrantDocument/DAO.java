package ru.tandemservice.narfu.component.studentmassprint.AddGrantDocument;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.narfu.entity.NarfuGrantDocument;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        model.setStudent(getNotNull(Student.class, model.getStudent().getId()));
        model.setFio(model.getStudent().getPerson().getFullFio());
        model.setNarfuGrantDocuments(getList(NarfuGrantDocument.class, NarfuGrantDocument.document().student(), model.getStudent()));
    }


    @Override
    public void prepareListDataSource(Model model) {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(NarfuGrantDocument.class, "doc")
                .where(DQLExpressions.eqValue(DQLExpressions.property(NarfuGrantDocument.document().student().id().fromAlias("doc")), model.getStudent().getId()));
        model.getDataSource().setCountRow(getList(builder).size());
        UniUtils.createPage(model.getDataSource(), builder, getSession());
    }
}
