package ru.tandemservice.narfu.dao.movestudent;

import com.google.common.collect.ImmutableList;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.uni.dao.IUniBaseDao;

import java.util.List;


public interface IOrderCopyDAO extends IUniBaseDao {

	/** Коды типов приказов о переводе */
    List<String> movestudentTransferOrderCodes = ImmutableList.of(
			// Сборные приказы
			StudentExtractTypeCodes.DEVELOP_FORM_TRANSFER_MODULAR_ORDER, StudentExtractTypeCodes.COMPENSATION_TYPE_TRANSFER_MODULAR_ORDER,
			StudentExtractTypeCodes.EDU_TYPE_TRANSFER_MODULAR_ORDER, StudentExtractTypeCodes.TRANSFER_EXT_MODULAR_ORDER,
			StudentExtractTypeCodes.TRANSFER_TO_INDIVIDUAL_PLAN_MODULAR_ORDER, StudentExtractTypeCodes.TRANSFER_TO_INDIVIDUAL_GRAPH_MODULAR_ORDER,
			StudentExtractTypeCodes.TRANSFER_GROUP_MODULAR_ORDER, StudentExtractTypeCodes.TRANSFER_COURSE_MODULAR_ORDER,
			StudentExtractTypeCodes.TRANSFER_EDU_LEVEL_MODULAR_ORDER, StudentExtractTypeCodes.TRANSFER_FORMATIVE_CHANGE_MODULAR_ORDER,
			StudentExtractTypeCodes.TRANSFER_TERRITORIAL_CHANVE_MODULAR_ORDER, StudentExtractTypeCodes.TRANSFER_TERRITORIAL_MODULAR_ORDER,
			StudentExtractTypeCodes.TRANSFER_DEV_CONDITIONS_AND_PERIOD_MODULAR_ORDER, StudentExtractTypeCodes.TRANSFER_DEV_CODTIDIONS_MODULAR_ORDER,
			StudentExtractTypeCodes.TRANSFER_PROFILE_GROUP_REASSIGN_MODULAR_ORDER, StudentExtractTypeCodes.TRANSFER_BUDGET_COMPENSATION_TYPE_MODULAR_ORDER,
			StudentExtractTypeCodes.TRANSFER_PROFILE_MODULAR_ORDER, StudentExtractTypeCodes.TRANSFER_COURSE_CHANGE_TERRITORIAL_MODULAR_ORDER,

			// Списочные приказы
			StudentExtractTypeCodes.COURSE_TRANSFER_LIST_ORDER, StudentExtractTypeCodes.TRANSFER_NEXT_COURSE_LIST_EXTRACT,
			StudentExtractTypeCodes.COURSE_NO_CHANGE_LIST_EXTRACT, StudentExtractTypeCodes.COURSE_TRANSFER_DEBTORS_LIST_EXTRACT,
			StudentExtractTypeCodes.TRANSFER_TERRITORIAL_LIST_ORDER, StudentExtractTypeCodes.TRANSFER_TERRITORIAL_LIST_EXTRACT,
			StudentExtractTypeCodes.TRANSFER_NEXT_COURSE_EDU_TYPE_LIST_ORDER, StudentExtractTypeCodes.TRANSFER_NEXT_COURSE_EDU_TYPE_LIST_EXTRACT,
			StudentExtractTypeCodes.TERRITORIAL_COURSE_TRANSFER_LIST_ORDER, StudentExtractTypeCodes.TERRITORIAL_COURSE_TRANSFER_LIST_EXTRACT,
			StudentExtractTypeCodes.GROUP_TRANSFER_LIST_ORDER, StudentExtractTypeCodes.GROUP_TRANSFER_LIST_EXTRACT);

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    void doCopyOrders();
}
