package ru.tandemservice.narfu.component.studentmassprint.documents.mpd1004.Add;

import org.tandemframework.shared.organization.base.entity.OrgUnit;

public interface IDAO extends
        ru.tandemservice.unirmc.component.studentmassprint.documents.Base.IDAO<Model>
{
    public String getDirectorFIO(OrgUnit orgUnit, String postCode);

    public String getPostCode(String managerPost);
}
