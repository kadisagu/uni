package ru.tandemservice.narfu.uniec.component.menu.examGroup.logic2.ExamGroupList;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.uniec.dao.print.IExamGroupSheetListPrintBean;
import ru.tandemservice.uni.util.ReportRenderer;
import ru.tandemservice.uniec.component.menu.examGroup.abstractExamGroupList.ExamGroupListModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Controller extends ru.tandemservice.uniec.component.menu.examGroup.logic2.ExamGroupList.Controller {

    @Override
    public void onClickPrintSheet(IBusinessComponent component) {
        List<Long> ids = Collections.singletonList((Long) component.getListenerParameter());
        downloadPrintExamGroupList2(component, ids);
    }

    @Override
    public void onClickPrintSheetList(IBusinessComponent component) {
        ExamGroupListModel model = (ExamGroupListModel) getModel(component);
        List<Long> ids = new ArrayList<Long>();
        for (Object entityObject : model.getDataSource().getEntityList()) {
            IEntity entity = (IEntity) entityObject;
            ids.add(entity.getId());
        }

        downloadPrintExamGroupList2(component, ids);
    }

    private void downloadPrintExamGroupList2(IBusinessComponent component, List<Long> ids) {
        IExamGroupSheetListPrintBean bean = (IExamGroupSheetListPrintBean) ApplicationRuntime.getBean("narfuExamGroupSheetListPrintBean");
        RtfDocument report = bean.generateExamGroupSheetReport(ids);
        BusinessComponentUtils.downloadDocument(new ReportRenderer("examGroupSheet.rtf", report), true);
    }
}
