package ru.tandemservice.narfu.base.bo.ReportPersonNarfu.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.block.BlockListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import ru.tandemservice.narfu.base.bo.ReportPersonNarfu.ui.Add.block.personnarfuData.PersonnarfuDataBlock;

@Configuration
public class ReportPersonNarfuAdd
        extends BusinessComponentManager
{

    // sub tab panels
    public static final String PERSONNARFU_TAB_PANEL = "personnarfuTabPanel";

    // second level tabs
    public static final String PERSONNARFU_DATA_TAB = "personnarfuDataTab";
    public static final String PERSONNARFU_PRINT_TAB = "personnarfuPrintTab";

    // tab block lists
    public static final String PERSONNARFU_DATA_BLOCK_LIST = "personnarfuDataBlockList";

    // block names
    public static final String PERSONNARFU_DATA = "personnarfuData";

    // print tab block lists
    public static final String PERSONNARFU_SCHEET_BLOCK_LIST = "personnarfuScheetBlockListExtPoint";


    @Bean
    public TabPanelExtPoint personnarfuTabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(PERSONNARFU_TAB_PANEL)
                .addTab(htmlTab(PERSONNARFU_DATA_TAB, "PersonnarfuDataTab"))
                .addTab(htmlTab(PERSONNARFU_PRINT_TAB, "PersonnarfuPrintTab"))
                .create();
    }


    @Bean
    public BlockListExtPoint personnarfuDataBlockListExtPoint()
    {
        return blockListExtPointBuilder(PERSONNARFU_DATA_BLOCK_LIST)
                .addBlock(htmlBlock(PERSONNARFU_DATA, "block/personnarfuData/PersonnarfuData"))
                .create();
    }

    @Bean
    public BlockListExtPoint personnarfuScheetBlockListExtPoint()
    {
        return blockListExtPointBuilder(PERSONNARFU_SCHEET_BLOCK_LIST)
                .addBlock(htmlBlock(PERSONNARFU_SCHEET_BLOCK_LIST, "print/personnarfu/Template"))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler needDormitoryOnReceiptPeriodDSHandler()
    {
        return PersonnarfuDataBlock.createNeedDormitoryOnReceiptPeriodDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler needDormitoryOnStudingPeriodDSHandler()
    {
        return PersonnarfuDataBlock.createNeedDormitoryOnStudingPeriodDS(getName());
    }


    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()

                .addDataSource(selectDS(PersonnarfuDataBlock.NEED_DORMITORY_ON_RECIPT_PERIOD_DS, needDormitoryOnReceiptPeriodDSHandler()))
                .addDataSource(selectDS(PersonnarfuDataBlock.NEED_DORMITORY_ON_STUDING_PERIOD_DS, needDormitoryOnStudingPeriodDSHandler()))
                .create();
    }

}
