package ru.tandemservice.narfu.uniec.component.order.EnrollmentOrderAddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.narfu.entity.EnrollmentOrderNARFU;
import ru.tandemservice.uniec.UniecDefines;

public class Model extends
        ru.tandemservice.uniec.component.order.EnrollmentOrderAddEdit.Model
{

    private EnrollmentOrderNARFU orderNarfu;
    private ISelectModel courseListModel;

    public boolean isOrderTypeUseProtocol()
    {
        if (getOrder() == null || getOrder().getType() == null)
            return false;
        else {
            return (getOrder().getType().getCode().equals(UniecDefines.ORDER_TYPE_TARGET_BUDGET)
                    || getOrder().getType().getCode().equals(UniecDefines.ORDER_TYPE_STUDENT_BUDGET)
                    || getOrder().getType().getCode().equals(UniecDefines.ORDER_TYPE_STUDENT_CONTRACT)
                    || getOrder().getType().getCode().equals(UniecDefines.ORDER_TYPE_LISTENER_CONTRACT)
                    // Переопределенные нами типы приказа "О зачислении на 2 и последующие курсы (бюджет/договор)"
                    || getOrder().getType().getCode().equals(UniecDefines.ORDER_TYPE_STUDENT_BUDGET_SHORT)
                    || getOrder().getType().getCode().equals(UniecDefines.ORDER_TYPE_STUDENT_CONTRACT_SHORT));
        }
    }

    public boolean isOrderTypeAfterFirstCourse() {
        if (getOrder() == null || getOrder().getType() == null)
            return false;
        else {
            // Переопределенные нами типы приказа "О зачислении на 2 и последующие курсы (бюджет/договор)"
            return (getOrder().getType().getCode().equals(UniecDefines.ORDER_TYPE_STUDENT_BUDGET_SHORT)
                    || getOrder().getType().getCode().equals(UniecDefines.ORDER_TYPE_STUDENT_CONTRACT_SHORT));
        }

    }

    public EnrollmentOrderNARFU getOrderNarfu() {
        return orderNarfu;
    }

    public void setOrderNarfu(EnrollmentOrderNARFU orderNarfu) {
        this.orderNarfu = orderNarfu;
    }

    public ISelectModel getCourseListModel() {
        return courseListModel;
    }

    public void setCourseListModel(ISelectModel courseListModel) {
        this.courseListModel = courseListModel;
    }

}
