package ru.tandemservice.narfu.component.settings.FormingOrgUnit2Commission.Edit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.narfu.entity.FormingOrgUnit2Commission;

@Input({
        @Bind(key = "enrollmentCampaignId", binding = "enrollmentCampaignId"),
        @Bind(key = "formingOrgUnitId", binding = "formingOrgUnitId")
})
public class Model {

    private Long enrollmentCampaignId;
    private Long formingOrgUnitId;
    private FormingOrgUnit2Commission formingOrgUnit2Commission;
    private ISelectModel commissionSelectModel;

    public FormingOrgUnit2Commission getFormingOrgUnit2Commission() {
        return formingOrgUnit2Commission;
    }

    public void setFormingOrgUnit2Commission(FormingOrgUnit2Commission formingOrgUnit2Commission) {
        this.formingOrgUnit2Commission = formingOrgUnit2Commission;
    }

    public ISelectModel getCommissionSelectModel() {
        return commissionSelectModel;
    }

    public void setCommissionSelectModel(ISelectModel commissionSelectModel) {
        this.commissionSelectModel = commissionSelectModel;
    }

    public Long getEnrollmentCampaignId() {
        return enrollmentCampaignId;
    }

    public void setEnrollmentCampaignId(Long enrollmentCampaignId) {
        this.enrollmentCampaignId = enrollmentCampaignId;
    }

    public Long getFormingOrgUnitId() {
        return formingOrgUnitId;
    }

    public void setFormingOrgUnitId(Long formingOrgUnitId) {
        this.formingOrgUnitId = formingOrgUnitId;
    }
}
