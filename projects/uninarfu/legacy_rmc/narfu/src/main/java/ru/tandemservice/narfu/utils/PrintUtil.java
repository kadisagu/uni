package ru.tandemservice.narfu.utils;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.uni.entity.catalog.EducationLevels;

public class PrintUtil {

    public static String getEducationOrgUnitLabel(EducationLevels currentEduLevel, GrammaCase grammaCase)
    {
        StringBuilder sb = new StringBuilder("");
        if (currentEduLevel.getLevelType().isBachelor() || currentEduLevel.getLevelType().isMaster()) {

            String profileTitle = getProfileTitle(grammaCase);
            if (currentEduLevel.getLevelType().isProfile()) {
                // профиль
                EducationLevels parentLevel = currentEduLevel.getParentLevel();
                String parentLevelTypeStr = "";
                if (parentLevel.getLevelType().isGos3() && parentLevel.getLevelType().isBachelor())
                    parentLevelTypeStr = "профиль";
                else if (parentLevel.getLevelType().isGos3() && parentLevel.getLevelType().isMaster())
                    parentLevelTypeStr = "программа";
                else if (parentLevel.getLevelType().isGos2() && parentLevel.getLevelType().isBachelor())
                    parentLevelTypeStr = "область";
                else if (parentLevel.getLevelType().isGos2() && parentLevel.getLevelType().isMaster())
                    parentLevelTypeStr = "программа";

                sb.append(profileTitle)
                        .append(" ")
                        .append(parentLevel.getEduProgramSubject().getSubjectCode())
                        .append(" \u00AB")
                        .append(currentEduLevel.getParentLevel().getTitle())
                        .append("\u00BB")
                        .append(", ")
                        .append(parentLevelTypeStr)
                        .append(" ")
                        .append(" \u00AB")
                        .append(currentEduLevel.getTitle()).append("\u00BB");
            }
            else {
                // не профиль
                sb.append(profileTitle)
                        .append(" ")
                        .append(currentEduLevel.getEduProgramSubject().getSubjectCode()).append(" \u00AB")
                        .append(currentEduLevel.getTitle()).append("\u00BB");
            }
        }
        else {
            // специализации
            String specializationTitle = getSpecializationTitle(grammaCase);
            if (currentEduLevel.getLevelType().isSpecialization()) {
                sb.append(specializationTitle)
                        .append(" ")
                        .append(currentEduLevel.getEduProgramSubject().getSubjectCode())
                        .append(" \u00AB")
                        .append(currentEduLevel.getParentLevel().getTitle())
                        .append("\u00BB")
                        .append(" специализация ")
                        .append("\u00AB")
                        .append(currentEduLevel.getTitle()).append("\u00BB");
            }
            else {
                sb.append(specializationTitle)
                        .append(" ")
                        .append(currentEduLevel.getEduProgramSubject().getSubjectCode()).append(" \u00AB")
                        .append(currentEduLevel.getTitle()).append("\u00BB");
            }
        }
        return sb.toString();
    }


    private static String getProfileTitle(GrammaCase grammaCase) {

        if (grammaCase.equals(GrammaCase.NOMINATIVE))
            return "направление подготовки";
        if (grammaCase.equals(GrammaCase.GENITIVE))
            return "направления подготовки";
        if (grammaCase.equals(GrammaCase.DATIVE))
            return "направлению подготовки";

        return "направления подготовки";
    }

    private static String getSpecializationTitle(GrammaCase grammaCase) {
        if (grammaCase.equals(GrammaCase.NOMINATIVE))
            return "специальность";
        if (grammaCase.equals(GrammaCase.GENITIVE))
            return "специальности";

        return "специальность";
    }
}
