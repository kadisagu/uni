package ru.tandemservice.narfu.component.studentmassprint.documents.mpd1004;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.narfu.component.documents.d1004.Add.DAO;
import ru.tandemservice.narfu.component.studentmassprint.documents.mpd1004.Add.Model;
import ru.tandemservice.unirmc.component.studentmassprint.AbstractStudentMassPrint;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Arrays;
import java.util.Date;

public class MPD1004
        extends AbstractStudentMassPrint<Model>
{


    @Override
    public boolean isNeedNewPage()
    {
        return true;
    }

    @Override
    public void iniModelForStudent(Student student, int documentNumber,
                                   Model model)
    {
        super.iniModelForStudent(student, documentNumber, model);

        // можно получить DAO контроллера и отинячить
        DAO dao = new DAO();

        dao.setHibernateTemplate(getHibernateTemplate());
        dao.setSessionFactory(getSessionFactory());


//      в дао одиночной печати много статичных данных, которые перезаписывают данные из формы
//		dao.prepare(model);

        model.setReasonList(Arrays.asList(
                new IdentifiableWrapper(0L, "промежуточной аттестации"),
                new IdentifiableWrapper(1L, "Подготовки и защиты выпускной квалификационной работы и сдачи итоговых государственных экзаменов")
        ));
        model.setReason(model.getReasonList().get(0));
        model.setFormingDate(new Date());
        model.setStudentTitleStr(PersonManager.instance().declinationDao().getDeclinationFIO(model.getStudent().getPerson().getIdentityCard(), GrammaCase.DATIVE).toUpperCase());
        model.setStudentTitleStrIminit(PersonManager.instance().declinationDao().getDeclinationFIO(model.getStudent().getPerson().getIdentityCard(), GrammaCase.NOMINATIVE).toUpperCase());
        model.setCourseList(DevelopGridDAO.getCourseList());
        model.setCourse(model.getStudent().getCourse());
//        OrgUnit formativeOrgUnit = model.getStudent().getEducationOrgUnit().getFormativeOrgUnit();
//        model.setTelephone(formativeOrgUnit.getPhone());
    }

}
