package ru.tandemservice.narfu.uniec.component.report.DailyEntrantList;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {

    public void createReport(Model model);
}
