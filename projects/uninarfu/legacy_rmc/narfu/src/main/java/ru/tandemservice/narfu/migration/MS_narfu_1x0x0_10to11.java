package ru.tandemservice.narfu.migration;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.constraints.IDBConstraint;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.util.Collection;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_narfu_1x0x0_10to11 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.10"),
                        new ScriptDependency("org.tandemframework.shared", "1.1.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.1.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // модуль aspirantrmc отключен - удаляем все его сущности

        // убедиться, что модуль и в самом деле удален
        if (ApplicationRuntime.hasModule("aspirantrmc"))
            throw new RuntimeException("Module 'aspirantrmc' is not deleted");

        MigrationUtils.removeModuleFromVersion_s(tool, "aspirantrmc");

        // удалить сущность outerParticipant
        if (tool.tableExists("outerparticipant_t")) {
            if (tool.constraintExists("outerparticipant_t", "fk_id_outerparticipant")) {
                tool.dropConstraint("outerparticipant_t", "fk_id_outerparticipant");
            }
            // удалить записи из базовых таблиц
            tool.deleteRowsFromParentTables("outerparticipant_t", "personrole_t");

            // удалить таблицу
            tool.dropTable("outerparticipant_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("outerParticipant");

        }

        // удалить сущность educationLevelScientific
        {
            // таблицы у сущности нет, но нужно удалить строки из таблицы educationLevel, где catalogcode_p = "educationLevelScientific"
            tool.executeUpdate("delete from educationlevels_t where catalogcode_p='educationLevelScientific'", new Object[0]);
            // удалить код сущности
            tool.entityCodes().delete("educationLevelScientific");

        }

        // удалить сущность typePublication
        if (tool.tableExists("typepublication_t")) {
            // удалить таблицу
            tool.dropTable("typepublication_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("typePublication");

        }

        // удалить сущность typeParticipation
        if (tool.tableExists("typeparticipation_t")) {
            // удалить таблицу
            tool.dropTable("typeparticipation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("typeParticipation");

        }

        // удалить сущность typeInvention
        if (tool.tableExists("typeinvention_t")) {
            // удалить таблицу
            tool.dropTable("typeinvention_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("typeInvention");

        }

        // удалить сущность typeAction
        if (tool.tableExists("typeaction_t")) {
            // удалить таблицу
            tool.dropTable("typeaction_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("typeAction");

        }

        // удалить сущность statusMission
        if (tool.tableExists("statusmission_t")) {
            // удалить таблицу
            tool.dropTable("statusmission_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("statusMission");

        }

        // удалить сущность statusAction
        if (tool.tableExists("statusaction_t")) {
            // удалить таблицу
            tool.dropTable("statusaction_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("statusAction");

        }

        // удалить сущность staffDissertationSoviet
        if (tool.tableExists("staffdissertationsoviet_t")) {
            // удалить таблицу
            tool.dropTable("staffdissertationsoviet_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("staffDissertationSoviet");

        }

        // удалить сущность scientificResearchParticipantType
        if (tool.tableExists("scntfcrsrchprtcpnttyp_t")) {
            // удалить таблицу
            tool.dropTable("scntfcrsrchprtcpnttyp_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("scientificResearchParticipantType");

        }

        // удалить сущность scientificResearch
        if (tool.tableExists("scientificresearch_t")) {
            // удалить таблицу
            tool.dropTable("scientificresearch_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("scientificResearch");

        }

        // удалить сущность scientificPublication
        if (tool.tableExists("scientificpublication_t")) {
            // удалить таблицу
            tool.dropTable("scientificpublication_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("scientificPublication");

        }

        // удалить сущность scientificPatent
        if (tool.tableExists("scientificpatent_t")) {
            // удалить таблицу
            tool.dropTable("scientificpatent_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("scientificPatent");

        }

        // удалить сущность scienceParticipant
        if (tool.tableExists("scienceparticipant_t")) {
            // удалить таблицу
            tool.dropTable("scienceparticipant_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("scienceParticipant");

        }

        // удалить сущность relEducationLevelScientificDissertationSoviet
        if (tool.tableExists("rledctnlvlscntfcdssrttnsvt_t")) {
            // удалить таблицу
            tool.dropTable("rledctnlvlscntfcdssrttnsvt_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("relEducationLevelScientificDissertationSoviet");

        }

        // удалить сущность relDissertationSovietStaffPost
        if (tool.tableExists("rldssrttnsvtstffpst_t")) {
            // удалить таблицу
            tool.dropTable("rldssrttnsvtstffpst_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("relDissertationSovietStaffPost");

        }

        // удалить сущность publishingHouse
        if (tool.tableExists("publishinghouse_t")) {
            // удалить таблицу
            tool.dropTable("publishinghouse_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("publishingHouse");

        }

        // удалить сущность practiceWork2Research
        if (tool.tableExists("practicework2research_t")) {
            // удалить таблицу
            tool.dropTable("practicework2research_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("practiceWork2Research");

        }

        // удалить сущность postDissertationSoviet
        if (tool.tableExists("postdissertationsoviet_t")) {
            // удалить таблицу
            tool.dropTable("postdissertationsoviet_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("postDissertationSoviet");

        }

        // удалить сущность leadOrganization2Research
        if (tool.tableExists("leadorganization2research_t")) {
            // удалить таблицу
            tool.dropTable("leadorganization2research_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("leadOrganization2Research");

        }

        // удалить сущность financing
        if (tool.tableExists("financing_t")) {
            // удалить таблицу
            tool.dropTable("financing_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("financing");

        }

        // удалить сущность dissertationSoviet
        if (tool.tableExists("dissertationsoviet_t")) {
            // удалить таблицу
            tool.dropTable("dissertationsoviet_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dissertationSoviet");

        }

        // удалить сущность aspActionSupervisor
        if (tool.tableExists("aspactionsupervisor_t")) {
            // удалить таблицу
            tool.dropTable("aspactionsupervisor_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("aspActionSupervisor");

        }

        // удалить сущность aspActionOriginator
        if (tool.tableExists("aspactionoriginator_t")) {
            // удалить таблицу
            tool.dropTable("aspactionoriginator_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("aspActionOriginator");

        }

        // удалить сущность aspAction
        if (tool.tableExists("aspaction_t")) {
            // удалить таблицу
            tool.dropTable("aspaction_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("aspAction");

        }

        Collection<IDBConstraint> constraints = tool.getTableConstraints("educationlevels_t");
        for (IDBConstraint c : constraints) {
            String sqlConstraint = c.getConstraintSQL(tool.getDialect());
        }


    }
}