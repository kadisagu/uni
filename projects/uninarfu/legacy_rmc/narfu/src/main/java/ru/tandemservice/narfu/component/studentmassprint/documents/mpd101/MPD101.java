package ru.tandemservice.narfu.component.studentmassprint.documents.mpd101;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.narfu.component.documents.d101.Add.DAO;
import ru.tandemservice.narfu.component.studentmassprint.documents.mpd101.Add.Model;
import ru.tandemservice.unirmc.component.studentmassprint.AbstractStudentMassPrint;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.StudentDocument;

public class MPD101
        extends AbstractStudentMassPrint<Model>
{


    @Override
    public boolean isNeedNewPage()
    {
        return true;
    }

    @Override
    public void iniModelForStudent(Student student, int documentNumber,
                                   Model model)
    {
        super.iniModelForStudent(student, documentNumber, model);

        // можно получить DAO контроллера и отинячить
        DAO dao = new DAO();

        dao.setHibernateTemplate(getHibernateTemplate());
        dao.setSessionFactory(getSessionFactory());

        dao.prepare(model);


    }

    @Override
    protected void applyDocumentNumberFilters(MQBuilder builder, String alias,
                                              Student student)
    {

        if (student == null)
            return;

        builder.add(MQExpression.eq(alias, StudentDocument.student().educationOrgUnit().developForm(), student.getEducationOrgUnit().getDevelopForm()));

    }


}
