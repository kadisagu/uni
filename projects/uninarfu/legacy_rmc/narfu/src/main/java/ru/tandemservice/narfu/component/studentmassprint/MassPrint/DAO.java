package ru.tandemservice.narfu.component.studentmassprint.MassPrint;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.narfu.entity.DocumentType2StudentCategory;
import ru.tandemservice.unirmc.component.studentmassprint.MassPrint.WrapStudentDocument;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.StudentDocumentType;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;

import java.util.ArrayList;
import java.util.List;

public class DAO extends ru.tandemservice.unirmc.component.studentmassprint.MassPrint.DAO implements IDAO {

    @Override
    public void prepare(ru.tandemservice.unirmc.component.studentmassprint.MassPrint.Model model) {
        super.prepare(model);
        Model myModel = (Model) model;
        myModel.setStudentCategorySelectModel(new LazySimpleSelectModel<>(getCatalogItemList(StudentCategory.class)));
        // только то, что поддерживает множественную печать
        model.setStudentTemplateDocumentListModel(new LazySimpleSelectModel<>(getTemplateDocsList(myModel)));
    }

    private List<WrapStudentDocument> getTemplateDocsList(Model model) {
        List<WrapStudentDocument> retVal = new ArrayList<>();

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(StudentDocumentType.class, "sdt")
                .fetchPath(DQLJoinType.inner, StudentDocumentType.scriptItem().fromAlias("sdt"), "tmp")
                .column("sdt")
                .where(DQLExpressions.in(DQLExpressions.property("sdt"), getStudentDocumentTypeList(model)));

        List<StudentDocumentType> lst = builder.createStatement(getSession()).<StudentDocumentType>list();

        for (StudentDocumentType std : lst) {
            // критерий того, что можно добавить в список вывода - наличие bean-а печати
            // по индексу документа
            String mpBeanName = "mpd" + std.getIndex();

            if (ApplicationRuntime.containsBean(mpBeanName)) {
                WrapStudentDocument w = new WrapStudentDocument();
                w.setId(std.getId());
                w.setTitle(std.getTitle());
                w.setIndex(std.getIndex());

                retVal.add(w);
            }
        }
        return retVal;
    }

    protected List<StudentDocumentType> getStudentDocumentTypeList(ru.tandemservice.unirmc.component.studentmassprint.MassPrint.Model model) {
        Object studentCategory = model.getSettings().get("studentCategory");

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(DocumentType2StudentCategory.class, "rel")
                .column(DocumentType2StudentCategory.studentDocumentType().fromAlias("rel").s())
                .predicate(DQLPredicateType.distinct);

        FilterUtils.applySelectFilter(dql, "rel", DocumentType2StudentCategory.studentCategory(), studentCategory);

        return getList(dql);
    }

    @Override
    protected void addAdditionalRestrictions(
            ru.tandemservice.unirmc.component.studentmassprint.MassPrint.Model model,
            DQLSelectBuilder builder, String alias)
    {

        Object studentCategory = model.getSettings().get("studentCategory");
        FilterUtils.applySelectFilter(builder, alias, Student.studentCategory(), studentCategory);
    }
}
