package ru.tandemservice.narfu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;

public class MS_narfu_1x0x0_18to19 extends IndependentMigrationScript {

    @Override
    public void run(DBTool tool) throws Exception {
        //удаляем старые печатные шаблоны RM#3619
        if (tool.tableExists("templatedocument_t")) {
            tool.executeUpdate("delete from templatedocument_t where code_p in ('narfuEntrantRequestVPO', 'narfuEntrantRequestSPO')");
        }
    }

}
