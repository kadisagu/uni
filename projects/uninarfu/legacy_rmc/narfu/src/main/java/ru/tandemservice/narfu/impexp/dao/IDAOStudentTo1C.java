package ru.tandemservice.narfu.impexp.dao;

import org.tandemframework.core.util.cache.SpringBeanCache;
import org.w3c.dom.Document;

import javax.xml.parsers.ParserConfigurationException;

/**
 * Импорт данных по студентам в 1c
 *
 * @author vch
 */
public interface IDAOStudentTo1C {

    String LOCKER_NAME = "daoStudentTo1C.lock";

    SpringBeanCache<IDAOStudentTo1C> instance = new SpringBeanCache<>("impexpStudentTo1C");

    Document getStudentInfoXML() throws ParserConfigurationException;

    void packStudentsInfoToFile() throws Exception;
}
