package ru.tandemservice.narfu.component.settings.EnrCampaignDevelopFormDates.List;

import ru.tandemservice.narfu.entity.EnrCampaignDevelopFormDates;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDao;

import java.util.Collections;
import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        super.prepare(model);
    }

    @Override
    public void prepareListDataSource(Model model) {
        List<EnrCampaignDevelopFormDates> result = getList(EnrCampaignDevelopFormDates.class);
        Collections.reverse(result);
        UniUtils.createPage(model.getDataSource(), result);
    }

}
