package ru.tandemservice.narfu.component.reports.StudentsByUGS;

import jxl.write.WritableWorkbook;
import ru.tandemservice.narfu.component.reports.StudentsByUGS.Add.Model;
import ru.tandemservice.narfu.entity.NarfuReportStudentsByUGS;

import java.io.ByteArrayOutputStream;

public class ReportData {
    protected WritableWorkbook workbook;
    protected ByteArrayOutputStream workbookStream;
    protected NarfuReportStudentsByUGS report;
    Model model;


    public WritableWorkbook getWorkbook() {
        return workbook;
    }

    public void setWorkbook(WritableWorkbook workbook) {
        this.workbook = workbook;
    }

    public ByteArrayOutputStream getWorkbookStream() {
        return workbookStream;
    }

    public void setWorkbookStream(ByteArrayOutputStream workbookStream) {
        this.workbookStream = workbookStream;
    }

    public NarfuReportStudentsByUGS getReport() {
        return report;
    }

    public void setReport(NarfuReportStudentsByUGS report) {
        this.report = report;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }
}
