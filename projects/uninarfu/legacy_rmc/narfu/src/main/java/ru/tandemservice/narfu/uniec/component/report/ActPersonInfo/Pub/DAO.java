package ru.tandemservice.narfu.uniec.component.report.ActPersonInfo.Pub;

import ru.tandemservice.narfu.uniec.entity.report.NarfuActPersonInfo;
import ru.tandemservice.uni.dao.UniDao;

public class DAO extends UniDao<Model>
        implements IDAO
{

    public DAO()
    {
    }

    public void prepare(Model model)
    {
        model.setReport((NarfuActPersonInfo) getNotNull(NarfuActPersonInfo.class, model.getReport().getId()));
    }

}
