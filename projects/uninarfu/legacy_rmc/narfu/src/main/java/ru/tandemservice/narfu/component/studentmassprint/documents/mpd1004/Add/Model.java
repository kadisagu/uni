package ru.tandemservice.narfu.component.studentmassprint.documents.mpd1004.Add;

import org.tandemframework.core.component.Input;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unirmc.component.studentmassprint.IModelStudentDocument;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.List;

@Input(keys = {"docIndex", "lstStudents", "number"}
        , bindings = {"docIndex", "lstStudents", "number"})
public class Model
        extends ru.tandemservice.narfu.component.documents.d1004.Add.Model
        implements IModelStudentDocument
{
    private boolean needSaveDocument;
    private boolean doSame;


    private List<Student> lstStudents;
    private int docIndex;
    private List<String> managerPostList;
    private List<String> managerList;
    private ISelectModel employeePostListModel;
    private EmployeePost employeePost;
    private Student firstStudent;
    private boolean displayWarning = false;
    private String warningMessage;

    public EmployeePost getEmployeePost() {
        return employeePost;
    }

    public void setEmployeePost(EmployeePost employeePost) {
        this.employeePost = employeePost;
    }

    public ISelectModel getEmployeePostListModel() {
        return employeePostListModel;
    }

    public void setEmployeePostListModel(ISelectModel employeePostListModel) {
        this.employeePostListModel = employeePostListModel;
    }

    public boolean isDisplayWarning() {
        return displayWarning;
    }

    public void setDisplayWarning(boolean displayWarning) {
        this.displayWarning = displayWarning;
    }

    public String getWarningMessage() {
        return warningMessage;
    }

    public void setWarningMessage(String warningMessage) {
        this.warningMessage = warningMessage;
    }

    public Student getFirstStudent() {
        return firstStudent;
    }

    public void setFirstStudent(Student firstStudent) {
        this.firstStudent = firstStudent;
    }

    public List<String> getManagerPostList() {
        return managerPostList;
    }

    public void setManagerPostList(List<String> managerPostList) {
        this.managerPostList = managerPostList;
    }

    public List<String> getManagerList() {
        return managerList;
    }

    public void setManagerList(List<String> managerList) {
        this.managerList = managerList;
    }

    public void setLstStudents(List<Student> lstStudents) {
        this.lstStudents = lstStudents;
    }

    public List<Student> getLstStudents() {
        return lstStudents;
    }

    public void setDocIndex(int docIndex) {
        this.docIndex = docIndex;
    }

    public int getDocIndex() {
        return docIndex;
    }

    @Override
    public void setNeedSaveDocument(boolean needSave) {

        this.needSaveDocument = needSave;
    }

    @Override
    public boolean isNeedSaveDocument() {
        return this.needSaveDocument;
    }

    @Override
    public void setDoSame(boolean doSame) {
        this.doSame = doSame;
    }

    @Override
    public boolean isDoSame() {
        return this.doSame;
    }
}
