package ru.tandemservice.uniec.dao.print;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uniecmarkextrmc.component.menu.MarkDistribution.EntrantDisciplineMarkWrapper;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.DisciplineDateSettingISTU;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniecmarkextrmc.component.menu.MarkDistribution.Model;

import java.util.ArrayList;
import java.util.List;

public class EntrantExamBlankPrintBean extends UniBaseDao implements IEntrantExamBlankPrintBean {
    @Override
    public byte[] generateEntrantExamBlank(Model model) {
        List<EntrantDisciplineMarkWrapper> wrps = model.getDataSourceExt().getEntityList();
        IScriptItem templateDocument = getCatalogItem(UniecScriptItem.class, "entrantExamBlank");
        String rtfString = new String(templateDocument.getCurrentTemplate());
        int indexOfHeader = rtfString.indexOf("\\ltrrow");
        int indexOfFooter = rtfString.indexOf("{\\*\\themedata");

        String rtfFooter = rtfString.substring(indexOfFooter, rtfString.length());
        String rtfHeader = rtfString.substring(0, indexOfHeader);
        String rtfPageTemplate = rtfString.substring(indexOfHeader, indexOfFooter);

        RtfDocument template = (new RtfReader()).read(templateDocument.getCurrentTemplate());
        String result = "";
        result += rtfHeader;


        List<EntrantDisciplineMarkWrapper> notValid = getEmptyWorkCodes(model);
        if (notValid.size() > 0) {
            RtfDocument retVal = RtfBean.getElementFactory().createRtfDocument();
            String errorText = "";
            errorText += "Печать невозможна, для следующих абитуриентов не задан шифр работы:";
            errorText += " \\par\\par ";
            for (EntrantDisciplineMarkWrapper item : notValid)
                errorText += "- " + item.getEntrantFIO() + " \\par ";
            retVal.setHeader(template.getHeader());
            retVal.setSettings(template.getSettings());
            IRtfText rtfText = RtfBean.getElementFactory().createRtfText(errorText);
            rtfText.setRaw(true);
            retVal.getElementList().add(rtfText);
            return RtfUtil.toByteArray(retVal);

        }
        else {
            for (EntrantDisciplineMarkWrapper item : wrps) {
                String currentBlank = new String();


                Person person = item.getEntrant().getPerson();
                currentBlank = rtfPageTemplate.replaceAll("workNumber", convertToRTFUnicode(item.getExamPassMarkIstu().getWorkCipher()));
                currentBlank = currentBlank.replaceAll("fio", convertToRTFUnicode(item.getEntrantFIO()));
                if (item.getEntrant().getPersonalNumber() != null) {
                    currentBlank = currentBlank.replaceAll("personalNumber", convertToRTFUnicode(item.getEntrant().getPersonalNumber()));
                }
                else {
                    currentBlank = currentBlank.replaceAll("personalNumber", convertToRTFUnicode(" "));
                }
                if (person.getIdentityCard() != null) {
                    IdentityCard doc = person.getIdentityCard();
                    if (doc.getCardType() != null)
                        currentBlank = currentBlank.replaceAll("docType", convertToRTFUnicode(doc.getCardType().getShortTitle()));
                    else
                        currentBlank = currentBlank.replaceAll("docType", convertToRTFUnicode(" "));
                    if (doc.getSeria() == null || doc.getNumber() == null) {
                        currentBlank = currentBlank.replaceAll("docSeria", convertToRTFUnicode(" "));
                        currentBlank = currentBlank.replaceAll("docNumber", convertToRTFUnicode(" "));

                    }
                    else {
                        currentBlank = currentBlank.replaceAll("docSeria", convertToRTFUnicode(doc.getSeria()));
                        currentBlank = currentBlank.replaceAll("docNumber", convertToRTFUnicode(doc.getNumber()));
                    }
                    if (doc.getIssuanceDate() == null && doc.getIssuancePlace() == null)
                        currentBlank = currentBlank.replaceAll("docInfo", convertToRTFUnicode(" "));
                    else {
                        String dinfo = "";
                        if (doc.getIssuanceDate() != null)
                            dinfo += DateFormatter.DEFAULT_DATE_FORMATTER.format(doc.getIssuanceDate());
                        if (doc.getIssuancePlace() != null)
                            dinfo += " " + doc.getIssuancePlace();
                        currentBlank = currentBlank.replaceAll("docInfo", convertToRTFUnicode(dinfo));

                    }
                    currentBlank = currentBlank.replaceAll("subjectTitle"
                            , convertToRTFUnicode(item.getExamPassDisciplineISTU().getExamPassDiscipline().getEnrollmentCampaignDiscipline().getEducationSubject().getTitle()));
                    currentBlank = currentBlank.replaceAll("subjectCode"
                            , convertToRTFUnicode(item.getExamPassDisciplineISTU().getExamPassDiscipline().getEnrollmentCampaignDiscipline().getEducationSubject().getCode()));


                }

                DisciplineDateSettingISTU dateSetting = item.getExamPassDisciplineISTU().getDateSetting();
                if (dateSetting != null && dateSetting.getPassDate() != null) {
                    currentBlank = currentBlank.replaceAll("examPassDate", convertToRTFUnicode(DateFormatter.DEFAULT_DATE_FORMATTER.format(dateSetting.getPassDate())));
                }
                else {
                    currentBlank = currentBlank.replaceAll("examPassDate", convertToRTFUnicode("          "));
                }
                result += currentBlank;
            }

            result += rtfFooter;
        }


        return result.getBytes();
    }

    List<EntrantDisciplineMarkWrapper> getEmptyWorkCodes(Model model) {
        List<EntrantDisciplineMarkWrapper> wrps = model.getDataSourceExt().getEntityList();
        List<EntrantDisciplineMarkWrapper> result = new ArrayList<>();
        for (EntrantDisciplineMarkWrapper item : wrps)
            if (item.getExamPassMarkIstu() == null || item.getExamPassMarkIstu().getWorkCipher() == null)
                result.add(item);
        return result;

    }

    private String convertToRTFUnicode(String s) {

        StringBuilder sb = new StringBuilder();
        for (char c : s.toCharArray()) {
            if (c <= 0x7f)
                sb.append(c);
            else
                sb.append("\\\\u" + Integer.valueOf(c) + "?");
        }
        return sb.toString();
    }
}
