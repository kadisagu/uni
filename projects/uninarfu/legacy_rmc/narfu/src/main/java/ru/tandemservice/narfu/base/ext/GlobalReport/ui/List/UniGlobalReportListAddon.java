package ru.tandemservice.narfu.base.ext.GlobalReport.ui.List;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.ui.List.GlobalReportListUI;
import ru.tandemservice.unibasermc.base.bo.StudentReport.ui.List.StudentReportListUI;

public class UniGlobalReportListAddon extends UIAddon {
    public static final String NAME = "uniGlobalReportListAddonNARFU";
    private GlobalReportListUI parent;

    public UniGlobalReportListAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
        this.parent = (GlobalReportListUI) presenter;
    }

    public static final String STUDENT_BLOCK = "student";


    public void onComponentActivate() {

        // Если попали сюда из StudentReportListUI, то ничего не делаем
        if (parent instanceof StudentReportListUI)
            return;

        //Убираем все отчеты модуля Контингент
        for (int i = 0; i < parent.getBlockList().size(); i++)
            if (STUDENT_BLOCK.equals(parent.getBlockList().get(i))) {
                parent.getBlockList().remove(i);
                i--;
            }
    }
}
