package ru.tandemservice.narfu.component.settings.StudentDocumentTypeUsed;

import ru.tandemservice.uni.entity.catalog.StudentCategory;

public interface IDAO extends ru.tandemservice.uni.component.settings.StudentDocumentTypeUsed.IDAO {

    public final static String DELIMETER = "_";

    public void updateRelation(Model model, Object[] parameters);

    public String getViewPropertyKey(StudentCategory studentCategory);
}
