package ru.tandemservice.narfu.unisession.component.sessionBulletin.SessionBulletinPub;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPub.IDAO;
import ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPub.Model;

public class Controller extends ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPub.Controller {

    @Override
    public void onClickPrint(IBusinessComponent component) {
        Model model = getModel(component);
        ((IDAO) getDao()).printBulletin(model);
    }

}
