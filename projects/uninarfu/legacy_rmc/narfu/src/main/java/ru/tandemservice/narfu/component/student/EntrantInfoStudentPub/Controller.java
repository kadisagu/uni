package ru.tandemservice.narfu.component.student.EntrantInfoStudentPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import ru.tandemservice.uni.entity.employee.Student;


public class Controller extends AbstractBusinessController<IDAO, Model> {


    @Override
    public void onActivateComponent(IBusinessComponent component) {
        super.onActivateComponent(component);
        activate(component);
    }

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        super.onRefreshComponent(component);
        activate(component);
    }

    private void activate(IBusinessComponent component) {
        final Student student = ((ru.tandemservice.uni.component.student.StudentPub.Model) component.getModel(component.getName())).getStudent();
        getModel(component).setStudent(student);
    }
}
