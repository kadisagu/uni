package ru.tandemservice.narfu.component.settings.FormingOrgUnit2Commission.List;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.organization.base.entity.OrgUnit;


public class Wrapper extends IdentifiableWrapper {

    private OrgUnit _formingOrgUnit;
    private OrgUnit _commission;


    public Wrapper(OrgUnit formingOrgUnit, OrgUnit commission)
    {
        super(formingOrgUnit.getId(), formingOrgUnit.getTitle());
        this._formingOrgUnit = formingOrgUnit;
        this._commission = commission;

    }

    public OrgUnit getFormingOrgUnit() {
        return _formingOrgUnit;
    }

    public OrgUnit getCommission() {
        return _commission;
    }
}
