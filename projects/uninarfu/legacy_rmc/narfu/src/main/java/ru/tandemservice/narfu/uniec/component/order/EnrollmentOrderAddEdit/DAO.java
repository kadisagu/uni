package ru.tandemservice.narfu.uniec.component.order.EnrollmentOrderAddEdit;

import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.narfu.entity.EnrollmentOrderNARFU;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Course;

import java.util.Calendar;
import java.util.Date;

public class DAO extends ru.tandemservice.uniec.component.order.EnrollmentOrderAddEdit.DAO {

    @Override
    public void prepare(ru.tandemservice.uniec.component.order.EnrollmentOrderAddEdit.Model model) {

        super.prepare(model);

        Model myModel = (Model) model;

        myModel.setCourseListModel(new FullCheckSelectModel() {
            public ListResult<Course> findValues(String filter) {
                return new ListResult<>(DevelopGridDAO.getCourseList());
            }
        });

        if (model.getOrder().getCommitDate() == null)
            model.getOrder().setCommitDate(new Date());

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, Calendar.SEPTEMBER);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        model.getOrder().setEnrollmentDate(calendar.getTime());

        EnrollmentOrderNARFU enrollmentOrderNARFU = get(EnrollmentOrderNARFU.class, EnrollmentOrderNARFU.enrollmentOrder(), model.getOrder());

        if (enrollmentOrderNARFU == null) {
            enrollmentOrderNARFU = new EnrollmentOrderNARFU();
            enrollmentOrderNARFU.setEnrollmentOrder(model.getOrder());
            enrollmentOrderNARFU.setProtocolDate(new Date());
            enrollmentOrderNARFU.setExecutorPost("Ректор");
            enrollmentOrderNARFU.setExecutorFIO("Е.В. Кудряшова");
        }

        ((Model) model).setOrderNarfu(enrollmentOrderNARFU);

    }

    @Override
    public void update(ru.tandemservice.uniec.component.order.EnrollmentOrderAddEdit.Model model, ErrorCollector errors) {


        super.update(model, errors);

        saveOrUpdate(((Model) model).getOrderNarfu());

    }


}
