package ru.tandemservice.narfu.base.bo.SessionReportNarfu.ui.SummaryBulletinAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;

@Configuration
public class SessionReportNarfuSummaryBulletinAdd extends
        BusinessComponentManager
{


    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(SessionReportManager.instance().eduYearDSConfig())
                .addDataSource(SessionReportManager.instance().yearPartDSConfig())
                .addDataSource(UniStudentManger.instance().courseDSConfig())
                .addDataSource(selectDS("resultDS", SessionReportManager.instance().resultsDSHandler()))
                .addDataSource(selectDS("discObligationsDS", SessionReportManager.instance().discKindDSHandler()))
                .create();
    }
}
